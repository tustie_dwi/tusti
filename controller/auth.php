<?php
class controller_auth extends comscontroller {

	function __construct() {
		parent::__construct();
		$this->session = new session( $this->config->session );
	}
	
	function index() {
		if(isset($_POST['b_submit'])){
			$this->logon();
			exit();
		}
		
		
		$this->add_script('js/auth/index.js');
		$this->view( 'auth2.php');
		//$this->view( 'page/index.php', $data );
		$data['msglogin'] = "";
		//$this->view( 'page/index.php', $data );
	}
	
	function logon() {
		$user = new model_user();
		
		if( $authenticatedUser = $user->authenticate( $_POST['username'], $_POST['password'] ) ) {
			$this->session->set("user", $authenticatedUser);
			$this->redirect('apps');
		} else {
			
			$data['post'] = false;//$mpage->read_content("", 'about');
					
		
			$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">Invalid login. Enter username and password correctly.</div>';
			$this->view('auth2.php', $data );
			exit;
		}
		
	}
	
	function logoff($username) {
		$user 		= new model_user();
		$user->log($username, 'logout');
		
		$this->session->destroy();
		$this->redirect('apps');
	}
}
?>