<?php
class controller_info extends comscontroller {

	function __construct() {
		parent::__construct();
		// /$this->require_auth('page');
	}
	
	
	function skripsi_json(){
		$mconf= new model_info();
		
		$row = $mconf->get_skripsi();
		$skripsi;
		foreach($row as $key){
			if($key->dosen1) {
				if(isset($url)){
					$link1= '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location($url.'/info/staff/'.$key->id1).'>'.$key->dosen1;
				}else{
					$link1= '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location('info/staff/'.$key->id1).'>'.$key->dosen1;
				}
				
				$dosen1 = $key->dosen1;
				if($key->gelar_awal) $dosen1.= ', '.$key->gelar_awal;
				if($key->gelar_akhir) $dosen1.= ', '.$key->gelar_akhir;
				
			}
			
			if($key->dosen2) {
				if(isset($url)){
					$link2= '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location($url.'/info/staff/'.$key->id2).'>'.$dt->dosen2;
				}else{
					$link2= '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location('info/staff/'.$key->id2).'>'.$key->dosen2;
				}
				
				$dosen2 = $key->dosen2;
				if($key->gelar_awal) $dosen2.= ', '.$key->gelar2;
				if($key->gelar_akhir) $dosen2.= ', '.$key->akhir2;
				
			}
						
			$tmp = array(
				'judul' =>$key->skripsi_judul,
				'status_id'=>$key->id,
				'dosen1'=>$dosen1,
				'dosen2'=>$dosen2,
				'link1'=>$link,
				'link2'=>$link2,
				'skripsi_jenis'=>$key->skripsi_jenis,
				'mhs_nim'=>$key->mhs_nim,
				'mhs_nama'=>$key->nama,
				'skripsi_status'=>$key->skripsi_status,
				'skripsi_tgl_start'=>$key->skripsi_tgl_start,
				'skripsi_tgl_semhasil'=>$key->skripsi_tgl_semhasil,
				'skripsi_tgl_sidang'=>$key->skripsi_tgl_sidang,
				'tempat'=>$key->tempat
			);
			
			if(! isset($skripsi[$key->id])) $skripsi[$key->id] = array();
			array_push($skripsi[$key->id], $tmp);
			
		}
		echo json_encode($skripsi);
	}
	
	 function get_data_skripsi($val=NULL){
		
		if($val) $id=$val;
		else $id=$_POST['id'];
		
		$mconf = new model_info();
		
		if($id!="-") $data['detail']= $mconf->get_skripsi($id, 0);		
		else $data['detail']="";
		//$data['mhs']=$mconf->get_yudisium_mhs($id);
		
		$this->view('info/skripsi/skripsi-detail.php', $data );
		
	}
	
	function skripsi(){
		$this->tesis();
		exit();
	}
	
	function tesis(){
			
		$mpage 		= new model_page();
				
		$data['lang'] = $this->config->lang;
		
	
		
		$data['unit'] = '0';
				
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');		
		
		$data['mpage'] 	= $mpage;		
		
		$data['view_slide']= 0;		
				
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['gallery'] = $mpage->read_slide('0',$data['lang'],8,"'video'");		
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news');
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		
		$mconf = new model_info();
		
		$data['posts'] = $mconf->get_skripsi_status();
		$data['mconf'] = $mconf;
								
		$this->view('info/skripsi/skripsi.php', $data );
	}
	
	
	
	
	function sitemap(){
		$mpage 		= new model_page();
		
		$data['lang'] 	= $this->config->lang;
		$data['unit'] ='0';
		
		$data['mpage']	= $mpage;
		
		$data['view_slide']= 0;
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['posts'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['gallery'] = $mpage->read_slide('0',$data['lang'],8,"'video'");		
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news');
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
	
		$this->view( 'info/general/sitemap.php', $data);
	}

	function services(){
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		$lang = $data['lang'];
		
		////$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');
		/*--- end page -- */	
		
		$data['apps'] = $mpage->read_content('0',$lang,'apps');
		
		$this->view('info/general/services.php', $data );
	}
	
	function yudisium(){
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		////$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');
		/*--- end page -- */	
		
		$mconf = new model_info();
		
		$data['posts'] = $mconf->get_yudisium();
		$data['mconf'] = $mconf;
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view('info/general/yudisium.php', $data );
		
	}
	
	function get_data_yudisium($str=NULL){
		if($str) $id=$str;
		else $id=$_POST['id'];
		
		$mconf = new model_info();
		
		$data['detail']= $mconf->get_yudisium($id);		
		$data['mhs']=$mconf->get_yudisium_mhs($id);
		
		$this->view('info/general/yudisium-detail.php', $data );
		
	}
	
	function daftar_krs($data=NULL){
	
				
		$mconf 		= new model_confinfo();
			
		$row 		= $mconf->get_semester_aktif();
		$semesterid = substr($row->tahun_akademik,0,7)."1";
		$data['semester'] 	= $semesterid;		
		$tgl				= date("Y-m-d");
		
		
		$data['kalender'] = $mconf->kalender_akademik('krs', $semesterid, $tgl);
		
		$this->add_style('ptiik/css/print.min.css');
		$this->add_script('js/info/krs.js');	
				
		$this->view( 'info/akademik/krs.php', $data );
	}
	
	function delete_sp() {
		$id = $_POST['id'];
		$mconf = new model_confinfo();
		$result = array();
		if($mconf->delete_sp($id)) {
			$result['nstatus'] = "OK";
			$result['error'] = $mconf->error;			
		} else {
			$result['nstatus'] = "NOK";
			$result['error'] = $mconf->error;
		}
		echo json_encode($result);
	}
	
	function daftar_krs_admin($data=NULL){
		
		
		$mconf 		= new model_confinfo();
			
		$row 		= $mconf->get_semester_aktif();
		$semesterid = substr($row->tahun_akademik,0,7)."1";
		$data['semester'] 	= $semesterid;		
		$tgl				= date("Y-m-d");
		
		$data['kalender'] = $mconf->kalender_akademik('krs', $semesterid, $tgl);
		$this->add_style('ptiik/css/print.min.css');				
		$this->add_script('js/info/krs.js');	
				
		$this->view( 'info/akademik/krs_admin.php', $data );
	}
	
	function cetak_kuitansi($semester=NULL, $mhs=NULL){	
		if($semester):
			$mhs 		= $mhs;
			$semester	= $semester;
		else:
			$mhs 		= $_POST['hidmhs'];
			$semester	= $_POST['hidsemester'];
		endif;
		
		$minfo = new model_info();

		$mconf = new model_confinfo();
		
		$data['semesterid'] = $semester;		
		$data['mhs']	= $minfo->get_mhs($mhs);
		$data['krs']	= $mconf->get_krs_tmp_mhs($semester, $mhs);
		$data['mconf']	= $mconf;
		
		$this->view("info/akademik/cetak_kuitansi.php", $data);
		
	}
	
	function save_daftar_pendek($is_valid=NULL){
	
		$mconf = new model_confinfo();
		
		$mhs 	= $_POST['hidmhs'];
		$semester	= $_POST['hidsemester'];
		if(isset($_POST['hidtgl'])){
			$tgldaftar = $_POST['hidtgl'];
		}else{
			$tgldaftar =date("Y-m-d H:i");
		}
		
		$mkid 	= $_POST['mkid'];
		$mk	  	= $_POST['mk'];
		$sks	= $_POST['sks'];
		$biaya	= $_POST['biaya'];
		$kelas	= $_POST['kelas'];
		
		if(isset($_POST['hidtmpmhs'])){
			$datanya=array('tahun_akademik'=>$semester, 'mahasiswa_id'=>$mhs);
			$mconf->delete_daftar_pendek($datanya);
		}
		
		if(isset($_POST['hidvalid'])&&($_POST['hidvalid'])){
			if($_POST['hidvalid']=='1') $strvalx =3;
			else $strvalx =$_POST['hidvalid'];
		}else{
			$strvalx =1;
		}
		if($is_valid!="")  $strval = $is_valid;
		else $strval = $strvalx;
		
				
		for($i=0;$i<count($mkid);$i++){
			$kode 	= $mconf->get_reg_daftar_pendek($semester, $mhs, $mkid[$i]);
			$str	= explode("-", $mk[$i]);
			$kodemk	= $str[0];
			$namamk	= $str[1];
			
						
			if(isset($_POST['hidtmp'])):
				$kodekrs = $mconf->get_reg_daftar_pendek_tmp($semester, $mhs, $mkid[$i]);
				
				if($kodekrs){
					$idnya = array("krs_id"=>$kodekrs->krs_id);
					
					
					$datanya = array('tgl_daftar'=>$tgldaftar, 'user_id'=>$_POST['hiduser'], 'last_update'=>date("Y-m-d H:i"), 'is_valid'=>$strval);
					
					$mconf->update_daftar_pendek($datanya, $idnya);
				}else{
					$datanya = array('tgl_daftar'=>$tgldaftar, 'krs_id'=>$kode, 'tahun_akademik'=>$semester, 'mahasiswa_id'=>$mhs, 'mkditawarkan_id'=>$mkid[$i], 'kode_mk'=>$kodemk, 'nama_mk'=>$namamk, 'sks'=>$sks[$i], 'kelas'=>$kelas[$i], 'is_approve'=>1, 'inf_biaya'=>$biaya[$i],'user_id'=>$_POST['hiduser'], 'last_update'=>date("Y-m-d H:i"), 'is_valid'=>$strval);
					$mconf->replace_daftar_pendek($datanya);
				}
			else:		
				$datanya = array('tgl_daftar'=>$tgldaftar, 'krs_id'=>$kode, 'tahun_akademik'=>$semester, 'mahasiswa_id'=>$mhs, 'mkditawarkan_id'=>$mkid[$i], 'kode_mk'=>$kodemk, 'nama_mk'=>$namamk, 'kelas'=>$kelas[$i], 'sks'=>$sks[$i], 'is_approve'=>0, 'inf_biaya'=>$biaya[$i]);
				
				$mconf->replace_daftar_pendek($datanya);
			endif;
		
			
			
		}
		
		$idnya = array('mahasiswa_id'=>$mhs);
		$datanya = array('email'=>$_POST['email'],  'telp'=>$_POST['telp'], 'hp'=>$_POST['hp'],
					'dosen_pembimbing'=>$_POST['cmbpembimbing'], 'last_update'=>date("Y-m-d H:i"));		
		$save 	= $mconf->update_mhs($datanya, $idnya);
				
		
		if( $save ) {
			$result['status'] = "OK";
			$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
		} else {
			$result['status'] = "NOK";
			$result['error'] = $mconf->error;
		}
		echo json_encode($result);
	}
	
	
	function akademik($str=NULL, $id=NULL){
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;		
	
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');
		/*--- end page -- */	
		
		switch($str){
			case 'krs':
				if(isset($_POST['inputmhs'])){
					$this->get_data_mhs_krs($data);				
				}else{
					$this->daftar_krs($data);	
				}
			break;
			case 'daftar':
				if(isset($_POST['nim_mhs'])){
					$this->daftar_ulang($_POST['nim_mhs'],"",$data);				
				}else{
					$this->daftar_ulang("","",$data);				
				}				
			break;
			case 'pilihan':				
					$this->pilihan_mk($id,$data);					
			break;
			case 'save':
				$this->save_daftar_ulang();	
			break;
			default:
				$this->daftar_ulang("","",$data);	
			break;
		}
		exit();
	}
	/*  daftar ulang */
	function daftar_ulang($str=NULL, $msg=NULL,$data=NULL){
		if(isset($_POST['input'])){
			$inputflag 	= md5($_POST["input"]);
			$sessiontmp = $_POST["flag"];
		}else{
			$inputflag 		= "";
			$sessiontmp = "";
		}
		
		
		
			$mservices	= new model_services();
			$mconfinfo	= new model_confinfo();					
						
			$tgl		= date("Y-m-d");
			
			$thn_akademik 		= $mconfinfo->get_semester_aktif()->tahun_akademik;
		
			$data['kalender'] = $mconfinfo->kalender_akademik('daftar', $thn_akademik , $tgl);
				
			
			if(isset($str)&& ($inputflag == $sessiontmp)){
				$nim				= $str;
						
				$cek_data_masuk		= $mservices->read_daftar_ulang($thn_akademik,$nim,"","","*");
				$cek_data_valid		= $mservices->read_daftar_ulang($thn_akademik,$nim,"","","*","1");
				$data_mhs			= $mservices->get_mhs($nim);
			
					if(!$cek_data_masuk){
						$data['mhs'] 	= $data_mhs;
						if($data['mhs']){						
							if($this->config->lang == "in") $data['msg'] 	= "Data anda telah masuk dan sedang diproses. Silahkan menghubungi akademik untuk informasi lebih lanjut.";
							else $data['msg'] 	= "Your data have entered and still in process. Please contact academics for more information.";
						}else{
							$nim="";
							
							if($this->config->lang == "in") $data['msg'] 	= "Data yang anda masukkan salah. Silahkan cek lagi data anda.";
							else $data['msg'] 	= "Please check your input.";
						}
					}
					else{
						
						if($this->config->lang == "in") $data['msg'] 	= "Data anda telah masuk dan sedang diproses. Silahkan menghubungi akademik untuk informasi lebih lanjut.";
						else $data['msg'] 	= "Your data have entered and still in process. Please contact academics for more information.";
						if($data_mhs) $data['mhs_entered'] 	= $cek_data_masuk;
					}
					
			}else{
				$nim="";
				
			}
			
			$data['nim']	= $nim;
			$data['prodi'] 	= $mconfinfo->get_prodi();
			
			$this->add_style('css/datepicker/datepicker.css');
			$this->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->add_script('js/submit.js');
			
			$this->view('page/services/daftar_ulang.php', $data);
		
	}
	/* end daftar ulang */
	
	function pilihan_mk($str=NULL, $data=NULL){
		
		
	
		if(isset($_POST['inputmhs'])){
			
		
			$this->get_data_mhs_kuesioner();				
		}else{
			$mconf 		= new model_confinfo();
					
			$tgl		= date("Y-m-d");
			
			$data['kalender'] = $mconf->kalender_akademik_kuesioner($tgl);
			if(isset($data['kalender']))	$data['semester'] = $data['kalender']->tahun_akademik;		
			else $data['semester'] = "";
					
			$this->add_script('js/info/form_mk.js');	
					
			$this->view( 'info/akademik/form_pilihan_mk.php', $data );
		}
	}
	
	function get_data_mhs_kuesioner(){
		if(isset($_POST['input'])){
			$inputflag 	= md5($_POST["input"]);
			$sessiontmp = $_POST["flag"];
		}else{
			$inputflag 		= "";
			$sessiontmp = "";
		}

		$minfo = new model_info();

		$mconf = new model_confinfo();
			
		$row 		= $mconf->kalender_akademik_kuesioner();
		
		$kuesioner  = $row->kuesioner_id;
		$data['semester']		= $row->tahun_akademik;
		$data['namasemester']	= $row->tahun. " ".$row->is_ganjil." ".$row->is_pendek;
		$data['kuesioner']		= $kuesioner;
		$angkatan	= (date("Y")-1);
				
		$str 		 	= $_POST['inputmhs'];
		$data['rmhs'] 	= $minfo->get_mhs($str, $angkatan);
		$data['dosen']	= $minfo->get_dosen();
		
		if(($data['rmhs']) && ($inputflag == $sessiontmp) ){
			$data['daftar'] = $minfo->get_mhs_mk_pilihan($kuesioner, $str);
			
			if($data['daftar']){
				$data['mhs'] = $data['daftar'];
			}else{
				$data['mhs'] = $data['rmhs'];
			}
			
			$this->add_script('js/info/tgl.js');
			$this->view( 'info/akademik/form_daftar_mk_pilihan.php', $data );
			
			$result['status'] = "OK";
				
		}else{
			
			echo "NOK";
			
		}
	}
	
	function save_form_mk_pilihan(){
	
	
		$mconf = new model_confinfo();
		
		$mhs 	= $_POST['hidmhs'];
		$kuesioner 	= $_POST['hidkuesioner'];
		$semester	= $_POST['cmbsemester'];
		if(isset($_POST['hidtgl'])){
			$tgldaftar = $_POST['hidtgl'];
		}else{
			$tgldaftar =date("Y-m-d H:i");
		}
		
		$mkid 	= $_POST['mkid'];
		$mk	  	= $_POST['mk'];
		$sks	= $_POST['sks'];
		
		
		for($i=0;$i<count($mkid);$i++){
			$kode 	= $mconf->get_reg_mk_pilihan($semester, $mhs, $mkid[$i],$kuesioner);
			$str	= explode("#", $mk[$i]);
			$kodemk	= $str[0];
			$namamk	= $str[1];
			
			if(isset($_POST['hidtmp'])):
				
			else:		
				$datanya = array('tgl_isi'=>$tgldaftar, 'id'=>$kode, 'kuesioner_id'=>$kuesioner, 'semester'=>$semester, 'mahasiswa_id'=>$mhs, 'matakuliah_id'=>$mkid[$i], 'kode_mk'=>$kodemk, 'nama_mk'=>$namamk, 'sks'=>$sks[$i]);
				
				$mconf->replace_mk_pilihan($datanya);
			
			endif;
		
			
			
		}
		
		$idnya = array('mahasiswa_id'=>$mhs);
		$datanya = array('email'=>$_POST['email'],  'telp'=>$_POST['telp'], 'hp'=>$_POST['hp'],
					'dosen_pembimbing'=>$_POST['cmbpembimbing'], 'last_update'=>date("Y-m-d H:i"));		
		$save 	= $mconf->update_mhs($datanya, $idnya);
		
				
		
		if( $save ) {
			$result['status'] = "OK";
			$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
		} else {
			$result['status'] = "NOK";
			$result['error'] = $mconf->error;
		}
		echo json_encode($result);
	}
	
	
	
	function staff($id=NULL){
	
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		/*--- end page -- */	
		
		$mconf = new model_info();
		
		$data['posts'] = $mconf->get_civitas($id);
		$data['minfo'] = $mconf;
		$data['kategori'] = "staff";
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view('info/general/staff.php', $data );
	
	}
	
	function ujian($str=NULL, $id=NULL, $id_page=NULL){ //fungsi Agie Ghazy F. (jangan di replace)
		$mpage 		= new model_page();
		$mjadwal = new model_jadwalinfo();
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data['jam'] = $mjadwal->get_jam_ujian();
		$data['jenis_ujian'] = $mjadwal->get_jenis_ujian();
		//$data['ruang'] = $mjadwal->get_ruang($this->config->cabang, $this->config->fakultas);
		//$data['dosen'] = $mjadwal->get_dosen($this->config->cabang, $this->config->fakultas);
		//$data['mk'] = $mjadwal->get_matakuliah($this->config->cabang, $this->config->fakultas);
		
		$data['range'] = $mjadwal->get_range_tgl();
		
		$this->add_script('js/jadwalkuliah/ujian.js');
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		$data['view_slide']= 0;
		
		//$data['mimg'] = new imageprocessing();
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news',"","","","",1);
		
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');		
								
		$this->view('info/jadwal_kuliah/ujian.php', $data );
	}
	
	
	function jadwal_ujian(){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal_ujian();
		$jadwal;
		
		foreach($data as $key){
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
						
			$tmp = array(
				'mk_id' => $key->mk_id,
				'matakuliah' => $key->keterangan,
				'kelas' => $key->kelas,
				'hari' => $key->hari,
				'tgl' => $key->tgl,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'ruang' => $key->ruang_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'jenis' => $key->jenis_ujian,
				'lang' => $this->config->lang
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		echo json_encode($jadwal);
	}
	
	
	function terpakai($tgl=NULL, $str=NULL, $id=NULL, $id_page=NULL){
		$mpage 		= new model_page();
		$mruang 	= new model_pemakaianruang();
		
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		$data['view_slide']= 0;
		
		//$data['mimg'] = new imageprocessing();
		
		if($tgl == NULL) $tgl = strtotime(date('Y-m-d'));
		$data['calendar'] = $this->calendar($tgl);
		
		$data['pemakaian'] = $mruang->get_pemakaian_ruang($tgl);
		$data['ruang'] = $mruang->get_ruang();
		
		$this->add_script('highchart/highcharts.js');
		$this->add_script('highchart/modules/exporting.js');
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news',"","","","",1);
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	
		
		$this->view('info/reservasi/pemakaian.php', $data );
	}
	
	function calendar($tgl=NULL, $style='calendar'){
		$strtgl = $tgl;
		$nid = strtotime('+1 month',$strtgl);
		$pid = strtotime('-1 month',$strtgl);
		$month= date("m", $strtgl);
		$nmonth= date("M", $strtgl);
		$year= date("Y", $strtgl);
		$title= date("F Y", $strtgl);
		$today=strtotime(date("Y-m-d"));
		
         if((substr($month, 0, 1)) == 0)
         {
            $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         $str = '<table class="table calendar-date-mini">';            
         $headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
         $str.= '<thead><tr><td colspan=2 align="right"><a href='.$this->location('info/terpakai/'.$pid).'><</a></td><td colspan="3" align="center"><b>'.$title.'</b></td>
				<td colspan=2 align="left"><a href='.$this->location('info/terpakai/'.$nid).'>></a></td></tr>
				<tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
				 $stra = strtotime($year."-".$month."-".$list_day);
                 $str.= '<div class="'. $style .'-day-number"><a href='.$this->location('info/terpakai/'.$stra).'><span class="text text-default">';
				if($stra==$strtgl){
					$str.= '<span class="label label-success">'.$list_day.'</span>';
				}else{
					$str.= $list_day;
				 }
				 $str.= '</span></a></div></td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
             
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
         $str.= '</tr>';
		 $str.= '<tr><td colspan=7 align=center  class="'. $style .'-day-head"><a href='.$this->location('info/terpakai/'.$today).'><span class="text text-default">Today</span></a></td></tr>';
		 $str.= '</tbody>';    
         $str.= '</table>';
		 
		 return $str;
	}
	
	function jadwal($str=NULL, $id=NULL, $id_page=NULL){ //fungsi Agie Ghazy F. (jangan di replace)
		$mpage 		= new model_page();
		$mjadwal = new model_jadwalinfo();
		
		$is_pendek = $mjadwal->get_is_pendek();
		
		$data['jam'] = $mjadwal->get_jam($is_pendek);
		$data['ruang'] = $mjadwal->get_ruang($this->config->cabang, $this->config->fakultas);
		$data['dosen'] = $mjadwal->get_dosen($this->config->cabang, $this->config->fakultas);
		$data['mk'] = $mjadwal->get_matakuliah($this->config->cabang, $this->config->fakultas);
		$data['praktikum'] = $mjadwal->get_praktikum($this->config->cabang, $this->config->fakultas);
		
				
		$this->add_script('js/jadwalkuliah/jadwal.js');
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		$data['view_slide']= 0;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news',"","","","",1);
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');		
								
		$this->view('info/jadwal_kuliah/index.php', $data );
	}
	
	
	function academic_cal_(){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		
		$lang = $data['lang'];
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal('UBM', 'PTIIK', $is_pendek, $data['lang']);
		$jadwal;
		
		if($data){
			foreach($data as $key){
				
				if($key->dosen == '') $dosen = 'Dosen Pengampu';
				else $dosen = $key->dosen;
				
				if($key->mk) $strmk = $key->mk;
				else $strmk = $key->mk_ori;
				
				$tmp = array(
					'str_kelas' => $key->strkelas,
					'matakuliah' => $strmk,
					'mk_id' => $key->mk_id,
					'kelas' => $key->kelas,
					'jam_mulai' => substr($key->jam_mulai,0,5),
					'jam_selesai' => substr($key->jam_selesai,0,5),
					'karyawan_id' => $key->karyawan_id,
					'dosen' => $dosen,
					'prodi' => $key->prodi,
					'urut' => $key->urut,
					'urut_selesai' => $key->urut_selesai,
					'praktikum' => $key->is_praktikum,
					'repeat_on' => $key->repeat_on,
					'tgl_mulai' => $key->tgl_mulai,
					'tgl_selesai' => $key->tgl_selesai
				);
				
				if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
				array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
			}
			
		}else{
			$jadwal="";
		}
		echo json_encode($jadwal);
	}
	
	
	function jadwal_prak_json($unit=NULL){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		
		$lang = $data['lang'];
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal_praktikum($this->config->cabang, $this->config->fakultas, $is_pendek, $data['lang'], $unit);
		$jadwal;
		
		foreach($data as $key){
			
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
			
			if($key->mk) $strmk = $key->mk;
			else $strmk = $key->mk_ori;
			
			$tmp = array(
				'str_kelas' => $key->strkelas,
				'matakuliah' => $strmk,
				'mk_id' => $key->mk_id,
				'kelas' => $key->kelas,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'karyawan_id' => $key->karyawan_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'urut_selesai' => $key->urut_selesai,
				'praktikum' => $key->is_praktikum
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		echo json_encode($jadwal);
	}
	
	function jadwal_json_show(){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal($this->config->cabang, $this->config->fakultas, $is_pendek, $data['lang']);
		$jadwal;
		
		foreach($data as $key){
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
			
			if($key->mk) $strmk = $key->mk;
			else $strmk = $key->mk_ori;
			
			$tmp = array(
				'str_kelas' => $key->strkelas,
				'matakuliah' => $strmk,
				'mk_id' => $key->mk_id,
				'kelas' => $key->kelas,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'karyawan_id' => $key->karyawan_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'urut_selesai' => $key->urut_selesai,
				'praktikum' => $key->is_praktikum
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		?>
			<pre><?php print_r($jadwal) ?></pre>
		<?php
	}
	function get_hari($hari){
		switch ($hari) {
			case '1': return 'senin'; break;
			case '2': return 'selasa'; break;
			case '3': return 'rabu'; break;
			case '4': return 'kamis'; break;
			case '5': return 'jumat'; break;
			case '6': return 'sabtu'; break;
			case '7': return 'minggu'; break;
			
			default: return 'minggu'; break;
		}
	}
	function ruang(){
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		/*--- end page -- */	
		
		$mreservasi = new model_reservasi();
		
		$fak = $this->config->fakultas;
		$cabang = $this->config->cabang;
		
		$ruang = '';
		$tgl_mulai = $tgl_selesai = date("Y-m-d");
		
		if( isset($_POST['tgl_mulai'])) $tgl_mulai = $_POST['tgl_mulai'];
		if( isset($_POST['tgl_selesai'])) $tgl_selesai = $_POST['tgl_selesai'];
		
		// if( isset($_POST['ruang'])) $ruang = $_POST['ruang'];
		
		$data['tgl_mulai_select'] = $tgl_mulai;
		$data['tgl_selesai_select'] = $tgl_selesai;
		// $data['ruang_select'] = $ruang;
		
		$this->add_style('css/bootstrap/DT_bootstrap.css');
		
		$this->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->add_script('datepicker/js/moment-2.4.0.js');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		
		$this->add_script('js/reservasi/reservasi.js');	
		
		if(strToTime(date("Y-m-d"))> strToTime($tgl_mulai) && strToTime(date("Y-m-d"))> strToTime($tgl_selesai)){
			$data['kegiatan']="";
		}else{
			$data['kegiatan'] = $mreservasi->get_kegiatan($tgl_mulai, $tgl_selesai, $ruang);
		}
		$data['ruang'] = $mreservasi->get_ruang($cabang, $fak, $ruang);		
		$data['ruang_all'] = $mreservasi->get_ruang($cabang, $fak, "");	
		$data['jenis_kegiatan'] = $mreservasi->get_jeniskegiatan();
		$this->view('info/reservasi/order.php', $data);
	}

	function mutu($str=NULL){
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		$data['unit_mutu']	= $mpage->read_mutu_unit($str);
		
		$data['kategori']	= $str;
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view( 'info/mutu/index.php', $data );
	}
	
	function hadir() {
		$mconf 		= new model_info();
		$magenda 	= new model_agendainfo();	
		
		//$data['mimg'] = new imageprocessing();
		
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		/*--- end page -- */	
		
		if(isset($_POST['tgl'])){
			$tgl	 = $_POST['tgl'];
		}else{
			$tgl	 = date('Y-m-d');
		}	
		
		
		$data['absen'] 			= $mconf->get_finger_print($tgl);
		$data['hdosen'] 		= $mconf->get_civitas("","dosen");
		$data['hstaff'] 		= $mconf->get_civitas("","staff");
		$data['dagenda']		= $mconf->get_agenda_civitas($tgl,"dosen");
		$data['sagenda']		= $mconf->get_agenda_civitas($tgl,"staff");
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view( 'info/absen/index.php', $data );
	}
	
	function vw($type=NULL, $str=NULL){
		$allowed = "/[^a-z0-9]/i";
	
		$id = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9+]/', '', urldecode(html_entity_decode(strip_tags($str))))));
		if(empty($id)) {
			$this->notfound('error tidak diketemukan');
			exit;
		}
		switch($type){
			case 'agenda';
				$mconf 				= new model_agendainfo();
				$data['posts']		= $mconf->get_detail_agenda($id);
				$data['pemateri']	= $mconf->get_peserta_agenda($id, 'pemateri','-');
				$data['undangan']	= $mconf->get_peserta_agenda($id, 'undangan');
				$data['staff']		= $mconf->get_peserta_agenda($id, 'peserta','staff');
				$data['peserta']	= $mconf->get_peserta_agenda($id, 'peserta','luar');
				$data['mhs']		= $mconf->get_peserta_agenda($id, 'peserta','mhs');
				$data['panitia']	= $mconf->get_peserta_agenda($id, 'panitia','-');		

				$this->view( 'info/hadir/view.php', $data );
			break;
			
			case 'ruang';
				$mconf = new model_ruanginfo();
			break;
		}
		
		
	}
	
		function details($str=NULL, $type=NULL, $style='calendar'){
		$url= $this->location('info/vw');
		
		$allowed = "/[^a-z0-9]/i";
		
		$id = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9+]/', '', urldecode(html_entity_decode(strip_tags($str))))));
		// if(empty($id)) {
			// $this->notfound('error tidak diketemukan');
			// exit;
		// }
		
		$magenda = new model_agendainfo();	
		$mconf = new model_info();
		
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		/*--- end page -- */	
		
		$data['posts'] = "";
		$data['type']  = $type;		

		$this->add_style('css/calendar/calendar.css');
		
		if(isset($_POST['month'])){
			$month = $_POST['month'];
		}else{
			$month = date('m');
		}
		
		if(isset($_POST['year'])){
			$year = $_POST['year'];
		}else{
			$year = date('Y');
		}
		
		if(isset($_POST['hidid'])){
			$data['civitasid']	= $_POST['hidid'];
		}else{
			$data['civitasid']	= $id;
		}
		
		if(isset($_POST['tgl'])){
			$tgl	 = $_POST['tgl'];
		}else{
			$tgl	 = date('Y-m-d');
		}	
		
		if(isset($_POST['hidmonth'])){
			$data['hidmonth']	= $_POST['hidmonth'];
		}else{
			$data['hidmonth']	= "";
		}
		
		if(isset($_POST['hidtgl'])){
			if(isset($_POST['b_now'])){
				$hidtgl	 = date('Y-m-d');
			}else{
				if(isset($_POST['b_prev'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('-7 days', $in));	
				}else if(isset($_POST['b_next'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('+7 days', $in));	
				}else{
					$hidtgl	 = $_POST['hidtgl'];
				}
			}
		}else{
			$hidtgl	 = date('Y-m-d');
		}	
		
		
		$allowed = "/[^a-z0-9]/i";
		$month = trim(preg_replace('/ +/', ' ', preg_replace($allowed, '', urldecode(html_entity_decode(strip_tags($month))))));
		$year  = trim(preg_replace('/ +/', ' ', preg_replace('/[^0-9+]/', '', urldecode(html_entity_decode(strip_tags($year))))));
		
		if(empty($year) && empty($month)) {
			$this->notfound('error tidak diketemukan');
			exit;
		}
		
		$this->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->add_script('datepicker/js/moment-2.4.0.js');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
			
		$data['month'] 		= $month;
		$data['year']  		= $year;
		$data['title'] 		= date('F Y',mktime(0,0,0,$month,1,$year));
		$data['tgl']		= $tgl;
		$data['hidtgl']		= $hidtgl;
		$data['hadir'] 		= $mconf->get_civitas($data['civitasid']);
		$data['absen']   	= $mconf->get_finger_print($tgl);	
		$data['kegiatan']	= $mconf->get_agenda_civitas($tgl,"",$data['civitasid']);
		$data['posts']    	= $this->draw_month_hadir($month, $year, $data['civitasid'], $style, $url);
		$data['weekdata']  	= $this->draw_week_hadir($month, $year, $hidtgl, $data['civitasid'], $style, $url);
		
		$this->view( 'info/hadir_2/index.php', $data );
	}

	function draw_month_hadir($month, $year, $uid, $style, $url=NULL) {
		$mhadir = new model_hadir();	
				
		 if(($month == NULL) || ($year == NULL))
         {
             // Month in numbers with the leading 0
             $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
         }else{
			 $title = date('F Y',mktime(0,0,0,$month,1,$year));
		 }

		/* We need to take the month value and turn it into one without a leading 0 */
         if((substr($month, 0, 1)) == 0)
         {
             // if value is between 01 - 09, drop the 0
             $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         /* draw table */
		
         $str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
        
         /* table headings */
         $headings = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
         $str.= '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         /* days and weeks vars now ... */
         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         /* row for week one */
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         /* print "blank" days until the first of the current week */
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 $list_day=0;
		 $skip = false;
		 
		 $data = $mhadir->get_kegiatan("", $month, $year, $running_day, "",$uid);
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
            
             /* add in the day number */
             $str.= '<div class="'. $style .'-day-number">'.$list_day.'</div>';
			 $tgl	= $year."-".$mhadir->addNol($month)."-".$mhadir->addNol($list_day);
			 if($data){
				$k=0;
				foreach($data as $row):
					if($row->tgl == $tgl) {
					 	$k++;
						$this->get_label($icon, $note, $sclass, $tclass, $row);
						$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
								<a href="#"><small><span class="'.$tclass.'"><i class="fa  fa-clock-o"></i> '.$row->jam_mulai.", R.".$row->ruang.", ".$note.'</span></small></a></div>';						 							
					}
				endforeach;
			}
			else{
				$str.= "&nbsp;";
			}	
									
            $str.= '</td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
    
         /* finish the rest of the days in the week */
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
    
         /* final row */
         $str.= '</tr>';
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
     }
     
	 function draw_week_hadir($month, $year, $tgl, $uid,$style, $url=NULL){
	 	$mhadir = new model_hadir();	
		// set current date
		$in = strtotime(date('Y-m-d'));
		$date = $tgl;
		$month= date("m", strtotime($date));
		$nmonth= date("M", strtotime($date));
		$nyear= date("Y", strtotime($date));
		
		$ts = strtotime($date);
		
		$year = date('o', $ts);
		$week = date('W', $ts);		
		
		$data = $mhadir->get_kegiatan("", $month, $year, "", "", $uid);
		// $data = $mhadir->get_kegiatan("", $month, $year, $running_day, "",$uid);
				
		$str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
      	
		$str.= '<thead><tr class="'. $style .'-row">';
		for($i = 0; $i < 7; $i++) {
			
			$ts = strtotime($year.'W'.$week.$i);
			$tw = date("N", $ts);
			$td = date("D", $ts);
			$tn = date("Y-m-d", $ts);
			//print date("m/d/Y l", $ts) . "\n";
			if($tw==$i){
				$tp = $ts;				
			 }else{
				$tp = strtotime(date($tn, strtotime('last day')));
				
			 }
			$list_day = date("d", $tp);	
			$str.= '<td class="'. $style .'-day-head">'.$td.", ".$list_day.'</td>';
		}
		/* final row */
         $str.= '</tr></thead><tbody>';
		 $blokwaktu	= $mhadir->get_blok_waktu();
			$str.= '<tr>';
					for($i = 0; $i < 7; $i++) {
						
						$ts = strtotime($year.'W'.$week.$i);
						$tw = date("N", $ts);
						$tn = date("Y-m-d", $ts);
						
						if($tw==$i){
							$tp = $ts;
									
						 }else{
							$tp = strtotime(date($tn, strtotime('last day')));
							
						 }
						 
						 $list_day = date("d", $tp);
						 $mmonth = date("m", $tp);
						 
						if($list_day == date("j",mktime(0,0,0,$month))) {
							$tstyle = $style .'-current-day';
						}else{
							$tstyle = $style;
						}
						
						$tgl	= $year."-".$mhadir->addNol($month)."-".$list_day;
						
						$str.= '<td width="10%" class="'.$tstyle.'">';	 
						 if($data){
							$k=0;
							foreach($data as $row):
								if($row->tgl == $tgl) {
								 	$k++;
									$this->get_label($icon, $note, $sclass, $tclass, $row);
									
									$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
											<a href="#"><small><span class="'.$tclass.'"><i class="fa  fa-clock-o"></i> '.$row->jam_mulai.", R.".$row->ruang.", ".$note.'</span></small></a></div>';						 							
								}
							endforeach;
								
							}else{
								$str.= "&nbsp;";
							}							
							
							
						 $str.= "</td>";
					}
			$str.= '</tr>';
		 //endforeach;
		 
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
	}

	function get_label(&$icon, &$note, &$sclass, &$tclass, $row){
		$mhadir = new model_hadir();
		switch (strToLower($row->jenis)){
			case 'konseling':
				$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'konseling';
				$tclass= 'text-putih';
			break;
			
			case 'rapat':
				$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'rapat';
				$tclass= 'text-putih';
			break;
			
			case 'kemahasiswaan':
				$icon = "";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'mhs';
				$tclass= 'text-putih';
			break;
			
			case 'kuliahtamu':
				$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'kuliahtamu';
				$tclass= 'text-putih';
			break;
			
			case 'wisuda':
				$icon = "<i class='fa fa-star'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'wisuda';
				$tclass= 'text-putih';
			break;
			
			case 'kunjungan':
				$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'kunjungan';
				$tclass= 'text-putih';
			break;
			
			case 'rekrutmen':
				$icon = "<i class='fa fa-signal'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'rekrutmen';
				$tclass= 'text-putih';
			break;
			
			case 'penelitian':
				$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'success';
			break;
			
			case 'pelatihan':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'pelatihan';
				$tclass= 'text-putih';
			break;
			
			case 'workshop':
				$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'workshop';
				$tclass= 'text-putih';
			break;
			case 'seminar':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'seminar';
				$tclass= 'text-putih';
			break;
			case 'porseni':
				$icon = "<i class='fa fa-flag'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'porseni';
				$tclass= 'text-putih';
			break;
			
			case 'uts':
				if($row->keterangan=='Pengawas'){
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}else{
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}								
			break;
			case 'uas':
				if($row->keterangan=='Pengawas'){
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}else{
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}								
			break;
			
			case 'kuliah':
				$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
				$note = $row->kegiatan." ".$row->namamk;	
				$sclass= 'success';	
				$tclass= 'text-putih';
			break;
			default:
				$icon = "<i class='fa  fa-clock-o'></i>".$row->jam_mulai;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'info';
				$tclass= 'text-putih';
			break;
		}
	}
	
	function details_old($str=NULL, $type=NULL, $style='calendar'){
		$url= $this->location('info/vw');
		
		$allowed = "/[^a-z0-9]/i";
		
		$id = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9+]/', '', urldecode(html_entity_decode(strip_tags($str))))));
		if(empty($id)) {
			$this->notfound('error tidak diketemukan');
			exit;
		}
		
		$magenda = new model_agendainfo();	
		$mconf = new model_info();
		
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		/*--- end page -- */	
		
		$data['posts'] = "";
		$data['type']  = $type;		

		$this->add_style('css/calendar/calendar.css');
		
		if(isset($_POST['month'])){
			$month = $_POST['month'];
		}else{
			$month = date('m');
		}
		
		if(isset($_POST['year'])){
			$year = $_POST['year'];
		}else{
			$year = date('Y');
		}
		
		if(isset($_POST['hidid'])){
			$data['civitasid']	= $_POST['hidid'];
		}else{
			$data['civitasid']	= $id;
		}
		
		
		if(isset($_POST['tgl'])){
			$tgl	 = $_POST['tgl'];
		}else{
			$tgl	 = date('Y-m-d');
		}	
		
		if(isset($_POST['hidmonth'])){
			$data['hidmonth']	= $_POST['hidmonth'];
		}else{
			$data['hidmonth']	= "";
		}
		
		if(isset($_POST['hidtgl'])){
			if(isset($_POST['b_now'])){
				$hidtgl	 = date('Y-m-d');
			}else{
				if(isset($_POST['b_prev'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('-7 days', $in));	
				}else if(isset($_POST['b_next'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('+7 days', $in));	
				}else{
					$hidtgl	 = $_POST['hidtgl'];
				}
			}
		}else{
			$hidtgl	 = date('Y-m-d');
		}	
		
		
		$allowed = "/[^a-z0-9]/i";
		$month = trim(preg_replace('/ +/', ' ', preg_replace($allowed, '', urldecode(html_entity_decode(strip_tags($month))))));
		$year  = trim(preg_replace('/ +/', ' ', preg_replace('/[^0-9+]/', '', urldecode(html_entity_decode(strip_tags($year))))));
		
		if(empty($year) && empty($month)) {
			$this->notfound('error tidak diketemukan');
			exit;
		}
				
		$data['month'] 		= $month;
		$data['year']  		= $year;
		$data['title'] 		= date('F Y',mktime(0,0,0,$month,1,$year));
		$data['tgl']		= $tgl;
		$data['hidtgl']		= $hidtgl;
		$data['hadir'] 		= $mconf->get_civitas($data['civitasid']);
		$data['absen']   	= $mconf->get_finger_print($tgl);	
		$data['kegiatan']	= $mconf->get_agenda_civitas($tgl,"",$data['civitasid']);
		/*$data['agenda']		= $mconf->get_civitas_detail($tgl);	
		$data['ujian']		= $magenda->getPengawasByDate($tgl);	*/		
		$data['posts']    	= $magenda->draw_month_hadir($month, $year, $data['civitasid'], $style, $url);
		$data['weekdata']  	= $magenda->draw_week_hadir($month, $year, $hidtgl, $data['civitasid'], $style, $url);
		
		$this->view( 'info/hadir/index.php', $data );
		
		
	}
	
	function map(){
		$mconf = new model_info();
		$data['post'] 	= $mconf->get_lokasi();
		
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		//$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		/*--- end page -- */	
			
		
		if(isset($_POST['cmblokasi'])){
			$data['cmblokasi'] = $_POST['cmblokasi'];
		}else{
			$data['cmblokasi'] = 'A1';
		}
		
		if($data['cmblokasi']!='0'){
			$data['dosen'] = $mconf->get_civitas("",'dosen',$data['cmblokasi']);
			$data['staff'] = $mconf->get_civitas("",'staff',$data['cmblokasi']);
		}else{
			$data['dosen'] = "";
			$data['staff'] = "";
		}
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view( 'info/map/index.php', $data );
	}

	function save_daftarulang(){
		$mservices	= new model_services();
		$mpage 		= new model_page();
		$mconfinfo	= new model_confinfo();
		
		if(isset($_POST['daftar_id'])&&$_POST['daftar_id']!=""){
			$daftarid	= $_POST['daftar_id'];
		}else $daftarid	= $mservices->daftar_id();
		
		$thn_akademik 	= $mconfinfo->get_semester_aktif()->tahun_akademik;
		$mhs_id			= $_POST['hidId_mhs'];
		$nama_mhs		= $_POST['nama_mhs'];
		$tmpt_lahir_mhs	= $_POST['tmpt_lahir_mhs'];
		$tgl_lahir_mhs	= $_POST['tgl_lahir_mhs'];
		$tgl_daftar		= date("Y-m-d H:i:s");
		$jalur_masuk	= $_POST['jalur_seleksi_mhs'];
		$email_mhs		= $_POST['email_mhs'];
		$alamat_mhs		= $_POST['alamat_mhs'];
		$tlp_mhs		= $_POST['tlp_mhs'];
		$hp_mhs			= $_POST['hp_mhs'];
		$nama_ortu		= $_POST['nama_ortu'];
		$alamat_ortu	= $_POST['alamat_ortu'];
		$alamat_surat	= $_POST['alamat_surat'];
		$catatan_surat	= $_POST['catatan_surat'];
		$email_ortu		= $_POST['email_ortu'];
		$tlp_ortu		= $_POST['tlp_ortu'];
		$hp_ortu		= $_POST['hp_ortu'];
		$is_valid		= 0;
		$is_finish		= 0;
		$last_update	= date("Y-m-d H:i:s");
		
		$data_daftar_ulang = array(
									'daftar_id'=>$daftarid,
									'tahun_akademik'=>$thn_akademik,
									'mahasiswa_id'=>$mhs_id,
									'nama'=>$nama_mhs,
									'tmp_lahir'=>$tmpt_lahir_mhs,
									'tgl_lahir'=>$tgl_lahir_mhs,
									'tgl_daftar'=>$tgl_daftar,
									'jalur_masuk'=>$jalur_masuk,
									'email'=>$email_mhs,
									'alamat_malang'=>$alamat_mhs,
									'telp'=>$tlp_mhs,
									'hp'=>$hp_mhs,
									'orang_tua'=>$nama_ortu,
									'alamat_ortu'=>$alamat_ortu,
									'alamat_surat'=>$alamat_surat,
									'catatan'=>$catatan_surat,
									'email_ortu'=>$email_ortu,
									'telp_ortu'=>$tlp_ortu,
									'hp_ortu'=>$hp_ortu,
									'is_valid'=>$is_valid,
									'is_finish'=>$is_finish,
									'last_update'=>$last_update
								  );
								  // var_dump($data_daftar_ulang);
		$save_daftarulang = $mservices->replace_daftarulang($data_daftar_ulang);
		// echo $save_daftarulang;
		if($save_daftarulang){
			/*echo "Berhasil";
			exit();*/
			$this->daftar_ulang($mhs_id);
			exit();
		}
		else{
			/*echo "Gagal";
			exit();*/
			$this->daftar_ulang($mhs_id, 'gagal');
			exit();
		}
	}

	/*
	 * 
	 * =================== KKNP ================================
	 * 
	 * */
	
	function kknp($str=NULL, $id=NULL, $id_page=NULL){
		$mkknp 		= new model_kknp();
		$mpage 		= new model_page();
		
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		$data['view_slide']= 0;

		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news',"","","","",1);
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		// $this->add_style('select/select2.css');
		// $this->add_script('select/select2.js');
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/kknp/kknp.js');
		
		switch ($str) {
			case 'pengajuan':				
				$data['header'] = 'KKNP Submission';
				
				$this->add_script('datepicker/js/moment-2.4.0.js');
				$this->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
				$this->add_style('datepicker/css/bootstrap-datetimepicker.css');
				$this->add_script('js/jquery/tagmanager.js');
				$this->add_style('css/bootstrap/tagmanager.css');
				$this->add_script('js/jquery/jquery.tokeninput.js');
				$this->add_style('css/bootstrap/token-input.css');
				
				$this->view( 'info/kknp/pengajuan.php', $data );
			break;
			default:	
				$data['header'] = 'KKNP';
				
				$data['posts']	= $mkknp->read();
				
				$this->view( 'info/kknp/index.php', $data );
			break;
		}
	}
	
	function mhs(){
		$mkknp 		= new model_kknp();
		$result 	= $mkknp->get_mhs();
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
	}
	
	function company(){
		$mkknp 		= new model_kknp();
		$result 	= $mkknp->get_company();
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
		
	}
	
	function save_kknp(){
		$mkknp 		= new model_kknp();
		$last_update= date("Y-m-d H:i:s");

		if(isset($_POST['hidId']) && $_POST['hidId']!=''){
			$pengajuan_id = $_POST['hidId'];
			$tgl_pengajuan = $_POST['tgl_pengajuan'];
		}else{
			$pengajuan_id = $mkknp->get_pengajuan_id();
			$tgl_pengajuan = $last_update;
		}
		
		$get_companyid = $this->company_save('from-save-kknp');
		
		$tgl_mulai = $_POST['tgl_mulai'];
		$tgl_selesai = $_POST['tgl_selesai'];
		$mhsall = $_POST['hidden-mhs_kknp'];
		
		$status = $_POST['status'];
		
		// $this->mhs_save($mhsall, $pengajuan_id);
		
		$data_kknp		 = array(
							'pengajuan_id'=>$pengajuan_id,
							'perusahaan_id'=>$get_companyid,
							'tgl_pengajuan'=>$tgl_pengajuan,
							'tgl_mulai'=>$tgl_mulai,
							'tgl_selesai'=>$tgl_selesai,
							'last_update'=>$last_update,
							'is_status'=>$status
						   );
		if($mkknp->replace_kknp($data_kknp)){
			
			$this->mhs_save($mhsall, $pengajuan_id);
			
			echo "Proses Simpan Berhasil!";
		}else{
			echo "Proses Simpan Gagal!";
		}
		
	}
	
	function mhs_save($mhsall, $pengajuan_id){
		$mkknp 		= new model_kknp();
		
		$mhs = explode(',', $mhsall);
		
		foreach ($mhs as $m) {
			$data_m = explode('-', $m);
			$nama_mhs = substr($data_m[1], 1);
			$nim_mhs = substr($data_m[0], 0, -1);
			
			$get_mhs_id = $mkknp->get_mhs($nama_mhs, $nim_mhs);
			
			$data_mhs	 = array(
							'kknp_mhs_id'=>$mkknp->get_mhs_kknp_id(),
							'pengajuan_id'=>$pengajuan_id,
							'mahasiswa_id'=>$get_mhs_id[0]->hid_id
						   );
			$mkknp->replace_kknp_mhs($data_mhs);
			
		}
		
	}
	
	function company_save($str=NULL){
		$mkknp 		= new model_kknp();
		
		$company_name = $_POST['perusahaan_name'];
		$company_address = $_POST['perusahaan_alamat'];
		
		if(isset($_POST['telp']) && $_POST['telp']!=''){
			$company_telp = $_POST['telp'];
		}else $company_telp = NULL;
		
		$perusahaan_id = $mkknp->get_company($company_name,$company_address);
		if(isset($perusahaan_id[0]->hid_id)){
			if($str=='from-save-kknp'){
				return $perusahaan_id[0]->hid_id;
			}			
		}else{
			$perusahaan_id = $mkknp->get_company_id();	
			$data_company	 = array(
							'perusahaan_id'=>$perusahaan_id,
							'nama'=>$company_name,
							'alamat'=>$company_address,
							'telp'=>$company_telp
						   );
			$mkknp->replace_perusahaan($data_company);	
		}
		
		return $perusahaan_id;
	}
	
	function get_data_mhs_krs(){
		$minfo = new model_info();

		$mconf = new model_confinfo();
			
		$row 		= $mconf->get_semester_aktif();
		//$semester 	= $row->tahun_akademik;
		$semester = substr($row->tahun_akademik,0,7)."1";
		$data['semester']	= $semester;
					
		$str 		 = $_POST['inputmhs'];
		$data['rmhs'] = $minfo->get_mhs($str);
		$data['dosen']	= $minfo->get_dosen();
		
		if($data['rmhs']){
			$data['daftar'] = $minfo->get_mhs_daftar_krs($semester, $str);
			
			if($data['daftar']){
				$data['mhs'] = $data['daftar'];
			}else{
				$data['mhs'] = $data['rmhs'];
			}
			
		
			$this->view( 'info/akademik/form_daftar_krs.php', $data );
		}else{
			echo "<div class='form-group'><span class='alert alert-warning'>Data NIM Anda tidak ditemukan sebagai mahasiswa aktif. Silahkan cek kembali NIM Anda.</span></div>";
		}
	}
	
	
}
?>