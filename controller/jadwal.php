<?php
class controller_jadwal extends comscontroller {

	function __construct() {
		parent::__construct();
		// /$this->require_auth('page');
	}
	
	function index($str=NULL, $id=NULL, $id_page=NULL){ //fungsi Agie Ghazy F. (jangan di replace)
		$mpage 		= new model_page();
		$mjadwal = new model_jadwalinfo();
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data['jam'] = $mjadwal->get_jam($is_pendek);
		$data['ruang'] = $mjadwal->get_ruang($this->config->cabang, $this->config->fakultas);
		$data['dosen'] = $mjadwal->get_dosen($this->config->cabang, $this->config->fakultas);
		$data['mk'] = $mjadwal->get_matakuliah($this->config->cabang, $this->config->fakultas);
		$data['praktikum'] = $mjadwal->get_praktikum($this->config->cabang, $this->config->fakultas);
		
		$this->add_script('js/jadwalkuliah/jadwal.js');
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		$data['view_slide']= 0;
		
		$data['mimg'] = new imageprocessing();
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news',"","","","",1);
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');		
								
		$this->view('info/jadwal_kuliah/index.php', $data );
	}
	
	function tes(){
		$mjadwal = new model_jadwalinfo();
		
		$mjadwal->get_jam();
	}
	
	function jadwal_json(){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		
		$lang = $data['lang'];
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal($this->config->cabang, $this->config->fakultas, $is_pendek, $data['lang']);
		$jadwal;
		
		foreach($data as $key){
			
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
			
			if($key->mk) $strmk = $key->mk;
			else $strmk = $key->mk_ori;
			
			$tmp = array(
				'str_kelas' => $key->strkelas,
				'matakuliah' => $strmk,
				'mk_id' => $key->mk_id,
				'kelas' => $key->kelas,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'karyawan_id' => $key->karyawan_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'urut_selesai' => $key->urut_selesai,
				'praktikum' => $key->is_praktikum
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		echo json_encode($jadwal);
	}
	
	
	function jadwal_prak_json($unit=NULL){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		
		$lang = $data['lang'];
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal_praktikum($this->config->cabang, $this->config->fakultas, $is_pendek, $data['lang'], $unit);
		$jadwal;
		
		foreach($data as $key){
			
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
			
			if($key->mk) $strmk = $key->mk;
			else $strmk = $key->mk_ori;
			
			$tmp = array(
				'str_kelas' => $key->strkelas,
				'matakuliah' => $strmk,
				'mk_id' => $key->mk_id,
				'kelas' => $key->kelas,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'karyawan_id' => $key->karyawan_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'urut_selesai' => $key->urut_selesai,
				'praktikum' => $key->is_praktikum
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		echo json_encode($jadwal);
	}
	
	function jadwal_json_show(){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal($this->config->cabang, $this->config->fakultas, $is_pendek, $data['lang']);
		$jadwal;
		
		foreach($data as $key){
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
			
			if($key->mk) $strmk = $key->mk;
			else $strmk = $key->mk_ori;
			
			$tmp = array(
				'str_kelas' => $key->strkelas,
				'matakuliah' => $strmk,
				'mk_id' => $key->mk_id,
				'kelas' => $key->kelas,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'karyawan_id' => $key->karyawan_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'urut_selesai' => $key->urut_selesai,
				'praktikum' => $key->is_praktikum
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		?>
			<pre><?php print_r($jadwal) ?></pre>
		<?php
	}
	
	function get_hari($hari){
		switch ($hari) {
			case '1': return 'senin'; break;
			case '2': return 'selasa'; break;
			case '3': return 'rabu'; break;
			case '4': return 'kamis'; break;
			case '5': return 'jumat'; break;
			case '6': return 'sabtu'; break;
			case '7': return 'minggu'; break;
			
			default: return 'minggu'; break;
		}
	}
	
	
	
	
	
	
}
?>