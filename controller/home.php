<?php
class controller_home extends comscontroller {

	function __construct() {
		parent::__construct();
		
	}
	
	function notfound($str=NULL){
		if($str){
			echo $str;
		}				
		exit();
	}
	
		
	function index($str=NULL){
		if($str){
			$this->read($str);
			exit();			
		}
		
		$mpage = new model_page();
		//$mimg = new imageprocessing();
		$data['lang'] = $this->config->lang;//'EN';
		
		$data['unit'] ='0';
		//$data['mimg'] = new imageprocessing();
		
		//$data['all'] = $this->search_json();
		
		$data['mpage']	= $mpage;
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
	//	$data['apps'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',10);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['kerjasama']	= $mpage->read_content('-',$data['lang'],'kerjasama');
		
		$data['event'] = $mpage->read_event($data['unit'],$data['lang'],"","",5);		
		//$data['slide']	= $mpage->read_slide($data['unit'],$data['lang'],4);
		$data['slide'] = $mpage->read_slide('-',$data['lang'],7,"'".$this->config->slider."'");	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		
		$data['color'] = "orange";
				
		if($str){
			$data['post'] = $mpage->read_content($data['unit'], $data['lang'],"", $str);	
		}else{
			$data['post'] = false;//$mpage->read_content("", 'about');
		}
		
		$this->view('page/index.php', $data);
	}
		
	
}
?>