<?php
class controller_apps extends comscontroller {

	function __construct() {
		
		parent::__construct();
		$this->require_auth('home');
		
	}
	
	function index() {
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		/*if($role=='mahasiswa' || $role=='dosen'){
			$this->redirect('module/elearning');
		}*/
		
		if(isset($_POST['tahun'])) $tahun = $_POST['tahun'];
		else $tahun = date('Y');
		
		if(isset($_POST['bulan'])) $bulan = $_POST['bulan'];
		else $bulan = date('m');
		
		$data['year'] = $tahun;
		$data['month'] = $bulan;
		
		$id = $this->authenticatedUser->staffid;
		
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_style('css/bootstrap/token-input.css');
		
		$this->add_style('select/select2.css');
		$this->add_script('select/select2.js');
		
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('tagmanager/tagmanager.js');
		$this->add_style('css/bootstrap/tagmanager.css');
		$this->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		
		$this->add_script('highchart/highcharts-3d.js');
		$this->add_script('highchart/modules/exporting.js');
		
		
		$this->add_script('js/home/home.js');
		$this->add_script('js/home/tasks.js');
		$this->add_script('js/base64.js');
	
		
		$this->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->add_script('js/jquery/jquery.tokeninput.js');
		
		$this->add_script('datepicker/js/moment-2.4.0.js');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
			
		$this->add_script('js/jquery/tagmanager.js');
		
		// $this->add_script('js/calendar.js');
		$this->add_style('css/calendar.css');
		$date = date('Y-m-d');
		
		$mhome = new model_home();		
		
		// $data['aktifitas'] = $mhome->get_aktifitas_for_calendar($role, $staff, $mhs);
		
		if($role != 'mahasiswa') $data['all_event'] = $mhome->get_all_event($staff, $bulan, $tahun);
		else $data['all_event'] = $mhome->get_all_event($mhs, $bulan, $tahun);
		
		$data['ruang'] = $mhome->get_ruangan($this->authenticatedUser->fakultas, $this->authenticatedUser->cabang);
				
		/* -- rekap mengajar --*/
		$data['hadir'] = $mhome->get_absen_dosen($this->authenticatedUser->staffid, $this->authenticatedUser->fakultas, $this->authenticatedUser->cabang, 'dosen');
		$data['keterangan'] = $this->authenticatedUser->name;
		$data['fakultas'] = "Program Teknologi Informasi dan Ilmu Komputer";
		$data['max'] = '14';
		/* -- end mengajar --*/
		
		/* -- rekap finger --*/
		$bln_start = '01';
		$bln_end = '12';
		$tahun = date("Y");
		
			
		$data['finger'] = $mhome->get_rekap_absen($this->authenticatedUser->staffid, $tahun, $bln_start, $bln_end);
		
		$data['rekap_hr'] = $mhome->get_rekap_hr_mengajar($this->authenticatedUser->staffid, $tahun, $bln_start, $bln_end);
		$data['tahun'] = $tahun;
		/* -- end finger --*/	
		
		/* rekap skripsi */
		$data['skripsi'] = $mhome->get_bimbingan_skripsi($this->authenticatedUser->staffid);
		$data['karyawanid'] = $this->authenticatedUser->staffid;
		
		
		$data['jenis_kegiatan'] = $mhome->get_jeniskegiatan();
		$data['viewby'] = '';
		$data['kegiatan'] = '';
		
		if($data['hadir']) $this->add_script('js/home/report.js');
		
		// $data['event_konten'] = $mhome->getEvent($list_day, $month, $year, $running_day,$mhs);
		
		$data['tasks'] = $mhome->get_task_now($this->authenticatedUser->id);
		
		$this->head(true);
		$this->view('home.php', $data );
		//$this->foot();
		
	}
	
	function get_data_event($data=NULL, $list_day=NULL, $month=NULL, $year=NULL, $running_day=NULL){
		echo "<br>";
		if(count($data)>0){
		 $i=0;
		 foreach($data as $row):
			 $paymentDate  = strtotime($year."-".$month."-".$list_day." 00:00:00");
			 
			 $contractDateBegin = strtotime($row->tgl." 00:00:00");
			 $contractDateEnd  = strtotime($row->tgl_selesai." 00:00:00");
			 
			 
			if($paymentDate >= $contractDateBegin && $paymentDate <= $contractDateEnd){
			 	
				$i++;
				
				$icon = $note = $sclass = $tclass = '';
				$this->cetak_label($row, $icon, $note, $sclass, $tclass);
				
				$data['row'] = $row;
				$data['icon'] = $icon;
				$data['note'] = $note;
				$data['sclass'] = $sclass;
				$data['tclass'] = $tclass;
				$data['i'] = $i;
				$this->view('home/label-calendar.php', $data );
			 }
			
		 endforeach;
		 if($i>1)echo '<div style="text-align:left"><small><a href="javascript::" class="show-calendar-it" data-mincal="0" data-maxcal="'.$i.'" class="text text-info"><span class="fa fa-angle-down"></span> More</a></small></div>';
		}
	}
	
	function get_data_calendar($list_day=NULL, $month=NULL, $year=NULL, $running_day=NULL){
		echo "<br>";
		$mhome = new model_home();
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		if($role=='mahasiswa'){
			$data = $mhome->getEvent($list_day, $month, $year, $running_day,$mhs);
		}else{
			$data = $mhome->getEvent($list_day, $month, $year, $running_day,$staff);
		}
		
		if(count($data)>0){
		 $i=0;
		 foreach($data as $row):
			$i++;
			
			$icon = $note = $sclass = $tclass = '';
			$this->cetak_label($row, $icon, $note, $sclass, $tclass);
			
			$data['row'] = $row;
			$data['icon'] = $icon;
			$data['note'] = $note;
			$data['sclass'] = $sclass;
			$data['tclass'] = $tclass;
			$data['i'] = $i;
			$this->view('home/label-calendar.php', $data );
			
		 endforeach;
		 // if($i>1)echo '<div style="text-align:left"><small><a href="javascript::" class="show-calendar-it" data-mincal="0" data-maxcal="'.$i.'" class="text text-info"><span class="fa fa-angle-down"></span> More</a></small></div>';
		}
		
	}
	
	function add_kegiatan(){
		$mhome = new model_home();	
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		$ruang = '';
		if(isset($_POST['ruang']))
		foreach($_POST['ruang'] as $x) $ruang = $ruang . ', ' . $x;
		
		if(empty($_POST['tanggal_sampai'])) $tgl_selesai = $_POST['tanggal'] ;
		else $tgl_selesai = $this->convert_date_format($_POST['tanggal_sampai']);
		
		$data = array(
			'aktifitas_id' => $mhome->get_aktifitasId(),
			'karyawan_id' => $staff,
			'mahasiswa_id' => $mhs,
			'user_id' => $user,
			'tgl' => $_POST['tanggal'],
			'tgl_selesai' => $tgl_selesai,
			'jam_mulai' => $this->convert_am($_POST['jam_mulai']),
			'jam_selesai' => $jam_selesai = $this->convert_am($_POST['jam_mulai']),
			'judul' => $_POST['judul'],
			'lokasi' => $_POST['lokasi'],
			'jenis_kegiatan_id' => $_POST['jenis_kegiatan'],
			'inf_ruang' => substr($ruang,2)
		);
		
		// foreach ($data as $key => $value) {
			// echo $key . ' => ' . $value . '<br>';
		// }
		
		$mhome->save_aktifitas($data);
	}

	function convert_date_format($date){
		$data = explode("/", $date);
		if(isset($data[2])) return $data[2].'-'.$data[0].'-'.$data[1];
		else return $date;
	}
	
	function convert_am($time){
		if(empty($time)) return "";
		$data = explode(":", $time);
		
		if(substr($time,-2) == 'PM') $am = 12;
		else $am = 0;
		
		return (substr($data[0],0,2) + $am) . ':' . substr($data[1], 0,2) . ':00';
	}
	
	function edit_kegiatan(){
		$ruang = '';
		if(isset($_POST['edit_ruang']))
			foreach($_POST['edit_ruang'] as $x) $ruang = $ruang . ', ' . $x;
		
		$mulai = $this->convert_am($_POST['edit_jam_mulai']);
		$selesai = $this->convert_am($_POST['edit_jam_selesai']);
		$tgl_selesai = $this->convert_date_format($_POST['edit_tgl_selesai']);
		
		$mhome = new model_home();	
		
		$mhome->edit_aktifitas($mulai, $selesai, $tgl_selesai, substr($ruang,2));
	}
	
	function hapus_kegiatan(){
		$mhome = new model_home();	
		$mhome->hapus_aktifitas();
	}
	
	// function index($page = '', $hal = '', $start = '') {
		// $role	= $this->authenticatedUser->role;
		// $user	= $this->authenticatedUser->id;
		// $staff	= $this->authenticatedUser->staffid;
		// $mhs	= $this->authenticatedUser->mhsid;
// 		
		// $id = $this->authenticatedUser->staffid;
// 		
		// $this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		// $this->add_style('css/bootstrap/token-input.css');
		// $this->add_style('css/bootstrap/tagmanager.css');
		// $this->add_style('css/bootstrap-timepicker.min.css'); //timepicker
// 		
		// $this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		// $this->add_script('js/jquery/jquery.autogrowtextarea.min.js');
		// $this->add_script('js/home/jquery-ias.js');
		// $this->add_script('js/home/home.js');
		// $this->add_script('js/home/tasks.js');
		// $this->add_script('js/user/index.js');
		// $this->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		// $this->add_script('js/jquery/jquery.tokeninput.js');
// 			
		// $this->add_script('js/jquery/tagmanager.js');
		// $this->add_script('js/home/ckeditor/ckeditor.js');
		// $this->add_script('ckeditor/adapters/jquery.js');
		// $date = date('Y-m-d');
// 			
		// $mhome= new model_home();		
// 		
		// $data['posts'] 		= "";
		// //$data['lists'] = $mhome->read('');
// 		
		// $data['shares'] = $mhome->share();
// 		
// 		
		// $data['tasks']	= $mhome->get_aktifitas('task', $date, $user);
// 		
		// if($role=="mahasiswa"){
			// $data['viewby'] 	= $mhome->view_by_mhs($mhs);
			// $data['kegiatan']	= $mhome->get_aktifitas('event', $date, $mhs);
		// }else{
			// $data['viewby'] 	= $mhome->view_by_dosen($staff);
			// $data['kegiatan']	= $mhome->get_aktifitas('event', $date, $staff);
		// }
// 					
		// // $page = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9\-]/', '', urldecode(html_entity_decode(strip_tags(strtolower($page)))))));
		// // if(empty($page)) {
			// // //$this->redirect('notfound');
			// // $page = 'view';
		// // }
// // 		
		// // $start = trim(preg_replace('/ +/', ' ', preg_replace('/[^0-9+]/', '', urldecode(html_entity_decode(strip_tags($start))))));
		// // if($hal != 'view') {
			// // $hal = 'view';
		// // }
		// // if(($start == '') || (!is_numeric($start))) {
			// // $start = 1;
		// // } else {
			// // $start = intval($start);
		// // }
// 		
		// $limit = 10;		
		// $base = $this->location('home/index/'.$page);
// 		
		// $total = $mhome->total();
// 		
		// $data['lists'] 		= $mhome->read($start,$limit);
		// $data['pagination'] = $this->pagination($base, $start, $total, $limit);
		// $data['comments'] 	= $mhome->comment();
// 	
		// $this->head();
		// $this->view('home.php', $data );
		// $this->foot();
// 		
	// }



function comments($status_id=NULL) { 
	
	$mhome= new model_home();
	
	$comments = $mhome->comment($status_id);
	
	?>
	
	<?php 
	
	$count=0;
	
	if( isset($comments)) {
		$commc=1;
		
			if(count($comments)>5){ ?>
                <div class="media comment-wrap" data-id="64fb82" data-showall=<?php echo $status_id ?>>
			  	<div class="media-body">
			    	<div class="keterangan"><a class='show-all-comment' href="javascript::" onclick="show_all_comment('<?php echo $status_id;?>')" data-commentclick=<?php echo $status_id;?> style="text-align:center;display:block"><i class='fa fa-refresh'></i> Show All</a></div>							
									</div>
				</div>
            
                
                <?php }
		foreach ($comments as $com): 
		// if ($com->parent_id==$status_id) {
			?>
			
			<div class="media comment-wrap" data-id="<?php echo $com->status_id ?>" data-showit=<?php echo $status_id ?> <?php if(count($comments)>5 && $commc<=5)echo 'style="display:none"'; ?>>
				<a class="pull-left" href="#">
					<?php
						if($com->foto){
						?>
							<img class="media-object" src="<?php echo $this->location($com->foto); ?>" alt="..." height="50px" width="50px">
						<?php
						}else{
						?>
						<img class="media-object" src="http://placehold.it/50x50" alt="..." height="50px" width="50px">
						<?php } ?>
				</a>
			  	<div class="media-body">
			    	<h4 class="media-heading"><a class="timeline-user" style="cursor:pointer;"><?php echo $com->name; ?></a></h4>
                    <div class="status-action">
            <small class="timeline-time"><?php echo date('M d, Y H:i',strtotime($com->last_update)); ?></small>&nbsp;
	    	<?php if ($this->authenticatedUser->id==$com->user_id) { ?>
				<a class='btn-edit-post delete-comment' onclick='delete_comment_status("<?php echo $this->location('home/delete/'.$com->status_id); ?>","<?php echo $com->status_id ?>","comment")'data-id="<?php echo $com->status_id ?>" href="javascript::" data-uri="<?php echo $this->location('home/delete/'.$com->status_id); ?>"><i class='fa fa-trash-o'></i></a>	&nbsp;									
			<?php }?>
            </div>
			    	<div class="keterangan"><?php echo $com->keterangan; ?></div>
			    	
				</div>
			</div>
                
			<?php 
				$commc++; 
			//}
			endforeach;  
	}  
}


function pagination($base_location, $page, $total, $limit) {

		// Initial page num setup
		if ($page == 0){ $page = 1; }
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil($total / $limit);
		$LastPagem1 = $lastpage - 1;
		$stages = 2;

		$paginate = '';
		if($lastpage > 1) {
			$paginate .= "<ul class='pagination pagination-sm'>";
			if ($page > 1) {
				$paginate.= "<li><a title='Previous' href='$base_location/page/$prev'>&laquo;</a></li>";
			} else {
				$paginate.= "<li class='disabled'><span>&laquo;</span></li>";
			}
			if ($lastpage < 7 + ($stages * 2)) {
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page){
						$paginate.= "<li class='active'><span>$counter <span class='sr-only'>(current)</span></span></li>";
					} else {
						$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
				}
			}
			elseif($lastpage > 5 + ($stages * 2)) {
				if($page < 1 + ($stages * 2)) {
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
						if ($counter == $page){
							$paginate.= "<li class='active'><span>$counter</span></li>";
						}else{
							$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
					}
					$paginate.= "<li><span>...</span></li>";
					$paginate.= "<li><a title='Page $LastPagem1' href='$base_location/page/$LastPagem1'>$LastPagem1</a></li>";
					$paginate.= "<li><a title='Page $lastpage' href='$base_location/page/$lastpage'>$lastpage</a></li>";
				}
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
				{
					$paginate.= "<li><a title='Page 1' href='$base_location/page/1'>1</a></li>";
					$paginate.= "<li><a title='Page 2' href='$base_location/page/2'>2</a></li>";
					$paginate.= "<li><span>...</span></li>";
					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
					{
						if ($counter == $page){
							$paginate.= "<li class='active'><span>$counter</span></li>";
						}else{
							$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
					}
					$paginate.= "<li><span>...</span></li>";
					$paginate.= "<li><a title='Page $LastPagem1' href='$base_location/page/$LastPagem1'>$LastPagem1</a></li>";
					$paginate.= "<li><a title='Page $lastpage' href='$base_location/page/$lastpage'>$lastpage</a></li>";
				}
				else
				{
					$paginate.= "<li><a title='Page 1' href='$base_location/page/1'>1</a></li>";
					$paginate.= "<li><a title='Page 2' href='$base_location/page/2'>2</a></li>";
					$paginate.= "<li><span>...</span></li>";
					for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page){
							$paginate.= "<li class='active'><span>$counter</span></li>";
						} else {
							$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
					}
				}
			}
			if ($page < $counter - 1) {
				$paginate.= "<li><a title='Next' href='$base_location/page/$next' class='next-page'>&raquo;</a></li>";
			} else {
				$paginate.= "<li class='disabled'><span>&raquo;</span></li>";
			}
			$paginate.= "</ul>";
		}
		 return $paginate;
	}

	function save(){
		if(isset($_POST['b_status'])!=""){
			$this->saveToDB();
			//$this->redirect('module/content/status/write');
			exit();
		}else{
			$this->write();
			exit;
		}
	}
	
	function delete($id){
		$mhome = new model_home();
		$mhome->del_comment($id);
	}
	
	function delete_status($id){
		$mhome = new model_home();
		$mhome->del_parent($id);
		$mhome->del_status($id);
	}
	
	function saveToDB(){
		ob_start();
		$mhome= new model_home();	
						
		if($_POST['hidId']!=""){
			$status_id 	= $_POST['hidId'];
			$action 	= "update";
		}else{
			$status_id	= $mhome->get_reg_number();
			$action 	= "insert";	
		}
		
		$keterangan		= $_POST['keterangan'];
		
		$parent_id		= $_POST['parent_id'];

		$user_id		= $this->authenticatedUser->id;
		
		$last_update	= date("Y-m-d H:i:s");	
		
		
		$datanya 	= Array(
						'status_id'=>$status_id, 
						'keterangan'=>$keterangan,
						'parent_id'=>$parent_id,
						'user_id'=>$user_id,
						'last_update'=>$last_update, 
						);
		$mhome->replace_status($datanya);
		
		/*Insert Comment*/
		if($_POST['jadwal_id']!=""){
			$comjadwal_id=$_POST['jadwal_id'];
		}else{
			$comjadwal_id="0";
		}
		
		if($_POST['mkditawarkan_id']!=""){
			$commkditawarkan_id=$_POST['mkditawarkan_id'];
		}else{
			$commkditawarkan_id="0";
		}
		
		$share_id	= $mhome->get_reg_share();
		$datasharecom 	= Array(
						'share_id'=>$share_id,
						'jadwal_id'=>$comjadwal_id, 
						'mkditawarkan_id'=>$commkditawarkan_id,
						'status_id'=>$status_id, 
					);
		$mhome->replace_share($datasharecom);
		/*End Insert Comment*/
		
		
		
		$hidjadwal	= $_POST['hidjadwal'];
		$hidmk	= $_POST['hidmk'];
		
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		//echo $hidjadwal;
		if ($hidjadwal){
			$j=explode(",",$hidjadwal);
			for ($i=0; $i<count($j); $i++){
				//echo $j[$i];
				$j1[$i] = explode("-",$j[$i]);
				for ($k=0; $k<count($j[$i]); $k++){
					//echo $i.'-'.$k.'-'.$j1[$i][$k]; echo '<br />';
					$namamk = $j1[$i][0];
					$kelas = $j1[$i][1];
					
					if($role=="mahasiswa"){
						$idjadwal = $mhome->get_jadwal_id_mhs($namamk, $kelas);
					}else if ($role=="Dosen") {
						$idjadwal = $mhome->get_jadwal_id_dosen($namamk, $kelas);
					} 	
					
					//echo $idjadwal;
					$share_id	= $mhome->get_reg_share();
					
					$datashare 	= Array(
						'share_id'=>$share_id,
						'jadwal_id'=>$idjadwal, 
						'status_id'=>$status_id, 
					);
					$mhome->replace_share($datashare);
				}	
			}
		} 
		
		if ($hidmk){
			$j=explode(",",$hidmk);
			for ($i=0; $i<count($j); $i++){
				
					$namamk = $j[$i];
					if($role=="mahasiswa"){
						$idmk = $mhome->get_mk_id_mhs($namamk);
					}else if ($role=="Dosen") {
						$idmk = $mhome->get_mk_id_dosen($namamk);
					} 	
					
					
				
					$share_id	= $mhome->get_reg_share();
					
					$datashare 	= Array(
						'share_id'=>$share_id,
						'mkditawarkan_id'=>$idmk, 
						'status_id'=>$status_id, 
					);
					$mhome->replace_share($datashare);
			}
		}
		
		$this->redirect('home');
		exit();
	}

	function mk(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mhome 	= new model_home();
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		if($role=="mahasiswa"){
			$data	= $mhome->get_mk_mhs($str,$mhs);
		}else if ($role=="Dosen") {
			$data	= $mhome->get_mk_dosen($str,$staff);
		} 	
		
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function jadwal(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mhome 	= new model_home();
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		if($role=="mahasiswa"){
			$data	= $mhome->get_jadwal_mhs($str,$mhs);
		}else if ($role=="Dosen") {
			$data	= $mhome->get_jadwal_dosen($str,$staff);
		} 	
	
		
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function savetaskToDB(){
		if(isset($_POST['judul'])&&$_POST['judul']!=""){
			ob_start();
			$mhome = new model_home();
			$today	= str_replace('-', '', date("y-m-d"));
			$jam	= str_replace(':', '', date("H:i:s"));
				
			if($_POST['hidId']!=""){
				$aktifitas_id 		= $_POST['hidId'];
				$action 			= "update";
			}else{
				$aktifitas_id		= $mhome->get_reg_aktifitas();
				$action 			= "insert";
			}
			
			$judul			= $_POST['judul'];
			$catatan		= $_POST['catatan'];
			$jam_mulai		= $_POST['jam_mulai'];
			$jam_selesai	= $_POST['jam_selesai'];
			
			$tgl = $_POST['tanggal'];
			$tgl_selesai = $tgl;
			
			$toHari = date_format(date_create($tgl), 'w');
			switch($toHari){
				case '0': $hari='minggu'; break;
				case '1': $hari='senin'; break;
				case '2': $hari='selasa'; break;
				case '3': $hari='rabu'; break;
				case '4': $hari='kamis'; break;
				case '5': $hari='jumat'; break;
				case '6': $hari='sabtu'; break;
			}
			
			if(isset($_POST['finish'])!=""){
				$finish	= $_POST['finish'];
			}else{
				$finish	= 0;
			}
			$user_id		= $this->authenticatedUser->id;
			$karyawan_id	= $this->authenticatedUser->staffid;
			$mhs			= $this->authenticatedUser->mhsid;
			$inf_kategori 	= "task";
			
			if($aktifitas_id){
				$datanya 	= Array('aktifitas_id' => $aktifitas_id,
									'karyawan_id' => $karyawan_id,
									'user_id' => $user_id,
									'mahasiswa_id' => $mhs,
									'hari' => $hari,
									'tgl' => $tgl,
									'tgl_selesai' => $tgl_selesai,
									'jam_mulai' => $jam_mulai,
									'jam_selesai' => $jam_selesai,
									'judul' => $judul,
									'catatan' => $catatan,
									'is_finish' => $finish,
									'inf_kategori' => $inf_kategori
									);
				$mhome->replace_aktifitas($datanya);
				echo "Berhasil Menambahkan Task!";
				exit();
			}
			else{
				echo "Gagal Menambahkan Task!";
				exit();
			}
			
			
		}else{
			$this->index();
			exit;
		}
		
	}

	function finish_task(){
		$id 	= $_POST['task_id'];
		$finish = $_POST['finish'];
		$mhome = new model_home();
		$finish = $mhome->finish_tasks($id,$finish);
	}
	
	function delete_task(){
		$id 	= $_POST['delete_id'];
		$mhome = new model_home();
		$delete = $mhome->delete_tasks($id);
		if($delete==TRUE){
			echo "Berhasil menghapus task!";
			exit();
		}
	}
	

	function cetak_label($row, &$icon, &$note, &$sclass, &$tclass){
		$mhome = new model_home();
		switch (strToLower($row->jenis_kegiatan)){
			case 'konseling':
				$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'konseling';
				$tclass= 'text-putih';
			break;
			
			case 'rapat':
				$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'rapat';
				$tclass= 'text-putih';
			break;
			
			case 'kemahasiswaan':
				$icon = "";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'mhs';
				$tclass= 'text-putih';
			break;
			
			case 'kuliah tamu':
				$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'kuliahtamu';
				$tclass= 'text-putih';
			break;
			
			case 'wisuda':
				$icon = "<i class='fa fa-star'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'wisuda';
				$tclass= 'text-putih';
			break;
			
			case 'kunjungan':
				$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'kunjungan';
				$tclass= 'text-putih';
			break;
			
			case 'rekrutmen':
				$icon = "<i class='fa fa-signal'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'rekrutmen';
				$tclass= 'text-putih';
			break;
			
			case 'penelitian':
				$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'success';
			break;
			
			case 'pelatihan':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'pelatihan';
				$tclass= 'text-putih';
			break;
			
			case 'workshop':
				$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'workshop';
				$tclass= 'text-putih';
			break;
			case 'seminar':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'seminar';
				$tclass= 'text-putih';
			break;
			case 'uts':
				$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
				$note = $row->jenis;	
				$sclass= 'info';	
				$tclass= 'text-info';									
			break;
			case 'uas':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				$note = $row->jenis;	
				$sclass= 'info';	
				$tclass= 'text-putih';
			break;
			case 'lain-lain':
				$icon = "";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'success';
				$tclass= 'text-putih';
			break;
			case 'olahraga dan seni':
				$icon = "<i class='fa fa-flag'></i>&nbsp;";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'porseni';
				$tclass= 'text-putih';
			break;
			default:
				$icon = "";
				$note = $mhome->potong_kalimat($row->judul,5);
				$sclass= 'info';
				$tclass= 'text-putih';
			break;
		}
	}
	
}
?>