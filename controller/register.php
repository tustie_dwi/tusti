<?php
class controller_register extends comscontroller {

	function __construct() {
		parent::__construct();		
	}
	

	function index(){
		if(isset($_POST['b_reg'])){
			$this->save($_POST['email']);
			exit();
		}	
		
		if(isset($_POST['b_reg_mhs'])){
			$this->save($_POST['emailmhs']);
			exit();
		}	

		
		$data['post']	= false;
		
		$data['msglogin']="";
	
		//$this->view("page/register.php", $data);
		$this->view( 'auth2.php', $data );
	}
	
	function msg($by=NULL, $email=NULL){
		if($by){
			
			switch($by){
				case 'ok':
					$data['msglogin'] = '<div class="alert alert-block alert-success fade in"> 
									OK! Registration success. Your password has been sent to your email <b>'.$email.'</b></div> ';
				break;
				case 'nok':
					$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">
									 Sorry, <b>'.$email.'</b> not registered in our data. Please contact <b>PSIK</b> for more information.</div>';
				break;
				case 'nok_email':
					$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">
									Your account has been registered. Please contact <b>PSIK</b> for more information.</div>';
				break;
				case 'nok_send':
					$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">
									Registration failed. Please contact <b>PSIK</b> for more information.</div>';
				break;
			}
		}else{
			$data['msglogin'] = "";
		}
		
		
		/*$mpage = new model_page();
				
		$data['page'] 	= $mpage->read_content('page');
		$data['news'] 	= $mpage->read_content('news');
		$data['video'] 	= $mpage->read_video();
		$data['course'] = $mpage->read_course('main');	
		$data['topic'] 	= $mpage->read_topic('page');
		$data['slide']	= $mpage->read_slide();		
		$data['post']	= false;*/
		
		//$this->view("page/register.php", $data);
		$this->view( 'auth2.php', $data );
		exit();
	}
	
	function save($email){		
		
		$mreg = new model_register();
		
		$user_account = $mreg->get_user_account($email);
		
		if($user_account){
			$this->msg('nok_email', $email);
		}else{			
			if(isset($_POST['nim']) && ($_POST['nim'])){
				$regmhs = $mreg->get_asisten_id($email, $_POST['nim']);
				if($regmhs) $strlevel = '13';
				else  $strlevel = '3';
				
				$regptiik = $mreg->get_mahasiswa_id($email, $_POST['nim']);
				$type='mhs';
			}else{
				$regptiik = $mreg->get_karyawan_id($email);
				$type='dosen';
			}
			
						
			if($regptiik){
				//require ('library/class.phpmailer.php');
				//$mail = new PHPMailer();
				
			
				$nama = $regptiik->nama;
				
				if($type=='mhs'){
					//$level 	= '3';
					$level 	= $strlevel;
					$staffid= "";
					$mhsid	= $regptiik->mahasiswa_id;
					$namalengkap = $nama;
					$gelarawal = "";
					$gelarakhir = "";
				}else{
					if($regptiik->is_status=='dosen'){
						$level 	= '2';
					}else{
						if($regptiik->level!=""){
							$level 	= $regptiik->level;
						}else{
							$level 	= '4';
						}
					}
					$staffid= $regptiik->karyawan_id;
					$mhsid	= "";
					
					if($regptiik->gelar_awal){
						$gelarawal = ", ".$regptiik->gelar_awal;
					}else{
						$gelarawal = "";
					}
					
					if($regptiik->gelar_akhir){
						$gelarakhir = ", ".$regptiik->gelar_akhir;
					}else{
						$gelarakhir = "";
					}
					
					$namalengkap = $nama.$gelarawal.$gelarakhir;				
				}
				
				$struser = explode("@", $email);
				
				$uname		= reset($struser);
				$email		= $email;
				$password	= $regptiik->pwd;
				$status		= 1;
				$fakultas	= $regptiik->fakultas_id;
				$foto		= $regptiik->foto;
				
				
				$saveuser = $mreg->save_user($uname, $password, $nama, $email, $level, $status, $staffid, $mhsid, $fakultas, $foto );
				
				if($saveuser){				
										
					$body='Yth. <b>'.$nama.$gelarawal.$gelarakhir.'</b><br><br><br>Terima kasih telah mendaftar di PTIIK Apps. <em>Account</em> 
								Anda sudah aktif dan dapat melakukan login ke halaman PTIIK Apps 
								dengan menggunakan <em>account</em> berikut: <br><br> Username : <b>'.$uname.'</b><br>Password  : <b>'. $password.'</b><br><br>Untuk login, silahkan mengunjungi <a href="http://filkom.ub.ac.id/auth">http://filkom.ub.ac.id/auth</a><br><br><br>Terima kasih, <br><br><br><br>
							<hr><small>Badan Pengembangan Teknologi Informasi dan Komunikasi<br>
							<b>PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER</b><br>
							<b>UNIVERSITAS BRAWIJAYA</b><br>
							Ruang B1.1 Gedung B PTIIK UB<br>
							Jl. Veteran no 8 Malang, 65145, Indonesia<br>
							telp : (0341) 577911 Fax : (0341) 577911</small>';
							
							 $text_body  = "Yth. ".$nama.$gelarawal.$gelarakhir.", \n\n";
								$text_body .= "Anda sudah aktif dan dapat melakukan login ke halaman PTIIK Apps dengan menggunakan account  berikut:\n\n";
								$text_body .= "Username:".$uname."\n";
								$text_body .= "Password:".$password."\n\n";
								$text_body .= "Untuk login, silahkan mengunjungi <a href='http://filkom.ub.ac.id/auth'>http://filkom.ub.ac.id/auth</a> \nTerima kasih, \n\n\n\n";
								$text_body .= "Badan Pengembangan Teknologi Informasi dan Komunikasi\nPROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER\nUNIVERSITAS BRAWIJAYA\nRuang B1.1 Gedung B PTIIK UB\n
							Jl. Veteran no 8 Malang, 65145, Indonesia\n
							telp : (0341) 577911 Fax : (0341) 577911";
							
					$data = array(
						'nama' => $nama,
						'email' => $email,
						'username' => $uname,
						'password' => $password,
						'status' => 1,
						'fakultas' => 'Fakultas Ilmu Komputer',
						'body' => $body,
						'textbody' => $text_body,
						'subject' => 'User Account PTIIK Apps'
					);
					
					if($this->kirim_email($data)){
						$error=0;
						$data['err'] = 1;
						$this->msg('ok', $email);
					}else{
						$this->msg('nok_email', $email);
					}
			
					
				}else{
					var_dump("asa");
					exit();
				}
			
			
			
			}else{
				$data['err'] = 3;
				$this->msg('nok', $email);
			}		
		}
	}
	
}
?>