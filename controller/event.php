<?php
class controller_event extends comscontroller {

	function __construct() {
		parent::__construct();
		//var_dump($this->method);
	}
	
	function  forgot(){
		$this->view("page/skin/isyg/forgot-pin.php");
	}
	
	function send_email_to_all(){
		$mconf = new model_eventinfo();
		
		$peserta = $mconf->get_peserta_notif();
		
		if($peserta){
			foreach($peserta as $key):
				//echo $key->title;
				$this->send_notif($key->peserta_id, $key->kegiatan_id, $key->nama, $key->title, $key->email, $key->pin, 'all');
			endforeach;	
		}
	}
	
	function pk2maba(){
		if($act=='submission'){
			 $this->read('ramadhan2015', 'submission', 'submission', $link, $id);			
		}else{
			$this->read('ramadhan2015', $act, $cat, $link, $id);
		}
		exit();
	}
	
	function ramadhan2015($act=NULL, $cat=NULL, $link=NULL, $id=NULL){
		if($act=='submission'){
			 $this->read('ramadhan2015', 'submission', 'submission', $link, $id);			
		}else{
			$this->read('ramadhan2015', $act, $cat, $link, $id);
		}
		exit();
	}
	
	function isyg($act=NULL, $cat=NULL, $link=NULL, $id=NULL){
		if($act=='submission'){
			 $this->read('isyg', 'submission', 'submission', $link, $id);			
		}else{
			$this->read('isyg', $act, $cat, $link, $id);
		}
		exit();
	}
	
	function download($id=NULL){
		$mconf = new model_eventinfo();
		$file = $mconf->get_dokumen($id);
		$val_url = $file->file_loc;
		$file_ = explode('/', $val_url);
							
		$file_ = explode('.', end($file_));
		$ext = end($file_);
						
		$url_val=$this->config->file_isyg.$val_url;
		//var_dump($url_val);	
		$this->download_file($url_val, $val_url, $ext);		
	}
	
	function delete_dok(){
		$id	 = $_POST['id'];
		$mconf = new model_eventinfo();
		$delete = $mconf->delete_dokumen($id);
		
		if($delete) echo "Success";
		else echo "Failed";
	}
	
	
	
	function index($str=NULL){
		//$this->redirect('page/skin/isyg/'.$str);
	}
	
	function get_data_event($kategori, $peserta, $kegiatan){
					
		$mconf = new model_eventinfo();
		
		$data['inf_peserta'] = $mconf->get_peserta($peserta);
		$data['inf_dokumen'] = $mconf->get_dokumen("",$peserta,"","-");
		$data['inf_tagihan'] = $mconf->get_tagihan("",$peserta,"-");
		
		$this->view('page/skin/isyg/view-data.php',$data);
	}
	
	function submit_file(){

		$allowed_ext = array('doc', 'docx', 'pdf');
		$mpage = new model_page();
	
		$kegiatanid = $_POST['kegiatan_id'];
		
		$peserta_id = (isset($_POST['tmpid']))?$_POST['tmpid']:$mpage->get_nextid_peserta_kegiatan("", $kegiatanid);
	
		if($_FILES['file']){	
			//$upload_id = $mpage->get_nextid_peserta_upload($_POST['hidval'],$peserta_id);
			$upload_id = $mpage->get_nextid_peserta_upload();
			
			$filedata    = $_FILES['file']['tmp_name'];
			$file_name = $_FILES["file"]["name"];
			
			$ext = explode('.', $file_name);
			$ext = array_pop($ext);
			
			$file_type = $_FILES["file"]["type"];
			$file_tmp_name = $_FILES["file"]["tmp_name"];
			$file_name_new = $upload_id.".".$ext;
			
			$file_size = $_FILES["file"]["size"];
			$file_url = $this->config->file_url;
			$upload_dir = 'main/assets/event/'.$_POST['hidval'].'/' .date('Y'). '-' .date('m') . DIRECTORY_SEPARATOR . $peserta_id . DIRECTORY_SEPARATOR ;	
			$upload_dir_db_ = 'event/'.$_POST['hidval'].'/' .date('Y'). '-' .date('m') . DIRECTORY_SEPARATOR . $peserta_id . DIRECTORY_SEPARATOR ;	
			$upload_dir_db = $upload_dir_db_.$file_name_new;		
			
										
			if($file_name != '') :	
				if(!in_array($ext,$allowed_ext)){			
					echo "Wrong type of file. Please upload document file with .pdf or .doc extention.";
					exit;
				}
			
				$this->upload_file_no_curl($upload_dir, $file_name_new, $filedata);
				
								
				$data_upload = array (
					'upload_id'=>$upload_id,
					'peserta_id'=>$peserta_id,
					'kategori_dokumen'=>$_POST['hidval'],
					'is_valid'=>(isset($_POST['is_valid']))?1:0,
					'keterangan'=>$_POST['note'],
					'file_loc'=>$upload_dir_db,
					'file_name'=>$file_name ,
					'file_type'=>$file_type,
					'file_size'=>$file_size,
					'tgl_upload'=>date('Y-m-d H:i:s'),
					'last_update'=>date('Y-m-d H:i:s')
				);
				$mpage->simpan_peserta_upload($data_upload);
				echo "Success";
			else:
				echo "<div class='alert alert-danger'>Please upload your file.</div>";
				exit;
			endif;			
		}
		
	}

	function save_reg(){
		$mpage = new model_page();
	
		$kegiatanid = $_POST['tmpid'];
		$komponen = $_POST['kid'];
		
		$isexist= $mpage->get_peserta_exist($_POST['ipt-email'], $kegiatanid);
	
		if($isexist):
			$this->redirect('event/isyg/reg/err');
			exit();
		else:
		
		$peserta_id = (isset($_POST['peserta_id']))?$_POST['peserta_id']:$mpage->get_nextid_peserta_kegiatan("", $kegiatanid);
		$tagihan_id =  $mpage->get_nextid_peserta_tagihan($peserta_id, $komponen);
		$pin = $this->generate_password(6);
		
		$data = array (
			"peserta_id"=>$peserta_id,
			"kegiatan_id"=>$kegiatanid,
			"pin"=>$pin,
			"jenis_peserta"=>$_POST['jpeserta'],
			"nama"=>$_POST['ipt-name'],
			"email"=>$_POST['ipt-email'],
			"tgl_registrasi"=>date("Y-m-d H:i:s"),
			"jenis_pendaftaran"=>'online',
			"instansi"=>$_POST['ipt-inst'],
			"alamat"=>$_POST['ipt-city'],
			"negara"=>$_POST['ipt-country'],
			"title"=>$_POST['ipt-title'],
			"telp"=>$_POST['ipt-telp'],
			"hp"=>$_POST['ipt-hp'],
			"is_valid"=>(isset($_POST['is_valid']))?1:0
		);
		$mpage->simpan_kegiatan_peserta($data);
		
		if($komponen && $komponen!="-"){
			$data_tagihan = array (
				"peserta_id"=>$peserta_id,
				"komponen_id"=>$komponen,
				"tagihan_id"=>$tagihan_id,
				"inf_jumlah"=>1,
				"inf_harga"=>$_POST['biaya']
			);
			$mpage->simpan_tagihan_peserta($data_tagihan);
		}
		
		if($_POST['jpeserta']=='peserta')	$this->redirect('event/isyg/reg/success');
		else $this->send_notif($peserta_id, $kegiatan_id, $_POST['ipt-name'], $_POST['ipt-title'], $_POST['ipt-email'], $pin);		
		exit;
		endif;	
	}	

	function save_feed(){
		$mpage = new model_page();
		
		$is_invalid = false;
		$kegiatanid = $_POST['tmpid'];
		$email = $_POST['feedback-email'];
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$is_invalid = true;
			$err_code = 'email';
		}
		
		$captcha = $_POST['chpid'];
		if($_POST['captcha-yx'] != $captcha) {
			$is_invalid = true;
			$err_code = 'captcha';
		}
		echo $err_code;
		if($is_invalid):
			$this->redirect('event/isyg/feed/err_feed/'.$err_code);
			exit();
		else:			
			$comment_id = $mpage->comment_id();
			$data = array (
				"comment_id"=>$comment_id,
				"content_id"=>'-',
				"event_id"=>$kegiatanid,
				"user_name"=>$_POST['feedback-name'],
				"email"=>$email,
				"title"=>$_POST['feedback-title'],
				"comment"=>$_POST['feedback-enquiry'],
				"comment_post"=>date("Y-m-d H:i:s"),
				"parent_id"=>'',
				"ip_address"=>$_SERVER['REMOTE_ADDR'],
				"is_approve"=>0,
				"last_update"=>date("Y-m-d H:i:s"),
				"category"=>'faq',
			);
			$mpage->replace_comment($data);
			$this->redirect('event/isyg/feed/success_feed');	
			exit();
		endif;	
	}

	function send_notif($pesertaid=NULL, $kegiatan=NULL, $nama=NULL, $title=NULL, $email=NULL, $pin=NULL,$cat=NULL ){
	
				
		$body='Dear <b>'.$title.". ".$nama.'</b><br><br><br>Thank you for your registration<br>
				Your ISyG2015 account has been created.
				You can now log in to ISyG website using your email address and the following pin:  <b>'. $pin.'</b><br><br>
				To log in, please follow this link <a href="'.$this->location().'event/isyg/submission">'.$this->location().'event/isyg</a><br><br><br>Regards, <br><br><br><br>
				<hr><small>ISyG 2015 comitee<br>
				Faculty of Computer Science (FILKOM f.k.a PTIIK) Brawijaya University<br>
				8 Veteran Road | Malang, Indonesia - 65145<br>
				General: (0341) 577-911 | Fax: (0341) 577-911 | email: ptiik[at]ub.ac.id
				</small>';
			
				$text_body  = "Dear ".$title.". ".$nama.", \n\n";
					$text_body .= "Thank you for your registration\n\n";
					$text_body .= "Your ISyG2015 account has been created. You can now log in to ISyG website using your email address and the following pin:".$pin."\n\n";
					$text_body .= "To log in, please follow this link <a href='http://filkom.ub.ac.id/event/isyg/submission'>http://filkom.ub.ac.id/event/isyg</a> \nRegard, \n\n\n\n";
					$text_body .= "ISyG 2015 comitee\nFaculty of Computer Science (FILKOM f.k.a PTIIK) Brawijaya University\n8 Veteran Road | Malang, Indonesia - 65145\n
				General: (0341) 577-911 | Fax: (0341) 577-911 | email: ptiik[at]ub.ac.id";
				
			
			
			$data = array(
				'nama' => $nama,
				'email' => $email,
				'username' => $email,
				'password' => $pin,
				'status' => 1,
				'fakultas' => 'Fakultas Ilmu Komputer',
				'body' => $body,
				'textbody' => $text_body,
				'subject' => 'PIN ISYG 2015'
			);
			//$this->kirim_email($data);
			if($cat=='forgot'){
				if($this->kirim_email($data)) $this->redirect('event/isyg/success/success_kirim');
				else $this->redirect('event/isyg/reg/err');
			}else{
				if($cat=='all'){
					$this->kirim_email($data);
				}else{
					if($this->kirim_email($data)) $this->redirect('event/isyg/success/presenter');
					else $this->redirect('event/isyg/reg/err');
				}
			}
				
	}
	
	
	function read_data($str=NULL, $url=NULL,$eventid=NULL, $data=NULL){
		$data_array = explode("/", $str);
			
		$cat = $data_array[0];
		$val = end($data_array);
		
		$data['lang'] 		= 'en';//'EN';
		$data['kategori'] = $cat;	
		
		if($cat=='reg_inf'){
					
			$mconf = new model_eventinfo();
						
			if(isset($_POST['id'])) $data['komponen'] = $_POST['id'];
			else $data['komponen'] = "";
			
			if(isset($_POST['kegiatan'])) $data['kegiatan_id'] = $_POST['kegiatan'];
			else $data['kegiatan_id'] = "";
			
			if(isset($_POST['peserta'])) $data['jenis_peserta'] = $_POST['peserta'];
			else $data['jenis_peserta'] = "";
			
			if(isset($_POST['kpeserta'])) $data['jenis_peserta_id'] = $_POST['kpeserta'];
			else $data['jenis_peserta_id'] = "";
			
			if(isset($_POST['satuan'])) $data['satuan'] = $_POST['satuan'];
			else $data['satuan'] = "";
			
			if(isset($_POST['harga'])) $data['harga'] = $_POST['harga'];
			else $data['harga'] = "";
			
			if(isset($_POST['biaya'])) $data['nama_biaya'] = $_POST['biaya'];
			else $data['biaya'] = "";
			
			$data['negara'] = $mconf->get_negara();
			$data['jpeserta'] = $mconf->get_jenis_peserta_by_biaya( $data['kegiatan_id']);
			
			$this->view('page/skin/isyg/reg_form.php',$data);
		}
		
		if($cat=='register'){
			if(isset($_POST['input'])){
				$inputflag 	= md5($_POST["input"]);
				$sessiontmp = $_POST["flag"];
			}else{
				$inputflag 		= "";
				$sessiontmp = "";
			}
			
			$mconf = new model_eventinfo();
			/*$data['negara'] = $mconf->get_negara();
			$data['jpeserta'] = $mconf->get_jenis_peserta_by_biaya($eventid);*/
			
			$data['mconf'] = $mconf;
			$data['komponen'] = $mconf->get_komponen_biaya($eventid,'jenis_peserta');
			
			$this->view('page/skin/isyg/registration.php',$data);
		}
		
		if($cat=='submission'){
			$data['submission']=1;
			$this->view('page/skin/isyg/submission.php',$data);
		}
		
		if($cat=='faq'){
			$mconf = new model_eventinfo();

			$faq_list = $mconf->get_feedback($eventid,'');
			if($faq_list) {
				foreach($faq_list as $fl){
					$tmp['id'] = $fl->comment_id; 
					$tmp['email'] = $fl->email;
					$tmp['author'] = $fl->user_name;
					$tmp['faq_question'] = $fl->comment;
					$jawaban = $mconf->get_feedback($eventid, $fl->comment_id);
					$tmp['faq_answer'] = $jawaban[0]->comment;
					$temp[] = $tmp;
				}
			}
			$data['faq_list'] = $temp;
			$this->view('page/skin/isyg/faq_form.php',$data);
		}
		
		if($cat=='feedback'){
			$data['jx'] = rand(1,20);
			$data['kx'] = rand(21,50);
			$data['yx'] = $data['kx'] - $data['jx'];
			$data['kegiatan_id'] = $eventid;
			$this->view('page/skin/isyg/feed_form.php', $data);
		}
	}
	
	function read($str=NULL,$act=NULL, $cat=NULL, $link=NULL, $id=NULL){
	
		if($cat){$this->detail($str,$act, $cat, $link, $id); exit(); }
		
		$mpage = new model_page();
	
		$data['unit'] ='-';
		
		$data['kevent'] = $str;
		
		if($str):
			$sevent= $mpage->get_ptiik_event($str);
			if($sevent):
				$event = $sevent->kegiatan_id;
				$title = $sevent->nama;
				$desc = $sevent->keterangan;
				$logo = $sevent->logo;
			else:
				$event ="";
				$title = "";
				$desc = "";
				$logo = "";
			endif;
		else:
			$event ="";
			$title = "";
			$desc = "";
			$logo = "";
		endif;
		
		if($str=='isyg'){$strskin = 'isyg';$data['lang'] = 'en';}
		else{ $strskin = 'event';$data['lang'] = 'in';}
		
		$data['skin'] = $strskin;
		
		$data['eventid'] = $event;
		$data['page_title'] = $title;
		$data['page_desc'] = $desc;
		$data['logo'] = $logo;
		
		$date = date("Y-m-d");
		$data['mpage']	= $mpage;
				
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',3,"urut","",$event);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu',"","","","","",$event);
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',3,"","",$event);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer',"","","","","",$event);
		$data['speaker'] = $mpage->read_slide($data['unit'],$data['lang'],4,"'speaker'","","",$event);		
		$data['slide'] = $mpage->read_slide($data['unit'],$data['lang'],5,"'slider'","","",$event);	
		$data['gallery'] = $mpage->read_slide($data['unit'],$data['lang'],5,"'gallery'","","",$event);	
		
		$data['kerjasama']	= $mpage->read_content($data['unit'],$data['lang'],'kerjasama',"","","","","",$event);		
		
		$data['bulan'] = date('m');
		$data['dateNow'] = $date;
		$data['jenis_kegiatan'] = $mpage->jenis_kegiatan();
		
		$this->add_script('js/page/agenda/agenda.js');		
		$this->add_style('calendar/zabuto_calendar.min.css');
		$this->add_script('calendar/zabuto_calendar.min.js');
		
		if($event) $this->view('page/skin/'.$strskin.'/index.php', $data);
		else $this->view('page/skin/'.$strskin.'/event.php', $data);
	}
	
	function detail($str=NULL, $act=NULL, $cat=NULL, $id=NULL, $id_page=NULL){
		$mpage = new model_page();
		
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $cat;
		$data['id'] 	= $id;
		
		$data['lang'] = 'en';//'EN';
		$data['unit'] ='-';
		
		$data['kevent'] = $str;
		
		if($str):
			$sevent= $mpage->get_ptiik_event($str);
			if($sevent):
				$event = $sevent->kegiatan_id;
				$title = $sevent->nama;
				$desc = $sevent->keterangan;
				$logo = $sevent->logo;
			else:
				$event ="";
				$title = "";
				$desc = "";
				$logo = "";
			endif;
		else:
			$event ="";
			$title = "";
			$desc = "";
			$logo = "";
		endif;
		
		if($str=='isyg') $strskin = 'isyg';
		else $strskin = 'event';
		$data['skin'] = $strskin;
					
		$data['eventid'] = $event;
		$data['page_title'] = $title;
		$data['page_desc'] = $desc;
		$data['logo'] = $logo;
		
		$data['eventid'] = $event;
		
		switch($cat){
		
			case 'kerjasama':								
				if($id):
					$data['detail'] = $mpage->read_content($data['unit'],$data['lang'],"", "", $id_page);	
				else:
					$data['post'] = $mpage->read_content($data['unit'], $data['lang'],'kerjasama',"","",100,"","",$event);	
				endif;
			break;
			
			case 'err':
				$data['msg'] = "<div class='alert alert-warning'>You have been registered to this event.</div>";
			break;
			
			case 'success':
				$data['msg'] = "<div class='alert alert-success'>Your registration was successfully submitted.</div>";
			break;
			
			case 'success_feed':
				$data['msg'] = "<div class='alert alert-success'>Thanks for contacting us, we will reply to you shortly.</div>";
			break;
			
			case 'err_feed':
				if($id == 'email') $data['msg'] = "<div class='alert alert-danger'>Not a valid email address.</div>";
				else if($id == 'captcha') $data['msg'] = "<div class='alert alert-danger'>That CAPTCHA was incorrect. Please try again</div>";
				else $data['msg'] = "<div class='alert alert-danger'>There's problem while submitting your question. Please try again</div>";
			break;
			
			case 'success_kirim':
				$data['msg'] = "<div class='alert alert-success'>Your PIN was send to your email address.</div>";
			break;
			
			case 'submission':
				$data['submission'] = 1;
				if(isset($_POST['ipt-pin'])) $pin = $_POST['ipt-pin'];
				else  $pin="";			
				
				if($pin):
					$data['pin'] = $pin;
					$data['detail_peserta'] = $mpage->get_master_kegiatan_peserta("", $event, "","","","",md5($pin));
				endif;
			break;
			
			case 'pin':
				
				if(isset($_POST['ipt-email'])) $email = $_POST['ipt-email'];
				else $email="";	
				if($email):
					$data['forgotpin']=1;
					$peserta = $mpage->get_master_kegiatan_peserta("", $event, "","","",$email);
					if($peserta){
						if($peserta->jenis_peserta=='peserta'){
							$data['pin'] = "<div class='alert alert-danger'>Sorry, you don't have any permission to access this page.</div>";	
						}else{
							$data['pin'] = "<";
							$this->send_notif($peserta->peserta_id, $peserta->kegiatan_id, $peserta->nama, $peserta->title, $peserta->email, $peserta->pin, 'forgot');
						}
					}else{
						$data['pin'] = "<div class='alert alert-warning'>Sorry, you don't have any permission to access this page. Un-registered email address.</div>";		
					}
								
				endif;
			
			break;
			
			case 'presenter':
				$data['msg'] = "<div class='alert alert-success'>Your registration was successfully submitted. Check your email to get PIN that will be used to submit paper.</div>";
			break;
		
			case 'news':				
				if($id):
				 if($id_page):
					$mpage->update_hits_content($id_page);
					$this->add_script('js/submit.js');
					$ok = $mpage->read_content($data['unit'],$data['lang'],"", "",$id_page);
					if($ok) $data['detail']= $ok;
					else $data['detail'] = $mpage->read_content($data['unit'],"","", "",$id_page,"","","",$event);
					//edit
					$data['title_page']=true;	
				 else:
					$mpage->update_hits_content($id);
					$this->add_script('js/submit.js');
					$data['detail'] = $mpage->read_content($data['unit'],$data['lang'],"", $id);
					//edit
					$data['title_page']=true;	
				endif;
				else:
					$data['post'] = $mpage->read_content($data['unit'], $data['lang'],'news',"","",100,"","",$event);	
				endif;
			break;
			
			case 'pengumuman':								
				if($id):
					if($id_page):
						$mpage->update_hits_content($id_page);
						$ok = $mpage->read_content('-',$data['lang'],"", "",$id_page);
						if($ok) $data['detail']= $ok;
						else $data['detail'] = $mpage->read_content('-',"","", "",$id_page);
						
					else:
						$mpage->update_hits_content($id_page);
						$data['detail'] = $mpage->read_content('-',$data['lang'],"", $id,"","","",$event);	
					endif;
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'pengumuman',"","",100,"","",$event);	
				endif;
			break;
			
			case 'register':
				$this->read_data('register',"", $data);
				exit();
			break;
			
			/*case 'event':								
				if($id):					
					$data['detail'] = $mpage->read_event($data['unit'],$data['lang'],"", $id_page,1,"","",$event);	
				else:
					$this->event();
				endif;
			break;*/
			
			
			
			default:
			
				if($id):
					$ok = $mpage->read_content($data['unit'],$data['lang'],"", "",$id);
					if($ok) $data['detail']= $ok;
					else $data['detail'] = $mpage->read_content($data['unit'],"","", "",$id,"","","",$event);
				else:
					$data['post'] = $mpage->read_content($data['unit'],$data['lang'],"", "",$id,1,"","",$event);	
				endif;
			break;
		}
		
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu',"","","","","",$event);
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',5,"","",$event);
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5,"","",$event);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer',"","","","","",$event);

		
		if($cat):	
			$this->add_script('js/datatables/jquery.dataTables.js');	
			$this->add_script('js/datatables/DT_bootstrap.js');
			
			$data['view_slide']= 0;
			if($str=='kontak'||$str=='contact') $this->add_script('js/submit.js');
			
			
			if(strtolower($str)=='gallery' || strtolower($str)=='video' || strtolower($str)=='video-streaming'){
				//$this->add_style('ptiik/css/menuapps.slider.min.css');	
				$this->view( 'page/view-gallery.php', $data);
			}else{
				$this->view( 'page/skin/'.$strskin.'/view-content.php', $data);
			}
		else:
			$this->view( 'page/index.php', $data);
		endif;
	}
}