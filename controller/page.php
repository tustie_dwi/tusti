<?php
class controller_page extends comscontroller {

	function __construct() {
		parent::__construct();
		$this->limit = 8;
	}
	/*-- rekrutmen --*/
	function save_submission(){
		$allowed_ext = array('doc', 'docx', 'pdf');
		$mpage = new model_page();
	
		$row = $mpage->get_master_kegiatan("",$_POST['tmpid'],"-");
		$kegiatanid = $row->kegiatan_id;
		
		$peserta_id = (isset($_POST['peserta_id']))?$_POST['peserta_id']:$mpage->get_nextid_peserta_kegiatan($_POST['hidId_mhs'], $kegiatanid);
		$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($row->judul))))));
				
		if($_FILES['file']){			
			$filedata    = $_FILES['file']['tmp_name'];
			$file_name = $_FILES["file"]["name"];
			$file_type = $_FILES["file"]["type"];
			$file_tmp_name = $_FILES["file"]["tmp_name"];
			$file_size = $_FILES["file"]["size"];
			$file_url = $this->config->file_url;
			$upload_dir = 'assets/rekrutmen/cv/' .date('Y'). '-' .date('m') . DIRECTORY_SEPARATOR . $_POST['hidId_mhs'] . DIRECTORY_SEPARATOR ;	
			$upload_dir_db_ = 'rekrutmen/cv/' .date('Y'). '-' .date('m') . DIRECTORY_SEPARATOR . $_POST['hidId_mhs'] . DIRECTORY_SEPARATOR ;	
			$upload_dir_db = $upload_dir_db_.$file_name;
			
			$ext = explode('.', $file_name);
			$ext = array_pop($ext);
			
										
			if($file_name != '') :	
				if(!in_array($ext,$allowed_ext)){			
					$this->read('rekrutmen', $_POST['tmpid'],"","<div class='alert alert-danger'>Wrong type of file. Please upload document file with .pdf or .doc extention.</div>");
					exit;
				}
			
				$this->upload_file($upload_dir, $filedata, $file_name, $file_name, $file_size);
				
				$upload_id = $mpage->get_nextid_peserta_upload('cv',$peserta_id);
				
				$data_upload = array (
					'upload_id'=>$upload_id,
					'peserta_id'=>$peserta_id,
					'kategori_dokumen'=>'cv',
					'is_valid'=>(isset($_POST['is_valid']))?1:0,
					'file_loc'=>$upload_dir_db,
					'file_name'=>$file_name ,
					'file_type'=>$file_type,
					'file_size'=>$file_size,
					'tgl_upload'=>date('Y-m-d H:i:s'),
					'last_update'=>date('Y-m-d H:i:s')
				);
				$mpage->simpan_peserta_upload($data_upload);
			else:
					$this->read('rekrutmen', $_POST['tmpid'],"","<div class='alert alert-danger'>Please upload your CV.</div>");
					exit;

			endif;			
		}
		
		if($_FILES['cover']){	
			$filedata_c   = $_FILES['cover']['tmp_name'];
			$file_name_c = $_FILES["cover"]["name"];
			$file_type_c = $_FILES["cover"]["type"];
			$file_tmp_name_c = $_FILES["cover"]["tmp_name"];
			$file_size_c = $_FILES["cover"]["size"];
			$file_url_c = $this->config->file_url;
			$upload_dir_c = 'assets/rekrutmen/cover/' .date('Y'). '-' .date('m') . DIRECTORY_SEPARATOR . $_POST['hidId_mhs'] . DIRECTORY_SEPARATOR ;	
			$upload_dir_db_c_ = 'rekrutmen/cover/' .date('Y'). '-' .date('m') . DIRECTORY_SEPARATOR . $_POST['hidId_mhs'] . DIRECTORY_SEPARATOR ;	
			$upload_dir_db_c = $upload_dir_db_c_.$file_name_c;
			
			$ext_c = explode('.', $file_name_c);
			$ext_c = array_pop($ext_c);
			
							
			if($file_name_c != '') :	
				if(!in_array($ext_c,$allowed_ext)){
					$this->read('rekrutmen', $_POST['tmpid'],"","<div class='alert alert-danger'>Wrong type of file. Please upload document file with .pdf or .doc extention.</div>");
					exit;
				}
			
				$this->upload_file($upload_dir_c, $filedata_c, $file_name_c, $file_name_c, $file_size_c);
				
				$upload_id_c = $mpage->get_nextid_peserta_upload('cover',$peserta_id);
				
				$data_upload_c = array (
					'upload_id'=>$upload_id_c,
					'peserta_id'=>$peserta_id,
					'kategori_dokumen'=>'cover',
					'is_valid'=>(isset($_POST['is_valid']))?1:0,
					'file_loc'=>$upload_dir_db_c,
					'file_name'=>$file_name_c,
					'file_type'=>$file_type_c,
					'file_size'=>$file_size_c,
					'tgl_upload'=>date('Y-m-d H:i:s'),
					'last_update'=>date('Y-m-d H:i:s')
				);
				$mpage->simpan_peserta_upload($data_upload_c);
				
			endif;			
		}
		
		
		$data = array (
			"peserta_id"=>$peserta_id,
			"kegiatan_id"=>$kegiatanid,
			"jenis_peserta"=>'mahasiswa',
			"mahasiswa_id"=>$_POST['hidId_mhs'],
			"nama"=>$_POST['nama'],
			"email"=>$_POST['email'],
			"tgl_registrasi"=>date("Y-m-d H:i:s"),
			"jenis_pendaftaran"=>'online',
			"telp"=>$_POST['telp'],
			"hp"=>$_POST['hp'],	
			"is_valid"=>(isset($_POST['is_valid']))?1:0
		);
		$mpage->simpan_kegiatan_peserta($data);
		unset($_POST['btn_sub']);	
		
		$this->redirect('page/rekrutmen/success/'.$content.'/'.$_POST['tmpid']);
		//$this->read('rekrutmen', $_POST['tmpid'],"","<div class='alert alert-success'>Your submission was successfully submitted!!</div>");
		exit;
		
	}
	
	function submission(){
				
		if(isset($_POST['nim_mhs'])){			
			$this->apply_form("","", $_POST['tmp_id'], $_POST['nim_mhs']);	
		}
	}	
	
		
	function apply_form($mpage_=NULL, $id=NULL, $tmp_id=NULL, $str=NULL, $err=NULL, $link=NULL){
				
		if(isset($_POST['input'])){
			$inputflag 	= md5($_POST["input"]);
			$sessiontmp = $_POST["flag"];
		}else{
			$inputflag 		= "";
			$sessiontmp = "";
		}
		
		$tgl		= date("Y-m-d");
		
		if($mpage_) $mpage=$mpage_;
		else $mpage = new model_page();
		
		$minfo = new model_services();
		
		$data['mpage']		= $mpage;
		$data['kalender'] 	= $mpage->get_master_kegiatan("",$id,"-",$tgl);
		$data['hidid'] 		= $tmp_id;
		$data['kegiatanid'] = $id;
				
		if($err) $data['msg'] 	=$err;
		$data['kegiatan'] = $mpage->get_master_kegiatan("",$tmp_id,"-");
		
		if(isset($str)&& ($inputflag == $sessiontmp)){
				$nim				= $str;
				$cek_data_masuk		= $mpage->get_master_kegiatan_peserta($nim,$tmp_id);
				$data_mhs			= $minfo->get_mhs($nim);
			
				if(! $cek_data_masuk){
					$data['mhs'] 	= $data_mhs;
					if($data['mhs']){						
						/*if($this->config->lang == "in") $data['msg'] 	= "Data anda telah masuk dan sedang diproses. Silahkan menghubungi akademik untuk informasi lebih lanjut.";
						else $data['msg'] 	= "Your data have entered and still in process. Please contact academics for more information.";*/
					}else{
						$nim="";						
						$data['msg'] 	= "Please check your input.";
					}
				}
				else{
					
					 $data['msg'] 	= "Your data have entered and still in process. Please contact ".$data['kegiatan']->unit." for more information.";
					if($data_mhs) $data['mhs_entered'] 	= $cek_data_masuk;
				}
				
			}else{
				$nim="";
				
			}
			
			$data['nim']	= $nim;
		
			$this->add_script('js/submit.js');
			
			$this->view('page/services/apply_form.php', $data);
		
	}
	
	function rekrutmen($strlink=NULL, $id=NULL, $str=NULL, $err=NULL){
	//echo isset($_POST['hidId_mhs']);
		if(isset($_POST['btn_sub']) ){ $this->save_submission(); exit(); }else{
		
		if($strlink=='success'){
			$this->read('rekrutmen', $str, $id, 'ok');
			exit();
		}
		
		if($strlink=='apply') $this->read('rekrutmen', $str, $id, $err);
		else $this->read('rekrutmen', $id);
		exit();		
		}		
	}
	/*-- end rekrutmrn --*/
	
	function read_gallery($str=NULL){
		if($str) $id=$str;
		else $id = $_POST['id'];
		
		$data['lang'] = $_POST['lang'];
		$data['unit'] = $_POST['unit'];
		
		$mpage = new model_page();
		
		if($id)	$mpage->update_hits_file($id);
		
		$mpage = new model_page();
		
		$data['detail'] = $mpage->read_slide(0,'in',1,"",$id);
		if($data['detail']->content_category=='video'){
			$data['post'] 	= $mpage->read_slide(0,'in',6,"'video'");
		}else{
			$data['post'] 	= $mpage->read_slide(0,'in',6,"'gallery'","",$data['detail']->parent_id);
		}
		
				
		$this->add_style('ptiik/css/tubestyle.min.css');
		$this->add_style('ptiik/css/base.tube.min.css');
		
		$this->view('page/gallery/index.php', $data);
		
	}
	
	function read_video($keyword=null){
		$mpage = new model_page();
		if(!$keyword)
			echo json_encode($mpage->read_gal(null,'in',$this->limit,"'video'",null,null,null,null,$_POST['hal']));
		else
			echo json_encode($mpage->read_gal(null,'in',$this->limit,"'video'",null,null,null,$keyword,$_POST['hal']));
	}
	
	function read_images($keyword=null){
		$mpage = new model_page();
		$count = 0;
		$parent = $mpage->read_gal(null,'in',null,"'gallery'","",'0');
		$result = array();
		foreach($parent as $key => $data){
			if($key<$_POST['hal']) continue;
			if(!$keyword)
				$data->content = $mpage->read_gal(null,'in',$this->limit,"'gallery'","",$data->file_id);
			else
				$data->content = $mpage->read_gal(null,'in',$this->limit,"'gallery'","",$data->file_id,null,$keyword);
			$result [] = $data;
			$count += ($data->content)? count($data->content) :0;
			if($count>=$this->limit) break;
		}
		echo json_encode($result);
	}
	
	function read_search(){
		$mpage = new model_page();
		echo "{\"video\":";
		$this->read_video(strToLower($_POST['keyword']));
		echo ",\"images\":";
		$this->read_images(strToLower($_POST['keyword']));
		echo "}";
	}
	
	
	function init_calendar_json(){
		$mpage = new model_page();
		$data = array();
		
		$date = date("Y-m-d");
		$sql = $mpage->get_agenda_json($date);
		
		if($sql){
			foreach($sql as $key){
				$temp = array(
					"date" => $key->tgl_mulai,
				    "badge" => true,
				    "title" => "-",
				    "body" => "-",
				    "footer" => "-",
				    "classname" => "-"
				);
				array_push($data, $temp);
			}
			echo json_encode($data);
		}
		else{
			echo "";
		}
		
	}
	
	function get_event_hari(){
		$data['lang'] = $this->config->lang;//'EN';
		//if(! isset($_POST['tgl'])) $this->redirect($this->location('page/event'));
		if(! isset($_POST['tgl'])) $date=date("Y-m-d");
		else $date = $_POST['tgl'];
		
		$tgl = explode("-", $date);
		$mpage = new model_page();
		$data = $mpage->get_agenda_bulan($tgl[2],$tgl[1], $tgl[0], $data['lang']);
		if($data){
			$data = json_encode($data);
			echo $data;
		}
		else echo '';
	}
	
	function event_json(){
		$data['lang'] = $this->config->lang;//'EN';
		if(! isset($_POST['init'])) $this->redirect($this->location('page/event'));
		$mpage = new model_page();
		$fak = $this->config->fakultas;
		$cabang = $this->config->cabang;
		
		$data = $mpage->get_agenda_json("",$data['lang']);
		
		if($data){
			$data = json_encode($data);
			echo $data;
		}
		else echo '';
	}
	
	function get_event_bulan(){
		$data['lang'] = $this->config->lang;//'EN';
		if(! isset($_POST['bulan'])) $this->redirect($this->location('page/event'));
		$mpage = new model_page();
		$data = $mpage->get_agenda_bulan("",$_POST['bulan'], $_POST['tahun'], $data['lang']);
		if($data){
			$data = json_encode($data);
			echo $data;
		}
		else echo '';
	}
	
	function event(){
		$mpage = new model_page();
		$data['lang'] = $this->config->lang;//'EN';
		$data['unit'] ='0';
		
		$data['mimg'] = new imageprocessing();
		
		$date = date("Y-m-d");
		$data['mpage']	= $mpage;
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['group_riset'] = $mpage->get_unit_kerja('riset','', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',5);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		$data['gallery'] = $mpage->read_slide('0',$data['lang'],8,"'video'");		
		
		$data['bulan'] = date('m');
		$data['dateNow'] = $date;
		$data['jenis_kegiatan'] = $mpage->jenis_kegiatan();
		$data['view_slide']= 0;
		
		$this->add_script('js/page/agenda/agenda.js');		
		$this->add_style('calendar/zabuto_calendar.min.css');
		$this->add_script('calendar/zabuto_calendar.min.js');
		// $this->add_script('js/datatables/jquery.dataTables.js');	
		// $this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view('page/event/event.php', $data);
	}
	
	function notfound($str=NULL){
		if($str){
			echo $str;
		}				
		exit();
	}
	
	
	function read_($str=NULL){
	
		if($str){
			$data['in_session'] = $str;
			
			$cek = new model_page();
			
			$file_ = $cek->cek_file($str);
			$publish = $file_->is_publish;
			
			if($publish==0){
				if(isset($_POST['password'])){
									
					$data['pass'] = $_POST['password'];
					$data['posts'] = $file_;
				}else{
					$data['posts'] = "";
				}
			}else{				
				
				$data['pass'] = $this->config->file_key;
				$data['posts'] = $file_;
			}
			
			$this->view("page/view-file.php", $data);
		}
	}
	
	function alumni($str=NULL){
	
		$minfo = new model_info();
		
				
		if($str){
			$data['posts'] = $minfo->get_wisudawan("","", $str);
		}else{
			$data['posts'] = $minfo->get_wisudawan("","", "");
		}
		
		$this->view( 'info/wisuda/index.php', $data );
	}
	
	function read_data($str=NULL, $url=NULL, $unit_=NULL){
		
			$data_array = explode("/", $str);
			
			$cat = $data_array[0];
			$val = end($data_array);
			
			$data['lang'] 		= $this->config->lang;//'EN';
			$data['kategori'] = $cat;	

			if($cat=="grafik_mhs" || $cat=="grafik_mhs_lulus"):
				$mconf = new model_info();
				
				$data['result'] = $mconf->get_angkatan_grafik();
				$data['mconf'] = $mconf;
			
				$this->add_script('admin/js/raphael-min.js');
				$this->add_script('admin/js/morris.min.js');
				$this->add_script('admin/js/grafik.js');
												
				$this->view( 'info/general/grafik.php', $data);
				
				
			endif;			
			
			if($cat=="apps"):				
				$this->redirect('info/services');
				exit();
			endif;			
			
			if($cat=="mutu"):
				$mpage = new model_page();
					
								
				$data['mpage'] 			= $mpage;
				$data['kategori_data']	= $val;
				
				$data['unit_mutu']	= $mpage->read_mutu_unit($val, $data['lang']);
				if($val=='aim')	$data['siklus']	= $mpage->read_mutu_siklus($val, $data['lang']);
				
				$this->add_script('js/datatables/jquery.dataTables.js');	
				$this->add_script('js/datatables/DT_bootstrap.js');
				
				$this->view( 'info/mutu/index.php', $data );
			endif;
			
			if($cat=="sitemap"):
				$mpage = new model_page();
				
				$data['mpage'] 			= $mpage;
				
			
				if($unit_):
					$data['unit_list'] 	= $unit_;
					$data['unit'] 		= $data['unit_list']->unit_id;
				else:
					$data['unit'] =0;
				endif;
				
				$data['posts'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
							
				$this->view( 'info/general/view-sitemap.php', $data);
			endif;
			
			
			if($cat=="prestasi"):
				$minfo = new model_info();
		
				$data['posts'] 		= $minfo->read_prestasi("",'non');
				$data['aposts'] 	= $minfo->read_prestasi("",'akademik');
								
				$this->view( 'info/prestasi/index.php', $data );
			endif;
			
			if($cat=="alumni"):
				$minfo = new model_info();
		
				$data['total'] = $minfo->get_wisudawan($data['lang'],$val);
				$data['angkatan'] = $minfo->get_angkatan_grafik();
				$this->view( 'info/wisuda/index_wisuda.php', $data );
			endif;
			
			if(($cat=="beasiswa") || ($cat=="karir")):
				
				$mpage = new model_page();
				$data['mpage'] 			= $mpage;
		
				$data['posts'] = $mpage->read_content('-', $data['lang'],$cat,"","",100);	
			
				$this->view('info/general/index.php', $data );
			endif;
			
			if($cat=="staff"):
				$minfo = new model_info();
				
							
				$data['kategori_data']	= $val;
				
				if($unit_):
					$data['unit_list'] 	= $unit_;
					$data['unit'] 		= $data['unit_list']->unit_id;
				else:
					$data['unit'] ="";
				endif;
						
				$data['posts'] = $minfo->get_civitas("",$val, "", "", "", $data['unit']);
				$data['abjad'] = $minfo->get_civitas_abjad("",$val, "", $data['unit']);
				
				$this->view('info/general/index.php', $data );
			endif;
			
			if(strtolower($cat)=="s2"):
				$minfo = new model_info();
				
				$data['kategori_data']	= $val;
				if($val=='dosen')	$data['posts'] = $minfo->get_civitas("",$val,"","S3");
				else 	$data['posts'] = $minfo->get_civitas("",$val);
				
				$data['abjad'] = $minfo->get_civitas_abjad("",$val);
				$this->view('info/general/index.php', $data );
			endif;
			
			
			if($cat=="fasilitas"):
				$mpage = new model_page();
				
				
				$data['kategori_data']	= $val;
				
				if($val=='lab')	$data['posts'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
				else $data['posts'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
				
				$this->view('info/general/index.php', $data );
			endif;
			
			if($cat=="jurnal"):
				$minfo = new model_info();
				
				$data['posts'] = $minfo->get_rekap_penelitian('publikasi',"",'jurnal');
				/*$data['kategori_data']	= $val;
				
				if($val=='lab')	$data['posts'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
				else $data['posts'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);*/
				
				$this->view('info/general/jurnal.php', $data );
			endif;
			
			if($cat=="pengabdian"):
				$minfo = new model_info();
				
				$data['posts'] = $minfo->get_rekap_pengabdian('pengabdian');
								
				$this->view('info/general/pengabdian.php', $data );
			endif;
			
			if($cat=="map"):
				
				$mconf = new model_info();
				$data['post'] 	= $mconf->get_lokasi();
		
				if(isset($_POST['cmblokasi'])){
					$data['cmblokasi'] = $_POST['cmblokasi'];
				}else{
					$data['cmblokasi'] = 'A1';
				}
				
				if($data['cmblokasi']!='0'){
					$data['dosen'] = $mconf->get_civitas("",'dosen',$data['cmblokasi']);
					$data['staff'] = $mconf->get_civitas("",'staff',$data['cmblokasi']);
				}else{
					$data['dosen'] = "";
					$data['staff'] = "";
				}
				
				$data['val'] = $cat;
				$this->view('info/map/index.php', $data );
			endif;
	}
	
	function search(){
		$mpage = new model_page();
		if(! isset($_POST['searchTxt'])) $this->redirect('page');
		
		$data['search'] = $_POST['searchTxt'];
		
		$data['lang'] = $this->config->lang;//'EN';
		$data['unit'] ='0';
		
		$data['mimg'] = new imageprocessing();
		
		$data['mpage']	= $mpage;
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['group_riset'] = $mpage->get_unit_kerja('riset','', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',5);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		
		$data['view_slide']= 0;
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view('page/view-search.php', $data);
	}
	
	function searchForId($id) {
		
		$array = $this->search_json();
	   foreach ($array as $key => $val) {
		   if ($val['uid'] === $id) {
			   return $key;
		   }
	   }
	   return null;
	}
	
	function search_json($nilai=NULL, $unit=NULL, $url_unit=NULL, $url_cat=NULL, $url_val=NULL){
		$mpage = new model_page();
		
		$fak = $this->config->fakultas;
		$cabang = $this->config->cabang;
		
		$data['mimg'] = new imageprocessing();
		$data = $mpage->get_search_content($nilai, $this->config->lang, $fak, $cabang, $unit);
		$json = array();
		
		if($data){
			foreach($data as $key){
				$kategori= $key->content_category;
				
				$judul_link=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($key->content_title))))));
				
				if($key->content_data && $kategori=='footer'):
					$url_web = $key->content_data;
				else:
					if(isset($url_unit)):
						$url = $url_unit."/".$url_cat."/".$url_val;
						if($kategori=='news' || $kategori=='pengumuman' || $kategori=='beasiswa'|| $kategori=='event'):
							$url_web= $this->location($url.'/read/'.$key->content_category.'/'.$judul_link.'/'.$key->id);
						else:
							$url_web= $this->location($url.'/read/'.$judul_link.'/'.$key->id);
						endif;
					else:
						if($kategori=='news' || $kategori=='pengumuman' || $kategori=='beasiswa' || $kategori=='event'):
							$url_web= $this->location('page/read/'.$key->content_category.'/'.$judul_link.'/'.$key->id);
						else:
							if($key->kode_unit){
								$url_web= $this->location('unit/'.$key->kategori.'/'.$key->kode_unit.'/read/'.$judul_link.'/'.$key->id);
							}else{
								$url_web= $this->location('page/read/'.$judul_link.'/'.$key->id);
							}
						endif;
					endif;
				endif;
				
				if($key->content_thumb_img) $imgthumb = $this->config->file_url_view."/".$key->content_thumb_img; 
				else $imgthumb = $this->config->default_thumb_web;
				$temp = array(
					'id' => $key->id,
					'judul' => $key->content_title,
					'isi' => strip_tags($key->content),
					'tgl' => $key->content_upload,
					'img' => $imgthumb,
					'kategori' => $key->content_category,
					'url_web'=> $url_web
				);
				
				array_push($json, $temp);
			}
		}

		$data = $mpage->get_search_karyawan($nilai);
		if($data){
			foreach($data as $key){
				if($key->foto) $img = $this->config->file_url_view."/".$key->foto; 
				else $img = $this->config->default_thumb_web;
				
				//$img = $this->location('fileupload/assets/'.$key->foto);
				$url_web = $this->location('info/staff/'.$key->karyawan_id);
				$temp = array(
					'id' => $key->karyawan_id,
					'judul' => $key->nama,
					'isi' => strip_tags($key->biografi),
					'tgl' => $key->last_update,
					'img' => $img,
					'kategori' => ucfirst($key->is_status),
					'url_web'=> $url_web
				);
				array_push($json, $temp);
			}
		}
		
		echo json_encode($json);
	}
	
	
	function index($str=NULL){
		var_dump("aa");
		if($str){
			$this->read($str);
			exit();			
		}
		
		$mpage = new model_page();
		//$mimg = new imageprocessing();
		$data['lang'] = $this->config->lang;//'EN';
		
		$data['unit'] ='0';
		//$data['mimg'] = new imageprocessing();
		
		//$data['all'] = $this->search_json();
		
		$data['mpage']	= $mpage;
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['group_riset'] = $mpage->get_unit_kerja('riset','', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
	//	$data['apps'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',10);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['kerjasama']	= $mpage->read_content('-',$data['lang'],'kerjasama');
		
		$data['event'] = $mpage->read_event($data['unit'],$data['lang'],"","",5);		
		//$data['slide']	= $mpage->read_slide($data['unit'],$data['lang'],4);
		$data['slide'] = $mpage->read_slide('-',$data['lang'],7,"'".$this->config->slider."'");	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		
		$data['color'] = "orange";
				
		if($str){
			$data['post'] = $mpage->read_content($data['unit'], $data['lang'],"", $str);	
		}else{
			$data['post'] = false;//$mpage->read_content("", 'about');
		}
		
		$this->view('page/index.php', $data);
	}
		
	function read($str=NULL, $id=NULL, $id_page=NULL, $err=NULL){
		$mpage = new model_page();
		
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		
		//$data['mimg'] = new imageprocessing();
	
		switch($str){
			case 'rekrutmen';
				if($err=='ok'){
					$success="<div class='alert alert-success'>Your submission was successfully submitted!!</div>";
					$data['success'] = $success;
				}else{
					if($err) $data['err'] = $err;
					if($id_page && $id_page=='apply') $data['apply'] =1; 
				}
				
				if($id) $data['detail'] = $mpage->get_master_kegiatan("",$id,"-");
				else $data['post_event'] = $mpage->get_master_kegiatan($str,"","0");
			break;
			
			case 'kerjasama':								
				if($id):
					$data['detail'] = $mpage->read_content($data['unit'],$data['lang'],"", "", $id_page);	
				else:
					$data['post'] = $mpage->read_content($data['unit'], $data['lang'],'kerjasama',"","",100);	
				endif;
			break;
			
			case 'gallery':								
				if($id):
					//$this->read_gallery($id);
					//exit();
					//$data['detail'] = $mpage->read_slide($data['unit'],"",1,"'video','gallery'", $id);	
					
				
					if($id)	$mpage->update_hits_file($id);
					
					$mpage = new model_page();
					
					$data['detail'] = $mpage->read_slide(0,'in',1,"",$id);
					if($data['detail']->content_category=='video'){
						$data['post'] 	= $mpage->read_slide(0,'in',6,"'video'");
					}else{
						$data['post'] 	= $mpage->read_slide(0,'in',6,"'gallery'","",$data['detail']->parent_id);
					}					
				
				else:
					$data['post'] = $mpage->read_slide($data['unit'],'in',50,"'video'");
					$data['gcat'] = $mpage->read_gal($data['unit'],'in',4,"'gallery'","",'0');
				endif;
			break;
			
			case 'video':								
				if($id):					
					$data['detail'] = $mpage->read_slide($data['unit'],"",1,"'video'", $id);	
				else:
					$data['post'] = $mpage->read_slide($data['unit'],$data['lang'],5,"'video'");
				endif;
			break;
			
			case 'news':				
				if($id):
				 if($id_page):
					$mpage->update_hits_content($id_page);
					$this->add_script('js/submit.js');
					$ok = $mpage->read_content($data['unit'],$data['lang'],"", "",$id_page);
					if($ok) $data['detail']= $ok;
					else $data['detail'] = $mpage->read_content("-","","", "",$id_page);
					//edit
					$data['title_page']=true;	
				 else:
					$mpage->update_hits_content($id);
					$this->add_script('js/submit.js');
					$data['detail'] = $mpage->read_content("",$data['lang'],"", $id);
					//edit
					$data['title_page']=true;	
				endif;
				else:
					$data['post'] = $mpage->read_content($data['unit'], $data['lang'],'news',"","",100);	
				endif;
			break;
			
			case 'pengumuman':								
				if($id):
					if($id_page):
						$mpage->update_hits_content($id_page);
						$ok = $mpage->read_content('-',$data['lang'],"", "",$id_page);
						if($ok) $data['detail']= $ok;
						else $data['detail'] = $mpage->read_content('-',"","", "",$id_page);
						
					else:
						$mpage->update_hits_content($id_page);
						$data['detail'] = $mpage->read_content('-',$data['lang'],"", $id);	
					endif;
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'pengumuman',"","",100);	
				endif;
			break;
			
			case 'event':								
				if($id):					
					$data['detail'] = $mpage->read_event($data['unit'],$data['lang'],"", $id_page,1);	
				else:
					$this->event();
					//$data['post'] = $mpage->read_event($data['unit'],$data['lang'],"","",100);	
				endif;
			break;
			
			case 'beasiswa':								
				if($id):
					if($id_page):
						$mpage->update_hits_content($id_page);
						$data['detail'] = $mpage->read_content('-',$data['lang'],"","", $id_page);	
					else:
						$mpage->update_hits_content($id_page);
						$data['detail'] = $mpage->read_content('-',$data['lang'],"", $id);	
					endif;
					
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'beasiswa',"","",100);	
				endif;
			break;
			
			case 'karir':								
				if($id):
					$mpage->update_hits_content($id_page);
					$data['detail'] = $mpage->read_content('-',$data['lang'],"", "",$id_page);	
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'karir',"","",100);	
				endif;
			break;
			
			default:
				/*if($id)	$data['detail'] = $mpage->read_content($data['lang'],"", $id,1);
				else $data['post'] = "";	*/
				if($id):
					$ok = $mpage->read_content($data['unit'],$data['lang'],"", "",$id,1);
					if($ok) $data['detail']= $ok;
					else $data['detail'] = $mpage->read_content($data['unit'],"","", "",$id,1);				
				else:
					$data['post'] = $mpage->read_content($data['unit'],$data['lang'],"", "",$id,1);	
				endif;
			break;
		}
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['group_riset'] = $mpage->get_unit_kerja('riset','', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',5);
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	

		$data['gallery'] = $mpage->read_slide('0',$data['lang'],8,"'video'");		
		if($str):	
			$data['view_slide']= 0;
			$this->add_script('js/datatables/jquery.dataTables.js');	
			$this->add_script('js/datatables/DT_bootstrap.js');
			
			if($str=='video'||$str=='video-streaming'){
				$this->add_style('ptiik/flowplayer/skin/minimalist.css');
				$this->add_style('ptiik/css/livestream.min.css');
			}
			
			if($str=='gallery'){
				$this->add_script('js/page/gallery/gallery.js');
					
				$this->add_style('ptiik/flowplayer/skin/minimalist.css');
				$this->add_style('ptiik/css/tubestyle.min.css');
				$this->add_style('ptiik/css/base.tube.min.css');
			}
			
			if($str=='kontak'||$str=='contact') $this->add_script('js/submit.js');
			
			if(strtolower($str)=='rekrutmen'){								
				$this->view( 'page/view-event.php', $data);
			}
			
			
			if(strtolower($str)=='gallery' || strtolower($str)=='video' || strtolower($str)=='video-streaming'){
				//$this->add_style('ptiik/css/menuapps.slider.min.css');	
				$this->view( 'page/view-gallery.php', $data);
			}else{
				$this->view( 'page/view-content.php', $data);
			}
		else:
			$this->view( 'page/index.php', $data);
		endif;
		
	}
	
		

	
	function add_comment(){
		$user = new model_user();
		$mpage = new model_page();
		if( $authenticatedUser = $user->authcoment( $_POST['comment-email'], $_POST['comment-password'] ) ) {
			// bisa nulis komen
			ob_start();
			
			$content_id 	= $_POST['content-news'];
			$comment 		= $_POST['comment-content'];
			$email 			= $_POST['comment-email'];
			$username		= $authenticatedUser->username;
			$parent_id 		= $_POST['parentid'];
			$tgl_comment	= date("Y-m-d H:i:s");
			$lastupdate		= date("Y-m-d H:i:s");
			$approve		= 0;
			$ip_address		= $_SERVER['REMOTE_ADDR'];
			$user			= $authenticatedUser->id;
			
			$cek_last_comment = $mpage->cek_last_comment($user,$content_id);
			
			if($cek_last_comment != 0 || $cek_last_comment == NULL){
				$datanya 	= Array('content_id' => $content_id,
									'comment' => $comment,
									'user_name' => $username,
									'email' => $email,
									'parent_id' => $parent_id,
									'tgl_comment' => $tgl_comment,
									'last_update' => $lastupdate,
									'approved' => $approve,
									'ip_address' => $ip_address,
									'user_id' => $user
							);
				$mpage->replace_comment($datanya);
				echo "Berhasil menambahkan comment! Comment anda selanjutnya akan diproses oleh admin terlebih dahulu!";
				exit();
			}
			else {
				echo "Comment anda yang sebelumnya belum di proses!";
				exit();
			}
		}
		else {
			//data tidak valid
			echo "Tidak Boleh Comment";
			exit();
		}
	}
	
	function detail_comment($content_id, $comment_id, $mpage){
		$detail = $mpage->read_comment($content_id, $comment_id);
		
		if($detail){
		
			foreach ($detail as $dt):
			?>
			
			<div class="media media-comment">
				<a class="pull-left media-img-container" href="#">
					<?php if($dt->foto){ ?>
						<img class="media-object" src="<?php echo $this->location($dt->foto); ?>" alt="...">
					<?php }else{
					?>
						<img class="media-object" src="<?php echo $this->location("assets/images/logo-ub-blue-header.png"); ?>" alt="...">
					<?php } ?>
				</a>
				<div class="media-body">
					<h4 class="media-heading"><?php echo $dt->name ?></h4>
					<p class="comment-paragraph"><?php echo $dt->comment ?></p>
					<div class="comment-action">
						<!-- <a href="" class="delete"><span class="glyphicon glyphicon-trash"></span></a> -->
						<a href="" class="reply" data-url="" data-parentid="<?php echo $dt->id; ?>"><span class="glyphicon glyphicon-share-alt"></span></a>
					</div>
					
				</div>
			</div>					
				
			<?php
			endforeach;
		}
	}
	
	//====feedback====//
	
	function feedback(){
		$mpage 		= new model_page();
		$mconfinfo	= new model_confinfo();
		
		$data['lang'] 	= $this->config->lang;
		$data['unit'] ='0';
		
		$data['mpage']	= $mpage;
		
		$data['view_slide']= 0;
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news');
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	
		$data['gallery'] = $mpage->read_slide('0',$data['lang'],8,"'video'");		
		
		$this->add_script('js/submit.js');
		
		$this->view( 'page/feedback.php', $data);
	}
	
	function save_feedback(){
		$user 		= new model_user();
		$mpage 		= new model_page();
		if( $authenticatedUser = $user->authcoment( $_POST['feedback-email'], $_POST['feedback-password'] ) ) {
			$content_id		= '-';
			$user_name		= $authenticatedUser->username;
			$email 			= $_POST['feedback-email'];
			$comment 		= $_POST['feedback-content'];
			$comment_post	= date("Y-m-d H:i:s");
			$ip_address		= $_SERVER['REMOTE_ADDR'];
			$approve		= 0;
			$user_id		= $authenticatedUser->id;
			$lastupdate		= date("Y-m-d H:i:s");
			$category		= "feedback";
			
			$cek_last_comment = $mpage->cek_last_comment($user_id,$content_id,$category);
			
			if($cek_last_comment != 0 || $cek_last_comment == NULL){
				$data_feedback	= array(
										'comment_id'=>$mpage->comment_id(),
										'content_id'=>$content_id,
										'user_name'=>$user_name,
										'email'=>$email,
										'comment'=>$comment,
										'comment_post'=>$comment_post,
										'ip_address'=>$ip_address,
										'is_approve'=>$approve,
										'user_id'=>$user_id,
										'last_update'=>$lastupdate,
										'category'=>$category
									   );
				$save	= $mpage->replace_comment($data_feedback);
				if($save) echo "Berhasil";
				else echo "Gagal";
			}
			else {
				echo "Feedback anda yang sebelumnya sedang di proses!";
				exit();
			}
		}
		else{
			echo "Anda tidak memiliki hak akses untuk memberikan feedback!";
		}
	} 
	
	
}
?>