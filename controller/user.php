<?php
class controller_user extends comscontroller {

	function __construct() {
		parent::__construct();
		
		// this controller requires authentication
		// initialize it
		$this->require_auth('auth');
	}
	
	function event($str=NULL, $type=NULL, $style='calendar'){
		$url= $this->location('info/vw');
		
		$magenda = new model_agendainfo();	
		$mconf = new model_info();
		
				
		$id 				= $this->authenticatedUser->staffid;
		$data['civitasid']	= $id;
		
		$data['posts'] = "";
		$data['type']  = $type;		

		$this->add_style('css/calendar/calendar.css');
		
		if(isset($_POST['month'])){
			$month = $_POST['month'];
		}else{
			$month = date('m');
		}
		
		if(isset($_POST['year'])){
			$year = $_POST['year'];
		}else{
			$year = date('Y');
		}
		
		if(isset($_POST['tgl'])){
			$tgl	 = $_POST['tgl'];
		}else{
			$tgl	 = date('Y-m-d');
		}	
		
		if(isset($_POST['hidmonth'])){
			$data['hidmonth']	= $_POST['hidmonth'];
		}else{
			$data['hidmonth']	= "";
		}
		
		if(isset($_POST['hidtgl'])){
			if(isset($_POST['b_now'])){
				$hidtgl	 = date('Y-m-d');
			}else{
				if(isset($_POST['b_prev'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('-7 days', $in));	
				}else if(isset($_POST['b_next'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('+7 days', $in));	
				}else{
					$hidtgl	 = $_POST['hidtgl'];
				}
			}
		}else{
			$hidtgl	 = date('Y-m-d');
		}	
		
		
		$allowed = "/[^a-z0-9]/i";
		$month = trim(preg_replace('/ +/', ' ', preg_replace($allowed, '', urldecode(html_entity_decode(strip_tags($month))))));
		$year  = trim(preg_replace('/ +/', ' ', preg_replace('/[^0-9+]/', '', urldecode(html_entity_decode(strip_tags($year))))));
		
		if(empty($year) && empty($month)) {
			$this->notfound('error tidak diketemukan');
			exit;
		}
				
		$data['month'] 		= $month;
		$data['year']  		= $year;
		$data['title'] 		= date('F Y',mktime(0,0,0,$month,1,$year));
		$data['tgl']		= $tgl;
		$data['hidtgl']		= $hidtgl;
		$data['hadir'] 		= $mconf->get_civitas($data['civitasid']);
		$data['absen']   	= $mconf->get_finger_print($tgl);	
		$data['kegiatan']	= $mconf->get_agenda_civitas($tgl,"",$data['civitasid']);
		$data['posts']    	= $magenda->draw_month_hadir($month, $year, $data['civitasid'], $style, $url);
		$data['weekdata']  	= $magenda->draw_week_hadir($month, $year, $hidtgl, $data['civitasid'], $style, $url);
		
		$this->view( 'users/hadir/index.php', $data );
		
		
	}
	
    
    function tasks(){
        if(isset($_POST['b_task'])){
			$this->save_task();
			exit();
		}
		
        $id = $this->authenticatedUser->staffid;
				
		$muser = new model_user();
		
		$data['users'] = $muser->getTaskList($id);
		
		
		$this->add_style('css/bootstrap/DT_bootstrap.css');
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');	
		
        $this->add_style('css/datepicker/datetimepicker.css');
	    $this->add_script('js/datepicker/bootstrap-datetimepicker.js');		
        $this->add_script('js/user/index.js');
		$this->add_script('js/user/create.js');
		
		$this->add_script('js/jstask.js');	
		
		$this->view("users/tasks/index.php", $data);
    }
    
	
	function write() {
		$mtags = new model_travel_tags();
		$mcats = new model_travel_categories();
				
		$data['user'] 	= $this->session->get("user");
		$data['tags'] 		= $mtags->getAllTags();
		$data['categories'] = $mcats->getAllCategories();

		$this->view('travel/write.php', $data);
	}
	
	function create_task() {
		
		$muser = new model_user();
        
        $id = $this->authenticatedUser->staffid;
		
		$mulai = $_POST['tgl'];
		$selesai = $_POST['tglselesai'];
		$task = $_POST['taskname'];
		$catatan = $_POST['catatan'];
		
		$status = 0; // suspended
		if( isset( $_POST['status'] ) ) 
			$status = $_POST['status'];
		
		if( $muser->save_task($mulai, $selesai, $task, $catatan, $status,"",$id ) ) 
			$result['status'] = 'OK';
		else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		echo json_encode($result);
	}
    
    function create() {
		
		$muser = new model_user();
		
		$username = $_POST['username'];
		$password = $_POST['password'];
		$name = $_POST['name'];
		$email = $_POST['email'];
		$clevel = $_POST['level'];
		$status = 0; // suspended
		if( isset( $_POST['status'] ) ) 
			$status = $_POST['status'];
		
		if( $muser->save($username, $password, $name, $email, $clevel, $status ) ) 
			$result['status'] = 'OK';
		else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		echo json_encode($result);
	}
	
	function getAllTags(){
		$mtags = new model_travel_tags();
		$tags = $mtags->getAllTags();
		$atags = array();
		foreach($tags as $tag)
			array_push( $atags, $tag->name );
		echo json_encode($atags);
	}
	
	function getAllCategories(){
		$mcats = new model_travel_categories();
		$cats = $mcats->getAllCategories();
		$acats = array();
		foreach($cats as $cat)
			array_push( $acats, $cat->name );
		echo json_encode($acats);
	}
	
	function save_task(){
		$staffid = $this->authenticatedUser->staffid;
        
        if(isset($_POST['id'])){
			$id = $_POST['id'];
		}else{
			$id = "";
		}
		
		$mulai = $_POST['tgl'];
		$selesai = $_POST['tglselesai'];
		$task 	 = $_POST['taskname'];
		$catatan = $_POST['catatan'];
		
		$status = 0; // suspended
		if( isset( $_POST['status'] ) ) 
			$status = $_POST['status'];
				
		
		$muser = new model_user();
		
        $save = $muser->save_task($mulai, $selesai, $task, $catatan, $status,$id,$staffid );
									
		if( $save ) {
			$result['status'] = "OK";
			$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
		} else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		
		echo json_encode($result);
	}
    
    function save(){
		$mconf = new model_confinfo();		
		
		if(isset($_POST['id'])){
			$id = $_POST['id'];
		}else{
			$id = "";
		}
		
		if(isset($_POST['hid'])){
			$hid = $_POST['hid'];
		}else{
			$hid = "";
		}
		
		$username = $_POST['username'];
		
		if(isset($_POST['cmbstaff'])){
			$act = 'insert';
			$name = $_POST['cmbstaff'];
			$staffid = $mconf->get_id_dosen($name);
		}else{
			$act = 'update';
			$name = $_POST['name'];
			$staffid = $hid;
		}
		
		if(isset($_POST['email'])){
			$email = $_POST['email'];
		}else{
			$email = NULL;
		}
		
		$clevel = $_POST['level'];
		$status = $_POST['status'];
				
		
		if(isset($_POST['password']))
			$password = $_POST['password'];
		else $password = NULL;
						
		
		$muser = new model_user();
		
			
		if($hid){
			$datanya = array('nickname'=>$_POST['nname'], 'hp'=>$_POST['hp'], 'telp'=>$_POST['telp'], 'interest'=>$_POST['interest'], 'biografi'=>$_POST['biografi'], 'interest_en'=>$_POST['interest_en'], 'biografi_en'=>$_POST['biografi_en'], 'about'=>$_POST['about'],  'about_en'=>$_POST['about_en'],  'website'=>$_POST['website']);
			$idnya   = array('karyawan_id'=>$staffid);
			
			$muser->save_staff($datanya, $idnya);
		
		}
		
		$save = $muser->save($username, $password, $name, $email, $clevel, $status, $id, $staffid);
		
		
							
		if( $save ) {
			$result['status'] = "OK";
			$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
		} else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		
		echo json_encode($result);
	}
	
	public function updatepassword() {
		$id = $_POST['id'];
		$password = $_POST['password'];
		$muser = new model_user();
		$save = $muser->setPassword($id, $password);
		$result['status'] = "OK";
		if( $save ) {
			$result['status'] = "OK";
			$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
		} else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}		
		echo json_encode($result);
	}
	
	function index($page = 1){
		if(isset($_POST['b_user'])){
			$this->save();
		}
		
		if($this->authenticatedUser->level > 1)
			$this->no_privileges();
		
		$muser = new model_user();
		$mconf = new model_confinfo();
		
		$data['users'] = $muser->getList($page, 50);
		$data['level'] = $muser->getLevel();
		$data['page']  = $page;
		$data['staff'] = $mconf->get_staff();
		
		
		$this->add_style('css/bootstrap/DT_bootstrap.css');
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_style('select/select2.css');
		$this->add_script('select/select2.js');
		
		$this->add_script('js/user/index.js');
		$this->add_script('js/user/create.js');
		
		$this->add_script('js/jsall.js');	
		
		$this->view("users/index.php", $data);
	}
	
	function delete() {
		$id = $_POST['id'];
		$muser = new model_user();
		$result = array();
		if($muser->delete($id)) 
			$result['status'] = "OK";
		else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		echo json_encode($result);
	}
	
	function delete_task() {
		$id = $_POST['id'];
		$muser = new model_user();
		$result = array();
		if($muser->delete_task($id)) 
			$result['status'] = "OK";
		else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		echo json_encode($result);
	}
	
	function profile() {
		$this->edit($this->authenticatedUser->id);	
	}
	
	function edit($id = NULL) {
		
		if( !$id ) {
			$this->redirect('user');
			exit;
		}
		
		$muser = new model_user();
		if($this->authenticatedUser->role=='mahasiswa'):
		$data['user'] = $muser->get_mhs_profile($id);
		else:
		$data['user'] = $muser->get_user_profile($id);
		endif;
		
		
		$data['dlevel'] = $muser->getLevel();
		

		$this->add_script('js/user/edit.js');
		$this->add_script('ckeditor/ckeditor.js');
	

		$this->view('users/edit.php', $data);
	}
	
	
	function togglestatus() {
		$id = $_POST['id'];
		$muser = new model_user();
		$result = array();
		if($muser->toggleStatus($id)) {
			$result['status'] = "OK";
			if( $muser->status == 1 )
				$result['ustatus'] = "Active";
			else $result['ustatus'] = "Suspended";
		} else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		echo json_encode($result);
	}
	
	function toggletask() {
		$id = $_POST['id'];
		$muser = new model_user();
		$result = array();
		if($muser->toggleTask($id)) {
			$result['status'] = "OK";
			if( $muser->status == 1 )
				$result['ustatus'] = "1";
			else $result['ustatus'] = "0";
		} else {
			$result['status'] = "NOK";
			$result['error'] = $muser->error;
		}
		echo json_encode($result);
	}
}
?>