<?php
class controller_pemakaianruang extends comscontroller {

	function __construct() {
		parent::__construct();
		// /$this->require_auth('page');
	}
	
	function pemakaian($tgl=NULL, $str=NULL, $id=NULL, $id_page=NULL){
		$mpage 		= new model_page();
		$mruang 	= new model_pemakaianruang();
		
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		$data['view_slide']= 0;
		
		$data['mimg'] = new imageprocessing();
		
		if($tgl == NULL) $tgl = strtotime(date('Y-m-d'));
		$data['calendar'] = $this->calendar($tgl);
		
		$data['pemakaian'] = $mruang->get_pemakaian_ruang($tgl);
		$data['ruang'] = $mruang->get_ruang();
		
		$this->add_script('highchart/highcharts.js');
		$this->add_script('highchart/modules/exporting.js');
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news',"","","","",1);
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	
		
		$this->view('info/reservasi/pemakaian.php', $data );
	}
	
	function calendar($tgl=NULL, $style='calendar'){
		$strtgl = $tgl;
		$nid = strtotime('+1 month',$strtgl);
		$pid = strtotime('-1 month',$strtgl);
		$month= date("m", $strtgl);
		$nmonth= date("M", $strtgl);
		$year= date("Y", $strtgl);
		$title= date("F Y", $strtgl);
		$today=strtotime(date("Y-m-d"));
		
         if((substr($month, 0, 1)) == 0)
         {
            $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         $str = '<table class="table calendar-date-mini">';            
         $headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
         $str.= '<thead><tr><td colspan=2 align="right"><a href='.$this->location('pemakaianruang/pemakaian/'.$pid).'><</a></td><td colspan="3" align="center"><b>'.$title.'</b></td>
				<td colspan=2 align="left"><a href='.$this->location('pemakaianruang/pemakaian/'.$nid).'>></a></td></tr>
				<tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
				 $stra = strtotime($year."-".$month."-".$list_day);
                 $str.= '<div class="'. $style .'-day-number"><a href='.$this->location('pemakaianruang/pemakaian/'.$stra).'><span class="text text-default">';
				if($stra==$strtgl){
					$str.= '<span class="label label-success">'.$list_day.'</span>';
				}else{
					$str.= $list_day;
				 }
				 $str.= '</span></a></div></td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
             
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
         $str.= '</tr>';
		 $str.= '<tr><td colspan=7 align=center  class="'. $style .'-day-head"><a href='.$this->location('pemakaianruang/pemakaian/'.$today).'><span class="text text-default">Today</span></a></td></tr>';
		 $str.= '</tbody>';    
         $str.= '</table>';
		 
		 return $str;
	}
}
?>