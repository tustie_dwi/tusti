<?php
class controller_hakakses extends comscontroller {

	function __construct() {
		parent::__construct();
		
		
	}
	
	public function index( $status = NULL){
	
		$this->require_auth('auth');
		if(isset($_POST['b_hakakses'])){
			$this->saveToDB();			
		}
		
		if($this->authenticatedUser->level > 1)
			$this->no_privileges();
	
		$mhakakses = new model_hakakses();	
		
		$data['role'] 	 = $mhakakses->read_role();
		$data['menu'] 	 = $mhakakses->read_menu(1);
		$data['posts'] 	 = $mhakakses->read_hakakses();
		$data['pmenu']   = $mhakakses->read();
		
		$this->add_style('css/bootstrap/DT_bootstrap.css');		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
	
		$this->add_style('select/select2.css');
		$this->add_script('select/select2.js');
	
		$this->add_script('js/jsall.js');		
				
		
		$this->view("hakakses/index.php", $data);
	}
	
	
	
	private function saveToDB(){
		//ob_start();
		
		$mhakakses = new model_hakakses();
		
		if(isset($_POST['cmbrole'])){
			$nrole	= $_POST['cmbrole'];
			$nmenu	= $_POST['cmbmenu'];
			//echo $nmenu;
			if($nmenu){
				for($i=0;$i<count($nmenu);$i++){
					
					if($nmenu[$i]==0){
						
						$mhakakses->insert_hakakses_all($nrole);
					}else{
						$datanya = array('level'=>$nrole, 'menu_id'=>$nmenu[$i]);					
						$mhakakses->insert_hakakses($datanya);	
					}
				}
			}
								
			/*$save = $mhakakses->save_hakakses($nrole, $nmenu);
							
			if( $save ) {
				$result['status'] = "OK";
				$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
			} else {
				$result['status'] = "NOK";
				$result['error'] = $mhakakses->error;
			}
			
			echo json_encode($result);
			*/
			
		}
		
		
	}	
}