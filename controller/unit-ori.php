<?php
class controller_unit extends comscontroller {

	function __construct() {
		parent::__construct();
		
	}
	
	function notfound($str=NULL){
		if($str){
			echo $str;
		}				
		exit();
	}
	
	function read_detail($unit=NULL, $url=NULL, $cat=NULL, $unit_list=NULL, $str=NULL, $id=NULL){
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		if($cat=='lab') $strcat = 'laboratorium';
		else $strcat = $cat;
			
		$data['unit_list'] = $unit_list;
		$data['unit'] = $unit;
		
		$data['mimg'] = new imageprocessing();
			
		
		$data['url'] = $url;
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		
		$data['footer'] = $mpage->read_content(0,$data['lang'],'footer');
		/*--- end page -- */	
	
				
		switch($str){
			case "staff":
				
				
				$mconf = new model_info();
				
				$data['posts'] = $mconf->get_civitas($id);
				$data['minfo'] = $mconf;
				$data['kategori'] = "staff";
							
				$this->add_script('js/datatables/jquery.dataTables.js');	
				$this->add_script('js/datatables/DT_bootstrap.js');
				
				$this->view('info/general/staff.php', $data );
			break;
		}
	}
	
	function read_data($str=NULL, $url=NULL){
		
			$data_array = explode("/", $str);
			
			$cat = $data_array[0];
			$val = end($data_array);
			
			$data['lang'] 		= $this->config->lang;//'EN';
			$data['kategori'] = $cat;
			
			$data['url'] = $url;
			
			
			if($cat=="mutu"):
				$mpage = new model_page();
				
								
								
				$data['mpage'] 			= $mpage;
				$data['kategori_data']	= $val;
				
				$data['unit_mutu']	= $mpage->read_mutu_unit($val, $data['lang']);
				if($val=='aim')	$data['siklus']	= $mpage->read_mutu_siklus($val, $data['lang']);
				
				$this->add_script('js/datatables/jquery.dataTables.js');	
				$this->add_script('js/datatables/DT_bootstrap.js');
				
				$this->view( 'info/mutu/index.php', $data );
			endif;
			
			if($cat=="prestasi"):
				$minfo = new model_info();
		
				$data['posts'] 		= $minfo->read_prestasi("",'non');
				$data['aposts'] 	= $minfo->read_prestasi("",'akademik');
								
				$this->view( 'info/prestasi/index.php', $data );
			endif;
			
			if($cat=="alumni"):
				$minfo = new model_info();
		
				$data['posts'] = $minfo->get_wisudawan($data['lang'],$val);
				$this->view( 'info/wisuda/index_wisuda.php', $data );
			endif;
			
			if(($cat=="beasiswa") || ($cat=="karir")):
				
				$mpage = new model_page();
				$data['mpage'] 			= $mpage;
		
				$data['posts'] = $mpage->read_content('-', $data['lang'],$cat,"","",100);	
			
				$this->view('info/general/index.php', $data );
			endif;
			
			if($cat=="staff"):
				$minfo = new model_info();
						
				$data['posts'] = $minfo->get_civitas("",$val);
				$data['abjad'] = $minfo->get_civitas_abjad("",$val);
				$this->view('info/general/index.php', $data );
			endif;
			
			if(strtolower($cat)=="s2"):
				$minfo = new model_info();
				
				$data['kategori_data']	= $val;
				if($val=='dosen')	$data['posts'] = $minfo->get_civitas("",$val,"","S3");
				else 	$data['posts'] = $minfo->get_civitas("",$val);
				
				$data['abjad'] = $minfo->get_civitas_abjad("",$val,"S3");
				$this->view('info/general/index.php', $data );
			endif;
			
			if($cat=="fasilitas"):
				$mpage = new model_page();
				
				
				$data['kategori_data']	= $val;
				
				if($val=='lab')	$data['posts'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
				else $data['posts'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
				
				$this->view('info/general/index.php', $data );
			endif;
	}
	
	
	function search($str=NULL, $url=NULL, $cat=NULL){
		$mpage = new model_page();
		if(! isset($_POST['searchTxt'])) $this->redirect('page');
		
		$data['search'] = $_POST['searchTxt'];
		
		$data['lang'] = $this->config->lang;//'EN';
		
		if($cat=='lab') $strcat = 'laboratorium';
		else $strcat = $cat;
			
		$data['unit_list'] = $mpage->get_unit_kerja($strcat,'PTIIK', $data['lang'],"",$str);
		$data['unit'] = $data['unit_list']->unit_id;
		
		$data['mimg'] = new imageprocessing();
			
		
		$data['url'] = 'unit/'.$cat.'/'.$data['unit_list']->kode;
		
		$data['mpage']	= $mpage;
		$data['lab'] 	= $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content('-',$data['lang'],'news','','',10);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		
		$data['view_slide']= 0;
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view('page/view-search.php', $data);
	}
	
	function searchForId($id) {
		
		$array = $this->search_json();
	   foreach ($array as $key => $val) {
		   if ($val['uid'] === $id) {
			   return $key;
		   }
	   }
	   return null;
	}
	
	function search_json($nilai=NULL, $unit=NULL, $url_unit=NULL, $url_cat=NULL, $url_val=NULL){
		
		$mpage = new model_page();
		
		$fak = $this->config->fakultas;
		$cabang = $this->config->cabang;
		$data = $mpage->get_search_content($nilai, $this->config->lang, $fak, $cabang, $unit);
		
		$json = array();
		
		if($data){
			foreach($data as $key){
				$kategori= $key->content_category;
				
				$judul_link=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($key->content_title))))));
				
				if(isset($url_unit)):
					$url = $url_unit."/".$url_cat."/".$url_val;
					if($kategori=='news' || $kategori=='pengumuman') $url_web= $this->location($url.'/read/'.$key->content_category.'/'.$judul_link.'/'.$key->id);
					else $url_web= $this->location($url.'/read/'.$judul_link.'/'.$key->id);
				else:
					if($kategori=='news' || $kategori=='pengumuman') $url_web= $this->location('page/read/'.$key->content_category.'/'.$judul_link.'/'.$key->id);
					else $url_web= $this->location('page/read/'.$judul_link.'/'.$key->id);
				endif;
				
				if($key->content_thumb_img) $imgthumb = $this->config->file_url_view."/".$key->content_thumb_img; 
				else $imgthumb = $this->config->default_thumb_web;
				$temp = array(
					'id' => $key->id,
					'judul' => $key->content_title,
					'isi' => strip_tags($key->content),
					'tgl' => $key->content_upload,
					'img' => $imgthumb,
					'kategori' => $key->content_category,
					'url_web'=> strtolower($url_web)
				);
				
				array_push($json, $temp);
			}
		}
		
		
		echo json_encode($json);
	}
	
	function jadwal($str=NULL, $url=NULL, $cat=NULL, $unit_list=NULL){
		$data['url'] = $url;
		
		$mpage 		= new model_page();
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		if($cat=='lab'):
			$strcat = "laboratorium";
			$data['color'] = "green";
		else:
			$strcat = $cat;
			$data['color'] = "blue";
		endif;
		
		$data['unit_list'] = $unit_list;
		//var_dump($cat);
		//$data['unit'] = $data['unit_list']->unit_id;
		$data['unit'] = $str;
		$data['kategori']	= $strcat;
		
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data['jam'] = $mjadwal->get_jam($is_pendek);
		$data['ruang'] = $mjadwal->get_ruang($this->config->cabang, $this->config->fakultas);
		$data['praktikum'] = $mjadwal->get_praktikum($this->config->cabang, $this->config->fakultas,$data['unit']);
		/*$*data['dosen'] = $mjadwal->get_dosen($this->config->cabang, $this->config->fakultas);
		$data['mk'] = $mjadwal->get_matakuliah($this->config->cabang, $this->config->fakultas);
		$data['praktikum'] = $mjadwal->get_praktikum($this->config->cabang, $this->config->fakultas);*/
		
		$this->add_script('js/jadwalkuliah/jadwal_prak.js');
		$data['mpage'] 	= $mpage;
		
		
		$data['view_slide']= 0;
		
		$data['mimg'] = new imageprocessing();
		
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news');
		
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');		
								
		$this->view('info/jadwal_kuliah/index_prak.php', $data );
	
	}
	
	function jadwal_prak_json($unit=NULL){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		
		$lang = $data['lang'];
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal_praktikum($this->config->cabang, $this->config->fakultas, $is_pendek, $data['lang'], $unit);
		$jadwal;
		
		foreach($data as $key){
			
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
			
			if($key->mk) $strmk = $key->mk;
			else $strmk = $key->mk_ori;
			
			$tmp = array(
				'str_kelas' => $key->strkelas,
				'matakuliah' => $strmk,
				'mk_id' => $key->mk_id,
				'kelas' => $key->kelas,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'karyawan_id' => $key->karyawan_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'urut_selesai' => $key->urut_selesai,
				'praktikum' => $key->is_praktikum
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		echo json_encode($jadwal);
	}
	
	//======START NILAI======//
	
	function nilai($str=NULL, $url=NULL, $cat=NULL, $unit_list=NULL){
		$data['url'] = $url;
		//echo $unit_list->kode;
		$mpage 		= new model_page();
		$mconf	 	= new model_confinfo();
		
		$data['lang'] = $this->config->lang;
		
		if($cat=='lab'):
			$strcat = "laboratorium";
			$data['color'] = "green";
		else:
			$strcat = $cat;
			$data['color'] = "blue";
		endif;
		
		$data['unit_list'] = $unit_list;
		
		$data['unit'] = $str;
		$data['kategori']	= $strcat;
		
		
		$data['thn_akademik'] 	= $mconf->get_semester();
		$this->add_style('css/bootstrap/DT_bootstrap.css');
		$this->add_script('js/datatables/jquery.dataTables.new.js');
		$this->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('select/select2.js');
		$this->add_style('select/select2.css');
		$this->add_script('js/nilai/nilai.js');
		
		$data['mpage'] 	= $mpage;
		
		
		$data['view_slide']= 0;
		
		$data['mimg'] = new imageprocessing();
		
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news');
		
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');		
								
		$this->view('info/nilai/index.php', $data );
	}

	function get_mk_nilai(){
		$mnilai	 	= new model_nilaiinfo();
		
		$thn	= $_POST['thn'];
		$unit	= $_POST['unit'];
		$data	= $mnilai->read_mk_praktikum($unit, $thn, $this->config->cabang, $this->config->fakultas, $this->config->lang);
		//var_dump($data);
		if($this->config->lang == 'in'){
			$title = "Mata Kuliah";
		}else $title = "Course";
		echo "<option value='0'>".$title."</option>";
		if($data){
			foreach($data as $dt){
				echo "<option value='".$dt->mkid."'>".$dt->mk."</option>";
			}
		}
	}

	function get_penilaian(){
		$mnilai	 	= new model_nilaiinfo();
		$thn		= $_POST['thn'];
		$unit		= $_POST['unit'];
		$mk			= $_POST['mk'];
		$kategori	= $_POST['kategori'];
		
		if(isset($_POST['kelas'])){
			if($kategori=="proses"){
				$jadwaL_kelas	= $_POST['kelas'];
				$data			= $mnilai->get_penilaian($jadwaL_kelas, 'proses', $mk, '', $kategori);
				// var_dump($data);
				$return_arr = array();
				if($data){
					foreach($data as $row){
						foreach ($row as $key => $value) {
							$arr[$key] = $value;				 
						}
						array_push($return_arr,$arr);
					}
					$json_response = json_encode($return_arr);
					if(isset($_GET["callback"])) {
						$json_response = $_GET["callback"] . "(" . $json_response . ")";
					}
					echo $json_response;
				}else{
					echo json_encode('failed');
				}
			}
		}
		else{ // kelas
			$data	= $mnilai->get_penilaian('', 'get_penilaian_by_kelas', $mk, '', $kategori);
			echo "<option value='0'>Class</option>";
			if($data){
				foreach($data as $dt){
					echo "<option value='".$dt->jdwl_id."'>".$dt->kelas_id."</option>";
				}
			}
		}
	}
	
	function get_nilai(){
		$mnilai	 		= new model_nilaiinfo();
		
		if(isset($_POST['nilai'])){ //proses
			$nilai_id 		= $_POST['nilai'];
			$data['mhs'] 	= $mnilai->get_data_nilai_mhs($nilai_id,'','bymhs');
			$data_penilaian	= $mnilai->get_penilaian('','list','',$nilai_id);
			if(isset($data_penilaian->materi_id)){
				$materiid 	= $data_penilaian->materi_id;
			}else $materiid	= '';
			
			if(isset($data_penilaian->jadwal_id)){
				$jadwalid 	= $data_penilaian->jadwal_id;
			}else $jadwalid	= '';
			
			if(isset($data_penilaian->mkditawarkan_id)){
				$mkid 		= $data_penilaian->mkditawarkan_id;
			}else $mkid		= '';
			$data['nilai_id']   = $nilai_id;
			$data['komponen']   = $mnilai->read_komponen('','','','','list',$jadwalid,$materiid,'','','',$nilai_id,'md5');
			
			$this->view('info/nilai/view_nilai.php', $data );
		}
		else{ //akhir
			// echo "TES";
			$jadwalid			= $_POST['kelas'];
			$nilai 					= $mnilai->get_nilaiid_byjadwal($jadwalid);
			$data['komp_akhir']		= $mnilai->read_komponen($jadwalid,'','','','komponen','','','','akhir');
			$data['nilai_id_inf'] 	= $mnilai->read_nilai_inf($jadwalid,'akhir');
			foreach ($nilai as $n) {
				$nilproses = $mnilai->get_nilai_proses($n->nilai_id,'','NA');
				if(isset($nilproses)){
					$komp[] = $nilproses;
				}
				$nilai_id[]= $n->nilaiid;
			}
			$data['nilai'] = $nilai_id;
			$divider = count($komp);
			$datanya = array();
			foreach ($komp as $kom) {
				foreach ($kom as $k) {
					if(!isset($datanya[$k->mahasiswa_id])) $datanya[$k->mahasiswa_id] = 0;
					$datanya[$k->mahasiswa_id] += ($k->nilai_akhir/$divider);
			    }
			}
			$check = $mnilai->check_NA($jadwalid);
			if(isset($check)&&$check!=''){
				$data['komponenby'] = $datanya;	
				$data['by'] 	  = 'after-save';
				$data['mhs'] 	  = $mnilai->get_data_nilai_mhs($check,'','bymhs','nomd5');
			}else{
				$data['komponen'] = $datanya;
				$data['mhs'] 	  = $mnilai->get_data_nilai_mhs($nilai[0]->nilaiid,'','bymhs');
			}
			$data['NAproses']		= $mnilai->check_NA_proses($jadwalid);
			
			$this->view('info/nilai/view_nilai_akhir.php', $data );
		}
	}
	
	function get_nilai_mhs_komponen($nilaiid,$mhsid){
		$mnilai			= new model_nilaiinfo();
		$data['mhs'] 	= $mnilai->get_data_nilai_mhs($nilaiid,$mhsid,'');
		$data['nilaiid']= $nilaiid;
		$data['mhsid']	= $mhsid;
		$this->view('info/nilai/list.php',$data);
	}
	
	function get_nilai_komponen_akhir($komponenid,$nilaiid,$mhsid,$param=NULL,$isprosesNA=NULL,$NA=NULL){
		$mnilai			= new model_nilaiinfo();
		$nilai 			= $mnilai->get_nilai_komp($komponenid,$nilaiid,$mhsid)->nilai;
		
		if($param=='view'){
			echo "<td>".$nilai."</td>";
		}else{
			echo '<input type="text" name="nilaiakhir[]" value="'.$nilai.'" class="form-control" ';
			echo "disabled";
			echo ' /> ';
		}
		
		return $NA+$nilai;
	}
	
	//======END NILAI======//
	
	function asisten($str=NULL, $url=NULL, $cat=NULL, $unit_list=NULL){
		$data['url'] = $url;
		//echo $unit_list->kode;
		$mpage 		= new model_page();
		$mconf	 	= new model_confinfo();
		
		$data['lang'] = $this->config->lang;
		
		if($cat=='lab'):
			$strcat = "laboratorium";
			$data['color'] = "green";
		else:
			$strcat = $cat;
			$data['color'] = "blue";
		endif;
		
		$data['unit_list'] = $unit_list;
		
		$data['unit'] = $str;
		$data['kategori']	= $strcat;
		
		
		$data['thn_akademik'] = $mconf->get_semester();
		$this->add_style('css/bootstrap/DT_bootstrap.css');
		$this->add_script('js/datatables/jquery.dataTables.new.js');
		$this->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('select/select2.js');
		$this->add_style('select/select2.css');
		$this->add_script('js/asisten/asisten.js');
		
		$data['mpage'] 	= $mpage;
		
		
		$data['view_slide']= 0;
		
		$data['mimg'] = new imageprocessing();
		
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news');
		
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');		
								
		$this->view('info/asisten/index.php', $data );
	}

	function asisten_prak_json($unit=NULL,$thn=NULL){
		$masisten = new model_asisteninfo();
		
		$data['lang'] = $this->config->lang;
		
		
		$lang = $data['lang'];
		
		$data  = $masisten->read_asisten($unit, $thn, $this->config->cabang, $this->config->fakultas, $data['lang']);
		$return_arr = array();
		
		if($data){
			foreach($data as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			$json_response = json_encode($return_arr);
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
			echo $json_response;
		}else{
			echo json_encode('failed');
		}
	}
	
	function prodi($str=NULL,$act=NULL, $strlink=NULL, $idpage = NULL, $id=NULL){
		
		$mpage = new model_page();
		$data['lang'] = $this->config->lang;//'EN';
		$data['color'] = "blue";
		
		
		$data['unit_list'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang'],$str);
		$data['unit'] = $data['unit_list']->unit_id;
		
		$data['url'] = 'unit/prodi/'.$str;
				
		if($act){		
			switch($act){
				case "search":
					$this->search($data['unit'],$data['url'], 'prodi');
				exit();	
				break;
				case "jadwal":
					$this->jadwal($data['unit'],$data['url'], 'prodi');
					exit();
				break;
				case "nilai":
					$this->nilai($data['unit'],$data['url'], 'prodi');
					exit();
				break;
				case "asisten":
					$this->asisten($data['unit'],$data['url'], 'prodi');
					exit();
				break;
				case "info":
					$this->read_detail($data['unit'],$data['url'], 'prodi', $data['unit_list'], $strlink, $idpage);
					exit();
				break;
				default:
					$this->read($str,$act,$strlink,$idpage,$id, 'prodi' );
				exit();	
				break;
				
			}
		}
		
		$data['mimg'] = new imageprocessing();
		
				
	
		$data['mpage']	= $mpage;
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
				
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content('0',$data['lang'],'news','','',10);
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['event'] = $mpage->read_event($data['unit'],$data['lang'],"","",5);		
		$data['slide']	= $mpage->read_slide($data['unit'],$data['lang'],4);
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');
				
		/*if($str){
			$data['post'] = $mpage->read_content($data['unit'], $data['lang'],"", $str);	
		}else{
			$data['post'] = false;//$mpage->read_content("", 'about');
		}*/
		
		$data['post'] = false;
		
		$this->view('page/index.php', $data);
	}
	
	function lab($str=NULL,$act=NULL, $strlink=NULL, $idpage = NULL,$id=NULL){
		$mpage = new model_page();
		$data['lang'] = $this->config->lang;//'EN';
		$data['color'] = "green";
		
		
		$data['unit_list'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang'],$str);
		$data['unit'] = $data['unit_list']->unit_id;
		
		$data['url'] = 'unit/lab/'.$str;
		
		$data['mimg'] = new imageprocessing();
				
		if($act){
			switch($act){
				case "search":
					$this->search($data['unit'],$data['url'], 'lab');
				exit();	
				break;
				case "jadwal":
					//echo $data['url'];
					$this->jadwal($data['unit'],$data['url'], 'lab',$data['unit_list'] );
					exit();
				break;
				case "nilai":
					$this->nilai($data['unit'],$data['url'], 'lab',$data['unit_list'] );
					exit();
				break;
				case "asisten":
					$this->asisten($data['unit'],$data['url'], 'lab',$data['unit_list'] );
					exit();
				break;
				default:
					$this->read($str,$act,$strlink,$idpage,$id, 'lab' );
				exit();	
				break;
				
			}
			
		}
				
		$data['mpage']	= $mpage;
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
				
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content('0',$data['lang'],'news','','',10);
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		
		$data['pengumuman']	= $mpage->read_content($data['unit'],$data['lang'],'pengumuman','','',10);
		$data['pengumuman_all']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',4);
		$data['event'] = $mpage->read_event($data['unit'],$data['lang'],"","",5);		
		$data['slide']	= $mpage->read_slide($data['unit'],$data['lang'],4);
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');
		
		$data['post'] = false;
				
		/*if($str){
			$data['post'] = $mpage->read_content($data['unit'], $data['lang'],"", $str);	
		}else{
			$data['post'] = false;//$mpage->read_content("", 'about');
		}*/
		
		$this->view('page/index.php', $data);
	}
	
	
	function read($str=NULL,$act=NULL, $strlink=NULL, $idpage = NULL, $id=NULL, $var=NULL){
		$mpage = new model_page();
		
		$data['mpage'] 		= $mpage;
		$data['kategori'] 	= $strlink;
		$data['id'] 		= $idpage;
		
		$data['lang'] 		= $this->config->lang;
		
		$data['url'] 		= 'unit/'.$var.'/'.$str;
		
		if($var=='lab') $strvar = 'laboratorium';
		else $strvar = 'prodi';
		$data['mimg'] = new imageprocessing();
		
		$data['unit_list'] 	= $mpage->get_unit_kerja($strvar,'PTIIK', $data['lang'],$str);
		$data['unit'] 		= $data['unit_list']->unit_id;
		
		//echo $data['unit'];
	
		switch($strlink){
			case 'news':								
				if($id):					
					$mpage->update_hits_content($id);
					$this->add_script('js/submit.js');
					$data['detail'] = $mpage->read_content('-',$data['lang'],"", "",$id);	
				else:
					//echo "masuk  sini ";
					$data['post'] = $mpage->read_content('-', $data['lang'],'news',"","",100);	
				endif;
			break;
			
			case 'course':								
				/*if($id):					
					$data['detail'] = $mpage->read_course('S1','-',$data['lang'], "",$id);	
				else:
					//echo "masuk  sini ";
					$data['post'] = $mpage->read_course('S1','-', $data['lang'],'main',"",100);	
				endif;*/
				if($id):
					$data['detail'] = $mpage->get_unit_kerja('minat', 'PTIIK', $data['lang'], '',$id,$data['unit']);
				else:
					$data['post'] = $mpage->get_unit_kerja('minat', 'PTIIK', $data['lang'], '','',$data['unit']);
				endif;
			break;
			
			case 'matakuliah':								
				/*if($id):					
					$data['detail'] = $mpage->read_course('S1','-',$data['lang'], "",$id);	
				else:
					//echo "masuk  sini ";
					$data['post'] = $mpage->read_course('S1','-', $data['lang'],'main',"",100);	
				endif;*/
				if($id):
					$data['detail'] = $mpage->get_unit_kerja('minat', 'PTIIK', $data['lang'], '',$id,$data['unit']);
				else:
					$data['post'] = $mpage->get_unit_kerja('minat', 'PTIIK', $data['lang'], '','',$data['unit']);
				endif;
			break;
			
			case 'pengumuman':								
				if($id):
					$mpage->update_hits_content($id_page);
				
					$data['detail'] = $mpage->read_content('-',$data['lang'],"", "", $id);	
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'pengumuman',"","",100);	
				endif;
			break;
			
			case 'event':								
				if($id):					
					$data['detail'] = $mpage->read_event($data['unit'],$data['lang'],"", $id,1);	
				else:
					$data['post'] = $mpage->read_event($data['unit'],$data['lang'],"","",100);	
				endif;
			break;
			case 'beasiswa':								
				if($id):
					$mpage->update_hits_content($id_page);
					$data['detail'] = $mpage->read_content('-',$data['lang'],"", $id);	
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'beasiswa',"","",100);	
				endif;
			break;
			
			default:				
				if($idpage)	$data['detail'] = $mpage->read_content($data['unit'],$data['lang'],"", $strlink,$idpage,1);
				else $data['post'] = $mpage->read_content($data['unit'],$data['lang'],"", $strlink,$idpage,1);					
			break;
		}
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content('0',$data['lang'],'news','','',10);
		$data['footer'] = $mpage->read_content('0',$data['lang'],'footer');
						
		if($act):	
			$data['view_slide']= 0;				
			$this->add_style('css/bootstrap/DT_bootstrap.css');
			// $this->add_script('bootstrap/js/bootstrap.min.js');
			$this->add_script('js/datatables/jquery.dataTables.js');	
			$this->add_script('js/datatables/DT_bootstrap.js');
			if($strlink=='video'||$strlink=='video-streaming'){
				$this->add_style('ptiik/flowplayer/skin/minimalist.css');
				$this->add_style('ptiik/css/livestream.min.css');
				
				$this->view( 'page/view-gallery.php', $data);
			}else{	
				if($strlink=='course' || $strlink=='matakuliah'){
					$this->view( 'page/view-course.php', $data);
				}else{
					$this->view( 'page/view-content.php', $data);
				}
			}
		else:
			$this->view( 'page/index.php', $data);
		endif;
		
	}
	
	function get_hari($hari){
		switch ($hari) {
			case '1': return 'senin'; break;
			case '2': return 'selasa'; break;
			case '3': return 'rabu'; break;
			case '4': return 'kamis'; break;
			case '5': return 'jumat'; break;
			case '6': return 'sabtu'; break;
			case '7': return 'minggu'; break;
			
			default: return 'minggu'; break;
		}
	}
		
}
?>