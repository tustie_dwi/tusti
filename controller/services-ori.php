<?php 
class controller_services extends comscontroller {

	function __construct() {
		parent::__construct();
		// /$this->require_auth('page');
	}
	
	function index(){
		//var_dump( "aaa");
	}
	
	function api($str_=NULL, $val=NULL, $param=NULL){
		if($str_):
			switch($str_){
				case 'akademik':
					$this->akademik($val, $param);
					exit();
				break;
				default:
					$this->api();
				break;
			}
		endif;
	}	
	
	function akademik($val=NULL, $param=NULL){
		switch($val){
			case 'jadwal':
				$this->get_jadwal_($param);
			break;
		}
	}
	
	function get_jadwal_($param=NULL){
		$mconf = new model_services();
		
		$result = $mconf->get_staff();		
		
		$return_arr = array();
		$arr_tmp = array();
		$arr_tmp_n = array();
		
		if($result):
			foreach($result as $key){
				$mk = $mconf->get_mk_aktif($key->karyawan_id,1);
				$arr_mk="";
				if($mk):
					foreach($mk as $dt):
						$arr_mk=array("mk_id"=>$dt->mk_id, "mk_name"=>$dt->nama_mk, "mk_kode"=>$dt->kode_mk);
					endforeach;	
					array_push($arr_tmp, $arr_mk);
					
					$arr = array(
						'id' => $key->karyawan_id,
						'name' => $key->nama,
						'gelar_awal' => $key->gelar_awal,
						'gelar_akhir'=>$key->gelar_akhir,
						'email'=>$key->email,
						'mk'=>$arr_tmp
					);				
					
					
				else:
					$arr_mk_n=array("mk_id"=>"", "mk_name"=>'None', "mk_kode"=>'');
					array_push($arr_tmp_n, $arr_mk_n);
					
					$arr = array(
						'id' => $key->karyawan_id,
						'name' => $key->nama,
						'gelar_awal' => $key->gelar_awal,
						'gelar_akhir'=>$key->gelar_akhir,
						'email'=>$key->email,
						'authors'=>$arr_tmp_n
					);			
				endif;				
				array_push($return_arr,$arr);
			}
		endif;
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
	
		# Return the response
		echo $json_response;
	}
}
?>