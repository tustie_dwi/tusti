<?php
class controller_page extends comscontroller {

	function __construct() {
		parent::__construct();
		
	}
	
	function read_gallery(){
		$id = $_POST['id'];
		$data['lang'] = $_POST['lang'];
		$data['unit'] = $_POST['unit'];
		
		$mpage = new model_page();
		
		if($id)	$mpage->update_hits_file($id);
						
		$data['detail'] = $mpage->read_slide($data['unit'],$data['lang'],1,"",$id);
		$data['post'] 	= $mpage->read_slide($data['unit'],$data['lang'],5,"'video'");
		
				
		$this->add_style('ptiik/css/tubestyle.min.css');
		$this->add_style('ptiik/css/base.tube.min.css');
		
		$this->view('page/gallery/index.php', $data);
		
	}
	
	function init_calendar_json(){
		$mpage = new model_page();
		$data = array();
		
		// $date = '2014-10-09';
		$date = date('Y-m-d');
		$sql = $mpage->get_agenda_json($date);
		
		if($sql){
			foreach($sql as $key){
				$temp = array(
					"date" => $key->tgl_mulai,
				    "badge" => true,
				    "title" => "-",
				    "body" => "-",
				    "footer" => "-",
				    "classname" => "-"
				);
				array_push($data, $temp);
			}
			echo json_encode($data);
		}
		else{
			echo "";
		}
		
	}
	
	function get_event_hari(){
		$data['lang'] = $this->config->lang;//'EN';
		if(! isset($_POST['tgl'])) $this->redirect($this->location('page/event'));
		$date = $_POST['tgl'];
		$tgl = explode("-", $date);
		$mpage = new model_page();
		$data = $mpage->get_agenda_bulan($tgl[2],$tgl[1], $tgl[0], $data['lang']);
		if($data){
			$data = json_encode($data);
			echo $data;
		}
		else echo '';
	}
	
	function event_json(){
		$data['lang'] = $this->config->lang;//'EN';
		if(! isset($_POST['init'])) $this->redirect($this->location('page/event'));
		$mpage = new model_page();
		$fak = $this->config->fakultas;
		$cabang = $this->config->cabang;
		
		$data = $mpage->get_agenda_json($_POST['date'],$data['lang']);
		
		if($data){
			$data = json_encode($data);
			echo $data;
		}
		else echo '';
	}
	
	function get_event_bulan(){
		$data['lang'] = $this->config->lang;//'EN';
		if(! isset($_POST['bulan'])) $this->redirect($this->location('page/event'));
		$mpage = new model_page();
		$data = $mpage->get_agenda_bulan("",$_POST['bulan'], $_POST['tahun'], $data['lang']);
		if($data){
			$data = json_encode($data);
			echo $data;
		}
		else echo '';
	}
	
	function event(){
		$mpage = new model_page();
		$data['lang'] = $this->config->lang;//'EN';
		$data['unit'] ='0';
		
		$data['mimg'] = new imageprocessing();
		
		$date = date("Y-m-d");
		$data['mpage']	= $mpage;
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',10);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		
		$data['bulan'] = date('m');
		$data['dateNow'] = $date;
		$data['jenis_kegiatan'] = $mpage->jenis_kegiatan();
		$data['view_slide']= 0;
		
		$this->add_script('js/page/agenda/agenda.js');		
		$this->add_style('calendar/zabuto_calendar.min.css');
		$this->add_script('calendar/zabuto_calendar.min.js');
		// $this->add_script('js/datatables/jquery.dataTables.js');	
		// $this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view('page/event/event.php', $data);
	}
	
	function notfound($str=NULL){
		if($str){
			echo $str;
		}				
		exit();
	}
	
	
	function read_($str=NULL){
	
		if($str){
			$data['in_session'] = $str;
			
			if(isset($_POST['password'])){
				$cek = new model_page();
				
				$data['pass'] = $_POST['password'];
				$data['posts'] = $cek->cek_file($str);
			}else{
				$data['posts'] = "";
			}
			
			$this->view("page/view-file.php", $data);
		}
	}
	
	function read_data($str=NULL, $url=NULL){
		
			$data_array = explode("/", $str);
			
			$cat = $data_array[0];
			$val = end($data_array);
			
			$data['lang'] 		= $this->config->lang;//'EN';
			$data['kategori'] = $cat;			
			
			if($cat=="mutu"):
				$mpage = new model_page();
					
								
				$data['mpage'] 			= $mpage;
				$data['kategori_data']	= $val;
				
				$data['unit_mutu']	= $mpage->read_mutu_unit($val, $data['lang']);
				if($val=='aim')	$data['siklus']	= $mpage->read_mutu_siklus($val, $data['lang']);
				
				$this->add_script('js/datatables/jquery.dataTables.js');	
				$this->add_script('js/datatables/DT_bootstrap.js');
				
				$this->view( 'info/mutu/index.php', $data );
			endif;
			
			if($cat=="prestasi"):
				$minfo = new model_info();
		
				$data['posts'] 		= $minfo->read_prestasi("",'non');
				$data['aposts'] 	= $minfo->read_prestasi("",'akademik');
								
				$this->view( 'info/prestasi/index.php', $data );
			endif;
			
			if($cat=="alumni"):
				$minfo = new model_info();
		
				$data['posts'] = $minfo->get_wisudawan($data['lang'],$val);
				$this->view( 'info/wisuda/index_wisuda.php', $data );
			endif;
			
			if(($cat=="beasiswa") || ($cat=="karir")):
				
				$mpage = new model_page();
				$data['mpage'] 			= $mpage;
		
				$data['posts'] = $mpage->read_content('-', $data['lang'],$cat,"","",100);	
			
				$this->view('info/general/index.php', $data );
			endif;
			
			if($cat=="staff"):
				$minfo = new model_info();
				
				$data['kategori_data']	= $val;
						
				$data['posts'] = $minfo->get_civitas("",$val);
				$data['abjad'] = $minfo->get_civitas_abjad("",$val);
				
				$this->view('info/general/index.php', $data );
			endif;
			
			if(strtolower($cat)=="s2"):
				$minfo = new model_info();
				
				$data['kategori_data']	= $val;
				if($val=='dosen')	$data['posts'] = $minfo->get_civitas("",$val,"","S3");
				else 	$data['posts'] = $minfo->get_civitas("",$val);
				
				$data['abjad'] = $minfo->get_civitas_abjad("",$val);
				$this->view('info/general/index.php', $data );
			endif;
			
			
			if($cat=="fasilitas"):
				$mpage = new model_page();
				
				
				$data['kategori_data']	= $val;
				
				if($val=='lab')	$data['posts'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
				else $data['posts'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
				
				$this->view('info/general/index.php', $data );
			endif;
			
			if($cat=="map"):
				
				$mconf = new model_info();
				$data['post'] 	= $mconf->get_lokasi();
		
				if(isset($_POST['cmblokasi'])){
					$data['cmblokasi'] = $_POST['cmblokasi'];
				}else{
					$data['cmblokasi'] = 'A1';
				}
				
				if($data['cmblokasi']!='0'){
					$data['dosen'] = $mconf->get_civitas("",'dosen',$data['cmblokasi']);
					$data['staff'] = $mconf->get_civitas("",'staff',$data['cmblokasi']);
				}else{
					$data['dosen'] = "";
					$data['staff'] = "";
				}
				
				$data['val'] = $cat;
				$this->view('info/map/index.php', $data );
			endif;
	}
	
	function search(){
		$mpage = new model_page();
		if(! isset($_POST['searchTxt'])) $this->redirect('page');
		
		$data['search'] = $_POST['searchTxt'];
		
		$data['lang'] = $this->config->lang;//'EN';
		$data['unit'] ='0';
		
		$data['mimg'] = new imageprocessing();
		
		$data['mpage']	= $mpage;
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',10);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		
		$data['view_slide']= 0;
		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');
		
		$this->view('page/view-search.php', $data);
	}
	
	function searchForId($id) {
		
		$array = $this->search_json();
	   foreach ($array as $key => $val) {
		   if ($val['uid'] === $id) {
			   return $key;
		   }
	   }
	   return null;
	}
	
	function search_json($nilai=NULL, $unit=NULL, $url_unit=NULL, $url_cat=NULL, $url_val=NULL){
		$mpage = new model_page();
		
		$fak = $this->config->fakultas;
		$cabang = $this->config->cabang;
		
		$data['mimg'] = new imageprocessing();
		$data = $mpage->get_search_content($nilai, $this->config->lang, $fak, $cabang, $unit);
		$json = array();
		
		if($data){
			foreach($data as $key){
				$kategori= $key->content_category;
				
				$judul_link=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($key->content_title))))));
				
				if(isset($url_unit)):
					$url = $url_unit."/".$url_cat."/".$url_val;
					if($kategori=='news' || $kategori=='pengumuman') $url_web= $this->location($url.'/read/'.$key->content_category.'/'.$judul_link.'/'.$key->id);
					else $url_web= $this->location($url.'/read/'.$judul_link.'/'.$key->id);
				else:
					if($kategori=='news' || $kategori=='pengumuman') $url_web= $this->location('page/read/'.$key->content_category.'/'.$judul_link.'/'.$key->id);
					else $url_web= $this->location('page/read/'.$judul_link.'/'.$key->id);
				endif;
				
				if($key->content_thumb_img) $imgthumb = $this->config->file_url_view."/".$key->content_thumb_img; 
				else $imgthumb = $this->config->default_thumb_web;
				$temp = array(
					'id' => $key->id,
					'judul' => $key->content_title,
					'isi' => strip_tags($key->content),
					'tgl' => $key->content_upload,
					'img' => $imgthumb,
					'kategori' => $key->content_category,
					'url_web'=> $url_web
				);
				
				array_push($json, $temp);
			}
		}

		$data = $mpage->get_search_karyawan($nilai);
		if($data){
			foreach($data as $key){
				if($key->foto) $img = $this->config->file_url_view."/".$key->foto; 
				else $img = $this->config->default_thumb_web;
				
				//$img = $this->location('fileupload/assets/'.$key->foto);
				$url_web = $this->location('info/staff/'.$key->karyawan_id);
				$temp = array(
					'id' => $key->karyawan_id,
					'judul' => $key->nama,
					'isi' => strip_tags($key->biografi),
					'tgl' => $key->last_update,
					'img' => $img,
					'kategori' => ucfirst($key->is_status),
					'url_web'=> $url_web
				);
				array_push($json, $temp);
			}
		}
		
		echo json_encode($json);
	}
	
	
	function index($str=NULL){
		if($str){
			$this->read($str);
			exit();			
		}
		
		$mpage = new model_page();
		//$mimg = new imageprocessing();
		$data['lang'] = $this->config->lang;//'EN';
		
		$data['unit'] ='0';
		$data['mimg'] = new imageprocessing();
		
		//$data['all'] = $this->search_json();
		
		$data['mpage']	= $mpage;
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		
		$data['about'] 	= $mpage->read_content($data['unit'],$data['lang'],'about','','',1);
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
	//	$data['apps'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news','','',10);
		$data['pengumuman']	= $mpage->read_content('-',$data['lang'],'pengumuman','','',5);
		$data['event'] = $mpage->read_event($data['unit'],$data['lang'],"","",5);		
		$data['slide']	= $mpage->read_slide($data['unit'],$data['lang'],4);
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		
		$data['color'] = "orange";
				
		if($str){
			$data['post'] = $mpage->read_content($data['unit'], $data['lang'],"", $str);	
		}else{
			$data['post'] = false;//$mpage->read_content("", 'about');
		}
		
		$this->view('page/index.php', $data);
	}
		
	function read($str=NULL, $id=NULL, $id_page=NULL){
		$mpage = new model_page();
		
		$data['mpage'] 	= $mpage;
		$data['kategori'] = $str;
		$data['id'] 	= $id;
		
		$data['lang'] = $this->config->lang;
		$data['unit'] ='0';
		
		$data['color'] = "orange";
		
		$data['mimg'] = new imageprocessing();
	
		switch($str){
			case 'gallery':								
				if($id):					
					$data['detail'] = $mpage->read_slide($data['unit'],"",1,"'video'", $id_page);	
				else:
					$data['post'] = $mpage->read_slide($data['unit'],$data['lang'],5,"'video'");
				endif;
			break;
			
			case 'video':								
				if($id):					
					$data['detail'] = $mpage->read_slide($data['unit'],"",1,"'video'", $id_page);	
				else:
					$data['post'] = $mpage->read_slide($data['unit'],$data['lang'],5,"'video'");
				endif;
			break;
			
			case 'news':				
				if($id):
					$mpage->update_hits_content($id_page);
					$this->add_script('js/submit.js');
					$data['detail'] = $mpage->read_content($data['unit'],"","", $id);
					//edit
					$data['title_page']=true;	
				else:
					$data['post'] = $mpage->read_content($data['unit'], $data['lang'],'news',"","",100);	
				endif;
			break;
			
			case 'pengumuman':								
				if($id):
					$mpage->update_hits_content($id_page);
					$data['detail'] = $mpage->read_content('-',"","", $id);	
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'pengumuman',"","",100);	
				endif;
			break;
			
			case 'event':								
				if($id):					
					$data['detail'] = $mpage->read_event($data['unit'],"","", $id_page,1);	
				else:
					$this->event();
					//$data['post'] = $mpage->read_event($data['unit'],$data['lang'],"","",100);	
				endif;
			break;
			
			case 'beasiswa':								
				if($id):
					$mpage->update_hits_content($id_page);
					$data['detail'] = $mpage->read_content('-',"","", $id);	
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'beasiswa',"","",100);	
				endif;
			break;
			
			case 'karir':								
				if($id):
					$mpage->update_hits_content($id_page);
					$data['detail'] = $mpage->read_content('-',"","", $id);	
				else:
					$data['post'] = $mpage->read_content('-', $data['lang'],'karir',"","",100);	
				endif;
			break;
			
			default:
				/*if($id)	$data['detail'] = $mpage->read_content($data['lang'],"", $id,1);
				else $data['post'] = "";	*/
				if($id)	$data['detail'] = $mpage->read_content($data['unit'],"","", $str,$id,1);
				else $data['post'] = $mpage->read_content($data['unit'],$data['lang'],"", $str,$id,1);					
			break;
		}
		
		$data['lab'] = $mpage->get_unit_kerja('laboratorium','PTIIK', $data['lang']);
		$data['prodi'] = $mpage->get_unit_kerja('prodi','PTIIK', $data['lang']);
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news');
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	

		$data['gallery'] = $mpage->read_slide('0',$data['lang'],8,"'gallery','video'");	
		//echo $str;
		if($str):	
			$data['view_slide']= 0;
			$this->add_script('js/datatables/jquery.dataTables.js');	
			$this->add_script('js/datatables/DT_bootstrap.js');
			
			if($str=='video'||$str=='video-streaming'){
				$this->add_style('ptiik/flowplayer/skin/minimalist.css');
				$this->add_style('ptiik/css/livestream.min.css');
			}
			
			if($str=='gallery'){
				$this->add_style('ptiik/flowplayer/skin/minimalist.css');
				$this->add_style('ptiik/css/tubestyle.min.css');
				$this->add_style('ptiik/css/base.tube.min.css');
			}
			
			
			if(strtolower($str)=='gallery' || strtolower($str)=='video' || strtolower($str)=='video-streaming'){
				//$this->add_style('ptiik/css/menuapps.slider.min.css');	
				$this->view( 'page/view-gallery.php', $data);
			}else{
				$this->view( 'page/view-content.php', $data);
			}
		else:
			$this->view( 'page/index.php', $data);
		endif;
		
	}
	
	
	
	function add_comment(){
		$user = new model_user();
		$mpage = new model_page();
		if( $authenticatedUser = $user->authcoment( $_POST['comment-email'], $_POST['comment-password'] ) ) {
			// bisa nulis komen
			ob_start();
			
			$content_id 	= $_POST['content-news'];
			$comment 		= $_POST['comment-content'];
			$email 			= $_POST['comment-email'];
			$username		= $authenticatedUser->username;
			$parent_id 		= $_POST['parentid'];
			$tgl_comment	= date("Y-m-d H:i:s");
			$lastupdate		= date("Y-m-d H:i:s");
			$approve		= 0;
			$ip_address		= $_SERVER['REMOTE_ADDR'];
			$user			= $authenticatedUser->id;
			
			$cek_last_comment = $mpage->cek_last_comment($user,$content_id);
			
			if($cek_last_comment != 0 || $cek_last_comment == NULL){
				$datanya 	= Array('content_id' => $content_id,
									'comment' => $comment,
									'user_name' => $username,
									'email' => $email,
									'parent_id' => $parent_id,
									'tgl_comment' => $tgl_comment,
									'last_update' => $lastupdate,
									'approved' => $approve,
									'ip_address' => $ip_address,
									'user_id' => $user
							);
				$mpage->replace_comment($datanya);
				echo "Berhasil menambahkan comment! Comment anda selanjutnya akan diproses oleh admin terlebih dahulu!";
				exit();
			}
			else {
				echo "Comment anda yang sebelumnya belum di proses!";
				exit();
			}
		}
		else {
			//data tidak valid
			echo "Tidak Boleh Comment";
			exit();
		}
	}
	
	function detail_comment($content_id, $comment_id, $mpage){
		$detail = $mpage->read_comment($content_id, $comment_id);
		
		if($detail){
		
			foreach ($detail as $dt):
			?>
			
			<div class="media media-comment">
				<a class="pull-left media-img-container" href="#">
					<?php if($dt->foto){ ?>
						<img class="media-object" src="<?php echo $this->location($dt->foto); ?>" alt="...">
					<?php }else{
					?>
						<img class="media-object" src="<?php echo $this->location("assets/images/logo-ub-blue-header.png"); ?>" alt="...">
					<?php } ?>
				</a>
				<div class="media-body">
					<h4 class="media-heading"><?php echo $dt->name ?></h4>
					<p class="comment-paragraph"><?php echo $dt->comment ?></p>
					<div class="comment-action">
						<!-- <a href="" class="delete"><span class="glyphicon glyphicon-trash"></span></a> -->
						<a href="" class="reply" data-url="" data-parentid="<?php echo $dt->id; ?>"><span class="glyphicon glyphicon-share-alt"></span></a>
					</div>
					
				</div>
			</div>					
				
			<?php
			endforeach;
		}
	}
	
	//====feedback====//
	
	function feedback(){
		$mpage 		= new model_page();
		$mconfinfo	= new model_confinfo();
		
		$data['lang'] 	= $this->config->lang;
		$data['unit'] ='0';
		
		$data['mpage']	= $mpage;
		
		$data['view_slide']= 0;
		
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		$data['news'] 	= $mpage->read_content($data['unit'],$data['lang'],'news',"","","","",1);
		$data['event'] 	= $mpage->read_event($data['unit'],$data['lang'],"","",5);	
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');	
		
		$this->add_script('js/submit.js');
		
		$this->view( 'page/feedback.php', $data);
	}
	
	function save_feedback(){
		$user 		= new model_user();
		$mpage 		= new model_page();
		if( $authenticatedUser = $user->authcoment( $_POST['feedback-email'], $_POST['feedback-password'] ) ) {
			$content_id		= '-';
			$user_name		= $authenticatedUser->username;
			$email 			= $_POST['feedback-email'];
			$comment 		= $_POST['feedback-content'];
			$comment_post	= date("Y-m-d H:i:s");
			$ip_address		= $_SERVER['REMOTE_ADDR'];
			$approve		= 0;
			$user_id		= $authenticatedUser->id;
			$lastupdate		= date("Y-m-d H:i:s");
			$category		= "feedback";
			
			$cek_last_comment = $mpage->cek_last_comment($user_id,$content_id,$category);
			
			if($cek_last_comment != 0 || $cek_last_comment == NULL){
				$data_feedback	= array(
										'comment_id'=>$mpage->comment_id(),
										'content_id'=>$content_id,
										'user_name'=>$user_name,
										'email'=>$email,
										'comment'=>$comment,
										'comment_post'=>$comment_post,
										'ip_address'=>$ip_address,
										'is_approve'=>$approve,
										'user_id'=>$user_id,
										'last_update'=>$lastupdate,
										'category'=>$category
									   );
				$save	= $mpage->replace_comment($data_feedback);
				if($save) echo "Berhasil";
				else echo "Gagal";
			}
			else {
				echo "Feedback anda yang sebelumnya sedang di proses!";
				exit();
			}
		}
		else{
			echo "Anda tidak memiliki hak akses untuk memberikan feedback!";
		}
	} 
	
	
}
?>