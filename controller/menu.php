<?php
class controller_menu extends comscontroller {

	function __construct() {
		parent::__construct();
		
		
	}
	
	public function index( $status = NULL){
	
		$this->require_auth('auth');
		if(isset($_POST['b_menu'])){
			$this->saveToDB();			
		}
		
		if($this->authenticatedUser->level > 1)
			$this->no_privileges();
	
		$mmenu = new model_menu();	
		
		$data['menu'] 	 = $mmenu->read_menu();
		$data['submenu'] = $mmenu->read_menu(1,1);
		$data['emenu'] 	 = "";
		
		$this->add_style('css/bootstrap/DT_bootstrap.css');		
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_style('select/select2.css');
		$this->add_script('select/select2.js');
		
		$this->add_script('js/other/menu.js');	
		$this->add_script('js/jsall.js');	
		
		$this->view("menu/index.php", $data);
	}
	
	function activate($id=NULL) {
		$mmenu = new model_menu();
		
		if($id){
			if($mmenu->activate($id))
			{	
				echo json_encode(array('result'=>"OK"));
			} else echo json_encode(array('result'=>"NOK"));
		}
	}
	
	function deactivate($id=NULL) {
		$mmenu = new model_menu();
		
		if($id){
			if($mmenu->deactivate($id))
			{	
				echo json_encode(array('result'=>"OK"));
			} else echo json_encode(array('result'=>"NOK"));
		}
	}
	
	private function saveToDB(){
		//ob_start();
		
		$mmenu = new model_menu();
		
		if(isset($_POST['ncontroller'])){
			$link 	= $_POST['ncontroller'];
			$judul 	= $_POST['njudul'];
			$nmodule= $_POST['nmodules'];
			$icon	= $_POST['nicon'];
			$parentid= $_POST['cmbmenu'];
			
			if(isset($_POST['isaktif'])){
				$isaktif	= $_POST['isaktif'];
			}else{
				$isaktif	= 0;
			}
			
			//$datanya = array('link'=>$link, 'judul'=>$judul, 'parent_id'=>$parentid, 'module'=>$nmodule, 'icon'=>$icon, 'is_aktif'=>$isaktif);
					
			$save = $mmenu->save_menu($link, $judul, $parentid, $nmodule, $icon, $isaktif);
							
			if( $save ) {
				$result['status'] = "OK";
				$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
			} else {
				$result['status'] = "NOK";
				$result['error'] = $mmenu->error;
			}
			
			echo json_encode($result);
		}
		
		
	}	
}