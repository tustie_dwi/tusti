<?php
class controller_absen extends comscontroller {

	function __construct() {
		parent::__construct();
		// /$this->require_auth('page');
	}
	
	function details($str=NULL, $type=NULL, $style='calendar'){
		$url= $this->location('info/vw');
		
		$allowed = "/[^a-z0-9]/i";
		
		$id = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9+]/', '', urldecode(html_entity_decode(strip_tags($str))))));
		// if(empty($id)) {
			// $this->notfound('error tidak diketemukan');
			// exit;
		// }
		
		$magenda = new model_agendainfo();	
		$mconf = new model_info();
		
		/*--- page -- */
		
		$mpage 		= new model_page();
		$data['lang'] 	= $this->config->lang;
		
		$data['mimg'] = new imageprocessing();
		
		$data['view_slide']= 0;
		
		$data['mpage']	= $mpage;
		
		$data['unit'] = '0';
		$data['page'] 	= $mpage->read_content($data['unit'],$data['lang'],'header_menu');
		
		$data['footer'] = $mpage->read_content($data['unit'],$data['lang'],'footer');
		/*--- end page -- */	
		
		$data['posts'] = "";
		$data['type']  = $type;		

		$this->add_style('css/calendar/calendar.css');
		
		if(isset($_POST['month'])){
			$month = $_POST['month'];
		}else{
			$month = date('m');
		}
		
		if(isset($_POST['year'])){
			$year = $_POST['year'];
		}else{
			$year = date('Y');
		}
		
		if(isset($_POST['hidid'])){
			$data['civitasid']	= $_POST['hidid'];
		}else{
			$data['civitasid']	= $id;
		}
		
		if(isset($_POST['tgl'])){
			$tgl	 = $_POST['tgl'];
		}else{
			$tgl	 = date('Y-m-d');
		}	
		
		if(isset($_POST['hidmonth'])){
			$data['hidmonth']	= $_POST['hidmonth'];
		}else{
			$data['hidmonth']	= "";
		}
		
		if(isset($_POST['hidtgl'])){
			if(isset($_POST['b_now'])){
				$hidtgl	 = date('Y-m-d');
			}else{
				if(isset($_POST['b_prev'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('-7 days', $in));	
				}else if(isset($_POST['b_next'])){
					$in = strtotime($_POST['hidtgl']);
					$hidtgl = date("Y-m-d", strtotime('+7 days', $in));	
				}else{
					$hidtgl	 = $_POST['hidtgl'];
				}
			}
		}else{
			$hidtgl	 = date('Y-m-d');
		}	
		
		
		$allowed = "/[^a-z0-9]/i";
		$month = trim(preg_replace('/ +/', ' ', preg_replace($allowed, '', urldecode(html_entity_decode(strip_tags($month))))));
		$year  = trim(preg_replace('/ +/', ' ', preg_replace('/[^0-9+]/', '', urldecode(html_entity_decode(strip_tags($year))))));
		
		if(empty($year) && empty($month)) {
			$this->notfound('error tidak diketemukan');
			exit;
		}
		
		$this->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->add_script('datepicker/js/moment-2.4.0.js');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
			
		$data['month'] 		= $month;
		$data['year']  		= $year;
		$data['title'] 		= date('F Y',mktime(0,0,0,$month,1,$year));
		$data['tgl']		= $tgl;
		$data['hidtgl']		= $hidtgl;
		$data['hadir'] 		= $mconf->get_civitas($data['civitasid']);
		$data['absen']   	= $mconf->get_finger_print($tgl);	
		$data['kegiatan']	= $mconf->get_agenda_civitas($tgl,"",$data['civitasid']);
		$data['posts']    	= $this->draw_month_hadir($month, $year, $data['civitasid'], $style, $url);
		$data['weekdata']  	= $this->draw_week_hadir($month, $year, $hidtgl, $data['civitasid'], $style, $url);
		
		$this->view( 'info/hadir_2/index.php', $data );
	}

	function draw_month_hadir($month, $year, $uid, $style, $url=NULL) {
		$mhadir = new model_hadir();	
				
		 if(($month == NULL) || ($year == NULL))
         {
             // Month in numbers with the leading 0
             $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
         }else{
			 $title = date('F Y',mktime(0,0,0,$month,1,$year));
		 }

		/* We need to take the month value and turn it into one without a leading 0 */
         if((substr($month, 0, 1)) == 0)
         {
             // if value is between 01 - 09, drop the 0
             $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         /* draw table */
		
         $str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
        
         /* table headings */
         $headings = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
         $str.= '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         /* days and weeks vars now ... */
         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         /* row for week one */
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         /* print "blank" days until the first of the current week */
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 $list_day=0;
		 $skip = false;
		 
		 $data = $mhadir->get_kegiatan("", $month, $year, $running_day, "",$uid);
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
            
             /* add in the day number */
             $str.= '<div class="'. $style .'-day-number">'.$list_day.'</div>';
			 $tgl	= $year."-".$mhadir->addNol($month)."-".$mhadir->addNol($list_day);
			 if($data){
				$k=0;
				foreach($data as $row):
					if($row->tgl == $tgl) {
					 	$k++;
						$this->get_label($icon, $note, $sclass, $tclass, $row);
						$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
								<a href="#"><small><span class="'.$tclass.'"><i class="fa  fa-clock-o"></i> '.$row->jam_mulai.", R.".$row->ruang.", ".$note.'</span></small></a></div>';						 							
					}
				endforeach;
			}
			else{
				$str.= "&nbsp;";
			}	
									
            $str.= '</td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
    
         /* finish the rest of the days in the week */
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
    
         /* final row */
         $str.= '</tr>';
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
     }
     
	 function draw_week_hadir($month, $year, $tgl, $uid,$style, $url=NULL){
	 	$mhadir = new model_hadir();	
		// set current date
		$in = strtotime(date('Y-m-d'));
		$date = $tgl;
		$month= date("m", strtotime($date));
		$nmonth= date("M", strtotime($date));
		$nyear= date("Y", strtotime($date));
		
		$ts = strtotime($date);
		
		$year = date('o', $ts);
		$week = date('W', $ts);		
		
		$data = $mhadir->get_kegiatan("", $month, $year, "", "", $uid);
		// $data = $mhadir->get_kegiatan("", $month, $year, $running_day, "",$uid);
				
		$str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
      	
		$str.= '<thead><tr class="'. $style .'-row">';
		for($i = 0; $i < 7; $i++) {
			
			$ts = strtotime($year.'W'.$week.$i);
			$tw = date("N", $ts);
			$td = date("D", $ts);
			$tn = date("Y-m-d", $ts);
			//print date("m/d/Y l", $ts) . "\n";
			if($tw==$i){
				$tp = $ts;				
			 }else{
				$tp = strtotime(date($tn, strtotime('last day')));
				
			 }
			$list_day = date("d", $tp);	
			$str.= '<td class="'. $style .'-day-head">'.$td.", ".$list_day.'</td>';
		}
		/* final row */
         $str.= '</tr></thead><tbody>';
		 $blokwaktu	= $mhadir->get_blok_waktu();
			$str.= '<tr>';
					for($i = 0; $i < 7; $i++) {
						
						$ts = strtotime($year.'W'.$week.$i);
						$tw = date("N", $ts);
						$tn = date("Y-m-d", $ts);
						
						if($tw==$i){
							$tp = $ts;
									
						 }else{
							$tp = strtotime(date($tn, strtotime('last day')));
							
						 }
						 
						 $list_day = date("d", $tp);
						 $mmonth = date("m", $tp);
						 
						if($list_day == date("j",mktime(0,0,0,$month))) {
							$tstyle = $style .'-current-day';
						}else{
							$tstyle = $style;
						}
						
						$tgl	= $year."-".$mhadir->addNol($month)."-".$list_day;
						
						$str.= '<td width="10%" class="'.$tstyle.'">';	 
						 if($data){
							$k=0;
							foreach($data as $row):
								if($row->tgl == $tgl) {
								 	$k++;
									$this->get_label($icon, $note, $sclass, $tclass, $row);
									
									$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
											<a href="#"><small><span class="'.$tclass.'"><i class="fa  fa-clock-o"></i> '.$row->jam_mulai.", R.".$row->ruang.", ".$note.'</span></small></a></div>';						 							
								}
							endforeach;
								
							}else{
								$str.= "&nbsp;";
							}							
							
							
						 $str.= "</td>";
					}
			$str.= '</tr>';
		 //endforeach;
		 
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
	}

	function get_label(&$icon, &$note, &$sclass, &$tclass, $row){
		$mhadir = new model_hadir();
		switch (strToLower($row->jenis)){
			case 'konseling':
				$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'konseling';
				$tclass= 'text-putih';
			break;
			
			case 'rapat':
				$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'rapat';
				$tclass= 'text-putih';
			break;
			
			case 'kemahasiswaan':
				$icon = "";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'mhs';
				$tclass= 'text-putih';
			break;
			
			case 'kuliahtamu':
				$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'kuliahtamu';
				$tclass= 'text-putih';
			break;
			
			case 'wisuda':
				$icon = "<i class='fa fa-star'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'wisuda';
				$tclass= 'text-putih';
			break;
			
			case 'kunjungan':
				$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'kunjungan';
				$tclass= 'text-putih';
			break;
			
			case 'rekrutmen':
				$icon = "<i class='fa fa-signal'></i>&nbsp;";
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'rekrutmen';
				$tclass= 'text-putih';
			break;
			
			case 'penelitian':
				$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'success';
			break;
			
			case 'pelatihan':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'pelatihan';
				$tclass= 'text-putih';
			break;
			
			case 'workshop':
				$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'workshop';
				$tclass= 'text-putih';
			break;
			case 'seminar':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'seminar';
				$tclass= 'text-putih';
			break;
			case 'porseni':
				$icon = "<i class='fa fa-flag'></i>&nbsp;";
				//$note = $row->jenis;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'porseni';
				$tclass= 'text-putih';
			break;
			
			case 'uts':
				if($row->keterangan=='Pengawas'){
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}else{
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}								
			break;
			case 'uas':
				if($row->keterangan=='Pengawas'){
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}else{
					$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
					$note = strToUpper($row->kegiatan)." ".$row->namamk;	
					$sclass= 'warning';	
					$tclass= 'text-info';
				}								
			break;
			
			case 'kuliah':
				$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
				$note = $row->kegiatan." ".$row->namamk;	
				$sclass= 'success';	
				$tclass= 'text-putih';
			break;
			default:
				$icon = "<i class='fa  fa-clock-o'></i>".$row->jam_mulai;
				$note = $mhadir->potong_kalimat($row->kegiatan,5);
				$sclass= 'info';
				$tclass= 'text-putih';
			break;
		}
	}
}
?>