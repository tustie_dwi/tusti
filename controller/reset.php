<?php
class controller_reset extends comscontroller {

	function __construct() {
		parent::__construct();		
	}
	

	function index(){
		if(isset($_POST['b_reg'])){
			$this->save($_POST['email']);
			exit();
		}	
		
			
		$data['post']	= false;
		
		$data['msglogin']="";
		
		//$this->view("page/register.php", $data);
		$this->view( 'reset.php', $data );
	}
	
		
	function msg($by=NULL, $email=NULL){
		if($by){
			
			switch($by){
				case 'ok':
					$data['msglogin'] = '<div class="alert alert-block alert-success fade in"> 
									OK! Reset password success. Please login to <b>PTIIK Apps</b> using your new password.</div> ';
				break;
				case 'nok':
					$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">
									 Sorry, your <b>'.$email.'</b> is not registered in our data. Please contact <b>PSIK</b> for more information.</div>';
				break;
				case 'nok_email':
					$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">
									Your account has been registered. Please contact <b>PSIK</b> for more information.</div>';
				break;
				case 'nok_send':
					$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">
									Reset password failed. Please contact <b>PSIK</b> for more information.</div>';
				break;
			}
		}else{
			$data['msglogin'] = "";
		}
		
		
		/*$mpage = new model_page();
				
		$data['page'] 	= $mpage->read_content('page');
		$data['news'] 	= $mpage->read_content('news');
		$data['video'] 	= $mpage->read_video();
		$data['course'] = $mpage->read_course('main');	
		$data['topic'] 	= $mpage->read_topic('page');
		$data['slide']	= $mpage->read_slide();		
		$data['post']	= false;*/
		
		//$this->view("page/register.php", $data);
		$this->view( 'auth.php', $data );
		exit();
	}
	
	function cek(){		
		
		$mreg = new model_register();
		
		$email=$_POST['username'];
		$pass_old = $_POST['oldpass'];
		$pass = $_POST['password'];
		
		$user_account = $mreg->get_user_account_old($email, $pass_old);
		
		if(! $user_account){
			$this->msg('nok', $email);
		}else{	
			
				$saveuser = $mreg->update_user_account($email, $pass, $user_account->id);
				
				if($saveuser){						
					$this->msg('ok', $email);
				}else{
					
					$this->msg('nok_send', $email);
				}
					
		}
			
			
		
	}
	
}
?>