<?php
class controller_rss extends controller {

	function __construct() {
		parent::__construct();
	}

	function index(){
		$this->redirect('rss/feed');
	}
	
	function feed($str=NULL, $language = ''){
		$mpage = new model_rss();
		if(!empty($language) && isset($language)) {
			if($language == 'in') {
				$data['lang'] = 'id';
				$bahasa = 'in';
			} else if($language == 'en') {
				$data['lang'] = 'en-us';
				$bahasa = 'en';
			}
		} else {			
			$bahasa = $this->config->lang;
			if($bahasa == 'in') $data['lang'] = 'en-us';
			else $data['lang'] = 'en-us';
		}
		if($str=='in' || $str=='en' || (! $str)){
			$tmp = $mpage->rss_feed($bahasa,"'news','pengumuman'");
		}else{
			if($str=='prodi'):
				$tmp = $mpage->read_unit_kerja('PTIIK', $bahasa,$str);
			else:
			if($str=='event') $tmp = $mpage->read_event($bahasa);
			else $tmp = $mpage->rss_feed($bahasa,"'".$str."'");
			endif;
		}
		if(isset($tmp)) {
			foreach($tmp as $t) {
				if($t->content_title) 	$t->content_title = trim(stripslashes(preg_replace( '/[^a-zA-Z0-9 -]/','',$t->content_title)));
				else $t->content_title = trim(stripslashes(preg_replace( '/[^a-zA-Z0-9 -]/','',$t->judul_ori)));
				
				$t->content_excerpt = $this->split_content(stripslashes($t->content_excerpt), 20);
				$t->content = $this->split_content($t->content, 50);
				if($str=='prodi'):
					$t->content_link = 'unit/'.$t->kategori.'/'.strToLower($t->content_link);
				else:
					if($t->kategori == 'lab' || $t->kategori=='laboratorium') {
						$t->content_link = 'unit/lab/'.$t->unit_id.'/read/'.$t->content_category.'/'.$t->content_link.'/'.$t->id;
					} else if($t->kategori == 'prodi') {
						$t->content_link = 'unit/prodi/'.$t->unit_id.'/read/'.$t->content_category.'/'.$t->content_link.'/'.$t->id;
					} else {
						$t->content_link = 'page/read/'.$t->content_category.'/'.$t->content_link.'/'.$t->id;
					}
				endif;
				$url = strToLower($this->location(htmlentities($t->content_link)));
				$t->content_link = $url;
				$t->content_upload = date('D, d M Y H:i:s O', strtotime($t->content_upload));
			}
		}
		$data['posts'] = $tmp;
		$this->view( 'page/rss.php', $data );
	}

	function split_content ($content, $length) {
		$content = htmlentities(strip_tags($content));
		$content = preg_replace( '/\s+/', ' ', $content );
		$tmp = explode(" ", $content);
		if(count($tmp) <= $length) {
			return implode(" ", $tmp);
		} else {
			$data = array();
			$i = 0;
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			if(count($tmp) > $length)
				$data[] = "[...]";

			return implode(" ", $data);
		}
	}
}
?>