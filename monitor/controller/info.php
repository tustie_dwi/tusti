<?php
class controller_info extends controller {

	function __construct() {
		parent::__construct();
	}
	
	function index($str=NULL) {
		$hari = $this->hari(date('w'));
		$waktu = $this->jam_monitor();
		$mconf = new model_monitor();
		
		
		//$data['berita']	= $mconf->berita();
		
		$data['berita']	= $mconf->read_content('news','en', 5);
		$data['cuaca']	= $mconf->get_cuaca();
		$data['jadwal'] = $mconf->get_jadwal($hari, $waktu, $str);
		$data['hari']	= $hari;
		$this->view( 'jadwal.php', $data );
	}
	
	function wall(){
		$mconf = new model_monitor();
	
		$data['posts']	= $mconf->read_content('pengumuman', '', 10);
		$data['berita']	= $mconf->read_content('news','en', 5);
		$data['cuaca']	= $mconf->get_cuaca();
		$data['tweet']	= $mconf->get_tweet(6);
		
		$this->view( 'wall.php', $data );
	}
	
	function absen(){
		$mconf		 	= new model_monitor();
		
		$data['berita']	= $mconf->read_content('news','en', 5);
		$data['mconf'] = $mconf;
		
		$data['cuaca']	= $mconf->get_cuaca();
		$data['tweet']	= $mconf->get_tweet(6);
		$data['absen'] 	= $mconf->get_absen(date('Y-m-d'));
		$data['absen_pimpinan'] = $mconf->get_absen_pimpinan(date('Y-m-d'));
		
		$this->view( 'absen.php', $data );
	}
	
	
	function hari($day){
		switch($day) {
			case 0 : $data = 'minggu';
				break;
			case 1 : $data = 'senin';
				break;
			case 2 : $data = 'selasa';
				break;
			case 3 : $data = 'rabu';
				break;
			case 4 : $data = 'kamis';
				break;
			case 5 : $data = 'jumat';
				break;
			case 6 : $data = 'sabtu';
				break;
		}
		return $data;
	}	
	
	function jam_monitor(){
		$time = strtotime(date('H:i'));
		switch($time) {
			case ($time >= strtotime('06:00') && $time <= strtotime('08:45')) :
				$sesi = 1;
				break;
			case (($time >= strtotime('08:46')) && ($time <= strtotime('12:00'))) :
				$sesi = 2;
				break;
			case ($time >= strtotime('12:01') && $time <= strtotime('14:45')) :
				$sesi = 3;
				break;
			case ($time >= strtotime('14:46') && $time <= strtotime('17:00')) :
				$sesi = 4;
				break;
			case ($time >= strtotime('17:01') && $time <= strtotime('21:00')) :
				$sesi = 5;
				break;
			default: $sesi = 1;
		}
		return $sesi;
	}
}
?>