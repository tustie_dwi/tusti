 <!-- Footer -->
        <footer id="footer">
            <div class="col-sm-8 footer-left">
                <div class="row">
                    <div class="col-sm-2 footer-logo">
                        <img src="<?php echo $this->asset("images/logo-ptiik-black.png") ?>" class="img-responsive img-logo-footer">
                    </div>
                    <div class="col-sm-10">
                        <div class="footer-info">
                            <div class="footer-slider">
                                <marquee scrollamount="3">
									<?php
									if($berita){
										foreach ($berita as $dt):
											?>
											<span><?php echo $dt->post_title ?></span>
											<?php
										endforeach;	
									}
									?>
                                  
                                </marquee>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 footer-right">
                <div class="col-sm-4 date-now">Hari,0 Bulan 2014</div>
                <div class="col-sm-4 clock-now">23:59</div>
                <div class="col-sm-4 weather"><img src="<?php if(isset($cuaca)){ $img= $cuaca->icon.".png"; } else{ $img= "cuaca-0.png";} echo $this->asset("images/". $img); ?>" width="50px" class="img-responsive pull-right"></div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- Script -->
        <script src="<?php echo $this->asset("v3/js/jquery-1.10.2.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.easing.1.3.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.bxslider.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.simpleWeather-2.3.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/function.js") ?>"></script>
        <script>
            $(document).ready(function() {
               firstStep();
               information();
               defaultStep();
            });

        </script>
    </body>
</html>
