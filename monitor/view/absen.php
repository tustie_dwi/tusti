<!DOCTYPE html>
<!--
Copyright 2014 rizky.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<html>
    <head>
        <title>Daftar Hadir</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Stylesheet -->
        <link rel="stylesheet" href="<?php echo $this->asset("v3/css/style.min.css") ?>">
		<script type="text/javascript">    
			function reFresh() {
			  location.reload(true).fadeIn(500);
			}
			
			window.setInterval("reFresh()",160000);
		</script>
    </head>
    <body>
        <!-- Content -->
        <section id="wrap" class="absen">
               <!-- Header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-8 title-pengumuman">
                            DAFTAR HADIR PIMPINAN
                        </div>
                        <div class="col-sm-4 logo-kanan">
                            <img src="<?php echo $this->assets("v3/images/header.png") ?>" class="img-responsive img-center">
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header -->

            <!-- Main Content -->
            <section class="container-fluid container-center">
                <div class="row">
                    <div class="col-sm-12 top-slider">
                        <div class="row">
                           <?php	
						if(	$absen_pimpinan){						
							foreach($absen_pimpinan as $dta){
								if($dta->foto==""){
									$foto= "http://ptiik.ub.ac.id/assets/ptiik/images/no_foto.png";
								} else {
									$foto = "http://adl.ptiik.ub.ac.id/fileupload/assets/".$dta->foto;
								}				
								
								
							
									if($dta->is_absen=='ada'){
										$class = "";
									} else {
										$class = "disabled";
									}
								
									
									
								?>
								 <div class="col-sm-3">
									<div class="div-image <?php echo $class; ?>">
										<img src="<?php echo $foto;?>" class="img-center img-responsive img-daftarhadir">
									</div>
									<div class="detail-nama <?php echo $class; ?>">
										<h3 class="nama"><?php echo $dta->nama; if($dta->gelar_awal!=""){ echo ", ".$dta->gelar_awal; } if($dta->gelar_akhir!=""){ echo ", ".$dta->gelar_akhir; } ?></h3>
										<div class="keterangan"><?php echo $dta->jabatan; ?></div>
									</div>
								</div>
								<?php
							}
						}
							?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <ul class="daftar-hadir-slider">
                               <?php 
							if($absen){
							foreach($absen as $dt){ 
								if($dt->foto==""){
									$foto= "http://ptiik.ub.ac.id/assets/ptiik/images/no_foto.png";
								} else {
									$foto = "http://adl.ptiik.ub.ac.id/fileupload/assets/".$dt->foto;
								}		

							
									if($dt->is_absen=='ada'){
										$class = "";
									} else {
										$class = "disabled";
									}
															
							?>
                                <li>
                                    <div class="">
                                        <div class="div-image <?php echo $class; ?>">
                                            <img src="<?php echo $foto; ?>" class="img-center img-responsive img-daftarhadir">
                                        </div>
                                        <div class="detail-nama <?php echo $class; ?>">
                                            <h3 class="nama"><?php echo $dt->nama; if($dt->gelar_awal!=""){ echo ", ".$dt->gelar_awal; } if($dt->gelar_akhir!=""){ echo ", ".$dt->gelar_akhir; } ?></h3>
                                            <div class="keterangan"><?php echo $dt->jabatan; ?></div>
                                        </div>
                                    </div>
                                </li>
								<?php } } ?>
                            </ul>
                        </div>


                    </div>
                </div>
            </section>
            <!-- End Main Content -->

        </section>
        <!-- End Content -->

        <!-- Footer -->
        <footer id="footer">
            <div class="col-sm-9 footer-left">
                <div class="row">
                    <div class="col-sm-2 footer-logo">
                        <img src="<?php echo $this->asset("images/logo-ptiik-black.png") ?>" class="img-responsive img-logo-footer">
                    </div>
                    <div class="col-sm-10">
                        <div class="footer-info">
                            <div class="footer-slider">
                                <marquee scrollamount="5">
                                    <?php
									if($berita){
										foreach ($berita as $dt):
											?>
											<span><?php echo $dt->post_title ?></span>
											<?php
										endforeach;	
									}
									?>
                                </marquee>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 footer-right">
                <div class="col-sm-4 date-now">Hari,<br>0 Bulan 2014</div>
                <div class="col-sm-4 clock-now">23:59</div>
                <div class="col-sm-4 weather"><img src="<?php if(isset($cuaca)){ $img= $cuaca->icon.".png"; } else{ $img= "cuaca-0.png";} echo $this->asset("images/". $img); ?>" width="50px" class="img-responsive pull-right"></div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- Script -->
        <script src="<?php echo $this->asset("v3/js/jquery-1.10.2.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.easing.1.3.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.bxslider.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.simpleWeather-2.3.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/function.js") ?>"></script>
        <script>
            $(document).ready(function() {
				firstStep();
                daftarHadir();
                defaultStep();
            });

        </script>
    </body>
</html>
