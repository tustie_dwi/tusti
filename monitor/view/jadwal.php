<?php $this->view( 'header.php' ); ?>
<script type="text/javascript">    
	function reFresh() {
	  location.reload(true).fadeIn(200);
	}			
	window.setInterval("reFresh()",30*60000);
</script>
 <section id="wrap" class="jadwal-a">
            <!-- Header -->
            <header id="header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-8 title-pengumuman">
                            JADWAL KULIAH <?php if ($lokasi) echo strToUpper($lokasi); ?>
                        </div>
                        <div class="col-sm-4 logo-kanan">
                             <img src="<?php echo $this->assets("v3/images/header.png") ?>" class="img-responsive img-center">
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header -->

            <!-- Main Content -->
            <section class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="content-left">
                            <div class="title">
                                <span class="title-black">
                                    <?php $bulan = chageToMonth(date("m", strtotime(date("Y-m-d"))));
												// $hari	= date("d", strtotime(date("Y-m-d")));
												 $tahun	= date("Y", strtotime(date("Y-m-d")));
												echo $hari.", ".date("d") ." ".$bulan." ".$tahun; ?>
                                </span>
                            </div>
                           <div style="padding-left: 30px"> 
                                <div class="header-list-jadwal">
                                 
								 <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-1">Ruang</div>
											<div class="col-lg-1">Jam</div>
                                            <div class="col-lg-3">Mata Kuliah</div>
                                            <div class="col-lg-1">Kelas</div>
                                            <div class="col-lg-2">Prodi</div>
                                            <div class="col-lg-3">Dosen Pengampu</div>
                                        </div>
                                    </div>
								
                                </div>
                                 	<div class="clearfix"></div>
                           
								<div class="list-jadwal">                                   
									
									<?php
										//if(isset($jadwal)) {
										$i = 1;
										foreach($jadwal as $dt): 
											if($dt->nama){											
												$nama = $dt->nama;
											}else{
												$nama = "Dosen Pengampu";
											}
											if(($dt->gelar_awal)&&($dt->gelar_awal!="-")){
												$gawal= ", ".$dt->gelar_awal;
											}else{
												$gawal="";
											}
											
											if(($dt->gelar_akhir)&&($dt->gelar_akhir!="-")){
												$gakhir= ", ".$dt->gelar_akhir;
											}else{
												$gakhir="";
											}
										?>
										<?php
											if($dt->prodi_id == "ILKOM") { $class = "label label-primary"; $prodi = "Informatika"; }
											else if($dt->prodi_id == "SI") { $class = "label label-warning"; $prodi = "Sistem Informasi"; }
											else if($dt->prodi_id == "SISKOM") { $class = "label label-success"; $prodi = "Sistem Komputer"; }
											else { $class = ""; $prodi = ""; }
										?>
										 <div class="col-lg-12">
											<div class="row">
													<span class="col-lg-1">R. <?php echo $dt->ruang;?></span>
													<span class="col-lg-1"><?php echo $dt->jam_mulai;?></span>
													<span class="col-lg-3"><?php echo $dt->namamk;?><?php if($dt->is_praktikum == 1) { echo " (Praktikum)"; } else { echo ""; } ?></span>
													<span class="col-lg-1"><?php echo $dt->kelas_id;?></span>
													<span class="col-lg-2"><?php echo $prodi;?></span>
													<span class="col-lg-3"><?php echo $nama.$gawal.$gakhir; ?></span>
												</div>
											</div>
									<?php $i++;
										endforeach;
										?>
										
                                </div>
								<div class="clearfix"></div>
							</div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- End Main Content -->

        </section>

 <!-- Footer -->
        <footer id="footer">
            <div class="col-sm-8 footer-left">
                <div class="row">
                    <div class="col-sm-2 footer-logo">
                        <img src="<?php echo $this->asset("images/logo-ptiik-black.png") ?>" class="img-responsive img-logo-footer">
                    </div>
                    <div class="col-sm-10">
                        <div class="footer-info">
                            <div class="footer-slider">
                                <marquee scrollamount="3">
									<?php
									if($berita){
										foreach ($berita as $dt):
											?>
											<span><?php echo $dt->post_title ?></span>
											<?php
										endforeach;	
									}
									?>
                                  
                                </marquee>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-sm-4 footer-right">
                <div class="col-sm-4 date-now">Hari,0 Bulan 2014</div>
                <div class="col-sm-4 clock-now">23:59</div>
                <div class="col-sm-4 weather"><img src="<?php if(isset($cuaca)){ $img= $cuaca->icon.".png"; } else{ $img= "cuaca-0.png";} echo $this->asset("images/". $img); ?>" width="50px" class="img-responsive pull-right"></div>
            </div>
        </footer>
        <!-- End Footer -->

        <!-- Script -->
        <script src="<?php echo $this->asset("v3/js/jquery-1.10.2.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.easing.1.3.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.bxslider.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/jquery.simpleWeather-2.3.min.js") ?>"></script>
        <script src="<?php echo $this->asset("v3/js/function.js") ?>"></script>
        <script>
            $(document).ready(function() {
                firstStep();
                jadwalA();
                defaultStep();
				
            });
			
			

        </script>
    </body>
</html>

<?php
function chageToMonth($number=NULL) {
    switch ($number) {
        case 1:
            return 'Januari';
            break;
        case 2:
            return 'Februari';
            break;
        case 3:
            return 'Maret';
            break;
        case 4:
            return 'April';
            break;
        case 5:
            return 'Mei';
            break;
        case 6:
            return 'Juni';
            break;
        case 7:
            return 'Juli';
            break;
        case 8:
            return 'Agustus';
            break;
        case 9:
            return 'September';
            break;
        case 10:
            return 'Oktober';
            break;
        case 11:
            return 'November';
            break;
        case 12:
            return 'Desember';
            break;
    }
}
?>
