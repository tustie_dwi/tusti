<?php 
$this->view("header.php");

?>    
<script type="text/javascript">    
	function reFresh() {
	  location.reload(true).fadeIn(200);
	}			
	window.setInterval("reFresh()",5*60000);
</script>   
	   <!-- Content -->
        <section id="wrap" class="pengumuman">
            <!-- Header -->
             <header id="header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-8 title-pengumuman">
                            PENGUMUMAN
                        </div>
                        <div class="col-sm-4 logo-kanan">
                            <img src="<?php echo $this->assets("v3/images/header.png") ?>" class="img-responsive img-center">
                        </div>
                    </div>
                </div>
            </header>
            <!-- End Header -->

            <!-- Main Content -->
            <section class="container-fluid">
                <div class="row">
                    <div class="col-sm-8">
                        
                            <ul class="pengumuman-slider">
                                    
								 <?php
									if($posts){
										$i = 0;
										foreach($posts as $dt):
											$i++;
										?>								
										<li>
											<div class="title">
												<span class="title-black">
												   <?php 
												 $bulan = chageToMonth(date("m", strtotime($dt->post_date)));
													 $tgl	= date("d", strtotime($dt->post_date));
													 $tahun	= date("Y", strtotime($dt->post_date));
													 $hari	= getHari(date("N", strtotime($dt->post_date)));
													echo $hari.", ".$tgl." ".$bulan." ".$tahun;
												 ?>
												</span>
											</div>
											<h2 class="judul"><?php 
											
											echo $dt->post_title; ?></h2>
											<div class="clearfix"></div>
											<div class="isi"><?php echo $dt->post_content; ?></div>
										</li>
										
										<?php
										endforeach;
									}
									?>
                                </ul>
								<div class="clearfix"></div>	
                    </div>	
									
                    <div class="col-sm-4">
                        <div class="content-right">
                            <div class="title">
                                <span class="title-white">
                                    @PTIIK_UB
                                </span>
                                <span class="pull-right logo-twitter"><span class="fa fa-twitter"></span></span>
                                <div class="recent-tweet-ptiik">
                                    <ul>
                                       
									<?php
											if(isset($tweet)){
												foreach($tweet as $dt):
												?>
													<li class="tweet">
													<?php echo $dt->keterangan;?>
													 <div class="time-tweet"><?php echo date("M d, Y H:i:s",strtotime($dt->created_at));?> </div>
													</li>
												<?php
												endforeach;
											}
											?>
									</ul>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Main Content -->

        </section>
        <!-- End Content -->
<?php 
$this->view("footer.php", $data);
?>

<?php
function chageToMonth($number=NULL) {
    switch ($number) {
        case 1:
            return 'Januari';
            break;
        case 2:
            return 'Februari';
            break;
        case 3:
            return 'Maret';
            break;
        case 4:
            return 'April';
            break;
        case 5:
            return 'Mei';
            break;
        case 6:
            return 'Juni';
            break;
        case 7:
            return 'Juli';
            break;
        case 8:
            return 'Agustus';
            break;
        case 9:
            return 'September';
            break;
        case 10:
            return 'Oktober';
            break;
        case 11:
            return 'November';
            break;
        case 12:
            return 'Desember';
            break;
    }
}
function getHari($number=NULL) {
    switch ($number) {
        case 1:
            return 'Senin';
            break;
        case 2:
            return 'Selasa';
            break;
        case 3:
            return 'Rabu';
            break;
        case 4:
            return 'Kamis';
            break;
        case 5:
            return 'Jumat';
            break;
        case 6:
            return 'Sabtu';
            break;
        case 7:
            return 'Minggu';
            break;
      
    }
}
?>