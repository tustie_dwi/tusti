/* 
 * Copyright 2014 rizky.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function firstStep() {
    $height = $('#footer').height();
    $('#wrap').animate({'marginTop': -$height}, 1000);
}

function information() {

    $sliderHeigt = $(window).height() - ($('header').height() + $('footer').height()) - 200;
    informationHeight($sliderHeigt);
    $(window).resize(function(){
    	$sliderHeigt = $(window).height() - ($('header').height() + $('footer').height()) - 200;
    	informationHeight($sliderHeigt);
    });
   
    var browsermozilla = /firefox/.test(navigator.userAgent.toLowerCase());
    if (browsermozilla == true) {

        $titleblackW = $('.title-black').outerWidth();
        $titlewhiteW = $('.title-white').outerWidth() + 18;// + 38;
        $('head').append('<style>.title-black:after{left:' + $titleblackW + 'px;top:11px!important}.title-white:after{left:' + $titlewhiteW + 'px;top:14px!important}</style>');

    } else {

        $titleblackW = $('.title-black').outerWidth() - 10;
        $titlewhiteW = $('.title-white').outerWidth()+ 13;//.width() + 36;
        $('head').append('<style>.title-black:after{left:' + $titleblackW + 'px}.title-white:after{left:' + $titlewhiteW + 'px}</style>');

    }

    $pengumuman = $('.pengumuman-slider').bxSlider({
        mode: 'horizontal',
//        useCSS: false,
//        easing: 'easeOutElastic',
        speed: 5000,
        pager: false,
        controls: false,
        autoStart: true,
        auto: true,
        pause: 15000,
        adaptiveHeight: true,
        slideMargin: 15
    });
}

function informationHeight($sliderHeigt){
	 $('.pengumuman-slider .isi').css({
        'max-height': $sliderHeigt
    });
    $('.recent-tweet-ptiik').css({
        'max-height': $sliderHeigt + 40,
        'min-height': $sliderHeigt + 40,
    });
}

function jadwalA() {
//     $widthSlider = $(window).width();
    $jadwal = $('.list-jadwal').bxSlider({
        mode: 'vertical',
        speed: 5000,
        pager: false,
        controls: false,
        autoStart: true,
        auto: true,
        pause: 10000,
        minSlides: 12,
        maxSlides: 12,
//        slideWidth: $widthSlider,
        slideMargin: 15,
        moveSlides: 5,
        adaptiveHeight: true,
    });
    var browsermozilla = /firefox/.test(navigator.userAgent.toLowerCase());
     if (browsermozilla == true) {

        $titleblackW = $('.title-black').outerWidth();
        $titlewhiteW = $('.title-white').outerWidth() + 18;// + 38;
        $('head').append('<style>.title-black:after{left:' + $titleblackW + 'px;top:11px!important}.title-white:after{left:' + $titlewhiteW + 'px;top:14px!important}</style>');

    } else {

        $titleblackW = $('.title-black').outerWidth() - 10;
        $titlewhiteW = $('.title-white').outerWidth()+ 13;//.width() + 36;
        $('head').append('<style>.title-black:after{left:' + $titleblackW + 'px}.title-white:after{left:' + $titlewhiteW + 'px}</style>');

    }

}

function daftarHadir() {
    $heightWindow = $(window).height() - $('footer').height() - $('header').height();
    $half = $heightWindow / 2;

    daftarHadirHeight($half);
    $(window).resize(function(){
    	
	    $heightWindow = $(window).height() - $('footer').height() - $('header').height();
	    $half = $heightWindow / 2;
	    daftarHadirHeight($half);
    });

    $widthSlider = $(window).width();

    $pengumuman = $('.daftar-hadir-slider').bxSlider({
        mode: 'horizontal',
        speed: 5000,
        pager: false,
        controls: false,
        autoStart: true,
        auto: true,
        pause: 2500,
        minSlides: 5,
        maxSlides: 5,
        slideWidth: $widthSlider,
        slideMargin: 15,
        moveSlides: 1,
        adaptiveHeight: true,
    });
}

function daftarHadirHeight($half){

    $('section#wrap .top-slider .div-image .img-daftarhadir').css({
        'max-height': $half - 95,
        'height': $half - 95,
    });

    $('section#wrap .daftar-hadir-slider .div-image .img-daftarhadir').css({
        'max-height': $half - 190,
        'height': $half - 190,
    });
}

function defaultStep() {
    setInterval(function() {
        var nDate = new Date();
        var day = nDate.getDate();
        var days = nDate.getDay();
        var month = nDate.getMonth();
        var year = nDate.getFullYear();
        var hours = nDate.getHours();
        var min = nDate.getMinutes();
        $(".clock-now").html((hours < 10 ? "0" : "") + hours + ":" + (min < 10 ? "0" : "") + min);

        $(".date-now").html(changeToDay(days) + ",<br>" + day + " " + chageToMonth(month) + " " + year);
    }, 1000);

    $.simpleWeather({
        woeid: '56000382',
        unit: 'c',
        success: function(weather) {
            html = '<img src="' + weather.image + '" style="width:80px" class="img-responsive img-center">';
            $(".weather").html(html);
        },
        error: function(error) {
            $(".weather").html("?");
        }
    });
}

function animate_up_down(sec) {
    $interval = sec * 1000;
    setInterval(function() {
        $height = $('#footer').height();
        $('#wrap').animate({'marginTop': 0}, 1000);
        $('#wrap').animate({'marginTop': -$height}, 1000);

    }, $interval);
}



function changeToDay(number) {
    switch (number) {
        case 1:
            return 'Senin';
            break;
        case 2:
            return 'Selasa';
            break;
        case 3:
            return 'Rabu';
            break;
        case 4:
            return 'Kamis';
            break;
        case 5:
            return 'Jumat';
            break;
        case 6:
            return 'Sabtu';
            break;
        case 7:
            return 'Minggu';
            break;
    }
}

function chageToMonth(number) {
    switch (number) {
        case 0:
        case 1:
            return 'Januari';
            break;
        case 2:
            return 'Februari';
            break;
        case 3:
            return 'Maret';
            break;
        case 4:
            return 'April';
            break;
        case 5:
            return 'Mei';
            break;
        case 6:
            return 'Juni';
            break;
        case 7:
            return 'Juli';
            break;
        case 8:
            return 'Agustus';
            break;
        case 9:
            return 'September';
            break;
        case 10:
            return 'Oktober';
            break;
        case 11:
            return 'November';
            break;
        case 12:
            return 'Desember';
            break;
    }
}