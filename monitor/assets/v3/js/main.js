function toggleCaption() {
    var caption = carouselContainer.find('.active').find('.carousel-caption');
    caption.slideToggle();
}

function setPaddingImageShow(width) {
    if ($('ul.image-show').width() > ((width - 199) / 2) + 199){
        $('ul.image-show').css("padding-left", (width - 199) / 2+"px");
    }else{
        $('ul.image-show').css("margin-left", -10+"px");
    }
}

$("#carousel-example-generic").carousel({
    interval: 10000
}).on('slide.bs.carousel', toggleCaption).trigger('slid');
var $heightBox1 = $('.box-1>.box-content').height();
var $heightBox1H2 = $('.box-1>.box-content h2').height();
$('.box-1>.box-content').css("padding-top", ($heightBox1 / 2) - $heightBox1H2+"px");
var $widthBox2 = $('.box-2').width();
var $widthULBox3 = $('ul.image-show').width();
$(window).resize(function() {
    $widthBox2 = $('.box-2').width();
    $widthULBox3 = $('ul.image-show').width();
    setPaddingImageShow($widthULBox3);
});

setPaddingImageShow($widthULBox3);
$('#slide-wrap').slideshow({
    width: $widthBox2,
    height: 175,
    delay: 5000,
    effect: 'random'
});

setInterval(function() {
    var nDate = new Date();
    var day = nDate.getDate();
    var days = nDate.getDay();
    var month = nDate.getMonth();
    var year = nDate.getFullYear();
    var hours = nDate.getHours();
    var min = nDate.getMinutes();
    $(".clock-now").html((hours < 10 ? "0" : "") + hours + ":" + (min < 10 ? "0" : "") + min);

    $(".date-now").html(changeToDay(days) + ", " + day + " " + chageToMonth(month) + " " + year);
}, 1000);

function changeToDay(number) {
    switch (number) {
        case 1:
            return 'Senin';
            break;
        case 2:
            return 'Selasa';
            break;
        case 3:
            return 'Rabu';
            break;
        case 4:
            return 'Kamis';
            break;
        case 5:
            return 'Jumat';
            break;
        case 6:
            return 'Sabtu';
            break;
        case 7:
            return 'Minggu';
            break;
    }
}

function chageToMonth(number) {
    switch (number) {
        case 0:
        case 1:
            return 'Januari';
            break;
        case 2:
            return 'Februari';
            break;
        case 3:
            return 'Maret';
            break;
        case 4:
            return 'April';
            break;
        case 5:
            return 'Mei';
            break;
        case 6:
            return 'Juni';
            break;
        case 7:
            return 'Juli';
            break;
        case 8:
            return 'Agustus';
            break;
        case 9:
            return 'September';
            break;
        case 10:
            return 'Oktober';
            break;
        case 11:
            return 'November';
            break;
        case 12:
            return 'Desember';
            break;
    }
}

$.simpleWeather({
    woeid: '56000382',
    unit: 'c',
    success: function(weather) {
        html = '<img src="' + weather.image + '" style="width:80px">';
        $("#weather").html(html);
    },
    error: function(error) {
        $("#weather").html("?");
    }
});