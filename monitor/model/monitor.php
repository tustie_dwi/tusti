<?php
class model_monitor extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_hadir($pin=NULL,$tgl=NULL){
		
	}
	
	function read_content($cat=NULL, $lang=NULL,$limit=NULL){
		$sql= "SELECT
					mid(md5(db_ptiik_coms.coms_content.content_id),9,7) AS id,
					db_ptiik_coms.coms_content.content_id,
					db_ptiik_coms.coms_content.content_category,
					db_ptiik_coms.coms_content.content_title as post_title,		
					db_ptiik_coms.coms_content.content AS post_content,					
					db_ptiik_coms.coms_content.content_excerpt as post_excerpt,
					db_ptiik_coms.coms_content.content_upload as post_date,
					db_ptiik_coms.coms_content.content_comment,
					db_ptiik_coms.coms_content.content_thumb_img AS thumb_img,
					db_ptiik_coms.coms_content.content_thumb_img AS icon,
					db_ptiik_coms.coms_content.is_sticky,
					db_ptiik_coms.coms_content.user_id,
					db_ptiik_coms.coms_content.last_update AS content_modified
				FROM  db_ptiik_coms.coms_content WHERE coms_content.content_category ='$cat' ";
		if($lang) $sql.= " AND coms_content.content_lang='$lang' ";
	
		$sql.=" ORDER BY db_ptiik_coms.coms_content.last_update DESC LIMIT 0,$limit ";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	 function berita() {
		$sqlx = "SELECT DISTINCT
				p.ID AS ID,
				p.post_date AS post_date,
				p.post_content AS post_content,
				p.post_title AS post_title,
				p.post_excerpt AS post_excerpt,
				p.guid AS guid,
				p.post_name
				FROM
				(((db_ptiik_news.wp_posts AS p
				JOIN db_ptiik_news.wp_term_relationships AS tr ON ((tr.object_id = p.ID)))
				JOIN db_ptiik_news.wp_terms AS t ON ((t.term_id = tr.term_taxonomy_id))))
				where ((`p`.`post_status` = 'publish') and (`p`.`post_type` = 'post') and (`t`.`name` in ('Kemahasiswaan','Akademik','Artikel'))  )
				order by `p`.`post_date` desc LIMIT 0,2
				";
		$sql= "SELECT
					mid(md5(db_ptiik_coms.coms_content.content_id),9,7) AS id,
					db_ptiik_coms.coms_content.content_id,
					db_ptiik_coms.coms_content.content_category,
					db_ptiik_coms.coms_content.menu_order,
					db_ptiik_coms.coms_content.unit_id,
					db_ptiik_coms.coms_content.fakultas_id,
					db_ptiik_coms.coms_content.cabang_id,
					db_ptiik_coms.coms_content.content_title as post_title,					
					db_ptiik_coms.coms_content.content_excerpt as post_excerpt,
					db_ptiik_coms.coms_content.content_upload as post_date,
					db_ptiik_coms.coms_content.content_comment,
					db_ptiik_coms.coms_content.content_thumb_img AS thumb_img,
					db_ptiik_coms.coms_content.content_thumb_img AS icon,
					db_ptiik_coms.coms_content.is_sticky,
					db_ptiik_coms.coms_content.user_id,
					db_ptiik_coms.coms_content.last_update AS content_modified
				FROM  db_ptiik_coms.coms_content WHERE content_category ='news' ORDER BY db_ptiik_coms.coms_content.last_update DESC ";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	 function find_feature($id) {
		$sql = "SELECT meta_value AS image FROM db_ptiik_news.wp_postmeta WHERE post_id = 
		(SELECT meta_value FROM db_ptiik_news.wp_postmeta WHERE meta_key ='_thumbnail_id' AND post_id = '".$id."') AND meta_key='_wp_attached_file'";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function read_slide(){
		$sql = "SELECT
					db_ptiik_coms.slide.slide_id,
					db_ptiik_coms.slide.slide_file,
					db_ptiik_coms.slide.slide_location,
					db_ptiik_coms.slide.slide_link,
					db_ptiik_coms.slide.slide_target,
					db_ptiik_coms.slide.slide_title,
					db_ptiik_coms.slide.is_active
					FROM
					db_ptiik_coms.slide
					WHERE
					db_ptiik_coms.slide.is_active = '1'
					LIMIT 0, 10";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_tweet($limit=NULL){
		$sql = "SELECT
					db_ptiik_coms.tbl_twitter.created_at,
					db_ptiik_coms.tbl_twitter.keterangan
					FROM
					db_ptiik_coms.tbl_twitter  ORDER BY created_at DESC LIMIT 0,".$limit."
					";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_cuaca($limit=NULL){
		$sql = "SELECT
					db_ptiik_coms.tbl_cuaca.main,
					db_ptiik_coms.tbl_cuaca.icon
					FROM
					db_ptiik_coms.tbl_cuaca LIMIT 0,1
					";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_quote(){
		$sql = "SELECT
					db_ptiik_coms.tbl_quote.keterangan,
					db_ptiik_coms.tbl_quote.by,
					db_ptiik_coms.tbl_quote.is_publish
					FROM
					db_ptiik_coms.tbl_quote WHERE is_publish='1' ORDER BY RAND() LIMIT 0,1
					";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
		
	/*master jadwal dosen */
	function get_jadwal_dosen($id){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`kode_mulai`,
				`vw_jadwaldosen`.`kode_selesai`,
				`vw_jadwaldosen`.`is_praktikum`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`,
				`vw_jadwaldosen`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					`vw_jadwaldosen`.`karyawan_id` = '".$id."' 
				";
		
		$sql = $sql . "ORDER BY `vw_jadwaldosen`.`ruang` ASC, `vw_jadwaldosen`.`jam_mulai` ASC, `vw_jadwaldosen`.`kelas_id` ASC";
		
		
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
	}
	
	function get_jadwal_by_ruang($id){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`kode_mulai`,
				`vw_jadwaldosen`.`kode_selesai`,	
				`vw_jadwaldosen`.`is_praktikum`,				
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`,
				`vw_jadwaldosen`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					`vw_jadwaldosen`.`ruang` = '".$id."' 
				";
		
		$sql = $sql . "ORDER BY `vw_jadwaldosen`.`ruang` ASC, `vw_jadwaldosen`.`jam_mulai` ASC, `vw_jadwaldosen`.`kelas_id` ASC";
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
	}
	
	function get_jadwal($id=NULL, $time=NULL, $lokasi=NULL){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`kode_mulai`,
				`vw_jadwaldosen`.`kode_selesai`,
				`vw_jadwaldosen`.`is_praktikum`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`,
				`vw_jadwaldosen`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					`vw_jadwaldosen`.`hari` = '".$id."' AND
					`vw_jadwaldosen`.`sesi_mulai` = '".$time."'
				";
		if($lokasi){
			$sql .= " AND left(`vw_jadwaldosen`.`ruang`,1) = '".strtoUpper($lokasi)."' ";
		}
		$sql = $sql . "ORDER BY  `vw_jadwaldosen`.`jam_mulai` ASC, `vw_jadwaldosen`.`ruang` ASC, `vw_jadwaldosen`.`kelas_id` ASC";
		
		
		$result = $this->db->query( $sql );
		
		
		return $result;
	}
	
	function get_jadwal_by_mk($id){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`kode_mulai`,
				`vw_jadwaldosen`.`kode_selesai`,
				`vw_jadwaldosen`.`is_praktikum`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`,
				`vw_jadwaldosen`.`prodi`
				FROM
				db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					`vw_jadwaldosen`.`mkditawarkan_id` = '".$id."' 
				";
		
		$sql = $sql . "ORDER BY `vw_jadwaldosen`.`jam_mulai` ASC";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	
	function get_jadwal_by_prak($id){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`kode_mulai`,
				`vw_jadwaldosen`.`kode_selesai`,
				`vw_jadwaldosen`.`is_praktikum`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`,
				`vw_jadwaldosen`.`prodi`
				FROM
				`vw_jadwaldosen`
				WHERE 
					`vw_jadwaldosen`.`mkditawarkan_id` = '".$id."' 
					AND `vw_jadwaldosen`.`is_praktikum`='1'
				";
		
		$sql = $sql . "ORDER BY `vw_jadwaldosen`.`jam_mulai` ASC";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_jadwal_by_prodi($id){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`kode_mulai`,
				`vw_jadwaldosen`.`kode_selesai`,
				`vw_jadwaldosen`.`is_praktikum`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`,
				`vw_jadwaldosen`.`prodi`
				FROM
				db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					`vw_jadwaldosen`.`prodi` = '".$id."' 
				";
		
		$sql = $sql . "ORDER BY `vw_jadwaldosen`.`jam_mulai` ASC";
		echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	
	/*master dosen */
	function get_dosen(){
		$sql= "SELECT karyawan_id as `id`, nama as `name`, nik as `nik` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE  is_dosen='1' AND is_aktif='aktif' 
				ORDER BY nama ASC";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/* master jam mulai*/
	function  get_jam_mulai(){
		$sql = "SELECT TIME_FORMAT(jam_mulai,'%H:%i') as `mulai`, TIME_FORMAT(jam_selesai,'%H:%i') as `selesai`, urut, is_istirahat FROM db_ptiik_apps.tbl_jam ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jam selesai*/
	function  get_jam_selesai(){
		$sql = "SELECT jam_mulai as `mulai`, jam_selesai as `selesai`, urut FROM db_ptiik_apps.tbl_jam  ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master hari*/
	function  get_hari(){
		$sql = "SELECT hari as `id`, hari as `value` FROM db_ptiik_apps.tbl_hari  ORDER BY tbl_hari.id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master ruang*/
	function  get_ruang(){
		$sql = "SELECT ruang_id as `id`, kode_ruang as `value` FROM db_ptiik_apps.tbl_ruang  WHERE is_aktif=1 ORDER BY ruang ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kelas*/
	function  get_kelas(){
		$sql = "SELECT kelas_id as `id`, kelas_id as `value` FROM db_ptiik_apps.tbl_kelas ORDER BY kelas_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master prodi*/
	function  get_prodi(){
		$sql = "SELECT prodi_id, kode_prodi  as `id`, keterangan as `value` FROM `db_ptiik_apps`.tbl_prodi ORDER BY singkat ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk ditawarkan */
	function get_mkditawarkan($ispraktikum){
		$sql="SELECT
				`tbl_mkditawarkan`.`mkditawarkan_id` as `id`,
				concat(`tbl_namamk`.`keterangan`,' (',`tbl_matakuliah`.`kode_mk`,')') as `value`			
				FROM
				db_ptiik_apps.`tbl_mkditawarkan`
				Inner Join db_ptiik_apps.`tbl_matakuliah` ON `tbl_mkditawarkan`.`matakuliah_id` = `tbl_matakuliah`.`matakuliah_id`
				Inner Join db_ptiik_apps.`tbl_namamk` ON `tbl_matakuliah`.`namamk_id` = `tbl_namamk`.`namamk_id`		
			  WHERE 1=1 
			";
			
		if($ispraktikum!=""){
			$sql = $sql . " AND `tbl_mkditawarkan`.`is_praktikum`= '".$ispraktikum."' ";
		}
		
		$sql = $sql . " ORDER BY `tbl_namamk`.`keterangan` ASC";
				
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* pembacaan informasi */
	function read_wall($id=NULL, $tgl=NULL,$page = NULL, $perpage = NULL) {
		if(! $page){
			$page = 1;
		}
		
		if(! $perpage){
			$perpage = 10;
		} 
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
					mid(md5(`tbl_wallinfo`.`wall_id`),5,5) as `id`,
					`tbl_wallinfo`.`wall_id`,
					`tbl_wallinfo`.`judul`,
					`tbl_wallinfo`.`keterangan`,
					`tbl_wallinfo`.`tgl_publish`,
					`tbl_wallinfo`.`is_publish`
				FROM
					`db_ptiik_apps`.`tbl_wallinfo`
				WHERE 1=1
				";
		if($id){
			$sql=$sql . " AND mid(md5(`tbl_wallinfo`.`wall_id`),5,5)='".$id."'";
		}
		if($tgl){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_wallinfo`.`tgl_publish` <= '".$tgl." '  AND is_publish='1' ";
		}
		$sql= $sql . "ORDER BY `tbl_wallinfo`.`tgl_publish` DESC LIMIT $offset, $perpage";
		
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function read_agenda($id=NULL, $tgl=NULL,$page = NULL, $perpage = NULL){
		if(! $page){
			$page = 1;
		}
		
		if(! $perpage){
			$perpage = 4;
		} 
		$offset 	= ($page-1)*$perpage;
		
		$nbulan = date('Y-m', strtotime($tgl));
		
		$in = date("Y-m-d", strtotime("-7 days", strtotime($tgl)));
		
		$out = date("Y-m-d", strtotime("+5 days", strtotime($tgl)));
		
					
		$sql = "SELECT DISTINCT
					mid(md5(`tbl_agenda`.agenda_id),5,5) as `id`,
					`tbl_agenda`.`agenda_id`,
					`tbl_agenda`.`lokasi`,
					`tbl_agenda`.`ruang`,
					`tbl_agenda`.`unit_id`,
					`tbl_agenda`.`inf_ruang`,
					`tbl_agenda`.`judul`,
					`tbl_agenda`.`inf_peserta`,
					`tbl_agenda`.`prodi_id`,
					`tbl_agenda`.`keterangan`,
					`tbl_agenda`.`inf_hari`,
					date_format(`tbl_agenda`.`tgl_mulai`, '%H:%i') as `from`,
					date_format(`tbl_agenda`.`tgl_selesai`, '%H:%i') as `to`,
					date_format(`tbl_agenda`.`tgl_mulai` ,'%Y-%m-%d') as `tmulai`, 
					date_format(`tbl_agenda`.`tgl_selesai`,'%Y-%m-%d') as `tselesai`, 
					`tbl_agenda`.`is_allday`,
					`tbl_agenda`.`is_publish`,
					`tbl_agenda`.`jenis_kegiatan_id`,
					`tbl_agenda`.penyelenggara,
					`tbl_agenda`.`user`,
					`tbl_agenda`.`last_update`,
					`tbl_jeniskegiatan`.`keterangan` as `jenis`,
					`db_ptiik_apps`.`tbl_unitkerja`.`nama` as `unit_penyelenggara`
				FROM
				`db_ptiik_apps`.`tbl_agenda`
				Left Join `db_ptiik_apps`.`tbl_jeniskegiatan` ON `db_ptiik_apps`.`tbl_agenda`.`jenis_kegiatan_id` = `db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`
				Left Join `db_ptiik_apps`.`tbl_unitkerja` ON `db_ptiik_apps`.`tbl_agenda`.`unit_id` = `db_ptiik_apps`.`tbl_unitkerja`.`unit_id`
				Inner Join `db_ptiik_apps`.`tbl_detailagenda` ON `db_ptiik_apps`.`tbl_agenda`.`agenda_id` = `db_ptiik_apps`.`tbl_detailagenda`.`agenda_id`
				WHERE `tbl_agenda`.`is_valid` = '1'  AND `tbl_agenda`.`is_publish` = '1'
				";
		if($id){
			$sql=$sql . " AND mid(md5(`tbl_agenda`.`agenda_id`),5,5)='".$id."'";
		}
		if($tgl){
			$sql=$sql . " AND ((date_format(`db_ptiik_apps`.`tbl_agenda`.`tgl_selesai`, '%Y-%m-%d') >= '".$out."') )";
			//$sql=$sql . " AND ((date_format(`db_ptiik_apps`.`tbl_agenda`.`tgl_mulai`, '%Y-%m-%d') > '".$in."') AND (date_format(`db_ptiik_apps`.`tbl_agenda`.`tgl_mulai`, '%Y-%m-%d') < '".$tgl."') )  ";
			//$sql=$sql . " AND ((date_format(`db_ptiik_apps`.`tbl_agenda`.`tgl_mulai`, '%Y-%m-%d') >= '".$tgl."') OR ('".$tgl."' BETWEEN `db_ptiik_apps`.`tbl_agenda`.`tgl_mulai` AND `db_ptiik_apps`.`tbl_agenda`.`tgl_selesai`) )  ";
		}
		$sql= $sql . "ORDER BY `tbl_agenda`.`tgl_mulai` ASC LIMIT $offset, $perpage";
		//echo $sql;
		
		$result = $this->db->query( $sql );
		
		if($result){
			return $result;	
		}else{
			$this->read_agenda_before($id, $tgl,$page, $perpage);
		}				
		
	}
	
	function read_agenda_before($id=NULL, $tgl=NULL,$page = NULL, $perpage = NULL){
		$in = date("Y-m-d", strtotime("-7 days", strtotime($tgl)));
		
		$this->read_agenda($id, $in,$page, $perpage);
	}
	
	/* master jabatan */
	function  get_absen($id){
		$sqlx = "SELECT
				`vw_absen`.`jabatan`,
				`vw_absen`.`nama`,
				`vw_absen`.`gelar_awal`,
				`vw_absen`.`gelar_akhir`,
				date_format(`vw_absen`.`tgl_absen`,'%Y-%m-%d') as 'tgl_absen',
				`vw_absen`.`jam_absen`,
				`vw_absen`.`foto`,
				`vw_absen`.`is_absen`
				FROM
				db_ptiik_apps.`vw_absen`				
				";
		$sql="SELECT DISTINCT 
				tbl_karyawan.karyawan_id,
				tbl_karyawan.nama,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir,
				tbl_master_jabatan.keterangan as jabatan,
				tbl_karyawan.foto,
				tbl_karyawan.pin,
				(SELECT is_absen FROM  db_ptiik_apps.tbl_absen_finger WHERE tbl_absen_finger.karyawan_id=tbl_karyawan.karyawan_id AND date_format(tgl_absen,'%Y-%m-%d') ='".date("Y-m-d")."' AND tbl_absen_finger.is_aktif=1) as `is_absen`
				FROM
				db_ptiik_apps.tbl_karyawan_kenaikan
				INNER JOIN db_ptiik_apps.tbl_master_jabatan ON tbl_karyawan_kenaikan.jabatan_id = tbl_master_jabatan.jabatan_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan_kenaikan.karyawan_id = tbl_karyawan.karyawan_id ORDER BY tbl_master_jabatan.jabatan_id ASC, pin DESC
				LIMIT 4,100
				";
						
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function  get_absen_pimpinan($id){
		/*$sql = "SELECT
				`vw_absenpimpinan`.`jabatan`,
				`vw_absenpimpinan`.`nama`,
				`vw_absenpimpinan`.`gelar_awal`,
				`vw_absenpimpinan`.`gelar_akhir`,
				date_format(`vw_absenpimpinan`.`tgl_absen`,'%Y-%m-%d') as 'tgl_absen',
				`vw_absenpimpinan`.`jam_absen`,
				`vw_absenpimpinan`.`foto`,
				`vw_absenpimpinan`.`is_absen`
				FROM
				db_ptiik_apps.`vw_absenpimpinan`
				";
		*/
		$sql="SELECT
				tbl_karyawan.karyawan_id,
				tbl_karyawan.nama,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir,
				tbl_master_jabatan.keterangan as jabatan,
				tbl_karyawan.foto,
				tbl_karyawan.pin,
				(SELECT is_absen FROM  db_ptiik_apps.tbl_absen_finger WHERE tbl_absen_finger.karyawan_id=tbl_karyawan.karyawan_id AND date_format(tbl_absen_finger.tgl_absen,'%Y-%m-%d') ='".date("Y-m-d")."' AND tbl_absen_finger.is_aktif=1) as `is_absen`
				FROM
				db_ptiik_apps.tbl_karyawan_kenaikan
				INNER JOIN db_ptiik_apps.tbl_master_jabatan ON tbl_karyawan_kenaikan.jabatan_id = tbl_master_jabatan.jabatan_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan_kenaikan.karyawan_id = tbl_karyawan.karyawan_id
				LIMIT 0,4
				";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
}