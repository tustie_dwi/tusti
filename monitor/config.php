<?php defined('PANADA') or die('Can\'t access directly!');
/**
 * EN: Panada Lite installation configuration
 */

$CONFIG['assets_folder'] = 'assets';

/**
 * Environment and error reporting
 * valid values:
 *    development, production
 */
$CONFIG['environment'] = 'production';

/**
 * Set this value to empty string instead of 'index.php' 
 * if you wish an url without "index.php" .
 * but don't forget to configure the .htaccess file if you change this
 */ 
$CONFIG['index_file']                       = ''; 

/**
 * Database configuration - master
 */
$CONFIG['db']['default']['driver']          = 'mysqli';
$CONFIG['db']['default']['host']            = '175.45.187.252'; 
$CONFIG['db']['default']['user']            = 'pt11k_h0st'; 
$CONFIG['db']['default']['password']        = 'pt11k.ub^2014_h0st'; 
$CONFIG['db']['default']['database']        = 'db_ptiik_apps';
$CONFIG['db']['default']['charset']         = 'utf8';
$CONFIG['db']['default']['collate']         = 'utf8_general_ci';
$CONFIG['db']['default']['persistent']      = false;

/**
 * regular expression in filtering method and controller names
 * comment out the following filter_regex configuration if you don't prefer filtering out 
 * controller and method name paresed from URI string
 */
$CONFIG['filter_regex'] = "/[^a-z0-9_]/i"; 

/** 
 * set the default controller and method if not set from the URL
 */
$CONFIG['default_controller'] = 'info';
$CONFIG['default_method'] = 'index';
