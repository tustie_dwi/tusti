<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil"></i> Create Privileges</div>
		<div class="panel-body">         
			<form method=post  action="<?php echo $this->location('hakakses'); ?>">
				
					<div class="form-group">
						<label>Role User</label>
					
							<select name="cmbrole" class="cmbmulti form-control">
								<option value="0">None</option>
								<?php
								foreach($role as $dt):
									echo "<option value='".$dt->id."' >".$dt->value."</option>";
								endforeach;
								?>
							</select>
					</div>
						<div class="form-group">
						<label>Menu</label>
					
							<select name="cmbmenu[]" class="cmbmulti form-control" multiple>
								<option value="0">All Menu</option>
								<?php
								foreach($menu as $dt):
									echo "<option value='".$dt->id."' >".$dt->judul."</option>";
								endforeach;
								?>
							</select>
							
					</div>				
				
				<div class="form-group">
				   <input type="submit" name="b_hakakses" value="Create Hak Akses" class="btn btn-primary btn-create-user" />
				</div>
			</form>
		</div>
	</div>
</div>
	