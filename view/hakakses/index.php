<?php $this->head(); ?>
<div class="row">
	<div class="col-md-8">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('home'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('hakakses'); ?>">Privileges</a></li>
		  <li class="active"><a href="#">Data</a></li>
		</ol>
			
			<div class="block-box">
			  <div class="header"><h4>Privileges</h4></div>
				  <div class="content">
	
					<?php if( isset($posts) ) : ?>
					
					<table class="table table-striped table-hover" id="example">
						<thead>
							<th>Role</th>
							<th>Menu</th>
						</thead>
						
					<?php 
							
					foreach($posts as $dt) { 
					?>
					
						<tr>
							<td><?php echo $dt->role; ?> </td>
							<td><ul class="list-inline"><?php
							
							if($pmenu){
								foreach($pmenu as $row){
									if($dt->id==$row->level){
										echo "<li class='label label-info' style='font-weight:normal'>".$row->judul."</li> ";
									}
								}
							}
							?></ul></td>				
						</tr>
						<?php	
						}
					?>
					</table>
					
					<?php else: ?>		
							<div class="well">Sorry, no menu to show. There are nothing in menu directory.</div>		
					<?php endif; ?>					
						</div>
					</div>
				</div>
					<div class="col-md-4">
						
						<?php $this->view('hakakses/edit.php', $data); ?>			
					</div>
				</div>


<?php $this->foot(); ?>