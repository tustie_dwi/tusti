
		<script src="<?php echo $this->asset("chart/chart/jquery-1.10.2.min.js"); ?>" ></script>
		<script src="<?php echo $this->asset("chart/chart/knockout-3.0.0.js"); ?>" ></script>
		<script src="<?php echo $this->asset("chart/chart/globalize.min.js"); ?>" ></script>
		<script src="<?php echo $this->asset("chart/chart/dx.chartjs.js"); ?>" ></script>   
		
		<script>
			$(function ()  
				{
					//alert($('#hidtgl').val());
					
					var a = $('#hidtgl').val(); 
					var b = $('#cmbkategori').val(); 
					
				   $.getJSON( base_url +'info/ruang_json/'+a+'/'+b, function(dataSource) {
					$('#chartContainer').dxChart({
						dataSource: dataSource,
						 commonSeriesSettings: {
							type: "line",
							argumentField: "ruang",
							valueField: 'kegiatan'
						},
							
						commonAxisSettings: {
							grid: {
								visible: true
							}
						},
						series: [{ valueField: 'kegiatan', name: "Total Jam Pemakaian Ruang", 
						selectionStyle: {
							color: "red",
							hatching: "none"
						}  },],
						tooltip:{
							enabled: true,
							customizeText: function () {
							return this.argumentText + " - " + this.valueText +" jam";
							}
						},
						title: 'Utilisasi Ruang',
						
						valueAxis: {
							label: {
								customizeText: function() {
									return this.value;
								}
								//visible: true
							}
						},
						legend: {
							 verticalAlignment: "bottom",
							horizontalAlignment: "center"
						},
						commonPaneSettings: {
							border:{
								visible: true,
								bottom: false
							}       
						}
					})
				});
				}

			);
		</script>
	<div id="chartContainer" style="width: 100%; height: 440px;"></div>
	
