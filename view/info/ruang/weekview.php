	<div class="table-responsive">	
	<?php 

		 $month = $month;    
         $year  = $year; 
	
	?>
	<h3><?php echo date("F d, Y", strtotime($hidtgl)); ?></h3>
	<form class="form-inline" method=post action="<?php echo $this->location('info/ruang/terpakai')?>" role="form">
		<div class="form-group">
			<select class="form-control" name="cmbkategori" onChange='form.submit();'>
				<?php
				echo "<option value=''>Semua Kategori</option>";
				foreach($kategoriruang as $dt):
					echo "<option value='".$dt->id."' ";
						if($kategori==$dt->id){ echo "selected"; }
					echo ">".ucWords($dt->value)."</option>";
				endforeach;
				?>
			</select>
		</div>
		<div class="form-group">
			<select class="form-control" name="cmbruang" onChange='form.submit();'>
				<option value="">Semua Ruang</option>
				<?php
				foreach($ruangan as $dt):
					echo "<option value='".$dt->id."' ";
						if($ruangid==$dt->id){ echo "selected"; }
					echo ">".$dt->value." </option>";
				endforeach;
				?>
			</select>	
		</div>	
		<div class="form-group">
			<input type="submit" name="b_prev" value="<< Prev" class="btn">
			<input type="submit" name="b_now" value="Today" class="btn">
			<input type="submit" name="b_next" value="Next >>" class="btn">
			<input type=hidden name="hidtgl" value="<?php echo $hidtgl; ?>">
		</div>
	</form><br>

	<?php
	if( isset($weekdata) ) :	
		echo $weekdata;
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
	</div>
