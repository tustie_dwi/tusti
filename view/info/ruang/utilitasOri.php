<?php
  $rows = array();
  $table = array();
  $table['cols'] = array(

    array('label' => 'Ruang', 'type' => 'string'),
    array('label' => 'Total kegiatan', 'type' => 'number')

);
    /* Extract the information from $result */
	if($utilitas){
		foreach($utilitas as $r) {

		  $temp = array();
		  $temp[] = array('v' => (string) $r->ruang); 
		  $temp[] = array('v' => (int) $r->kegiatan); 
		  $rows[] = array('c' => $temp);
		}
	}

	$table['rows'] = $rows;
	$jsonTable = json_encode($table);
?>

    <!--Load the Ajax API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
   <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
    <script type="text/javascript">

    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(<?=$jsonTable?>);
      var options = {
           title: 'Index kegiatan',
          is3D: 'true'
        };
      // Instantiate and draw our chart, passing in some options.
      // Do not forget to check your div ID
      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
    </script>

  <!--this is the div that will hold the pie chart-->
    <div id="chart_div"></div>
