
<?php 
$this->view("header-v3.php"); 
//$this->view("navbar-info.php"); 
$header			= "Ketersediaan Ruang";	


?>
<section id="wrap" class="mini-side-open ruang-tersedia">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
	<legend><?php echo $header; ?></legend>
		<div class="col-md-12">
		<form name="frmDosen" id="frmDosen" class="form-inline" method=post action="<?php echo $this->location('info/ruang/tersedia')?>" role="form">			
			<div class="form-group">			
				<input type="text" name="tgl" class="date date-time-picker form-control" placeholder='Tgl..' value="<?php echo $tgl; ?>" required data-format="YYYY-MM-DD" id="tgl">
			</div>
			<div class="form-group">				
				<select class="form-control" name="cmbkategori" onChange='form.submit();' id="cmbkategori">
					<?php
					//echo "<option value=''>Semua Kategori</option>";
					foreach($kategoriruang as $dt):
						echo "<option value='".$dt->id."' ";
							if($kategori==$dt->id){ echo "selected"; }
						echo ">".ucWords($dt->value)."</option>";
					endforeach;
					?>
				</select>
			</div>
			<div class="form-group">				
				<select class="form-control" name="cmbruang" onChange='form.submit();' id="cmbruang">
					<option value="">Semua Ruang</option>
					<?php
					foreach($ruangan as $dt):
						echo "<option value='".$dt->id."' ";
							if($ruangid==$dt->id){ echo "selected"; }
						echo ">".$dt->value." </option>";
					endforeach;
					?>
				</select>	
			</div>
			<div class="form-group">
				<button type="submit" name="b_cek" class="btn btn-info btn-no-radius"><i class="fa fa-search"></i> View </button>
			</div>
		</form>	
		</div>
		<div class="col-md-12">
			<div class="form-reservasi"></div>
			<div class="table-responsive view-tersedia">		
				<span class="label label-success pull-center">&nbsp;&nbsp;v&nbsp;&nbsp;</span>&nbsp;<em><small>Finished, kegiatan sudah selesai</small></em>&nbsp;
				 <span class="label label-important pull-center">&nbsp;&nbsp;x&nbsp;&nbsp;</span>&nbsp;<em><small>Reserved, kegiatan masih terlaksana atau akan terlaksana</small></em>
				 <span class="label label-warning pull-center">&nbsp;&nbsp;v&nbsp;&nbsp;</span>&nbsp;<em><small>Finished, menunggu validasi admin</small></em>&nbsp;
				 <span class="label label-danger pull-center">&nbsp;&nbsp;x&nbsp;&nbsp;</span>&nbsp;<em><small>Reserved, menunggu validasi admin</small></em>&nbsp;
				 &nbsp;<small>* <b>blank</b></small>&nbsp;<em><small>Available</small></em>
			 
				<?php			
					echo $this->view("info/ruang/view_tersedia.php", $data); 
				 ?>			
			</div>	
		</div>		
	</div>
</div>
</section>
</section>
	<?php
$this->view("footer-v3.php"); 

?>
