<?php $this->view("header-v3.php"); 
//$this->view("navbar-info.php"); ?>
<SCRIPT language="javascript">
function OpenPage(page) {
	if (page != "-1") {
		window.open(page, '_self');
	}
}
</SCRIPT>
<section id="wrap" class="mini-side-open">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
    
		<legend>Pemakaian Ruang</legend>
	
		<div class="control-group">
			<?php if((isset($_POST['month']))||(isset($_POST['year']))){ ?>
				<ul class="nav nav-pills nav-pills-color" id="writeTab">
					<li><a href="#list" data-toggle="tab" class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Harian</span></a></li>
					<li><a href="#week" data-toggle="tab" class="pills-blue"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Mingguan</span></a></li>
					<li class="active"><a href="#grid" data-toggle="tab" class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Bulanan</span></a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane fade" id="list">	<?php $this->view('info/ruang/listview.php', $data); ?>	</div>
					<div class="tab-pane fade" id="week"><?php  $this->view('info/ruang/weekview.php', $data); ?></div>
					<div class="tab-pane active fade in" id="grid"><?php  $this->view('info/ruang/gridview.php', $data); ?></div>
				</div>
			<?php } else { 
				if	((isset($_POST['b_next']))||(isset($_POST['b_prev'])) ||(isset($_POST['b_now'])) ){		?>
					<ul class="nav nav-pills nav-pills-color" id="writeTab">
						<li><a href="#list" data-toggle="tab" class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Harian</span></a></li>
						<li class="active"><a href="#week" data-toggle="tab" class="pills-blue"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Mingguan</span></a></li>
						<li><a href="#grid" data-toggle="tab" class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Bulanan</span></a></li>
					</ul>
					<div class="tab-content">			
						<div class="tab-pane fade" id="list">	<?php $this->view('info/ruang/listview.php', $data); ?>	</div>
						<div class="tab-pane fade in active" id="week"><?php  $this->view('info/ruang/weekview.php', $data); ?></div>
						<div class="tab-pane fade" id="grid"><?php  $this->view('info/ruang/gridview.php', $data); ?></div>
					</div>
			<?php }else{ ?>
					<ul class="nav nav-pills nav-pills-color" id="writeTab">
						<li class="active"><a href="#list" data-toggle="tab" class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Harian</span></a></li>
						<li><a href="#week" data-toggle="tab" class="pills-blue"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Mingguan</span></a></li>
						<li><a href="#grid" data-toggle="tab" class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text">Bulanan</span></a></li>
					</ul>
					<div class="tab-content">			
						<div class="tab-pane fade in active" id="list">	<?php $this->view('info/ruang/listview.php', $data); ?>	</div>
						<div class="tab-pane fade" id="week"><?php  $this->view('info/ruang/weekview.php', $data); ?></div>
						<div class="tab-pane fade" id="grid"><?php  $this->view('info/ruang/gridview.php', $data); ?></div>
					</div>

			<?php }
			} ?>
		</div>
	</div>
</div>
</div>
</section>
</section>
<?php $this->view("footer-v3.php"); ?>