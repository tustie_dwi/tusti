<table cellpadding="0" cellspacing="0" class="table table-bordered">
<?php

		$minfo = new model_ruanginfo();
		$blokwaktu	= $minfo->get_blok_waktu();

		?>
		<thead><tr class="calendar-row"><td class="calendar-day-head"><small>J/R</small></td>
		<?php
			foreach($blokwaktu as $dt):
				echo  '<td class="calendar-day-head"><small>'.$dt->jam_mulai.'</small></td>';
			endforeach;
			?>
		</tr></thead><tbody>
		<?php
		if($tgl){
			$ruang = $minfo->get_ruang($kategori, $ruangid);
			
			date_default_timezone_set('Asia/Jakarta');
			$jam=date("H:i", strtotime("now"));
			$tsekarang=date('Y-m-d');
			
			$waktu = str_replace(":", "",$jam);
			
		
			$no = 0;		
			foreach($ruang as $dt):
				$no++;
				echo  '<tr >';
					echo  '<td class="ruang"><small><b>R. '.$dt->id.'</b></small></td>';
					foreach($blokwaktu as $data):
						$cek 	= $minfo->cek_ketersediaan($tgl, $dt->id, $data->jam_mulai);
						$popid 	= str_replace(".","",$dt->id).substr($data->jam_mulai,0,2);
						
						$tglo=str_replace("-", "",$tgl);
						
						
						switch($tglo){
							case ($tglo==str_replace("-", "",$tsekarang) && ($waktu<str_replace(":","",$data->jam_mulai))):
								$strclass= "label-important ";
								$tdclass = "popup-form-resv-disabled";
								$strtitle= "Reserved";
								$stricon = "x";
								
							break;
							case ($tglo==str_replace("-", "",$tsekarang) && ($waktu>str_replace(":","",$data->jam_mulai))):
								$strclass= "label-success popup-form-resv";
								$tdclass = "popup-form-resv";
								$strtitle= "Finished";
								$stricon = "v";
							break;
							case ($tglo<str_replace("-", "",$tsekarang)):
								$strclass= "label-success popup-form-resv";
								$tdclass = "popup-form-resv";
								$strtitle= "Finished";
								$stricon = "v";
							break;
							case ($tglo>str_replace("-", "",$tsekarang)):
								$strclass= "label-important";
								$tdclass = "popup-form-resv-disabled";
								$strtitle= "Reserved";
								$stricon = "x";
							break;
							
						}
						
						if($cek){							
							?>
							<td class="<?php echo $tdclass; ?>" data-id="<?php echo $dt->id ?>" data-mulai="<?php echo $data->jam_mulai ?>" >
							<?php												
							$content = "";
							$i=0;
							$strapprove = 0;
							foreach($cek as $key):
								$i++;
								/*$content.= '<small><b>'.$cek->kegiatan.'</b>';
								if($cek->catatan){
									$content.= '<br><em>'. $cek->catatan.'</em><br>';
								}											
								$content.= 'R. '.$dt->id. ' ('.$cek->jam_mulai.' - '.$cek->jam_selesai.')</small><br>';*/
								$content.= $key->kegiatan. " - ".'R. '.$dt->id. ' ('.$key->jam_mulai.' - '.$key->jam_selesai.')';
								$strapprove = $strapprove + $key->is_approve;
							endforeach;		

							if ($strapprove	=='0') 
								if($stricon=='x')
									$strclassok = "label-danger";
								else
									$strclassok = "label-warning";
							else $strclassok = $strclass;
							
							echo  '<a href="#" data-toggle="tooltip" data-placement="left" title="'.$content.'"><span class="label '.$strclassok.' pull-center">&nbsp;&nbsp;'.$stricon.'&nbsp;&nbsp;</span></a>';
							
							echo "</td>";			
						}else{
							?>
							<td class="popup-form-resv" data-id="<?php echo $dt->id ?>" data-mulai="<?php echo $data->jam_mulai ?>" >
							
							</td>
						<?php } ?>
						
						
						<?php
					endforeach;
					?>
				</tr>
				<?php
			endforeach;
			
		}
		?>
		</tbody></table>

