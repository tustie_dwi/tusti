<div class="table-responsive">	
	<?php 
		 $month = $month;    
         $year  = $year; 	 		
		
	?>
	<h3><?php echo $title; ?></h3>
	<form class="form-inline" method=post action="<?php echo $this->location('info/ruang/terpakai')?>"role="form">
		<div class="form-group">
			<select class="form-control" name="month" onChange='form.submit();' class='input-small'>
			<?php
				for($x = 1; $x <= 12; $x++)
				{
				
				echo "<option value='".$x."' ";
					if($month==$x){
						echo "selected";
					}
					
				echo ">".date('F',mktime(0,0,0,$x,1,$year))."</option>";
				 
				}

			?>
			</select>
		</div>
		<div class="form-group">
			<?php
			 $year_range = 10;
			 $selectYear = '<select class="form-control" name="year" onChange="form.submit();" class="input-small">';
			 for($x = ($year-floor($year_range/2)); $x <= ($year+floor($year_range/2)); $x++)
			 {
				 $selectYear.= '<option value="'.$x.'"'.($x != $year ? '' : ' selected="selected"').'>'.$x.'</option>';
			 }
			 $selectYear.= '</select>';
			 
			 echo $selectYear;
			?>
		</div>
		<div class="form-group">
			<select class="form-control" name="cmbkategori" onChange='form.submit();'>
				<?php
				echo "<option value=''>Semua Kategori</option>";
				foreach($kategoriruang as $dt):
					echo "<option value='".$dt->id."' ";
						if($kategori==$dt->id){ echo "selected"; }
					echo ">".ucWords($dt->value)."</option>";
				endforeach;
				?>
			</select>
		</div>
		<div class="form-group">
			<select class="form-control" name="cmbruang" onChange='form.submit();'>
				<option value="">Semua Ruang</option>
				<?php
				foreach($ruangan as $dt):
					echo "<option value='".$dt->id."' ";
						if($ruangid==$dt->id){ echo "selected"; }
					echo ">".$dt->value." </option>";
				endforeach;
				?>
			</select>	
		</div>
		<!--<select name="cmbkegiatan" onChange='form.submit();' class='input-large'>
			<option value="">Semua Jenis Kegiatan</option>
			<?php
				/*foreach($kegiatan as $dt):
					echo "<option value='".$dt->jenis_kegiatan_id."' ";
					if($jeniskegiatan==$dt->jenis_kegiatan_id){
						echo "selected ";
					}
					echo  ">".$dt->keterangan."</option>";
				endforeach;*/
			?>
		</select>-->
	</form><br>
	
		<?php
		if(isset($viewlist)) :	
			echo $viewlist;
		 else: 
		 ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php endif; ?>
	</div>
