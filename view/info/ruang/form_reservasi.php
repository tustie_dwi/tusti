<?php
session_start();
$_SESSION['count'] = time();
$image;
?>
	<div class="clearfix"></div>
	<fieldset>
		<h5>&nbsp;</h5>
	<div class="row">
		
			<?php switch ($msg) {
				case 'success':
					echo '<div class="alert alert-success fade-out">
						      <strong>Oke,</strong> Anda telah berhasil melakukan pemesanan ruangan, admin akan melakukan validasi data :)
						    </div>';
					break;
					
				case 'err':
					echo '<div class="alert alert-danger fade-out">
						      <strong>Oops,</strong> proses pemesanan ruangan gagal, pastikan data yang Anda masukkan benar :(
						    </div>';
					break;
				
			} ?>
			
			<form role="form" method="post" id="form-save-resv">
			  <div class="form-group">
				<?php create_image(); ?>
				<div style="display:block;margin-bottom:10px;margin-top:10px;">
					<img src="<?php echo $this->asset("uploads/captcha/image".$_SESSION['count'].".png") ?>">					
				</div>
				<input type="text" name="input" id="input-kode" class="form-control" placeholder="Masukkan Text Yang Tertera Pada Gambar Diatas" required >
				<input type="hidden" name="flag" value="<?php echo md5($_SESSION['captcha_string']); ?>" id="flag" />
				<div class="col-md-12">&nbsp;</div>
			    <div class="input-group">
		          <span class="input-group-btn">
		          	<button type="submit" class="btn btn-warning" onClick="return checkEnterResv(event);"><i class="fa fa-check"></i> Validasi</button>
		          </span>
		          <input type="text" name="id" id="input-id" class="form-control" placeholder="Masukkan Data Identitas Anda" onkeypress="return checkEnterResv(event);" required>
				  <input type="hidden" id="input-ruang">
				  <input type="hidden" id="input-mulai">
		        </div>
		       
			    <span class="help-block">
			    	Masukkan NIM jika Anda mahasiswa, masukkan email jika Anda dosen/karyawan.
			    </span>
			  </div>
			  
			</form>
		
	</div>
	</fieldset>
	<?php
	function create_image()
		{
		global $image;
		$image = imagecreatetruecolor(200, 50) or die("Cannot Initialize new GD image stream");
		 
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 0, 255, 255);
		$line_color = imagecolorallocate($image, 0, 0, 150);
		$pixel_color = imagecolorallocate($image, 0, 0, 255);
		 
		imagefilledrectangle($image, 0, 0, 200, 50, $background_color);
		 
		for ($i = 0; $i < 3; $i++) {
		imageline($image, 0, rand() % 50, 200, rand() % 50, $line_color);
		}
		 
		for ($i = 0; $i < 450; $i++) {
		imagesetpixel($image, rand() % 200, rand() % 50, $pixel_color);
		}
		 
		 
		$letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$len = strlen($letters);
		$letter = $letters[rand(0, $len - 1)];
		 
		$text_color = imagecolorallocate($image, 0, 0, 0);
		$word = "";
		for ($i = 0; $i < 6; $i++) {
		$letter = $letters[rand(0, $len - 1)];
		imagestring($image, 7, 5 + ($i * 30), 20, $letter, $text_color);
		$word .= $letter;
		}
		$_SESSION['captcha_string'] = $word;
		 
		$images = glob("assets/uploads/captcha/*.png");
		foreach ($images as $image_to_delete) {
		@unlink($image_to_delete);
		}
		imagepng($image, "assets/uploads/captcha/image" . $_SESSION['count'] . ".png");
		 
		}
	?>