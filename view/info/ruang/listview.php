	<?php
    
	/*if($tgl){
		echo "<h3>Tanggal : ".date('F d, Y', strtotime($tgl))."</h3>";
	}
	
	?>
	
	<form class="form-inline" method=post action="<?php echo $this->location('info/ruang/terpakai')?>" ole="form">
		<div class="form-group">			
			<input type="text" name="tgl" class="date date-time-picker form-control" placeholder='Tgl..' value="<?php echo $tgl; ?>" required data-format="YYYY-MM-DD">
		</div>
		<div class="form-group">	
			<select class="form-control" name="cmbkategori" onChange='form.submit();'>
				<?php
				echo "<option value=''>Semua Kategori</option>";
				foreach($kategoriruang as $dt):
					echo "<option value='".$dt->id."' ";
						if($kategori==$dt->id){ echo "selected"; }
					echo ">".ucWords($dt->value)."</option>";
				endforeach;
				?>
			</select>
		</div>
		<div class="form-group">
			<select class="form-control" name="cmbruang" onChange='form.submit();'>
				<option value="">Semua Ruang</option>
				<?php
				foreach($ruangan as $dt):
					echo "<option value='".$dt->id."' ";
						if($ruangid==$dt->id){ echo "selected"; }
					echo ">".$dt->value." </option>";
				endforeach;
				?>
			</select>	
		</div>
			<!--<select name="cmbkegiatan" onChange='form.submit();'>
			<option value="">Semua Jenis Kegiatan</option>
			<?php
				/*foreach($kegiatan as $dt):
					echo "<option value='".$dt->jenis_kegiatan_id."' ";
					if($jeniskegiatan==$dt->jenis_kegiatan_id){
						echo "selected ";
					}
					echo  ">".$dt->keterangan."</option>";
				endforeach;*/
			?>
		<!--</select>-->
		<!--<div class="form-group">
			<button type="submit" name="b_cek" class="btn btn-no-radius"> View </button>
		</div>
	</form><br>
	-->
	<div class="table-responsive">	
	<?php
		
	if( isset($posts) ) :		 
		$str="<table class='table table-striped table-hover' id='example'>
					<thead>
						<tr>
							<th>Ruang</th>
							<th>Jam</th>
							<th>Kegiatan</th>	
						</tr>
					</thead>
					<tbody>";		
				$i = 1;			
				foreach ($posts as $dt): 
					$str.="
							<tr id='post-".$dt->detail_id."' data-id='".$dt->detail_id."' valign=top valign=top>
								<td><span class='text text-info'>R. ".$dt->ruang_id."</span><br><code>".$dt->kategori_ruang."</code></td>
								<td>".$dt->from." - ".$dt->to."</td>
								";		
								
						$str.= "<td>".$dt->kegiatan;
								if(($dt->jenis_kegiatan_id=='kuliah')||($dt->jenis_kegiatan_id=='praktikum')||($dt->jenis_kegiatan_id=='bimbingan')){
									$str.= '<br><small><em>'.$dt->catatan.'</em></small>';
								}
								
						$str.= "</td></tr>";
				endforeach; 
				
			$str.= "</tbody></table>";
			
			echo $str;
		 else: 
		 ?>
		<div class="alert">Sorry, no content to show</div>
		<?php endif; ?>
	</div>