	<div class="clearfix"></div>
	<fieldset>
		<h5>&nbsp;</h5>
	<div class="row">
		
			<?php switch ($msg) {
				case 'success':
					echo '<div class="alert alert-success fade-out">
						      <strong>Oke,</strong> Anda telah berhasil melakukan pemesanan ruangan, admin akan melakukan validasi data :)
						    </div>';
					break;
					
				case 'err':
					echo '<div class="alert alert-danger fade-out">
						      <strong>Oops,</strong> proses pemesanan ruangan gagal, pastikan data yang Anda masukkan benar :(
						    </div>';
					break;
				
			} ?>
			
			<form role="form" method="post" id="form-save-resv">
			  <div class="form-group">
				
			    <div class="input-group">
		          <span class="input-group-btn">
		          	<button type="submit" class="btn btn-warning" onClick="return checkEnterResv(event);"><i class="fa fa-check"></i> Validasi</button>
		          </span>
		          <input type="text" name="id" id="input-id" class="form-control" placeholder="Masukkan Data Identitas Anda" onkeypress="return checkEnterResv(event);" required>
				  <input type="hidden" id="input-ruang">
				  <input type="hidden" id="input-mulai">
		        </div>
		       
			    <span class="help-block">
			    	Masukkan NIM jika peminjam mahasiswa, masukkan email jika peminjam dosen/karyawan.
			    </span>
			  </div>
			  
			</form>
		
	</div>
	</fieldset>
	