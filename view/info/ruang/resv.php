<?php 
	
	if($user->aktor == 'mahasiswa') {
		$user_id_title = 'NIM';
		$user_init = 'mhs';
	}
	else {
		$user_id_title = 'NIK';
		$user_init = $user->aktor;
	}
?>
<fieldset>
	<h5>&nbsp;</h5>
	<div class="row well">
		
			<form class="form-inline" onsubmit="return cek_ruang_choose()" role="form" method="post" action="<?php echo $this->location('booking/save')  ?>">
			
				<div class="form-group">
				    <label>Nama</label>
				    
				      <input type="text" class="form-control" readonly value="<?php echo $user->nama ?>" id="user_nama">
				   
				</div>
				<div class="form-group">
			    	<label><?php echo $user_id_title ?></label>
				      <input type="text" class="form-control" readonly value="<?php echo $user->user_id ?>">
				      <input type="hidden" name="user_id" value="<?php echo $user->id ?>" id="user_id">
				      <input type="hidden" name="user_init" value="<?php echo $user_init ?>" id="user_init">
						<input type="hidden" id="input-kegiatan" name="input-kegiatan">
					   <input type="hidden" id="input-ruang">
					   <input type="hidden" id="input-mulai">
				</div>				
				<div class="form-group">
				    <label>Tgl. Pelaksanan </label>
					<div class="row">
				    <div class="col-md-6">
				    	<input type='text' class="form-control" id="datetimepicker8" data-format="YYYY-MM-DD HH:mm" placeholder="Tanggal Mulai" required name="tgl_mulai" autocomplete="off" />
				    </div>
				    <div class="col-md-6">
				      <input type='text' class="form-control" id="datetimepicker9" data-format="YYYY-MM-DD HH:mm" placeholder="Tanggal Selesai" required name="tgl_selesai" autocomplete="off" />
				    </div>
					</div>
				</div>
				
				<div class="form-group">
				    <label>Ruang &nbsp; 
				    <h4 id="wrap-ruang"><a href="#" id="tambah-ruang" class="label label-warning"><i class="fa fa-plus"></i> Tambah Ruang</a> &nbsp; </h4>	</label>	   
				</div>				
				
				<div class="clearfix"></div>
				
				<div class="form-tambah-ruang">
					<div class="form-group">
						<label>Tambah Ruang </label>				  
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" onclick="addRuang(document.getElementById('sel-ruang').value)">Pilih Ruangan <i class="fa fa-check"></i></button>
							</span>
							<select class="form-control" id="sel-ruang">
								<?php 
									foreach($ruang as $key){
										echo "<option value='".$key->ruang.'|'.$key->ruang .' '.$key->keterangan."'>" . '' . $key->ruang . ' - '.$key->keterangan . ' - [' . $key->kapasitas . "] orang</option>";
									}
								?> 
							</select>
						</div>				  
				</div></div>
				
				
				<div class="form-group">
				    <label>Jenis Kegiatan</label>
				   
				      <select class="form-control" name="jenis_kegiatan_id" id="cmbkegiatan">
						<option value="-">Please Select</option>
				      	<?php 
					      	foreach($kegiatan as $key) {
					      		echo "<option value='".$key->jenis_kegiatan_id."'>".$key->keterangan."</option>";
					      	}
						?>
				      </select>
				  
				</div>
				
				<div id="kegiatan-content"></div>	
				
				<div class="form-group">
					<label>&nbsp;</label>
						<br>
				     	<button type="submit" class="btn btn-primary disabled" id="btn-simpan"><i class="fa fa-check-square"></i> Simpan Reservasi</button>
				      <a href="#" class="btn btn-default btn-close-resv" onclick="return confirm('Apakah Anda yakin ingin membatalkan pemesanan ruangan?')"><i class="fa fa-ban"></i> Batal</a>		    
				 </div>
			
			</form>
	
	</div> <!-- row -->
</fieldset>
<script>
$(".form-tambah-ruang").hide();
$('#tambah-ruang').click(function(e){	
	add_ruang_available();		
	$(".form-tambah-ruang").show('slow');
		
});

$(function () {
    $('#datetimepicker8').datetimepicker({
       pickTime : true,
	   icons: {
					time: "fa fa-clock-o",
					date: "fa fa-calendar",
					up: "fa fa-arrow-up",
					down: "fa fa-arrow-down"
				}
    });
	
    $('#datetimepicker9').datetimepicker({
       pickTime : true,
	   icons: {
					time: "fa fa-clock-o",
					date: "fa fa-calendar",
					up: "fa fa-arrow-up",
					down: "fa fa-arrow-down"
				}
    });
    $("#datetimepicker8").blur("change.dp",function (e) {
       $('#datetimepicker9').data("DateTimePicker").setStartDate(e.date);
    });
    $("#datetimepicker9").blur("change.dp",function (e) {
       $('#datetimepicker8').data("DateTimePicker").setEndDate(e.date);
    });
});


$('.btn-close-resv').click(function(e){	
	$(".form-reservasi").removeClass("col-md-4");
    $(".view-tersedia").removeClass("col-md-8");
	$(".form-reservasi").hide();
		
});



$('#cmbkegiatan').change(function(e){	
	var jenis 	= document.getElementById("cmbkategori");
	var jenisid	= $(jenis).val();
	
	var kegiatan 	= document.getElementById("cmbkegiatan");
	var kegiatanid	= $(kegiatan).val();
	
	var user 		= document.getElementById("user_id");
	var userid		= $(user).val();
	
	var type 		= document.getElementById("user_init");
	var typeid		= $(type).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'booking/get_form_kegiatan',
		data : $.param({
			id : userid,
			kategori: kegiatanid,
			type : typeid
		}),
		success : function(msg) {	
			$("#btn-simpan").removeClass("disabled");
			$('#input-kegiatan').val(jenisid);
			$("#kegiatan-content").html(msg);				
			
		}
	});
		
});

$(document).ready(function(){
	$('.fade-out').delay(10000).slideUp();
	
		
	$('#datetimepicker8, #datetimepicker9').blur(function(){
		add_ruang_available();		
	});
	
});

function add_ruang_available(){
	var ruang 	= document.getElementById("input-ruang");
	var ruangid	= $(ruang).val();
	
	var kategori 	= document.getElementById("cmbkategori");
	var kategoriid	= $(kategori).val();
		
	var tgl_mulai_ = $('#datetimepicker8').val();
	var tgl_selesai_ = $('#datetimepicker9').val();
	var uri = base_url + 'booking/cek_ruang';

	if(tgl_mulai_ != '' && tgl_selesai_ != ''){
		
		$('#wrap-ruang').html('');
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : uri,
			data : $.param({tgl_mulai : tgl_mulai_, tgl_selesai : tgl_selesai_, kategori_ruang:kategoriid}),
			success:function(msg){
				$('#sel-ruang').html(msg);					
				var r_id = "'"+ruangid+"'";
				var el = '<label style="margin-right: 2px" class="label label-primary" r_id="'+ruangid+'" onclick="del_data('+r_id+')">'+ruangid+' <input type="hidden" name="ruang[]" value="'+ruangid+'"> <i class="fa fa-times"></i></label>';
				$('#wrap-ruang').append(el);
				
				$('#input-ruang').val(ruangid);		
			}
		});
	}
}

function addRuang(nilai){
	
	var data = nilai.split('|');
	if(cek_ruang(data[0])){
		var r_id = "'"+data[0]+"'";
		var el = '<label style="margin-right: 2px" class="label label-primary" r_id="'+data[0]+'" onclick="del_data('+r_id+')">'+data[1]+' <input type="hidden" name="ruang[]" value="'+data[0]+'"> <i class="fa fa-times"></i></label>';
		$('#wrap-ruang').append(el);
	}
	else{
		alert('Maaf, ruangan sudah dipilih!');
	}
}

function cek_ruang(r_id){
	var init = true;
	$("#wrap-ruang label").each(function(){
		var ruang = $(this).find('input').val();
		if(ruang == r_id) init = false;
	});
	
	return init;
}

function cek_ruang_choose(){
	var init = false;
	$("#wrap-ruang label").each(function(){
		init = true;
	});
	
	if(! init) alert('Silakan pilih ruangan yang ingin dipesan!!');
	else{
		return confirm('Apakah Anda yakin ingin melakukan pemesanan ruangan?');	
	}
}

function del_data(ruang){
	if(confirm('Apakah Anda membatalkan pemesanan ruangan ' + ruang + '?')){
		$(".label[r_id='"+ruang+"']").fadeOut();
		$(".label[r_id='"+ruang+"']").remove();
	}
}

function pop(no){
	$(".item-row-"+no).popover({
		html : true
	});
	$(".item-row-"+no).popover('show');
}

function pophide(no){
	$(".item-row-"+no).popover('hide');
}

</script>