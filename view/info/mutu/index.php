

<div class="row">
	<div class="col-md-12">    
		<?php
		$url=$this->config->file_url_view;
		$url_old=$this->config->file_url_old;
		if($kategori_data=='aim'):
			?>
				<!-- Nav tabs -->
				<ul class="nav nav-pills nav-pills-color" id="writeTab">
					<?php 
					$arr_data=array();
					$j=0;
					foreach($siklus as $dt):
						$j++;
						if($j==1) $strclass=  "class='active' ";
						else $strclass="";
						
						$arr_temp = array("unit_id"=>$dt->unit_id, "siklus"=>$dt->siklus, "class_str"=>$strclass);
						array_push($arr_data, $arr_temp);
						?>
						 <li><a href="#<?php echo $dt->siklus ?>" role="tab" data-toggle="tab"><?php  echo $dt->nama. " ".$dt->siklus; ?></a></li>
						<?php
					endforeach;
				
					?>					
				</ul>

				<div class="tab-content">
					<?php
					for($i=0;$i<count($arr_data);$i++){
						$list_data = $mpage->read_mutu($kategori_data, $lang, $arr_data[$i]["unit_id"],$arr_data[$i]["siklus"]);
						
						?>
						 <div class="tab-pane" id="<?php  echo $arr_data[$i]["siklus"];?>"><?php  read_data_list($list_data, $url,$url_old, $mpage);	?></div>
						<?php
					}
					?>
				</div>					
				
			<?php
		else:
			
			if(count($unit_mutu)<2):
			
				foreach($unit_mutu as $dt):
					$list_data = $mpage->read_mutu($kategori_data, $lang, $dt->unit_id);
					//if($dt->unit_id==0):
						read_data_list($list_data, $url);					
					//endif;
				endforeach;
			else:
				//echo count($unit_mutu);
				?>
				<!-- Nav tabs -->
				<ul class="nav nav-pills nav-pills-color" id="writeTab">
					<?php 
					$arr_data=array();
					$j=0;
					foreach($unit_mutu as $dt):
						$j++;
						if($j==1) $strclass=  " active ";
						else $strclass="";
						
						$arr_temp = array("unit_id"=>$dt->unit_id, "index_list"=>$dt->id, "class_str"=>$strclass);
						array_push($arr_data, $arr_temp);
						?>
						 <li class="<?php echo $strclass;?>"><a href="#<?php echo $dt->id ?>" role="tab" data-toggle="tab"><?php  echo $dt->unit; ?></a></li>
						<?php
					endforeach;
				
					?>					
				</ul>

				<div class="tab-content">
					<?php
					for($i=0;$i<count($arr_data);$i++){
						$list_data = $mpage->read_mutu($kategori_data, $lang, $arr_data[$i]["unit_id"]);
						
						?>
						 <div class="tab-pane <?php  echo $arr_data[$i]["class_str"];?>" id="<?php  echo $arr_data[$i]["index_list"];?>"><?php  read_data_list($list_data, $url,$url_old, $mpage);	?></div>
						<?php
					}
					?>
				</div>					
				
			<?php
			endif;
		endif;
		
		?>
	</div>
</div>
	<?php
	function read_data_list($list_data, $url){
	?>
		<table class="table table-hover">
			<thead>
				<tr><th colspan="2">Document List</th></tr>
			</thead>
			<tbody>
		<?php
		$n=0;
		foreach($list_data as $key):
			$n++;
			
			$file_name = $url."/".$key->file_loc;
			$print = base64_encode(md5(urlencode($file_name)));
						
			$safe = $file_name;
			
			$mpage = new model_page();
			if($mpage->check_pict_session($print, $safe, 'check')==FALSE){
				$datanya = array('val_session' => $print,
								 'val_url' => $safe,
								 'is_publish'=>$key->is_publish
								);
				$mpage->save_pict_session($datanya);
			}
			?>
			<tr>
				<td><?php echo $n.".";  ?></td>
				<td>
					<div class="col-md-10">
						<?php echo $key->judul  ?>
					</div>
					<div class="col-md-2">
						<?php if($key->is_publish=='1') :	?>
							<a href="<?php echo 'https://'.$_SERVER['HTTP_HOST']."/page/read_/".$print ; //echo $url."/".$key->file_loc; // echo 'https://'.$_SERVER['HTTP_HOST']."/page/read_/".$print ;//echo $mpage->str_content($url."/".$key->file_loc,$url,$url_old); //echo $url."/".$key->file_loc  ?>" target="_blank"><i class="fa fa-download"></i> <?php if($key->is_publish=='0') echo "<span class='text text-danger'><small><b>*</b></small></span>"; ?></a> 
						<?php else : ?>
						<a href="<?php echo "https://ptiik.ub.ac.id/auth"; // echo 'https://'.$_SERVER['HTTP_HOST']."/page/read_/".$print ;//echo $mpage->str_content($url."/".$key->file_loc,$url,$url_old); //echo $url."/".$key->file_loc  ?>" target="_blank"><i class="fa fa-download"></i> <?php if($key->is_publish=='0') echo "<span class='text text-danger'><small><b>*</b></small></span>"; ?></a> 
						<?php endif; ?>						
					</div>
			</td></tr>
			<?php
		endforeach;
		?>
			</tbody>
		</table>
	<?php
	}
	?>
	
