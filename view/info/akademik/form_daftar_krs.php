<?php 
if(isset($mhs)){
	$nim = $mhs->mahasiswa_id;
	$nama = $mhs->nama;
	$lahir	= $mhs->tmp_lahir;
	$tgl	= $mhs->tgl_lahir;
	$email	= $mhs->email;
	$seleksi= $mhs->jalur_seleksi;
	$prodi	= $mhs->prodi;
	$prodiid= $mhs->prodi_id;
	$alamat	= $mhs->alamat;
	$telp	= $mhs->telp;
	$hp		= $mhs->hp;
	$pembimbingid = $mhs->dosen_pembimbing;
	$pembimbing = $mhs->pembimbing;
	
}else{
	$nim	= "";
	$nama 	= "";
	$lahir	= "";
	$tgl	= "";
	$email	= "";
	$seleksi= "";
	$prodi	= "";
	$prodiid= "";
	$alamat	= "";
	$telp	= "";
	$hp		= "";
	$pembimbingid="";
	$pembimbing="";
	
}

if(isset($daftar)){
?>
	
	<div class="col-lg-12 hide-from-print">
		<br>
		<div class="alert alert-warning">
			Anda sudah melakukan pengisian program semester pendek. Silahkan melakukan validasi dan verifikasi dengan bagian Akademik untuk proses selanjutnya.
		</div>
			<p>&nbsp;</p>
	</div>
	
<?php
}
?>
			<div class="col-lg-6 hide-from-print">
				
				<h4>Biodata Pribadi</h4>
				<div class="well">
					<div class="form-group">
						<div class="control-label">Nama</div>	
						<input type="text" name="nama" value="<?php echo ucWords(strToLower($nama)); ?>" placeholder="Nama lengkap" class="form-control" readonly>	
						<input type="hidden" id="hidmhs" name="hidmhs" value="<?php echo $nim; ?>">
						<input type="hidden" id="hidsemester" name="hidsemester" value="<?php echo $semester; ?>">							
					</div>
										
					<div class="form-group">
						<div class="control-label">Program Studi</div>	
						<input type="text" name="prodi" class="form-control" value="<?php echo $prodi; ?>" placeholder="Program studi" readonly>		
						<input type="hidden" name="hidprodi" value="<?php echo $prodiid; ?>">
					</div>
					<div class="form-group">
						<div class="control-label">Email</div>
						<input type="email" name="email" class="form-control" value="<?php echo $email; ?>" placeholder="Email aktif" required>
					</div>
										
					<div class="form-group">
						<div class="control-label">Telp</div>
						<input type="text" name="telp" class="form-control" value="<?php echo $telp; ?>" placeholder="Telepon" required>
					</div>
					
					<div class="form-group">
						<div class="control-label">HP</div>
						<input type="text" name="hp" class="form-control" value="<?php echo $hp; ?>" placeholder="HP" required>
					</div>
					
				</div>
				<?php
				if(!$daftar):
				?>
				<div class="clearfix"></div>
				<div class="form-actions hide-from-print">
				
					<input type="submit" id="btn-save-daftar" name="b_proses" value = " Proses Daftar Semester Pendek " class="btn btn-primary" onClick="return save_proses();">
					<a href="#" id="btn-cetak-daftar"  class="btn btn-danger" onClick="return cetak_kuitansi();"><i class="fa fa-print"></i> Cetak Lembar Pengisian</a>
					<span class="status-daftar" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>
				
				</div>
				<?php else: ?>
					<div class="clearfix"></div>
						<div class="form-actions hide-from-print">
							<?php if($daftar->is_valid==0): ?> <input type="submit" id="btn-save-daftar-ok" name="b_proses" value = " Proses Daftar Semester Pendek " class="btn btn-primary" onClick="return save_proses();"><?php endif; ?>	
							<a href="#" id="btn-cetak-daftar-ok"  class="btn btn-danger" onClick="return cetak_kuitansi();"><i class="fa fa-print"></i> Cetak Lembar Pengisian</a>
							<span class="status-daftar" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>
					</div>
				<?php endif; ?>
			</div>
			
			<div class="col-lg-6 " id="content-print">
				<h4 class="hide-from-print">&nbsp;</h4>
					<div class="form-group hide-from-print">
						<label class="control-label">Dosen Pembimbing</label>
						<select class="col-md-12 e2 form-control" name="cmbpembimbing" id="cmbpembimbing">
							<option value="-">Please Select</option>
						<?php
						 if($dosen):
							foreach($dosen as $dt):
								?>
								<option value="<?php echo $dt->id; ?>" <?php if($pembimbingid==$dt->id) echo "selected";?>><?php echo $dt->name; ?></option>
								<?php
							endforeach;
						 endif;
						?>
						</select>
					</div>
							
				<?php 
				$mconf = new model_confinfo();

				?>
				<?php if((!$daftar)|| ($daftar)&&($daftar->is_valid==0)): ?>				
				<div class="form-tambah-mk hide-from-print">
						<div class="form-group">
							<label class="control-label">Matakuliah Semester Pendek</label>
							<div class="input-group">	
								<select class="form-control col-md-4" id="sel-kelas">
									<option value="A">Kelas A</option>
									<option value="B">Kelas B</option>
								</select>
							</div>
							<div class="input-group">	
								<select class="col-md-8 form-control" id="sel-mk">
									<?php 
										
										$mk = $mconf->get_mkditawarkan(0,$semester);
										$biaya = $mconf->konfigurasi_akademik();
										
										foreach($mk as $key){
											echo "<option value='".$key->id.'|'.$key->sks.'|'.$key->kode_mk .' - '.$key->namamk."'>" . '' . $key->kode_mk . ' - '.$key->namamk ." [".$key->sks." sks] </option>";
										}
									?> 
								</select>
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button" onclick="addmk(document.getElementById('sel-mk').value, document.getElementById('sel-kelas').value)">Pilih MK <i class="fa fa-check"></i></button>
								</span>
							</div>				  
						</div>
						
				</div>
				<?php
			
				endif;
				
				$krs = $mconf->get_krs_tmp_mhs($semester, $nim);
				if($krs):
					echo '<input type=hidden name=hidtmpmhs value="'.$nim.'">';
					
					if($daftar->is_valid=='0'):
					
						?>
						
						<div class="form-group hide-from-print">
						
						<table class="table">
							<thead>
								<tr>
									<th>
										<div class="col-md-6">MK</div>
										<div class="col-md-2">SKS</div>
										<div class="col-md-3">Biaya</div>
										<div class="col-md-1">&nbsp;</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="wrap-mk">
										<?php 
										$sks = 0;
										$total = 0;
										foreach($krs as $key): 
											$sks = $sks + $key->sks;
											$nbiaya = ($key->inf_biaya * $key->sks);
											$total = $total + $nbiaya;
										?>		
											<div class="mk row" r_id="<?php echo $key->mkditawarkan_id ?>" style="padding:5px;">
												<div class="col-md-6"><?php echo $key->kode_mk. ' - '. $key->nama_mk;?>, Kelas <?php 
												if(!$key->kelas) echo 'A';
												else echo $key->kelas; ?>
												<input type="hidden" name="kelas[]" value="<?php echo $key->kelas;?>">
												<input type="hidden" name="sks[]" value="<?php echo $key->sks;?>"><input type="hidden" name="mk[]" value="<?php echo $key->kode_mk. ' - '. $key->nama_mk;?>"><input type="hidden" name="mkid[]" value="<?php echo $key->mkditawarkan_id;?>" class="mkidtmp"><input type="hidden" name="biaya[]" value="<?php echo $key->inf_biaya;?>'">
												</div>
												<div class="col-md-2"><?php echo $key->sks;?> sks</div>
												<div class="col-md-3">Rp. <?php echo number_format($nbiaya);?></div>
												<div class="col-md-1"><a href="#" onclick="del_data_sp('<?php echo $key->mkditawarkan_id;?>','<?php echo $key->kode_mk. ' - '. $key->nama_mk;?>','<?php echo $key->sks;?>','<?php echo $nbiaya;?>','<?php echo $key->krs_id; ?>')"><i class="fa fa-trash-o"></i></a></div>
											</div>
										<?php 
										endforeach; ?>
									</td>
								</tr>
								<tr><td>
									<div class="col-md-6"><div class="pull-right"><b>Total SKS</b></div></div>
									<div class="col-md-2"><span id="sks"><b><?php echo $sks;?> sks</b></span></div>
									<div class="col-md-3"><span id="biaya"><b>Rp. <?php echo number_format($total);?></b></span></div>
									<div class="col-md-1">&nbsp;</div>
									</td>
								</tr>
							</tbody>
						</table>

						<input type="hidden" id="no" value="0">
						<input type="hidden" id="tmpb" value="<?php echo $biaya->biaya_sks; ?>">
						<input type="hidden" id="tsks" value="<?php echo $sks;?>">
						<input type="hidden" id="tbiaya" value="<?php echo $total;?>">	
						<input type="hidden" id="totalsks" value="12">						
					</div>
						<?php
					else:
					?>
					<?php if($daftar->is_valid==1) echo '<br><div class="alert alert-danger hide-from-print">Data sudah di validasi Akademik.</div>' ?>
					<?php if($daftar->is_valid==3) echo '<br><div class="alert alert-danger hide-from-print">Data sudah di validasi Akademik & Keuangan. Proses pendaftaran selesai.</div>' ?>
					<table class="table">
							<thead>
								<tr>
									<th>
										<div class="col-md-6">MK</div>
										<div class="col-md-2">SKS</div>
										<div class="col-md-3">Biaya</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<?php 
										$sks = 0;
										$total = 0;
										foreach($krs as $key): 
											$sks = $sks + $key->sks;
											$biaya = ($key->inf_biaya * $key->sks);
											$total = $total + $biaya;
										?>										
											<div class="col-md-6"><?php echo $key->kode_mk. ' - '. $key->nama_mk;?>, Kelas <?php 
											if(!$key->kelas) echo 'A';
												else echo $key->kelas;?></div>
											<div class="col-md-2"><?php echo $key->sks;?> sks</div>
											<div class="col-md-3">Rp. <?php echo number_format($biaya);?></div>
										<?php 
										endforeach; ?>
									</td>
								</tr>
								<tr><td>
									<div class="col-md-6"><div class="pull-right"><b>Total SKS</b></div></div>
									<div class="col-md-2"><span id="sks"><b><?php echo $sks;?> sks</b></span></div>
									<div class="col-md-3"><span id="biaya"><b>Rp. <?php echo number_format($total);?></b></span></div>
									<div class="col-md-1">&nbsp;</div>
									</td>
								</tr>
							</tbody>
						</table>				       
					<?php 
					endif;
				else:?>				
									
					<div class="form-group table-mk hide-from-print">
						<input type="hidden" id="no" value="0">
						<input type="hidden" id="tmpb" value="<?php echo $biaya->biaya_sks; ?>">
						<input type="hidden" id="tsks" value=0>
						<input type="hidden" id="totalsks" value="12">
						<input type="hidden" id="tbiaya" value=0>						
						<table class="table">
							<thead>
								<tr>
									<th>
										<div class="col-md-6">MK</div>
										<div class="col-md-2">SKS</div>
										<div class="col-md-3">Biaya</div>
										<div class="col-md-1">&nbsp;</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><td id="wrap-mk">&nbsp;</td></tr>
								<tr><td><div class="col-md-6"><div class="pull-right"><b>Total SKS</b></div></div>
										<div class="col-md-2"><span id="sks"><b>0 sks</b></span></div>
										<div class="col-md-3"><span id="biaya"><b>Rp. 0</b></span></div>
										<div class="col-md-1">&nbsp;</div></td></tr>
							</tbody>
						</table>				       
					</div>
				<?php endif; ?>
				
				
			</div>
			
	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->location('assets/js/info/select2/select2.css')?>" />
	<script src="<?php echo $this->location('assets/js/info/select2/select2.js')?>"></script>
	<script src="<?php echo $this->location('assets/js/info/accounting.js')?>"></script>
	<script src="<?php echo $this->location('assets/js/info/jskrs.js')?>"></script>
	

