<?php $this->view("page/header.php", $data); 

?>

<section class="content content-gray hide-from-print">
	<div class="container">
		<div class="col-md-12">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php echo semester_pendek;?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo semester_pendek ?></span></li>
					</ol>
					<!-- End Breadcrumb -->
		</div>
	</div>
</section>


<section class="content content-white">
	<div class="container">
		<div class="col-md-12">	 				
				<?php
				
				
				if(isset($kalender) && ($kalender)){	
					$tgl_ = date("Y-m-d H:i");
					//echo $tgl_.strtotime($kalender->tgl_selesai)." ".strtotime($tgl_);
					
					
					if(strtotime($kalender->tgl_selesai) < strtotime($tgl_)){
						?>
						<div class="alert alert-warning">Perhatian! Proses pengisian semester pendek dimulai pada 
						<span class="label label-warning" style="font-size:110%"><?php echo date("M d, Y H:i", strtotime($kalender->tgl_mulai)); ?></span> sampai <span class="label label-warning" style="font-size:110%"><?php echo date("M d, Y H:i", strtotime($kalender->tgl_selesai)); ?></span>. Silahkan menghubungi Akademik untuk informasi lebih lanjut.</div>
						<?php
					}else{
				
						?>				
						<form method="post" id="form-save-daftar" role="form" action="<?php echo $this->location('info/akademik'); ?>">
							<div class="col-lg-6 hide-from-print">
								<div class="form-group">					
									<div class="input-group">
									  <input type="text" class="form-control" id="inputmhs" placeholder="Enter NIM" onkeypress="return checkEnter(event);" >
									  <span class="input-group-btn">
										<button class="btn btn-primary" onClick="get_mhs();" type="button">Go!</button>
									  </span>
									</div><!-- /input-group -->
								</div>
							</div>
							<div class="col-lg-6 hide-from-print">
								<span class="status-daftara" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>
							</div>
							<div class="clearfix"></div>
							<div id="form-content-daftar"></div>				
						</form>
				<?php
					}
				}else{
					$mpost = new model_confinfo();
					$dt	= $mpost->kalender_akademik('krs', $semester);
				?>
					<div class="alert alert-warning">Perhatian! Proses pengisian semester pendek dimulai pada 
					<span class="label label-warning" style="font-size:110%"><?php echo date("M d, Y H:i", strtotime($dt->tgl_mulai)); ?></span> sampai <span class="label label-warning" style="font-size:110%"><?php echo date("M d, Y H:i", strtotime($dt->tgl_selesai)); ?></span>. Silahkan menghubungi Akademik untuk informasi lebih lanjut.</div>
				<?php
				}
				?>
				</div>
			
		</div>
</section>
<?php 
$this->view("page/footer.php", $data);?>