<?php 
$this->view("page/header.php", $data); 
?>
<?php
//session_start();
$_SESSION['count'] = time();
$image;
?>

<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<section itemscope="" itemtype="http://schema.org/Article">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Perancangan MK Pilihan'; else echo "Optional Course";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Perancangan MK Pilihan'; else echo "Optional Course";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			</section>
		</div>
	</div>
</section>

	<section class="content content-white">
		<div class="container container-content"> 	
			<div class="col-md-12">	 
				
				<?php
				
				
				if(isset($kalender) && ($kalender)){	
				
				
				?>
					<div class="alert alert-info"><?php echo $kalender->keterangan; ?></div>
				<form method="post" id="form-save-daftar" role="form" action="<?php echo $this->location('info/akademik'); ?>">
					<div class="col-lg-6 hide-from-print" id="frm-tmp">
						<div class="form-group" >
							<?php create_image(); ?>
							<div class="col-md-6 well">
								<div style="display:block;margin-bottom:5px;margin-top:0px;">									
									<img src="<?php echo $this->asset("upload/captcha/image".$_SESSION['count'].".png") ?>">&nbsp;
									<a  href="#" onclick="window.location.href = window.location.href" class="btn" style="margin-top:15px;position:absolute"><i class="fa fa-refresh"></i> </a>
								</div>
							</div>
							<div class="col-md-6">
							<input type="text" name="input" id="input-kode" class="form-control" placeholder="Enter Text Pada Gambar Disamping" required >
							<input type="hidden" name="flag" value="<?php echo md5($_SESSION['captcha_string']); ?>" id="flag" /><br>
							</div>
							<div class="col-md-6 input-group">
							  <input type="text" class="form-control" id="inputmhs" placeholder="Enter NIM" onkeypress="return checkEnter(event);" >
							  <span class="input-group-btn">
								<button class="btn btn-warning" id="btn-validasi" onClick="get_mhs();" type="button">Validasi</button> 
								
							  </span>
							 
							</div><!-- /input-group -->
							 &nbsp;&nbsp;&nbsp;&nbsp;<small><span class="text text-danger">Klik tombol Validasi atau tekan Enter</span></small>
						</div>
					</div>
					<div class="col-lg-6 hide-from-print">
						<span class="status-daftara" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>
					</div>
					<div class="clearfix"></div>
					<div id="form-content-daftar"></div>				
				</form>
				<?php
				}else{
					$mpost = new model_confinfo();
					$tgl_	= $mpost->kalender_akademik_kuesioner();
				?>
					<div class="alert alert-warning">Perhatian! Pengisian form MK pilihan dijadwalkan pada 
					<span class="label label-warning"><?php echo date("M d, Y", strtotime($tgl_->tgl_mulai)); ?></span> sampai <span class="label label-warning"><?php echo date("M d, Y", strtotime($tgl_->tgl_selesai)); ?></span>. Silahkan menghubungi Akademik untuk informasi lebih lanjut.</div>
				<?php
				}
				?>
				</div>
			</div>
				
		</div>
	</section>
<?php 
$this->view("page/footer.php", $data); 

 unset($_SESSION['captcha_string']); 
	function create_image()
		{
		global $image;
		$image = imagecreatetruecolor(200, 50) or die("Cannot Initialize new GD image stream");
		 
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 0, 255, 255);
		$line_color = imagecolorallocate($image, 0, 0, 150);
		$pixel_color = imagecolorallocate($image, 0, 0, 255);
		 
		imagefilledrectangle($image, 0, 0, 200, 50, $background_color);
		 
		for ($i = 0; $i < 3; $i++) {
		//imageline($image, 0, rand() % 50, 200, rand() % 50, $line_color);
		}
		 
		for ($i = 0; $i < 150; $i++) {
		imagesetpixel($image, rand() % 200, rand() % 50, $pixel_color);
		}
		 
		 
		$letters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$len = strlen($letters);
		$letter = $letters[rand(0, $len - 1)];
		 
		$text_color = imagecolorallocate($image, 0, 0, 0);
		$word = "";
		for ($i = 0; $i < 6; $i++) {
		$letter = $letters[rand(0, $len - 1)];
		imagestring($image, 7, 5 + ($i * 30), 20, $letter, $text_color);
		$word .= $letter;
		}
		$_SESSION['captcha_string'] = $word;
		 
		$images = glob("assets/upload/captcha/*.png");
		foreach ($images as $image_to_delete) {
		@unlink($image_to_delete);
		}
		imagepng($image, "assets/upload/captcha/image" . $_SESSION['count'] . ".png");
				 
		}
	?>