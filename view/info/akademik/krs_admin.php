<?php $this->view("page/header.php", $data); //$this->view("navbar-info.php"); ?>
<section id="wrap" class="mini-side-open">
	<section class="content content-white">
		<div class="container container-content"> 	
			<div class="col-md-12">	 
				<legend class="hide-from-print">Form Daftar Semester Pendek</legend>	
				
				<form method="post" id="form-save-daftar" role="form" action="<?php echo $this->location('info/akademik'); ?>">
					<div class="col-lg-6 hide-from-print">
						<div class="form-group">					
							<div class="input-group">
							  <input type="text" class="form-control" id="inputmhs" placeholder="Enter NIM" onkeypress="return checkEnter(event);" >
							  <span class="input-group-btn">
								<button class="btn btn-primary" onClick="get_mhs();" type="button">Go!</button>
							  </span>
							</div><!-- /input-group -->
						</div>
					</div>
					<div class="col-lg-6 hide-from-print">
						<span class="status-daftara" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>
					</div>
					<div class="clearfix"></div>
					<div id="form-content-daftar"></div>				
				</form>
				
				</div>
			</div>
				
		</div>
	</section>
</section>
<?php $this->view("page/footer.php", $data);?>