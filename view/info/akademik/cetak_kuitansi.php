<!--<div class="hide-from-print" style="border-top:dashed 2px #000; padding:8px;">&nbsp;</div>-->
<?php

$semester	= $mconf->get_tahun_akademik_name($semesterid);
$strsemester = $semester->tahun."/".($semester->tahun + 1);

if(($mhs->gelar_awal)&&($mhs->gelar_awal!="-")){
	$gawal= ", ".$mhs->gelar_awal;
}else{
	$gawal="";
}

if(($mhs->gelar_akhir)&&($mhs->gelar_akhir!="-")){
	$gakhir= ", ".$mhs->gelar_akhir;
}else{
	$gakhir="";
}

$kuitansi = array('Akademik','Keuangan', 'Mahasiswa');

for($i=0;$i<count($kuitansi);$i++){
	
	?>
	<div id="header-report">
		<table  width="100%">
			<tr valign="top">
				<td width="12%">
					<img src="<?php echo $this->asset("upload/ptiik.jpg") ?>" class="img img-responsive-report" width="80px">
				</td>
			<td>
				<div style="padding-top:10px;">KEMENTERIAN RISET, TEKNOLOGI, DAN PENDIDIKAN TINGGI<br>UNIVERSITAS BRAWIJAYA <br>						
					  <!--<b>PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER</b>-->
					  <b>FAKULTAS ILMU KOMPUTER</b>
				</div>			
			</td>
			<td align="right" width="10%">				
			<div style="border:solid 1px #000; padding:6px;align:right"><h4><?php echo $kuitansi[$i] ?></h4></div>			
			</td>
			</tr>
		</table>
		
		<p>&nbsp;</p>		
		<div class="row">				
			<h5 align="center"><b>LEMBAR PENGISIAN PERMOHONAN PROGRAM SEMESTER PENDEK <br>				
			<?php 
			//echo "&nbsp;".strToupper($semester->is_ganjil); 
			//if($semester->is_pendek){ echo  " ".strToupper($semester->is_pendek);}

			echo " TAHUN AJARAN ". $semester->tahun."/".($semester->tahun + 1);
			?> <hr>						
			</b></h5>
		</div>
	</div>
	<div class="content-report row">
		<table style="width:100%">
		<tr>
			<td colspan="3">Yang bertanda tangan dibawah ini:</td>
		</tr>
		<tr>
			<td width="20%">Nama</td>
			<td>:</td>
			<td><b><?php echo $mhs->nama;?></b></td>
		</tr>
		<tr>
			<td>NIM</td>
			<td>:</td>
			<td><b><?php echo $mhs->nim;?></b></td>
		</tr>
		<tr>
			<td>Telp/HP</td>
			<td>:</td>
			<td><?php if($mhs->telp!="-") echo $mhs->telp.'/'.$mhs->hp; 
			else echo $mhs->hp;?></td>
		</tr>
		<tr>
			<td>E-mail</td>
			<td>:</td>
			<td><?php echo $mhs->email;?></td>
		</tr>
		<tr>
			<td>Prodi</td>
			<td>:</td>
			<td><?php echo $mhs->prodi;?></td>
		</tr>
	</table>
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="2%">No</th>
				<th>Kode MK</th>
				<th>Mata Kuliah*</th>
				<th>SKS</th>
				<th>Biaya</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$j=0;
		$sks = 0;
		$total = 0;
		$biaya = 0;
		if($krs):
			foreach($krs as $key):
				$j++;	
				if($key->is_approve!='2'){				
					$sks = $sks + $key->sks;
					$biaya = ($key->inf_biaya * $key->sks);
					$total = $total + $biaya;			
				
				?>
				<tr>
					<td><?php echo $j;?>.</td>
					<td><?php echo $key->kode_mk ?></td>
					<td><?php echo $key->nama_mk ?></td>
					<td align="center"><?php echo $key->sks ?> sks</td>
					<td align="right">Rp. <?php echo number_format($biaya);?></td>												
				</tr>
				<?php
				}else{
					$sks = $sks;
					$biaya = $biaya;
					$total = $total;
					
				}
			endforeach;
			?>
			<tr>
				<td colspan="3"><div class="pull-right"><b>Jumlah</b></div></td>
				<td align="center"><b><?php echo $sks;?> sks</b></td>
				<td align="right"><b>Rp. <?php echo number_format($total);?></b></td>
			</tr>
			<?php
		endif;
	?>
	</tbody>
	</table>
	<small>*) KHS mata kuliah yang diajukan terlampir</small><br>
	<div style="line-height: 120%;display: block;"><p>Matakuliah yang saya program diatas adalah matakuliah yang pernah saya tempuh sebelum pelaksanaan semester pendek <?php echo $strsemester; ?>, dan nilai akhirnya sudah keluar. Bilamana matakuliah yang saya program dalam semester pendek <?php echo $strsemester; ?> adalah matakuliah yang belum pernah ditempuh sebelum pelaksanaan semester pendek <?php echo $strsemester; ?>, maka saya bersedia menerima sanksi berupa <em><b>"digugurkannya seluruh matakuliah yang saya tempuh dalam Semester Pendek <?php echo $strsemester; ?>".</b></em><br>
	Demikian isian permohonan ini saya buat dengan sebenarnya.</p>
	</div>
	<p>&nbsp;</p>
		<div class="row">		
			<div class="col-lg-8 pull-left">
				Mengetahui<br>
					Dosen Pembimbing Akademik,
					<p>&nbsp;</p><p>&nbsp;</p>
					<span style="padding:1px">
						<?php echo $mhs->pembimbing.$gawal.$gakhir ?>
					</span><br>
					<span style="border-top:solid 1.1px #000; margin-top:2px"><b><?php echo strToUpper($mhs->is_nik) ?>. <?php echo $mhs->nik ?></b></span>
			</div>
			<div class="col-lg-4 pull-right">
				
					Malang, <b><?php echo date("d"). " ". $this->get_nama_bulan(date("m")) ." ". date("Y"); ?></b><br>
					Pemohon,
					<p>&nbsp;</p><p>&nbsp;</p>
					<span style="padding:1px">
						<?php echo $mhs->nama ?>
					</span><br>
					<span style="border-top:solid 1.1px #000; margin-top:1px"><b>NIM. <?php echo $mhs->nim ?></b></span>
			</div>		
		</div>
	</div>
	<div class="row" style="border-top:dashed 1px #000; margin-top:10px;">&nbsp;</div>
	<div class="row">
		<div style="border:solid 1px #000; padding:5px;">
			<table width="100%">
				<tr>
					<td width="20%">Sudah terima dari</td>
					<td width="2%">:</td>
					<td width="50%"><?php echo $mhs->nama ?></td>
					<td><div class="pull-right"><b>NIM. <?php echo $mhs->nim ?></b></div></td>
				</tr>
				<tr>
					<td width="20%">Jumlah uang</td>
					<td>:</td>
					<td colspan="2"><b>Rp. <?php echo number_format($total);?></b></td>
				</tr>
				<tr valign="top">
					<td width="20%">Untuk pembayaran</td>
					<td>:</td>
					<td colspan="2">Semester Pendek <?php echo $strsemester ?> Program Studi <?php echo $mhs->prodi ?>, Jumlah SKS <b><?php echo $sks ?></b>, Jumlah Matakuliah <b><?php echo count($krs); ?></b></td>
				</tr>
				<tr valign="top">
					<td width="20%">Terbilang</td>
					<td>:</td>
					<td colspan="2"><b>#<?php echo $this->terbilang($total);?>#</b></td>
				</tr>
			</table>
			<p>&nbsp;</p>
			<div class="row">
				<div class="col-md-6 pull-left"><br>
					Bagian Keuangan,
						<p>&nbsp;</p>
						<p>&nbsp;</p>
				(...........................)						
				</div>
				<div class="col-md-2">&nbsp;</div>
				<div class="col-md-4 pull-right">
						Malang, <br>
						Bagian Akademik,
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						(...........................)
				</div>
			</div>
		
		</div>
	</div>
	<div class="row" style="border-top:dashed 1px #000; margin-top:10px;">
	<small><span class="text text-danger"><b>Catatan: Jika terdapat coretan atau tipe-ex maka lembar ini dianggap tidak sah.</b></span></small>
	</div>
	<div class="page-break" id="footer"></div>
	<div class="row hide-from-print" style="border-bottom:solid 1px #ddd; padding:10px;"></div>
	<?php
	
}

?>
