<?php $this->view("header-v3.php"); //$this->view("navbar-info.php"); ?>
<section id="wrap" class="mini-side-open">
	<section class="content content-white">
		<div class="container container-content"> 	
			<div class="col-md-12">	 
				<legend>Form Daftar Ulang Mahasiswa</legend>	
				<?php
				if(isset($kalender) && ($kalender)){				
				
				?>				
				<form method="post" id="form-save-daftar" role="form" action="<?php echo $this->location('info/akademik'); ?>">
					<div class="col-lg-6">
						<div class="form-group">					
							<div class="input-group">
							  <input type="text" class="form-control" id="inputmhs" placeholder="Enter NIM" onkeypress="return checkEnter(event);" >
							  <span class="input-group-btn">
								<button class="btn btn-default" onClick="get_mhs();" type="button">Go!</button>
							  </span>
							</div><!-- /input-group -->
						</div>
					</div>
					<div class="col-lg-6">
						<span class="status-daftara" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>
					</div>
					<div class="clearfix"></div>
					<div id="form-content-daftar"></div>				
				</form>
				<?php
				}else{
					$mpost = new model_confinfo();
					$data	= $mpost->kalender_akademik('daftar', $semester);
				?>
					<div class="alert alert-warning">Perhatian! Jadwal daftar ulang belum aktif. Proses daftar ulang akademik akan dimulai pada 
					<span class="label label-warning"><?php echo date("M d, Y", strtotime($data->tgl_mulai)); ?></span> sampai <span class="label label-warning"><?php echo date("M d, Y", strtotime($data->tgl_selesai)); ?></span>. Silahkan menghubungi Akademik untuk informasi lebih lanjut.</div>
				<?php
				}
				?>
				</div>
				</div>
		</div>
	</section>
</section>
<?php $this->view("footer-v3.php");?>