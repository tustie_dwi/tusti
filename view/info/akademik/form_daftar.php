<?php 
if(isset($mhs)){
	$nim = $mhs->mahasiswa_id;
	$nama = $mhs->nama;
	$lahir	= $mhs->tmp_lahir;
	$tgl	= $mhs->tgl_lahir;
	$email	= $mhs->email;
	$seleksi= $mhs->jalur_seleksi;
	$prodi	= $mhs->prodi;
	$prodiid= $mhs->prodi_id;
	$alamat	= $mhs->alamat;
	$telp	= $mhs->telp;
	$hp		= $mhs->hp;
	$ortu	= $mhs->nama_ortu;
	$emailo	= $mhs->email_ortu;
	$alamato= $mhs->alamat_ortu;
	$telpo	= $mhs->telp_ortu;
	$hpo	= $mhs->hp_ortu;
	$catatan= $mhs->catatan;
	$alamats= $mhs->alamat_surat;
}else{
	$nim	= "";
	$nama 	= "";
	$lahir	= "";
	$tgl	= "";
	$email	= "";
	$seleksi= "";
	$prodi	= "";
	$prodiid= "";
	$alamat	= "";
	$telp	= "";
	$hp		= "";
	$ortu	= "";
	$emailo	= "";
	$alamato= "";
	$telpo	= "";
	$hpo	= "";
	$catatan= "";
	$alamats= "";
}

if(isset($daftar)){
?>
	
	<div class="col-lg-12">
		<br>
		<div class="alert alert-warning">
			Anda sudah memasukkan biodata pribadi dan orang tua untuk proses daftar ulang. Silahkan melakukan validasi dan verifikasi dengan bagian Akademik untuk proses daftar ulang selanjutnya.
		</div>
			<p>&nbsp;</p>
	</div>
	
<?php
}
?>
			<div class="col-lg-6">
				
				<h4>Biodata Pribadi</h4>
				<div class="well">
					<div class="form-group">
						<div class="control-label">Nama</div>	
						<input type="text" name="nama" value="<?php echo ucWords(strToLower($nama)); ?>" placeholder="Nama lengkap"  required>	
						<input type="hidden" name="hidmhs" value="<?php echo $nim; ?>">						
					</div>
					<div class="form-group">
						<div class="control-label">TTL</div>	
						<input type="text" name="lahir" value="<?php echo $lahir; ?>" placeholder="Tempat lahir" required><br>
						<div class="tgl-lahir input-group">
							  <input type="text" name="tgl" class="tgl-lahir form-control " value="<?php echo $tgl; ?>" placeholder="Tgl lahir YYYY-MM-DD"  required >
							  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
					
					</div>
					<div class="form-group">
						<div class="control-label">Jalur Seleksi</div>	
						<!--<input type="text" name="jalurmasuk" value="<?php //echo $seleksi; ?>" placeholder="Jalur seleksi masuk UB" required>-->
						<select name="jalurmasuk">
							<option value="reguler" <?php if($seleksi =='reguler'){ echo "selected"; } ?>>Reguler</option>
							<option value="sap"  <?php if($seleksi =='sap'){ echo "selected"; } ?>>Alih Jenjang/SAP</option>
						</select>
					</div>
					<div class="form-group">
						<div class="control-label">Program Studi</div>	
						<input type="text" name="prodi" value="<?php echo $prodi; ?>" placeholder="Program studi" readonly>		
						<input type="hidden" name="hidprodi" value="<?php echo $prodiid; ?>">
					</div>
					<div class="form-group">
						<div class="control-label">Email</div>
						<input type="email" name="email" class="span6" value="<?php echo $email; ?>" placeholder="Email aktif" required>
					</div>
					<div class="form-group">
						<div class="control-label">Alamat Malang</div>			
						<textarea name="alamat" placeholder="Alamat di Malang" required><?php echo $alamat; ?></textarea>		
					</div>
					
					<div class="form-group">
						<div class="control-label">Telp</div>
						<input type="text" name="telp" class="span6" value="<?php echo $telp; ?>" placeholder="Telepon" required>
					</div>
					
					<div class="form-group">
						<div class="control-label">HP</div>
						<input type="text" name="hp" class="span6" value="<?php echo $hp; ?>" placeholder="HP" required>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<h4>Data Orang Tua</h4>
				<div class="well">
					<div class="form-group">
						<div class="control-label">Nama</div>	
						<input type="text" name="namaortu" value="<?php echo $ortu; ?>" placeholder="Nama orang tua" required>			
					</div>
					<div class="form-group">
						<div class="control-label">Email</div>
						<input type="email" name="emailortu" value="<?php echo $emailo; ?>" placeholder="Email orang tua" class="span6">
					</div>
					<div class="form-group">
						<div class="control-label">Alamat</div>			
						<textarea name="alamatortu" placeholder="Alamat orang tua" required><?php echo $alamato; ?></textarea>		
					</div>
					
					<div class="form-group">
						<div class="control-label">Telp</div>
						<input type="text" name="telportu" value="<?php echo $telpo; ?>" placeholder="Telepon orang tua" class="span6" required>
					</div>
					
					<div class="form-group">
						<div class="control-label">HP</div>
						<input type="text" name="hportu" class="span6" value="<?php echo $hpo; ?>" placeholder="HP orang tua" required>
					</div>
				</div>
				
				<h4>Surat Menyurat</h4>
				<div class="well">
				<div class="form-group">
				<span class="text text-danger">Tuliskan alamat surat menyurat dengan benar. Apabila alamat surat menyurat berbeda dengan alamat orang tua, beri keterangan pada kolom <b>Catatan</b></span>
				</div>
				<div class="form-group">
					<div class="control-label">Alamat</div>			
					<textarea name="alamatsurat" placeholder="Alamat surat menyurat" required><?php echo $alamats; ?></textarea>		
				</div>
				
				<div class="form-group">
					<div class="control-label">Catatan *)</div>			
					<textarea name="catatan" placeholder="Catatan"><?php echo $catatan; ?></textarea>		
				</div>
				</div>
			</div>
		<?php
		if(!$daftar){
		?>
		<div class="clearfix"></div>
		<div class="col-lg-12">
		<div class="form-actions">
			<input type="submit" id="btn-save-daftar" name="b_proses" value = " Proses Daftar Ulang " class="btn btn-primary" onClick="save_proses();">
			<span class="status-daftar" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>
		</div>
		</div>
		<?php } ?>
	
	
	

