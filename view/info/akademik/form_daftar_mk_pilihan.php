<?php 
if(isset($mhs)){
	$nim = $mhs->mahasiswa_id;
	$nama = $mhs->nama;
	$lahir	= $mhs->tmp_lahir;
	$tgl	= $mhs->tgl_lahir;
	$email	= $mhs->email;
	$seleksi= $mhs->jalur_seleksi;
	$prodi	= $mhs->prodi;
	$prodiid= $mhs->prodi_id;
	$alamat	= $mhs->alamat;
	$telp	= $mhs->telp;
	$hp		= $mhs->hp;
	$pembimbingid = $mhs->dosen_pembimbing;
	$pembimbing = $mhs->pembimbing;
	if($daftar):
		$semestermk	= $daftar->semester;
	else:
		$semestermk= "";
	endif;
}else{
	$nim	= "";
	$nama 	= "";
	$lahir	= "";
	$tgl	= "";
	$email	= "";
	$seleksi= "";
	$prodi	= "";
	$prodiid= "";
	$alamat	= "";
	$telp	= "";
	$hp		= "";
	$pembimbingid="";
	$pembimbing="";
	$semestermk= "";
}

//$semestermkpilihan = array(5,7,9,11,13);
$semestermkpilihan = array(4,5,6,7,8,9,10,11,12);

//if(isset($daftar)){
?>
	<!--
	<div class="col-lg-12 hide-from-print">
		<br>
		<div class="alert alert-warning">
			Anda sudah melakukan pengisian form perancangan MK pilihan. Silahkan menghubungi bagian Akademik untuk info selanjutnya.
		</div>
			<p>&nbsp;</p>
	</div>-->
	
<?php
//}
?>
			<div class="col-lg-6 hide-from-print">
				
				<h4>Biodata Pribadi</h4>
				<div class="well">
					<div class="form-group">
						<div class="control-label">Nama</div>	
						<input type="text" class="form-control" name="nama" value="<?php echo ucWords(strToLower($nama)); ?>" placeholder="Nama lengkap"  readonly>	
						<input type="hidden" id="hidmhs" name="hidmhs" value="<?php echo $nim; ?>">
						<input type="hidden" id="hidsemester" name="hidsemester" value="<?php echo $semester; ?>">
						<input type="hidden" id="hidkuesioner" name="hidkuesioner" value="<?php echo $kuesioner; ?>">						
					</div>
										
					<div class="form-group">
						<div class="control-label">Program Studi</div>	
						<input type="text" class="form-control" name="prodi" value="<?php echo $prodi; ?>" placeholder="Program studi" readonly>		
						<input type="hidden" name="hidprodi" value="<?php echo $prodiid; ?>">
					</div>
					<div class="form-group">
						<div class="control-label">Email</div>
						<input type="email" name="email" class="form-control" value="<?php echo $email; ?>" placeholder="Email aktif" required>
					</div>
										
					<div class="form-group">
						<div class="control-label">Telp</div>
						<input type="text" name="telp" class="form-control" value="<?php echo $telp; ?>" placeholder="Telepon" required>
					</div>
					
					<div class="form-group">
						<div class="control-label">HP</div>
						<input type="text" name="hp" class="form-control" value="<?php echo $hp; ?>" placeholder="HP" required>
					</div>
					
				</div>
				<?php
				if(!$daftar):
				?>
				<div class="clearfix"></div>
				<div class="form-actions hide-from-print">
				
					<input type="submit" id="btn-save-daftar" name="b_proses" value = " Proses Form Perancangan MK Pilihan " class="btn btn-primary" onClick="return save_proses();">
					<!--<a href="#" id="btn-cetak-daftar"  class="btn btn-danger" onClick="return cetak_kuitansi();"><i class="fa fa-print"></i> Cetak Lembar Pengisian</a>-->
					<a href="<?php echo $this->location('info/akademik/pilihan/mk'); ?>"  class="btn btn-danger"><i class="fa fa-check"></i> Data Baru</a>
					<!--<div class="clearfix"></div>
					<span class="status-daftar" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>-->
				
				</div>
				<?php else: ?>
					
					<div class="clearfix"></div>
						<div class="form-actions hide-from-print">
							<a href="<?php echo $this->location('info/akademik/pilihan/mk'); ?>"  class="btn btn-danger"><i class="fa fa-check"></i> Data Baru</a>
							<!--<?php //if($daftar->is_valid==0): ?> <input type="submit" id="btn-save-daftar-ok" name="b_proses" value = " Proses Rancangan MK Pilihan " class="btn btn-primary" onClick="return save_proses();"><?php //endif; ?>	
							<a href="#" id="btn-cetak-daftar-ok"  class="btn btn-danger" onClick="return cetak_kuitansi();"><i class="fa fa-print"></i> Cetak Lembar Pengisian</a>
							<span class="status-daftar" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>-->
					</div>
				<?php endif; ?>
			</div>
			
			<div class="col-lg-6 " id="content-print">
				<h4 class="hide-from-print">&nbsp;</h4>
					<div class="form-group hide-from-print">
						<label class="control-label">Dosen Pembimbing</label>
						<select class="form-control e2" name="cmbpembimbing" id="cmbpembimbing">
							<option value="-">Please Select</option>
						<?php
						 if($dosen):
							foreach($dosen as $dt):
								?>
								<option value="<?php echo $dt->id; ?>" <?php if($pembimbingid==$dt->id) echo "selected";?>><?php echo $dt->name; ?></option>
								<?php
							endforeach;
						 endif;
						?>
						</select>
					</div>
					
					<div class="form-group hide-from-print">
						<label class="control-label">Semester</label>
						<select class="form-control e2" name="cmbsemester" id="cmbsemester">
							<option value="-">Please Select</option>
						<?php
						
							for($i=0;$i<count($semestermkpilihan); $i++){
								?>
								<option value="<?php echo $semestermkpilihan[$i]; ?>" <?php if($semestermk==$semestermkpilihan[$i]) echo "selected";?>><?php echo $semestermkpilihan[$i]; ?></option>
								<?php
							}
						 
						?>
						</select>
					</div>
							
				<?php 
				$mconf = new model_confinfo();

				?>
				<?php if(!$daftar): ?>				
				<div class="form-tambah-mk hide-from-print">					
											
	
						<div class="form-group">
							<label class="control-label">Matakuliah Pilihan Semester <?php echo ucwords($namasemester);?> </label>
							<div class="input-group">							
								<select class="form-control" id="sel-mk">
									<?php 
										
										$mk = $mconf->get_mkditawarkan_kuesioner($kuesioner, $prodiid);
										
										foreach($mk as $key){
											echo "<option value='".$key->id.'|'.$key->sks.'|'.$key->kode_mk .'#'.$key->namamk."'>" . '' . $key->kode_mk . ' - '.$key->namamk ." [".$key->sks." sks] </option>";
										}
									?> 
								</select>
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button" onclick="addmk(document.getElementById('sel-mk').value)">Pilih MK <i class="fa fa-check"></i></button>
								</span>
							</div>				  
					</div>
				</div>
				<?php
			
				endif;
				
				$krs = $mconf->get_mk_pilihan_mhs($kuesioner, $nim);
				if($krs):
					?>
					<div class="alert alert-danger hide-from-print">Anda sudah mengisi form perancangan pilihan MK. Silahkan menghubungi Akademik untuk informasi selengkapnya.</div>
					<table class="table">
							<thead>
								<tr>
									<th>
										<div class="col-md-9">MK</div>
										<div class="col-md-2">SKS</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<?php 
										$sks = 0;
										$total = 0;
										foreach($krs as $key): 
											$sks = $sks + $key->sks;
											
										?>										
											<div class="col-md-9"><?php echo $key->kode_mk. ' - '. $key->nama_mk;?></div>
											<div class="col-md-2"><?php echo $key->sks;?> sks</div>
										<?php 
										endforeach; ?>
									</td>
								</tr>
								<tr><td>
									<div class="col-md-9"><div class="pull-right"><b>Total SKS</b></div></div>
									<div class="col-md-2"><span id="sks"><b><?php echo $sks;?> sks</b></span></div>
									</td>
								</tr>
							</tbody>
						</table>				       
					<?php 
					
				else:?>				
									
					<div class="form-group table-mk hide-from-print">
						<input type="hidden" id="no" value="0">
						<input type="hidden" id="totalmk" value="10">
						<input type="hidden" id="tsks" value=0>
						<input type="hidden" id="totalsks" value="12">			
						<table class="table">
							<thead>
								<tr>
									<th>
										<div class="col-md-9">MK</div>
										<div class="col-md-2">SKS</div>
										<div class="col-md-1">&nbsp;</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><td id="wrap-mk">&nbsp;</td></tr>
								<tr><td><div class="col-md-9"><div class="pull-right"><b>Total SKS</b></div></div>
										<div class="col-md-2"><span id="sks"><b>0 sks</b></span></div>
										<div class="col-md-1">&nbsp;</div></td></tr>
							</tbody>
						</table>				       
					</div>
				<?php endif; ?>
				
				
			</div>
			
	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->location('assets/js/info/select2/select2.css')?>" />
	<script src="<?php echo $this->location('assets/js/info/select2/select2.js')?>"></script>
	<script src="<?php echo $this->location('assets/js/info/accounting.js')?>"></script>
	<script src="<?php echo $this->location('assets/js/info/kuesioner_detail.js')?>"></script>
	

