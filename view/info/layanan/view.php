<?php $this->view("header-v3.php");?>
<section id="wrap" class="mini-side-open">
<section class="content content-white">
<div class="container container-content"> 	
<div class="row">
<div class="col-md-12">
    
	<legend class="legend-title-padding-bottom">
	<div class='pull-right'>
		<a href="<?php echo $this->location('info/layanan'); ?>" class="btn btn-info">
		<i class="fa fa-th-list"></i> Layanan List</a>&nbsp;
		
		<a href="<?php echo $this->location('module/layanan/write'); ?>" class="btn btn-default">
		<i class="fa fa-edit"></i> New Layanan</a>	
	</div>
	Detail Progress
	</legend>
		<?php
	if($layanan) :
		foreach ($layanan as $dt):	
		?>
		<dl class="dl-horizontal">
			<dt>No Permintaan</dt>
				<dd>
					<p><span class='text-info'><?php echo $dt->no_permintaan; ?></span></p>
				<dd>
			<dt>Request By</dt>		
				<dd><p><?php echo $dt->nama; ?></p></dd>
			<dt>Tgl Permintaan</dt>		
				<dd><p><i class="fa fa-clock-o"></i>&nbsp; <?php echo date("M d, Y", strtotime($dt->tgl_permintaan)); ?></p></dd>
			
			<dt>Keterangan</dt>		
				<dd><p><span class='text-warning'><?php echo $dt->judul; ?></span>
				<blockquote>
					
					<?php
					if($dt->keterangan){ echo str_replace("<p>","",$dt->keterangan); } 
					if($dt->nama_file){
					?>
					<br><a href="<?php echo $this->location($dt->lokasi); ?>" target='_blank'><i class='icon-file'></i>Dokumen Pendukung</a>
					<?php
					}					
					?>					
					
				</blockquote></p></dd>
			<dt>Unit Dituju</dt>
				<dd><p><?php echo $dt->nama_unit_dituju; ?></p></dd>
			<dt>Tgl Deadline</dt>
				<dd><p><i class="fa fa-clock-o"></i>&nbsp;<?php echo date("M d, Y", strtotime($dt->tgl_harapan_selesai));?></p></dd>
			<dt>Proses</dt>
				<dd><p>
				<?php
				if($dt->disetujui_oleh){
				?>
				<div class="row">
                        <div class="col-md-2" style="margin-bottom:5px">
						<?php
						
							echo $dt->st_proses ." by";
						
						?>
					  </div>
					   <div class="col-md-10" style="margin-bottom:5px"><span class="label label-info"><?php echo $dt->disetujui_oleh; ?></span></div>
					   <div class="col-md-2" style="margin-bottom:5px">PIC</div>
                       <div class="col-md-10" style="margin-bottom:5px">
						 <?php 
							if($pelaksana){
							
								$jsonArray = json_decode($pelaksana);
								
								$i=0;
								foreach($jsonArray as $value){								
									$id 	= $value->permintaan_id;
									$nama 	= $value->nama;								
									
									if($id == $dt->permintaan_id){
										$i++;							
										
										echo "<span class='label label-default'>".$nama."</span>&nbsp;";
									}
								}
							}							
						?>
						
					  </div>
					  <div class="col-md-2" style="margin-bottom:5px">Pengerjaan</div>
                        <div class="col-md-10" style="margin-bottom:5px"><i class="fa fa-clock-o"></i> <?php 
						  echo date("M d, Y", strtotime($dt->tgl_mulai_pengerjaan))." - ". date("M d, Y", strtotime($dt->tgl_deadline)); ?></span>					
						</div>
						<div class="col-md-2" style="margin-bottom:5px">Catatan</div>
                        <div class="col-md-10" style="margin-bottom:5px"> <blockquote><?php 
						  if($dt->catatan){ echo str_replace("<p>","",$dt->catatan); }  ?></blockquote></span>					
						</div>
					</div>
				<?php
				}
				?>
				</p>		
				</dd>
		</dl>
		<?php
		endforeach;
		 	
	if( $detail ) :	
	?>

	<h4>Progress</h4>
	<table class="table table-hover table-striped example" data-id='module/layanan/progress'>
		<thead>
			<tr>
				<th width="20%">PIC</th>
				<th width="30%">Pelaksanaan</th>
				<th>Kegiatan</th>
				<!--<th>Act</th>-->
			</tr>
		</thead>
		<tbody>
		<?php
			$i = 1;
			if($detail > 0){
				foreach ($detail as $dt): 
				?>
				<tr id="post-<?php echo $dt->progress_id;?>" data-id="<?php echo $dt->progress_id; ?>" valign=top>
					<td><?php echo $dt->nama; ?></td>
					<td><small>
						<div class="row">
                        <div class="col-md-6" style="margin-bottom:5px">Tgl Mulai:</div>
                        <div class="col-md-6" style="margin-bottom:5px">
							<span class="badge"><?php echo date("M d, Y", strtotime($dt->tgl_mulai)); ?></span>
						 </div>
                        <div class="col-md-6" style="margin-bottom:5px">Target Selesai:</div>
						<div class="col-md-6" style="margin-bottom:5px">
						  	<span class="badge"> <?php echo date("M d, Y", strtotime($dt->tgl_target_selesai)); ?></span>
						</div>
                        <div class="col-md-6" style="margin-bottom:5px">Tgl Penyelesaian:</div>
                        <div class="col-md-6" style="margin-bottom:5px">
							<span class="badge"><?php 
							  if($dt->tgl_selesai!='0000-00-00'){
								echo date("M d, Y", strtotime($dt->tgl_selesai)); 
							  }else{
								echo "-";
							  } ?></span>
						
						  </div>
						</div>
						</small>				
					  
					</td>
					<td><?php echo $dt->judul; ?>
						<blockquote>
							<small>
								<?php if($dt->keterangan){ echo str_replace("<p>","",$dt->keterangan); } ?>
							</small>
						</blockquote>						
					</td>
					
				
					<!--<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href="<?php //echo $this->location('module/layanan/progress/comment/'.$dt->id); ?>"><i class='icon-refresh'></i> Add Comment</a>	
									</li>
								
								  </ul>
								</li>
							</ul>
						</td>-->
				
					</tr>
						<?php
				 endforeach; 
			 }
			 ?>
		</tbody></table>
		<?php
		 else: 
		 ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php endif; 
		endif;
		?>
	
</div>
</div>
</div>
</section>
</section>
<?php $this->view("footer-v3.php");?>