<?php $this->view("header-v3.php"); //$this->view("navbar-info.php"); ?>
<section id="wrap" class="mini-side-open">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
 
    
	<legend>Permintaan Layanan List</legend>
	
	 <?php
	
	
	 if( $posts ) :	
	?>
	
	<table class="table table-hover table-striped example" >
		<thead>
			<tr>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
				?>
				<tr id='post-<?php echo $dt->permintaan_id;?>' data-id='<?php echo $dt->permintaan_id; ?>' valign=top>
					
					<td>
					<div class="col-md-10">
						<b><a href="<?php echo $this->location('info/progress/'.$dt->id); ?>"><span class="text text-default"><?php echo $dt->judul; ?></span></a></b>
						<small>
						<span class="text text-info"><i class="fa fa-clock-o"></i>&nbsp;<?php echo date("M d, Y", strtotime($dt->tgl_permintaan));?></span>
						<i class='fa fa-user'></i>&nbsp;<?php echo $dt->nama;?></small>
						<br>
					
						<span class="label label-warning"><?php echo $dt->nama_unit_dituju; ?></span>
						<small><span class="text text-danger">Deadline <i class='fa fa-clock-o'></i>&nbsp;<b><?php echo date("M d, Y", strtotime($dt->tgl_harapan_selesai)); ?></b></span>	</small>					
						
					</div>
					
					<div class="col-md-2">
						<?php
						switch($dt->status_proses){
							case "1":
								$strclass= "warning";
							break;
							case "2":
								$strclass= "success";
							break;
							case "3":
								$strclass= "default";
							break;
							case "4":
								$strclass= "danger";
							break;
							case "5":
								$strclass= "info";
							break;
							case "6":
								$strclass= "info";
							break;
						}	
						?>
						<span class='label label-<?php echo $strclass; ?>' style='font-weight:normal'><?php echo $dt->st_proses; ?></span>	
					</div>
					
						
					</td>
					
					<!--<td>  
						<?php
						//if($dt->status_proses==2){
						?>	
							<span class="label label-default"><a class='btn-edit-post' href="<?php echo $this->location('info/progress/'.$dt->id); ?>" style='font-weight:normal; color:white'>View Progress</a></span> 	
							<?php
							//}
							?>
						</td>	-->				
					</tr>
						<?php
				 endforeach; 
			 }
			 ?>
		</tbody></table>
	<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
	
	</div>
	</div>
</section>
</section>
<?php $this->view("footer-v3.php");?>