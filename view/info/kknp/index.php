<?php $this->view("page/header.php", $data); ?>

<section class="content content-white">
	<div class="container"> 
	
	<!-- Main Content -->
	
	<div class="col-md-12">	
		<h2 class="title-content margin-top-no margin-bottom-no">
			<?php echo $header ?>
		</h2>
		
		<div class="table-responsive">
			<table class='table table-striped table-hover web-page-search'>
				<thead>
					<tr style="display:none;border:0px;">
						<td>&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<?php 
						if($posts){
							foreach ($posts as $p) {
					?>
						<tr>
							<td>
								<div class="col-md-10">
									<i class="fa fa-building-o"></i> <?php echo $p->nama_perusahaan ?> <code><?php echo $p->alamat ?></code>
									<br>
									<?php 
									$nama_mhs = explode(',', $p->nama_mhs);
									foreach ($nama_mhs as $mhs) {
										echo "<i class='fa fa-user'>&nbsp;&nbsp;</i>".$mhs."&nbsp;&nbsp;&nbsp;";
									}
									?>
								</div>
								<div class="col-md-2">
									<?php echo "<div class='label label-warning' align='center'>".$p->is_status."</div>" ?>
								</div>
							</td>
						</tr>
					<?php 
							}
						} 
					?>
				</tbody>
			</table>
		</div>
		
	</div>
	
	<!-- End Main Content -->
	
	</div>
</section>

<?php $this->view("page/footer.php", $data);  ?>