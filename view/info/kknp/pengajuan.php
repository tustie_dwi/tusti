<?php $this->view("page/header.php", $data); ?>

<section class="content content-white">
	<div class="container"> 
	
	<!-- Main Content -->
	
	<div class="col-md-12">	
		<h2 class="title-content margin-top-no margin-bottom-no">
			<?php echo $header ?>
		</h2>
		
		<div class="row">
			<form id="form-daftar-ulang">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Company Name</label><br>
						<input type="text" required="required" class="form-control typeahead" name="perusahaan_name" id="perusahaan_name" value="<?php if(isset($perusahaan_name)) echo $perusahaan_name ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Company Address</label><br>
						<input type="text" required="required" class="form-control" name="perusahaan_alamat" id="perusahaan_alamat" value="<?php if(isset($perusahaan_alamat)) echo $perusahaan_alamat ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Tanggal Mulai</label><br>
						<input type="text" required="required" class="form-control" name="tgl_mulai" id="tgl_mulai" value="<?php if(isset($tgl_mulai)) echo $tgl_mulai ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Tanggal Selesai</label><br>
						<input type="text" required="required" class="form-control" name="tgl_selesai" id="tgl_selesai" value="<?php if(isset($tgl_selesai)) echo $tgl_selesai ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Mahasiswa</label><br>
						<input type="text" required="required" class="form-control typeahead" name="mhs_kknp" id="mhs_kknp" value="<?php if(isset($mhs_kknp)) echo $mhs_kknp ?>"/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">&nbsp;</label><br>
						<input type="hidden" name="hidId" value="<?php if(isset($hid_id))echo $hid_id; ?>" />
						<input type="hidden" name="tgl_pengajuan" value="<?php if(isset($tgl_pengajuan))echo $tgl_pengajuan; ?>" />
						<a href="javascript::" class="btn btn-default" id="submit-submission" type="submit">Submit</a>
					</div>
				</div>
			</form>
		</div>
	</div>
	
	<!-- End Main Content -->
	
	</div>
</section>

<?php $this->view("page/footer.php", $data);  ?>