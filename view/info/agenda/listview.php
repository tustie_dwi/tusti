	
	<?php
    
	if($tgl){
		echo "<h3>Tanggal : ".date('F d, Y', strtotime($tgl))."</h3>";
	}
	
	?>
    <div class="row">
    <div class="col-md-6">
    <form class="form-inline" method=post action="<?php echo $this->location('info/agenda')?>"  role="form">
    <div class="form-group">			
			<input type="text" name="tgl" class="date date-time-picker form-control" placeholder='Tgl..' value="<?php echo $tgl; ?>" required data-format="YYYY-MM-DD">
			</div>
            <div class="form-group"> 
			<select class="form-control" name="cmbkegiatan" onChange='form.submit();'>
			<option value="">Semua Jenis Kegiatan</option>
			<?php
				foreach($kegiatan as $dt):
					echo "<option value='".$dt->jenis_kegiatan_id."' ";
					if($jeniskegiatan==$dt->jenis_kegiatan_id){
						echo "selected ";
					}
					echo  ">".$dt->keterangan."</option>";
				endforeach;
			?>
		</select></div>
        <div class="form-group"> 
			<button type="submit" name="b_cek" class="btn btn-info btn-no-radius"> View </button>
            </div>
		</form>		
        
    </div>
    
    <div class="col-md-6">
    <div class="text-right-md">
    <a href="#list" data-toggle="tab" class="btn-mod mod-orange active"><span class="btn-mod-icon fa fa-calendar"></span><span class="btn-mod-text">Harian</span></a>
    <a href="#week" data-toggle="tab" class="btn-mod mod-blue"><span class="btn-mod-icon fa fa-calendar"></span><span class="btn-mod-text">Mingguan</span></a>
    <a href="#grid" data-toggle="tab" class="btn-mod mod-blue"><span class="btn-mod-icon fa fa-calendar"></span><span class="btn-mod-text">Bulanan</span></a>
    </div>
                </div>
                </div>
	<?php
	
	if( isset($agenda) ) :		
	?> 
	<div class='table-responsive well-ptiik'><table class='table table-striped table-hover' id='example'>
				<thead>
					<tr>
						<th width="10%">Jam</th>
						<th width="40%">Kegiatan</th>	
						<th width="15%">Lokasi</th>
						<th width="15%">Penyelenggara</th>
						<th>Kategori</th>
					</tr>
				</thead>
				<tbody>
	<?php
			$i = 1;			
			foreach ($agenda as $dt): 
				echo "
						<tr valign=top>
							<td>".$dt->from." - ".$dt->to."</td>";		
							
							echo "<td><a href='".$this->location('info/detail/agenda/'.$dt->id)."'>".$dt->judul."</a></td><td>".$dt->lokasi;
							if($dt->ruang){
								echo  " <code>R. ".$dt->ruang."</code>";
							}
					echo "</small></td><td>".$dt->penyelenggara;
					if($dt->unit_penyelenggara){
						echo "<br><em>".$dt->unit_penyelenggara."</em>";
					}
					echo  "</td><td>".$dt->jenis."</td></tr>";
					
			endforeach; 
		?>
			
		</tbody></table></div>
		
		<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
	