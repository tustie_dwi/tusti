<div class='control-group'> 
	<label class=control-label><abbr title='Informasi peserta yang mengikuti kegiatan'>Informasi Peserta</abbr></label>
	<div class=controls>
		<textarea name='infpeserta'><?php echo $infpeserta;?></textarea>
	</div>
</div>						
							
<div class='control-group'> 
	<label class=control-label><abbr title='Peserta dari dosen/staff'>Peserta dari Dosen/Staff</abbr></label>
	<div class=controls>
		<?php
		if(isset($staff)){
			if(count($staff)!=0){
			echo "<p>";
					$i=0;
					$nama="";
					foreach($staff as $dt):
						$i++;
						$nama = $nama . $dt->nama .",";
					echo "<span class='label label-info'>".$dt->nama;
						if($dt->instansi){
						echo "@".$dt->instansi;
						}
						echo "</span> ";									
					endforeach;
				echo "</p>";
			}
		}
		?>
		<input type='text' name='tags' placeholder='Nama Peserta..' class='tagStaff input-xxlarge'/>
	</div>
</div>
	
<div class='control-group'> 
	<label class=control-label><abbr title='Peserta dari mahasiswa'>Peserta dari mahasiswa</abbr></label>
	<div class=controls>
		<?php
		if(isset($mhs)){
			if(count($mhs)!=0){
			echo "<p>";
					$i=0;
					$nama="";
					foreach($mhs as $dt):
						$i++;
						$nama = $nama . $dt->nama .",";
					echo "<span class='label label-info'>".$dt->nama;
						if($dt->instansi){
						echo "@".$dt->instansi;
						}
						echo "</span> ";									
					endforeach;
				echo "</p>";
			}
		}
		?>
		<input type='text' name='tags' placeholder='Nama Mahasiswa..' class='tagMhs input-xxlarge'/>
	</div>
</div>
	
<div class='control-group'> 
	<label class=control-label><abbr title='Peserta dari luar'>*) Peserta luar</abbr></label>
	<div class=controls>
	<?php
		if(isset($peserta)){
			if(count($peserta)!=0){
			echo "<p>";
					$i=0;
					$nama="";
					foreach($peserta as $dt):
						$i++;
						$nama = $nama . $dt->nama .",";
						echo "<span class='label label-info'>".$dt->nama;
						if($dt->instansi){
						echo "@".$dt->instansi;
						}
						echo "</span> ";									
					endforeach;
				echo "</p>";
			}
		}
		?>
		<input type='text' name='tags' placeholder='Nama Peserta..' class='tagPeserta input-xxlarge'/>
	</div>
</div>
						
						