<?php
$this->head();


if($agenda!=""){
	$header		= "Edit Agenda";	

	foreach ($agenda as $dt):
		$id			= $dt->agenda_id;
		$jenisid	= $dt->jenis_kegiatan_id;
		$mulai		= $dt->tmulai;
		$selesai	= $dt->tselesai;
		$judul		= $dt->judul;
		$note		= $dt->keterangan;
		$lokasi		= $dt->lokasi;
		$ruang		= $dt->ruang;
		$ruangid	= $dt->inf_ruang;
		$prodiid	= $dt->prodi_id;
		$infpeserta	= $dt->inf_peserta;
		$infhari	= $dt->inf_hari;
		$isallday	= $dt->is_allday;
		$ispublish	= $dt->is_publish;
		$penyelenggara=$dt->penyelenggara;
		$unitid		= $dt->unit_id;
	endforeach;
}else{
	$header		= "Write New Agenda";	

	
	$id			= "";
	$jenisid	= "";
	$mulai		= "";
	$selesai	= "";
	$judul		= "";
	$note		= "";
	$ruangid	= "";
	$ruang		= "";
	$infpeserta="";
	$infhari	= "";
	$prodiid	= "";
	$lokasi		= "";
	$isallday	= 0;
	$ispublish	= 0;
	$penyelenggara="";
	$unitid		= "-";
}
	
?>

<div class="container-fluid">  
	<fieldset>
	<legend>
		<a href="<?php echo $this->location('module/masterdata/agenda'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> Agenda List</a> 
		<?php if($agenda !=""){	?>
		<a href="<?php echo $this->location('module/masterdata/agenda/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Agenda</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row-fluid">    
        <div class="span12">

<?php		
			$str = "<form method=post  action=".$this->location('module/masterdata/agenda/save')."  enctype='multipart/form-data'  class='form-horizontal'>";
						
						$str.= "<div class='control-group'>";
							$str.= "<label class=control-label>Jenis Kegiatan</label>";
							$str.= "<div class=controls>";
								$str.= "<select name=jenis>";
									$str.= "<option value='-'>Please Select..</option>";
									foreach($jenis as $dt):
										$str.= "<option value='".$dt->id."' ";
										if($jenisid==$dt->id){
											$str.= "selected";
										}
										$str.= ">".$dt->value."</option>";
									endforeach;
								$str.= "</select>";
							$str.= "</div>";
						$str.= "</div>";
						
						$str.= "<div class='control-group'>";
							$str.= "<label class=control-label>Tgl</label>";
							$str.= "<div class=controls>";
								//$str.= "<input type=text name='from' id='datetimepicker' >";
								//$str.= "<input type=text name='from' id='from' > sampai  <input type=text name='to' id='to' >";
								$str.= "<input type='text' class='form_datetime' name='from' value='".$mulai."'> sampai  <input type=text name='to' value='".$selesai."' class='form_datetime'><br>	
										<label class=checkbox><input type=checkbox value=1 name=allday ";										
										if($isallday==1){
											$str.= "checked";
										}
								$str.= ">All Day Event</label>";
						$str.= "</div>";
						$str.= "</div>";
						
						$str.= "<div class='control-group'>"; 
							$str.= "<label class=control-label>Day of Event</label>";
							$str.= "<div class=controls>";
						
								$chari = explode(",", $infhari);				
								
								$i=0;
								$skip = false;
								
								for($i = 0; $i < count($hari); $i++){						
									$skip=false;
									if($infhari!=""){	
										
										for($j=0;$j<count($chari);$j++){	
											
											if($chari[$j] == $hari[$i]->id){
												$str.= "<label class='checkbox inline'><input type=checkbox name='chkhari[]' value='".$hari[$i]->id."' checked";
												
												$skip=true;
												break;										
											}
										}
										if(!$skip) $str.= "<label class='checkbox inline'><input type=checkbox name='chkhari[]' value='".$hari[$i]->id."' ";								
									}else{
										$str.= "<label class='checkbox inline'><input type=checkbox name='chkhari[]' value='".$hari[$i]->id."' ";
									}
									
									$str.= ">".ucFirst($hari[$i]->value)."</label> ";
								}
						
							$str.= "</div>";
						$str.= "</div>";
						
						
						$str.= "<div class='control-group'>"; 
							$str.= "<label class=control-label>Judul Kegiatan</label>";
							$str.= "<div class=controls>";
								$str.= "<textarea name='judul' cols='80' required class='span12'>".$judul."</textarea><br>";
							$str.= "</div>";
						$str.= "</div>";
						
						if($this->coms->authenticatedUser->level==1 || $this->coms->authenticatedUser->username==6){				
							$str.= "<div class='control-group'>";
								$str.= "<label class=control-label>Publish?</label>";
								$str.= "<div class=controls>";
									$str.= "<label class=checkbox><input type=checkbox value='1' name=ispublish ";
									if($ispublish==1){ $str.= "checked"; }
									$str.= ">Yes</label>";
								$str.= "</div>";
							$str.= "</div>";
						}
						
						echo $str;
						
						?>
						<div class="control-group">
							<ul class="nav nav-tabs" id="writeTab">
								<li class="active"><a href="#info">General Info</a></li>
								<li><a href="#lokasi">Lokasi/Ruang</a></li>
								<li><a href="#pemateri">Pemateri</a></li>
								<li><a href="#peserta">Peserta</a></li>
								<li><a href="#undangan">Undangan</a></li>
								<li><a href="#panitia">Panitia</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="info">	
									<?php 
									
									$str= "<div class='control-group'>"; 
									$str.= "<label class=control-label>Penyelenggara</label>";
									$str.= "<div class=controls>";
										$str.= "<textarea name='penyelenggara' cols='80' class='span12' required>".$penyelenggara."</textarea><br>";
									$str.= "</div>";
									$str.= "</div>";
									
									$str.= "<div class='control-group'>"; 
									$str.= "<label class=control-label>Unit Penyelenggara</label>";
									$str.= "<div class=controls>";
										$str.= "<select name='cmbunit' id='cmbunit'  class='span12 populate'>";
										$str.= "<option value='-'>None</option>";
										foreach($unit as $dt):
											$str.= "<option value='".$dt->id."' ";
											if($unitid==$dt->id){
												$str.= "selected";
											}
											$str.= ">".$dt->value."</option>";
										endforeach;
										$str.= "</select>";
									$str.= "</div>";
									$str.= "</div>";
									
									$str.= "<div class='control-group'>"; 
										$str.= "<label class=control-label>Keterangan</label>";
										$str.= "<div class=controls>";
											$str.= "<textarea cols='80' class='ckeditor' id='keterangan' name='keterangan' rows='10' rows='10'>".$note."</textarea><br>";												
										$str.= "</div>";
									$str.= "</div>";
									
									$str.= "<div class='control-group'>"; 
										$str.= "<label class=control-label><b>File</b></label>";
										$str.= "<div class=controls>";
											$str.= "<input type=file name=filein id=filein class='span5'><br><br>";
										$str.= "</div>";
									$str.= "</div>";		
									
									echo $str;
									
									?>
								</div>
								<div class="tab-pane" id="lokasi">	
									<?php 
									
									$data['namaruang']	= $ruang;
									$data['hidruang'] = $ruangid;
									$data['lokasi']  = $lokasi;
									$data['prodiid'] = $prodiid;
									
									$this->view('agenda/lokasi.php', $data);	
									
									?>
								</div>
								<div class="tab-pane" id="pemateri">	
									<?php 
									
									$this->view('agenda/pemateri.php', $data);	
									
									?>
								</div>
								<div class="tab-pane" id="peserta">	
									<?php 
									
									$data['infpeserta']	= $infpeserta;
									
									$this->view('agenda/peserta.php', $data);	
									
									?>
								</div>
								<div class="tab-pane" id="undangan">	
									<?php 
									$this->view('agenda/undangan.php', $data);										
									?>
								</div>
								<div class="tab-pane" id="panitia">	
									<?php 
									
									$this->view('agenda/panitia.php', $data);	
									
									?>
								</div>
							</div>
						</div>
						
						<?php
						
						
					
												
						$str= "<div class='control-group'>"; 
							$str.= "<label class=control-label>&nbsp;</label>";
							$str.= "<div class=controls>";
							//	$str.= "<input type=file name=file id=file class='input-xlarge'><br><br>";
								$str.= "<input type=hidden name=hidId value='".$id."'>";
								$str.= "<input type=submit name='b_agenda' id='b_agenda' value='   Save   ' class='btn btn-primary'>";
							$str.= "</div>";
						$str.= "</div>";
					$str.= "</form>";
					
					echo $str;
		echo "</div></div></fieldset></div>";
$this->foot();

?>
