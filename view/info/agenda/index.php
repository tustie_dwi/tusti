<?php $this->view("header-v3.php", $data);
// $this->view("navbar-info.php"); 
?>
<SCRIPT language="javascript">
function OpenPage(page) {
	if (page != "-1") {
		window.open(page, '_self');
	}
}
</SCRIPT>
<!-- Content -->
<section id="wrap" class="mini-side-open">
<section class="content content-white">
    <div class="container container-content"> 	
    <div class="row">
    <div class="col-md-12">
		<legend>Agenda PTIIK</legend>
		
		<?php
		if(isset($type)){
		?>
				<div class="control-group">
				<?php if($type=='month'){ ?>
					
					<div class="tab-content">
						<div class="tab-pane fade" id="list">	<?php $this->view('info/agenda/listview.php', $data); ?>	</div>
						<div class="tab-pane fade" id="week"><?php  $this->view('info/agenda/weekcalendar.php', $data); ?></div>
						<div class="tab-pane active fade in" id="grid"><?php  $this->view('info/agenda/calendar.php', $data); ?></div>
					</div>
				<?php } else {

						if	($type=='week'){		?>
						
						<div class="tab-content">			
							<div class="tab-pane fade" id="list">	<?php $this->view('info/agenda/listview.php', $data); ?>	</div>
							<div class="tab-pane active fade in" id="week"><?php  $this->view('info/agenda/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/agenda/calendar.php', $data); ?></div>
						</div>
				<?php }else{ ?>
						
						<div class="tab-content">			
							<div class="tab-pane fade in active" id="list">	<?php $this->view('info/agenda/listview.php', $data); ?>	</div>
							<div class="tab-pane fade" id="week"><?php  $this->view('info/agenda/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/agenda/calendar.php', $data); ?></div>
						</div>

				<?php }
				}
				?>
			</div>
		<?php
		}else{
		?>
		
			<div class="control-group">
				<?php if((isset($_POST['month']))||(isset($_POST['year'])) ){ 
					
					?>
					
					<div class="tab-content">
						<div class="tab-pane fade" id="list">	<?php $this->view('info/agenda/listview.php', $data); ?>	</div>
						<div class="tab-pane fade" id="week"><?php  $this->view('info/agenda/weekcalendar.php', $data); ?></div>
						<div class="tab-pane active fade in" id="grid"><?php  $this->view('info/agenda/calendar.php', $data); ?></div>
					</div>
				<?php } else {

						if	((isset($_POST['b_next']))||(isset($_POST['b_prev'])) ||(isset($_POST['b_now']))){		?>
						
						<div class="tab-content">			
							<div class="tab-pane fade" id="list">	<?php $this->view('info/agenda/listview.php', $data); ?>	</div>
							<div class="tab-pane active fade in" id="week"><?php  $this->view('info/agenda/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/agenda/calendar.php', $data); ?></div>
						</div>
				<?php }else{ ?>
						
						<div class="tab-content">			
							<div class="tab-pane fade in active" id="list">	<?php $this->view('info/agenda/listview.php', $data); ?>	</div>
							<div class="tab-pane fade" id="week"><?php  $this->view('info/agenda/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/agenda/calendar.php', $data); ?></div>
						</div>

				<?php }
				}
				?>
			</div>
		<?php } ?>
	</div>
</div>
</div>
</section>
</section>
<?php $this->view("footer-v3.php", $data);?>