<div class='control-group'> 
	<label class=control-label>Lokasi</label>
	<div class=controls>
		<textarea name='lokasi' cols='80' required><?php echo $lokasi; ?></textarea>
	</div>
</div>

<div class='control-group'> 
	<label class=control-label>Ruang</label>
	<div class=controls>
		<!--<input type=text name='ruang' id='ruang' placeholder='Please select..'  value='<?php echo $namaruang; ?>'><input type=hidden name='ruangid' id='ruangid' value='<?php //echo $hidruang; ?>'>-->
		<?php
		$cruang = array();
		if($hidruang){
			$rruang = explode(",", $hidruang);
			for($i=0;$i<count($rruang);$i++){
				$cruang[] = $rruang[$i];
			}
		}
		?>
		<select name="cmbruang[]" multiple  id='cmbruang'  class='span12 populate'>
			<?php
			foreach ($ruang as $dt):
				echo "<option value='".$dt->id."' ";
				if(in_array($dt->id, $cruang)){
					echo "selected";
				}
				echo ">".$dt->value."</option>";
			endforeach;
			?>
		</select>
	</div>
</div>

<div class='control-group'> 
	<label class=control-label>Prodi</label>
	<div class=controls>
		<input type=text name='prodi' id='prodi' placeholder='Please select..' value='<?php echo $prodiid; ?>' ><input type=hidden name='prodiid' id='prodiid' value='<?php echo $prodiid; ?>'>
	
	</div>
</div>
			