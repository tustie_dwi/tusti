<?php
$this->head();
?>

<div class="container-fluid">  
	<fieldset>
	<legend>
		<?php //echo $header; ?>
    </legend> 
    <div class="row-fluid">    
        <div class="span12">
		 <script type="text/javascript">
        $(document).ready(function() {     
			var view="month";          
           
            var DATA_FEED_URL = "/module/masterdata/agenda/viewagenda/list";
            var op = {
                view: view,
                theme:3,
                showday: new Date(),
                EditCmdhandler:Edit,
                DeleteCmdhandler:Delete,
                ViewCmdhandler:View,    
                onWeekOrMonthToDay:wtd,
                onBeforeRequestData: cal_beforerequest,
                onAfterRequestData: cal_afterrequest,
                onRequestDataError: cal_onerror, 
                autoload:true,
                url: DATA_FEED_URL + "/list",  
                quickAddUrl: DATA_FEED_URL + "/add", 
                quickUpdateUrl: DATA_FEED_URL + "/update",
                quickDeleteUrl: DATA_FEED_URL + "/remove"        
            };
            var $dv = $("#calhead");
            var _MH = document.documentElement.clientHeight;
            var dvH = $dv.height() + 2;
            op.height = _MH - dvH;
            op.eventItems =[];

            var p = $("#gridcontainer").bcalendar(op).BcalGetOp();
            if (p && p.datestrshow) {
                $("#txtdatetimeshow").text(p.datestrshow);
            }
            $("#caltoolbar").noSelect();
            
            $("#hdtxtshow").datepicker({ picker: "#txtdatetimeshow", showtarget: $("#txtdatetimeshow"),
            onReturn:function(r){                          
                            var p = $("#gridcontainer").gotoDate(r).BcalGetOp();
                            if (p && p.datestrshow) {
                                $("#txtdatetimeshow").text(p.datestrshow);
                            }
                     } 
            });
            function cal_beforerequest(type)
            {
                var t="Loading data...";
                switch(type)
                {
                    case 1:
                        t="Loading data...";
                        break;
                    case 2:                      
                    case 3:  
                    case 4:    
                        t="The request is being processed ...";                                   
                        break;
                }
                $("#errorpannel").hide();
                $("#loadingpannel").html(t).show();    
            }
            function cal_afterrequest(type)
            {
                switch(type)
                {
                    case 1:
                        $("#loadingpannel").hide();
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $("#loadingpannel").html("Success!");
                        window.setTimeout(function(){ $("#loadingpannel").hide();},2000);
                    break;
                }              
               
            }
            function cal_onerror(type,data)
            {
                $("#errorpannel").show();
            }
            function Edit(data)
            {
               var eurl="edit.db.php?id={0}&start={2}&end={3}&isallday={4}&title={1}";   
                if(data)
                {
                    var url = StrFormat(eurl,data);
                    OpenModelWindow(url,{ width: 600, height: 400, caption:"Ubah Agenda Kegiatan",onclose:function(){
                       $("#gridcontainer").reload();
                    }});
                }
            }    
            function View(data)
            {
                var str = "";
                $.each(data, function(i, item){
                    str += "[" + i + "]: " + item + "\n";
                });
                             
            }    
            function Delete(data,callback)
            {           
                
                $.alerts.okButton="Ok";  
                $.alerts.cancelButton="Cancel";  
                hiConfirm("Anda yakin akan menghapus kegiatan ini", 'Confirm',function(r){ r && callback(0);});           
            }
            function wtd(p)
            {
               if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $("#showdaybtn").addClass("fcurrent");
            }
            
            $("#showdaybtn").click(function(e) {
               
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("day").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            
            $("#showweekbtn").click(function(e) {
                
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("week").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            
            $("#showmonthbtn").click(function(e) {
                
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("month").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            
            $("#showreflashbtn").click(function(e){
                $("#gridcontainer").reload();
            });
            
            
            $("#faddbtn").click(function(e) {
                var url ="edit.db.php";
                OpenModelWindow(url,{ width: 500, height: 400, caption: "Tambah Agenda Kegiatan"});
            });
            
            $("#showtodaybtn").click(function(e) {
                var p = $("#gridcontainer").gotoDate().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }


            });
            
            $("#sfprevbtn").click(function(e) {
                var p = $("#gridcontainer").previousRange().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            
            $("#sfnextbtn").click(function(e) {
                var p = $("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            
        });
    </script>    
			<div>
			
			  <div id="calhead" style="padding-left:1px;padding-right:1px;">          
					<div class="cHead"><div class="ftitle">Agenda Kegiatan PTIIK</div>
					<div id="loadingpannel" class="ptogtitle loadicon" style="display: none;">Loading data...</div>
					 <div id="errorpannel" class="ptogtitle loaderror" style="display: none;">Maaf, data tidak bisa ditampilkan saat ini, silahkan dicoba beberapa saat lagi.</div>
					</div>          
					
					<div id="caltoolbar" class="ctoolbar">
					  <div id="faddbtn" class="fbutton">
						<div><span title='Klik untuk Menambah Kegiatan Baru' class="addcal">
						Tambah Agenda              
						</span></div>
					</div>
					<div class="btnseparator"></div>
					 <div id="showtodaybtn" class="fbutton">
						<div><span title='Klik untuk kembali ke hari ini ' class="showtoday">
						Hari Ini</span></div>
					</div>
					  <div class="btnseparator"></div>

					<div id="showdaybtn" class="fbutton">
						<div><span title='Day' class="showdayview">Harian</span></div>
					</div>
					  <div  id="showweekbtn" class="fbutton fcurrent">
						<div><span title='Week' class="showweekview">Mingguan</span></div>
					</div>
					  <div  id="showmonthbtn" class="fbutton">
						<div><span title='Month' class="showmonthview">Bulanan</span></div>

					</div>
					<div class="btnseparator"></div>
					  <div  id="showreflashbtn" class="fbutton">
						<div><span title='Refresh view' class="showdayflash">Refresh</span></div>
						</div>
					 <div class="btnseparator"></div>
					<div id="sfprevbtn" title="Prev"  class="fbutton">
					  <span class="fprev"></span>

					</div>
					<div id="sfnextbtn" title="Next" class="fbutton">
						<span class="fnext"></span>
					</div>
					<div class="fshowdatep fbutton">
							<div>
								<input type="hidden" name="txtshow" id="hdtxtshow" />
								<span id="txtdatetimeshow">Loading</span>

							</div>
					</div>
					
					<div class="clear"></div>
					</div>
			  </div>
			  <div style="padding:1px;">

				<div class="t1 chromeColor">
					&nbsp;</div>
				<div class="t2 chromeColor">
					&nbsp;</div>
				<div id="dvCalMain" class="calmain printborder">
					<div id="gridcontainer" style="overflow-y: visible;">
					</div>
				</div>
				<div class="t2 chromeColor">

					&nbsp;</div>
				<div class="t1 chromeColor">
					&nbsp;
				</div>   
				</div>
			 
		  </div>
		
		</div>
		
	</div>
</div>
<?php		
$this->foot();

?>