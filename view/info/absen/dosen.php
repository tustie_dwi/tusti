
<?php
	
	if( isset($hdosen) ) :	

		$term=array();
		
		if($absen){
			foreach($absen as $dt):
				$term[] = $dt->pin;
			endforeach;
		}
		
	?> 
	<div class="col-md-12 table-responsive">
		<table class='table table-striped table-hover web-page-search'>
				<thead>
					<tr style="display:none;border:0px;"><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
				</thead>
				<tbody>
	<?php
			$i = 1;			
			foreach ($hdosen as $dt): 
				if(! $dt->foto){
					$foto= "no_foto.png";
				} else {
					$foto = $dt->foto;
				}			

							
					if(in_array($dt->pin, $term)){										
						$strclass = "absen";
						$strisi = "<div class='label label-success' align='center'>Hadir</div>";
						$class = "";										
					}else{
						$class = "gray";
						$strclass = "nabsen";
						$strisi = "<div class='label label-danger' align='center'>Tidak Hadir</div>";
					}
					
					if($dt->foto) $foto = $this->config->file_url_view."/".$dt->foto; 
					else $foto = $this->config->default_thumb_web;
							
					?>
					<tr>
							<td style="display:none;border:0px;"></td>
					<td>
						<div class="col-md-6">
							<div class="media">
							  <a class="pull-left" href="<?php echo $this->location('info/details/'.$dt->id.'/month'); ?>">
								<img class="media-object img-thumbnail" src="<?php echo $foto; ?>"  width="64" height="64">&nbsp;
							  </a>
							  <div class="media-body">
								<b>
								<a href="<?php echo $this->location('info/details/'.$dt->id.'/month'); ?>"><span class="text text-default" style="color:#444"><?php echo $dt->nama; if($dt->gelar_awal!=""){ echo ", ".$dt->gelar_awal; } if($dt->gelar_akhir!=""){ echo ", ".$dt->gelar_akhir; } ?></span></a></b>
								<?php
								//echo "<span class='label label-default' style='font-weight:normal'>".$dt->is_status."</span>";
								?>
								<br>
								<?php 
								if($dt->nik!="-"){
									echo "<small>".strToUpper($dt->is_nik).". ".$dt->nik."</small><br>";
								}
								if($dt->jabatan){
									$jabatan = explode("-",$dt->jabatan);
									if($lang=='in') $jabatan=$jabatan[0];
									else $jabatan=$jabatan[1];
								?>
									<span class='text-info'><i class='icon-tag'></i> <small><?php echo $jabatan; ?></small></span><br>
								<?php
								}
								if($dt->unitkerja){								
									
									$unitk = explode("@", $dt->unitkerja);
								
									for($i=0;$i<count($unitk);$i++){
										$unitid = explode("-", $unitk[$i]);
										
										for($k=0;$k<count($unitid);$k++){							
											if($k==0){											
												echo "<span class='label label-info' style='font-weight:normal'>".$unitid[$k]."</span> ";									
											}
										}
									}
									echo "<br>";
								}
								
								if($dt->ruangkerja){
									
									$ruang = explode("@", $dt->ruangkerja);
										
									for($k=0;$k<count($ruang);$k++){		
																					
										echo "<small><code>R. ".$ruang[$k]."</code></small>&nbsp;";									
										
									}
								}							
								?>										
								
							  </div>
							</div>
						</div>
						<div class="col-md-4">
							<?php
							
							if($dagenda){
								foreach($dagenda as $row):
									if($row->karyawan_id==$dt->karyawan_id){	
										switch (strtolower($row->jenis_kegiatan)){																						
											case 'uts':
												if($row->keterangan=='Pengawas'){
													echo "<span class='text text-info'><small><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", <em>Pengawas ".strToUpper($row->kegiatan)." ".$row->namamk."</em> </small></span><br>";
												}else{
													echo "<span class='text text-info'><small><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", ".strToUpper($row->kegiatan)." ".$row->namamk."</em> </small></span><br>";
												}								
											break;
											case 'uas':
												if($row->keterangan=='Pengawas'){
													echo "<span class='text text-info'><small><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", <em>Pengawas ".strToUpper($row->kegiatan)." ".$row->namamk."</em> </small></span><br>";
												}else{
													echo "<span class='text text-info'><small><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", ".strToUpper($row->kegiatan)." ".$row->namamk."</em> </small></span><br>";
												}								
											break;
											case 'bimbingan':
												echo "<span class='label label-warning' style='font-weight:normal'><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang." - ".$row->kegiatan."  </span><br>";
											break;
											case 'kuliah':
												echo "<span class='text text-warning'><small><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", <em>".$row->kegiatan." ".$row->namamk."</em> </small></span><br>";
											break;
											/*case 'rapat':
												if($row->ruang){
													echo "<span class='text text-info' style='font-weight:normal'><small><i class='fa  fa-clock-o'></i> 
															".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", ".$row->kegiatan."  </small></span><br>";
												}else{
													echo "<span class='text text-info' style='font-weight:normal'><small><i class='fa  fa-clock-o'></i> 
															".$row->jam_mulai." - ".$row->jam_selesai.", ".$row->kegiatan."  </small></span><br>";
												}
											break;*/
											default:
												if($row->ruang){
													echo "<span class='text text-info' style='font-weight:normal'><small><i class='fa  fa-clock-o'></i> 
															".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", ".$row->kegiatan."  </small></span><br>";
												}else{
													echo "<span class='text text-info' style='font-weight:normal'><small><i class='fa  fa-clock-o'></i> 
															".$row->jam_mulai." - ".$row->jam_selesai.", ".$row->kegiatan."  </small></span><br>";
												}
											break;
										}
										
									}
								endforeach;
							}
							
														
							?>
							<small><a href="<?php echo $this->location('info/details/'.$dt->id.'/week'); ?>" class='text text-danger'><span class="nav-pills-icon fa fa-calendar"></span> Weekly</a></small>&nbsp;
							<small><a href="<?php echo $this->location('info/details/'.$dt->id.'/month'); ?>" class='text text-danger'><span class="nav-pills-icon fa fa-calendar"></span> Monthly</a></small>
						</div>
						<div class="col-md-2">
							<?php echo $strisi; ?>
						</div>
						</td>
					</tr>
			<?php
			endforeach; 
		?>
			
		</tbody></table></div>
		
		<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
	