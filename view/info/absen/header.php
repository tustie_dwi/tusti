<!DOCTYPE html>
<html lang="en">
	<head>
		<title>PTIIK - Absen</title>
		<meta http-equiv="refresh" content="70" >

		<script type="text/javascript" language="javascript" src="<?php echo $this->asset("js/jquery/jquery.min.js"); ?>"></script>
		<script type="text/javascript" language="javascript" src="<?php echo $this->asset("js/jquery/jquery.carouFredSel.min.js"); ?>"></script>
		<script type="text/javascript" language="javascript" src="<?php echo $this->asset("js/jquery/jquery.bxslider.min.js"); ?>"></script>
		<link href="<?php echo $this->asset("css/absen.css"); ?>" rel="stylesheet">
		<link href="<?php echo $this->asset("css/bxslider.css"); ?>" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo $this->asset("image/favicon.ico"); ?>">
		<script type="text/javascript">
			$(document).ready(function() {
				$('.slider8').bxSlider({
					mode: 'vertical',
					slideWidth: 650,
					minSlides: 5,
					maxSlides: 5,
					moveSlides: 2,
					auto: true,
					pager: false,
					speed: 2000,
					pause: 6000
				});
				
				showTime();
			});
			
			function showTime() {
				var today = new Date();
				var d = today.getDay();
				var h = today.getHours();
				var m = today.getMinutes();
				var s = today.getSeconds();
				
				d = checkDay(d);
				h = checkTime(h);
				m = checkTime(m);
				s = checkTime(s);
				
				$("#clock").text(d+", "+h+":"+m+":"+s);
				t=setTimeout('showTime()',1000);
			}
			
			function checkTime(i) {
				if (i<10) {
					i="0" + i;
				}
				return i;
			}
			
			function checkDay(i) {
				if (i==1) {
					i="SENIN";
				} else if (i==2) {
					i="SELASA";
				} else if (i==3) {
					i="RABU";
				} else if (i==4) {
					i="KAMIS";
				} else if (i==5) {
					i="JUMAT";
				} else if (i==6) {
					i="SABTU";
				} else {
					i="MINGGU";
				}
				return i;
			}
		</script>
	</head>

	<body>
		<section class="wrapper">
			<section class="header">
				<div class="logo"></div>
			</section>
			<section class="navigation">
				<h3>Daftar Hadir Pimpinan</h3>
			</section>
		