<?php $this->view("page/header.php", $data); 
?>
<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php echo daftar_hadir  ?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo daftar_hadir ?></span></li>
					</ol>
					<!-- End Breadcrumb -->
		</div>
	</div>
</section>
		<!-- Content -->
<section class="content content-white">
	<div class="container">  
		<section itemscope="" itemtype="http://schema.org/Article">
			<section itemprop="articleBody">
			<!--<h3 class="font-raleway"><?php echo date('F d, Y'); ?></h3>-->
				 <div class="col-md-12"> 						
						<ul class="nav nav-pills nav-pills-color pull-right" id="writeTab">
							<li><a href="#tdos" class="pills-blue active" data-toggle="tab" ><span class="nav-pills-icon fa fa-user"></span>&nbsp;<span class="nav-pills-text">Dosen</span></a></li>
							<li><a href="#tstaff"  class="pills-blue" data-toggle="tab"><span class="nav-pills-icon fa fa-user"></span>&nbsp;<span class="nav-pills-text">Staff</span></a></li>
						</ul>
				</div>
				 <div class="col-md-12"> 	
						<div class="tab-content">
							<div class="tab-pane active fade in" id="tdos">	<?php $this->view('info/absen/dosen.php', $data); ?>	</div>
							<div class="tab-pane fade" id="tstaff"><?php  $this->view('info/absen/staff.php', $data); ?></div>
						</div>
				</div>
			</section>
		</section>
	</div>
</section>
	
<?php $this->view("page/footer.php", $data); ?>