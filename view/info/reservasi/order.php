<?php 
$this->view("page/header.php", $data); 

?>
<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<section itemscope="" itemtype="http://schema.org/Article">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Manajemen Ruang'; else echo "Room Management";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Manajemen Ruang'; else echo "Room Management";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			</section>
		</div>
	</div>
</section>

<section class="content content-white">
	<div class="container">
		<div class="col-md-12">
		<h2 class="title-content margin-top-sm margin-bottom-no font-raleway"><?php echo pemakaian_ruang . " &nbsp;<small><span class='text text-danger'>".date("M d, Y", strtotime($tgl_mulai_select))." - ".date("M d, Y", strtotime($tgl_selesai_select))."</span></small>";
				
					?></h2>
		<div class='block-box'>
			<form class="form-inline" id="form-reservasi" role="form" method="POST" action="<?php echo $this->location("info/ruang") ?>">
				<div class="form-group">
				    <input type="text" value="<?php echo $tgl_mulai_select ?>" name="tgl_mulai" class="pick-date form-control" data-format="YYYY-MM-DD" value="" placeholder="Tanggal Kegiatan" >
				</div>
				<div class="form-group">
				    <input type="text" value="<?php echo $tgl_selesai_select ?>" name="tgl_selesai" class="pick-date form-control" data-format="YYYY-MM-DD" value="" placeholder="Tanggal Kegiatan" >
				</div>
				<div class="form-group">
				    <select class="form-control ruang-select" name="ruang">
				    	<option value=""><?php echo semua. " ". ruang?></option>
				    	<?php 
				    		if($ruang_all) :
					    		foreach($ruang_all as $key) :
									echo "<option value='".$key->ruang_id."'>".$key->kode_ruang."</option>";
								endforeach;
							endif;
						?>
				    </select>
				</div>
				<div class="form-group">
				    <button class="btn btn-default filter-room"><?php echo lihat ?></button>
				</div>
				<div class="form-group">
				    <a href="<?php echo $this->location("module/master/reservasi") ?>" class="btn btn-primary filter-room"><?php echo reservasi ?></a>
				</div>
			</form>
			
			<br>
			<?php
				$kegiatan_arr = array();
				if($kegiatan) :
					foreach($kegiatan as $key){
						$kegiatan_arr[$key->ruang] = array();
					}
					
					foreach($kegiatan as $key){
						$tmp["jam_mulai"] = $key->jam_mulai;
						$tmp["jam_selesai"] = $key->jam_selesai;
						$tmp["kegiatan"] = $key->kegiatan;
						
						array_push($kegiatan_arr[$key->ruang], $tmp);
					}
				endif;
			?>
			
			<!-- <pre><?php echo print_r($kegiatan_arr); ?></pre> -->
			<?php if($ruang) : ?>
				<table class="table table-bordered table-condensed" <?php if($this->config->skin=='ub') echo "style='font-size:70%'";?>>
					<thead>
						<tr>
							<td><?php echo ruang?></td>
							<?php 
								for($i=7; $i<=18; $i++){
									if(strlen($i) == 1) $jam = '0'.$i;
									else $jam = $i;
									
									echo "<td>".$jam.":00</td>";
									echo "<td>".$jam.":30</td>";
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php foreach($ruang as $key) : ?>
							<tr row-ruang="<?php echo $key->ruang_id ?>">
								<td><b>R. <?php echo $key->kode_ruang ?></b></td>
								<?php 
								for($i=7; $i<=18; $i++){
									if(strlen($i) == 1) $jam = '0'.$i;
									else $jam = $i;
									
									if(strlen(($i+1)) == 1) $selesai = '0'.($i+1);
									else $selesai = $i;
									
									echo "<td style='text-align: center'>".get_kegiatan($kegiatan_arr, $jam.':00:00', $selesai . ':29:00', $key->ruang_id)."</td>";
									echo "<td style='text-align: center'>".get_kegiatan($kegiatan_arr, $jam.':30:00', $selesai . ':00:00', $key->ruang_id)."</td>";
								}
							?>
							</tr>	
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else : ?>
				<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
			</div>
		</div>
	</div>	
</section>
<?php 
$this->view("page/footer.php", $data); 


	
	function get_kegiatan($kegiatan, $jam, $selesai, $ruang){
		$nilai = "order_ruang(\"$ruang\",\"$jam\",\"$selesai\")";
		$isi = "<span style='cursor: pointer' class='label label-success' onclick='".$nilai."'><i class='fa fa-check'></i></span>";
		if(! isset($kegiatan[$ruang])) return $isi;
		else{
			if($kegiatan[$ruang]) :
				foreach($kegiatan[$ruang] as $key){
					$date1 = DateTime::createFromFormat('H:i:s', $jam);
					$date_end = DateTime::createFromFormat('H:i:s', $selesai);
					$date2 = DateTime::createFromFormat('H:i:s', $key["jam_mulai"]);
					$date3 = DateTime::createFromFormat('H:i:s', $key["jam_selesai"]);
					
					// $isi = $jam . ' ' . $selesai . ' ' .  $key["jam_mulai"] . ' ' .  $key["jam_selesai"];
					$isi = '';
					if ( ($date1 >= $date2 && $date1 <= $date3) || ($date_end >= $date2 && $date_end <= $date3) )
					{
						$title = $key['kegiatan'] . ' - ' . $ruang . ' (' . substr($key["jam_mulai"],0,5) . ' - ' . substr($key["jam_selesai"],0,5) . ')';
					   	$isi .= '<span style="cursor: pointer" class="label label-danger" title="'.$title.'"><i class="fa fa-times"></i></span>';
						return $isi;
					}
				}
			endif;
			
			$isi = "<span style='cursor: pointer' class='label label-success'  onclick='".$nilai."'><i class='fa fa-check'></i></span>";
			return $isi;
		}
	}
?>

<style>
	#ruang-field .btn-xs{
		margin-bottom: 3px;
	}
	.del-ruang{
		cursor: pointer;
	}
</style>
































