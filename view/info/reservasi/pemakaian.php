<?php 
$this->view("page/header.php", $data); 

?>

<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<section itemscope="" itemtype="http://schema.org/Article">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Manajemen Ruang'; else echo "Room Management";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Manajemen Ruang'; else echo "Room Management";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			</section>
		</div>
	</div>
</section>

  
<section class="content content-white">
	<div class="container">   		
		<div class="col-md-12">	
			<h2 class="title-content margin-top-no margin-bottom-no"><?php echo ruang_terpakai?></h2>
			<div class="content-detail-description margin-bottom-sm">
				<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43"><?php echo date("M d, Y"); ?></time>
				<span class="category"><span class="fa fa-tags"></span><?php echo ruang_terpakai?></span>
			</div>
		</div>
		<div class="col-md-4">
			<?php echo $calendar ?>
			
			<div id="container"></div>
		</div>
		
		<div class="col-md-8">
			<form class="form-horizontal" role="form">
				
				<div class="form-group">
					<div class="col-sm-4">
					    <select class="form-control" onchange="seleksi_kategori(this.value)">
							<option value="kuliah"><?php echo ruang_kuliah ?></option>
							<option value="lab"><?php echo ruang_lab?></option>
							<option value="seminar"><?php echo ruang_seminar?></option>
						</select>
					</div>
					<div class="col-sm-8">
					    <select id="ruang-kuliah" class="form-control seleksi-ruang" onchange="seleksi_ruang(this.value)">
					    	<option value='all'><?php echo semua ?></option>
							<?php
								if($ruang) : 
									foreach($ruang as $key){
										if($key->kategori_ruang == 'kuliah')
										echo "<option value='$key->ruang_id'>".$key->kode_ruang." - $key->lokasi</option>";
									}
								endif;
							?>
						</select>
						
						<select style="display: none" id="ruang-lab" class="form-control seleksi-ruang" onchange="seleksi_ruang(this.value)">
					    	<option value='all'><?php echo semua ?></option>
							<?php
								if($ruang) : 
									foreach($ruang as $key){
										if($key->kategori_ruang == 'lab')
										echo "<option value='$key->ruang_id'>".$key->kode_ruang." - $key->lokasi</option>";
									}
								endif;
							?>
						</select>
						
						<select style="display: none" id="ruang-seminar" class="form-control seleksi-ruang" onchange="seleksi_ruang(this.value)">
					    	<option value='all'><?php echo semua ?></option>
							<?php
								if($ruang) : 
									foreach($ruang as $key){
										if($key->kategori_ruang == 'seminar')
										echo "<option value='$key->ruang_id'>".$key->kode_ruang." - $key->lokasi</option>";
									}
								endif;
							?>
						</select>
					</div>
				</div>
			</form>
			
			<?php 
				$kegiatan = '';
				if($pemakaian) :
					foreach($pemakaian as $key){
						if(! isset($kegiatan[$key->ruang])) $kegiatan[$key->ruang] = array();
						
						$tmp = array(
							'jam_mulai' => $key->jam_mulai,
							'jam_selesai' => $key->jam_selesai,
							'kegiatan' => $key->kegiatan
						);
						
						array_push($kegiatan[$key->ruang], $tmp);
					}
				endif;
				
				?>
					<!-- <pre><?php print_r($kegiatan) ?></pre> -->
				
				<?php
					
				if($ruang) : 
					echo "<table class='table table-bordered table-condensed'>";
						echo "<thead>";
							echo "<tr>";
								echo "<th class='text-center' width='20%'>".ruang."</th>";
								echo "<th class='text-center'>Event</th>";
							echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
							foreach($ruang as $key){
								echo "<tr kategori='$key->kategori_ruang' ruang='$key->ruang_id'>";
									echo "<td>".$key->kode_ruang." ".$key->kategori_ruang."</td>";
									echo "<td>";
										if(isset($kegiatan[$key->ruang_id])) :
											foreach($kegiatan[$key->ruang_id] as $key){
												echo "<i class='fa fa-clock-o'></i> ";
												echo substr($key['jam_mulai'],0,5) . ' - ' . substr($key['jam_selesai'],0,5);
												echo ", <strong>" . $key['kegiatan'] . "</strong>";
												echo "<br>";
											}
										endif;
									echo "</td>";
								echo "</tr>";
							}
						echo "</tbody>";
					echo "</table>";
				endif;
			?>
		</div>
	 </div>
</section>
	
<?php $this->view("page/footer.php", $data);  ?>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		seleksi_kategori('kuliah');
	});
	
	function seleksi_kategori(kategori){
		$("[kategori]").hide();
		$("[kategori='"+kategori+"']").show();
		
		$(".seleksi-ruang").hide();
		$("#ruang-"+kategori).show();
	}
	
	function seleksi_ruang(ruang){
		if(ruang == 'all'){
			$("[ruang]").show();
		}
		else{
			$("[ruang]").hide();
			$("[ruang='"+ruang+"']").show();
		}
	}
</script>

<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Utilisasi Ruangan'
        },
        xAxis: {
            categories: [<?php foreach($kegiatan as $key => $value) echo $key . ', '; ?>]
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Ruangan',
            data: [<?php foreach($kegiatan as $key => $value) : $i=0; foreach($value as $dt) $i++; echo $i . ', '; endforeach; ?>]
        }]
    });
});
</script>



