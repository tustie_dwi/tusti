
			<?php
			 if( isset($posts) ) :	
			?>
			
			<table class='table table-hover table-striped web-page-search'>
				<thead>
				
						<tr><th style="display:none">&nbsp;</th><th>&nbsp;</th></tr>
				</thead>
				<tbody>
				<?php				
					$i = 0;		
					if($posts){			
						foreach ($posts as $dt): 
							$i++;
							?>
							<tr>
								<td style="display:none">&nbsp;</td>
							<td><div class="col-md-7">
							<span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
								echo date("M d, Y", strtotime($dt->tgl_mulai)); 
								if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
									echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
								}
									?>
									</span><br>
									<?php echo "<b>".$dt->judul."</b> <span class='label label-default'>".strToUpper($dt->jenis_id)."</span>"; ?>
									<span class="label label-success"><?php echo ucWords($dt->semester) ?> </span><br>
									<small> 
									 <?php if($dt->penyelenggara){ ?>
									<i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara ?>
									<?php } if($dt->lokasi) { ?>
									&nbsp; <i class="fa fa-map-marker"></i> <?php echo $dt->lokasi ?>	
									<?php } ?>
									</small>
									<?php if($dt->link_berita) echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-search'></i> <b><a href=".$dt->link_berita." target='_blank' class='text text-danger'>Baca Berita</a></b></span></small>" ?>
								</div>
								<div class="col-md-5">						
								<?php 
								
								$mhs = explode(",", $dt->mhs);
								
								
								for($j=0;$j<count($mhs);$j++){ 
									$k = $j+1;
									if($k==count($mhs)){
										$str = " ";
									}else{
										$str = ", ";
									}
									
									echo "<span class='text text-danger'>".$mhs[$j]."</span>".$str;
									
								}
							
								?><br><small><?php echo ucWords($dt->inf_prestasi); ?></small>&nbsp;<span class="label label-danger"><?php echo ucwords($dt->tingkat); ?></span>
								&nbsp;</div>
								</td>
								
							</tr>
							<?php
						endforeach; 
					}
				?>
				</tbody></table>
			<?php
			 else: 
			 ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php endif; ?>
		