<?php 
$this->view("header-v3.php"); 
?>
<section id="wrap" class="mini-side-open">
		<section class="content content-white">
			<div class="container container-content"> 	
				<div class="row">
					<div class="col-md-12">	
		<?php
		if($jadwal){
			foreach($jadwal as $dt) {
				$namamk	= $dt->namamk;
				$kodemk	= $dt->kode_mk;
				$dosen	= $dt->nama;
				
				if(($dt->gelar_awal!="")&&($dt->gelar_awal!="-")){
					$gelarawal= ", ".$dt->gelar_awal;
				}else{
					$gelarawal="";
				}
					
				if(($dt->gelar_akhir!="")&&($dt->gelar_akhir!="-")){
					$gelarakhir= ", ".$dt->gelar_akhir;
				}else{
					$gelarakhir="";
				}
				
				
			}
			
		}else{
			$namamk	= "";
			$kodemk	= "";
			$dosen	= "";
			$gelarawal="";
			$gelarakhir="";
			?>
				<!-- Main Content -->
				<legend class="legend-title-padding-bottom">
					Jadwal Kuliah
					<!--<button class="btn btn-no-radius btn-info pull-right" data-toggle="modal" data-target="#navigationJadwalModal"><span class="fa fa-list"></span>  Navigation</button>	-->						
				</legend>
				<div class="row">
				   <div class="col-md-6 col-md-offset-6" align="right"><?php  $this->view("info/jadwal/navigation.php", $data); ?>		</div>
				</div>
			<?php
			
			echo "Data tidak ditemukan
					</div>
				</div>
			</section>
		</section>";
			
		}

		switch($by){
			case 'hari';
				$strheader = "Hari : ".ucFirst($idhari);
			break;

			case 'dosen':
				$strheader = "Dosen Pengampu : ".$dosen.$gelarawal.$gelarakhir;
			break;

			case 'mk':
				$strheader = $kodemk. " - ". $namamk;
			break;

			case 'prak':
				$strheader = "Praktikum : ".$kodemk. " - ". $namamk;
			break;

			case 'ruang':
				$strheader = "Ruang Kuliah : ".ucFirst($idruang);
			break;

			case 'prodi':
				$strheader = "Program Studi : ".ucWords($idprodi);
			break;
		}
		?>
			
			
			<!-- Main Content -->
			<legend class="legend-title-padding-bottom">				
				<?php echo $strheader; ?>					
			
			</legend>
			<div class="row">
			  <div class="col-md-8"></div>
			  <div class="col-md-4" align="right"><?php  $this->view("info/jadwal/navigation.php", $data); ?>		</div>
			</div>

			<div class="legenda-jadwal">
				<div class="row row-jadwal">
					<div class="alert alert-danger">
						<strong>Perhatian!</strong> Mulai hari <strong>Kamis, 19 September 2013</strong>, perkuliahan di <span class="label label-danger">C1.13</span> dipindahkan ke <span class="label label-danger">5.6 (Perpus)</span>.
					</div>

					<div class="col-md-6">
						<span class="label label-info">&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Informatika / Ilmu Komputer</small>
						<span class="label label-success">&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Sistem Komputer</small>
						<span class="label label-warning">&nbsp;&nbsp;&nbsp;</span>
						<small>Sistem Infomasi</small>
						<span class="label label-danger">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Kelas Tambahan</small>
					</div>

					<div class="col-md-6">
						<code class=" pull-right">Perubahan Penggunaan Ruang Kuliah, Harap Menghubungi Akademik PTIIK.</code>
					</div>
				</div>
			</div>