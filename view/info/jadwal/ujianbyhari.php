	
	<?php
		if($tgl){
				
		?>
		<ul class="nav nav-tabs" id="writeTab">
			<?php
			$k=0;
			foreach($tgl as $dt):
				$k++;
			
				if($k==1){ $str="class=active";  }else{ $str="";}
				?><li <?php echo $str; ?>><a href="#<?php echo date("Ymd", strtotime($dt->tgl)); ?>" data-toggle="tab"><?php echo date("M d, Y", strtotime($dt->tgl)); ?></a></li><?php
			endforeach;
			?>
		</ul>
		<div class="tab-content">	
			<?php			
			$k=0;
			foreach($tgl as $dt):
			$k++;
			
			if($k==1){ $str="active";  }else{ $str="";}
			?>
			<div class="tab-pane fade-in <?php echo $str; ?>" id="<?php echo date("Ymd", strtotime($dt->tgl)); ?>">
				<?php
				
					if($jadwal){					
						foreach($jadwal as $row) {
							$namamk	= $row->namamk;
							$kodemk	= $row->kode_mk;
							$dosen	= $row->nama;
							break;
						}
						switch($by){
							case 'hari';
								$strheader = ucFirst($idhari).", ". date("M d, Y", strtotime($dt->tgl));
							break;

							case 'dosen':
								$strheader = "<a class='btn btn-small disabled' href='#'><i class='icon-time'></i></a> ".date("M d, Y", strtotime($dt->tgl))." <span class='text-warning'> ".$dosen."</span>";
							break;

							case 'mk':
								$strheader = "<a class='btn btn-small disabled' href='#'><i class='icon-time'></i></a> ".date("M d, Y", strtotime($dt->tgl)). " <span class='text-warning'> ". $kodemk. " - ". $namamk."</span>";
							break;

							case 'ruang':
								$strheader = "<a class='btn btn-small disabled' href='#'><i class='icon-time'></i></a> ".date("M d, Y", strtotime($dt->tgl)). " <span class='text-warning'> R. ".ucFirst($idruang)."</span>";
							break;

							case 'prodi':
								$strheader = "Program Studi : ".ucWords($idprodi);
							break;
						}
					?>
					<h4><?php echo $strheader; ?></h4>
					<div class="legenda">
						<div class="alert alert-danger">
							  <button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Perhatian!</strong> Untuk jadwal ujian hari <span class="label label-danger">Jumat</span>, ujian sesi ke <b>2</b> dimulai pukul <span class="label label-danger">09.30</span>.
							</div>
						<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Prodi Informatika</small>
						<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Prodi Sistem Komputer</small>
						<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Prodi Sistem Infomasi</small>
						<span class="label label-inverse">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Project</small>
						<code class="pull-right">Informasi jadwal ujian pada semester aktif.</code>
					</div>
					<div class="table-responsive">
					<table class="table table-bordered table-jadwal">
						<thead>
							<tr class="calendar-row">
								<th class="calendar-day-head"><small>R/J</small></th>
								<?php
									$i=0;
									foreach($jam as $dtjam):
										$i++;
										
										if($idhari=='jumat'):
											if($i==2):
												$mulai = date("H:i", strtotime('-30 minutes', strtotime($dtjam->mulai)));
												$selesai = date("H:i", strtotime('-30 minutes', strtotime($dtjam->selesai)));
											else:
												$mulai = date("H:i", strtotime($dtjam->mulai));
												$selesai = date("H:i", strtotime($dtjam->selesai));
											endif;
											
											echo "<th class='calendar-day-head'><small>".$mulai." - ".$selesai."</small></th>";
										else:
											echo "<th class='calendar-day-head'><small>".$dtjam->mulai." - ".$dtjam->selesai."</small></th>";
										endif;
									endforeach;
								?>
							</tr>
						</thead>
						<tbody>
						<?php
								foreach($ruang as $dtruang):
									echo "<tr><td class='ruang'>".ucFirst($dtruang->value)."</td>";
										$i=0;
										$skip = false;
										for($i = 0; $i< count($jam); $i++){

											if($jam[$i]->is_istirahat==1){
												$class = "istirahat";
											}else{
												$class = "";
											}

											if($idhari!='0'){
												$skip = false;
												foreach($jadwal as $mk) {
													
													if(($mk->jam_mulai == $jam[$i]->mulai) && (strtolower($mk->ruang) == strtolower($dtruang->id)) && ($mk->tgl==$dt->tgl)) {
														$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;

														
														$colspan = $colspan;
														
														
														switch($mk->prodi_id){
															case 'SISKOM':
																$class = "siskom";
															break;
															case 'SI':
																$class = "si";
															break;
															case 'ILKOM':
																$class = "tif";
															break;
														}
													
														if($mk->is_project=='0'){
															echo '<td colspan="'.$colspan.'" class="'.$class.'"><strong>'.$mk->namamk.'</strong><br>'.$mk->nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';
														}else{
															echo '<td colspan="'.$colspan.'" class="'.$class.'"><strong>'.$mk->namamk.'</strong><br><span class="label label-inverse">'.$mk->nama.'</span><br>( Kelas - '.$mk->kelas_id.' )</td>';
														}
														
														
														$i+=$colspan-1;
														$skip = true;
														break;
													}
												}
												if(!$skip) echo "<td class=$class>&nbsp;</td>";
											}else{
												 echo "<td class=$class>&nbsp;</td>";
											}
										}

									echo "</tr>";
								endforeach;
							?>
						</tbody>
					</table>
					<div class="legenda">
						<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Informatika / Ilmu Komputer</small>
						<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Sistem Komputer</small>
						<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Sistem Infomasi</small>
					</div>
					</div>
					<?php 
					
					} else {
							echo "<div class='well'><span class='text text-error'>Data tidak ditemukan</span></div>";
						}
					?>
				</div>
			<?php
			endforeach;
			?>
		</div>
	<?php
	}else{
		echo "<div class='well'><span class='text text-error'>Data tidak ditemukan</span></div>";
	}
	?>
		</div>
			</div>
		</div>
	</section>
</section>
