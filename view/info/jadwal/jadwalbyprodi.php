<div class="row-fluid">
	<?php
		if(count($jadwal) > 0){
			foreach($jadwal as $dt) {
				$namamk	= $dt->namamk;
				$kodemk	= $dt->kode_mk;
				$dosen	= $dt->nama;
				break;
			}
		}else{
			$namamk	= "";
			$kodemk	= "";
			$dosen	= "";
		}

		switch($by){
			case 'hari';
				$strheader = "Hari : ".ucFirst($idhari);
			break;

			case 'dosen':
				$strheader = "Dosen Pengampu : ".$dosen;
			break;

			case 'mk':
				$strheader = $kodemk. " - ". $namamk;
			break;

			case 'prak':
				$strheader = "Praktikum : ".$kodemk. " - ". $namamk;
			break;

			case 'ruang':
				$strheader = "Ruang Kuliah : ".ucFirst($idruang);
			break;

			case 'prodi':
				$strheader = "Program Studi : ".ucWords($idprodi);
			break;
		}
	?>
	<legend><?php echo $strheader; ?></legend>
	<div class="legenda">
	<div class="alert alert-error">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong>Perhatian!</strong> Mulai hari <strong>Kamis, 19 September 2013</strong>, perkuliahan di <span class='label label-important'>C1.13</span> dipindahkan ke <span class='label label-important'>5.6 (Perpus)</span>.
		</div>
		<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<small class="margin">Informatika/Ilmu Komputer</small>
		<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<small class="margin">Sistem Komputer</small>
		<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<small>Sistem Infomasi</small>
		<code class="pull-right">Perubahan Penggunaan Ruang Kuliah, Harap Menghubungi Akademik PTIIK.</code>
	</div>
	<table class="table table-bordered">
		<thead>
			<tr class="calendar-row">
				<th class="calendar-day-head"><small>H/J</small></th>
				<?php
					foreach($jam as $dt):
						echo "<th class='calendar-day-head'><small>".$dt->mulai."</small></th>";
					endforeach;
				?>
			</tr>
		</thead>
		<tbody>
		<?php
					foreach($hari as $dthari):			
						echo "<tr><td><a href='#' data-toggle='tooltip' data-placement='right' title='R. Kelas'><small>".ucFirst($dthari->value)."</small></a></td>";
							$i=0;
							$skip = false;
							for($i = 0; $i< count($jam); $i++){		
							
								if($jam[$i]->is_istirahat==1){
									$class = "istirahat";
								}else{
									$class = "";
								}
								
								$skip = false;
								
								if(count($jadwal)>0){
									foreach($jadwal as $mk) {
										if($mk->jam_mulai == $jam[$i]->mulai && strtolower($mk->hari) == strtolower($dthari->value)) {
											$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;
											
											if($colspan < 2){
												$colspan = $colspan +1;
											}else{
												$colspan = $colspan;
											}
											
											switch($mk->prodi_id){
												case 'SISKOM':
													$class = "siskom";
												break;
												case 'SI':
													$class = "si";
												break;
												case 'ILKOM':
													$class = "tif";
												break;
											}
											if($mk->is_praktikum=='0'){
												echo '<td colspan="'.$colspan.'" class="'.$class.'"><strong>'.$mk->namamk.'</strong><br>'.$mk->nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';
											}else{
												echo '<td colspan="'.$colspan.'" class="'.$class.'"><strong>'.$mk->namamk.'</strong><br><span class="label label-inverse">'.$mk->nama.'</span><br>( Kelas - '.$mk->kelas_id.' )</td>';
											}
											
											$i+=$colspan-1;
											$skip = true;
											break;
										}
									}
									if(!$skip) echo "<td class=$class>&nbsp;</td>";
								}else{
									 echo "<td class=$class>&nbsp;</td>";
								}
							}
						
						echo "</tr>";
					endforeach;	
/*
==========================================
				foreach($ruang as $dtruang):
					echo "<tr><td><a href='#' data-toggle='tooltip' data-placement='right' title='R. Kelas'><small>".ucFirst($dtruang->value)."</small></a></td>";
						$i=0;
						$skip = false;
						for($i = 0; $i< count($jam); $i++){

							if($jam[$i]->is_istirahat==1){
								$class = "istirahat";
							}else{
								$class = "";
							}

							if($idhari!='0'){
								$skip = false;
								foreach($jadwal as $mk) {
									if($mk->jam_mulai == $jam[$i]->mulai && strtolower($mk->ruang) == strtolower($dtruang->id)) {
										$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;

										if($colspan < 2){
											$colspan = $colspan +1;
										}else{
											$colspan = $colspan;
										}

										if($mk->is_praktikum=='0'){
											switch($mk->prodi_id){
												case 'SISKOM':
													$class = "siskom";
												break;
												case 'SI':
													$class = "si";
												break;
												case 'ILKOM':
													$class = "tif";
												break;
											}
										}else{
											$class = "prak";
										}
										echo '<td colspan="'.$colspan.'" class="'.$class.'"><strong>'.$mk->namamk.'</strong><br>'.$mk->nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';

										$i+=$colspan-1;
										$skip = true;
										break;
									}
								}
								if(!$skip) echo "<td class=$class>&nbsp;</td>";
							}else{
								 echo "<td class=$class>&nbsp;</td>";
							}
						}

					echo "</tr>";
				endforeach;
				*/
			?>
		</tbody>
	</table>
	<div class="legenda">
		<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<small class="margin">Informatika/Ilmu Komputer</small>
		<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<small class="margin">Sistem Komputer</small>
		<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<small>Sistem Infomasi</small>
	</div>
</div>		