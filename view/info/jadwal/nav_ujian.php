<!--<div id="navModal" tabindex="-1" role="dialog" aria-labelledby="navModalLabel" aria-hidden="true" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 class="modal-title">Menu Navigasi</h3>
	</div>
	<div class="modal-body">
		<form method="post" class="form-horizontal">
			<div class="control-group">
				<label class="control-label">Hari Aktif</label>
				<div class="controls">
					<select name='selHari' onChange='javascript:OpenPage(this.value)'>
						<option value='-1'>Please Select..</option>
						<?php
						foreach ($hari as $dt):
							echo "<option value='".$this->location().'info/ujian/hari/'.$dt->id."' ";
								if($idhari==$dt->id){
									echo "selected";
								}
								
							echo ">".ucfirst($dt->value)."</option>";
						endforeach;		
						?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Ruang Kuliah</label>
				<div class="controls">
					<select name='selRuang' onChange='javascript:OpenPage(this.value)'>
						<option value='-1'>Please Select..</option>
						<?php
						foreach ($ruang as $dt):
							echo "<option value='".$this->location().'info/ujian/ruang/'.$dt->id."' ";
								if($idruang==$dt->id){
									echo "selected";
								}
								
							echo ">$dt->value</option>";
						endforeach;		
						?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Dosen Pengampu</label>
				<div class="controls">
					<select name='selDosen' onChange='javascript:OpenPage(this.value)'>
						<option value='-1'>Please Select..</option>
						<?php
						foreach ($dosen as $dt):
							echo "<option value='".$this->location().'info/ujian/dosen/'.$dt->id."' ";
								if($iddosen==$dt->id){
									echo "selected";
								}
								
							echo ">$dt->name</option>";
						endforeach;		
						?>
					</select>
				</div>
			</div>
						
			<div class="control-group">
				<label class="control-label">Mata Kuliah</label>
				<div class="controls">
					<select name='selMK' onChange='javascript:OpenPage(this.value)' class="input-xlarge">
						<option value='-1'>Please Select..</option>
						<?php
						foreach ($mk as $dt):
							echo "<option value='".$this->location().'info/ujian/mk/'.$dt->id."' ";
								if($idmk==$dt->id){
									echo "selected";
								}
								
							echo ">".ucfirst($dt->value)."</option>";
						endforeach;		
						?>
					</select>
				</div>
			</div>
			
		</form>
	</div>
</div>-->

<form class="form-inline menu-jadwal-navigation" role="form">
	  <div class="form-group">
		<label class="sr-only" for="cmbselect">Pilih Jadwal Berdasarkan</label>
		<select name='cmbpilih' onChange='javascript:ShowMenuNavJadwal(this.value)' id="cmbselect" class="form-control">
			<option value='-1'>Pilih Jadwal Berdasarkan</option>
			<option value='.jadwal-hari'>Hari</option>
			<option value='.jadwal-ruang'>Ruang Ujian</option>
			<option value='.jadwal-dosen-pembimbing'>Dosen Pengampu</option>
			<option value='.jadwal-mk'>Matakuliah</option>
		</select>
	  </div>
	  <div class="form-group jadwal-hari fade">
		<label class="sr-only" for="cmbselect">Hari</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect" class="form-control">
		<option value='-1'>Please Select..</option>
		<?php
		
		foreach ($hari as $dt):
			echo "<option value='".$this->location().'info/ujian/'.$jenis.'/hari/'.$dt->id."' ";
				if($idhari==$dt->id){
					echo "selected";
				}
				
			echo ">".ucfirst($dt->value)."</option>";
		endforeach;		
		?>
		</select>
	  </div>
	  
	  <div class="form-group jadwal-ruang fade">
		<label class="sr-only" for="cmbselect">Ruang Ujian</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect"  class="form-control">
			<option value='-1'>Please Select..</option>
			<?php
				foreach ($ruang as $dt):
					echo "<option value='".$this->location().'info/ujian/'.$jenis.'/ruang/'.$dt->id."' ";
						if($idruang==$dt->id){
							echo "selected";
						}
						
					echo ">$dt->value</option>";
				endforeach;		
				?>
		</select>
	  </div>
	  
	  <div class="form-group jadwal-dosen-pembimbing fade">
		<label class="sr-only" for="cmbselect">Dosen Pengampu</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect"  class="form-control">
			<option value='-1'>Please Select..</option>
			<?php
			foreach ($dosen as $dt):
				echo "<option value='".$this->location().'info/ujian/'.$jenis.'/dosen/'.$dt->id."' ";
					if($iddosen==$dt->id){
						echo "selected";
					}
					
				echo ">$dt->name</option>";
			endforeach;		
			?>
		</select>
	  </div>
	  
	   <div class="form-group jadwal-mk fade">
		<label class="sr-only" for="cmbselect">Matakuliah</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect"  class="form-control">
			<option value='-1'>Please Select..</option>
			<?php
			foreach ($mk as $dt):
				echo "<option value='".$this->location().'info/ujian/'.$jenis.'/mk/'.$dt->id."' ";
					if($idmk==$dt->id){
						echo "selected";
					}
					
				echo ">".ucfirst($dt->value)."</option>";
			endforeach;		
			?>
		</select>
	  </div>
</form>