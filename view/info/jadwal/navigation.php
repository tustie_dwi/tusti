
<form class="form-inline menu-jadwal-navigation" role="form">
	  <div class="form-group">
		<label class="sr-only" for="cmbselect">Pilih Jadwal Berdasarkan</label>
		<select name='cmbpilih' onChange='javascript:ShowMenuNavJadwal(this.value)' id="cmbselect" class="form-control">
			<option value='-1'>Pilih Jadwal Berdasarkan</option>
			<option value='.jadwal-hari'>Hari</option>
			<option value='.jadwal-ruang'>Ruang Kuliah</option>
			<option value='.jadwal-dosen-pengampu'>Dosen Pengampu</option>
			<option value='.jadwal-mk'>Matakuliah</option>
			<option value='.jadwal-praktikum'>Praktikum</option>
		</select>
	  </div>
	  <div class="form-group jadwal-hari fade">
		<label class="sr-only" for="cmbselect">Hari</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect" class="form-control">
		<option value='-1'>Please Select..</option>
		<?php
		
		if($hari){ $strdata = $hari; }else{$strdata = $chari; }
		
		foreach ($strdata as $dt):
			echo "<option value='".$this->location().'info/jadwal/hari/'.$dt->id."' ";
				if($idhari==$dt->id){
					echo "selected";
				}
				
			echo ">".ucfirst($dt->value)."</option>";
		endforeach;		
		?>
		</select>
	  </div>
	  
	  <div class="form-group jadwal-ruang fade">
		<label class="sr-only" for="cmbselect">Ruang Kuliah</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect"  class="form-control">
			<option value='-1'>Please Select..</option>
			<?php
				if($ruang){ $strruang = $ruang; }else{$strruang= $cruang; }
				
				foreach ($strruang as $dt):
					echo "<option value='".$this->location().'info/jadwal/ruang/'.$dt->id."' ";
						if($idruang==$dt->id){
							echo "selected";
						}
						
					echo ">$dt->value</option>";
				endforeach;		
				?>
		</select>
	  </div>
	  
	  <div class="form-group jadwal-dosen-pengampu fade">
		<label class="sr-only" for="cmbselect">Dosen Pengampu</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect"  class="form-control">
			<option value='-1'>Please Select..</option>
			<?php
			if($dosen){ $strdosen = $dosen; }else{$strdosen = $cdosen; }
			
			foreach ($dosen as $dt):
				echo "<option value='".$this->location().'info/jadwal/dosen/'.$dt->id."' ";
					if($iddosen==$dt->id){
						echo "selected";
					}
					
				echo ">$dt->name</option>";
			endforeach;		
			?>
		</select>
	  </div>

	  <div class="form-group jadwal-mk fade">
		<label class="sr-only" for="cmbselect">Matakuliah</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect"  class="form-control">
			<option value='-1'>Please Select..</option>
			<?php
			foreach ($mk as $dt):
				echo "<option value='".$this->location().'info/jadwal/mk/'.$dt->id."' ";
					if($idmk==$dt->id){
						echo "selected";
					}
					
				echo ">".ucfirst($dt->value)."</option>";
			endforeach;		
			?>
		</select>
	  </div>
	  
	  
	  <div class="form-group jadwal-praktikum fade">
		<label class="sr-only" for="cmbselect">Praktikum</label>
		<select name='cmbpilih' onChange='javascript:OpenPage(this.value)' id="cmbselect"  class="form-control">
			<option value='-1'>Pilih Praktikum..</option>
			<?php
				foreach ($prak as $dt):
					echo "<option value='".$this->location().'info/jadwal/prak/'.$dt->id."' ";
						if($idprak==$dt->id){
							echo "selected";
						}
						
					echo ">".ucfirst($dt->value)."</option>";
				endforeach;		
				?>
		</select>
	 </div>
  
</form>