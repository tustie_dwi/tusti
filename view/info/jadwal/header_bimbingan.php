<?php $this->view("header-v3.php"); //$this->view("navbar-info.php"); 
switch($type){
	case 'hari';
		$strheader = "Hari : ".ucFirst($id);
	break;

	case 'dosen':			
		$strheader = "Dosen Pembimbing : ".$dosenname;
	break;

	case 'ruang':
		$strheader = "Ruang Bimbingan : ".ucFirst($id);
	break;
	
	default:
		$strheader = "Jadwal Bimbingan & Konsultasi";
	break;

}
?>

<section id="wrap" class="mini-side-open">
	<section class="content content-white">
		<div class="container container-content"> 	
			<div class="row">
				<div class="col-md-12">	
	<!-- Main Content -->
			<legend class="legend-title-padding-bottom">				
				<?php echo $strheader; ?>					
			
			</legend>
			<div class="row">
			   <div class="col-md-6 col-md-offset-6" align="right"><?php  $this->view("info/jadwal/nav_bimbingan.php", $data); ?>		</div>
			</div>

			