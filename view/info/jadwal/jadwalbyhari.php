
						<div class="table-responsive">
						<table class="table table-bordered table-jadwal">
							<thead>
								<tr class="calendar-row">
									<th class="calendar-day-head"><small>R/J</small></th>
									<?php
										foreach($jam as $dt):
											echo "<th class='calendar-day-head'><small>".$dt->mulai." - ".$dt->selesai."</small></th>";
										endforeach;
									?>
								</tr>
							</thead>
							<tbody>
							<?php
							$tgl 	= date("Y-m-d");
							$tglval = strtotime(date("Y-m-d"));
	
							foreach($ruang as $dtruang):
								echo "<tr><td class='ruang'>".ucFirst($dtruang->value)."</td>";
									$i=0;
									$skip = false;
									for($i = 0; $i< count($jam); $i++){

										if($jam[$i]->is_istirahat==1){
											$class = "istirahat";
										}else{
											$class = "";
										}

										if($idhari!='0'){
											$skip = false;
											foreach($jadwal as $mk) {
												if($mk->jam_mulai == $jam[$i]->mulai && strtolower($mk->ruang) == strtolower($dtruang->id)) {
													$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;
													
													if($mk->repeat_on==""){
														$drepeat = "monthly";
														$dtmulai 	= $mk->tgl_mulai;
														$dtselesai 	= $mk->tgl_selesai;
														$label		= "<b>".$mk->namamk."</b>";
													}else{
														$drepeat = $mk->repeat_on;
														$dtmulai 	= $mk->tgl_mulai;
														$dtselesai 	=  $mk->tgl_selesai;
														
														$in = strtotime($dtselesai);
														$tglselesai	= strtotime(date("Y-m-d", strtotime('+1 days', $in)));
														
														
														if($drepeat=='daily'){
															if($tglval>$in){
																$label  = "";
															}else{
																$label	= "<b>".$mk->namamk."</b><br><span class='label label-danger'>".$mk->tgl_mulai." *</span>";
															}
														}else{
															$label		= "<b>".$mk->namamk."</b>";
														}
													}
													
													
													if($colspan < 2){
														$colspan = $colspan +1;
													}else{
														$colspan = $colspan;
													}
													
													switch($mk->prodi_id){
														case 'SISKOM':
															$class = "siskom";
														break;
														case 'SI':
															$class = "si";
														break;
														case 'ILKOM':
															$class = "tif";
														break;
													}
													
													if($mk->nama){
														$nama = $mk->nama;
													}else{
														$nama = 'Dosen Pengampu';
													}
							
													if($label){
														if($mk->is_praktikum=='0'){
															echo '<td colspan="'.$colspan.'" class="'.$class.'">'.$label.'<br>'.$nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';
														}else{
															echo '<td colspan="'.$colspan.'" class="'.$class.'">'.$label.'<br><span class="label label-inverse">'.$nama.'</span><br>( Kelas - '.$mk->kelas_id.' )</td>';
														}
													}else{
														echo '<td colspan="'.$colspan.'">&nbsp;</td>';
													}
													
													$i+=$colspan-1;
													$skip = true;
													break;
												}
											}
											if(!$skip) echo "<td class=$class>&nbsp;</td>";
										}else{
											 echo "<td class=$class>&nbsp;</td>";
										}
									}

								echo "</tr>";
							endforeach;
						?>
					</tbody>
				</table>
				<div class="col-md-6">
					<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<small class="margin">Informatika / Ilmu Komputer</small>
					<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<small class="margin">Sistem Komputer</small>
					<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<small>Sistem Infomasi</small>
				</div>
			
			</div>
				</div>
			</div>
		</div>
	</section>
</section>