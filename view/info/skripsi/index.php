<?php $this->view("header-v3.php"); ?>
<section id="wrap" class="mini-side-open">
	<section class="content content-white">
		<div class="container container-content"> 	
		<div class="row">
		
		<legend class="legend-title-padding-bottom"><a href="<?php echo $this->location('module/skripsi'); ?>" class="btn btn-info pull-right">
		<i class="fa fa-edit"></i> New Skripsi</a> Skripsi List
		</legend>
		<?php
		 if( isset($posts) ) :	
		?>
		
		<table class="table table-hover table-striped" id="example">
			<thead>
				<tr>
					<th>&nbsp;</th>	
				</tr>
			</thead>
			<tbody>
			<?php				
				$i = 0;		
				if($posts){			
					foreach ($posts as $dt): 
						$i++;
						?>
						<tr>
							<td>
							<div class="col-md-8">
								<span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php echo date("M d, Y", strtotime($dt->tgl_pengajuan)); ?></span>
								
								<span class="label label-warning"><?php echo $dt->prodi_id; ?></span><br>
								<b><?php echo ucwords(strtoLower($dt->judul_proposal)); ?></b>
								<br>
								<small><i class='fa fa-user'></i>&nbsp;<?php echo ucwords(strtolower($dt->nama)). "&nbsp;<span class='text text-danger'><abbr title='NIM ".$dt->nim."'>".$dt->nim."</abbr></span>"; ?></small>
								
								<?php echo ucWords($dt->semester);?>
							</div>
							<div class="col-md-4">
								<?php //if($dt->nama){ echo $dt->nama; } 
							
								$pembimbing=explode(",", $dt->pembimbing);
								
								for($i=0;$i<count($pembimbing);$i++){
									//echo "<span class='text text-danger'>".$pembimbing[$i]."</span>&nbsp;";
								}
								
								echo "<span class='text text-danger'>".$dt->pembimbing."</span>&nbsp;";
							 ?>
							</div>
							</td>
							
							
						</tr>
						<?php
					endforeach; 
				}
			?>
			</tbody></table>
		<?php
		 else: 
		 ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php endif; ?>
	
		</div>
	</div>
	</section>
</section>
<?php $this->view("footer-v3.php");?>