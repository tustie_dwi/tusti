<?php 
$this->view("page/header.php", $data); 

?>

<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Skripsi'; else echo "Thesis";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'/info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Skripsi'; else echo "Thesis";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
		</div>
	</div>
</section>
<section class="content content-white">
	<div class="container">  
		
	<div class="col-md-12">	
		<?php
		if($posts):
			if(isset($url))	echo '<select  class="form-control" id="cmbskripsi">';
			else echo '<select  class="form-control" id="cmbstmp">'
			?>
			
			
			<option value='-'>Please Select</option>
			<?php
			$i=0;
			foreach($posts as $dt):
				$i++;		
				
				echo "<option value='".$dt->id."'>".$dt->keterangan."</option>";
			endforeach;
			?>
			</select>
			<?php
		endif;
		?>
	
		<span id="loading" class="pull-right" style="display:none">
			<i class="fa fa-refresh fa-spin"></i>Loading data
		</span>
		
		<div id="view-data">
		
		
		</div>
		</div>
 </div>
</section>
	
<?php $this->view("page/footer.php", $data);  ?>

<script>
	$('#cmbskripsi').change(function (e){	
		//$('#view-data').html("");
		
		var formURL = base_url + '/data_skripsi';
				
		var val  = $(document.getElementById("cmbskripsi")).val();
		$.ajax({
		url : formURL,
		type: "POST",
		data : $.param({
			id : val
		}),
		success : function(msg) {	
			$('#view-data').html(msg);
			$("#loading").fadeOut();
			//$('#view-data').show();			
		}
		});
	});
	
	$('#cmbstmp').change(function (e){	
		
		var formURL = base_url + 'info/get_data_skripsi';

		var val  = $(document.getElementById("cmbstmp")).val();
			$.ajax({
				url : formURL,
				type: "POST",
				data : $.param({
					id : val
			}),
			success : function(msg) {	
				
				$('#view-data').html(msg);
				$("#loading").fadeOut();
				
			}
    
		});
	});
</script>