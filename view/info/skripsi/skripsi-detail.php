		
		<h3 class='font-raleway'><?php echo count($detail) ?> data</h3>
		<?php
		if(isset($detail) && $detail){
			?>
			<table class="table web-page-search" id="skripsi">
			<thead>
				<tr style="display:none"><th>&nbsp;</th><th>&nbsp;</th></tr>
			</thead>
			<tbody>
			<?php
			foreach($detail as $dt):
				?>
					<tr>
						<td style="display:none"></td>
						<td >
						<?php 
						if($dt->artikel_id) echo '<a href="'.$this->location('doro/archives/detail/'.$dt->artikel_id).'" style="color:#222"><b>'.$dt->skripsi_judul.'</b></a>';
						else echo '<b>'.$dt->skripsi_judul.'</b>'; 
						echo '&nbsp;<span class="label label-default" style="font-weight:normal">'.$dt->skripsi_jenis.'</span> <span class="label label-success" style="font-weight:normal">'.$dt->skripsi_status.'</span><br>';
						if($dt->mhs_nim) echo '<code>'.$dt->mhs_nim.'</code> <span class="text text-info">'.$dt->nama.'</span>';
						if($dt->dosen1) {
							if(isset($url)){
								echo '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location($url.'/info/staff/'.$dt->id1).'>'.$dt->dosen1;
							}else{
								echo '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location('info/staff/'.$dt->id1).'>'.$dt->dosen1;
							}
							//if($dt->gelar_awal) echo ', '.$dt->gelar_awal;
							//if($dt->gelar_akhir) echo ', '.$dt->gelar_akhir;
							echo '</a>';
						}
						
						if($dt->dosen2){
							if(isset($url)){
								echo '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location($url.'/info/staff/'.$dt->id2).'>'.$dt->dosen2;
							}else{
								echo '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location('info/staff/'.$dt->id2).'>'.$dt->dosen2;
							}
							
							//echo '&nbsp;<i class="fa fa-user"></i> <a href='.$this->location($url.'/info/staff/'.$dt->id2).'>'.$dt->dosen2;
							//if($dt->gelar2) echo ', '.$dt->gelar2;
							//if($dt->akhir2) echo ', '.$dt->akhir2;
							echo '</a>';
						}
						if(strToLower($dt->skripsi_status)=='selesai'){
							echo '<small>';
							if($dt->skripsi_tgl_start!='0000-00-00 00:00:00') echo '<br><b>Seminar Progress</b> <i class="fa fa-calendar"></i> '.date("M d, Y H:i", strtotime($dt->skripsi_tgl_start)).' <i class="fa fa-map-marker"></i> '. $dt->tempat;
							if($dt->skripsi_tgl_semhasil!='0000-00-00 00:00:00') echo '<br><b>Pameran</b> <i class="fa fa-calendar"></i> '.date("M d, Y H:i", strtotime($dt->skripsi_tgl_semhasil)).' <i class="fa fa-map-marker"></i> '. $dt->tempat;
							if($dt->skripsi_tgl_sidang!='0000-00-00 00:00:00') echo '<br><b>Sidang</b> <i class="fa fa-calendar"></i> '.date("M d, Y H:i", strtotime($dt->skripsi_tgl_sidang)).' <i class="fa fa-map-marker"></i> '. $dt->tempat;
							echo '</small>';
						}else{
							echo '<small>';
							if(strToLower($dt->skripsi_status)=='dikerjakan' && ($dt->skripsi_tgl_start!='0000-00-00 00:00:00')) echo '<br><b>Seminar Progress</b> <i class="fa fa-calendar"></i> '.date("M d, Y H:i", strtotime($dt->skripsi_tgl_start)).' <i class="fa fa-map-marker"></i> '. $dt->tempat;
							if(strToLower($dt->skripsi_status)=='pameran' && ($dt->skripsi_tgl_semhasil!='0000-00-00 00:00:00')) echo '<br><b>Pameran</b> <i class="fa fa-calendar"></i> '.date("M d, Y H:i", strtotime($dt->skripsi_tgl_semhasil)).' <i class="fa fa-map-marker"></i> '. $dt->tempat;
							if(strToLower($dt->skripsi_status)=='sidang' && ($dt->skripsi_tgl_sidang!='0000-00-00 00:00:00')) echo '<br><b>Sidang</b> <i class="fa fa-calendar"></i> '.date("M d, Y H:i", strtotime($dt->skripsi_tgl_sidang)).' <i class="fa fa-map-marker"></i> '. $dt->tempat;
							echo '</small>';
						}
						?>
						</td>
					</tr>
				<?php
			endforeach;
			?>
				</tbody>
			</table>
			<?php
		}else{
			echo "<div class='alert alert-danger'>No record found!</div>";
		}
		?>
	<script>
		$(document).ready( function() {
		  $('.web-page-search').dataTable( {
			"fnInitComplete": function(oSettings, json) {
			
			},
			"sDom": "<'row'<'col-md-8'f><'col-md-4'>><'table-responsive't><'row'<'col-md-12 pull-right'p>>",
			"sPaginationType": "bootstrap",
			"aaSorting": [[0, 'desc']],
			"oLanguage": {
				"sLengthMenu": "_MENU_ records",
				"sSearch": ""
			},
			"iDisplayLength" : 5
		  } );
		} )
</script>