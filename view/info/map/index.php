<?php if(!isset($val)) $this->view("page/header.php", $data); ?>

<section class="content content-white">
	<div class="container">  
    <div class="col-md-12">
			
				<legend class="legend-title-padding-bottom">PTIIK Maps 
				<?php
				if( isset($cmblokasi) && ($cmblokasi !='0') ) :	
							$gd = substr($cmblokasi, 0,1);
							$lt = substr($cmblokasi, 1,1);
							
							$loc = "<small> Gedung ".$gd. ", Lt.".$lt."</small>";
							
							echo $loc;
				endif;
							?>
				</legend>
				<div>
				<div class="row">				 
				  <div class="col-md-4 pull-right" align="right">
					<form class="form-inline menu-jadwal-navigation" method=post action="<?php echo $this->location('info/map')?>" role="form"> 		
						<div class="form-group">
							
							<select class="form-control" name="cmblokasi" onChange='form.submit();'>
								<?php
								echo "<option value='0'>Select Location</option>";
								foreach($post as $dt):
									$gedung = substr($dt->id, 0,1);
									$lantai = substr($dt->id, 1,1);
									
									$lokasi = "Gedung ".$gedung. ", Lt.".$lantai;
									echo "<option value='".$dt->id."' ";
										if($cmblokasi==$dt->id){ echo "selected"; }
									echo ">".$lokasi."</option>";
								endforeach;
								?>
							</select>
						</div>				
						
					</form>
					</div>
				</div>
				
					<div class="row">	
					<?php						
						if( isset($cmblokasi) && ($cmblokasi !='0') ) :							
							$gambar = $cmblokasi.".jpg";							
					?>		
							
							<div class="col-md-8">
								<img src="<?php echo $this->config->file_url_view."/upload/file/PTIIK/map/".$gambar; ?>" class="img-responsive img-thumbnail"/> 
							</div>
							<div class="col-md-4">
													
								<ul class="nav nav-tabs" id="writeTab">
									<li class="active"><a href="#tdos" class="pills-blue active" data-toggle="tab" ><span class="nav-pills-text">Dosen</span></a></li>
									<li><a href="#tstaff"  class="pills-blue" data-toggle="tab"><span class="nav-pills-text">Staff</span></a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active fade in" id="tdos">	<?php $this->view('info/map/dosen.php', $data); ?>	</div>
									<div class="tab-pane fade" id="tstaff"><?php  $this->view('info/map/staff.php', $data); ?></div>
								</div>
							</div>
							
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php if(!isset($val)) $this->view("page/footer.php", $data); ?>