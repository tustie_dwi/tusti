<?php 
	if($dosen){
	?>
		<table class='table table-striped table-hover web-page'>
				<thead>
					<tr style="display:none;border:0px;"><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
				</thead>
				<tbody>
			<?php
			foreach($dosen as $dt):
			?>
				<tr style="border-top:0px;">
								<td style="display:none;border:0px;"></td>
							<td style="border-top:0px;">
								<?php 
								$url_content =$url_content = $this->location('info/details/'.$dt->id.'/month');
								
								if($dt->foto) $imgthumb = $this->config->file_url_view."/".$dt->foto; 
								else $imgthumb = $this->config->default_thumb_web;
								?>							
                               
								 <div class="media">
									<a class="pull-left" href="<?php echo $url_content; ?>">
										<img class="media-object img-responsive img-thumbnail" src="<?php echo $imgthumb; ?>" width="50px">
									</a>
									
									 <div class="media-body">
										<a class="title-article" href="<?php  echo $url_content; ?>"><?php echo $dt->nama; 
											if($dt->gelar_awal!=""){ echo ", ".$dt->gelar_awal; } if($dt->gelar_akhir!=""){ echo ", ".$dt->gelar_akhir; } ?></a>
												<small>
												<span class="fa fa-tags"></span>&nbsp;<?php echo $dt->is_status ?><br>
												</small>
										
											<?php
												if($dt->nik!="-"){
														echo strToUpper($dt->is_nik).". ".$dt->nik;
													}
													
													if($dt->jabatan){
													?>
														<span class="text text-info" style='font-weight:normal'><?php echo $dt->jabatan; ?></span>
													<?php
													}	
													
													if($dt->unitkerja){
														$unitk = explode("@", $dt->unitkerja);
													
														for($i=0;$i<count($unitk);$i++){
															$unitid = explode("-", $unitk[$i]);
															
															for($k=0;$k<count($unitid);$k++){							
																if($k==0){											
																	echo "<span class='label label-default' style='font-weight:normal'>".$unitid[$k]."</span> ";								
																}
															}
														}
													
													}

													if($dt->ruangkerja){
														$ruang = explode("@", $dt->ruangkerja);
															
														for($k=0;$k<count($ruang);$k++){				
																										
															echo "<code>R. ".$ruang[$k]."</code> ";									
															
														}
													}							
											
											?>
									</div>
								</div>
						</td></tr>
						<?php
			endforeach;
			?>
			</tbody>
		</table>
	<?php } ?>