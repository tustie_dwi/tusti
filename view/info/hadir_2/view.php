<?php $this->view("header-v3.php", $data);
// $this->view("navbar-info.php"); 

foreach ($posts as $dt):
			$id		= $dt->agenda_id;
			$jenis	= $dt->jenis;
			$mulai	= $dt->tgl_mulai;
			$selesai= $dt->tgl_selesai;
			$judul	= $dt->judul;
			$note	= $dt->keterangan;
			$lokasi	= $dt->lokasi;
			$ruang	= $dt->ruang;
			$prodi	= $dt->prodi;
			$penyelenggara	= $dt->penyelenggara;
			$upenyelenggara	= $dt->unit_penyelenggara;
			$infpeserta	= $dt->inf_peserta;
		endforeach;		
?>

<!-- Content -->
<section id="wrap">
	<section class="content content-white">
		<div class="container container-content"> 	
			<div class="row">
				<div class="col-md-12">
					
					<legend>Agenda PTIIK</legend>
					<div class="control-group">
						
							<ul class="nav nav-pills nav-pills-color" id="writeTab">
								<li><a href="<?php echo $this->location('info/hadir/day'); ?>"  class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text"><?php echo harian ?></span></a></li>
								<li><a href="<?php echo $this->location('info/hadir/week'); ?>"  class="pills-blue"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text"><?php echo mingguan ?></span></a></li>
								<li class="active"><a href="<?php echo $this->location('info/hadir/month'); ?>"  class="pills-orange"><span class="nav-pills-icon fa fa-calendar"></span><span class="nav-pills-text"><?php echo bulanan ?></span></a></li>
							</ul>
											
						</div>
				</div>
				
				<div class="col-md-12">
					<h3>General Info</h3>
					<table class='table table-bordered'>
						<tbody>
							<tr>
								<td width="20%"><em>Jenis Kegiatan</em></td>
								<td><?php if($jenis){ echo $jenis;} else{ echo "Lain-lain";} ?></td>
							</tr>
							<tr>
								<td width="20%"><em>Judul</em></td>
								<td><?php echo $judul; ?></td>
							</tr>
							<tr>
								<td width="20%"><em>Penyelenggara</em></td>
								<td><?php echo $penyelenggara;
								
								if($upenyelenggara){  echo " - ".$upenyelenggara;} ?></td>
							</tr>
							<tr>
								<td width="20%"><em>Keterangan</em></td>
								<td><?php if($note){ echo $note;} else {echo "-";} ?></td>
							</tr>
							<tr>
								<td width="20%"><em>Informasi Peserta</em></td>
								<td><?php if($infpeserta){ echo $infpeserta; } else{ echo "-"; } ?></td>
							</tr>
						</tbody>
					</table>
					
					<h3>Pelaksanaan</h3>
					<table class='table table-bordered'>
						<tbody>
							<tr>
								<td width="20%"><em>Tgl Kegiatan</em></td>
								<td><?php 
								if(date("M d, Y",strtotime($mulai)) == date("M d, Y",strtotime($selesai))){
										echo date("M d, Y",strtotime($mulai));		
									
									}else{
										echo date("M d",strtotime($mulai))." - ".date("d, Y",strtotime($selesai));
									}
									echo "</span>&nbsp;<i class='icon-time'></i> <span class='text text-warning'>".date("H:i",strtotime($dt->tmulai))." - ".date("H:i",strtotime($dt->tselesai))."</span>"; ?></td>
							</tr>
							<tr>
								<td width="20%"><em>Lokasi</em></td>
								<td><?php if($lokasi){echo $lokasi; } else{ echo "-"; } ?></td>
							</tr>
							<tr>
								<td width="20%"><em>Ruang</em></td>
								<td><?php if($ruang){echo $ruang;  } else{ echo "-"; } ?></td>
							</tr>
							
						</tbody>
					</table>
					
				<?php if($pemateri){ ?>
				<h3>Pemateri</h3>
				<table class='table table-bordered'>
						<tbody>
							<tr>
								<td width="20%"><em>Pemateri</em></td>
								<td><?php 
								$nama="";
								if(count($pemateri)!=0){
									$i=0;
									foreach($pemateri as $dt):
										$i++;
										$nama = $nama . $dt->nama .",";
										echo "<span class='label label-info' style='font-weight:normal'>".$dt->nama."</span>&nbsp;";		
										if($dt->instansi){
											echo "<code>".$dt->instansi."</code>&nbsp;";
										}
									endforeach;
									
								}else{
									echo "-";
								}?></td>
							</tr>							
						</tbody>
					</table>
				<?php
				}
				?>
				
				<?php if($undangan){ ?>
				<h3>Undangan</h3>
				<table class='table table-bordered'>
					<tbody>
						<tr>
							<td width="20%"><em>Undangan</em></td>
							<td width="80%">
						
							<ul class="list-inline">
							<?php 
							$nama="";
							if(count($undangan)!=0){
								$i=0;
								foreach($undangan as $dt):
									$i++;
									if($i % 8){
										$str = "&nbsp;";
									}else{
										$str = "</br>";
									}
									
									$nama = $nama . $dt->nama .",";
									echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama."";		
									if($dt->instansi){
										echo "<code>".$dt->instansi."</code>";
									}
									
									echo "</li> ";
								endforeach;
								
							}else{
								echo "-";
							}?></ul></td>
						</tr>							
					</tbody>
				</table>
				<?php
				}
				?>
				
				<?php if($panitia){ ?>
				<h3>Panitia</h3>
				<table class='table table-bordered'>
					<tbody>
						<tr>
							<td width="20%"><em>Panitia</em></td>
							<td><ul class="list-inline"><?php 
							if(count($panitia)!=0){
								$i=0;
								foreach($panitia as $dt):
									$i++;	
									if($i % 8){
										$str = "&nbsp;";
									}else{
										$str = "</br>";
									}
									
									echo "<li class='label label-warning' style='font-weight:normal'>".$dt->nama;		
									if($dt->instansi){
										echo "<code>".$dt->instansi."</code>";
									}	
									echo "</li> ";
								endforeach;							
							}else{
								echo "-";
							}?></ul></td>
						</tr>							
					</tbody>
				</table>
				<?php
				}
				?>
				<h3>Peserta</h3>
				<table class='table table-bordered'>
					<tbody>
						<tr>
							<td width="20%"><em>Dosen/Staff</em></td>
							<td>	<ul class="list-inline"><?php 
							if(count($staff)!=0){
								$i=0;
								foreach($staff as $dt):
									$i++;		
									if($i % 8){
										$str = "&nbsp;";
									}else{
										$str = "</br>";
									}
									
									echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama;		
									if($dt->instansi){
										echo "<code>".$dt->instansi."</code>";
									}	
									echo "</li> ";
								endforeach;
								
							}else{
								echo "-";
							}?></ul></td>
						</tr>		

						<tr>
							<td colspan=2><b>Mahasiswa</b></td></tr>
						<tr>
							<td><em>&nbsp;&nbsp;By Angkatan</em></td>
							<td><ul class="list-inline"><?php 
							
							$nangkatan="";
								if(isset($mangkatan)){
									if(count($mangkatan)!=0){										
										$i=0;										
										foreach($mangkatan as $dt):
											$i++;											
											
											//$nangkatan = $nangkatan .$dt->group_by.",";		
											echo "<li class='label label-info' style='font-weight:normal'>".$dt->group_by."</li> ";	
										endforeach;										
									}
								}
							?>
						</ul></td></tr>
						<tr>
							<td><em>&nbsp;&nbsp;By Nama</em></td>
							<td width="80%"><ul class="list-inline"><?php 
							if(count($mhs)!=0){
								$i=0;
								foreach($mhs as $dt):
									$i++;	
									if($i % 8){
										$str = "&nbsp;";
									}else{
										$str = "</br>";
									}
									echo "<li class='label label-info' style='font-weight:normal'>".ucwords(strTolower($dt->nama));		
									if($dt->instansi){
										echo "<code>".$dt->instansi."</code>";
									}		
									echo "</li> ";
								endforeach;							
							}else{
								echo "-";
							}?></ul></td>
						</tr>	

						<tr>
							<td width="20%"><em>Peserta Luar</em></td>
							<td width="80%"><ul class="list-inline"><?php 
							if(count($peserta)!=0){
							$i=0;
							foreach($peserta as $dt):
								$i++;							
								echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama;		
								if($dt->instansi){
									echo "<code>".$dt->instansi."</code>";
								}	
								echo "</li> ";
							endforeach;							
						}else{
							echo "-";
						}?></ul></td>
						</tr>								
					</tbody>
				</table>
				</div>
		
			</div>
		</div>

	</section>
</section>

 

<?php $this->view("footer-v3.php");?>
