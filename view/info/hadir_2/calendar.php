	
	<?php 

		 $month = $month;    
         $year  = $year; 
	
	?>
	<h3><?php echo $title; ?></h3>
	<div class="row">
    <div class="col-md-6">
	<form class="form-inline" method=post action="<?php echo $this->location('info/details/'.$civitasid)?>">
		<div class="form-group">
			<select class="form-control" name="month" onChange='form.submit();'>
			<?php
				for($x = 1; $x <= 12; $x++)
				{
				
				echo "<option value='".$x."' ";
					if($month==$x){
						echo "selected";
					}
					
				echo ">".date('F',mktime(0,0,0,$x,1,$year))."</option>";
				 
				}

			?>
			</select>
		</div>
		<div class="form-group">
		<?php
		$year_range = 10;
         $selectYear = '<select class="form-control" name="year" onChange="form.submit();">';
         for($x = ($year-floor($year_range/2)); $x <= ($year+floor($year_range/2)); $x++)
         {
             $selectYear.= '<option value="'.$x.'"'.($x != $year ? '' : ' selected="selected"').'>'.$x.'</option>';
         }
         $selectYear.= '</select>';
		 
		 echo $selectYear;		 
		 
		?>
		</div>
		<input type="hidden" name="hidid" value="<?php echo $civitasid ?>">
		<input type=hidden name="hidmonth" value="<?php echo $hidmonth; ?>">
	</form>
	</div>
    
    <div class="col-md-6">
    <div class="text-right-md pull-right">
    <a href="#list" data-toggle="tab" class="btn-mod mod-blue"><span class="btn-mod-icon fa fa-calendar"></span>&nbsp;<span class="btn-mod-text"><?php echo harian ?></span></a>
    <a href="#week" data-toggle="tab" class="btn-mod mod-blue"><span class="btn-mod-icon fa fa-calendar"></span>&nbsp;<span class="btn-mod-text"><?php echo mingguan ?></span></a>
    <a href="#grid" data-toggle="tab" class="btn-mod mod-orange active"><span class="btn-mod-icon fa fa-calendar"></span>&nbsp;<span class="btn-mod-text"><?php echo bulanan ?></span></a>
    </div>
    </div>
    </div>
	<div class="table-responsive">
	<?php
	if( isset($posts) ) :	
		echo $posts;
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
	</div>
