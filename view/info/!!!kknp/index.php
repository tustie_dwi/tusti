<?php $this->view("header-v3.php"); ?>
<section id="wrap" class="mini-side-open">
	<section class="content content-white">
		<div class="container container-content"> 	
		<div class="row">
		
		<legend class="legend-title-padding-bottom"><a href="<?php echo $this->location('module/kknp'); ?>" class="btn btn-info pull-right">
			<i class="fa fa-edit"></i> New KKNP</a> KKNP List
			</legend>
			<?php
			 if( isset($posts) ) :	
			?>
			
			<table class='table table-hover table-striped' id='example'>
				<thead>
					<tr>
						<th>&nbsp;</th>
						
					</tr>
				</thead>
				<tbody>
				<?php				
					$i = 0;		
					if($posts){			
						foreach ($posts as $dt): 
							$i++;
							?>
							<tr>
								<td>
								<div class="col-md-7">
								<i class='fa fa-clock-o'></i>&nbsp;<span class='text text-info'><?php 
								echo date("M d, Y", strtotime($dt->tgl_mulai)); 
								if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
									echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
								}
								
								echo "</span>";
								
								echo "<br><b>";
								if($dt->perusahaan){ echo $dt->perusahaan; }else{ echo "-"; }
								echo "</b> ";
								
								echo "<span class='label label-default'>".strToUpper($dt->is_status)."</span> "; 
								echo "<span class='label label-success'>".ucWords($dt->semester)."</span> "; 
								
								echo "<br><small><i class='fa fa-map-marker'></i> ";
								if($dt->alamat){ echo $dt->alamat; }else{ echo "-"; }
								echo "</small> ";
								
								
								?>
								
								  <?php 
								  
								  if($dt->nama){ echo "&nbsp;<small><i class='fa fa-user'></i></small><code>".$dt->nama."</code>"; }
								  ?>
								</div>
								
								<div class="col-md-5">
								
								<?php 						
								$mhs = explode("@", $dt->mhs);
							
								for($i=0;$i<count($mhs);$i++){
									//$str.= "<span class='label label-info'>".ucwords(strToLower($mhs[$i]))."</span>&nbsp;<br>";
									$mhsaktif = explode("-", $mhs[$i]);
									
									for($k=0;$k<count($mhsaktif);$k++){							
										if($k==0){								
											if((substr($mhs[$i],-1,1)==0)){
												echo "<span class='text text-warning'><abbr title='Dibatalkan'>* ".ucwords(strToLower($mhsaktif[$k]))."</abbr></span>&nbsp; ";
											}else{
												echo "<span class='text text-danger'>".ucwords(strToLower($mhsaktif[$k]))."</span>&nbsp;";
											}
											echo "<small>".ucwords(strToLower($mhsaktif[2]))."</small><br>";
										}
										
										
									}
								}							
								 
								?>
								<?php //if($dt->nama){ echo $dt->nama; } 
								
								$objek = explode(",", $dt->objek);
								if($objek){
									//echo "<blockquote>";
									for($j=0;$j<count($objek);$j++){ 
										if($objek[$j]){
											echo "<span class='label label-danger'>".$objek[$j]."</span> ";
										}
									}
									//echo "</blockquote>";
								}
								 ?>
								
								 </div>
								 </td>
							</tr>
							<?php
						endforeach; 
					}
				?>
				</tbody></table>
			<?php
			 else: 
			 ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
	</section>
</section>
<?php $this->view("footer-v3.php");?>