<?php
				 if( isset($posts) ) :	
					
				?>
				<h4 class="font-raleway"><?php echo count($posts)." Alumni ";?></h4>
				
				<table class="table web-page">
					<thead style="display:none">
						<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
					</thead>
					<tbody>
					<?php				
						$i = 0;		
						if($posts){			
							foreach ($posts as $dt): 
								$i++;
								?>
								<tr style="border-top:0px;">
									<td style="display:none;border:0px;"></td>
									<td style="border-top:0px;">								
										<div class="media">
											 <a class="pull-left" href="#">
												<img class="media-object img-thumbnail" src="<?php echo $dt->foto; ?>"  width="64" height="60">
											  </a>
											<div class="media-body">											
												<b><span class="text text-default"><?php echo $dt->nama; ?></span></b> <span class='text text-danger'><small><?php echo $dt->prodi ?></small></span><br>
												<?php echo "<small>NIM. ".$dt->nim."</small> &nbsp;"; ?><span class="label label-danger"><?php echo $dt->angkatan; ?></span>&nbsp;
												<span class="label label-info"><?php echo $dt->tahun; ?> periode <?php echo substr($dt->periode,'-1',1);?></span>
												<br>
												<small><i class='fa fa-clock-o'></i>&nbsp;<span class='text text-info'><?php 
												echo date("M d, Y", strtotime($dt->tgl_lulus)); 
												echo "</span>";												
												?>
												</small>											
											</div>
										</div>								
									 </td>
								</tr>
								<?php
							endforeach; 
						}
					?>
				</tbody></table>			
			<?php
			 else: 
			 ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php endif; ?>
<script>
	$(document).ready( function() {
	  $('.web-page').dataTable( {
		"fnInitComplete": function(oSettings, json) {
		  //alert( 'DataTables has finished its initialisation.' );
		},
		"sDom": "<'row'<'col-md-8'><'col-md-2'>><'table-responsive't><'row'<'col-md-12 pull-right'p>>",
		"sPaginationType": "bootstrap",
		"aaSorting": [[0, 'desc']],
		"oLanguage": {
			"sLengthMenu": "_MENU_ records",
			"sSearch": ""
		},
		"iDisplayLength" : 5
	  } );
	} )
</script>
	