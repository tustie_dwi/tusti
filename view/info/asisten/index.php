<?php 
$this->view("page/header.php", $data); 

?>

  
<section class="content content-white">
	<div class="container">   		

		<!-- Main Content -->
		
		<div class="col-md-12">	
			<h2 class="title-content margin-top-no margin-bottom-no"><?php echo asisten_lab ?></h2>
			<div class="content-detail-description margin-bottom-sm">
				<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43"><?php echo date("M d, Y"); ?></time>
				<span class="category"><span class="fa fa-tags"></span>asisten</span>
			</div>
		</div>
		
		<!-- SELECT TAHUN -->
		<div class="col-md-12">
			<select id="pilih_thn" class="form-control" onchange="pilih_thn(this.val)">
				<option value="0"><?php echo asisten_lab_thn ?></option>
				<?php 
					foreach($thn_akademik as $th){
						echo "<option value='".$th->tahun_akademik."'";
						echo ">".$th->semester."</option>";
					}
				?>
			</select>
		</div>
		
		<!-- KONTEN -->
		<div class="col-md-12">
			<input id="unitid" value="<?php echo $unit; ?>" type="hidden">
			<input id="url_view" value="<?php echo $this->config->file_url_view; ?>" type="hidden">
			<input id="default_pic" value="<?php echo $this->config->default_pic; ?>" type="hidden">
			<input id="unitid" value="<?php echo $unit; ?>" type="hidden">
			<br><label class="label label-danger no_data" style="display: none">No Data Found!</label>
			<table class="table table-striped web-page" id="asisten_table">
				<thead>
					<tr>
						<th style="width: 500px">Nama</th>
						<th style="min-width: 200px">NIM</th>
						<th style="min-width: 200px">Mata Kuliah</th>
					</tr>
				</thead>
				<tbody id="asisten_data">
				</tbody>
			</table>
		</div>
	
	</div>
</section>
<?php $this->view("page/footer.php", $data);  ?>