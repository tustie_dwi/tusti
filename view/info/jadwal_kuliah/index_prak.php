<?php 
$this->view("page/header.php", $data); 

?>

  
<section class="content content-white">
	<div class="container">   		

	<!-- Main Content -->
	
	<div class="col-md-12">	
		<h2 class="title-content margin-top-no margin-bottom-no"><?php echo jadwal_praktikum ?></h2>
		<div class="content-detail-description margin-bottom-sm">
			<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43"><?php echo date("M d, Y"); ?></time>
			<span class="category"><span class="fa fa-tags"></span>jadwal</span>
		</div>
	</div>
		<div class="col-md-4">
		<select id="pilih_tipe" class="form-control" onchange="pilih_tipe(this.value)">
			<option value="-"><?php echo jadwal_berdasar ?></option>
			<option value="hari"><?php echo hari ?></option>
		
			<option value="praktikum"><?php echo praktikum ?></option>
		</select>
		</div>
		<div class="col-md-6">
		<select id="pilih_hari" style="display: none" class="pilih-form form-control" onchange="jadwal_hari(this.value)">
			<option value="-"><?php echo silahkan_pilih ?></option>
			<option value="senin"><?php echo senin ?></option>
			<option value="selasa"><?php echo selasa ?></option>
			<option value="rabu"><?php echo rabu ?></option>
			<option value="kamis"><?php echo kamis ?></option>
			<option value="jumat"><?php echo jumat ?></option>
			<option value="sabtu"><?php echo sabtu ?></option>
			<option value="minggu"><?php echo minggu ?></option>
		</select>
		
				
		<select id="pilih_praktikum" style="display: none" class="pilih-form form-control" onchange="jadwal_praktikum(this.value)">
			<option value="-"><?php echo praktikum ?></option>
			<?php
				if($praktikum) :
					foreach($praktikum as $key){
						echo "<option value='".$key->mk_id."'>".$key->keterangan." ($key->kode_mk)"."</option>";
					}
				endif;
			?>
		</select>
		</div>
		
		<div class="col-md-12">
			<br>
			<span class="label label-info">&nbsp;&nbsp;&nbsp;</span>
			<small class="margin">Informatika / Ilmu Komputer</small>
			<span class="label label-success">&nbsp;&nbsp;&nbsp;</span>
			<small class="margin">Sistem Komputer</small>
			<span class="label label-warning">&nbsp;&nbsp;&nbsp;</span>
			<small>Sistem Infomasi</small>
			<span class="label label-danger">&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<small>Kelas Tambahan</small>
		</div>
				
		<div class="col-md-12">
		<input id="hari_ini" value="<?php echo strToLower($this->get_hari(date('N'))); ?>" type="hidden">
		<input id="langid" value="<?php echo $lang; ?>" type="hidden">
		<input id="unitid" value="<?php echo $unit; ?>" type="hidden">
		<br>
		<table class="table table-bordered table-condensed">
			<thead class="text-center">
				<tr>
					<td colspan="2">R/J</td>
					<?php
						$jml_jam = 0;
						if($jam) :
							foreach($jam as $key){
								echo "<td>".substr($key->jam_mulai,0,5)." - ".substr($key->jam_selesai,0,5)."</td>";
								$jml_jam++;
							}
						endif;
					?>
					
				</tr>
			</thead>			
			<tbody id="jadwal-wrap">
				
			</tbody>
		</table>
		</div>
		<input id="jml_jam" value="<?php echo $jml_jam ?>" type="hidden">
		

	<!-- End Main Content -->

	<!-- Sidebar -->
	 
	 </div>
</section>
	
<?php $this->view("page/footer.php", $data);  ?>
<script>
	var jam = {
		<?php
			if($jam) :
				foreach($jam as $key){
					echo '"'.substr($key->jam_mulai,0,5) . '":' . $key->urut . ', ';
				}
			endif;
		?>
		akhir : "0"
	};
</script>







