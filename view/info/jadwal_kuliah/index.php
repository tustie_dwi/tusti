<?php 
$this->view("page/header.php", $data); 

?>

<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<section itemscope="" itemtype="http://schema.org/Article">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Jadwal Akademik'; else echo "Academic Schedule";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Jadwal Akademik'; else echo "Academic Schedule";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			</section>
		</div>
	</div>
</section>
  
<section class="content content-white">
	   		
	<section itemscope="" itemtype="http://schema.org/Article">
		<section itemprop="articleBody">
			<div class="<?php if($this->config->skin=='ub') echo "container-fluid";
			else echo "container"
			?>">
				<div class="row">
				<div class="col-md-12">
				<h2 class="title-content <?php if($this->config->skin=='ub') echo "margin-top-sm";
			else echo "margin-top-no"
			?> margin-bottom-no font-raleway">
					<span id="sub-header"></span>
				</h2>
				<div class="col-md-8">
				<select id="pilih_tipe" class="form-control" onchange="pilih_tipe(this.value)">
					<option value="-"><?php echo jadwal_berdasar ?></option>
					<option value="hari"><?php echo hari ?></option>
					<option value="prodi"><?php echo prodi ?></option>
					<option value="ruang"><?php echo ruang_kuliah ?></option>
					<option value="dosen"><?php echo dosen_pengampu ?></option>
					<option value="mk"><?php echo matakuliah ?></option>
					<option value="praktikum"><?php echo praktikum ?></option>
				</select>
				
				<select id="pilih_hari" style="display: none" class="pilih-form form-control" onchange="jadwal_hari(this.value)">
					<option value="-"><?php echo silahkan_pilih ?></option>
					<option value="senin|<?php echo senin ?>"><?php echo senin ?></option>
					<option value="selasa|<?php echo selasa ?>"><?php echo selasa ?></option>
					<option value="rabu|<?php echo rabu ?>"><?php echo rabu ?></option>
					<option value="kamis|<?php echo kamis ?>"><?php echo kamis ?></option>
					<option value="jumat|<?php echo jumat ?>"><?php echo jumat ?></option>
					<option value="sabtu|<?php echo sabtu ?>"><?php echo sabtu ?></option>
					<option value="minggu|<?php echo minggu ?>"><?php echo minggu ?></option>
				</select>
				
				<select id="pilih_prodi" style="display: none" class="pilih-form form-control" onchange="jadwal_prodi(this.value)">
					<option value="-">Please Select</option>
					<?php
						if($prodi) :
							foreach($prodi as $key){
								echo "<option value='".$key->kode."|".$key->unit."'>".$key->kode." - ".$key->unit."</option>";
							}
						endif;
					?>
				</select>
				
				<select id="pilih_ruang" style="display: none" class="pilih-form form-control" onchange="jadwal_ruang(this.value)">
					<option value="-"><?php echo ruang_kuliah ?></option>
					<?php
						if($ruang) :
							foreach($ruang as $key){
								echo "<option value='".$key->ruang_id."|".$key->kode_ruang." - ".$key->keterangan."'>".$key->kode_ruang." - ".$key->keterangan."</option>";
							}
						endif;
					?>
				</select>
				
				<select id="pilih_dosen" style="display: none" class="pilih-form form-control" onchange="jadwal_dosen(this.value)">
					<option value="-"><?php echo dosen_pengampu ?></option>
					<?php
						if($dosen) :
							foreach($dosen as $key){
								echo "<option value='".$key->karyawan_id."|".$key->gelar_awal." ".$key->nama." ".$key->gelar_akhir."'>".$key->nama."</option>";
							}
						endif;
					?>
				</select>
				
				<select id="pilih_mk" style="display: none" class="pilih-form form-control" onchange="jadwal_mk(this.value)">
					<option value="-"><?php echo matakuliah ?></option>
					<?php
						if($mk) :
							foreach($mk as $key){
								echo "<option value='".$key->mk_id."|".$key->keterangan."'>".$key->keterangan." ($key->kode_mk)"."</option>";
							}
						endif;
					?>
				</select>
				
				<select id="pilih_praktikum" style="display: none" class="pilih-form form-control" onchange="jadwal_praktikum(this.value)">
					<option value="-"><?php echo matakuliah ?></option>
					<?php
						if($praktikum) :
							foreach($praktikum as $key){
								echo "<option value='".$key->mk_id."|".$key->keterangan."'>".$key->keterangan." ($key->kode_mk)"."</option>";
							}
						endif;
					?>
				</select>
				</div>
				<div class="col-md-4">
					<span id="loading" class="pull-right">
						<i class="fa fa-refresh fa-spin"></i>Loading schedule data
					</span>
				</div>
				</div>
				</div>
				
				<div class="col-md-12">
					<br>
					<span class="label label-milkom">&nbsp;&nbsp;&nbsp;</span>
					<small class="margin">Magister Ilmu Komputer/Informatika</small>
					<span class="label label-info">&nbsp;&nbsp;&nbsp;</span>
					<small class="margin">Informatika / Ilmu Komputer</small>
					<span class="label label-success">&nbsp;&nbsp;&nbsp;</span>
					<small class="margin">Sistem Komputer</small>
					<span class="label label-warning">&nbsp;&nbsp;&nbsp;</span>
					<small>Sistem Infomasi</small>
					<span class="label label-danger">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<small>Kelas Tambahan</small>
				</div>
						
				<div class="col-md-12">
				<input id="hari_ini" value="<?php echo strToLower($this->get_hari(date('N'))); ?>" type="hidden">
				<input id="langid" value="<?php echo $lang; ?>" type="hidden">
				<input id="unitid" value="<?php echo $unit; ?>" type="hidden">
				<br>
				
				<table class="table table-bordered table-condensed" <?php if($this->config->skin=='ub') echo 'style="font-size:80%"';
			?> >
					<thead class="text-center">
						<tr>
							<td colspan="2">R/J</td>
							<?php
								$jml_jam = 0;
								if($jam) :
									foreach($jam as $key){
										echo "<td>".substr($key->jam_mulai,0,5)." - ".substr($key->jam_selesai,0,5)."</td>";
										$jml_jam++;
									}
								endif;
							?>
							
						</tr>
					</thead>			
					<tbody id="jadwal-wrap">
						
					</tbody>
				</table>
				</div>
				<input id="jml_jam" value="<?php echo $jml_jam ?>" type="hidden">
				

			<!-- End Main Content -->

			<!-- Sidebar -->
			 </div>
			</section>
		</section>
	
</section>
	
<?php $this->view("page/footer.php", $data);  ?>
<script>
	var jam = {
		<?php
			if($jam) :
				foreach($jam as $key){
					echo '"'.substr($key->jam_mulai,0,5) . '":' . $key->urut . ', ';
				}
			endif;
		?>
		akhir : "0"
	};
</script>







