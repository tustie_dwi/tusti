<?php 
$this->view("page/header.php", $data); 

?>
<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<section itemscope="" itemtype="http://schema.org/Article">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php echo jadwal_ujian ?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo jadwal_ujian ?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			</section>
		</div>
	</div>
</section>
  
<section class="content content-white">
	<div class="container">   		
		<section itemscope="" itemtype="http://schema.org/Article">
			<section itemprop="articleBody">
				<!-- Main Content -->
				
				<div class="col-md-12">	
					<h2 class="title-content margin-top-no margin-bottom-no">
						<i class='fa fa-calendar'></i> Monday</span>
						
						<span id="tipe-judul" style="background-color: #EE510A" class="label pull-right">  <?php echo strToUpper($jenis_ujian->jenis_ujian) ?></span>
					</h2>
					
				</div>
					<div class="col-md-4">
						<select id="pilih_jenis" style="display: nonex" class="pilih-form form-control" onchange="jadwal_tipe(this.value)">
							<!--<option value="UTS">UTS</option>-->
							<option value="<?php echo strToUpper($jenis_ujian->jenis_ujian) ?>"><?php echo strToUpper($jenis_ujian->jenis_ujian) ?></option>
						</select>
					</div>
					<div class="col-md-4">
						<select id="pilih_hari" style="display: nonex" class="pilih-form form-control" onchange="jadwal_hari(this.value, '-')">
							<option value="-"><?php echo silahkan_pilih ?></option>
							<option value="senin|<?php echo senin ?>"><?php echo senin ?></option>
							<option value="selasa|<?php echo selasa ?>"><?php echo selasa ?></option>
							<option value="rabu|<?php echo rabu ?>"><?php echo rabu ?></option>
							<option value="kamis|<?php echo kamis ?>"><?php echo kamis ?></option>
							<option value="jumat|<?php echo jumat ?>"><?php echo jumat ?></option>
							<option value="sabtu|<?php echo sabtu ?>"><?php echo sabtu ?></option>
							<option value="minggu|<?php echo minggu ?>"><?php echo minggu ?></option>
						</select>
					</div>
					<div class="col-md-12">
					<br>
					<div class="alert alert-danger">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						  <strong>Perhatian!</strong> <!--Untuk jadwal ujian hari <span class="label label-danger">Jumat</span>, ujian sesi ke <b>2</b> dimulai pukul <span class="label label-danger">09.30</span>.<br>-->Selama bulan puasa, ujian sesi ke 1 dimulai pukul <span class="label label-danger">08.00</span>
						</div>
					</div>
					<div class="col-md-12">
						<span class="label label-milkom">&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Magister Ilmu Komputer/Informatika</small>
						<span class="label label-info">&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Informatika / Ilmu Komputer</small>
						<span class="label label-success">&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Sistem Komputer</small>
						<span class="label label-warning">&nbsp;&nbsp;&nbsp;</span>
						<small>Sistem Infomasi</small>
					</div>
							
					<div class="col-md-12">
					<!-- <input id="hari_ini" value="<?php echo strToLower($this->get_hari(date('N'))); ?>" type="hidden"> -->
					<input id="hari_ini" value="senin" type="hidden">
					<input id="hari_eng" value="Monday" type="hidden">
					<input id="langid" value="<?php echo $lang; ?>" type="hidden">
					<input id="unitid" value="<?php echo $unit; ?>" type="hidden">
					<br>
					
					
					<ul id="myTab" class="nav nav-tabs" role="tablist">
					</ul>
					
					<table class="table table-bordered table-condensed" <?php if($this->config->skin=='ub') echo 'style="font-size:90%"';
			?>>
						<thead class="text-center">
							<tr>
								<td colspan="2">R/J</td>
								<?php
									$jml_jam = 0;
									if($jam) :
										foreach($jam as $key){
											if(substr($key->jam_mulai,0,5)=='07:30'){
												$mulai = '08:00';
											}else{
												$mulai = substr($key->jam_mulai,0,5);
											}
											
											if(substr($key->jam_selesai,0,5)=='09:30'){
												$selesai = '10:00';
											}else{
												$selesai = substr($key->jam_selesai,0,5);
											}
											
											
											echo "<td><b>".$mulai." - ".$selesai."</b></td>";
											$jml_jam++;
										}
									endif;
								?>
								
							</tr>
						</thead>			
						<tbody id="jadwal-wrap">
							
						</tbody>
					</table>
					</div>
					<input id="jml_jam" value="<?php echo $jml_jam ?>" type="hidden">
					

				<!-- End Main Content -->

	<!-- Sidebar -->
			</section>
		</section>
	 </div>
</section>
	
<?php $this->view("page/footer.php", $data);  ?>
<script>
	var jam = {
		<?php
			if($jam) :
				foreach($jam as $key){
					echo '"'.substr($key->jam_selesai,0,5) . '":' . $key->urut . ', ';
				}
			endif;
		?>
		akhir : "0"
	};
	
	var range = {
		<?php
			if($range) :
				foreach($range as $key){
					echo '"'.$key->hari. '": [';
						foreach(explode(",", $key->tgl) as $dt) echo '"'.$dt . '", ';
					echo '], ';
				}
			endif;
		?>
	};
</script>







