<?php 
$this->view("page/header.php", $data); 

?>

  
<section class="content content-white">
	<div class="container">   		

		<!-- Main Content -->
		
		<div class="col-md-12">	
			<h2 class="title-content margin-top-no margin-bottom-no"><?php echo nilai_praktikum ?></h2>
			<div class="content-detail-description margin-bottom-sm">
				<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43"><?php echo date("M d, Y"); ?></time>
				<span class="category"><span class="fa fa-tags"></span>nilai</span>
			</div>
		</div>
		
		<!-- SELECT TAHUN -->
		<div class="col-md-6">
			<input id="unitid" value="<?php echo $unit; ?>" type="hidden">
			<div class="form-group">
				<select id="pilih_thn" class="e9">
					<option value="0"><?php echo thn_akademik ?></option>
					<?php 
						foreach($thn_akademik as $th){
							echo "<option value='".$th->tahun_akademik."'";
							echo ">".ucwords($th->semester)."</option>";
						}
					?>
				</select>
			</div>
			<div class="form-group">
				<select id="pilih_kategori" class="e9">
					<option value="0"><?php echo kategori ?></option>
					<option value="proses">Proses</option>
					<option value="akhir">Akhir</option>
				</select>
			</div>
			<div class="form-group">
				<select id="pilih_mk" class="e9">
					<option value="0"><?php echo matakuliah ?></option>
				</select>
			</div>
			<div class="form-group">
				<select id="pilih_kelas" class="e9">
					<option value="0"><?php echo kelas ?></option>
				</select>
			</div>
		</div>
		
		<!--NILAI-->
		<div class="col-md-6">
			<table class="table table-hover example" id="tbl_penilaian">
				<thead>
					<tr>
						<th>Details</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		
		<div class="col-md-12">
			<div id="daftar_nilai">
				
			</div>
		</div>
	</div>
</section>
<?php $this->view("page/footer.php", $data);  ?>