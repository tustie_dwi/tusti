<?php 
	$parentnum=0;
	$childnum=0;
	$inforow=0;
	if(isset($komponen)){
	 foreach ($komponen as $k) {
		if($k->parent_id=='0'){
			$parentnum++;
			$inforow = $inforow + $k->inforow;
		}
		if($k->parent_id!='0'){
			$childnum++;
		}
	 }
	}
	// echo $nilai_id;
?>

<form method=post name="form" id="form-nilai-mhs" class="form-horizontal" style="background: white;">
	<input type="hidden" name="nilaiid" value="<?php echo $nilai_id ?>" />
	<input type="hidden" name="isproses" value="<?php echo $isproses ?>" />
	<?php if(isset($komponen)){ ?>
	<?php if(isset($mhs)){ ?>
	<table class="table table-bordered" id="myTable">
		<thead>
		<tr>
			<th rowspan="3">Mahasiswa</th>
			<th colspan="<?php echo $childnum+$inforow ?>">Nilai</th>
		</tr>
		<tr>
			<?php 
			foreach ($komponen as $k) { 
			  if($k->parent_id=='0'){
			?>		
			<th <?php if($k->countchild!=0) echo "colspan=' ".$k->countchild." '";else echo "rowspan='2' "; ?> ><?php echo $k->judul." [".round($k->bobot)."%]" ?></th>
			<?php 
			  }
			} ?>
		</tr>
		<tr>		
			<?php 
			foreach ($komponen as $k) { 
			  if($k->parent_id!='0'){
			?>		
			<th><?php echo $k->judul." [".round($k->bobot)."%]" ?></th>
			<?php 
			  }
			} ?>
		</tr>
		</thead>
		<tbody>
			<?php foreach ($mhs as $m) { ?>
			<tr>
				<td>
					<small><?php echo $m->mahasiswa_id; ?></small></b>
					<br><?php echo $m->nama; ?>
				</td>
				<?php $this->get_nilai_mhs_komponen($nilai_id,$m->mahasiswa_id); ?>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	
	<?php }else{
		echo '<div class="well" align="center">';
		echo 'Belum terdapat Mahasiswa Praktik untuk jadwal ini.';
		echo '</div>';
	} ?>
	<?php }else{ ?>
		<div class="well" align="center">
			Belum terdapat komponen untuk jadwal ini.
		</div>
	<?php } ?>
</form>