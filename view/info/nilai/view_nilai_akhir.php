<?php $i=0; ?>
<?php if(isset($komponen)){ ?>
<table class="table table-bordered" id="myTable">
	<thead>
	<tr>
		<th>Mahasiswa</th>
		<th>Nilai Akhir</th>
		<?php 
		 if(isset($komp_akhir)){  $j=0;
			foreach ($komp_akhir as $ka) {
		?>
		<th>
			<?php echo $ka->judul ?> 
			<a href="javascript::" onclick="delete_KA_view('<?php echo $ka->kpn_id ?>','<?php echo $nilai_id_inf[$j]->nilai_id ?>')" class="del-komponenAkhir" style="margin-left: 5px;">
				<i class="fa fa-minus-circle"></i>
			</a>
		</th>
		<?php	$j++;
			}	
		  };
		?>
	</tr>
	</thead>
	<tbody>
		<?php foreach ($komponen as $m) { ?>
		<tr>
			<td>
				<small><?php echo $mhs[$i]->mahasiswa_id ?></small><br>
				<?php echo $mhs[$i]->nama ?>
				<input type="hidden" name="mhsid[]" value="<?php echo $mhs[$i]->mahasiswa_id; ?>" class="form-control" />
			</td>
			<td>
				<input type="text" name="nilaiakhir[]" value="<?php echo substr($m, 0,5); ?>" class="form-control" disabled />
				<input type="hidden" name="type[]" value="nilaiakhir" class="form-control" />
				<input type="hidden" name="kpnid[]" value="<?php //echo $ka->komponen_id ?>" />
				<input type="hidden" name="nilid[]" value="<?php //echo $nilai_id_inf[$k]->nilaiid ?>" />
				<input type="hidden" name="mhsid-komp[]" value="<?php echo $mhs[$i]->mahasiswa_id; ?>" class="form-control" />
			</td>
			<?php 
			 if(isset($komp_akhir)){ $k=0;
				foreach ($komp_akhir as $ka) {
			?>
			<td>
				<?php $this->get_nilai_komponen_akhir($ka->komponen_id, $nilai_id_inf[$k]->nilaiid,$mhs[$i]->mahasiswa_id,'',$NAproses); ?>
				<!-- <input type="text" name="nilaiakhir[]" value="0" class="form-control" /> -->
				<input type="hidden" name="type[]" value="komponenakhir" class="form-control" />
				<input type="hidden" name="kpnid[]" value="<?php echo $ka->komponen_id ?>" />
				<input type="hidden" name="nilid[]" value="<?php echo $nilai_id_inf[$k]->nilaiid ?>" />
				<input type="hidden" name="mhsid-komp[]" value="<?php echo $mhs[$i]->mahasiswa_id; ?>" class="form-control" />
			</td>
			<?php	$k++;
				}	
			  };
			?>
		</tr>
		<?php $i++; } ?>
	</tbody>
</table>
<input type="hidden" name="divider" value="<?php echo (count($komp_akhir)+1) ?>" />
<input type="hidden" name="hidId" value="<?php echo $jadwalhid ?>" />
<input type="hidden" name="nilai_" value="<?php echo $nilai_ ?>" />
<input type="hidden" name="prodi_" value="<?php echo $prodid ?>" />
<?php }elseif(isset($komp_akhir)){ ?>
<!-- kompakhir -->
<table class="table table-bordered" id="myTable">
	<thead>
	<tr>
		<th>Mahasiswa</th>
		<?php 
		 if(isset($komp_akhir)){  $j=0;
			foreach ($komp_akhir as $ka) {
		?>
		<th>
			<?php echo $ka->judul ?> 
			<?php if($ka->judul!='Nilai Akhir'){ ?>
			<a href="javascript::" onclick="delete_KA_view('<?php echo $ka->kpn_id ?>','<?php echo $nilai_id_inf[$j]->nilai_id ?>')" class="del-komponenAkhir" style="margin-left: 5px;<?php if(isset($NAproses)&&$NAproses!=0)echo "display: none;" ?>">
				<i class="fa fa-minus-circle"></i>
			</a>
			<?php } ?>
		</th>
		<?php	$j++;
			}	
		  };
		?>
		<th>
			<center>NA</center>
		</th>
	</tr>
	</thead>
	<tbody>
		
		<?php foreach ($mhs as $m) { ?>
		<tr>
			<td>
				<small><?php echo $m->mahasiswa_id ?></small><br>
				<?php echo $m->nama ?>
				<input type="hidden" name="mhsid[]" value="<?php echo $m->mahasiswa_id; ?>" class="form-control" />
			</td>
			<?php 
			 if(isset($komp_akhir)){ $k=0;
			 	$NA = 0;
				foreach ($komp_akhir as $ka) {
			?>
			<td>
				<?php   
				$NA = $this->get_nilai_komponen_akhir($ka->komponen_id, $nilai_id_inf[$k]->nilaiid,$m->mahasiswa_id,'',$NAproses, $NA); ?>
				<input type="hidden" name="type[]" value="komponenakhir" class="form-control" />
				<input type="hidden" name="kpnid[]" value="<?php echo $ka->komponen_id ?>" />
				<input type="hidden" name="nilid[]" value="<?php echo $nilai_id_inf[$k]->nilaiid ?>" />
				<input type="hidden" name="mhsid-komp[]" value="<?php echo $m->mahasiswa_id; ?>" class="form-control" />
			</td>
			<?php	$k++;
				}	
			  };
			?>
			<td>
				<center><strong><?php echo $NA/$j; ?></strong></center>
			</td>
		</tr>
		<?php $i++; } ?>
	</tbody>
</table>
<input type="hidden" name="divider" value="<?php echo (count($komp_akhir)) ?>" />
<input type="hidden" name="hidId" value="<?php echo $jadwalhid ?>" />
<input type="hidden" name="nilai_" value="<?php echo $nilai_ ?>" />
<input type="hidden" name="prodi_" value="<?php echo $prodid ?>" />
<!-- kompakhir -->	
<?php } ?>