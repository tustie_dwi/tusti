<?php $this->view("page/header.php", $data); 
?>

<SCRIPT language="javascript">
function OpenPage(page) {
	if (page != "-1") {
		window.open(page, '_self');
	}
}
</SCRIPT>
<!-- Content -->
		<!-- Content -->
<section class="content content-white">
	<div class="container">  
    <div class="col-md-12">
		<!--<legend class="legend-title-padding-bottom"><a href="<?php //echo $this->location('info/hadir'); ?>" class="btn btn-info pull-right">
			<i class="fa fa-align-justify"></i> <?php //echo daftar_hadir  ?></a> <?php //echo kehadiran_dan_aktifitas ?>
			</legend>-->
	<legend class="legend-title-padding-bottom"><?php echo kehadiran_dan_aktifitas ?></legend>
			<div class="row kehadiran-header">
		<?php
		
		if($hadir): 
			if($hadir->foto) $foto = $this->config->file_url_view."/".$hadir->foto; 
			else $foto = $this->config->default_thumb_web;
				
				
				?>
				<div class="col-md-12">
					<div class="col-md-2 col-sm-3 col-xs-4">
							<img src="<?php echo $foto; ?>" class='img-thumbnail' width="171" height="180">
					 </div>
					<div class="col-md-10 col-sm-9 col-xs-8">
							<h2 class="kehadiran-nama"><?php 
							echo $hadir->nama; if($hadir->gelar_awal!=""){ echo ", ".$hadir->gelar_awal; } if($hadir->gelar_akhir!=""){ echo ", ".$hadir->gelar_akhir; }	
							echo "&nbsp;<span class='badge badge-info' style='font-weight:normal'>".$hadir->is_status."</span>";									
							echo "</h2>";
														
							if($hadir->nik!="-"){
								echo "<h4>".strToUpper($hadir->is_nik).". ".$hadir->nik."</h4>";
							}
							
							if($hadir->jabatan){
							?>
								<div class="kehadiran-pangkat"><span class="text text-info" style='font-weight:normal'><?php echo $hadir->jabatan; ?></span></div>
							<?php
							}	
							
							if($hadir->unitkerja){
								$unitk = explode("@", $hadir->unitkerja);
							
								for($i=0;$i<count($unitk);$i++){
									$unitid = explode("-", $unitk[$i]);
									
									for($k=0;$k<count($unitid);$k++){							
										if($k==0){											
											echo "<span class='label label-default' style='font-weight:normal'>".$unitid[$k]."</span> ";								
										}
									}
								}
							
							}

							if($hadir->ruangkerja){
								$ruang = explode("@", $hadir->ruangkerja);
									
								for($k=0;$k<count($ruang);$k++){				
																				
									echo "<code>R. ".$ruang[$k]."</code> ";									
									
								}
							}							
											
							?>										
						</div>
					</div>
				<?php
		endif;
		?>
		</div>
		<?php if(isset($type)){
		?>
				<div class="control-group">
				<?php if($type=='month'){ ?>
					
					<div class="tab-content">
						<div class="tab-pane fade" id="list">	<?php $this->view('info/hadir/listview.php', $data); ?>	</div>
						<div class="tab-pane fade" id="week"><?php  $this->view('info/hadir/weekcalendar.php', $data); ?></div>
						<div class="tab-pane active fade in" id="grid"><?php  $this->view('info/hadir/calendar.php', $data); ?></div>
					</div>
				<?php } else {

						if	($type=='week'){		?>
						
						<div class="tab-content">			
							<div class="tab-pane fade" id="list">	<?php $this->view('info/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane active fade in" id="week"><?php  $this->view('info/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/hadir/calendar.php', $data); ?></div>
						</div>
				<?php }else{ ?>
						
						<div class="tab-content">			
							<div class="tab-pane fade in active" id="list">	<?php $this->view('info/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane fade" id="week"><?php  $this->view('info/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/hadir/calendar.php', $data); ?></div>
						</div>

				<?php }
				}
				?>
			</div>
		<?php
		}else{
		?>
		
			<div class="control-group">
				<?php if((isset($_POST['month']))||(isset($_POST['year'])) ){ ?>
					
					<div class="tab-content">
						<div class="tab-pane fade" id="list">	<?php $this->view('info/hadir/listview.php', $data); ?>	</div>
						<div class="tab-pane fade" id="week"><?php  $this->view('info/hadir/weekcalendar.php', $data); ?></div>
						<div class="tab-pane active fade in" id="grid"><?php  $this->view('info/hadir/calendar.php', $data); ?></div>
					</div>
				<?php } else {

						if	((isset($_POST['b_next']))||(isset($_POST['b_prev'])) ||(isset($_POST['b_now']))){		?>
						
						<div class="tab-content">			
							<div class="tab-pane fade" id="list">	<?php $this->view('info/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane active fade in" id="week"><?php  $this->view('info/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/hadir/calendar.php', $data); ?></div>
						</div>
				<?php }else{ ?>
						
						<div class="tab-content">			
							<div class="tab-pane fade in active" id="list">	<?php $this->view('info/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane fade" id="week"><?php  $this->view('info/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('info/hadir/calendar.php', $data); ?></div>
						</div>

				<?php }
				}
				?>
			</div>
		<?php } ?>
	</div>
</div>
</section>
<?php $this->view("page/footer.php", $data);?>