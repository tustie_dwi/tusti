<?php 
$this->view("header-v2.php", $data); ?>

<!-- Content -->
<section id="main-content">
<section class="content content-white">
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="content-divider">
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-md-offset-3">
            
                <div class="content-title">Provide The Best Service
                </div>
                
            </div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
        	<div class="col-md-4">
            	<a class="content-apps content-apps-center prepare-fadeInLeft" href="<?php echo $this->location('info/jadwal');?>">
                <article>
                	<figure><img class="img-apps img-responsive img-circle" src="<?php echo $this->asset('images/jadwal.png');?>" alt="APPS Jadwal Icon"></figure>
                    <div class="content-apps-title">Jadwal Kuliah</div>
                    <p class="content-apps-text">Informasi jadwal perkuliahan pada semester aktif. Pengguna dapat melihat jadwal perkuliahan berdasarkan hari, matakuliah, dosen pengampu atau ruang. 
                    </p>
                    </article>
                </a>
            </div>
        	<div class="col-md-4">
            	<a class="content-apps content-apps-center" href="<?php echo $this->location('info/agenda');?>">
                <article>
                	<figure><img class="img-apps img-responsive img-circle" src="<?php echo $this->asset('images/agenda.png');?>" alt="APPS Agenda Icon"></figure>
                    <div class="content-apps-title">Agenda Kegiatan</div>
                    <p class="content-apps-text">Informasi agenda kegiatan yang sudah berlangsung, sedang berlangsung dan akan berlangsung di PTIIK. Dilengkasi dengan informasi peserta, undangan dan kepanitiaan untuk masing-msing kegiatan tersebut.</p>
                    </article>
                </a>
            </div>
        	<div class="col-md-4">
            <a class="content-apps content-apps-center" href="<?php echo $this->location('info/layanan');?>">
            <article>
                	<figure><img class="img-apps img-responsive img-circle" src="<?php echo $this->asset('images/layanan.png');?>" alt="APPS Permintaan Layanan Icon"></figure>
                    <div class="content-apps-title">Permintaan Layanan</div>
                    <p class="content-apps-text">
                    	Informasi permintaan layanan yang ditujukan ke tiap-tiap unit kerja di PTIIK, dilengkapi dengan informasi <em>progress</em> pekerjaan dari setiap layanan yang diminta. 
                    </p>
                    </article>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="content content-blue">
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="content-divider">
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-md-offset-3">
            
                <div class="content-title">PTIIK Best Service
                </div>
                
            </div>
        </div>
    </div>
    <div class="container content-big-margin-top">
    	<div class="row">
        	<div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/hadir');?>">
                <article>
                	
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/daftar-hadir.png');?>" alt="APPS Daftar Hadir Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Daftar Hadir Civitas</div>
                    <p class="content-apps-text">
                    	Informasi daftar kehadiran civitas PTIIK, yang meliputi dosen dan staff, dilengkapi dengan agenda kegiatan pada masing-masing civitas.
                    </p>
                    </div> 
                    </article>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/jadwal/bimbingan');?>">
                <article>
                
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/daftar-hadir.png');?>" alt="APPS Daftar Hadir Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Jadwal Bimbingan</div>
                    <p class="content-apps-text">
                    	Informasi jadwal bimbingan dan konsultasi dosen pembimbing.
                    </p>
                    </div>
                    </article>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/ruang/tersedia');?>">
                <article>
                
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/manajemen-ruang.png');?>" alt="APPS Manajemen Ruangan Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Ketersediaan Ruangan</div>
                   <p class="content-apps-text">
                    	Informasi ketersediaan ruangan di PTIIK.
                    </p>
                    </div>
                    </article>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/ruang/terpakai');?>">
                <article>
                	
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/manajemen-ruang.png');?>" alt="APPS Manajemen Ruangan Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Pemakaian Ruangan</div>
                    <p class="content-apps-text">
                    	Informasi pemakaian ruangan untuk tiiap-tiap kegiatan yang berlangsung di PTIIK. 
                    </p>
                    </div>
                    </article>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/skripsi');?>">
                <article>                	
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/akademik.png');?>" alt="APPS Pendaftaran Akademik Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Skripsi</div>
                    <p class="content-apps-text">
                    	Informasi daftar skripsi mahasiswa PTIIK beserta <em>progress report</em> dari kegiatan skripsi mahasiswa yang bersangkutan.
                    </p>
                    </div>
                    </article>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/kknp');?>">
                <article>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/akademik.png');?>" alt="APPS Pendaftaran Akademik Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">KKNP</div>
                    <p class="content-apps-text">
                    	Informasi daftar KKNP mahasiswa PTIIK beserta informasi dosen pembimmbing.
                    </p>
                    </div>
                    </article>
                </a>
            </div>
            
            <div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/prestasi');?>">
                <article>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/prestasi.png');?>" alt="APPS Daftar Prestasi Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Daftar Prestasi</div>
                    <p class="content-apps-text">
                    	Informasi prestasi yang sudah dicapai mahasiswa PTIIK pada setiap tahun akademik yang berlangsung.
                    </p>
                    </div>
                </article>                    
                </a>
            </div>
            
            <div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/map');?>">
                <article>
                	
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/manajemen-ruang.png');?>" alt="APPS Manajemen Ruangan Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">PTIIK Maps</div>
                    <p class="content-apps-text">
                    	Denah ruangan di PTIIK. 
                    </p>
                    </div>
                    </article>
                </a>
            </div>
			<div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/akademik/daftar');?>">
                <article>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/akademik.png');?>" alt="APPS Pendaftaran Akademik Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Daftar Ulang</div>
                    <p class="content-apps-text">
                    	Form isian registrasi akademik pada semester aktif.
                    </p>
                    </div>
                    </article>
                </a>
            </div>
			
			<div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/akademik/krs');?>">
                <article>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/akademik.png');?>" alt="APPS Pendaftaran Akademik Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Daftar Semester Pendek</div>
                    <p class="content-apps-text">
                    	Form isian permohonan program semester pendek.
                    </p>
                    </div>
                    </article>
                </a>
            </div>
			
			<div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/akademik/pilihan/mk');?>">
                <article>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/akademik.png');?>" alt="APPS Pendaftaran Akademik Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Form Rancangan MK Pilihan</div>
                    <p class="content-apps-text">
                    	Form rancangan MK pilihan mahasiswa, untuk melihat minat terhadap MK pilihan.
                    </p>
                    </div>
                    </article>
                </a>
            </div>
			
			<div class="col-md-4 col-sm-6">
            	
            	<a class="content-apps" href="<?php echo $this->location('info/wisuda');?>">
                <article>
                    <div class="col-md-4 col-sm-4 col-xs-2">
                    <figure><img class="img-responsive img-rounded" src="<?php echo $this->asset('images/daftar-hadir.png');?>" alt="APPS Pendaftaran Akademik Icon"></figure>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                    <div class="content-apps-title content-apps-title-md">Daftar Alumni</div>
                    <p class="content-apps-text">
                    	Daftar Alumni PTIIK
                    </p>
                    </div>
                    </article>
                </a>
            </div>
        	
        </div>
    </div>
</section>

<section class="content content-white">
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="content-divider">
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-6 col-md-offset-3">
            
                <div class="content-title">Academic Best Service
                </div>
                
            </div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
            <article>
                <div class="content-ready">
                	<div class="big-content-ready">We're almost ready, are you?</div>
                    <div class="med-content-ready">Sign Up for beta access when we launch</div>
                    <div class="content-ready-btn"><a href="<?php echo $this->location("registrasi");?>" class="btn btn-content-ready btn-lg btn-no-radius btn-shadow">Got an account? Register here</a></div>
                </div>
                </article>
            </div>
        </div>
    </div>
</section>
</section>
<?php $this->view("footer-v2.php", $data); 
?>