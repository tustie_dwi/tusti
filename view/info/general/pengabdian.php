
<div class="row">
	<div class="col-md-12">
<?php
if($posts):

?>	<h3 class="font-raleway"><?php echo count($posts)." workshop/lokakarya "; ?></h3>
	<table class="table web-page-search">
		<thead style="display:none">
			<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
		</thead>
		
		<tbody>
			<?php
				foreach($posts as $dt):
					
					
					$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
					
					if(isset($url)):
						if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content);
						else $url_content = $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
					else:
						if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content);
						else $url_content = $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
					endif;
											
					?>
					<tr style="border-top:0px;">
						<td style="display:none;border:0px;"></td>
							<td style="border:0px;">	
								<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;line-height:1.6;color:#444;text-align:justify">							
							<?php 
							$peserta = explode("@", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
								$strpeserta = explode("-",$peserta[$j]);
								$npeserta = UcWords(strToLower(reset($strpeserta)));
								$ketua = end($strpeserta);
								if($ketua):
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								else:
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								endif;
							}
							
							if(count($peserta)>1) echo  ".".$dt->tahun. ". ";
							else echo $dt->tahun. ". ";
							
							if($dt->jurnal_link)  echo "<a href='#' target='_blank'><b>".$dt->judul."</b></a>. ";					
							else echo "<b>".$dt->judul."</b>. ";
							
							?>
							</div>
					</td></tr>
			<?php
					
				endforeach;
				?>
			</tbody>
		</table>	
	<?php
	endif;

?>
	</div>
</div>
