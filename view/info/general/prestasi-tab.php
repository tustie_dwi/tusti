		<div class="col-md-8">

					<table class="table table-hover web-page">
					<thead>
						<tr>
							<th>&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
					<?php
					$mdosen = new model_info();
					$prestasi = $mdosen->get_rekap_prestasi($posts->karyawan_id);
						
					if($prestasi):
						foreach($prestasi as $dt):
							
						?>
					<tr>
						<td>
							<div class="col-md-8">
								<small><span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
									echo date("M d, Y", strtotime($dt->tgl_mulai)); 
									if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
										echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
									}
								?>
								</span></small><br>
								<?php echo "<b>".$dt->judul."</b> &nbsp; ";  if($dt->penghargaan) echo "<span class='text text-success'><i class='fa fa-bookmark'></i> ".$dt->penghargaan."</span>"; ?><br>
								 
								<small><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara ?>
								&nbsp; <i class="fa fa-map-marker"></i> <?php echo $dt->lokasi ?></small>
								<?php if($dt->link_berita) echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-search'></i> <b><a href=".$dt->link_berita." target='_blank' class='text text-danger'>Baca Berita</a></b></span></small>" ?>
							
							</div>
							<div class="col-md-4">
							
							<?php 
							
							$peserta = explode(",", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
									
								echo "<span class='text text-danger'>".$peserta[$j]."</span>".$str;
							}								
							?><br><small><?php echo $dt->inf_prestasi; ?></small>&nbsp;<span class="label label-danger"><?php echo ucwords($dt->tingkat); ?></span>
							<span class="label label-default"><?php echo ucwords($dt->jenis_prestasi); ?></span></div>
						</td>
					</tr>
					<?php endforeach ?>
					</tbody>
						<?php endif; ?>
					</table>					
				</div>
	