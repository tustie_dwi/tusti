		<div class="col-md-12">
			<h2><?php echo penelitian ?></h2>
				<?php list_data($data['posts']); ?>
				<h2><?php echo publikasi ?></h2>		
				<?php list_data($data['posts'], "publikasi"); ?>				
		</div>
	<?php
	function list_data($posts=NULL, $str=NULL){
	?>
	<table class="table table-hover web-page">
			<thead>
				<tr>
					<th>&nbsp;</th>	
				</tr>
			</thead>
			<tbody>
			<?php
			$minfo = new model_info();
			$penelitian = $minfo->get_rekap_penelitian($str, $posts->karyawan_id);
				
			if($penelitian):
				foreach($penelitian as $dt):
					
				?>
			<tr>
				<td>
					<div class="col-md-8">
						
						<?php echo "<b>".$dt->judul."</b> &nbsp; ";  ?>
						 
						<i class="fa fa-check-square-o"></i> <?php echo $dt->tahun. " ". $dt->is_ganjil. " ".$dt->is_pendek;
							echo " <span class='label label-default'> ".$dt->status."</span>";								?>
													
					</div>
					<div class="col-md-4">
					
					<?php 
					
					$peserta = explode(",", $dt->peserta);
					
					for($j=0;$j<count($peserta);$j++){ 
						$k = $j+1;
						if($k==count($peserta)){
							$str = " ";
						}else{
							$str = ", ";
						}
						$strpeserta = explode("-",$peserta[$j]);
						$npeserta = reset($strpeserta);
						$ketua = end($strpeserta);
						echo "<span class='text text-danger'>".$npeserta ;
						if($ketua) echo " *";
						echo "</span>".$str;
					}								
					?></div>
				</td>
			</tr>
			<?php endforeach ?>
			</tbody>
				<?php endif; ?>
			</table>

	<?php
	}
	?>