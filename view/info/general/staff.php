
<?php 
if(isset($unit_list)):
	if(strToLower($unit_list->strata)=='s2')	$this->view("page/s2/header-s2.php", $data); 
	else $this->view("page/header.php", $data); 
else:
$this->view("page/header.php", $data); 
endif;
?>

  
<section class="content content-white">
	<div class="container">  
	<!-- Main Content -->
	<div class="col-md-12">	
		<!-- Edit Breadcrumb -->
			<ol class="breadcrumb" itemprop="breadcrumb">
			  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
				else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
				
			  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo tentang ?></span></li>
			   <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo $posts->nama;
				if($posts->gelar_awal!=""){ echo ", ".$posts->gelar_awal; } if($posts->gelar_akhir!=""){ echo ", ".$posts->gelar_akhir; } 
			   ?></span></li>
			</ol>
			<!-- End Breadcrumb -->
					
		<section itemscope="" itemtype="http://schema.org/Article">
			<div class="profile-page" style="line-height:1.8">
			<?php
			 if($posts->foto) $imgthumb = $this->config->file_url_view."/".$posts->foto; 
			else $imgthumb = $this->config->default_thumb_web;
			
			if($lang=='en')	$about = $posts->about_en;								
			else $about = $posts->about;
			?>
						<div class="profile-header">
							<div class="row">
								<div class="col-sm-4 col-md-2">
									<a href="<?php echo $imgthumb; ?>" class="fancybox">
										<figure class="text-center-sm">
											<img src="<?php echo $imgthumb; ?>" class="img img-responsive img-responsive-center">
										</figure>
									</a>
								</div>
								<div class="col-sm-8 col-md-10">
									<h2 class="margin-top-sm"><?php echo $posts->nama;
											if($posts->gelar_awal!=""){ echo ", ".$posts->gelar_awal; } if($posts->gelar_akhir!=""){ echo ", ".$posts->gelar_akhir; } 
										   ?>				
										<small>
											<?php if($posts->jabatan){
												$jabatan = explode("-",$posts->jabatan);
												
												if($lang=='en') $jabatan_= $jabatan[1];
												else $jabatan_= $jabatan[0];
												?>
													<span class="text text-info" style='font-weight:normal'> <?php echo $jabatan_; ?></span>
												<?php
												}?>
											
										</small>
									</h2>
									<h5><?php if(($posts->nik)&&($posts->nik!="-")){
											echo strToUpper($posts->is_nik).". ".$posts->nik."<br>";
										}?></h5>
									<div class="row">
										<div class="col-md-6">
											<ul class="profile-list list-unstyled">
												<?php
												if($lang=='en') $n=1;
												else $n =0;
															
												if($posts->unitkerja){
													$unitk = explode("@", $posts->unitkerja);
												
													for($i=0;$i<count($unitk);$i++){
														$unitid = explode("-", $unitk[$i]);
														
														for($k=0;$k<count($unitid);$k++){	
															
															if($k==$n){	
																$link_= $this->get_unit_url($unitid[$k]);
																
																?>
																<li><span class="fa fa-check-square-o"></span><?php if($link_){ ?><a href="<?php echo $link_ ?>"><?php echo $unitid[$k]; ?></a><?php }else{ echo $unitid[$k];} ?></li>
																<?php
																//echo "<span class='label label-default' style='font-weight:normal'>".$unitid[$k]."</span> ";								
															}
														}
													}
												
												}
												?>
																						
											</ul>
										</div>
										<div class="col-md-6">
											<ul class="profile-list list-unstyled">
												<li>
												<?php if($lang=='en'){
												echo "<span class='fa fa-building-o'></span> PTIIK, 8 Veteran Road";
												if($posts->ruangkerja){
															$ruang = explode("@", $posts->ruangkerja);																		
															for($k=0;$k<count($ruang);$k++){																											
																echo "<br><span style='margin-left:25px'><small>".substr($ruang[$k],0,1)." building, ".substr($ruang[$k],1,1)."st floor, Room ".$ruang[$k]."</small></span>";	
																
															}
														}			
												}else{ ?>
													<span class="fa fa-building-o"></span>PTIIK, Jl. Veteran No 8 <?php if($posts->ruangkerja){
															$ruang = explode("@", $posts->ruangkerja);																		
															for($k=0;$k<count($ruang);$k++){																											
																echo "<br><span style='margin-left:25px'><small>Gedung ".substr($ruang[$k],0,1).", Lt.".substr($ruang[$k],1,1).", R. ".$ruang[$k]."</small></span>";	
																
															}
														}			
												} ?></li>
												<li><span class="fa fa-envelope-o"></span><?php 
												$str = explode("@", $posts->email);
												$strname = reset($str);
												$strend = end($str);
												 $stremail = str_replace(".", " [dot] ", $strend);
												 echo $strname. " [at] ". $stremail;
												//echo str_replace("@", " [at] ", $stremail); ?></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php if ($posts->is_status=='dosen'): ?>
						<div class="profile-body">
							<div class="row">
							<?php 
							
							
							if(isset($posts) && $about): ?>
								<div class="col-md-12">
									<br>
									<!--<h2 class="title-content title-underline title-underline-orange margin-bottom-sm"><span><?php echo tentang ?></span></h2>-->
									<div class="profile-content"><?php 
											 if(isset($posts)&& $about) echo $about;
											 else echo "-";
											 ?>
									</div>
									<!--<hr class="hr-profile margin-top margin-bottom">-->
								</div>
								
								<?php endif; ?>
								
								<?php /* if(isset($posts) && $posts->interest):?>
								<div class="col-md-12">
									<h2 class="title-content title-underline title-underline-orange margin-bottom-sm margin-top-sm"><span>Interest</span></h2>
									<div class="profile-content"><?php 
										 if(isset($posts) && $posts->interest) echo $posts->interest;
										 else echo "-";
										 ?>
									</div>
									<!--<hr class="hr-profile margin-top margin-bottom">-->
								</div>
								<?php 
								else:
									echo "<br>";
								endif;*/?>
								
								
								<div class="col-md-12"> <br>
									<ul class="nav nav-pills nav-pills-color" id="writeTab">												
										<li class="active"><a href="#bio" role="tab" data-toggle="tab"><?php  echo biografi ?></a></li>
										<li><a href="#publikasi" role="tab" data-toggle="tab"><?php  echo publikasi ?></a></li>
										<li><a href="#penelitian" role="tab" data-toggle="tab"><?php  echo penelitian ?></a></li>
										<li><a href="#afiliasi" role="tab" data-toggle="tab"><?php  echo afiliasi ?></a></li>													
										<li><a href="#prestasi" role="tab" data-toggle="tab"><?php  echo penghargaan ?></a></li>
										<li><a href="#seminar" role="tab" data-toggle="tab"><?php  echo aktifitas_ilmiah ?></a></li>
										<li><a href="#teknis" role="tab" data-toggle="tab"><?php  echo laporan_teknis ?></a></li>		
									</ul>

									<div class="tab-content">	
										<div class="tab-pane active" id="bio">	<div style="padding:8px 0 8px;line-height:1.6;color:#444"><?php 
										if($lang=='en')	$bio = $posts->biografi_en;								
										else $bio = $posts->biografi;
										
										echo $bio;
										?></div></div>
										<div class="tab-pane" id="publikasi"><?php list_data($lang,$minfo,$posts,'publikasi'); ?></div>	
										<div class="tab-pane" id="penelitian">	<?php list_data($lang, $minfo,$posts,''); ?></div>
										<div class="tab-pane" id="afiliasi"><?php list_data_afiliasi($lang,$minfo,$posts); ?></div>
										<div class="tab-pane" id="prestasi"><?php list_data_prestasi($minfo,$posts); ?></div>
										<div class="tab-pane" id="seminar"><?php list_data_kegiatan($minfo,$posts); ?></div>
										<div class="tab-pane" id="teknis"><?php list_data($lang,$minfo,$posts,'technical reports'); ?></div>
									</div>					
								</div>
								
						
						</div>
					</div>
				<?php endif; ?>
		
			</div>	
		</section>
	</div>
	<!-- End Main Content -->
	</div>
</section>
	
<?php 
if(isset($unit_list)):
	if(strToLower($unit_list->strata)=='s2')	$this->view("page/s2/footer-s2.php", $data); 
	else $this->view("page/footer.php", $data); 
else:
$this->view("page/footer.php", $data); 
endif;
	
	function list_data($lang=NULL, $minfo=NULL,$posts=NULL, $str_=NULL){
	
		//$minfo = new model_info();
		if($str_):
			if($str_=='publikasi'):
				$penelitian_ = $minfo->get_rekap_penelitian($str_, $posts->karyawan_id, 'jurnal', 'internasional');
					
				if($penelitian_):
					if($lang=='en')	echo "<h4>International Journal Papers</h4>";
					else echo "<h4>Jurnal Internasional</h4>";
					
					foreach($penelitian_ as $dt):			
							
							?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;line-height:1.6;color:#444;text-align:justify">
							<?php					
							$peserta = explode("@", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
								$strpeserta = explode("-",$peserta[$j]);
								$npeserta = reset($strpeserta);
								$ketua = end($strpeserta);
								if($ketua):
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								else:
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								endif;
							}
							echo  $dt->tahun. ". ";
							if($dt->jurnal_link)  echo "<a href='".$dt->jurnal_link."' target='_blank'><b>".$dt->judul."</b></a>. ";					
							else echo "<b>".$dt->judul."</b>. ";
							if(strToLower($dt->kategori_publikasi)=='seminar'){
								if($dt->editor)		echo "<br>In: ".$dt->editor." (eds) ";
								if($dt->jurnal_nama)	echo ", ".$dt->jurnal_nama.", ";
								if($dt->jurnal_edisi) echo $dt->jurnal_edisi.", ";
								if($dt->jurnal_volume) echo "vol.".$dt->jurnal_volume.", ";
								if($dt->jurnal_page) echo "pp. ".$dt->jurnal_page.". ";
								echo "<em>".$dt->nama_kegiatan."</em>. ".$dt->lokasi.", ".date("M Y", strtotime($dt->tgl_pelaksanaan)). ". ";
									if ($dt->jurnal_lokasi) echo $dt->jurnal_lokasi." : ";
								if($dt->jurnal_penerbit) echo $dt->jurnal_penerbit;
							}else{
								if($dt->jurnal_nama) echo $dt->jurnal_nama. ", ". $dt->jurnal_edisi;
								if($dt->jurnal_volume) echo " vol. ".$dt->jurnal_volume;
								if($dt->jurnal_page) echo ", pp. ". $dt->jurnal_page;
							}
							?>
							</div>
							<?php
					endforeach;
					echo "<br>";
				endif;
				
				$prosiding_ = $minfo->get_rekap_penelitian($str_, $posts->karyawan_id, 'seminar', 'internasional');
					
				if($prosiding_):
					if($lang=='en')	echo "<h4>International Conference Papers</h4>";
					else echo "<h4>Prosiding Internasional</h4>";
					
						foreach($prosiding_ as $dt):			
							
							?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;line-height:1.6;color:#444;text-align:justify">
							<?php					
							$peserta = explode("@", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
								$strpeserta = explode("-",$peserta[$j]);
								$npeserta = reset($strpeserta);
								$ketua = end($strpeserta);
								if($ketua):
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								else:
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								endif;
							}
							echo  $dt->tahun. ". ";
							if($dt->jurnal_link)  echo "<a href='".trim($dt->jurnal_link)."' target='_blank'><b>".$dt->judul."</b></a>. ";					
							else echo "<b>".$dt->judul."</b>. ";
							if(strToLower($dt->kategori_publikasi)=='seminar'){
								if($dt->editor)		echo "<br>In: ".$dt->editor." (eds) ";
								if($dt->jurnal_nama)	echo ", ".$dt->jurnal_nama.", ";
								if($dt->jurnal_edisi) echo $dt->jurnal_edisi.", ";
								if($dt->jurnal_volume) echo "vol.".$dt->jurnal_volume.", ";
								if($dt->jurnal_page) echo "pp. ".$dt->jurnal_page.". ";
								echo "<em>".$dt->nama_kegiatan."</em>. ".$dt->lokasi.", ".date("M Y", strtotime($dt->tgl_pelaksanaan)). ". ";
									if ($dt->jurnal_lokasi) echo $dt->jurnal_lokasi." : ";
								if($dt->jurnal_penerbit) echo $dt->jurnal_penerbit;
							}else{
								if($dt->jurnal_nama) echo $dt->jurnal_nama. ", ". $dt->jurnal_edisi;
								if($dt->jurnal_volume) echo " vol. ".$dt->jurnal_volume;
								if($dt->jurnal_page) echo ", pp. ". $dt->jurnal_page;
							}
							?>
							</div>
							<?php
					endforeach;
					echo "<br>";
				endif;
				
				$penelitian_in = $minfo->get_rekap_penelitian($str_, $posts->karyawan_id, 'jurnal', 'nasional');
					
				if($penelitian_in):
					
					if($lang=='en')	echo "<h4>National Journal Papers</h4>";
					else echo "<h4>Jurnal Nasional</h4>";
					
					foreach($penelitian_in as $dt):			
							
							?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;line-height:1.6;color:#444;text-align:justify">
							<?php					
							$peserta = explode("@", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
								$strpeserta = explode("-",$peserta[$j]);
								$npeserta = reset($strpeserta);
								$ketua = end($strpeserta);
								if($ketua):
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								else:
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								endif;
							}
							echo  $dt->tahun. ". ";
							if($dt->jurnal_link)  echo "<a href='".trim($dt->jurnal_link)."' target='_blank'><b>".$dt->judul."</b></a>. ";					
							else echo "<b>".$dt->judul."</b>. ";
							if(strToLower($dt->kategori_publikasi)=='seminar'){
								if($dt->editor)		echo "<br>In: ".$dt->editor." (eds) ";
								if($dt->jurnal_nama)	echo ", ".$dt->jurnal_nama.", ";
								if($dt->jurnal_edisi) echo $dt->jurnal_edisi.", ";
								if($dt->jurnal_volume) echo "vol.".$dt->jurnal_volume.", ";
								if($dt->jurnal_page) echo "pp. ".$dt->jurnal_page.". ";
								echo "<em>".$dt->nama_kegiatan."</em>. ".$dt->lokasi.", ".date("M Y", strtotime($dt->tgl_pelaksanaan)). ". ";
									if ($dt->jurnal_lokasi) echo $dt->jurnal_lokasi." : ";
								if($dt->jurnal_penerbit) echo $dt->jurnal_penerbit;
							}else{
								if($dt->jurnal_nama) echo $dt->jurnal_nama. ", ". $dt->jurnal_edisi;
								if($dt->jurnal_volume) echo " vol. ".$dt->jurnal_volume;
								if($dt->jurnal_page) echo ", pp. ". $dt->jurnal_page;
							}
							?>
							</div>
							<?php
					endforeach;
					echo "<br>";
				endif;
				
				
				$prosiding_in = $minfo->get_rekap_penelitian($str_, $posts->karyawan_id, 'seminar', 'nasional');
					
				if($prosiding_in):
					
					if($lang=='en')	echo "<h4>National Conference Papers</h4>";
					else echo "<h4>Prosiding Nasional</h4>";
					
						foreach($prosiding_in as $dt):			
							
							?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;line-height:1.6;color:#444;text-align:justify">
							<?php					
							$peserta = explode("@", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
								$strpeserta = explode("-",$peserta[$j]);
								$npeserta = reset($strpeserta);
								$ketua = end($strpeserta);
								if($ketua):
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								else:
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								endif;
							}
							echo  $dt->tahun. ". ";
							if($dt->jurnal_link)  echo "<a href='".trim($dt->jurnal_link)."' target='_blank'><b>".$dt->judul."</b></a>. ";					
							else echo "<b>".$dt->judul."</b>. ";
							if(strToLower($dt->kategori_publikasi)=='seminar'){
								if($dt->editor)		echo "<br>In: ".$dt->editor." (eds) ";
								if($dt->jurnal_nama)	echo ", ".$dt->jurnal_nama.", ";
								if($dt->jurnal_edisi) echo $dt->jurnal_edisi.", ";
								if($dt->jurnal_volume) echo "vol.".$dt->jurnal_volume.", ";
								if($dt->jurnal_page) echo "pp. ".$dt->jurnal_page.". ";
								echo "<em>".$dt->nama_kegiatan."</em>. ".$dt->lokasi.", ".date("M Y", strtotime($dt->tgl_pelaksanaan)). ". ";
								if ($dt->jurnal_lokasi) echo $dt->jurnal_lokasi." : ";
								if($dt->jurnal_penerbit) echo $dt->jurnal_penerbit;
							}else{
								if($dt->jurnal_nama) echo " . ".$dt->jurnal_nama. ", ". $dt->jurnal_edisi;
								if($dt->jurnal_volume) echo " vol. ".$dt->jurnal_volume;
								if($dt->jurnal_page) echo ", pp. ". $dt->jurnal_page;
							}
							?>
							</div>
							<?php
					endforeach;
					echo "<br>";
				endif;
			else:
				$reports = $minfo->get_rekap_penelitian($str_, $posts->karyawan_id);
				if($reports):
					foreach($reports as $dt):
						?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;line-height:1.6;color:#444;text-align:justify">
							<?php					
							$peserta = explode("@", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
								$strpeserta = explode("-",$peserta[$j]);
								$npeserta = reset($strpeserta);
								$ketua = end($strpeserta);
								if($ketua):
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								else:
									echo "<span class='text text-info'>".$npeserta ;
									echo "</span>".$str;
								endif;
							}
							echo  $dt->tahun. ". ";
							if($dt->jurnal_link)  echo "<a href='".trim($dt->jurnal_link)."' target='_blank'><b>".$dt->judul."</b></a>. ";					
							else echo "<b>".$dt->judul."</b>. ";
							?>
							</div>
							<?php
					endforeach;
				endif;
			endif;
			
		else:
			$penelitian = $minfo->get_rekap_penelitian($str_, $posts->karyawan_id);
				
			if($penelitian):
			//if($lang=='en')	echo "<h4>International Journal Papers</h4>";
				foreach($penelitian as $dt):				
						?>
						<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;text-align:justify">
						<?php
						if($dt->jurnal_link)  echo "<a href='".trim($dt->jurnal_link)."' target='_blank'><b>".$dt->judul."</b></a>";					
						else echo "<h4>".$dt->judul."</h4>";  ?>						
						
						<?php 
						 if($str_!="publikasi") echo "<b>".peneliti.": </b>";
						
						$peserta = explode("@", $dt->peserta);
						
						for($j=0;$j<count($peserta);$j++){ 
							$k = $j+1;
							if($k==count($peserta)){
								$str = " ";
							}else{
								$str = ", ";
							}
							$strpeserta = explode("-",$peserta[$j]);
							$npeserta = reset($strpeserta);
							$ketua = end($strpeserta);
							if($ketua):
								echo "<span class='text text-info'>".$npeserta ;
								echo "</span>".$str;
							else:
								echo "<span class='text text-info'>".$npeserta ;
								echo "</span>".$str;
							endif;
						}								
						if($dt->keterangan) echo "<br>".$dt->keterangan; ?>
					
						</div>
				<?php 
					
				endforeach;
			else: 
				echo "<small><em>Data not available</em></small>";
			endif;
		endif;
		
	}


	function list_data_ori($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$penelitian = $minfo->get_rekap_penelitian($str, $posts->karyawan_id);
				
			if($penelitian):
			?>
		<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo judul ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($penelitian as $dt):
					
				?>
			<tr>
				<td style="display:none"></td>
				<td>
					<?php 
					if($dt->jurnal_link)  echo "<a href='".$dt->jurnal_link."' target='_blank'><b>".$dt->judul."</b></a>";					
					else echo "<b>".$dt->judul."</b>";  ?> <span class="label label-default"><?php echo $dt->tahun ?></span><br>
					
					<small>				
					<?php 
					 if($str!="publikasi") echo "<b>".peneliti.": </b>";
					
					$peserta = explode("@", $dt->peserta);
					
					for($j=0;$j<count($peserta);$j++){ 
						$k = $j+1;
						if($k==count($peserta)){
							$str = " ";
						}else{
							$str = ", ";
						}
						$strpeserta = explode("-",$peserta[$j]);
						$npeserta = reset($strpeserta);
						$ketua = end($strpeserta);
						if($ketua):
							echo "<span class='text text-danger'>".$npeserta ;
							echo "</span>".$str;
						else:
							echo "<span class='text text-default'>".$npeserta ;
							echo "</span>".$str;
						endif;
					}								
					?><br>
					<em><?php if($dt->kategori_publikasi) echo ucWords($dt->kategori_publikasi); ?>  <?php if($dt->lokasi) echo " . ".$dt->lokasi;?>
					<?php if($dt->jurnal_nama) echo " . ".$dt->jurnal_nama. " ". $dt->jurnal_edisi;?>
					</em>
					</small>
				</td>
			</tr>
			<?php endforeach ?>
			</tbody>
			</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
	<?php
	}
	
	function list_data_prestasi($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$prestasi = $minfo->get_rekap_prestasi($posts->karyawan_id);
				
			if($prestasi):
				
				foreach($prestasi as $dt):							
						?>
				
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc;line-height:1.6;color:#444;text-align:justify">
							<?php if($dt->tgl_mulai!='0000-00-00'){ 
									echo "<span class='text text-info'>".date("M Y", strtotime($dt->tgl_mulai))."</span>. "; 
								
								}
								echo $dt->judul.". ";  
								if($dt->penghargaan) echo "<span class='text text-success'>".$dt->penghargaan."</span>"; 
								if($dt->penyelenggara) echo ". ". $dt->penyelenggara;
								if($dt->lokasi) echo ". ". $dt->lokasi;
								if($dt->link_berita) echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-search'></i> <a href=".$dt->link_berita." target='_blank' class='text text-danger'>Baca Berita</a></span></small>" ?>						
							
							</div>
					<?php endforeach;

		else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
									
	<?php
	}
	
	function list_data_kegiatan($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$kegiatan = $minfo->get_rekap_kegiatan($posts->karyawan_id,"'pembicara','pemakalah','penyaji'");
			$reviewer = $minfo->get_rekap_kegiatan($posts->karyawan_id,"'external reviewer','reviewer'");
			$lecturer = $minfo->get_rekap_kegiatan($posts->karyawan_id,"'guest lecturer'");
			$peserta = $minfo->get_rekap_kegiatan($posts->karyawan_id,"'peserta'");
				
			if($kegiatan):
				echo "<h4>".pembicara."</h4>";
				
				foreach($kegiatan as $dt):
						?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc">
								<?php 
									echo "<span class='text text-info'>".date("M Y", strtotime($dt->tgl_mulai))."</span>, ". $dt->nama_kegiatan;  
									if($dt->tempat_kegiatan!=""){  echo ". ".$dt->tempat_kegiatan; } 
									if($dt->topik) echo ". ".topik." : <em>". $dt->topik."</em>.";
									?>
							</div>
					<?php 
				endforeach;					
			endif; 
			
			if($reviewer):
				echo "<br><h4>External Reviewer</h4>";
				
				foreach($reviewer as $dt):
					?>
					<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc">
						<?php 
							echo $dt->nama_kegiatan; 
							if($dt->tgl_mulai!='0000-00-00'){
								if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
									 echo ", ".date("M d", strtotime($dt->tgl_mulai)). " - ". date("M d, Y", strtotime($dt->tgl_selesai));
								}else{
									 echo ", ". date("M d, Y", strtotime($dt->tgl_mulai));
								}
							}
							if($dt->tempat_kegiatan!=""){  echo ". ".$dt->tempat_kegiatan; } 
							if($dt->kota_kegiatan!=""){  echo ", ".$dt->kota_kegiatan; } 
							?>
					</div>
					<?php 
				endforeach;					
			endif; 
			
			if($lecturer):
				echo "<br><h4>Guest Lecturer</h4>";
				
				foreach($lecturer as $dt):
						?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc">
								<?php 
									echo $dt->nama_kegiatan;  
									if($dt->tempat_kegiatan!=""){  echo ". ".$dt->tempat_kegiatan; } 
									if($dt->topik) echo ". ".topik." : <em>". $dt->topik."</em>. ";
									if($dt->tgl_mulai!='0000-00-00'){
										echo date("Y", strtotime($dt->tgl_mulai)).".";
									}
									?>
							</div>
					<?php 
				endforeach;					
			endif; 
		
			if($peserta):
				echo "<br><h4>".peserta."</h4>";
				
				foreach($peserta as $dt):
						?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc">
								<?php 
									echo "<span class='text text-info'>".date("M Y", strtotime($dt->tgl_mulai))."</span>, ". $dt->nama_kegiatan;  
									if($dt->tempat_kegiatan!=""){  echo ". ".$dt->tempat_kegiatan; } 
									if($dt->topik) echo ". ".topik." : <em>". $dt->topik."</em>.";
									?>
							</div>
					<?php 
				endforeach;					
			endif; 
	}
	
	function list_data_kegiatan_ori($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$kegiatan = $minfo->get_rekap_kegiatan($posts->karyawan_id);
				
			if($kegiatan):
				?>
			<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo nama ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($kegiatan as $dt):
							
						?>
					<tr>
						<td style="display:none"></td>
						<td>
							<div class="col-md-12">
								<small><span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
									echo date("M d, Y", strtotime($dt->tgl_mulai)); 
									if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
										echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
									}
								?>
								</span>&nbsp;<span class="text text-danger"><i class="fa fa-tag"></i> <?php echo ucwords($dt->sebagai);?></span></small><br>
								<?php echo $dt->nama_kegiatan;   echo " <span class='label label-default'>".$dt->jenis_kegiatan."</span>"; ?><br>
								 
								<small><?php if($dt->penyelenggara!=""){ ?><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara."&nbsp;";
								}?> 
								<?php if($dt->tempat_kegiatan!=""){ ?><i class="fa fa-map-marker"></i> <?php echo $dt->tempat_kegiatan;  ?><?php } ?></small>
							
							</div>
							
						</td>
					</tr>
					<?php endforeach ?>
					</tbody>
				</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
									
	<?php
	}
	
	function list_data_afiliasi($lang=NULL, $minfo=NULL,$posts=NULL, $str=NULL){
		$afiliasi = $minfo->get_rekap_organisasi($posts->karyawan_id);
				
			if($afiliasi):
				//echo "<h4>".afiliasi."</h4>";
				
				foreach($afiliasi as $dt):
						?>
							<div style="padding:8px 0 8px;border-bottom:dashed 1px #ccc">
								<?php 
									if($dt->periode_mulai!='0000-00-00'){
										//echo "<span class='text text-info'>".date("M Y", strtotime($dt->tgl_mulai))."</span>, ". $dt->nama_organisasi;  
										echo "<span class='text text-info'>";
										if(strtotime($dt->periode_selesai)!=strtotime($dt->periode_mulai)){
											if($dt->is_aktif=='1') echo date("M Y", strtotime($dt->periode_mulai)). " - current";
											else echo date("M d", strtotime($dt->periode_mulai)). " - ". date("M d, Y", strtotime($dt->periode_selesai));
										}else{
											date("M d, Y", strtotime($dt->periode_mulai));
										}
										echo "</span>. ";
									}
									echo $dt->nama_organisasi; 
									if($dt->sebagai){
										if($lang=='en' && strToLower($dt->sebagai)=='anggota'):
											echo ". <em>Member</em>";
										else:
											echo ". <em>".$dt->sebagai."</em>";
										endif;
									}
									
									//if($dt->topik) echo ". ".topik.": <em>". $dt->topik."</em>.";
									?>
							</div>
					<?php 
				endforeach;		
			else:
				echo "<small><em>Data not available</em></small>";
			endif; 
			
	}
	
	?>