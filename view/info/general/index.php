
<div class="row">
<?php
if($posts):

	if($kategori=='staff' || $kategori=='dosen' || $kategori=='s2'):
		$mconf = new model_page();
		
		if($kategori_data== 'dosen')	$strunit = $mconf->get_unit_kerja('laboratorium','PTIIK', $lang);
		else $strunit = $mconf->get_unit_kerja('','PTIIK',$lang);
		
		?>
			  <!-- Main Content -->
                    <div class="col-md-12">	
                    	<div class="row">
                    		<div class="col-sm-9  col-sm-push-3">
								<div class="pull-right">
								<?php foreach($abjad as $key): 
												
									echo "<a href='javascript::' class='filter' data-filter='.".$key->value."' >".$key->value."</a> | ";
													
									endforeach;
								?>
																	
								</div>
								<div class="col-sm-12">
									<div class="row pull-right">
									<small><span class='label label-danger'>*</span> <span class='text text-danger'><?php if($lang=='en') echo 'On study leave'; else echo 'Sedang studi lanjut';?></span></small>	
									</div>
								</div>
									<!--<h4>
										<div class="row">
											<div class="col-md-6"><?php echo nama ?></div>
											<div class="col-md-6"><?php if($kategori_data=='dosen') echo interest;
											else echo tanggung_jawab;
											?></div>
										</div>
									</h4>-->
                    			<div class="content-filter">
		                        	<div class="row">
		                        		<div class="mixit-content">											
											 <?php
												$i=0;
												foreach($posts as $dt): 
													
														$i++;
														
														
														if($dt->foto) $imgthumb = $this->config->file_url_view."/".$dt->foto; 
														else $imgthumb = $this->asset("ptiik/images/no_foto.png"); //$this->config->default_thumb_web;
														?>
														<!--col-sm-3 col-xs-4-->
														<div class="col-sm-12 col-xs-12 isi-kategori mix<?php if($dt->jabatan)echo " leader";?><?php if($dt->unitkerja){$unitk=explode("@",$dt->unitkerja);for($i=0;$i<count($unitk);$i++){$unitid=explode("-",$unitk[$i]);for($k=0;$k<count($unitid);$k++){if($k==2){echo " ".strtolower($unitid[$k]);}}}} echo " ".strToUpper(substr($dt->nama,0,1)) ?>" data-myorder="<?php if($dt->interest) echo strToUpper(substr(trim($dt->interest),0,1));
														else "-";?>" style="border-bottom:dashed 1px #ccc;padding:8px 0 8px">
															<div class="col-md-6">
															<a href="<?php 
															if(isset($url)) echo  $this->location($url.'/info/staff/'.$dt->id);
															else echo  $this->location('info/staff/'.$dt->id);
															?>"> <?php echo  $dt->nama; if($dt->gelar_awal!=""){ echo ", ".$dt->gelar_awal; } if($dt->gelar_akhir!=""){ echo ", ".$dt->gelar_akhir; } ?></a>
															<?php if($dt->is_aktif!='aktif') echo "&nbsp;<small><span class='label label-danger'>*</span><small><em>&nbsp;</em></small></small>"; ?>
															</div>
															<div class="col-md-6">
																<small><?php if($lang=='en') echo ucWords($dt->interest_en);
																else echo  ucWords($dt->interest); ?></small>
															</div>
														</div>												
													 <?php 
												 endforeach;
												 ?>          			
		                        			<div class="col-sm-12 col-xs-12 gap"></div>
											<div class="col-sm-12 col-xs-12 gap"></div>
		                        		</div>
		                        	</div>
		                        </div>
                    			
                    		</div>
                    		<script>
                    		var $typemix="profile";
                    		var $abjadmix=[
                    		<?php 
											
											foreach($abjad as $key): 
												echo '"'.$key->value.'",';
												
												endforeach;
												?>
									]	
									var classAbjadText="col-sm-12 col-xs-12 mix leader filter <?php foreach($strunit as $dt1): echo strtolower($dt1->kode)." "; endforeach;?>abjad-kategori";
									
							</script>
                    		<div class="col-sm-3 col-sm-pull-9">
							 <!-- <div class="col-sm-pull-9"><label>Sort:</label>  
							  <button class="sort" data-sort="myorder:asc">Asc</button>
							  <button class="sort" data-sort="myorder:desc">Desc</button>
							  </div>-->
                    			
		                       <div class="filter-by">Filter by:</div> 
                    			<div class="menu-filter left-pane"> 
		                        	<div class="menu-filter-box text-left">
			                        	<ul class="filters list-unstyled"> 
			                        		<li class="active"><a href="#" class="filter" data-filter="all">All</a></li> 
			                        		<li><a href="javascript::" class="filter" data-filter=".leader"><?php echo pimpinan; ?></a></li> 
											<?php 
											if($strunit): 
												foreach($strunit as $key):
												?>
													<li><a href="javascript::" class="filter" data-filter=".<?php echo strtolower($key->kode);  ?>"><?php echo $key->unit;  ?></a></li> 
												<?php 
												endforeach;
											endif; ?>
			                        	</ul>
										
										
		                        	</div>
		                        </div>
                    		</div>
                    	</div>
                        
                        
                </div>
		<?php
	else:
?>
	<table class="table web-page">
		<thead style="display:none">
			<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
		</thead>
		<tbody>
			<?php
				foreach($posts as $dt):
					if(($kategori!="staff")&& ($kategori!="fasilitas")):
					
					$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
					
					if(isset($url)):
						if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content);
						else $url_content = $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
					else:
						if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content);
						else $url_content = $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
					endif;
											
					?>
					<tr style="border-top:0px;">
						<td style="display:none;border:0px;"></td>
							<td style="border-top:0px;">
								<div>
								<a class="title-article" href="<?php echo $url_content; ?>"><?php if($dt->judul) echo $dt->judul;
								else echo $dt->judul_ori;
								?></a>
								<time class="time-post"><small><?php if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
								else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time></div>
					</td></tr>
			<?php
					else:
						if($kategori=='fasilitas'):
							if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
								else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));


								if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
								else $imgthumb = $this->config->default_thumb_web;
								?>												
						
							<tr style="border-top:0px;">
								<td style="display:none;border:0px;"></td>
									<td style="border-top:0px;">	

										 <div class="media">
											<a class="pull-left" href="<?php echo $this->location('unit/'.$kategori_data.'/'.strtolower($dt->kode)); ?>">
												<img class="media-object img-responsive img-thumbnail" src="<?php echo $imgthumb; ?>" width="100px">
											</a>
											
											 <div class="media-body">
												<a class="title-article" href="<?php echo $this->location('unit/'.$kategori_data.'/'.strtolower($dt->kode)); ?>"><h4 class="media-heading">
												<?php if($dt->unit) echo ucWords($dt->unit);
															else echo ucWords($dt->unit_ori); ?></h4></a>
														<small>
													
											</div>
										</div>											
									</td></tr>
							<?php
						else:
					?>
						<tr style="border-top:0px;">
								<td style="display:none;border:0px;"></td>
							<td style="border-top:0px;">
								<?php 
								if(isset($url)):
									if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content);
									else $url_content = $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
								else:
									if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content);
									else $url_content = $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
								endif;
								
								if($dt->foto) $imgthumb = $this->config->file_url_view."/".$dt->foto; 
								else $imgthumb = $this->config->default_thumb_web;
								?>							
                               
								 <div class="media">
									<a class="pull-left" href="#<?php //echo $url_content; ?>">
										<img class="media-object img-responsive img-thumbnail" src="<?php echo $imgthumb; ?>" width="100px">
									</a>
									
									 <div class="media-body">
										<a class="title-article" href="#<?php // echo $url_content; ?>"><h4 class="media-heading"><?php echo $dt->nama; 
											if($dt->gelar_awal!=""){ echo ", ".$dt->gelar_awal; } if($dt->gelar_akhir!=""){ echo ", ".$dt->gelar_akhir; } ?></h4></a>
												<small>
												<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43">&nbsp;<?php echo date("M d, Y", strtotime($dt->tgl_lahir)); ?></time>
												<span class="category"><span class="fa fa-tags"></span>&nbsp;<?php echo $dt->is_status ?></span>
												</small><p></p>
												
											<p class="post-content">
											<?php
												if($dt->nik!="-"){
														echo "<h5>".strToUpper($dt->is_nik).". ".$dt->nik."</h5>";
													}
													
													if($dt->jabatan){
													?>
														<div class="kehadiran-pangkat"><span class="text text-info" style='font-weight:normal'><?php echo $dt->jabatan; ?></span></div>
													<?php
													}	
													
													if($dt->unitkerja){
														$unitk = explode("@", $dt->unitkerja);
													
														for($i=0;$i<count($unitk);$i++){
															$unitid = explode("-", $unitk[$i]);
															
															for($k=0;$k<count($unitid);$k++){							
																if($k==0){											
																	echo "<span class='label label-default' style='font-weight:normal'>".$unitid[$k]."</span> ";								
																}
															}
														}
													
													}

													if($dt->ruangkerja){
														$ruang = explode(",", $dt->ruangkerja);
															
														for($k=0;$k<count($ruang);$k++){				
																										
															echo "<code>R. ".$ruang[$k]."</code> ";									
															
														}
													}							
											
											?>
											</p>
										
									</div>
								</div>
						</td></tr>
					<?php
						endif;
					endif;
				endforeach;
				?>
			</tbody>
		</table>	
	<?php
	endif;
endif;
?>
</div>
