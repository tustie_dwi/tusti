 <div class="content row">
	<div class="col-md-11">
		
		<?php
		if(isset($result)):
		
		$prodi = $mconf->get_prodi_grafik();
		?>
		<table class="table">
			<thead>
				<tr>
					<th rowspan="2" colspan="1"><?php echo tahun ?></th>
					<th rowspan="1" colspan="<?php echo count($prodi) ?>"><?php echo prodi ?></th>
					<th rowspan="2" colspan="1"><?php echo jumlah ?></th>
				</tr>
				
				<tr>
					<?php 
					foreach($prodi as $dt):
						?>
						<th><?php echo $dt->prodi ?></th>
						<?php
					endforeach;
					?>
					
				</tr>
			</thead>
			<tbody <?php if($kategori=='grafik_mhs_lulus') echo 'id="content-graph-wrap"' ?>>
				<?php
				if($kategori=='grafik_mhs'){
				foreach($result as $dt):
					?>
					<tr>
						<td><?php echo $dt->angkatan ?></td>
						<?php 
						$t_juml=0;
						foreach($prodi as $key):
							$mhs = $mconf->get_mhs_grafik($dt->angkatan, $key->prodi);
							if($mhs) $jml = $mhs->jml;
							else $jml = 0;
							
							$t_juml=$t_juml + $jml;
							?>
							<td><?php echo $jml ?></td>
								
							<?php
						endforeach;
						?>
						<td><?php echo $t_juml ?></td>
					</tr>
					<?php
				endforeach;
				}
				?>
			</tbody>
		</table>
		<?php
	endif;
	?>
	</div>
	
	<div class="col-md-11">
		<div class="graph-container">
			<?php if($kategori=='grafik_mhs') echo '<div id="graphmhs"></div>' ?>
			<?php if($kategori=='grafik_mhs_lulus') echo '<div id="graphmhslulus"></div>' ?>
		</div>
	</div>
</div>
<style>
.graph-container {
    border: 1px solid #ccc;
    box-shadow: 1px 1px 2px #eee;
    margin: 10px 0;
    padding: 30px;
    position: relative;
}
</style>