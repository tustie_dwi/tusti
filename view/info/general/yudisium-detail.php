<h3 class='font-raleway'><?php echo $detail->judul ?></h3>
		<table width="100%">
			<tr>
				<td width="10%">Tgl Yudisium</td>
				<td width="1%">:</td>
				<td><?php echo date("M d, Y", strtotime($detail->tgl_yudisium));?></td>
			</tr>
			
			<tr>
				<td>Ruang</td>
				<td>:</td>
				<td><?php echo $detail->ruang_id;?></td>
			</tr>
			
			<tr>
				<td>Total Peserta</td>
				<td>:</td>
				<td><?php echo count($mhs);?> peserta</td>
			</tr>
			<?php
			if ($detail->keterangan):
			?>
			<tr>
				<td>Catatan</td>
				<td>:</td>
				<td><?php echo $detail->keterangan;?></td>
			</tr>
			<?php
			endif;
			?>
		</table>
		
		<h3 class='font-raleway'>Daftar Mahasiswa</h3>
		<?php
		if(isset($mhs) && $mhs){
			?>
			<table class="table web-page-search" id="yudisium">
			<thead>
				<tr><th>NIM</th><th>Nama</th></tr>
			</thead>
			<tbody>
			<?php
			foreach($mhs as $dt):
				?>
					<tr>
						
							<td >
								<?php echo $dt->mahasiswa_id; ?>
					</td><td >
								<?php echo $dt->nama; ?>
					</td></tr>
				<?php
			endforeach;
			?>
				</tbody>
			</table>
			<?php
		}
		?>
	<script>
		$(document).ready( function() {
		  $('.web-page-search').dataTable( {
			"fnInitComplete": function(oSettings, json) {
			
			},
			"sDom": "<'row'<'col-md-8'f><'col-md-2'>><'table-responsive't><'row'<'col-md-12 pull-right'p>>",
			"sPaginationType": "bootstrap",
			"aaSorting": [[0, 'desc']],
			"oLanguage": {
				"sLengthMenu": "_MENU_ records",
				"sSearch": ""
			},
			"iDisplayLength" : 5
		  } );
		} )
</script>