<?php 
$this->view("page/header.php", $data); 

?>
<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Temukan Layananmu'; else echo "Our Services";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
		</div>
	</div>
</section>

<section class="content content-white">
<div class="container">	
	<section itemscope="" itemtype="http://schema.org/Article">
		<div class="col-md-12">	
			<div class="row">
				<div class="col-sm-3">
				
					<div class="menu-filter left-pane"> 
						<div class="menu-filter-box">
							<ul class="filters list-unstyled" style="width:100%"> 
								
								<?php 
								if($apps): 
									$arr_data=array();
									foreach($apps as $key):
										$arr_temp = array("id"=>$key->id, "content"=>$key->keterangan, "judul"=>$key->judul, "link"=>$key->content_data);
										array_push($arr_data, $arr_temp);
									?>
										<!--<li><a href="javascript::" class="filter our-services" data-filter=".<?php echo strtolower($key->id);  ?>" data-list="<?php echo strtolower($key->content_data);  ?>"><?php echo $key->judul ?></a></li> -->
										<li><a href="<?php echo $this->location()."/".$key->content_data;  ?>" class="filter" data-list="<?php echo strtolower($key->content_data);  ?>"><?php echo $key->judul ?></a></li>
									<?php 
									endforeach;
								endif; ?>
							</ul>
						</div>
					</div>
				</div>			
				<div class="col-sm-9 view-services">
					<div class="our-service-content">
						<div class="row">
						<?php 
						for($i=0;$i<count($arr_data);$i++){
						?>
							<div class="col-sm-6">
								<div class="our-service-box content-md" style="min-height:90px">
									<a href="<?php echo $this->location."/".$arr_data[$i]["link"];  ?>" class="our-service-link">										
										<h3 class="font-raleway"><i class="fa fa-th-large"></i><?php echo $arr_data[$i]["judul"] ?></h3>
									</a>
									<div class="our-service-description">
										<?php echo $arr_data[$i]["content"] ?>
									</div>
								</div>
							</div>
						<?php
						}
						?>
							
						</div>
					</div>
					
				</div>	
			</div>		
		</div>
			
	</section>
</div>
</section>
<?php 
$this->view("page/footer.php", $data); 
?>
<script>
		$(document).ready( function() {
		  $('.web-page-search').dataTable( {
			"fnInitComplete": function(oSettings, json) {
			  //alert( 'DataTables has finished its initialisation.' );
			},
			//"sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
			"sDom": "<'row'<'col-md-9'><'col-md-3 pull-right'f>><'table-responsive't><'row'<'col-md-6'><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"aaSorting": [],
				//"aaSorting": [[0, 'desc']],
				"oLanguage": {
					"sLengthMenu": "_MENU_ records",
					"sSearch": ""
				}
		  } );
		} )

	</script>
<script>
	//$('.web-page-search').DataTable();
	$('.our-services').click(function (e){		
		var val = $(this).data("list");
		var formURL = base_url + val;
	
		
		$.ajax({
		url : formURL,
		type: "POST",
		data : $.param({
						val_view : 'services'
					}),
		success : function(msg) {	
			
			//$('#view-data').html(msg);
			$('.view-services').html(msg);			
		}
		});
	});
	
	
</script>