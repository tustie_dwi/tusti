<?php 
$this->view("page/header.php", $data); 
?>

  
<section class="content content-white">
	<div class="container">  
	<!-- Main Content -->
	<div class="col-md-12">	
		<!-- Edit Breadcrumb -->
			<ol class="breadcrumb" itemprop="breadcrumb">
			  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
				else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
				
			  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo tentang ?></span></li>
			   <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo $posts->nama;
				if($posts->gelar_awal!=""){ echo ", ".$posts->gelar_awal; } if($posts->gelar_akhir!=""){ echo ", ".$posts->gelar_akhir; } 
			   ?></span></li>
			</ol>
			<!-- End Breadcrumb -->
					
		<section itemscope="" itemtype="http://schema.org/Article">
			<div class="profile-page">
			<?php
			 if($posts->foto) $imgthumb = $this->config->file_url_view."/".$posts->foto; 
			else $imgthumb = $this->config->default_thumb_web;
			?>
								<div class="profile-header">
									<div class="row">
										<div class="col-sm-4 col-md-2">
											<a href="<?php echo $imgthumb; ?>" class="fancybox">
												<figure class="text-center-sm">
													<img src="<?php echo $imgthumb; ?>" class="img img-responsive img-responsive-center">
												</figure>
											</a>
										</div>
										<div class="col-sm-8 col-md-10">
											<h2 class="margin-top-sm"><?php echo $posts->nama;
													if($posts->gelar_awal!=""){ echo ", ".$posts->gelar_awal; } if($posts->gelar_akhir!=""){ echo ", ".$posts->gelar_akhir; } 
												   ?>				
												<small>
													<?php if($posts->jabatan){
														?>
															<span class="text text-info" style='font-weight:normal'> <?php echo $posts->jabatan; ?></span>
														<?php
														}?>
													
												</small>
											</h2>
											<h5><?php if($posts->nik!="-"){
													echo strToUpper($posts->is_nik).". ".$posts->nik."<br>";
												}?></h5>
											<div class="row">
												<div class="col-md-6">
													<ul class="profile-list list-unstyled">
														<?php
														if($posts->unitkerja){
															$unitk = explode("@", $posts->unitkerja);
														
															for($i=0;$i<count($unitk);$i++){
																$unitid = explode("-", $unitk[$i]);
																
																for($k=0;$k<count($unitid);$k++){							
																	if($k==0){	
																		?>
																		<li><span class="fa fa-check-square-o"></span><?php echo $unitid[$k];?></li>
																		<?php
																		//echo "<span class='label label-default' style='font-weight:normal'>".$unitid[$k]."</span> ";								
																	}
																}
															}
														
														}
														?>
														<li><span class="fa fa-map-marker"></span><?php if($posts->ruangkerja){
																	$ruang = explode("@", $posts->ruangkerja);
																		
																	for($k=0;$k<count($ruang);$k++){				
																													
																		echo "<code>R. ".$ruang[$k]."</code> ";									
																		
																	}
																}			?></li>														
													</ul>
												</div>
												<div class="col-md-6">
													<ul class="profile-list list-unstyled">
														<li><span class="fa fa-home"></span><?php echo $posts->alamat; ?></li>
														<li><span class="fa fa-envelope-o"></span><?php 
														$str = explode("@", $posts->email);
														$strname = reset($str);
														$strend = end($str);
														 $stremail = str_replace(".", " [dot] ", $strend);
														 echo $strname. " [at] ". $stremail;
														//echo str_replace("@", " [at] ", $stremail); ?></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if ($posts->is_status=='dosen'): ?>
								<div class="profile-body">
									<div class="row">
									<?php  if(isset($posts) && $posts->biografi): ?>
										<div class="col-md-12">
											<h2 class="title-content title-underline title-underline-orange margin-bottom-sm"><span><?php echo biografi ?></span></h2>
                                			<div class="profile-content"><?php 
													 if(isset($posts) && $posts->biografi) echo $posts->biografi;
													 else echo "-";
													 ?>
											</div>
											<!--<hr class="hr-profile margin-top margin-bottom">-->
										</div>
										<?php else:	?>
										<br>
										<?php endif; ?>
										
										<?php if(isset($posts) && $posts->interest):?>
										<div class="col-md-12">
											<h2 class="title-content title-underline title-underline-orange margin-bottom-sm margin-top-sm"><span>Interest</span></h2>
                                			<div class="profile-content"><?php 
												 if(isset($posts) && $posts->interest) echo $posts->interest;
												 else echo "-";
												 ?>
											</div>
											<!--<hr class="hr-profile margin-top margin-bottom">-->
										</div>
										<?php 
										else:
											echo "<br>";
										endif;?>
										
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-6">
													<h2 class="title-content title-underline title-underline-orange margin-bottom-sm margin-top"><span><?php echo penelitian ?></span></h2>
													<div class="profile-content profile-content-table">
														<div class="table-responsive">
															<?php list_data($minfo,$posts); ?>
															
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<h2 class="title-content title-underline title-underline-orange margin-bottom-sm margin-top"><span><?php echo publikasi ?></span></h2>
													<div class="profile-content profile-content-table">
														<div class="table-responsive">
															<?php list_data($minfo,$posts,'publikasi'); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
										

										<div class="col-md-12">
											<div class="row">
												<div class="col-md-6">
													<h2 class="title-content title-underline title-underline-orange margin-bottom-sm margin-top-sm"><span><?php echo prestasi ?></span></h2>
													<div class="profile-content profile-content-table">
														<div class="table-responsive">
															<?php list_data_prestasi($minfo,$posts); ?>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<h2 class="title-content title-underline title-underline-orange margin-bottom-sm margin-top-sm"><span>Seminar/Workshop</span></h2>
													<div class="profile-content profile-content-table">
														<div class="table-responsive">
															<?php list_data_kegiatan($minfo,$posts); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
								</div>
							</div>
						<?php endif; ?>
		
			</div>	
		</section>
	</div>
	<!-- End Main Content -->
	</div>
</section>
	
<?php $this->view("page/footer.php", $data); 


	function list_data($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$penelitian = $minfo->get_rekap_penelitian($str, $posts->karyawan_id);
				
			if($penelitian):
			?>
		<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo judul ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($penelitian as $dt):
					
				?>
			<tr>
				<td style="display:none"></td>
				<td>
					<div class="col-md-8">
						
						<?php echo $dt->judul;  ?><br>
						 
						<small><span class="text text-info"><i class="fa fa-check-square-o"></i> <?php echo $dt->tahun. " ". $dt->is_ganjil. " ".$dt->is_pendek."</span>";
							//echo " <span class='label label-default'> ".$dt->status."</span>";								?></small>
													
					</div>
					<div class="col-md-4"><small>
					
					<?php 
					
					$peserta = explode("@", $dt->peserta);
					
					for($j=0;$j<count($peserta);$j++){ 
						$k = $j+1;
						if($k==count($peserta)){
							$str = " ";
						}else{
							$str = ", ";
						}
						$strpeserta = explode("-",$peserta[$j]);
						$npeserta = reset($strpeserta);
						$ketua = end($strpeserta);
						if($ketua):
							echo "<span class='text text-danger'>".$npeserta ;
							echo " *";
							echo "</span>".$str;
						else:
							echo "<span class='text text-default'>".$npeserta ;
							echo "</span>".$str;
						endif;
					}								
					?></small></div>
				</td>
			</tr>
			<?php endforeach ?>
			</tbody>
			</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
	<?php
	}
	
	function list_data_prestasi($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$prestasi = $minfo->get_rekap_prestasi($posts->karyawan_id);
				
			if($prestasi):
				?>
			<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo nama ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($prestasi as $dt):
							
						?>
					<tr>
						<td style="display:none"></td>
						<td>
							<div class="col-md-8">
								<small><span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
									echo date("M d, Y", strtotime($dt->tgl_mulai)); 
									if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
										echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
									}
								?>
								</span></small><br>
								<?php echo $dt->judul."&nbsp; ";  if($dt->penghargaan) echo "<span class='text text-success'><i class='fa fa-bookmark'></i> ".$dt->penghargaan."</span>"; ?><br>
								 
								<small><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara ?>
								&nbsp; <i class="fa fa-map-marker"></i> <?php echo $dt->lokasi ?></small>
								<?php if($dt->link_berita) echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-search'></i> <b><a href=".$dt->link_berita." target='_blank' class='text text-danger'>Baca Berita</a></b></span></small>" ?>
							
							</div>
							<div class="col-md-4">
							
							<?php 
							
							$peserta = explode(",", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
									
								echo "<span class='text text-danger'>".$peserta[$j]."</span>".$str;
							}								
							?><br><small><?php echo $dt->inf_prestasi; ?></small>&nbsp;<span class="label label-danger"><?php echo ucwords($dt->tingkat); ?></span>
							<!--<span class="label label-default"><?php //echo ucwords($dt->jenis_prestasi); ?></span>--></div>
						</td>
					</tr>
					<?php endforeach ?>
					</tbody>
				</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
									
	<?php
	}
	
	function list_data_kegiatan($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$kegiatan = $minfo->get_rekap_kegiatan($posts->karyawan_id);
				
			if($kegiatan):
				?>
			<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo nama ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($kegiatan as $dt):
							
						?>
					<tr>
						<td style="display:none"></td>
						<td>
							<div class="col-md-12">
								<small><span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
									echo date("M d, Y", strtotime($dt->tgl_mulai)); 
									if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
										echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
									}
								?>
								</span>&nbsp;<span class="text text-danger"><i class="fa fa-tag"></i> <?php echo ucwords($dt->sebagai);?></span></small><br>
								<?php echo $dt->nama_kegiatan;   echo " <span class='label label-default'>".$dt->jenis_kegiatan."</span>"; ?><br>
								 
								<small><?php if($dt->penyelenggara!=""){ ?><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara."&nbsp;";
								}?> 
								<?php if($dt->tempat_kegiatan!=""){ ?><i class="fa fa-map-marker"></i> <?php echo $dt->tempat_kegiatan;  ?><?php } ?></small>
							
							</div>
							
						</td>
					</tr>
					<?php endforeach ?>
					</tbody>
				</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
									
	<?php
	}
	?>