
		<ul class="nav-main-nav nav-left" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
		<?php if(isset($unit_list)): ?>
		<li class="active"><a itemprop="url" href="<?php echo $this->location($url); ?>" title="Home"> Home</a></li>
		<?php else: ?>
		<li class="active"><a itemprop="url" href="<?php echo $this->location(); ?>" title="Home">Home</a></li>
		<?php
		endif;
		
		if($posts):			
			foreach($posts as $dt):
			$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
			if($child):
				?>
				<li>
						<a rel="alternate" hreflang="x-default" href="#" ><?php echo $dt->content_title?></a>
						<?php
							$i=0;
							 $count=round(count($child)/2);
							 $parent=0;
						?>
						
						<div class="col-md-12">
						<ul class="menu-sub" role="menu">
									
							 <?php 
							 
							 foreach ($child as $key):
								 $i++;
								if(isset($unit_list)):
									read_sub_menu_($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location($url) ,$key->content_data);
								else:
									read_sub_menu_($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location('page'),$key->content_data );
								endif;
							 endforeach; ?>
						</ul>						
						</div>
				</li>
				<?php
			else:
				if($dt->content_data && $dt->content_title=='PTIIK Apps'):
					echo "<li><a rel='alternate' hreflang='x-default' href=".$dt->content_data." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
				else:
					if(isset($unit_list)):
						echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location($url.'/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
					else:
						echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location('page/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
					endif;
				endif;
			endif;

		endforeach;
		endif;
		?>
			</ul>
			
	
<?php 
function read_sub_menu_($i,$count,$parent,$unit=NULL, $lang=NULL, $mpage=NULL, $id=NULL, $title=NULL, $content_page=NULL,$url_menu=NULL, $data_link=NULL){
										
	$child = $mpage->read_sub_content($unit, $lang, "", $id, $title);
			
	if($child):
		?>
		<li class="nav-head">
				<a href="#" ><?php echo $title?></a>
				<div class="" style="margin-left:5px;">
				<ul class="menu-sub" role="menu">
						 <?php 
						 $parent=1;
						 foreach ($child as $key):
								read_sub_menu_($i,$count,$parent,$unit, $lang, $mpage,$key->id,$key->content_title,$key->content_page, $url_menu,$data_link);
						 endforeach; ?>
				</ul>
				</div>
		</li>
		<?php
	else:
	
		if($data_link && $title=='PTIIK Apps'):
					echo "<li class='nav-list'><a itemprop='url' rel='alternate' hreflang='x-default' href=".$data_link." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
				else:
					if(isset($unit_list)):
						echo "<li><a itemprop='url' rel='alternate' hreflang='x-default' href=".$this->location($url.'/read/'.$content_page.'/'.$id)." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
						//echo "<li class='nav-list'><a href=".$url_menu.'page/read/'.$content_page.'/'.$id.">".$title."</a></li>";
					else:
						echo "<li class='nav-list'><a itemprop='url' rel='alternate' hreflang='x-default' href=".$url_menu.'/read/'.$content_page.'/'.$id." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					endif;
				endif;
	
	endif;
	
}	
?>