<?php 
$this->view("page/header.php", $data); 

?>

<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Yudisium'; else echo "Judicium";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Yudisium'; else echo "Judisium";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
		</div>
	</div>
</section>
<section class="content content-white">
	<div class="container">  
		
	<div class="col-md-12">	
		<?php
		if($posts):
			
			?>
			<select name="cmbperiode" class="form-control" id="cmbyudisium">
			<?php
			$i=0;
			foreach($posts as $dt):
				$i++;
				
				if($i==1) $strid = $dt->id;
				
				echo "<option value='".$dt->id."'>".date("M d, Y", strtotime($dt->tgl_yudisium))."</option>";
			endforeach;
			?>
			</select>
			<?php
		endif;
		?>
		<div id="view-data">
		
		<?php $this->get_data_yudisium($strid); ?>
		</div>
		</div>
 </div>
</section>
	
<?php $this->view("page/footer.php", $data);  ?>

<script>
	//$('#yudisium').DataTable();
	//$('.table').DataTable();
	$('#cmbyudisium').click(function (e){		
		
		var formURL = base_url + 'info/get_data_yudisium';
				
		var val  = $(document.getElementById("cmbyudisium")).val();
		$.ajax({
		url : formURL,
		type: "POST",
		data : $.param({
			id : val
		}),
		success : function(msg) {	
			
			$('#view-data').html(msg);
			//$('#view-data').show();			
		}
		});
	});
</script>