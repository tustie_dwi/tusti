<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:description" content="Official Site - <?php echo $this->page_title(); ?> Universitas Brawijaya">
<meta http-equiv="cache-control" content="no-cache">
<meta name="rating" content="Mature" />

    <!-- Bootstrap -->
   
	 <link href="<?php echo $this->asset("css/v4/style.min.css"); ?>" rel="stylesheet">
	  <link href="<?php echo $this->asset('css/v4/login.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo $this->asset("css/v4/jquery.bxslider.css"); ?>" rel="stylesheet">
    <link href="<?php echo $this->asset("css/minimalist.css"); ?>" rel="stylesheet">
 </head>
    <body class="login-page" data-offset="100">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-9 right-panel">
                    <section class="content-panel">
                       <a href="<?php echo $this->location('page'); ?>" class="image-link">
                                <img class="img-logo-header img-responsive" src="<?php echo $this->asset("ptiik/images/ptiik.png");?>">
                            </a>
                        <form class="form content-register is-register" method="post" action="<?php echo $this->location("register"); ?>">
                            <h3 class="title">Join with us</h3>
                            <p style="padding-bottom:35px">Enter your registered e-mail. Perform activation through a link that we sent to the e-mail address.</p>
							
							

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="#teacher" role="tab" data-toggle="tab">Lecturer</a></li>
                                <li><a href="#student" role="tab" data-toggle="tab">Student</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active row the-input-form" id="teacher">
                                    <div class="col-xs-8">
                                        <input type="email" name="email" class="form-control" placeholder="Email" id="email">
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="submit" class="btn btn-register  btn-block" name="b_reg" value="Register">
                                    </div>
                                </div>
                                <div class="tab-pane fade row the-input-form" id="student">
                                    <div class="col-xs-12">
                                        <input type="text" name="nim" class="form-control" placeholder="NIM" id="nim">
                                    </div>
                                    <div class="col-xs-8">
                                        <input type="email" name="emailmhs" id="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="col-xs-4">
                                        <!--<button type="button" class="btn btn-register btn-block" name="b_reg" onClick="registrasi()">Register</button>-->
										<input type="submit" class="btn btn-register  btn-block" name="b_reg_mhs" value="Register">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form class="form content-login is-login" action="<?php echo $this->location("auth/logon/"); ?>" name="auth" method="post">
                            <h3 class="title">&nbsp;</h3>
                            <p style="padding-bottom:30px">PTIIK Apps is an information system created and developed in PTIIK in order to meet the needs of an integrated, rapid, precise and accurate data. </p>
							
							<?php
							if(isset($msglogin)){
								echo $msglogin;
							}
							?>

                            <div class="the-input-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="username" placeholder="User name or email" name="username" >
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-login">Login </button>
                                </div>
                            </div>
                        </form>

                        <footer class="form-footer">
                            <div class="footer-first">
                                <div class="register-footer is-register">Have a PTIIK Apps account? <a href="#" class="to-login">Login here</a></div>
                                <div class="login-footer is-login">Dont have a PTIIK Apps account? <a href="#" class="to-register">Register here</a></div>
                            </div>
                            <div class="copy">&copy;2014 BPTIK</div>
                        </footer>
                    </section>
                </div>
                <div class="col-md-3 left-panel">
                    <aside class="content-panel-right">
                        <img src="<?php echo $this->asset("images/v4/ub-white.png") ?>" class="img img-responsive">
                        <div class="quote">
                            Passion is energy. Feel the power that comes from focusing on what excites you.
                            <div class="small-text">Oprah Winfrey</div>
                        </div>
                        <div class="weather">

                        </div>
                    </aside>
                </div>

            </div>
        </div>

        <!--Script-->
		<script src="<?php echo $this->asset("js/v4/jquery-1.11.1.min.js"); ?>"></script>
		<script src="<?php echo $this->asset("js/v4/affix.js"); ?>"></script>
		<script src="<?php echo $this->asset("js/v4/scrollspy.js"); ?>"></script>		
		<script src="<?php echo $this->asset("js/v4/collapse.js"); ?>"></script>
		<script src="<?php echo $this->asset("js/v4/dropdown.js"); ?>"></script>
		<script src="<?php echo $this->asset("js/v4/jquery.bxslider.min.js"); ?>"></script>
        <script src="<?php echo $this->asset("js/page/vplayer.min.js"); ?>"></script>
		<script src="<?php echo $this->asset("js/v4/function.js"); ?>"></script>
		<script src="<?php echo $this->asset("js/v4/tab.js"); ?>"></script>
		<script src="<?php echo $this->asset("js/v4/jquery.simpleWeather.min.js"); ?>"></script>
        
        <script>
            $.simpleWeather({
                location: 'Malang',
                woeid: '56000382',
                unit: 'c',
                success: function(weather) {
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }
					
					var monthNames = [ "January", "February", "March", "April", "May", "June",
					"July", "August", "September", "October", "November", "December" ];

                   // today = convert_month(mm) + ' ' + dd + ', &nbsp;' + yyyy;
				   today = monthNames[today.getMonth()]+ ' ' + dd + ', &nbsp;' + yyyy;
                    html = '<div class="w-today">'+today+'</div>';
                    html += '<h3 class="w-location">' + weather.city + '&nbsp;&nbsp;&nbsp;' + weather.temp + '&deg;' + weather.units.temp + '</h3>';
                    html += '<i class="icon-weather icon-weather-' + weather.code + '"></i>';
					 html += '<div style="margin-top:-35px;"><small>' + weather.currently + '</small></div>';

                    $(".weather").html(html);
                },
                error: function(error) {
                    $(".weather").html('<p>' + error + '</p>');
                }
            });
			
			function registrasi(){
			
					var email = $('#email').val();
					var nim = $('#nim').val();
					
					$.ajax({			
						type : "POST",
						dataType : "html",
						url : base_url + 'register/save',
						data : $.param({
							email : email, 
							nim : nim
						}),
						
						success : function(err) {	
							alert(msg);
							if(err==1){
								$(".status-daftar").html("<div class='alert alert-block alert-success fade in'>OK! Silahkan melakukan aktifasi account anda melalui link yang kami kirimkan ke <b>"+email+"</b></div>" );	
							}else if(err==2){
								$(".status-daftar").html("<div class='alert alert-block alert-danger fade in'>Account anda sudah terdaftar di PJJ. Gunakan account yang kami kirimkan ke alamat email Anda <b>"+email+"</b> untuk melakukan login. Silahkan menghubungi <b>PSIK</b> untuk informasi lebih lanjut.</div>" );
							}else if(err==3){
								$(".status-daftar").html("<div class='alert alert-block alert-danger fade in' Maaf, alamat email <b>"+email+"</b> tidak terdaftar dalam data kepegawaian kami. Silahkan menghubungi <b>PSIK</b> untuk informasi lebih lanjut.</div>" );
							}											
						}
					});				
				}
        </script>
    </body>
</html>

