<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title><?php echo $this->page_title(); ?></title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo $this->asset("ptiik/css/login2.min.css"); ?>" rel="stylesheet">
    </head>
   

        <body class="login">
        <section id="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                    	<form class="form form-horizontal form-login box-form" action="<?php echo $this->location("reset/cek"); ?>" method="post">
                            <div class="content-box-form content-login">
                                <div class="form-group text-center">
                                    <img class="img-responsive img-responsive-center img-logo-login" src="<?php echo $this->asset("ptiik/images/ptiikappslogo.png") ?>">
                                </div>
                                <div class="form-group text-center">
                               		<h1>RESET ACCOUNT</h1>
                                </div>
								
                               <!-- <div class="alert alert-login text-center" role="alert">
                                	Invalid login. Enter username and password correctly.
                                </div>-->
								<?php
									if(isset($msglogin)){
										echo $msglogin;
									}
									?>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="sr-only" for="exampleInputEmail1">Username or Email</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Username or Email" name="username">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="sr-only" for="exampleInputEmail1">Old Password</label>
                                        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Old Password" name="oldpass">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
									    <div class="input-group">
                                        <label class="sr-only" for="exampleInputPassword1">New Password</label>
									      <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="New Password">
                                   
									      <span class="input-group-btn">
									        <button class="btn btn-default btn-block" type="submit"><span class="fa fa-unlock-alt"></span></button>
									      </span>
									    </div><!-- /input-group -->
									  </div><!-- /.col-lg-6 -->
									</div><!-- /.row -->
                                
                                <div class="form-group">
                                    <div class="col-xs-8">
                                        
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <a href="<?php echo $this->location('page'); ?>"><img class="img img-responsive img-responsive-center" src="<?php echo $this->asset("ptiik/images/logo-login-small.png"); ?>"></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </section>              
        <footer id="footer">
                            <div>Copyright &copy; 2014 BPTIK PTIIK UB</div>
                            <div>All rights reserved</div>
        </footer>
        <script src="<?php echo $this->asset("ptiik/js/jquery-1.11.1.min.js"); ?>"></script>
        <script src="<?php echo $this->asset("ptiik/js/tab.js"); ?>"></script>
        <script src="<?php echo $this->asset("ptiik/js/transition.js"); ?>"></script>
        <script>
           $(document).ready(function () {
                generateMargin();

                $(window).resize(function () {
                    generateMargin();
                });
                $('.register-now,.login-now').click(function (e) {
                    e.preventDefault();
                    $('.form-login').toggle('slow');
                    $('.form-register').toggle('slow');
                    $data = $(this).data('type');
                    $('body').toggleClass('login register');
                });
                function generateMargin() {
                var $formtop = ($('body').height() - ($('.form').height() + $('#footer').height()));
                if ($formtop <= 0)
                    $formtop = '20%';
                $('body').css('padding-top', $formtop);
            	}
           
            });
        </script>
    </body>
</html>
