<?php
if($kegiatan){
	?>
	<ul class="list-group">
	<?php
			
				foreach($kegiatan as $row):	
					
						echo "<li class='list-group-item'>";
						switch ($row->jenis_kegiatan_id){
							case 'UTS':
								echo "<small><span class='text text-warning'>".$row->nama_mk." *</span></small><br><small><span class='text text-danger'><i class='fa  fa-clock-o'></i> ".$row->jam_mulai."-".$row->jam_selesai."</span> <code> R. ".$row->inf_ruang."</code></small>";
							break;
							case 'bimbingan':
								echo "<small>".$row->judul."</small><br><small><span class='text text-danger'><i class='fa  fa-clock-o'></i> ".$row->jam_mulai."-".$row->jam_selesai."</span> <code> R. ".$row->inf_ruang."</code></small>";
							break;
							case 'kuliah':
								echo "<small>".$row->nama_mk."</small><br><small><span class='text text-danger'><i class='fa  fa-clock-o'></i> ".$row->jam_mulai."-".$row->jam_selesai."</span> <code> R. ".$row->inf_ruang."</code></small>";
							break;
							default:
								if($row->inf_ruang){
									echo "<small>".$row->judul."</small><br><small><span class='text text-danger'><i class='fa  fa-clock-o'></i> ".$row->jam_mulai."-".$row->jam_selesai."</span> <code> R. ".$row->inf_ruang."</code></small>";
								}else{
									echo "<small>".$row->judul."</small><br><small><span class='text text-danger'><i class='fa  fa-clock-o'></i> ".$row->jam_mulai."-".$row->jam_selesai."</span> <code>".$row->lokasi."</code></small>";
								}
							break;
						}
						echo "</li>";
					
					
				endforeach;
			
	?>
		</ul>
	<?php
}else{
	?>
	<div class='alert alert-waning'>Tidak ditemukan informasi agenda Anda pada <?php echo date("M d, Y"); ?> </div>
	<?php
}
?>