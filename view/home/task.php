	
<div id="display-tasks">
	<?php
	if($tasks){	
		?>
		<form id="frmtask"><ul class="list-group" id="list-task">
		<?php
			foreach($tasks as $row):
				?>
				<li class="list-group-item" id="user-<?php echo $row->aktifitas_id; ?>" data-id="<?php echo $row->aktifitas_id; ?>" <?php if($row->is_finish==1){  echo "style='text-decoration:line-through'"; }else{ echo "style='text-decoration:none'";} ?>>
					<input type="hidden" class="label-tasks" value="<?php echo $row->is_finish; ?>"><?php
					echo "<input type='checkbox' onclick='finish_tasks(\"".$row->aktifitas_id."\",this);' id='finish".$row->aktifitas_id."' class='btn-toggle-task' data-uid='".$row->aktifitas_id."' value='1' ";
					if($row->is_finish==1){
						echo "checked";
					}
					echo "> <span class='label-task'>".$row->judul."</span><br>";
					?>
					<small><span class='text text-danger'><i class='fa fa-clock-o'></i> <?php echo substr($row->jam_mulai, 0,5)." - ".substr($row->jam_selesai, 0,5)?></span>
					
					<?php if($row->catatan){ echo "&nbsp;<i class='fa fa-file-text-o'></i> ".$row->catatan; }
					
					?></small>
					<button type="button" class="btn btn-danger btn-xs pull-right" onclick="doDeleteTask('<?php echo $row->aktifitas_id; ?>',this)" href="#">
					  <i class="fa fa-trash-o"></i> Delete
					</button> 
					<button type="button" class="btn btn-warning btn-xs pull-right" data-task="" onclick="doEditTask('<?php echo base64_encode(json_encode($row)) ?>',this)" href="#">
					  <i class="fa fa-trash-o"></i> Edit
					</button>
				</li>
				<?php				
			endforeach;
		?>
		</ul></form>
		<?php
	}else{
		?>
		<div class='alert alert-waning'>Tidak ditemukan informasi catatan kegiatan Anda pada hari ini</div>
		<?php 
	}
	?>
</div>

<div class="row">
	<div class="col-md-12" id="display-newtask" style="display: none;">
		<div class="col-md-12" style="text-align:right">
        
		</div>
		<br>
		<form id="form-newtask" class="form-horizontal">
		<small>
			<div class="form-group">	
				 <label class="col-sm-2 control-label">Tanggal Mulai</label>
				 <div class="controls">
					<div class="col-sm-10">
						<input type="text" id="form_date" name="tanggal" class="form_date form-control input-sm" required="required"> 
					</div>
				</div>
			</div>
			<div class="form-group">	
				 <label class="col-sm-2 control-label">Jam</label>
				 <div class="controls">
					<div class="col-sm-4">
						<div class='input-group timepicker'>
		                    <input type="text" id="jam_mulai" name="jam_mulai" class="form-control input-sm" required="required" style="z-index:0;">
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-time"></span>
		                    </span>
		                </div>
					</div>
					<div class="col-sm-2">
						<center style="padding-top: 5px;">sampai</center>
					</div>
					<div class="col-sm-4">
						<div class='input-group timepicker'>
		                    <input type="text" id="jam_selesai" name="jam_selesai" class="form-control input-sm" required="required" style="z-index:0;">
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-time"></span>
		                    </span>
		                </div> 
					</div>
				</div>
			</div>
			<div class="form-group">	
				<label class="col-sm-2 control-label">Judul</label>
				<div class="controls">
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="judulTask" name='judul' required="required">
					</div>
				</div>
			</div>
			<div class="form-group">	
				<label class="col-sm-2 control-label">Catatan</label>
				<div class="controls">
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="catatan" name='catatan' required="required">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="controls">
					<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="hidId" id="hidId" value="">
						<button type="button" id="btn-submit-task" onclick="submit_tasks()" class="btn btn-primary btn-sm">Simpan</button>
						<button type="button" id="btn-cancel-task" onclick="cancel_submit_tasks()" class="btn btn-default btn-sm">cancel</button>
					</div>
				</div>
			</div>
			</small>
		</form>
	</div>
</div>
