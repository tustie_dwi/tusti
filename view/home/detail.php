<?php
if(($kegiatan)||($agenda)){
	?>
	<table class='table table-striped table-hover' id='example'>
	<thead>
		<tr>
			<th>Jam</th>
			<th>Kegiatan</th>	
			<th>Lokasi</th>
			<th>Penyelenggara</th>
			<th>Kategori</th>
		</tr>
	</thead>
	<tbody>
	<?php
			if($kegiatan){
				foreach($kegiatan as $dt):
					echo "<tr valign=top valign=top>
						<td>".$dt->jam_mulai." - ".$dt->jam_selesai."</td>";		
						
					echo "<td>".$dt->judul."</td><td>".$dt->lokasi;
						if($dt->ruang){
							echo " <code>R. ".$dt->ruang."</code>";
						}
					echo "</td><td>".$dt->penyelenggara."<br><em>".$dt->unit_penyelenggara."</em></td><td>".$dt->jenis."</td></tr>";
				endforeach;
			}
					
			if($agenda){
				foreach($agenda as $dt):
					echo "<tr valign=top valign=top>
						<td>".$dt->jam_mulai." - ".$dt->jam_selesai."</td>";		
						
					echo "<td>".$dt->namamk."</td><td>PTIIK ";
						if($dt->ruang){
							echo " <code>R. ".$dt->ruang."</code>";
						}
					echo "</td><td>PTIIK</td><td>".$dt->namamk."</td></tr>";
				
				endforeach;
			}
	?>
		</tbody>
	</table>
	<?php
}else{
	?>
	<div class='alert alert-waning'>Tidak ditemukan informasi agenda Anda pada hari ini</div>
	<?php
}
?>