
	
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li class="active"><a href="#">Dashboard</a></li>
	</ol>
    <ul class="breadcumb-extra">
        <li class="orange time-ticker time-breadcumb">&nbsp;</li>
    	<li class="blue date-breadcumb"><span class="fa fa-calendar"></span>&nbsp;<span class="date-ticker"></span></li>
    </ul>

	<div class="row">
		<div class="col-sm-4">
			<?php
				$feed = file_get_contents("http://filkom.ub.ac.id/rss/feed/");
				
				 $xml = new SimpleXmlElement($feed, LIBXML_NOCDATA);

				 if(isset($xml->channel)) {
				  $cnt = count($xml->channel->item);
				 
				   for($i=0; $i<1; $i++)
				  {
				 
					   $url = $xml->channel->item[$i]->link;
					   $title = $xml->channel->item[$i]->title;
					   $desc = $xml->channel->item[$i]->description;
					   $str = explode("/", $url);
					   //echo $str[5];
					   ?>
						<a 	href="<?php echo $url;?>" class="top-dashboard top-event" target="_blank">
								<div class="top-dashboard-title"><?php echo $title ?></div>
								<ul class="top-list list-inline">
								<li><?php echo substr($desc,0,150) ?></li>
								</ul>
							</a>
					   <?php
					   
						//echo '<li class="list-group-item"><a href="'.$url.'" target="_blank">'.$title.'</a></li>';				
				  }
				
				 }
			?>
					
			
		</div>
		<div class="col-sm-4">
			<a href="<?php echo $this->location('page/read/news');?>" class="top-dashboard top-news" target="_blank">
				<h3><i class="fa fa-newspaper-o"></i> News</h3>				
			</a>
		</div>
		<div class="col-sm-4">
			<a href="<?php echo $this->location('page/read/gallery');?>" class="top-dashboard top-announcement" target="_blank">
				<h3><i class="fa fa-image"></i> Gallery</h3>
			</a>
		</div>
	</div>
	
<div class="row">
	<?php 
	if($this->authenticatedUser->role=='dosen' || $this->authenticatedUser->role=='pimpinan'){
	?>
		<div class="col-md-6">		
			<div id="parameter" class="block-box">
			  <div class="header blue"><h4>Kalender Kegiatan</h4></div>
			  <div class="content" >
				<?php $this->view("calendar.php", $data); ?>
			  </div>
			</div>
			
		<!--	<div id="parameter" class="block-box">
			  <div class="header yellow"><h4>Aktifitas</h4></div>
			  <div class="content">
				<?php// $this->view("home/event.php", $data); ?>
			  </div>
			</div>-->			
			

			<div id="parameter" class="block-box">
			  <div class="header black"><h4>Rekap HR Mengajar</h4></div>
			  <div class="content">
				<?php if($rekap_hr):?>
				<div id="container-mengajar" ></div>
				<?php else: echo "Data not available"; endif; ?>
			  </div>
			</div>			
		</div>
		<div class="col-md-6">	
			<div id="parameter" class="block-box">
			  <div class="header black"><h4>Rekap Bimbingan Skripsi</h4></div>
			  <div class="content">
				<?php if($skripsi):?>
					<div id="container-skripsi"></div>
				<?php else: echo "Data not available"; endif; ?>
			  </div>
			</div>
			
			<div id="parameter" class="block-box">
			  <div class="header black"><h4>Rekap Absen Mengajar</h4></div>
			  <div class="content">
				<?php if($hadir):?>
				<div id="container"></div>
				<?php else: echo "Data not available"; endif; ?>
			  </div>
			</div>
			
			<div id="parameter" class="block-box">
			  <div class="header black"><h4>Rekap Finger</h4></div>
			  <div class="content">
				<div id="container-finger"></div>
			  </div>
			</div>
			
			<div id="parameter" class="block-box">
			   <div class="header green"><h4>Catatan Kegiatan
				  <span class="pull-right heading-btn">
					  <a id="button_newtask" data-toggle="tab" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Tambah</a>
					  <a id="button_tasklist" data-toggle="tab" class="btn btn-default btn-sm" style="display:none"><i class="fa fa-bars"></i> List</a>
				  </span>
				  </h4>
			  </div>
			  <div class="content">
				<?php $this->view("home/task.php", $data); ?>			
			  </div>
			</div>		
		</div>
		
		
	
	<?php
	}else{
	?>
	<div class="col-md-6">
		
		<div id="parameter" class="block-box">
		  <div class="header blue"><h4>Kalender Kegiatan</h4></div>
		  <div class="content">
			<?php $this->view("calendar.php", $data); ?>
		  </div>
		</div>
		
		<div id="parameter" class="block-box">
		  <div class="header yellow"><h4>Aktifitas</h4></div>
		  <div class="content">
			<?php $this->view("home/event.php", $data); ?>
		  </div>
		</div>
	</div>
	<div class="col-md-6">		
		<div id="parameter" class="block-box">
		  <div class="header black"><h4>Rekap Finger</h4></div>
		  <div class="content">
			<div id="container-finger" style="height: 400px;"></div>
		  </div>
		</div>
	
		<div id="parameter" class="block-box">
		   <div class="header green"><h4>Catatan Kegiatan
			  <span class="pull-right heading-btn">
				  <a id="button_newtask" data-toggle="tab" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Tambah</a>
				  <a id="button_tasklist" data-toggle="tab" class="btn btn-default btn-sm" style="display:none"><i class="fa fa-bars"></i> List</a>
			  </span>
			  </h4>
          </div>
		  <div class="content">
			<?php $this->view("home/task.php", $data); ?>			
		  </div>
		</div>			
	</div>
	<?php } ?>
</div>

<?php
	/*-- skripsi --*/
	$rskripsi='';	
	$kategori_skripsi = '';
	$jml1 = 0;
	$total_juml1 = 0;
	$sum_jml1 = 0;
	$jml2 = 0;
	$total_juml2 = 0;
	$sum_jml2 = 0;
	$total_all = 0;
	
	$cat_pie = '';
	$jmls = 0;
	$tjmls=0;
	
	if($skripsi){
		$jml_1 = 0;
		$jml_2 = 0;
		foreach($skripsi as $key){		
			$rskripsi[$key->skripsi_status]['status'] = ucWords(strToLower($key->skripsi_status));
			if($key->dosen1){
				$rskripsi[$key->skripsi_status]['jml1'] = $key->jml;
			}
			if($key->dosen2){
				$rskripsi[$key->skripsi_status]['jml2'] = $key->jml;
			}	
			
		}
		
		$i=60;
		
		foreach($rskripsi as $key){
			$kategori_skripsi .= ", '" . $key['status']. "'";
			
				$jml1 .= ", ". $key['jml1'];	
				if(isset($key['jml1']) && $key['jml1']) $total_juml1 = $key['jml1'];	
				else 	$total_juml1 =0;		
				
				
			
				$jml2 .= ", ". $key['jml2'];		
				if(isset($key['jml2']) && $key['jml2']) $total_juml2 = $key['jml2'];	
				else 	$total_juml2 =0;	
				
			
				$total_all .= ", ". ($total_juml1+$total_juml2);
			
				$cat_pie.= ", { name: '" . $key['status'] . "',  color: 'rgba(255, ".$i.", 0, 1)', y:". ($total_juml1+$total_juml2)."}";
				
				$i = $i + 60;
		}
		$kategori_skripsi = substr($kategori_skripsi, 2);		
		$jml1 = substr($jml1, 2);	
		$jml2 = substr($jml2, 2);	
		$total_all = substr($total_all, 2);	
		$cat_pie = substr($cat_pie, 2);	
	
	}
	

	/*-- end skripsi */
	$absen='';
	
	if($hadir){
		foreach ($hadir as $key) {
		
				if(! isset($absen[$key->keterangan.$key->kelas.$key->prodi_id]) ){
					$absen[$key->keterangan.$key->kelas.$key->prodi_id]['keterangan'] = $key->keterangan . ' ('.$key->kelas.') '. $key->prodi_id;
					$absen[$key->keterangan.$key->kelas.$key->prodi_id]['hadir'] = $key->jml;
				}
			
		}
		
	}

	$kategori = '';
	$masuk = '';
	$masuk_kumulatif = 0;
	$kumulatif = '';
	$max_tmp = '';
	
	if($hadir){
		foreach($absen as $key){
			$kategori .= ", '" . $key['keterangan'] . "'";
			$masuk .= ", ". $key['hadir'];
		
			$masuk_kumulatif += $key['hadir'];
			$kumulatif .= ", ". $masuk_kumulatif;
			
			$max_tmp .= ", null";
		}
		$kategori = substr($kategori, 2);
		$kumulatif = substr($kumulatif, 2);
		$max_tmp = substr($max_tmp, 2);
		$masuk = substr($masuk, 2);
		
	
	}
	
	/*------------ finger ----------------*/
	$fmasuk = $ijin = $sakit = $alpha = $periode =  '';
	$max_f = 0;
	$max_b=0;
	$persen = 0;
	if($finger){
		foreach ($finger as $key) {
			$fmasuk .= ', '.$key->hadir;
			$ijin .= ', '.$key->ijin;
			$sakit .= ', '.$key->sakit;
			$alpha .= ', '.$key->alpha;
			
			$periode .= ', '. get_bulan($key->bulan);
			$max_b .=','.countDays($tahun, intval($key->bulan), array(0,6));
			$persen .= ','.number_format((($key->hadir/countDays($tahun, intval($key->bulan), array(0,6)))*100),2);
			
			$max_f += countDays($tahun, intval($key->bulan), array(0,6)); // 23
		}
	
		$fmasuk = substr($fmasuk, 2);
		$ijin = substr($ijin, 2);
		$sakit = substr($sakit, 2);
		$alpha = substr($alpha, 2);
		$periode = substr($periode, 2);
		$max_b = substr($max_b, 2);
		$persen = substr($persen, 2);
	}
	
	/*-------- hr mengajar ----*/
	$hr=0;
	$periode_hr='';
	if($rekap_hr){		
		foreach($rekap_hr as $key):
			$hr .= ', '.$key->total;
			$periode_hr .= ', '. get_bulan($key->bulan);			
		endforeach;
		$hr = substr($hr,2);
		$periode_hr=substr($periode_hr,2);
	}
	
	function get_bulan($init){
		switch ($init) {
			case '01' : return '"Jan"';break;
			case '02' : return '"Feb"';break;
			case '03' : return '"Mar"';break;
			case '04' : return '"Apr"';break;
			case '05' : return '"Mei"';break;
			case '06' : return '"Jun"';break;
			case '07' : return '"Jul"';break;
			case '08' : return '"Agt"';break;
			case '09' : return '"Sep"';break;
			case '10' : return '"Okt"';break;
			case '11' : return '"Nov"';break;
			case '12' : return '"Des"';break;
			default: return '"Jan"'; break;
		}
	}
	
	function countDays($year, $month, $ignore) {
	    $count = 0;
	    $counter = mktime(0, 0, 0, $month, 1, $year);
	    while (date("n", $counter) == $month) {
	        if (in_array(date("w", $counter), $ignore) == false) {
	            $count++;
	        }
	        $counter = strtotime("+1 day", $counter);
	    }
	    return $count;
	}
	
	/*------------ finger ----------------*/
?>
<?php $this->foot(); ?>


<style type="text/css">
	#container {
		height: 400px; 
		min-width: 310px; 
		max-width: 800px;
		margin: 0 auto;
	}
</style>

<script type="text/javascript">
$(function () {		
	Highcharts.getOptions().plotOptions.pie.colors = (function () {
        var colors = [],
            base = Highcharts.getOptions().colors[0],
            i;

        for (i = 0; i < 10; i += 1) {
            // Start out with a darkened base color (negative brighten), and end
            // up with a much brighter color
            colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        }
        return colors;
    }());		

	    $('#container').highcharts({
	
	        chart: {
	            type: 'column',
				  marginTop: 90,
            	marginRight: 40,
	            options3d: {
	                enabled: true,
	                alpha: 10,
					beta: 25,
					depth: 70
					}
	        },
			
	        title: {
	            text: 'Daftar Hadir <?php echo $keterangan ?>'
	        },
			subtitle: {
	            text: '<?php echo $fakultas ?>'
	        },
	        xAxis: {
	        	categories: [<?php echo $kategori ?>],
	        	labels:
				{
					enabled: true
				}
	        },
	
	        yAxis: {
				 opposite: false,
	            allowDecimals: false,
	            min: 0,
	            title: {
	                text: 'Jumlah Kehadiran'
	            }
	        },
	
	        tooltip: {
	            headerFormat: '<b>{point.key}</b><br>',
	            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
	        },
	
	        plotOptions: {
			 column: {
				stacking: 'normal',
                depth: 40,
				dataLabels:{
					enabled:true,
					formatter:function() {
						var pcnt = (this.y / <?php echo $max ?>) * 100;
						if(pcnt > 0) return '<span style="color:#fff">'+Highcharts.numberFormat(pcnt) + '%</span>';
					}
				}
            }
	           
				
	        },
	
	        series: [{
	            name: 'Kehadiran',
				type: 'column',
	            data: [<?php echo $masuk ?>],
	            stack: 'hadir',
	            color: '#2980b9'
	        }]
	    });
	
	
	$('#container-finger').highcharts({
	
	        chart: {
	            type: 'line',
	            options3d: {
	                enabled: false,
	                alpha: 10,
	                beta: 20,
	                depth: 60
	            },
	            marginTop: 90,
            	marginRight: 40
	        },
			
	        title: {
	            text: 'Daftar Hadir Finger  <?php echo date("Y"); ?>'
	        },
			subtitle: {
	            text: '<?php echo $keterangan ?>'
	        },
	        xAxis: {
	        	categories: [<?php echo $periode ?>],
	        	labels:
				{
					enabled: true
				}
	        },
	
	        yAxis: {
	            allowDecimals: false,
	            min: 0,
	            title: {
	                text: 'Jumlah Kehadiran'
	            }
	        },
	
	        tooltip: {
	       
				valueSuffix: ' '
	        },
			
			
	        plotOptions: {
				line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
			
	        },
	
	        series: [
		        {
		            name: 'Masuk',
				    data: [<?php echo $fmasuk ?>]
		        },
		 
				{
		            name: 'Max Hadir',
					data: [<?php echo $max_b ?>]
		        },
				{
		            name: 'Persentase Kehadiran',
					data: [<?php echo $persen ?>]
		        }
	        ]
	    });
	$('#container-mengajar').highcharts({
	
	        chart: {
	            type: 'line',
	            options3d: {
	                enabled: false,
	                alpha: 10,
	                beta: 20,
	                depth: 60
	            },
	            marginTop: 90,
            	marginRight: 40
	        },
			
	        title: {
	            text: 'Rekap HR Mengajar  <?php echo date("Y"); ?>'
	        },
			subtitle: {
	            text: '<?php echo $keterangan ?>'
	        },
	        xAxis: {
	        	categories: [<?php echo $periode_hr ?>],
	        	labels:
				{
					enabled: true
				}
	        },
	
	        yAxis: {
	            allowDecimals: false,
	            min: 0,
	            title: {
	                text: 'Jumlah HR'
	            }
	        },
	
	        tooltip: {
	       
				valueSuffix: ' '
	        },
			
			
	        plotOptions: {
				line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
			
	        },
	
	        series: [
		        {
		            name: 'HR',
				    data: [<?php echo $hr ?>]
		        }
	        ]
	    });
		
		$('#container-skripsi').highcharts({
	
	      
	        title: {
	            text: 'Rekap Jumlah Bimbingan'
	        },
			subtitle: {
	            text: '<?php echo $keterangan ?>'
	        },
	        xAxis: {
	        	categories: [<?php echo $kategori_skripsi ?>],
	        	labels:
				{
					enabled: true
				}
	        },
	
	        yAxis: {
	            allowDecimals: false,
	            min: 0,
	            title: {
	                text: 'Jumlah Mhs'
	            },				
				stackLabels: {
					enabled: false,
					style: {
						fontWeight: 'bold',
						color: 'gray'
					}
				}
	        },
			labels: {
				items: [{
					html: 'Total bimbingan',
					style: {
						left: '10%',
						top: '0px',
						color: 'black'
					}
				}]
			},

	       
	        series: [
		        {
					type:'column',
					stacking: 'normal',
		            name: 'Pembimbing I',
				    data: [<?php echo $jml1 ?>],
					tooltip: {
							valueSuffix: ' mhs'
						}
		        },
		 
				{	
					type:'column',
					stacking: 'normal',
		            name: 'Pembimbing II',
					data: [<?php echo $jml2 ?>],
					tooltip: {
							valueSuffix: ' mhs'
						}
		        }, 
				{
				type: 'spline',
				name: 'Total',
				data: [<?php echo $total_all ?>],
					marker: {
						lineWidth: 2
					},
					tooltip: {
							valueSuffix: ' mhs'
						}
				},
				{
					type: 'pie',
					name: 'Total',
					data: [<?php echo $cat_pie ?>],
					center: ['6%', 35],
					size: 60,
					showInLegend: false ,
					allowPointSelect: true,
					dataLabels: {
						enabled: true,
						format: '{point.name}<br>{point.y} mhs',
						style: {
							fontSize:'9px',
							color: 'black'
						}
					}
				}
	        ]
	    });
	});
</script>
<?php //endif; ?>

