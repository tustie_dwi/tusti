<h3><?php echo peserta ?></h3>
<table class='table table-bordered'>
	<tbody>
		<tr>
			<td width="20%"><em><?php echo dosen ?>/Staff</em></td>
			<td>	<ul class="list-inline"><?php 
			if(count($staff)!=0){
				$i=0;
				foreach($staff as $dt):
					$i++;		
					if($i % 8){
						$str = "&nbsp;";
					}else{
						$str = "</br>";
					}
					
					echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama;		
					if($dt->instansi){
						echo "<code>".$dt->instansi."</code>";
					}	
					echo "</li> ";
				endforeach;
				
			}else{
				echo "-";
			}?></ul></td>
		</tr>		

		<tr>
			<td colspan=2><b><?php echo mahasiswa ?></b></td></tr>
		<tr>
			<td><em>&nbsp;&nbsp;By Angkatan</em></td>
			<td><ul class="list-inline"><?php 
			
			$nangkatan="";
				if(isset($mangkatan)){
					if(count($mangkatan)!=0){										
						$i=0;										
						foreach($mangkatan as $dt):
							$i++;											
							
							//$nangkatan = $nangkatan .$dt->group_by.",";		
							echo "<li class='label label-info' style='font-weight:normal'>".$dt->group_by."</li> ";	
						endforeach;										
					}
				}
			?>
		</ul></td></tr>
		<tr>
			<td><em>&nbsp;&nbsp;By <?php echo nama ?></em></td>
			<td width="80%"><ul class="list-inline"><?php 
			if(count($mhs)!=0){
				$i=0;
				foreach($mhs as $dt):
					$i++;	
					if($i % 8){
						$str = "&nbsp;";
					}else{
						$str = "</br>";
					}
					echo "<li class='label label-info' style='font-weight:normal'>".ucwords(strTolower($dt->nama));		
					if($dt->instansi){
						echo "<code>".$dt->instansi."</code>";
					}		
					echo "</li> ";
				endforeach;							
			}else{
				echo "-";
			}?></ul></td>
		</tr>	

		<tr>
			<td width="20%"><em><?php echo peserta_luar ?></em></td>
			<td width="80%"><ul class="list-inline"><?php 
			if(count($peserta)!=0){
			$i=0;
			foreach($peserta as $dt):
				$i++;							
				echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama;		
				if($dt->instansi){
					echo "<code>".$dt->instansi."</code>";
				}	
				echo "</li> ";
			endforeach;							
		}else{
			echo "-";
		}?></ul></td>
		</tr>								
	</tbody>
</table>