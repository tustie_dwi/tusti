<?php 
$this->view("page/header.php", $data); 
?>

  
		
	<?php
	if($kategori=='video' || $kategori=='video-streaming'):
		?>
		 <section class="content content-theater">
            	<div class="container">
            		<h2 class="title-theater margin-top-no"><span class="fa fa-video-camera"></span> Live Streaming</h2>
                	<div id="liveStream" class="video-content splash-video"></div>

		<?php
	else:
	?>
	<section class="content content-white">
		<div class="container">   
	<!-- Main Content -->
		<div style="color:#444;font:1.1em/1.562em 'Segoe UI',Tahoma,Arial,Helvetica,sans-serif;text-align:justify" class="<?php if(isset($kategori) && (($kategori=='lecturer') || ($kategori=='staff')|| ($kategori=='dosen')|| ($kategori=='staff-kependidikan'))) echo "col-md-12"; 
					else echo "col-md-8"?>">	
			<?php	

		
			if(isset($detail)):
				if($kategori=='event'):
					$this->view("page/detail_event.php", $data);
				else:
					if(isset($detail->content_data) && ($detail->content_data=="map")):	
				
					else:	
						if(isset($detail->content_data) && $detail->content_data=="jadwal"){
							//$this->redirect($detail->content_data);
						}
				
					?>
					<!-- Edit Breadcrumb -->
					<ol class="breadcrumb" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<?php
					
						if(isset($detail) && (strToLower($detail->judul)!=$kategori)):
							if(isset($detail) && (isset($detail->parent))):?>
							  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php 
							if(isset($url)):
								echo $this->location($url.'/read/'.$kategori.'/'.$detail->id);
							else:
								echo $this->location('page/read/'.$kategori.'/'.$detail->id);
							endif; ?>" itemprop="url"><span itemprop="title"><?php echo ucfirst($detail->parent);?></span></a></li>
						<?php  else : ?>
							<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php 
							if(isset($url)):
								echo $this->location($url.'/read/'.$kategori);
							else:
								echo $this->location('page/read/'.$kategori);
							endif; ?>" itemprop="url"><span itemprop="title"><?php echo ucfirst($kategori);?></span></a></li>
							<?php endif; 
						endif;
						?>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($detail->judul) echo $detail->judul;else echo $detail->judul_ori;	?></span></li>
					</ol>
					<!-- End Breadcrumb -->
					
					<!-- Edit Content Article -->
					<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-sm margin-bottom-no" itemprop="name"><?php if($detail->judul) echo $detail->judul; 
					else echo $detail->judul_ori;
				
					?></h1>
						<div class="content-detail-description margin-bottom-sm">
						<!--<span class="author"><span class="fa fa-user"></span>Humas</span>-->
						<?php if(isset($detail->content_modified)): ?>
						<span class="fa fa-calendar"></span><time  datetime="<?php echo date("Y-m-d H:i:s", strtotime($detail->content_modified)); ?>" itemprop="datePublished"><?php echo date("M d, Y", strtotime($detail->content_modified)); ?></time>
						<?php endif;?>
						<span class="category"><span class="fa fa-tags"></span><?php echo $kategori; ?></span>
					</div>
					<?php
					endif;
					if($kategori=='course' || $kategori=='matakuliah' ):
						$mconf = new model_page;
						$komponen = $mconf->read_silabus("",$lang,$detail->id);
						
						foreach($komponen as $dt): 
								echo "<h3>".$dt->komponen."</h3>";
								echo $dt->keterangan;
						endforeach; 
						?>
						<hr>
						<div class="pull-right"><a class="btn btn-danger" href="<?php echo $this->location('auth'); ?>">Go to Course</a></div>
						<?php
					else:
						if($detail->keterangan)	$strcontent	= $detail->keterangan;
						else $strcontent = $detail->keterangan_ori;	
					
									?>
						
						<section itemprop="articleBody">
							<?php
							echo add_alt_tags($mpage->str_content($strcontent,$this->config->file_url_view,$this->config->file_url_old),"a"); 
							
							if(isset($detail->content_data)):
								if(isset($url)) $url_link=$url;
								else $url_link = "";
								$this->read_data($detail->content_data, $url_link);
							endif;
							?>
							</section>
							<?php
								
					endif;
					if($kategori=='news'):
						$data['comment'] = $detail->content_comment;
						$data['id']		 = $detail->id;
						$data['mpage']	 = $mpage;
						
						//$this->view("page/comment.php", $data);
					endif;
				endif;
				?>
				</section>
				<!-- End Edit Content Article -->
				<?php
				
				
			
			endif;	
			
				
			if( isset($post) ) : 
				?>
				
				<!-- Edit Breadcrumb -->
					<ol class="breadcrumb" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if(($lang=='en')&&($kategori=='pengumuman')) echo "Announcement";				
						else echo ucWords($kategori);	?></span></li>
					</ol>
					<!-- End Breadcrumb -->
					
				
				<table class="table web-page">
					<thead style="display:none">
						<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
					</thead>
					<tbody>
					<?php
						if($post) : 
						foreach($post as $dt) { 
							if(isset($dt->judul)):
							$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
						?>
						<tr style="border-top:0px;">
								<td style="display:none;border:0px;"></td>
							<td style="border-top:0px;">
								<?php 
								if(isset($url)):
									if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
									else $url_content = $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
								else:
									if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
									else $url_content = $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
								endif;
								
								if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
								else $imgthumb = $this->config->default_thumb_web;
								?>
								
                               
                                    <?php 
									if($kategori=='news' || $kategori=='course' || $kategori=='matakuliah') :
									?>
									 <div class="media">
										<a class="pull-left" href="<?php echo $url_content; ?>">
											<?php if($kategori=='news') : ?>
											<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="200px">
											<?php else : ?>
											<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="100px">
											<?php endif; ?>
										</a>
										
										 <div class="media-body">
											<a class="title-article" href="<?php echo $url_content; ?>"><h4 class="media-heading"><?php echo $dt->judul; ?></h4></a>
												<?php if($kategori=='news'){
													?>
													<small>
													<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43">&nbsp;<?php echo date("M d, Y", strtotime($dt->content_modified)); ?></time>
													<span class="category"><span class="fa fa-tags"></span>&nbsp;<?php echo $kategori; ?></span>
													</small><p></p>
													<!--<time class="time-post"><small><?php //if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
													//else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>-->
												<p class="post-content">
												<?php
												$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
												$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
												$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
											   echo htmlspecialchars($isi);
					
											?>
											<?php //var_dump($dt); ?>
												<a href="<?php echo $url; ?>" class="more-orange">more..</a>
											</p>
											<?php } ?>
										</div>
									</div>					
								<?php 
								else:
									
								?>
									<a class="title-article" href="<?php echo $url_content; ?>"><?php if($dt->judul) echo $dt->judul;
									else echo $dt->judul_ori;
									?></a>
									<?php if($kategori!='course' || $kategori!='matakuliah'): ?>
									<time class="time-post"><small><?php if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
									else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>
									<?php
									$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
												$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
												$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
											//   echo htmlspecialchars($isi);
									endif;
								endif;?>
                                   
							</td>
						</tr>
						<?php	
							endif;
						} 
						endif;	
					?>
				</tbody>
			</table>	
		<?php
			
		endif; 
		?>
					
	</div>
	<!-- End Main Content -->
	
	<!-- Sidebar -->
	<div class="col-md-4">
	<?php
		if(isset($detail->content_data) && (($detail->content_data=="map")||($detail->content_data=="staff/dosen")|| ($detail->content_data=="staff/staff")|| ($detail->content_data=="s2/dosen"))):	
			//var_dump($detail->content_data);
		else:
			
			$this->view("page/sidebar.php",$data);
		endif;
	endif;
	?>
	 </div>
	 <!-- End Sidebar -->
	 
	   </div>
    </section>
	
<?php $this->view("page/footer.php", $data); 
function add_alt_tags($content,$title)
{
        preg_match_all('/<img (.*?)\/>/', $content, $images);
        if(!is_null($images))
        {
			
			$mpage = new model_page();
						
                foreach($images[1] as $index => $value)
                {
				
					$imgsrc = explode('src', $value);
							preg_match_all('/".*?"|\'.*?\'/', $imgsrc[1], $source);
							
							if(isset($source[0][0])){
								$file_tmp = substr(str_replace('%20', ' ', $source[0][0]), 1, -1);
								$cek = explode('/', $file_tmp);
								$strtitle = end($cek);
								$strtitle = explode(".", $strtitle);
								$title  = reset($strtitle);
							}else{
								$file_tmp ="";
								$title = "-";
							}
							
					//if(!preg_match('/alt=/', $value))
                      //  {
							
				
							$new_img = str_replace('<img', '<a href="'.$file_tmp.'" class="fancybox content-gallery" title="'.$title.'"><img class="img-gallery" alt="'.$title.'"', $images[0][$index])."</a>";
							
							/*$print = base64_encode(md5(urlencode($file_tmp)));
						
							$safe = $file_tmp;
			
							if($mpage->check_pict_session($print, $safe, 'check')==FALSE){
								$datanya = array('val_session' => $print,
												 'val_url' => $safe
												);
								$mpage->save_pict_session($datanya);
							}
			
							$new_img = str_replace('<img', '<a href="https://'.$_SERVER['HTTP_HOST']."/page/read_/".$print.'" class="fancybox content-gallery" title="'.$title.'"><img class="img-gallery" alt="'.$title.'"', $images[0][$index])."</a>";*/
							$content = str_replace($images[0][$index], $new_img, $content);
								
                       // }
                }
        }
        return $content;
}


?>