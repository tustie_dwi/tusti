<?php 
$this->view("page/header.php", $data); 
?>

  
	<section class="content content-white">
		<div class="container">   
	<!-- Main Content -->
		<div style="color:#444;font:1.1em/1.562em 'Segoe UI',Tahoma,Arial,Helvetica,sans-serif" class="col-md-12">	
			<!-- Edit Breadcrumb -->
			<ol class="breadcrumb" itemprop="breadcrumb">
			  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
				else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
				
			  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if(($lang=='en')&&($kategori=='pengumuman')) echo "Announcement";				
				else echo ucWords($kategori);	?></span></li>
			</ol>
			<!-- End Breadcrumb -->
			<section itemscope="" itemtype="http://schema.org/Article">
				<section itemprop="articleBody">
		<?php
			
				
			if( isset($post) ) : 
				?>
				
				
							<div class="row">
								<div class="col-md-12">
			                    	<div class="row">
			                    		<div class="col-sm-9  col-sm-push-3">
											
			                    			<div class="content-filter">
					                        	<div class="row">
					                        		<div class="tab-content">
														<?php 
														if($post): 
															foreach($post as $key):
															?>
																<div class="tab-pane text-justify" id="<?php echo strtolower($key->id);  ?>"><?php read_content($mpage, $lang, $unit, $key->unit_id); ?></div>	
															<?php 
															endforeach;
														endif; ?>
															
					                        		</div>
					                        	</div>
					                        </div>
			                    			
			                    		</div>
			                    	
			                    		<div class="col-sm-3 col-sm-pull-9">							
					                      
			                    			<div class="menu-filter left-pane menu-filter-other"> 
					                        	<div class="menu-filter-box">
						                        	<ul class="filters list-unstyled"> 
						                        		
														<?php 
														if($post): 
															foreach($post as $key):
															?>
																<li><a href="#<?php echo strtolower($key->id);  ?>" role="tab" data-toggle="tab" class="filter"><?php  echo $key->unit; ?></a></li>	
															<?php 
															endforeach;
														endif; ?>
						                        	</ul> 
					                        	</div>
					                        </div>
			                    		</div>
			                    </div>
			                        
			                </div>
		                </div>
	              
				
			<?php endif;
			if(isset($detail)):
				?>
				<div class="row">
					<div class="col-md-8">
						<div class="content-filter">
						<div class="row">
							<div class="tab-content">							
								<div class="tab-pane text-justify" id="<?php echo strtolower($detail->id);  ?>"><?php read_content($mpage, $lang, $unit, $detail->unit_id); ?></div>																	
							</div>
						</div>
					</div>
					</div>
					<div class="col-md-4">
					<?php
						
							$this->view("page/sidebar.php",$data);
					?>
					 </div>
				 </div>
				<?php
			endif;
			?>
				  </section>
                </section>
			</div>
	   </div>
    </section>
	
<?php $this->view("page/footer.php", $data); 

function read_content($mpage=NULL, $lang=NULL, $unit=NULL, $unitid=NULL){
	$post = $mpage->read_content($unitid, $lang,'inside_menu');	
	
	if($post){
		
			?>
				<ul class="nav nav-pills nav-pills-other nav-pills-color" id="writeTab">
					<?php 
					$arr_data=array();
					$j=0;
					foreach ($post as $dt):
						$j++;
						if($j==1) $strclass=  "active ";
						else $strclass="";
						
						$arr_temp = array("id"=>$dt->id, "content"=>$dt->keterangan, "class_str"=>$strclass);
						array_push($arr_data, $arr_temp);
						?>
						 <li class="<?php echo $strclass;?>"><a href="#<?php echo $dt->id ?>" role="tab" data-toggle="tab"><?php  echo $dt->judul; ?></a></li>
						<?php
					endforeach;
				
					?>					
				</ul>

				<div class="tab-content">
					<?php
					for($i=0;$i<count($arr_data);$i++){						
						
						?>
						 <div class="tab-pane <?php  echo $arr_data[$i]["class_str"];?>" id="<?php  echo $arr_data[$i]["id"];?>"><?php  echo $arr_data[$i]["content"];	?></div>
						<?php
					}
					?>
				</div>					
			<?php
		
	}
}


function add_alt_tags($content,$title)
{
        preg_match_all('/<img (.*?)\/>/', $content, $images);
        if(!is_null($images))
        {
			
			$mpage = new model_page();
						
                foreach($images[1] as $index => $value)
                {
				
					$imgsrc = explode('src', $value);
							preg_match_all('/".*?"|\'.*?\'/', $imgsrc[1], $source);
							
							if(isset($source[0][0])){
								$file_tmp = substr(str_replace('%20', ' ', $source[0][0]), 1, -1);
								$cek = explode('/', $file_tmp);
								$strtitle = end($cek);
								$strtitle = explode(".", $strtitle);
								$title  = reset($strtitle);
							}else{
								$file_tmp ="";
								$title = "-";
							}
							
					//if(!preg_match('/alt=/', $value))
                      //  {
							
				
							$new_img = str_replace('<img', '<a href="'.$file_tmp.'" class="fancybox content-gallery" title="'.$title.'"><img class="img-gallery" alt="'.$title.'"', $images[0][$index])."</a>";
							
							/*$print = base64_encode(md5(urlencode($file_tmp)));
						
							$safe = $file_tmp;
			
							if($mpage->check_pict_session($print, $safe, 'check')==FALSE){
								$datanya = array('val_session' => $print,
												 'val_url' => $safe
												);
								$mpage->save_pict_session($datanya);
							}
			
							$new_img = str_replace('<img', '<a href="https://'.$_SERVER['HTTP_HOST']."/page/read_/".$print.'" class="fancybox content-gallery" title="'.$title.'"><img class="img-gallery" alt="'.$title.'"', $images[0][$index])."</a>";*/
							$content = str_replace($images[0][$index], $new_img, $content);
								
                       // }
                }
        }
        return $content;
}


?>