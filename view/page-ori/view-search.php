<?php 
$this->view("page/header.php", $data); 

?>

  
<section class="content content-white">
	<div class="container">   		

	<!-- Main Content -->
	<div class="col-md-8">	
		<!-- <div class="input-group">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button">Cari <i class="fa fa-search"></i></button>
          </span>
          <input type="text" class="form-control" onkeyup="get_konten(this.value)">
        </div> -->
        
		<div class="table-responsive">
			<table class="table">
			<thead style="display:none">
				<tr><td>&nbsp;</td></tr>
			</thead>
					
				<tbody id="konten-wrap">
					
				</tbody>
			</table>
			
			<div>
				<ul id="pagination" class="pagination pull-right">
				  
				</ul>
			</div>
		</div>
	</div>
	<!-- End Main Content -->

	<!-- Sidebar -->
	<div class="col-md-4">
	<?php
		 $this->view("page/sidebar.php",$data);
	?>
	 </div>
	 <!-- End Sidebar -->
	 
	   </div>
</section>

<?php $this->view("page/footer.php", $data); ?>
<script type="text/javascript" charset="utf-8">
	
	$(document).ready(function(){
		var url_ = base_apps + 'page/search_json/<?php echo $search ?>';
		
		console.log(url_);
		
		$.ajax({
	        url : url_,
	        type: "POST",
	        dataType : "json",.
	        success:function(msg) 
	        {
	        	konten = msg;
	        	get_konten('');
	        }
	    });
	});
	
</script>
