  <?php 
  if(!isset($_COOKIE['lang-switch'])){
	  	setcookie('lang-switch','en',time()+3600*24,"/");
  		include("library/en.php");
	  	$data['lang']="en";
  }
  else{
  		include("library/".$_COOKIE['lang-switch'].".php");
	  	$data['lang']=$_COOKIE['lang-switch'];
  }
  ?>
<!DOCTYPE html>
<html lang="<?php if($data['lang']=='en'){echo $data['lang'];}else{echo "id";}?>" itemscope="" itemtype="http://schema.org/WebPage">
  <meta charset="utf-8">
  
  	<?php if(isset($detail) && !isset($unit_list)){?>
    <title><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; echo " | "; //echo $this->page_title(); ?>PTIIK</title>
    <meta property="og:title" content="<?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; ?>">
    <?php }else if(isset($unit_list)){?>
    	<?php if(isset($detail)){?>
    		   	<title><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; echo " | "; echo $unit_list->unit; ?> PTIIK</title>
    			<meta property="og:title" content="<?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; ?>">
   	    <?php }else{ ?>
   	    		<title><?php echo $unit_list->unit;echo " | ";//echo $this->page_title(); ?>PTIIK</title>
   	    		<meta property="og:title" content="<?php echo $unit_list->unit;//echo " | ";echo $this->page_title(); ?>">
   	    <?php } ?>
    <?php }else{ ?>
    <title><?php echo $this->page_title(); ?></title>
    <meta property="og:title" content="<?php echo $this->page_title(); ?>">
    <?php }	?>
    <meta property="og:type" content="website" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Official Site - <?php echo $this->page_title(); ?> Universitas Brawijaya">
	<meta name="keywords" content="learning,informatika,informatic,sistem komputer,sistem informasi,information systems,computer systems,ilmu komputer,computer science,ptiik,unibraw">
    <meta name="author" content="BPTIK">
    <meta name="HandheldFriendly" content="true">

    
    <meta property="og:site_name" content="<?php echo $this->page_title(); ?>">
    <meta property="og:url" content="<?php echo full_url_path();?>">
   	<?php if(isset($unit_list)&&$unit_list->logo!=""){ ?>
    <meta property="og:image" content="<?php echo $this->config->file_url_view."/".$unit_list->logo;?>">
    <?php }else{?>
    	
    <meta property="og:image" content="<?php echo $this->asset("ptiik/images/ptiik.png");?>">
    	<?php } ?>
    <meta property="og:description" content="Official Site - <?php echo $this->page_title(); ?> Universitas Brawijaya">
    <meta name="rating" content="Mature" />
    <link href="<?php 
	if(! isset($unit_list)):
		// echo $this->asset("ptiik/css/base.orange.min.css"); 
		/*if(isset($_COOKIE['stylecolor'])):
			echo $this->asset("ptiik/css/base.".$_COOKIE['stylecolor'].".min.css"); 
		else:*/
			echo $this->asset("ptiik/css/base.orange.min.css"); 
		//endif;
	else:
		
		if((($unit_list->kategori=='laboratorium')||(isset($kategori)=='laboratorium')) && $unit_list->kategori!='prodi' ):
			if(isset($_COOKIE['stylecolor'])):
				echo $this->asset("ptiik/css/base.".$_COOKIE['stylecolor'].".min.css"); 
			else:
				echo $this->asset("ptiik/css/base.green.min.css"); 
			endif;
		else:
			if(isset($_COOKIE['stylecolor'])):
				echo $this->asset("ptiik/css/base.".$_COOKIE['stylecolor'].".min.css"); 
			else:
				echo $this->asset("ptiik/css/base.blue.min.css"); 
			endif;
		endif;
	endif;
	
	
	?>" rel="stylesheet">
	<link href="<?php echo $this->asset("ptiik/css/insideptiik.slider.min.css"); ?>" rel="stylesheet">
	<!-- Edit 1 -->
	<link href="<?php echo $this->asset("ptiik/css/jquery.fancybox.css"); ?>" rel="stylesheet">
	<!-- End Edit 1 -->
	
	
	 <?php $styles = $this->get_styles(); 
        if(is_array( $styles )) : 
    	
		foreach($styles as $s) : 
	?><link href="<?php echo $this->asset($s); ?>" rel="stylesheet">
	<?php endforeach; endif; ?>
	<?php if( isset($mstyles) and is_array($mstyles)) { 
	foreach( $mstyles as $s) : ?><link href="<?php echo $this->location($s); ?>" rel="stylesheet">
	<?php endforeach; } 
	
	?>

    <!-- Le fav and touch icons -->
   <link href="<?php echo $this->location('rss/feed'); ?>" title="Program Teknologi Informasi dan Ilmu Komputer (PTIIK) Universitas Brawijaya RSS Feed" type="application/rss+xml" rel="alternate">
    
	<link rel="shortcut icon" href="<?php echo $this->asset("images/favicon.ico"); ?>"/>
    <!-- Le base URL helper for javascript -->
    <script type="text/javascript">
		var base_url = '<?php if(isset($url)) echo $this->location($url); 
		else echo $this->location(); ?>';
		var base_apps = '<?php echo $this->location(); ?>';
    </script>   

<style>
h1{
	font-size:24px;
}
		 .left {
  border: 0 none;
  float:left;
  margin:12px;
}

.right{
  border: 0 none;
  float:right;
  margin:12px;
}
	</style>	
   
  </head>
      <body>
        <header id="header">
            <section class="nav-top">
                <div class="container">
                    <div class="nav-header">
                      
                        <a class="nav-brand nav-toggle collapsed" data-toggle="collapse" data-target="#nav-top-collapse" href="#">
                            <span class="fa fa-cog"></span> Action
                        </a>
                    </div>
                    <div class="nav-top-collapse" id="nav-top-collapse">
                        <ul class="nav-top-nav nav-left">

											<li><a href="http://ub.ac.id" target="_blank" title="UB Official">UB Official</a></li>
											<li><a href="http://bits.ub.ac.id" target="_blank" title="BITS">BITS</a></li>
											<li><a href="http://mail.ub.ac.id" target="_blank" title="Webmail">Webmail</a></li>
											<li><a href="http://prasetya.ub.ac.id" target="_blank" title="UB News">UB News</a></li>
                        </ul>
                         
                        <ul class="nav-top-nav nav-right">
                           <!--<li><a href="#"><span class="fa fa-facebook"></span><span class="hide-large"> Facebook</span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span><span class="hide-large"> Twitter</span></a></li>
-->
							 <li><a href="#" class="font-red lang-switch with-tooltip" data-toggle="tooltip" data-placement="bottom" title="Bahasa Indonesia" data-lang="in">ID</a></li>
                            <li><a href="#" class="font-red lang-switch with-tooltip" data-toggle="tooltip" data-placement="bottom" title="English" data-lang="en">EN</a></li>
                            <li>
                                <a href="<?php echo $this->location('auth'); ?>" class="with-tooltip" data-toggle="tooltip" data-placement="bottom" title="Sign In"> <span class="fa fa-unlock-alt"></span> Sign In</a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </section>
            <section class="top-header <?php if(isset($view_slide)) echo "with-bg"; ?>">
                <div class="container">
                    <div class="row">
						<?php 
						if(isset($unit_list)): 
							if($unit_list->logo) $img = $this->config->file_url_view."/".$unit_list->logo;
							else $img = $this->config->default_thumb_web;
						?>
								<div class="col-md-6">
									<a href="<?php echo $this->location($url); ?>" class="image-link">
										<img class="img-logo-header img-responsive" src="<?php echo $img; //$mimg->base64_image($img);?>" alt="Image Logo">
									</a>
								</div>
								 <div class="col-md-6">
									<div class="navigation-top-header">
										<a href="<?php echo $this->location(); ?>" class="image-link">
											<img class="img-logo-header img-responsive" src="<?php echo $this->asset("ptiik/images/ptiik.png");//$mimg->base64_image($this->asset("ptiik/images/ptiik.png"));?>" alt="Image Logo">
										</a>
									</div>
								</div>
						<?php else:?>
							<div class="col-md-6">
									<a href="<?php echo $this->location(); ?>" class="image-link">
										<img class="img-logo-header img-responsive" src="<?php echo $this->asset("ptiik/images/ptiik.png"); //$mimg->base64_image($this->asset("ptiik/images/ptiik.png"));?>" alt="Image Logo">
									</a>
								</div>
								<div class="col-md-6">
								
									<div class="navigation-top-header">
										<ul class="navigation-menu">
											<!--<li><span class="nav-text-nav nav-right"><i class="fa fa-language"></i>&nbsp;<a href="#" class="font-red lang-switch" data-lang="in">ID</a> | <a href="#" class="font-blue lang-switch" data-lang="en">EN</a></span></li>-->
										</ul>
									</div>
									
								</div>
						<?php endif;?>
                    </div>
                </div>
            </section>
            <section class="nav-main">
                <div class="container">
                    <div class="nav-header">
                        <button type="button" class="nav-toggle collapsed" data-toggle="collapse" data-target="#nav-main-collapse">
                            <span class="fa fa-bars"></span>
                        </button>
                        <span class="nav-main-menu">Menu</span>
                    </div>
                    <div class="nav-main-collapse" id="nav-main-collapse">
                        <ul class="nav-main-nav nav-left" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
							<?php if(isset($unit_list)): ?>
							<li class="active"><a itemprop="url" href="<?php echo $this->location($url); ?>" title="Home"><span class="fa fa-home"></span> <span class="visible-sm-inline visible-xs-inline" itemprop='name'>Home</span></a></li>
							<?php else: ?>
                            <li class="active"><a itemprop="url" href="<?php echo $this->location(); ?>" title="Home"><span class="fa fa-home"></span> <span class="visible-sm-inline visible-xs-inline" itemprop='name'>Home</span></a></li>
                         	<?php
							endif;
							
							if($page):
							
							
								foreach($page as $dt):
									$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
									if($child):
										?>
										<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown" ><?php echo $dt->content_title?></a>
												<?php
													$i=0;
													 $count=round(count($child)/2);
													 $parent=0;
												?>
												
												<div class="dropdown-menu with-background background-bottom-right<?php if($count+1<4)echo " mini-dropdown"?>">
												
												
												<div class="<?php if($count+1<4)echo " col-md-12";
												else echo "col-md-5";
												?>">
												<ul class="menu-sub" role="menu">
															
													 <?php 
													 
													 foreach ($child as $key):
														 $i++;
														if(isset($unit_list)):
															read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location($url) );
														else:
															read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location('page') );
														endif;
													 endforeach; ?>
												</ul>
												</div>
												
												<img src="<?php echo $this->asset("ptiik/images/back-submenu3.png");?>" class="img img-responsive img-navbar-header" alt="Image Dropdown">
												</div>
										</li>
										<?php
									else:
										
										if(isset($unit_list)):
											echo "<li><a href=".$this->location($url.'/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
											//echo "<li><a href=".$this->location('page/read/'.$dt->content_page.'/'.$dt->id).">".$dt->content_title."</a></li>";
										else:
											echo "<li><a href=".$this->location('page/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
										endif;
									endif;

								endforeach;
								
								
								
							endif;
							
							if(isset($unit_list) && ($unit_list->kategori=="laboratorium")): 
							?>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo praktikum ?></a>
									<div class="dropdown-menu with-background background-bottom-right mini-dropdown">
									<div class="col-md-5">
									<ul class="menu-sub" role="menu">
									<?php
										echo "<li><a href=".$this->location(strtolower($url).'/asisten').">".asisten_lab."</a></li>";
										echo "<li><a href=".$this->location(strtolower($url).'/jadwal').">".jadwal_praktikum."</a></li>";
										echo "<li><a href=".$this->location(strtolower($url).'/nilai').">".nilai_praktikum."</a></li>";
										?>
									</ul>
									</div>
					
									<img src="<?php echo $this->asset("ptiik/images/back-submenu3.png");?>" class="img img-responsive img-navbar-header" alt="Image Dropdown">
								</div>
								</li>
								<?php
							endif;
						?>
                        </ul>
                        <form id="form-search" method="POST" action="<?php 
                        if(isset($unit_list)):
                        	echo $this->location($url.'/search');
						else:
							echo $this->location('page/search');
						endif; ?>" class="nav-form-nav nav-top-nav nav-right">
                            <input id="searchTxt" onkeyup="return cari_konten(event)" name="searchTxt" type="text" class="form-control" placeholder="Type here to search.." value="">
                            <span id="searchBg"></span>
                            <button id="searchIcon" name="searchIcon" type="submit" class="search-button"><span class="fa fa-search"></span></button>
                        </form>
                        
                    </div>
                </div>
            </section>
			<?php if(! isset($view_slide)): ?>
            <div class="slide <?php //if(isset($unit_list)) echo " mini-slide"; ?>">
                <div  id="slider" class="owl-carousel">
                   
					<?php
					if(isset($slide) && ($slide)):
						foreach($slide as $dt):
							
						?>
						 <div class="item">
								<img src="<?php echo str_replace(" ","%20",$this->config->file_url_view.'/'.$dt->file_loc);?>" alt="Image Slider">
								<div class="caption">		
									<?php
									 if($dt->file_title!=""){ ?>							
									<h1 class="title"><?php echo $dt->file_title?></h1>
									<?php } ?>
									<?php if($dt->file_note!=""){ ?>	
									<div class="detail">
										<?php echo $dt->file_note?>
									</div>
									<?php } ?>
								</div>
							</div>
						<?php
						endforeach;
					else:
					?>
						 <div class="item">
							<img src="<?php echo "http://adl.ptiik.ub.ac.id/fileupload/assets/upload/file/PTIIK/konten/slider/2014-10/darkblackpatern.png";//$mimg->base64_image($this->asset("ptiik/images/slide-1.jpg"));?>" alt="Image Slider">
							<div class="caption">
							</div>
						</div>
						
					<?php
					endif;
					?>
                  
                   
                </div>
                <div class="slide-navigation">
                    <a href="#" class="owl-prev slide-prev"><span class="fa fa-chevron-left"></span></a>
                    <a href="#" class="owl-next slide-next"><span class="fa fa-chevron-right"></span></a>
                </div>
            </div>
<?php 
			endif;
			
    		function read_sub_menu($i,$count,$parent,$unit=NULL, $lang=NULL, $mpage=NULL, $id=NULL, $title=NULL, $content_page=NULL,$url_menu=NULL){
												
					$child = $mpage->read_sub_content($unit, $lang, "", $id, $title);
					if($i==$count+1 && $parent==0 && ($count+1>=4)){
						echo '</ul></div>
						<div class="col-md-5">
												<ul class="menu-sub" role="menu">';
						
					} 
					
					if($child):
						?>
						<li class="nav-head">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $title?></a>
								<div class="" style="margin-left:40px;">
								<ul class="menu-sub" role="menu">
										 <?php 
										 $parent=1;
										 foreach ($child as $key):
												read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id,$key->content_title,$key->content_page, $url_menu);
										 endforeach; ?>
								</ul>
								</div>
						</li>
						<?php
					else:
					
						//echo "<li><a href=#>".$title."</a></li>";
						if(isset($unit_list)):
							echo "<li><a itemprop='url' href=".$this->location($url.'/read/'.$content_page.'/'.$id)." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
							//echo "<li class='nav-list'><a href=".$url_menu.'page/read/'.$content_page.'/'.$id.">".$title."</a></li>";
						else:
							echo "<li class='nav-list'><a itemprop='url' href=".$url_menu.'/read/'.$content_page.'/'.$id." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
						endif;
					
					endif;
					
					if($i==$count+1 && $parent==0 && ($count+1>=4)){
						//echo '</ul></div>';
						
					} 
			}		

function full_url_path()
{
    $s = &$_SERVER;
    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
    $sp = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
    $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    $uri = $protocol . '://' . $host . $s['REQUEST_URI'];
    $segments = explode('?', $uri, 2);
    $url = $segments[0];
    return $url;
}
    ?>
        </header>
        <section id="wrapper">
		