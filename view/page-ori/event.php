<?php 
$this->view("header-page.php", $data); 
$mpage = new model_page();
?>


<!-- Main Content -->
	<section class="col-sm-8">
		<section class="main-content">
			<section class="news-list">
				 <h2 class="title-content">PJJ Event</h2>
				<hr class="hr-content">
				<table class="table page table-noborder">
					<thead>
						<tr><td>&nbsp;</td></tr>
					</thead>
					<tbody>
				<?php 
						if($post ) : 
						foreach($post as $dt) { 
						?>
						<tr><td>
						<article>
							<div class="row">
								<div class="col-xs-2">
									<div class="time-posted-ribbon">
										<div class="time-posted-ribbon-date"><?php echo date("d", strtotime($dt->tgl_mulai)) ?></div>
										<div class="time-posted-ribbon-month"><?php echo date("F", strtotime($dt->tgl_mulai)) ?></div>
										<div class="time-posted-ribbon-year"><?php echo date("Y", strtotime($dt->tgl_mulai)) ?></div>
									</div>
								</div>
								<div class="col-xs-10">
									<!--<figure class="thumbnail-article always-full">
										<img src="https://175.45.187.253/beta/pjj/assets/upload/thumb/images.jpg" class="img img-responsive">
									</figure>-->
									<h3 class="title-article"><a class="title-article" href="<?php echo $this->location('page/event/'.$dt->page_link); ?>"><?php echo $dt->judul ?></a></h3>
									
									<?php $content=strip_tags($dt->keterangan);
									echo (strlen($content) > 300) ? substr($content,0,200).'...' : $content; ?>
									<a href="<?php echo $this->location('page/event/'.$dt->page_link); ?>" class="redmore-article">Read More</a>
								</div>
							</div>
						</article>
						</td></tr>
						<?php	
						}
					?>
					<?php endif; ?>
					</tbody>
				</table>
				
			</section>
		</section>
	</section>

		<!-- End Main Content -->

		<!-- Sidebar -->
		 <?php
		$data['mpage'] = $mpage;
		 $this->view("page/sidebar.php",$data);?>
		<!-- End Sidebar -->
	</section>
</section>
    
<?php $this->view("footer-page.php", $data); ?>