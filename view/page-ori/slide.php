	<ul>
          <li>
            <div class="slide-body" data-group="slide">
              <div class="container">
                <div class="wrapper">
                  <div class="caption img-2" data-animate="slideAppearLeftToRight" data-delay="200">
                    <img src="<?php echo $this->asset("images/banner.png");?>">
                  </div>
                  <div class="background-content">
                  <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="500" data-length="300">
                    <h2> Learning is changing</h2>
                    <div class="caption sub" data-animate="slideAppearLeftToRight" data-delay="800" data-length="300"><p>It's not just about ideas. It's about making ideas happen. Do it. Register here</p>
                    <!--<p>
                    <a href="<?php echo $this->location('auth'); ?>" class="btn btn-mod1">Login</a> <a href="<?php echo $this->location('register'); ?>" class="btn btn-mod1">Sign Up</a></p>--></div>
                  </div>
                  </div>
                  <!--<div class="caption img-3" data-animate="slideAppearLeftToRight">
                    <img src="assets/images/css3.png">
                  </div>-->
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="slide-body" data-group="slide">
              <div class="container">
                <div class="wrapper">
                  <div class="caption img-2" data-animate="slideAppearLeftToRight" data-delay="200">
                    <img src="<?php echo $this->asset("images/banner2.png");?>">
                  </div>
                  <div class="background-content">
                  <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="500" data-length="300">
                    <h2>Learning is changing</h2>
                    <div class="caption sub" data-animate="slideAppearLeftToRight" data-delay="800" data-length="300"><p>It's not just about ideas. It's about making ideas happen. Do it. Register here</p>
                    <!--<p>
                    <a href="<?php echo $this->location('auth'); ?>" class="btn btn-mod1">Login</a> <a href="<?php echo $this->location('register'); ?>" class="btn btn-mod1">Sign Up</a></p>--></div>
                  </div>
                  </div>
                  <!--<div class="caption img-5" data-animate="slideAppearDownToUp" data-delay="200">
                    <img src="assets/images/bootstrap.png">
                  </div>
                  <div class="caption img-4" data-animate="slideAppearUpToDown">
                    <img src="assets/images/twitter.png">
                  </div>-->
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="slide-body" data-group="slide">
              <div class="container">
                <div class="wrapper">
                  <div class="caption img-2" data-animate="slideAppearLeftToRight" data-delay="200">
                    <img src="<?php echo $this->asset("images/banner3.png");?>">
                  </div>
                  <div class="background-content">
                  <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="500" data-length="300">
                    <h2>Learning is changing</h2>
                    <div class="caption sub" data-animate="slideAppearLeftToRight" data-delay="800" data-length="300"><p>It's not just about ideas. It's about making ideas happen. Do it. Register here</p>
                    <!--<p>
                    <a href="<?php echo $this->location('auth'); ?>" class="btn btn-mod1">Login</a> <a href="<?php echo $this->location('register'); ?>" class="btn btn-mod1">Sign Up</a></p>--></div>
                  </div>
                  </div>
                  <!--<div class="caption img-1" data-animate="slideAppearDownToUp" data-delay="200">
                    <img src="assets/images/jquery.png">
                  </div>-->
                </div>
              </div>
            </div>
          </li>
        </ul>