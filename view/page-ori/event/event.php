<?php 
$this->view("page/header.php", $data); 

?>

  
<section class="content content-white">
	<div class="container">   		
	
	<input type="hidden" value="<?php echo $dateNow ?>" id="date-now"/>
	<input type="hidden" name="some_name" value="<?php echo $this->location('page/read/event/') ?>" id="url-event"/>	
	<!-- Main Content -->
	
	<div class="col-md-8">	
        <div class="row">
        	<div class="col-md-4">
        		 <div id="my-calendar"></div>
        		<p style="margin-top: 10px" class="page-header">Kategori Kegiatan</p>
				<ul class="nav nav-pills nav-stacked" id="nav-jenis-kegiatan" style="max-width: 300px;">
					<li role="presentation"><a onclick="today()">Today</a></li>
			      	<?php
				      	if($jenis_kegiatan){
				      		foreach($jenis_kegiatan as $key){
				      			echo '<li role="presentation"><a onclick="get_event(\''.$key->jenis_kegiatan_id.'\',\''.$key->keterangan.'\')">'.$key->keterangan.'</a></li>';
				      		}
				      	}
			      	?>
			    </ul>
			</div>
			
			<div class="col-md-8">
				<h3 style="margin-top: 0" id="title" class="page-header">Today Event</h3>
				<div  id="event-konten"></div>
				<ul id="pagination-event" class="pagination pull-right"></ul>
			</div>
        </div>
	</div>
	<!-- End Main Content -->

	<!-- Sidebar -->
	<div class="col-md-4">
	<?php
		 $this->view("page/sidebar.php",$data);
	?>
	 </div>
	 <!-- End Sidebar -->
	 
	   </div>
</section>

<?php $this->view("page/footer.php", $data); ?>
<script type="text/javascript" charset="utf-8">
	var data;
	var max_page = 0;
	$(document).ready(function(){
		var url_ = base_url + 'page/event_json';
		var date_now = $("#date-now").val();
		
		$.ajax({
	        url : url_,
	        type: "POST",
	        dataType : "html",
	        data : $.param({init : '', date : date_now}),
	        success:function(msg) 
	        {
				console.log(msg);
	        	data = JSON.parse(msg);
	        	today();
	        }
	    });
	});
	
</script>
