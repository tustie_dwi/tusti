<?php 
$this->view("page/header.php", $data); 
?>
<div id="content-video">
<?php
	if($kategori=='video' || $kategori=='video-streaming'):
		?>
		 <section class="content content-theater">
            	<div class="container">
            		<h2 class="title-theater margin-top-no"><span class="fa fa-video-camera"></span> Live Streaming</h2>
                	<div id="liveStream" class="video-content splash-video"></div>
			</div>
		</section>
		<?php
	else:
	?>
		 <section class="content content-white">
                <div class="container-fluid container-tube">
                	<div class="row">
                		<div class="col-sm-3 menu-tube-pane">
                			<div class="menu-tube">
                				<div class="menu-tube-header">
                					<button type="button" class="menu-tube-toggle collapsed" data-toggle="collapse" data-target="#menu-tube-collapse">
			                            <span class="fa fa-bars"></span>
			                        </button>
			                        <span class="menu-tube-main-menu">Menu</span>
                				</div>
                				<div class="menu-tube-collapse" id="menu-tube-collapse">
                					<ul class="list-unstyled">
                						<li class="active"><a href="#"><span class="fa fa-home"></span> Home</a></li>
                						<li><a href="#"><span class="fa fa-clock-o"></span> Latest Video</a></li>
                						
                					</ul>
                					<hr class="hr-tube">
                					<h5 class="title-menu-tube">Category</h5>
                					<ul class="list-unstyled">
                						<li><a href="#"><span class="fa fa-play"></span> Video</a></li>
                						<li><a href="#"><span class="fa fa-play"></span> Images</a></li>
                						
                					</ul>
                				</div>
                			</div>
                		</div>
                		<div class="col-sm-9 content-tube-pane">
                			
                				<div class="content-pane-tube">
	                				<h2 class="title-tube margin-top-no">Latest Video</h2>
	                				<div class="row">										
										<?php
										if(isset($post) && ($post)){
											$i=0;
											
											foreach($post as $key):
											$i++;
											
											if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
											else $imgthumb = $this->config->default_thumb_web;
											
											if($i==1):
											?>
											<div class="col-sm-6">
												<div class="tube-head-home">
													<a href="#">
														<figure class="tube-figure">
															<img alt="<?php echo $key->file_title ;?>" src="<?php echo $imgthumb ;?>" class="img-responsive img-responsive-center img-tube">
														</figure>	
													</a>
													<a href="#" class="tube-title-link tube-title-link-default" data-list="<?php echo $key->id; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>"><?php echo $key->file_title ;?></a>
													<div class="tube-metadata">
														<div>
															<span class="view-count"><?php echo $key->content_hit; ?> views</span>
															<span class="time-upload"><?php echo date("M d, Y", strtotime($key->last_update)); ?></span>
														</div>
													</div>
												</div>
											</div>
											<?php endif; ?>
											<div class="col-sm-6">
												<div class="row">
												<?php
												if($i>1):
														
													?>
													<div class="col-md-12 tube-side-list">
														<div class="row">
															<div class="col-xs-4">
																<a href="#">
																	<figure class="tube-figure">
																		<img alt="<?php echo $key->file_title ;?>" src="<?php echo $imgthumb ;?>" class="img-responsive img-responsive-center img-tube img-tube-side">
																	</figure>	
																</a>
															</div>
															<div class="col-xs-8">
																<a href="#" class="tube-title-link tube-title-link-default" data-list="<?php echo $key->id; ?>"><?php echo $key->file_title ;?></a>
																<div class="tube-metadata">
																	<div>
																		<span class="view-count"><?php echo $key->content_hit; ?> views</span>
																		<span class="time-upload"><?php echo date("M d, Y", strtotime($key->last_update)); ?></span>
																	</div>
																</div>
															</div>
														</div>
													</div>									
													<?php
														endif;
													?>
	                						</div>
	                					</div>
	                					<?php
											endforeach;
										}
										?>
	                				</div>
                				</div>
                				
                				
                				
		                			<!--	<div class="content-pane-tube">
			                				
			                				<h4 class="title-tube margin-top-no">Categori 1
		                					<a href="#" class="btn btn-primary btn-more-cat text-small pull-right"><span class="fa fa-chevron-circle-right"></span> More</a>
		                					</h4>
		                					<div class="content-list-category-landscape">
			                					<div class="row">
			                						<div class="col-md-12">
			                							<div class="col-sm-3">
			                								<a href="#">
					                							<figure class="tube-figure">
					                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
					                							</figure>	
				                							</a>
				                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
				                							<div class="tube-metadata">
				                								<div>
				                									<span class="view-count">1000 views</span>
				                									<span class="time-upload">22 Januari 2014</span>
				                								</div>
				                							</div>
			                							</div>
			                						
				                						<div class="col-sm-3">
				                								<a href="#">
						                							<figure class="tube-figure">
						                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
						                							</figure>	
					                							</a>
					                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
					                							<div class="tube-metadata">
					                								<div>
					                									<span class="view-count">1000 views</span>
					                									<span class="time-upload">22 Januari 2014</span>
					                								</div>
					                							</div>
				                							</div>
				                						
				                						<div class="col-sm-3">
				                								<a href="#">
						                							<figure class="tube-figure">
						                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
						                							</figure>	
					                							</a>
					                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
					                							<div class="tube-metadata">
					                								<div>
					                									<span class="view-count">1000 views</span>
					                									<span class="time-upload">22 Januari 2014</span>
					                								</div>
					                							</div>
				                							</div>
				                						
				                						<div class="col-sm-3">
				                								<a href="#">
						                							<figure class="tube-figure">
						                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
						                							</figure>	
					                							</a>
					                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
					                							<div class="tube-metadata">
					                								<div>
					                									<span class="view-count">1000 views</span>
					                									<span class="time-upload">22 Januari 2014</span>
					                								</div>
					                							</div>
				                							</div>
				                						
			                						</div>
			                					</div>
			                				</div>
			                			
			                				<hr class="hr-tube">
			                				<h4 class="title-tube margin-top-no">Categori 2
		                					<a href="#" class="btn btn-primary btn-more-cat text-small pull-right"><span class="fa fa-chevron-circle-right"></span> More</a>
		                					</h4>
		                					<div class="content-list-category-landscape">
			                					<div class="row">
			                						<div class="col-md-12">
			                							<div class="col-sm-3">
			                								<a href="#">
					                							<figure class="tube-figure">
					                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
					                							</figure>	
				                							</a>
				                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
				                							<div class="tube-metadata">
				                								<div>
				                									<span class="view-count">1000 views</span>
				                									<span class="time-upload">22 Januari 2014</span>
				                								</div>
				                							</div>
			                							</div>
			                						
				                						<div class="col-sm-3">
				                								<a href="#">
						                							<figure class="tube-figure">
						                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
						                							</figure>	
					                							</a>
					                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
					                							<div class="tube-metadata">
					                								<div>
					                									<span class="view-count">1000 views</span>
					                									<span class="time-upload">22 Januari 2014</span>
					                								</div>
					                							</div>
				                							</div>
				                						
				                						<div class="col-sm-3">
				                								<a href="#">
						                							<figure class="tube-figure">
						                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
						                							</figure>	
					                							</a>
					                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
					                							<div class="tube-metadata">
					                								<div>
					                									<span class="view-count">1000 views</span>
					                									<span class="time-upload">22 Januari 2014</span>
					                								</div>
					                							</div>
				                							</div>
				                						
				                						<div class="col-sm-3">
				                								<a href="#">
						                							<figure class="tube-figure">
						                								<img src="assets/images/DIES NATALIS.jpg" class="img-responsive img-responsive-center img-tube img-tube-thumb">
						                							</figure>	
					                							</a>
					                							<a href="#" class="tube-title-link tube-title-link-default">DIES NATALIS PTIIK</a>
					                							<div class="tube-metadata">
					                								<div>
					                									<span class="view-count">1000 views</span>
					                									<span class="time-upload">22 Januari 2014</span>
					                								</div>
					                							</div>
				                							</div>
				                						
			                						</div>
			                					</div>
			                				</div>
			                			
			                			</div>-->
	                				
                			</div>
                		</div>
                	</div>
                
            </section>
			
  
		
		<?php endif;?>
</div>
	
<?php $this->view("page/footer.php", $data); ?>