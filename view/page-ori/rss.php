<?php header("Content-Type: application/rss+xml"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
<atom:link href="http://ptiik.ub.ac.id/rss/feed" rel="self" type="application/rss+xml" />
<title>Program Teknologi Informasi dan Ilmu Komputer</title>
<description>Official Site Program Teknologi Informasi dan Ilmu Komputer</description>
<link>http://ptiik.ub.ac.id</link>
<lastBuildDate><?php echo date("D, d M Y H:i:s O"); ?></lastBuildDate>
<copyright>BPTIK UB</copyright>
<language><?php if(isset($lang)) { echo $lang; } else { echo "en-US"; } ?></language>
<ttl>120</ttl>
<?php
if(isset($posts)) {
foreach($posts as $post) { ?>
<item>
<title><?php echo $post->content_title; ?></title>
<description><?php echo $post->content; ?></description>
<link><?php echo $post->content_link; ?></link>
<guid isPermaLink="true"><?php echo $post->content_link; ?></guid>
<pubDate><?php echo $post->content_upload; ?></pubDate>
<content:encoded><![CDATA[<?php echo html_entity_decode($post->content, ENT_QUOTES, 'utf-8'); ?>]]></content:encoded>
<dc:creator><?php echo $post->author; ?></dc:creator>
</item>
<?php }} ?>
</channel>
</rss>