<?php 
$this->view("header-page.php", $data); 
$mpage = new model_page();
?>

<!-- Main Content -->
<section class="col-sm-8">
	<section class="main-content">
		<section class="news-list">
		<?php
		if($post){
			foreach($post as $dt):
			?>
			<h2 class="title-content"><?php echo $dt->content_title ?></h2>
			<hr class="hr-content">
			<article>
				<div class="row">
					<div class="col-xs-2">
						<div class="time-posted-ribbon">
							<div class="time-posted-ribbon-date"><?php echo date("d", strtotime($dt->content_modified)) ?></div>
							<div class="time-posted-ribbon-month"><?php echo date("F", strtotime($dt->content_modified)) ?></div>
							<div class="time-posted-ribbon-year"><?php echo date("Y", strtotime($dt->content_modified)) ?></div>
						</div>
					</div>
					<div class="col-xs-10">
						<!--<figure class="thumbnail-article always-full full-height">
							<img src="https://175.45.187.253/beta/pjj/assets/upload/thumb/images.jpg" class="img img-responsive">
						</figure>-->
						<!--<h3 class="title-article">Lorem Ipsum Omnis voluptas</h3>-->
						<span class="time-posted">By: <?php echo $dt->content_author_id ?></span>
						<?php echo $dt->content_content ?>
						
					</div>
				</div>
			</article>

			<!--Comment-->
			
			<?php
				$data['comment'] = $dt->comment_status;
				$data['id']		 = $dt->id;
				$data['mpage']	 = $mpage;
				
				$this->view("page/comment.php", $data);
			endforeach;
		}
		?>
		
		</section> <!-- end section news -->
	</section>
</section>
	<!-- End Main Content -->

	<!-- Sidebar -->
	 <?php
	$data['mpage'] = $mpage;
	 $this->view("page/sidebar.php",$data);?>
	  <!-- End Sidebar -->
</section>

</section>
<!--End Content-->
<?php 
$data['mpage'] = $mpage;
$this->view("footer-page.php", $data); ?>
    
