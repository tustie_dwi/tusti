
	<aside class="sidebar">
	
		 <div class="box-sidebar">
            <h3 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm no-padding font-orange"><span><?php echo paling_sering?></span></h3>
			<div class="list-detail-content">
			<?php
			if($news):
			foreach($news as $dt):
				
				 if($dt->thumb_img){
					$gambar = $this->location($dt->thumb_img);
				 }else{
					$gambar = $this->asset('images/PTIIK.jpg');
				 }
				 
				 ?>
				 <ul class="media-list">
					<li class="media"><a href="<?php 
					if(isset($url)):
						echo $this->location($url.'/read/news/'.$dt->content_page.'/'.$dt->id);
					else:
						echo $this->location('page/read/news/'.$dt->content_page.'/'.$dt->id);
					endif;
					 ?>"><?php echo $dt->content_title; ?></a></li>
				 </ul>
				
			<?php									
			endforeach;
			endif;
			?>	
			</div>
			
		</div>
		 <div class="box-sidebar">
              <h3 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm no-padding font-orange"><span><?php echo kategori ?></span></h3>
			<div class="list-detail-content">
				 <ul class="media-list media-list-checklist">
					<li class="media"><a href="<?php 
					if(isset($url)):
						echo $this->location($url.'/read/pengumuman');
					else:
						echo $this->location('page/read/pengumuman');
					endif;
					
					 ?>"><?php echo pengumuman ?></a></li>
					<li class="media"><a href="<?php 
					if(isset($url)):
						echo $this->location($url.'/read/news');
					else:
						echo $this->location('page/read/news');
					endif; ?>"><?php echo berita ?></a></li>
					<li class="media"><a href="<?php 
					if(isset($url)):
						echo $this->location($url.'/read/event');
					else:
						echo $this->location('page/read/event');
					endif;
					?>"><?php echo kegiatan ?></a></li>
					<li class="media"><a href="<?php 
					if(isset($url)):
						echo $this->location($url.'/read/beasiswa');
					else:
						echo $this->location('page/read/beasiswa');
					endif;
					?>"><?php echo beasiswa ?></a></li>
				 </ul>				
			</div>
		</div>
		
		<div class="box-sidebar">
			<h3 class="title-content title-underline title-underline-orange margin-top-no margin-bottom-sm no-padding font-orange"><span>Gallery</span></h3>
			<div class="list-detail-content">
				<ul class="list-unstyled list-gallery">
					<?php 
					
					if(isset($gallery)&&($gallery)){
						foreach($gallery as $key):
							if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
								else $imgthumb = $this->config->default_thumb_web;
								
							?>
								<li>
									<a href="<?php echo $this->location('page/read/gallery'); ?>">
										<img class="img-responsive img-responsive-center img-thumbnail" src="<?php echo $imgthumb; ?>">
										<span class="img-overlay"></span>
									</a>
								</li>
							<?php
						endforeach;
					}
					
					?>
					
					
				</ul>			
			</div>
		</div>
                        
	</aside>
	
	<!-- Sidebar -->
                   