<?php $this->view("page/header.php", $data); ?>
<section class="content content-white">
	<div class="container">
		<div class="col-md-12">
			<h2 class="title-underline"><?php echo form_daftar_ulang ?></h2>
		<?php if(!isset($waktu_daftar)){ ?>
			<br>			
			<div class="row">
				<form id="form-daftar-ulang">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">NIM</label><br>
							<input type="text" required="required" class="form-control" name="nim_mhs" id="nim_mhs" placeholder="<?php echo masukkan_nim ?>" value="<?php if(isset($nim)) echo $nim ?>"/>
						</div>
						<div class="form-group">
							<label class="control-label">Password</label><br>
							<input type="password" required="required" class="form-control" name="pass_mhs" id="pass_mhs" placeholder="Password"/>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">&nbsp;</label><br>
							<button class="btn btn-default" type="submit">Submit</button>
						</div>
					</div>
				</form>
			</div>
		<?php if(!isset($auth_msg)){ ?>
			<?php if(isset($msg)){ ?>
				<?php if(isset($mhs_entered) && $mhs_entered->is_valid=='0') { ?> 
					<label class="label label-warning msg"><?php echo $msg; ?></label><br><br>
				<?php } ?>
				
				<div class="row">
					<div class="col-md-6">
					  <div class="well">
						<table class="table" style="font-size: 13px">
							<thead>
								<tr>
									<th style="min-width: 125px">&nbsp;</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo nama; ?></td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->nama;?></td>
								</tr>
								<tr>
									<td><?php echo ttl; ?></td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->tmp_lahir;?>, <?php if(isset($mhs_entered)) echo $mhs_entered->tgl_lahir;?></td>
								</tr>
								<tr>
									<td><?php echo jalur_seleksi; ?></td>
									<td>: <?php if(isset($mhs_entered)) echo ucfirst($mhs_entered->jalur_masuk);?></td>
								</tr>
								<tr>
									<td><?php echo prodi; ?></td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->prodi;?></td>
								</tr>
								<tr>
									<td>Email</td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->email;?></td>
								</tr>
								<tr>
									<td><?php echo alamat_malang; ?></td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->alamat_malang;?></td>
								</tr>
								<tr>
									<td><?php echo telp ?>/ <?php echo hp ?></td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->telp." / ".$mhs_entered->hp;?></td>
								</tr>
								<tr>
									<td><?php echo orangtua?></td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->orang_tua;?></td>
								</tr>
								<tr>
									<td><?php echo email_ortu?></td>
									<td>: <?php if(isset($mhs_entered)&&$mhs_entered->email_ortu!="") echo $mhs_entered->email_ortu; else echo "-";?></td>
								</tr>
								<tr>
									<td><?php echo alamat_ortu?></td>
									<td>: <p><?php if(isset($mhs_entered)) echo $mhs_entered->alamat_ortu;?></p></td>
								</tr>
								<tr>
									<td><?php echo telp_ortu ?></td>
									<td>: <?php if(isset($mhs_entered)) echo $mhs_entered->telp_ortu." / ".$mhs_entered->hp_ortu;?></td>
								</tr>
								<tr>
									<td><?php echo alamat_surat ?></td>
									<td>: <p><?php if(isset($mhs_entered)) echo $mhs_entered->alamat_surat;?></p></td>
								</tr>
								<tr>
									<td><?php echo note ?></td>
									<td>: <?php if(isset($mhs_entered)&&$mhs_entered->catatan!="") echo $mhs_entered->catatan; else echo "-";?></td>
								</tr>
							</tbody>
						</table>
					  </div>
					</div>
				</div>
			<?php } ?>
			<?php if(isset($mhs)){ ?>
			<div class="row">
				<br><br>
				<form method="post" id="form-mhs-daftar-ulang">
				 	<div class="col-md-6">
				 		<div class="block-box well">
			 				<div class="header"><h4><?php echo biodata_pribadi; ?></h4></div>
			 				<div class="content">
								<div class="form-group">
									 <label class="control-label"><?php echo nama; ?></label>
									 <input type="text" required="required" class="form-control" name="nama_mhs" id="nama_mhs" placeholder="<?php echo nama; ?>" value="<?php if(isset($mhs)) echo $mhs->nama;?>"/>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo ttl; ?></label>
										<input type="text" required="required" class="form-control" name="tmpt_lahir_mhs" id="tmpt_lahir_mhs" placeholder="<?php echo tempat_lahir; ?>" value="<?php if(isset($mhs)) echo $mhs->tmp_lahir;?>"/>
									 <label class="control-label">&nbsp;</label>
										<input type="text" required="required" class="form-control form_datetime" name="tgl_lahir_mhs" id="tgl_lahir_mhs" placeholder="<?php echo tgl_lahir; ?>" value="<?php if(isset($mhs)) echo $mhs->tgl_lahir; ?>"/>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo jalur_seleksi ?></label>
									 <select class="form-control e9" name="jalur_seleksi_mhs" id="jalur_seleksi_mhs">
									 	<option value="reguler" <?php if(isset($mhs)&&$mhs->jalur_seleksi=="reguler") echo "selected"; ?> >Reguler</option>
									 	<option value="sap" <?php if(isset($mhs)&&$mhs->jalur_seleksi=="sap") echo "selected"; ?>>SAP</option>
									 </select>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo prodi ?></label>
									 <select class="form-control e9" name="prodi_mhs" id="prodi_mhs" disabled="disabled">
									 	<option value="0">Pilih Prodi</option>
									 	<?php
										if (count($prodi) > 0) {
											foreach ($prodi as $dt) :
												echo "<option value='" . $dt -> id . "' ";
												if(isset($mhs)&&$mhs->prodi_id==$dt -> id){
													echo "selected";
												}
												echo ">" . $dt -> value . "</option>";
											endforeach;
										}
										?>
									 </select>
									 <?php if(isset($mhs)){ ?>
									 	<input type="hidden" name="prodi_mhs" value="<?php if(isset($mhs))echo $mhs->prodi_id; ?>" />
								 	 <?php } ?>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo email ?></label>
									 <input type="email" required="required" class="form-control" name="email_mhs" id="email_mhs" placeholder="<?php echo email ?>" value="<?php if(isset($mhs))echo $mhs->email; ?>"/>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo alamat_malang ?></label>
									 <textarea required="required" class="form-control" name="alamat_mhs" id="alamat_mhs" placeholder="<?php echo alamat_malang ?>"><?php if(isset($mhs))echo $mhs->alamat; ?></textarea>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo telp ?></label>
									 <input type="text" required="required" class="form-control" name="tlp_mhs" id="tlp_mhs" placeholder="<?php echo telp ?>" value="<?php if(isset($mhs))echo $mhs->telp; ?>"/>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo hp ?></label>
									 <input type="text" required="required" class="form-control" name="hp_mhs" id="hp_mhs" placeholder="<?php echo hp ?>" value="<?php if(isset($mhs))echo $mhs->hp; ?>"/>
								</div>
							</div>
		 				</div>
		 			</div>
		 			<div class="col-md-6">
				 		<div class="block-box well">
			 				<div class="header"><h4><?php echo data_orang_tua ?></h4></div>
			 				<div class="content">
								<div class="form-group">
									 <label class="control-label"><?php echo orangtua ?></label>
									 <input type="text" required="required" class="form-control" name="nama_ortu" id="nama_ortu" placeholder="<?php echo orangtua ?>" value="<?php if(isset($mhs)) echo $mhs->nama_ortu; ?>"/>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo email_ortu ?></label>
									 <input type="email" class="form-control" name="email_ortu" id="email_ortu" placeholder="<?php echo email_ortu ?>" value="<?php if(isset($mhs)) echo $mhs->email_ortu; ?>"/>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo alamat_ortu ?></label>
									 <textarea required="required" class="form-control" name="alamat_ortu" id="alamat_ortu" onchange="alamat()" placeholder="<?php echo alamat_ortu ?>"><?php if(isset($mhs)) echo $mhs->alamat_ortu; ?></textarea>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo telp_ortu ?></label>
									 <input type="text" required="required" class="form-control" name="tlp_ortu" id="tlp_ortu" placeholder="<?php echo telp_ortu ?>" value="<?php if(isset($mhs)) echo $mhs->telp_ortu; ?>"/>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo hp_ortu ?></label>
									 <input type="text" required="required" class="form-control" name="hp_ortu" id="hp_ortu" placeholder="<?php echo hp_ortu ?>" value="<?php if(isset($mhs)) echo $mhs->hp_ortu; ?>"/>
								</div>
							</div>
		 				</div>
		 				<div class="block-box well">
						  <div class="header"><h4><?php echo surat_menyurat ?></h4></div>
							<div class="content">
								<div class="form-group">
									 <label class="control-label" style="color: red"><small><?php echo warning_note_surat ?></small></label>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo alamat_surat ?></label>
										<textarea required="required" class="form-control" name="alamat_surat" id="alamat_surat" placeholder="<?php echo alamat_surat ?>"><?php if(isset($mhs)) echo $mhs->alamat_surat; ?></textarea>
								</div>
								<div class="form-group">
									 <label class="control-label"><?php echo note ?></label>
										<textarea class="form-control" name="catatan_surat" id="catatan_surat" placeholder="<?php echo note ?>"><?php if(isset($mhs)) echo $mhs->catatan; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								 <input type="hidden" name="hidId_mhs" value="<?php echo $nim; ?>"/>
								 <input type="hidden" name="daftar_id" value=""/>
								 <button type="submit" class="btn btn-primary">Submit</button>
								 <button type="button" class="btn btn-danger" onclick="location.href='<?php echo $this->location('info/akademik/daftar'); ?>'">Cancel</button>
							</div>
						</div>
		 			</div>
		 		</form>
		 	</div>
			<?php } ?>
		<?php } else {
				echo '<label class="label label-danger msg">'.$auth_msg.'</label>';
			  } 
		} else{?>
			<div class="alert alert-warning"><?php echo $waktu_daftar; ?></div>
		<?php
		}?>
		</div>
	</div>
</section>
<?php $this->view("page/footer.php", $data); ?>