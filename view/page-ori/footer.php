 </section>
<footer id="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
            <section class="footer-top">
                <div class="container">
                    <div class="col-md-8">
                        <h4 class="title-footer title-underline title-underline-black font-orange"><span><?php echo ptiik_link ?></span></h4>
                        <div class="row">
							<?php
							if($footer):
								foreach($footer as $dt):
									if(isset($unit_list)) $unit=0;
									else $unit = $unit;
									
									$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
									?>
									 <div class="col-md-3 col-sm-6">
									 <h4><?php echo $dt->content_title?></h4>
										<?php
										if($child):
											?>									
											 <ul class="list-unstyled list-footer">										
												 <?php 
												 foreach ($child as $key):
													//read_sub_footer($unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location() );?>
													 <li><a href="<?php echo $key->content_data?>"><?php echo $key->content_title?></a></li>
													<?php
												 endforeach; ?>
											 </ul>
													
											<?php
										else:
											echo "<li><a href=".$dt->content_data.">".$dt->content_title."</a></li>";
										endif;
									?>
									</div>
									<?php
								endforeach;
							endif;
							?>						
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 class="title-footer title-underline title-underline-black font-orange"><span><?php echo ptiik_tweet ?></span></h4>
                        <ul class="list-unstyled list-twitter">
							<?php
							$tweet = $mpage->read_tweet(3);
							if($tweet):
								foreach($tweet as $dt):
								?>
								 <li>
										<span class="fa fa-twitter"></span>
										<div class="twitter-content">
											<a href="//twitter.com/PTIIK_UB" target="_blank">@PTIIKUB</a> <?php echo $dt->keterangan ?>
										</div>
										<time><?php //echo date("M d, Y h:i", strtotime($dt->created_at)); ?></time>
									</li>
								<?php
								endforeach;
							endif;
							?>                            
                        </ul>
                    </div>
					<div class="col-md-12">
						<small>Follow us on</small>						
							<a href="https://www.facebook.com/PTIIKUB" class="facebook" target="_blank"><span class="fa fa-facebook"></span></a>
							<a href="https://twitter.com/PTIIK_UB" class="twitter" target="_blank"><span class="fa fa-twitter"></span></a>
							<a href="<?php echo $this->location('rss/feed'); ?>" class="feed" target="_blank"><span class="fa fa-rss"></span></a>					
					</div>
                </div>
            </section>
            <section class="footer-bottom">
                <div class="container">
                    <div class="col-sm-4"><img src="<?php echo $this->asset("ptiik/images/ptiik-black.png"); //$mimg->base64_image($this->asset("ptiik/images/ptiik-black.png")); ?>" class="img-responsive img-responsive-center"></div>
                    <div class="col-sm-8 text-right">
                         <?php echo ptiik_alamat ?>
                         <div class="sr-only">Copyright &copy; <span itemprop="copyrightYear">2014</span> <span itemprop="copyrightHolder">BPTIK</span></div>
                    </div>
                </div>
            </section>
        </footer>
        <?php if(isset($unit_list)): ?>
        <div class="tools tools-color"> 
            <a href="#" class="show-tools fa fa-cogs"></a> 
            <div class="inner-tools"> 
                <div class="chang-style"> 
                    <h5>Pick your favourite color</h5> 
                    <ul class="colors"> 
                        <li><a href="#" class="blue<?php if((isset($_COOKIE['stylecolor']) && $_COOKIE['stylecolor']=="blue")){echo " selected";}?>" data-stylecolor="blue"></a></li> 
                        <li><a href="#" class="tosca<?php if((isset($_COOKIE['stylecolor']) && $_COOKIE['stylecolor']=="tosca")){echo " selected";}?>" data-stylecolor="tosca"></a></li> 
                        <li><a href="#" class="green<?php if((isset($_COOKIE['stylecolor']) && $_COOKIE['stylecolor']=="green")){echo " selected";}?>" data-stylecolor="green"></a></li> 
                    	<li><a href="#" class="red<?php if((isset($_COOKIE['stylecolor']) && $_COOKIE['stylecolor']=="red")){echo " selected";}?>" data-stylecolor="red"></a></li> 
                    	<li><a href="#" class="orange<?php if((isset($_COOKIE['stylecolor']) && $_COOKIE['stylecolor']=="orange")||(!isset($_COOKIE['stylecolor']))){echo " selected";}?>" data-stylecolor="orange"></a></li> 
                        <li><a href="#" class="yellow<?php if((isset($_COOKIE['stylecolor']) && $_COOKIE['stylecolor']=="yellow")){echo " selected";}?>" data-stylecolor="yellow"></a></li> 
                    </ul> 
                    <div>
                        <a href="#" class="clear-color">set to default</a>
                    </div>
                    <div class="clearfix"></div>
                </div> 
            </div> 
        </div>
		<?php endif; ?>
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo $this->asset("ptiik/js/jquery-1.11.1.min.js"); ?>"></script>		
		  <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $this->asset("ptiik/js/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo $this->asset("ptiik/js/owl.carousel.min.js"); ?>"></script>

    <script src="<?php echo $this->asset("ptiik/js/jquery.fancybox.pack.js"); ?>"></script>
	  <script type="text/javascript" src="<?php echo $this->asset("ptiik/js/helpers/jquery.fancybox-media.js?v=1.0.6"); ?>"></script>
	  
    <script src="<?php echo $this->asset("ptiik/js/jquery.mixitup.min.js"); ?>"></script>

	<script src="<?php echo $this->asset("js/jquery/jquery.cookie.js"); ?>"></script>
	<script src="<?php echo $this->asset("js/search.js"); ?>"></script>

	
	<?php $scripts = $this->get_scripts(); 
	foreach( $scripts as $s) : ?><script src="<?php echo $this->asset($s); ?>"></script>
	<?php endforeach; ?>
	<?php if( isset($mscripts) and is_array($mscripts)) { 
	foreach( $mscripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
	<?php endforeach; } ?>
       
	<script>
    /*   document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
           //alert('No F-12');
            return false;
        }
    }
    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
    document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
	var message="";
	function clickIE() {if (document.all) {(message);return false;}} function clickNS(e) {if (document.layers||(document.getElementById&&!document.all)) { if (e.which==2||e.which==3) {(message);return false;}}} if (document.layers) {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;} else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;} document.oncontextmenu=new Function("return false") 
	*/</script>
     <script>
		function search(e) {
			var nilai = $("#searchTxt").val();	
		    if (e.keyCode == 13 && nilai != '') {
		    	console.log(nilai);
		    	search_link(nilai);
		        return false;
		    }
		}
		
		function search_link(){
			document.getElementById("main-search-form").submit();
		}
		
		$(document).ready(function() {
			$(".with-tooltip").tooltip({trigger: "hover focus", container: "body"});
				if ($(".menu-navigation-slider").length) {
					var $menunav = $('.menu-navigation-slider').owlCarousel({
							items: 3,
							autoPlay: false,
							pagination: false,
							theme: "slider-theme"
						});
					$(".menu-navigation-next").click(function (e) {
						e.preventDefault();
						$menunav.trigger('owl.next');
					});
				}

				if ($("#slider").length) {
					var $owl = $("#slider")
					$owl.owlCarousel({
						// navigation : true,
						singleItem: true,
						transitionStyle: "backSlide",
						autoPlay: true,
						stopOnHover: true,
						pagination: false,
						autoHeight: true,
						theme: "slider-top-theme"
					});
					$(".slide-next").click(function (e) {
						e.preventDefault();
						$owl.trigger('owl.next');
					});
					$(".slide-prev").click(function (e) {
						e.preventDefault();
						$owl.trigger('owl.prev');
					});
				}
				$('.search-button').click(function (e) {
					e.preventDefault();
					$('.nav-form-nav').addClass('focus').delay(100);
					$("input[name=searchTxt]").focus();
				});

				$("input[name=searchTxt]").focus(function () {
					$('.nav-form-nav').addClass('focus');
				});

				$("input[name=searchTxt]").blur(function () {
					$('.nav-form-nav').removeClass('focus');
				});
				$('.nav-main').affix({
					offset: {
						top: $(".nav-main").position().top
					}
				});
				if ($("#insideptiik").length) {
					var owl = $("#insideptiik");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						theme: "slider-theme"
					});
				}
				if ($("#insidelabslide").length) {
					var owl = $("#insidelabslide");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						theme: "slider-theme"
					});
				}
				if ($(".laboratory-list-slide").length) {
					var owl = $(".laboratory-list-slide");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						autoHeight: true,
						theme: "slider-theme"
					});
				}
				
				 if ($(".mixit-content").length){
					if(typeof $typemix !== 'undefined' && $typemix=="profile"){
						$.each($abjadmix,function(e,f){
							$(".mixit-content").append('<div class="'+classAbjadText+' '+f+'"><h4>'+f+'</h4></div>');
						});
						$.each($abjadmix,function(e,f){
							  		$(".isi-kategori."+f).insertAfter(".abjad-kategori."+f);
							  });
					}
				}
				
				 if ($(".mixit-content").length) {
					var mix = $(".mixit-content");

					mix.mixItUp({
						
    					multiFilter: true,
    					controls: {
							live: true
						},
						animation: {
							effects: 'fade stagger(10ms) scale',
							staggerSequence: function(i){
								return i % 4;
							}
						},
						callbacks: {
						onMixEnd: function(state){
							state.$show.addClass("activeshow");
							state.$hide.removeClass("activeshow");
							if ( typeof $abjadmix !== 'undefined' && typeof $typemix !== 'undefined' && $typemix=="profile") {
							  $.each($abjadmix,function(e,f){
							  	count = $(".activeshow.isi-kategori."+f).length;
							  	if(count<1){
							  		$(".abjad-kategori."+f).slideUp(500);
							  	}
							  });
							}
						}	
						}
					});
				}
				
				if ($(".fancybox").length) {
					$(".fancybox")
					.attr('rel', 'gallery')
					.fancybox({
						helpers : 	{
										media : {},
										 title: {
											type: 'inside',
											position: 'bottom'
										}
									},
						beforeShow: function () {
							/* Disable right click */
							$.fancybox.wrap.bind("contextmenu", function (e) {
									return false; 
							});
						}
					});
				}
				
				$('.tools-color .show-tools').click(function (e) {
					e.preventDefault();
					if ($('.tools-color').hasClass('active')) {
						$('.tools-color').css('right', '').removeClass('active');
					} else {
						$('.tools-color').css('right', '0').addClass('active');
					}
				});
				$('.tools .colors a').click(function (e) {
					e.preventDefault();
					var data = $(this).data('stylecolor');
					var name = "stylecolor";
					  $.cookie(name, data, { expires: 365 ,path:"/"});
					  window.location.href = "";
				});
				$('.tools .clear-color').click(function (e) {
					e.preventDefault();
						var name = "stylecolor";
					  $.removeCookie(name, { path: '/' });
					  window.location.href = "";
				});
			});

							
	 $(".lang-switch").click(function(e){
			e.preventDefault();
	        var lang = $(this).data("lang");
	        var name = "lang-switch";
	          $.cookie(name, lang, { expires: 365 ,path:"/"});
	          window.location.href = base_url;
      });
	  
	  $(document).ready(function(){
	  	<?php if(isset($url)): ?> var url_ = base_url + 'unit/search_json/<?php if(isset($search)) echo $search ?>/<?php echo $unit ?><?php if(isset($url)) echo "/".$url ?>';
		<?php else: ?> var url_ = base_url + 'page/search_json/<?php if(isset($search)) echo $search ?>/<?php echo $unit ?><?php if(isset($url)) echo "/".$url ?>';
		<?php endif; ?>
		
		//alert(url_);
		$.ajax({
	        url : url_,
	        type: "POST",
	        dataType : "json",
	        success:function(msg) 
	        {
	        	konten = msg;
	        	get_konten('');
	        	console.log(konten);
	        }
	    });
	});
	
		$(".tube-title-link").click(function(){
			var id = $(this).data("list");
			var lang = $(this).data("lang");
			var unit = $(this).data("unit");
			//	alert("aa"+ val);
			$.ajax({
					type : "POST",
					  dataType: "HTML",
					  url: base_url + "/page/read_gallery",
					data : $.param({
						id : id,lang:lang, unit:unit
					}),
					success : function(data) {
						$('#content-video').html(data);
					}
			}); 
		});
        </script>
		<?php if(isset($kategori) && ($kategori=='video' || $kategori=='video-streaming')){ ?>
		<script src="<?php echo $this->asset("ptiik/jwplayer/jwplayer.js"); ?>"></script>
        <script>
			
        	if($("#playStream").length){
        		$("#playStream").flowplayer({ 
        			swf: "/assets/flowplayer/flowplayer.swf",
        			 	native_fullscreen: true,
				      	splash: true,
				      	ratio: 9/16,
				      	embed: false,
				      	height: 500
        		 });
        	}
        	if($("#liveStream").length){
        		 $("#liveStream").flowplayer({ 
        			 swf: "/assets/flowplayer/flowplayer.rtmp-3.2.0.swf",
				     	live: true, // mandatory with live streams
  						 rtmp: "rtmp://175.45.187.248/live/livestream",
  						 native_fullscreen: true,
				      	splash: true
        		 });
        		/*jwplayer('liveStream').setup({ 
				   // file: "http://175.45.187.253/beta/apps3/assets/video/Raisa%20-%20LDR%20(Official%204K%20MV).mp4",\
				    file: 'rtmp://175.45.187.248/live/livestream', 
				    
				      width: "100%",
				      aspectratio: "16:9",
    					//skin: "http://175.45.187.253/beta/apps3/assets/jwplayer/skins/five.xml",
				  });
        		}*/
        </script>
		<?php } ?>
        <script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
				ga('create', 'UA-42722624-1', 'ub.ac.id');
				ga('send', 'pageview');
        </script>
    </body>
</html>
