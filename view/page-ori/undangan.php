<?php if($undangan){ ?>
	<h3>Undangan</h3>
	<table class='table table-bordered'>
		<tbody>
			<tr>
				<td width="20%"><em>Undangan</em></td>
				<td width="80%">
			
				<ul class="list-inline">
				<?php 
				$nama="";
				if(count($undangan)!=0){
					$i=0;
					foreach($undangan as $dt):
						$i++;
						if($i % 8){
							$str = "&nbsp;";
						}else{
							$str = "</br>";
						}
						
						$nama = $nama . $dt->nama .",";
						echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama."";		
						if($dt->instansi){
							echo "<code>".$dt->instansi."</code>";
						}
						
						echo "</li> ";
					endforeach;
					
				}else{
					echo "-";
				}?></ul></td>
			</tr>							
		</tbody>
	</table>
	<?php
	}else{
		echo "<p><div class='alert alert-warning'>Sorry, no content to show</div></p>";
	}