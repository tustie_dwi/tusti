

<div class="menu-navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 title-menu">
                            <a href="" class="more-service">
                            	<div class="temu"><?php echo temu_layanan ?></div>
                            	<div class="layanan"><?php echo layananmu ?></div>
                            </a>
                        </div>
                        <div class="col-md-10 content-slide">
                            <div class="row">
                                <div class="col-xs-10">
                                    <div class="menu-navigation-slider">
									<?php
									$apps = $mpage->read_content('0',$lang,'apps');
									
									if($apps):
									if(! isset($unit_list)):
										$strcolor="orange";
									else:
										if(isset($_COOKIE['stylecolor'])) $strcolor=$_COOKIE['stylecolor'];
										else $strcolor=$color;
									endif;
									
										foreach($apps as $dt):
											/*if($dt->icon) $img = $this->config->file_url_view."/".$dt->icon;
											else $img = $this->config->default_thumb_web;*/
											
											
											$str=explode('/',$dt->icon);
											$img_name = end($str);
											$img= $this->asset("ptiik/images/".$strcolor."/".$img_name);
											?>
											<div class="slide">
												<a href="<?php echo $this->location($dt->content_data);?>" class="menu-navigation-link with-tooltip" data-toggle="tooltip" data-placement="top" title="<?php if($dt->keterangan) echo $dt->keterangan; ?>">
													<img class="menu-navigation-img img-responsive" src="<?php echo $img; //echo $mimg->base64_image($img);?>" alt="<?php echo $dt->judul ?>">
													<span class="menu-navigation-detail"><?php echo $dt->judul ?></span>
												</a>
											</div>
											<?php
										endforeach;
									endif;
									?>
                                                                   
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <a href="#" class="menu-navigation-next">
                                        <span class="fa fa-chevron-right"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<?php if(! isset($unit_list)): ?>
			  <section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
								<?php
									if($prodi):
										foreach($prodi as $dt):
											if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
											else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));

											if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
											else $imgthumb = $this->config->default_thumb_web;	

											/*switch(strtolower($dt->kode)){
												case "si":
													$strabout = "Program studi sistem informasi pada Program Teknologi Informasi dan Ilmu Komputer (PTIIK) Universitas Brawijaya berfokus pada peningkatan kemampuan manajerial teknologi informasi dan komunikasi serta sumber daya manusia dimana diharapkan akan menciptakan sebuah sistem yang mampu mengakomodir akan kebutuhan informasi yang berkembang pesat.";
													$strabouten  = "Program study of information systems in PTIIK UB focuses on improving managerial capabilities of information technology, communication and human resources which expected to create a system that able to accommodate the needs of the rapidly growing information.  ";
												break;
												case "ilkom":
												$strabout = "Program studi Informatika pada Program Teknologi Informasi dan Ilmu Komputer (PTIIK) Universitas Brawijaya menyajikan pemanfaatan teknologi informasi dalam proses identifikasi masalah, pengolahan data dan informasi, serta pemecahan masalah dan pengambilan keputusan sesuai dengan prinsip keilmuan dan kerekayasaan.";
													$strabouten  = "Informatics study program of PTIIK UB presents usage of information technology in problem identification process, data and information processing also problem-solving and decision-making in accordance with the principles of science and engineering. ";
												
												break;
												case "siskom":
												$strabout = "Program Studi Sistem Komputer pada Program Teknologi Informasi dan Ilmu Komputer (PTIIK) Universitas Brawijaya men-sinergikan kecerdasan manusia dengan kecanggihan teknologi perangkat lunak dan kehandalan perangkat keras untuk mewujudkan sebuah sistem cerdas yang mampu meningkatkan kesejahteraan manusia.";
													$strabouten  = "Studies Program of the Information Technology ang Computer Science Program UB Synergize human intelligence with sophisticated software technology and hardware reability to realize an intelligent system that can improve human welfare. ";
												
												break;
												
											}*/
											
												$strabout = $dt->about;
												$strabouten = $dt->about;
											
										?>										
											 <div class="col-sm-3">
													<a href="<?php echo $this->location('unit/prodi/'.strtolower($dt->kode)); ?>" class="prodi-link text-center">
														<figure>
															<img src="<?php echo $imgthumb; //$mimg->base64_image($imgthumb); ?>" class="img-responsive img-responsive-center margin-top" alt="Image Logo <?php if($dt->unit) echo ucWords($dt->unit);
														else echo ucWords($dt->unit_ori);
													?>">
														</figure>
														<h3 class="margin-top-sm title-prodi"><?php if($dt->unit) echo ucWords($dt->unit);
														else echo ucWords($dt->unit_ori);
													?></h3>
														<div class="prodi-text"><?php if(strtolower($lang)=='in') echo $strabout; 
														else echo $strabouten;
														?></div>
													</a>
												</div>
										<?php
										endforeach;		
									
									endif;
									?>	                              
                            </div>
                        </div>


                    </div>
                </div>
            </section>
			<section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-sm font-orange"><span><?php echo berita ?></span></h2>
                        <div id="insideptiik">
							<?php
									if(isset($news) && ($news) ){
										foreach ($news as $dt):
											$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));									
											?>	
											<div>
												<div class="inside-box">
													<div class="content-inside-box">
														<?php 
														if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
														else $imgthumb = $this->config->default_thumb_web;
														?>
														<a href="<?php echo $this->location('page/read/news/'.$content.'/'.$dt->id); ?>" >
														<img class="img-inside-box img-responsive" src="<?php echo $imgthumb; //$mimg->base64_image($imgthumb); ?>" alt="<?php echo $mpage->content($dt->judul,5);?>"></a>
														<a class="title-inside-box" href="<?php echo $this->location('page/read/news/'.$content.'/'.$dt->id); ?>"><h4><?php echo $mpage->content($dt->judul,5);?></h4></a>
														<div class="paragraph-inside-box"><?php echo $mpage->content($dt->keterangan,20);?></div>
														<a href="<?php echo $this->location('page/read/news/'.$content.'/'.$dt->id); ?>" class="more-inside-box">More</a>
													</div>
												</div>
											</div>
							
										<?php
										endforeach;
									}
									?>	
                        </div>
                        <div class="text-right"><a href="<?php echo $this->location('page/read/news/'); ?>" class="more-link">More <?php echo berita ?></a></div>
                    </div>
                </div>
            </section>
			<?php else:		?>
			 <section class="content content-white about">
                <div class="container">
                    <div class="col-md-12">
						<?php 
						if($about): 
							foreach($about as $dt):
							?>
							<h2 class="title-content text-center font-orange margin-top-sm"><?php echo $dt->judul ?></h2>
							<div class="row">
							<?php echo $mpage->str_content($dt->keterangan,$this->config->file_url_view); ?></div>
							<!--<hr class="hr-orange hr-center margin-top">-->
							<?php 
							endforeach;
						else:
							if(isset($unit_list)):
								?>
								<h2 class="title-content text-center font-orange"><?php if($unit_list->unit) echo $unit_list->unit;
								else echo $unit_list->unit_ori;
								?></h2>
								<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet eros in leo pellentesque vestibulum. Pellentesque sed mi sed libero ultricies tincidunt ac vel lacus. Curabitur a eros finibus, ultricies quam ac, vulputate dolor. Donec augue ligula, scelerisque eu dictum vitae, tincidunt a felis. Nam at mollis leo, vel elementum velit. Donec accumsan ex mi, sit amet consectetur orci rhoncus euismod. In hac habitasse platea dictumst. Donec at ex nisi. Praesent vitae varius sem. Maecenas volutpat pretium dolor, a posuere ex commodo non.</p>
								<?php
							else:
							?>
								<h2 class="title-content text-center font-orange"><?php echo ptiik;
								?></h2>
								<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet eros in leo pellentesque vestibulum. Pellentesque sed mi sed libero ultricies tincidunt ac vel lacus. Curabitur a eros finibus, ultricies quam ac, vulputate dolor. Donec augue ligula, scelerisque eu dictum vitae, tincidunt a felis. Nam at mollis leo, vel elementum velit. Donec accumsan ex mi, sit amet consectetur orci rhoncus euismod. In hac habitasse platea dictumst. Donec at ex nisi. Praesent vitae varius sem. Maecenas volutpat pretium dolor, a posuere ex commodo non.</p>
							<?php
							endif;
						endif; ?>
					</div>
                </div>
            </section>
			<?php
			endif;
			
			
			if(! isset($unit_list)):
			?>
			 <section class="content content-dark">
                <div class="container">
                    <div class="col-md-6">
                        <h2 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm"><span><?php echo pengumuman ?></span></h2>
                        <ul class="list-unstyled list-announcement">
							<?php
							if($pengumuman):
								foreach($pengumuman as $dt):
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	
								?>
								 <li <?php if($dt->is_sticky==1) echo 'class="sticky"' ?>>
										<a href="<?php echo $this->location('page/read/pengumuman/'.$content.'/'.$dt->id); ?>" title="<?php echo $dt->judul ?>">
											<div>
												<span class="fa fa-bookmark"></span>
												<span class="announcement-title">
													<?php echo $dt->judul ?>
												</span>
												<ul class="list-inline">
													<!--<li>Sticky</li>-->
													<li><time datetime="<?php echo date("Y-m-d H:i", strtotime($dt->content_modified));?>"><?php if($lang=='in') echo date("Y-m-d H:i", strtotime($dt->content_modified));
													else echo date("M d, Y H:i", strtotime($dt->content_modified));
													?></time></li>
													<li class="announcement-organization"><?php if($dt->unit_note) echo $dt->unit_note;
													else echo "PTIIK";?></li>
												</ul>
											</div>
										</a>
									</li>
								<?php
								endforeach;
						
							endif;
							?>

                        </ul>
						<div class="text-left"><a href="<?php echo $this->location('page/read/pengumuman/'); ?>" class="more-link">More <?php echo pengumuman ?></a></div>
                    </div>
                    <div class="col-md-6">
                        <h2 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm"><span><?php echo kegiatan ?></span></h2>
                        <ul class="list-unstyled featured-event">
							<?php
							if($event):
								foreach($event as $dt):
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	
								?>
									 <li>
										<a href="<?php echo $this->location('page/read/event/'.$content.'/'.$dt->id); ?>">
											<div class="row">
												<div class="col-xs-2 text-center event-left">
													<?php if($lang=='in'): ?>
													<div><?php 
													$hari = $mpage->get_hari(date("N", strtotime($dt->content_modified)));
													echo $hari; ?></div>
													<div><?php echo date("d M", strtotime($dt->content_modified)); ?></div>
													<?php else: ?>
													<div><?php echo date("D", strtotime($dt->content_modified)); ?></div>
													<div><?php echo date("M d", strtotime($dt->content_modified)); ?></div>
													<?php endif; ?>
												</div>
												<div class="col-xs-10 event-right">
													<div class="title-event">
														<?php if($dt->judul) echo $dt->judul;
														else echo $dt->judul_ori;
														?>
													</div>
													<ul class="list-inline">
														<li><?php echo lokasi ?> : <?php echo $dt->lokasi; ?></li>
														<li><?php echo waktu ?> : <?php echo date("H:i", strtotime($dt->content_modified)); ?></li>
														<!--<li class="event-organization"><?php //if($dt->unit) echo ucWords($dt->unit);
													//	else echo ucWords($dt->unit_ori);
														?></li>-->
													</ul>
												</div>
											</div>
										</a>
									</li>
								<?php
								endforeach;
						
							endif;
							?>								
                          
                        </ul>
						<div class="text-left"><a href="<?php echo $this->location('page/read/event/'); ?>" class="more-link">More <?php echo kegiatan ?></a></div>
                    </div>
                </div>
            </section>
			<?php
			else:
				if($unit_list->kategori=='laboratorium'):
			?>
				<section class="content content-black-patern">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-sm font-orange"><span><?php echo informasi ?></span></h2>

                        <div id="insidelabslide">
							<?php
									if(isset($pengumuman) && ($pengumuman) ){
										foreach ($pengumuman as $dt):
											$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));									
											?>	
											
											
											<div>
												<div class="inside-lab">
													<div class="content-inside-box">
														<?php 
														if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
														else $imgthumb = $this->config->default_thumb_web;
														?>
														<a href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" >
														<img class="img-inside-box img-responsive" src="<?php echo $imgthumb; //$mimg->base64_image($imgthumb); ?>"></a>
														<h4 class="title-inside-box"><?php echo $mpage->content($dt->judul,5);?></h4>
														<div class="paragraph-inside-box"><?php echo $mpage->content($dt->keterangan,20);?></div>
														<a href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" class="more-inside-box">More</a>
													</div>
												</div>
											</div>
							
										<?php
										endforeach;
									}else{
										if(isset($pengumuman_all) && ($pengumuman_all) ){
											foreach ($pengumuman_all as $dt):
											$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));									
											?>	
											
											
											<div>
												<div class="inside-lab">
													<div class="content-inside-box">
														<?php 
														if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
														else $imgthumb = $this->config->default_thumb_web;
														?>
														<a href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" >
														<img class="img-inside-box img-responsive" src="<?php echo $imgthumb; //$mimg->base64_image($imgthumb); ?>"></a>
														<h4 class="title-inside-box"><?php echo $mpage->content($dt->judul,5);?></h4>
														<div class="paragraph-inside-box"><?php echo $mpage->content($dt->keterangan,20);?></div>
														<a href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" class="more-inside-box">More</a>
													</div>
												</div>
											</div>
							
										<?php
										endforeach;
										}
									}
									?>	                      

                        </div>
                        <div class="text-right"><a href="<?php echo $this->location($url.'/read/pengumuman/'); ?>" class="more-link">More <?php echo pengumuman ?></a></div>
                    </div>
                </div>
            </section>
			
			
			<?php
				endif;
			endif;
			?>
			
			<!-- laboartorium list -->
			
			 <section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-sm font-orange"><span><?php echo laboratorium ?></span></h2>
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="laboratory-list-slide  owl-carousel slider-theme">
									<?php
										if($lab):
											foreach($lab as $dt):
												if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
												else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));


												if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
												else $imgthumb = $this->config->default_thumb_web;
												?>												
										
																							
												<div>
													<a href="<?php echo $this->location('unit/lab/'.strtolower($dt->kode)); ?>" class="link-to-laboratory col-sm-3 col-xs-6">
														<div class="text-center">
															<div class="show-content">
																<img src="<?php echo $imgthumb; //$mimg->base64_image($imgthumb); ?>" class="img-responsive img-responsive-center" width="100" alt="<?php if($dt->unit) echo ucWords($dt->unit);
															else echo ucWords($dt->unit_ori);
															?>">
															</div>
															<div class="hide-content">
																<?php if($dt->unit) echo ucWords($dt->unit);
															else echo ucWords($dt->unit_ori);
															?>
															</div>
														</div>
													</a>
												</div>
											<?php
											endforeach;
									
										endif;
										?>						
                                    
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			 			
             <section class="content our-partnership">
                <div class="layer">
                    <div class="container">
                        <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange"><span><?php echo partner_kerjasama ?></span></h2>
                        <div class="row text-center partnership">
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/gdp.png"); //$mimg->base64_image($this->asset("ptiik/images/gdp.png")); ?>" class="img-responsive img-responsive-center" alt="GDP Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/nokia.png"); //$mimg->base64_image($this->asset("ptiik/images/nokia.png")); ?>" class="img-responsive img-responsive-center" alt="Nokia Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ibm.png"); //$mimg->base64_image($this->asset("ptiik/images/ibm.png")); ?>" class="img-responsive img-responsive-center" alt="IBM Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/cisco.png"); //$mimg->base64_image($this->asset("ptiik/images/cisco.png")); ?>" class="img-responsive img-responsive-center" alt="Cisco Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/oracle.png"); //$mimg->base64_image($this->asset("ptiik/images/oracle.png")); ?>" class="img-responsive img-responsive-center" alt="Oracle Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ni.png"); //$mimg->base64_image($this->asset("ptiik/images/ni.png")); ?>" class="img-responsive img-responsive-center" alt="NI Logo"></div>
                        </div>
                        </div>
                    </div>
                </div>
            </section>

