<?php 
if(isset($user)){
	$role = $user->role;
	
	
	if($foto){
		$strimg = $this->config->file_url_view."/".$foto;
	}else{
		$strimg = $this->config->default_thumb_web;
	}
}
$mhome = new model_home();
$quote = $mhome->get_quote();
		
?>

<aside id="navigation-side" class="black hide-from-print">
	<div class="navbar-header hide-from-print">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
	  <a class="navbar-brand" href="#"><img src="<?php echo $this->asset("inside/images/ptiikappslogogray.png") ?>" class="img img-responsive" alt="PTIIK APPS version 2.0"></a>
	 </div>
	 <div class="navbar-collapse black hide-from-print">
		<div class="user-panel">
			<div class="row">
				<div class="profile-image col-sm-5 col-xs-3">
					<a href="<?php echo $this->location('user/profile'); ?>"><img src="<?php echo $strimg ?>" class="img img-responsive img-responsive-center"></a>
				</div>
				<div class="col-sm-7 col-xs-9">
					<?php  $user	= $this->authenticatedUser->username;
						   $role	= $this->authenticatedUser->role;							?>
					<h5 class="user-name"><?php echo $user;?></h5>
					<div class="user-level"><?php echo $role;?></div>
					<div class="user-action">
						<a href="<?php echo $this->location('auth/logoff/'.$user); ?>"><span class="fa fa-power-off"></span></a>
						<a href="<?php echo $this->location('user/profile'); ?>"><span class="fa fa-user"></span></a>
						<?php if($role=='administrator'){ ?>
					
						<a class="dropdown-toggle" data-toggle="dropdown" id="dLabel" aria-haspopup="true" aria-expanded="false">
						  <i class="fa fa-cog"></i> 
						</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						  <li><a href="<?php echo $this->location('user'); ?>"><i class="icon-user"></i> User Management</a></li>
						  <li><a href="<?php echo $this->location('module'); ?>"><i class="icon-th"></i> Modules</a></li>
						  <li><a href="<?php echo $this->location('menu'); ?>"><i class="icon-th"></i> Menu</a></li>
						<li><a href="<?php echo $this->location('hakakses'); ?>"><i class="icon-th"></i> Privileges</a></li>
						</ul>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		
			<ul class="nav navbar-nav navbar-side hide-from-print">
			   <li style="display:none" class="li-before"><a href="<?php echo $this->location('apps'); ?>" class="nav-before" data-value="null">Back</a></li>
				<li data-navmenugroup="1" class="dashboard"><a href="<?php echo $this->location('apps'); ?>"><span class="fa fa-home"></span><span class="text-side"><span class="text-title">Dashboard</span><span class="text-description">&nbsp;</span></span></a></li>
			
			
				<?php
				
				if(isset($menus)){
				$datamenu	= $menus;	
				
				if($datamenu){
				$mods = array();
				$tree = array();
				$sub  = array();
				
				foreach($datamenu as $dt):
					if($dt->parent_id==0){
					foreach($datamenu as $data):
						if($data->parent_id == $dt->id){
							$tree[] = $data->parent_id;
							$sub[] = $data->id;					
						}				
					endforeach;
					}
				endforeach;
				
				$countNow=1;
				$count=1;
				
				foreach($datamenu as $dt):
					if(in_array($dt->id, $tree)){
						
						?>
						<li class="dropdown" data-navmenugroup="<?php echo $count;?>">
						  <a class="dropdown-toggle" data-toggle="dropdown" role="button" href="#"> <span class="<?php echo str_replace("icon", "fa fa", $dt->icon);?>"></span>
						 <?php echo $dt->judul;?>
						  </a> 
							  <ul class="dropdown-menu" id="<?php echo $dt->id;?>" role="menu">						
								<?php
								foreach($datamenu as $data):
								if(in_array($data->id, $sub) && $data->parent_id==$dt->id){
									?>
									<li>
									<a href="<?php echo $this->location('module/'.$data->module.'/'.$data->link); ?>">
									
									<!--coba -->
									<span class="<?php 	echo str_replace("icon", "fa fa", $data->icon);?>"></span> 
									
									<?php echo $data->judul;?></a></li>
									<?php
								}
								endforeach;
								?>
							  </ul>
						  </li>
						<?php
						$countNow++;
						if($countNow%6==0){
								$count++;
							}
					}else{
						if(! in_array($dt->id, $sub)){
						?>
						<li data-navmenugroup="<?php echo $count;?>"><a href="<?php echo $this->location('module/'.$dt->module.'/'.$dt->link); ?>"><span class="<?php echo str_replace("icon", "fa fa", $dt->icon);//echo $dt->icon;?>"></span> 
						<?php echo $dt->judul;?></a></li>
						<?php
							$countNow++;
							if($countNow%6==0){
								$count++;
							}
						}
					}
					
				endforeach;
			}
			}
			?>
			  
			   <li style="<?php
			  if($count<=1) echo "display:none";
			  ?>" class="li-after"><a href="<?php echo $this->location('apps'); ?>" class="nav-more" data-value="<?php
			  if($count>1)echo 2;else echo "null";
			  ?>" data-valuemax="<?php echo $count; ?>">More</a></li>
			  
			</ul>

		
	 </div>
</aside>
<section id="wrapper">
	<div class="main-container">
		<header id="top-header" class="hide-from-print <?php
		if(isset($ishome)&&$ishome==true)echo "dashboard";
		else echo "not-dahsboard";
		 ?>">
			<div class="container-fluid hide-from-print">
				<div class="col-sm-12">
					<div class="row">
						
						<div class="col-md-2 col-md-push-10">
							<div class="weather">Get Weather Data</div>
						</div>
						<div class="col-md-10 col-md-pull-2">
							<div class="top-quotes">
								<?php 
								if($quote){
								?>
									<span class="quotes"><?php echo $quote->keterangan; ?> <span class="owner"> - <?php echo $quote->by; ?></span></span>
								<?php
								}else{
								?>
									<span class="quotes">Stay hungry, stay foolish - <span class="owner"> - Steve Jobs</span></span>
								<?php } ?>	    						
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
<div class="container-fluid container-content">
	<div class="col-md-12">
