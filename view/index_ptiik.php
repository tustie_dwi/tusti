<!DOCTYPE html>
<!--
Copyright 2014 rizky.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<html>
    <head>
        <title>PTIIK New</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--Style-->
        <link rel="stylesheet" href="assets/ptiik/css/base.min.css">
        <link rel="stylesheet" href="assets/ptiik/layerslider/css/layerslider.css">
        <link rel="stylesheet" href="assets/ptiik/css/insideptiik.slider.min.css">
    </head>
    <body>
        <header id="header">
            <section class="signin-section">
                <div class="signin-area">
                    <a href="#" class="signin-link">APPS LOGIN <span class="fa fa-sign-in"></span></a>
                </div>
            </section>
            <section class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="image-link">
                                <img class="img-logo-header img-responsive" src="assets/ptiik/images/ptiik.png">
                            </a>
                        </div>
                        <div class="col-md-6">
                            <div class="navigation-top-header">
                                <ul class="navigation-menu">
                                    <li><a href="#">UB Official</a></li>
                                    <li><a href="#">BITS</a></li>
                                    <li><a href="#">Webmail</a></li>
                                    <li><a href="#">UB News</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="nav-main">
                <div class="container">
                    <div class="nav-header">
                        <button type="button" class="nav-toggle collapsed" data-toggle="collapse" data-target="#nav-main-collapse">
                            <span class="fa fa-bars"></span>
                        </button>
                        <span class="nav-main-menu">Menu</span>
                    </div>
                    <div class="nav-main-collapse" id="nav-main-collapse">
                        <ul class="nav-main-nav nav-left">
                            <li class="active"><a href="#"><span class="fa fa-home"></span> <span class="visible-sm-inline visible-xs-inline">Home</span></a></li>
							<?php 
							/*foreach($header_menu as $dt):
								?>
								 <li><?php echo $dt->content_title; ?></li>
								<?php
							endforeach;*/
							?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About</a>
                                <div class="dropdown-menu with-background background-bottom-right" style="background-image: url('assets/ptiik/images/back-submenu1.png')">

                                    <div class="col-md-5">
                                        <ul class="menu-sub" role="menu">
                                            <li class="nav-head"><a href="#">Profile</a></li>
                                            <li class="nav-list"><a href="#">Sejarah</a></li>
                                            <li class="nav-list"><a href="#">Visi, Misi & Tujuan</a></li>
                                            <li class="nav-list"><a href="#">Struktur Organisasi</a></li>
                                            <li class="nav-list"><a href="#">Pimpinan Program</a></li>
                                            <li class="nav-list"><a href="#">Renstra dan Proker</a></li>
                                            <li class="nav-list"><a href="#">Prestasi</a></li>
                                            <li class="nav-list"><a href="#">Dokumen Resmi</a></li>
                                            <li class="nav-head"><a href="#">Sumber Daya Manusia</a></li>
                                            <li class="nav-list"><a href="#">Dosen</a></li>
                                            <li class="nav-list"><a href="#">Karyawan</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5">

                                        <ul class="menu-sub" role="menu">
                                            <li class="nav-head"><a href="#">Jurusan / Prodi</a></li>
                                            <li class="nav-list"><a href="#">Informatika</a></li>
                                            <li class="nav-list"><a href="#">Sistem Informasi</a></li>
                                            <li class="nav-list"><a href="#">Sistem Komputer</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </li>
                            <li><a href="#">Pendidikan</a></li>
                            <li><a href="#">Litbang</a></li>
                            <li><a href="#">Kemahasiswaan</a></li>
                            <li><a href="#">Layanan</a></li>
                        </ul>
                        <form onsubmit="doSearch();
                                return false;" class="nav-form-nav nav-right">
                            <input id="searchTxt" name="searchTxt" type="text" class="form-control" placeholder="Type here to search.." value="">
                            <span id="searchBg"></span>
                            <button id="searchIcon" name="searchIcon" type="submit" class="search-button"><span class="fa fa-search"></span></button>
                        </form>
                        <p class="nav-text-nav nav-right"><a href="#" class="font-red">ID</a> | <a href="#" class="font-blue">EN</a></p>
                    </div>
                </div>
            </section>
            <div class="herowide-wrapper with-shadow-bottom">
                <div class="hero-slide">
                    <div id="layerslider-front-inside-ptiik" style="width:100%;height:400px;">
                        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
                            <img src="assets/ptiik/images/slider/fw-1.jpg" class="ls-bg" alt="Slide background"/>
                            <img class="ls-l" style="top:280px;left:50%;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:300;delayin:500;offsetxout:0;offsetyout:-50;durationout:1000;showuntil:220;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;transformoriginout:50% top 0;" src="assets/ptiik/images/slider/s1.png" alt="">
                            <img class="ls-l" style="top:230px;left:50%;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:30;delayin:1720;scalexin:0.9;scaleyin:0.9;offsetxout:0;offsetyout:300;durationout:1000;scalexout:0.5;scaleyout:0.5;transformoriginout:50% bottom 0;" src="assets/ptiik/images/slider/s2.png" alt="">
                            <img class="ls-l" style="top:65%;left:50%;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:250;durationin:950;delayin:500;offsetxout:0;offsetyout:-8;durationout:1000;showuntil:270;easingout:easeInOutQuart;scalexout:1.2;scaleyout:1.2;" src="assets/ptiik/images/slider/s2.jpg" alt="">
                            <img class="ls-l" style="top:195px;left:50%;white-space: nowrap;" data-ls="offsetxin:0;delayin:1720;easingin:easeInOutQuart;scalexin:0.7;scaleyin:0.7;offsetxout:-800;durationout:1000;" src="assets/ptiik/images/slider/s1.jpg" alt="">
                            <p class="ls-l" style="top:150px;left:116px;font-weight: 300;height:40px;padding-right:10px;padding-left:10px;font-size:30px;line-height:37px;color:#ffffff;background:#82d10c;border-radius:3px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;">
                                FRESH FEATURES
                            </p>
                            <p class="ls-l" style="top:190px;left:125px;font-family:'Indie Flower';font-size:31px;color:#6db509;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-400;">
                                for starter
                            </p>
                        </div>
                        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
                            <img src="assets/ptiik/images/slider/fw-1.jpg" class="ls-bg" alt="Slide background"/>
                            <img class="ls-l" style="top:157px;left:284px;white-space: nowrap;" data-ls="offsetxin:300;durationin:2000;offsetxout:-300;parallaxlevel:-1;" src="assets/ptiik/images/slider/l1.png" alt="">
                            <img class="ls-l" style="top:20px;left:50%;white-space: nowrap;" data-ls="offsetxin:600;durationin:2000;offsetxout:-600;parallaxlevel:1;" src="assets/ptiik/images/slider/l2.png" alt="">
                            <img class="ls-l" style="top:37px;left:564px;white-space: nowrap;" data-ls="offsetxin:900;durationin:2000;offsetxout:-900;parallaxlevel:4;" src="assets/ptiik/images/slider/l3.png" alt="">
                            <p class="ls-l" style="top:170px;left:174px;font-weight: 300;height:40px;padding-right:10px;padding-left:10px;font-size:30px;line-height:37px;color:#ffffff;background:#f04705;border-radius:3px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;parallaxlevel:10;">
                                SPICY PARALLAX EFFECT
                            </p>
                            <p class="ls-l" style="top:210px;left:183px;font-family:'Indie Flower';font-size:31px;color:#f04705;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-400;parallaxlevel:8;">
                                for main course
                            </p>
                        </div>
                        <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;">
                            <img src="assets/ptiik/images/slider/fw-1.jpg" class="ls-bg" alt="Slide background"/>
                            <img class="ls-l" style="top:129px;left:487px;white-space: nowrap;" data-ls="offsetxin:400;durationin:2000;offsetxout:400;" src="assets/ptiik/images/slider/d1.png" alt="">
                            <img class="ls-l" style="top:104px;left:70px;white-space: nowrap;" data-ls="offsetxin:-200;durationin:2000;offsetxout:-200;" src="assets/ptiik/images/slider/d2.png" alt="">
                            <p class="ls-l" style="top:320px;left:830px;font-weight: 300;height:40px;padding-right:10px;padding-left:10px;font-size:30px;line-height:37px;color:#ffffff;background:#544f8c;border-radius:3px;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-400;durationout:1000;">
                                SWEET TRANSITIONS
                            </p>
                            <p class="ls-l" style="top:360px;left:836px;font-family:'Indie Flower';font-size:31px;color:#544f8c;white-space: nowrap;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-600;">
                                for dessert
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section id="wrapper">
            <section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center">Program Teknologi Informasi dan Ilmu Komputer</h2>
                        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet eros in leo pellentesque vestibulum. Pellentesque sed mi sed libero ultricies tincidunt ac vel lacus. Curabitur a eros finibus, ultricies quam ac, vulputate dolor. Donec augue ligula, scelerisque eu dictum vitae, tincidunt a felis. Nam at mollis leo, vel elementum velit. Donec accumsan ex mi, sit amet consectetur orci rhoncus euismod. In hac habitasse platea dictumst. Donec at ex nisi. Praesent vitae varius sem. Maecenas volutpat pretium dolor, a posuere ex commodo non.</p>
                        <hr class="hr-orange hr-center margin-top">

                        <nav class="tab-pane-menu margin-top-lg margin-bottom-minus">
                            <ul class="tab-pane-list">
                                <li class="active"><a href="#ptiikatgrances" role="tab" data-toggle="tab">PTIIK at Grances</a></li>
                                <li><a href="#boardofour" role="tab" data-toggle="tab">Board of Our</a></li>
                                <li><a href="#department" role="tab" data-toggle="tab">Department</a></li>
                                <li><a href="#laboratory" role="tab" data-toggle="tab">Laboratory</a></li>
                                <li><a href="#ptiikapps" role="tab" data-toggle="tab">PTIIK APPS</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </section>

            <section class="content content-dark-white with-shadow-bottom">
                <div class="container">
                    <div class="col-md-12">





                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="ptiikatgrances">
                                <h2 class="title-content text-center title-underline title-underline-blue"><span>Our Story</span></h2>
                                <div class="row">
                                    <div class="col-sm-4 text-center">
                                        <img src="assets/ptiik/images/ptiikglances.png" class="img img-responsive margin-bottom img-responsive-center">
                                    </div>
                                    <div class="col-sm-8">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in convallis tortor, in iaculis ante. Pellentesque posuere erat et magna tincidunt, vel dignissim arcu fermentum. Aenean ac finibus neque. Ut dictum aliquam ornare. Sed pellentesque lobortis purus vitae sagittis. Sed eget urna id arcu dapibus viverra. Donec ultrices finibus velit eu imperdiet. Aliquam scelerisque diam sed quam porta bibendum. Sed varius magna id turpis auctor rutrum. Quisque convallis lacus ac nulla faucibus, ac bibendum turpis consectetur. Praesent viverra venenatis rhoncus. Phasellus mattis nulla eu neque tempus tristique. Ut elementum dolor turpis, et mollis quam tempus id.
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="boardofour">
                                <h2 class="title-content text-center title-underline title-underline-blue"><span>Board of Our</span></h2>
                                <div class="row">
                                    <div class="col-sm-4 text-center">
                                        <img src="assets/ptiik/images/boardofour.png" class="img img-responsive margin-bottom img-responsive-center">
                                    </div>
                                    <div class="col-sm-8">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in convallis tortor, in iaculis ante. Pellentesque posuere erat et magna tincidunt, vel dignissim arcu fermentum. Aenean ac finibus neque. Ut dictum aliquam ornare. Sed pellentesque lobortis purus vitae sagittis. Sed eget urna id arcu dapibus viverra. Donec ultrices finibus velit eu imperdiet. Aliquam scelerisque diam sed quam porta bibendum. Sed varius magna id turpis auctor rutrum. Quisque convallis lacus ac nulla faucibus, ac bibendum turpis consectetur. Praesent viverra venenatis rhoncus. Phasellus mattis nulla eu neque tempus tristique. Ut elementum dolor turpis, et mollis quam tempus id.
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="department">
                                <h2 class="title-content text-center title-underline title-underline-blue"><span>Department</span></h2>
                                <div class="row">
                                    <div class="col-sm-4 text-center">
                                        <a href="" title="Informatika" class="link-department">
                                            <img src="assets/ptiik/images/logo-if.png" class="img img-responsive margin-bottom img-responsive-center" alt="Informatika">
                                        </a>
                                    </div>
                                    <div class="col-sm-4 text-center">
                                        <a href="" title="Sistem Informasi" class="link-department">
                                            <img src="assets/ptiik/images/logo-si.png" class="img img-responsive margin-bottom img-responsive-center" alt="Informatika">
                                        </a>
                                    </div>
                                    <div class="col-sm-4 text-center">
                                        <a href="" title="Sistem Komputer" class="link-department">
                                            <img src="assets/ptiik/images/logo-siskom.png" class="img img-responsive margin-bottom img-responsive-center" alt="Informatika">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="laboratory">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2 class="title-content title-underline title-underline-blue"><span>Laboratory</span></h2>
                                        <ul class="list-unstyled margin-bottom">
                                            <li class=""><a href="/beta.ptiik/laboratorium/labjarkom" target="_blank">Laboratorium Jaringan Komputer</a></li>
                                            <li class=""><a href="/beta.ptiik/laboratorium/labrpl" target="_blank">Laboratorium Rekayasa Perangkat Lunak</a></li>
                                            <li class=""><a href="/beta.ptiik/laboratorium/labgame" target="_blank">Laboratorium Game (Research)</a></li>
                                            <li class=""><a href="/beta.ptiik/laboratorium/labsi" target="_blank">Laboratorium Sistem Informasi</a></li>
                                            <li class=""><a href="/beta.ptiik/laboratorium/labpapb" target="_blank">Laboratorium Pemrograman Aplikasi Perangkat Bergerak</a></li>
                                            <li class=""><a href="/beta.ptiik/laboratorium/labkomdas" target="_blank">Laboratorium Komputer Dasar</a></li>
                                            <li class=""><a href="/beta.ptiik/laboratorium/labkcv" target="_blank">Laboratorium Komputasi Cerdas dan Visualisasi</a></li>
                                            <li class=""><a href="/beta.ptiik/laboratorium/labsiskombot" target="_blank">Laboratorium Sistem Komputer dan Robotika</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane fade" id="ptiikapps">
                                <h2 class="title-content text-center title-underline title-underline-blue"><span>Our Services</span></h2>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/hadir">
                                            <article>

                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/daftar-hadir.png" alt="APPS Daftar Hadir Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Daftar Hadir Civitas</div>
                                                    <p class="content-apps-text">
                                                        Informasi daftar kehadiran civitas PTIIK, yang meliputi dosen dan staff, dilengkapi dengan agenda kegiatan pada masing-masing civitas.
                                                    </p>
                                                </div> 
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/jadwal/bimbingan">
                                            <article>

                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/daftar-hadir.png" alt="APPS Daftar Hadir Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Jadwal Bimbingan</div>
                                                    <p class="content-apps-text">
                                                        Informasi jadwal bimbingan dan konsultasi dosen pembimbing.
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/ruang/tersedia">
                                            <article>

                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/manajemen-ruang.png" alt="APPS Manajemen Ruangan Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Ketersediaan Ruangan</div>
                                                    <p class="content-apps-text">
                                                        Informasi ketersediaan ruangan di PTIIK.
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/ruang/terpakai">
                                            <article>

                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/manajemen-ruang.png" alt="APPS Manajemen Ruangan Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Pemakaian Ruangan</div>
                                                    <p class="content-apps-text">
                                                        Informasi pemakaian ruangan untuk tiiap-tiap kegiatan yang berlangsung di PTIIK. 
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/skripsi">
                                            <article>                	
                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/akademik.png" alt="APPS Pendaftaran Akademik Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Skripsi</div>
                                                    <p class="content-apps-text">
                                                        Informasi daftar skripsi mahasiswa PTIIK beserta <em>progress report</em> dari kegiatan skripsi mahasiswa yang bersangkutan.
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/kknp">
                                            <article>
                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/akademik.png" alt="APPS Pendaftaran Akademik Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">KKNP</div>
                                                    <p class="content-apps-text">
                                                        Informasi daftar KKNP mahasiswa PTIIK beserta informasi dosen pembimmbing.
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/prestasi">
                                            <article>
                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/prestasi.png" alt="APPS Daftar Prestasi Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Daftar Prestasi</div>
                                                    <p class="content-apps-text">
                                                        Informasi prestasi yang sudah dicapai mahasiswa PTIIK pada setiap tahun akademik yang berlangsung.
                                                    </p>
                                                </div>
                                            </article>                    
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/map">
                                            <article>

                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/manajemen-ruang.png" alt="APPS Manajemen Ruangan Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">PTIIK Maps</div>
                                                    <p class="content-apps-text">
                                                        Denah ruangan di PTIIK. 
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>
                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/akademik/daftar">
                                            <article>
                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/akademik.png" alt="APPS Pendaftaran Akademik Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Daftar Ulang</div>
                                                    <p class="content-apps-text">
                                                        Form isian registrasi akademik pada semester aktif.
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/akademik/krs">
                                            <article>
                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/akademik.png" alt="APPS Pendaftaran Akademik Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Daftar Semester Pendek</div>
                                                    <p class="content-apps-text">
                                                        Form isian permohonan program semester pendek.
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/akademik/pilihan/mk">
                                            <article>
                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/akademik.png" alt="APPS Pendaftaran Akademik Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Form Rancangan MK Pilihan</div>
                                                    <p class="content-apps-text">
                                                        Form rancangan MK pilihan mahasiswa, untuk melihat minat terhadap MK pilihan.
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                    <div class="col-md-4 col-sm-6">

                                        <a class="content-apps" href="http://ptiik.ub.ac.id/apps/info/wisuda">
                                            <article>
                                                <div class="col-md-4 col-sm-4 col-xs-2">
                                                    <figure><img class="img-responsive img-rounded" src="http://ptiik.ub.ac.id/apps/assets/images/daftar-hadir.png" alt="APPS Pendaftaran Akademik Icon"></figure>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-10 content-no-center">
                                                    <div class="content-apps-title content-apps-title-md">Daftar Alumni</div>
                                                    <p class="content-apps-text">
                                                        Daftar Alumni PTIIK
                                                    </p>
                                                </div>
                                            </article>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-sm"><span>Inside PTIIK</span></h2>
                    </div>
                    <div id="insideptiik">
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex1.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex2.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex3.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex4.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex5.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex2.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex4.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex1.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex3.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="inside-box">
                                <div class="content-inside-box">
                                    <img class="img-inside-box img-responsive" src="assets/ptiik/images/ex5.png">
                                    <h4 class="title-inside-box">Judul</h4>
                                    <div class="paragraph-inside-box">Lorem Ipsum</div>
                                    <a href="#" class="more-inside-box">More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right"><a href="#" class="more-link">More Inside PTIIK</a></div>
                </div>
            </section>

            <section class="content content-dark">
                <div class="container">
                    <div class="col-md-6">
                        <h2 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm"><span>Announcement</span></h2>
                        <ul class="list-unstyled list-announcement">
                            <li class="sticky">
                                <a href="#" title="#">
                                    <div>
                                        <span class="fa fa-bookmark"></span>
                                        <span class="announcement-title">
                                            Penundaan Jadwal Pelaksanaan Bootcamp Cisco Networking Academy Semester 1
                                        </span>
                                        <ul class="list-inline">
                                            <li>Sticky</li>
                                            <li><time>30 September 2014</time></li>
                                            <li class="announcement-organization">Network Laborartory</li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                            <li class="sticky">
                                <a href="#" title="#">
                                    <div>
                                        <span class="fa fa-bookmark"></span>
                                        <span class="announcement-title">
                                            Penundaan Jadwal Pelaksanaan Bootcamp Cisco Networking Academy Semester 1
                                        </span>
                                        <ul class="list-inline">
                                            <li>Sticky</li>
                                            <li><time>30 September 2014</time></li>
                                            <li class="announcement-organization">Network Laborartory</li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="#">
                                    <div>
                                        <span class="fa fa-bookmark"></span>
                                        <span class="announcement-title">
                                            Penundaan Jadwal Pelaksanaan Bootcamp Cisco Networking Academy Semester 1
                                        </span>
                                        <ul class="list-inline">
                                            <li>Sticky</li>
                                            <li><time>30 September 2014</time></li>
                                            <li class="announcement-organization">Network Laborartory</li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="#">
                                    <div>
                                        <span class="fa fa-bookmark"></span>
                                        <span class="announcement-title">
                                            Penundaan Jadwal Pelaksanaan Bootcamp Cisco Networking Academy Semester 1
                                        </span>
                                        <ul class="list-inline">
                                            <li>Sticky</li>
                                            <li><time>30 September 2014</time></li>
                                            <li class="announcement-organization">Network Laborartory</li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="#">
                                    <div>
                                        <span class="fa fa-bookmark"></span>
                                        <span class="announcement-title">
                                            Penundaan Jadwal Pelaksanaan Bootcamp Cisco Networking Academy Semester 1
                                        </span>
                                        <ul class="list-inline">
                                            <li>Sticky</li>
                                            <li><time>30 September 2014</time></li>
                                            <li class="announcement-organization">Network Laborartory</li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h2 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm"><span>Featured Event</span></h2>
                        <ul class="list-unstyled featured-event">
                            <li>
                                <a href="#">
                                    <div class="row">
                                        <div class="col-xs-2 text-center event-left">
                                            <div>Sat</div>
                                            <div>11 Oct</div>
                                        </div>
                                        <div class="col-xs-10 event-right">
                                            <div class="title-event">
                                                Bootcamp CISCO Networking Academy Semester 1
                                            </div>
                                            <ul class="list-inline">
                                                <li>Location : Network Loratory</li>
                                                <li>Time : 09.00</li>
                                                <li class="event-organization">Network Laborartory</li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="row">
                                        <div class="col-xs-2 text-center event-left">
                                            <div>Sat</div>
                                            <div>11 Oct</div>
                                        </div>
                                        <div class="col-xs-10 event-right">
                                            <div class="title-event">
                                                Bootcamp CISCO Networking Academy Semester 1
                                            </div>
                                            <ul class="list-inline">
                                                <li>Location : Network Loratory</li>
                                                <li>Time : 09.00</li>
                                                <li class="event-organization">Network Laborartory</li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="row">
                                        <div class="col-xs-2 text-center event-left">
                                            <div>Sat</div>
                                            <div>11 Oct</div>
                                        </div>
                                        <div class="col-xs-10 event-right">
                                            <div class="title-event">
                                                Bootcamp CISCO Networking Academy Semester 1
                                            </div>
                                            <ul class="list-inline">
                                                <li>Location : Network Loratory</li>
                                                <li>Time : 09.00</li>
                                                <li class="event-organization">Network Laborartory</li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="row">
                                        <div class="col-xs-2 text-center event-left">
                                            <div>Sat</div>
                                            <div>11 Oct</div>
                                        </div>
                                        <div class="col-xs-10 event-right">
                                            <div class="title-event">
                                                Bootcamp CISCO Networking Academy Semester 1
                                            </div>
                                            <ul class="list-inline">
                                                <li>Location : Network Loratory</li>
                                                <li>Time : 09.00</li>
                                                <li class="event-organization">Network Laborartory</li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="row">
                                        <div class="col-xs-2 text-center event-left">
                                            <div>Sat</div>
                                            <div>11 Oct</div>
                                        </div>
                                        <div class="col-xs-10 event-right">
                                            <div class="title-event">
                                                Bootcamp CISCO Networking Academy Semester 1
                                            </div>
                                            <ul class="list-inline">
                                                <li>Location : Network Loratory</li>
                                                <li>Time : 09.00</li>
                                                <li class="event-organization">Network Laborartory</li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>


            <section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange">Our Partnership</h2>
                        <div class="row text-center partnership">
                            <div class="col-md-2 col-xs-4"><img src="assets/ptiik/images/gdp.png" class="img-responsive img-responsive-center"></div>
                            <div class="col-md-2 col-xs-4"><img src="assets/ptiik/images/nokia.png" class="img-responsive img-responsive-center"></div>
                            <div class="col-md-2 col-xs-4"><img src="assets/ptiik/images/ibm.png" class="img-responsive img-responsive-center"></div>
                            <div class="col-md-2 col-xs-4"><img src="assets/ptiik/images/cisco.png" class="img-responsive img-responsive-center"></div>
                            <div class="col-md-2 col-xs-4"><img src="assets/ptiik/images/oracle.png" class="img-responsive img-responsive-center"></div>
                            <div class="col-md-2 col-xs-4"><img src="assets/ptiik/images/ni.png" class="img-responsive img-responsive-center"></div>
                        </div>
                    </div>
                </div>
            </section>

        </section>

        <footer id="footer"></footer>
        <script src="assets/ptiik/js/jquery-1.11.1.min.js"></script>
        <script src="assets/ptiik/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="assets/ptiik/layerslider/js/greensock.js"></script>
        <script type="text/javascript" src="assets/ptiik/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
        <script type="text/javascript" src="assets/ptiik/layerslider/js/layerslider.transitions.js"></script>
        <script src="assets/ptiik/js/owl.carousel.min.js"></script>
        <script>
                            $(document).ready(function () {
                                if ($('#layerslider-front-inside-ptiik').length) {
                                    $('#layerslider-front-inside-ptiik').layerSlider({
                                        responsive: true,
                                        responsiveUnder: 1280,
                                        layersContainer: 1280,
                                        skin: 'fullwidth',
                                        hoverPrevNext: true,
                                        autoPlayVideos: false,
                                        skinsPath: 'assets/layerslider/skins/'
                                    });
                                }
                                $('.search-button').click(function (e) {
                                    e.preventDefault();
                                    $('.nav-form-nav').addClass('focus').delay(100);
                                    $("input[name=searchTxt]").focus();
                                });

                                $("input[name=searchTxt]").focus(function () {
                                    $('.nav-form-nav').addClass('focus');
                                });

                                $("input[name=searchTxt]").blur(function () {
                                    $('.nav-form-nav').removeClass('focus');
                                });
                                $('.nav-main').affix({
                                    offset: {
                                        top: $("#wrapper").position().top
                                    }
                                });
                                if ($("#insideptiik").length) {
                                    var owl = $("#insideptiik");

                                    owl.owlCarousel({
                                        items: 4,
                                        autoPlay: true,
                                        stopOnHover: true,
                                        theme: "slider-theme"
                                    });
                                }
                            });
        </script>
    </body>
</html>
