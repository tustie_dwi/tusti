<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title><?php echo $this->page_title(); ?></title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo $this->asset("ptiik/css/login.min.css"); ?>" rel="stylesheet">
    </head>
    <body class="login">
        <section id="wrapper">
            <div class="container">
                <div class="row">
					
                    <div class="col-md-4 col-md-offset-8 col-sm-8 col-sm-offset-4">
						
                        <form class="form form-horizontal form-login box-form" action="<?php echo $this->location("reset/cek"); ?>" name="auth" method="post">
                            <div class="content-box-form content-login">
								<?php
							if(isset($msglogin)){
								echo $msglogin;
							}
							?>
                                <div class="form-group text-center">
                                    <img class="img-responsive img-responsive-center img-logo-login" src="<?php echo $this->asset("ptiik/images/ptiikappslogo.png") ?>">
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="sr-only" for="exampleInputEmail1">Username or Email</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="User name or email" name="username">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="sr-only" for="exampleInputEmail1">Old Passwaord</label>
                                        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Old Password" name="oldpass">
                                    </div>
                                </div>
								
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <label class="sr-only" for="exampleInputPassword1">New Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="New Password">
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-default btn-block">Reset</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-8">
                                        
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <a href="<?php echo $this->location('page'); ?>"><img class="img img-responsive img-responsive-center" src="<?php echo $this->asset("ptiik/images/ptiik-small.png") ?>"></a>
                                    </div>
                                </div>
                            </div>
                        </form>


                        <footer id="footer">
                            <div>Copy Right &copy; 2014 BPTIK PTIIK UB</div>
                            <div>All rights reserved</div>
                        </footer>
                    </div>
                </div>
            </div>
        </section>
           <script src="<?php echo $this->asset("ptiik/js/jquery-1.11.1.min.js"); ?>"></script>		
		  	<script>
       document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
           //alert('No F-12');
            return false;
        }
    }
    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
    document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
	var message="";
	function clickIE() {if (document.all) {(message);return false;}} function clickNS(e) {if (document.layers||(document.getElementById&&!document.all)) { if (e.which==2||e.which==3) {(message);return false;}}} if (document.layers) {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;} else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;} document.oncontextmenu=new Function("return false") 
	</script>
        <script>
            $(document).ready(function () {
                generateMargin();

                $(window).resize(function () {
                    generateMargin();
                });
                $('.register-now').click(function (e) {
                    e.preventDefault();
                    $('.form-login').animate({
                        "width": "0",
                        "right": "-100%",
                        "opacity": "0"
                    }).hide();
                    $('.form-register').show()
                            .animate({
                                "width": "100%",
                                "right": "0",
                                "opacity": "1"
                            });
                });
                if ($('body.login').length) {
                    $('.form-register').css({
                        "width": "0",
                        "display": "none",
                        "right": "-100%",
                        "opacity": "0"
                    });
                }
                $('.login-now').click(function (e) {
                    e.preventDefault();
                    $('.form-register').animate({
                        "width": "0",
                        "right": "-100%",
                        "opacity": "0"
                    }).hide();
                    $('.form-login').show()
                            .animate({
                                "width": "100%",
                                "right": "0",
                                "opacity": "1"
                            });
                });
                if ($('body.register').length) {
                    $('.form-login').css({
                        "width": "0",
                        "display": "none",
                        "right": "-100%",
                        "opacity": "0"
                    });
                }
            });
            function generateMargin() {
                $('.content-login').css('margin-top', ($('.form-login').height() - $('.content-login').height()) - 30);
                $('.content-register').css('margin-top', ($('.form-register').height() - $('.content-register').height()) - 30);
                var $formtop = ($('body').height() - ($('.form').height() + $('#footer').height())) - 100;
                if ($formtop <= 0)
                    $formtop = 0;
                $('.form').css('margin-top', $formtop);
            }
        </script>
    </body>
</html>

