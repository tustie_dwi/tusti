<?php 
$this->view("page/header.php", $data); 

?>
<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($lang=='in') echo 'Jadwal Akademik'; else echo "Academic Schedule";?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $this->location($url."info/services"); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></li>
					</ol>
					<!-- End Breadcrumb -->
		</div>
	</div>
</section>
  
<section class="content content-white">
	<div class="container">   		
	<section itemscope="" itemtype="http://schema.org/Article">
	<!-- Main Content -->
	<div class="col-md-12">	
				
		<select id="pilih_tipe" class="form-control" onchange="pilih_tipe(this.value)">
			<option value="-">Pilih jadwal berdasarkan</option>
			<option value="hari">Hari</option>
			<option value="ruang">Ruang Kuliah</option>
			<option value="dosen">Dosen Pengampu</option>
			<option value="mk">Matakuliah</option>
			<option value="praktikum">Praktikum</option>
		</select>
		
		<select id="pilih_hari" style="display: none" class="pilih-form form-control" onchange="jadwal_hari(this.value)">
			<option value="-">Pilih hari</option>
			<option>Senin</option>
			<option>Selasa</option>
			<option>Rabu</option>
			<option>Kamis</option>
			<option>Jumat</option>
			<option>Sabtu</option>
			<option>Minggu</option>
		</select>
		
		<select id="pilih_ruang" style="display: none" class="pilih-form form-control" onchange="jadwal_ruang(this.value)">
			<option value="-">Pilih ruang</option>
			<?php
				if($ruang) :
					foreach($ruang as $key){
						echo "<option value='".$key->ruang_id."'>".$key->kode_ruang." - ".$key->keterangan."</option>";
					}
				endif;
			?>
		</select>
		
		<select id="pilih_dosen" style="display: none" class="pilih-form form-control" onchange="jadwal_dosen(this.value)">
			<option value="-">Pilih dosen</option>
			<?php
				if($dosen) :
					foreach($dosen as $key){
						echo "<option value='".$key->karyawan_id."'>".$key->nama."</option>";
					}
				endif;
			?>
		</select>
		
		<select id="pilih_mk" style="display: none" class="pilih-form form-control" onchange="jadwal_mk(this.value)">
			<option value="-">Pilih matakuliah</option>
			<?php
				if($mk) :
					foreach($mk as $key){
						echo "<option value='".$key->mk_id."'>".$key->keterangan." ($key->kode_mk)"."</option>";
					}
				endif;
			?>
		</select>
		
		<select id="pilih_praktikum" style="display: none" class="pilih-form form-control" onchange="jadwal_praktikum(this.value)">
			<option value="-">Pilih praktikum</option>
			<?php
				if($praktikum) :
					foreach($praktikum as $key){
						echo "<option value='".$key->mk_id."'>".$key->keterangan." ($key->kode_mk)"."</option>";
					}
				endif;
			?>
		</select>
		<br>
		<input id="hari_ini" value="<?php echo $this->get_hari(date('N')) ?>" type="hidden">
		<table class="table table-bordered table-condensed">
			<thead class="text-center">
				<tr>
					<td colspan="2">R/J</td>
					<?php
						$jml_jam = 0;
						if($jam) :
							foreach($jam as $key){
								echo "<td>".substr($key->jam_mulai,0,5)." - ".substr($key->jam_selesai,0,5)."</td>";
								$jml_jam++;
							}
						endif;
					?>
					
				</tr>
			</thead>
			
			<tbody id="jadwal-wrap">
				
			</tbody>
		</table>
		
		<input id="jml_jam" value="<?php echo $jml_jam ?>" type="hidden">
	</div>
	<!-- End Main Content -->

	<!-- Sidebar -->
		</section>
	 </div>
</section>
	
<?php $this->view("page/footer.php", $data);  ?>
<script>
	var jam = {
		<?php
			if($jam) :
				foreach($jam as $key){
					echo '"'.substr($key->jam_mulai,0,5) . '":' . $key->urut . ', ';
				}
			endif;
		?>
		akhir : "0"
	};
</script>







