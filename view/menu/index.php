<?php $this->head(); ?>
<div class="row">
	<div class="col-md-8">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('home'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('menu'); ?>">Menu Configuration</a></li>
		  <li class="active"><a href="#">Data</a></li>
		</ol>
			
			<div class="block-box">
			  <div class="header"><h4>List Menu</h4></div>
				  <div class="content">
			
						<?php if( isset($menu) ) : ?>
						
						<table class="table table-striped table-hover" id="example">
							<thead>
								<th>Identifier</th>
								<th>Description</th>
								 <th>Status</th>
								<th>Operation</th>
							</thead>
							
						<?php 
						$dtname = array();
						
						foreach($menu as $dt) { 
							$dtname[] = $dt->id;
						?>
						
							<tr id="menu-<?php echo $dt->id; ?>" 
								data-id="<?php echo $dt->id; ?>">
								<td><em><?php echo $dt->module; ?> </em><code><?php echo $dt->link; ?></code></td>
								<td><strong><?php echo $dt->judul; ?></strong><br><em>icon:</em> <?php echo $dt->icon; ?></td>	
								<td style="width: 80px;"><?php if($dt->is_aktif==1){ echo '<span class="label label-success" id="status-'.$dt->id.'">Active</a></span>'; }
								else { echo '<span class="label label-default" id="status-'.$dt->id.'">Inactive</a></span>';}  ?></td>
								<td>				
									<ul class="nav nav-pills" style="margin:0;">
									<li class="dropdown">
									  <a class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" href="#">Action <b class="caret"></b></a>
									  <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
										<li>
											<a class="btn-toggle-status" data-loading-text="Toggling..." href="#"><i class="icon-refresh"></i> Toggle Status</a>	
										</li>
									  </ul>
									</li>
									</ul>            
								
								</td>
							</tr>
							<?php	
							}
						?>
						</table>
				
				<?php else: ?>		
						<div class="well">Sorry, no menu to show. There are nothing in menu directory.</div>		
				<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<?php $this->view('menu/edit.php', $data); ?>			
		</div>
	</div>


<?php $this->foot(); ?>