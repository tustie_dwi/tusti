<?php
if($emenu){
	$header		= "Edit menu";	
	
	foreach ($emenu as $row):
		$menuid			= $row->id;
		$ncontroller	= $row->link;
		$nmodule		= $row->module;
		$nicon			= $row->icon;
		$njudul			= $row->judul;
		$isaktif		= $row->is_aktif;
	endforeach;		
	
}else{
	$header			= "Write New Menu";
	$menuid			= "";
	$ncontroller	= "";
	$nmodule		= "";
	$nicon			= "";
	$njudul			= "";
	$isaktif		= 1;
}
?>
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil"></i> Create Menu</div>
	<div class="panel-body">  
		<form method=post  action="<?php echo $this->location('menu'); ?>">
			
				<div class="form-group">
					<label>Parent Menu</label>
					
						<select name="cmbmenu" class="span12 cmbmulti form-control" populate>
							<option value="0">None</option>
							<?php
							foreach($submenu as $dt):
								echo "<option value='".$dt->id."' >".$dt->judul."</option>";
							endforeach;
							?>
						</select>
				</div>	
				<div class="form-group">
					<label>Judul</label>
					
						<input type="text" name="njudul" value="<?php echo $njudul; ?>" class="form-control" />
						
				</div>	
				
				<div class="form-group">
					<label>Modules</label>
					<input type="text" name="nmodules" value="<?php echo $nmodule; ?>" class="form-control" />
				</div>	
				<div class="form-group">
					<label>Controller</label>
					<input type="text" name="ncontroller" value="<?php echo $ncontroller; ?>" class="form-control" />
				</div>	
				<div class="form-group">
					<label>Icon</label>
					
					<input type="text" name="nicon" value="<?php echo $nicon; ?>" class="form-control" />
				</div>	
				<div class="checkbox">
					<label>
					  <input type="checkbox" name='isaktif' value='1'>	Aktif
					</label>
				  </div>
				
			<div class="form-group">
			   <input type="submit" name="b_menu" value="Create Menu" class="btn btn-primary btn-create-user" />
			</div>
		</form>
	</div>
</div>
			