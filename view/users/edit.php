<?php $this->head(); 
if(isset($user)){
	$id			= $user->id;
	$hid		= $user->karyawan_id;
	$foto 		= $user->foto;
	$username 	= $user->username;
	$lahir	 	= $user->tgl_lahir;
	$name		= $user->name;
	$password	= $user->password;
	$email		= $user->email;
	$biografi	= $user->biografi;
	$biografien	= $user->biografi_en;
	$about	= $user->about;
	$abouten	= $user->about_en;
	$nickname	= $user->nickname;
	$hp			= $user->hp;
	$interest	= $user->interest;
	$interesten	= $user->interest_en;
	$website	= $user->website;
	$telp		= $user->telp;
	$alamat		= $user->alamat;
	$level		= $user->level;
	$status		= $user->status;
}else{
	$id			= "";
	$hid		= "";
	$foto 		= "";
	$lahir		= "";
	$username 	= "";
	$name		= "";
	$nickname	= "";
	$hp			= "";
	$interest	= "";
	$website	= "";
	$password	= "";
	$email		= "";
	$biografi	= "";
	$telp		= "";
	$alamat		= "";
	$level		= "";
	$status		= "";
}
?>


	
    	<div class="col-md-12">
            <h3 class="heading-title">User Profile</h3>
			 <ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('home');?>"><span class="fa fa-home"></span>&nbsp;Home</a></li>
			  <li class="active"><a href="#">Profile</a></li>
			</ol>
			<ul class="breadcumb-extra">
				<li class="orange time-ticker time-breadcumb">00:00</li>
				<li class="blue date-breadcumb"><span class="fa fa-calendar"></span>&nbsp;<?php echo date("M d, Y"); ?></li>
			</ul>
		</div>	
		
		<div class="col-md-12">
				
              <ul class="nav nav-tabs">
				
				  <li class="active"><a href="#profileinfo" data-toggle="tab">Profile Info</a></li>
				  <li><a href="#profileaccount" data-toggle="tab">Account</a></li>
				</ul>
            
            <div class="tab-content">
              <div class="tab-pane active" id="profileinfo">
				
              	<div class="col-md-12 block-box">	
					<?php
					
								
						if($foto){
							$strimg = $this->config->file_url_view."/".$foto;
						}else{
							$strimg = $this->config->default_thumb_web;
						}
					
					?>
						<div class="col-md-2" style="text-align:center">
						<img src="<?php echo $strimg; ?>" class="image-profile-page img-thumbnail"/>
						</div>
						<div class="col-md-10">
						<ul class="profile-info-detail">
							<h3><?php echo $name; ?></h3>
							<p>
							<?php 
							echo $biografi; 
							if($website){
								echo '<a href="#">'.$website.'</a>';
							}
							?>
							</p>
							
							<ul class="list-inline">
								<li><i class="fa fa-calendar"></i>&nbsp; <?php echo date("M d, Y", strtotime($lahir)); ?></li>
								<li><i class="fa fa-map-marker"></i>&nbsp; <?php echo $alamat; ?></li>
								<li><i class="fa fa-envelope"></i>&nbsp; <?php echo $email; ?></li>								
							
								<?php if($telp){ ?>
									<li><i class="fa fa-phone"></i>&nbsp; <?php echo $telp; ?></li>
								<?php } ?>
								
								<?php if($hp){ ?>
									<li><i class="fa fa-mobile"></i>&nbsp; <?php echo $hp; ?></li>
								<?php } ?>
												
							</ul>
						</ul>
					</div>										
                </div>
				<div class="row">&nbsp;</div>
				<?php if($this->authenticatedUser->role!='mahasiswa'): ?>
				<div class="col-md-12 block-box">
				<?php $minfo = new  model_info(); ?>
					<div class="col-md-12">  
						<ul class="nav nav-tabs" id="writeTab">												
							<li class="active"><a href="#penelitian" role="tab" data-toggle="tab">Penelitian</a></li>	
							<li><a href="#publikasi" role="tab" data-toggle="tab">Publikasi</a></li>
							<li><a href="#prestasi" role="tab" data-toggle="tab">Prestasi</a></li>
							<li><a href="#seminar" role="tab" data-toggle="tab">Seminar/Workshop</a></li>	
							<li><a href="#doro" role="tab" data-toggle="tab">Jurnal DORO</a></li>	
						</ul>

						<div class="tab-content">												
							<div class="tab-pane" id="penelitian">	<?php list_data($minfo,$this->authenticatedUser->staffid); ?></div>
							<div class="tab-pane" id="publikasi"><?php list_data($minfo,$this->authenticatedUser->staffid,'publikasi'); ?></div>	
							<div class="tab-pane" id="prestasi"><?php list_data_prestasi($minfo,$this->authenticatedUser->staffid); ?></div>
							<div class="tab-pane" id="seminar"><?php list_data_kegiatan($minfo,$this->authenticatedUser->staffid); ?></div>
							<div class="tab-pane" id="doro"><?php list_data_doro($minfo,$this->authenticatedUser->staffid, $this->config->file_url_view.'/upload/file/PTIIK/doro/'); ?></div>
						</div>					
					</div>
				</div>
				<?php endif; ?>
				
              </div>
              <div class="tab-pane" id="profileaccount">
              	<div class="row block-box">
                	<div class="col-md-3">
						<nav class="nav-menu-tabbable-side">
						<ul class="nav nav-tabs">
							<?php if($this->authenticatedUser->role!='mahasiswa'): ?>
							<li class="active">
								<a href="#personalinfo" data-toggle="tab"><span class="fa fa-cog"></span> Personal Info</a>
							</li>
							<li class="">
								<a href="#changepassword" data-toggle="tab"><span class="fa fa-lock"></span> Change Password</a>
							</li>
							<?php else: ?>
								<li class="active">
									<a href="#changepassword" data-toggle="tab"><span class="fa fa-lock"></span> Change Password</a>
								</li>
							<?php endif; ?>
							</ul>
						</nav>
                    </div>
                    <div class="col-md-9 block-box">
                        <div class="tab-content">
							<?php if($this->authenticatedUser->role!='mahasiswa'): ?>
                          <div class="tab-pane active" id="personalinfo">                          
                          
								<form action="<?php echo $this->location(); ?>" method="post" id="form-save-user" class="form-horizontal">
								<input type="hidden" name="id" value="<?php echo $id; ?>" />
								<input type="hidden" name="hid" value="<?php echo $hid; ?>" />
								
								
								<div class="form-group">
									<label>Username</label>
									
									<input type="text" name="username" value="<?php echo $username; ?>" readonly class="form-control" />
								</div>
								<div class="form-group">
									<label>Nickname</label>
									
									<input type="text" name="nname" required="required" id="input-name" value="<?php echo $nickname; ?>" class="form-control" />
								</div>
								<div class="form-group">
									<label>Full Name</label>
									
										<input type="text" name="name" required="required" id="input-name" value="<?php echo $name; ?>" class="form-control" />	
								</div>
																
							   <div class="form-group">
									<label>E-mail</label>
									
									<input type="text" name="email" id="input-email" value="<?php echo $email; ?>" class="form-control" />
								</div>
								<div class="form-group">
									<label>Mobile Phone</label>
									
									<input type="text" name="hp" id="input-hp" value="<?php echo $hp;  ?>" placeholder="+62 8133 001221" class="form-control" />
								</div>
								<div class="form-group">
									<label>Phone</label>
									
									<input type="text" name="telp" id="input-telp" value="<?php  echo $telp; ?>" placeholder="0341 400500" class="form-control" />
								</div>
								
								<div class="form-group">
									<label>About</label>									
									<textarea name="about" class="ckeditor form-control" id="about" ><?php if(isset($about)) echo $about; ?></textarea>
								</div>
								
								<div class="form-group">
									<label>Biografi</label>									
									<textarea name="biografi" class="ckeditor form-control" id="bio"><?php echo $biografi; ?></textarea>
								</div>
								<div class="form-group">
									<label>Interests</label>									
									<input type="text" name="interest" id="input-interest" value="<?php echo $interest; ?>" placeholder="Design, Web, etc" class="form-control" />
								</div>
								
								<div class="form-group">
									<label>About (en)</label>									
									<textarea name="about_en" class="ckeditor form-control" id="about_en"><?php if(isset($abouten)) echo $abouten; ?></textarea>
								</div>
								
								<div class="form-group">
									<label>Biografi (en)</label>									
									<textarea name="biografi_en" class="ckeditor form-control" id="bio_en"><?php if(isset($biografien)) echo $biografien; ?></textarea>
								</div>
								<div class="form-group">
									<label>Interests (en)</label>									
									<input type="text" name="interest_en" id="input-interest" value="<?php if(isset($interesten)) echo $interesten;  ?>" placeholder="Design, Web, etc" class="form-control" />
								</div>
								
								<div class="form-group">
									<label>Website URL</label>
									
									<input type="text" name="website" id="input-interest" value="<?php echo $website; ?>"  class="form-control" />
								</div>
								<?php if($this->authenticatedUser->id != $id) : ?>
								<div class="form-group">
									<label>Privileges Level</label>
									
										 <select name="level" class="input-large form-control">
										   <?php //for($level = ((int)$this->authenticatedUser->level)+1; $level <= 10; $level++) { ?>
										  <!-- <option value="<?php //echo $level; ?>"><?php //echo $level; ?></option>-->
										   <?php //} 
										   if(isset($dlevel)){										  
											foreach($dlevel as $dt):
												echo "<option value=".$dt->level." ";
												if($level == $dt->level) echo "selected";
												echo ">".$dt->description."</option>";
											endforeach;
										   }
										   ?>
									</select>
								</div>    
								<div class="checkbox">
									<label>
									  <input type="checkbox" value="1" <?php if($status) echo 'checked="checked"'; ?> name="status" /> Active
									</label>
								  </div>								
								
								<?php else: ?>
								<input type="hidden" name="level" value="<?php echo $level; ?>" />
									<input type="hidden" name="status" value="<?php echo $status; ?>" />
								<?php endif; ?>
								<div class="clearfix"></div>
								
								<div class="form-actions">
									<?php if(strToLower($this->authenticatedUser->role)=='administrator' || strToLower($this->authenticatedUser->role)=='bptik'){ ?>
									<a href="<?php echo $this->location('user'); ?>" class="btn pull-right"><i class="icon-list"></i> Back to User List</a>
									<?php } ?>
									<input type="submit" value="Save User Profile" class="btn btn-primary btn-save-user" data-loading-text="Saving..." />
								</div>
								<span id="status-save" style="margin-left:1em;">&nbsp;</span>                   
							</form>
                
                          </div>
						  <?php endif; ?>
                          <div class="tab-pane <?php if($this->authenticatedUser->role=='mahasiswa') echo "active" ?>" id="changepassword">
								<form action="<?php echo $this->location(); ?>" method="post" id="form-update-password" class="form-horizontal">
									<input type="hidden" name="id" value="<?php echo $user->id; ?>" />
									<div class="">
									<fieldset>
										<div class="form-group">
											<label>Password</label>
											
											<input type="password" name="password" id="input-password" class="form-control" />
										</div>
										<div class="form-group">
											<label>Password (Again)</label>
											
											<input type="password" name="password2" id="input-password2" class="form-control" />
										</div>
										<div class="form-actions">										
										<input type="submit" value="Change Password" class="btn btn-primary btn-update-password" data-loading-text="Updating..." /><br>
										<span id="status-password" style="margin-left:1em;">&nbsp;</span>
										</div>										
										
									</fieldset>
									</div>                
								</form>
                          </div>
                        </div>
                </div>
              </div>
            </div>
    	</div>
	</div>


<?php $this->foot(); 

function list_data_doro($minfo=NULL,$posts=NULL, $path_link=NULL){
	
			//$minfo = new model_info();
			$dtdoro = $minfo->get_rekap_doro($posts);
				
			if($dtdoro):
			?>
		<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo judul ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($dtdoro as $dt):
					$path = $path_link.$dt->periode.'/'.$dt->jurnal_type.'/'.$dt->full_file_in;		
					$str = preg_replace('/<p[^>]*?>/','',$dt->judul);
					$judul = str_replace("</p>","",$str);
				?>
				<tr>
				<td style="display:none"></td>
				<td>
					<a href="<?php echo $path ?>" target="_blank"><b><?php echo $judul;?></b></a><br>
					<small>Volume <?php echo $dt->volume; ?>&nbsp; No. <?php echo $dt->no; ?></small>
				</td>
			</tr>
			<?php endforeach ?>
			</tbody>
			</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
	<?php
	}
	
	function list_data($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$penelitian = $minfo->get_rekap_penelitian($str, $posts);
				
			if($penelitian):
			?>
		<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo judul ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($penelitian as $dt):
					
				?>
			<tr>
				<td style="display:none"></td>
				<td>
					<?php 
					if($dt->jurnal_link)  echo "<a href='".$dt->jurnal_link."' target='_blank'><b>".$dt->judul."</b></a>";					
					else echo "<b>".$dt->judul."</b>";  ?> <span class="label label-default"><?php echo $dt->tahun ?></span><br>													
					<small><b>Author:</b>					
					<?php 
					
					$peserta = explode("@", $dt->peserta);
					
					for($j=0;$j<count($peserta);$j++){ 
						$k = $j+1;
						if($k==count($peserta)){
							$str = " ";
						}else{
							$str = ", ";
						}
						$strpeserta = explode("-",$peserta[$j]);
						$npeserta = reset($strpeserta);
						$ketua = end($strpeserta);
						if($ketua):
							echo "<span class='text text-danger'>".$npeserta ;
							echo "</span>".$str;
						else:
							echo "<span class='text text-default'>".$npeserta ;
							echo "</span>".$str;
						endif;
					}								
					?><br>
					<em><?php if($dt->kategori_publikasi) echo ucWords($dt->kategori_publikasi); ?>  <?php if($dt->lokasi) echo " . ".$dt->lokasi;?>
					<?php if($dt->jurnal_nama) echo " . ".$dt->jurnal_nama. " ". $dt->jurnal_edisi;?>
					</em>
					</small>
				</td>
			</tr>
			<?php endforeach ?>
			</tbody>
			</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
	<?php
	}
	
	function list_data_prestasi($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$prestasi = $minfo->get_rekap_prestasi($posts);
				
			if($prestasi):
				?>
			<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo nama ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($prestasi as $dt):
							
						?>
					<tr>
						<td style="display:none"></td>
						<td>
							<div class="col-md-8">
								<small><span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
									echo date("M d, Y", strtotime($dt->tgl_mulai)); 
									if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
										echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
									}
								?>
								</span></small><br>
								<?php echo $dt->judul."&nbsp; ";  if($dt->penghargaan) echo "<span class='text text-success'><i class='fa fa-bookmark'></i> ".$dt->penghargaan."</span>"; ?><br>
								 
								<small><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara ?>
								&nbsp; <i class="fa fa-map-marker"></i> <?php echo $dt->lokasi ?></small>
								<?php if($dt->link_berita) echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-search'></i> <b><a href=".$dt->link_berita." target='_blank' class='text text-danger'>Baca Berita</a></b></span></small>" ?>
							
							</div>
							<div class="col-md-4">
							
							<?php 
							
							$peserta = explode(",", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
									
								echo "<span class='text text-danger'>".$peserta[$j]."</span>".$str;
							}								
							?><br><small><?php echo $dt->inf_prestasi; ?></small>&nbsp;<span class="label label-danger"><?php echo ucwords($dt->tingkat); ?></span>
							<!--<span class="label label-default"><?php //echo ucwords($dt->jenis_prestasi); ?></span>--></div>
						</td>
					</tr>
					<?php endforeach ?>
					</tbody>
				</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
									
	<?php
	}
	
	function list_data_kegiatan($minfo=NULL,$posts=NULL, $str=NULL){
	
			//$minfo = new model_info();
			$kegiatan = $minfo->get_rekap_kegiatan($posts);
				
			if($kegiatan):
				?>
			<table class="table table-striped web-page">
			<thead>
				<tr>
					<th style="display:none"></th>
					<th># <?php echo nama ?></th>	
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($kegiatan as $dt):
							
						?>
					<tr>
						<td style="display:none"></td>
						<td>
							<div class="col-md-12">
								<small><span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
									echo date("M d, Y", strtotime($dt->tgl_mulai)); 
									if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
										echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
									}
								?>
								</span>&nbsp;<span class="text text-danger"><i class="fa fa-tag"></i> <?php echo ucwords($dt->sebagai);?></span></small><br>
								<?php echo $dt->nama_kegiatan;   echo " <span class='label label-default'>".$dt->jenis_kegiatan."</span>"; ?><br>
								 
								<small><?php if($dt->penyelenggara!=""){ ?><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara."&nbsp;";
								}?> 
								<?php if($dt->tempat_kegiatan!=""){ ?><i class="fa fa-map-marker"></i> <?php echo $dt->tempat_kegiatan;  ?><?php } ?></small>
							
							</div>
							
						</td>
					</tr>
					<?php endforeach ?>
					</tbody>
				</table>	
		<?php else: 
			echo "<small><em>Data not available</em></small>";
		endif; ?>
									
	<?php
	}
?>