<?php $this->head(); ?>

	<div class="row-fluid">
    	<div class="span12">
            <h3 class="heading-title">Tasks</h3>
			 <ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('home');?>"><span class="fa fa-home"></span>&nbsp;Home</a></li>
			  <li class="active"><a href="#">Tasks</a></li>
			</ol>
			<ul class="breadcumb-extra">
				<li class="orange time-ticker">00:00</li>
				<li class="blue"><span class="fa fa-calendar"></span>&nbsp;<?php echo date("M d, Y"); ?></li>
			</ul>
		</div>	
	</div>
	
	<div class="row-fluid">
		<div class="span8">
			<fieldset>
				<legend>Task List</legend>
				<?php if( isset($users) ) : ?>
				<form id="frmtask">
				<table class="table table-striped" id="example">
					<thead>
						<th>Task</th>
                        <th>&nbsp;</th>
					</thead>
					<?php
					foreach($users as $user) {
						?>
						<tr id="user-<?php echo $user->task_id; ?>" data-id="<?php echo $user->task_id; ?>" class="label-task" <?php if($user->progress=='1'){  echo "style='text-decoration:line-through'"; }else{ echo "style='text-decoration:none'";} ?>>
							<td>
								<i class="fa fa-clock-o"></i> <?php 
								echo date("M d, Y H:i",strtotime($user->tgl_mulai))."<br>";
								
								echo "<input type='checkbox' onClick='strikeIt(this)' class='btn-toggle-task' data-uid='".$user->task_id."' value='1' ";
								if($user->progress==1){
									echo "checked";
								}
								echo "> &nbsp; ";
								echo "<b>".$user->judul; ?></b>						
								<blockquote><?php echo $user->keterangan; ?></blockquote>
								<input type="hidden" class="label-tasks" value="<?php echo $user->progress; ?>">
							</td>
							<td style="min-width: 100px;">
							  <ul class="nav nav-pills" style="margin:0;">
							  <li class="dropdown">
				                <a class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" href="#">Action <b class="caret"></b></a>
				                <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
				                    <li>									
									<a class="btn-delete-task" data-uid="<?php echo $user->task_id; ?>" href="#">
									  <i class="icon-remove"></i> Delete
									</a>
											                  
				                  </li>
				                </ul>
				              </li>
							  </ul>
						</td>
					</tr>
					<?php	
				}
				?>
			</table></form>
		<?php else: ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no posts to show</div>
		</div>
	<?php endif; ?>
	
</fieldset>
</div><!--/span6-->
<div class="span4">
	<?php //var_dump($user->level); ?>
	
		<legend>Create New Task</legend>
		
			<form action="<?php echo $this->location('user/tasks'); ?>" method="post" id="form-create-task" class="form-vertical">
			   <div class="well">
				
			   	    <div class="control-group">
                        <label class="control-label">Tgl</label>
                        <div class="controls">
                          <input type="text" name="tgl"  class="span8 form_datetime" />s/d
                             <input type="text" name="tglselesai"  class="span8 form_datetime" /
                        </div>
				    </div><!--/control-group-->
					<div class="control-group">
                        <label class="control-label">Task</label>
                        <div class="controls">
                          <input type="text" name="taskname" required="required" id="input-task" />
                        </div>
				    </div><!--/control-group-->		
                   <div class="control-group">
                        <label class="control-label">Catatan</label>
                        <div class="controls">
                          <textarea name="catatan"></textarea>
                        </div>
				    </div><!--/control-group-->		
				    <div class="control-group">
					    <div class="controls">
					    <label class="checkbox inline">
					    <input type="checkbox" value="1" name="status" /> Finish</label>
					    </div>
				    </div>
			   </div>
				   <div class="form-actions">
					   <input type="submit" value="Create Task" name="b_task" class="btn btn-primary btn-create-task" />
				   </div>
			   </form>
		   </div>
   </div><!--/row-->
</div><!--/container-->

<?php $this->foot(); ?>