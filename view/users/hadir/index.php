<?php $this->head(); ?>
    <div class="row">
    <div class="col-md-12">
		<div class="row kehadiran-header">
		<?php
		if($hadir){
		foreach ($hadir as $dt): 
			if(! $dt->foto){
					$foto= "no_foto.png";
				} else {
					$foto = $dt->foto;
				}	
				
				if($dt->is_dosen==0){
					$isdosen = 'Staff';
				}else{
					$isdosen = 'Dosen';				
				}
				?>
				<div class="col-md-12">
					<div class="col-md-2 col-sm-3 col-xs-4">
							<img src="<?php echo $this->asset('uploads/foto/'.$foto); ?>" class='img-thumbnail' width="171" height="180">
					 </div>
					<div class="col-md-10 col-sm-9 col-xs-8">
							<h2 class="kehadiran-nama"><?php 
							echo $dt->nama; if($dt->gelar_awal!=""){ echo ", ".$dt->gelar_awal; } if($dt->gelar_akhir!=""){ echo ", ".$dt->gelar_akhir; }	
							echo "&nbsp;<span class='badge badge-info' style='font-weight:normal'>".$isdosen."</span>";									
							echo "</h2>";
														
							if($dt->nik!="-"){
								echo "<h4>".strToUpper($dt->is_nik).". ".$dt->nik."</h4>";
							}
							
							if($dt->jabatan){
							?>
								<div class="kehadiran-pangkat"><span class="text text-info" style='font-weight:normal'><?php echo $dt->jabatan; ?></span></div>
							<?php
							}	
							
							if($dt->unitkerja){
								$unitk = explode("@", $dt->unitkerja);
							
								for($i=0;$i<count($unitk);$i++){
									$unitid = explode("-", $unitk[$i]);
									
									for($k=0;$k<count($unitid);$k++){							
										if($k==0){											
											echo "<span class='label label-default' style='font-weight:normal'>".$unitid[$k]."</span> ";								
										}
									}
								}
							
							}

							if($dt->ruangkerja){
								$ruang = explode(",", $dt->ruangkerja);
									
								for($k=0;$k<count($ruang);$k++){				
																				
									echo "<code>R. ".$ruang[$k]."</code> ";									
									
								}
							}							
											
							?>										
						</div>
					</div>
				<?php
		endforeach;
		
		?>
		</div>
		<?php if(isset($type)){
		?>
				<div class="control-group">
				<?php if($type=='month'){ ?>
					
					<div class="tab-content">
						<div class="tab-pane fade" id="list">	<?php $this->view('users/hadir/listview.php', $data); ?>	</div>
						<div class="tab-pane fade" id="week"><?php  $this->view('users/hadir/weekcalendar.php', $data); ?></div>
						<div class="tab-pane active fade in" id="grid"><?php  $this->view('users/hadir/calendar.php', $data); ?></div>
					</div>
				<?php } else {

						if	($type=='week'){		?>
						
						<div class="tab-content">			
							<div class="tab-pane fade" id="list">	<?php $this->view('users/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane active fade in" id="week"><?php  $this->view('users/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('users/hadir/calendar.php', $data); ?></div>
						</div>
				<?php }else{ ?>
						
						<div class="tab-content">			
							<div class="tab-pane fade in active" id="list">	<?php $this->view('users/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane fade" id="week"><?php  $this->view('users/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('users/hadir/calendar.php', $data); ?></div>
						</div>

				<?php }
				}
				?>
			</div>
		<?php
		}else{
		?>
		
			<div class="control-group">
				<?php if((isset($_POST['month']))||(isset($_POST['year'])) ){ ?>
					
					<div class="tab-content">
						<div class="tab-pane fade" id="list">	<?php $this->view('users/hadir/listview.php', $data); ?>	</div>
						<div class="tab-pane fade" id="week"><?php  $this->view('users/hadir/weekcalendar.php', $data); ?></div>
						<div class="tab-pane active fade in" id="grid"><?php  $this->view('users/hadir/calendar.php', $data); ?></div>
					</div>
				<?php } else {

						if	((isset($_POST['b_next']))||(isset($_POST['b_prev'])) ||(isset($_POST['b_now']))){		?>
						
						<div class="tab-content">			
							<div class="tab-pane fade" id="list">	<?php $this->view('users/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane active fade in" id="week"><?php  $this->view('users/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('users/hadir/calendar.php', $data); ?></div>
						</div>
				<?php }else{ ?>
						
						<div class="tab-content">			
							<div class="tab-pane fade in active" id="list">	<?php $this->view('users/hadir/listview.php', $data); ?>	</div>
							<div class="tab-pane fade" id="week"><?php  $this->view('users/hadir/weekcalendar.php', $data); ?></div>
							<div class="tab-pane fade" id="grid"><?php  $this->view('users/hadir/calendar.php', $data); ?></div>
						</div>

				<?php }
				}
				?>
			</div>
		<?php }
	}else{
		?>
		<div class="span3" align="center">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php
	}?>
	</div>
</div>
<?php $this->foot(); ?>
