<br>
<div class="row">
    <div class="col-md-6">
	<form class="form-inline" method=post action="<?php echo $this->location('user/event')?>" role="form">
		<div class="form-group">			
			<input type="text" name="tgl" class="date date-time-picker form-control" placeholder='Tgl..' value="<?php echo $tgl; ?>" required data-format="YYYY-MM-DD">
		</div>
		<div class="form-group"> 
			<input type="hidden" name="hidid" value="<?php echo $civitasid ?>">
			<button type="submit" name="b_cek" class="btn btn-info btn-no-radius"> View </button>
		</div>
	</form>
        
    </div>
    
    <div class="col-md-6">
    <div class="text-right-md">
    <a href="#list" data-toggle="tab" class="btn-mod mod-orange active"><span class="btn-mod-icon fa fa-calendar"></span><span class="btn-mod-text">Harian</span></a>
    <a href="#week" data-toggle="tab" class="btn-mod mod-blue"><span class="btn-mod-icon fa fa-calendar"></span><span class="btn-mod-text">Mingguan</span></a>
    <a href="#grid" data-toggle="tab" class="btn-mod mod-blue"><span class="btn-mod-icon fa fa-calendar"></span><span class="btn-mod-text">Bulanan</span></a>
    </div>
                </div>
                </div>
	
	<?php
    
	if($tgl){
		echo "<h3>".date('F d, Y', strtotime($tgl))."</h3>";
	}
	
	?>
	<?php
	
	
	if( isset($hadir) ) :	

		$term=array();
		
		if($absen){
			foreach($absen as $dt):
				$term[] = $dt->pin;
			endforeach;
		}
		
	?all><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", <em>".$row->kegiatan." ".$row->namamk."</em> </small></span><br>";
											break;
											default:
												if($row->ruang){
													echo "<span class='text text-info' style='font-weight:normal'><small><i class='fa  fa-clock-o'></i> 
															".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", ".$row->kegiatan."  </small></span><br>";
												}else{
													echo "<span class='text text-info' style='font-weight:normal'><small><i class='fa  fa-clock-o'></i> 
															".$row->jam_mulai." - ".$row->jam_selesai.", ".$row->kegiatan."  </small></span><br>";
												}
											break;
										}
										
									}
								endforeach;
							}else{
								echo "-";
							}
							?>
						</td>
						<td><?php echo $strisi; ?></td>
					</tr>
			<?php
			endforeach; 
		}
		?>
			
		</tbody></table></div>
		
		<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif;  ?>> 
	<div class="table-responsive">
		<table class='table table-striped table-hover' >
				<thead>
					<tr>
						<th>Kegiatan</th>
						<th>Kehadiran</th>
					</tr>
				</thead>
				<tbody>
	<?php
		if($hadir){
			$i = 1;			
			foreach ($hadir as $dt): 
					if(in_array($dt->pin, $term)){										
						$strclass = "absen";
						$strisi = "<div class='label label-success' align='center'>Hadir</div>";
						$class = "";										
					}else{
						$class = "gray";
						$strclass = "nabsen";
						$strisi = "<div class='label label-danger' align='center'>Tidak Hadir</div>";
					}
					?>
					<tr>
						<td>
							<?php
							
							if($kegiatan){
								foreach($kegiatan as $row):
									if($row->karyawan_id==$dt->karyawan_id){	
										switch ($row->kegiatan){
											case 'uts':
												echo "<span class='text text-danger'><small><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang.", <em>Pengawas ".strToUpper($row->kegiatan)." ".$row->namamk."</em> </small></span><br>";
											break;
											case 'Bimbingan & Konsultasi':
												echo "<span class='label label-warning' style='font-weight:normal'><i class='fa  fa-clock-o'></i> ".$row->jam_mulai." - ".$row->jam_selesai.", R. ".$row->ruang." - ".$row->kegiatan."  </span><br>";
											break;
											case 'Perkuliahan':
												echo "<span class='text text-warning'><sm