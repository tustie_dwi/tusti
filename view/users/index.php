<?php $this->head(); ?>
<div class="row">
	<div class="col-md-8">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('home'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('user'); ?>">User Configuration</a></li>
		  <li class="active"><a href="#">Data</a></li>
		</ol>
			<div class="block-box">
				 <div class="header"><h4>User List</h4></div>
				 <div class="content">
			
				<table class="table table-striped" id="example">
					<thead>
						<th>User</th>
						<th>Operation</th>
					</thead>
						<?php if( isset($users) ) : ?>
					<?php
					foreach($users as $user) {
						?>
						<tr id="user-<?php echo $user->id; ?>" data-id="<?php echo $user->id; ?>">
							<td>
							<?php echo $user->name; ?>
							<span class="badge badge-info">Level <?php echo $user->level; ?></span>
							<?php 
							switch($user->status) { 
								case 1:
								echo '<span class="label label-success label-status">Active</span>';
								break;
								case 0:
								echo '<span class="label label-warning label-status">Suspended</span>';
							} 
							?>
							<p>
							<code><?php echo $user->username; ?></code>
							<?php if( $user->email ) echo '<i class="icon-envelope"></i> <small>'.$user->email.'</small>'; ?>
							</p>
							</td>
							<td style="min-width: 100px;">
							  <ul class="nav nav-pills" style="margin:0;">
							  <li class="dropdown">
				                <a class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" href="#">Action <b class="caret"></b></a>
				                <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
				               
				                <!--  <li>
				                  <a class="btn-toggle-status-user" data-uid="<?php //echo $user->id; ?>" data-loading-text="Toggling..." href="#"><i class="icon-refresh"></i> Toggle Status</a>	
				                  </li>
				                  <li class="divider"></li>-->
				                 
				                  <li>
									<a class="btn-edit-user" href="<?php echo $this->location('user/edit/'.$user->id); ?>">
										<i class="icon-pencil"></i> Edit
									</a> 
				                  </li>
				                  <li>
								
									<a class="btn-delete-user" data-uid="<?php echo $user->id; ?>" href="#">
									  <i class="icon-remove"></i> Delete
									</a>
									<a class="btn-change-password"href="<?php echo $this->location('user/edit/'.$user->id."#password"); ?>">
										<i class="icon-lock"></i> Change Password
									</a> 
											                  
				                  </li>
				                </ul>
				              </li>
							  </ul>
						</td>
					</tr>
					<?php	
				}
				?>
				<?php endif; ?>	
			</table>
		</div>
	
	</div>

</div><!--/span6-->
<div class="col-md-4">

		<div class="panel panel-default">
			  <div class="panel-heading"><i class="fa fa-pencil"></i> Users Form</div>
				<div class="panel-body">
		
					<form action="<?php echo $this->location('user'); ?>" method="post" id="form-create-user" class="form-vertical">
					 
						<div class="form-group">
								<label>Name</label>
								<select name="cmbstaff" class="form-control cmbmulti populate">
									<option value="-">Select Karyawan</option>
									<?php
									foreach($staff as $dt):
										echo "<option value='".$dt->tag."' >".$dt->tag."</option>";
									endforeach;
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Username</label>							
							  <input type="text" name="username" class="form-control" required="required" id="input-username" />
							</div><!--/form-group-->
							<div class="form-group">
								<label>Password</label>								
								<input type="password" name="password" class="form-control" id="input-password" />
							</div>
							<div class="form-group">
								<label>Password (Again)</label>								
								<input type="password" name="password2" class="form-control" id="input-password2" />
							</div><!--/form-group-->
						
							<div class="form-group">
								<label>Privileges</label>
								
									<select name="level" class="input-large form-control">
									 
									   <?php //} 
									   if(isset($level)){
										foreach($level as $dt):
											echo "<option value=".$dt->level.">".$dt->description."</option>";
										endforeach;
									   }
									   ?>
								   </select>
							</div>
							<div class="form-group">
								<label>Status</label>
								
								<label class="checkbox">
								<input type="checkbox" value="1" checked="checked" name="status" /> Active</label>
							</div>
							 <div class="form-group">
							   <input type="submit" value="Create User" name="b_user" class="btn btn-primary btn-create-user" />
							</div>						  
						</form>
					</div>
				</div>
			</div>
	   </div>
   </div><!--/row-->

<?php $this->foot(); ?>