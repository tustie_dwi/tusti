<?php 
$this->view("header-page.php", $data); 
$mpage = new model_page();
?>

<!-- Main Content -->
	<section class="col-sm-8">
		<section class="main-content">
			<section class="news-list">
				<?php
					if($post){						
					?>
					<h2 class="title-content"><?php echo $post->judul ?></h2>
					<hr class="hr-content">
				
					<article class="content-news">
						
							<div class="flowplayer">
							  <video>
								 <source type="video/webm" src="<?php echo $this->asset($post->file_loc);  ?>">
								 <source type="video/mp4" src="<?php echo $this->asset($post->file_loc);  ?>">
								 <source type="video/flash" src="<?php echo $this->asset($post->file_loc);  ?>">
							  </video>
						   </div>
						
							<div class="col-md-12">
							<?php 
							echo "<h4>".$post->judul."</h4>";
							echo "<small>".$post->keterangan."</small>"; ?>
							</div>
					</article>
					<?php } ?>
			</section>
		</section>
	</section>

		<!-- End Main Content -->

		<!-- Sidebar -->
		 <?php
		$data['mpage'] = $mpage;
		 $this->view("page/sidebar.php",$data);?>
		<!-- End Sidebar -->
	</section>
</section>
    
<?php $this->view("footer-page.php", $data); ?>

