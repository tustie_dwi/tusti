<hr/>
<h3>Ask a Question</h3>
<form id="main-registration" method="POST" action="<?php echo $this->location('event/save_feed'); ?>" rule="form" class="form-horizontal">
	<input type="hidden" name="tmpid" value="<?php echo $kegiatan_id ?>">
	<input type="hidden" name="chpid" value="<?php echo $yx ?>">
	<div class="well content content-white" style="padding-top:20px;padding-bottom:20px">
		<div class="form-group">
			<label class="control-label col-sm-2" for="inputEmail" style="text-align:left;font-weight:400">Email <small class="text-danger">*</small></label>
			<div class="col-sm-10">
				<input type="email" id="feedback-email" name="feedback-email" placeholder="Email" class="form-control" required />
			</div>
		</div>
		<div class="form-group">
			<label for="inputName" class="control-label col-sm-2" style="text-align:left;font-weight:400">Name <small class="text-danger">*</small></label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="feedback-name" name="feedback-name" placeholder="Name" required />
			</div>
		</div>
		<div class="form-group">
			<label for="inputTitle" class="control-label col-sm-2" style="text-align:left;font-weight:400">Title <small class="text-danger">*</small></label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="feedback-title" name="feedback-title" placeholder="Title" required />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEnquiry" class="control-label col-sm-2" style="text-align:left;font-weight:400">Enquiry</label>
			<div class="col-sm-10">
				<textarea class="form-control" id="feedback-enquiry" rows="6" name="feedback-enquiry" placeholder="Message enquiry..."></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="inputCaptcha" class="control-label col-sm-2" style="text-align:left;font-weight:400">Security Code <small class="text-danger">*</small></label>				
			<div class="col-sm-10">
				<input style="width:50px;display:inline-block" class="form-control" type="text" id="captcha-yx" name="captcha-yx" required /> + <?php echo $jx; ?> = <?php echo $kx; ?>
			</div>
		</div>
		<hr/>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button id="btn-submit" type="button submit" class="btn btn-default">Submit</button>
				<button id="btn-reset" type="button reset" class="btn btn-danger" onclick="resetForm()">Cancel</button>
			</div>
		</div>
		<p><small class="text-danger">field (*) is required</small></p>
	</div>
</form>
<script>
	function resetForm(){
		$(":text").val('').text('');
		$("[type='email']").val('').text('');
		$("#feedback-enquiry").val('').text('');
	}
</script>