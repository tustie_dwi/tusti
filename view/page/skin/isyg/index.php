<?php $this->view( 'page/skin/isyg/header.php', $data ); ?>
<?php 
if($about):
	$i=0;
	foreach($about as $dt):	
		$i++;
		if($i>1) $sclass = 'content-white';
		else $sclass='content-dark';
	?>
		<section itemscope="" itemtype="http://schema.org/Article">			
			<section  itemprop="articleBody" class="content <?php echo $sclass?> padding-bottom-no">
				<div class="container">				
					<div class="col-md-12" style="padding-left:0">			
					<h2 class="title-content margin-top-no margin-bottom-sm font-orange uppercase font-raleway"><span><?php	echo $dt->judul; ?></span></h2>
					<div class="wrap-body"><?php echo $dt->keterangan; ?></div>	
					</div>							
				</div>
			</section>
		</section>
<?php 
	endforeach;
endif; ?>

<section itemscope="" itemtype="http://schema.org/Article">	
	 <section itemprop="articleBody" class="content content-white">
		<div class="container">
			<div class="col-md-12" style="padding-left:0">
				<h2 class="title-content title-underline margin-top-no margin-bottom-sm font-orange font-raleway uppercase"><span>Keynote Speaker</span></h2>
				<div class="speaker-people text-center">
					<div class="row">
						<?php
						if($speaker):
							foreach($speaker as $key):
								if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
								else $imgthumb = $this->config->default_thumb_web;
							
								?>
																
								<div class="col-md-3 col-xs-6">
									<div class="speaker-people-box">
									<img class="img img-responsive img-responsive-center" src="<?php echo $imgthumb; ?>"><!--</a>-->
										<div class="speaker-people-desc">
											<div class="speaker-people-text text-left text-center-sm">
												<h5 class="margin-top-no margin-bottom-no uppercase"><?php echo $key->file_title; ?></h5>
												<small><?php echo $key->file_note; ?></small>
											</div>
										</div>
									</div>
								</div>
								
								
								<?php
							endforeach;
						endif;
						?>
						<div class="col-md-6">						
							<img src="<?php echo $this->asset_file('ptiik/images/keynote-speaker.jpg'); ?>" class="img img-responsive img-responsive-center" >
						</div>
				
					</div>
				</div>

			</div>
		</div>
	</section>


	<section itemprop="articleBody" class="content content-white padding-top-no">
		<div class="container">
			<div class="col-md-12" style="padding-left:0">
				<h2 class="title-content title-underline margin-top-no margin-bottom-sm font-orange uppercase font-raleway"><span>News</span></h2>
				<div class="row">
					<?php
						if(isset($news) && ($news) ){
							foreach ($news as $dt):
								$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	

								if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
								else $imgthumb = $this->config->default_thumb_web;
											
								?>

								<div class="col-sm-4">
									<div class="iysg-news">
										<h2 class="margin-top-no"><?php echo $mpage->content($dt->judul,5);?></h2>
										<div><?php echo $mpage->content($dt->keterangan,50);?> <a itemprop="url" rel="alternate" hreflang="x-default" href="<?php echo $this->location('event/'.$kevent.'/read/news/'.$content.'/'.$dt->id); ?>" class="read-more font-orange">more</a>
										</div>
									</div>
								</div>
								
							<?php
							endforeach;
						}
						?>	
				</div>

			</div>
		</div>
	</section>		

 <section itemprop="articleBody" class="content content-white" style="background:rgba(0,0,0,0.05)">
	<div class="layer">
		<div class="container">
			<div class="col-md-12" style="padding-left:0">
				<h2 class="title-content font-orange margin-top-no uppercase font-raleway"><span>Our Sponsorship</span></h2>
				<div class="row text-center partnership">
					<?php
					if(isset($kerjasama) && $kerjasama){
						foreach($kerjasama  as $dt):
							if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
							else $imgthumb = $this->config->default_thumb_web;
							
							$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
							
							
							if (isset($url)) $link = $this->location($url.'/read/kerjasama/'.$content.'/'.$dt->id);
							else $link = $this->location('page/read/kerjasama/'.$content.'/'.$dt->id);
							?>
							
							<div class="col-xs-3">
									<img src="<?php echo $imgthumb; ?>" class="img-responsive img-responsive-center" alt="<?php echo $dt->judul ?>">
								</div>									
					
							<?php
						endforeach;
					} ?>
					
				</div>
			</div>
		</div>
	</div>
</section>
			
			
<section itemprop="articleBody" class="content content-dark padding-top-no">
	<div class="container">
		<div class="col-md-12 text-center">
			<!--<h2 class="title-content margin-top-no margin-bottom-sm font-orange uppercase font-raleway text-center"><span>JOIN US</span></h2>-->
			<h2 class="padding-top-no">Register now and you can get benefits</h2>

<p class="text-left" style="font-size:20px">&bull; Your latest research results will be shared and acknowledged by scholars around the world.<br />
&bull; The title and abstract of your speech will be posted on the conference website and program booklets.<br />
&bull; The title and abstract of your speech will be published in the proceeding with ISSN.<br />
&bull; You can enjoy a beautiful sceenery of Mount Bromo.</p>
			<div class="text-center font-raleway">
				<a itemprop="url" rel="alternate" hreflang="x-default" href="<?php echo $this->location('event/'.$kevent.'/read/registration/f539899') ?>" class="btn btn-info btn-lg">Register Here</a>&nbsp;
				<a itemprop="url" rel="alternate" hreflang="x-default" href="<?php echo $this->location('event/'.$kevent.'/read/call-for-paper/27f70ce') ?>" class="btn btn-default btn-lg">Call for Paper</a>&nbsp;
				<a itemprop="url" rel="alternate" hreflang="x-default" href="<?php echo $this->location('event/'.$kevent.'/submission') ?>" class="btn btn-default btn-lg">Paper Submission</a>
			</div>

		</div>
	</div>
</section>
</section>
	<?php $this->view( 'page/skin/isyg/footer.php', $data ); ?>	