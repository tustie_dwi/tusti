<?php $this->view( 'page/skin/isyg/header.php', $data ); ?>
<section class="content content-dark">
		<div class="container">	
			<div class="row">
			<div class="col-md-12 margin-bottom-no">
			<?php		
			if(isset($detail)):	
				
					
				if($detail->keterangan)	$strcontent	= $detail->keterangan;
				else $strcontent = $detail->keterangan_ori;	
			
						?>
					<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-sm font-raleway font-orange padding-bottom-no" itemprop="name"><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori;
				
					?></h1>
						
					</section>				
					<section itemprop="articleBody">
						<?php
						//echo $detail->content_data;
						$strcontent_ = $mpage->str_content($strcontent,$this->config->file_url_view,$this->config->file_url_old);
						echo $this->add_alt_tags($strcontent_,"a"); 
						
						if(isset($detail->content_data)):
							if(isset($url)) $url_link=$url;
							else $url_link = "";
							$this->read_data($detail->content_data, $url_link, $eventid);
						endif;
						?>
						<div class="content-pane-tube text-right margin-bottom-sm">                						
							<div class="tube-share-button">
							<h4>
								<span class="fa fa-share-alt" title="Share"></span> <small>Share</small>
								<a class="fb" href="javascript::" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','fbshare','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-facebook-square"></span></a>
								<a class="twitter" href="javascript::" onclick="window.open('https://twitter.com/intent/tweet?text=Judul&url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>&via=PTIIK_UB','twt','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-twitter-square"></span></a>
								<a class="gplus" href="javascript::" onclick="window.open('https://plus.google.com/share?url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','gp','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-google-plus-square"></span></a>
								</h4>
							</div>
						</div>

					</section>
					
				<?php
			endif;	
			
			if(isset($msg)) echo $msg;
			
			if(isset($submission)){
				if(! isset($pin)):
				?>
				<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-sm font-raleway font-orange padding-bottom-no" itemprop="name">Submission</h1>
				</section>
				<?php
				endif;
				if(isset($detail_peserta) && $detail_peserta->jenis_peserta!='peserta'){
					$this->view('page/skin/isyg/submission.php', $data);
				}else{
					echo "<div class='alert alert-danger'>Sorry, you don't have any permission to access this page.</div>";
				}
			}
			if(isset($forgotpin)){
				echo $pin;
			}
			?>
			</div>
		</div>
	</div>
</section>
<?php $this->view( 'page/skin/isyg/footer.php', $data ); ?>	