<div class="container">

<div class="row">
	<div class="well padding-top-no">
		<h2 class="font-raleway"><small>Registration Type: </small> <span class="text text-danger"><?php echo $jenis_peserta ?></span></h2>
	</div>
</div>

<div class="row">
	<div class="well">
		<ul class="small">
			<li><span class="text-danger">Fields marked with (*) are mandatory.</span></li>
			<li><strong>Only a selected number of characters, letters, numbers and punctuation are accepted by this website.</strong><br/>
				<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal">Show</button> Click here to see the list of characters allowed.</li>
			<li>This website requires the internet browser version IE 5.0 or later with Javascript enabled.</li>
			<li>If Javascript has not been activated you will not be able to complete the form.</li>
		</ul>
	</div>
</div>

<div class="row">
	<form id="main-registration" method="POST" action="<?php echo $this->location('event/save_reg'); ?>" class="form-horizontal">
			
			<div class="well content content-white">
			<h3>Personal Details</h3>
			<div class="form-group">
				<label for="inputTitle" class="control-label col-sm-2" style="text-align:left">Title</label>							
				<div class="col-sm-10">
					<select id="ipt-title" name="ipt-title" class="form-control">
						<option value="Mr">Mr.</option>
						<option value="Miss">Miss.</option>
						<option value="Mrs">Mrs.</option>
						<option value="Dr">Dr.</option>
						<option value="Assoc. Prof">Assoc. Prof</option>
						<option value="Prof">Prof.</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputName" class="control-label col-sm-2" style="text-align:left">Name <small class="text-danger">(*)</small></label>
				<div class="col-sm-10">
				<input type="text" class="form-control" id="ipt-name" name="ipt-name" placeholder="Name" required>
			</div>
			</div>
			
			<div class="form-group">
				<label for="inputAddress" class="control-label col-sm-2" style="text-align:left">Institution <small class="text-danger">(*)</small></label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="ipt-inst" name="inst" placeholder="Institution" required>
			</div>
			</div>
			
			<div class="form-group">
				<label for="inputCity" class="control-label col-sm-2" style="text-align:left">City </label>				
				<div class="col-sm-10">
				<input type="text" class="form-control" id="ipt-city" name="ipt-city" placeholder="City" >
			</div>
			</div>
			<div class="form-group">
				<label for="inputPostal" class="control-label col-sm-2" style="text-align:left">Country <small class="text-danger">(*)</small></label>
				<div class="col-sm-10">
					<select id="ipt-country" name="ipt-country" class="form-control" required>
						<option value="0">Please select</option>
							<?php 
							if(isset($negara)) { 
							foreach($negara as $dt):
								echo "<option value='".$dt->negara."' >".$dt->negara."</option>";
							endforeach;
							}?>										
						
					</select>
			</div>
			</div>
			<div class="form-group">
				<label for="inputTelp" class="control-label col-sm-2" style="text-align:left">Telephone<small class="text-danger">(*)</small></label>
				<div class="col-sm-10">				
				<input type="text" class="form-control" id="ipt-telp" name="ipt-telp" placeholder="Telephone" required>
			</div>			
			</div>			
			<div class="form-group">
				<label for="inputEmail" class="control-label col-sm-2" style="text-align:left">Email <small class="text-danger">(*)</small></label>							
				<div class="col-sm-10">
				<input type="email" class="form-control" id="ipt-email" name="ipt-email" placeholder="Email" required>
				</div>	
			</div>
		</div>
		<input type="hidden" name="tmpid" value="<?php echo $kegiatan_id ?>">
		<input type="hidden" name="jpeserta" value="<?php echo $jenis_peserta_id?>">
		<input type="hidden" name="kid" value="<?php echo $komponen ?>">
		<input type="hidden" name="biaya" value="<?php echo $harga ?>">
		<?php if($komponen && $komponen!="-"): ?>
		<div class="well content content-white">
			<h3>List of Bills</h3>
			<table class="table">
				<tr><td width="40%"><?php echo $nama_biaya ?>&nbsp;<span class="label label-default"><?php echo $jenis_peserta?></span></td><td><b><?php echo strToUpper($satuan).". ".number_format($harga); ?></b></td></tr>
			</table>
		</div>
		<?php endif; ?>
		
		<div class="form-group">
			<div class="col-sm-3">
				<button id="btn-reset" type="button" class="btn btn-danger btn-lg" style="width: 100%; margin-bottom: 20px;" onclick="resetForm()">Clear/Reset Form</button>
			</div>
			<div class="col-md-3 col-md-push-6">
				<button id="btn-submit" type="button submit" class="btn btn-default btn-lg" style="width: 100%; margin-bottom: 20px;">Submit Registration</button>
			</div>
		</div>
	</form>
</div>
</div>
<p>&nbsp;</p>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h3>List of the allowed characters that can be used</h3>
</div>
<div class="modal-body">
<p>! # $ % & ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } </p>
</p>Please note: the character ["], quotation mark, is NOT allowed.</p>
</div>
</div>
</div>
</div>
<script>
function resetForm(){
$(":text").val('').text('');
$("[type='email']").val('').text('');
$(":selected, :checked").removeAttr('checked').removeAttr('selected');
}
$(document).ready(function(){
$('#isAgreement').click(function() {
var checked_status = this.checked;
if (checked_status == true) {
   $("#btn-submit").removeAttr("disabled");
} else {
   $("#btn-submit").attr("disabled", "disabled");
}
});
});
</script>