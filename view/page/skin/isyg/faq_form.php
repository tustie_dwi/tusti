<style type="text/css">
	.isyg-faq .panel-heading{padding:0}.isyg-faq .panel-heading a{padding:10px 5px;display:block;text-decoration:none}.isyg-faq .panel-isyg .media .isyg-head-icon{font-size:1.75em;margin-right:5px}.isyg-faq.panel-group .panel{border-radius:0;margin-bottom:20px;border-bottom-color:#46b8da;background-color:transparent}.isyg-faq .panel-collapse .panel-body{background-color:#fff}
</style>
<?php if($faq_list) { ?>
<div class="panel-group isyg-faq" id="isyg-faq-accordion" role="tablist" aria-multiselectable="true">
	<?php foreach($faq_list as $fl) { ?>
	<div class="panel panel-isyg">
		<div class="panel-heading" role="tab" id="faq-head-<?php echo $fl['id']; ?>">
			<h4 class="panel-title">
				<a role="button" data-toggle="collapse" data-parent="#isyg-faq-accordion" href="#faq-<?php echo $fl['id']; ?>" class="collapsed" aria-expanded="false" aria-controls="faq-<?php echo $fl['id'];; ?>">
					<div class="media">
						<div class="media-left pull-left">
							<?php if($fl['author'] == 'admin_content') { ?>
								<i class="isyg-head-icon fa fa-file-text-o"></i>
							<?php } else { ?>
								<i class="isyg-head-icon fa fa-comments-o"></i>
							<?php } ?>
						</div>
						<div class="media-body">
							<h4 class="media-heading" style="margin-bottom:10px"><?php echo strip_tags($fl['faq_question']); ?></h4>
							<?php if($fl['author'] == 'admin_content') { ?>
								<p class="small text-muted">Written by <strong>Administrator</strong></p>
							<?php } else { ?>
								<p class="small text-muted">Asked by <strong><?php echo $fl['author']; ?></strong></p>
							<?php } ?>
						</div>
					</div>
				</a>
			</h4>
		</div>
		<div id="faq-<?php echo $fl['id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="faq-head-<?php echo $fl['id']; ?>">
			<div class="panel-body">
				<?php echo $fl['faq_answer']; ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php } ?>