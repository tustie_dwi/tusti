  <?php 
  if(!isset($_COOKIE['lang-switch'])){
	  	setcookie('lang-switch','en',time()+3600*24,"/");
  		include("library/en.php");
	  	$data['lang']="en";
  }
  else{
  		include("library/".$_COOKIE['lang-switch'].".php");
	  	$data['lang']=$_COOKIE['lang-switch'];
  }
  ?>
<!DOCTYPE html>
<html lang="<?php if($data['lang']=='en'){echo $data['lang'];}else{echo "id";}?>">
	<head itemscope itemtype="http://schema.org/WebPage">
  <meta charset="utf-8">
 
	<?php 
	if(isset($kategori) && (($kategori=='lecturer') || ($kategori=='staff')|| ($kategori=='dosen')|| ($kategori=='staff-kependidikan'))){
		if(isset($posts) && $posts):?>
			<title itemprop="name"><?php echo $posts->nama ." | ". $this->page_title(); ?></title>
			<meta property="og:title" content="<?php if($posts->nama) echo $posts->nama; else echo "About"; ?>">
			<?php
		endif;
	}else{ 

		if(isset($detail) && !isset($unit_list)){?>
		<title itemprop="name"><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; echo " | "; echo $this->page_title(); ?></title>
		<meta property="og:title" content="<?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; ?>">
		<?php }else if(isset($unit_list)){?>
			<?php if(isset($detail)){?>
					<title><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; echo " | "; echo $unit_list->unit; ?> PTIIK</title>
					<meta property="og:title" content="<?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; ?>">
			<?php }else{ ?>
					<title><?php echo $unit_list->unit;echo " | ";echo $this->page_title(); ?></title>
					<meta property="og:title" content="<?php echo $unit_list->unit;echo " | ";echo $this->page_title(); ?>">
			<?php } ?>
		<?php }else{ ?>
		<title itemprop="name"><?php echo $this->page_title(); ?></title>
		<meta property="og:title" content="<?php echo $this->page_title(); ?>">
		<?php }	
	}
	?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Official Site - <?php echo $this->page_title(); ?> Universitas Brawijaya">
	<meta name="keywords" content="<?php echo $this->config->page_keywords; ?>">
    <meta name="author" content="BPTIK">
	<meta name="robots" content="index, follow" />
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="cleartype" content="on">

    
    <meta property="og:site_name" content="<?php echo $this->page_title(); ?>">
    <meta property="og:url" content="<?php echo full_url_path();?>">
   	<?php if(isset($unit_list)&&$unit_list->logo!=""){ ?>
    <meta property="og:image" content="<?php echo $this->config->file_url_view."/".$unit_list->logo;?>">
    <?php }else{?>
    	
    <meta property="og:image" content="<?php echo $this->asset("ptiik/images/ptiik.png");?>">
    	<?php } ?>
    <meta property="og:description" content="Official Site - <?php echo $this->page_title(); ?> Universitas Brawijaya">
    <meta http-equiv="cache-control" content="no-cache">
	<meta name="rating" content="Mature" />
 	
	<link href="<?php 
			if(! isset($unit_list)):				
				echo $this->asset("skin/ub/css/base.orange.min.css"); 
			else:
				if((($unit_list->kategori=='laboratorium')||(isset($kategori)=='laboratorium')) && $unit_list->kategori!='prodi' && $unit_list->kategori!='riset' ):
					if(isset($_COOKIE['stylecolor'])):
						echo $this->asset("skin/ub/css/base.".$_COOKIE['stylecolor'].".min.css"); 
					else:
						echo $this->asset("skin/ub/css/base.green.min.css"); 
					endif;
				else:
					if(isset($_COOKIE['stylecolor'])):
						echo $this->asset("skin/ub/css/base.".$_COOKIE['stylecolor'].".min.css"); 
					else:
						echo $this->asset("skin/ub/css/base.blue.min.css"); 
					endif;
				endif;
			endif;
			?>" rel="stylesheet">
	<!--<link href="<?php //echo $this->asset("ptiik/css/base.orange.min.css"); ?>" rel="stylesheet">-->
	
	
	<link href="<?php echo $this->asset("ptiik/css/s2stylemore.min.css"); ?>" rel="stylesheet">
	<link href="<?php echo $this->asset("ptiik/css/insideptiik.slider.min.css"); ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $this->asset('admin/css/plugins/morris.css')  ?>">
	<!-- Edit 1 -->
	<link href="<?php echo $this->asset("ptiik/css/jquery.fancybox.css"); ?>" rel="stylesheet">
	<!-- End Edit 1 -->
	
	
	 <?php $styles = $this->get_styles(); 
        if(is_array( $styles )) : 
    	
		foreach($styles as $s) : 
	?><link href="<?php echo $this->asset($s); ?>" rel="stylesheet">
	<?php endforeach; endif; ?>
	<?php if( isset($mstyles) and is_array($mstyles)) { 
	foreach( $mstyles as $s) : ?><link href="<?php echo $this->location($s); ?>" rel="stylesheet">
	<?php endforeach; } ?>

    <!-- Le fav and touch icons -->
   <link href="<?php echo $this->location('rss/feed'); ?>" title="Program Teknologi Informasi dan Ilmu Komputer (PTIIK) Universitas Brawijaya RSS Feed" type="application/rss+xml" rel="alternate"></link>
    
	<link rel="shortcut icon" href="<?php echo $this->asset("images/favicon.ico"); ?>"/>
    <!-- Le base URL helper for javascript -->
    <script type="text/javascript">
		var base_url = '<?php if(isset($url)) echo $this->location($url); 
		else echo $this->location(); ?>';
		var base_apps = '<?php echo $this->location(); ?>';
		var file_url = '<?php echo $this->config->file_url_view; ?>';
    </script>   
	<style>
		audio, canvas, progress, video { display: inline-block;  vertical-align: baseline;}
	</style>
	
	
  </head>
 <body>
 	<canvas id="the-canvas" style="z-index:-1;position:fixed"></canvas>
	 <div class="container" style="z-index:1">
		    <header id="header" class="hide-from-print">
            <section class="nav-top">
                <div class="container">
					 <div class="nav-header">
						
                        <a class="nav-brand nav-toggle collapsed" data-toggle="collapse" data-target="#nav-top-collapse" href="#">
                            <span class="fa fa-cog"></span> Action
                        </a>
                    </div>
                    <div class="nav-top-collapse" id="nav-top-collapse">
                        <ul class="nav-top-nav nav-left">

											<li><a href="http://ub.ac.id" target="_blank">UB Official</a></li>
											<li><a href="http://bits.ub.ac.id" target="_blank">BITS</a></li>
											<li><a href="http://mail.ub.ac.id" target="_blank">Webmail</a></li>
											<li><a href="http://prasetya.ub.ac.id" target="_blank">UB News</a></li>
                        </ul>
						
                         
                        <ul class="nav-top-nav nav-right">
							<li style="font-size:12px;padding:3.5px 2px;border-right: 2px solid rgba(0, 0, 0, 0.1);box-shadow:1px 0 0 rgba(255, 255, 255, 0.1);">
								
								 <form id="form-search" method="POST" action="<?php 
									if(isset($unit_list)):
										echo $this->location($url.'/search');
									else:
										echo $this->location('page/search');
									endif; ?>" class="nav-form-nav nav-top-nav nav-right">
									
										<input id="searchTxt" onkeyup="return cari_konten(event)" name="searchTxt" type="text" class="form-control" placeholder="Type here to search.." value="">
										<span id="searchBg"></span>									
										<button id="searchIcon" name="searchIcon" type="submit" class="search-button with-tooltip" data-toggle="tooltip" data-placement="bottom" title="Search" ><a href="#" class="font-red"><span class="fa fa-search"></span></a></button>
								
									</form>
								
							</li>
							 <li><a href="#" class="font-red lang-switch with-tooltip" data-toggle="tooltip" data-placement="bottom" title="Bahasa Indonesia" data-lang="in">ID</a></li>
                            <li><a href="#" class="font-red lang-switch with-tooltip" data-toggle="tooltip" data-placement="bottom" title="English" data-lang="en">EN</a></li>
                            <li>
                                <a href="<?php echo $this->location('auth'); ?>" class="with-tooltip" data-toggle="tooltip" data-placement="bottom" title="Sign In"> <span class="fa fa-unlock-alt"></span> Sign In</a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </section>
            <section class="top-header <?php if(isset($view_slide)) echo "with-bg"; ?>">
                <div class="container-fluid">
                    <div class="row">
						<?php 
						if(isset($unit_list)): 
							if($unit_list->logo) $img = $this->config->file_url_view."/".$unit_list->logo;
							else $img = $this->config->default_thumb_web;
						?>
								<div class="col-md-6">
									<a href="<?php echo $this->location($url); ?>" class="image-link">
										<img class="img-logo-header img-responsive" src="<?php echo $img; //$mimg->base64_image($img);?>">
									</a>
								</div>
								 <div class="col-md-6">
									<div class="navigation-top-header">
										<a href="<?php echo $this->config->web_base_url; ?>" class="image-link">
											<img class="img-logo-header img-responsive" src="<?php echo $this->asset("ptiik/images/filkom.png");?>">
										</a>
									</div>
								</div>
						<?php else:?>
							<div class="col-md-6">
									<a href="<?php echo $this->location(); ?>" class="image-link">
										<img class="img-logo-header img-responsive" src="<?php echo $this->asset("ptiik/images/filkom.png"); ?>">
									</a>
								</div>
								<div class="col-md-6">
								
									<div class="navigation-top-header">
										<ul class="navigation-menu">
										</ul>
									</div>
									
								</div>
						<?php endif;?>
                    </div>
                </div>
            </section>
            <nav class="navbar navbar-default navbar-static-top margin-bottom-no">
					  <div class="container-fluid">					   
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      
					      <a href="javascript::" class="navbar-toggle-menu collapsed navbar-brand navbar-brand-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					      	<span>Menu</span><img alt="Brand" src="<?php echo $this->asset("skin/ub/images/ptiik-white.png");?>">
					      </a>					    
					    </div>
					
					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
					      <ul class="nav navbar-nav" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
							<?php if(isset($unit_list)): ?>
							<li><a itemprop="url" href="<?php echo $this->location($url); ?>" title="Home"><span class="fa fa-home"></span> <span class="sr-only" itemprop='name'>Home</span></a></li>
							<?php else: ?>
                            <li><a itemprop="url" href="<?php echo $this->location(); ?>" title="Home"><span class="fa fa-home"></span> <span class="sr-only" itemprop='name'>Home</span></a></li>
							<?php
							endif;
							if($page):
							
							
								foreach($page as $dt):
									$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
									if($child):
										?>
										  <li class="dropdown">
												<a rel="alternate" hreflang="x-default" href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" ><?php echo $dt->content_title?></a>
												<?php
													$i=0;
													 $count=round(count($child)/2);
													 $parent=0;
												?>		
												
												 <ul class="dropdown-menu" role="menu">														
													 <?php 													 
													 foreach ($child as $key):
														 $i++;
														if(isset($unit_list)):
															read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location($url) ,$key->content_data);
														else:
															read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location('page'),$key->content_data );
														endif;
													 endforeach; ?>
												</ul>
												
										</li>
										<?php
									else:
										if($dt->content_data && $dt->content_title=='PTIIK Apps'):
											echo "<li><a rel='alternate' hreflang='x-default' href=".$dt->content_data." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
										else:
											if(isset($unit_list)):
												echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location($url.'/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
											else:
												echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location('page/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
											endif;
										endif;
									endif;

								endforeach;
								
								
								
							endif;
							
							if(isset($unit_list) && ($unit_list->kategori=="laboratorium")): 
							?>
								<li class="dropdown">
									<a rel="alternate" hreflang="x-default" href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" ><?php echo praktikum ?></a>									
								 <ul class="dropdown-menu" role="menu">				
									<?php
										echo "<li><a itemprop='url' rel='alternate' hreflang='x-default' href=".$this->location(strtolower($url).'/asisten')."><span itemprop='name'>".asisten_lab."</span></a></li>";
										echo "<li><a itemprop='url' rel='alternate' hreflang='x-default' href=".$this->location(strtolower($url).'/jadwal')."><span itemprop='name'>".jadwal_praktikum."</span></a></li>";
										echo "<li><a itemprop='url' rel='alternate' hreflang='x-default' href=".$this->location(strtolower($url).'/nilai')."><span itemprop='name'>".nilai_praktikum."</span></a></li>";
										?>
									</ul>
									
								</li>
								<li>
									<a rel="alternate" hreflang="x-default" href="<?php echo $this->location(strtolower($url).'/skripsi')?>" ><?php echo skripsi ?></a>							
								
								</li>
								<?php
							endif;
						?>
					      </ul>
					    </div><!-- /.navbar-collapse -->
						
					  </div><!-- /.container-fluid -->
					</nav>
			<?php if(! isset($view_slide)): ?>
		
            <div class="slide">
                <div  id="slider" class="owl-carousel" width="970px">
                  	<?php
					if(isset($slide) && ($slide)):
						foreach($slide as $dt):
							
						?>
						 <div class="item">
								<a href="<?php if($dt->file_data) echo $dt->file_data; 
								else echo "#";
								?>"><img src="<?php echo $this->config->file_url_view.'/'.$dt->file_loc;?>" alt="...">
								<div class="caption">									
									<h1 class="title"><?php $dt->file_title?></h1>
									<div class="detail">
										<?php $dt->file_note?>
									</div>
								</div>
								</a>
							</div>
						<?php
						endforeach;
					else:
					?>
						 <div class="item">
							<img src="<?php echo "http://file.filkom.ub.ac.id/fileupload/assets/upload/file/PTIIK/konten/slider/2014-10/darkblackpatern.png";?>" alt="...">
							<div class="caption">
							</div>
						</div>
						
					<?php
					endif;
					?>
                  
                   
                </div>
                <div class="slide-navigation">
                    <a href="#" class="owl-prev slide-prev"><span class="fa fa-chevron-left"></span></a>
                    <a href="#" class="owl-next slide-next"><span class="fa fa-chevron-right"></span></a>
                </div>
            </div>
<?php 
			endif;
			
    		function read_sub_menu($i,$count,$parent,$unit=NULL, $lang=NULL, $mpage=NULL, $id=NULL, $title=NULL, $content_page=NULL,$url_menu=NULL){
												
					$child = $mpage->read_sub_content($unit, $lang, "", $id, $title);
				
					if($child):
						?>
						
						 <li class="menu-item dropdown dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $title?></a>
							
								<ul class="dropdown-menu">
										 <?php 
										 $parent=1;
										 foreach ($child as $key):
												read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id,$key->content_title,$key->content_page, $url_menu);
										 endforeach; ?>
								</ul>
						</li>
						
						<?php
					else:
					
						if(isset($unit_list)):
							echo "<li><a itemprop='url' href=".$this->location($url.'/read/'.$content_page.'/'.$id)." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
						else:
							echo "<li><a itemprop='url' href=".$url_menu.'/read/'.$content_page.'/'.$id." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
						endif;
					
					endif;
			}

function full_url_path()
{
    $s = &$_SERVER;
    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
    $sp = strtolower($s['SERVER_PROTOCOL']);
    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
    $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
    $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
    $uri = $protocol . '://' . $host . $s['REQUEST_URI'];
    $segments = explode('?', $uri, 2);
    $url = $segments[0];
    return $url;
}
    ?>
        </header>
		
        <section id="wrapper">
		