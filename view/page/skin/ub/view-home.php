

<div class="menu-navigation">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2 title-menu font-raleway uppercase">
					<a href="<?php echo $this->location('info/services');?>" class="more-service">
						<div class="temu"><?php echo temu_layanan ?></div>
						<div class="layanan"><?php echo layananmu ?></div>
					</a>
				</div>
				<div class="col-md-10 content-slide">
					<div class="row">
						<div class="col-xs-10">
							<div class="menu-navigation-slider">
							<?php
							$apps = $mpage->read_content('0',$lang,'apps');
							
							if($apps):
							if(! isset($unit_list)):
								$strcolor="orange";
							else:
								if(isset($_COOKIE['stylecolor'])) $strcolor=$_COOKIE['stylecolor'];
								else $strcolor=$color;
							endif;
							
								foreach($apps as $dt):
									
									$str=explode('/',$dt->icon);
									$img_name = end($str);
									if($img_name) $img= $this->asset("ptiik/images/".$strcolor."/".$img_name);
									else $img= $this->asset("ptiik/images/".$strcolor."/akademik.png");
									?>
									<div class="slide">
										<a href="<?php echo $this->location($dt->content_data);?>" class="menu-navigation-link with-tooltip" data-toggle="tooltip" data-placement="top" title="<?php if($dt->keterangan) echo $dt->keterangan; ?>">
											<span class="menu-navigation-detail"><i class="fa fa-th-large" style="font-color:#ddd"></i> <?php echo $dt->judul ?></span>
										</a>
									</div>
									<?php
								endforeach;
							endif;
							?>
														   
							</div>
						</div>
						<div class="col-xs-2">
							<a href="#" class="menu-navigation-next">
								<span class="fa fa-chevron-right"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<section class="content content-white">
		<div class="container-fluid">
			<section itemscope="" itemtype="http://schema.org/Article">
				<section itemprop="articleBody">
					<div class="col-md-12">
						<?php if($about): 
							foreach($about as $dt):
							?>
							<h1 class="title-content text-center font-orange margin-top-no margin-bottom-no font-raleway"><?php echo $dt->judul ?></h1>
							<?php
							if(isset($unit_list)) $str_= "text-justify";
							else $str_= "text-center";
							?>
							<div class="margin-top-no <?php echo $str_; ?>">
							<?php echo $mpage->str_content($dt->keterangan,$this->config->file_url_view); ?></div>
							<!--<hr class="hr-orange hr-center margin-top">-->
							<?php 
							endforeach;
						else:
							if(isset($unit_list)):
								?>
								<h1 class="title-content text-center font-orange"><?php if($unit_list->unit) echo $unit_list->unit;
								else echo $unit_list->unit_ori;
								?></h1>
								<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet eros in leo pellentesque vestibulum. Pellentesque sed mi sed libero ultricies tincidunt ac vel lacus. Curabitur a eros finibus, ultricies quam ac, vulputate dolor. Donec augue ligula, scelerisque eu dictum vitae, tincidunt a felis. Nam at mollis leo, vel elementum velit. Donec accumsan ex mi, sit amet consectetur orci rhoncus euismod. In hac habitasse platea dictumst. Donec at ex nisi. Praesent vitae varius sem. Maecenas volutpat pretium dolor, a posuere ex commodo non.</p>
								<?php
							else:
							?>
								<h1 class="title-content text-center font-orange"><?php echo ptiik;
								?></h1>
								<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet eros in leo pellentesque vestibulum. Pellentesque sed mi sed libero ultricies tincidunt ac vel lacus. Curabitur a eros finibus, ultricies quam ac, vulputate dolor. Donec augue ligula, scelerisque eu dictum vitae, tincidunt a felis. Nam at mollis leo, vel elementum velit. Donec accumsan ex mi, sit amet consectetur orci rhoncus euismod. In hac habitasse platea dictumst. Donec at ex nisi. Praesent vitae varius sem. Maecenas volutpat pretium dolor, a posuere ex commodo non.</p>
							<?php
							endif;
						endif;  ?>
					</div>
				</section>
			</section>
		</div>
	</section>
			
			<section class="content content-white" style='padding:13px'>
				 <div class="container-fluid">
					
	               	<div class="row">
						<!-- start -->
						<div class="col-md-6 col-md-push-6">
	               			<div class="row">
							<?php if(! isset($unit_list)) { ?>
	               				<div class="col-md-12">
									<section itemscope="" itemtype="http://schema.org/Article">
										<section itemprop="articleBody">
											<div class="cards-panel">
												<div class="cards-header">
													<h2 class="cards-header-title cards-header-title-background font-raleway uppercase"><?php if($lang=='in') echo "Program Studi"; else echo "Department/Study Program";?> <a href="<?php echo $this->location('rss/feed/prodi'); ?>" class="feed pull-right" target="_blank" style="color:#fff"><span class="fa fa-rss"></span></a></h2>
												</div>
												<div class="cards-body">					               		
													<div class="news-media">
														<?php
												if($prodi):
													$i=0;
													foreach($prodi as $dt):
														$i++;
														if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
														else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));

														if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
														else $imgthumb = $this->config->default_thumb_web;	

														
														$strabout = $dt->about;
													?>	
														<a class="news-media-link news-media-simple" href="<?php if($dt->host_name) echo $dt->host_name; 
														else echo $this->location('unit/prodi/'.strtolower($dt->kode)); ?>" itemprop="url" rel="alternate" hreflang="x-default" style="color:#333"><h5 class="news-media-title margin-top-no margin-bottom-no"><?php if($dt->unit) echo ucWords($dt->unit); else echo ucWords($dt->unit_ori);?></h5>
														</a>									
													<?php
													//}
													endforeach;								
												endif;
												?>	
																						
													</div>
												</div>
											</div>
										</section>
									</section>
			               		</div>
			               		<?php } ?>
			               		<div class="col-md-6">
									<section itemscope="" itemtype="http://schema.org/Article">
										<section itemprop="articleBody">
											<div class="cards-panel">
												<div class="cards-header">
													<h2 class="cards-header-title cards-header-title-background font-raleway uppercase"><?php echo pengumuman ?> <a href="<?php echo $this->location('rss/feed/pengumuman'); ?>" class="feed pull-right" target="_blank" style="color:#fff"><span class="fa fa-rss"></span></a></h2>
												</div>
												<div class="cards-body">
													<div class="news-media"><?php
													if($pengumuman):
														foreach($pengumuman as $dt):
															$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
																											
															if (isset($url)) $link = $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id);
															else $link = $this->location('page/read/pengumuman/'.$content.'/'.$dt->id);
															
															$imgthumb = $this->asset("s2/images/event-icon.png");
														?>
														
														<a class="news-media-link news-media-simple" itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $link; ?>" >
														<h5 class="news-media-title margin-top-no margin-bottom-no"><?php 
														echo "<small>";
															echo '<time datetime="'.date("Y-m-d H:i", strtotime($dt->content_modified)).'">';
															if($lang=='in') echo date("Y-m-d H:i", strtotime($dt->content_modified));
															else echo date("M d, Y H:i", strtotime($dt->content_modified));
															echo " <label>";
															if($dt->unit_note) echo $dt->kode_unit;	else echo "FILKOM UB";
															echo "</label></small><br>";
														if($dt->judul) echo $dt->judul;	else echo $dt->judul_ori;?></h5></a>
														<?php
														endforeach;
												
													endif;
													?>
													</div>
												</div>
												<div class="cards-footer text-right">
													<a href="<?php if (isset($url)) echo $this->location($url.'/read/pengumuman/'); else echo $this->location('page/read/pengumuman/'); ?>">More <?php echo pengumuman ?></a>
													
												</div>
											</div>
										</section>
									</section>
			               		</div>
								<div class="col-md-6" >
									<section itemscope="" itemtype="http://schema.org/Article">
										<section itemprop="articleBody">
										<div class="cards-panel">
											<div class="cards-header">
												<h2 class="cards-header-title cards-header-title-background font-raleway uppercase"><?php echo kegiatan ?> <a href="<?php echo $this->location('rss/feed/event'); ?>" class="feed pull-right" target="_blank" style="color:#fff"><span class="fa fa-rss"></span></a></h2>
											</div>
											<div class="cards-body">
												<div class="news-media"><?php
												if($event):
													foreach($event as $dt):
														$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
																									
														if (isset($url)) $link = $this->location($url.'/read/event/'.$content.'/'.$dt->id);
														else $link = $this->location('page/read/event/'.$content.'/'.$dt->id);
														$imgthumb = $this->asset("s2/images/event-icon.png");
													?>
													
													<a class="news-media-link news-media-simple" itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $link; ?>" >
													<h5 class="news-media-title margin-top-no margin-bottom-no"><small><?php 
													if($lang=='in'):
														$hari = $mpage->get_hari(date("N", strtotime($dt->content_modified)));
														echo $hari.", ".date("d M, Y H:i", strtotime($dt->content_modified)); 
													else: 
														echo date("D", strtotime($dt->content_modified)).", ".date("M d, Y H:i", strtotime($dt->content_modified));  
													endif; 
													echo "</small><br>";
													
													if($dt->judul) echo $dt->judul;	else echo $dt->judul_ori;?></h5></a>
													<?php 
													endforeach;
											
												endif;
											?></div>
											</div>
											<div class="cards-footer text-right">
												<a href="<?php if (isset($url)) echo $this->location($url.'/read/event/'); else echo $this->location('page/read/event'); ?>">More <?php echo kegiatan ?></a>
											</div>
									   </div>
									 </section>
									</section>
								</div>
	               			</div>
	               		</div>
	               		
	               		<div class="col-md-6 col-md-pull-6">
							<section itemscope="" itemtype="http://schema.org/Article">
								<section itemprop="articleBody">
								<div class="cards-panel">
									<div class="cards-header">
										<h2 class="cards-header-title cards-header-title-background font-raleway uppercase"><?php echo berita ?> <a href="<?php echo $this->location('rss/feed/news'); ?>" class="feed pull-right" target="_blank" style="color:#fff"><span class="fa fa-rss"></span></a></h2>
										
									</div>
									<div class="cards-body">
										<div class="news-media">
										
											<?php
											if(isset($news) && ($news) ){
												$i=0;
												foreach ($news as $dt):
													$i++;
													$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
													
													if (isset($url)) $link = $this->location($url.'/read/news/'.$content.'/'.$dt->id);
													else $link = $this->location('page/read/news/'.$content.'/'.$dt->id);
													
													if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
													else $imgthumb = $this->config->default_thumb_web;
													if($i<=2):								
													?>
												<a class="news-media-link news-media-link-detail" href="<?php echo $link; ?>" itemprop="url" rel="alternate" hreflang="x-default">
												<div class="row">
													<div class="col-xs-4 col-sm-4">
														<figure>
															<img class="news-media-img img-responsive img-responsive-center" src="<?php echo $imgthumb; ?>" alt="<?php echo $mpage->content($dt->judul,5);?>">
														</figure>
													</div>
													<div class="col-xs-8 col-sm-8">
														<h4 class="news-media-title margin-top-no margin-bottom-no"><?php echo $mpage->content($dt->judul,5);?></h5>
														<div class="news-media-content"><?php echo $mpage->content($dt->keterangan,20);?>
														</div>
													</div>
												</div>
											</a>
													<hr class="divider">
												<?php
													else:
														if($i < 8):
												?>
													<a class="news-media-link news-media-simple" href="<?php echo $link; ?>" itemprop="url" rel="alternate" hreflang="x-default">
														<h5 class="news-media-title margin-top-no margin-bottom-no"><?php echo $mpage->content($dt->judul,15);?></h5>
													</a>
												<?php
														endif;
													endif;
												endforeach;
											}
											?>			               			
											
										</div>
									</div>
									<div class="cards-footer text-right">
										<a href="<?php if (isset($url)) echo $this->location($url.'/read/news/'); else echo $this->location('page/read/news/'); ?>">More <?php echo berita ?></a>
									</div>
							   </div>
							</section></section>
	               		</div>
						<!-- end -->
					</div>
				</div>
			</section>
			 
			 
			 			
             <section class="content our-partnership" >
                <div class="layer">
                    <div class="container-fluid">
                        <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange font-raleway uppercase margin-top-no margin-bottom-sm"><span><?php echo partner_kerjasama ?></span></h2>
                        <div class="row text-center partnership">
							<?php
							if(isset($kerjasama) && $kerjasama){
								foreach($kerjasama  as $dt):
									if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
									else $imgthumb = $this->config->default_thumb_web;
									
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
									
									
									if (isset($url)) $link = $this->location($url.'/read/kerjasama/'.$content.'/'.$dt->id);
									else $link = $this->location('page/read/kerjasama/'.$content.'/'.$dt->id);
									?>
									<div class="col-md-2 col-xs-4"><a href="<?php echo $link; ?>" ><img src="<?php echo $imgthumb; ?>" class="img-responsive img-responsive-center" alt="GDP Logo"></a></div>
									<?php
								endforeach;
							}else{
							?>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/gdp.png"); //$mimg->base64_image($this->asset("ptiik/images/gdp.png")); ?>" class="img-responsive img-responsive-center" alt="GDP Logo"><?php echo count($kerjasama)?></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/nokia.png"); //$mimg->base64_image($this->asset("ptiik/images/nokia.png")); ?>" class="img-responsive img-responsive-center" alt="Nokia Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ibm.png"); //$mimg->base64_image($this->asset("ptiik/images/ibm.png")); ?>" class="img-responsive img-responsive-center" alt="IBM Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/cisco.png"); //$mimg->base64_image($this->asset("ptiik/images/cisco.png")); ?>" class="img-responsive img-responsive-center" alt="Cisco Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/oracle.png"); //$mimg->base64_image($this->asset("ptiik/images/oracle.png")); ?>" class="img-responsive img-responsive-center" alt="Oracle Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ni.png"); //$mimg->base64_image($this->asset("ptiik/images/ni.png")); ?>" class="img-responsive img-responsive-center" alt="NI Logo"></div>
							<?php } ?>
                        </div>
                        </div>
                    </div>
                </div>
            </section>

