 </section>
<footer id="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter"  <?php if(isset($kategori) && ($kategori=='gallery' || $kategori=='video')) echo "class='margin-top-lg'"; ?>>
            <section class="footer-top" class="hide-from-print">
                <div class="container">
                    <div class="col-md-8">
                        <h4 class="title-footer title-underline title-underline-black font-orange"><span><?php echo ptiik_link ?></span></h4>
                        <div class="row">
							<?php
							if($footer):
								foreach($footer as $dt):
									if(isset($unit_list)) $unit=0;
									else $unit = $unit;
									
									$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
									?>
									 <div class="col-md-3 col-sm-6">
									 <h4><?php echo $dt->content_title?></h4>
										<?php
										if($child):
											?>									
											 <ul class="list-unstyled list-footer">										
												 <?php 
												 foreach ($child as $key):
													//read_sub_footer($unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location() );?>
													 <li><a href="<?php echo $key->content_data?>"><?php echo $key->content_title?></a></li>
													<?php
												 endforeach; ?>
											 </ul>
													
											<?php
										else:
											echo "<li><a href=".$dt->content_data.">".$dt->content_title."</a></li>";
										endif;
									?>
									</div>
									<?php
								endforeach;
							endif;
							?>						
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 class="title-footer title-underline title-underline-black font-orange"><span><?php echo ptiik_tweet ?></span></h4>
                        <ul class="list-unstyled list-twitter">
							<?php
							$tweet = $mpage->read_tweet(3);
							if($tweet):
								foreach($tweet as $dt):
								?>
								 <li>
										<span class="fa fa-twitter"></span>
										<div class="twitter-content">
											<a href="//twitter.com/filkomUB" target="_blank">@FILKOMUB</a> <?php echo $dt->keterangan ?>
										</div>
										<time><?php //echo date("M d, Y h:i", strtotime($dt->created_at)); ?></time>
									</li>
								<?php
								endforeach;
							endif;
							?>                            
                        </ul>
                    </div>
					<div class="col-md-12">
						<small>Follow us on</small>						
							<a href="https://www.facebook.com/PTIIKUB" class="facebook" target="_blank"><span class="fa fa-facebook"></span></a>&nbsp;
							<a href="https://twitter.com/filkomUB" class="twitter" target="_blank"><span class="fa fa-twitter"></span></a>&nbsp;
							<a href="<?php echo $this->location('rss/feed'); ?>" class="feed" target="_blank"><span class="fa fa-rss"></span></a>					
					</div>
                </div>
            </section>
            <section class="footer-bottom">
                <div class="container">
                    <div class="col-sm-4"><img src="<?php echo $this->asset("skin/ub/images/ptiik-filkom-ub.png");  ?>" class="img-responsive img-responsive-center" width="50%"></div>
                    <div class="col-sm-8 text-right" style="font-size:90%">
                         <?php echo ptiik_alamat ?>
                         <div class="sr-only">Copyright &copy; <span itemprop="copyrightYear">2014</span> <span itemprop="copyrightHolder">BPTIK</span></div>
                    </div>
                </div>
            </section>
        </footer>
        <?php //if(isset($unit_list)): ?>
        <div class="tools tools-color"> 
            <a href="#" class="show-tools fa fa-cogs"></a> 
            <div class="inner-tools"> 
                <div class="chang-style"> 
                   				
					<h5>Pick your favourite skin</h5> 
                    <ul class="skins"> 
                       <li><a href="#" class="blue<?php if((isset($_COOKIE['skin-switch']) && $_COOKIE['skin-switch']=="ub")||(!isset($_COOKIE['skin-switch']))){echo " selected";}?>" data-styleskin="ub"><img src="<?php echo $this->asset("ptiik/images/ub-style.jpg");  ?>" class="img-responsive img-responsive-center"></a></li> 
					    <li><a href="#" class="orange<?php if((isset($_COOKIE['skin-switch']) && $_COOKIE['skin-switch']=="ptiik")){echo " selected";}?>" data-styleskin="ptiik"><img src="<?php echo $this->asset("ptiik/images/ptiik-style.jpg");  ?>" class="img-responsive img-responsive-center"></a></li> 
                    </ul> 
                    <div>
                        <a href="#" class="clear-skin">set to default</a>
                    </div>
                    <div class="clearfix"></div>
                </div> 
            </div> 
        </div>
		<?php //endif; ?>
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo $this->asset("ptiik/js/jquery-1.11.1.min.js"); ?>"></script>		
		  <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $this->asset("ptiik/js/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo $this->asset("ptiik/js/owl.carousel.min.js"); ?>"></script>

    <script src="<?php echo $this->asset("ptiik/js/jquery.fancybox.pack.js"); ?>"></script>
	<script src="<?php echo $this->asset("ptiik/js/helpers/jquery.fancybox-media.js?v=1.0.6"); ?>"></script>
	  
    <script src="<?php echo $this->asset("ptiik/js/jquery.mixitup.min.js"); ?>"></script>

	<script src="<?php echo $this->asset("js/jquery/jquery.cookie.js"); ?>"></script>
	<script src="<?php echo $this->asset("js/search.js"); ?>"></script>

	
	<?php $scripts = $this->get_scripts(); 
	foreach( $scripts as $s) : ?><script src="<?php echo $this->asset($s); ?>"></script>
	<?php endforeach; ?>
	<?php if( isset($mscripts) and is_array($mscripts)) { 
	foreach( $mscripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
	<?php endforeach; } ?>
       
	<script>
       document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
           //alert('No F-12');
            return false;
        }
    }
    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
    document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
	var message="";
	function clickIE() {if (document.all) {(message);return false;}} function clickNS(e) {if (document.layers||(document.getElementById&&!document.all)) { if (e.which==2||e.which==3) {(message);return false;}}} if (document.layers) {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;} else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;} document.oncontextmenu=new Function("return false") 
	</script>
     <script>
		function search(e) {
			var nilai = $("#searchTxt").val();	
		    if (e.keyCode == 13 && nilai != '') {
		    	// console.log(nilai);
		    	search_link(nilai);
		        return false;
		    }
		}
		
		function search_link(){
			document.getElementById("main-search-form").submit();
		}
		
		$(document).ready(function() {
			$(".with-tooltip").tooltip({trigger: "hover focus", container: "body"});
				if ($(".menu-navigation-slider").length) {
					var $menunav = $('.menu-navigation-slider').owlCarousel({
							items: 3,
							autoPlay: false,
							pagination: false,
							theme: "slider-theme"
						});
					$(".menu-navigation-next").click(function (e) {
						e.preventDefault();
						$menunav.trigger('owl.next');
					});
				}

				if ($("#slider").length) {
					var $owl = $("#slider")
					$owl.owlCarousel({
						// navigation : true,
						singleItem: true,
						transitionStyle: "fade",
						autoPlay: true,
						stopOnHover: true,
						pagination: false,
						autoHeight: true,
						theme: "slider-top-theme"
					});
					$(".slide-next").click(function (e) {
						e.preventDefault();
						$owl.trigger('owl.next');
					});
					$(".slide-prev").click(function (e) {
						e.preventDefault();
						$owl.trigger('owl.prev');
					});
				}
				$('.search-button').click(function (e) {
					e.preventDefault();
					$('.nav-form-nav').addClass('focus').delay(100);
					$("input[name=searchTxt]").focus();
				});

				$("input[name=searchTxt]").focus(function () {
					$('.nav-form-nav').addClass('focus');
				});

				$("input[name=searchTxt]").blur(function () {
					$('.nav-form-nav').removeClass('focus');
				});
				$('.nav-main').affix({
					offset: {
						top: $(".nav-main").position().top
					}
				});
				if ($("#insideptiik").length) {
					var owl = $("#insideptiik");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						theme: "slider-theme"
					});
				}
				if ($("#insidelabslide").length) {
					var owl = $("#insidelabslide");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						theme: "slider-theme"
					});
				}
				if ($(".laboratory-list-slide").length) {
					var owl = $(".laboratory-list-slide");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						autoHeight: true,
						theme: "slider-theme"
					});
				}
				
				 if ($(".mixit-content").length){
					if(typeof $typemix !== 'undefined' && $typemix=="profile"){
						$.each($abjadmix,function(e,f){
							$(".mixit-content").append('<div class="'+classAbjadText+' '+f+'"><h4>'+f+'</h4></div>');
						});
						$.each($abjadmix,function(e,f){
							  		$(".isi-kategori."+f).insertAfter(".abjad-kategori."+f);
							  });
					}
				}
				
				 if ($(".mixit-content").length) {
					var mix = $(".mixit-content");

					mix.mixItUp({
						
    					multiFilter: true,
    					controls: {
							live: true
						},
						animation: {
							effects: 'fade stagger(10ms) scale',
							staggerSequence: function(i){
								return i % 4;
							}
						},
						callbacks: {
						onMixEnd: function(state){
							state.$show.addClass("activeshow");
							state.$hide.removeClass("activeshow");
							if ( typeof $abjadmix !== 'undefined' && typeof $typemix !== 'undefined' && $typemix=="profile") {
							  $.each($abjadmix,function(e,f){
							  	count = $(".activeshow.isi-kategori."+f).length;
							  	if(count<1){
							  		$(".abjad-kategori."+f).slideUp(500);
							  	}
							  });
							}
						}	
						}
					});
				}
				
				if ($(".fancybox").length) {
					$(".fancybox")
					.attr('rel', 'gallery')
					.fancybox({
						helpers : 	{
										media : {},
										 title: {
											type: 'inside',
											position: 'bottom'
										}
									},
						beforeShow: function () {
						
							$.fancybox.wrap.bind("contextmenu", function (e) {
									return false; 
							});
						}
					});
				}
				
				$('.tools-color .show-tools').click(function (e) {
					e.preventDefault();
					if ($('.tools-color').hasClass('active')) {
						$('.tools-color').css('right', '').removeClass('active');
					} else {
						$('.tools-color').css('right', '0').addClass('active');
					}
				});
				
				$('.tools .skins a').click(function (e) {
					e.preventDefault();
					var data = $(this).data('styleskin');
					var name = "skin-switch";
					  $.cookie(name, data, { expires: 365 ,path:"/"});
					  window.location.href = "";
				});
				$('.tools .clear-skin').click(function (e) {
					e.preventDefault();
						var name = "skin-switch";
						var data="ub";
					  $.removeCookie(name, { path: '/' });
					  window.location.href = "";
					  /*$.cookie(name, data, { expires: 365 ,path:"/"});
					  window.location.href = "";*/
				});
				
				$('.tools .colors a').click(function (e) {
					e.preventDefault();
					var data = $(this).data('stylecolor');
					var name = "stylecolor";
					  $.cookie(name, data, { expires: 365 ,path:"/"});
					  window.location.href = "";
				});
				$('.tools .clear-color').click(function (e) {
					e.preventDefault();
						var name = "stylecolor";
					  $.removeCookie(name, { path: '/' });
					  window.location.href = "";
				});
			});

							
	 $(".lang-switch").click(function(e){
			e.preventDefault();
	        var lang = $(this).data("lang");
	        var name = "lang-switch";
	          $.cookie(name, lang, { expires: 365 ,path:"/"});
	          window.location.href = "";
      });
	  
	  $(document).ready(function(){
	  	<?php if(isset($url)): ?> var url_ = base_url + 'unit/search_json/<?php if(isset($search)) echo $search ?>/<?php echo $unit ?><?php if(isset($url)) echo "/".$url ?>';
		<?php else: ?> var url_ = base_url + 'page/search_json/<?php if(isset($search)) echo $search ?>/<?php echo $unit ?><?php if(isset($url)) echo "/".$url ?>';
		<?php endif; ?>
		
		$.ajax({
	        url : url_,
	        type: "POST",
	        dataType : "json",
	        success:function(msg) 
	        {
	        	konten = msg;
	        	get_konten('');
	       
	        }
	    });
	});
	
	$(".tube-title-link").click(function(){
			var id = $(this).data("list");
			var lang = $(this).data("lang");
			var unit = $(this).data("unit");
			if(unit!=0){
				url = base_url + "/read/read_gallery";
			}else{
				url = base_url + "/page/read_gallery";
			}
		
			$.ajax({
					type : "POST",
					  dataType: "HTML",
					  url: url,
					data : $.param({
						id : id,lang:lang, unit:unit
					}),
					success : function(data) {
						$('#content-video').html(data);
					}
			}); 
		});
        </script>
		<script>
		$("#cmbangkatan").change(function(e){
			var angkatan = $("#cmbangkatan").val();
			
			<?php if(isset($url)): ?> 
			var unit_ = $("#ktmp").val();
			var url_ = base_apps + 'unit/alumni/'+angkatan+'/'+unit_;
			<?php else: ?> var url_ = base_url + 'page/alumni/'+angkatan;
			<?php endif; ?>
			
				$.ajax({
				url : url_,
				type: "POST",
				data : $.param({
					id : angkatan
				}),
				success : function(msg) {						
					$('.view-alumni').html(msg);	
				}
				});
		});
		</script>
		
		<?php if(isset($kategori) && ($kategori=='video' || $kategori=='video-streaming')){ ?>
		
		<script src="<?php echo $this->asset("ptiik/jwplayer/jwplayer.js"); ?>"></script>
		<script src="<?php echo $this->asset("ptiik/flowplayer/flowplayer.min.js"); ?>"></script>
        <script>
        	if($("#playStream").length){
        		$("#playStream").flowplayer({ 
        			swf: base_url+"assets/ptiik/flowplayer/flowplayer.swf",
        			 	native_fullscreen: true,
				      	splash: true,
				      	ratio: 9/16,
				      	embed: false,
				      	height: 500
        		 });
        	}
        	if($("#liveStream").length){
        	
        		jwplayer('liveStream').setup({ 
				    file: 'rtmp://175.45.187.248/live/livestream', 
				    
				      width: "100%",
				      aspectratio: "4:3",
    					//skin: "http://175.45.187.253/beta/apps3/assets/jwplayer/skins/five.xml",
				  });
				 }
        </script>
		<?php } ?>
        </body>
</html>
