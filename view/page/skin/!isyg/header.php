<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>International Symposium on Geoinformatics 2015</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?php echo $this->asset("skin/isyg/css/swiper.min.css"); ?>" rel="stylesheet">
		<link href="<?php echo $this->asset("skin/isyg/css/bootstrap.min.css"); ?>" rel="stylesheet">
		<link href="<?php echo $this->asset("skin/isyg/css/style.css"); ?>" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->asset("skin/isyg/css/jquery.smartmenus.bootstrap.css"); ?>" />
		
		 <?php $styles = $this->get_styles(); 
				if(is_array( $styles )) : 
				
				foreach($styles as $s) : 
			?><link href="<?php echo $this->asset($s); ?>" rel="stylesheet">
			<?php endforeach; endif; ?>
			<?php if( isset($mstyles) and is_array($mstyles)) { 
			foreach( $mstyles as $s) : ?><link href="<?php echo $this->location($s); ?>" rel="stylesheet">
			<?php endforeach; } ?>
			
	</head>
	<body>
		<header class="navbar navbar-fixed-top nav-custom">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle collapsed" data-target=".navbar-collapse" data-toggle="collapse" type="button">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $this->location('event/'.$kevent); ?>">ISYG</a>
				</div>
				<div class="navbar-collapse navbar-right collapse">
					
					<ul class="nav navbar-nav" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
						 <li><a itemprop="url" href="<?php echo $this->location('event/'.$kevent); ?>" title="Home"><span class="fa fa-home"></span> <span class="sr-only" itemprop='name'>Home</span> Home</a></li>
						<?php
						if($page):							
							foreach($page as $dt):
								$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
								if($child):
									?>
									  <li>
											<a rel="alternate" hreflang="x-default" href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" ><?php echo $dt->content_title?></a>
											<?php
												$i=0;
												 $count=round(count($child)/2);
												 $parent=0;
											?>		
											 <ul class="dropdown-menu" role="menu">														
												 <?php 													 
												 foreach ($child as $key):
													 $i++;
													if(isset($unit_list)):
														read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location($url) ,$key->content_data);
													else:
														read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location('event/'.$kevent),$key->content_data );
													endif;
												 endforeach; ?>
											</ul>
											
									</li>
									<?php	
								else:
									echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location('event/'.$kevent.'/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
								endif;
							endforeach;
						endif;
						?>
					</ul>
				</div>
			</div>
			<?php
			function read_sub_menu($i,$count,$parent,$unit=NULL, $lang=NULL, $mpage=NULL, $id=NULL, $title=NULL, $content_page=NULL,$url_menu=NULL){												
				$child = $mpage->read_sub_content($unit, $lang, "", $id, $title);			
				if($child):
					?>
					
					 <li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $title?></a>						
							<ul class="dropdown-menu" role="menu">
							 <?php 
							 $parent=1;
							 foreach ($child as $key):
									read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id,$key->content_title,$key->content_page, $url_menu);
							 endforeach; 
							 ?>
							</ul>
					</li>
					
					<?php
				else:
					if(isset($unit_list)):
						echo "<li><a itemprop='url' href=".$this->location($url.'/read/'.$content_page.'/'.$id)." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					else:
						echo "<li><a itemprop='url' href=".$url_menu.'/read/'.$content_page.'/'.$id." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					endif;					
				endif;
			}

			?>
		</header>
	