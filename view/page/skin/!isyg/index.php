<?php $this->view( 'page/skin/isyg/header.php', $data ); ?>
	<section class="swiper-container">
			<div class="swiper-wrapper">
			<?php
			echo count($slide);
			if(isset($slide) && $slide):
				foreach($slide as $dt):
							
						?>
						<div class="swiper-slide">
							<a href="<?php if($dt->file_data) echo $dt->file_data; 
								else echo "#";
								?>"><img class="img img-responsive" src="<?php echo $this->config->file_url_view.'/'.$dt->file_loc; ?>" alt="<?php echo $sl->file_title; ?>"/></a>
						</div>
						
						<?php
						endforeach;
				endif;
					?>	
				
			</div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
		</section>
		<div class="container">
			<div class="row">
				<div class="wrapper">
				
				<?php if($about):
						foreach($about as $dt):
						?>
						<div class="wrap-title">
							<h1 class="text-title"><?php	echo $dt->judul; ?></h1>
						</div>
						<div class="wrap-body">
						
							<?php
							echo $dt->keterangan;
							
							?>
						</div>
					<?php 
						endforeach;
					endif; ?>						
					
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">Keynote & Speaker</h1>
					</div>
					<div class="wrap-body">
						<div class="row">
							<?php
							if($speaker):
								foreach($speaker as $key):
									if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
									else $imgthumb = $this->config->default_thumb_web;
								
									?>
									<div class="col-sm-3">
										<a href="<?php if(isset($url)):
													echo $this->location($url.'/read/gallery');
												else:
													echo $this->location('event/'.$kevent);
												endif; ?>"><img class="img-responsive img-responsive-center img-thumbnail" src="<?php echo $imgthumb; ?>">
											<span class="img-overlay"></span>
										</a>
									</div>
									<?php
								endforeach;
							endif;
							?>
														
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">ISYG 2015 News</h1>
					</div>
					<div class="wrap-body">
						<div class="row">
							<?php
								if(isset($news) && ($news) ){
									foreach ($news as $dt):
										$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	

										if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
										else $imgthumb = $this->config->default_thumb_web;
													
										?>

										<div class="col-sm-4 news">
											<h3 class="text-title"><?php echo $mpage->content($dt->judul,5);?></h3>
											<div><?php echo $mpage->content($dt->keterangan,20);?> <a href="<?php echo $this->location('event/'.$kevent.'/read/news/'.$content.'/'.$dt->id); ?>" class="read-more font-orange">more..</a>
											</div>
										</div>						
									<?php
									endforeach;
								}
								?>	
						
							
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">Our Sponsor</h1>
					</div>
					<div class="wrap-body">
						<div class="flex">
							<?php
							if(isset($kerjasama) && $kerjasama){
								foreach($kerjasama  as $dt):
									if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
									else $imgthumb = $this->config->default_thumb_web;
									
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
									
									
									if (isset($url)) $link = $this->location($url.'/read/kerjasama/'.$content.'/'.$dt->id);
									else $link = $this->location('page/read/kerjasama/'.$content.'/'.$dt->id);
									?>
									
									<div class="flex-cell text-center">
											<img src="<?php echo $imgthumb; ?>" class="img-responsive img-responsive-center" alt="<?php echo $dt->judul ?>">
										</div>	
							
									<?php
								endforeach;
							} ?>
							
							
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">Join Us</h1>
					</div>
					<div class="wrap-body">
						<div class="text-center">
							<h1>We're almost ready, are you?</h1>
							<h3><a href="#" class="font-orange">Submit Paper</a></h3>
						</div>
					</div>
				</div>
			</div>
	<?php $this->view( 'page/skin/isyg/footer.php', $data ); ?>	