	<footer class="row">
				<div class="col-sm-6">
					<p>International Symposium on Geoinformatics 2015</p>
				</div>
				<div class="col-sm-6">
					<div class="text-right">
						<p>FILKOM</p>
					</div>
				</div>
			</footer>
		</div>
		
		<script src="<?php echo $this->asset("skin/isyg/js/jquery.min.js"); ?>"></script>
		<script src="<?php echo $this->asset("skin/isyg/js/bootstrap.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo $this->asset("skin/isyg/js/jquery.smartmenus.min.js");?>"></script>
		<script type="text/javascript" src="<?php echo $this->asset("skin/isyg/js/jquery.smartmenus.bootstrap.min.js");?>"></script>
		<script src="<?php echo $this->asset("skin/isyg/js/classie.js"); ?>"></script>
		<script src="<?php echo $this->asset("skin/isyg/js/swiper.jquery.min.js"); ?>"></script>
		
		<?php $scripts = $this->get_scripts(); 
		foreach( $scripts as $s) : ?><script src="<?php echo $this->asset($s); ?>"></script>
		<?php endforeach; ?>
		<?php if( isset($mscripts) and is_array($mscripts)) { 
		foreach( $mscripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
		<?php endforeach; } ?>
	
		
		<script>
			function init() {
				window.addEventListener('scroll', function(e){
					var distanceY = window.pageYOffset || document.documentElement.scrollTop,
						shrinkOn = document.querySelector(".swiper-container").offsetTop,
						header = document.querySelector("header");
					if (distanceY > shrinkOn) {
						classie.add(header,"smaller");
					} else {
						if (classie.has(header,"smaller")) {
							classie.remove(header,"smaller");
						}
					}
				});
			}
			window.onload = init();
			
			$(document).ready(function(){
				var swiper = new Swiper('.swiper-container', {
					pagination: '.swiper-pagination',
					paginationClickable: true,
					loop: true,
					autoplay: 6000
				});
			});
		</script>
	</body>
</html>