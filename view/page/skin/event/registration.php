<div  id="registration">
	
	<?php
	$arr_data=array();
	
	
	if(isset($komponen)):
		foreach($komponen as $dt):
			$detail = $mconf->get_komponen_biaya($dt->id,"","",$dt->jenis_peserta);	
			
			foreach($detail as $key):
				$arr_temp = array("jenis_peserta"=>$dt->jenis_peserta, "jpeserta"=>$key->jpeserta, "kategori"=>$key->kategori, "satuan"=>$key->satuan, "harga"=>$key->harga, "komponen_id"=>$key->komponen_id, "kegiatan_id"=>$key->kegiatan_id, "nama_biaya"=>$key->nama_biaya);
				array_push($arr_data, $arr_temp);
			endforeach;
			
				if($detail && $dt->jenis_peserta=='peserta'):			
					echo '<h3>'.$dt->keterangan.'</h3><table class="table">';
					
					for($i=0;$i<count($arr_data);$i++){														
						if($arr_data[$i]['jenis_peserta']=='peserta'){
							echo '<tr>';
							if($arr_data[$i]['kategori'])	echo '<td>'. $arr_data[$i]['jpeserta'].'</td><td>'.$arr_data[$i]['kategori'].'</td>';
							else echo '<td width=40%>'.$arr_data[$i]['nama_biaya'].'</td>'; 
							echo '<td><b>'.strToUpper($arr_data[$i]['satuan']).". ".number_format($arr_data[$i]['harga']).'</b></td><td>';
							?>
							<a href="#" onclick="reg('<?php echo $arr_data[$i]['komponen_id'] ?>','<?php echo $arr_data[$i]['kegiatan_id'] ?>','<?php echo $arr_data[$i]['jenis_peserta'] ?>','<?php echo $arr_data[$i]['jpeserta'] ?>', '<?php echo $arr_data[$i]['satuan']; ?>', '<?php echo $arr_data[$i]['harga']; ?>', '<?php echo $arr_data[$i]['nama_biaya']; ?>')" class="btn btn-danger">Register</a>
							<?php
							echo '</td></tr>';
						}
					}
					echo '</table>';
				else:
					?>
						<h3><?php echo $dt->keterangan ?>&nbsp;<a href="#" onclick="reg('-','<?php echo $dt->kegiatan_id ?>','<?php echo $dt->jenis_peserta ?>','<?php echo $dt->keterangan ?>', '-', '0', '0')" class="btn btn-danger">Register</a></h3>
						<?php
						$row = $mconf->get_komponen_biaya($dt->id,"nama_biaya","",$dt->jenis_peserta);
						if($row):
							echo "<table class='table'>";
							foreach($row as $key):
								for($i=0;$i<count($arr_data);$i++){		
									if($arr_data[$i]['jenis_peserta']==$dt->jenis_peserta && $arr_data[$i]['nama_biaya']==$key->nama_biaya){
										if($arr_data[$i]['kategori']=='Reguler'){
											$reguler = $arr_data[$i]['harga'];
										}else{
											$early = $arr_data[$i]['harga'];
										}
										$satuan =  strToUpper($arr_data[$i]['satuan']);
									}
								}
								echo "<tr>";
									echo "<td rowspan=2 width=40%>".$key->nama_biaya."</td>";
									echo "<td width=40%>Early Bird Price</td>";
									echo "<td>".$satuan.". ".number_format($early)."</td>";
								echo "</tr>";
								echo "<tr>";
									echo "<td>Reguler Price</td>";
									echo "<td>".$satuan.". ".number_format($reguler)."</td>";
								echo "</tr>";
							endforeach;
							echo "</table>";
						endif;
				endif;
			
		endforeach;
	endif;
	?>
</div>
<script>
	function reg(id, kegiatan, kpeserta, peserta, satuan, harga, biaya){
		var URL = base_url + 'event/read_data/reg_inf';
        $.ajax({
             type: "POST",
			 dataType : "html",
			 url : URL,
			 data : $.param ({
				id : id, 
				kegiatan : kegiatan,
				kpeserta:kpeserta,
				peserta: peserta,
				satuan: satuan,
				harga: harga, 
				biaya: biaya
			}),
			success:function(msg) 
	        {
				$("#registration").html(msg);
	        },
	        error: function(msg) 
	        {
	            alert ('FAILED');    
	        }
	    });
	    e.preventDefault(); //STOP default action
	    return false;
		
		
	}
</script>