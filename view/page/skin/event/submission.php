<?php
if(isset($detail_peserta) && $detail_peserta):
?>
	<section itemscope="" itemtype="http://schema.org/Article">
			<h1 class="title-content margin-top-no margin-bottom-sm font-raleway font-orange padding-bottom-no" itemprop="name"><?php echo $detail_peserta->title.". ".$detail_peserta->nama; ?></h1>
		</section>
<?php
endif;
?>
<section itemprop="articleBody">
	<?php 
	
	if(isset($detail_peserta) && $detail_peserta):
		$mconf = new model_eventinfo();
		$tagihan = $mconf->get_tagihan("",$detail_peserta->peserta_id,"-");
		if($tagihan){
			$paid = $mconf->get_tagihan("",$detail_peserta->peserta_id,1);
			$unpaid = $mconf->get_tagihan("",$detail_peserta->peserta_id,0);
			$pay_status = '<span class="text text-success">Paid ('.count($paid).')</span> <span class="text text-danger">Unpaid ('.count($unpaid).')</span>';
		}else{
			$pay_status = '<span class="text text-danger">None</span>';
		}
		
		
		$abstrak = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'abstrak',"0");
		$abstrak_valid = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'abstrak',1);	
		$abstrak_rev = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'abstrak',2);
		$abstrak_rej = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'abstrak',3);
		
		if(!$abstrak) $abs_status = '';
		else $abs_status = '<span class="text text-info">Submitted ('.count($abstrak).')</span>';
			
		if($abstrak_valid){
			$abs_status = $abs_status.' <span class="text text-success">Accepted ('.count($abstrak_valid).')</span>';
			
			$paper = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'paper',"0");
			$paper_valid = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'paper',1);
			$paper_rev = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'paper',2);
			$paper_rej = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'paper',3);
			
			if($paper) $paper_status = '<span class="text text-info">Submitted ('.count($paper).')</span>';
			else $paper_status = '<span class="text text-danger">None</span>';
			
			if($paper_valid){
				$paper_status.= ' <span class="text text-success">Accepted ('.count($paper_valid).')</span>';
			
				$revisi = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'revisi',"0");
				$revisi_valid = $mconf->get_dokumen("",$detail_peserta->peserta_id, 'revisi',1);
				
				if($revisi) $rev_status = '<span class="text text-info">Submitted ('.count($revisi).')</span>';
				else $rev_status = '<span class="text text-danger">None</span>';
				
			}else{
				$rev_status = '<span class="text text-danger">None</span>';
			}
		}else{
			if(!$abstrak) $abs_status = '<span class="text text-danger">None</span>';
			else $abs_status = '<span class="text text-info">Submitted ('.count($abstrak).')</span>';
			$paper_status = '<span class="text text-danger">None</span>';
			$rev_status = '<span class="text text-danger">None</span>';
		}
		
		?>
						
		<div class="well content content-white" id="view-show">
			<h3 class="margin-top-no margin-bottom-sm padding-bottom-no" itemprop="name">Manuscript Submission and Status</h3>
			<ul>
				<li>Information such as submission and payment status updates can be checked by Author.</li>
				<li>Conference Registration details can't be edited</li>
			</ul>
			<table class="table">
				<thead>
					<tr><th>No</th><th>Item</th><th>Status</th><th>Action</th></tr>
				</thead>
				<tbody>
					<tr>
						<td>1.</td>
						<td>Conference  Registration</td>
						<td><span class="text text-success">As <?php echo $detail_peserta->jpeserta; ?></span></td>
						<td><a href='#' onClick="view_event('peserta','<?php echo $detail_peserta->peserta_id; ?>','<?php echo $detail_peserta->kegiatan_id; ?>' );"><span class="text text-info">View</span></a></td>
					</tr>
					<tr>
						<td>2.</td>
						<td>Abstract Submission</td>
						<td><?php echo $abs_status ?></td>
						<td><?php if(!$abstrak || !$abstrak_valid): ?><a href='#' class='btn btn-xs btn-danger' onClick="upload_file('abstrak');">Upload</a>
							<?php endif; ?> <a href='#' onClick="view_event('abstrak','<?php echo $detail_peserta->peserta_id; ?>','<?php echo $detail_peserta->kegiatan_id; ?>' );"><span class="text text-info">View</span></a>
						</td>
					</tr>
					<tr>
						<td>3.</td>
						<td>Full Paper Submission</td>
						<td><?php echo $paper_status ?></td>
						<td><?php if($abstrak_valid): ?><a href='#' class='btn btn-xs btn-danger' onClick="upload_file('paper');">Upload</a>
							<?php endif; ?> <a href='#' onClick="view_event('paper','<?php echo $detail_peserta->peserta_id; ?>','<?php echo $detail_peserta->kegiatan_id; ?>' );"><span class="text text-info">View</span></a>
						</td>
					</tr>
					<tr>
						<td>4.</td>
						<td>Revision Paper Submission</td>
						<td><?php echo $rev_status ?></td>
						<td><?php if($paper_valid): ?><a href='#' class='btn btn-xs btn-danger' onClick="upload_file('revisi');">Upload</a>
							<?php endif; ?> <a href='#' onClick="view_event('revisi','<?php echo $detail_peserta->peserta_id; ?>','<?php echo $detail_peserta->kegiatan_id; ?>' );"><span class="text text-info">View</span></a>
						</td>
					</tr>
					<tr>
						<td>5.</td>
						<td>Payment</td>
						<td><span class="text text-danger"><?php echo $pay_status ?></span></td>
						<td><a href='#' onClick="view_event('tagihan','<?php echo $detail_peserta->peserta_id; ?>','<?php echo $detail_peserta->kegiatan_id; ?>' );"><span class="text text-info">View</span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="view-event" style="display:none"></div>
		<div id="view-form-file" style="display:none">	
			<div id="alert-msg"></div>
				<form enctype="multipart/form-data" method="post" id="form-file">		
					<div class="well content content-white">		
						<h3 class="margin-top-no margin-bottom-sm padding-bottom-no" itemprop="name" id="judul">Upload File</h3>
						<div class="form-group">
							<label>File <span class="text text-danger">* required</span></label><br>
							<input type="file" name="file" id="file" required/>
						</div>
						<div class="form-group">
							<label>Note</label><br>							
							<textarea name="note" id="note" cols=30></textarea>
						</div>
							
						<div class="form-group">
							 <input type="hidden" name="tmpid" value="<?php echo $detail_peserta->peserta_id; ?>"/>
							  <input type="hidden" name="pesertaid" value="<?php echo $detail_peserta->jenis_peserta; ?>"/>
							 <input type="hidden" name="kegiatan_id" value="<?php echo $detail_peserta->kegiatan_id; ?>"/>
							<input type="hidden" id="hidval" name="hidval">
							 <button type="submit" class="btn btn-primary" name="btn_sub">Submit</button>
							 <button type="button" class="btn btn-danger" onClick="cancelForm()">Cancel</button>
						</div>
					</div>
			</form>
		</div>
		<?php
	else:
		if(isset($pin)):
			?>
			<div class="alert alert-danger">Wrong PIN. Re-type your PIN.</div>
			<?php
		endif;
	?>
	<form method="post" action="<?php echo $this->config->web_base_url.'/event/isyg/submission'; ?>" class="form-inline">
		<div class="form-group">
			  <div class="form-group">
				<label>PIN</label>
				<input type="password" class="form-control" name="ipt-pin" placeholder="PIN">
			  </div>
			  <button type="submit" class="btn btn-default">Confirm identity</button>
		</div>
	</form>
	<?php endif; ?>
</section>


