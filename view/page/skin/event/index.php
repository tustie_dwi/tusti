<?php $this->view( 'page/skin/'.$skin.'/header.php', $data ); ?>
<?php 
if($about):
	$i=0;
	foreach($about as $dt):	
		$i++;
		if($i>1) $sclass = 'content-white';
		else $sclass='content-dark';
	?>
			<section class="content <?php echo $sclass?> padding-bottom-no">
				<div class="container">				
					<div class="col-md-12" style="padding-left:0">			
					<h2 class="title-content margin-top-no margin-bottom-sm font-orange uppercase font-raleway"><span><?php	echo $dt->judul; ?></span></h2>
					<div class="wrap-body"><?php echo $dt->keterangan; ?></div>	
					</div>
							
				</div>
			</section>
<?php 
	endforeach;
endif; ?>

 <section class="content content-white">
	<div class="container">
		<div class="col-md-12" style="padding-left:0">
			<h2 class="title-content title-underline margin-top-no margin-bottom-sm font-orange font-raleway uppercase"><span>Agenda Kegiatan</span></h2>
			
				<div class="row">					
															
				<div class="col-sm-6">					
					<div class="iysg-news">
						<a href='http://filkom.ub.ac.id/event/ramadhan2015/read/pengajian-umum/f630caa' style="color:#444"><h3 class="margin-top-no"><i class="fa fa-star"></i> Pengajian Sambut Ramadhan </h3></a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="iysg-news">
						<a href="http://filkom.ub.ac.id/event/ramadhan2015/read/kajian-bada-dzhuhur/cdc2724" style="color:#444"><h3 class="margin-top-no"><i class="fa fa-star"></i> Kajian Ba`da Dzuhur</h3></a>
					</div>					
				</div>
				<div class="col-sm-6">
					<div class="iysg-news">
						<a href="http://filkom.ub.ac.id/event/ramadhan2015/read/tahsin-belajar-baca-alquran/89c4084" style="color:#444"><h3 class="margin-top-no"><i class="fa fa-star"></i> Tahsin (Belajar Membaca Al Qur`an)</h3></a>
					</div>						
				</div>
				<div class="col-sm-6">
					<div class="iysg-news">
						<a href="http://filkom.ub.ac.id/event/ramadhan2015/read/kajian-wanita/7c45561" style="color:#444"><h3 class="margin-top-no"><i class="fa fa-star"></i> Kajian Wanita</h3></a>
					</div>	
				</div>
				<div class="col-sm-6">
					<div class="iysg-news">
						<a href="http://filkom.ub.ac.id/event/ramadhan2015/read/buka-puasa-dan-tarawih-bersama/f743976" style="color:#444"><h3 class="margin-top-no"><i class="fa fa-star"></i> Buka Puasa dan Tarawih Bersama</h3></a>
					</div>						
				</div>
				<div class="col-sm-6">
					<div class="iysg-news">
						<a href="http://filkom.ub.ac.id/event/ramadhan2015/read/halal-bihalal/38603a4" style="color:#4caf50"><h3 class="margin-top-no"><i class="fa fa-star-o"></i> Halal Bihalal</h3></a>
					</div>	
				</div>							
			
			</div>

		</div>
	</div>
</section>
<?php if(isset($news) && ($news) ){?>
<section class="content content-white padding-top-no">
	<div class="container">
		<div class="col-md-12" style="padding-left:0">
			<h2 class="title-content title-underline margin-top-no margin-bottom-sm font-orange uppercase font-raleway"><span>Berita</span></h2>
			<div class="row">
				<?php
					if(isset($news) && ($news) ){
						foreach ($news as $dt):
							$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	

							if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
							else $imgthumb = $this->config->default_thumb_web;
										
							?>

							<div class="col-sm-4">
								<div class="iysg-news">
									<h2 class="margin-top-no"><a href="<?php echo $this->location('event/'.$kevent.'/read/news/'.$content.'/'.$dt->id); ?>"  style="color:#333"><?php echo $mpage->content($dt->judul,7);?></a></h2>
									<div><?php echo $mpage->content($dt->keterangan,30);?> <a href="<?php echo $this->location('event/'.$kevent.'/read/news/'.$content.'/'.$dt->id); ?>" class="read-more font-orange">selengkapnya</a>
									</div>
								</div>
							</div>
							
						<?php
						endforeach;
					}
					?>	
			</div>
			<div class="pull-right">
				<a href="<?php if (isset($url)) echo $this->location($url.'/read/news/'); else echo $this->location('event/'.$kevent.'/read/news/'); ?>" class="read-more font-orange btn btn-success">Berita Lainnya</a>
			</div>
		</div>
		
	</div>
</section>
<?php } 
	if(isset($gallery) && $gallery){
?>		
	<section class="content content-black">
		<div class="container">
			<div class="row">
				<?php
				if(isset($gallery) && ($gallery) ){
					foreach ($gallery as $key):
						if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
						else $imgthumb = $this->config->default_thumb_web;
										
					?>
						<div class="col-sm-3 col-xs-6">
							<a class="picture-landing-link text-center" href="<?php echo $key->file_data ;?>">
								<img class="img img-responsive img-responsive-center" src="<?php echo $imgthumb ?>">
								<div class="picture-landing-link-hidden text-right-md padding-right-md padding-left-sm">
									<h4 class="h4 picture-landing-link-title"><?php echo $key->file_title ;?></h4>
									<div class="picture-landing-link-description padding-bottom-sm"><?php echo $key->file_note ;?></div>
								</div>
							</a>
						</div>
					<?php
					endforeach;
				}
				if(count($gallery < 2)){
				?>
					
						<div class="col-sm-3 col-xs-6">
							<a class="picture-landing-link text-center" href="#">
								<img class="img img-responsive img-responsive-center" src="http://placehold.it/400x400&text=Semarak Ramadhan FILKOM 2015">
								<div class="picture-landing-link-hidden text-right-md padding-right-md padding-left-sm">
									<h4 class="h4 picture-landing-link-title">Tahsin</h4>
									<div class="picture-landing-link-description padding-bottom-sm">Rengkuh hidayah, gapai ampunan dan ridloNya. Dengan semangat Ramadhan menuju era baru FILKOM.</div>
								</div>
							</a>
						</div>
						<div class="col-sm-3 col-xs-6">
							<a class="picture-landing-link text-center" href="#">
								<img class="img img-responsive img-responsive-center" src="http://placehold.it/400x400&text=Semarak Ramadhan FILKOM 2015">
								<div class="picture-landing-link-hidden text-right-md padding-right-md padding-left-sm">
									<h4 class="h4 picture-landing-link-title">Kajian Wanita</h4>
									<div class="picture-landing-link-description padding-bottom-sm">Rengkuh hidayah, gapai ampunan dan ridloNya. Dengan semangat Ramadhan menuju era baru FILKOM.</div>
								</div>
							</a>
						</div>
				<?php
				}
				?>
			
			</div>
		</div>
	</section>
	<?php 	
	}
	$this->view( 'page/skin/'.$skin.'/footer.php', $data ); ?>	 