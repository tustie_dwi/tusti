<?php $this->view( 'page/skin/'.$skin.'/header.php', $data ); ?>
<section class="content content-dark">
		<div class="container">	
			<div class="row">
			<div class="col-md-12 margin-bottom-no">
			<?php		
			if(isset($detail)):	
				
					
				if($detail->keterangan)	$strcontent	= $detail->keterangan;
				else $strcontent = $detail->keterangan_ori;	
			
						?>
					<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-sm font-raleway font-orange padding-bottom-no" itemprop="name"><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori;
				
					?></h1>
						
					</section>				
					<section itemprop="articleBody">
						<?php
						//echo $detail->content_data;
						$strcontent_ = $mpage->str_content($strcontent,$this->config->file_url_view,$this->config->file_url_old);
						echo $this->add_alt_tags($strcontent_,"a"); 
						
						if(isset($detail->content_data)):
							if(isset($url)) $url_link=$url;
							else $url_link = "";
							$this->read_data($detail->content_data, $url_link, $eventid);
						endif;
						?>
						<div class="content-pane-tube text-right margin-bottom-sm">                						
							<div class="tube-share-button">
							<h4>
								<span class="fa fa-share-alt font-orange" title="Share"></span> <small>Share</small>
								<a class="fb font-orange" href="javascript::" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','fbshare','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-facebook-square"></span></a>
								<a class="twitter font-orange" href="javascript::" onclick="window.open('https://twitter.com/intent/tweet?text=Judul&url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>&via=PTIIK_UB','twt','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-twitter-square"></span></a>
								<a class="gplus font-orange" href="javascript::" onclick="window.open('https://plus.google.com/share?url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','gp','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-google-plus-square"></span></a>
								</h4>
							</div>
						</div>

					</section>
					
				<?php
			endif;	
			
			if(isset($msg)) echo $msg;
			
			if(isset($submission)){
				if(! isset($pin)):
				?>
				<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-sm font-raleway font-orange padding-bottom-no" itemprop="name">Submission</h1>
				</section>
				<?php
				endif;
				$this->view('page/skin/isyg/submission.php', $data);
			}
			
			if( isset($post) ) : 
				?>
				<section itemscope="" itemtype="http://schema.org/Article">
				<section itemprop="articleBody">
					
						<table class="table web-page">
							<thead style="display:none">
								<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
							</thead>
							<tbody>
							<?php
								if($post) : 
								foreach($post as $dt) { 
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
								?>
								<tr style="border-top:0px;">
										<td style="display:none;border:0px;"></td>
									<td style="border-top:0px;">
										<?php 
										if(isset($url)):
											if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
											else $url_content = $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
										else:
											if($kategori=='news') $url_content= $this->location('event/'.$kevent.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
											else $url_content = $this->location('event/'.$kevent.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
										endif;
										
										if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
										else $imgthumb = $this->config->default_thumb_web;
										?>
										
									   
											<?php 
											if($kategori=='news' || $kategori=='matakuliah' || $kategori=='course') :
											?>
											 <div class="media">
												<a class="pull-left" href="<?php echo $url_content; ?>" class="font-orange">
													<?php if($kategori=='news') : ?>
													<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="200px">
													<?php else : ?>
													<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="100px">
													<?php endif; ?>
												</a>
												
												 <div class="media-body">
													<a class="title-article font-orange" href="<?php echo $url_content; ?>"><h3 class="media-heading"><?php echo $dt->judul; ?></h3></a>
														<?php if($kategori=='news'){
															?>
															<small>
															<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43">&nbsp;<?php echo date("M d, Y", strtotime($dt->content_modified)); ?></time>
															<span class="category"><span class="fa fa-tags"></span>&nbsp;<?php echo $kategori; ?></span>
															</small><p></p>
															<!--<time class="time-post"><small><?php //if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
															//else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>-->
														<p class="post-content text-justify">
														<?php
														$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
														$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
														$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
													   echo htmlspecialchars($isi);
							
													?>
													<?php //var_dump($dt); ?>
														<a href="<?php echo $url; ?>" class="more-orange font-orange">..selengkapnya</a>
													</p>
													<?php } ?>
												</div>
											</div>					
										<?php 
										else:
											
										?>
											 <div class="media"><a class="title-article" href="<?php echo $url_content; ?>"><?php if($dt->judul) echo $dt->judul;
											else echo $dt->judul_ori;
											?></a>
											<time class="time-post"><small><?php if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
											else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>
											</div>
											<?php
											$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
														$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
														$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
													//   echo htmlspecialchars($isi);
										endif;?>
										   
									</td>
								</tr>
								<?php	
								} 
								endif;	
							?>
						</tbody>
						</table>
					
				</section>
			</section>
		<?php
			
		endif; 
		?>
			
			</div>
		</div>
	</div>
</section>
<?php $this->view( 'page/skin/'.$skin.'/footer.php', $data ); ?>	