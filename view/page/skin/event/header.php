  <?php 
  if(!isset($_COOKIE['lang-switch'])){
	  	setcookie('lang-switch','en',time()+3600*24,"/");
  		include("library/en.php");
	  	$data['lang']="en";
  }
  else{
  		include("library/".$_COOKIE['lang-switch'].".php");
	  	$data['lang']=$_COOKIE['lang-switch'];
  }
  ?>
<!DOCTYPE html>
<html lang="en-US">
	<head>	
	<?php 
		if(isset($detail)&&$detail){?>
			<title><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; echo " | "; echo $page_title; ?></title>
			<meta property="og:title" content="<?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; ?>">
			<meta itemprop="name" content="<?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori; ?>">

		<?php }else{ ?>
			<title><?php echo $page_title; ?></title>
			<meta property="og:title" content="<?php echo $page_title; ?>">
			<meta itemprop="name" content="<?php echo $page_title; ?>">
		<?php }	 ?>				
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Official Site - <?php echo $page_desc; ?>">
		<meta name="keywords" content="ptiik, filkom, event, kegiatan, ramadhan, 2015, ub, isyg" >
		<meta name="author" content="BPTIK">
		<meta name="robots" content="index, follow" />
		<meta name="HandheldFriendly" content="true">
		<meta http-equiv="cleartype" content="on">
		<meta http-equiv="cache-control" content="no-cache">
		<meta name="rating" content="Mature" />
			
		<link href="<?php echo $this->asset_file("ptiik/css/event.min.css"); ?>" rel="stylesheet">
		<link href="<?php echo $this->asset_file("ptiik/css/insideptiik.slider.min.css"); ?>" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo $this->asset_file('admin/css/plugins/morris.css')  ?>">
		<!-- Edit 1 -->
		<link href="<?php echo $this->asset_file("ptiik/css/jquery.fancybox.css"); ?>" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo $this->asset_file("images/favicon.ico"); ?>"/>
		
		 <?php $styles = $this->get_styles(); 
				if(is_array( $styles )) : 
				
				foreach($styles as $s) : 
			?><link href="<?php echo $this->asset($s); ?>" rel="stylesheet">
			<?php endforeach; endif; ?>
			<?php if( isset($mstyles) and is_array($mstyles)) { 
			foreach( $mstyles as $s) : ?><link href="<?php echo $this->location($s); ?>" rel="stylesheet">
			<?php endforeach; } ?>
			<script type="text/javascript">
				var base_url = '<?php if(isset($url)) echo $this->location($url); 
				else echo $this->location(); ?>';
				var base_apps = '<?php echo $this->location(); ?>';
			</script>  		
			
	</head>
	 <body>
		<!--<canvas id="the-canvas" style="z-index:-1;position:fixed"></canvas>
		<div style="z-index:1">-->
        <header id="header">
            
           <nav class="navbar nav-main navbar-default navbar-static-top margin-bottom-no">
					  <div class="container">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <?php
						  if($logo) $img = $this->config->file_url_view."/".$logo;
							else $img = $this->config->default_thumb_web;?>
							
					      <a href="<?php echo $this->location('event/'.$kevent); ?>" class="navbar-toggle-menu collapsed navbar-brand navbar-brand-menu" >
					      	<img alt="Brand" src="<?php echo $img; ?>">
					      </a>
					      <!-- <a class="navbar-brand" href="#">Brand</a> -->
					    </div>
					
					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav navbar-right" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
							 <li><a itemprop="url" href="<?php echo $this->location('event/'.$kevent); ?>" title="Home"><span class="fa fa-home"></span> <span class="sr-only" itemprop='name'>Home</span></a></li>
					     			
							<?php
						if($page):							
							foreach($page as $dt):
								$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
								if($child):
									?>
									  <li class="dropdown">
											<a rel="alternate" hreflang="x-default" href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" ><?php echo $dt->content_title?><span class="caret"></span></a>
											<?php
												$i=0;
												 $count=round(count($child)/2);
												 $parent=0;
											?>		
											 <ul class="dropdown-menu" role="menu">														
												 <?php 													 
												 foreach ($child as $key):
													 $i++;
													if(isset($unit_list)):
														read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location($url) ,$key->content_data);
													else:
														read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location('event/'.$kevent),$key->content_data );
													endif;
												 endforeach; ?>
											</ul>
											
									</li>
									<?php	
								else:
									echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location('event/'.$kevent.'/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
								endif;
							endforeach;
						endif;
						?>
						
					       
					      </ul>
					    
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
					
	
			<?php
			if(! isset($view_slide)): ?>
            <div class="slide <?php //if(isset($unit_list)) echo " mini-slide"; ?>">	
				 
                <div  id="slider" class="owl-carousel">
                   
					<?php
					
					if(isset($slide) && ($slide)):
						foreach($slide as $dt):
							
						?>
						 <div class="item">
								<a rel="alternate" hreflang="x-default" href="<?php if($dt->file_data) echo $dt->file_data; 
								else echo "#";
								?>"><img src="<?php echo $this->config->file_url_view.'/'.$dt->file_loc;?>" alt="...">
								<div class="caption" style="padding-top:15%">									
									<h3 class="font-raleway"><?php //echo $dt->file_title?></h3>
									<div class="detail padding-top-no font-raleway">
										<?php //echo $dt->file_note?>
									</div>
								</div>
								</a>
							</div>
						<?php
						endforeach;
					else:
					?>
						 <div class="item">
							<img src="<?php echo "http://file.filkom.ub.ac.id/fileupload/assets/upload/file/PTIIK/konten/slider/2014-10/darkblackpatern.png";?>" alt="...">
							<div class="caption">
							</div>
						</div>
						
					<?php
					endif;
					?>
                </div>
              <!--  <div class="slide-navigation">
                    <a href="#" class="owl-prev slide-prev"><span class="fa fa-chevron-left"></span></a>
                    <a href="#" class="owl-next slide-next"><span class="fa fa-chevron-right"></span></a>
                </div>-->
            </div>
<?php 
			endif;
			function read_sub_menu($i,$count,$parent,$unit=NULL, $lang=NULL, $mpage=NULL, $id=NULL, $title=NULL, $content_page=NULL,$url_menu=NULL){												
				$child = $mpage->read_sub_content($unit, $lang, "", $id, $title);			
				if($child):
					?>
					
					 <li class="menu-item dropdown dropdown-submenu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" ><?php echo $title?></a>						
							<ul class="dropdown-menu" role="menu">
							 <?php 
							 $parent=1;
							 foreach ($child as $key):
									read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id,$key->content_title,$key->content_page, $url_menu);
							 endforeach; 
							 ?>
							</ul>
					</li>
					
					<?php
				else:
					if(isset($unit_list)):
						echo "<li><a itemprop='url' href=".$this->location($url.'/read/'.$content_page.'/'.$id)." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					else:
						echo "<li><a itemprop='url' href=".$url_menu.'/read/'.$content_page.'/'.$id." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					endif;					
				endif;
			}

		
			?>
</header>
  <section id="wrapper">