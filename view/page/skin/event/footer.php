	</section>
	<footer id="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter" >
	  <div class="container">
				<section class="footer-bottom">
						<div class="container">
						<div class="col-sm-4"><a href="http://ptiik.ub.ac.id"><img src="<?php echo $this->asset_file("ptiik/images/filkom-black.png");  ?>" class="img-responsive img-responsive-center" width="50%"></a></div>
						<div class="col-sm-8 text-right" style="font-size:70%;line-height:1.4">
							 <?php echo ptiik_alamat ?>
							 <div class="sr-only">Copyright &copy; <span itemprop="copyrightYear">2015</span> <span itemprop="copyrightHolder">BPTIK</span></div>
						</div>
					</div>
						
				</section>
			</div>
	</footer>
<!--</div>-->
		
	<!--	<script src="<?php //echo $this->asset_file("ptiik/cur/TweenLite.js"); ?>"></script>-->
	<!--	<script src="<?php //echo $this->asset_file("ptiik/cur/EasePack.js"); ?>"></script>
		
		<script src="<?php //echo $this->asset_file("ptiik/cur/isyg.js"); ?>"></script>	-->
    <script src="<?php echo $this->asset_file("ptiik/js/jquery-1.11.1.min.js"); ?>"></script>		
		  <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $this->asset_file("ptiik/js/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo $this->asset_file("ptiik/js/owl.carousel.min.js"); ?>"></script>

    <script src="<?php echo $this->asset_file("ptiik/js/jquery.fancybox.pack.js"); ?>"></script>
	  <script type="text/javascript" src="<?php echo $this->asset_file("ptiik/js/helpers/jquery.fancybox-media.js?v=1.0.6"); ?>"></script>
	  
    <script src="<?php echo $this->asset_file("ptiik/js/jquery.mixitup.min.js"); ?>"></script>

	<script src="<?php echo $this->asset_file("js/jquery/jquery.cookie.js"); ?>"></script>
	<script src="<?php echo $this->asset_file("js/search.js"); ?>"></script>

	
	<?php $scripts = $this->get_scripts(); 
	foreach( $scripts as $s) : ?><script src="<?php echo $this->asset($s); ?>"></script>
	<?php endforeach; ?>
	<?php if( isset($mscripts) and is_array($mscripts)) { 
	foreach( $mscripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
	<?php endforeach; } ?>
	
	<script>
		function view_event(a,b,c){
	
			url =base_url+"event/get_data_event/"+a+"/"+b+"/"+c;
			
			$.ajax({
				type: "POST",
				 dataType: "HTML",				 
				 url: url,
					success:function(msg, textStatus, jqXHR) {
					$("#view-event").show();
					$("#view-event").html(msg);
					switch(a){
						case 'peserta':
							$("#profile").show();
							$("#dokumen").hide();
							$("#tagihan").hide();
						break;
						case 'abstrak':
							$("#dokumen").show();
							$("#abstrak").show();
							$("#paper").hide();
							$("#revisi").hide();
							$("#profile").hide();
							$("#tagihan").hide();
						break;
						case 'paper':
							$("#dokumen").show();
							$("#paper").show();
							$("#abstrak").hide();
							$("#revisi").hide();
							$("#profile").hide();
							$("#tagihan").hide();
						break;
						case 'revisi':
							$("#dokumen").show();
							$("#revisi").show();
							$("#paper").hide();
							$("#abstrak").hide();
							$("#profile").hide();
							$("#tagihan").hide();
						break;
						case 'tagihan':
							$("#profile").hide();
							$("#dokumen").hide();
							$("#tagihan").show();
						break;
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert ('Failed!');     
				  
				},
				cache: false,
				contentType: false,
				processData: false
			});
			e.preventDefault(); //STOP default action
			return false;
		}
		
		function upload_file(jenis){
			$("#view-form-file").show();
			$("#hidval").val(jenis);
			if(jenis=='paper')	$("#judul").html("Upload Paper File");
			if(jenis=='abstrak') $("#judul").html("Upload Abstract File");
			if(jenis=='revisi')	$("#judul").html("Upload Revision File");
			$("#file").focus();
		}
		
		function cancelForm(){
			$("#form-file")[0].reset();
			$("#view-form-file").hide();
		}
		
			
		$("#form-file").submit(function (e) {
			var postData = new FormData($('#form-file')[0]);		
			$.ajax({
				type: "POST",
				url: base_url+"event/submit_file",
				data: postData,
				success:function(data, textStatus, jqXHR) {
					alert(data);
					$("#form-file")[0].reset();
					if(data=='Success'){
						$("#view-form-file").hide();
						$("#view-show").load();
						location.reload(); 
						}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert ('Failed!');     
				  
				},
				cache: false,
				contentType: false,
				processData: false
			});
			e.preventDefault(); //STOP default action
			return false;
		});
		
		
	</script>

       
	<script>
       document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
           //alert('No F-12');
            return false;
        }
    }
    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
    document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode == 123) {
            //alert('No F-keys');
            return false;
        }
    }
	var message="";
	function clickIE() {if (document.all) {(message);return false;}} function clickNS(e) {if (document.layers||(document.getElementById&&!document.all)) { if (e.which==2||e.which==3) {(message);return false;}}} if (document.layers) {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;} else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;} document.oncontextmenu=new Function("return false") 
	</script>
     <script>
		function search(e) {
			var nilai = $("#searchTxt").val();	
		    if (e.keyCode == 13 && nilai != '') {
		    	// console.log(nilai);
		    	search_link(nilai);
		        return false;
		    }
		}
		
		function search_link(){
			document.getElementById("main-search-form").submit();
		}
		
		$(document).ready(function() {					
			$(".with-tooltip").tooltip({trigger: "hover focus", container: "body"});
				if ($(".menu-navigation-slider").length) {
					var $menunav = $('.menu-navigation-slider').owlCarousel({
							items: 3,
							autoPlay: false,
							pagination: false,
							theme: "slider-theme"
						});
					$(".menu-navigation-next").click(function (e) {
						e.preventDefault();
						$menunav.trigger('owl.next');
					});
				}

				if ($("#slider").length) {
					var $owl = $("#slider")
					$owl.owlCarousel({
						// navigation : true,
						singleItem: true,
						transitionStyle: "fade",
						autoPlay: true,
						stopOnHover: true,
						pagination: false,
						autoHeight: true,
						theme: "slider-top-theme"
					});
					$(".slide-next").click(function (e) {
						e.preventDefault();
						$owl.trigger('owl.next');
					});
					$(".slide-prev").click(function (e) {
						e.preventDefault();
						$owl.trigger('owl.prev');
					});
				}
				$('.search-button').click(function (e) {
					e.preventDefault();
					$('.nav-form-nav').addClass('focus').delay(100);
					$("input[name=searchTxt]").focus();
				});

				$("input[name=searchTxt]").focus(function () {
					$('.nav-form-nav').addClass('focus');
				});

				$("input[name=searchTxt]").blur(function () {
					$('.nav-form-nav').removeClass('focus');
				});
				$('.nav-main').affix({
					offset: {
						top: $(".nav-main").position().top
					}
				});
				if ($("#insideptiik").length) {
					var owl = $("#insideptiik");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						theme: "slider-theme"
					});
				}
				if ($("#insidelabslide").length) {
					var owl = $("#insidelabslide");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						theme: "slider-theme"
					});
				}
				if ($(".laboratory-list-slide").length) {
					var owl = $(".laboratory-list-slide");

					owl.owlCarousel({
						items: 4,
						autoPlay: true,
						stopOnHover: true,
						autoHeight: true,
						theme: "slider-theme"
					});
				}
				
				 if ($(".mixit-content").length){
					if(typeof $typemix !== 'undefined' && $typemix=="profile"){
						$.each($abjadmix,function(e,f){
							$(".mixit-content").append('<div class="'+classAbjadText+' '+f+'"><h4>'+f+'</h4></div>');
						});
						$.each($abjadmix,function(e,f){
							  		$(".isi-kategori."+f).insertAfter(".abjad-kategori."+f);
							  });
					}
				}
				
				 if ($(".mixit-content").length) {
					var mix = $(".mixit-content");

					mix.mixItUp({
						
    					multiFilter: true,
    					controls: {
							live: true
						},
						animation: {
							effects: 'fade stagger(10ms) scale',
							staggerSequence: function(i){
								return i % 4;
							}
						},
						callbacks: {
						onMixEnd: function(state){
							state.$show.addClass("activeshow");
							state.$hide.removeClass("activeshow");
							if ( typeof $abjadmix !== 'undefined' && typeof $typemix !== 'undefined' && $typemix=="profile") {
							  $.each($abjadmix,function(e,f){
							  	count = $(".activeshow.isi-kategori."+f).length;
							  	if(count<1){
							  		$(".abjad-kategori."+f).slideUp(500);
							  	}
							  });
							}
						}	
						}
					});
				}
				
				if ($(".fancybox").length) {
					$(".fancybox")
					.attr('rel', 'gallery')
					.fancybox({
						helpers : 	{
										media : {},
										 title: {
											type: 'inside',
											position: 'bottom'
										}
									},
						beforeShow: function () {
							/* Disable right click */
							$.fancybox.wrap.bind("contextmenu", function (e) {
									return false; 
							});
						}
					});
				}
				
				$('.tools-color .show-tools').click(function (e) {
					e.preventDefault();
					if ($('.tools-color').hasClass('active')) {
						$('.tools-color').css('right', '').removeClass('active');
					} else {
						$('.tools-color').css('right', '0').addClass('active');
					}
				});
				
				$('.tools .skins a').click(function (e) {
					e.preventDefault();
					var data = $(this).data('styleskin');
					var name = "skin-switch";
					  $.cookie(name, data, { expires: 365 ,path:"/"});
					  window.location.href = "";
				});
				$('.tools .clear-skin').click(function (e) {
					e.preventDefault();
						var name = "skin-switch";
						var data="ub";
					  $.removeCookie(name, { path: '/' });
					  window.location.href = "";
					  /*$.cookie(name, data, { expires: 365 ,path:"/"});
					  window.location.href = "";*/
				});
				
				$('.tools .colors a').click(function (e) {
					e.preventDefault();
					var data = $(this).data('stylecolor');
					var name = "stylecolor";
					  $.cookie(name, data, { expires: 365 ,path:"/"});
					  window.location.href = "";
				});
				$('.tools .clear-color').click(function (e) {
					e.preventDefault();
						var name = "stylecolor";
					  $.removeCookie(name, { path: '/' });
					  window.location.href = "";
				});
				
				
				$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
					// Avoid following the href location when clicking
					event.preventDefault(); 
					// Avoid having the menu to close when clicking
					event.stopPropagation(); 
					// If a menu is already open we close it
					$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
					// opening the one you clicked on
					$(this).parent().addClass('open');
				});
			});

							
	 $(".lang-switch").click(function(e){
			e.preventDefault();
	        var lang = $(this).data("lang");
	        var name = "lang-switch";
	          $.cookie(name, lang, { expires: 365 ,path:"/"});
	          window.location.href = "";
      });
	  
	  $(document).ready(function(){
	  	<?php if(isset($url)): ?> var url_ = base_url + 'unit/search_json/<?php if(isset($search)) echo $search ?>/<?php echo $unit ?><?php if(isset($url)) echo "/".$url ?>';
		<?php else: ?> var url_ = base_url + 'page/search_json/<?php if(isset($search)) echo $search ?>/<?php echo $unit ?><?php if(isset($url)) echo "/".$url ?>';
		<?php endif; ?>
		
		//alert(url_);
		$.ajax({
	        url : url_,
	        type: "POST",
	        dataType : "json",
	        success:function(msg) 
	        {
	        	konten = msg;
	        	get_konten('');
	        	// console.log(konten);
	        }
	    });
	});
	
	$(".tube-title-link").click(function(){
			var id = $(this).data("list");
			var lang = $(this).data("lang");
			var unit = $(this).data("unit");
			if(unit!=0){
				url = base_url + "/read/read_gallery";
			}else{
				url = base_url + "/page/read_gallery";
			}
				//alert("aa"+ url);
			$.ajax({
					type : "POST",
					  dataType: "HTML",
					  url: url,
					data : $.param({
						id : id,lang:lang, unit:unit
					}),
					success : function(data) {
						$('#content-video').html(data);
					}
			}); 
		});		
					
        </script>
		
		
       </body>
</html>
