<?php
	if(isset($inf_peserta)):
	?>
		<div id="profile">
			<h3>Profile</h3>
			<div class="well content content-white">
			<h1><?php echo $inf_peserta->title.". ".$inf_peserta->nama ?> <small><?php echo $inf_peserta->kategori_peserta ?></small></h1>
			<i class="fa fa-envelope-o"></i> <?php echo $inf_peserta->email ?> &nbsp; <i class="fa fa-map-marker"></i> <?php echo $inf_peserta->alamat.", ". $inf_peserta->negara?>
			<?php
			
			?>
			</div>
		</div>
	<?php
	endif;
	
	$arr_data=array();
	$status = array('0'=>'Submitted', '1'=>'Accepted', '2'=>'Under Review', '3'=>'Rejected');
	
	if(isset($inf_dokumen)):
		foreach($inf_dokumen as $key):
			switch($key->is_valid){
				case '0':
					$status = '<span class="text text-info">Submitted</span>';
				break;
				case '1':
					$status = '<span class="text text-success">Accepted</span>';
				break;
				case '2':
					$status = '<span class="text text-warning">Under Review</span>';
				break;
				case '3':
					$status = '<span class="text text-danger">Rejected</span>';
				break;
			}
			
			$arr_temp = array("kategori"=>$key->kategori_dokumen, "judul"=>$key->file_name, "lokasi"=>$key->file_loc, "is_valid"=>$key->is_valid, "status"=>$status, "catatan"=>$key->keterangan, 'tgl_upload'=>$key->tgl_upload, 'id'=>$key->id);
			array_push($arr_data, $arr_temp);
		endforeach;
	endif;
	?>
		<div id="dokumen">
			<div id="abstrak">
				<h3>Abstract</h3>
				<div class="well content content-white">
				<table class="table">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Name</th>
							<th>Status</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$j=0;
					for($i=0;$i<count($arr_data);$i++){														
						if($arr_data[$i]['kategori']=='abstrak'){
						$j++;
						?>
						<tr id ="<?php echo "abstrak".$arr_data[$i]['id'] ?>">
							<td><?php echo $j ?></td>
							<td><a href="<?php echo $this->config->file_url_view."/".$arr_data[$i]['lokasi']; ?>" target="_blank"><?php echo $arr_data[$i]['judul']; ?></a><br><small><i class="fa fa-clock-o"></i> <?php echo date("M d, Y H:i", strtotime($arr_data[$i]['tgl_upload'])) ?></small></td>
							<td><?php echo $arr_data[$i]['status']; ?></td>
							<td><?php if($arr_data[$i]['is_valid']=='0'){?><a href="javascript::" class="delete_dok" data-jenis="abstrak" data-dok="<?php echo $arr_data[$i]['id'] ?>"><i class="fa fa-trash-o"></i> Delete</a><?php } ?></td>
						</tr>
						<?php
						}
					}
					?>
					</tbody>
				</table>
				</div>
			</div>
			
			<div id="paper">
				<h3>Paper</h3>
				<div class="well content content-white">
				<table class="table">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Name</th>
							<th>Status</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$j=0;
					for($i=0;$i<count($arr_data);$i++){														
						if($arr_data[$i]['kategori']=='paper'){
						$j++;
						?>
						<tr id ="<?php echo "paper".$arr_data[$i]['id'] ?>">
							<td><?php echo $j ?></td>
							<td><a href="<?php echo $this->config->file_url_view."/".$arr_data[$i]['lokasi']; ?>" target="_blank"><?php echo $arr_data[$i]['judul']; ?></a><br><small><i class="fa fa-clock-o"></i> <?php echo date("M d, Y H:i", strtotime($arr_data[$i]['tgl_upload'])) ?></small></td>
							<td><?php echo $arr_data[$i]['status']; ?></td>
							<td><?php if($arr_data[$i]['is_valid']=='0'){
								?><a href="javascript::" class="delete_dok" data-jenis="paper" data-dok="<?php echo $arr_data[$i]['id'] ?>"><i class="fa fa-trash-o"></i> Delete</a><?php 
								} ?></td>
						</tr>
						<?php
						}
					}
					?>
					</tbody>
				</table>
				</div>
			</div>
			
			<div id="revisi">
				<h3>Revision</h3>
				<div class="well content content-white">
				<table class="table">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th>Name</th>
							<th>Status</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php
					$j=0;
					for($i=0;$i<count($arr_data);$i++){														
						if($arr_data[$i]['kategori']=='revisi'){
						$j++;
						?>
						<tr id ="<?php echo "revisi".$arr_data[$i]['id'] ?>">
							<td><?php echo $j ?></td>
							<td><a href="<?php echo $this->config->file_url_view."/".$arr_data[$i]['lokasi']; ?>" target="_blank"><?php echo $arr_data[$i]['judul']; ?></a><br><small><i class="fa fa-clock-o"></i> <?php echo date("M d, Y H:i", strtotime($arr_data[$i]['tgl_upload'])) ?></small></td>
							<td><?php echo $arr_data[$i]['status']; ?></td>
							<td><?php if($arr_data[$i]['is_valid']=='0'){ ?><a href="javascript::" class="delete_dok" data-jenis="revisi" data-dok="<?php echo $arr_data[$i]['id'] ?>"><i class="fa fa-trash-o"></i> Delete</a><?php }?></td>
						</tr>
						<?php
						}
					}
					?>
					</tbody>
				</table>
				</div>
			</div>
		</div>
	<script>
	$(".delete_dok").click(function(){
		
		var x = confirm("Are you sure?");
		if(x){
			var id = $(this).data("dok");
			var jenis = $(this).data("jenis");
			$.ajax({
				type : "POST",
				dataType : "html",
				url : base_url + 'event/delete_dok',
				data : $.param({
					id : id,
					jenis : jenis
				}),
				success : function(msg) {
					alert(msg);
					$("#"+jenis+id).fadeOut();					
				}
			});
		}
	});			
		
	</script>