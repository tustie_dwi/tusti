<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>International Symposium on Geoinformatics 2015</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?php echo $this->asset("isyg/css/swiper.min.css"); ?>" rel="stylesheet">
		<link href="<?php echo $this->asset("isyg/css/bootstrap.min.css"); ?>" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="<?php echo $this->asset("isyg/css/jquery.smartmenus.bootstrap.css");?>" />
		<link href="<?php echo $this->asset("isyg/css/style.css"); ?>" rel="stylesheet">
		
		 <?php $styles = $this->get_styles(); 
				if(is_array( $styles )) : 
				
				foreach($styles as $s) : 
			?><link href="<?php echo $this->asset($s); ?>" rel="stylesheet">
			<?php endforeach; endif; ?>
			<?php if( isset($mstyles) and is_array($mstyles)) { 
			foreach( $mstyles as $s) : ?><link href="<?php echo $this->location($s); ?>" rel="stylesheet">
			<?php endforeach; } ?>
			
	</head>
	<body>
		<header class="navbar navbar-fixed-top nav-custom">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle collapsed" data-target=".navbar-collapse" data-toggle="collapse" type="button">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $this->location('event/'.$kevent); ?>">ISYG</a>
				</div>
				<div class="navbar-collapse navbar-right collapse">
					
					<ul class="nav nav navbar-nav" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
						 <li><a itemprop="url" href="<?php echo $this->location('event/'.$kevent); ?>" title="Home"><span class="fa fa-home"></span> <span class="sr-only" itemprop='name'>Home</span> Home</a></li>
						<?php
						if($page):							
							foreach($page as $dt):
								$child = $mpage->read_sub_content($unit,$lang, "", $dt->id, $eventid);
								if($child):
									?>
									  <li class="dropdown">
											<a rel="alternate" hreflang="x-default" href="#"  ><?php echo $dt->content_title?></a>
											<?php
												$i=0;
												 $count=round(count($child)/2);
												 $parent=0;
											?>		
											 <ul class="dropdown-menu" role="menu">														
												 <?php 													 
												 foreach ($child as $key):
													 $i++;
													if(isset($unit_list)):
														read_sub_menu($eventid, $i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location($url) ,$key->content_data);
													else:
														read_sub_menu($eventid, $i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location('event/'.$kevent),$key->content_data );
													endif;
												 endforeach; ?>
											</ul>
											
									</li>
									<?php	
								else:
									echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location('event/'.$kevent.'/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
								endif;
							endforeach;
						endif;
						?>
					</ul>
				</div>
			</div>
			<?php
			function read_sub_menu($eventid=NULL, $i,$count,$parent,$unit=NULL, $lang=NULL, $mpage=NULL, $id=NULL, $title=NULL, $content_page=NULL,$url_menu=NULL){												
				$child = $mpage->read_sub_content($unit, $lang, "", $id, $title, $eventid);			
				if($child):
					?>
					
					 <li>
							<a href="#" ><?php echo $title?></a>						
							<ul class="dropdown-menu" role="menu">
							 <?php 
							 $parent=1;
							 foreach ($child as $key):
									read_sub_menu($eventid, $i,$count,$parent,$unit, $lang, $mpage,$key->id,$key->content_title,$key->content_page, $url_menu);
							 endforeach; 
							 ?>
							</ul>
					</li>
					
					<?php
				else:
					if(isset($unit_list)):
						echo "<li><a itemprop='url' href=".$this->location($url.'/read/'.$content_page.'/'.$id)." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					else:
						echo "<li><a itemprop='url' href=".$url_menu.'/read/'.$content_page.'/'.$id." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					endif;					
				endif;
			}

			?>
		</header>
		<section class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide blue">Slide 1</div>
				<div class="swiper-slide green">Slide 2</div>
				<div class="swiper-slide red">Slide 3</div>
				<div class="swiper-slide gray">Slide 4</div>
				<div class="swiper-slide white">Slide 5</div>
				<div class="swiper-slide black">Slide 6</div>
				<div class="swiper-slide purple">Slide 7</div>
				<div class="swiper-slide blue">Slide 8</div>
				<div class="swiper-slide gray">Slide 9</div>
				<div class="swiper-slide purple">Slide 10</div>
			</div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
		</section>
		<div class="container">
			<div class="row">
				<div class="wrapper">
				
				<?php if($about):
						foreach($about as $dt):
						?>
						<div class="wrap-title">
							<h1 class="text-title"><?php	echo $dt->judul; ?></h1>
						</div>
						<div class="wrap-body">
						
							<?php
							echo $dt->keterangan;
							
							?>
						</div>
					<?php 
						endforeach;
					endif; ?>						
					
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">Keynote & Speaker</h1>
					</div>
					<div class="wrap-body">
						<div class="row">
							<div class="col-sm-3">
								<image class="img-responsive" src="http://adl.ptiik.ub.ac.id/fileupload/assets/upload/file/PTIIK/konten/2015-03/Penampilan salah satu grup perwakilan mahasiswa FILKOM.jpg" alt="" />
							</div>
							<div class="col-sm-3">
								<image class="img-responsive" src="http://adl.ptiik.ub.ac.id/fileupload/assets/upload/file/PTIIK/konten/2015-03/Penampilan salah satu grup perwakilan mahasiswa FILKOM.jpg" alt="" />
							</div>
							<div class="col-sm-3">
								<image class="img-responsive" src="http://adl.ptiik.ub.ac.id/fileupload/assets/upload/file/PTIIK/konten/2015-03/Penampilan salah satu grup perwakilan mahasiswa FILKOM.jpg" alt="" />
							</div>
							<div class="col-sm-3">
								<image class="img-responsive" src="http://adl.ptiik.ub.ac.id/fileupload/assets/upload/file/PTIIK/konten/2015-03/Penampilan salah satu grup perwakilan mahasiswa FILKOM.jpg" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">ISYG 2015 News</h1>
					</div>
					<div class="wrap-body">
						<div class="row">
							<?php
								if(isset($news) && ($news) ){
									foreach ($news as $dt):
										$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	

										if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
										else $imgthumb = $this->config->default_thumb_web;
													
										?>

										<div class="col-sm-4 news">
											<h3 class="text-title"><?php echo $mpage->content($dt->judul,5);?></h3>
											<div><?php echo $mpage->content($dt->keterangan,20);?> <a href="<?php echo $this->location('event/'.$kevent.'/read/news/'.$content.'/'.$dt->id); ?>" class="read-more font-orange">more..</a>
											</div>
										</div>						
									<?php
									endforeach;
								}
								?>	
						
							
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">Our Sponsor</h1>
					</div>
					<div class="wrap-body">
						<div class="flex">
							<?php
							if(isset($kerjasama) && $kerjasama){
								foreach($kerjasama  as $dt):
									if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
									else $imgthumb = $this->config->default_thumb_web;
									
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
									
									
									if (isset($url)) $link = $this->location($url.'/read/kerjasama/'.$content.'/'.$dt->id);
									else $link = $this->location('page/read/kerjasama/'.$content.'/'.$dt->id);
									?>
									
									<div class="flex-cell text-center">
											<img src="<?php echo $imgthumb; ?>" class="img-responsive img-responsive-center" alt="<?php echo $dt->judul ?>">
										</div>	
							
									<?php
								endforeach;
							} ?>
							
							
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="wrapper">
					<div class="wrap-title">
						<h1 class="text-title">Join Us</h1>
					</div>
					<div class="wrap-body">
						<div class="text-center">
							<h1>We're almost ready, are you?</h1>
							<h3><a href="#" class="font-orange">Submit Paper</a></h3>
						</div>
					</div>
				</div>
			</div>
			<footer class="row">
				<div class="col-sm-6">
					<p>International Symposium on Geoinformatics 2015</p>
				</div>
				<div class="col-sm-6">
					<div class="text-right">
						<p>FILKOM</p>
					</div>
				</div>
			</footer>
		</div>
		
		<script src="<?php echo $this->asset("isyg/js/jquery.min.js"); ?>"></script>
		<script src="<?php echo $this->asset("isyg/js/bootstrap.min.js"); ?>"></script>
			<script type="text/javascript" src="<?php echo $this->asset("isyg/js/jquery.smartmenus.min.js");?>"></script>
		<script type="text/javascript" src="<?php echo $this->asset("isyg/js/jquery.smartmenus.bootstrap.min.js");?>"></script>
		<script src="<?php echo $this->asset("isyg/js/classie.js"); ?>"></script>
		<script src="<?php echo $this->asset("isyg/js/swiper.jquery.min.js"); ?>"></script>
		
		<?php $scripts = $this->get_scripts(); 
		foreach( $scripts as $s) : ?><script src="<?php echo $this->asset($s); ?>"></script>
		<?php endforeach; ?>
		<?php if( isset($mscripts) and is_array($mscripts)) { 
		foreach( $mscripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
		<?php endforeach; } ?>
	
		
		<script>
			function init() {
				window.addEventListener('scroll', function(e){
					var distanceY = window.pageYOffset || document.documentElement.scrollTop,
						shrinkOn = document.querySelector(".swiper-container").offsetTop,
						header = document.querySelector("header");
					if (distanceY > shrinkOn) {
						classie.add(header,"smaller");
					} else {
						if (classie.has(header,"smaller")) {
							classie.remove(header,"smaller");
						}
					}
				});
			}
			window.onload = init();
			
			$(document).ready(function(){
				var swiper = new Swiper('.swiper-container', {
					pagination: '.swiper-pagination',
					paginationClickable: true,
					loop: true,
					autoplay: 6000
				});
			});
		</script>
	</body>
</html>