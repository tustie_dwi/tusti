<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>International Symposium on Geoinformatics 2015</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?php echo $this->asset("isyg/css/swiper.min.css"); ?>" rel="stylesheet">
		<link href="<?php echo $this->asset("isyg/css/bootstrap.min.css"); ?>" rel="stylesheet">
		<link href="<?php echo $this->asset("isyg/css/style.css"); ?>" rel="stylesheet">
		
		 <?php $styles = $this->get_styles(); 
				if(is_array( $styles )) : 
				
				foreach($styles as $s) : 
			?><link href="<?php echo $this->asset($s); ?>" rel="stylesheet">
			<?php endforeach; endif; ?>
			<?php if( isset($mstyles) and is_array($mstyles)) { 
			foreach( $mstyles as $s) : ?><link href="<?php echo $this->location($s); ?>" rel="stylesheet">
			<?php endforeach; } ?>
			
	</head>
	<body>
		<header class="navbar navbar-fixed-top nav-custom">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle collapsed" data-target=".navbar-collapse" data-toggle="collapse" type="button">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $this->location('event/'.$kevent); ?>">ISYG</a>
				</div>
				<div class="navbar-collapse navbar-right collapse">
					
					<ul class="nav menu nav navbar-nav" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
						 <li><a itemprop="url" href="<?php echo $this->location('event/'.$kevent); ?>" title="Home"><span class="fa fa-home"></span> <span class="sr-only" itemprop='name'>Home</span> Home</a></li>
						<?php
						if($page):							
							foreach($page as $dt):
								$child = $mpage->read_sub_content($unit,$lang, "", $dt->id);
								if($child):
									?>
									  <li class="dropdown">
											<a rel="alternate" hreflang="x-default" href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" >sdsd<?php echo $dt->content_title?></a>
											<?php
												$i=0;
												 $count=round(count($child)/2);
												 $parent=0;
											?>		
											 <ul class="dropdown-menu" role="menu">														
												 <?php 													 
												 foreach ($child as $key):
													 $i++;
													if(isset($unit_list)):
														read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location($url) ,$key->content_data);
													else:
														read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id, $key->content_title,$key->content_page, $this->location('page'),$key->content_data );
													endif;
												 endforeach; ?>
											</ul>
											
									</li>
									<?php	
								else:
									echo "<li><a rel='alternate' hreflang='x-default' href=".$this->location('event/'.$kevent.'/read/'.$dt->content_page.'/'.$dt->id)." itemprop='url' ><span itemprop='name'>".$dt->content_title."</span></a></li>";
								endif;
							endforeach;
						endif;
						?>
					</ul>
				</div>
			</div>
			<?php
			function read_sub_menu($i,$count,$parent,$unit=NULL, $lang=NULL, $mpage=NULL, $id=NULL, $title=NULL, $content_page=NULL,$url_menu=NULL){												
				$child = $mpage->read_sub_content($unit, $lang, "", $id, $title);			
				if($child):
					?>
					
					 <li class="menu-item dropdown dropdown-submenu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $title?></a>						
							<ul class="dropdown-menu">
							 <?php 
							 $parent=1;
							 foreach ($child as $key):
									read_sub_menu($i,$count,$parent,$unit, $lang, $mpage,$key->id,$key->content_title,$key->content_page, $url_menu);
							 endforeach; 
							 ?>
							</ul>
					</li>
					
					<?php
				else:
					if(isset($unit_list)):
						echo "<li><a itemprop='url' href=".$this->location($url.'/read/'.$content_page.'/'.$id)." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					else:
						echo "<li><a itemprop='url' href=".$url_menu.'/read/'.$content_page.'/'.$id." title='".$title."'><span itemprop='name'>".$title."</span></a></li>";
					endif;					
				endif;
			}

			?>
		</header>
		
		<div class="container">			
			<div class="row">
				<?php		
				if(isset($detail)):	
				
					
				if($detail->keterangan)	$strcontent	= $detail->keterangan;
				else $strcontent = $detail->keterangan_ori;	
			
						?>
					<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no" itemprop="name"><?php if($detail->judul) echo $detail->judul; else echo $detail->judul_ori;
				
					?></h1>
						
					</section>
					
				
					<section itemprop="articleBody">
					<?php
					echo $mpage->str_content($strcontent,$this->config->file_url_view,$this->config->file_url_old); 
					
					if(isset($detail->content_data)):
						if(isset($url)) $url_link=$url;
						else $url_link = "";
						$this->read_data($detail->content_data, $url_link);
					endif;
					?>
					<div class="content-pane-tube text-right margin-bottom-sm">                						
						<div class="tube-share-button">
						<h4>
							<span class="fa fa-share-alt" title="Share"></span> <small>Share</small>
							<a class="fb" href="javascript::" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','fbshare','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-facebook-square"></span></a>
							<a class="twitter" href="javascript::" onclick="window.open('https://twitter.com/intent/tweet?text=Judul&url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>&via=PTIIK_UB','twt','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-twitter-square"></span></a>
							<a class="gplus" href="javascript::" onclick="window.open('https://plus.google.com/share?url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','gp','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-google-plus-square"></span></a>
							</h4>
						</div>
					</div>
					</section>
					<?php
					

				?>
				</section>
				<!-- End Edit Content Article -->
				<?php
				
				
			
			endif;	
			?>
				</div>
			</div>
			
			<footer class="row">
				<div class="col-sm-6">
					<p>International Symposium on Geoinformatics 2015</p>
				</div>
				<div class="col-sm-6">
					<div class="text-right">
						<p>FILKOM</p>
					</div>
				</div>
			</footer>
		</div>
		
		<script src="<?php echo $this->asset("isyg/js/jquery.min.js"); ?>"></script>
		<script src="<?php echo $this->asset("isyg/js/bootstrap.min.js"); ?>"></script>
		<script src="<?php echo $this->asset("isyg/js/classie.js"); ?>"></script>
		<script src="<?php echo $this->asset("isyg/js/swiper.jquery.min.js"); ?>"></script>
		
		<?php $scripts = $this->get_scripts(); 
		foreach( $scripts as $s) : ?><script src="<?php echo $this->asset($s); ?>"></script>
		<?php endforeach; ?>
		<?php if( isset($mscripts) and is_array($mscripts)) { 
		foreach( $mscripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
		<?php endforeach; } ?>
	
		
		<script>
			function init() {
				window.addEventListener('scroll', function(e){
					var distanceY = window.pageYOffset || document.documentElement.scrollTop,
						shrinkOn = document.querySelector(".swiper-container").offsetTop,
						header = document.querySelector("header");
					if (distanceY > shrinkOn) {
						classie.add(header,"smaller");
					} else {
						if (classie.has(header,"smaller")) {
							classie.remove(header,"smaller");
						}
					}
				});
			}
			window.onload = init();
			
			$(document).ready(function(){
				var swiper = new Swiper('.swiper-container', {
					pagination: '.swiper-pagination',
					paginationClickable: true,
					loop: true,
					autoplay: 6000
				});
			});
		</script>
	</body>
</html>