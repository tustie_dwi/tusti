<?php if($pemateri){ ?>
	<h3>Pemateri</h3>
	<table class='table table-bordered'>
			<tbody>
				<tr>
					<td width="20%"><em>Pemateri</em></td>
					<td><ul class="list-inline"><?php 
					$nama="";
					if(count($pemateri)!=0){
						$i=0;
						foreach($pemateri as $dt):
							$i++;
							$nama = $nama . $dt->nama .",";
							echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama;		
							if($dt->instansi){
								echo "<code>".$dt->instansi."</code>&nbsp;";
							}
							echo "</li> ";
						endforeach;
						
					}else{
						echo "-";
					}?></ul></td>
				</tr>							
			</tbody>
		</table>
	<?php
	}else{
		echo "<p><div class='alert alert-warning'>Sorry, no content to show</div></p>";
	}
				?>