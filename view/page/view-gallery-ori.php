<?php 
$this->view("page/header.php", $data); 
?>
<?php
	if($kategori=='video' || $kategori=='video-streaming'):
		?>
		 <section class="content content-theater">
            	<div class="container">
            		<h2 class="title-theater margin-top-no"><span class="fa fa-video-camera"></span> Live Streaming</h2>
                	<div id="liveStream" class="video-content splash-video"></div>

		<?php
	else:
	?>
  
		<section class="content content-white">
			<div class="container">   		

			 <!-- Main Content -->
				<div class="col-md-8">	
					<div class="menu-filter"> 
						<!--<span class="filter-by">Filter by:</span> -->
						<div class="menu-filter-box">
							<ul class="filters list-unstyled"> 
								<li class="active"><a href="#" class="filter" data-filter="all">All</a></li> 
								<li><a href="#a" class="filter" data-filter=".video">Video</a></li> 
								<li><a href="#s" class="filter" data-filter=".gallery">Gallery</a></li> 
							</ul> 
						</div>
					</div>
					<div class="content-filter">
						<div class="row">
							<div class="mixit-content">
								<?php
								if(isset($post) && ($post)){
									foreach($post as $key):
										if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
										else $imgthumb = $this->config->default_thumb_web;
									?>
										<div class="col-sm-4 col-xs-8 mix <?php echo $key->content_category; ?>">							
											<a href="<?php echo $key->file_data; ?>" data-fancybox-type="iframe" class="fancybox content-gallery" title="<?php echo $key->file_title ;?>"> 
												<div class="gallery-item"> 
													<div class="img-holder"> 
														<img alt="<?php echo $key->file_title ;?>" src="<?php echo $imgthumb ;?>" class="img img-responsive img-gallery"> 
													</div> 
													<div class="name-holder"><?php echo $key->file_title ;?>										
													</div> 
												</div> 
											</a>
										</div>
									<?php
									endforeach;
								}
								?>
								
							</div>
						</div>
					</div>

							
			</div>
			<!-- End Main Content -->
			<div class="col-md-4">
			<?php
				
					$this->view("page/sidebar.php",$data);
			?>
			 </div>
		<?php endif;?>
	</div>
</section>
	
<?php $this->view("page/footer.php", $data); ?>