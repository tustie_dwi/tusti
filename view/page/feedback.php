<?php $this->view("page/header.php", $data); ?>
<section class="content content-white">
	<div class="container">
		<div class="col-md-8">
			<h2 class="title-content margin-top-no margin-bottom-no">Feedback</h2>
				
			<div class="row  well">
				<form id="form-feedback-page">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">&nbsp;</label><br>
							<input type="email" class="form-control" required="required" name="feedback-email" id="feedback-email" placeholder="Email">
						</div>
						<div class="form-group">
							<label class="control-label">&nbsp;</label><br>
							<input type="password" class="form-control" required="required" name="feedback-password" id="feedback-password" placeholder="Password">
						</div>
						<div class="form-group">
							<label class="control-label">&nbsp;</label><br>
							<textarea class="form-control" required="required" name="feedback-content" id="feedback-content" placeholder="Message"></textarea>
						</div>
						<div class="form-group">
							<label class="control-label">&nbsp;</label><br>
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="button" class="btn btn-danger">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-4">
			<?php $this->view("page/sidebar.php",$data) ?>
		</div>
	</div>
</section>
<?php $this->view("page/footer.php", $data); ?>