<?php 
$this->view("page/header.php", $data); 
?>

<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
		<?php 
		if (isset($detail)):
		?>
			
					<!-- Edit Content Article -->
					<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no" itemprop="name"><?php if($kategori=='faq'){ echo 'Frequently Asked Questions'; }else{ if($detail->judul) echo $detail->judul; 
					else echo $detail->judul_ori; }
				
					?></h1>
						<!-- Edit Breadcrumb -->
					<!-- Edit Breadcrumb -->
					<ol class="breadcrumb breadcrumb-style margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'page/rekrutmen'); 
						else echo $this->location('page/rekrutmen'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Rekrutmen'; else echo "Rekrutment";?></span></a></li>
						<?php if(isset($detail) && (isset($detail->parent))):?>
							  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php 
							if(isset($url)):
								echo $this->location($url.'/read/'.$kategori.'/'.$detail->id);
							else:
								echo $this->location('page/read/'.$kategori.'/'.$detail->id);
							endif; ?>" itemprop="url"><span itemprop="title"><?php echo ucfirst($detail->parent);?></span></a></li>
						<?php  else : 
							if($kategori=='news'|| $kategori=='berita' || $kategori=='event'|| $kategori=='kerjasama'|| $kategori=='pengumuman'):
								?>
									<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php 
									if(isset($url)):
										echo $this->location($url.'/read/'.$kategori);
									else:
										echo $this->location('page/read/'.$kategori);
									endif; ?>" itemprop="url"><span itemprop="title"><?php 
									if($lang=='en'){
										switch($kategori){
											case 'pengumuman':
												echo "Announcement" ;
											break;
											case 'kerjasama':
												echo "Partnership" ;
											break;								
											default :
												echo ucfirst($kategori);
											break;
										}
										
									}else{
										if($kategori=='faq') echo 'FAQ';
										else echo ucfirst($kategori);
									} 									
									?></span></a></li>
									<?php 
								endif;
							endif; ?>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($detail->judul) echo $detail->judul;else echo $detail->judul_ori;	?></span></li>
					</ol>
					<!-- End Breadcrumb -->
					</section>
					<?php
		else:
		
	//	if(isset($post_event)):
			?>
				<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($kategori=='faq'){ 
					echo 'Frequently Asked Questions';
					} else{
						if($lang=='en'){
							switch($kategori){
								case 'pengumuman':
									echo "Announcement" ;
								break;
								case 'kerjasama':
									echo "Partnership" ;
								break;								
								default :
									echo ucfirst($kategori);
								break;
							}
							
						}else{
							if($kategori=='faq') echo 'FAQ';
							else echo ucfirst($kategori);
						} 
					}?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb breadcrumb-style margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
						
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php 
					  if($lang=='en'){
							switch($kategori){
								case 'pengumuman':
									echo "Announcement" ;
								break;
								case 'kerjasama':
									echo "Partnership" ;
								break;	
								case 'faq':
									echo "FAQ" ;
								break;									
								default :
									echo ucfirst($kategori);
								break;
							}
							
						}else{
							if($kategori=='faq') echo 'FAQ';
							else echo ucfirst($kategori);
						} 
						?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			<?php
		endif;
		?>
		</div>
	</div>
</section>

<div id="content-video"> 
	<section class="content content-white padding-top-sm">
		<div class="container">   				
		<!-- Main Content -->
			<div style="line-height:1.8" class="col-md-12">
				<?php
				if(isset($detail)):
				?>
					<section itemscope="" itemtype="http://schema.org/Article">
						<section itemprop="articleBody">
							<?php echo $detail->keterangan; 
							if(isset($success)) echo $success;
							
							$pdata = $mpage->get_master_kegiatan("","",$detail->id);
							if(!$pdata && !isset($success)){
								if(isset($err)) echo $err;								
								?>
								<div id="btn-apply"><a href="#" onClick="javascript:apply();" class="btn btn-lg btn-warning">Apply Now</a></div>
								<div id="form-apply" style="display:none">
									
								<?php
									$this->apply_form($mpage, $detail->kegiatan_id, $detail->id);
								?>
								</div>
								<?php
							}
							
							
							if($pdata):
								?>
								<h3><?php if($lang=='en') echo "Available Position";
								else echo "Lowongan"; ?></h3>
								<table class="table web-page">
									<thead>
										<tr><th style="display:none;border:0px;"></th><th><div class="col-md-3"><?php echo tgl ?></div><div class="col-md-3"><?php echo position ?></div></th></tr>
									</thead>
								<tbody>
								<?php
								foreach($pdata as $dt):
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
									
									if(isset($url)):
										if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
										else $url_content = $this->location($url.'/'.$kategori.'/apply/'.$content.'/'.$dt->id);
									else:
										if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
										else $url_content = $this->location('page/'.$kategori.'/apply/'.$content.'/'.$dt->id);
									endif;
									?>
									<tr>
										<td style="display:none;border:0px;"></td>
										<td>
											<div class="col-md-3"><?php echo date("M d, Y",strtotime($dt->tgl_mulai)) ." to ". date("M d, Y",strtotime($dt->tgl_selesai)) ; ?></div>
											<div class="col-md-6"><?php echo $dt->judul; ?></div>
											<div class="col-md-3"><a class="title-article" href="<?php echo $url_content; ?>">Detail</a></div>
										</td>
									</tr>
									<?php
								endforeach;
								?>
								</tbody>
								</table>
								<?php
							endif;
							?>
						</section>
					</section>
				<?php
				endif;
				
				if(isset($post_event)):
				?>
					<section itemscope="" itemtype="http://schema.org/Article">
						<section itemprop="articleBody">
							<table class="table web-page">
								<thead style="display:none">
									<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
								</thead>
								<tbody>
								<?php
								
									foreach($post_event as $dt): 									
										$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
										
										if(isset($url)):
											if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
											else $url_content = $this->location($url.'/'.$kategori.'/'.$content.'/'.$dt->id);
										else:
											if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
											else $url_content = $this->location('page/'.$kategori.'/'.$content.'/'.$dt->id);
										endif;
										
										/*if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
										else $imgthumb = $this->config->default_thumb_web;*/
										
									?>
										<tr style="border-top:0px;">
											<td style="display:none;border:0px;"></td>
											<td style="border-top:0px;">
												 <div class="media">
													<a class="title-article" href="<?php echo $url_content; ?>"><h3><?php if($dt->judul) echo $dt->judul;
													else echo $dt->judul_ori;
													?></a>&nbsp;<span class="label label-default"><?php echo $dt->unit ?></span>
													<time class="time-post_event"><small><?php if($lang=='in') echo date( 'd M Y',strtotime($dt->tgl_mulai));									
													else echo date( 'M d, Y',strtotime($dt->tgl_mulai));?></small></time></h3>
													
													 <div class="media-body">
														<p class="post_event-content text-justify">
															<?php
															$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); 
															$isi = substr($isi_berita, 0, 300); // ambil sebanyak 220 karakter
															$isi = substr($isi_berita, 0, strrpos($isi, " "));
															 echo htmlspecialchars($isi);
															?>
														</p>
													 </div>
												</div>
											</td>
										</tr>
									<?php
									endforeach;
								?>
								</tbody>
							</table>
						</section>
					</section>
				<?php
				endif;
				?>
			</div>
		</div>
	</section>
</div>

<?php 
$this->view("page/footer.php", $data); 
?>
<script>
function apply(){
	//alert('aa');
	$("#btn-apply").hide();
	$("#form-apply").show();
}
</script>
