				<?php
		if($detail){
			
			
				$detail->tgl_mulai 	 = date("Ymd", strtotime($detail->tgl_mulai));
				$detail->tgl_selesai = date("Ymd", strtotime($detail->tgl_selesai));
				
				$id = $detail->agenda_id;
				
				$data['pemateri']	= $mpage->get_peserta_agenda($id, 'pemateri','-');
				$data['undangan']	= $mpage->get_peserta_agenda($id, 'undangan');
				$data['staff']		= $mpage->get_peserta_agenda($id, 'peserta','staff');
				$data['peserta']	= $mpage->get_peserta_agenda($id, 'peserta','luar');
				$data['mhs']		= $mpage->get_peserta_agenda($id, 'peserta','mhs');
				$data['mangkatan']	= $mpage->get_peserta_by_group($id, 'peserta','mhs');
			if(isset($unit_list) && strtolower($unit_list->strata)=='s2'){
			
			}else{
			?>
				<section itemscope="" itemtype="http://schema.org/Article">
						
			<?php }  ?>
			<!--<article>
				<div class="row">-->
					
					<h3 class="margin-top-no">General Info</h3>
					<table class='table table-bordered'>
						<tbody>
							<tr>
								<td width="20%"><em><?php echo jenis_kegiatan ?></em></td>
								<td><?php if($detail->kegiatan){ echo $detail->kegiatan;} else{ echo "Lain-lain";} ?></td>
							</tr>
							
							<tr>
								<td width="20%"><em><?php echo penyelenggara ?></em></td>
								<td><?php echo $detail->penyelenggara;
								
								if($detail->unit){  echo " - ".$detail->unit;} ?></td>
							</tr>
							<tr>
								<td width="20%"><em><?php echo keterangan ?></em></td>
								<td><?php if($detail->keterangan){ echo $detail->keterangan;} else {echo "-";} ?></td>
							</tr>
							<tr>
								<td width="20%"><em><?php echo peserta ?></em></td>
								<td><?php if($detail->inf_peserta){ echo $detail->inf_peserta; } else{ echo "-"; } ?></td>
							</tr>
						</tbody>
					</table>
					
					<h3><?php echo pelaksanaan ?></h3>
					<table class='table table-bordered'>
						<tbody>
							<tr>
								<td width="20%"><em><?php echo tgl_kegiatan ?></em></td>
								<td><?php 
								if(date("M d, Y",strtotime($detail->tgl_mulai)) == date("M d, Y",strtotime($detail->tgl_selesai))){
										echo date("M d, Y",strtotime($detail->tgl_mulai));		
									
									}else{
										echo date("M d",strtotime($detail->tgl_mulai))." - ".date("d, Y",strtotime($detail->tgl_selesai));
									}
									echo "</span>&nbsp;<i class='fa fa-clock-o'></i> <span class='text text-warning'>".date("H:i",strtotime($detail->content_modified))." - ".
									date("H:i",strtotime($detail->content_modified_end))."</span>"; ?></td>
							</tr>
							<tr>
								<td width="20%"><em><?php echo lokasi ?></em></td>
								<td><?php if($detail->lokasi){echo $detail->lokasi; } else{ echo "-"; } ?></td>
							</tr>
							<tr>
								<td width="20%"><em><?php echo ruang ?></em></td>
								<td><?php if($detail->inf_ruang){echo "R.". $detail->inf_ruang;  } else{ echo "-"; } ?></td>
							</tr>
							
						</tbody>
					</table>
					<div class="col-md-12">						
						<div class="row">
					
						<ul class="nav nav-tabs">
							  <li class="active"><a href="#peserta" data-toggle="tab"><?php echo peserta ?></a></li>
							  <li><a href="#pemateri" data-toggle="tab"><?php echo pemateri ?></a></li>
							  <li><a href="#undangan" data-toggle="tab"><?php echo undangan ?></a></li>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
							  <div class="tab-pane active" id="peserta"><?php $this->view("page/peserta.php",$data);?></div>
							  <div class="tab-pane" id="pemateri"><?php $this->view("page/pemateri.php",$data);?></div>
							  <div class="tab-pane" id="undangan"><?php $this->view("page/undangan.php",$data);?></div>
							</div>
						</div>
					</div>
				<!--</div>
			</article>-->
			
			<?php				
		
		}
		?>
		
	