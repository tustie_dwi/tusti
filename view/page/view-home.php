

<div class="menu-navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 title-menu font-raleway uppercase">
                            <a href="<?php echo $this->location('info/services') ?>" class="more-service">
                            	<div class="temu"><?php echo temu_layanan ?></div>
                            	<div class="layanan"><?php echo layananmu ?></div>
                            </a>
                        </div>
                        <div class="col-md-10 content-slide">
                            <div class="row">
                                <div class="col-xs-10">
                                    <div class="menu-navigation-slider">
									<?php
									$apps = $mpage->read_content('0',$lang,'apps');
									
									if($apps):
									if(! isset($unit_list)):
										$strcolor="orange";
									else:
										if(isset($_COOKIE['stylecolor'])) $strcolor=$_COOKIE['stylecolor'];
										else $strcolor=$color;
									endif;
									
										foreach($apps as $dt):
											
											$str=explode('/',$dt->icon);
											$img_name = end($str);
											if($img_name) $img= $this->asset("ptiik/images/".$strcolor."/".$img_name);
											else $img= $this->asset("ptiik/images/".$strcolor."/akademik.png");
											?>
											<div class="slide">
												<a itemprop="url" rel="alternate" hreflang="x-default" href="<?php echo $this->location($dt->content_data);?>" class="menu-navigation-link with-tooltip" data-toggle="tooltip" data-placement="top" title="<?php if($dt->keterangan) echo $dt->keterangan; ?>">
													<span class="menu-navigation-detail" itemprop="name"><i class="fa fa-th-large"></i> <?php echo $dt->judul ?></span>
												</a>
											</div>
											<?php
										endforeach;
									endif;
									?>
                                                                   
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <a href="#" class="menu-navigation-next">
                                        <span class="fa fa-chevron-right"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<?php if(! isset($unit_list)): ?>
			 
			  <section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
						<section itemscope="" itemtype="http://schema.org/Article">
							<section itemprop="articleBody">
                    	<?php if($about):
								foreach($about as $dt):
						?>
								<h2 class="text-center title-content margin-top-sm margin-bottom-no font-raleway font-orange">
									<span><?php
									echo $dt->judul;
									?>
									</span>
								</h2>
								<div class="text-center margin-bottom-sm"></div>
								<div class="text-center margin-bottom" style="line-height:1.8">
									<?php
									echo $dt->keterangan;
									
									?>
								</div>
							<?php 
								endforeach;
							endif; ?>
								</section>
						</section>
                    </div>
                </div>
            </section>
			 
			 
            
            <section class="content content-dark">
                <div class="container">
                    <div class="col-md-12">
						<section itemscope="" itemtype="http://schema.org/Article">
							<section itemprop="articleBody">
								<h2 class="title-content text-center margin-top-sm margin-bottom-no font-raleway"><span><?php if($lang=='in') echo "Program Studi"; else echo "Department/Study Program";?></span></h2>
								
								<div class="row">
																	 
										<?php
											if($prodi):
												$i=0;
												foreach($prodi as $dt):
													$i++;
													if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
													else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));

													if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
													else $imgthumb = $this->config->default_thumb_web;	

													
													$strabout = $dt->about;
												?>		
												
												 <div class="col-sm-6">
															<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location('unit/prodi/'.strtolower($dt->kode)); ?>" class="prodi-link">
																<div class="row">
																	<div class="col-sm-3">
																		<figure>
																			<img src="<?php echo $imgthumb;  ?>" class="img-responsive img-responsive-center margin-top-sm" alt="Image Logo <?php if($dt->unit) echo ucWords($dt->unit);
																				else echo ucWords($dt->unit_ori);
																			?>">
																		</figure>
																</div>
																	<div class="col-sm-9">
																		<h3 class="margin-top-sm title-prodi font-raleway uppercase"><span itemprop="name"><?php if($dt->unit) echo ucWords($dt->unit);
																			else echo ucWords($dt->unit_ori);
																		?></span></h3>
																			<div class="prodi-text" style="overflow:hidden;text-overflow: ellipsis !important;"><?php if(strtolower($lang)=='in') echo $strabout; 
																			else echo $strabout;
																			?></div>
																	</div>
																</div>
																
																
															</a>
														</div>
														
												<?php
												//}
												endforeach;								
											endif;
											?>	
								</div>
							</section>
						</section>
                    </div>
                </div>
            </section>
			
			<section class="content content-white"  style="padding-bottom:0px !important;">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-no font-orange font-raleway uppercase"><span><?php echo berita ?></span></h2>
                        <div id="insideptiik">
							<?php
									if(isset($news) && ($news) ){
										foreach ($news as $dt):
											$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));									
											?>	
											<div>
												<div class="inside-box">
													<div class="content-inside-box">
														<?php 
														if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
														else $imgthumb = $this->config->default_thumb_web;
														?>
														<a href="<?php echo $this->location('page/read/news/'.$content.'/'.$dt->id); ?>" itemprop="url" rel="alternate" hreflang="x-default" >
														<img class="img-inside-box img-responsive" src="<?php echo $imgthumb;  ?>" alt="<?php echo $mpage->content($dt->judul,5);?>"></a>
														<a class="title-inside-box" href="<?php echo $this->location('page/read/news/'.$content.'/'.$dt->id); ?>"><h4><?php echo $mpage->content($dt->judul,5);?></h4></a>
														<div class="paragraph-inside-box"><?php echo $mpage->content($dt->keterangan,20);?></div>
														<a href="<?php echo $this->location('page/read/news/'.$content.'/'.$dt->id); ?>" class="more-inside-box">More</a>
													</div>
												</div>
											</div>
							
										<?php
										endforeach;
									}
									?>	
                        </div>
                        <div class="text-right"><a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location('page/read/news/'); ?>" class="more-link">More <?php echo berita ?></a></div>
                    </div>
                </div>
            </section>
			<?php else:		?>
			 <section class="content content-white about">
                <div class="container margin-bottom-no">
                    <div class="col-md-12">
						<?php 
						if($about): 
							foreach($about as $dt):
							?>
							<h2 class="title-content text-center font-orange margin-top-sm margin-bottom-sm font-raleway"><?php echo $dt->judul ?></h2>
							<div class="row">
							<?php echo $mpage->str_content($dt->keterangan,$this->config->file_url_view); ?></div>
							<!--<hr class="hr-orange hr-center margin-top">-->
							<?php 
							endforeach;
						else:
							if(isset($unit_list)):
								?>
								<h2 class="title-content text-center font-orange margin-top-sm margin-bottom-no font-raleway"><?php if($unit_list->unit) echo $unit_list->unit;
								else echo $unit_list->unit_ori;
								?></h2>
								<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet eros in leo pellentesque vestibulum. Pellentesque sed mi sed libero ultricies tincidunt ac vel lacus. Curabitur a eros finibus, ultricies quam ac, vulputate dolor. Donec augue ligula, scelerisque eu dictum vitae, tincidunt a felis. Nam at mollis leo, vel elementum velit. Donec accumsan ex mi, sit amet consectetur orci rhoncus euismod. In hac habitasse platea dictumst. Donec at ex nisi. Praesent vitae varius sem. Maecenas volutpat pretium dolor, a posuere ex commodo non.</p>
								<?php
							else:
							?>
								<h2 class="title-content text-center font-orange margin-top-sm margin-bottom-no font-raleway"><?php echo ptiik;
								?></h2>
								<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet eros in leo pellentesque vestibulum. Pellentesque sed mi sed libero ultricies tincidunt ac vel lacus. Curabitur a eros finibus, ultricies quam ac, vulputate dolor. Donec augue ligula, scelerisque eu dictum vitae, tincidunt a felis. Nam at mollis leo, vel elementum velit. Donec accumsan ex mi, sit amet consectetur orci rhoncus euismod. In hac habitasse platea dictumst. Donec at ex nisi. Praesent vitae varius sem. Maecenas volutpat pretium dolor, a posuere ex commodo non.</p>
							<?php
							endif;
						endif; ?>
					</div>
                </div>
            </section>
			<?php
			endif;
			
			
			if(! isset($unit_list)):
			?>
			 <section class="content content-dark">
                <div class="container">
                    <div class="col-md-6">
                        <h2 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm font-raleway uppercase"><span><?php echo pengumuman ?></span></h2>
                        <ul class="list-unstyled list-announcement">
							<?php
							if($pengumuman):
								foreach($pengumuman as $dt):
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	
								?>
								 <li <?php if($dt->is_sticky==1) echo 'class="sticky"' ?>>
										<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location('page/read/pengumuman/'.$content.'/'.$dt->id); ?>" title="<?php echo $dt->judul ?>">
											<div>
												<span class="fa fa-bookmark"></span>
												<span class="announcement-title">
													<?php echo $dt->judul ?>
												</span>
												<ul class="list-inline">
													<!--<li>Sticky</li>-->
													<li><time datetime="<?php echo date("Y-m-d H:i", strtotime($dt->content_modified));?>"><?php if($lang=='in') echo date("Y-m-d H:i", strtotime($dt->content_modified));
													else echo date("M d, Y H:i", strtotime($dt->content_modified));
													?></time></li>
													<li class="announcement-organization"><?php if($dt->unit_note) echo $dt->unit_note;
													else echo "PTIIK";?></li>
												</ul>
											</div>
										</a>
									</li>
								<?php
								endforeach;
						
							endif;
							?>

                        </ul>
						<div class="text-left"><a href="<?php echo $this->location('page/read/pengumuman/'); ?>" class="more-link">More <?php echo pengumuman ?></a></div>
                    </div>
                    <div class="col-md-6">
                        <h2 class="title-content title-underline title-underline-orange margin-top-sm margin-bottom-sm font-raleway uppercase"><span><?php echo kegiatan ?></span></h2>
                        <ul class="list-unstyled featured-event">
							<?php
							if($event):
								foreach($event as $dt):
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));	
								?>
									 <li>
										<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location('page/read/event/'.$content.'/'.$dt->id); ?>">
											<div class="row">
												<div class="col-xs-2 text-center event-left">
													<?php if($lang=='in'): ?>
													<div><?php 
													$hari = $mpage->get_hari(date("N", strtotime($dt->content_modified)));
													echo $hari; ?></div>
													<div><?php echo date("d M", strtotime($dt->content_modified)); ?></div>
													<?php else: ?>
													<div><?php echo date("D", strtotime($dt->content_modified)); ?></div>
													<div><?php echo date("M d", strtotime($dt->content_modified)); ?></div>
													<?php endif; ?>
												</div>
												<div class="col-xs-10 event-right">
													<div class="title-event">
														<?php if($dt->judul) echo $dt->judul;
														else echo $dt->judul_ori;
														?>
													</div>
													<ul class="list-inline">
														<li><?php echo lokasi ?> : <?php echo $dt->lokasi; ?></li>
														<li><?php echo waktu ?> : <?php echo date("H:i", strtotime($dt->content_modified)); ?></li>
														<!--<li class="event-organization"><?php //if($dt->unit) echo ucWords($dt->unit);
													//	else echo ucWords($dt->unit_ori);
														?></li>-->
													</ul>
												</div>
											</div>
										</a>
									</li>
								<?php
								endforeach;
						
							endif;
							?>								
                          
                        </ul>
						<div class="text-left"><a href="<?php echo $this->location('page/read/event/'); ?>" class="more-link">More <?php echo kegiatan ?></a></div>
                    </div>
                </div>
            </section>
			<?php
			else:
				if($unit_list->kategori=='laboratorium'):
			?>
				<section class="content content-black-patern">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-sm font-orange font-raleway uppercase"><span><?php echo informasi ?></span></h2>

                        <div id="insidelabslide">
							<?php
									if(isset($pengumuman) && ($pengumuman) ){
										foreach ($pengumuman as $dt):
											$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));									
											?>	
											
											
											<div>
												<div class="inside-lab">
													<div class="content-inside-box">
														<?php 
														if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
														else $imgthumb = $this->config->default_thumb_web;
														?>
														<a itemprop="url" rel="alternate" hreflang="x-default" href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" >
														<img class="img-inside-box img-responsive" src="<?php echo $imgthumb;  ?>"></a>
														<h4 class="title-inside-box"><?php echo $mpage->content($dt->judul,5);?></h4>
														<div class="paragraph-inside-box"><?php echo $mpage->content($dt->keterangan,20);?></div>
														<a href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" class="more-inside-box">More</a>
													</div>
												</div>
											</div>
							
										<?php
										endforeach;
									}else{
										if(isset($pengumuman_all) && ($pengumuman_all) ){
											foreach ($pengumuman_all as $dt):
											$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));									
											?>	
											
											
											<div>
												<div class="inside-lab">
													<div class="content-inside-box">
														<?php 
														if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
														else $imgthumb = $this->config->default_thumb_web;
														?>
														<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" >
														<img class="img-inside-box img-responsive" src="<?php echo $imgthumb;  ?>"></a>
														<h4 class="title-inside-box"><?php echo $mpage->content($dt->judul,5);?></h4>
														<div class="paragraph-inside-box"><?php echo $mpage->content($dt->keterangan,20);?></div>
														<a href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" class="more-inside-box">More</a>
													</div>
												</div>
											</div>
							
										<?php
										endforeach;
										}
									}
									?>	                      

                        </div>
                        <div class="text-right"><a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location($url.'/read/pengumuman/'); ?>" class="more-link">More <?php echo pengumuman ?></a></div>
                    </div>
                </div>
            </section>
			
			
			<?php
				else:
					if($unit_list->kategori=='riset'):
					?>
					<section class="content content-dark with-patern">
						<h2 class="text-center title-content margin-top-sm margin-bottom-no uppercase font-raleway font-orange"><span><?php echo berita_agenda ?></span></h2>
						<div class="content-text text-center margin-bottom"></div>
						<div class="container container-dark">
							<div class="row">
								<div class="col-md-6 dark-left">
								
									<?php
											if(isset($news) && ($news) ){
												foreach ($news as $dt):
													$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
													
													/*if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
													else $imgthumb = $this->config->default_thumb_web;*/
													$imgthumb = $this->asset("s2/images/new-icon.png");
																
													?>
													<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location($url.'/read/news/'.$content.'/'.$dt->id); ?>" class="content-news-event">
														<div class="row">
															<div class="col-xs-2">
																<img class="img-inside-box img-responsive" src="<?php echo $imgthumb; ?>" alt="<?php echo $mpage->content($dt->judul,5);?>">
															</div>
															<div class="col-xs-10">
																<div class="the-title uppercase font-raleway"><?php echo $mpage->content($dt->judul,5);?></div>
																<div class="content-text"><?php echo $mpage->content($dt->keterangan,20);?></div>
															</div>
														</div>
													</a>
									
													
									
												<?php
												endforeach;
											}
											?>
									
								
								</div>
								<div class="col-md-6">
									<?php
									if($event):
										foreach($event as $dt):
											$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
											//$imgthumb = $this->config->default_thumb_web;
											$imgthumb = $this->asset("s2/images/event-icon.png");
										?>
											<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location($url.'/read/event/'.$content.'/'.$dt->id); ?>" class="content-news-event">
												<div class="row">
													<!-- revisi 13/1/15 -->
													<div class="col-xs-2 text-right">
														<img class="img-inside-box img-responsive img-responsive-center" src="<?php echo $imgthumb; ?>">
													</div>
													<!--
													<div class="col-xs-2">
														<img class="img-inside-box img-responsive" src="<?php //echo $imgthumb; ?>">
													</div>
													-->
													<div class="col-xs-10">
														<div class="the-title uppercase font-raleway"><?php if($dt->judul) echo $dt->judul;
																else echo $dt->judul_ori;
																?></div>
														<div class="content-text"><?php echo lokasi ?> : <?php echo $dt->lokasi; ?>, <?php echo waktu ?> : <?php echo date("H:i", strtotime($dt->content_modified)); ?></div>
													</div>
												</div>
											</a>
											
										<?php
										endforeach;
								
									endif;
									?>
								</div>
							</div>
						</div>
								
					</section>

					<?php
					endif;
				endif;
			endif;
			
			if(isset($unit_list->kategori)=='riset'):
				?>
				<section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-sm font-orange font-raleway uppercase"><span><?php echo grup_riset ?></span></h2>
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="laboratory-list-slide  owl-carousel slider-theme">
										<?php
											if($group_riset):
												foreach($group_riset as $dt):
													if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
													else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));

													
													if($dt->icon):
														$str_ = explode("/", $dt->icon);
														//$imgthumb = $this->config->file_url_view."/upload/icons/unit-kerja/icon/white/".$str_[4]; 
														$imgthumb = $this->config->file_url_view."/".$dt->icon;
													else:
														//2014100026$imgthumb = $this->config->default_thumb_web;
														$imgthumb = $this->config->file_url_view."/upload/icons/unit-kerja/icon/2014100022.png";
													endif;
													//if($dt->is_riset=='1'):
														?>												
														
														 <div>
															<a itemprop="url" rel="alternate" hreflang="x-default"   href="<?php echo $this->location('unit/riset/'.strtolower($dt->kode)); ?>" class="link-to-laboratory col-md-3">
																<div class="text-center">
																	<div class="show-content">
																		<img src="<?php echo $imgthumb;?>" class="img-responsive img-responsive-center" alt="<?php if($dt->unit) echo ucWords($dt->unit);
																	else echo ucWords($dt->unit_ori);
																	?>">
																	</div>
																	<div class="hide-content ">
																		<?php if($dt->unit) echo ucWords($dt->unit);
																			else echo ucWords($dt->unit_ori);
																			?>
																	</div>
																	<div class="more-detail"></div>
																</div>
															</a>
														</div>
													<?php
													//endif;
												endforeach;
										
											endif;
											?>
										   </div>
									</div>
								</div>
							</div>
						</div>
					</section>
				<?php
			else:
			?>
			
			<!-- laboartorium list -->
			
			 <section class="content content-white">
                <div class="container">
                    <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange margin-top-sm font-orange font-raleway uppercase"><span><?php echo laboratorium ?></span></h2>
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="laboratory-list-slide  owl-carousel slider-theme">
									<?php
										if($lab):
											foreach($lab as $dt):
												if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
												else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));


												if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
												else $imgthumb = $this->config->default_thumb_web;
												?>												
										
																							
												<div>
													<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location('unit/lab/'.strtolower($dt->kode)); ?>" class="link-to-laboratory col-sm-3 col-xs-6">
														<div class="text-center">
															<div class="show-content">
																<img src="<?php echo $imgthumb;  ?>" class="img-responsive img-responsive-center" width="100" alt="<?php if($dt->unit) echo ucWords($dt->unit);
															else echo ucWords($dt->unit_ori);
															?>">
															</div>
															<div class="hide-content">
																<?php if($dt->unit) echo ucWords($dt->unit);
															else echo ucWords($dt->unit_ori);
															?>
															</div>
														</div>
													</a>
												</div>
											<?php
											endforeach;
									
										endif;
										?>						
                                    
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			<?php endif; ?>
			 			
             <section class="content our-partnership">
                <div class="layer">
                    <div class="container">
                        <div class="col-md-12">
                        <h2 class="title-content text-center title-underline title-underline-orange font-raleway uppercase"><span><?php echo partner_kerjasama ?></span></h2>
                        <div class="row text-center partnership">
							<?php
							if(isset($kerjasama) && $kerjasama){
								foreach($kerjasama  as $dt):
									if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
									else $imgthumb = $this->config->default_thumb_web;
									
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
									
									
									if (isset($url)) $link = $this->location($url.'/read/kerjasama/'.$content.'/'.$dt->id);
									else $link = $this->location('page/read/kerjasama/'.$content.'/'.$dt->id);
									?>
									<div class="col-md-2 col-xs-4"><a href="<?php echo $link; ?>" ><img src="<?php echo $imgthumb; ?>" class="img-responsive img-responsive-center" alt="<?php echo $dt->judul ?>"></a></div>
									<?php
								endforeach;
							}else{
							?>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/gdp.png"); //$mimg->base64_image($this->asset("ptiik/images/gdp.png")); ?>" class="img-responsive img-responsive-center" alt="GDP Logo"><?php echo count($kerjasama)?></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/nokia.png"); //$mimg->base64_image($this->asset("ptiik/images/nokia.png")); ?>" class="img-responsive img-responsive-center" alt="Nokia Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ibm.png"); //$mimg->base64_image($this->asset("ptiik/images/ibm.png")); ?>" class="img-responsive img-responsive-center" alt="IBM Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/cisco.png"); //$mimg->base64_image($this->asset("ptiik/images/cisco.png")); ?>" class="img-responsive img-responsive-center" alt="Cisco Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/oracle.png"); //$mimg->base64_image($this->asset("ptiik/images/oracle.png")); ?>" class="img-responsive img-responsive-center" alt="Oracle Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ni.png"); //$mimg->base64_image($this->asset("ptiik/images/ni.png")); ?>" class="img-responsive img-responsive-center" alt="NI Logo"></div>
							<?php } ?>
                        </div>
                        </div>
                    </div>
                </div>
            </section>

