
 <div class="comment-area">
	<h3 class="comment-title">Comment</h3>
	<?php
	if($comment==1){				
	$data_comment = $mpage->read_comment($id,'0');
	?>
	<section class="comment content">
		<?php
		if($data_comment):
			foreach($data_comment as $dt):
				?>
				<div class="media media-comment">
					<a class="pull-left media-img-container" href="#">
						<?php if($dt->foto){ ?>
							<img class="media-object" src="<?php echo $this->location($dt->foto); ?>" alt="...">
						<?php }else{
						?>
							<img class="media-object" src="<?php echo $this->location("assets/images/logo-ub-blue-header.png"); ?>" alt="...">
						<?php } ?>
					</a>
					<div class="media-body">
						<h4 class="media-heading"><?php echo $dt->name ?></h4>
						<p class="comment-paragraph"><?php echo $dt->comment ?></p>
						<div class="comment-action">
							<!-- <a href="" class="delete"><span class="glyphicon glyphicon-trash"></span></a> -->
							<a href="" class="reply" data-url="" data-parentid="<?php echo $dt->id; ?>"><span class="fa fa-share"></span></a>
						</div>
						<?php
						$this->detail_comment($id, $dt->id, $mpage);
						?>
					</div>
				</div>
				<?php
			endforeach;
		endif;
		?>
	</section>

	<a href="javascript::" data-url="" class="btn btn-default comment-post" style="display:none"><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
	<form class="form form-comment" id="form-submit-comment-news">
		<input type="hidden" name="parentid" value="0" id="parentid"/>
		<div class="row">
			<div class="form-group">
				<label class="col-sm-12 control-label"></label>
				<div class="col-md-6">
					<input type="email" class="form-control" required="required" name="comment-email" id="comment-email" placeholder="Email">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-12 control-label"></label>
				<div class="col-md-6">
					<input type="password" class="form-control" required="required" name="comment-password" id="comment-password" placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-12 control-label"></label>
				<div class="col-md-12">
					<textarea class="form-control" required="required" name="comment-content" id="comment-content" placeholder="Comment"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-12 control-label"></label>
				<div class="col-md-12">
					<input type="hidden" name="content-news" value="<?php if(isset($id)){echo $id;} ?>" />
					<button type="submit" onclick="submit_comment_news()" class="btn btn-info">Comment</button>
				</div>
			</div>
		</div>
	</form>
	<?php }else{ ?>
	<section class="comment content">
		<div class="well well-comment">
			Comment Off
		</div>
	</section>
	<?php
	}?>
</div>

