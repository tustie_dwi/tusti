<?php 
$this->view("page/header.php", $data); 
?>


<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
		<?php 
		if (isset($detail)):
		?>
			
					<!-- Edit Content Article -->
					<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no" itemprop="name"><?php if($kategori=='faq'){ echo 'Frequently Asked Questions'; }else{ if($detail->judul) echo $detail->judul; 
					else echo $detail->judul_ori; }
				
					?></h1>
						<!-- Edit Breadcrumb -->
					<!-- Edit Breadcrumb -->
					<ol class="breadcrumb breadcrumb-style margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<?php if(isset($detail) && (isset($detail->parent))):?>
							  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php 
							if(isset($url)):
								echo $this->location($url.'/read/'.$kategori.'/'.$detail->id);
							else:
								echo $this->location('page/read/'.$kategori.'/'.$detail->id);
							endif; ?>" itemprop="url"><span itemprop="title"><?php echo ucfirst($detail->parent);?></span></a></li>
						<?php  else : 
							if($kategori=='news'|| $kategori=='berita' || $kategori=='event'|| $kategori=='kerjasama'|| $kategori=='pengumuman'):
								?>
									<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php 
									if(isset($url)):
										echo $this->location($url.'/read/'.$kategori);
									else:
										echo $this->location('page/read/'.$kategori);
									endif; ?>" itemprop="url"><span itemprop="title"><?php 
									if($lang=='en'){
										switch($kategori){
											case 'pengumuman':
												echo "Announcement" ;
											break;
											case 'kerjasama':
												echo "Partnership" ;
											break;								
											default :
												echo ucfirst($kategori);
											break;
										}
										
									}else{
										if($kategori=='faq') echo 'FAQ';
										else echo ucfirst($kategori);
									} 									
									?></span></a></li>
									<?php 
								endif;
							endif; ?>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if($detail->judul) echo $detail->judul;else echo $detail->judul_ori;	?></span></li>
					</ol>
					<!-- End Breadcrumb -->
					</section>
					<?php
		endif;
		
		if(isset($post)):
			?>
				<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php if($kategori=='faq'){ 
					echo 'Frequently Asked Questions';
					} else{
						if($lang=='en'){
							switch($kategori){
								case 'pengumuman':
									echo "Announcement" ;
								break;
								case 'kerjasama':
									echo "Partnership" ;
								break;								
								default :
									echo ucfirst($kategori);
								break;
							}
							
						}else{
							if($kategori=='faq') echo 'FAQ';
							else echo ucfirst($kategori);
						} 
					}?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb breadcrumb-style margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php 
					  if($lang=='en'){
							switch($kategori){
								case 'pengumuman':
									echo "Announcement" ;
								break;
								case 'kerjasama':
									echo "Partnership" ;
								break;	
								case 'faq':
									echo "FAQ" ;
								break;									
								default :
									echo ucfirst($kategori);
								break;
							}
							
						}else{
							if($kategori=='faq') echo 'FAQ';
							else echo ucfirst($kategori);
						} 
						?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			<?php
		endif;
		?>
		</div>
	</div>
</section>
 <div id="content-video"> 
<section class="content content-white padding-top-sm">
	<div class="container">   		
		
	<!-- Main Content -->
		<div style="line-height:1.8" class="<?php if(isset($kategori) && (($kategori=='member-riset') ||($kategori=='lecturers') ||($kategori=='lecturers') ||($kategori=='lecturers') || ($kategori=='lecturer') || ($kategori=='staff')|| ($kategori=='dosen')|| ($kategori=='staff-kependidikan'))) echo "col-md-12"; 
					else echo "col-md-8"?>">	
			<?php		
			if(isset($detail)):
				if($kategori=='event'):
					$this->view("page/detail_event.php", $data);
				else:
					if(isset($detail->content_data) && ($detail->content_data=="map")):	
				
					else:	
						if(isset($detail->content_data) && $detail->content_data=="jadwal"){
							//$this->redirect($detail->content_data);
						}
				
					?>
										
					<!-- Edit Content Article -->
					<section itemscope="" itemtype="http://schema.org/Article">
					
					<?php
					endif;
					if($kategori=='course' || $kategori=='matakuliah'):
						$mconf = new model_page;
						$komponen = $mconf->read_silabus("",$lang,$detail->id);
						
						foreach($komponen as $dt): 
								echo "<h3>".$dt->komponen."</h3>";
								echo $dt->keterangan;
						endforeach; 
					else:
						if($detail->keterangan)	$strcontent	= $detail->keterangan;
						else $strcontent = $detail->keterangan_ori;	
					
									?>
						
						<section itemprop="articleBody">
							<?php
							
							$strcontent_ = $mpage->str_content($strcontent,$this->config->file_url_view,$this->config->file_url_old);
							echo $this->add_alt_tags($strcontent_,"a"); 
							
							if(isset($detail->content_data)):
								if(isset($url)) $url_link=$url;
								else $url_link = "";
								
								if(isset($unit_list)) $unit_ = $unit_list;
								else $unit_="";
								
								$this->read_data($detail->content_data, $url_link, $unit_);
							endif;
							
							if($kategori=='kontak' || $kategori=='contact'):
								?>
								
									<hr>
								
									<h3 class="font-raleway margin-top-no margin-bottom-sm">Feedback</h3>
									<form id="form-feedback-page">										
											<div class="form-group">
												<input type="email" class="form-control" required="required" name="feedback-email" id="feedback-email" placeholder="Email">
											</div>
											<div class="form-group">
												<input type="password" class="form-control" required="required" name="feedback-password" id="feedback-password" placeholder="Password PTIIK Apps">
											</div>
											<div class="form-group">
												<textarea class="form-control" required="required" name="feedback-content" id="feedback-content" placeholder="Message"></textarea>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-default">Submit</button>
												<button type="button" class="btn btn-danger btn-cancel-feedback">Cancel</button>
											</div>
										
									</form>
								<?php
							endif;
							?>
							<div class="content-pane-tube pull-right">                						
								<div class="tube-share-button">
								<h4>
									<span class="fa fa-share-alt" title="Share"></span> <small>Share</small>
									<a class="fb" href="javascript::" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','fbshare','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-facebook-square"></span></a>
									<a class="twitter" href="javascript::" onclick="window.open('https://twitter.com/intent/tweet?text=Judul&url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>&via=PTIIK_UB','twt','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-twitter-square"></span></a>
									<a class="gplus" href="javascript::" onclick="window.open('https://plus.google.com/share?url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','gp','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-google-plus-square"></span></a>
									</h4>
								</div>
							</div>
							</section>
							<?php
								
					endif;
					
					if($kategori=='news'):
						$data['comment'] = $detail->content_comment;
						$data['id']		 = $detail->id;
						$data['mpage']	 = $mpage;
						
						//$this->view("page/comment.php", $data);
					endif;
				endif;
				?>
				</section>
				<!-- End Edit Content Article -->
				<?php
				
				
			
			endif;	
			
				
			if( isset($post) ) : 
				?>
				<section itemscope="" itemtype="http://schema.org/Article">
				<section itemprop="articleBody">
					
						<table class="table web-page">
							<thead style="display:none">
								<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
							</thead>
							<tbody>
							<?php
								if($post) : 
								foreach($post as $dt) { 
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
								?>
								<tr style="border-top:0px;">
										<td style="display:none;border:0px;"></td>
									<td style="border-top:0px;">
										<?php 
										if(isset($url)):
											if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
											else $url_content = $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
										else:
											if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
											else $url_content = $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
										endif;
										
										if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
										else $imgthumb = $this->config->default_thumb_web;
										?>
										
									   
											<?php 
											if($kategori=='news' || $kategori=='matakuliah' || $kategori=='course') :
											?>
											 <div class="media">
												<a class="pull-left" href="<?php echo $url_content; ?>">
													<?php if($kategori=='news') : ?>
													<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="200px">
													<?php else : ?>
													<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="100px">
													<?php endif; ?>
												</a>
												
												 <div class="media-body">
													<a class="title-article" href="<?php echo $url_content; ?>"><h4 class="media-heading"><?php echo $dt->judul; ?></h4></a>
														<?php if($kategori=='news'){
															?>
															<small>
															<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43">&nbsp;<?php echo date("M d, Y", strtotime($dt->content_modified)); ?></time>
															<span class="category"><span class="fa fa-tags"></span>&nbsp;<?php echo $kategori; ?></span>
															</small><p></p>
															<!--<time class="time-post"><small><?php //if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
															//else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>-->
														<p class="post-content text-justify">
														<?php
														$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
														$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
														$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
													   echo htmlspecialchars($isi);
							
													?>
													<?php //var_dump($dt); ?>
														<a href="<?php echo $url; ?>" class="more-orange">more..</a>
													</p>
													<?php } ?>
												</div>
											</div>					
										<?php 
										else:
											
										?>
											 <div class="media"><a class="title-article" href="<?php echo $url_content; ?>"><?php if($dt->judul) echo $dt->judul;
											else echo $dt->judul_ori;
											?></a>
											<time class="time-post"><small><?php if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
											else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>
											</div>
											<?php
											$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
														$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
														$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
													//   echo htmlspecialchars($isi);
										endif;?>
										   
									</td>
								</tr>
								<?php	
								} 
								endif;	
							?>
						</tbody>
						</table>
					
				</section>
			</section>
		<?php
			
		endif; 
		?>
					
	</div>
	<!-- End Main Content -->
	
	<!-- Sidebar -->
	<div class="col-md-4">
	<?php
		if(isset($detail->content_data) && (($detail->content_data=="map")||($detail->content_data=="staff/dosen")|| ($detail->content_data=="staff/staff") || ($detail->content_data=="s2/dosen"))):	
			//var_dump($detail->content_data);
		else:
			
			$this->view("page/sidebar.php",$data);
		endif;
	?>
	 </div>
	 <!-- End Sidebar -->
	 
	   </div>
            </section>
</div>	
<?php $this->view("page/footer.php", $data); 

function replace_table($content){
	preg_match_all('/<table (.*?)\/>/', $content, $str);
	
	if(!is_null($str)){
		foreach($str[1] as $index => $value){
								
			$new_img = str_replace('<table', '<table class="table"', $src[0][$index])."</a>";
		
			$content = str_replace($src[0][$index], $new_img, $content);
		}
	}
	 return $content;
}


?>