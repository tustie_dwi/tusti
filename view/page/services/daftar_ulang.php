<?php $this->view("page/header.php", $data); 
//session_start();
$_SESSION['count'] = time();
$image;
?>

<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php echo daftar_ulang;?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url.'info/services'); 
						else echo $this->location('info/services'); ?>" itemprop="url"><span itemprop="title"><?php if($lang=='in') echo 'Layanan'; else echo "Services";?></span></a></li>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php echo daftar_ulang ?></span></li>
					</ol>
					<!-- End Breadcrumb -->
		</div>
	</div>
</section>


<section class="content content-white">
	<div class="container">
		<div class="col-md-12">
			
					
						<div class="row">
							<div class="col-md-12">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading">
									  <h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse" class="collapsed" aria-expanded="false" aria-controls="collapse">
										 Panduan Daftar Ulang
										</a>
									  </h4>
									</div>
									<div id="collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading">
									  <div class="panel-body"><?php 
									  $mpage = new model_page();
									  
									  echo $mpage->str_content($kalender->keterangan,$this->config->file_url_view,$this->config->file_url_old); 
									  
									  //echo $kalender->keterangan 
									  
									  ?></div>
									</div>
								  </div>
							</div>
						</div>
					</div>
					<?php 
					
							
					if(isset($kalender) && ($kalender)){				
						 if(isset($msg)) { ?> 
							<div class="alert alert-warning msg"><?php echo $msg; ?></div>
					<?php
						}
					if(!$nim){ ?>	
					<h3 class="font-raleway"><?php echo form_daftar_ulang ?></h3>
					<div class="row">
						
						<form id="form-daftar-ulang">
							<div class="col-md-8">
								
								<div class="form-group">
									<?php create_image(); ?>							
										<div style="display:block;margin-bottom:5px;margin-top:0px;"  class="well">									
											<img src="<?php echo $this->asset("upload/captcha/image".$_SESSION['count'].".png") ?>">&nbsp;
											<a  href="#" onclick="window.location.href = window.location.href" class="btn" style="margin-top:15px;position:absolute"><i class="fa fa-refresh"></i> </a>
											
										</div>
								</div>
								<div class="form-group">
										<input type="text" name="input" id="input-kode" class="form-control" placeholder="Enter Text Pada Gambar Diatas" required >
										<input type="hidden" name="flag" value="<?php echo md5($_SESSION['captcha_string']); ?>" id="flag" />
										
								</div>
								<div class="form-group">
									<input type="text" required="required" class="form-control" name="nim_mhs" id="nim_mhs" placeholder="<?php echo masukkan_nim ?>" value="<?php if(isset($nim)) echo $nim ?>"/>
								</div>
								<div>
									<button class="btn btn-default" type="submit">Submit</button>
								</div>
							</div>
							
						</form>
					</div>
				<?php 
				}
				}else{
					$mpost = new model_confinfo();
							$dt	= $mpost->kalender_akademik('daftar', $semester);
						?>
							<div class="alert alert-warning">Perhatian! Jadwal daftar ulang belum aktif. Proses daftar ulang akademik akan dimulai pada 
							<span class="label label-warning"><?php echo date("M d, Y", strtotime($dt->tgl_mulai)); ?></span> sampai <span class="label label-warning"><?php echo date("M d, Y", strtotime($dt->tgl_selesai)); ?></span>. Silahkan menghubungi Akademik untuk informasi lebih lanjut.</div>
						<?php
				}
				
				
				if(!isset($auth_msg)){ ?>
					<?php 
				
					if(isset($msg)&&(isset($kalender) && ($kalender))){
						
						
						if(isset($mhs_entered)&&($mhs_entered)):?>
						
						<div class="row">
							<div class="col-md-8">
							  <div class="well">
								<table class="table ">
									<thead>
										<tr>
											<th width="30%">&nbsp;</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><?php echo nama; ?></td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->nama;?></td>
										</tr>
										<tr>
											<td><?php echo ttl; ?></td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->tmp_lahir;?>, <?php if(isset($mhs_entered)) echo $mhs_entered->tgl_lahir;?></td>
										</tr>
										<tr>
											<td><?php echo jalur_seleksi; ?></td>
											<td><?php if(isset($mhs_entered)) echo ucfirst($mhs_entered->jalur_masuk);?></td>
										</tr>
										<tr>
											<td><?php echo prodi; ?></td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->prodi;?></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->email;?></td>
										</tr>
										<tr>
											<td><?php echo alamat_malang; ?></td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->alamat_malang;?></td>
										</tr>
										<tr>
											<td><?php echo telp ?>/ <?php echo hp ?></td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->telp." / ".$mhs_entered->hp;?></td>
										</tr>
										<tr>
											<td><?php echo orangtua?></td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->orang_tua;?></td>
										</tr>
										<tr>
											<td><?php echo email_ortu?></td>
											<td><?php if(isset($mhs_entered)&&$mhs_entered->email_ortu!="") echo $mhs_entered->email_ortu; else echo "-";?></td>
										</tr>
										<tr>
											<td><?php echo alamat_ortu?></td>
											<td><p><?php if(isset($mhs_entered)) echo $mhs_entered->alamat_ortu;?></p></td>
										</tr>
										<tr>
											<td><?php echo telp_ortu ?></td>
											<td><?php if(isset($mhs_entered)) echo $mhs_entered->telp_ortu." / ".$mhs_entered->hp_ortu;?></td>
										</tr>
										<tr>
											<td><?php echo alamat_surat ?></td>
											<td><p><?php if(isset($mhs_entered)) echo $mhs_entered->alamat_surat;?></p></td>
										</tr>
										<tr>
											<td><?php echo note ?></td>
											<td><?php if(isset($mhs_entered)&&$mhs_entered->catatan!="") echo $mhs_entered->catatan; else echo "-";?></td>
										</tr>
									</tbody>
								</table>
							  </div>
							</div>
						</div>
					<?php 
					endif;
					} 

					if(isset($mhs) && $mhs){ ?>
					<div class="row">
						<br><br>
						<form method="post" id="form-mhs-daftar-ulang" action="<?php echo $this->location('info/save_daftarulang'); ?>">
							<div class="col-md-8">
								<div class="block-box well">
									<div class="header"><h4><?php echo biodata_pribadi; ?></h4></div>
									<div class="content">
										<div class="form-group">
											 <label class="control-label"><?php echo nama; ?></label>
											 <input type="text" required="required" class="form-control" name="nama_mhs" id="nama_mhs" placeholder="<?php echo nama; ?>" value="<?php if(isset($mhs)) echo $mhs->nama;?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo ttl; ?></label>
												<input type="text" required="required" class="form-control" name="tmpt_lahir_mhs" id="tmpt_lahir_mhs" placeholder="<?php echo tempat_lahir; ?>" value="<?php if(isset($mhs)) echo $mhs->tmp_lahir;?>"/>
											 <label class="control-label">&nbsp;</label>
												<input type="text" required="required" class="form-control form_datetime" name="tgl_lahir_mhs" id="tgl_lahir_mhs" placeholder="<?php echo tgl_lahir; ?>" value="<?php if(isset($mhs)) echo $mhs->tgl_lahir; ?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo jalur_seleksi ?></label>
											 <select class="form-control e9" name="jalur_seleksi_mhs" id="jalur_seleksi_mhs">
												<option value="reguler" <?php if(isset($mhs)&&$mhs->jalur_seleksi=="reguler") echo "selected"; ?> >Reguler</option>
												<option value="sap" <?php if(isset($mhs)&&$mhs->jalur_seleksi=="sap") echo "selected"; ?>>SAP</option>
											 </select>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo prodi ?></label>
											 <select class="form-control e9" name="prodi_mhs" id="prodi_mhs" disabled="disabled">
												<option value="0">Pilih Prodi</option>
												<?php
												if (count($prodi) > 0) {
													foreach ($prodi as $dt) :
														echo "<option value='" . $dt -> id . "' ";
														if(isset($mhs)&&$mhs->prodi_id==$dt -> id){
															echo "selected";
														}
														echo ">" . $dt -> value . "</option>";
													endforeach;
												}
												?>
											 </select>
											 <?php if(isset($mhs)){ ?>
												<input type="hidden" name="prodi_mhs" value="<?php if(isset($mhs))echo $mhs->prodi_id; ?>" />
											 <?php } ?>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo email ?></label>
											 <input type="email" required="required" class="form-control" name="email_mhs" id="email_mhs" placeholder="<?php echo email ?>" value="<?php if(isset($mhs))echo $mhs->email; ?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo alamat_malang ?></label>
											 <textarea required="required" class="form-control" name="alamat_mhs" id="alamat_mhs" placeholder="<?php echo alamat_malang ?>"><?php if(isset($mhs))echo $mhs->alamat; ?></textarea>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo telp ?></label>
											 <input type="text" required="required" class="form-control" name="tlp_mhs" id="tlp_mhs" placeholder="<?php echo telp ?>" value="<?php if(isset($mhs))echo $mhs->telp; ?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo hp ?></label>
											 <input type="text" required="required" class="form-control" name="hp_mhs" id="hp_mhs" placeholder="<?php echo hp ?>" value="<?php if(isset($mhs))echo $mhs->hp; ?>"/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="block-box well">
									<div class="header"><h4><?php echo data_orang_tua ?></h4></div>
									<div class="content">
										<div class="form-group">
											 <label class="control-label"><?php echo orangtua ?></label>
											 <input type="text" required="required" class="form-control" name="nama_ortu" id="nama_ortu" placeholder="<?php echo orangtua ?>" value="<?php if(isset($mhs)) echo $mhs->nama_ortu; ?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo email_ortu ?></label>
											 <input type="email" class="form-control" name="email_ortu" id="email_ortu" placeholder="<?php echo email_ortu ?>" value="<?php if(isset($mhs)) echo $mhs->email_ortu; ?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo alamat_ortu ?></label>
											 <textarea required="required" class="form-control" name="alamat_ortu" id="alamat_ortu" onchange="alamat()" placeholder="<?php echo alamat_ortu ?>"><?php if(isset($mhs)) echo $mhs->alamat_ortu; ?></textarea>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo telp_ortu ?></label>
											 <input type="text" required="required" class="form-control" name="tlp_ortu" id="tlp_ortu" placeholder="<?php echo telp_ortu ?>" value="<?php if(isset($mhs)) echo $mhs->telp_ortu; ?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo hp_ortu ?></label>
											 <input type="text" required="required" class="form-control" name="hp_ortu" id="hp_ortu" placeholder="<?php echo hp_ortu ?>" value="<?php if(isset($mhs)) echo $mhs->hp_ortu; ?>"/>
										</div>
									</div>
								</div>
								<div class="block-box well">
								  <div class="header"><h4><?php echo surat_menyurat ?></h4></div>
									<div class="content">
										<div class="form-group">
											 <label class="control-label" style="color: red"><small><?php echo warning_note_surat ?></small></label>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo alamat_surat ?></label>
												<textarea required="required" class="form-control" name="alamat_surat" id="alamat_surat" placeholder="<?php echo alamat_surat ?>"><?php if(isset($mhs)) echo $mhs->alamat_surat; ?></textarea>
										</div>
										<div class="form-group">
											 <label class="control-label"><?php echo note ?></label>
												<textarea class="form-control" name="catatan_surat" id="catatan_surat" placeholder="<?php echo note ?>"><?php if(isset($mhs)) echo $mhs->catatan; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										 <input type="hidden" name="hidId_mhs" value="<?php echo $nim; ?>"/>
										 <input type="hidden" name="daftar_id" value=""/>
										 <button type="submit" class="btn btn-primary">Submit</button>
										 <button type="button" class="btn btn-danger" onclick="location.href='<?php echo $this->location('info/akademik/daftar'); ?>'">Cancel</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<?php } ?>
				<?php } else {
					echo '<label class="label label-danger msg">'.$auth_msg.'</label>';
				} ?>
			
		</div>
	</div>
</section>
<?php 

$this->view("page/footer.php", $data); 
unset($_SESSION['captcha_string']); 


	function create_image()
		{
		global $image;
		$image = imagecreatetruecolor(200, 50) or die("Cannot Initialize new GD image stream");
		 
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 0, 255, 255);
		$line_color = imagecolorallocate($image, 0, 0, 150);
		$pixel_color = imagecolorallocate($image, 0, 0, 255);
		 
		imagefilledrectangle($image, 0, 0, 200, 50, $background_color);
		 
		for ($i = 0; $i < 3; $i++) {
		//imageline($image, 0, rand() % 50, 200, rand() % 50, $line_color);
		}
		 
		for ($i = 0; $i < 150; $i++) {
		imagesetpixel($image, rand() % 200, rand() % 50, $pixel_color);
		}
		 
		 
		$letters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$len = strlen($letters);
		$letter = $letters[rand(0, $len - 1)];
		 
		$text_color = imagecolorallocate($image, 0, 0, 0);
		$word = "";
		for ($i = 0; $i < 6; $i++) {
		$letter = $letters[rand(0, $len - 1)];
		imagestring($image, 7, 5 + ($i * 30), 20, $letter, $text_color);
		$word .= $letter;
		}
		$_SESSION['captcha_string'] = $word;
		 
		$images = glob("assets/upload/captcha/*.png");
		foreach ($images as $image_to_delete) {
		@unlink($image_to_delete);
		}
		imagepng($image, "assets/upload/captcha/image" . $_SESSION['count'] . ".png");
				 
		}

	?>