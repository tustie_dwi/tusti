<?php
//session_start();
$_SESSION['count'] = time();
$image;
?>


	<div class="row">
		<div class="col-md-12">
			<h2 class="font-raleway">Application Form</h2>
					<?php 
					
							
					if(isset($kalender) && ($kalender)){				
						 if(isset($msg)) { ?> 
							<div class="alert alert-warning msg"><?php echo $msg; ?></div>
					<?php
						}
					if(!$nim){ ?>	
					<div class="row">
						
						<form id="form-apply-sub"  method="post" action="<?php $this->location('page/submission');?>">
							<div class="col-md-8">
								
								<div class="form-group">
									<?php create_image(); ?>							
										<div style="display:block;margin-bottom:5px;margin-top:0px;"  class="well">									
											<img src="<?php echo $this->asset("upload/captcha/image".$_SESSION['count'].".png") ?>">&nbsp;
											<a  href="#" onclick="window.location.href='<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI']?>'" class="btn" style="margin-top:15px;position:absolute"><i class="fa fa-refresh"></i> </a>
											
										</div>
								</div>
								<div class="form-group">
										<input type="text" name="input" id="input-kode" class="form-control" placeholder="Enter Text Pada Gambar Diatas" required >
										<input type="hidden" name="flag" value="<?php echo md5($_SESSION['captcha_string']); ?>" id="flag" />
										
								</div>
								<div class="form-group">
									<input type="text" required="required" class="form-control" name="nim_mhs" id="nim_mhs" placeholder="NIM" value="<?php if(isset($nim)) echo $nim ?>"/>
									 <input type="hidden" name="tmp_id" value="<?php echo $hidid; ?>"/>
								</div>
								<div>
									<button class="btn btn-default" type="submit">Submit</button>
								</div>
							</div>
							
						</form>
					</div>
				<?php 
				}
				}else{
					
							$dt	= $mpage->get_master_kegiatan("",$hidid,"-","");
						?>
							<div class="alert alert-warning">You can submit your resume on 
							<span class="label label-warning"><?php echo date("M d, Y", strtotime($dt->tgl_mulai)); ?></span> to <span class="label label-warning"><?php echo date("M d, Y", strtotime($dt->tgl_selesai)); ?></span>. Please contact <?php echo $dt->unit; ?> for detail information.</div>
						<?php
				}
				
				
				
				if(isset($mhs)){ ?>
					<div class="col-md-12">
						<form id="form-apply-submit" enctype="multipart/form-data" method="post" action="<?php $this->location('page/rekrutmen');?>">
								<div class="block-box well">
										<div class="content">
										<div class="form-group">
											 <label class="control-label">Nama</label>
											 <input type="text" required="required" class="form-control" name="nama" id="nama_mhs" placeholder="Name" value="<?php if(isset($mhs)) echo $mhs->nama;?>"/>
										</div>
										
										<div class="form-group">
											 <label class="control-label">Email</label>
											 <input type="email" required="required" class="form-control" name="email" id="email_mhs" placeholder="<?php echo email ?>" value="<?php if(isset($mhs))echo $mhs->email; ?>"/>
										</div>
										
										<div class="form-group">
											 <label class="control-label">Telp</label>
											 <input type="text" required="required" class="form-control" name="telp" id="tlp_mhs" placeholder="<?php echo telp ?>" value="<?php if(isset($mhs))echo $mhs->telp; ?>"/>
										</div>
										<div class="form-group">
											 <label class="control-label">HP</label>
											 <input type="text" required="required" class="form-control" name="hp" id="hp_mhs" placeholder="<?php echo hp ?>" value="<?php if(isset($mhs))echo $mhs->hp; ?>"/>
										</div>
										
										
									</div>
								</div>
							
								<div class="block-box well">
									<div class="form-group">
										<label for="is_aktif">Cover Letter</label><br>
										<input type="file" name="cover" id="cover"/>
									</div>
									<div class="form-group">
										<label for="is_aktif">CV <span class="text text-danger">* required</span></label><br>
										<input type="file" name="file" id="file" required/>
									</div>
										
									<div class="form-group">
										 <input type="hidden" name="hidId_mhs" value="<?php echo $nim; ?>"/>
										 <input type="hidden" name="tmpid" value="<?php echo $hidid; ?>"/>
										 <input type="hidden" name="kegiatan_id" value="<?php echo $kegiatanid; ?>"/>
										 <button type="submit" class="btn btn-primary" name="btn_sub">Submit</button>
										 <button type="button" class="btn btn-danger" onclick="location.href='<?php echo $this->location('page/rekrutmen'); ?>'">Cancel</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<?php } ?>
			
		</div>
	</div>
<?php 

unset($_SESSION['captcha_string']); 


	function create_image()
		{
		global $image;
		$image = imagecreatetruecolor(200, 50) or die("Cannot Initialize new GD image stream");
		 
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 0, 255, 255);
		$line_color = imagecolorallocate($image, 0, 0, 150);
		$pixel_color = imagecolorallocate($image, 0, 0, 255);
		 
		imagefilledrectangle($image, 0, 0, 200, 50, $background_color);
		 
		for ($i = 0; $i < 3; $i++) {
		//imageline($image, 0, rand() % 50, 200, rand() % 50, $line_color);
		}
		 
		for ($i = 0; $i < 150; $i++) {
		imagesetpixel($image, rand() % 200, rand() % 50, $pixel_color);
		}
		 
		 
		$letters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$len = strlen($letters);
		$letter = $letters[rand(0, $len - 1)];
		 
		$text_color = imagecolorallocate($image, 0, 0, 0);
		$word = "";
		for ($i = 0; $i < 6; $i++) {
		$letter = $letters[rand(0, $len - 1)];
		imagestring($image, 7, 5 + ($i * 30), 20, $letter, $text_color);
		$word .= $letter;
		}
		$_SESSION['captcha_string'] = $word;
		 
		$images = glob("assets/upload/captcha/*.png");
		foreach ($images as $image_to_delete) {
		@unlink($image_to_delete);
		}
		imagepng($image, "assets/upload/captcha/image" . $_SESSION['count'] . ".png");
				 
		}

	?>