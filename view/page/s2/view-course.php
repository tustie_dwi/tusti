<?php 
$this->view("page/s2/header-s2.php", $data); 
?>


<section class="content content-gray">
	<div class="container" >
		<?php 
		if (isset($detail)):
		?>
			<!-- Edit Content Article -->
			<section itemscope="" itemtype="http://schema.org/Article">
			<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no" itemprop="name"><?php if($detail->unit) echo $detail->unit; 
			else echo $detail->unit_ori;
		
			?></h1>
				<!-- Edit Breadcrumb -->
			<ol class="breadcrumb" itemprop="breadcrumb">
				  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
					else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
					
				  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if(($lang=='en')&&($kategori=='pengumuman')) echo "Announcement";				
					else echo ucWords($kategori);	?></span></li>
					 <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php if(($lang=='en')&&($kategori=='pengumuman')) echo "Announcement";				
					else echo ucWords($detail->unit);	?></span></li>
				</ol>
			<!-- End Breadcrumb -->
			</section>
			<?php
		endif;
		?>
  		
	</div>
</section>
		
<section class="content content-white">
	<div class="container" > 
		<div class="row">
	<!-- Main Content -->
		<div class="col-md-8" style="line-height:1.8;text-align:justify">				
		<?php
			if( isset($post) ) : 
				?>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-sm-9  col-sm-push-3">
									
									<div class="content-filter">
										<div class="row">
											<div class="tab-content">
												<?php 
												if($post): 
													foreach($post as $key):
													?>
														<div class="tab-pane text-justify" id="<?php echo strtolower($key->id);  ?>"><?php read_content($mpage, $lang, $unit, $key->unit_id); ?></div>	
													<?php 
													endforeach;
												endif; ?>
													
											</div>
										</div>
									</div>
									
								</div>
							
								<div class="col-sm-3 col-sm-pull-9">							
								  
									<div class="menu-filter left-pane menu-filter-other"> 
										<div class="menu-filter-box">
											<ul class="filters list-unstyled"> 
												
												<?php 
												if($post): 
													foreach($post as $key):
													?>
														<li><a href="#<?php echo strtolower($key->id);  ?>" role="tab" data-toggle="tab" class="filter"><?php  echo $key->unit; ?></a></li>	
													<?php 
													endforeach;
												endif; ?>
											</ul> 
										</div>
									</div>
								</div>
						</div>
							
					</div>
				</div>
				
			<?php endif;
			if(isset($detail)):
				read_content($mpage, $lang, $unit, $detail->unit_id,$this->config->file_url_view,$this->config->file_url_old); 
			endif;
			?>				 
			</div>
			<div class="col-md-4">
			<?php				
			$this->view("page/s2/sidebar.php",$data);
			?>
			</div>
	 <!-- End Sidebar -->	
		</div>
   </div>
</section>

	
<?php $this->view("page/s2/footer-s2.php", $data); 

function read_content($mpage=NULL, $lang=NULL, $unit=NULL, $unitid=NULL, $link=NULL, $linkold=NULL){
	$post = $mpage->read_content($unitid, $lang,'inside_menu');	
	
	if($post){
		$j=0;
			$arr_data=array();
		foreach ($post as $dt):
			$j++;
			if($j==2) $strclass=  "active ";
			else $strclass="";
						
			if($j!=1){
				$arr_temp = array("id"=>$dt->id, "content"=>$dt->keterangan, "class_str"=>$strclass, "judul"=>$dt->judul, "icon"=>$dt->icon);
				array_push($arr_data, $arr_temp);
			}
			if($j==1){
				//echo "<h3>".$dt->judul."</h3>";
				echo $dt->keterangan;
			}
		endforeach;
		?>
		
		<div role="tabpanel"  class="tabpanel">

		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
			<?php
			for($i=0;$i<count($arr_data);$i++){	
			?>
				<li role="presentation" class="<?php  echo $arr_data[$i]["class_str"];?> font-raleway margin-top-no"><a href="#<?php  echo $arr_data[$i]["id"];?>" role="tab" data-toggle="tab"><h4><?php  echo $arr_data[$i]["judul"];?></h4></a></li>
			<?php } ?>
		  </ul>
		
		  <!-- Tab panes -->
		  <div class="tab-content">
			<?php
			for($i=0;$i<count($arr_data);$i++){	
			?>
			<div role="tabpanel" class="tab-pane <?php  echo $arr_data[$i]["class_str"];?>" id="<?php  echo $arr_data[$i]["id"];?>">
				<?php if($arr_data[$i]["icon"]): ?>
				<div class="media">
					<a class="pull-left" href="#">
						<img class="media-object img-responsive"  src="<?php  echo $this->config->file_url_view."/".$arr_data[$i]["icon"];	?>">
					</a>
					<div class="media-body">
						<?php  echo $arr_data[$i]["content"];	?>
					</div>
				</div>
				<?php else: echo $mpage->str_content($arr_data[$i]["content"], $link, $linkold); endif; ?>
			</div>
			<?php } ?>
		  </div>
		
		</div>
		<?php
		/*
			?>
				<!--<ul class="nav nav-pills nav-pills-other nav-pills-color" id="writeTab">
					<?php 
					$arr_data=array();
					$j=0;
					foreach ($post as $dt):
						$j++;
						if($j==1) $strclass=  "active ";
						else $strclass="";
						
						$arr_temp = array("id"=>$dt->id, "content"=>$dt->keterangan, "class_str"=>$strclass);
						array_push($arr_data, $arr_temp);
						?>
						 <li class="<?php echo $strclass;?>"><a href="#<?php echo $dt->id ?>" role="tab" data-toggle="tab"><?php  echo $dt->judul; ?></a></li>
						<?php
					endforeach;
				
					?>					
				</ul>

				<div class="tab-content">
					<?php
					for($i=0;$i<count($arr_data);$i++){						
						
						?>
						 <div class="tab-pane <?php  echo $arr_data[$i]["class_str"];?>" id="<?php  echo $arr_data[$i]["id"];?>"><?php  echo $arr_data[$i]["content"];	?></div>
						<?php
					}
					?>
				</div>	--?				
			<?php*/
		
	}
}


?>