

<div class="menu-navigation">
	<div class="container">
		<div class="row">
			<div class="col-md-2 title-menu font-raleway uppercase">
				<a href="<?php echo $this->location('info/services') ?>" class="more-service">
					<div class="temu"><?php echo temu_layanan ?></div>
					<div class="layanan"><?php echo layananmu ?></div>
				</a>
			</div>
			<div class="col-md-10 content-slide">
				<div class="row">
					<div class="col-xs-10">
						<div class="menu-navigation-slider">
						<?php
						$apps = $mpage->read_content('0',$lang,'apps');
						
						if($apps):
						if(! isset($unit_list)):
							$strcolor="orange";
						else:
							if(isset($_COOKIE['stylecolor'])) $strcolor=$_COOKIE['stylecolor'];
							else $strcolor=$color;
						endif;
						
							foreach($apps as $dt):
								/*if($dt->icon) $img = $this->config->file_url_view."/".$dt->icon;
								else $img = $this->config->default_thumb_web;*/
								
								
								$str=explode('/',$dt->icon);
								$img_name = end($str);
								if($img_name) $img= $this->asset("ptiik/images/".$strcolor."/".$img_name);
								else $img= $this->asset("ptiik/images/".$strcolor."/akademik.png");
								?>
								<div class="slide" style="font-size:18px;">
									<a href="<?php echo $this->location($dt->content_data);?>" class="menu-navigation-link with-tooltip" data-toggle="tooltip" data-placement="top" title="<?php if($dt->keterangan) echo $dt->keterangan; ?>">
										<!--<img class="menu-navigation-img img-responsive" src="<?php echo $img; //echo $mimg->base64_image($img);?>" alt="<?php echo $dt->judul ?>">-->
										<span class="menu-navigation-detail"><span class="menu-navigation-detail"><i class="fa fa-th-large"></i> <?php echo $dt->judul ?></span>
									</a>
								</div>
								<?php
							endforeach;
						endif;
						?>
													   
						</div>
					</div>
					<div class="col-xs-2">
						<a href="#" class="menu-navigation-next">
							<span class="fa fa-chevron-right"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- add 13/1/15 -->
<section class="content content-white text-center">
	<div class="container">
		<div class="col-md-12" style='line-height:1.8'>
			<section itemscope="" itemtype="http://schema.org/Article">
				<section itemprop="articleBody">
			<?php if($about):
					foreach($about as $dt):
			?>
					<h3 class="text-center title-content margin-top-sm margin-bottom-no font-raleway font-orange">
						<span><?php
						echo $dt->judul;
						?>
						</span>
					</h3>
					<div class="text-center margin-bottom-sm"></div>
					<div class="text-center margin-bottom">
						<?php
						echo $dt->keterangan;
						
						?>
					</div>
				<?php 
					endforeach;
				endif; ?>
				</section>
			</section>
		</div>
	</div>
</section>			
<section class="content content-first-top">
	<div class="container">
		<div class="col-md-12">
			<section itemscope="" itemtype="http://schema.org/Article">
				<section itemprop="articleBody">
					<h2 class="text-center title-content margin-top-sm margin-bottom-no font-raleway"><span><?php echo keminatan ?></span></h2>
					<div class="text-center margin-bottom-sm"></div>
					
					<div class="row">
						<!-- add 13/1/15 -->
						<!--<div class="col-sm-10 col-sm-offset-1">-->
						<div>
							<?php
							if($minat):
								foreach($minat as $dt):
									if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
									else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));

									if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
									else $imgthumb = $this->config->default_thumb_web;	

									
										//$strabout = $mpage->content($dt->about,10);
										$strabout = $dt->about;
														
								?>
									<div class="col-sm-3">
										<a href="<?php echo $this->location($url.'/read/minat/'.$content.'/'.$dt->id); ?>" class="prodi-link text-center">
											<figure>
												<img src="<?php echo $imgthumb ?>" class="img-responsive img-responsive-center margin-top-no">
											</figure>
											<h3 class="margin-top-sm title-prodi uppercase font-raleway"><?php if($dt->unit) echo ucWords($dt->unit);
																else echo ucWords($dt->unit_ori); ?></h3>
											<div class="prodi-text text-justify"><?php  echo $strabout; ?></div>
										</a>
									</div>
								<?php
								endforeach;
							endif;
							?>
					</section>
					</section>
				</div>
			</div>


		</div>
	</div>
</section>

<section class="content content-blue">
	<h3 class="text-center title-content margin-top-no margin-bottom-no padding-bottom-no font-raleway"><span><?php 
	if($lang=='en') echo 'Join Master of Computer Science/Informatics </span> <a itemprop="url" rel="alternate" hreflang="x-default"  href="http://selma.ub.ac.id/penerimaan-mahasiswa-baru-program-studi-magister-s2-ilmu-komputerinformatika-program-teknologi-informasi-dan-ilmu-komputer-universitas-brawijaya/" target="_blank" class="btn btn-info">Click Here</a>';
	else echo 'Segera bergabung bersama Program Magister Ilmu Komputer/Informatika</span> <a itemprop="url" rel="alternate" hreflang="x-default"  href="http://selma.ub.ac.id/penerimaan-mahasiswa-baru-program-studi-magister-s2-ilmu-komputerinformatika-program-teknologi-informasi-dan-ilmu-komputer-universitas-brawijaya/" target="_blank" class="btn btn-info">Klik Disini</a>' ?></h3>
			
</section>
<section class="content content-dark with-patern">
	<h2 class="text-center title-content margin-top-sm margin-bottom-no uppercase font-raleway"><span><?php echo berita_agenda ?></span></h2>
	<div class="content-text text-center margin-bottom"></div>
	<div class="container container-dark">
		<div class="row">
			<div class="col-md-6 dark-left">
			
				<?php
						if(isset($pengumuman) && ($pengumuman) ){
							foreach ($pengumuman as $dt):
								$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
								
								/*if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
								else $imgthumb = $this->config->default_thumb_web;*/
								$imgthumb = $this->asset("s2/images/new-icon.png");
											
								?>
								<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location($url.'/read/pengumuman/'.$content.'/'.$dt->id); ?>" class="content-news-event">
									<div class="row">
										<div class="col-xs-2">
											<img class="img-inside-box img-responsive" src="<?php echo $imgthumb; ?>" alt="<?php echo $mpage->content($dt->judul,5);?>">
										</div>
										<div class="col-xs-10">
											<div class="the-title uppercase font-raleway"><?php echo $mpage->content($dt->judul,5);?></div>
											<div class="content-text"><?php echo $mpage->content($dt->keterangan,20);?></div>
										</div>
									</div>
								</a>				
							<?php
							endforeach;
						}
						?>
				
			
			</div>
			<div class="col-md-6">
				<?php
				if($event):
					foreach($event as $dt):
						$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
						//$imgthumb = $this->config->default_thumb_web;
						$imgthumb = $this->asset("s2/images/event-icon.png");
					?>
						<a itemprop="url" rel="alternate" hreflang="x-default"  href="<?php echo $this->location($url.'/read/event/'.$content.'/'.$dt->id); ?>" class="content-news-event">
							<div class="row">
								<!-- revisi 13/1/15 -->
								<div class="col-xs-2 text-right">
									<img class="img-inside-box img-responsive img-responsive-center" src="<?php echo $imgthumb; ?>">
								</div>
								<!--
								<div class="col-xs-2">
									<img class="img-inside-box img-responsive" src="<?php //echo $imgthumb; ?>">
								</div>
								-->
								<div class="col-xs-10">
									<div class="the-title uppercase font-raleway"><?php if($dt->judul) echo $dt->judul;
											else echo $dt->judul_ori;
											?></div>
									<div class="content-text"><?php echo lokasi ?> : <?php echo $dt->lokasi; ?>, <?php echo waktu ?> : <?php echo date("H:i", strtotime($dt->content_modified)); ?></div>
								</div>
							</div>
						</a>
						
					<?php
					endforeach;
			
				endif;
				?>
			</div>
		</div>
	</div>
			
</section>

<section class="content content-white">
	<div class="container">
		<div class="col-md-12">
			<section itemscope="" itemtype="http://schema.org/Article">
				<section itemprop="articleBody">
					<h2 class="title-content text-center margin-top-sm margin-bottom-no uppercase font-raleway"><span><?php echo alasan_s2; ?></span></h2>
					<div class="text-center margin-bottom"></div>
					
					<div class="alasan">
						<div class="row">
							
						<div class="col-sm-10  col-sm-push-2">
											
								<div class="content-filter">
									<div class="row">
										<div class="tab-content" style='line-height:1.8'>
											<?php
											
												if(isset($alasan) && ($alasan)){
													$arr_data=array();
													$j=0;
													foreach ($alasan as $dt):
															$j++;
															if($j==1) $strclass=  "active ";
															else $strclass="";
															
															if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
																else $imgthumb = $this->asset("s2/images/s2-alasan/".($j).".jpg");
															
															$arr_temp = array("id"=>$dt->id, "content"=>$dt->keterangan, "class_str"=>$strclass, "judul"=>$dt->judul, "img"=>$imgthumb);
															array_push($arr_data, $arr_temp);
															?>
															
															<?php
														endforeach;
												}
												
												for($i=0;$i<count($arr_data);$i++){						
													
													?>
													 <div class="tab-pane text-justify fade <?php if($i==0)echo " in ";?><?php  echo $arr_data[$i]["class_str"];?>" id="<?php  echo $arr_data[$i]["id"];?>"><div class="row">
														<div class="col-md-8 col-xs-10">
															<?php  echo str_replace("<h4>","<h4 class='font-raleway font-orange text-uppercase'>",$arr_data[$i]["content"]);	?>
														</div>
														<div class="col-md-4 col-xs-2">
															<?php
															//var_dump($alasan);
															?>
															<img class="img img-responsive" src="<?php echo $arr_data[$i]["img"]; ?>" alt="Gambar Alasan">
														</div>
														</div>
													 </div>
													<?php
												}
										?>
										</div>
									</div>
								</div>
								
							</div>
										
							<div class="col-sm-2 col-sm-pull-10">							
										  
								<div class="menu-filter left-pane menu-filter-other menu-filter-other-2"> 
									<div class="menu-filter-box">
										<ul class="filters list-unstyled"> 	
											<?php
											for($i=0;$i<count($arr_data);$i++){						
													
												?>
												 <li class="<?php  echo $arr_data[$i]["class_str"];?>"><a href="#<?php  echo $arr_data[$i]["id"];?>" role="tab" data-toggle="tab"><img src ="<?php echo $this->asset("s2/images/s2-alasan/icon/".($i+1).".png"); ?>" alt="<?php  echo $arr_data[$i]["judul"];	?>" class="img img-responsive img-responsive-center img-alasan"></a></li>
												<?php
											}
											?>						
										</ul> 
									</div>
								</div>
							</div>
						
					
							<div class="clearfix visible-xs-block"></div>
						</div>
					</div>
					<!--<hr />-->
					</section>
				</section>
			</div>
		  </div>
		</section>

<section class="content content-dark">
	<div class="container">
		<div class="col-md-12">
			<h2 class="title-content text-center margin-top-sm margin-bottom-no font-raleway"><span><?php echo grup_riset?></span></h2>
			<div class="text-center content-text margin-bottom-sm"></div>
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="laboratory-list-slide">
						<?php
							if($group_riset):
								foreach($group_riset as $dt):
									if($dt->unit) $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit))))));	
									else $content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->unit_ori))))));

									
									if($dt->icon):
										$str_ = explode("/", $dt->icon);
										//$imgthumb = $this->config->file_url_view."/upload/icons/unit-kerja/icon/white/".$str_[4]; 
										$imgthumb = $this->config->file_url_view."/".$dt->icon;
									else:
										//2014100026$imgthumb = $this->config->default_thumb_web;
										$imgthumb = $this->config->file_url_view."/upload/icons/unit-kerja/icon/white/2014100022.png";
									endif;
									//if($dt->is_riset=='1'):
										?>												
										
										 <div>
											<a itemprop="url" rel="alternate" hreflang="x-default"   href="<?php echo $this->location('unit/riset/'.strtolower($dt->kode)); ?>" class="link-to-laboratory col-md-3">
												<div class="text-center">
													<div class="show-content">
														<img src="<?php echo $imgthumb;?>" class="img-responsive img-responsive-center" alt="<?php if($dt->unit) echo ucWords($dt->unit);
													else echo ucWords($dt->unit_ori);
													?>">
													</div>
													<div class="hide-content ">
														<?php if($dt->unit) echo ucWords($dt->unit);
															else echo ucWords($dt->unit_ori);
															?>
													</div>
													<div class="more-detail"></div>
												</div>
											</a>
										</div>
									<?php
									//endif;
								endforeach;
						
							endif;
							?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



 <section class="content our-partnership">
	<div class="layer">
		<div class="container">
			<div class="col-md-12">
			<h2 class="title-content text-center title-underline title-underline-orange font-raleway"><span><?php echo partner_kerjasama ?></span></h2>
			<div class="row text-center partnership">
				<?php
							if(isset($kerjasama) && $kerjasama){
								foreach($kerjasama  as $dt):
									if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
									else $imgthumb = $this->config->default_thumb_web;
									
									$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
									
									
									if (isset($url)) $link = $this->location($url.'/read/kerjasama/'.$content.'/'.$dt->id);
									else $link = $this->location('page/read/kerjasama/'.$content.'/'.$dt->id);
									?>
									<div class="col-md-2 col-xs-4"><a href="<?php echo $link; ?>" ><img src="<?php echo $imgthumb; ?>" class="img-responsive img-responsive-center" alt="<?php echo $dt->judul ?>"></a></div>
									<?php
								endforeach;
							}else{
							?>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/gdp.png"); //$mimg->base64_image($this->asset("ptiik/images/gdp.png")); ?>" class="img-responsive img-responsive-center" alt="GDP Logo"><?php echo count($kerjasama)?></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/nokia.png"); //$mimg->base64_image($this->asset("ptiik/images/nokia.png")); ?>" class="img-responsive img-responsive-center" alt="Nokia Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ibm.png"); //$mimg->base64_image($this->asset("ptiik/images/ibm.png")); ?>" class="img-responsive img-responsive-center" alt="IBM Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/cisco.png"); //$mimg->base64_image($this->asset("ptiik/images/cisco.png")); ?>" class="img-responsive img-responsive-center" alt="Cisco Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/oracle.png"); //$mimg->base64_image($this->asset("ptiik/images/oracle.png")); ?>" class="img-responsive img-responsive-center" alt="Oracle Logo"></div>
                            <div class="col-md-2 col-xs-4"><img src="<?php echo $this->asset("ptiik/images/ni.png"); //$mimg->base64_image($this->asset("ptiik/images/ni.png")); ?>" class="img-responsive img-responsive-center" alt="NI Logo"></div>
							<?php } ?>
			</div>
			</div>
		</div>
	</div>
</section>

