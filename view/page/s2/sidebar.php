<!-- Sidebar -->
	<aside class="sidebar">
		
       	<?php if(isset($data["kategori"])&&($data["kategori"]=="contact"||$data["kategori"]=="kontak")){?>
		<div class="box-sidebar well">	
			<?php echo ptiik_alamat_magister?>
		</div>
       	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.437228990733!2d112.61483199999999!3d-7.953687!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78827bc2587bc3%3A0x84290d50ab4804e0!2sProgram+Teknologi+Informasi+dan+Ilmu+Komputer+Universitas+Brawijaya!5e0!3m2!1sid!2sid!4v1420619704188" width="100%" height="250" frameborder="0" style="border:0"></iframe>
		<?php }else{ ?>	
		 <div class="box-sidebar">
            <h3 class="title-content title-underline title-underline-orange margin-top-no margin-bottom-sm no-padding font-raleway uppercase"><span><?php echo paling_sering?></span></h3>
			<div class="list-detail-content">
			<?php
			if($side):
			foreach($side as $dt):
				
				 if($dt->thumb_img){
					$gambar = $this->location($dt->thumb_img);
				 }else{
					$gambar = $this->asset('images/PTIIK.jpg');
				 }
				 
				 ?>
				 <ul class="media-list">
					<li class="media"><a href="<?php 
					if(isset($url)):
						echo $this->location($url.'/read/news/'.$dt->content_page.'/'.$dt->id);
					else:
						echo $this->location('page/read/news/'.$dt->content_page.'/'.$dt->id);
					endif;
					 ?>"><?php echo $dt->content_title; ?></a></li>
				 </ul>
				
			<?php									
			endforeach;
			endif;
			?>	
			</div>
			
		</div>
		
		<div class="box-sidebar">
			<h3 class="title-content title-underline title-underline-orange margin-top-no margin-bottom-sm no-padding font-raleway uppercase"><span>Gallery</span></h3>
			<div class="list-detail-content">
				<ul class="list-unstyled list-gallery">
					<?php 
					
					if(isset($gallery)&&($gallery)){
						foreach($gallery as $key):
							if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
								else $imgthumb = $this->config->default_thumb_web;
								
							?>
								<li>
									<a href="<?php if(isset($url)):
												echo $this->location($url.'/read/gallery');
											else:
												echo $this->location('page/read/gallery');
											endif; ?>">
											<!--<a href="#" class="tube-title-link" data-list="<?php //echo $key->id; ?>" data-lang="<?php //echo $lang; ?>" data-unit="<?php //echo $unit; ?>">-->
										<img class="img-responsive img-responsive-center img-thumbnail" src="<?php echo $imgthumb; ?>">
										<span class="img-overlay"></span>
									</a>
								</li>
							<?php
						endforeach;
					}
					
					?>
					
					
				</ul>			
			</div>
		</div>
         <?php } ?>              
	</aside>
	
	<!-- Sidebar -->
                   