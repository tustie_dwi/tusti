<?php 
$this->view("page/s2/header-s2.php", $data); 
?>


<section class="content content-gray">
	<div class="container">
		<div class="col-md-12">
		<?php 
		if (isset($detail)):
		?>
			
					<!-- Edit Content Article -->
					<section itemscope="" itemtype="http://schema.org/Article">
					<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no" itemprop="name"><?php if($kategori=='faq'){ echo 'Frequently Asked Questions'; }else{ if($detail->judul) echo $detail->judul; 
					else echo $detail->judul_ori; }
				
					?></h1>
						<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						<?php if(isset($detail) && (isset($detail->parent))):?>
							  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php 
									if(isset($url)):
										echo $this->location($url.'/read/'.$kategori.'/'.$detail->id);
									else:
										echo $this->location('page/read/'.$kategori.'/'.$detail->id);
									endif; ?>" itemprop="url"><span itemprop="title"><?php echo ucfirst($detail->parent);?></span></a></li>
						<?php  else : 
									if($kategori=='news'|| $kategori=='berita' || $kategori=='event' || $kategori=='pengumuman' || $kategori=='kerjasama'):
									echo '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href='.$this->location($url.'/read/'.$kategori).' itemprop="url"><span itemprop="title">';
									if($lang=='en'){
										switch($kategori){
											case 'pengumuman':
												echo "Announcement" ;
											break;
											case 'kerjasama':
												echo "Partnership" ;
											break;								
											default :
												echo ucfirst($kategori);
											break;
										}										
									}else{
										if($kategori=='faq') echo 'FAQ';
										else echo ucfirst($kategori);
									} 
									
									echo '</span></a></li>';
									endif;
						?>
									
							<?php endif; ?>
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php 
					  if($kategori=='faq'){ echo 'FAQ'; }else{ if($detail->judul) echo $detail->judul; else echo $detail->judul_ori;}	?></span></li>
					</ol>
					<!-- End Breadcrumb -->
					</section>
					<?php
		endif;
		
		if(isset($post)):
			?>
				<h1 class="title-content margin-top-no margin-bottom-no font-raleway font-orange padding-bottom-no"itemprop="name"><?php
				if($kategori=='faq'){ 
					echo 'Frequently Asked Questions';
					} else{
						if($lang=='en'){
							switch($kategori){
								case 'pengumuman':
									echo "Announcement" ;
								break;
								case 'kerjasama':
									echo "Partnership" ;
								break;								
								default :
									echo ucfirst($kategori);
								break;
							}
							
						}else{
							if($kategori=='faq') echo 'FAQ';
							else echo ucfirst($kategori);
						} 
					} ?></h1>
			<!-- Edit Breadcrumb -->
					<ol class="breadcrumb margin-bottom-no" itemprop="breadcrumb">
					  <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php if(isset($url)) echo $this->location($url); 
						else echo $this->location(); ?>" itemprop="url"><span itemprop="title">Home</span></a></li>
						
					  <li class="active" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php  if($lang=='en'){
							switch($kategori){
								case 'pengumuman':
									echo "Announcement" ;
								break;
								case 'kerjasama':
									echo "Partnership" ;
								break;	
								case 'faq':
									echo "FAQ" ;
								break;									
								default :
									echo ucfirst($kategori);
								break;
							}
							
						}else{
							if($kategori=='faq') echo 'FAQ';
							else echo ucfirst($kategori);
						} ?></span></li>
					</ol>
					<!-- End Breadcrumb -->
			<?php
		endif;
		?>
		</div>
	</div>
</section>
		
<section class="content content-white">
	<div class="container">   		
		<div class="row">
			<div class="col-md-12">
	<!-- Main Content -->
		<div style="line-height:1.8" class="<?php if(isset($kategori) && (($kategori=='lecturer') || ($kategori=='staff')|| ($kategori=='dosen')|| ($kategori=='staff-kependidikan'))) echo "col-md-12"; 
					else echo "col-md-8"?>">	
			<?php		
			if(isset($detail)):
				if($kategori=='event'):
					$this->view("page/detail_event.php", $data);
				else:
					
					
					if($kategori=='course' || $kategori=='matakuliah'):
						$mconf = new model_page;
						$komponen = $mconf->read_silabus("",$lang,$detail->id);
						
						foreach($komponen as $dt): 
								echo "<h3>".$dt->komponen."</h3>";
								echo $dt->keterangan;
						endforeach; 
					else:
						if($detail->keterangan)	$strcontent	= $detail->keterangan;
						else $strcontent = $detail->keterangan_ori;	
					
									?>
						
						<section itemprop="articleBody" class="text-justify">
														
																
							<?php
							$strcontent_ = $mpage->str_content($strcontent,$this->config->file_url_view,$this->config->file_url_old);
							echo $this->add_alt_tags($strcontent_,"a"); 
							
							if(isset($detail->content_data)):
								if(isset($url)) $url_link=$url;
								else $url_link = "";
								$this->read_data($detail->content_data, $url_link);
							endif;
							
							if($detail->judul=='Curriculum' || $detail->judul=='Kurikulum'){
																
								$child = $mpage->read_sub_content("", $lang, "", $detail->id);
								if($child):
								
									echo '<div class="panel-group text-left margin-top-sm" id="accordion" role="tablist" aria-multiselectable="true">';
									$i=0;
									foreach ($child as $key):
										$i++;
										?>
										
										 <div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading<?php echo $i; ?>">
											  <h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" <?php if($i==1) echo 'aria-expanded="true"'; else echo 'class="collapsed" aria-expanded="false"'; ?> aria-controls="collapse<?php echo $i; ?>">
												 <?php echo $key->judul ?>
												</a>
											  </h4>
											</div>
											<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php if($i==1) echo 'in'; ?>" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>">
											   <div class="panel-body"><?php echo str_replace('<p>&nbsp;</p>','&nbsp;',$key->keterangan) ?></div>
											</div>
										  </div>
										<?php
									endforeach;
									echo '</div>';
								endif;
							}
							
							$tab_content = $mpage->read_sub_content($unit, $lang, "tab_content", $detail->id);
							if($tab_content && ($detail->judul!='Curriculum' && $detail->judul!='Kurikulum')){
								?>
								<div class="tabpanel" role="tabpanel">
									<!-- Nav tabs -->
									<ul role="tablist" class="nav nav-pills">
										<?php 
										$arr_data=array();
										$j=0;
										foreach($tab_content as $dt):
											$j++;
											if($j==1) $strclass=  " active ";
											else $strclass="";
											
											
											$strcontent_ = $mpage->str_content($dt->keterangan,$this->config->file_url_view,$this->config->file_url_old);
						
											$content= $this->add_alt_tags($strcontent_,"a"); 
											
											$arr_temp = array("unit_id"=>$dt->unit_id, "content_id"=>$dt->content_id, "id"=>$dt->id, "judul"=>$dt->judul, "content"=>$content, "class_str"=>$strclass);
											array_push($arr_data, $arr_temp);
											?>
											 <li class="<?php echo $strclass;?>" role="presentation"><a href="#<?php echo $dt->id ?>" data-toggle="tab" role="tab" ><?php  echo $dt->judul; ?></a></li>
											<?php
										endforeach;
									
										?>					
									</ul>

									<div class="tab-content">
										<?php
										for($i=0;$i<count($arr_data);$i++){																					
											?>
											 <div class="tab-pane <?php  echo $arr_data[$i]["class_str"];?>" id="<?php  echo $arr_data[$i]["id"];?>" role="tabpanel"><?php  echo $arr_data[$i]["content"];	?></div>
											<?php
										}
										?>
									</div>					
								</div>	
								<?php
							
							}
							?>
							
							<div class="content-pane-tube pull-right">                						
								<div class="tube-share-button">
								<h4>
									<span class="fa fa-share-alt" title="Share"></span> <small>Share</small>
									<a class="fb" href="javascript::" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','fbshare','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-facebook-square"></span></a>
									<a class="twitter" href="javascript::" onclick="window.open('https://twitter.com/intent/tweet?text=Judul&url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>&via=PTIIK_UB','twt','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-twitter-square"></span></a>
									<a class="gplus" href="javascript::" onclick="window.open('https://plus.google.com/share?url=<?php echo $this->config->web_base_url.$_SERVER['REQUEST_URI'] ;?>','gp','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-google-plus-square"></span></a>
									</h4>
								</div>
							</div>
							</section>
							<?php
								
					endif;
					
				
				endif;
							
			endif;	
			
				
			if( isset($post) ) : 
				if($kategori=='faq'){
					?>
					<p>
						<?php if($lang=='en'){ ?>
						Understandably, many of you have asked questions about Master Program of Computer Science/Informatics. We have attempted to capture a number of these questions and respond to them in the following FAQ.
						<?php }else { 
							echo "Berikut adalah tanggapan atas beberapa pertanyaan tentang Program Studi Magister Ilmu Komputer/Informatika PTIIK UB yang berhasil dikelompokkan, antara lain:";
						}?>
						
						
						</p>
					<?php
					echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
					$i=0;
					foreach($post as $dt):
						$i++;
						?>
						
						 <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading<?php echo $i; ?>">
							  <h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" <?php if($i==1) echo 'aria-expanded="true"'; else echo 'class="collapsed" aria-expanded="false"'; ?> aria-controls="collapse<?php echo $i; ?>">
								 <?php echo $dt->judul ?>
								</a>
							  </h4>
							</div>
							<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php if($i==1) echo 'in'; ?>" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>">
							  <div class="panel-body"><?php echo $dt->keterangan ?></div>
							</div>
						  </div>
						<?php
					endforeach;
					echo '</div>';
				}else{
				?>
			
				<table class="table web-page">
					<thead style="display:none">
						<tr><td style="display:none;border:0px;"></td><td>&nbsp;</td></tr>
					</thead>
					<tbody>
					<?php
						if($post) : 
						foreach($post as $dt) { 
							$content=trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($dt->judul))))));
						?>
						<tr style="border-top:0px;">
								<td style="display:none;border:0px;"></td>
							<td style="border-top:0px;">
								<?php 
								if(isset($url)):
									if($kategori=='news') $url_content= $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
									else $url_content = $this->location($url.'/read/'.$kategori.'/'.$content.'/'.$dt->id);
								else:
									if($kategori=='news') $url_content= $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
									else $url_content = $this->location('page/read/'.$kategori.'/'.$content.'/'.$dt->id);
								endif;
								
								if($dt->icon) $imgthumb = $this->config->file_url_view."/".$dt->icon; 
								else $imgthumb = $this->config->default_thumb_web;
								?>
								
                               
                                    <?php 
									if($kategori=='news' || $kategori=='matakuliah' || $kategori=='course') :
									?>
									 <div class="media">
										<a class="pull-left" href="<?php echo $url_content; ?>">
											<?php if($kategori=='news') : ?>
											<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="200px">
											<?php else : ?>
											<img class="media-object img-responsive" src="<?php echo $imgthumb; ?>" width="100px">
											<?php endif; ?>
										</a>
										
										 <div class="media-body">
											<a class="title-article" href="<?php echo $url_content; ?>"><h4 class="media-heading"><?php echo $dt->judul; ?></h4></a>
												<?php if($kategori=='news'){
													?>
													<small>
													<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43">&nbsp;<?php echo date("M d, Y", strtotime($dt->content_modified)); ?></time>
													<span class="category"><span class="fa fa-tags"></span>&nbsp;<?php echo $kategori; ?></span>
													</small><p></p>
													<!--<time class="time-post"><small><?php //if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
													//else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>-->
												<p class="post-content text-justify">
												<?php
												$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
												$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
												$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
											   echo htmlspecialchars($isi);
					
											?>
											<?php //var_dump($dt); ?>
												<a href="<?php echo $url; ?>" class="more-orange">more..</a>
											</p>
											<?php } ?>
										</div>
									</div>					
								<?php 
								else:
									
								?>
									<a class="title-article" href="<?php echo $url_content; ?>"><?php if($dt->judul) echo $dt->judul;
									else echo $dt->judul_ori;
									?></a>
									<time class="time-post"><small><?php if($lang=='in') echo date( 'd M Y',strtotime($dt->content_modified));									
									else echo date( 'M d, Y',strtotime($dt->content_modified));?></small></time>
									<?php
									
									$isi_berita = htmlentities(stripslashes(strip_tags($dt->keterangan))); // membuat paragraf pada isi berita dan mengabaikan tag html
												$isi = substr($isi_berita, 0, 220); // ambil sebanyak 220 karakter
												$isi = substr($isi_berita, 0, strrpos($isi, " ")); // potong per spasi kalimat
											//   echo htmlspecialchars($isi);
								endif;?>
                                   
							</td>
						</tr>
						<?php	
						} 
						endif;	
					?>
				</tbody>
			</table>	
		<?php
			}
		endif; 
		?>
					
	</div>
	<!-- End Main Content -->
	
	<!-- Sidebar -->
	<div class="col-md-4">
	<?php
		if(isset($detail->content_data) && (($detail->content_data=="map")||($detail->content_data=="staff/dosen")|| ($detail->content_data=="staff/staff") || ($detail->content_data=="s2/dosen"))):	
			//var_dump($detail->content_data);
		else:
			
			$this->view("page/s2/sidebar.php",$data);
		endif;
	?>
	 </div>
	 <!-- End Sidebar -->
		</div>
		</div>
	   </div>
   </section>
<?php $this->view("page/s2/footer-s2.php", $data);?>