<?php 
if(isset($detail) && $detail){
	$file_data = $detail->file_data;
	$file_judul = $detail->file_title;
	$file_date = date("M d, Y", strtotime($detail->last_update));
	$file_hit = $detail->content_hit;
	$file_desc = $detail->file_note;
}
?>
        <section id="wrapper">    

            <section class="content content-white">
                <div class="container-fluid container-tube">
                	<div class="row">
                		<div class="col-sm-12 content-theater">
                			<!-- <div id="playStream" class="video-content splash-video color-alt2">
                				<video>
                				<source src="http://175.45.187.253/beta/apps3/assets/video/Raisa%20-%20LDR%20(Official%204K%20MV).mp4" type="video/mp4">
                				</video>
                			</div> -->
                			<?php if($detail->content_category=='video'):
							$file_data = $detail->file_data;
							?>
                			<iframe class="youtube-player" src="<?php if($detail->file_data!="-") echo str_replace("https://www.youtube.com/watch?v=", "//www.youtube.com/embed/",$detail->file_data); ?>" frameborder="0" allowfullscreen></iframe>
							<?php else: 
							$file_data=$this->location().'page/read/gallery/'.$detail->id;
							if($detail->file_loc) $imgthumb = $this->config->file_url_view."/".$detail->file_loc; 
							else $imgthumb = $this->config->default_thumb_web;
							?>
							<img class="img img-responsive img-responsive-center" src="<?php echo $imgthumb ?>">
							<?php endif; ?>
                		</div>
                		<div class="col-sm-12 content-tube-pane">
                			<div class="container">
                				<div class="col-sm-8">
                					<div class="content-pane-tube">
                						<h1 class="title-tube margin-top-no"><?php echo $detail->file_title; ?></h1>
                						<div class="row">
                							<div class="col-sm-8">
                								<div class="metadata-player">
                									Published on <?php echo $file_date ?>
                								</div>
                							</div>
                							<div class="col-sm-4">
                								<div class="viewcount-player">
                									<?php echo $file_hit ?>
                								</div>
                							</div>
                						</div>
                						<hr class="hr-tube">
                						<div class="row">
                							<div class="col-sm-6">
                								<div class="tube-share">
                									<a href="javascript::" type="button" class="with-tooltip collapsed" data-toggle="collapse" data-target="#tube-share-pane"><span class="fa fa-share-alt" title="Share"></span> Share</a>
                								</div>
                							</div>
                						</div>
                					</div>
                					<div class="content-pane-tube" id="tube-share-pane">
                						<h4 class="title-tube margin-top-no">Share</h4>
                						<div class="tube-share-button">
                							<a class="fb" href="javascript::" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $file_data ?>','fbshare','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-facebook-square"></span></a>
                							<a class="twitter" href="javascript::" onclick="window.open('https://twitter.com/intent/tweet?text=Judul&url=<?php echo $file_data ?>&via=PTIIK_UB','twt','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-twitter-square"></span></a>
                							<a class="gplus" href="javascript::" onclick="window.open('https://plus.google.com/share?url=<?php echo $file_data ?>','gp','toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1, width=400, height=300')"><span class="fa fa-google-plus-square"></span></a>
                						</div>
                					</div>
                					<div class="content-pane-tube">
                						<h4 class="title-tube margin-top-no">Deskripsi</h4>
                						<p><?php echo $file_desc ?></p>
                					</div>
                				</div>
                				<div class="col-sm-4">
                					<div class="content-pane-tube">
                						<!--<h3 class="title-tube margin-top-no">Other</h3>-->
                						<div class="row">
											<?php
											if(isset($post) && $post):
												foreach($post as $key):
													if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
													else $imgthumb = $this->config->default_thumb_web;
													if($key->file_id !=$detail->file_id){
											?>
	                							<div class="col-md-12 tube-side-list">
	                								<div class="row">
	                									<div class="col-xs-4">
	                										<a href="#">
					                							<figure class="tube-figure">
					                								<img alt="<?php echo $key->file_title ;?>" src="<?php echo $imgthumb ;?>" class="img-responsive img-responsive-center img-tube img-tube-side">
					                							</figure>	
				                							</a>
	                									</div>
	                									<div class="col-xs-8">
	                										<a href="#" class="tube-title-link tube-title-link-default" data-list="<?php echo $key->id; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>"><?php echo $key->file_title ;?></a>
															<div class="tube-metadata">
																<div>
																	<span class="view-count"><?php echo $key->content_hit; ?> views</span>
																	<span class="time-upload"><?php echo date("M d, Y", strtotime($key->last_update)); ?></span>
																</div>
															</div>
	                									</div>
	                								</div>
	                							</div>
	                						
	                						<?php 
												}
												endforeach;
											endif; ?>
	                						
	                						
	                					</div>
                					</div>
                				</div>
		                	</div>
                		</div>
                		</div>
                	</div>
                
            </section>

        </section>
      <script>
		$(".tube-title-link").click(function(){
			var id = $(this).data("list");
			var lang = $(this).data("lang");
			var unit = $(this).data("unit");
			//	alert("aa"+ val);
			if(unit!=0){
				url = base_url + "/read/read_gallery";
			}else{
				url = base_url + "/page/read_gallery";
			}
				//alert("aa"+ id);
			$.ajax({
					type : "POST",
					  dataType: "HTML",
					  url: url,
					data : $.param({
						id : id,lang:lang, unit:unit
					}),
					success : function(data) {
						$('#content-video').html(data);
					}
			}); 
		});
	  </script>
	  