<html>
    <head>
        <title><?php echo $this->page_title(); ?></title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<?php if(isset($pass) && ($pass==$this->config->file_key)){	
		?>
		<link href="<?php echo $this->asset("ptiik/css/login2.min.css"); ?>" rel="stylesheet">
		<?php
		}else{ ?>
        <link href="<?php echo $this->asset("ptiik/css/login2.min.css"); ?>" rel="stylesheet">
		<?php } ?>
        <link rel="shortcut icon" href="<?php echo $this->asset("images/favicon.ico"); ?>"/>
    
    </head>
	
	 <body>
        <section id="wrapper">
            <div class="container">
                <div class="row">
				<?php
				if(isset($pass) && ($pass==$this->config->file_key)){
					if($posts):
						$val_url = $posts->val_url;
						
						
						$filex = explode(':', $val_url);
						$first = reset($filex);
						
						$file_ = explode('/', $val_url);
							
						$file_ = explode('.', end($file_));
						$ext = end($file_);
						
						$url_var = array('http', 'https');
								
								if(in_array($first, $url_var)){
									$url_val = $val_url;
								}
								else{
									$cek = explode("/", $val_url);
								  if(in_array('kcfinder', $cek)) $url_val= $this->config->web_base_url.$val_url;	  
								  else	$url_val=$this->config->file_base_url.$val_url;
								}
						echo download($url_val, $val_url, $ext);
						exit();
						
						if($ext):
							$img_ext = array('png', 'jpg', 'jpeg', 'gif');
							if(in_array($ext, $img_ext)){
								/*require('library/imageprocessing.php');		
								$imgprocess = new imageprocessing();
							
								$fileprocess = $imgprocess->base64_image($this->config->file_base_url.$val_url);
								if($fileprocess) $file_img=$fileprocess;
								else $file_img = $this->config->file_base_url.$val_url;*/
								$url_var = array('http', 'https');
								
								if(in_array($first, $url_var)){
									$file_img = $val_url;
								}
								else{
									//$file_img=$this->config->file_base_url.$val_url;
									$cek = explode("/", $val_url);
									  if(in_array('kcfinder', $cek)) $file_img= $this->config->web_base_url.$val_url;	  
									  else	$file_img=$this->config->file_base_url.$val_url;
								}
								
							
								echo ' <div class="col-md-4 col-md-offset-4"> <div class="content-box-form content-login"><img src="'.$file_img.'" width="600px"/></div>';
							}else{
								$url_var = array('http', 'https');
								
								if(in_array($first, $url_var)){
									$url_val = $val_url;
								}
								else{
									$cek = explode("/", $val_url);
								  if(in_array('kcfinder', $cek)) $url_val= $this->config->web_base_url.$val_url;	  
								  else	$url_val=$this->config->file_base_url.$val_url;
								}
								?>
								<!--<div class="col-md-4"><h3>PTIIK Files</h3><a href="javascript::" class="btn btn-danger d-file" id="d-file" onclick="window.open('<?php echo $url_val?>','_top','toolbar=no,menubar=no,location=no,status=0,scrollbars=yes,resizable=yes, width=400, height=300')" >Download Here</a></div>-->
								<?php
								 $this->download_file($url_val, $val_url, $ext);
								/*echo '<div class="col-md-8 col-md-offset-4"><iframe src="//docs.google.com/viewer?url='.$url_val.'&embedded=true" width="100%" height="100%"
										style="border: none;"></iframe></div>';*/
									
							}
						endif;
					endif;											
					
				}else{
				?>
                    <div class="col-md-4 col-md-offset-4">
                    	<form class="form form-horizontal form-login box-form" action="<?php echo $this->location("page/read_/".$in_session); ?>" method="post">
                            <div class="content-box-form content-login">
                                <div class="form-group text-center">
                                    <img class="img-responsive img-responsive-center img-logo-login" src="<?php echo $this->asset("ptiik/images/ptiikappslogo.png") ?>">
                                </div>
                                <div class="form-group text-center">
                               		<h1>VIEW FILE</h1>
                                </div>
								
								<div class="alert alert-login text-center" role="alert">
									<?php 
									if(isset($pass) && ($pass!=$this->config->file_key)) echo "Fill in the password correctly";
									else echo "Fill in the password to view the file.";							
									?>
                                </div>
								
                                <div class="form-group">
                                    <div class="col-sm-12">
									    <div class="input-group">
                                        <label class="sr-only" for="exampleInputPassword1">Password</label>
									      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                                   
									      <span class="input-group-btn">
									        <button class="btn btn-default btn-block" type="submit"><span class="fa fa-unlock-alt"></span></button>
									      </span>
									    </div><!-- /input-group -->
									  </div><!-- /.col-lg-6 -->
									</div><!-- /.row -->
                                
                                <div class="form-group">
                                    <div class="col-xs-8">
                                        
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <a href="<?php echo $this->location('page'); ?>"><img class="img img-responsive img-responsive-center" src="<?php echo $this->asset("ptiik/images/logo-login-small.png"); ?>"></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                       
                    </div>
					<?php } ?>
                </div>
            </div>
        </section>
        <footer id="footer">
                          <div>Copyright &copy; 2014 BPTIK PTIIK UB</div>
                            <div>All rights reserved</div>
        </footer>
		<?php
		
		//=============DOWNLOAD===============//
	function getHeaders($url){
	  $ch = curl_init($url);
	  curl_setopt( $ch, CURLOPT_NOBODY, true );
	  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
	  curl_setopt( $ch, CURLOPT_HEADER, false );
	  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	  curl_setopt( $ch, CURLOPT_MAXREDIRS, 3 );
	  curl_exec( $ch );
	  $headers = curl_getinfo( $ch );
	  curl_close( $ch );
	
	  return $headers;
	}
	
	function take_file($url, $path, $ext){
	
		$output_filename = basename($url);
			 			 

		$resource = curl_init();
		curl_setopt($resource, CURLOPT_URL, $url);
		curl_setopt($resource, CURLOPT_HEADER, 0);
		curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_SSLVERSION,3);
		curl_setopt($resource, CURLOPT_BINARYTRANSFER, 1);
		$file = curl_exec($resource);
		curl_close($resource);
		
		$xpath = "assets/upload/".$output_filename;
		
		unlink($xpath);

		$fh = fopen($xpath, 'x');
		fwrite($fh, $file);
		fclose($fh);

		@ob_end_clean(); 
		 
			
		 header('Content-Type: ' . $ext);
		 header('Content-Disposition: attachment; filename="'.$output_filename.'"');
		 header("Content-Transfer-Encoding: binary");
		 header('Accept-Ranges: bytes');
		 
		 /* The three lines below basically make the 
			download non-cacheable */
		 header("Cache-control: private");
		 header('Pragma: private');
		 header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			 
		 header("Content-Length: ". filesize($xpath));
		ob_clean();
		flush();
		readfile($xpath);
		unlink($xpath);
		exit;
		
	}
	
	function download($url, $filename, $ext){
				
		$path = "/".$filename;
		
		$url = str_replace(" ", "%20", $url);
		$headers = getHeaders($url);
		
		if ($headers['http_code'] == 200) {
			$known_mime_types=array(
				"pdf" => "application/pdf",
				"txt" => "text/plain",
				"html" => "text/html",
				"htm" => "text/html",
				"exe" => "application/octet-stream",
				"zip" => "application/zip",
				"doc" => "application/msword",
				"xls" => "application/vnd.ms-excel",
				"ppt" => "application/vnd.ms-powerpoint",
				"gif" => "image/gif",
				"png" => "image/png",
				"jpeg"=> "image/jpg",
				"jpg" =>  "image/jpg",
				"php" => "text/plain"
			 );
			 
			 if($mime_type==''){
				 $file_extension = $ext;
				 if(array_key_exists($file_extension, $known_mime_types)){
					$mime_type=$known_mime_types[$file_extension];
				 } else {
					$mime_type="application/force-download";
				 };
			 };
			 
		
		  if (take_file($url, $path, $mime_type)){
		    echo 'Download complete!';
		  }
		  else{
		  	echo 'Failed!';
		  }
		}
		else{
			echo "File Not Found!";
		}
	}
	
		if(isset($pass) && ($pass==$this->config->file_key)){	
		}else{
		?>
        <script src="<?php echo $this->asset("ptiik/js/jquery-1.11.1.min.js"); ?>"></script>
        <script src="<?php echo $this->asset("ptiik/js/tab.js"); ?>"></script>
        <script src="<?php echo $this->asset("ptiik/js/transition.js"); ?>"></script>
        <script>
           $(document).ready(function () {
				var url_ = $(this).data('url');
				
                generateMargin();

                $(window).resize(function () {
                    generateMargin();
                });
               
                function generateMargin() {
                var $formtop = ($('body').height() - ($('.form').height() + $('#footer').height()));
                if ($formtop <= 0)
                    $formtop = '1%';
                $('body').css('padding-top', $formtop);
            	}
				
				$('a#d-file').attr({target: '_blank', 
                    href  : url_});
           
            });
        </script>
		<?php } ?>
		<script>
			
			$('a.d-file').attr({target: '_blank', 
                    href  : 'http://localhost/directory/file.pdf'});
					
			   document.onkeypress = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123) {
				   //alert('No F-12');
					return false;
				}
			}
			document.onmousedown = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123) {
					//alert('No F-keys');
					return false;
				}
			}
			document.onkeydown = function (event) {
				event = (event || window.event);
				if (event.keyCode == 123) {
					//alert('No F-keys');
					return false;
				}
			}
			var message="";
			function clickIE() {if (document.all) {(message);return false;}} function clickNS(e) {if (document.layers||(document.getElementById&&!document.all)) { if (e.which==2||e.which==3) {(message);return false;}}} if (document.layers) {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;} else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;} document.oncontextmenu=new Function("return false") 
			</script>
    </body>
</html>


	
