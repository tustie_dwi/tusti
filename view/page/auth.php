<?php
class controller_auth extends comscontroller {

	function __construct() {
		parent::__construct();
		$this->session = new session( $this->config->session );
	}
	
	function index() {
		if(isset($_POST['b_submit'])){
			$this->logon();
			exit();
		}	
		$this->add_script('js/auth/index.js');
		$this->view( 'page/index.php' );
		$data['msglogin'] = "";
		//$this->view( 'page/index.php', $data );
	}
	
	function logon() {
		$user = new model_user();
		
		if( $authenticatedUser = $user->authenticate( $_POST['username'], $_POST['password'] ) ) {
			$this->session->set("user", $authenticatedUser);
			$this->redirect('home');
		} else {
			$mpage = new model_page();
				
			$data['page'] 	= $mpage->read_content('page');
			$data['news'] 	= $mpage->read_content('news');
			$data['video'] 	= $mpage->read_video();
			$data['course'] = $mpage->read_course('main');	
			$data['topic'] 	= $mpage->read_topic('page');
			$data['slide']	= $mpage->read_slide();			
			$data['post'] = false;//$mpage->read_content("", 'about');
					
		
			$data['msglogin'] = '<div class="alert alert-block alert-danger fade in">Invalid login. Masukkan username dan password anda dengan benar.</div>';
			$this->view( 'page/index.php', $data );
			exit;
		}
		
	}
	
	function logoff($username) {
		$user 		= new model_user();
		$user->log($username, 'logout');
		
		$this->session->destroy();
		$this->redirect('home');
	}
}
?>