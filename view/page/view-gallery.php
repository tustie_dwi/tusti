<?php 
$this->view("page/skin/ptiik/header.php", $data); 
?>
<div id="content-video">
<script>
	var lang = "<?php echo $lang; ?>";
	var unit = "<?php echo $unit; ?>";
	var limit = 8;
</script>
<?php
if(isset($detail)&&$detail):
	$this->view("page/gallery/index.php", $data); 
else:


	if($kategori=='video' || $kategori=='video-streaming'):
		?>
		 	<section class="content content-theater">
            	<div class="container">
            		<h2 class="title-theater margin-top-no"><span class="fa fa-video-camera"></span> Live Streaming</h2>
                	<div id="liveStream" class="video-content splash-video"></div>
				</div>
			</section>
		<?php
	else:
	?>
		 <section class="content content-white">
                <div class="container container-tube">
                	<div class="row">
                		<div class="col-sm-3 menu-tube-pane">
                			<div class="menu-tube">
                				<div class="menu-tube-header">
                					<button type="button" class="menu-tube-toggle collapsed" data-toggle="collapse" data-target="#menu-tube-collapse">
			                            <span class="fa fa-bars"></span>
			                        </button>
			                        <span class="menu-tube-main-menu">Menu</span>
                				</div>
                				<div class="menu-tube-collapse" id="menu-tube-collapse">
                					<ul class="list-unstyled">
                						<li class="active"><a href="#" id="galleryHome"><span class="fa fa-home"></span> Home</a></li>
                						<li><a href="#"><span class="fa fa-clock-o"></span> Latest Video</a></li>
                					</ul>
                					<hr class="hr-tube">
                					<h5 class="title-menu-tube">Category</h5>
                					<ul class="list-unstyled">
                						
										<li><a href="<?php  echo $this->location('page/read/video-streaming');  ?>"><span class="fa fa-play"></span> Live Streaming</a></li>
                						<li><a href="#" id="videoType"><span class="fa fa-play"></span> Video</a></li>
                						<li><a href="#" id="imagesType"><span class="fa fa-play"></span> Images</a></li>
                					</ul>
                				</div>
                			</div>
                		</div>
                		<div class="col-sm-9 content-tube-pane" id="content-pencarian">
                				<div class="content-pane-tube" id="content-search">
									<input type="text" placeholder="Pencarian Gallery" class="form-control" id="search">
								</div>
                				<div class="content-pane-tube">
	                				<h2 class="title-tube margin-top-no">Latest Video</h2>
	                				<div class="row">										
										<?php
										if(isset($post) && ($post)){
											$i=0;
											
											?>
											<div class="col-sm-6">
											<?php
											foreach($post as $key):
											$i++;
											
											if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
											else $imgthumb = $this->config->default_thumb_web;
											
											if($i==1):
											?>
												<div class="tube-head-home">
													<a href="#" class="tube-title-link" data-list="<?php echo $key->id; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>">
														<figure class="tube-figure">
															<img alt="<?php echo $key->file_title ;?>" src="<?php echo $imgthumb ;?>" class="img-responsive img-responsive-center img-tube">
														</figure>	
													</a>
													<a href="#" class="tube-title-link tube-title-link-default" data-list="<?php echo $key->id; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>"><?php echo $key->file_title ;?></a>
													<div class="tube-metadata">
														<div>
															<span class="view-count"><?php echo $key->content_hit; ?> views</span>
															<span class="time-upload"><?php echo date("M d, Y", strtotime($key->last_update)); ?></span>
														</div>
													</div>
												</div>
											</div>
											<?php 
											endif;
											endforeach;
											 ?>
											<div class="col-sm-6">
												<div class="row">
												<?php
												$i=0;
												$arr_data=array();
													foreach($post as $key):
														if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
														else $imgthumb = $this->config->default_thumb_web;
														$i++;
														if($i>1 && $i<7):
																
															?>
															<div class="col-md-12 tube-side-list">
																<div class="row">
																	<div class="col-xs-4">
																		<a href="#" class="tube-title-link" data-list="<?php echo $key->id; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>">
																			<figure class="tube-figure">
																				<img alt="<?php echo $key->file_title ;?>" src="<?php echo $imgthumb ;?>" class="img-responsive img-responsive-center img-tube img-tube-side">
																			</figure>	
																		</a>
																	</div>
																	<div class="col-xs-8">
																		<a href="#" class="tube-title-link tube-title-link-default" data-list="<?php echo $key->id; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>"><?php echo $key->file_title ;?></a>
																		<div class="tube-metadata">
																			<div>
																				<span class="view-count"><?php echo $key->content_hit; ?> views</span>
																				<span class="time-upload"><?php echo date("M d, Y", strtotime($key->last_update)); ?></span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>									
															<?php
													
														else:
															$arr_temp = array("id"=>$key->id, "hit"=>$key->content_hit, "no"=>$i, "judul"=>$key->file_title, "icon"=>$imgthumb,"tgl"=>$key->last_update, "note"=>$key->file_note);
															array_push($arr_data, $arr_temp);
														endif;
													endforeach;
													?>
	                						</div>
	                					</div>
	                					<?php
											
										}
										?>
										
										
										
	                				</div>
                				</div>
								<!-- end head -->
								<div class="content-pane-tube">
									<h4 class="title-tube margin-top-no">Video
									<!--<a href="#" class="btn btn-primary btn-more-cat text-small pull-right"><span class="fa fa-chevron-circle-right"></span> More</a>-->
									</h4>
									<div class="content-list-category-landscape">
										<div class="row">
											<div class="col-md-12">
												<?php												
												for($i=0;$i<count($arr_data);$i++){	
													if($i<5){
														?>
														<div class="col-sm-3">
															<a href="#" class="tube-title-link" data-list="<?php echo $arr_data[$i]["id"]; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>">
																<figure class="tube-figure">
																	<img src="<?php echo $arr_data[$i]["icon"]; ?>" class="img-responsive img-responsive-center img-tube img-tube-thumb">
																</figure>	
															</a>
															<a href="#" class="tube-title-link tube-title-link-default" data-list="<?php echo $arr_data[$i]["id"]; ?>" data-lang="<?php echo $lang; ?>" data-unit="<?php echo $unit; ?>"><?php echo $arr_data[$i]["judul"]; ?></a>
															<div class="tube-metadata">
																<div>
																	<span class="view-count"><?php echo $arr_data[$i]["hit"]; ?> views</span>
																	<span class="time-upload"><?php echo date("M d, Y", strtotime($arr_data[$i]["tgl"])); ?></span>
																</div>
															</div>
														</div>	
														<?php 
													}
												} ?>
											</div>
										</div>
									</div>
			                		<!-- end video -->	
									<?php
									if(isset($gcat) && $gcat){
										foreach($gcat as $dt):
											$detail_data = $mpage->read_slide('0','in',10,"'gallery'","",$dt->file_id);
											if($detail_data):
										?>
											<hr class="hr-tube">
											<h4 class="title-tube margin-top-no"><?php echo $dt->file_title; ?>
												<!--<a href="#" class="btn btn-primary btn-more-cat text-small pull-right"><span class="fa fa-chevron-circle-right"></span> More</a>-->
												</h4>
												<div class="content-list-category-landscape">
													<div class="row">
														<div class="col-md-12">
															<?php
															$i=0;
															foreach($detail_data as $key):	
																$i++;
																if($i<5){
																	if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
																	else $imgthumb = $this->config->default_thumb_web;
																	?>
																	<div class="col-sm-3 ">
																		<a href="#" class="tube-title-link" data-list="<?php echo $key->id; ?>" data-lang="in" data-unit="<?php echo $unit; ?>">
																			<figure class="tube-figure">
																				<img src="<?php echo $imgthumb; ?>" class="img-responsive img-responsive-center img-tube img-tube-thumb">
																			</figure>	
																		</a>
																		<a href="#" class="tube-title-link tube-title-link-default" data-list="<?php echo $key->id; ?>" data-lang="in" data-unit="<?php echo $unit; ?>"><?php echo $key->file_title; ?></a>
																		<div class="tube-metadata">
																			<div>
																				<span class="view-count"><?php echo $key->content_hit; ?> views</span>
																				<span class="time-upload"><?php echo date("M d, Y", strtotime($key->last_update)); ?></span>
																			</div>
																		</div>
																	</div>	
																	<?php 
																}
															endforeach; ?>
														</div>
													</div>
												</div>
												<!-- end category -->	
										<?php
											endif;
										endforeach;
									}
									?>
			                						                			
			                	</div>
							<!-- end -->
                				
                			</div>
                		</div>
                	</div>
                
            </section>
	<?php endif;
endif;
	?>
</div>
	
<?php $this->view("page/skin/ptiik/footer.php", $data); ?>