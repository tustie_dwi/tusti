<?php
class comscontroller extends controller {
    
	private $scripts = array();
	private $styles = array();
	
	private $page_title = NULL;
	private $isAuthenticated = false;
	
	public $session = NULL;
	public $authenticatedUser = NULL;
	
	private $comsmodules = NULL;
	
    public function __construct(){
		
        parent::__construct();  
  		
  		if(!defined('MODULE'))
  			define('MODULE','modules/');
  		      
		$this->page_title = $this->config->page_title;		
		$this->loadAllModules();
		
		/*if(!$this->authenticatedUser) {
			$this->authenticatedUser->username = "guest";
			$this->authenticatedUser->level = 0;
			$this->authenticatedUser->role = "guest";
			$this->authenticatedUser->module = "all";
			$this->authenticatedUser->name = "Guest";
			$this->authenticatedUser->status = 0;
			$this->authenticatedUser->staffid = 0;
		}	*/	
		
	}
	
	public function loadAllModules() {

		$mmodule = new model_module();
		$this->comsmodules = $mmodule->getAllActiveModules(); //var_dump($this->comsmodules);
		
		
		autoloader::register(array($this, 'autoload_module_controller'));
		autoloader::register(array($this, 'autoload_module_model'));
		autoloader::register(array($this, 'autoload_module_library'));	
	}
	
	public function require_auth($authcontroller = false) {
		$this->session = new session( $this->config->session );
		if( $authcontroller !== false ) {
				
			$this->authenticatedUser = $this->session->get('user');
			if($this->authenticatedUser) {
				$this->headerdata['authenticated'] = true;
			} else {
				$this->sredirect($authcontroller);
				exit;
			}		
			
		} else {
			$this->headerdata['authenticated'] = false;
			$this->headerdata['user'] = NULL;
		}		
	}
	
	
	
	function  get_nama_bulan($bln=NULL){
		switch($bln){
			   case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;    
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }
		}
		
		return $bln;
	}
	
	function kekata($x=NULL) { 
		$x = abs($x); 
		$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"); 
		$temp = ""; 
		if ($x <12) { 
			$temp = " ". $angka[$x]; 
		} else if ($x <20) { 
			$temp = $this->kekata($x - 10). " belas"; 
		} else if ($x <100) { 
			$temp = $this->kekata($x/10)." puluh". $this->kekata($x % 10); 
		} else if ($x <200) { 
			$temp = " seratus" . $this->kekata($x - 100); 
		} else if ($x <1000) { 
			$temp = $this->kekata($x/100) . " ratus" . $this->kekata($x % 100); 
		} else if ($x <2000) { 
			$temp = " seribu" . $this->kekata($x - 1000); 
		} else if ($x <1000000) { 
			$temp = $this->kekata($x/1000) . " ribu" . $this->kekata($x % 1000); 
		} else if ($x <1000000000) { 
			$temp = $this->kekata($x/1000000) . " juta" . $this->kekata($x % 1000000); 
		} else if ($x <1000000000000) { 
			$temp = $this->kekata($x/1000000000) . " milyar" . $this->kekata(fmod($x,1000000000)); 
		} else if ($x <1000000000000000) {
			$temp = $this->kekata($x/1000000000000) . " trilyun" . $this->kekata(fmod($x,1000000000000)); 
		} 
		
		return $temp; 
	} 

	function terbilang($x, $style=NULL) { 
		if($x<0) { 
			$hasil = "minus ". trim($this->kekata($x)); 
		} else { 
			if($x==0){ 
				$hasil = trim($this->kekata($x))." "; 
			}else{ 
				$hasil = trim($this->kekata($x))." rupiah"; 
			} 
		} 
		
		switch ($style) { 
			case 1: $hasil = strtoupper($hasil); break; 
			case 2: $hasil = strtolower($hasil); break; 
			case 3: $hasil = ucwords($hasil); break; 
			default: $hasil = ucfirst($hasil); break; 
		} 
		
		return $hasil; 
	} 
	
	
	public function upload_file($upload_dir, $filedata, $filename, $filenamenew, $filesize){
		$url	     = $this->config->file_url;
		
		$headers = array("Content-Type:multipart/form-data");
		$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		$ch = curl_init();
		$options = array(
			 CURLOPT_URL => $url,
			CURLOPT_HEADER => true,
			CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
			CURLOPT_POST => 1,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POSTFIELDS => $postfields,
			CURLOPT_INFILESIZE => $filesize,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => 2
		
		); // cURL options 
		curl_setopt_array($ch, $options);
		curl_exec($ch);
		if(!curl_errno($ch))
		{
			$info = curl_getinfo($ch);
			if ($info['http_code'] == 200)
			   $errmsg = "File uploaded successfully";
		}
		else
		{
			$errmsg = curl_error($ch);
		}
		curl_close($ch);
		//------UPLOAD USING CURL----------------
	}
	
	
	public function generate_password($length){
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			
		return substr(str_shuffle($chars),0,$length);	
	}
	
	public function kirim_email($data){
		$status		= 1;
		
		require ('library/PHPMailerAutoload.php');
		//var_dump($data['email']);
		
		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->Host = 'smtp.ub.ac.id';
		$mail->SMTPAuth = true;
		$mail->Username = $this->config->default_email_acc;
		$mail->Password = $this->config->default_email_pass;
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465;

		$mail->From = $this->config->default_email_acc;
		$mail->FromName = 'Badan Pengembangan Teknologi Informasi dan Komunikasi PTIIK UB';
		$mail->addAddress($data['email'], $data['nama']);
		$mail->addReplyTo($this->config->default_email_acc,'Badan Pengembangan Teknologi Informasi dan Komunikasi PTIIK UB');

		$mail->isHTML(true);

		$mail->Subject = $data['subject'];
		$mail->Body    = $data['body'];
		$mail->AltBody = $data['textbody'];

		if($mail->send()){
			$error=0;
			$data['err'] = 1;
	
			return true;
		}else{
			$error=1;
			$data['err'] = 2;

			return false;
		}
	}
	
	
	
	public function head($ishome=false) {
		$mmodule = new model_module();
		
		$this->comsmenu	= $mmodule->getAllActiveMenu($this->authenticatedUser->level);
		
		$navbar['user'] 	= $this->authenticatedUser;   //$this->session->get("user");
		$navbar['modules'] 	= $this->comsmodules;
		$navbar['menus']  	= $this->comsmenu;
		
		$navbar['userid'] 	= $this->authenticatedUser->id;
		$navbar['username'] = $this->authenticatedUser->username;
		$navbar['role']   	= $this->authenticatedUser->role;
		$navbar['level']   	= $this->authenticatedUser->level;
		$navbar['staffid'] 	= $this->authenticatedUser->staffid;
		$navbar['mhsid']   	= $this->authenticatedUser->mhsid;
		$navbar['foto']   	= $this->authenticatedUser->foto;
		$navbar['ishome']	= $ishome;
				
		$this->view('header.php', $navbar);
		$this->view('navbar.php', $navbar);	
		//$this->view('sidebar.php', $navbar);
		
	}
	
	public function foot() {
		$this->view('footer.php');		
	}
	
	public function footblank() {
		$this->view('footer-nonjs.php');		
	}
	
	public function add_style($stylepath) {
		if(file_exists( $this->config->assets_folder . "/" . $stylepath ))
			$this->styles[]	= $stylepath;
	}
	
	public function add_script($scriptpath) {
		if(file_exists( $this->config->assets_folder . "/" . $scriptpath ))
			$this->scripts[] = $scriptpath;
	}
	
	public function get_scripts() {
		return $this->scripts;
	}
	
	public function get_styles() {
		return $this->styles;	
	}
	
	public function page_title($appended_string = NULL) {
		return $this->page_title . $appended_string;	
	}
	
	public function no_privileges() {
		$this->view('error/noprivileges.php');
		exit;
	}
	
	public function autoload_module_controller( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		@include_once MODULE . $this->module->name . "/controller/" 
			. trim( strstr( $classPath, "/" ), "/" ) .'.php';
			
	}
	
	public function autoload_module_model( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		include_once MODULE . $this->module->name . "/model/" 
			. trim( strstr( $classPath, "/" ), "/" ) .'.php';
			
	}
	
	public function autoload_module_library( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		@include_once MODULE . $this->module->name . "/library/" 
			. trim( strstr( $classPath, "/" ), "/" ) .'.php';
				
	}
	
}