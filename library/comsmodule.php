<?php

class comsmodule extends comscontroller {
	
	private $module = NULL;
	private $coms = NULL;
	private $styles = array();
	private $scripts = array();
	
	public function __construct($coms = NULL) {
		if( $coms ) {
			$this->module = $coms->module;
			$this->coms = $coms;
		}
		
		$mmodule = new model_module();
		
	
		//$this->comsmenu	= $mmodule->getAllActiveMenu($this->authenticatedUser->level);
				
		$this->comsmodules = $mmodule->getAllActiveModules();
							
		parent::__construct();
	}
	
	function  get_nama_bulan($bln=NULL){
		switch($bln){
			   case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;    
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }
		}
		
		return $bln;
	}
	
	function kekata($x=NULL) { 
		$x = abs($x); 
		$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"); 
		$temp = ""; 
		if ($x <12) { 
			$temp = " ". $angka[$x]; 
		} else if ($x <20) { 
			$temp = $this->kekata($x - 10). " belas"; 
		} else if ($x <100) { 
			$temp = $this->kekata($x/10)." puluh". $this->kekata($x % 10); 
		} else if ($x <200) { 
			$temp = " seratus" . $this->kekata($x - 100); 
		} else if ($x <1000) { 
			$temp = $this->kekata($x/100) . " ratus" . $this->kekata($x % 100); 
		} else if ($x <2000) { 
			$temp = " seribu" . $this->kekata($x - 1000); 
		} else if ($x <1000000) { 
			$temp = $this->kekata($x/1000) . " ribu" . $this->kekata($x % 1000); 
		} else if ($x <1000000000) { 
			$temp = $this->kekata($x/1000000) . " juta" . $this->kekata($x % 1000000); 
		} else if ($x <1000000000000) { 
			$temp = $this->kekata($x/1000000000) . " milyar" . $this->kekata(fmod($x,1000000000)); 
		} else if ($x <1000000000000000) {
			$temp = $this->kekata($x/1000000000000) . " trilyun" . $this->kekata(fmod($x,1000000000000)); 
		} 
		
		return $temp; 
	} 

	function terbilang($x, $style=NULL) { 
		if($x<0) { 
			$hasil = "minus ". trim($this->kekata($x)); 
		} else { 
			if($x==0){ 
				$hasil = trim($this->kekata($x))." "; 
			}else{ 
				$hasil = trim($this->kekata($x))." rupiah"; 
			} 
		} 
		
		switch ($style) { 
			case 1: $hasil = strtoupper($hasil); break; 
			case 2: $hasil = strtolower($hasil); break; 
			case 3: $hasil = ucwords($hasil); break; 
			default: $hasil = ucfirst($hasil); break; 
		} 
		
		return $hasil; 
	} 
	
	
	
	function getHeaders($url){
	  $ch = curl_init($url);
	  curl_setopt( $ch, CURLOPT_NOBODY, true );
	  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
	  curl_setopt( $ch, CURLOPT_HEADER, false );
	  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	  curl_setopt( $ch, CURLOPT_MAXREDIRS, 3 );
	  curl_exec( $ch );
	  $headers = curl_getinfo( $ch );
	  curl_close( $ch );
	
	  return $headers;
	}
	
	function take_file($url, $path, $ext){
	
		$output_filename = basename($url);
			 			 

		$resource = curl_init();
		curl_setopt($resource, CURLOPT_URL, $url);
		curl_setopt($resource, CURLOPT_HEADER, 0);
		curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_SSLVERSION,3);
		curl_setopt($resource, CURLOPT_BINARYTRANSFER, 1);
		$file = curl_exec($resource);
		curl_close($resource);
		
		$xpath = "assets/upload/".$output_filename;
		
		unlink($xpath);

		$fh = fopen($xpath, 'x');
		fwrite($fh, $file);
		fclose($fh);

		@ob_end_clean(); 
		 
			
		 header('Content-Type: ' . $ext);
		 header('Content-Disposition: attachment; filename="'.$output_filename.'"');
		 header("Content-Transfer-Encoding: binary");
		 header('Accept-Ranges: bytes');
		
		 header("Cache-control: private");
		 header('Pragma: private');
		 header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			 
		 header("Content-Length: ". filesize($xpath));
		ob_clean();
		flush();
		readfile($xpath);
		unlink($xpath);
		exit;
		
	}
	
	public function download_file($url, $filename, $ext){
				
		$path = "/".$filename;
		
		$url = str_replace(" ", "%20", $url);
		$headers = $this->getHeaders($url);
		
		if ($headers['http_code'] == 200) {
			$known_mime_types=array(
				"pdf" => "application/pdf",
				"txt" => "text/plain",
				"html" => "text/html",
				"htm" => "text/html",
				"exe" => "application/octet-stream",
				"zip" => "application/zip",
				"doc" => "application/msword",
				"xls" => "application/vnd.ms-excel",
				"ppt" => "application/vnd.ms-powerpoint",
				"gif" => "image/gif",
				"png" => "image/png",
				"jpeg"=> "image/jpg",
				"jpg" =>  "image/jpg",
				"php" => "text/plain"
			 );
			 
			 if($mime_type==''){
				 $file_extension = $ext;
				 if(array_key_exists($file_extension, $known_mime_types)){
					$mime_type=$known_mime_types[$file_extension];
				 } else {
					$mime_type="application/force-download";
				 };
			 };
			 
		
		  if ($this->take_file($url, $path, $mime_type)){
		    echo 'Download complete!';
		  }
		  else{
		  	echo 'Failed!';
		  }
		}
		else{
			echo "File Not Found!";
		}
	}
	
	
	public function ext_file($str){
		
		$ext = explode(".", $str);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	public function upload_file($upload_dir, $filedata, $filename, $filenamenew, $filesize){
		$url	     = $this->config->file_url;
		
		$headers = array("Content-Type:multipart/form-data");
		$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		$ch = curl_init();
		$options = array(
			 CURLOPT_URL => $url,
			CURLOPT_HEADER => true,
			CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
			CURLOPT_POST => 1,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POSTFIELDS => $postfields,
			CURLOPT_INFILESIZE => $filesize,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => 2
		
		); // cURL options 
		curl_setopt_array($ch, $options);
		curl_exec($ch);
		if(!curl_errno($ch))
		{
			$info = curl_getinfo($ch);
			if ($info['http_code'] == 200)
			   $errmsg = "File uploaded successfully";
		}
		else
		{
			$errmsg = curl_error($ch);
		}
		curl_close($ch);
		//------UPLOAD USING CURL----------------
	}
	public function add_alt_tags($content,$title){
        preg_match_all('/<img (.*?)\/>/', $content, $images);
        if(!is_null($images))
        {
			
			//$mpage = new model_page();
						
                foreach($images[1] as $index => $value)
                {
				
					$imgsrc = explode('src', $value);
							preg_match_all('/".*?"|\'.*?\'/', $imgsrc[1], $source);
							
							if(isset($source[0][0])){
								$file_tmp = substr(str_replace('%20', ' ', $source[0][0]), 1, -1);
								$cek = explode('/', $file_tmp);
								$strtitle = end($cek);
								$strtitle = explode(".", $strtitle);
								$title  = reset($strtitle);
							}else{
								$file_tmp ="";
								$title = "-";
							}
							
									
							$new_img = str_replace('<img', '<a href="'.$file_tmp.'" class="fancybox content-gallery" title="'.$title.'"><img class="img-gallery" alt="'.$title.'"', $images[0][$index])."</a>";
						
							$content = str_replace($images[0][$index], $new_img, $content);
                }
        }
        return $content;
	}
	
	function kirim_sms($data){
		$sms = new model_sms();		
		
		$data = array('SendingDateTime'=>$data['created_date'],'DestinationNumber'=>$data['to'], 'TextDecoded'=>$data['note']);		
		
		$sms->send_sms_notif($data);
	}

	public function kirim_email($data, $init=1){
		$status		= 1;
		
		if($init==1) require ('library/PHPMailerAutoload.php');

		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->Host = 'smtp.ub.ac.id';
		$mail->SMTPAuth = true;
		$mail->Username = $this->config->default_email_acc;
		$mail->Password = $this->config->default_email_pass;
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465;

		$mail->From = $this->config->default_email_acc;
		$mail->FromName = 'Badan Pengembangan Teknologi Informasi dan Komunikasi PTIIK UB';
		$mail->addAddress($data['email'], $data['nama']);
		$mail->addReplyTo($this->config->default_email_acc,'Badan Pengembangan Teknologi Informasi dan Komunikasi PTIIK UB');

		$mail->isHTML(true);

		$mail->Subject = $data['subject'];
		$mail->Body    = $data['body'];
		$mail->AltBody = $data['textbody'];

		if($mail->send()){
			$error=0;
			$data['err'] = 1;
			
			echo "sukses";
			return true;
		}else{
			$error=1;
			$data['err'] = 2;			
			
			echo "gagal";
			return false;
		}
	}
	

	
	public function head() {
		$navbar['user'] = $this->coms->authenticatedUser;   //$this->session->get("user");
		$navbar['modules'] = $this->comsmodules;		
		//$navbar['menus']	= $this->comsmenu;
		
				
		$navbar['userid'] 	= $this->coms->authenticatedUser->id;
		$navbar['username'] = $this->coms->authenticatedUser->username;
		$navbar['role']   	= $this->coms->authenticatedUser->role;
		$navbar['module'] 	= $this->coms->authenticatedUser->module;
		$navbar['menus']   = $this->coms->authenticatedUser->menus;
		$navbar['staffid'] 	= $this->coms->authenticatedUser->staffid;
		$navbar['mhsid']   	= $this->coms->authenticatedUser->mhsid;
		$navbar['foto']   	= $this->coms->authenticatedUser->foto;
		
		$sidebar = $navbar;
			
		$this->coms->view('header.php', array('mstyles'=>$this->styles));
		$this->coms->view('navbar.php', $navbar);	
		//$this->coms->view('sidebar.php', $navbar);	
	}
	
	public function foot() {
		//var_dump($this->coms->get_scripts());
		//var_dump($this->scripts);
		$this->coms->view('footer.php', array('mscripts'=>$this->scripts));		
	}
	
	public function view( $viewpath, $data = array() ) {
		
		$view = APPLICATION . "modules/" . $this->module->name . "/view/" . $viewpath;
		
		// extract given data arguments array as variables
		extract( $data, EXTR_SKIP ); //var_dump( $data) ;
		if( is_readable( $view ) )
			include $view;
		else $this->error->notfound( "View: " . $view . " could not be found!" );		
		

	}
	
	public function location( $path = NULL ) {
		
		if(substr($this->config->index_file, strlen($this->config->index_file)-1, 1) != "/" 
			&& strlen(trim($this->config->index_file)) > 0 )
			$this->config->index_file .= "/";
		$location = str_replace( "//", "/",  $this->config->index_file . $path );
		
		return $this->config->base_url() . $location;
	}
	
	public function assets( $path = NULL, $coms = false ) {
		$location = str_replace( "//", "/modules/", $this->module->name . "/assets/" . $path );
		return $this->config->base_url() . $location;		
	}
	
	public function add_style($stylepath) {
		$style = 'modules/' . $this->module->name . "/assets/" . $stylepath;
		if(file_exists( $style ))
			$this->styles[]	= $style;
	}
	
	public function add_script($scriptpath) {
		$script = 'modules/' . $this->module->name . "/assets/" . $scriptpath;
		if(file_exists( $script ))
			$this->scripts[] = $script;
	}
	
	public function get_scripts() {
		return $this->scripts;
	}
	
	public function get_styles() {
		return $this->styles;	
	}
	
	/*
	 * Fungsi tambahan
	 * created by Agie Ghazy Falah : 4 Agt 2015
	 * untuk keperluan Modul Skripsi V3
	 */
	 
	 public function getJabatan($nik=NULL){
		 $mservice = new model_services();		
		 return $mservice->get_jabatan($nik);
	 }
	 
	 public function get_next_id($nama_table=NULL, $primary_key=NULL){
	 	$mservice = new model_services();		
		return $mservice->get_next_id($nama_table, $primary_key);
	}
	
	
	public function get_date_format($data){
		$data = explode("-", $data);
		if(! isset($data[2])) return '-';
		
		switch ($data[1]) {
		    case '01': $bln = "Jan"; break;
		    case '02': $bln = "Feb"; break;
		    case '03': $bln = "Mar"; break;
		    case '04': $bln = "Apr"; break;
		    case '05': $bln = "Mei"; break;
		    case '06': $bln = "Jun"; break;
		    case '07': $bln = "Jul"; break;
		    case '08': $bln = "Agt"; break;
		    case '09': $bln = "Sep"; break;
		    case '10': $bln = "Okt"; break;
		    case '11': $bln = "Nov"; break;
		    case '12': $bln = "Des"; break;
		    default: $bln = 'Jan'; break;
		}
		
		return $data[2] . ' ' . $bln . ' ' . $data[0];
	}
	public function get_day($tgl){
		$day = date('D', strtotime($tgl));
		
		switch ($day) {
			case 'Mon': $day = 'Senin'; break;
			case 'Tue': $day = 'Selasa'; break;
			case 'Wed': $day = 'Rabu'; break;
			case 'Thu': $day = 'Kamis'; break;
			case 'Fri': $day = 'Jumat'; break;
			case 'Sat': $day = 'Sabtu'; break;
			case 'Sun': $day = 'Minggu'; break;
		}
		return $day;
	}
	public function get_date_range($data){
		$start_date = new DateTime($data);
		$since_start = $start_date->diff(new DateTime(date('Y-m-d H:i:s')));
		
		if($since_start->y > 0) return $since_start->y.' tahun';
		elseif($since_start->m > 0) return $since_start->m.' bulan';
		elseif($since_start->d > 0) {
			if($since_start->d / 7 > 1) return round($since_start->d / 7) . ' minggu';
			else return $since_start->d.' hari';
		}
		elseif($since_start->h > 0) return $since_start->h.' jam';
		elseif($since_start->i > 0) return $since_start->i.' menit';
		elseif($since_start->s > 0) return $since_start->s.' detik';
		else return '1 detik';
	}	
	
	public function get_date_range_from_now($data){
		$start_date = new DateTime(date('Y-m-d H:i:s'));
		$since_start = $start_date->diff(new DateTime(date($data)));
		
		if($since_start->y > 0) return $since_start->y.' tahun';
		elseif($since_start->m > 0) return $since_start->m.' bulan';
		elseif($since_start->d > 0) {
			if($since_start->d / 7 > 1) return round($since_start->d / 7) . ' minggu';
			else return $since_start->d.' hari';
		}
		elseif($since_start->h > 0) return $since_start->h.' jam';
		elseif($since_start->i > 0) return $since_start->i.' menit';
		elseif($since_start->s > 0) return $since_start->s.' detik';
		else return '1 detik';
	}	
	
	public function file_size_format($data){
		if($data/1000000000 > 1) return round(($data/1000000000),2) . ' GB';
		elseif($data/1000000 > 1) return round(($data/1000000),2) . ' MB';
		elseif($data/1000 > 1) return round(($data/1000),2) . ' KB';
		else return $data . ' B';
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}