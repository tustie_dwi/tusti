<?php
class imageprocessingtmp {
	
    public function base64_img ( $image_path, $height = null, $width = null )
	{
		// Cek if url image is valid
		$segments = @file_get_contents( $image_path , 0 ,NULL , 0 , 1 );
		if( $segments )
		{
		
			// Get type of file image from target path
			$type = pathinfo( $image_path, PATHINFO_EXTENSION );
			
			// Read image binary and convert into string
			$data = file_get_contents( $image_path );
			
			// Is image height and width set?
			if( isset( $height ) && isset( $width ) )
			{
				$base64 = 'data:image/' . $type . ';base64,' . base64_encode( $data ) . '" height="' . $height . '" width="' . $width . '"';
			}
			else
			{
				$base64 = 'data:image/' . $type . ';base64,' . base64_encode( $data );
			}
			//return $base64;
		}
		else
		{
				
			// Show default image if function failed find image location
			$image_path = $this->config->default_thumb ;
			$type = pathinfo( $image_path, PATHINFO_EXTENSION );
			
			$data = file_get_contents( $image_path );
			$base64 =  $image_path;
			//$base64 = 'data:image/' . $type . ';base64,' . base64_encode( $data );
		}
		//return "aa";
		return $base64;
	}
	
}