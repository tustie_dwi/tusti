<?php
class model_layanan extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL, $level=NULL, $unit=NULL, $oleh=NULL, $proses=NULL){
		$sql = "SELECT
					mid(md5(`tbl_layanan`.`permintaan_id`),5,5) as `id`,
					`db_ptiik_apps`.`tbl_layanan`.`permintaan_id`,
					`db_ptiik_apps`.`tbl_layanan`.`unit_kerja_dituju`,
					`db_ptiik_apps`.`tbl_layanan`.`status_proses`,
					`db_ptiik_apps`.`tbl_layanan`.`judul`,
					`db_ptiik_apps`.`tbl_layanan`.`keterangan`,
					`db_ptiik_apps`.`tbl_layanan`.`no_permintaan`,
					`db_ptiik_apps`.`tbl_layanan`.`periode`,
					`db_ptiik_apps`.`tbl_layanan`.`tgl_permintaan`,
					`db_ptiik_apps`.`tbl_layanan`.`tgl_harapan_selesai`,
					`db_ptiik_apps`.`tbl_layanan`.`approve_by`,
					`db_ptiik_apps`.`tbl_layanan`.`catatan`,
					`db_ptiik_apps`.`tbl_layanan`.`tgl_proses`,
					`db_ptiik_apps`.`tbl_layanan`.`tgl_mulai_pengerjaan`,
					`db_ptiik_apps`.`tbl_layanan`.`tgl_deadline`,
					`db_ptiik_apps`.`tbl_layanan`.`request_dari`,
					`db_ptiik_apps`.`tbl_layanan`.`is_close`,
					`db_ptiik_apps`.`tbl_layanan`.`is_publish`,
					`db_ptiik_apps`.`tbl_layanan`.`is_jenis`,
					`a`.`nama`,
					`a`.`email` as `email_user`,
					`c`.`nama` as `disetujui_oleh`, 
					`c`.`email` as `email_persetujuan`,
					`b`.`nama` AS `unit_peminta`,
					`db_ptiik_apps`.`tbl_layanan_file`.`nama_file`,
					`db_ptiik_apps`.`tbl_layanan_file`.`lokasi`,
					`db_ptiik_apps`.`tbl_unit_kerja`.`nama` AS `nama_unit_dituju`,
					`db_ptiik_apps`.`tbl_layanan_status`.`keterangan` AS `st_proses`
				FROM
					`db_ptiik_apps`.`tbl_layanan`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` AS `a` ON `db_ptiik_apps`.`tbl_layanan`.`request_dari` = `a`.`karyawan_id`
					left Join `db_ptiik_apps`.`tbl_karyawan` AS `c` ON `db_ptiik_apps`.`tbl_layanan`.`approve_by` = `c`.`karyawan_id`
					Left Join `db_ptiik_apps`.`tbl_unit_kerja` AS `b` ON `a`.`unit_id` = `b`.`unit_id`
					Left Join `db_ptiik_apps`.`tbl_unit_kerja` ON `db_ptiik_apps`.`tbl_layanan`.`unit_kerja_dituju` = `db_ptiik_apps`.`tbl_unit_kerja`.`unit_id`
					Left Join `db_ptiik_apps`.`tbl_layanan_status` ON `db_ptiik_apps`.`tbl_layanan`.`status_proses` = `db_ptiik_apps`.`tbl_layanan_status`.`status_proses`
					Left Join `db_ptiik_apps`.`tbl_layanan_file` ON `db_ptiik_apps`.`tbl_layanan`.`permintaan_id` = `db_ptiik_apps`.`tbl_layanan_file`.`permintaan_id` AND right(`db_ptiik_apps`.`tbl_layanan_file`.`file_id`,1) =  1
					WHERE 1=1 
					";
		if($id){
			$sql=$sql . " AND (mid(md5(`tbl_layanan`.`permintaan_id`),5,5)='".$id."' OR `tbl_layanan`.`permintaan_id`='".$id."')";
		}
			
		
		if($oleh){			
			if($unit){
				//$unitid = explode(",", $unit);
				
					
				$sql=$sql . " AND ((`db_ptiik_apps`.`tbl_layanan`.`unit_kerja_dituju` IN (SELECT unit_id FROM db_ptiik_apps.tbl_unit_karyawan WHERE karyawan_id='".$oleh."' )) OR `db_ptiik_apps`.`tbl_layanan`.`request_dari`='".$oleh."') ";
			}else{
				$sql=$sql . " AND `db_ptiik_apps`.`tbl_layanan`.`request_dari`='".$oleh."' ";
			}
		}else{
			if($unit){
				$sql=$sql . " AND `db_ptiik_apps`.`tbl_layanan`.`unit_kerja_dituju`='".$unit."' ";
			}
		}
		
		if($proses){
			$sql = $sql. " AND `db_ptiik_apps`.`tbl_layanan`.`status_proses` = '".$proses."' ";
		}
		
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`tbl_layanan`.`tgl_permintaan` DESC,`tbl_layanan`.`no_permintaan` DESC";
		
		$result = $this->db->query( $sql );
		
		
	
		return $result;	
	}
	
	function get_pelaksana($id=NULL){
		$sql = "SELECT
				`db_ptiik_apps`.`tbl_karyawan`.`nama`,
				`db_ptiik_apps`.`tbl_layanan_pelaksana`.`permintaan_id`,
				`db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id`,
				`db_ptiik_apps`.`tbl_layanan_pelaksana`.`karyawan_id`
				FROM
				`db_ptiik_apps`.`tbl_layanan_pelaksana`
				Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_layanan_pelaksana`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE 1 = 1
				";
		if($id){
			$sql=$sql . " AND (mid(md5(`tbl_layanan_pelaksana`.`permintaan_id`),5,5)='".$id."' OR `tbl_layanan_pelaksana`.`permintaan_id`='".$id."') ";
		}
		
		$sql = $sql . " ORDER BY `db_ptiik_apps`.`tbl_karyawan`.`nama` ASC";
		$result = $this->db->query( $sql );
	
		return $result;	
	}
	
	function get_karyawan($id=NULL, $unit=NULL, $jabatan=NULL){
		$sqla = "SELECT 
				`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`,
				`db_ptiik_apps`.`tbl_karyawan`.`nama`,
				`db_ptiik_apps`.`tbl_karyawan`.`email`,
				`db_ptiik_apps`.`tbl_karyawan`.`unit_id`,
				`db_ptiik_apps`.`tbl_jabatan`.`jabatan_id`,
				`db_ptiik_apps`.`tbl_jabatan`.`keterangan` as `jabatan`,
				`db_ptiik_apps`.`tbl_unit_kerja`.`nama` AS `unitkerja`
				FROM
				`db_ptiik_apps`.`tbl_karyawan`
				Left Join `db_ptiik_apps`.`tbl_jabatan` ON `db_ptiik_apps`.`tbl_karyawan`.`jabatan_id` = `db_ptiik_apps`.`tbl_jabatan`.`jabatan_id`
				Left Join `db_ptiik_apps`.`tbl_unit_kerja` ON `db_ptiik_apps`.`tbl_karyawan`.`unit_id` = `db_ptiik_apps`.`tbl_unit_kerja`.`unit_id`
				WHERE 1 = 1 
					";
		$sql = "SELECT
					db_ptiik_apps.tbl_karyawan.karyawan_id,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.email,
					db_ptiik_apps.tbl_karyawan.unit_id,
					db_ptiik_apps.tbl_jabatan.jabatan_id,
					db_ptiik_apps.tbl_jabatan.keterangan AS jabatan,
					db_ptiik_apps.tbl_karyawan.unit_id as `unitkerja`
					FROM
					db_ptiik_apps.tbl_karyawan
					LEFT JOIN db_ptiik_apps.tbl_jabatan ON db_ptiik_apps.tbl_karyawan.jabatan_id = db_ptiik_apps.tbl_jabatan.jabatan_id
					LEFT JOIN db_ptiik_apps.tbl_unit_karyawan ON db_ptiik_apps.tbl_karyawan.karyawan_id = db_ptiik_apps.tbl_unit_karyawan.karyawan_id
					INNER JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_apps.tbl_unit_karyawan.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
					WHERE 1 = 1 ";
		if(($id)&&(! $unit)){
			$sql = $sql . "AND `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` = '".$id."' ";
		}
		
		if($jabatan){
			//$sql = $sql . "AND `db_ptiik_apps`.`tbl_jabatan`.unit_id LIKE  '%".$jabatan."%' ";
			$sql=$sql . " AND ((db_ptiik_apps.tbl_unit_kerja.unit_id IN (SELECT unit_id FROM db_ptiik_apps.tbl_unit_karyawan WHERE karyawan_id='".$id."' )) 
				OR `db_ptiik_apps`.`tbl_unit_kerja`.`parent_id` IN (SELECT unit_id FROM db_ptiik_apps.tbl_unit_karyawan WHERE karyawan_id='".$id."' )) ";
		}
		
		if(($unit) && ($id)){
			//$sql = $sql . "AND (`db_ptiik_apps`.`tbl_karyawan`.`unit_id` IN ('".$unit."') OR `db_ptiik_apps`.`tbl_unit_kerja`.`parent_id` IN ('".$unit."'))";
			$sql=$sql . " AND ((db_ptiik_apps.tbl_unit_kerja.unit_id IN (SELECT unit_id FROM db_ptiik_apps.tbl_unit_karyawan WHERE karyawan_id='".$id."' )) 
				OR `db_ptiik_apps`.`tbl_unit_kerja`.`parent_id` IN (SELECT unit_id FROM db_ptiik_apps.tbl_unit_karyawan WHERE karyawan_id='".$id."' )) ";
		}
		
		if($unit){
			$result = $this->db->query( $sql );
		}else{
			$result = $this->db->getRow( $sql );
		}
		
		
		return $result;	
	}
	
	function get_permintaan_id($id=NULL){
		$sql = "SELECT `db_ptiik_apps`.`tbl_layanan_pelaksana`.`permintaan_id` FROM `db_ptiik_apps`.`tbl_layanan_pelaksana` 
				WHERE (mid(md5(`db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id`),5,5) = '".$id."' OR `db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id`='".$id."') ";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function read_progress($staff=NULL, $id=NULL){
		$sql = "SELECT
					mid(md5(`db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id`),5,5) as `id`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`karyawan_id`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`permintaan_id`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`is_koordinator`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`tgl_proses`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`is_proses`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`tgl_selesai`,
					`db_ptiik_apps`.`tbl_layanan_pelaksana`.`is_finish`,
					`db_ptiik_apps`.`vw_permintaan`.`judul`,
					`db_ptiik_apps`.`vw_permintaan`.`keterangan`,
					`db_ptiik_apps`.`vw_permintaan`.`no_permintaan`,
					`db_ptiik_apps`.`vw_permintaan`.`periode`,
					`db_ptiik_apps`.`vw_permintaan`.`tgl_permintaan`,
					`db_ptiik_apps`.`vw_permintaan`.`approve_by`,
					`db_ptiik_apps`.`vw_permintaan`.`tgl_proses` AS `tgl_approve`,
					`db_ptiik_apps`.`vw_permintaan`.`tgl_mulai_pengerjaan`,
					`db_ptiik_apps`.`vw_permintaan`.`tgl_deadline`,
					`db_ptiik_apps`.`vw_permintaan`.`request_dari`,
					`db_ptiik_apps`.`vw_permintaan`.`nama` AS `request_by`,
					`db_ptiik_apps`.`vw_permintaan`.`disetujui_oleh`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`,
					`db_ptiik_apps`.`tbl_karyawan`.`email`,
					`db_ptiik_apps`.`vw_permintaan`.`email_user`,
					`db_ptiik_apps`.`vw_permintaan`.`email_persetujuan`,
					`db_ptiik_apps`.`vw_permintaan`.`tgl_harapan_selesai`,
					`db_ptiik_apps`.`vw_permintaan`.`catatan`,
					`db_ptiik_apps`.`vw_permintaan`.`unit_peminta`,
					`db_ptiik_apps`.`vw_permintaan`.`nama_file`,
					`db_ptiik_apps`.`vw_permintaan`.`lokasi`,
					`db_ptiik_apps`.`vw_permintaan`.`is_jenis`,
					`db_ptiik_apps`.`vw_permintaan`.`nama_unit_dituju`,
					`db_ptiik_apps`.`vw_permintaan`.`st_proses`
					FROM
					`db_ptiik_apps`.`tbl_layanan_pelaksana`
					Inner Join `db_ptiik_apps`.`vw_permintaan` ON `db_ptiik_apps`.`tbl_layanan_pelaksana`.`permintaan_id` = `db_ptiik_apps`.`vw_permintaan`.`permintaan_id`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_layanan_pelaksana`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
					WHERE 1=1 
					";
		if($staff){
			$sql = $sql . "AND `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` = '".$staff."' ";
		}
		
		if($id){
			$sql = $sql . "AND (mid(md5(`db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id`),5,5) = '".$id."' OR  `db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id` = '".$id."')";
		}
		
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`vw_permintaan`.`tgl_permintaan` DESC,`vw_permintaan`.`no_permintaan` DESC";
		
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function read_pelaksana_progress($id=NULL){
		$sql = "SELECT DISTINCT
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`,
					`db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id`
					FROM
					`db_ptiik_apps`.`tbl_layanan_pelaksana`
					Inner Join `db_ptiik_apps`.`tbl_layanan_progress` ON `db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id` = `db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_layanan_pelaksana`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE 1 = 1 	
			    ";
		if($id){
			$sql = $sql . "AND (mid(md5(`db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id`),5,5) = '".$id."' OR  `db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id` = '".$id."')";
		}
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function read_detail_progress($id=NULL){
		$sql = "SELECT
				 mid(md5(`db_ptiik_apps`.`tbl_layanan_progress`.`progress_id`),5,5) as `id`,
				`db_ptiik_apps`.`tbl_karyawan`.`nama`,
				`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
				`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`progress_id`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`tgl_mulai`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`keterangan`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`tgl_target_selesai`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`tgl_selesai`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`judul`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`catatan`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`last_update`,
				`db_ptiik_apps`.`tbl_layanan_progress`.`inf_progress`
				FROM
				`db_ptiik_apps`.`tbl_layanan_pelaksana`
				Inner Join `db_ptiik_apps`.`tbl_layanan_progress` ON `db_ptiik_apps`.`tbl_layanan_pelaksana`.`pelaksana_id` = `db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id`
				Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_layanan_pelaksana`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE 1=1 
			";
		if($id){
			$sql = $sql . "AND (mid(md5(`db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id`),5,5) = '".$id."' OR  `db_ptiik_apps`.`tbl_layanan_progress`.`pelaksana_id` = '".$id."')";
		}
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function replace_progress($datanya){
		$result= $this->db->replace('db_ptiik_apps`.`tbl_layanan_progress', $datanya);
		
		return $result;
	}
	
	function update_proses_pelaksana($datanya, $idnya){
		$result= $this->db->update('db_ptiik_apps`.`tbl_layanan_pelaksana',$datanya, $idnya);
		//var_dump($datanya);
		return $result;
	}
	
	function replace_permintaan_layanan($datanya) {
		$result= $this->db->replace('db_ptiik_apps`.`tbl_layanan',$datanya);
		return $result;
	}
	
	function replace_pelaksana($datanya){
		$result= $this->db->replace('db_ptiik_apps`.`tbl_layanan_pelaksana',$datanya);
		
		return $result;
	}
	
	function replace_file($datanya){
		$result= $this->db->replace('db_ptiik_apps`.`tbl_layanan_file',$datanya);
	
		return $result;
	}
	
	function proses_permintaan($datanya, $idnya) {
		$result= $this->db->update('db_ptiik_apps`.`tbl_layanan',$datanya, $idnya);
		//var_dump($datanya);
		return $result;
	}
	
		
	
	function delete_komponen($datanya) {
		return $this->db->delete('db_purwandana`.`tbl_kategori_barang',$datanya);
	}
	
	function get_no_permintaan($str=NULL, $jenis=NULL){
		$sql="SELECT RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(no_permintaan,4) AS unsigned)), 0) + 1 AS unsigned)),4) as `data` 
				FROM db_ptiik_apps.tbl_layanan WHERE left(periode,4)='".$str."' AND is_jenis='".$jenis."'";		
		$result = $this->db->getRow( $sql );
		
		return $result->data;
	}
	
	function pelaksana($id=NULL){
				
		$data = $this->get_pelaksana($id);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		return $json_response;
	}
	
	
}

?>