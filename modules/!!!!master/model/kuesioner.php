<?php
class model_kuesioner extends model {
	private $coms;
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL,$all=NULL,$prodi=NULL){
		$sql = "SELECT tbl_kuesioner_mk.*,
					   tbl_kuesioner_detail.*,
					   `tbl_unit_kerja`.`fakultas_id`,
					   mid(md5(tbl_kuesioner_mk.kuesioner_id),6,6) as kuesionerid
				FROM `db_ptiik_apps`.`tbl_kuesioner_mk`
				LEFT JOIN `db_ptiik_apps`.`tbl_kuesioner_detail` 
					ON `tbl_kuesioner_detail`.`kuesioner_id` = `tbl_kuesioner_mk`.`kuesioner_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_unit_kerja` 
        			ON `tbl_unit_kerja`.`unit_id` = `tbl_kuesioner_detail`.`prodi_id`
				WHERE 1";
		if($id!=""){
			$sql .= " AND mid(md5(tbl_kuesioner_mk.kuesioner_id),6,6) = '".$id."'";
		}
		if($prodi!=""){
			$sql .= " AND tbl_kuesioner_detail.prodi_id = '".$prodi."'";
		}
		if($all!="all"){
			$sql .= " GROUP BY `tbl_kuesioner_mk`.`kuesioner_id`";
			if($id!=""){
				// echo $sql;
				$result = $this->db->getRow( $sql );
				return $result;
			}
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_mk($fakultas=NULL,$mkid=NULL){
		$sql = "SELECT `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` as `id`, 
			   		   CONCAT(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ', `db_ptiik_apps`.`tbl_namamk`.`keterangan`,' (',`db_ptiik_apps`.`tbl_matakuliah`.`sks`,' sks)') as `value`,
			   		   `db_ptiik_apps`.`tbl_namamk`.`keterangan` as nama_mk,
			   		   `db_ptiik_apps`.`tbl_matakuliah`.`sks`,
			   		   `db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`
			    FROM `db_ptiik_apps`.`tbl_matakuliah`
		        INNER JOIN `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id` 
			    WHERE 1";
		if($fakultas){
			$sql .= " AND tbl_namamk.fakultas_id = '".$fakultas."'";
		}
		if($mkid){
			$sql .= " AND tbl_matakuliah.matakuliah_id = '".$mkid."'";
			$result = $this->db->getRow( $sql );
			return $result;
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_tahun_akademik($tahun=NULL, $is_ganjil=NULL, $is_pendek=NULL){
		$sql = "SELECT *
				FROM `db_ptiik_apps`.`tbl_tahunakademik`
				WHERE 1";
		if($tahun){
			$sql .= " AND tbl_tahunakademik.tahun = '".$tahun."'";
		}
		if($is_ganjil){
			$sql .= " AND tbl_tahunakademik.is_ganjil = '".$is_ganjil."'";
		}
		if($is_pendek){
			$sql .= " AND tbl_tahunakademik.is_pendek = '".$is_pendek."'";
		}
		$result = $this->db->getRow( $sql );
		return $result;
	}
	
	function kuesioner_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kuesioner_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_kuesioner_mk`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function detail_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(detail_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_kuesioner_detail`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_kuesioner_mk($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kuesioner_mk', $datanya);
	}
	
	function replace_kuesioner_detail($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kuesioner_detail', $datanya);
	}
}
?>