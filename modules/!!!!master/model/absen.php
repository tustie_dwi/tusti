<?php
class model_absen extends model {
	private $coms;
	public function __construct() {
		parent::__construct();	
	}
	
	function get_mk_praktikum($mkid=NULL){
		$sql = "SELECT DISTINCT
				db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
				db_ptiik_apps.tbl_jadwalmk.is_praktikum
				FROM
				db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id
				WHERE db_ptiik_apps.tbl_jadwalmk.is_praktikum='1' AND db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id='$mkid'";
		
		return $this->db->getRow($sql);
	}
	
	function get_semester($aktif=NULL){
		$sql = "SELECT 
					tbl_tahunakademik.tahun, 
					tbl_tahunakademik.is_ganjil, 
					tbl_tahunakademik.is_pendek,
					tbl_tahunakademik.tahun_akademik
				FROM db_ptiik_apps.tbl_tahunakademik
				WHERE 1
				";
		if($aktif){
			$sql .= " AND tbl_tahunakademik.is_aktif = '1'";
			return $this->db->getRow($sql);
		}
		$sql .= " ORDER BY tbl_tahunakademik.tahun_akademik DESC";
		return $this->db->query($sql);
	}
	
	function get_prodi($id=NULL){
		$sql = "SELECT 
				tbl_unit_kerja.unit_id as prodi, 
				tbl_unit_kerja.keterangan
				FROM db_ptiik_apps.tbl_unit_kerja
				WHERE kategori='prodi' ";
		
		if($id){
			$sql .= " AND tbl_unit_kerja.unit_id = '$id' ";
			
			$dt = $this->db->getRow( $sql );			
			$strresult = $dt->keterangan;
			return $strresult;
		}else{
			return $this->db->query($sql);
		}
		
		// echo $sql;
		
	}
	
	function get_mk($tahun=NULL, $is_dosen=1, $id=NULL){
		if($is_dosen == 1){
			// $sqlx = "SELECT DISTINCT tbl_namamk.keterangan, tbl_jadwalmk.mkditawarkan_id, tbl_jadwalmk.kelas, tbl_jadwalmk.prodi_id
					// FROM db_ptiik_apps.tbl_absen
					// INNER JOIN db_ptiik_apps.tbl_absen_dosen ON tbl_absen_dosen.absen_id = tbl_absen.absen_id
					// INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
					// INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
					// INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
					// INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
					// WHERE tbl_mkditawarkan.tahun_akademik = '$tahun'
					// ORDER BY tbl_namamk.keterangan, tbl_jadwalmk.kelas";
			$sql= "SELECT DISTINCT tbl_namamk.keterangan, tbl_jadwalmk.mkditawarkan_id, tbl_jadwalmk.kelas, tbl_jadwalmk.prodi_id
					FROM
					db_ptiik_apps.tbl_jadwalmk
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_pengampu.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					WHERE tbl_mkditawarkan.tahun_akademik = '$tahun' AND tbl_pengampu.karyawan_id = '$id'
					ORDER BY tbl_namamk.keterangan, tbl_jadwalmk.kelas";
		}
		else{
			$sql = "SELECT DISTINCT tbl_namamk.keterangan, tbl_jadwalmk.mkditawarkan_id, tbl_jadwalmk.kelas, tbl_jadwalmk.prodi_id
					FROM db_ptiik_apps.tbl_jadwalmk
					INNER JOIN db_ptiik_apps.tbl_praktikum_asisten ON tbl_praktikum_asisten.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_praktikum_asisten.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
					WHERE tbl_jadwalmk.is_praktikum = 1 AND tbl_mkditawarkan.tahun_akademik = '$tahun' AND tbl_praktikum_asisten.mahasiswa_id = '$id'
					ORDER BY tbl_namamk.keterangan, tbl_jadwalmk.kelas";
			// $sql = "SELECT DISTINCT tbl_namamk.keterangan, tbl_jadwalmk.mkditawarkan_id, tbl_jadwalmk.kelas, tbl_jadwalmk.prodi_id
					// FROM db_ptiik_apps.tbl_absen
					// INNER JOIN db_ptiik_apps.tbl_absenmhs ON tbl_absenmhs.absen_id = tbl_absen.absen_id
					// INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
					// INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
					// INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
					// INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
					// WHERE tbl_mkditawarkan.tahun_akademik = '$tahun'
					// ORDER BY tbl_namamk.keterangan, tbl_jadwalmk.kelas";
		}
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_nama($kelas=NULL, $prodi=NULL,$mk=NULL, $tahun=NULL, $is_dosen=1){
		if($is_dosen == 1){
			// $sqlx = "SELECT DISTINCT tbl_karyawan.nama, tbl_karyawan.nik, tbl_pengampu.karyawan_id, tbl_absen_dosen.pengampu_id
					// FROM db_ptiik_apps.tbl_absen
					// INNER JOIN db_ptiik_apps.tbl_absen_dosen ON tbl_absen_dosen.absen_id = tbl_absen.absen_id
					// INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
					// INNER JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_absen_dosen.pengampu_id
					// INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
					// INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_pengampu.mkditawarkan_id
					// WHERE 1";
			
			$sql = "SELECT DISTINCT tbl_karyawan.nama, tbl_karyawan.nik, tbl_pengampu.karyawan_id, tbl_absen_dosen.pengampu_id
					FROM db_ptiik_apps.tbl_absen
					INNER JOIN db_ptiik_apps.tbl_absen_dosen ON tbl_absen_dosen.absen_id = tbl_absen.absen_id
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
					INNER JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_absen_dosen.pengampu_id
					INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_pengampu.mkditawarkan_id
					WHERE 1";
			if($tahun!=""){
				$sql .= " AND tbl_mkditawarkan.tahun_akademik = '$tahun' ";
			}
			
			if($mk!=""){
				$sql .= " AND tbl_mkditawarkan.mkditawarkan_id = '$mk' ";
			}
			
			if($kelas!=""){
				$sql .= " AND tbl_jadwalmk.kelas='$kelas' ";
			}
			
			if($prodi!=""){
				$sql .= " AND tbl_jadwalmk.prodi_id='$prodi' ";
			}
			
			if(!$tahun && !$mk && !$kelas && !$prodi){
				$sql .= " GROUP BY tbl_karyawan.nama ";
			}
			
			$sql .= " ORDER BY tbl_karyawan.nama ";
			
		}
		else{
			$sql = "SELECT DISTINCT tbl_mahasiswa.nama, tbl_mahasiswa.nim, tbl_mahasiswa.mahasiswa_id
					FROM db_ptiik_apps.tbl_praktikum_asisten
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_praktikum_asisten.mahasiswa_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_praktikum_asisten.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
					WHERE tbl_jadwalmk.is_praktikum = 1 AND tbl_mahasiswa.is_aktif = 'aktif'";
			if($tahun!=""){
				$sql .= " AND tbl_mkditawarkan.tahun_akademik = '$tahun' ";
			}
			
			if($mk!=""){
				$sql .= " AND tbl_mkditawarkan.mkditawarkan_id = '$mk' ";
			}
			
			if($kelas!=""){
				$sql .= " AND tbl_jadwalmk.kelas='$kelas' ";
			}
			
			if($prodi!=""){
				$sql .= " AND tbl_jadwalmk.prodi_id='$prodi' ";
			}
			
			if(!$tahun && !$mk && !$kelas && !$prodi){
				$sql .= " GROUP BY tbl_mahasiswa.nama ";
			}
			
			$sql .= " ORDER BY tbl_mahasiswa.nama ";
		}	
		
		return $this->db->query($sql);
	}

	function get_bayar($id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_keu_bayar.bayar_id,
					db_ptiik_apps.tbl_keu_bayar_mengajar.absendosen_id,
					db_ptiik_apps.tbl_keu_bayar.last_update
					FROM
					db_ptiik_apps.tbl_keu_bayar_mengajar
					INNER JOIN db_ptiik_apps.tbl_keu_bayar ON db_ptiik_apps.tbl_keu_bayar_mengajar.bayar_id = db_ptiik_apps.tbl_keu_bayar.bayar_id WHERE absendosen_id='$id' ";
		return $this->db->getRow($sql);
	}
	
	function rekap_absen($thnid=NULL, $prodiid=NULL, $mk=NULL, $kelas=NULL, $dosen=NULL, $datemulai=NULL, $dateselesai=NULL, $bymk=NULL, $jadwal=NULL, $absen=NULL){
		$sql = "SELECT
				tbl_karyawan.nama,
		        tbl_karyawan.karyawan_id,
		        tbl_karyawan.nik,
		        tbl_karyawan.golongan,
		        tbl_absen.absen_id as hidId,
		        tbl_absen.total_pertemuan total,
		        tbl_absen.tgl as tgl_pertemuan,
		        tbl_absen.materi,
		        tbl_absen_dosen.user,
			    tbl_absen_dosen.last_update,
			    tbl_absen_dosen.absendosen_id,
		        tbl_absen.sesi_ke as sesi,
		        tbl_absen_dosen.is_hadir,
		        tbl_absen_dosen.absendosen_id,
		        tbl_absen_dosen.pengampu_id, 
		        tbl_namamk.keterangan namamk,
				tbl_jadwalmk.kelas kelas,
				tbl_matakuliah.kode_mk,
				tbl_matakuliah.sks,";
		if(!$jadwal){
			$sql .= "COUNT(tbl_absen_dosen.absendosen_id) jml,";
		}
		$sql .= "tbl_absen.jam_masuk,
		        tbl_absen.jam_selesai,
		        tbl_jadwalmk.hari,
		        tbl_jadwalmk.ruang_id,
		        tbl_jadwalmk.prodi_id,
		        tbl_jadwalmk.jadwal_id,
		        tbl_jadwalmk.mkditawarkan_id,
		        tbl_mkditawarkan.tahun_akademik,
		        tbl_unit_kerja.kode as prodi_mk,
		        tbl_unit_kerja.keterangan as prodi_keterangan
		        
				FROM db_ptiik_apps.tbl_absen 
				LEFT JOIN db_ptiik_apps.tbl_absen_dosen ON tbl_absen_dosen.absen_id = tbl_absen.absen_id 
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_absen_dosen.pengampu_id 
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
				LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_jadwalmk.prodi_id
				WHERE 1";
		if($absen){
			$sql .= " AND tbl_absen.absen_id = '".$absen."' ";
			// echo $sql;
			return $this->db->getRow($sql);
		}
		
		if($thnid){
			$sql .= " AND tbl_mkditawarkan.tahun_akademik = '".$thnid."' ";
		}
		
		if($prodiid){
			$sql .= " AND tbl_jadwalmk.prodi_id='".$prodiid."' ";
		}
		
		if($mk){
			$sql .= " AND tbl_mkditawarkan.mkditawarkan_id = '".$mk."' ";
		}
		
		if($kelas){
			$sql .= " AND tbl_jadwalmk.kelas = '".$kelas."' ";
		}
		
		if($dosen){
			$sql .= " AND tbl_karyawan.karyawan_id = '".$dosen."' ";
		}
		
		if(isset($datemulai) && isset($dateselesai)){
			$sql .= " AND tbl_absen.tgl BETWEEN '".$datemulai."' AND '".$dateselesai."'";
		}
		
		// if($jadwal){
			// $sql .= " AND tbl_absen.jadwal_id = '".$jadwal."' ";
		// }
		if($bymk=='bymk'){
			$sql .= " AND tbl_absen_dosen.is_hadir = '1'
					  GROUP BY tbl_karyawan.nama, tbl_jadwalmk.mkditawarkan_id, tbl_jadwalmk.kelas, tbl_jadwalmk.prodi_id
					  ORDER BY tbl_karyawan.nama, tbl_jadwalmk.prodi_id, tbl_namamk.keterangan, tbl_jadwalmk.kelas ";
		}else if($bymk=='byjadwal'){
			$sql .= " AND tbl_absen_dosen.is_hadir = '1'";
		}else{
			$sql .= " AND tbl_absen_dosen.is_hadir = '1' ";
			$sql .= " GROUP BY tbl_karyawan.nama ORDER BY tbl_karyawan.nama ";
		}
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function rekap_absen_asisten($thnid=NULL, $prodiid=NULL, $mk=NULL, $kelas=NULL, $mhs=NULL, $datemulai=NULL, $dateselesai=NULL,$jadwal=NULL, $absen=NULL){
		$sql = "SELECT tbl_mahasiswa.nama,
						tbl_mahasiswa.nim,
						tbl_mahasiswa.mahasiswa_id,
						tbl_praktikum_asisten_absen.user_id as user,
						tbl_praktikum_asisten_absen.is_hadir,
						tbl_namamk.keterangan namamk,
						tbl_jadwalmk.kelas kelas,
						tbl_matakuliah.kode_mk,
						tbl_matakuliah.sks,
						tbl_absen.absen_id,
						tbl_absen.sesi_ke as sesi,
						tbl_absen.total_pertemuan as total,
						tbl_absen.tgl AS tgl_pertemuan,
						tbl_absen.jam_masuk AS jam_masuk,
						tbl_absen.jam_selesai AS jam_selesai,
						tbl_absen.user AS USER,
						tbl_absen.last_update,
						tbl_absen.materi,
						tbl_praktikum_asisten.asisten_id,
						tbl_praktikum_asisten_absen.jadwal_asisten_id,";
		   if(!$jadwal){
				$sql .= "COUNT(tbl_praktikum_asisten_absen.jadwal_asisten_id) jml,";
		   }
				$sql .= "tbl_jadwalmk.hari,
						tbl_jadwalmk.ruang_id,
						tbl_jadwalmk.prodi_id,
						tbl_jadwalmk.jadwal_id,
						tbl_jadwalmk.mkditawarkan_id,
						tbl_mkditawarkan.tahun_akademik,
						tbl_unit_kerja.kode AS prodi_mk,
						tbl_unit_kerja.keterangan AS prodi_keterangan,
						tbl_praktikum_kelompok.kelompok_id,
						tbl_praktikum_kelompok.nama as kelompok
				FROM db_ptiik_apps.tbl_absen
				LEFT JOIN db_ptiik_apps.tbl_praktikum_asisten_absen ON tbl_praktikum_asisten_absen.absen_id = tbl_absen.absen_id
				LEFT JOIN db_ptiik_apps.tbl_praktikum_asisten ON tbl_praktikum_asisten.asisten_id = tbl_praktikum_asisten_absen.asisten_id
				LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_praktikum_asisten.mahasiswa_id
				LEFT JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
				LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_jadwalmk.prodi_id
				LEFT JOIN db_ptiik_apps.tbl_praktikum_kelompok ON tbl_praktikum_kelompok.kelompok_id = tbl_absen.kelompok
				WHERE 1";
		if($absen){
			$sql .= " AND tbl_absen.absen_id = '".$absen."' ";
			// echo $sql;
			return $this->db->getRow($sql);
		}
		
		if($thnid!=""){
			$sql .= " AND tbl_mkditawarkan.tahun_akademik = '$thnid' ";
		}
		
		if($mk!=""){
			$sql .= " AND tbl_mkditawarkan.mkditawarkan_id = '$mk' ";
		}
		
		if($kelas!=""){
			$sql .= " AND tbl_jadwalmk.kelas='$kelas' ";
		}
		
		if($prodiid!=""){
			$sql .= " AND tbl_jadwalmk.prodi_id='$prodiid' ";
		}
		
		if($mhs){
			$sql .= " AND tbl_mahasiswa.mahasiswa_id = '".$mhs."' ";
		}
		
		if(isset($datemulai) && isset($dateselesai)){
			$sql .= " AND tbl_absen.tgl BETWEEN '".$datemulai."' AND '".$dateselesai."'";
		}
		
		if(!$jadwal){
			$sql .= " AND tbl_praktikum_asisten_absen.is_hadir = 1
					  GROUP BY tbl_mahasiswa.nama, tbl_jadwalmk.mkditawarkan_id, tbl_jadwalmk.kelas, tbl_jadwalmk.prodi_id
					  ORDER BY tbl_mahasiswa.nama, tbl_jadwalmk.prodi_id, tbl_namamk.keterangan, tbl_jadwalmk.kelas ";
	    }
		else{
			$sql .= " AND tbl_praktikum_asisten_absen.is_hadir = 1";
		}
		// echo $sql;
		return $this->db->query($sql);
	}

	function get_jadwal($thn_akademik=NULL,$dosen=NULL,$jadwal=NULL){
		$sql = "SELECT tbl_jadwalmk.jadwal_id,
					   tbl_jadwalmk.kelas,
					   tbl_jadwalmk.jam_mulai,
					   tbl_jadwalmk.jam_selesai,
					   tbl_matakuliah.kode_mk,
					   tbl_namamk.keterangan as nama_mk,
					   tbl_unit_kerja.kode as prodi,
					   tbl_mkditawarkan.mkditawarkan_id as mkid
				FROM db_ptiik_apps.tbl_jadwalmk
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_jadwalmk.prodi_id
				WHERE 1
				AND tbl_jadwalmk.is_praktikum = '0'
			   ";
		if($thn_akademik){
			$sql .= " AND tbl_mkditawarkan.tahun_akademik = '".$thn_akademik."'";
		}
		if($dosen){
			$sql .= " AND tbl_pengampu.karyawan_id = '".$dosen."'";
		}
		
		if($jadwal){
			$sql .= " AND tbl_jadwalmk.jadwal_id = '".$jadwal."'";
			return $this->db->getRow($sql);
		}
		$sql .= " GROUP BY tbl_jadwalmk.jadwal_id
				  ORDER BY mkid, prodi, kelas";
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_jadwal_asisten($thn_akademik=NULL,$asisten=NULL,$jadwal=NULL){
		$sql = "SELECT tbl_jadwalmk.jadwal_id,
					   tbl_jadwalmk.kelas,
					   tbl_jadwalmk.jam_mulai,
					   tbl_jadwalmk.jam_selesai,
					   tbl_matakuliah.kode_mk,
					   tbl_namamk.keterangan as nama_mk,
					   tbl_unit_kerja.kode as prodi,
					   tbl_mkditawarkan.mkditawarkan_id as mkid
				FROM db_ptiik_apps.tbl_jadwalmk
				LEFT JOIN db_ptiik_apps.tbl_absen ON tbl_absen.jadwal_id = tbl_jadwalmk.jadwal_id
				LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_praktikum_asisten ON tbl_praktikum_asisten.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_jadwalmk.prodi_id
				WHERE 1
				AND tbl_jadwalmk.is_praktikum = '1'
			   ";
		if($thn_akademik){
			$sql .= " AND tbl_mkditawarkan.tahun_akademik = '".$thn_akademik."'";
		}
		if($asisten){
			$sql .= " AND tbl_praktikum_asisten.asisten_id = '".$asisten."'";
		}
		
		if($jadwal){
			$sql .= " AND tbl_jadwalmk.jadwal_id = '".$jadwal."'";
			return $this->db->getRow($sql);
		}
		$sql .= " GROUP BY tbl_jadwalmk.jadwal_id
				  ORDER BY mkid, prodi, kelas";
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function absen_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(absen_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_absen`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function absen_dosenid(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(absendosen_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_absen_dosen`
				where left(absendosen_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function absen_asistenid(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jadwal_asisten_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_praktikum_asisten_absen`
				where left(jadwal_asisten_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_absen($datanya=NULL){
		return $this->db->replace('db_ptiik_apps`.`tbl_absen', $datanya);
	}
	
	function replace_absen_dosen($datanya=NULL){
		return $this->db->replace('db_ptiik_apps`.`tbl_absen_dosen', $datanya);
	}
	
	function replace_absen_asisten($datanya=NULL){
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum_asisten_absen', $datanya);
	}
	
	function delete_absen($id=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_absen_dosen WHERE tbl_absen_dosen.absen_id='".$id."'";
		$sql1 = "DELETE FROM db_ptiik_apps.tbl_absen WHERE tbl_absen.absen_id='".$id."'"; 
		$del_absen = $this->db->query($sql);
		if($del_absen){
			$this->db->query($sql1);
			return TRUE;
		}
	}

	function delete_absen_asisten($id=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_praktikum_asisten_absen WHERE tbl_praktikum_asisten_absen.absen_id='".$id."'";
		$sql1 = "DELETE FROM db_ptiik_apps.tbl_absen WHERE tbl_absen.absen_id='".$id."'"; 
		$del_absen = $this->db->query($sql);
		if($del_absen){
			$this->db->query($sql1);
			return TRUE;
		}
	}
	
	function kelompok($id=NULL){
		$sql = "SELECT
					tbl_praktikum_kelompok.kelompok_id,
					tbl_praktikum_kelompok.nama
				FROM
				db_ptiik_apps.tbl_praktikum_kelompok
				WHERE 1";
		if($id){
			$sql .= " AND tbl_praktikum_kelompok.kelompok_id = '$id'";
		}
		return $this->db->query($sql);
	}
}
?>