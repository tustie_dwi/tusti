<?php
class model_pengampu extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id)
	{
		$sql = "SELECT mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`),6,6) as pengampu_id,
				`db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` as hid_id,
				mkditawarkan_id,
				karyawan_id,				
				(SELECT k.nama FROM `db_ptiik_apps`.`tbl_karyawan` as k 
				WHERE k.karyawan_id = `db_ptiik_apps`.`tbl_pengampu`.karyawan_id) as nama,				
				(SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah`, 
				`db_ptiik_apps`.`tbl_mkditawarkan` 
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id 
				AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id) as namamk,
				is_koordinator				
				FROM `db_ptiik_apps`.`tbl_pengampu` WHERE 1";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`),6,6)='".$id."' ";
		}
		$result = $this->db->query( $sql );	
		return $result;	
	}
	
	function get_namamkpengampu(){
		$sql = "SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan as keterangan
			  	FROM `db_ptiik_apps`.`tbl_namamk`, 
			  		 `db_ptiik_apps`.`tbl_matakuliah`, 
			  		 `db_ptiik_apps`.`tbl_mkditawarkan`, 
			  		 `db_ptiik_apps`.`tbl_pengampu`
			  	WHERE `db_ptiik_apps`.`tbl_namamk`. namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id
			 	AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id
			  	AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id
			  	GROUP BY `db_ptiik_apps`.`tbl_namamk`.keterangan"; 
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_reg_number($mkid=NULL, $staff=NULL){
	 	$sql = "SELECT pengampu_id FROM db_ptiik_apps.tbl_pengampu WHERE mkditawarkan_id ='".$mkid."' AND karyawan_id='".$staff."'  ";
	 	$result = $this->db->getRow( $sql );	
 		if($result){			
			$strresult = $result->pengampu_id;
		}else{
			$kode = $mkid;
			$sql = "SELECT concat('".$kode."',RIGHT(concat( '00' , CAST(IFNULL(MAX(CAST(right(pengampu_id,2) AS unsigned)), 0) + 1 AS unsigned)),2)) as `data` 
					FROM db_ptiik_apps.tbl_pengampu WHERE mkditawarkan_id ='".$mkid."'";
			// $dt = $this->db->getRow( $sql );				
			// $strresult = $dt->data;
			
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		 }
		 
		 // echo $sql;
		return $strresult;
	}
	
	function get_pengampu($term){
		$sql = "SELECT k.nama, k.karyawan_id  
				FROM `db_ptiik_apps`.`tbl_karyawan` as k
				WHERE mid(md5(k.`karyawan_id`),6,6) = '".$term."'"; 
		$result = $this->db->query( $sql );
		if(isset($result)){
			return $result;
		}	
	}
	
	function get_mk_pengampu($term){
		$sql = "SELECT 
				(SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan 
				FROM `db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah` 
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id ) as namamk
				FROM `db_ptiik_apps`.`tbl_pengampu`, `db_ptiik_apps`.`tbl_mkditawarkan`
				WHERE mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`),6,6) = '".$term."'
				AND `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id"; 
		$result = $this->db->query( $sql );
		if(isset($result)){
			return $result;
		}	
	}
	
	function get_dosen(){
		$sql = "SELECT 
				`db_ptiik_apps`.`tbl_karyawan`.nama, 
				`db_ptiik_apps`.`tbl_pengampu`.karyawan_id, 
				mid(md5(`db_ptiik_apps`.`tbl_pengampu`.karyawan_id),6,6) as hid_karyawan
				FROM `db_ptiik_apps`.`tbl_pengampu`, `db_ptiik_apps`.`tbl_karyawan`
				WHERE `db_ptiik_apps`.`tbl_pengampu`.karyawan_id = `db_ptiik_apps`.`tbl_karyawan`.karyawan_id
				GROUP BY `db_ptiik_apps`.`tbl_karyawan`.nama"; 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_matakuliah(){
		$sql = "SELECT
					(SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan 
					FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah` 
					WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
					AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk,
					(SELECT `db_ptiik_apps`.`tbl_fakultas`.keterangan 
					FROM `db_ptiik_apps`.`tbl_fakultas`, `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah` 
					WHERE `db_ptiik_apps`.`tbl_fakultas`.fakultas_id = `db_ptiik_apps`.`tbl_namamk`.fakultas_id 
					AND `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
					AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as fakultas,
               	`db_ptiik_apps`.`tbl_pengampu`.karyawan_id,
				`db_ptiik_apps`.`tbl_pengampu`.is_koordinator,
                mid(md5(`db_ptiik_apps`.`tbl_pengampu`.pengampu_id),6,6) as pengampu_id,
                (SELECT keterangan FROM `db_ptiik_apps`.`tbl_cabang` WHERE `db_ptiik_apps`.`tbl_cabang`.`cabang_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`cabang_id`) as cabang
				FROM `db_ptiik_apps`.`tbl_mkditawarkan`, `db_ptiik_apps`.`tbl_pengampu`, `db_ptiik_apps`.`tbl_karyawan`	
				WHERE `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id
				AND `db_ptiik_apps`.`tbl_pengampu`.karyawan_id = `db_ptiik_apps`.`tbl_karyawan`.karyawan_id"; 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_mkditawarkan($term, $cabang=NULL)
	{
		
		$sql = "SELECT `tbl_mkditawarkan`.`mkditawarkan_id`
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
				LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` ON `tbl_matakuliah`.`matakuliah_id`=`tbl_mkditawarkan`.`matakuliah_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON `tbl_matakuliah`.`namamk_id` = `tbl_namamk`.`namamk_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` ON `tbl_tahunakademik`.`tahun_akademik` = `tbl_mkditawarkan`.`tahun_akademik`
				WHERE `tbl_namamk`.`keterangan` = '".$term."'
				AND `tbl_mkditawarkan`.`cabang_id` = '".$cabang."'
				AND `tbl_tahunakademik`.`is_aktif` = 1
			   ";

		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->mkditawarkan_id;
			return $strresult;
		}
	}
	
	function cek_koor($term)
	{
		$sql = "SELECT `tbl_karyawan`.`nama` as nama 
				FROM `db_ptiik_apps`.`tbl_pengampu` 
				LEFT JOIN `db_ptiik_apps`.`tbl_karyawan` ON `tbl_karyawan`.`karyawan_id` = `tbl_pengampu`.`karyawan_id`
				WHERE `tbl_pengampu`.`is_koordinator` = '1'
				AND `tbl_pengampu`.`mkditawarkan_id` = '".$term."'
			   ";

		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->nama;
			return $strresult;
		}
	}
	
	function replace_pengampu($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_pengampu',$datanya);
	}
	
	function update_koor($pengampuid, $iskoor){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_pengampu`
				SET `is_koordinator`= '".$iskoor."'
				WHERE `pengampu_id`= '".$pengampuid."'
			   ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_pengampu($id)
	{
		$sql="DELETE FROM `db_ptiik_apps`.`tbl_pengampu` 
			  WHERE `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` = '".$id."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;
		}
	}
	
}
?>