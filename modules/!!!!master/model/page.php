<?php

class model_page extends model {
	public function __construct() {
		parent::__construct();
	}
	function get_peserta_by_group($id=NULL, $jenis=NULL, $status=NULL){
		$sql = "SELECT DISTINCT group_by 
				FROM
				 `db_ptiik_apps`.`tbl_agenda_peserta`
				WHERE 
					 group_by<> 'nama' AND agenda_id = '".$id."' AND  sebagai='".$jenis."' 
				";
		if($status){
			$sql = $sql . "AND `tbl_agenda_peserta`.`jenis_peserta`='".$status."' ";
		}			
		
		return $this->db->query( $sql );
	}
	
	
	function get_peserta_agenda($id=NULL, $jenis=NULL, $status=NULL, $grup=NULL){
		$sql = "SELECT
				tbl_agenda_peserta.`peserta_id`,
				tbl_agenda_peserta.`karyawan_id`,
				tbl_agenda_peserta.`mahasiswa_id`,
				tbl_agenda_peserta.`agenda_id`,
				tbl_agenda_peserta.`jenis_peserta`,
				tbl_agenda_peserta.`nama`,
				tbl_agenda_peserta.`instansi`,
				tbl_agenda_peserta.`sebagai`
				FROM
				 `db_ptiik_apps`.tbl_agenda_peserta
				WHERE 
					1= 1 AND (mid(md5(tbl_agenda_peserta.agenda_id),5,5)= '".$id."' OR `db_ptiik_apps`.tbl_agenda_peserta.agenda_id = '".$id."') AND  sebagai='".$jenis."' 
				";
		if($status){
			$sql = $sql . "AND tbl_agenda_peserta.`jenis_peserta`='".$status."' ";
		}
		
		if($grup){
			$sql = $sql . "AND tbl_agenda_peserta.`group_by`='".$grup."' ";
		}
		
		$sql = $sql. "ORDER BY tbl_agenda_peserta.`nama` ASC";
		
		return $this->db->query( $sql );
	}
	
	function read_event($unit=NULL, $lang=NULL, $str=NULL, $id=NULL, $limit = NULL){
		$sql = "SELECT
					mid(md5(db_ptiik_apps.tbl_agenda.agenda_id),9,7) as `id`,
					db_ptiik_apps.tbl_agenda.agenda_id,
					db_ptiik_apps.tbl_agenda.unit_id,
					db_ptiik_apps.tbl_agenda.jenis_kegiatan_id,
					db_ptiik_apps.tbl_agenda.judul as judul_ori, db_ptiik_apps.tbl_agenda.keterangan as note_ori,
					db_ptiik_apps.tbl_agenda.lokasi,";
					
		if($lang=='IN') $sql .= "db_ptiik_apps.tbl_agenda.judul as judul, db_ptiik_apps.tbl_agenda.keterangan,db_ptiik_apps.tbl_unit_kerja.keterangan AS unit," ;
		else $sql .= "db_ptiik_apps.tbl_agenda.english_version as judul, db_ptiik_apps.tbl_agenda.note_english as keterangan, db_ptiik_apps.tbl_unit_kerja.english_version AS unit," ;
		
		$sql .= "db_ptiik_apps.tbl_agenda.penyelenggara,
					db_ptiik_apps.tbl_agenda.tgl_mulai,
					db_ptiik_apps.tbl_agenda.tgl_selesai,
					db_ptiik_apps.tbl_agenda.is_allday,
					db_ptiik_apps.tbl_agenda.page_link,
					db_ptiik_apps.tbl_agenda.inf_peserta,
					db_ptiik_apps.tbl_agenda.inf_hari,
					db_ptiik_apps.tbl_agenda.icon_thumb,
					db_ptiik_apps.tbl_agenda.jenis_agenda,
					db_ptiik_apps.tbl_agenda.status_agenda,
					db_ptiik_apps.tbl_agenda.inf_ruang,
					db_ptiik_apps.tbl_agenda.is_delete,
					db_ptiik_apps.tbl_agenda.group_peserta,
					db_ptiik_apps.tbl_agenda.inf_undangan,
					db_ptiik_apps.tbl_agenda.user_id,
					db_ptiik_apps.tbl_agenda.last_update,
					db_ptiik_apps.tbl_agenda.last_update as content_modified,
					db_ptiik_apps.tbl_unit_kerja.keterangan AS unit_ori,
					db_ptiik_apps.tbl_jeniskegiatan.keterangan AS kegiatan
					FROM
					db_ptiik_apps.tbl_agenda
					INNER JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_apps.tbl_agenda.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
					INNER JOIN db_ptiik_apps.tbl_jeniskegiatan ON db_ptiik_apps.tbl_agenda.jenis_kegiatan_id = db_ptiik_apps.tbl_jeniskegiatan.jenis_kegiatan_id WHERE 1 = 1 ";
		if($id){
			$sql = $sql. " AND (tbl_agenda.page_link = '".$id."' OR mid(md5(db_ptiik_apps.tbl_agenda.agenda_id),9,7) = '".$id."') ";
		}
		
		/*if($str){			
			if($str=='main'){
				$limit = "0,4";	
			}else{
				$limit = "0,2";	
			}		
		}else{
			$limit = "0, 100";
		}*/
		
		if($unit && $unit!=0) $sql.= " AND db_ptiik_apps.tbl_agenda.unit_id ='$unit' ";
		
		$sql = $sql. " ORDER BY tbl_agenda.tgl_mulai DESC LIMIT 0, ".$limit;
		
		
		if($id) $result = $this->db->getRow( $sql );
		else $result = $this->db->query( $sql );
				
		return $result;

	}
	
	function read_comment($content=NULL, $parent=NULL){
		$sql = "SELECT
					content_comments.id,
					content_comments.content_id,
					content_comments.comment,
					content_comments.user_name,
					content_comments.email,
					content_comments.parent_id,
					content_comments.last_update,
					content_comments.approved,
					content_comments.ip_address,
					contents.content_page,
					contents.content_title,
					coms_user.foto,
					coms_user.username,
					coms_user.name,
					content_comments.tgl_comment,
					content_comments.user_id
				FROM
					db_ptiik_coms.content_comments
					INNER JOIN db_ptiik_coms.contents ON db_ptiik_coms.content_comments.content_id = db_ptiik_coms.coms_content.content_id
					LEFT JOIN db_ptiik_coms.coms_user ON db_ptiik_coms.content_comments.email = db_ptiik_coms.coms_user.email
				WHERE 1 ";
		if($parent!=""){
			$sql = $sql. "AND content_comments.approved='1'  
					 AND content_comments.parent_id = '".$parent."' ";
		}
		if($content!=""){
			$sql = $sql. " AND content_comments.content_id = '".$content."' ";
		}
		$sql = $sql. " ORDER BY approved ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;

	}
	
	function cek_last_comment($user_id,$id){
		$sql = "SELECT content_comments.approved as approved
				FROM db_ptiik_coms.content_comments
				WHERE content_comments.content_id = '".$id."'
				AND content_comments.user_id = '".$user_id."'
				ORDER BY content_comments.tgl_comment DESC
				LIMIT 1";
		$result = $this->db->getRow( $sql );
		if(isset($result)){
			$appr = $result->approved;
			return $appr;
		}
		else {
			return FALSE;	
		}
		//echo $sql;
	}
	
	function delete_comments($id=NULL){
		$sql = "DELETE FROM db_ptiik_coms.content_comments
				WHERE content_comments.id = '".$id."'";
		$result = $this->db->query( $sql );
		if($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	function update_status_comments($id=NULL,$status=NULL,$lastupdate=NULL){
		$sql = "UPDATE db_ptiik_coms.content_comments
				SET content_comments.approved = '".$status."', content_comments.last_update = '".$lastupdate."'
				WHERE content_comments.id = '".$id."'";
		$result = $this->db->query( $sql );
		if($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	
	function read_slide($unit=NULL, $lang=NULL, $limit=NULL){
		
		$sql= "SELECT
					coms_file.file_id,
					coms_file.content_category,
					coms_file.content_data,
					coms_file.fakultas_id,
					coms_file.unit_id,
					coms_file.cabang_id,
					coms_file.file_lang,
					coms_file.file_title,
					coms_file.file_note,
					coms_file.file_data,
					coms_file.file_group,
					coms_file.file_type,
					coms_file.file_name,
					coms_file.file_size,
					coms_file.file_loc,
					coms_file.is_publish,
					coms_file.user_id,
					coms_file.last_update
					FROM
					db_ptiik_coms.coms_file WHERE is_publish='1' AND coms_file.file_lang = '$lang' AND coms_file.unit_id = '$unit' ORDER BY last_update DESC LIMIT $limit";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function read_course($unit=NULL, $lang=NULL,$str=NULL, $id=NULL){
		
		if($id):
			$sql= "SELECT
					mid(md5(db_ptiik_apps.tbl_silabus.mkditawarkan_id),9,7) as `id`,
					db_ptiik_apps.tbl_namamk.keterangan AS judul,
					db_ptiik_apps.tbl_silabus_komponen.keterangan AS komponen,
					db_ptiik_apps.tbl_silabus_detail.keterangan
					FROM
					db_ptiik_apps.tbl_mkditawarkan
					INNER JOIN db_ptiik_apps.tbl_silabus ON db_ptiik_apps.tbl_silabus.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					INNER JOIN db_ptiik_apps.tbl_silabus_detail ON db_ptiik_apps.tbl_silabus.silabus_id = db_ptiik_apps.tbl_silabus_detail.silabus_id
					INNER JOIN db_ptiik_apps.tbl_silabus_komponen ON db_ptiik_apps.tbl_silabus_detail.komponen_id = db_ptiik_apps.tbl_silabus_komponen.komponen_id
				WHERE 1 = 1 ";
		else:
			$sql = "SELECT DISTINCT
					mid(md5(tbl_mkditawarkan.mkditawarkan_id),9,7) AS id,
					db_ptiik_apps.tbl_namamk.keterangan AS namamk,
					db_ptiik_apps.tbl_fakultas.keterangan AS fakultas,
					db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id,
					db_ptiik_apps.tbl_namamk.keterangan as judul,
					db_ptiik_apps.tbl_fakultas.keterangan AS keterangan,
					db_ptiik_apps.tbl_mkditawarkan.icon AS icon_mk
					FROM
						db_ptiik_apps.tbl_mkditawarkan
						INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
						INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
						INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
						INNER JOIN db_ptiik_apps.tbl_fakultas ON db_ptiik_apps.tbl_namamk.fakultas_id = db_ptiik_apps.tbl_fakultas.fakultas_id	
							INNER JOIN db_ptiik_apps.tbl_silabus ON db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = db_ptiik_apps.tbl_silabus.mkditawarkan_id						
					WHERE
						db_ptiik_apps.tbl_tahunakademik.is_aktif = 1 
					";
		endif;
		
		if($id){
			$sql = $sql. "AND mid(md5(db_ptiik_apps.tbl_silabus.mkditawarkan_id),9,7) = '".$id."' ";
		}
		
		if($str){			
			if($str=='main'){
				$limit = "0,8";	
			}else{
				$limit = "0,4";	
			}		
		}else{
			$limit = "0, 100";
		}
		
		if($id)	$sql.= " ORDER BY db_ptiik_apps.tbl_silabus_komponen.menu_order ASC LIMIT ".$limit;	
		else $sql.= " ORDER BY db_ptiik_apps.tbl_matakuliah.kode_mk ASC, db_ptiik_apps.tbl_namamk.keterangan ASC LIMIT ".$limit;
		
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
	}
	
	function get_komponen_silabus(){
		$sql = "SELECT
					`tbl_silabus_komponen`.`komponen_id`,
					`tbl_silabus_komponen`.`keterangan` as `keterangan`,
					`tbl_silabus_komponen`.`is_aktif`
					FROM
					`db_ptiik_apps`.`tbl_silabus_komponen`
					WHERE `tbl_silabus_komponen`.`is_aktif` = 1 ORDER BY `tbl_silabus_komponen`.`menu_order` ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function read_silabus($unit=NULL, $lang=NULL,$id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_silabus_komponen.keterangan AS komponen,
					db_ptiik_apps.tbl_silabus_detail.keterangan,
					`tbl_silabus_komponen`.`komponen_id`
					FROM
					db_ptiik_apps.tbl_silabus
					INNER JOIN db_ptiik_apps.tbl_silabus_detail ON db_ptiik_apps.tbl_silabus.silabus_id = db_ptiik_apps.tbl_silabus_detail.silabus_id
					INNER JOIN db_ptiik_apps.tbl_silabus_komponen ON db_ptiik_apps.tbl_silabus_detail.komponen_id = db_ptiik_apps.tbl_silabus_komponen.komponen_id
				WHERE 1 = 1 
					";
		if($id){
			$sql = $sql. "AND mid(md5(db_ptiik_apps.tbl_silabus.mkditawarkan_id),9,7) = '".$id."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function read_topic($unit=NULL, $lang=NULL,$str=NULL, $id=NULL){
		$sql = "SELECT
				tbl_fakultas.fakultas_id,
				tbl_fakultas.keterangan AS fakultas,
				tbl_namamk.keterangan AS namamk,
				mid(md5(tbl_materimk.mkditawarkan_id),9,7) as `id`,
				mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7) as `mid`,
				tbl_mkditawarkan.icon AS icon_mk,
				tbl_materimk.icon,
				tbl_materimk.judul,
				tbl_materimk.keterangan,
				tbl_materimk.is_publish,
				tbl_materimk.hits,
				tbl_materimk.`status`,
				tbl_materimk.materi_id,
				tbl_materimk.is_valid,
				tbl_materimk.mkditawarkan_id
				FROM
				db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
				INNER JOIN db_ptiik_apps.tbl_fakultas ON db_ptiik_apps.tbl_namamk.fakultas_id = db_ptiik_apps.tbl_fakultas.fakultas_id
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
				INNER JOIN db_ptiik_apps.tbl_materimk ON db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = db_ptiik_apps.tbl_materimk.mkditawarkan_id
				WHERE
				db_ptiik_apps.tbl_materimk.is_valid ='1' AND db_ptiik_apps.tbl_materimk.is_publish ='1'  
		";
		if($id){			
			$sql = $sql . " AND mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7) = '".$id."' ";
			$limit = "0, 20";
		}
		
		if($str){
			if($str=='main'){
				$limit = "0,2";	
			}else{
				$limit = "0,8";	
			}			
		}else{
			$limit = "0, 100";
		}
		
		$sql = $sql. " ORDER BY tbl_materimk.hits DESC, tbl_materimk.last_update DESC, db_ptiik_apps.tbl_namamk.keterangan ASC, db_ptiik_apps.tbl_materimk.menu_order LIMIT ".$limit;
		
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
		
	}
	
	function read_tweet($limit=NULL){
		$sql = "SELECT
					db_ptiik_coms.tbl_twitter.created_at,
					db_ptiik_coms.tbl_twitter.id,
					db_ptiik_coms.tbl_twitter.id_str,
					db_ptiik_coms.tbl_twitter.keterangan,
					db_ptiik_coms.tbl_twitter.`name`,
					db_ptiik_coms.tbl_twitter.screen_name,
					db_ptiik_coms.tbl_twitter.profile_image_url
					FROM
					db_ptiik_coms.tbl_twitter ORDER BY created_at DESC LIMIT 0,$limit
					";
		return $this->db->query( $sql );
	}
	
	function read_sub_content($unit=NULL, $lang=NULL,$cat=NULL, $parent=NULL){
		$sql= "SELECT
				mid(md5(db_ptiik_coms.coms_content.content_id),9,7) as `id`, 
				db_ptiik_coms.coms_content.content_id,
				db_ptiik_coms.coms_content.content_category,
				db_ptiik_coms.coms_content.menu_order,
				db_ptiik_coms.coms_content.unit_id,
				db_ptiik_coms.coms_content.fakultas_id,
				db_ptiik_coms.coms_content.cabang_id,
				db_ptiik_coms.coms_content.content_title,
				db_ptiik_coms.coms_content.content_title as judul,
				db_ptiik_coms.coms_content.content_excerpt,
				db_ptiik_coms.coms_content.content as keterangan,
				db_ptiik_coms.coms_content.content_link as content_page,
				db_ptiik_coms.coms_content.content_data,
				db_ptiik_coms.coms_content.content_status,
				db_ptiik_coms.coms_content.content_lang,
				db_ptiik_coms.coms_content.content_hit as hits,
				db_ptiik_coms.coms_content.content_author,
				db_ptiik_coms.coms_content.content_upload,
				db_ptiik_coms.coms_content.content_comment,
				db_ptiik_coms.coms_content.content_thumb_img,
				db_ptiik_coms.coms_content.content_thumb_img as icon,
				db_ptiik_coms.coms_content.is_sticky,
				db_ptiik_coms.coms_content.user_id,
				db_ptiik_coms.coms_content.last_update,
				db_ptiik_coms.coms_content.parent_id
				FROM
				db_ptiik_coms.coms_content WHERE content_status ='publish' 
				";
		if($cat) $sql.= " AND coms_content.content_category='$cat' ";
		if($lang) $sql.= " AND coms_content.content_lang='$lang' ";
		if($parent) $sql.= " AND (coms_content.parent_id='$parent' OR mid(md5(db_ptiik_coms.coms_content.parent_id),9,7)='$parent') ";
		$sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		//if($unit) $sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		
		return $this->db->query( $sql );
	}
	
	public function read_content($unit=NULL, $lang=NULL,$cat=NULL, $url=NULL, $id=NULL, $slimit=NULL,$order_by=NULL){
	
		$sql= "SELECT
					mid(md5(db_ptiik_coms.coms_content.content_id),9,7) AS id,
					db_ptiik_coms.coms_content.content_id,
					db_ptiik_coms.coms_content.content_category,
					db_ptiik_coms.coms_content.menu_order,
					db_ptiik_coms.coms_content.unit_id,
					db_ptiik_coms.coms_content.fakultas_id,
					db_ptiik_coms.coms_content.cabang_id,
					db_ptiik_coms.coms_content.content_title,
					db_ptiik_coms.coms_content.content_title AS judul,
					db_ptiik_coms.coms_content.content_excerpt,
					db_ptiik_coms.coms_content.content AS keterangan,
					db_ptiik_coms.coms_content.content_link AS content_page,
					db_ptiik_coms.coms_content.content_data,
					db_ptiik_coms.coms_content.content_status,
					db_ptiik_coms.coms_content.content_lang,
					db_ptiik_coms.coms_content.content_hit AS hits,
					db_ptiik_coms.coms_content.content_author,
					db_ptiik_coms.coms_content.content_upload,
					db_ptiik_coms.coms_content.content_comment,
					db_ptiik_coms.coms_content.content_thumb_img AS thumb_img,
					db_ptiik_coms.coms_content.content_thumb_img AS icon,
					db_ptiik_coms.coms_content.is_sticky,
					db_ptiik_coms.coms_content.user_id,
					db_ptiik_coms.coms_content.last_update AS content_modified,
					db_ptiik_coms.coms_content.parent_id, ";
			if($lang=='in') $sql.= " db_ptiik_apps.tbl_unit_kerja.keterangan as unit_note ";
			else  $sql.= " db_ptiik_apps.tbl_unit_kerja.english_version as unit_note ";
			$sql.= "
					FROM
					db_ptiik_coms.coms_content
					LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_coms.coms_content.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id 
					WHERE content_status ='publish' 
				";
		
		if($unit!='-')	$sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		//if($unit) $sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		if($lang) $sql.= " AND db_ptiik_coms.coms_content.content_lang = '$lang' ";
		
		if($cat){
			if($cat!='pengumuman')	$sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
			
			$sql = $sql . " AND db_ptiik_coms.coms_content.content_category='".$cat."' AND db_ptiik_coms.coms_content.parent_id='0'  ";
			
			
			if($slimit){
				$limit = "0,".$slimit;
				
				if($cat='header_menu')  $order = " db_ptiik_coms.coms_content.menu_order ASC ";
				else $order = " db_ptiik_coms.coms_content.content_id DESC,db_ptiik_coms.coms_content.content_title ASC ";
			}else{
				if($cat=='news'){
					$order = " db_ptiik_coms.coms_content.content_hit DESC ";
					$limit = "0,5";
				}else{
					if($cat='header_menu') :
						$order = " db_ptiik_coms.coms_content.menu_order ASC ";
						$limit = "0,100";
					else:
						$limit = "0,10";
					endif; 
				}
			}
		}
		if($url){
			$sql = $sql . " AND db_ptiik_coms.coms_content.content_link='".$url."' ";
			$order = " coms_content.last_update DESC ";
			$limit = "0,1";
		}
		
		if($id){
			$sql = $sql . " AND (db_ptiik_coms.coms_content.content_id='".$id."' OR mid(md5(db_ptiik_coms.coms_content.content_id),9,7) = '".$id."') ";
			$order = " coms_content.last_update DESC ";
			$limit = "0,1";
		}
		
		
		$sql.= " ORDER BY ".$order. " LIMIT ".$limit;
	//echo $sql."<br>";
		if($url) $result = $this->db->getRow( $sql );
		else $result = $this->db->query( $sql );
		return $result;	
		
	}
	
	function  read_video($unit=NULL, $lang=NULL,$id=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_file.file_id,
				mid(md5(db_ptiik_apps.tbl_file.file_id),9,7) as `id`,
				db_ptiik_apps.tbl_file.judul,
				db_ptiik_apps.tbl_file.keterangan,
				db_ptiik_apps.tbl_file.file_name,
				db_ptiik_apps.tbl_file.file_loc,
				db_ptiik_apps.tbl_file.tgl_upload,
				db_ptiik_apps.tbl_file.jenis_file_id,
				db_ptiik_apps.tbl_file.materi_id
				FROM
				db_ptiik_apps.tbl_file
				INNER JOIN db_ptiik_apps.tbl_jenisfile ON db_ptiik_apps.tbl_file.jenis_file_id = db_ptiik_apps.tbl_jenisfile.jenis_file_id
				WHERE
				db_ptiik_apps.tbl_file.jenis_file_id = '2013110002' AND db_ptiik_apps.tbl_file.is_publish='1' AND db_ptiik_apps.tbl_file.materi_id='' ";
		if($id){
			$sql = $sql . " AND (db_ptiik_apps.tbl_file.file_id='".$id."'  OR mid(md5(db_ptiik_apps.tbl_file.file_id),9,7) = '".$id."') ";
		}
		$sql = $sql. " ORDER BY db_ptiik_apps.tbl_file.last_update DESC LIMIT 0,2";
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
	}
	
	function content($content, $length) {	
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
	function get_statusmateri(){
		$sql = "SELECT
				db_ptiik_apps.tbl_statusmateri.`status`,
				db_ptiik_apps.tbl_statusmateri.keterangan
				FROM
				db_ptiik_apps.tbl_statusmateri
				";
		$result = $this->db->query($sql);		

		return $result;
	}
	
	function update_hits($id=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_materimk SET hits=hits+1 WHERE mid(md5(tbl_materimk.materi_id),9,7)='".$id."' ";
		return $this->db->query($sql);	
	}
	
	function update_hits_content($str=NULL){
		$sql = "UPDATE db_ptiik_coms.coms_content SET content_hit=content_hit+1 WHERE mid(md5(db_ptiik_coms.coms_content.content_id),9,7)='".$str."' ";
		
		return $this->db->query($sql);	
	}
	
	function replace_comment($datanya){
		return $this->db->replace('db_ptiik_coms`.`content_comments',$datanya);
	}
	
	function get_hari($str=NULL){
		switch($str){
			case '1': $hari="Senin";break;
			case '2': $hari="Selasa";break;
			case '3': $hari="Rabu";break;
			case '4': $hari="Kamis";break;
			case '5': $hari="Jumat";break;
			case '6': $hari="Sabtu";break;
			case '7': $hari="Minggu";break;
		}
		
		return $hari;
		
		
	}

}