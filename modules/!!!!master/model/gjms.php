<?php
class model_gjm extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL, $fakultas=NULL, $cabang=NULL, $unit=NULL){
		$sql = "SELECT
					mid(md5(`tbl_gjm_file`.`file_id`),6,6) AS file_id,
					db_ptiik_apps.tbl_gjm_file.file_id AS hid_id,
					db_ptiik_apps.tbl_gjm_file.kategori_id,
					db_ptiik_apps.tbl_gjm_file.judul,
					db_ptiik_apps.tbl_gjm_file.keterangan,
					db_ptiik_apps.tbl_gjm_file.no_dokumen,
					db_ptiik_apps.tbl_gjm_file.unit_id,
					db_ptiik_apps.tbl_gjm_file.fakultas_id,
					db_ptiik_apps.tbl_gjm_file.cabang_id,
					db_ptiik_apps.tbl_gjm_file.file_type,
					db_ptiik_apps.tbl_gjm_file.file_name,
					db_ptiik_apps.tbl_gjm_file.file_loc,
					db_ptiik_apps.tbl_gjm_file.file_size,
					db_ptiik_apps.tbl_gjm_file.jenis_file,
					db_ptiik_apps.tbl_gjm_file.is_publish,
					db_ptiik_apps.tbl_gjm_kategori.keterangan AS kategori
					FROM
					db_ptiik_apps.tbl_gjm_file
					INNER JOIN db_ptiik_apps.tbl_gjm_kategori ON db_ptiik_apps.tbl_gjm_file.kategori_id = db_ptiik_apps.tbl_gjm_kategori.kategori_id
					 WHERE 1 ";
				
		if($id) $sql .= " AND (mid(md5(file_id),6,6) = '" . $id ."' OR file_id='$id')";	
		if($fakultas) $sql.= " AND db_ptiik_apps.tbl_gjm_file.fakultas_id='$fakultas' ";
		if($cabang) $sql.= " AND db_ptiik_apps.tbl_gjm_file.cabang_id='$cabang' ";
		if($unit) $sql.= " AND db_ptiik_apps.tbl_gjm_file.unit_id='$unit' ";
		
		$sql .= "ORDER BY `tbl_gjm_file`.`file_id` DESC";
		
		if($id) $result = $this->db->getRow( $sql );
		else $result = $this->db->query( $sql );	
		
		return $result;	
	}
	
	function read_category($id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_gjm_kategori.kategori_id,
					db_ptiik_apps.tbl_gjm_kategori.keterangan,
					db_ptiik_apps.tbl_gjm_kategori.parent_id
					FROM
					db_ptiik_apps.tbl_gjm_kategori WHERE 1 ";
		if($id) $sql.= " AND tbl_gjm_kategori.kategori_id='$id' ";
		
		if($id) $result = $this->db->getRow( $sql );
		else $result = $this->db->query( $sql );	
		
		return $result;	

	}

	function get_reg_number(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_gjm_file WHERE left(file_id,6) = '".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_gjm($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_gjm_file',$datanya);
	}
		
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, 
					   keterangan, extention, 
					   max_size 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		return $result;

	}
	
	
}
?>