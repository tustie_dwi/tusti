<?php
class model_jadwalmk extends model {
	private $coms;
	
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL)
	{
		$sql = "SELECT
				db_ptiik_apps.tbl_jadwalmk.jadwal_id AS hid_id,
				mid(md5(`tbl_jadwalmk`.`jadwal_id`),6,6) as jadwal_id,
				db_ptiik_apps.tbl_jadwalmk.kelas,
				db_ptiik_apps.tbl_jadwalmk.pengampu_id,
				db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
				db_ptiik_apps.tbl_jadwalmk.cabang_id,
				mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),6,6) as `prodi_id`,
				db_ptiik_apps.tbl_jadwalmk.jam_mulai,
				db_ptiik_apps.tbl_jadwalmk.jam_selesai,
				db_ptiik_apps.tbl_jadwalmk.hari,
				concat(`db_ptiik_apps`.`tbl_ruang`.keterangan, ' - ',`db_ptiik_apps`.`tbl_ruang`.ruang_id, ' - Kapasitas : ',`db_ptiik_apps`.`tbl_ruang`.kapasitas) as `ruangedit`,
				db_ptiik_apps.tbl_jadwalmk.is_online,
				db_ptiik_apps.tbl_jadwalmk.is_praktikum,
				db_ptiik_apps.tbl_jadwalmk.user_id,
				db_ptiik_apps.tbl_jadwalmk.is_aktif,
				DATE_FORMAT(db_ptiik_apps.tbl_jadwalmk.last_update, '%Y-%m-%d') as `YMD`,
				TIME_FORMAT(db_ptiik_apps.tbl_jadwalmk.last_update, '%H:%i') as `waktu`,
				db_ptiik_apps.tbl_mkditawarkan.tahun_akademik as `tahunakademik`,
				db_ptiik_apps.tbl_mkditawarkan.icon,
				db_ptiik_apps.tbl_namamk.keterangan AS namamk,
				db_ptiik_apps.tbl_prodi.keterangan AS prodi,
				db_ptiik_apps.tbl_karyawan.nama AS pengampu,
				db_ptiik_apps.tbl_karyawan.karyawan_id,
				db_ptiik_apps.tbl_ruang.keterangan AS ruang,
				db_ptiik_apps.tbl_ruang.ruang_id, 
				mid(md5(tbl_prodi.fakultas_id),6,6) AS fakultasid,
				db_coms.coms_user.`name` AS `user`,
				concat(db_ptiik_apps.tbl_tahunakademik.tahun,' ', db_ptiik_apps.tbl_tahunakademik.is_ganjil,' ', db_ptiik_apps.tbl_tahunakademik.is_pendek) as `thnakademik`
				FROM
				db_ptiik_apps.tbl_jadwalmk
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
				INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_jadwalmk.pengampu_id = db_ptiik_apps.tbl_pengampu.pengampu_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_ruang ON db_ptiik_apps.tbl_jadwalmk.ruang_id = db_ptiik_apps.tbl_ruang.ruang_id
				LEFT JOIN db_coms.coms_user ON db_ptiik_apps.tbl_jadwalmk.user_id = db_coms.coms_user.id
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
				 WHERE 1=1 ";
		
		if($id){
			$sql=$sql . " AND mid(md5(`tbl_jadwalmk`.`jadwal_id`),6,6)='".$id."' ";
		}
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_excel_data($fakultas=NULL,$thnakademik=NULL){
		$sql = "SELECT
				tbl_namamk.english_version,
				tbl_namamk.keterangan as namamk,
				tbl_matakuliah.kode_mk,
				tbl_matakuliah.sks,
                tbl_matakuliah.kurikulum,
				tbl_jadwalmk.kelas,
				tbl_jadwalmk.prodi_id,
				tbl_jadwalmk.ruang_id,
				tbl_jadwalmk.jam_mulai,
				tbl_jadwalmk.jam_selesai,
				tbl_jadwalmk.hari,
				CASE tbl_jadwalmk.is_praktikum 
				  WHEN 1 THEN 'Praktikum'
				END as is_praktikum,
				tbl_karyawan.nama as dosen
				FROM db_pjj.tbl_jadwalmk 
				LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_jadwalmk.pengampu_id = tbl_pengampu.pengampu_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_pengampu.karyawan_id = tbl_karyawan.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_jadwalmk.prodi_id = tbl_unit_kerja.unit_id
				WHERE 1
				AND tbl_unit_kerja.kategori = 'prodi'";
				
		if($fakultas){
			$sql=$sql . " AND tbl_unit_kerja.fakultas_id = '".$fakultas."' ";
		}
		
		if($thnakademik){
			$sql=$sql . " AND tbl_mkditawarkan.tahun_akademik = '".$thnakademik."' ";
		}
		
		$sql .= " ORDER BY tbl_unit_kerja.unit_id ASC, tbl_jadwalmk.is_praktikum ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_excel_dosen($fakultas){
		$sql = "SELECT
				tbl_karyawan.karyawan_id,
				tbl_karyawan.nama
				FROM db_ptiik_apps.tbl_karyawan
				WHERE 1";
				
		if($fakultas){
			$sql=$sql . " AND tbl_karyawan.fakultas_id = '".$fakultas."' ";
		}
		
		$sql .= " ORDER BY tbl_karyawan.nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_excel_mk($fakultas){
		$sql = "SELECT 
				tbl_namamk.keterangan as 'namamk',
				tbl_namamk.english_version
				FROM db_ptiik_apps.tbl_namamk
				WHERE 1";
				
		if($fakultas){
			$sql=$sql . " AND tbl_namamk.fakultas_id = '".$fakultas."' ";
		}
		
		$sql .= " ORDER BY tbl_namamk.keterangan ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	// function excel_check_namamk($namamk=NULL){
		// $sql = "SELECT tbl_namamk.namamk_id
				// FROM db_ptiik_apps.tbl_namamk 
				// WHERE 1
			   // ";
// 		
		// if($namamk){
			// $sql .= " AND tbl_namamk.keterangan = '".$namamk."' ";
		// }
// 		
		// $dt = $this->db->getRow( $sql );
		// if(isset($dt)){
			// $strresult = $dt->namamk_id;
			// return $strresult;
		// }else return NULL;
	// }
	
	function get_data($id){
		$sql="SELECT `tbl_mkditawarkan`.tahun_akademik,
			  `tbl_mkditawarkan`.cabang_id,
			  mkditawarkan_id,
	    	 (SELECT mid(md5(`db_ptiik_apps`.`tbl_fakultas`.fakultas_id),6,6)
	    	 FROM 
	    	 `db_ptiik_apps`.`tbl_fakultas`, 
	    	 `db_ptiik_apps`.`tbl_namamk`, 
	    	 `db_ptiik_apps`.`tbl_matakuliah` 
	    	 WHERE `db_ptiik_apps`.`tbl_fakultas`.fakultas_id =  `db_ptiik_apps`.`tbl_namamk`.fakultas_id 
	    	 AND `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
	    	 AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as fakultasid,
	    	 
             (SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan 
             FROM 
             `db_ptiik_apps`.`tbl_namamk`, 
             `db_ptiik_apps`.`tbl_matakuliah` 
             WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
             AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk
			 
			 FROM `db_ptiik_apps`.`tbl_mkditawarkan`
			 WHERE mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) = '".$id."'";
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	function get_pengampubymk($id){
		$sql="SELECT 
			  (SELECT `db_ptiik_apps`.`tbl_karyawan`.nama 
			  FROM `db_ptiik_apps`.`tbl_karyawan` 
			  WHERE `db_ptiik_apps`.`tbl_karyawan`.karyawan_id =  `db_ptiik_apps`.`tbl_pengampu`.karyawan_id) as nama,
			  `db_ptiik_apps`.`tbl_pengampu`.pengampu_id
			  FROM 
			  `db_ptiik_apps`.`tbl_pengampu`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan`
			  
			  WHERE `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id 
              AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = (SELECT `db_ptiik_apps`.`tbl_jadwalmk`.mkditawarkan_id FROM `db_ptiik_apps`.`tbl_jadwalmk` WHERE mid(md5(`db_ptiik_apps`.`tbl_jadwalmk`.`jadwal_id`),6,6) = '".$id."' )"; 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_pengampu($id){
		$sql="SELECT 
			  (SELECT 
			  `db_ptiik_apps`.`tbl_karyawan`.nama 
			  FROM `db_ptiik_apps`.`tbl_karyawan`
			  WHERE `db_ptiik_apps`.`tbl_karyawan`.karyawan_id =  `db_ptiik_apps`.`tbl_pengampu`.karyawan_id) as nama,
			   `db_ptiik_apps`.`tbl_pengampu`.pengampu_id
			  FROM 
			  `db_ptiik_apps`.`tbl_pengampu`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan`
			  WHERE mid(md5(`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`),6,6)='".$id."'
			   AND mid(md5( `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`),6,6)='".$id."'"; 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_isonline_from_mk($id){
		$sql = "SELECT is_online FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
			  	WHERE tbl_mkditawarkan.mkditawarkan_id = '".$id."'"; 
		// echo $sql;
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->is_online;
		return $strresult;
		}
	}
	
	function get_isonline_from_mkditawarkan($id){
		$sql="SELECT is_online FROM `db_ptiik_apps`.`tbl_mkditawarkan` where mid(md5(mkditawarkan_id),6,6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->is_online;
	
		return $strresult;}
	}
	
	function get_is_online_by_mkditawarkan($id){
		$sql="SELECT is_online FROM `db_ptiik_apps`.`tbl_mkditawarkan` where mkditawarkan_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->is_online;
	
		return $strresult;}
	}
	
	function get_prodi_by_fakultas($fakultasid){
		$sql="SELECT mid(md5(`db_ptiik_apps`.`tbl_prodi`.`prodi_id`),6,6) as `prodi_id`,
			  `db_ptiik_apps`.`tbl_prodi`.`prodi_id` as hid_id,
			  `db_ptiik_apps`.`tbl_prodi`.`keterangan`
			  FROM `db_ptiik_apps`.`tbl_prodi` 
			  LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` ON `db_ptiik_apps`.`tbl_prodi`.`fakultas_id` = `db_ptiik_apps`.`tbl_fakultas`.`fakultas_id`
			  WHERE 1
			 "; 
		
		if($fakultasid!=""){
			$sql=$sql . " AND mid(md5(`db_ptiik_apps`.`tbl_fakultas`.`fakultas_id`),6,6) = '".$fakultasid."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_mkbythnakademik($thnakademikid, $prodiid){
		
		  $sql="SELECT `db_ptiik_apps`.`vw_mk_by_dosen`.namamk,
				mid(md5(`db_ptiik_apps`.`vw_mk_by_dosen`.mkditawarkan_id),6,6) as mkditawarkan_id
				FROM `db_ptiik_apps`.`vw_mk_by_dosen` 
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan`
				ON `db_ptiik_apps`.`vw_mk_by_dosen`.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id
			  "; 
		
		if($thnakademikid){
			$sql=$sql . " WHERE `db_ptiik_apps`.`tbl_mkditawarkan`.tahun_akademik = '".$thnakademikid."' ";
		}
		
		if($prodiid){
			$sql=$sql . " AND mid(md5(`db_ptiik_apps`.`vw_mk_by_dosen`.prodi_id),6,6) = '".$prodiid."' ";
		}
		
		$sql=$sql . " GROUP BY `db_ptiik_apps`.`vw_mk_by_dosen`.mkditawarkan_id ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_pengampuindex($id=NULL){
		if(!$id){
			$sql="SELECT 
				  (SELECT `db_ptiik_apps`.`tbl_karyawan`.nama 
				  FROM `db_ptiik_apps`.`tbl_karyawan`
				  WHERE `db_ptiik_apps`.`tbl_karyawan`.karyawan_id = `db_ptiik_apps`.`tbl_pengampu`.karyawan_id) as nama,
				  
			  	  mid(md5(`db_ptiik_apps`.`tbl_pengampu`.pengampu_id),6,6) as pengampu_id
			      
			      FROM 
			      `db_ptiik_apps`.`tbl_pengampu`, 
			      `db_ptiik_apps`.`tbl_mkditawarkan`
                  WHERE `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id 
                  GROUP BY pengampu_id
			"; 
		}
		else {
			$sql="SELECT 
				  (SELECT `db_ptiik_apps`.`tbl_karyawan`.nama 
				  FROM `db_ptiik_apps`.`tbl_karyawan`
				  WHERE `db_ptiik_apps`.`tbl_karyawan`.karyawan_id = `db_ptiik_apps`.`tbl_pengampu`.karyawan_id) as nama,
				  
			  	  mid(md5(`db_ptiik_apps`.`tbl_pengampu`.pengampu_id),6,6) as pengampu_id
			      
			      FROM 
			      `db_ptiik_apps`.`tbl_pengampu`, 
			      `db_ptiik_apps`.`tbl_mkditawarkan`
				  WHERE mid(md5(`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) LIKE '%".$id."%'
				   AND mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`),6,6) LIKE '%".$id."%'
				"; 
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_allkelas(){
		$sql="SELECT `kelas` FROM `db_ptiik_apps`.`tbl_jadwalmk` WHERE 1 group by `kelas`
			"; 
	$result = $this->db->query( $sql );
	return $result;
	}
	
	function get_fakultas(){
		$sql="SELECT fakultas_id, keterangan
			  FROM `db_ptiik_apps`.`tbl_fakultas`
			 ";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_prodi(){
		$sql="SELECT mid(md5(`tbl_prodi`.`prodi_id`),6,6) prodi_id, keterangan
			  FROM `db_ptiik_apps`.`tbl_prodi` 
			  WHERE is_aktif = 1
			 ";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_kelas($x,$y){
		$sql="SELECT ruang_id 
			  FROM `db_ptiik_apps`.`tbl_ruang` 
			  WHERE keterangan = '".$x."' AND ruang_id = '".$y."'
			 ";
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->ruang_id;
	
		return $strresult;}
	}
	
	function get_pengampubydos_mk($karyawanid=NULL, $mkditawarkan=NULL){
		$sql="SELECT pengampu_id 
			  FROM `db_ptiik_apps`.`tbl_pengampu` 
			  WHERE karyawan_id = '".$karyawanid."'
			  AND mkditawarkan_id = '".$mkditawarkan."'
			 ";
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->pengampu_id;
	
		return $strresult;}
	}
	
	function get_namamk(){
		$sql = "SELECT 
				(SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan 
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah`, 
				`db_ptiik_apps`.`tbl_mkditawarkan`
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id 
				AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_jadwalmk`.mkditawarkan_id ) as namamk,
				mid(md5(`tbl_jadwalmk`.`mkditawarkan_id`),6,6) as mkditawarkan_id
				FROM `db_ptiik_apps`.`tbl_jadwalmk`
				GROUP BY mkditawarkan_id
				";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_tahun_akademik(){
		$sql = "SELECT  `tbl_tahunakademik`.`tahun_akademik` , CONCAT( tahun,  ' ', is_ganjil,  ' ', is_pendek ) AS tahun
				FROM  `db_ptiik_apps`.`tbl_tahunakademik` 
				WHERE is_aktif = '1'
				";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_all_thn_akademik(){
		$sql = "SELECT tahun_akademik, CONCAT(tahun , ' ' , is_ganjil , ' ' , is_pendek) as thnakademik FROM `db_ptiik_apps`.`tbl_tahunakademik`
				ORDER BY tahun_akademik DESC
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function ceknamamk_mkditawarkan($term){
		$sql="SELECT mkditawarkan_id 
			  FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
			  WHERE `tbl_mkditawarkan`.`mkditawarkan_id` = '".$term."'
			 ";
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
		
		return $strresult;}
	}
	
	function get_karyawanid($name=NULL){
		$sql="SELECT karyawan_id 
			  FROM `db_ptiik_apps`.`tbl_karyawan` 
			  WHERE `tbl_karyawan`.`nama` = '".$name."'
			 ";
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->karyawan_id;
		
		return $strresult;}
	}
	
	function get_pengampu_excel($karyawan_id=NULL,$mkditawarkan=NULL){
		$sql="SELECT pengampu_id 
			  FROM `db_ptiik_apps`.`tbl_pengampu` 
			  WHERE `tbl_pengampu`.`karyawan_id` = '".$karyawan_id."'
			  AND `tbl_pengampu`.`mkditawarkan_id` = '".$mkditawarkan."'
			 ";
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->pengampu_id;
		
		return $strresult;}
	}
	
	function cek_jadwalmk($kelas=NULL,$pengampu_id=NULL,$mkditawarkan=NULL,$prodi=NULL,$ruang=NULL,$jam_mulai=NULL,$jam_selesai=NULL,$hari=NULL){
		$sql="SELECT jadwal_id 
			  FROM `db_ptiik_apps`.`tbl_jadwalmk` 
			  WHERE `tbl_jadwalmk`.`kelas` = '".$kelas."'
			  AND `tbl_jadwalmk`.`pengampu_id` = '".$pengampu_id."'
			  AND `tbl_jadwalmk`.`mkditawarkan_id` = '".$mkditawarkan."'
			  AND `tbl_jadwalmk`.`prodi_id` = '".$prodi."'
			  AND `tbl_jadwalmk`.`jam_mulai` = '".$jam_mulai."'
			  AND `tbl_jadwalmk`.`jam_selesai` = '".$jam_selesai."'
			  AND `tbl_jadwalmk`.`hari` = '".$hari."'
			  AND `tbl_jadwalmk`.`ruang_id` = '".$ruang."'
			 ";
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->jadwal_id;
		
		return $strresult;}
	}
	
	function cek_mkditawarkan_by_namamk($namamk=NULL,$thn_akademik=NULL,$praktikum=NULL){
		$sql = "SELECT tbl_mkditawarkan.mkditawarkan_id
				FROM db_ptiik_apps.tbl_mkditawarkan
				LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				WHERE 1
			   ";
		
		if($namamk){
			$sql .= " AND tbl_namamk.keterangan = '".$namamk."' ";
		}
		
		if($thn_akademik){
			$sql .= " AND tbl_mkditawarkan.tahun_akademik = '".$thn_akademik."' ";
		}
		
		if($praktikum){
			$sql .= " AND tbl_mkditawarkan.is_praktikum = '".$praktikum."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
		return $strresult;}
		
	}
	
	function ceknamamk_matakuliah($term){
		$sql="SELECT matakuliah_id 
			  FROM `db_ptiik_apps`.`tbl_matakuliah` 
			  WHERE (SELECT a.keterangan 
				     FROM `db_ptiik_apps`.`tbl_namamk` as a 
				     WHERE a.namamk_id = `tbl_matakuliah`.namamk_id ) = '".$term."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->matakuliah_id;
		return $strresult;}
	}
	
	function ceknamamk_namamk($term){
		$sql="SELECT namamk_id 
			  FROM `db_ptiik_apps`.`tbl_namamk` 
			  WHERE keterangan = '".$term."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->namamk_id;
			return $strresult;
		}
	}
	
	function get_mkditawarkan($cabangid=NULL, $fakultasid=NULL, $thnakademikid=NULL, $ceknamamkval=NULL){
		$sql="SELECT mkditawarkan_id 
		      FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
			  WHERE 1
			 ";
		
		if($ceknamamkval){
		$sql = $sql . " AND (SELECT a.keterangan 
				     		 FROM `db_ptiik_apps`.`tbl_namamk` as a, `db_ptiik_apps`.`tbl_matakuliah` as b 
				     		 WHERE a.namamk_id = b.namamk_id 
				     		 AND b.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id ) = '".$ceknamamkval."' ";
		}
		
		if($cabangid){
		$sql = $sql . " AND tbl_mkditawarkan.cabang_id = '".$cabangid."' ";
		}
		
		if($fakultasid){
		$sql = $sql . " AND (SELECT mid(md5(a.fakultas_id),6,6)
				     		 FROM `db_ptiik_apps`.`tbl_namamk` as a, `db_ptiik_apps`.`tbl_matakuliah` as b 
				     		 WHERE a.namamk_id = b.namamk_id 
				     		 AND b.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id ) = '".$fakultasid."' ";
		}
		
		if($thnakademikid){
		$sql = $sql . " AND tbl_mkditawarkan.tahun_akademik = '".$thnakademikid."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
		return $strresult;}
	}
	
	function getmkditawarkan_byjadwalid($id=NULL){
		$sql="SELECT mid(md5(mkditawarkan_id),6,6) as mkditawarkan_id 
		      FROM `db_ptiik_apps`.`tbl_jadwalmk` 
			  WHERE mid(md5(`tbl_jadwalmk`.`jadwal_id`),6,6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
		return $strresult;}
	}
	
	function get_reg_number($mkid=NULL, $staff=NULL){
		$sql = "SELECT jadwal_id FROM db_ptiik_apps.tbl_jadwalmk WHERE mkditawarkan_id ='".$mkid."' AND pengampu_id='".$staff."'  ";
		$result = $this->db->getRow( $sql );
		
		if($result){			
			 $strresult = $result->jadwal_id;
		}else{
			$kode = $mkid;
			
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jadwal_id,4) AS 
					unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_jadwalmk"; 
					
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		}
		return $strresult;
	}
	
	function replace_jadwalmk($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
	}
	
	function delete($id){
		$sql="DELETE FROM `db_ptiik_apps`.`tbl_jadwalmk` 
			  WHERE mid(md5(`db_ptiik_apps`.`tbl_jadwalmk`.`jadwal_id`),6,6)= '".$id."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;
		}
		
	}
	
	function getmktype(){
		$sql="SELECT DISTINCT is_online FROM `db_ptiik_apps`.`tbl_jadwalmk` WHERE 1
			 "; 
	$result = $this->db->query( $sql );
	return $result;
	}
	
	function get_prodiid($id=NULL){
		$sql="SELECT prodi_id
			  FROM `db_ptiik_apps`.`tbl_prodi` 
			  WHERE mid(md5(prodi_id),6,6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->prodi_id;
			return $strresult;
		}
	}
	
	function get_pengampubyfak($id){
		$sql="SELECT karyawan_id, nama
		      FROM `db_ptiik_apps`.`tbl_karyawan` 
			  WHERE fakultas_id = '".$id."'
			 "; 
	$result = $this->db->query( $sql );
	return $result;
	}
	
	function read_by_parameter($prodiid=NULL, $mkid=NULL, $pengampuid=NULL, $hariid=NULL, $thnakademikid=NULL, $typeid=NULL, $fakultasid=NULL,$cabangid=NULL)
	{
		if($prodiid==NULL&&$mkid==NULL&&$pengampuid==NULL&&$hariid==NULL&&$thnakademikid==NULL&&$typeid==NULL&&$fakultasid==NULL&&$cabangid==NULL){
			return null;
		}
		else{		        
		$sql = "SELECT
				db_ptiik_apps.vw_mk_by_dosen.karyawan_id,
				db_ptiik_apps.vw_mk_by_dosen.pengampu_id,
				db_ptiik_apps.vw_mk_by_dosen.nama as pengampu,
				db_ptiik_apps.vw_mk_by_dosen.namamk,
				db_ptiik_apps.vw_mk_by_dosen.prodi,
				db_ptiik_apps.vw_mk_by_dosen.kelas,
				db_ptiik_apps.vw_mk_by_dosen.jam_mulai,
				db_ptiik_apps.vw_mk_by_dosen.jam_selesai,
				db_ptiik_apps.vw_mk_by_dosen.hari,
				db_ptiik_apps.vw_mk_by_dosen.ruang_id as ruang,
				db_ptiik_apps.vw_mk_by_dosen.mkditawarkan_id,
				db_ptiik_apps.vw_mk_by_dosen.prodi_id,
				mid(md5(vw_mk_by_dosen.`jadwal_id`),6,6) as jadwal_id,
				db_ptiik_apps.vw_mk_by_dosen.jadwal_id as hid_id,
				db_ptiik_apps.vw_mk_by_dosen.namamk_id,
				(SELECT a.username FROM `db_coms`.`coms_user` as a WHERE a.id =  `tbl_jadwalmk`.user_id) as user,
				substring(`tbl_jadwalmk`.last_update, 1,10) as YMD,
        		substring(`tbl_jadwalmk`.last_update, 12,8) as waktu,
				 tbl_jadwalmk.is_aktif
				FROM
				db_ptiik_apps.vw_mk_by_dosen
                LEFT JOIN db_ptiik_apps.tbl_jadwalmk
                ON db_ptiik_apps.vw_mk_by_dosen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
                LEFT JOIN db_ptiik_apps.tbl_namamk
                ON db_ptiik_apps.vw_mk_by_dosen.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
                LEFT JOIN db_ptiik_apps.tbl_fakultas
                ON db_ptiik_apps.tbl_namamk.fakultas_id = db_ptiik_apps.tbl_fakultas.fakultas_id
                WHERE 1
				";
				
		if($cabangid){
			$sql=$sql . " AND `tbl_jadwalmk`.`cabang_id` = '".$cabangid."' ";
		}
		if($typeid){
			$sql=$sql . " AND `tbl_jadwalmk`.`is_online` LIKE '%".$typeid."%' ";
		}
		if($fakultasid){
			$sql=$sql . " AND mid(md5(`tbl_fakultas`.`fakultas_id`),6,6) = '".$fakultasid."' ";
		}
		if($thnakademikid){
			$sql=$sql . " AND `vw_mk_by_dosen`.`tahun_akademik` LIKE '%".$thnakademikid."%' ";
		}
		if($prodiid){
			$sql=$sql . " AND mid(md5(`vw_mk_by_dosen`.`prodi_id`),6,6) LIKE '%".$prodiid."%' ";
		}
		if($mkid){
			$sql=$sql . " AND mid(md5(`vw_mk_by_dosen`.`mkditawarkan_id`),6,6) LIKE '%".$mkid."%' ";
		}
		if($pengampuid){
			$sql=$sql . " AND mid(md5(`vw_mk_by_dosen`.`pengampu_id`),6,6) LIKE '%".$pengampuid."%' ";
		}
		if($hariid){
			$sql=$sql . " AND `tbl_jadwalmk`.`kelas` LIKE '%".$hariid."%' ";
		}

		$result = $this->db->query( $sql );
		return $result;
		}	
	}
	
}
?>