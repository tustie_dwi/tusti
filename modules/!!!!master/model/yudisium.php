<?php
class model_yudisium extends model {

	public function __construct() {
		parent::__construct();	
	}

	function get_yudisium(){
		$sql = "SELECT 
					MID(MD5(tbl_yudisium.yudisium_id),8,6) yudisium_id,
					tbl_yudisium.periode,
					tbl_yudisium.judul,
					tbl_yudisium.keterangan,
					tbl_yudisium.last_update,
					
					tbl_tahunakademik.tahun,
					tbl_tahunakademik.is_ganjil,
					tbl_tahunakademik.is_pendek,
					
					tbl_ruang.keterangan ruang,
					tbl_ruang.kode_ruang
				FROM db_ptiik_apps.tbl_yudisium
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_yudisium.tahun_akademik
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_ruang.ruang_id = tbl_yudisium.ruang_id
				ORDER BY tbl_yudisium.last_update DESC";
		
		return $this->db->query($sql);
	}
	
	function get_tahunakademik(){
		$sql = "SELECT 
					tbl_tahunakademik.tahun_akademik,
					tbl_tahunakademik.tahun,
					tbl_tahunakademik.is_ganjil,
					tbl_tahunakademik.is_pendek
				FROM db_ptiik_apps.tbl_tahunakademik
				ORDER BY tbl_tahunakademik.tahun DESC, tbl_tahunakademik.is_ganjil";
		return $this->db->query($sql);
	}
	
	function get_ruang($cabang_id=NULL){
		$sql = "SELECT 
					tbl_ruang.ruang_id,
					tbl_ruang.keterangan,
					tbl_ruang.kode_ruang
				FROM db_ptiik_apps.tbl_ruang
				WHERE tbl_ruang.cabang_id = '$cabang_id'
				ORDER BY tbl_ruang.kode_ruang";
		return $this->db->query($sql);
	}
	
	function get_yudisium_id(){
		$sql = "SELECT 
					concat('" . date("Ym") . "',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(yudisium_id,4) AS 
					unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_yudisium WHERE left(yudisium_id,6)='" . date("Ym") . "' ";
		$strresult = $this -> db -> getRow($sql);
		return $strresult -> data;
	}
	
	function save_yudisium($data){
		$this-> db->replace('db_ptiik_apps`.`tbl_yudisium', $data);
	}
	
	//edit yudisium
	function get_yudisium_detail($yudisium_id){
		$sql = "SELECT 
					MID(MD5(tbl_yudisium.yudisium_id),8,6) yudisium_id,
					tbl_yudisium.periode,
					tbl_yudisium.judul,
					tbl_yudisium.keterangan,
					tbl_yudisium.tahun_akademik,
					tbl_yudisium.ruang_id,
					tbl_yudisium.tgl_yudisium,
					
					tbl_tahunakademik.tahun,
					tbl_tahunakademik.is_ganjil,
					tbl_tahunakademik.is_pendek,
					
					tbl_ruang.keterangan ruang,
					tbl_ruang.kode_ruang
				FROM db_ptiik_apps.tbl_yudisium
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_yudisium.tahun_akademik
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_ruang.ruang_id = tbl_yudisium.ruang_id
				WHERE MID(MD5(tbl_yudisium.yudisium_id),8,6) = '$yudisium_id'";
		
		return $this->db->getRow($sql);
	}
	
	function edit_yudisium($data, $where){
		$this-> db->update('db_ptiik_apps`.`tbl_yudisium', $data, $where);
	}
	function del_yudisium($yudisium_id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_yudisium WHERE MID(MD5(tbl_yudisium.yudisium_id),8,6) = '$yudisium_id'";
		$this->db->query($sql);
	}
	
	//detail yudisium
	function get_mahasiswa(){
		$sql = "SELECT 
					tbl_mahasiswa.mahasiswa_id,
					tbl_mahasiswa.nim,
					tbl_mahasiswa.nama,
					tbl_mahasiswa.angkatan
				FROM db_ptiik_apps.tbl_mahasiswa
				WHERE tbl_mahasiswa.is_aktif ='aktif'
				ORDER BY tbl_mahasiswa.nama";
		return $this->db->query($sql);
	}
	
	function get_yudisium_mhs($yudisium_id){
		$sql = "SELECT 
					MID(MD5(tbl_yudisium_mhs.mhs_id),8,6) mhs_id,
					tbl_mahasiswa.mahasiswa_id,
					tbl_mahasiswa.nim,
					tbl_mahasiswa.nama,
					tbl_mahasiswa.angkatan
				FROM db_ptiik_apps.tbl_yudisium_mhs
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_yudisium_mhs.mahasiswa_id
				WHERE MID(MD5(tbl_yudisium_mhs.yudisium_id),8,6) = '$yudisium_id'
				ORDER BY tbl_mahasiswa.nama";
		return $this->db->query($sql);
	}
	
	function del_mhs($mhs_id, $yudisium_id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_yudisium_mhs 
				WHERE MID(MD5(tbl_yudisium_mhs.mhs_id),8,6) = '$mhs_id'
					AND MID(MD5(tbl_yudisium_mhs.yudisium_id),8,6) = '$yudisium_id'";
		$this->db->query($sql);
	}
	
	function dekrip_yudisiumid($yudisium_id){
		$sql = "SELECT tbl_yudisium.yudisium_id FROM db_ptiik_apps.tbl_yudisium WHERE MID(MD5(tbl_yudisium.yudisium_id),8,6) = '$yudisium_id'";
		$strresult = $this -> db -> getRow($sql);
		return $strresult -> yudisium_id;
	}
	
	function save_mhs($data){
		$this-> db->replace('db_ptiik_apps`.`tbl_yudisium_mhs', $data);
	}
	
	
}
?>