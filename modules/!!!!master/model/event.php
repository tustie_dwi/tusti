<?php
class model_event extends model {
	 public $title;
     public $calendar;
     public $month;
     public $year;
     public $style;
     public $selectMonth;
     public $selectYear;
     public $nextMonth;
     public $previousMonth;
     public $yearNextMonth;
     public $yearPreviousMonth;
     public $nextMonthUrl;
     public $previousMonthUrl;
     public $storeNextLink;
     public $storePreviousLink;
     public $displayControls;
     public $startForm;
     public $closeForm;


	public function __construct() {
		parent::__construct();	
	}
	
	function get_id_dosen($str=NULL){
		$sql= "SELECT karyawan_id FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE nama like '%".$str."%' ";
		$result = $this->db->getRow( $sql );	
		
		return $result;	
	}
	
	function get_agenda_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(agenda_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_agenda WHERE left(agenda_id,6)='".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		
		$result = $dt->data;
		
		return $result;
	}
	
	function get_aktifitas_id($agenda=NULL, $karyawan=NULL){
		$sql = " SELECT aktifitas_id FROM db_ptiik_apps.tbl_aktifitas WHERE agenda_id='".$agenda."' AND karyawan_id = '".$karyawan."' ";
		$rs = $this->db->getRow( $sql );
		if($rs){
			$result = $rs->aktifitas_id;
		}else{
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '000000' , CAST(IFNULL(MAX(CAST(right(aktifitas_id,6) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_aktifitas WHERE left(aktifitas_id,6)='".date("Ym")."' "; 
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	function get_detail_event($id=NULL, $fakultas=NULL){
		$sql = "SELECT
					`coms_user`.`name`,
					mid(md5(`tbl_agenda`.agenda_id),6,6) as `id`,
					`tbl_agenda`.`agenda_id`,
					`tbl_agenda`.`lokasi`,
					`tbl_agenda`.`unit_id`,
					`tbl_agenda`.`inf_ruang`,
					`tbl_agenda`.`judul`,
					`tbl_agenda`.`english_version`,
					`tbl_agenda`.`note_english`,
					`tbl_agenda`.`inf_peserta`,
					`tbl_agenda`.`keterangan`,
					`tbl_agenda`.`inf_hari`,
					date_format(`tbl_agenda`.`tgl_mulai` ,'%Y-%m-%d %H:%i') as `tmulai`, 
					date_format(`tbl_agenda`.`tgl_selesai`,'%Y-%m-%d %H:%i') as `tselesai`, 
					`tbl_agenda`.`tgl_mulai`,
					`tbl_agenda`.`tgl_selesai`,
					`tbl_agenda`.`is_allday`,
					`tbl_agenda`.`inf_undangan`,
					`tbl_agenda`.`group_peserta`,
					`tbl_agenda`.`status_agenda`,
					`tbl_agenda`.`jenis_agenda`,
					`tbl_agenda`.`is_delete`,
					`tbl_agenda`.`icon_thumb`,
					`tbl_agenda`.`jenis_kegiatan_id`,
					`tbl_agenda`.penyelenggara,
					`tbl_agenda`.`user_id`,
					`tbl_agenda`.`last_update`,
					`tbl_jeniskegiatan`.`keterangan` as `jenis`,
					`db_ptiik_apps`.`tbl_unit_kerja`.`keterangan` as `unit_penyelenggara`
				FROM
					`db_ptiik_apps`.`tbl_agenda`
					left Join `db_ptiik_apps`.`tbl_jeniskegiatan` ON `db_ptiik_apps`.`tbl_agenda`.`jenis_kegiatan_id` = `db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`
					left Join `db_coms`.`coms_user` ON `db_ptiik_apps`.`tbl_agenda`.`user_id` = `db_coms`.`coms_user`.`id`
					Left Join `db_ptiik_apps`.`tbl_unit_kerja` ON `db_ptiik_apps`.`tbl_agenda`.`unit_id` = `db_ptiik_apps`.`tbl_unit_kerja`.`unit_id`
				WHERE 1 = 1
				";
		if($id) $sql.= "AND (mid(md5(`tbl_agenda`.agenda_id),6,6) = '".$id."' OR `db_ptiik_apps`.`tbl_agenda`.agenda_id = '".$id."') ";
		
		if($fakultas) $sql.= " AND tbl_agenda.fakultas_id='$fakultas' ";
		
		$sql.= "ORDER BY `db_ptiik_apps`.`tbl_agenda`.`last_update` DESC, date_format(`tbl_agenda`.`tgl_mulai` ,'%Y-%m-%d') DESC LIMIT 0,100";
		
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
	//	echo $sql;
		return $result;
	}
	
	/*get peserta*/
	function get_peserta_event($id=NULL, $jenis=NULL, $status=NULL, $grup=NULL){
		$sql = "SELECT
				`tbl_agenda_peserta`.`peserta_id`,
				`tbl_agenda_peserta`.`karyawan_id`,
				`tbl_agenda_peserta`.`mahasiswa_id`,
				`tbl_agenda_peserta`.`agenda_id`,
				`tbl_agenda_peserta`.`jenis_peserta`,
				`tbl_agenda_peserta`.`nama`,
				`tbl_agenda_peserta`.`instansi`,
				`tbl_agenda_peserta`.`sebagai`,
				`tbl_agenda_peserta`.`jenis_peserta` as status_peserta
				FROM
				 `db_ptiik_apps`.`tbl_agenda_peserta`
				WHERE 
					 1 = 1  
				";
		if($id){
			$sql = $sql . " AND ( mid(md5(agenda_id),6,6) = '".$id."' OR `agenda_id`='".$id."' ) ";
		}	
		
		if($jenis){
			$sql = $sql . " AND  sebagai='".$jenis."' ";
		}
		if($status){
			$sql = $sql . " AND `tbl_agenda_peserta`.`jenis_peserta`='".$status."' ";
		}
		
		if($grup){
			$sql = $sql . " AND `tbl_agenda_peserta`.`group_by`='".$grup."' ";
		}
		
		return $this->db->query( $sql );
	}
	
	function get_peserta_notif($id=NULL, $jenis=NULL, $status=NULL, $grup=NULL){
		$sql = "SELECT DISTINCT 
				`tbl_agenda_peserta`.`karyawan_id`,
				`tbl_agenda_peserta`.`agenda_id`,
				`tbl_agenda_peserta`.`notifikasi`
				FROM
				 `db_ptiik_apps`.`tbl_agenda_peserta`
				WHERE 
					 `tbl_agenda_peserta`.`notifikasi` = '0'   
				";
		if($id){
			$sql = $sql . " AND  agenda_id = '".$id."' ";
		}	
		
		if($jenis){
			$sql = $sql . " AND  jenis_peserta='".$jenis."' ";
		}
		if($status){
			$sql = $sql . " AND `tbl_agenda_peserta`.`status_peserta`='".$status."' ";
		}
		
		if($grup){
			$sql = $sql . " AND `tbl_agenda_peserta`.`group_by`='".$grup."' ";
		}
		
		return $this->db->query( $sql );
	}
	
	function get_karyawan($id=NULL){
		
		$sql = "SELECT
					db_ptiik_apps.tbl_karyawan.karyawan_id,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_karyawan.email
					FROM
					db_ptiik_apps.tbl_karyawan
					
					WHERE 1 = 1 ";
		if($id){
			$sql = $sql . "AND `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` = '".$id."' ";
		}
		
		$result = $this->db->getRow( $sql );

		return $result;	
	}
	
	function get_peserta_by_group($id=NULL, $jenis=NULL, $status=NULL){
		$sql = "SELECT DISTINCT group_by 
				FROM
				 `db_ptiik_apps`.`tbl_agenda_peserta`
				WHERE 
					 group_by<> 'nama' AND agenda_id = '".$id."' AND  jenis_peserta='".$jenis."' 
				";
		if($status){
			$sql = $sql . "AND `tbl_agenda_peserta`.`status_peserta`='".$status."' ";
		}			
		
		return $this->db->query( $sql );
	}
	
	function get_data_peserta_mhs($id=NULL){
		$sql = "SELECT mahasiswa_id, nama, angkatan FROM db_ptiik_apps.tbl_mahasiswa WHERE angkatan = '".$id."' AND is_aktif='aktif' ORDER BY nama ASC";
		
		return $this->db->query( $sql );
	}
	
	/* draw calendar */
	function drawCalendar($month, $year, $style) {		
		 if(($month == NULL) || ($year == NULL))
         {
             // Month in numbers with the leading 0
             $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
         }else{
			 $title = date('F Y',mktime(0,0,0,$month,1,$year));
		 }

		/* We need to take the month value and turn it into one without a leading 0 */
         if((substr($month, 0, 1)) == 0)
         {
             // if value is between 01 - 09, drop the 0
             $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         /* draw table */
		 //$str = '<h2>'.$title.'</h2>';
         $str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
        
         /* table headings */
         $headings = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
         $str.= '<tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr>';

         /* days and weeks vars now ... */
         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         /* row for week one */
         $str.= '<tr class="'. $style .'-row">';
    
         /* print "blank" days until the first of the current week */
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;
                 }
             }
            
                 /* add in the day number */
                 $str.= '<div class="'. $style .'-day-number">'.$list_day.'</div>';
				 
				 $data = $this->getEvent($list_day, $month, $year, $running_day);
				 
				 if(count($data)>0){
					 foreach($data as $row):
						switch (strToLower($row->jenis)){
							case 'konseling':
									$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'rapat':
									$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliah tamu':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='fa fa-star'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='fa fa-signal'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
									$note = $row->jenis;
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									$note = $row->jenis;
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
									$note = $row->jenis;
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									$note = $row->jenis;
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'uts':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-info';									
								break;
								case 'uas':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-putih';
								break;
								case 'lain-lain':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
									$tclass= 'text-putih';
								break;
								case 'olahraga dan seni':
									$icon = "<i class='fa fa-flag'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'porseni';
									$tclass= 'text-putih';
								break;
								default:
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'info';
									$tclass= 'text-putih';
								break;
							}
						  if((($running_day == "0") || ($running_day == "6")) && ((strToLower($row->jenis)=='uts')||(strToLower($row->jenis)=='uas')))
						 {
							// do nothing
						 }else{
							$str.= '<div class="alert-event alert-event-'.$sclass.'"><a href=event/viewevent/'.$row->agenda_id.'>
										<small><span class="'.$tclass.'">'.$icon.$note.'</span></small></a></div>';
						 }
							
						
					 endforeach;
				 }
                
             $str.= '</td>';
			 
			 
					 
			 
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
    
         /* finish the rest of the days in the week */
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
    
         /* final row */
         $str.= '</tr>';
    
         /* end the table */
         $str.= '</table>';
		 
		 return $str;
     }

	
	function getEvent($day,$month,$year, $runday){
		$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		$tglend	= $year."-".$month."-".$this->addNol(($day+1));
		
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
		}
		
		$sql = "SELECT mid(md5(agenda_id),5,5) as `id`, agenda_id, judul, 
					keterangan, (SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_agenda.jenis_kegiatan_id) as `jenis`,
					DATE_FORMAT(tgl_mulai,'%d') as `ds`, 
					DATE_FORMAT(tgl_selesai, '%d') as `de` 
				FROM `db_ptiik_apps`.`tbl_agenda` 
					WHERE ((tgl_mulai like '".$tgl."%' OR  tgl_selesai like '".$tgl."%')  AND inf_hari like '%".$hari."%' )
						OR ((date_format(tgl_mulai,'%Y-%m-%d') < '".$tgl."' AND date_format(tgl_selesai,'%Y-%m-%d') > '".$tgl."')  AND inf_hari like '%".$hari."%' ) ORDER BY tgl_mulai ASC";
		
		return $this->db->query( $sql );
	
	}
	
	function getEventByMonth($day,$month,$year){
		$tgl	= $year."-".$this->addNol($month);
		$tglend	= $year."-".$month."-".($day+1);
		//$sql = "select Id, keterangan,DATE_FORMAT(tgl_mulai,'%d') as `ds`, DATE_FORMAT(tgl_selesai, '%d') as `de` from `test`.`tbl_agenda` WHERE tgl_mulai between '".$tgl."' AND '".$tglend."' ORDER BY tgl_mulai ASC";
		$sql = "SELECT mid(md5(agenda_id),5,5) as `id`, agenda_id, keterangan,DATE_FORMAT(tgl_mulai,'%d') as `ds`, DATE_FORMAT(tgl_selesai, '%d') as `de` 
				FROM `db_ptiik_apps`.`tbl_agenda` WHERE tgl_mulai like '".$tgl."%' ORDER BY tgl_mulai ASC";
	
		return $this->db->query( $sql );
	
	}
	
	function get_data_peserta($id=NULL){
		$sql = "SELECT
				`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`,
				`db_ptiik_apps`.`tbl_karyawan`.`nama`
				FROM
				`db_ptiik_apps`.`tbl_karyawan`
				WHERE 1 =1 AND is_aktif IN ('aktif', 'belajar')  ";
		if($id){
			$sql = $sql . " AND `tbl_karyawan`.`is_status` = '".$id."' ";
		}
		
		
		
		return $this->db->query( $sql );
	}
	
	
	function replace_event($datanya) {
		
		return $this->db->replace('db_ptiik_apps`.`tbl_agenda',$datanya);
	}
	
	function replace_jadwal_ruang($datanya) {
		//var_dump($datanya);
		return $this->db->replace('db_ptiik_apps`.`tbl_pemakaian_ruang',$datanya);
	}
	
	function replace_detail_jadwal_ruang($datanya) {
		//var_dump($datanya);
		return $this->db->replace('db_ptiik_apps`.`tbl_pemakaian_ruang_detail',$datanya);
	}
	
	
	function update_event($datanya, $idnya) {
		
		return $this->db->update('db_ptiik_apps`.`tbl_agenda',$datanya, $idnya);
	}
	
	function replace_detail_event($datanya) {
		//var_dump($datanya);
		return $this->db->replace('db_ptiik_apps`.`tbl_agenda_detail',$datanya);
	}
	
	function replace_peserta($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_agenda_peserta',$datanya);
	}
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 15) {

			// truncate string
			$stringCut = substr($string, 0, 25);

			// make sure it ends in a word so assassinate doesn't become ass...
			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}
	function potong_kalimat ($content, $length) {
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
	function addnol($string){
		if($string < 10){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	public function delete($id) {
		$result = $this->db->delete("db_ptiik_apps`.`tbl_agenda", array('agenda_id'=>$id));
		if( !$result )
			$this->error = $this->db->getLastError();
		return $result;
	}
	
	public function delete_detail($id) {
		return $this->db->delete("db_ptiik_apps`.`tbl_agenda_detail", array('agenda_id'=>$id));
		
		//return $result;
	}
	
	public function delete_peserta($id) {
		return $this->db->delete("db_ptiik_apps`.`tbl_agenda_peserta", array('agenda_id'=>$id));
		
	}
	
	public function delete_peserta_kegiatan($id) {
		return $this->db->delete("db_ptiik_apps`.`tbl_aktifitas", array('agenda_id'=>$id));
		
	}
	
	function replace_event_staff($datanya){
		var_dump($datanya);
		$result= $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$datanya);
		var_dump($result);
		return $result;
	}
	
	function update_peserta_notif($datanya, $id){
		$result= $this->db->update('db_ptiik_apps`.`tbl_agenda_notifikasi',$datanya, $id);
		//var_dump($result);
		return $result;
	}
	
}