<?php
class model_mhs extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT mid(md5(`tbl_mahasiswa`.`mahasiswa_id`),6,6) as mhs_id,
					   tbl_mahasiswa.*,
					   `db_ptiik_apps`.`tbl_prodi`.keterangan as prodi, 
					   tbl_fakultas.keterangan as fakultas
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi`
					ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas`
					ON tbl_prodi.fakultas_id = tbl_fakultas.fakultas_id";
		if($id){
			$sql=$sql . " WHERE mid(md5(`tbl_mahasiswa`.`mahasiswa_id`),6,6)='".$id."' ";
		}
		$sql = $sql . " ORDER BY `tbl_mahasiswa`.`mahasiswa_id` ASC";
		
		// echo $sql;
		$result = $this->db->getRow( $sql );	
		return $result;
	}
	
	function angkatan($fakultas_id=NULL, $cabang=NULL){
		$sql = "SELECT DISTINCT(tbl_mahasiswa.angkatan) as angkatan
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` 
					ON `db_ptiik_apps`.`tbl_prodi`.prodi_id = `db_ptiik_apps`.`tbl_mahasiswa`.prodi_id
				WHERE `db_ptiik_apps`.`tbl_prodi`.fakultas_id = '".$fakultas_id."'
				AND `db_ptiik_apps`.`tbl_mahasiswa`.cabang_id = '".$cabang."'
				";
		$sql = $sql . "ORDER BY angkatan DESC";
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function tampilkan_index($fakultas_id=NULL,$cabang=NULL,$angkatan=NULL){
		$sql = "SELECT mid(md5(`tbl_mahasiswa`.`mahasiswa_id`),6,6) as mhs_id,
					   tbl_mahasiswa.nama,
					   tbl_mahasiswa.nim,
					   tbl_mahasiswa.is_aktif,
					   tbl_prodi.keterangan as prodi,
					   tbl_mahasiswa.foto,
						tbl_mahasiswa.alamat,
						tbl_mahasiswa.email,
						tbl_mahasiswa.hp,
						tbl_mahasiswa.telp,
						tbl_mahasiswa.jenis_kelamin,
						tbl_mahasiswa.tmp_lahir,
						tbl_mahasiswa.tgl_lahir
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` 
					ON `db_ptiik_apps`.`tbl_prodi`.prodi_id = `db_ptiik_apps`.`tbl_mahasiswa`.prodi_id
				WHERE `db_ptiik_apps`.`tbl_prodi`.fakultas_id = '".$fakultas_id."'
				AND `db_ptiik_apps`.`tbl_mahasiswa`.cabang_id = '".$cabang."'";
		if($angkatan!="*"){
			$sql .= " AND `db_ptiik_apps`.`tbl_mahasiswa`.angkatan = '".$angkatan."'";
		}

		$sql = $sql . "ORDER BY `tbl_mahasiswa`.`mahasiswa_id` ASC";
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function ceknama_mhs($nama){
		$sql = "SELECT tbl_mahasiswa.nama
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				WHERE `db_ptiik_apps`.`tbl_mahasiswa`.nama = '".$nama."'";
		$result = $this->db->getRow( $sql );
		if($result){
			return $result->nama;
		}
	}
	
	function cekemail_mhs($email){
		$sql = "SELECT tbl_mahasiswa.email
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				WHERE `db_ptiik_apps`.`tbl_mahasiswa`.email = '".$email."'";
		$result = $this->db->getRow( $sql );
		if($result){
			return $result->email;
		}
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, 
					   keterangan, extention, 
					   max_size 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		return $result;
	}
	
	function cek_nama_icon($term){
		$sql = "SELECT icon
				FROM `db_ptiik_apps`.`tbl_prodi`
				WHERE icon = '".$term."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->icon;
			return $result;
		}
	}
	
	function get_fakultas_by_prodi($prodi){
		$sql = "SELECT fakultas_id
				FROM `db_ptiik_apps`.`tbl_prodi`
				WHERE prodi_id = '".$prodi."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->fakultas_id;
			return $result;
		}
	}
	
	function replace_mhs($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_mahasiswa',$datanya);
	}

}
?>