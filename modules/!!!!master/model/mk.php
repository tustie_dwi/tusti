<?php
class model_mk extends model {
	private $coms;
	
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id){
		$sql="SELECT 
			 mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) as mkditawarkan_id,
			 `tbl_mkditawarkan`.`mkditawarkan_id` as hid_id,
			 `tbl_mkditawarkan`.tahun_akademik,
			 `tbl_mkditawarkan`.matakuliah_id,
			 `tbl_mkditawarkan`.is_blok,
			 `tbl_mkditawarkan`.is_online,
			 `tbl_mkditawarkan`.is_praktikum,
			 `tbl_mkditawarkan`.parent_id,
			 `tbl_mkditawarkan`.kuota,
			 `tbl_mkditawarkan`.icon,
			 `tbl_mkditawarkan`.parent_id,
			 `tbl_mkditawarkan`.cabang_id,
			 
             (SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan 
			  FROM 
			  `db_ptiik_apps`.`tbl_namamk`, 
			  `db_ptiik_apps`.`tbl_matakuliah`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan` as tblmkditawarkan
			  WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			  AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = tblmkditawarkan.matakuliah_id 
			  AND tblmkditawarkan.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.parent_id) as parentmk,
			
			 (SELECT `db_coms`.`coms_user`.username FROM `db_coms`.`coms_user` WHERE `db_coms`.`coms_user`.id =  `tbl_mkditawarkan`.user_id) as user_id,
			 
			 `tbl_mkditawarkan`.last_update,
			 substring(`db_ptiik_apps`.`tbl_mkditawarkan`.last_update, 1,10) as YMD,
        	 substring(`db_ptiik_apps`.`tbl_mkditawarkan`.last_update, 12,8) as waktu,
        	 
        	 (SELECT 
        	 mid(md5(`db_ptiik_apps`.`tbl_namamk`.fakultas_id),6,6) 
        	 FROM 
        	 `db_ptiik_apps`.`tbl_namamk`, 
        	 `db_ptiik_apps`.`tbl_matakuliah` 
        	 WHERE 
        	 `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
        	 AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as fakultas_id,
             
             (SELECT 
             `db_ptiik_apps`.`tbl_namamk`.keterangan 
             FROM 
             `db_ptiik_apps`.`tbl_namamk`, 
             `db_ptiik_apps`.`tbl_matakuliah`
             WHERE 
             `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
             AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk,
             
             (SELECT 
             `db_ptiik_apps`.`tbl_matakuliah`.kode_mk 
             FROM 
             `db_ptiik_apps`.`tbl_namamk`, 
             `db_ptiik_apps`.`tbl_matakuliah`
             WHERE 
             `db_ptiik_apps`.`tbl_namamk`.`namamk_id` = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
             AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as kodemk,
             
             (SELECT 
             CONCAT( `db_ptiik_apps`.`tbl_tahunakademik`.tahun,  ' ', `db_ptiik_apps`.`tbl_tahunakademik`.is_ganjil,  ' ', `db_ptiik_apps`.`tbl_tahunakademik`.is_pendek ) 
             FROM 
             `db_ptiik_apps`.`tbl_tahunakademik`
             WHERE `db_ptiik_apps`.`tbl_tahunakademik`.tahun_akademik = `db_ptiik_apps`.`tbl_mkditawarkan`.tahun_akademik) as thnakademik
			 
			 FROM `db_ptiik_apps`.`tbl_mkditawarkan`
			 WHERE 1
			 ";
		
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6)='".$id."' ";
		}
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	function read_by_parameter($thnakademik=NULL, $fakultas=NULL,$cabang=NULL)
	{
		if($thnakademik==NULL&&$fakultas==NULL&&$cabang==NULL){
			return null;
		}
		else{
		$sql ="SELECT 
			 mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) as mkditawarkan_id,
			 `tbl_mkditawarkan`.`mkditawarkan_id` as hid_id,
			 `tbl_mkditawarkan`.tahun_akademik,
			 `tbl_mkditawarkan`.matakuliah_id,
			 `tbl_mkditawarkan`.is_blok,
			 `tbl_mkditawarkan`.is_praktikum,
			 `tbl_mkditawarkan`.parent_id,
			 `tbl_mkditawarkan`.kuota,
			 `tbl_mkditawarkan`.icon,
			 `tbl_mkditawarkan`.parent_id,
			 
             (SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan 
			  FROM 
			  `db_ptiik_apps`.`tbl_namamk`, 
			  `db_ptiik_apps`.`tbl_matakuliah`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan` as tblmkditawarkan
			  WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			  AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = tblmkditawarkan.matakuliah_id 
			  AND tblmkditawarkan.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.parent_id) as parentmk, 
			 
			 (SELECT `db_coms`.`coms_user`.username FROM `db_coms`.`coms_user` WHERE `db_coms`.`coms_user`.id =  `tbl_mkditawarkan`.user_id) as user_id,
			 
			 `tbl_mkditawarkan`.last_update,
			 substring(`db_ptiik_apps`.`tbl_mkditawarkan`.last_update, 1,10) as YMD,
        	 substring(`db_ptiik_apps`.`tbl_mkditawarkan`.last_update, 12,8) as waktu,
        	 
        	 (SELECT 
        	 `db_ptiik_apps`.`tbl_namamk`.fakultas_id 
        	 FROM 
        	 `db_ptiik_apps`.`tbl_namamk`, 
        	 `db_ptiik_apps`.`tbl_matakuliah` 
        	 WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
        	 AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as fakultas_id,
             
             (SELECT 
             `db_ptiik_apps`.`tbl_namamk`.keterangan 
             FROM 
             `db_ptiik_apps`.`tbl_namamk`, 
             `db_ptiik_apps`.`tbl_matakuliah` 
             WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
             AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk,
             
             (SELECT 
             `db_ptiik_apps`.`tbl_matakuliah`.kode_mk 
             FROM 
             `db_ptiik_apps`.`tbl_namamk`, 
             `db_ptiik_apps`.`tbl_matakuliah`
             WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
             AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as kodemk,
             
             (SELECT 
             CONCAT( a.tahun,  ' ', a.is_ganjil,  ' ', a.is_pendek ) 
             FROM 
             `db_ptiik_apps`.`tbl_tahunakademik` as a 
             WHERE a.tahun_akademik = `db_ptiik_apps`.`tbl_mkditawarkan`.tahun_akademik) as thnakademik
			 
			 FROM `db_ptiik_apps`.`tbl_mkditawarkan`
			 WHERE 1
			 ";
		
		if($thnakademik!=""){
			$sql=$sql . " AND `tbl_mkditawarkan`.`tahun_akademik` LIKE '%".$thnakademik."%' ";
		}
		
		if($fakultas!=""){
			$sql=$sql . " AND (SELECT 
	        			    	mid(md5(`db_ptiik_apps`.`tbl_namamk`.fakultas_id),6,6) as fakultas_id
			        	 	   FROM 
			        	 		`db_ptiik_apps`.`tbl_namamk`, 
			        	 		`db_ptiik_apps`.`tbl_matakuliah` 
			        	 	   WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			        	 	   AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) = '".$fakultas."' ";
		}
		
		if($cabang!=""){
			$sql=$sql . " AND `tbl_mkditawarkan`.`cabang_id` = '".$cabang."' ";
		}
		
		$sql=$sql . " ORDER BY `tbl_mkditawarkan`.`last_update` DESC ";
		
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
		}
	}
	
	function get_dosen_pengampu($term=NULL){
		$sql="SELECT 
			  (SELECT `db_ptiik_apps`.`tbl_karyawan`.nama FROM `db_ptiik_apps`.`tbl_karyawan` WHERE `db_ptiik_apps`.`tbl_karyawan`.karyawan_id = `db_ptiik_apps`.`tbl_pengampu`.karyawan_id) as nama,
			  `db_ptiik_apps`.`tbl_pengampu`.is_koordinator,
			  `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`			  
			  FROM 
			  `db_ptiik_apps`.`tbl_pengampu`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan`
			  
			  WHERE mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`),6,6)='".$term."'
			  AND `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id
			"; 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_all_mk_from_mkditawarkan(){
		$sql = "SELECT c.keterangan, a.mkditawarkan_id as mkid
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` as a, `db_ptiik_apps`.`tbl_matakuliah` as b, `db_ptiik_apps`.`tbl_namamk` as c
				WHERE a.matakuliah_id = b.matakuliah_id
                AND c.namamk_id = b.namamk_id
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_all_thn_akademik(){
		$sql = "SELECT tahun_akademik, CONCAT(tahun , ' ' , is_ganjil , ' ' , is_pendek) as thnakademik FROM `db_ptiik_apps`.`tbl_tahunakademik` ORDER BY tahun_akademik DESC
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_mkid_from_namamk($term){
		$sql="SELECT a.matakuliah_id
			FROM `db_ptiik_apps`.`tbl_matakuliah` as a, `db_ptiik_apps`.`tbl_namamk` as b
			WHERE b.keterangan = '".$term."' 
			AND a.namamk_id = b.namamk_id"; 
	$dt = $this->db->getRow( $sql );
	if(isset($dt)){
	$result = $dt->matakuliah_id;
	
	return $result;}
	}
	
	function get_namamk_id_from_namamk($term){
		$sql="SELECT namamk_id
			FROM `db_ptiik_apps`.`tbl_namamk`
			WHERE keterangan = '".$term."' 
			"; 
	$dt = $this->db->getRow( $sql );
	if(isset($dt)){
	$result = $dt->namamk_id;
	
	return $result;}
	}
	
	function update_koor($pengampuid, $iskoor){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_pengampu`
				SET `is_koordinator`= '".$iskoor."'
				WHERE `pengampu_id`= '".$pengampuid."'
			   ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_pengampu($id){
		$sql="DELETE FROM `db_ptiik_apps`.`tbl_pengampu` 
			  WHERE `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` = '".$id."'
			 "; 
		return($this->db->query( $sql ));
	}
	
	function get_mkditawarkan_data($mkditawarkan){
		$sql="SELECT *
			  FROM `db_ptiik_apps`.`tbl_mkditawarkan`
			  WHERE mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) = '".$mkditawarkan."'
			 "; 
		return $this->db->getRow( $sql );

	}
	
	function delete_mkditawarkan($mkditawarkan,$thnakademik){
		$sql_krs="DELETE FROM `db_ptiik_apps`.`tbl_krs` 
				  WHERE `db_ptiik_apps`.`tbl_krs`.`mkditawarkan_id` = '".$mkditawarkan."'
				  AND `db_ptiik_apps`.`tbl_krs`.`inf_semester` = '".$thnakademik."'
				 "; 
		$this->db->query( $sql_krs );
		
		$sql_jadwal="DELETE FROM `db_ptiik_apps`.`tbl_jadwalmk` 
				  WHERE `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` = '".$mkditawarkan."'
				 ";
		$this->db->query( $sql_jadwal );
			
		$sql_pengampu="DELETE FROM `db_ptiik_apps`.`tbl_pengampu` 
				  WHERE `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = '".$mkditawarkan."'
				 "; 
		$this->db->query( $sql_pengampu );
		
		$sql_mk="DELETE FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
				  WHERE `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` = '".$mkditawarkan."'
				 "; 
		return($this->db->query( $sql_mk ));
	}
	
	function get_namamk_reg_number($term=NULL){
		$sql_ = "SELECT `tbl_namamk`.`namamk_id`
				FROM `db_ptiik_apps`.`tbl_namamk`
				WHERE `tbl_namamk`.`keterangan` = '".$term."'
				";
		$id = $this->db->getrow( $sql_ );
		if($id){
			$strresult=$id->namamk_id;
		}else{
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(namamk_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_namamk"; 
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		}
		return $strresult;
	}
	
	function get_matakuliah_reg_number(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(matakuliah_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_matakuliah"; 
	$dt = $this->db->getRow( $sql );
	
	$strresult = $dt->data;
	
	return $strresult;
	}
	
	function get_mkditawarkan_reg_number(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(mkditawarkan_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_mkditawarkan"; 
		$dt = $this->db->getRow( $sql );
		
		$result = $dt->data;
		return $result;
	}
	
	function get_fakultas_id($id=NULL){
		$sql = "SELECT fakultas_id
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE mid(md5(fakultas_id),6,6) = '".$id."'
				";		
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->fakultas_id;}
	}
	
	function get_mk_from_mkditawarkan($id){
		$sql = "SELECT 
				`db_ptiik_apps`.`tbl_namamk`.keterangan,  
				`db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id as mkid
				
				FROM 
				`db_ptiik_apps`.`tbl_mkditawarkan`, 
				`db_ptiik_apps`.`tbl_matakuliah`, 
				`db_ptiik_apps`.`tbl_namamk`
				WHERE  
				`db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id = `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id
                AND `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id
				AND `db_ptiik_apps`.`tbl_namamk`.fakultas_id = '".$id."'
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_status($term){
		$sql = "SELECT karyawan_id
				FROM `db_ptiik_apps`.tbl_karyawan as a
				WHERE a.nama = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		//echo $sql;
		if(isset($id)){
		return $result = $id->karyawan_id;}
	}
	
	function get_pengampu_reg_number($mkid=NULL, $staff=NULL){
	 	$sql = "SELECT pengampu_id FROM db_ptiik_apps.tbl_pengampu WHERE mkditawarkan_id ='".$mkid."' AND karyawan_id='".$staff."'  ";
	 	$result = $this->db->getRow( $sql );	
 		if($result){			
			$strresult = $result->pengampu_id;
		}else{
			$kode = $mkid;
			$sql = "SELECT concat('".$kode."',RIGHT(concat( '00' , CAST(IFNULL(MAX(CAST(right(pengampu_id,2) AS unsigned)), 0) + 1 AS unsigned)),2)) as `data` 
					FROM db_ptiik_apps.tbl_pengampu WHERE mkditawarkan_id ='".$mkid."'";
			// $dt = $this->db->getRow( $sql );				
			// $strresult = $dt->data;
			
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		 }
		 
		 // echo $sql;
		return $strresult;
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, keterangan, extention, max_size FROM `db_ptiik_apps`.`tbl_jenisfile` WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		//echo $sql;
		if(isset($result)){
		return $result;
		}

	}
	
	function replace_namamk($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_namamk',$datanya);
	}
	
	function replace_matakuliah($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_matakuliah',$datanya);
	}
	
	function replace_mkditawarkan($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_mkditawarkan',$datanya);
	}
	
	function replace_pengampu($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_pengampu',$datanya);
	}
	
	
	function read_matakuliah($id,$fakultas){
		$sql = "SELECT mid(md5(`db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`),6,6) as matakuliah_id,
				`tbl_matakuliah`.`matakuliah_id` as hid_id,
				`tbl_matakuliah`.`namamk_id`,
				`tbl_matakuliah`.`kode_mk`,
				`tbl_matakuliah`.`kurikulum`,
                `tbl_matakuliah`.`sks`,
                `tbl_matakuliah`.`strata`,
				
                `tbl_namamk`.`keterangan` as namamk,
                `tbl_namamk`.`english_version` as engver,
                
                mid(md5(`db_ptiik_apps`.`tbl_fakultas`.fakultas_id),6,6) as fakultas_id,
                `tbl_fakultas`.keterangan as fakultas
                                
				FROM `db_ptiik_apps`.`tbl_matakuliah`
                LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` ON `tbl_fakultas`.`fakultas_id` = `tbl_namamk`.`fakultas_id`
				WHERE 1
				";

		if($id!=""){
			$sql=$sql . " AND mid(md5(`db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`),6,6) = '".$id."' ";
		}
		
		if($fakultas){
			$sql=$sql . " AND `tbl_fakultas`.fakultas_id = '".$fakultas."' ";
		}
		
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	function read_namamk($id=NULL){
		$sql = "SELECT mid(md5(`db_ptiik_apps`.`tbl_namamk`.`namamk_id`),6,6) as namamk_id,
				`tbl_namamk`.`namamk_id` as hid_id,
				mid(md5(fakultas_id),6,6) as fakultas_id,
				`tbl_namamk`.`keterangan`,
				`tbl_namamk`.`english_version`,
                (select `db_ptiik_apps`.`tbl_fakultas`.keterangan 
                from `db_ptiik_apps`.`tbl_fakultas` 
                where `db_ptiik_apps`.`tbl_fakultas`.fakultas_id = `db_ptiik_apps`.`tbl_namamk`.`fakultas_id`) as fakultas
                
				FROM `db_ptiik_apps`.`tbl_namamk`
				
				WHERE 1
				";

		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_namamk`.`namamk_id`),6,6)='".$id."' ";
		}
		
		//echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_reg_numbermk(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(matakuliah_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_matakuliah"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function cek_mk_from_namamk($term=NULL){
		$sql = "SELECT `tbl_namamk`.`namamk_id`
				FROM `db_ptiik_apps`.`tbl_namamk`
				WHERE `tbl_namamk`.`keterangan` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->namamk_id;}
	}
	
	function cek_kode_mk_by_mkid($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`kode_mk`
				FROM `db_ptiik_apps`. `tbl_matakuliah`
				WHERE `tbl_matakuliah`.`matakuliah_id` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->kode_mk;}
	}
	
	function cek_kode_mk($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`kode_mk`
				FROM `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_matakuliah`.`kode_mk` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->kode_mk;
		}
	}
	
	function cek_nama_mk($term=NULL, $strata=NULL){
		$sql = "SELECT `tbl_namamk`.`namamk_id`
				FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
                AND `tbl_namamk`.`keterangan` = '".$term."' AND `tbl_matakuliah`.strata='$strata' 
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->namamk_id;}
	}
	
	function cek_mata_kuliah_id($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`matakuliah_id`
				FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
                AND `tbl_namamk`.`keterangan` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->matakuliah_id;}
	}
	
	function cek_kurikulum_mk($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`kurikulum`
				FROM `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_matakuliah`.`kurikulum` =  '".$term."'
				";
		if($this->db->getrow( $sql )){
			$id = $this->db->getrow( $sql );
			return $result = $id->kurikulum;
		}
		
	}
	
}
?>