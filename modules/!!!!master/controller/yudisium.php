<?php
class master_yudisium extends comsmodule {
	
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}

	function index(){
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$myudisium = new model_yudisium();
		$data['posts'] = $myudisium->get_yudisium();
		$this->view('yudisium/index.php', $data );
	}
	
	function add(){
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		
		$myudisium = new model_yudisium();
		$data['tahun'] = $myudisium->get_tahunakademik();
		$data['ruang'] = $myudisium->get_ruang($this->config->cabang);
		$data['mahasiswa'] = $myudisium->get_mahasiswa();
		$this->view('yudisium/add.php', $data );
	}
	
	function save(){
		$myudisium = new model_yudisium();
		
		$yudisium_id = $myudisium->get_yudisium_id();
		$data = array(
			'yudisium_id' => $yudisium_id,
			'tahun_akademik' => $_POST['tahun_akademik'],
			'periode' => $_POST['periode'],
			'judul' => $_POST['judul'],
			'keterangan' => $_POST['keterangan'],
			'tgl_yudisium' => $_POST['tgl_yudisium'],
			'ruang_id' => $_POST['ruang'],
			'last_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->coms->authenticatedUser->id
		);
		$myudisium->save_yudisium($data);
		
		if($_POST['mhs']){
			foreach($_POST['mhs'] as $mhs){
				$data = array(
					'mhs_id' => $mhs,
					'yudisium_id' => $yudisium_id,
					'mahasiswa_id' => $mhs
				);	
				
				$myudisium->save_mhs($data);
			}
		}
		$this->redirect('module/master/yudisium');
	}
	
	function edit($yudisium_id=NULL){
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		
		$myudisium = new model_yudisium();
		$data['tahun'] = $myudisium->get_tahunakademik();
		$data['ruang'] = $myudisium->get_ruang($this->config->cabang);
		$data['yudisium'] = $myudisium->get_yudisium_detail($yudisium_id);
		
		$this->view('yudisium/edit.php', $data );
	}
	
	function editdata(){
		$myudisium = new model_yudisium();
		$data = array(
			'tahun_akademik' => $_POST['tahun_akademik'],
			'periode' => $_POST['periode'],
			'judul' => $_POST['judul'],
			'keterangan' => $_POST['keterangan'],
			'tgl_yudisium' => $_POST['tgl_yudisium'],
			'ruang_id' => $_POST['ruang'],
			'last_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->coms->authenticatedUser->id
		);
		
		$where = array(
			'MID(MD5(tbl_yudisium.yudisium_id),8,6)' => $_POST['yudisium_id']
		);
		
		$myudisium->edit_yudisium($data, $where);
		$this->redirect('module/master/yudisium');
	}

	function del($yudisium_id=NULL){
		$myudisium = new model_yudisium();
		$myudisium->del_yudisium($yudisium_id);
		$this->redirect('module/master/yudisium');
	}
	
	//detail
	function detail($yudisium_id=NULL){
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		
		$myudisium = new model_yudisium();
		$data['tahun'] = $myudisium->get_tahunakademik();
		$data['ruang'] = $myudisium->get_ruang($this->config->cabang);
		$data['yudisium'] = $myudisium->get_yudisium_detail($yudisium_id);
		$data['mahasiswa'] = $myudisium->get_mahasiswa();
		$data['yudisium_id'] = $yudisium_id;
		$data['yudisium_mhs'] = $myudisium->get_yudisium_mhs($yudisium_id);
		
		$this->view('yudisium/detail.php', $data );
	}
	
	function add_mhs(){
		if($_POST['mhs_id'] == "-") $this->redirect('module/master/yudisium/detail/'.$_POST['yudisium_id']);
		else{
			$myudisium = new model_yudisium();
			
			$data = array(
				'mhs_id' => $_POST['mhs_id'],
				'yudisium_id' => $myudisium->dekrip_yudisiumid($_POST['yudisium_id']),
				'mahasiswa_id' => $_POST['mhs_id']
			);
			$myudisium->save_mhs($data);
			$this->redirect('module/master/yudisium/detail/'.$_POST['yudisium_id']);
		}
	}
	
	function del_mhs($mhs_id, $yudisium_id){
		$myudisium = new model_yudisium();
		$myudisium->del_mhs($mhs_id, $yudisium_id);
		$this->redirect('module/master/yudisium/detail/'.$yudisium_id);
	}
	
	
	
	
	
	
}
?>