<?php
class master_report extends comsmodule {
	private $coms;
	
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function keuangan($str=NULL){
		if($str=="hr"):
			$mconf = new model_honor();		
			
						
			$data['periode'] = $mconf->get_periode_bayar();
			
			
			
				//$data['posts'] = $mconf->get_rekap_absen_bayar("", "", "", $kode);
			
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			
			$this->add_script('js/keuangan/jsall.js');	
	
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			
			$this->view( 'keuangan/hr/rekap-dosen.php', $data );
				
		endif;
	}
	
	function view_hr(){
		if(isset($_POST['kode'])):
			$kode = $_POST['kode'];
			
			$data['karyawanid'] = $this->coms->authenticatedUser->staffid;
			$data['role']		= $this->coms->authenticatedUser->role;
			
			$data['kode'] 	= $kode;
			
			$mconf = new model_honor();		
						
			if($data['role']!="administrator"):
				$data['posts'] = $mconf->get_rekap_absen_bayar("", "", "", $kode, "", $data['karyawanid']);
			else:
				$data['posts'] = $mconf->get_rekap_absen_bayar("", "", "", $kode);
			endif;
			
			$this->view( 'keuangan/hr/view-rekap-dosen.php', $data );
		endif;
	}
	
	function akademik($str=NULL,$val=NULL){
		if($str=="absen"){
			if($val=="asisten"){
				$mabsen	= new model_absen();
				$data['type']		= "mahasiswa";
				$data['role']		= $this->coms->authenticatedUser->role;
				$data['mhsid']		= $this->coms->authenticatedUser->mhsid;
				$data['tahunAkademik'] = $mabsen->get_semester();
				$data['prodi'] 		= $mabsen->get_prodi();
				$data['mhs'] 		= $mabsen->get_nama("","","","",0);
				
				$this->add_script('js/absen/absen.js');
			}
			else{
					if(($this->coms->authenticatedUser->role =='mahasiswa')|| ($this->coms->authenticatedUser->role =='asisten')) $this->coms->no_privileges();
					
				$mabsen	= new model_absen();
				$data['type']		= "dosen";
				$data['role']		= $this->coms->authenticatedUser->role;
				$data['karyawanid'] = $this->coms->authenticatedUser->staffid;
				$data['tahunAkademik'] = $mabsen->get_semester();
				$data['prodi'] 		= $mabsen->get_prodi();
				$data['dosen'] 		= $mabsen->get_nama("","","","",1);
				
				$this->add_script('js/absen/absen.js');
			}
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_script('datepicker/js/moment-2.4.0.js');
			$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
			$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.css');
			$this->coms->add_script('js/select2/select2.js');
			$this->coms->add_style('js/select2/select2.css');
			
			$this->view( 'absen/index.php', $data );
		}
	}
	
	function detail(){
		$mabsen	= new model_absen();
		
		if(isset($_POST['thnid']) && $_POST['thnid']!='0' && $_POST['thnid']!= 'undefined'){
			$thnid 	 = $_POST['thnid'];
		}else $thnid = NULL;
		
		if(isset($_POST['prodi']) && $_POST['prodi']!='0' && $_POST['prodi']!= 'undefined'){
			$prodiid   = $_POST['prodi'];
		}else $prodiid = NULL;
		
		if(isset($_POST['mk']) && $_POST['mk']!='0' && $_POST['mk']!= 'undefined'){
			$mk   = $_POST['mk'];
		}else $mk = NULL;
		
		if(isset($_POST['kelas']) && $_POST['kelas']!='0' && $_POST['kelas']!= 'undefined'){
			$kelas 	 = $_POST['kelas'];
		}else $kelas = NULL;
		
		if(isset($_POST['data']) && $_POST['data']!='0' && $_POST['data']!= 'undefined'){
			$dosen_mhs 	 = $_POST['data'];
		}else $dosen_mhs = NULL;
		
		if(isset($_POST['datemulai']) && $_POST['datemulai']!='' && $_POST['datemulai']!= 'undefined'){
			$datemulai 	 = $_POST['datemulai'];
		}else $datemulai = NULL;
		
		if(isset($_POST['dateselesai']) && $_POST['dateselesai']!='' && $_POST['dateselesai']!= 'undefined'){
			$dateselesai   = $_POST['dateselesai'];
		}else $dateselesai = NULL;
		
		if(isset($_POST['type']) && $_POST['type']!='0' && $_POST['type']!= 'undefined'){
			$type 	 = $_POST['type'];
		}else $type = NULL;
		
		//echo $type;
		
		$data['thnid'] 	 	 = $thnid;
		$data['prodiid']	 = $prodiid;
		$data['mk'] 		 = $mk;
		$data['kelas'] 		 = $kelas;
		$data['dosen_mhs'] 	 = $dosen_mhs;
		$data['datemulai']   = $datemulai;
		$data['dateselesai'] = $dateselesai;
		$data['type'] 	 	 = $type;
		
		if(isset($_POST['view'])){
			$view = $_POST['view'];
		}else $view = 'view';
		$data['view_']		 = $view;
		// $data['absen'] = $mabsen->rekap_absen($thnid, $prodiid, $mk, $kelas, $dosen, $datemulai, $dateselesai);
		// if(!$prodiid && !$mk && !$kelas && !$dosen){
			$data['prodi'] 	 = $mabsen->get_prodi();
		// }
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.css');
		$this->coms->add_script('js/select2/select2.js');
		$this->coms->add_style('js/select2/select2.css');
		$this->add_script('js/absen/absen.js');
		
		if($view=='cetak')	$this->view( 'absen/report-absen.php', $data );	 
		else $this->view( 'absen/detail.php', $data );
	}

	function save_absen(){
		$mabsen	= new model_absen();
		
		if(isset($_POST['hidId_absen'])&&$_POST['hidId_absen']!=""){
			$absenid = $_POST['hidId_absen'];
		}else $absenid = $mabsen->absen_id();
		
		if(isset($_POST['hidId_absendosen'])&&$_POST['hidId_absendosen']!=""){
			$absen_dosenid = $_POST['hidId_absendosen'];
		}else $absen_dosenid = $mabsen->absen_dosenid();
		
		$jadwal		= $_POST['jadwal-mk'];
		$tgl		= $_POST['tgl_pertemuan'];
		$jam_mulai	= $_POST['jam_mulai'];
		$jam_selesai= $_POST['jam_selesai'];
		$time1 		= strtotime($jam_mulai);
		$time2 		= strtotime($jam_selesai);
		$jumlah_jam = (($time2 - $time1)/60);
		$materi		= $_POST['materi'];
		$total	 	= $_POST['total'];
		$sesi		= $_POST['sesi'];
		$hadir		= 1;
		$dosenid 	= $_POST['dosen'];
		$last_update= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->username;
		
		$data_absen = array(
							'absen_id'=>$absenid,
							'jadwal_id'=>$jadwal,
							'total_pertemuan'=>$total,
							'tgl'=>$tgl,
							'jam_masuk'=>$jam_mulai,
							'jam_selesai'=>$jam_selesai,
							'jumlah_jam'=>$jumlah_jam,
							'materi'=>$materi,
							'sesi_ke'=>$sesi,
							'praktikum'=>0,
							'last_update'=>$last_update,
							'user'=>$user
						   );
		$save_absen = $mabsen->replace_absen($data_absen);
	
		$data_absendosen = array(
							'absendosen_id'=>$absen_dosenid,
							'pengampu_id'=>$dosenid,
							'absen_id'=>$absenid,
							'is_hadir'=>1,
							'last_update'=>$last_update,
							'user'=>$user
						   );
		if($save_absen){
			$save_absendosen = $mabsen->replace_absen_dosen($data_absendosen);
			if($save_absendosen){
				echo "Berhasil";
				exit();
			}
			else{
				echo "Gagal";
				exit();
			}
		}
	}

	function hapus_absen(){
		$mabsen		= new model_absen();
		$absen_id 	= $_POST['absen'];
		$hapus		= $mabsen->delete_absen($absen_id);
		if($hapus){
			echo "Hapus data berhasil!";
		}
		else echo "Hapus data gagal!";
	}
	
	function save_absen_asisten(){
		$mabsen	= new model_absen();
		
		if(isset($_POST['hidId_absen'])&&$_POST['hidId_absen']!=""){
			$absenid = $_POST['hidId_absen'];
		}else $absenid = $mabsen->absen_id();
		
		if(isset($_POST['hidId_asistenabsen'])&&$_POST['hidId_asistenabsen']!=""){
			$hidId_asistenabsen = $_POST['hidId_asistenabsen'];
		}else $hidId_asistenabsen = $mabsen->absen_asistenid();
		
		$jadwal		= $_POST['jadwal-mk'];
		$tgl		= $_POST['tgl_pertemuan'];
		$jam_mulai	= $_POST['jam_mulai'];
		$jam_selesai= $_POST['jam_selesai'];
		$time1 		= strtotime($jam_mulai);
		$time2 		= strtotime($jam_selesai);
		$jumlah_jam = (($time2 - $time1)/60);
		$materi		= $_POST['materi'];
		$total	 	= $_POST['total'];
		$sesi		= $_POST['sesi'];
		$hadir		= 1;
		$asistenid 	= $_POST['asisten'];
		$kelas		= $_POST['kelas'];
		$kelompok	= $_POST['kelompok'];
		$last_update= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->username;
		
		$data_absen = array(
							'absen_id'=>$absenid,
							'jadwal_id'=>$jadwal,
							'total_pertemuan'=>$total,
							'tgl'=>$tgl,
							'jam_masuk'=>$jam_mulai,
							'jam_selesai'=>$jam_selesai,
							'jumlah_jam'=>$jumlah_jam,
							'materi'=>$materi,
							'sesi_ke'=>$sesi,
							'praktikum'=>1,
							'kelompok'=>$kelompok,
							'last_update'=>$last_update,
							'user'=>$user
						   );
						    //var_dump($data_absen);
		$save_absen = $mabsen->replace_absen($data_absen);
		
		$data_absenasisten = array(
									'jadwal_asisten_id'=>$hidId_asistenabsen,
									'praktikum_id'=>"",
									'absen_id'=>$absenid,
									'asisten_id'=>$asistenid,
									'kelas'=>$kelas,
									'is_hadir'=>1,
									'last_update'=>$last_update,
									'user_id'=>$user
								   );
								   //var_dump($data_absenasisten);
		if($save_absen){
			$save_absenasisten = $mabsen->replace_absen_asisten($data_absenasisten);
			if($save_absenasisten){
				echo "Berhasil";
				exit();
			}
			else{
				echo "Gagal";
				exit();
			}
		}
		else{
			echo "Gagal";
			exit();
		}
	}

	function hapus_absen_asisten(){
		$mabsen		= new model_absen();
		$absen_id 	= $_POST['absen'];
		$hapus		= $mabsen->delete_absen_asisten($absen_id);
		if($hapus){
			echo "Hapus data berhasil!";
		}
		else echo "Hapus data gagal!";
	}

//======================fungsi tambahan==================//
	function get_rekap($thnid, $prodiid, $mk, $kelas, $dosenid, $datemulai, $dateselesai, $view_){
		$data['level']		= $this->coms->authenticatedUser->level;
		
		$mabsen	= new model_absen();
		
		$data['mconf'] = $mabsen;
		
		$data['no']	   = 1;
		if(($prodiid=='-')) $prodiid="";
		if(($mk=='-')) $mk="";
		if(($dosenid=='-')) $dosenid="";
		if(($kelas=='-')) $kelas="";
		
		$data['absen'] = $mabsen->rekap_absen($thnid, $prodiid, $mk, $kelas, $dosenid, $datemulai, $dateselesai, 'bymk');
		
		$data['thnid'] 		= $thnid;
		$data['thn_aktif']	= $mabsen->get_semester('aktif')->tahun_akademik;
		$data['prodiid'] 	= $prodiid;
		$data['mk'] 		= $mk;
		$data['kelas'] 		= $kelas;
		$data['dosenid'] 	= $dosenid;
		$data['datemulai'] 	= $datemulai;
		$data['dateselesai'] = $dateselesai;
		$data['view_']		 = $view_;
		if($view_!='view'){
			$data['nama_prodi'] = $mabsen->get_prodi($prodiid); //will group by prodi for print
		}else{
			$data['nama_prodi'] = '';
		}
		
		$data['tahunAkademik'] = $mabsen->get_semester();
		$data['prodi']	= $mabsen->get_prodi();
		$data['dosen']	= $mabsen->get_nama("","","","",1);
		$thn_aktif		= $mabsen->get_semester('aktif');
		// $data['jadwal']	= $mabsen->get_jadwal($thn_aktif->tahun_akademik, $dosen);
		if($view_!='cetak')	$this->view( 'absen/rekap.php', $data );
		else $this->view( 'absen/rekap-print.php', $data );
	}

	function get_rekap_asisten($thnid, $prodiid, $mk, $kelas, $mhsid, $datemulai, $dateselesai, $view_){
		$data['level']		= $this->coms->authenticatedUser->level;
		
		$mabsen	= new model_absen();
		
		$data['mconf'] = $mabsen;
		
		$data['no']	   = 1;
		if(($prodiid=='-')) $prodiid="";
		if(($mk=='-')) $mk="";
		if(($mhsid=='-')) $mhsid="";
		if(($kelas=='-')) $kelas="";
		
		$data['absen'] = $mabsen->rekap_absen_asisten($thnid, $prodiid, $mk, $kelas, $mhsid, $datemulai, $dateselesai);
		
		$data['thnid'] 		= $thnid;
		$data['thn_aktif']	= $mabsen->get_semester('aktif')->tahun_akademik;
		$data['prodiid'] 	= $prodiid;
		$data['mk'] 		= $mk;
		$data['kelas'] 		= $kelas;
		$data['mhsid'] 		= $mhsid;
		$data['datemulai'] 	= $datemulai;
		$data['dateselesai'] = $dateselesai;
		$data['view_']		 = $view_;
		$data['kelompok']	= $mabsen->kelompok();
		if($view_!='view'){
			$data['nama_prodi'] = $mabsen->get_prodi($prodiid); //will group by prodi for print
		}else{
			$data['nama_prodi'] = '';
		}
		
		$data['tahunAkademik'] = $mabsen->get_semester();
		$data['prodi']	= $mabsen->get_prodi();
		$data['mhs'] 		= $mabsen->get_nama("","","","",0);
		$thn_aktif		= $mabsen->get_semester('aktif');
		// $data['jadwal']	= $mabsen->get_jadwal($thn_aktif->tahun_akademik, $dosen);
		if($view_!='cetak')	$this->view( 'absen/rekap.php', $data );
		else $this->view( 'absen/rekap-print.php', $data );
		//var_dump($data['absen']);
	}
	
	function get_mk(){
		$mabsen		= new model_absen();	
		$thn 		= $_POST['tahunId'];
		$id		 	= $_POST['id'];
		
		if($_POST['tipe'] == 'dosen'){
			$data['level']		= $this->coms->authenticatedUser->level;
			if($data['level']!=1){
				$mk = $mabsen->get_mk($thn,1,$id);
			}
			else{
				$mk = $mabsen->get_mk($thn,1);
			}
			
		}
		else{
			$mk = $mabsen->get_mk($thn,0,$id);
		}
		
		if($mk) :
			echo "<option value='0'>Pilih Matakuliah</option>";
			foreach($mk as $key){
				$isi = $key->keterangan. ' - ' . $key->kelas . ' (' . $key->prodi_id . ")";
				echo "<option data-prodi='".$key->prodi_id."' value='".$isi. "|" .$key->kelas. "|" .$key->prodi_id. "|" .$key->mkditawarkan_id. "|" . $thn ."'>".$isi ."</option>";
			}
		else :
			echo "<option value=''>Matakuliah tidak ditemukan</option>";
		endif;
	}
	
	function get_jadwal(){
		$mabsen			= new model_absen();
		$thn_aktif		= $mabsen->get_semester('aktif');
		$dosen 			= $_POST['karyawan'];
		$jadwal			= $mabsen->get_jadwal($thn_aktif->tahun_akademik, $dosen);
		if($jadwal){
			echo '<option value="0">Silahkan Pilih</option>';
			foreach($jadwal as $j){
				echo "<option value='".$j->jadwal_id."' start='".$j->jam_mulai."' finish='".$j->jam_selesai."'";
				echo "><strong>[".$j->kode_mk."]</strong> ".$j->nama_mk." - ".$j->kelas." [".$j->prodi."]</option>";
			}
		}
	}
	
	function get_jadwal_asisten(){
		$mabsen			= new model_absen();
		$thn_aktif		= $mabsen->get_semester('aktif')->tahun_akademik;
		$mhs 			= $_POST['mhs'];
		$asisten		= $_POST['asisten'];
		$jadwal			= $mabsen->get_jadwal_asisten($thn_aktif, $asisten);
		if($jadwal){
			echo '<option value="0">Silahkan Pilih</option>';
			foreach($jadwal as $j){
				echo "<option value='".$j->jadwal_id."' start='".$j->jam_mulai."' finish='".$j->jam_selesai."' kelas='".$j->kelas."' ";
				echo "><strong>[".$j->kode_mk."]</strong> ".$j->nama_mk." - ".$j->kelas." [".$j->prodi."]</option>";
			}
		}
	}
	
	function get_dosen(){
		$mabsen	= new model_absen();		
		$role		= $this->coms->authenticatedUser->role;
		$karyawanid = $this->coms->authenticatedUser->staffid;
		$mhsid		= $this->coms->authenticatedUser->mhsid;
		if($_POST['tipe'] == 'dosen'){
			// if($_POST['periodeId']==''){
				// $tahun = $_POST['tahunId'];
				// $dosen = $mabsen->get_dosen('','','',$tahun,1);
			// }else{
			$tahun = explode("|", $_POST['periodeId']);
			$dosen = $mabsen->get_nama($tahun[1], $tahun[2],$tahun[3],$tahun[4]);
			// }
			
			if($dosen) :
				echo "<option value=''>Semua Dosen</option>";
				foreach($dosen as $key){
					if($key->karyawan_id==$karyawanid || $role=="admin"){
						$isi = $key->nama. ' - ' . $key->nik;
						echo "<option value='".$key->karyawan_id."'>".$isi ."</option>";
					}
				}
			else :
				echo "<option value='all'>Dosen tidak ditemukan</option>";
			endif;
		}
		else{
			$tahun = explode("|", $_POST['periodeId']);
			$mhs = $mabsen->get_nama($tahun[1], $tahun[2],$tahun[3],$tahun[4],0);
			// }
			
			if($mhs) :
				//var_dump($mhs);
				echo "<option value=''>Semua Mahasiswa</option>";
				foreach($mhs as $key){
					if($key->mahasiswa_id==$mhsid || $role=="admin"){
						$isi = $key->nama. ' - ' . $key->nim;
						echo "<option value='".$key->mahasiswa_id."'>".$isi ."</option>";
					}
				}
			else :
				echo "<option value='all'>Dosen tidak ditemukan</option>";
			endif;
		}
	}

	function cetak_rekap($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_){
		$mabsen	= new model_absen();
		
		$data['mconf'] = $mabsen;
		
		$data['no']	   = 1;
		
		$data['thnid'] 		= $thnid;
		$data['prodiid'] 	= $prodiid;
		$data['mk'] 		= $mk;
		$data['kelas'] 		= $kelas;
		$data['dosenid'] 	= $dosen_mhs;
		$data['datemulai'] 	= $datemulai;
		$data['dateselesai'] = $dateselesai;
		$data['view_']		 = $view_;
		
		$data['absen'] = $mabsen->rekap_absen($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, 'bymk');
	
		$this->view( 'absen/cetak-rekap.php', $data );
	}
	
	function cetak_rekap_asisten($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_){
		$mabsen	= new model_absen();
		
		$data['mconf'] = $mabsen;
		
		$data['no']	   = 1;
		
		$data['thnid'] 		= $thnid;
		$data['prodiid'] 	= $prodiid;
		$data['mk'] 		= $mk;
		$data['kelas'] 		= $kelas;
		$data['mhsid'] 		= $dosen_mhs;
		$data['datemulai'] 	= $datemulai;
		$data['dateselesai'] = $dateselesai;
		$data['view_']		 = $view_;
		
		$data['absen'] = $mabsen->rekap_absen_asisten($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai);
	
		$this->view( 'absen/cetak-rekap.php', $data );
	}
	
	function get_absen(){
		$mabsen	= new model_absen();
		$absen 	= $_POST['absen'];
		$data	= $mabsen->rekap_absen("", "", "", "", "", "", "", "", "", $absen);
		
		$return_arr = array();
		if($data){
			foreach($data as $row => $value){
				$arr[$row] = $value;
			}
			array_push($return_arr,$arr);
			
			$json_response = json_encode($return_arr);
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}
	
	function get_absen_asisten(){
		$mabsen	= new model_absen();
		$absen 	= $_POST['absen'];
		$data	= $mabsen->rekap_absen_asisten("", "", "", "", "", "", "", "", $absen);
		
		$return_arr = array();
		if($data){
			foreach($data as $row => $value){
				$arr[$row] = $value;
			}
			array_push($return_arr,$arr);
			
			$json_response = json_encode($return_arr);
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}
}
?>