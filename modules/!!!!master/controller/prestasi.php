<?php
class master_prestasi extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mprestasi = new model_prestasi();
		
		$data['posts'] = $mprestasi->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->view("prestasi/index.php", $data);
	}
	
	function write($id = NULL){
		$mconf = new model_confinfo();	
		$mprestasi = new model_prestasi();
		
		if($id){
			$data['posts'] 		= $mprestasi->read($id);
		}
		$data['semester']	= $mconf->get_semester();
		$data['jenislomba']	= $mprestasi->get_jenis_lomba();
		
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		/*$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tagmanager.css');
		$this->coms->add_script('js/bootstrap/bootstrap-tagmanager.js');*/
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('tagmanager/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		
		$this->add_script('js/prestasi/prestasi.js');
		
		$this->view("prestasi/edit.php", $data);
	}
	
	function save(){
		if(isset($_POST['b_prestasi'])){
			
			ob_start();
			
			$mprestasi = new model_prestasi();
			
			if(isset($_POST['hidId']) && $_POST['hidId']){
				$kode = $_POST['hidId'];
			}else{
				$kode = $mprestasi->get_reg_number();				
			}
			
			$semester		= $_POST['cmbsemester'];
			$jenislomba		= $_POST['cmbjenislomba'];
			$lokasi			= $_POST['lokasi'];
			$judul			= $_POST['judul'];
			$penyelenggara	= $_POST['penyelenggara'];
			$mulai			= $_POST['mulai'];
			$selesai		= $_POST['selesai'];
			$jenisprestasi	= $_POST['rjenis'];
			$tingkat		= $_POST['tingkat'];
			$prestasi		= $_POST['prestasi'];
			$catatan		= $_POST['catatan'];
			$infpeserta		= $_POST['infpeserta'];	
			$linkberita		= $_POST['linkberita'];

			$lastupdate	= date("Y-m-d H:i:s");
			$user	 	= $this->coms->authenticatedUser->id;		
			echo $_POST['hidMhs'];
			//exit();
			
			/* -- peserta mhs --*/
			if(isset($_POST['hidMhs'])&&(count($_POST['hidMhs'])>0)){
				$datanya = array('prestasi_id'=>$kode);
				$mprestasi->delete_peserta($datanya);
				
				$mhs = explode("&", $_POST['hidMhs']);
		
				for($i=0;$i<count($mhs);$i++){
					//$mhsid		= $mprestasi->get_mhs_id($mhs[$i]);
					$mhs_= explode("-", $mhs[$i]);
					$mhsid = trim($mhs_[0]);
					$nama_ = trim($mhs_[1]);
					$pesertaid 	= $mprestasi->get_reg_mhs($kode,$nama_ );
					
					if($mhs[$i]!=""){															
						$datanya=array('peserta_id'=>$pesertaid, 'prestasi_id'=>$kode, 'mahasiswa_id'=>$mhsid, 'inf_nama'=>$nama_);
						$mprestasi->replace_peserta($datanya);
					}
					
				}
			}
			
			$datanya = array('prestasi_id'=>$kode, 'kategori'=>'mahasiswa', 'jenis_id'=>$jenislomba, 'tahun_akademik'=>$semester, 'judul'=>$judul, 
							 'penyelenggara'=>$penyelenggara, 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai, 'lokasi'=>$lokasi,
							 'inf_prestasi'=>$prestasi, 'tingkat'=>$tingkat, 'jenis_prestasi'=>$jenisprestasi, 'keterangan'=>$catatan,
							 'inf_peserta'=>$infpeserta, 'link_berita'=>$linkberita, 'user_id'=>$user, 'last_update'=>$lastupdate);
			$mprestasi->replace_prestasi($datanya);
			
			$this->redirect("module/master/prestasi");	
			exit();
		}
	}
	
	function delete(){
	
	}
}

?>