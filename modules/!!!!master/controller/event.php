<?php
class master_event extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}

		
	function  index($month = NULL, $year = NULL, $style = "calendar"){
		
		$mevent = new model_event();		
		
		$this->coms->add_style('css/calendar/calendar.css');
		
		if(isset($_POST['month'])){
			$month = $_POST['month'];
		}else{
			$month = date('m');
		}
		
		if(isset($_POST['year'])){
			$year = $_POST['year'];
		}else{
			$year = date('Y');
		}
		
		$data['month'] = $month;
		$data['year']  = $year;
		$data['title'] = date('F Y',mktime(0,0,0,$month,1,$year));
		
		$data['event'] = $mevent->get_detail_event();
		
		//$data['posts']  = $mevent->drawCalendar($month, $year, $style);
				
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->view('agenda/index.php', $data );
	}
	
	function viewevent($id){
		$mevent = new model_event();
		$mconf	 = new model_general();
		
		$data['posts']		= $mevent->get_detail_event($id);
		$data['hidid']		= $id;
		$data['pemateri']	= $mevent->get_peserta_event($id, 'pemateri','-');
		$data['undangan']	= $mevent->get_peserta_event($id, 'undangan','-');
		$data['mangkatan']	= $mevent->get_peserta_by_group($id, 'peserta','mhs');
		$data['staff']		= $mevent->get_peserta_event($id, 'peserta','staff');
		$data['peserta']	= $mevent->get_peserta_event($id, 'peserta','luar');
		$data['mhs']		= $mevent->get_peserta_event($id, 'peserta','mhs','nama');
		$data['panitia']	= $mevent->get_peserta_event($id, 'panitia','-');
		$data['dosen']		= $mconf->get_dosen();
		
		$this->view('agenda/view.php', $data);		
	}
	
	function edit($id){
		$mevent = new model_event();
		$mconf	 = new model_general();
		
		$fakultas = $this->coms->authenticatedUser->fakultas;
				
		$data['jenis']  	= $mconf->get_kegiatan();
		$data['hari']   	= $mconf->get_hari();
		$data['event']		= $mevent->get_detail_event($id);
		$data['pemateri']	= $mevent->get_peserta_event($id, 'pemateri','-');
		$data['panitia']	= $mevent->get_peserta_event($id, 'panitia','-');
		$data['undangan']	= $mevent->get_peserta_event($id, 'undangan','-');
		$data['mangkatan']	= $mevent->get_peserta_by_group($id, 'peserta','mhs');
		$data['staff']		= $mevent->get_peserta_event($id, 'peserta','staff','nama');
		$data['peserta']	= $mevent->get_peserta_event($id, 'peserta','luar','nama');
		$data['mhs']		= $mevent->get_peserta_event($id, 'peserta','mhs','nama');
		$data['dosen']		= $mconf->get_dosen($fakultas);
		$data['ruang']  	= $mconf->get_ruang("",$fakultas);
		$data['unit']   	= $mconf->get_unit($fakultas);
		$data['fakultas'] = $fakultas;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('tagmanager/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
		$this->add_script('js/jasny-bootstrap.min.js');
		$this->add_style('css/bootstrap/jasny-bootstrap.min.css');
		$this->add_script('js/event/event.js');
		
		$this->view('agenda/edit.php', $data);		
	}
	
	
	function write(){
		
		$mconf = new model_general();	
	
		$data['jenis']  = $mconf->get_kegiatan();
		
		$fakultas = $this->coms->authenticatedUser->fakultas;
			
		$data['hari']   = $mconf->get_hari();
		$data['ruang']  = $mconf->get_ruang("",$fakultas);
		$data['unit']   = $mconf->get_unit($fakultas);
		$data['fakultas'] = $fakultas;
		
		//$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		//$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('tagmanager/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
		$this->add_script('js/event/event.js');
		$this->add_script('js/jasny-bootstrap.min.js');
		$this->add_style('css/bootstrap/jasny-bootstrap.min.css');
				
		$this->view('agenda/edit.php', $data);
	}
	
	function save(){
		if(isset($_POST['b_agenda'])){
			$this->save_agenda('publish');
		}else{
			$this->save_agenda('draft');
		}		
		exit();
	}
	
	function save_agenda($status=NULL){
				
		$mevent = new model_event();
		$mconf	 = new model_general();
		
		ob_start();
		
		
		if(isset($_POST['hidid']) && ($_POST['hidid'])){
			$kode	= $_POST['hidid'];
			$action	= 'update';
		}else{
			$kode	= $mevent->get_agenda_id();
			$action	= 'insert';
		}
		
		
		
		if(isset($_POST['cmbjenis'])){
			$lokasi		= $_POST['lokasi'];
			
			if(isset($_POST['cmbruang'])){
				$ruang		= implode(",", $_POST['cmbruang']);
				$ruangid	= implode(",", $_POST['cmbruang']);
			}else{
				$ruang	= "";
				$ruangid	= "";
			}
			
			if(isset($_POST['rgroup'])){
				$infundangan	= $_POST['rgroup'];
			}else{
				$infundangan	= "";
			}
			if(isset($_POST['srgroup'])){
				$groupby	= $_POST['srgroup'];
			}else{
				$groupby	= "";
			}
			
			if(isset($_POST['chkhari'])){
				$chkhari = $_POST['chkhari'];
			}else{
				$chkhari = "";
			}
			
			$judul		= $_POST['judul'];
			$keterangan	= stripslashes($_POST['keterangan']);
			$jenis		= $_POST['cmbjenis'];
			$mulai		= $_POST['from'];
			$selesai	= $_POST['to'];		
			$jammulai 	= substr($mulai,-5,8);
			$jamselesai = substr($selesai,-5,8);
			$infpeserta	= $_POST['infpeserta'];	
			$jenisagenda= $_POST['jenis'];
			if(isset($_POST['chkhari'])){		
				$hari	= implode($_POST['chkhari'],",");
			}else{
				$hari = "";
			}
			$plink		=  trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($judul))))));
		}else{
			$jenis 	= "";
		}
		if(isset($_POST['isdelete'])){
			$isdelete = $_POST['isdelete'];
		}else{
			if(isset($_POST['hiddel'])){
				$isdelete = $_POST['hiddel'];
			}else{
				$isdelete = 0;
			}
		}
		
		
		if(isset($_POST['allday'])){
			$isallday = $_POST['allday'];
		}else{
			$isallday = 0;
		}
		
	
		$lastupdate	= date("Y-m-d H:i:s");
		$user	 	= $this->coms->authenticatedUser->id;
		$fakultas = $this->coms->authenticatedUser->fakultas;
		
		if($jenis){		
			
			$strfile	= $_FILES['files']['name'];
			$periode	= substr($mulai, 0, 7);
				
				if($strfile){
					$filename = stripslashes(@$_FILES['files']['name']); 
					$extension = $this->getExtension($filename); 
					$extension = strtolower($extension); 
					
							
					if (($extension != "png") && ($extension != "jpg") && ($extension != "jpeg") ){ 
						$result['error'] = "Unknown extension! Please upload images only";
						print "<script> alert('Unknown extension! Please upload images  only'); </script>"; 
						$errors=1; 
						
						$this->index();
						exit();
					}
					
					
					$picname=$kode.".".$extension;  
					
					//$files		= $kode.".pdf";
					//$file_loc		= "assets/upload/file/".$periode."/event/".$picname;
				//	$upload_dir = 'assets/upload/file/PTIIK/event/';	
					$upload_dir = 'assets/upload/file/PTIIK/event/';	
					$upload_dir_db = 'upload/file/PTIIK/event/';	
					$file_loc = $upload_dir_db . $picname;
				
					//$dir = "assets/upload/file/".$periode."/event";
					
					
					if(!file_exists($dir) OR !is_dir($dir)){
						mkdir($upload_dir, 0777, true);    
						chmod($upload_dir, 0777);						
					} 
					
					$newname="upload/file/".$periode."/event/".$picname;  
					$file_size = $_FILES['files']['size'];
				
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
					
						//------UPLOAD USING CURL----------------
						//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
						$url	     = $this->config->file_url;
						$filedata    = $_FILES['files']['tmp_name'];
						$filename    = $filename;
						$filenamenew = $picname;
						$filesize    = $file_size;
					
						$headers = array("Content-Type:multipart/form-data");
						$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
						$ch = curl_init();
						$options = array(
							 CURLOPT_URL => $url,
							CURLOPT_HEADER => true,
							CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
							CURLOPT_POST => 1,
							CURLOPT_HTTPHEADER => $headers,
							CURLOPT_POSTFIELDS => $postfields,
							CURLOPT_INFILESIZE => $filesize,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => 2
						
						); // cURL options 
						curl_setopt_array($ch, $options);
						curl_exec($ch);
						if(!curl_errno($ch))
						{
							$info = curl_getinfo($ch);
							if ($info['http_code'] == 200)
							   $errmsg = "File uploaded successfully";
						}
						else
						{
							$errmsg = curl_error($ch);
						}
						curl_close($ch);
						//------UPLOAD USING CURL----------------
						
						// rename($file_tmp_name, $upload_dir . $file);
						// unlink($upload_dir . $file);
						$copied = move_uploaded_file($_FILES['files']['tmp_name'], $upload_dir.$picname); 
						
					}
					
					//$copied = move_uploaded_file($_FILES['files']['tmp_name'], $newname); 
					
					if (!$copied){  
						print "<script> alert('Copy unsuccessfull!'); </script>"; 
						$errors=1; 
						
						$this->index();
						exit();
						
					}else{
						$errors=0;
						print "<script> alert('Copy successfull!'); </script>"; 						
					}					
				}else{
					$file_loc	= "";
				}
			
			if($file_loc){
				$datanya=array('agenda_id'=>$kode, 'group_peserta'=>$groupby, 'inf_undangan'=>$infundangan, 'judul'=>$judul,'keterangan'=>$keterangan, 
								'inf_ruang'=>$ruangid, 'lokasi'=>$lokasi, 'jenis_kegiatan_id'=>$jenis, 
								'is_allday'=>$isallday, 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai, 'is_delete'=>$isdelete, 'jenis_agenda'=>$jenisagenda, 'status_agenda'=>$status, 
								'page_link'=>$plink, 'inf_peserta'=>$infpeserta, 
								'penyelenggara'=>$_POST['penyelenggara'],'inf_hari'=>$hari, 'unit_id'=>$_POST['cmbunit'], 'icon_thumb'=>$file_loc,'last_update'=>$lastupdate, 'user_id'=>$user, 
								'fakultas_id'=>$fakultas, 'english_version'=>$_POST['judul_en'], 'note_english'=>$_POST['note_en'] );
				$mevent->replace_event($datanya);
			
				$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
            }else{
				if($_POST['hidid']){
					$datanya=array('judul'=>$judul, 'group_peserta'=>$groupby,'inf_undangan'=>$infundangan,'keterangan'=>$keterangan, 'inf_ruang'=>$ruangid, 
									'lokasi'=>$lokasi, 'jenis_kegiatan_id'=>$jenis, 
									'is_allday'=>$isallday, 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai, 'is_delete'=>$isdelete, 'jenis_agenda'=>$jenisagenda, 'status_agenda'=>$status, 
									'page_link'=>$plink, 'inf_peserta'=>$infpeserta, 
									'penyelenggara'=>$_POST['penyelenggara'],'inf_hari'=>$hari, 'unit_id'=>$_POST['cmbunit'], 'last_update'=>$lastupdate, 'user_id'=>$user,
									'fakultas_id'=>$fakultas, 'english_version'=>$_POST['judul_en'], 'note_english'=>$_POST['note_en']);
					$idnya	= array('agenda_id'=>$kode);
					$mevent->update_event($datanya, $idnya);
					
					$mevent->delete_detail($kode);
					$mevent->delete_peserta($kode);
					
					$mevent->delete_peserta_kegiatan($kode);
				}else{
					$datanya=array('agenda_id'=>$kode, 'group_peserta'=>$groupby, 'inf_undangan'=>$infundangan,'judul'=>$judul,'keterangan'=>$keterangan, 'inf_ruang'=>$ruangid, 
								'lokasi'=>$lokasi, 'jenis_kegiatan_id'=>$jenis, 
								'is_allday'=>$isallday, 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai, 'is_delete'=>$isdelete, 'jenis_agenda'=>$jenisagenda, 'status_agenda'=>$status, 
								'page_link'=>$plink, 'inf_peserta'=>$infpeserta, 
								'penyelenggara'=>$_POST['penyelenggara'],'inf_hari'=>$hari, 'unit_id'=>$_POST['cmbunit'], 'icon_thumb'=>$file_loc,'last_update'=>$lastupdate, 'user_id'=>$user, 
								'english_version'=>$_POST['judul_en'], 'note_english'=>$_POST['note_en']);
					$mevent->replace_event($datanya);
				}
			
				//$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
			}
			
			if($ruangid){
				for($i=0;$i<count($_POST['cmbruang']);$i++){
					$this->insert_detail_jadwal($i,$mconf, $mevent, $kode, $mulai, $selesai, $chkhari, $jammulai, $jamselesai, $user, $action, $_POST['cmbruang'][$i], 
					$jenis, $judul, $keterangan, $lastupdate);
				}
			}else{
				$this->insert_detail_jadwal(0,$mconf, $mevent, $kode, $mulai, $selesai, $chkhari, $jammulai, $jamselesai, $user, $action, $_POST['cmbruang'][$i], 
					$jenis, $judul, $keterangan, $lastupdate);
			}
		}		
		
		//echo $_POST['hidPemateri'];
		
		
		if(isset($_POST['hidPemateri'])&&(count($_POST['hidPemateri'])>0)){
			
			$pemateri = explode("&", $_POST['hidPemateri']);
	
			for($i=0;$i<count($pemateri);$i++){
				$pesertaid	= "1".$kode.$mevent->addNol($i);
				if($pemateri[$i]){
					$temp	= $mevent->get_id_dosen($pemateri[$i]);
					
					if($temp){
						$pemateriid = $temp->karyawan_id;
						$idkaryawan	= $pemateriid;
					}else{
						$pemateriid="";
						$idkaryawan	= "-";
					}
					
					$instansi= explode("@", $pemateri[$i]);
					
					for($u=0;$u<count($instansi);$u++){						
						$ins =$instansi[1];
						$nama=$instansi[0];	
					}
					
							
					$datanya=array('peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>$idkaryawan,'sebagai'=>'pemateri', 'nama'=>$nama, 'instansi'=>$ins, 'jenis_peserta'=>'-',  'last_update'=>$lastupdate, 'user_id'=>$user);
					$mevent->replace_peserta($datanya);
					
					if($idkaryawan!='-'){
						$aktifitas = $mevent->get_aktifitas_id($kode,$idkaryawan);
						$datanya1 = array('aktifitas_id'=>$aktifitas, 'karyawan_id'=>$idkaryawan, 'tgl'=>$mulai, 'tgl_selesai'=>date("Y-m-d", strtotime($selesai)), 'inf_ruang'=>$ruangid, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jenis,  'judul'=>$judul, 'catatan'=>$keterangan, 'agenda_id'=>$kode,'inf_kategori'=>'event','lokasi'=>$lokasi);
						$mevent->replace_event_staff($datanya1);
					}
					//$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
				}
				
			}
		}
		
		
		if(isset($_POST['hidUndangan'])&&(count($_POST['hidUndangan'])>0)){
			
			$undangan = explode("&", $_POST['hidUndangan']);
	
			for($i=0;$i<count($undangan);$i++){
				$pesertaid	= "2".$kode.$mevent->addNol($i);
				if($undangan[$i]){
					$temp	= $mevent->get_id_dosen($undangan[$i]);
					
					if($temp){
						$undanganid = $temp->karyawan_id;
						$idkaryawan	= $undanganid;
					}else{
						$undanganid="";
						$idkaryawan	= "-";
					}
					
					$instansi= explode("@", $undangan[$i]);
					
					if($instansi){
						for($u=0;$u<count($instansi);$u++){								
							$ins =$instansi[1];						
							$nama=$instansi[0];						
						}
					}else{
						$ins = "-";
						$nama= "-";
					}
					
					$datanya=array('peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>$idkaryawan,'sebagai'=>'undangan', 'nama'=>$nama, 'instansi'=>$ins, 'jenis_peserta'=>'-',  'last_update'=>$lastupdate, 'user_id'=>$user);
					$mevent->replace_peserta($datanya);
					
					if($idkaryawan!='-'){
						$aktifitas = $mevent->get_aktifitas_id($kode,$idkaryawan);
						$datanya1 = array('aktifitas_id'=>$aktifitas, 'karyawan_id'=>$idkaryawan, 'tgl'=>$mulai, 'tgl_selesai'=>date("Y-m-d", strtotime($selesai)), 'inf_ruang'=>$ruangid, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jenis,  'judul'=>$judul, 'catatan'=>$keterangan, 'agenda_id'=>$kode,'inf_kategori'=>'event','lokasi'=>$lokasi);
						$mevent->replace_event_staff($datanya1);
					}
		
					//$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
				}
				
			}
		}
		
		if(isset($_POST['rgroup'])){
			switch($_POST['rgroup']){
				case  'staff':
					$peserta = $mevent->get_data_peserta('staff');
				break;
				case  'dosen':
					$peserta = $mevent->get_data_peserta('dosen');
				break;
				case  'all':
					$peserta = $mevent->get_data_peserta();
				break;
				default:
					$peserta = "";
				break;
			}
			
			$i=0;
			
			if($peserta){
				foreach($peserta as $dt):
					$i++;
					
					$pesertaid	= "3".$kode.$mevent->addNol($i);
					$idkaryawan	= $dt->karyawan_id;
					$nama		= $dt->nama;
					
					$datanya=array('peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>$idkaryawan,'sebagai'=>'undangan', 'nama'=>$nama, 'jenis_peserta'=>'undangan',
							'group_by'=>$_POST['rgroup'],  'last_update'=>$lastupdate, 'user_id'=>$user);
					$mevent->replace_peserta($datanya);
					
					if($idkaryawan){
						$aktifitas = $mevent->get_aktifitas_id($kode,$idkaryawan);
						$datanya1 = array('aktifitas_id'=>$aktifitas, 'karyawan_id'=>$idkaryawan, 'tgl'=>$mulai, 'tgl_selesai'=>date("Y-m-d", strtotime($selesai)), 'inf_ruang'=>$ruangid, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jenis,  'judul'=>$judul, 'catatan'=>$keterangan, 'agenda_id'=>$kode,'inf_kategori'=>'event','lokasi'=>$lokasi);
						$mevent->replace_event_staff($datanya1);
					}
				endforeach;
			}
		}
		
		/*--panitia --*/
		if(isset($_POST['hidPanitia'])&&(count($_POST['hidPanitia'])>0)){
			
			$panitia = explode("&", $_POST['hidPanitia']);
	
			for($i=0;$i<count($panitia);$i++){
				$pesertaid	= "4".$kode.$mevent->addNol($i);
				if($panitia[$i]){
					$temp	= $mevent->get_id_dosen($panitia[$i]);
					
					if($temp){
						$undanganid = $temp->karyawan_id;
						$idkaryawan	= $undanganid;
					}else{
						$undanganid="";
						$idkaryawan	= "-";
					}
					
					$instansi= explode("@", $panitia[$i]);
					
					
					if($instansi){
						for($u=0;$u<count($instansi);$u++){						
							$ins =$instansi[1];
							$nama=$instansi[0];						
						}
						//$ins="";
					}else{
						$ins="";
					}
					
					$datanya=array('peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>$idkaryawan,'sebagai'=>'panitia', 'nama'=>$nama, 'instansi'=>$ins, 'jenis_peserta'=>'-',  'last_update'=>$lastupdate, 'user_id'=>$user);
					$mevent->replace_peserta($datanya);
					
					if($idkaryawan!='-'){
						$aktifitas = $mevent->get_aktifitas_id($kode,$idkaryawan);
						$datanya1 = array('aktifitas_id'=>$aktifitas, 'karyawan_id'=>$idkaryawan, 'tgl'=>$mulai, 'tgl_selesai'=>date("Y-m-d", strtotime($selesai)), 'inf_ruang'=>$ruangid, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jenis,  'judul'=>$judul, 'catatan'=>$keterangan, 'agenda_id'=>$kode,'inf_kategori'=>'event','lokasi'=>$lokasi);
						$mevent->replace_event_staff($datanya1);
					}
					//$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
				}
				
			}
		}
		
		/* -- peserta mhs --*/
		
	
		if(isset($_POST['hidMhs'])&&(count($_POST['hidMhs'])>0)){
			
			$mhs = explode("&", $_POST['hidMhs']);
	
			for($i=0;$i<count($mhs);$i++){
				$pesertaid	= "5".$kode.$mevent->addNol($i);
				if($mhs[$i]){
					$namamhs= explode(" - ", $mhs[$i]);
					
					
					if($namamhs){
						for($u=0;$u<count($namamhs);$u++){						
							$nim =$namamhs[0];
							$nama=$namamhs[1];						
						}
						//$ins="";
					}else{
						$nim="";
						$nama="";
					}
					
					
					if($nama){
						$datanya=array('peserta_id'=>$pesertaid, 'group_by'=>'nama', 'agenda_id'=>$kode, 'karyawan_id'=>'-','sebagai'=>'peserta', 'mahasiswa_id'=>$nim, 'nama'=>$nama, 'jenis_peserta'=>'mhs',  'last_update'=>$lastupdate, 'user_id'=>$user);
						$mevent->replace_peserta($datanya);
					}
					if($nim){
						$aktifitas = $mevent->get_aktifitas_id($kode,$nim);
						$datanya1 = array('aktifitas_id'=>$aktifitas, 'mahasiswa_id'=>$nim, 'tgl'=>$mulai, 'tgl_selesai'=>date("Y-m-d", strtotime($selesai)), 'inf_ruang'=>$ruangid, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jenis,  'judul'=>$judul, 'catatan'=>$keterangan, 'agenda_id'=>$kode,'inf_kategori'=>'event','lokasi'=>$lokasi);
						$mevent->replace_event_staff($datanya1);
					}
					//$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
				}
				
			}
		}
		
		if(isset($_POST['hidStaff'])&&(count($_POST['hidStaff'])>0)){
			
			$staff = explode("&", $_POST['hidStaff']);
	
			for($i=0;$i<count($staff);$i++){
				$pesertaid	= "6".$kode.$mevent->addNol($i);
				if($staff[$i]){		
					$dtstaff	= $mevent->get_id_dosen($staff[$i]);
					
					if($dtstaff){
						$staffid	= $dtstaff->karyawan_id;
						$idkaryawan	= $staffid;
					}else{
						$staffid	= "";
						$idkaryawan	= "-";
					}
					
					$datanya=array('group_by'=>'nama', 'peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>$idkaryawan,'sebagai'=>'peserta', 'nama'=>$staff[$i], 'jenis_peserta'=>'staff',  'last_update'=>$lastupdate, 'user_id'=>$user);
					$mevent->replace_peserta($datanya);
					
					if($idkaryawan!='-'){
						$aktifitas = $mevent->get_aktifitas_id($kode,$idkaryawan);
						
						$datanya1 = array('aktifitas_id'=>$aktifitas, 'karyawan_id'=>$idkaryawan, 'tgl'=>$mulai, 'tgl_selesai'=>date("Y-m-d", strtotime($selesai)), 'inf_ruang'=>$ruangid, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jenis,  'judul'=>$judul, 'catatan'=>$keterangan, 'agenda_id'=>$kode,'inf_kategori'=>'event','lokasi'=>$lokasi);
						$mevent->replace_event_staff($datanya1);
					}
					//$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
					
				}
				//echo $_POST['hidStaff'];
				
				
			}
			
			
		}
		
		if(isset($_POST['hidPeserta'])&&(count($_POST['hidPeserta'])>0)){
			
			$peserta = explode("&", $_POST['hidPeserta']);
	
			for($i=0;$i<count($peserta);$i++){
				$pesertaid	= "7".$kode.$mevent->addNol($i);
				if($peserta[$i]){															
					$datanya=array('peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>'-','sebagai'=>'peserta', 'nama'=>$peserta[$i], 'jenis_peserta'=>'luar',  'last_update'=>$lastupdate, 'user_id'=>$user);
					$mevent->replace_peserta($datanya);
					//$mconf->log("db_pjj.tbl_agenda", $datanya, $user, $action);	
				}
				
			}
		}
		
		if(isset($_POST['srgroup'])){
			switch($_POST['srgroup']){
				case  'staff':
					$peserta = $mevent->get_data_peserta('staff');
				break;
				case  'dosen':
					$peserta = $mevent->get_data_peserta('dosen');
				break;
				case  'all':
					$peserta = $mevent->get_data_peserta();
				break;
				default:
					$peserta = "";
				break;
			}
			
			$i=0;
			
			if($peserta){
				foreach($peserta as $dt):
					$i++;
					
					$pesertaid	= "8".$kode.$mevent->addNol($i);
					$idkaryawan	= $dt->karyawan_id;
					$nama		= $dt->nama;
					
					$datanya=array('peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>$idkaryawan,'sebagai'=>'peserta', 'nama'=>$nama, 'jenis_peserta'=>'staff',
							'group_by'=>$_POST['srgroup'],  'last_update'=>$lastupdate, 'user_id'=>$user);
					$mevent->replace_peserta($datanya);
					
					if($idkaryawan){
						$aktifitas = $mevent->get_aktifitas_id($kode,$idkaryawan);
						$datanya1 = array('aktititas_id'=>$aktifitas, 'karyawan_id'=>$idkaryawan, 'tgl'=>$mulai, 'tgl_selesai'=>$selesai, 'ruang'=>$ruangid, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jenis,  'judul'=>$judul, 'catatan'=>$keterangan, 'agenda_id'=>$kode);
						$mevent->replace_event_staff($datanya1);
					}
					
				endforeach;
			}
		}
		
		if(isset($_POST['hidAngkatan'])&&(count($_POST['hidAngkatan'])>0)){
			
			$angkatan = explode("&", $_POST['hidAngkatan']);
	
			for($i=0;$i<count($angkatan);$i++){
				if($angkatan[$i]){
										
					$mhs = $mevent->get_data_peserta_mhs($angkatan[$i]);
					
					if($mhs){
						$j=0;
						foreach ($mhs as $dt):	
							$j++;
							
							$pesertaid	= "9".$kode.$i.$mevent->addNol($j);
							
							$datanya=array('peserta_id'=>$pesertaid, 'agenda_id'=>$kode, 'karyawan_id'=>'-','sebagai'=>'peserta', 'nama'=>$dt->nama, 'jenis_peserta'=>'mhs', 
											 'group_by'=>$angkatan[$i], 'last_update'=>$lastupdate, 'user_id'=>$user);
							$mevent->replace_peserta($datanya);
							
						endforeach;
					}
				}
				
			}
		}
		
		$this->send_notifikasi($kode);
		
		$this->redirect("module/master/event");	
		
		exit();
	}
	
	
	function insert_detail_jadwal($x,$mconf, $mevent, $kode, $mulai, $selesai, $chkhari, $jammulai, $jamselesai, $user, $action, $ruangid, $jenis, $judul, $keterangan, $lastupdate){
		$jadwalid 	= "1".$kode.$x;
		$jammulai 	= substr($mulai,-5,8);
		$jamselesai = substr($selesai,-5,8);
		$in = strtotime($mulai);
		$out= strtotime($selesai);
	
		
		$n = $this->dateDiff($in,$out);				
		$i=0;
		$skip = false;
						
		for($i=0;$i<=$n;$i++){
			$skip=false;
			
			$shari 	= $this->getHari(date("N",  strtotime('+'.$i.' days', $in)));
			$nhari	= date("N",  strtotime('+'.$i.' days', $in));
			$tgl	= date("Y-m-d", strtotime('+'.$i.' days', $in));
		
			if($chkhari){
				for($j=0;$j<count($chkhari);$j++){
					
					if($shari==$chkhari[$j]){
						
						$this->insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i,$ruangid, $kode, $jenis,'-','-','-');
						
						$skip=true;
						break;	
					}
				}						
			}else{
				$this->insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i,$ruangid, $kode, $jenis,'-','-','-');
			}				
			
		}
		//exit();
		
		if($ruangid){
			$datanya = array('jadwal_id'=>$jadwalid, 'ruang_id'=>$ruangid, 'kegiatan'=>$judul, 'catatan'=>$keterangan, 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai, 
							 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai , 'repeat_on'=>'daily', 'user_id'=>$user, 'last_update'=>$lastupdate, 'jenis_kegiatan_id'=>$jenis );
			$mevent->replace_jadwal_ruang($datanya);
			
			$mconf->log("db_pjj.tbl_pemakaian_ruang", $datanya, $user, $action);	
		}
	}
	
	function detail($id){
		$mevent = new model_event();
		
		$data['posts'] = $mevent->get_detail_event($id);
		
		$this->view('agenda/view.php', $data);		
	}
	
	
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		
		return $ext; 
	}  
	
	function dateDiff($in,$out){		
		$interval =round(($out - $in)/(3600*24));
		return  $interval ;
	} 
	
	function getHari($str){
		switch($str){
			case '1':
				$strout	= 'senin';
			break;
			case '2':
				$strout	= 'selasa';
			break;
			case '3':
				$strout	= 'rabu';
			break;
			case '4':
				$strout	= 'kamis';
			break;
			case '5':
				$strout	= 'jumat';
			break;
			case '6':
				$strout	= 'sabtu';
			break;
			case '7':
				$strout	= 'minggu';
			break;
		}
		
		return $strout;
	}
	
	function insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i, $ruang, $infjadwal, $infjeniskegiatan, $dosen, $kelas, $mk){
		$mevent = new model_event();	
		//$mconf 	 = new model_general();			
		
		$detailid = $nhari.$jadwalid.$i;
		
		if($ruang){		
			$datanya = array('detail_id'=>$detailid, 'jadwal_id'=>$jadwalid, 'tgl'=>$tgl, 'hari'=>$shari, 'inf_hari'=>$nhari, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'ruang'=>$ruang, 'inf_jadwal_id'=>$infjadwal, 'inf_jenis_kegiatan'=>$infjeniskegiatan, 'dosen_id'=>$dosen, 'kelas'=>$kelas, 'mk_id'=>$mk);
			$mevent->replace_detail_jadwal_ruang($datanya);
			
			//$mconf->log("db_pjj.tbl_pemakaian_ruang_detail", $datanya, $user, $action);	
		}	

		$datanya = array('detail_id'=>$detailid, 'agenda_id'=>$infjadwal, 'ruang'=>$ruang, 'tgl'=>$tgl,'hari'=>$shari, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai);
		$mevent->replace_detail_event($datanya);
		
	}
	
	function delete($id){
		
		$mevent = new model_event();	
		$result = array();
		
		if($mevent->delete($id)){ 
			$mevent->delete_detail($id);
			$mevent->delete_peserta($id);			
			$mevent->delete_peserta_kegiatan($id);
			$result['status'] = "OK";
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mevent->error;
		}
		echo json_encode($result);		

	}
	
	
	function send_notifikasi($id=NULL){
		require ('library/class.phpmailer.php');
		
		$mevent = new model_event();	
		
		$mail     = new PHPMailer();
		
		$event  	= $mevent->get_detail_event($id);
		$undangan 	= $mevent->get_peserta_notif($id);
		
		$mulai	= date("Ymd", strtotime($event->tgl_mulai));
		$selesai= date("Ymd", strtotime($event->tgl_selesai));
		
		
		
		$body='<br>Berikut adalah event kegiatan yang tercatat pada PTIIK Apps:';
		
		$text_body = "\n Berikut adalah event kegiatan yang tercatat pada PTIIK Apps :";
		
		$body.= '<table>';
			//foreach ($event as $dt):			
			if($event){				
				$body.= '<tr>';
					$body.= '<td>Kegiatan</td><td>:</td><td><b>'.$event->judul.'</b></td>';
				$body.= '</tr>';

				$text_body .= "Kegiatan		:".$event->judul."\n";				
				
				if($mulai==$selesai){
					$body.= '<tr>';
						$body.= '<td>Tgl Kegiatan</td><td>:</td><td>'.date("M d, Y", strtotime($event->tgl_mulai)).'</td>';
					$body.= '</tr>';
					
					$text_body .= "Tgl Kegiatan		:".date("M d, Y", strtotime($event->tgl_mulai))."\n";			
				}else{
					$body.= '<tr>';
						$body.= '<td>Tgl Kegiatan</td><td>:</td><td>'.date("M d, Y", strtotime($event->tgl_mulai)).' s/d '.date("M d, Y", strtotime($event->tgl_selesai)).'</td>';
					$body.= '</tr>';
					
					$text_body .= "Tgl Kegiatan		:".date("M d, Y", strtotime($event->tgl_mulai)).' s/d '.date("M d, Y", strtotime($event->tgl_selesai))."\n";		
				}
				
				$body.= '<tr>';
					$body.= '<td>Waktu</td><td>:</td><td>'.date("H:i", strtotime($event->tgl_mulai)).' s/d '.date("H:i", strtotime($event->tgl_selesai)).'</td>';
				$body.= '</tr>';
					
				$body.= '<tr valign=top>';
					$body.= '<td>Lokasi</td><td>:</td><td>'.$event->lokasi.'</td>';
				$body.= '</tr>';
				$body.= '<tr>';
					$body.= '<td>Ruang</td><td>:</td><td>'.$event->inf_ruang.'</td>';
				$body.= '</tr>';				
				$body.= '<tr>';
					$body.= '<td>Penyelenggara</td><td>:</td><td>'.$event->penyelenggara.'</td>';
				$body.= '</tr>';				
				
				$text_body .= "Waktu		:".date("H:i", strtotime($event->tgl_mulai)).' s/d '.date("H:i", strtotime($event->tgl_selesai))."\n";		
				$text_body .= "Lokasi		:".$event->lokasi."\n";		
				$text_body .= "Ruang		:".$event->inf_ruang."\n";	
				$text_body .= "Penyelenggara:".$event->penyelenggara."\n";						
			//endforeach;
			}
			if($undangan){
				$text_body .= "Undangan/Peserta :\n";
				$body.= '<tr valign=top>';
					$body.= '<td>Undangan/Peserta</td><td>:</td><td>';
					$i=0;
						$body.= "<ul>";
							//$rpic = array();
							foreach($undangan as $dt):
								$i++;
								if($dt->karyawan_id){
									$pic 	= $mevent->get_karyawan($dt->karyawan_id);
									
									$epic 	= $pic->email;
									$npic	= $pic->nama;
									$kid	= $pic->karyawan_id;
								
									$body.= "<li>".$npic."</li>";
									$text_body .= "- ".$npic."\n";
								}else{
									$epic 	= "";
									$npic	= "";
									$kid	= "";
								}
								$rpic[]= array($epic, $npic, $kid, $id);
							endforeach;
						$body.= "</ul>";
					$body.= '</td>';
				$body.= '</tr>';
			}
		$body.= '</table>';
		
		$body.= '<br><br>Untuk informasi lebih lanjut, silahkan mengunjungi <a href="http://ptiik.ub.ac.id/apps">http://ptiik.ub.ac.id/apps</a><br><br><br>Terima kasih, <br><br><br><br>
				<hr><small>Badan Pengembangan Teknologi Informasi dan Komunikasi<br>
				<b>PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER</b><br>
				<b>UNIVERSITAS BRAWIJAYA</b><br>
				Ruang B1.1 Gedung B PTIIK UB<br>
				Jl. Veteran no 8 Malang, 65145, Indonesia<br>
				telp : (0341) 577911 Fax : (0341) 577911</small>';
				
		$text_body_footer = "Untuk informasi lebih lanjut, silahkan mengunjungi <a href='http://ptiik.ub.ac.id/apps/info/layanan'>http://ptiik.ub.ac.id/apps/auth</a> \nTerima kasih, \n\n\n\n
		Badan Pengembangan Teknologi Informasi dan Komunikasi\nPROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER\nUNIVERSITAS BRAWIJAYA\nRuang B1.1 Gedung B PTIIK UB\n
							Jl. Veteran no 8 Malang, 65145, Indonesia\n
							telp : (0341) 577911 Fax : (0341) 577911";
		

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = "smtp.ub.ac.id"; // SMTP server
		$mail->SMTPDebug  = false;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "STARTTLS";                 // sets the prefix to the servier
		$mail->Host       = "smtp.ub.ac.id";      // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
		$mail->Username   = $this->config->default_email;  // GMAIL username
		$mail->Password   = $this->config->default_email_pass;            // GMAIL password
		
		$mail->SetFrom($this->config->default_email, 'Badan Pengembangan Teknologi Informasi dan Komunikasi PTIIK UB');
		$mail->AddReplyTo($this->config->default_email,"Badan Pengembangan Teknologi Informasi dan Komunikasi PTIIK UB");				

		$mail->Subject    = "PTIIK Apps - $event->judul";

		
		//$address = $email;
		
		foreach ($rpic as $data){
			
			if($data[1]){
				//echo $data[1].$body;
				$mail->MsgHTML('Yth. '.$data[1]. ', <br>'. $body);
				
				$mail->AltBody    = "Yth. ".$data[1]. ", \n". $text_body.$text_body_footer; 
				
				$mail->ClearAddresses();
				 
				$mail->AddAddress($data[0], $data[1]);
				
				if($mail->Send()){
					$error=0;
					$this->update_notif($data[2], $data[3]);
				}else{
					// echo "Mailer Error: " . $mail->ErrorInfo;
					$error=1;
				}
			}
		}
		
		//exit();
		
	}
	
	function update_notif($karyawan, $event){
		$datanya 	= array('notifikasi'=>1);
		$idnya	  	= array('karyawan_id'=>$karyawan, 'agenda_id'=>$event);
		
		$mevent = new model_event();	
		
		$mevent->update_peserta_notif($datanya, $idnya);
	}
	
}
?>