<?php
class master_akademik extends comsmodule {
	private $coms;
	
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function write($str){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		switch ($str) {
			case 'mkditawarkan':
				$this->write_mkditawarkan();
			break;
			case 'mk':
				$this->write_mk();
			break;
		}
	}
	
	function edit($str, $id){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		switch ($str) {
			case 'mkditawarkan':
				$this->edit_mkditawarkan($id);
			break;
			case 'mk':
				$this->edit_mk($id);
			break;
		}
	}
	
	function detail($str, $id){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		switch ($str) {
			case 'mkditawarkan':
				$this->detail_mkditawarkan($id);
			break;
		}
	}
	
	/*
	 * 
	 * -------START FUNCTION MK DITAWARKAN-----------
	 * 
	 * */
	function mkditawarkan(){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		$mmk 		= new model_mk();	
		$mconf	    = new model_conf();		
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		//-- SIDEBAR --//
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		$data['thnakademik']	= $mmk->get_all_thn_akademik();
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
		}
		$data['cabang']			= $mconf->get_cabangub();
		//-- SIDEBAR --//
		
		$data['user']  			= $user;
		$data['view_body']		= 'index';
		
		if(isset($_POST['thn_akademik'])){
			$thnakademik = $_POST['thn_akademik'];
			$data['thnakademikid']=$thnakademik;
		}else $thnakademik=NULL;
		
		if(isset($_POST['fakultas'])){
			$fakultas = $_POST['fakultas'];
			$data['fakultasid']=$fakultas;
		}else $fakultas=NULL;
		
		if(isset($_POST['cabang'])){
			$cabang = $_POST['cabang'];
			$data['cabangid']=$cabang;
		}else $cabang=NULL;
		
		$data['posts'] = $mmk->read_by_parameter($thnakademik, $fakultas, $cabang);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		$this->add_script('js/mk/mkditawarkan.js');
		$this->view('mkditawarkan/index.php', $data);
	}
	
	function detail_mkditawarkan($id){
		$mmk 				= new model_mk();	
		$user = $this->coms->authenticatedUser->role;
		
		$data['posts']		= $mmk->read($id);
		$data['pengampu']	= $mmk->get_dosen_pengampu($id);
		$data['user']		= $user;
		
		$this->add_style('css/bootstrap/DT_bootstrap.css');
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');	
				
		$this->view( 'mkditawarkan/detail.php', $data );
	}
	
	function write_mkditawarkan(){
		$mmk = new model_mk();	
		$mconf	   = new model_conf();		
		$user = $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		//-- SIDEBAR --//
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		$data['thnakademik']	= $mmk->get_all_thn_akademik();
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
		}
		$data['cabang']			= $mconf->get_cabangub();
		//-- SIDEBAR --//
		
		$data['user']  			= $user;
		$data['view_body']		= 'write';
		$data['posts'] 			= '';
		
		$this->add_script('js/jquery/tagmanager.js');
		$this->add_style('css/bootstrap/tagmanager.css');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');

		$this->add_script('js/mk/mkditawarkan.js');
		
		$this->view('mkditawarkan/index.php', $data);
	}
	
	function edit_mkditawarkan($id){
		$mmk = new model_mk();	
		$mconf	   = new model_conf();		
		$user = $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		//-- SIDEBAR --//
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		$data['thnakademik']	= $mmk->get_all_thn_akademik();
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
		}
		$data['cabang']			= $mconf->get_cabangub();
		//-- SIDEBAR --//
			
		$data['user']  			= $user;
		$data['view_body']		= 'edit';
		$data['posts']			= $mmk->read($id);
		$data['edit']			= '1';
		$data['getdatamk'] 		= $mmk->get_all_mk_from_mkditawarkan();
		$data['pengampu']		= $mmk->get_dosen_pengampu($id);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jquery/tagmanager.js');
		$this->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/mk/mkditawarkan.js');
		
		$this->view('mkditawarkan/index.php', $data);
	}
	
	function koor_pengampu(){
		ob_start();
		$mmk = new model_mk();	
		$pengampuid 	= $_POST['pengampuid'];
		$iskoor 		= $_POST['iskoor'];
		
		$mmk->update_koor($pengampuid, $iskoor);
		ob_end_clean();
	}
	
	function delete_pengampu(){
		$mmk = new model_mk();	
		$pengampuid 	= $_POST['delete_id'];
		if($mmk->delete_pengampu($pengampuid)){
			echo "sukses";
		}
	}
	
	function delete_mkditawarkan(){
		$mmk = new model_mk();	
		$mkditawarkanid 	= $_POST['mkid'];
		$data = $mmk->get_mkditawarkan_data($mkditawarkanid);
		
		$mkid = $data->mkditawarkan_id;
		$thnakademik = $data->tahun_akademik;
		
		if($mmk->delete_mkditawarkan($mkid,$thnakademik)){
			echo "sukses";
		}
	}
	
	function add_pengampu(){
		$mmk = new model_mk();
		$karyawanid 	= $_POST['karyawan_id'];
		$mkid 			= $_POST['mk_id'];
		$iskoor			= 0;
		$pengampuid		= $mmk->get_pengampu_reg_number($mkid, $karyawanid);
		
		$datanya 	= Array(
						'pengampu_id'=>$pengampuid, 
						'mkditawarkan_id'=>$mkid, 
						'karyawan_id'=>$karyawanid, 
						'is_koordinator'=>$iskoor
						);
		$mmk->replace_pengampu($datanya);
	}
	
	function blok_mk()
	{
		$cek = $_POST['fakultas_id'];
		if($cek=='0'){
			echo '';
		}
		else{	
			$mmk = new model_mk();
			$id = $mmk->get_fakultas_id($_POST['fakultas_id']);
			$parent = $mmk->get_mk_from_mkditawarkan($id);
			if(isset($parent)){
				echo "<option value='0'>Select Mata Kuliah</option>" ;
				foreach($parent as $p )
				{
					echo "<option value='".$p->mkid."'>".$p->keterangan."</option>" ;	
				}	
			}else echo '';
		}
	}
	
	function save_mkditawarkan_ToDB(){
		ob_start();
		$mmk = new model_mk();		
		//-------------------cek mata kuliah and insert if there isnt any-----------------------
		
		$matakuliahid	= $mmk->get_mkid_from_namamk($_POST['namamk']);
		
		if(!isset($matakuliahid)){
			$ceknamamkid = $mmk->get_namamk_id_from_namamk($_POST['namamk']);
			if(!isset($ceknamamkid)){
				//-----di tbl_namamk dan tbl_matakuliah tidak ada datanya-------------------
				
				//-------------insert tbl_namamk------------------------------------				
				$namamk_id	= $mmk->get_namamk_reg_number();
				$fakultasid = $mmk->get_fakultas_id($_POST['fakultas']);
				$keterangan = $_POST['namamk'];
				$engver		= "";
				
				$datanya 	= Array(
								'namamk_id'=>$namamk_id, 
								'fakultas_id'=>$fakultasid, 
								'keterangan'=>$keterangan, 
								'english_version'=>$engver
								);
				$mmk->replace_namamk($datanya);
				//-------------end of insert tbl_namamk----------------------------
				
				//-------------insert tbl_matakuliah------------------------------------
				$matakuliahid	= $mmk->get_matakuliah_reg_number();
				$kodemk 		= "";
				$kurikulum		= "";
				$sks			= "";	
				
				$datanya 	= Array(
										'matakuliah_id'=>$matakuliahid, 
										'namamk_id'=>$namamk_id, 
										'kode_mk'=>$kodemk, 
										'kurikulum'=>$kurikulum,
										'sks'=>$sks
										);
					$mmk->replace_matakuliah($datanya);
				//-------------end of insert tbl_matakuliah----------------------------
				
			}
			else {
				//-----di tbl_namamk ada datanya dan tbl_matakuliah tidak ada datanya-------
				
				//-------------insert tbl_matakuliah------------------------------------
				$matakuliahid	= $mmk->get_matakuliah_reg_number();
				$kodemk 		= "";
				$kurikulum		= "";
				$sks			= "";	
				
				$datanya 	= Array(
										'matakuliah_id'=>$matakuliahid, 
										'namamk_id'=>$ceknamamkid, 
										'kode_mk'=>$kodemk, 
										'kurikulum'=>$kurikulum,
										'sks'=>$sks
										);
					$mmk->replace_matakuliah($datanya);
				//-------------end of insert tbl_matakuliah----------------------------
				
			}
		}
		
		//-------------------------------------------------------------------------------------
		
		if($_POST['hidId']!=""){
			$mkditawarkan_id 	= $_POST['hidId'];
			$action 			= "update";
		}else{
			$mkditawarkan_id	= $mmk->get_mkditawarkan_reg_number();	
			$action 			= "insert";	
		}
		
		if(isset($_POST['isblok'])!=""){
			$isblok	= $_POST['isblok'];
			$parent_id		= $_POST['parent_mk'];
		}else{
			$isblok	= 0;
			$parent_id		= "";
		}
		
		if(isset($_POST['iscourse'])&&$_POST['iscourse']!=""){//id is_course = 1 is_online = 0
			$isonline	= 0;
		}else{
			$isonline	= 1;
		}
		
		if(isset($_POST['ispraktikum'])!=""){
			$ispraktikum	= $_POST['ispraktikum'];
		}else{
			$ispraktikum	= 0;
		}
		
		if(isset($_POST['iskoordinator'])!=""){
			$iskoor	= $_POST['iskoordinator'];
		}else{
			$iskoor	= 0;
		}
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$cabangid		= $_POST['cabang'];
		$thnakademik	= $_POST['tahun_akademik'];
		
		//-----isi dosen pengampu-------------//
		$dosenkoor		= $_POST['dosenkoor'];
		$iskoor			= $_POST['iskoordinator'];
		$dosenpengampu	= $_POST['hidden-dosenpengampu'];
		$kuota			= $_POST['kuota'];
		$month 			= date('m');
		$year 			= date('Y');
		
		$cekicon = '';
		foreach ($_FILES['icon'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					echo $cekicon = 'error';
				}
			}
		}
		
		//------------------UPLOAD FILE START---------------------------------------------
		if($cekicon!='error'){
			foreach ($_FILES['icon'] as $id => $icon) {
				$brokeext			= $this->get_extension($icon);
				if($id == 'name'){
					$ext 	= $brokeext;
					$file	= $icon;
				}
				if($id == 'type'){
					$file_type = $_FILES['icon']['type'];
				}
				if($id == 'size'){
					$file_size = $_FILES['icon']['size'];
				}
				if($id == 'tmp_name'){
					$file_tmp_name = $_FILES['icon']['tmp_name'];
				}
			}
			$seleksi_ext 	= $mmk->get_ext($ext);
			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
		
				switch(strToLower($jenis)){
					case 'image':
					$extpath = "image";
					break;
				}
			}
			else{
				$this->redirect('module/akademik/mkditawarkan/index/failed');
			}
			$fkid = $mmk->get_fakultas_id($_POST['fakultas']);
			
			$upload_dir = 'assets/upload/file/thumb/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
			$upload_dir_db = 'upload/file/thumb/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;	
			
			// if (!file_exists($upload_dir)) {
				// mkdir($upload_dir, 0777, true);
				// chmod($upload_dir, 0777);
			// }
			$file_loc = $upload_dir_db . $file;
			
			$allowed_ext = array('jpg','jpeg','png','gif');
			if(!in_array($ext,$allowed_ext)){
				$this->redirect('module/akademik/mkditawarkan/index/wrongfiletype');
			}
			if($file_size > $maxfilesize){
				$this->redirect('module/akademik/mkditawarkan/index/toobig');
			}
		}
		else {
				$file_loc	= "notinc";
		}			
		//------------------UPLOAD FILE END-----------------------------------------------------
		
		
		if(isset($mkditawarkan_id, $matakuliahid, $thnakademik, $isblok)){

			if($_SERVER['REQUEST_METHOD'] == 'POST') {
				// rename($file_tmp_name, $upload_dir . $file);
				
				//------UPLOAD USING CURL----------------
				//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
				$url	     = $this->config->file_url;
				$filedata    = $file_tmp_name;
				$filename    = $file;
				$filenamenew = $file;
				$filesize    = $file_size;
			
				$headers = array("Content-Type:multipart/form-data");
				$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
	            $ch = curl_init();
	            $options = array(
	                CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
	            ); // cURL options 
				curl_setopt_array($ch, $options);
		        curl_exec($ch);
		        if(!curl_errno($ch))
		        {
		            $info = curl_getinfo($ch);
		            if ($info['http_code'] == 200)
		              echo $errmsg = "File uploaded successfully";
		        }
		        else
		        {
		            echo $errmsg = curl_error($ch);
		        }
		        curl_close($ch);
				//------UPLOAD USING CURL----------------
			}
			//----------------insert mkditawarkan---------------------
			if($file_loc=="notinc"){
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			else {
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'icon'=>$file_loc,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			$mmk->replace_mkditawarkan($datanya);
			
			//----------------end insert ditawarkan---------------------
			
			//----------------insert koor---------------------
			
			$dosenkoorstat = $mmk->get_status($dosenkoor);
			$pengampuidkoor = $mmk->get_pengampu_reg_number($mkditawarkan_id, $dosenkoorstat);
			
			$datanya2 	= Array(
								'pengampu_id'=>$pengampuidkoor, 
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'karyawan_id'=>$dosenkoorstat, 
								'is_koordinator'=>$iskoor
								);
			$mmk->replace_pengampu($datanya2);
			// echo $pengampuidkoor."<br>";
			//----------------end insert koor---------------------
			
			//---------------------insert pengampu------------------------
			foreach ($dosenpengampu as $d) {
				$dosen=explode(",",$d);
			}
			
			for($x=0;$x<count($dosen);$x++){
				$karyawanid = $mmk->get_status($dosen[$x]);
				
				$id = $mmk->get_pengampu_reg_number($mkditawarkan_id, $karyawanid);
				
				$datanya3 	= Array(
							'pengampu_id'=>$id, 
							'mkditawarkan_id'=>$mkditawarkan_id, 
							'karyawan_id'=>$karyawanid, 
							'is_koordinator'=>0
							);
				// echo $id."<br>";
				$mmk->replace_pengampu($datanya3);	
			}		
			//---------------------end insert pengampu------------------------
 			
				
			// $this->redirect('module/akademik/mkditawarkan/index/ok');
			// exit();
		}else{
			// $this->redirect('module/akademik/mkditawarkan/index/nok');
			// exit();
		 }
	}
	
	function save_editmkditawarkan_ToDB(){
		ob_start();
		$mmk = new model_mk();

		if($_POST['hidId']!=""){
			$mkditawarkan_id 	= $_POST['hidId'];
			$action 			= "update";
		}else{
			$mkditawarkan_id	= $mmkdtw->get_reg_number();	
			$action 			= "insert";	
		}
		
		$month 			= date('m');
		$year 			= date('Y');
		$userid			= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		$thnakademik	= $_POST['tahun_akademik'];
		$matakuliahid	= $mmk->get_mkid_from_namamk($_POST['namamk']);
		$kuota			= $_POST['kuota'];
		$cabangid		= $_POST['cabang'];
		
		if(isset($_POST['isblok'])!=""){
			$isblok	= $_POST['isblok'];
			$parent_id		= $_POST['parent_mk'];
		}else{
			$isblok	= 0;
			$parent_id		= "";
		}
		
		if(isset($_POST['iscourse'])&&$_POST['iscourse']!=""){//id is_course = 1 is_online = 0
			$isonline	= 0;
		}else{
			$isonline	= 1;
		}
		
		$cekicon = '';
		foreach ($_FILES['icon'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
			}
		}
		
		if($cekicon!='error'){
			//------------------UPLOAD FILE START---------------------------------------------
			foreach ($_FILES['icon'] as $id => $icon) {
				$brokeext			= $this->get_extension($icon);
				if($id == 'name'){
					$ext 	= $brokeext;
					$file	= $icon;
				}
				if($id == 'type'){
					$file_type = $_FILES['icon']['type'];
				}
				if($id == 'size'){
					$file_size = $_FILES['icon']['size'];
				}
				if($id == 'tmp_name'){
					$file_tmp_name = $_FILES['icon']['tmp_name'];
				}
			}
			$seleksi_ext 	= $mmk->get_ext($ext);
			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
		
				switch(strToLower($jenis)){
					case 'image':
					$extpath = "image";
					break;
				}
			}
			else{
				$this->redirect('module/akademik/mkditawarkan/index/failed');
			}
			$upload_dir = 'assets/upload/file/thumb/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
			$upload_dir_db = 'upload/file/thumb/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;	
			
			// if (!file_exists($upload_dir)) {
				// mkdir($upload_dir, 0777, true);
				// chmod($upload_dir, 0777);
			// }
			$file_loc = $upload_dir_db . $file;
			
			$allowed_ext = array('jpg','jpeg','png','gif');
			if(!in_array($ext,$allowed_ext)){
				$this->redirect('module/akademik/mkditawarkan/index/wrongfiletype');
			}
			if($file_size > $maxfilesize){
				$this->redirect('module/akademik/mkditawarkan/index/toobig');
			}
			
			if($_SERVER['REQUEST_METHOD'] == 'POST') {
				// rename($file_tmp_name, $upload_dir . $file);
				//------UPLOAD USING CURL----------------
				//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
				$url	     = $this->config->file_url;
				$filedata    = $file_tmp_name;
				$filename    = $file;
				$filenamenew = $file;
				$filesize    = $file_size;
			
				$headers = array("Content-Type:multipart/form-data");
				$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
	            $ch = curl_init();
	            $options = array(
	                CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
	            ); // cURL options 
				curl_setopt_array($ch, $options);
		        curl_exec($ch);
		        if(!curl_errno($ch))
		        {
		            $info = curl_getinfo($ch);
		            if ($info['http_code'] == 200)
		               $errmsg = "File uploaded successfully";
		        }
		        else
		        {
		            $errmsg = curl_error($ch);
		        }
		        curl_close($ch);
				//------UPLOAD USING CURL----------------
			}			
			//------------------UPLOAD FILE END-----------------------------------------------------
		}else{
			if(!($_POST['icon_loc'])){
				$file_loc	= "notinc";
			}else {
				$file_loc	= $_POST['icon_loc'];
			}
		}
		
		
		if(isset($mkditawarkan_id, $thnakademik, $isblok)){
			
			//----------------insert mkditawarkan---------------------
			if($file_loc=="notinc"){
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			else {
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'icon'=>$file_loc,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			$mmk->replace_mkditawarkan($datanya);
			
			//----------------end insert ditawarkan---------------------
			

			// $this->redirect('module/akademik/mkditawarkan/index/ok');
			// exit();
		}else{
			// $this->redirect('module/akademik/mkditawarkan/index/nok');
			// exit();
		 }
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	/*
	 * 
	 * -------END FUNCTION MK DITAWARKAN-----------
	 * 
	 * -------START FUNCTION SETTING MK------------
	 * 
	 * */
	
	function mk(){
		$mmk 		 	= new model_mk();		
		$mconf	  		= new model_conf();
		
		$user 		 	= $this->coms->authenticatedUser->role;
		$fakultasid 	= $this->coms->authenticatedUser->fakultas;
		$data['user']	= $user;
		
		//-- SIDEBAR --//
		$data['fakultasid'] 	= $fakultasid;
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
		}
		//-- SIDEBAR --//
		
		if($user=='student employee'||$user=='administrator'){
			if(isset($_POST['fakultas'])){
				$fakultas = $_POST['fakultas'];
				$data['fakultasid']=$fakultas;
				$data['posts']=$mmk->read_matakuliah('',$fakultas);
				// echo "isset";
			}else {
				$data['posts']=$mmk->read_matakuliah('','');
				// echo "!isset";
			}
		}else{
			if(isset($_POST['fakultas'])){
				$fakultas = $_POST['fakultas'];
				$data['fakultasid']=$fakultas;
			}else $fakultas=NULL;
			
			if($fakultas==NULL){
				$data['posts']=$mmk->read_matakuliah('',$fakultasid);
			}else{
				$data['posts']=$mmk->read_matakuliah('',$fakultas);
			}
		}
		
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/mk/mk.js');	
		
		if($user!="mahasiswa"){
			$this->view( 'mk/index.php', $data );
		}
	}
	
	function write_mk(){
		$mmk 		= new model_mk();
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
			
			$fakultasid 	 = $this->coms->authenticatedUser->fakultas;
			$data['user']	= $user;
			
			//-- SIDEBAR --//
			$data['fakultasid'] 	= $fakultasid;
			if($user=='dosen'||$user=='mahasiswa'){
				$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			}else{
				$data['get_fakultas']	= $mconf->get_fakultas();
			}
			//-- SIDEBAR --//
			
			$data['posts']		= "";
			// $data['getnamamk']	= $mmk->read_namamk("");
			$data['fakultas'] 	= $mconf->get_fakultas();
			$data['fakultasid'] = $this->coms->authenticatedUser->fakultas;
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->add_script('js/mk/mk.js');
	
			$this->view( 'mk/edit.php', $data );
		}
		
	}
	
	function edit_mk($id=NULL){
		$mmk	= new model_mk();
		$mconf	= new model_conf();
			
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
			
			$fakultasid 	 = $this->coms->authenticatedUser->fakultas;
			$data['user']	= $user;
			
			//-- SIDEBAR --//
			$data['fakultasid'] 	= $fakultasid;
			if($user=='dosen'||$user=='mahasiswa'){
				$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			}else{
				$data['get_fakultas']	= $mconf->get_fakultas();
			}
			//-- SIDEBAR --//
			
			$data['posts'] = $mmk->read_matakuliah($id,'');
			// $data['getnamamk']	=$mmk->read_namamk("");
			$data['fakultas'] = $mconf->get_fakultas();
			//$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->add_script('js/mk/mk.js');
			
			$this->view( 'mk/edit.php', $data );
		}
	}
	
	function saveToDBmk(){
		ob_start();
		$mmk	= new model_mk();
		
		$hid_id 	= $_POST['hidId'];
		$kodemk		= $_POST['kodemk'];
		$kurikulum	= $_POST['kurikulum'];
		$ceknew 	= $_POST['ceknew'];
		$namamk		= $mmk->cek_mk_from_namamk($_POST['namamk']);
		$kodemk		= $_POST['kodemk'];
		$cekkodebymk= $mmk->cek_kode_mk_by_mkid($hid_id);
		$eng_ver	= $_POST['namamkeng'];
		$strata		= $_POST['strata'];
		
		$kodemkcek		= $mmk->cek_kode_mk($_POST['kodemk']);		
		$idcek			= $mmk->cek_nama_mk($_POST['namamk'],$strata);
		$idmkcek		= $mmk->cek_mata_kuliah_id($_POST['namamk']);
		$kurikulumcek	= $mmk->cek_kurikulum_mk($_POST['kurikulum']);
		
		$user			= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		
		if($ceknew==1){
			if(isset($idcek)||isset($kodemkcek)){
				// $this->redirect('module/akademik/mk/indexmk/duplicate');
				echo 'Data Sudah ada!';
				exit();
			}
		}
		elseif (isset($idmkcek)&&$idmkcek!=$hid_id||isset($kodemkcek)&&$kodemkcek!=$cekkodebymk) {
			// $this->redirect('module/akademik/mk/indexmk/duplicate');
			echo 'Data Sudah ada!';
			exit();
		}


		if($_POST['hidId']!=""){
			$matakuliah_id 	= $hid_id;
			$namamkid		= $_POST['namamkid'];
			$action 	= "update";
		}else{
			$matakuliah_id	= $mmk->get_reg_numbermk();	
			$action 	= "insert";	
		}
		
		$fakultas	= $mmk->get_fakultas_id($_POST['fakultas']);
		$sks		= $_POST['sks'];
			
		if($action=="update"){
			if(isset($kodemk, $kurikulum, $sks)){
			
			$datanya 	= Array(
								'namamk_id'=>$namamkid,
								'fakultas_id'=>$fakultas, 
								'keterangan'=>$_POST['namamk'], 
								'english_version'=>$eng_ver
								);
			$mmk->replace_namamk($datanya);
			
			$datanya 	= Array(
								'matakuliah_id'=>$matakuliah_id, 
								'namamk_id'=>$namamkid, 
								'kode_mk'=>$kodemk, 
								'kurikulum'=>$kurikulum,
								'sks'=>$sks,
								'strata'=>$strata
								);
			$mmk->replace_matakuliah($datanya);
			// $this->redirect('module/akademik/mk/index/ok');
			echo 'Proses Edit Berhasil!';
			exit();
			}
			else{
			// $this->redirect('module/akademik/mk/index/nok');
			echo 'Proses Edit Gagal!';
			exit();
			}
		}
		else{
			if(isset($kodemk, $kurikulum, $sks, $fakultas)){
				$namamk	= $mmk->get_namamk_reg_number($_POST['namamk']);
				$keterangan	= $_POST['namamk'];

				
				//--------------------------insert ke tabel namamk-------------------------------	
		
				$datanya 	= Array(
								'namamk_id'=>$namamk, 
								'fakultas_id'=>$fakultas, 
								'keterangan'=>$keterangan, 
								'english_version'=>$eng_ver
								);
				$mmk->replace_namamk($datanya);
				
				//--------------------------END insert ke tabel namamk-------------------------------
				
				//--------------------------insert ke tabel mata kuliah-------------------------------
				
				$datanya 	= Array(
									'matakuliah_id'=>$matakuliah_id, 
									'namamk_id'=>$namamk, 
									'kode_mk'=>$kodemk, 
									'kurikulum'=>$kurikulum,
									'sks'=>$sks,
									'strata'=>$strata
									);
				$mmk->replace_matakuliah($datanya);
				
				//--------------------------END insert ke tabel mata kuliah-------------------------------
				
				// $this->redirect('module/akademik/mk/index/ok');
				echo 'Proses Simpan Berhasil!';
				exit();
				}
			else{
				// $this->redirect('module/akademik/mk/index/nok');
				echo 'Proses Simpan Gagal!';
				exit();
				}
			}
		
	}
	
	/*
	 * 
	 * START FUNCTION ABSEN ==============================================
	 * 
	 * */
	
	function absen(){
		$mabsen	= new model_absen();		
		
		$this->add_script('js/absen/absen.js');
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		$this->add_style('css/bootstrap/DT_bootstrap.css');
		$this->add_script('js/datatables/jquery.dataTables.js');	
		$this->add_script('js/datatables/DT_bootstrap.js');	

		$data['tahunAkademik'] = $mabsen->get_semester();
		$data['prodi'] = $mabsen->get_prodi();
		$data['dosen'] = $mabsen->get_dosen();
		$this->view( 'absen/index.php', $data );
	}
	
	function detail_absen(){
		$mabsen	= new model_absen();
		
		$data['role'] = $this->coms->authenticatedUser->role;
		
		if(isset($_POST['thnid']) && $_POST['thnid']!='0' && $_POST['thnid']!= 'undefined'){
			$thnid 	 = $_POST['thnid'];
		}else $thnid = NULL;
		
		if(isset($_POST['prodi']) && $_POST['prodi']!='0' && $_POST['prodi']!= 'undefined'){
			$prodiid   = $_POST['prodi'];
		}else $prodiid = NULL;
		
		if(isset($_POST['mk']) && $_POST['mk']!='0' && $_POST['mk']!= 'undefined'){
			$mk   = $_POST['mk'];
		}else $mk = NULL;
		
		if(isset($_POST['kelas']) && $_POST['kelas']!='0' && $_POST['kelas']!= 'undefined'){
			$kelas 	 = $_POST['kelas'];
		}else $kelas = NULL;
		
		if(isset($_POST['dosen']) && $_POST['dosen']!='0' && $_POST['dosen']!= 'undefined'){
			$dosen 	 = $_POST['dosen'];
		}else $dosen = NULL;
		
		if(isset($_POST['datemulai']) && $_POST['datemulai']!='' && $_POST['datemulai']!= 'undefined'){
			$datemulai 	 = $_POST['datemulai'];
		}else $datemulai = NULL;
		
		if(isset($_POST['dateselesai']) && $_POST['dateselesai']!='' && $_POST['dateselesai']!= 'undefined'){
			$dateselesai   = $_POST['dateselesai'];
		}else $dateselesai = NULL;
		
		$data['thnid'] 	 	 = $thnid;
		$data['prodiid']	 = $prodiid;
		$data['mk'] 		 = $mk;
		$data['kelas'] 		 = $kelas;
		$data['dosen'] 		 = $dosen;
		$data['datemulai']   = $datemulai;
		$data['dateselesai'] = $dateselesai;
		if(isset($_POST['view'])){
			$view = $_POST['view'];
		}else $view = 'view';
		$data['view_']		 = $view;
		// $data['absen'] = $mabsen->rekap_absen($thnid, $prodiid, $mk, $kelas, $dosen, $datemulai, $dateselesai);
		// if(!$prodiid && !$mk && !$kelas && !$dosen){
			$data['prodi'] 	 = $mabsen->get_prodi();
		// }
		$this->add_script('js/absen/absen.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->view( 'absen/detail.php', $data );
	}
	
	function save_absen(){
		$mabsen	= new model_absen();
		
		if(isset($_POST['hidId_absen'])&&$_POST['hidId_absen']!=""){
			$absenid = $_POST['hidId_absen'];
		}else $absenid = $mabsen->absen_id();
		
		if(isset($_POST['hidId_absendosen'])&&$_POST['hidId_absendosen']!=""){
			$absen_dosenid = $_POST['hidId_absendosen'];
		}else $absen_dosenid = $mabsen->absen_dosenid();
		
		$jadwal		= $_POST['jadwal-mk'];
		$tgl		= $_POST['tgl_pertemuan'];
		$jam_mulai	= $_POST['jam_mulai'];
		$jam_selesai= $_POST['jam_selesai'];
		$time1 		= strtotime($jam_mulai);
		$time2 		= strtotime($jam_selesai);
		$jumlah_jam = (($time2 - $time1)/60);
		$materi		= $_POST['materi'];
		$total	 	= $_POST['total'];
		$sesi		= $_POST['sesi'];
		$hadir		= 1;
		$dosenid 	= $_POST['dosen'];
		$last_update= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->email;
		
		$data_absen = array(
							'absen_id'=>$absenid,
							'jadwal_id'=>$jadwal,
							'total_pertemuan'=>$total,
							'tgl'=>$tgl,
							'jam_masuk'=>$jam_mulai,
							'jam_selesai'=>$jam_selesai,
							'jumlah_jam'=>$jumlah_jam,
							'materi'=>$materi,
							'sesi_ke'=>$sesi,
							'praktikum'=>0,
							'last_update'=>$last_update,
							'user'=>$user
						   );
		$save_absen = $mabsen->replace_absen($data_absen);
	
		$data_absendosen = array(
							'absendosen_id'=>$absen_dosenid,
							'dosen_id'=>$dosenid,
							'absen_id'=>$absenid,
							'is_hadir'=>1,
							'last_update'=>$last_update,
							'user'=>$user
						   );
		if($save_absen){
			$save_absendosen = $mabsen->replace_absen_dosen($data_absendosen);
			if($save_absendosen){
				echo "Berhasil";
				exit();
			}
			else{
				echo "Gagal";
				exit();
			}
		}
	}

	function hapus_absen(){
		$mabsen		= new model_absen();
		$absen_id 	= $_POST['absen'];
		$hapus		= $mabsen->delete_absen($absen_id);
		if($hapus){
			echo "Hapus data berhasil!";
		}
		else echo "Hapus data gagal!";
	}


	//===function tambahan===//
	
	function get_rekap($thnid, $prodiid, $mk, $kelas, $dosen, $datemulai, $dateselesai, $view_){
		$mabsen	= new model_absen();
		
		$data['no']	   = 1;
		$data['absen'] = $mabsen->rekap_absen($thnid, $prodiid, $mk, $kelas, $dosen, $datemulai, $dateselesai, 'bymk');
		
		$data['thnid'] 		= $thnid;
		$data['prodiid'] 	= $prodiid;
		$data['mk'] 		= $mk;
		$data['kelas'] 		= $kelas;
		$data['dosen'] 		= $dosen;
		$data['datemulai'] 	= $datemulai;
		$data['dateselesai'] = $dateselesai;
		$data['view_']		 = $view_;
		
		if($view_!='view'){
			$data['prodi'] = $mabsen->get_prodi($prodiid); //will group by prodi for print
		}else{
			$data['prodi'] = '';
		}

		$this->coms->add_style('bootstrap/css/bootstrap.min.css');
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$data['tahunAkademik'] = $mabsen->get_semester();
		$data['prodi']	= $mabsen->get_prodi();
		$data['dosen']	= $mabsen->get_dosen();
		$thn_aktif		= $mabsen->get_semester('aktif');
		// $data['jadwal']	= $mabsen->get_jadwal($thn_aktif->tahun_akademik, $dosen);
		
		$this->view( 'absen/rekap.php', $data );
	}
	
	function get_mk(){
		$mabsen	= new model_absen();	
		$dt = $_POST['tahunId'];
		
		if($_POST['tipe'] == 'dosen'){
			$mk = $mabsen->get_mk($dt);
		}
		else{
			$mk = $mabsen->get_mk($dt,0);
		}
		
		if($mk) :
			echo "<option value=''>Pilih Matakuliah</option>";
			foreach($mk as $key){
				$isi = $key->keterangan. ' - ' . $key->kelas_id . ' (' . $key->prodi_id . ")";
				echo "<option data-prodi='".$key->prodi_id."' value='".$isi. "|" .$key->kelas_id. "|" .$key->prodi_id. "|" .$key->mkditawarkan_id. "|" . $dt ."'>".$isi ."</option>";
			}
		else :
			echo "<option value=''>Matakuliah tidak ditemukan</option>";
		endif;
	}
	
	function get_dosen(){
		$mabsen	= new model_absen();		
		
		if($_POST['tipe'] == 'dosen'){
			// if($_POST['periodeId']==''){
				// $tahun = $_POST['tahunId'];
				// $dosen = $mabsen->get_dosen('','','',$tahun,1);
			// }else{
			$tahun = explode("|", $_POST['periodeId']);
			$dosen = $mabsen->get_dosen($tahun[1], $tahun[2],$tahun[3],$tahun[4]);
			// }
			
			if($dosen) :
				echo "<option value=''>Semua Dosen</option>";
				foreach($dosen as $key){
					$isi = $key->nama. ' - ' . $key->nik;
					echo "<option value='".$key->karyawan_id."'>".$isi ."</option>";
				}
			else :
				echo "<option value='all'>Dosen tidak ditemukan</option>";
			endif;
		}
		else{
			// $dosen = $mabsen->get_dosen($tahun[1], $tahun[2],$tahun[3],$tahun[4], 0);
// 			
			// if($dosen) :
				// echo "<option value='all'>Semua Mahasiswa</option>";
				// foreach($dosen as $key){
					// $isi = $key->nama. ' - ' . $key->nim;
					// echo "<option value='".$isi ."|". $tahun[4] . '|' .$key->mahasiswa_id."'>".$isi ."</option>";
				// }
			// else :
				// echo "<option value='all'>Mahasiswa tidak ditemukan</option>";
			// endif;
		}
	}

	function get_jadwal(){
		$mabsen			= new model_absen();
		$thn_aktif		= $mabsen->get_semester('aktif');
		$dosen 			= $_POST['karyawan'];
		$jadwal			= $mabsen->get_jadwal($thn_aktif->tahun_akademik, $dosen);
		if($jadwal){
			echo '<option value="0">Silahkan Pilih</option>';
			foreach($jadwal as $j){
				echo "<option value='".$j->jadwal_id."' start='".$j->jam_mulai."' finish='".$j->jam_selesai."'";
				echo "><strong>[".$j->kode_mk."]</strong> ".$j->nama_mk." - ".$j->kelas_id." [".$j->prodi."]</option>";
			}
		}
	}

	function get_absen(){
		$mabsen	= new model_absen();
		$absen 	= $_POST['absen'];
		$data	= $mabsen->rekap_absen("", "", "", "", "", "", "", "", "", $absen);
		
		$return_arr = array();
		if($data){
			foreach($data as $row => $value){
				$arr[$row] = $value;
			}
			array_push($return_arr,$arr);
			
			$json_response = json_encode($return_arr);
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}
		
}
?>
	