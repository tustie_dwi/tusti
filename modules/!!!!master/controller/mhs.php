<?php
class master_mhs extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mmhs = new model_mhs();
		$mconf = new model_general();	
		
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		
		$data['posts'] = $mmhs->read();
		$data['fakultas_id'] = $fakultas_id;
		$data['fakultas'] = $mconf->get_fakultas();
		$data['cabang'] = $mconf->get_cabangub();

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');	
		$this->add_script('js/conf/mhs.js');
		
		$this->view( 'mahasiswa/index.php', $data );
	}
	
	function tampilkan_indexangkatan()
	{
		$mmhs = new model_mhs();
		$fakultas_id = $_POST['fakultas_id'];
		$cabang = $_POST['cabang'];
		
		$angkatan 	= $mmhs->angkatan($fakultas_id,$cabang);
		echo "<option value='0'>Select Angkatan</option>" ;
		foreach($angkatan as $p )
		{
			echo "<option value='".$p->angkatan."'>".$p->angkatan."</option>" ;	
		}	
	}
	
	function tampilkan_index()
	{
		$mmhs 		= new model_mhs();

		$fakultas_id	= $_POST['fakultas_id'];
		$cabang 		= $_POST['cabang'];
		$angkatan 		= $_POST['angkatan'];
		
		$mhs 			= $mmhs->tampilkan_index($fakultas_id,$cabang,$angkatan);
		
		$data['mhs'] = $mhs;
		
				
		$this->view('mahasiswa/viewselection.php',$data);
	}

	function write(){
		$mconf = new model_general();	
		
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		$data['cabang'] = $mconf->get_cabangub();
		$data['prodi'] = $mconf->get_prodiub();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js'); 	
		$this->add_script('js/conf/mhs.js');
		
		$this->view( 'mahasiswa/edit.php',$data);
	}
	
	function edit($id=NULL){
		if(  !$id ) {
			$this->redirect('module/master/mhs');
			exit();
		}
		$mconf = new model_general();
		$mmhs = new model_mhs();	
		
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		$data['posts'] = $mmhs->read($id);
		$data['cabang'] = $mconf->get_cabangub();
		$data['prodi'] = $mconf->get_prodiub();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js'); 	
		$this->add_script('js/conf/mhs.js');
		
		$this->view( 'mahasiswa/edit.php',$data);
	}
	
	function detail($id=NULL){
		if(  !$id ) {
			$this->redirect('module/master/mhs');
			exit();
		}

		$mmhs = new model_mhs();
		$data['posts'] = $mmhs->read($id);
		
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');	
		$this->add_script('js/conf/mhs.js');
		
		$this->view( 'mahasiswa/detail.php',$data);
	}
	
	function getprodi(){
		$mconf = new model_general();	
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		$cabang = $_POST['cabang'];
		$prodi = $mconf->get_prodiub($fakultas_id,$cabang);
		
		echo "<option value='0'>Pilih Prodi</option>" ;
		foreach($prodi as $p )
		{
			echo "<option value='".$p->prodi_id."'>".$p->keterangan."</option>" ;	
		}
		
	}
	
	function savetoDB(){
		ob_start();
		
		$mmhs 		= new model_mhs();
		
		$ceknew 	= $_POST['ceknew'];
		$ceknama 	= $mmhs->ceknama_mhs($_POST['nama_mhs']);
		$cekemail	= $mmhs->cekemail_mhs($_POST['email_mhs']);
		
		if($ceknew==1){
			if(isset($ceknama)||isset($cekemail)){
				echo "Data sudah ada!";
				exit();
			}
		}
		
		if($_POST['hidId']!=""){
			$mhs_id 	= $_POST['hidId'];
			$action 	= "update";
		}else{
			$mhs_id		= $_POST['nim_mhs'];
			$action 	= "insert";	
		}
		
		$nama 			= $_POST['nama_mhs'];
		$jenis_kelamin	= $_POST['rjenis'];		
		$tmp_lahir 		= $_POST['tmpt_lahir_mhs'];
		$tgl_lahir		= $_POST['tgl_lahir_mhs'];
		$jalur_seleksi	= $_POST['jalur_seleksi_mhs'];
		$strata			= $_POST['strata_mhs'];
		$angkatan		= $_POST['angkatan_mhs'];
		$cabang_mhs		= $_POST['cabang_mhs'];
		$prodi_id		= $_POST['prodi_mhs'];
		$email			= $_POST['email_mhs'];
		$alamat			= $_POST['alamat_mhs'];
		$telp			= $_POST['tlp_mhs'];
		$hp				= $_POST['hp_mhs'];
		$nama_ortu		= $_POST['nama_ortu'];
		$email_ortu		= $_POST['email_ortu'];
		$alamat_ortu	= $_POST['alamat_ortu'];
		$telp_ortu		= $_POST['tlp_ortu'];
		$hp_ortu		= $_POST['hp_ortu'];
		$alamat_surat	= $_POST['alamat_surat'];
		$catatan		= $_POST['catatan_surat'];
		$user_id 		= $this->coms->authenticatedUser->id;
		$last_update	= date("Y-m-d H:i:s");
		if(isset($_POST['is_aktif'])){
			$is_aktif		= "aktif";
		}
		else $is_aktif		= "lulus";
		$tgl_mulai_studi_mhs	= $_POST['tgl_mulai_studi_mhs'];
		
		$fakultas = $mmhs->get_fakultas_by_prodi($prodi_id);
		
		
		//upload image//
		foreach ($_FILES['files'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		$month = date('m');
		$year = date('Y');
		if($cekicon!='error'){
				$name = $_FILES['files']['name'];
				$ext	= $this->get_extension($name);
				$seleksi_ext = $mmhs->get_ext($ext);
				if($seleksi_ext!=NULL){
						$jenis_file_id 	= $seleksi_ext->jenis_file_id;
						$jenis			= $seleksi_ext->keterangan;
						$maxfilesize	= $seleksi_ext->max_size;
						
						switch(strToLower($jenis)){
							case 'image':
								$extpath = "image";
							break;
						}
				}
				else{
						echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
						exit();
				}
				
				$allowed_ext = array('jpg','jpeg','png', 'gif');
				
				$icon_size=$_FILES["files"]["size"] ; 
				$ceknamaicon = $mmhs->cek_nama_icon($name);
				$upload_dir = 'assets/upload/foto/mhs/'.$fakultas. DIRECTORY_SEPARATOR.$angkatan. DIRECTORY_SEPARATOR;
				$upload_dir_db = 'upload/foto/mhs/'.$fakultas. DIRECTORY_SEPARATOR.$angkatan. DIRECTORY_SEPARATOR;
				
				if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
							chmod($upload_dir, 0777);
				}
				$nama_file = $mhs_id.".".$ext;
				$icon_loc = $upload_dir_db . $nama_file;
				if(!in_array($ext,$allowed_ext)){
						echo "Maaf, tipe file yang di upload salah";
						exit();
				}
				elseif (($ceknamaicon==NULL) && ($icon_size <= $maxfilesize)){
					// //echo $file_size;				
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
						//------UPLOAD USING CURL----------------
						//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
						$url	     = $this->config->file_url;
						$filedata    = $_FILES['files']['tmp_name'];
						$filename    = $nama_file;
						$filenamenew = $nama_file;
						$filesize    = $icon_size;
					
						$headers = array("Content-Type:multipart/form-data");
						$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
			            $ch = curl_init();
			            $options = array(
			                CURLOPT_URL => $url,
			                CURLOPT_HEADER => true,
							// CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
			                CURLOPT_POST => 1,
			                CURLOPT_HTTPHEADER => $headers,
			                CURLOPT_POSTFIELDS => $postfields,
			                CURLOPT_INFILESIZE => $filesize,
			                CURLOPT_RETURNTRANSFER => true,
			                CURLOPT_SSL_VERIFYPEER => false,
			  				CURLOPT_SSL_VERIFYHOST => 2
			            ); // cURL options 
						curl_setopt_array($ch, $options);
				        curl_exec($ch);
				        if(!curl_errno($ch))
				        {
				            $info = curl_getinfo($ch);
				            if ($info['http_code'] == 200)
				               $errmsg = "File uploaded successfully";
				        }
				        else
				        {
				            $errmsg = curl_error($ch);
				        }
				        curl_close($ch);
						//------UPLOAD USING CURL----------------
						// rename($_FILES['files']['tmp_name'], $upload_dir . $nama_file);
					}
				}
		}
		else{
			if(isset($_POST['hidimg'])){
				$icon_loc = $_POST['hidimg'];
			}
		}
	
		$datanya 	= Array(
						'mahasiswa_id'=>$mhs_id,
						'nim'=>$mhs_id, 
						'nama'=>$nama,
						'strata'=>$strata, 
						'angkatan'=>$angkatan,
						'jenis_kelamin'=>$jenis_kelamin,
						'tmp_lahir'=>$tmp_lahir,
						'tgl_lahir'=>$tgl_lahir,
						'alamat'=>$alamat, 
						'email'=>$email, 
						'hp'=>$hp, 
						'telp'=>$telp,
						'is_aktif'=>$is_aktif,
						'cabang_id'=>$cabang_mhs,
						'prodi_id'=>$prodi_id,
						'tgl_mulai_Studi'=>$tgl_mulai_studi_mhs,
						'jalur_seleksi'=>$jalur_seleksi,
						'nama_ortu'=>$nama_ortu,
						'alamat_ortu'=>$alamat_ortu,
						'catatan'=>$catatan,
						'alamat_surat'=>$alamat_surat,
						'telp_ortu'=>$telp_ortu,
						'hp_ortu'=>$hp_ortu,
						'email_ortu'=>$email_ortu,
						'user_id'=>$user_id,
						'last_update'=>$last_update
						);
		
		if($is_aktif=="lulus"){
			$ipk_lulus		= $_POST['ipk_lulus_mhs'];
			$tgl_lulus_mhs	= $_POST['tgl_lulus_mhs'];
			$masa_studi_mhs	= $_POST['masa_studi_mhs'];
			$tgl_selesai_studi_mhs	= $_POST['tgl_selesai_studi_mhs'];
			$datanya2 	= Array(
							'ipk_lulus'=>$ipk_lulus,
							'tgl_lulus'=>$tgl_lulus_mhs,
							'inf_masa_studi'=>$masa_studi_mhs,
							'tgl_selesai_Studi'=>$tgl_selesai_studi_mhs
							);
			$datanya = $datanya+$datanya2;
		}

		if(isset($icon_loc)){
			$datanya3 	= Array(
							'foto'=>$icon_loc
							);
			$datanya = $datanya+$datanya3;
		}
		
		$mmhs->replace_mhs($datanya);
		echo "Data berhasil disimpan";
		exit();
	}

	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
}
?>