<?php
class master_kuesioner extends comsmodule {
	private $coms;
	
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mconf		= new model_conf();
		$mkuesioner	= new model_kuesioner();
		
		$fakultas 	= $this->coms->authenticatedUser->fakultas;
		$data['fakultas'] 	= $mconf->get_unit("","","fakultas");
		if($fakultas=="-"){
			if(isset($_POST['fakultas'])){
				$fakultas		= $_POST['fakultas'];
			}
		}
		$data['prodi'] 	= $mconf->get_unit($fakultas,"","prodi");
		$data['mk']		= $mkuesioner->get_mk($fakultas);
		if(isset($_POST['id'])&&$_POST['id']!=""){
			$id = $_POST['id'];
			$data['edit']	= $mkuesioner->read($id,"notall");
			$data['detail']	= $mkuesioner->read($id,"all");
		}
		$data['posts']	= $mkuesioner->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.css');
		$this->coms->add_script('js/select2/select2.js');
		$this->coms->add_style('js/select2/select2.css');
		$this->add_script('js/kuesioner/kuesioner.js');
		
		$this->view('kuesioner/index.php',$data);
	}

	function save(){
		$mconf		= new model_conf();
		$mconfinfo	= new model_confinfo();
		$mkuesioner	= new model_kuesioner();
		
		$tahun		= $_POST['tahun'];
		$semester	= $_POST['semester'];
		$judul		= $_POST['judul'];
		$keterangan	= $_POST['keterangan'];
		$tgl_mulai	= $_POST['tgl_mulai'];
		$tgl_selesai= $_POST['tgl_selesai'];
		
		if($semester=="ganjil"){
			$isganjil	= "01";
		}else $isganjil	= "02";
		
		if(isset($_POST['is_aktif'])){
			$is_aktif	= 1;
		}else $is_aktif	= 0;
		
		if(isset($_POST['is_pendek'])){
			$ispendek	= "01";
			$is_pendek	= "Pendek";
		}else{
			$ispendek	= "00";
			$is_pendek	= "";
		}
		
		$user 			= $this->coms->authenticatedUser->id;
		$last_update	= date("Y-m-d H:i:s");
		
		//cek tabel tahun_akademik
		$cek_tahun_akademik	= $mkuesioner->cek_tahun_akademik($tahun, $semester, $is_pendek);
		$thn_akademik = $tahun.$isganjil.$ispendek;
		if(!$cek_tahun_akademik){
			$data_semester	= array(
									'tahun_akademik'=>$thn_akademik,
									'tahun'=>$tahun,
									'is_ganjil'=>$semester,
									'is_aktif'=>'0',
									'is_pendek'=>$is_pendek,
									'user_id'=>$user,
									'last_update'=>$last_update
								   );
			$mconfinfo->replace_semester($data_semester);
		}
		
		//insert ke tabel kuesioner mk
		if(isset($_POST['kuesionerid'])&&$_POST['kuesionerid']!=""){
			$kuesioner_id	= $_POST['kuesionerid'];
		}
		else $kuesioner_id	= $mkuesioner->kuesioner_id();
		
		$data_kuesioner	= array(
								'kuesioner_id'=>$kuesioner_id,
								'judul'=>$judul,
								'keterangan'=>$keterangan,
								'tahun_akademik'=>$thn_akademik,
								'tahun'=>$tahun,
								'is_ganjil'=>$semester,
								'is_pendek'=>$is_pendek,
								'is_aktif'=>$is_aktif,
								'tgl_mulai'=>$tgl_mulai,
								'tgl_selesai'=>$tgl_selesai,
								'user_id'=>$user,
								'last_update'=>$last_update
							    );
		$save_kuesioner_mk	= $mkuesioner->replace_kuesioner_mk($data_kuesioner);
		
		//MK
		$fakultas	= $_POST['fakultas'];
		$data_prodi	= $mconf->get_unit($fakultas,"","prodi");
		foreach($data_prodi as $dt){
			$mk_by_prodi	= $_POST[$dt->unit_id];
			$cek_mk_edit	= $mkuesioner->read($id,"notall",$dt->unit_id);
			foreach($cek_mk_edit as $cek){
				if(!in_array($cek->matakuliah_id, $mk_by_prodi)){
					$deleted_mk[] = $cek->matakuliah_id;
				}
			}
			for($i=0;$i<count($mk_by_prodi);$i++){
				$detail_id	= $mkuesioner->detail_id();
				$mk			= $mk_by_prodi[$i];
			
				$data_mk	= $mkuesioner->get_mk($fakultas,$mk);
				$data_detail= array(
									'detail_id'=>$detail_id,
									'matakuliah_id'=>$mk,
									'kuesioner_id'=>$kuesioner_id,
									'nama_mk'=>$data_mk->nama_mk,
									'kode_mk'=>$data_mk->kode_mk,
									'sks'=>$data_mk->sks,
									'prodi_id'=>$dt->unit_id
								   );
				//$save_kuesioner_detail	= $mkuesioner->replace_kuesioner_detail($data_detail);
				$save_kuesioner_detail	= "";
			}
		}
		
		if($save_kuesioner_detail){
			echo "Berhasil";
		}
		else{
			echo "Gagal";
		}
		
	}
}
?>