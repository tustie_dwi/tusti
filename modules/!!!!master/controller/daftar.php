<?php
class master_daftar extends comsmodule {
	
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function daftarulang(){
		$mservices	= new model_services();
		$mconfinfo	= new model_confinfo();
		$mmhs 		= new model_mhs();
		$mconf		= new model_general();
		
		$fakultas_id 			= $this->coms->authenticatedUser->fakultas;
		$data['fakultas_id'] 	= $fakultas_id;
		$data['fakultas'] 		= $mconf->get_fakultas();
		$data['cabang'] 		= $mconf->get_cabangub();
		$data['thn_akademik']	= $mconfinfo->get_semester();
		
		$this->coms->add_script('js/excellentexport.js');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/daftarulang/daftarulang.js');
		
		
		$this->view('daftarulang/index.php',$data);
	}
	
	function detail($id=NULL){
		if(  !$id ) {
			$this->redirect('module/master/daftar/daftarulang');
			exit();
		}
		
		$mservices	= new model_services();
		$mconfinfo	= new model_confinfo();
		
		$thn_akademik 	= $mconfinfo->get_semester_aktif()->tahun_akademik;
		
		$mhs 			= $mservices->read_daftar_ulang($thn_akademik,$id,"","","*");
		
		$data['posts'] = $mhs;
		
				
		$this->view('daftarulang/detail.php',$data);
	}
	
	function tampilkan_indexangkatan()
	{
		$mmhs = new model_mhs();
		$fakultas_id = $_POST['fakultas_id'];
		$cabang = $_POST['cabang'];
		
		$angkatan 	= $mmhs->angkatan($fakultas_id,$cabang);
		echo "<option value='0'>Select Angkatan</option>" ;
		if($angkatan){
			echo '<option value="*">Tampilkan Semua Angkatan</option>';
			foreach($angkatan as $p )
			{
				echo "<option value='".$p->angkatan."'>".$p->angkatan."</option>" ;	
			}
		}	
	}
	
	function tampilkan_index()
	{
		$mservices	= new model_services();
		$mconfinfo	= new model_confinfo();
		
		$fakultas_id	= $_POST['fakultas_id'];
		$cabang 		= $_POST['cabang'];
		$valid 			= $_POST['data'];
		$thn_akademik 	= $_POST['tahun_akademik'];
		$angkatan 		= $_POST['angkatan'];
		
		$mhs 			= $mservices->read_daftar_ulang($thn_akademik,"",$fakultas_id,$cabang,$angkatan,$valid);
		
		$data['mhs'] 	= $mhs;
		
				
		$this->view('daftarulang/viewselection.php',$data);
	}
	
	function validate(){
		$daftarid	= $_POST['id'];
		$mhsid		= $_POST['mhs'];
		if($daftarid!="" && $mhsid!=""){
			$mservices		= new model_services();
			$mconfinfo		= new model_confinfo();
			$userid			= $this->coms->authenticatedUser->id;
			$thn_akademik 	= $mconfinfo->get_semester_aktif()->tahun_akademik;
			$mhs 			= $mservices->read_daftar_ulang($thn_akademik,$mhsid,"","","*");
			$validasi		= $mservices->validate_daftarulang($daftarid, $mhsid, $mhs,$userid);
			if($validasi){
				echo "Berhasil";
			}
			else echo "Gagal";
		}
	}
	
	function export(){
		$mservices	= new model_services();
		$mconfinfo	= new model_confinfo();
		
		$thn_akademik 	= $_POST['tahun_akademik'];
		$fakultas_id	= $_POST['fakultas_id'];
		$cabang 		= $_POST['cabang'];
		$valid 			= $_POST['data'];
		$angkatan 		= $_POST['angkatan'];
		
		$data['cols'] 	= $mservices->read_daftar_ulang_report($thn_akademik,$fakultas_id,$cabang,$angkatan,$valid,'1');
		$data['mhs'] 	= $mservices->read_daftar_ulang_report($thn_akademik,$fakultas_id,$cabang,$angkatan,$valid,'0');
		
		$this->coms->add_script('js/jsread.js');
		
		$this->view( 'daftarulang/export.php', $data );
	}
}
?>