<?php
class master_gjm extends comsmodule {
	
	//---kenaikan belum edit--------------------------
	
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($str=NULL, $id=NULL){
		if(isset($_POST['b_submit'])) $this->saveToDB(); 
		
		$mconf 	= new model_general();	
		$mmutu	= new model_gjm();
		
		$level = $this->coms->authenticatedUser->level;
		
		if($level=='1'){
			$data['fakultas']	= $mconf->get_fakultas();
		}else{
			$data['fakultas']	= $mconf->get_fakultas($this->coms->authenticatedUser->fakultas);
		}
		
		$data['cabang']		= $mconf->get_cabang();
		$data['fakultas_id']= $this->coms->authenticatedUser->fakultas; 
		$data['kategori']	= $mmutu->read_category();
		$data['mconf']		= $mconf;
		
		if(isset($_POST['fakultas'])){
			$fakultas = $_POST['fakultas'];
			$data['fakultas_id']=$fakultas;
		}else $fakultas=NULL;
		
		if(isset($_POST['cabang'])&&$_POST['cabang']!='0'){
			$cabang = $_POST['cabang'];
			$data['cabangid']=$cabang;
		}else $cabang=NULL;
		
		if(isset($_POST['unit'])){
			$unit = $_POST['unit'];
			$data['unitid']=$unit;
		}else $unit=NULL;
		
		$data['posts'] = $mmutu->read($id, $fakultas, $cabang);
	
		//$data['unit_master']= $mconf->get_unit($fakultas);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		$this->add_script('js/gjm/gjm.js');
		
		if($str=='write'){
			$data['form'] = 'write';
		}
		
		if($id) $data['edit'] = 1;
			
		$this->view('gjm/index.php', $data );
	}
	
	function read_unit(){
		$mconf	    	= new model_conf();
		
		$fakultasid 	= $_POST['fakultasid'];
		$unitid 		= $_POST['unit_id'];
		$unit	= $mconf->get_unit($fakultasid,'getfakultas');
		echo '<option value="0">Pilih Unit</option>';
		if(isset($unit)){
			foreach($unit as $u) :
				echo '<optgroup label="'.$u->fakultas.'">';
				$this->read_unit_child($u->fakultas_id,$unitid);
				echo '</optgroup>';
			endforeach;
		}
	}
	
	function read_unit_child($fakultasid,$unitid){
		$mconf	    = new model_conf();	
		$unit 		= $mconf->get_unit($fakultasid,'');
		
		foreach($unit as $u) :
			echo "<option value='".$u->unit_id."' ";
			if(isset($unitid)){
				if($unitid==$u->unit_id){
					echo "selected";
				}
			}
			echo " >".ucWords($u->keterangan)."</option>";
		endforeach;
	}
	
	
	function detail($id){
		if(  !$id ) {
			$this->redirect('module/master/staff');
			exit;
		}
		$mconf 	= new model_dosen();			
		
		$data['posts'] 		= $mconf->read($id, '', '');
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->view('staff/detail.php', $data);
		
	}
	
	function write(){
		$this->index('write');
		exit;		
	}	
	
	function edit($id){
		$this->index('write', $id);
		exit;	
	}
	
	
	function saveToDB(){
			ob_start();
		$mconf 	= new model_gjm();
		
		//---------------DATA----------------------------------------
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		if(isset($_POST['hidid']) && ($_POST['hidid'])) $fileid=$_POST['hidid'];		
		else $fileid=$mconf->get_reg_number();
		//$id=$mconf->get_reg_number();
		
	
		$fak 		= $_POST['fakultas'];
		$cabang 	= $_POST['cabang'];
		$unit 		= $_POST['unit'];
		$judul		= $_POST['judul'];
		$judul_en 	= $_POST['judul_en'];
		$keterangan = $_POST['keterangan'];
		$no_dok		= $_POST['no_dok'];
		$kategori	= $_POST['cmbkategori'];
		$siklus		= $_POST['siklus'];
				
		if(isset($_POST['chkpublish'])) $ispublish = $_POST['chkpublish'];
		else $ispublish = 0;
		
		$i=0;
		
	
			foreach ($_FILES['uploads']['name'] as $id => $file){
				$lastupdate	= date("Y-m-d H:i:s");
				$name  = $_FILES['uploads']['name'][$id];				
			
				if($name):
						$ext   = $this->get_extension($name);
						switch(strToLower($ext)){
							case 'jpg':
							case 'jpeg':
							case 'png':
							case 'gif':
								$jenisfile = "image";
								break;
							case 'doc':
							case 'ppt':
							case 'pdf':
							case 'xls':
							case 'docx':
							case 'pptx':
							case 'xlsx':
								$jenisfile = "document";
								break;
						}
						$file_type=$_FILES["uploads"]["type"][$id];
						$file_size=$_FILES["uploads"]["size"][$id];
						$upload_dir = 'assets/upload/file/'.$fak. '/gjm/'.$kategori.'/';			
						
						$upload_dir_db = 'upload/file/'.$fak. '/gjm/'.$kategori.'/';
						
						
						$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx');
						if(!in_array($ext,$allowed_ext)){
							echo "Maaf, tipe data file yang anda upload tidak diperbolehkan!";
							exit();
						}
						else{
							 if (!file_exists($upload_dir)) {
								 mkdir($upload_dir, 0777, true);
								 chmod($upload_dir, 0777);
							 }
							$file_loc = $upload_dir_db . $name;
							if($_SERVER['REQUEST_METHOD'] == 'POST') {
								rename($_FILES['uploads']['tmp_name'][$id], $upload_dir . $name);
								//------UPLOAD USING CURL----------------
								//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
								$url	     = $this->config->file_url;
								$filedata    = $_FILES["uploads"]['tmp_name'][$id];
								$filename    = $name;
								$filenamenew = $name;
								$filesize    = $file_size;
							
								$headers = array("Content-Type:multipart/form-data");
								$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
								$ch = curl_init();
								$options = array(
											CURLOPT_URL => $url,
									CURLOPT_HEADER => true,
									CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
									CURLOPT_POST => 1,
									CURLOPT_HTTPHEADER => $headers,
									CURLOPT_POSTFIELDS => $postfields,
									CURLOPT_INFILESIZE => $filesize,
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_SSL_VERIFYPEER => false,
									CURLOPT_SSL_VERIFYHOST => 2
								); // cURL options 
								curl_setopt_array($ch, $options);
								curl_exec($ch);
								if(!curl_errno($ch))
								{
									$info = curl_getinfo($ch);
									if ($info['http_code'] == 200)
									   $errmsg = "File uploaded successfully";
								}
								else
								{
									$errmsg = curl_error($ch);
								}
								curl_close($ch);
								//------UPLOAD USING CURL----------------
							}
							
							//$file_id = $mdocument->file_id();
							$datanya 	= Array('file_id' => $fileid,
												'jenis_file' => $jenisfile,
												'kategori_id'=>$kategori,
												'unit_id'=>$unit,
												'judul' => $judul,
												'english_version'=>$judul_en,
												'keterangan' => $keterangan,
												'file_name' => $name,
												'file_type' => $file_type,
												'file_loc' => $file_loc,
												'file_size' => $file_size,
												'is_publish'=>$ispublish,
												'siklus'=>$siklus,
												'no_dokumen'=>$no_dok,
												'user_id' => $user,
												'last_update' => $lastupdate
												);
							$mconf->replace_gjm($datanya);
							
							//echo $title[$i]."\n".$name."\n".$file_type."\n".$file_size."\n".$jenisfile."\n";
							$i++;
						}
				else:
					$idnya = array('file_id' => $fileid);
					$datanya 	= Array('jenis_file' => $jenisfile,
										'kategori_id'=>$kategori,
										'unit_id'	=> $unit,
										'judul' => $judul,
										'english_version'=>$judul_en,
										'keterangan' => $keterangan,										
										'is_publish'=>$ispublish,
										'siklus'=>$siklus,
										'no_dokumen'=>$no_dok,
										'user_id' => $user,
										'last_update' => $lastupdate
										);
					$mconf->update_gjm($datanya, $idnya);
				endif;
			}	
		
		echo "Upload file Success!";
		$this->redirect('module/master/gjm');
		exit();
		
	}	
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
}
?>