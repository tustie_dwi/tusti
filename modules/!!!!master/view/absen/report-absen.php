<link rel="stylesheet" type="text/css" href="<?php echo $this->asset("css/info-inside/style-print.min.css");?>" />
	<style>
	.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead{ border-top:solid 1px #444;}
		.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{
			font-size:12px;
			border: solid 1px #444;
		}
		table{ font-size:12px;
			border: solid 0px #fff; }
	</style>
	<div class="content-print row" id="content-print">
		<?php 			
			if($prodiid){
					if($type=="dosen"){
						$this->get_rekap($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
					}
					else{
						$this->get_rekap_asisten($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
					}
				}else{
					if($view_=='view'){
						if($type=="dosen"){
							$this->get_rekap($thnid, '', $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
						}
						else{
							$this->get_rekap_asisten($thnid, '', $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
						}
					}
					else{
						
							foreach ($prodi as $p){
								echo "<div id='content_print'><h2>Program Studi " .$p->keterangan."</h2>";
								if($type=="dosen"){
									$this->get_rekap($thnid, $p->prodi_id, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
								}
								else{
									$this->get_rekap_asisten($thnid, $p->prodi_id, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
								}
								?>
								<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
								<div class="page-break" id="footer"></div>
								<?php
							}
							
					} //will group by prodi for print
			}
		?>
	</div>
