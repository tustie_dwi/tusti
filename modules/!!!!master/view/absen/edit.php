<?php $this->head(); ?>
<fieldset id="cetak">
	<legend>
		<?php if($param == 'edit'){ ?>
			<button class="btn btn-primary pull-right btn_cetak"><i class="fa fa-plus"></i> Tambah Absen</button>
		<?php } ?>
		<h3>Tambah Absen</h3>
	</legend>
	
	<div class="col-md-12">
		<form id="form-absen-dosen">
			<div class="control-group">
		        <label class="control-label">Jadwal Matakuliah</label>
		        <div class="controls">
		        	<select name="jadwal-mk" class="e9">
		        		<option value="0">Silahkan Pilih</option>
		        		<?php
		        			foreach($jadwal as $j){
		        				echo "<option value='".$j->jadwal_id."' start='".$j->jam_mulai."' finish='".$j->jam_selesai."'";
								echo "><strong>[".$j->kode_mk."]</strong> ".$j->nama_mk." - ".$j->kelas_id." [".$j->prodi."]</option>";
		        			}
		        		?>
		        	</select>
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label">Tanggal</label>
		        <div class="controls">
		        	<input type="text" class="date" placeholder="Masukkan Tanggal Pertemuan"/>
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label">Jam Mulai</label>
		        <div class="controls">
		        	<input type="text" class="time" name="jam_mulai" placeholder="Masukkan Jam Mulai"/>
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label">Jam Selesai</label>
		        <div class="controls">
		        	<input type="text" class="time" name="jam_selesai" placeholder="Masukkan Jam Selesai"/>
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label">Materi</label>
		        <div class="controls">
		        	<input type="text" name="materi" placeholder="Masukkan Nama Materi"/>
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label">Sesi</label>
		        <div class="controls">
		        	<select name="sesi" class="e9">
		        		<?php for($i=1;$i<21;$i++){ ?>
		        			<option value="<?php echo $i ?>">Sesi <?php echo $i ?></option>
		        		<?php } ?>
		        	</select>
		        </div>
		    </div>
			<div class="control-group">
		        <label class="control-label">Kehadiran</label>
		        <div class="controls">
		        	<label class="radio-inline">
		        		<input type="radio" name="hadir" value="1"/> Hadir
		        	</label>
		        	<label class="radio-inline">
		        		<input type="radio" name="hadir" value="0"/> Tidak Hadir
		        	</label>
		        </div>
		    </div>	 
		    <div class="control-group">
		        <label class="control-label"></label>
		        <div class="controls">
		        	<input type="hidden" name="hidId" value=""/>
		        	<button type="submit" class="btn btn-primary">Data Valid & Save</button>
		        	<button type="button" class="btn btn-danger">Cancel</button>
		        </div>
		    </div>   
	    </form>
	</div>
	
</fieldset>
<?php $this->foot(); ?>