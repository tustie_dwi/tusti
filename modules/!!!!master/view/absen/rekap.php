<?php if(isset($absen)): ?>
	
<?php 
$num = 1;
$kryn='';
$karyawannum = Array();
// var_dump($absen);
foreach ($absen as $dt) {
	if(isset($dt->karyawan_id)){
		echo "masuk";
		if($kryn == $dt->karyawan_id){
			$karyawannum[$dt->karyawan_id] = $num++;
		}else{
			$num = 1;
			$karyawannum[$dt->karyawan_id] = 0;
		}
		$kryn = $dt->karyawan_id;
		$type = "dosen";
	}
	elseif(isset($dt->mahasiswa_id)){
		if($kryn == $dt->mahasiswa_id){
			$karyawannum[$dt->mahasiswa_id] = $num++;
		}else{
			$num = 1;
			$karyawannum[$dt->mahasiswa_id] = 0;
		}
		$kryn = $dt->mahasiswa_id;
		$type = "mahasiswa";
	}
}
//echo $type;
// print_r($karyawannum);
?>

	
<?php //echo $prodi; ?>
<table class="table table-bordered table-condensed">
<thead>
	<tr>
		<th><center>No</center></th>
		<th><center>Nama</center></th>
		<?php if($type=="dosen"){ ?>
		<th><center>Gol</center></th>
		<?php } ?>
		<th><center>Mata Kuliah</center></th>
		<?php if($type=="dosen"){ ?>
		<th><center>SKS</center></th>
		<?php } ?>
		<th><center>Jumlah Absensi</center></th>
	</tr>
</thead>
<tbody>
	
	<?php $num = 1; $kryn='';?>
	<?php foreach ($absen as $dt) { 
			$praktikum = $mconf->get_mk_praktikum($dt->mkditawarkan_id);
				if($praktikum):
					$ispraktikum = 1;
					$sks_valid = $dt->sks - 1;
					$strclass= "panel-danger";
				else:
					$ispraktikum = "";
					$sks_valid = $dt->sks;
					$strclass= "panel-default";
				endif;
			?>
	<?php if(isset($dt->karyawan_id)){ ?>
	
	<tr>
		<?php if($kryn != $dt->karyawan_id){ ?>
			<td align="center" valign="center" <?php if($kryn != $dt->karyawan_id)echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?> >
			<?php 
			if($kryn != $dt->karyawan_id){
				echo $no++;
			}
			?>
			</td>
			<td <?php if($kryn != $dt->karyawan_id)echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?>>
				<?php 
				if($kryn != $dt->karyawan_id){
					echo $dt->nama;
					if($dt->nik=='') echo "<br><small><strong>NIK. -</strong></small>";
					else echo "<br><small><strong>NIK. ". $dt->nik ."</strong></small>";
				}
				?>
				
			</td>
			<td <?php if($kryn != $dt->karyawan_id) echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?>>
				<?php if($kryn != $dt->karyawan_id){ echo $dt->golongan; } ?>
				<?php $kryn = $dt->karyawan_id; ?>
			</td>
		<?php } ?>
		
		<td width="50%">
			<?php if($view=='cetak'):							
					echo "<strong>[".$dt->kode_mk."]</strong> ";
					echo $dt->namamk." - ".$dt->kelas;
					echo " [".$dt->prodi_mk."]";
					if($ispraktikum) echo "*";			
			?>
			<?php else: ?>
			<div class="panel-group" id="accordion">
			  <div class="panel <?php echo $strclass ?>">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" href="#<?php $link = str_replace(' ', '', $dt->namamk." - ".$dt->kelas." - ".$dt->prodi_mk); echo str_replace(str_split('&/.'), '', $link); ?>" style="font-size: 13px;">
						<?php 
							
							echo "<strong>[".$dt->kode_mk."]</strong> ";
							echo $dt->namamk." - ".$dt->kelas;
							echo " [".$dt->prodi_mk."]";
							if($ispraktikum) echo "*";
						?>
					 </a>
      			  </h4>
   			 	</div>
   			 	<?php if($view_!="cetak"){ ?>
   			 	<div id="<?php $link = str_replace(' ', '', $dt->namamk." - ".$dt->kelas." - ".$dt->prodi_mk); echo str_replace(str_split('&/.'), '', $link); ?>" class="panel-collapse collapse">
      				<div class="panel-body">
      					<?php //echo $dt->jadwal_id;
      						//$mabsen = new model_absen();
							$absen_dosen = $mconf->rekap_absen($dt->tahun_akademik, $dt->prodi_id, $dt->mkditawarkan_id, $dt->kelas, $dt->karyawan_id, $datemulai, $dateselesai, "byjadwal", $dt->jadwal_id);
      					?>
      					<?php //echo $thn_aktif."<br>".$dt->tahun_akademik."<br>".$level ?>
      					<?php if(($dt->tahun_akademik == $thn_aktif) && ($level!=2)){ ?>
      					<a href="javscript:" class="btn btn-primary tambah_absen" data-toggle="modal" data-target="#myModal" onclick="add_absen('<?php echo $dt->karyawan_id ?>','<?php echo $dt->pengampu_id ?>','<?php echo $dt->jadwal_id ?>')"><i class="fa fa-plus"></i> Tambah Absen</a>
      					<?php } ?>
      					<table class="table noborder example" style="font-size: 12px;">
      						<thead>
      							<tr>
	      							<th>Hari / Tanggal</th>
	      							<th>Ruang</th>
	      							<th>Materi</th>
	      							<th>Kehadiran</th>
	      							<th class="action_btn">&nbsp;</th>
      							</tr>
      						</thead>
      						<tbody>
      							<?php foreach($absen_dosen as $a){
      									$tgl = explode("-",$a->tgl_pertemuan);
										$tgl = $tgl[2]."-".$tgl[1]."-".$tgl[0];
	      								if($a->is_hadir==1){
	      									$hadir = "Hadir";
	      								}else $hadir = "Tidak Hadir";
	      						?>
	      							<tr>
	      								<td>
	      									<?php
	      										$day = date("l", strtotime($tgl));
												switch($day){
													case "Monday" : $hari = "Senin";
																	break;
													case "Tuesday" : $hari = "Selasa";
																	break;
													case "Wednesday" : $hari = "Rabu";
																	break;
													case "Thursday" : $hari = "Kamis";
																	break;
													case "Friday" : $hari = "Jumat";
																	break;
													case "Saturday" : $hari = "Sabtu";
																	break;
													case "Sunday" : $hari = "Minggu";
																	break;
												}; 
	      										echo ucfirst($hari) .", ".$tgl."<br>"; 
												echo "<i class='fa fa-clock-o'></i> ".substr($a->jam_masuk,0,5)." - ";
												echo substr($a->jam_selesai,0,5); 
												
												echo "<br><small><span class='text text-danger'><em>by ".$a->user."<br><i class='fa fa-clock-o'></i> ".$a->last_update."</em></span></small>";
											?>
										</td>
	      								<td><?php echo $a->ruang_id ?></td>
	      								<td><?php if($a->materi) echo ucfirst($a->materi); else echo "&nbsp;"; ?></td>
	      								<td><?php echo $hadir; ?></td>
	      								<td class="action_btn" width="10%">
											<?php 
											$terbayar = $mconf->get_bayar($a->absendosen_id);
											
											if(($level==8 ||  $level==5)&& (! $terbayar)){
											
											?>
	      									<a href="#" title="Edit" data-toggle="modal" data-target="#myModal" onclick="edit_absen('<?php echo $a->hidId; ?>', '<?php echo $a->karyawan_id; ?>')"><i class="fa fa-edit" style="color: blue;"></i></a>&nbsp;&nbsp;&nbsp;
	      									<a href="#" title="Delete" onclick="delete_absen('<?php echo $a->hidId; ?>')"><i class="fa fa-trash-o" style="color: red;"></i></a>
											<br>
											<?php } 
											
											if($level==1){
											
											?>
	      									<a href="#" title="Edit" data-toggle="modal" data-target="#myModal" onclick="edit_absen('<?php echo $a->hidId; ?>', '<?php echo $a->karyawan_id; ?>')"><i class="fa fa-edit" style="color: blue;"></i></a>&nbsp;&nbsp;&nbsp;
	      									<a href="#" title="Delete" onclick="delete_absen('<?php echo $a->hidId; ?>')"><i class="fa fa-trash-o" style="color: red;"></i></a>
											<br>
											<?php } 
											
											if($terbayar) echo "<span class='text text-danger'><small>* proses spj on </small><b>".$terbayar->last_update."</b></span>";
											else echo "<small><b>* Belum SPJ</b></small>";
											?>
	      								</td>
	      							</tr>
      							<?php } ?>
      						</tbody>
      					</table>
      				</div>
  				</div>
  				<?php } ?>
			</div>
		   </div>
		   <?php endif;?>
		</td>
		<td><center>
			<?php 
				if($sks_valid > 3){
					$sks= ($sks_valid/2);
					//echo $dt->sks;
				}else{
					$sks= $sks_valid;					
				}				
			?><h4><?php echo $sks ?> sks</h4></center>
		</td>
		<td><center>
			<h4><span class="text text-danger"><?php echo $dt->jml; ?></span></h4></center>
		</td>
	</tr>
	<?php } ?>
	
	
	<!--ASISTEN-->
	<?php if(isset($dt->mahasiswa_id)){ ?>
		<tr>
			<?php if($kryn != $dt->mahasiswa_id){ ?>
				<td align="center" valign="center" <?php if($kryn != $dt->mahasiswa_id)echo 'rowspan="'.($karyawannum[$dt->mahasiswa_id]+1).'"' ?> >
				<?php 
				if($kryn != $dt->mahasiswa_id){
					$index = $no;
					echo $no++;
				}
				?>
				</td>
				<td <?php if($kryn != $dt->mahasiswa_id)echo 'rowspan="'.($karyawannum[$dt->mahasiswa_id]+1).'"' ?>>
					<?php 
					if($kryn != $dt->mahasiswa_id){
						echo $dt->nama;
						if($dt->nim=='') echo "<br><small><strong>NIK. -</strong></small>";
						else echo "<br><small><strong>NIK. ". $dt->nim ."</strong></small>";
					}
					?>
					<?php $kryn = $dt->mahasiswa_id; ?>
				</td>
			<?php } ?>
			<td width="50%">
				<?php if($view=='cetak'):							
						echo "<strong>[".$dt->kode_mk."]</strong> ";
						echo $dt->namamk." - ".$dt->kelas;
						echo " [".$dt->prodi_mk."]";
						if($ispraktikum) echo "*";			
				?>
				<?php else: ?>
				<div class="panel-group" id="accordion">
				  <div class="panel <?php echo $strclass ?>">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" href="#<?php $link = str_replace(' ', '', $dt->namamk." - ".$dt->kelas." - ".$dt->prodi_mk." - ".$index); echo str_replace(str_split('&/.'), '', $link); ?>" style="font-size: 13px;">
							<?php 
								
								echo "<strong>[".$dt->kode_mk."]</strong> ";
								echo $dt->namamk." - ".$dt->kelas;
								echo " [".$dt->prodi_mk."]";
								if($ispraktikum) echo "*";
							?>
						 </a>
	      			  </h4>
	   			 	</div>
	   			 	<?php if($view_!="cetak"){ ?>
	   			 	<div id="<?php $link = str_replace(' ', '', $dt->namamk." - ".$dt->kelas." - ".$dt->prodi_mk." - ".$index); echo str_replace(str_split('&/.'), '', $link); ?>" class="panel-collapse collapse">
	      				<div class="panel-body">
	      					<?php //echo $dt->jadwal_id;
	      						//$mabsen = new model_absen();
								$absen_asisten = $mconf->rekap_absen_asisten($dt->tahun_akademik, $dt->prodi_id, $dt->mkditawarkan_id, $dt->kelas, $dt->mahasiswa_id, $datemulai, $dateselesai, $dt->jadwal_id);
	      					?>
	      					<?php //echo $thn_aktif."<br>".$dt->tahun_akademik."<br>".$level ?>
	      					<?php //if(($dt->tahun_akademik == $thn_aktif) && ($level!=2)){ ?>
	      					<a href="javscript:" class="btn btn-primary tambah_absen" data-toggle="modal" data-target="#myModal_asisten" onclick="add_absen_asisten('<?php echo $dt->mahasiswa_id ?>','<?php echo $dt->asisten_id ?>','<?php echo $dt->jadwal_id ?>')"><i class="fa fa-plus"></i> Tambah Absen</a>
	      					<?php //} ?>
	      					<table class="table noborder example" style="font-size: 12px;">
	      						<thead>
	      							<tr>
		      							<th>Hari / Tanggal</th>
		      							<th>Ruang</th>
		      							<th>Materi</th>
		      							<th>Kehadiran</th>
		      							<th class="action_btn">&nbsp;</th>
	      							</tr>
	      						</thead>
	      						<tbody>
	      							<?php foreach($absen_asisten as $a){
	      									$tgl = explode("-",$a->tgl_pertemuan);
											$tgl = $tgl[2]."-".$tgl[1]."-".$tgl[0];
		      								if($a->is_hadir==1){
		      									$hadir = "Hadir";
		      								}else $hadir = "Tidak Hadir";
		      						?>
		      							<tr>
		      								<td>
		      									<?php
		      										$day = date("l", strtotime($tgl));
													switch($day){
														case "Monday" : $hari = "Senin";
																		break;
														case "Tuesday" : $hari = "Selasa";
																		break;
														case "Wednesday" : $hari = "Rabu";
																		break;
														case "Thursday" : $hari = "Kamis";
																		break;
														case "Friday" : $hari = "Jumat";
																		break;
														case "Saturday" : $hari = "Sabtu";
																		break;
														case "Sunday" : $hari = "Minggu";
																		break;
													}; 
		      										echo ucfirst($hari) .", ".$tgl."<br>"; 
													echo "<i class='fa fa-clock-o'></i> ".substr($a->jam_masuk,0,5)." - ";
													echo substr($a->jam_selesai,0,5); 
													
													echo "<br><small><span class='text text-danger'><em>by ".$a->user."<br><i class='fa fa-clock-o'></i> ".$a->last_update."</em></span></small>";
												?>
											</td>
		      								<td><?php 
		      										if($a->kelompok!="") $kelompok = $a->kelompok;
													else $kelompok = "Belum Ada Kelompok";
		      										echo $a->ruang_id."<br><small>(".$kelompok.")</small>" ?></td>
		      								<td><?php if($a->materi) echo ucfirst($a->materi); else echo "&nbsp;"; ?></td>
		      								<td><?php echo $hadir; ?></td>
		      								<td class="action_btn" width="10%">
												<?php 
												//$terbayar = $mconf->get_bayar($a->absendosen_id);
												
												if(($level==8 || $level==5)){//&& (! $terbayar)){
												
												?>
		      									<a href="#" title="Edit" data-toggle="modal" data-target="#myModal_asisten" onclick="edit_absen_asisten('<?php echo $dt->mahasiswa_id ?>','<?php echo $dt->asisten_id ?>','<?php echo $dt->jadwal_id ?>','<?php echo $a->absen_id ?>')"><i class="fa fa-edit" style="color: blue;"></i></a>&nbsp;&nbsp;&nbsp;
		      									<a href="#" title="Delete" onclick="delete_absen_asisten('<?php echo $a->absen_id; ?>')"><i class="fa fa-trash-o" style="color: red;"></i></a>
												<br>
												<?php } 
												
												if($level==1){
												
												?>
		      									<a href="#" title="Edit" data-toggle="modal" data-target="#myModal_asisten" onclick="edit_absen_asisten('<?php echo $dt->mahasiswa_id ?>','<?php echo $dt->asisten_id ?>','<?php echo $dt->jadwal_id ?>','<?php echo $a->absen_id ?>')"><i class="fa fa-edit" style="color: blue;"></i></a>&nbsp;&nbsp;&nbsp;
		      									<a href="#" title="Delete" onclick="delete_absen_asisten('<?php echo $a->absen_id; ?>')"><i class="fa fa-trash-o" style="color: red;"></i></a>
												<br>
												<?php } 
												
												//if($terbayar) echo "<span class='text text-danger'><small>* proses spj on </small><b>".$terbayar->last_update."</b></span>";
												//else echo "<small><b>* Belum SPJ</b></small>";
												?>
		      								</td>
		      							</tr>
	      							<?php } ?>
	      						</tbody>
	      					</table>
	      				</div>
	  				</div>
	  				<?php } ?>
				</div>
			   </div>
			   <?php endif;?>
			</td>
			<td><center>
				<h4><span class="text text-danger"><?php echo $dt->jml; ?></span></h4></center>
			</td>
		</tr>
		
	<?php } ?>
	<?php } ?>
	
</tbody>
</table>
<?php else: ?>
	<div class="well" align="center">
		No Data Show
	</div>
<?php endif; ?>

<!-- Modal dosen -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        	<form id="form-absen-dosen">
					<div class="form-group">
				        <label class="control-label">Jadwal Matakuliah</label>
				        <div class="controls">
				        	<select name="jadwal-mk" class="e9 form-control">
				        		<option value="0">Silahkan Pilih</option>
				        	</select>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Tanggal</label>
				        <div class="controls">
				        	<input type="text" name="tgl_pertemuan" class="date form-control" placeholder="Masukkan Tanggal Pertemuan"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Jam Mulai</label>
				        <div class="controls">
				        	<input type="text" class="time form-control" name="jam_mulai" placeholder="Masukkan Jam Mulai"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Jam Selesai</label>
				        <div class="controls">
				        	<input type="text" class="time form-control" name="jam_selesai" placeholder="Masukkan Jam Selesai"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Materi</label>
				        <div class="controls">
				        	<input type="text" class="form-control" name="materi" placeholder="Masukkan Nama Materi"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Total Pertemuan</label>
				        <div class="controls">
				        	<input type="text" class="form-control" name="total" placeholder="Masukkan Total Pertemuan" value="14"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Sesi</label>
				        <div class="controls">
				        	<select name="sesi" class="e9 form-control">
				        		<?php for($i=1;$i<21;$i++){ ?>
				        			<option value="<?php echo $i ?>">Sesi <?php echo $i ?></option>
				        		<?php } ?>
				        	</select>
				        </div>
				    </div>	 
				    <div class="form-group">
				        <label class="control-label"></label>
				        <div class="controls">
				        	<input type="hidden" name="hidId_absen"/>
				        	<input type="hidden" name="hidId_absendosen"/>
				        	<input type="hidden" name="dosen"/>
				        	<button type="submit" class="btn btn-primary">Data Valid & Save</button>
				        	<a href="javascript:" class="btn btn-danger close_btn" data-dismiss="modal">Cancel</a>
				        </div>
				    </div>  
		    </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal asisten -->
<div class="modal fade" id="myModal_asisten" tabindex="-1" role="dialog" aria-labelledby="myModal_asistenLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModal_asistenLabel"></h4>
      </div>
      <div class="modal-body">
        	<form id="form-absen-asisten">
        		<div class="form-group">
			        <label class="control-label">Jadwal Matakuliah</label>
			        <div class="controls">
			        	<select name="jadwal-mk" class="e9 form-control">
			        		<option value="0">Silahkan Pilih</option>
			        	</select>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label">Tanggal</label>
			        <div class="controls">
			        	<input type="text" name="tgl_pertemuan" class="date form-control" placeholder="Masukkan Tanggal Pertemuan"/>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label">Jam Mulai</label>
			        <div class="controls">
			        	<input type="text" class="time form-control" name="jam_mulai" placeholder="Masukkan Jam Mulai"/>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label">Jam Selesai</label>
			        <div class="controls">
			        	<input type="text" class="time form-control" name="jam_selesai" placeholder="Masukkan Jam Selesai"/>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label">Materi</label>
			        <div class="controls">
			        	<input type="text" class="form-control" name="materi" placeholder="Masukkan Nama Materi"/>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label">Total Pertemuan</label>
			        <div class="controls">
			        	<input type="text" class="form-control" name="total" placeholder="Masukkan Total Pertemuan" value="14"/>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label">Sesi</label>
			        <div class="controls">
			        	<select name="sesi" class="e9 form-control">
			        		<?php for($i=1;$i<21;$i++){ ?>
			        			<option value="<?php echo $i ?>">Sesi <?php echo $i ?></option>
			        		<?php } ?>
			        	</select>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label">Kelompok</label>
			        <div class="controls">
			        	<select name="kelompok" class="e9 form-control">
			        		<?php foreach($kelompok as $k){ ?>
			        			<option value="<?php echo $k->kelompok_id ?>"><?php echo $k->nama ?></option>
			        		<?php } ?>
			        	</select>
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="control-label"></label>
			        <div class="controls">
			        	<input type="hidden" name="hidId_absen"/>
			        	<input type="hidden" name="hidId_asistenabsen"/>
			        	<input type="hidden" name="asisten"/>
			        	<input type="hidden" name="kelas"/>
			        	<button type="submit" class="btn btn-primary">Data Valid & Save</button>
			        	<a href="javascript:" class="btn btn-danger close_btn" data-dismiss="modal">Cancel</a>
			        </div>
			    </div>
        	</form>
      </div>
    </div>
  </div>
</div>