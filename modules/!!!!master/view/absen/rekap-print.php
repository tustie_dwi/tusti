<link rel="stylesheet" type="text/css" href="<?php echo $this->asset("css/v4/style-print.min.css");?>" />
	<style>
	.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead{ border-top:solid 1px #444;}
		.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{
			font-size:12px;
			border: solid 1px #444;
		}
		table{ font-size:12px;
			border: solid 0px #fff; }
	</style>


<?php 
if($prodiid==""){
	if(isset($dosenid)){
		echo "<h2>Rekap Absen Dosen</h2>";
	}else{
		echo "<h2>Rekap Absen Asisten</h2>";
	}
	echo "<h4>".date("M d, Y", strtotime($datemulai))." s/d ". date("M d, Y", strtotime($dateselesai))."</h4>";
	foreach ($prodi as $p){
		echo "<h4>Program Studi " .$p->keterangan."</h4>";
		if(isset($dosenid)){
			$this->cetak_rekap($thnid, $p->prodi, $mk, $kelas, $dosenid, $datemulai, $dateselesai, $view_);
		}
		else{
			$this->cetak_rekap_asisten($thnid, $p->prodi, $mk, $kelas, $mhsid, $datemulai, $dateselesai, $view_);
		}
		?>
		<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
		<div class="page-break" id="footer"></div>
		<?php
	}
}
else{
	if(isset($dosenid)){
		echo "<h2>Rekap Absen Dosen</h2>";
	}else{
		echo "<h2>Rekap Absen Asisten</h2>";
	}
	echo "<h4>".date("M d, Y", strtotime($datemulai))." s/d ". date("M d, Y", strtotime($dateselesai))."</h4>";
	echo "<h4>Program Studi " .$nama_prodi."</h4>";
	if(isset($dosenid)){
		$this->cetak_rekap($thnid, $prodiid, $mk, $kelas, $dosenid, $datemulai, $dateselesai, $view_);
	}
	else{
		$this->cetak_rekap_asisten($thnid, $prodiid, $mk, $kelas, $mhsid, $datemulai, $dateselesai, $view_);
	}
	?>
	<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
	<div class="page-break" id="footer"></div>
	<?php
}
?>
