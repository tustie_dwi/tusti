<?php $this->head(); ?>
<fieldset id="cetak">
	<legend>
	<?php 
	$role = strToLower($this->coms->authenticatedUser->role);
	if($role!="dosen"){ ?>
	<a href="<?php echo $this->location('module/penjadwalan/absen/write'); ?>" class="btn btn-info pull-right" style="margin:0px 2px">
    <i class="fa fa-plus"></i> Tambah Absen</a>
	<?php } ?>
	
	<a href="<?php echo $this->location('module/master/report/akademik/absen'); ?>" class="btn btn-default pull-right" style="margin:0px 5px">
    <i class="fa fa-check"></i> Rekap Absen</a>
	
	
	<button class="btn btn-primary pull-right btn_cetak" onclick="cetak('<?php echo $thnid ?>', '<?php echo $mk ?>', '<?php echo $prodiid ?>', '<?php echo $kelas?>', '<?php echo $dosen_mhs?>', '<?php echo $datemulai?>', '<?php echo $dateselesai?>', '<?php echo $type?>')"><i class="fa fa-print"></i> Cetak</button>
	<h3>
		<?php if($type!="dosen") $jabatan = "Asisten"; else $jabatan = "Dosen" ?>
		Rekap Absen <?php echo ucwords($jabatan)." ".date("M d, Y", strtotime($datemulai))?> s/d <?php echo date("M d, Y", strtotime($dateselesai))?>
	</h3>
	</legend>
	<div class="col-md-12">
		<div class="block-box">
		
			<?php 	
				if($prodiid){
					if($type=="dosen"){
						$this->get_rekap($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
					}
					else{
						$this->get_rekap_asisten($thnid, $prodiid, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
					}
				}else{
					if($view_=='view'){
						if($type=="dosen"){
							$this->get_rekap($thnid, '', $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
						}
						else{
							$this->get_rekap_asisten($thnid, '', $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
						}
					}
					else{
						
							foreach ($prodi as $p){
								echo "<div id='content_print'><h2>Program Studi " .$p->keterangan."</h2>";
								if($type=="dosen"){
									$this->get_rekap($thnid, $p->prodi_id, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
								}
								else{
									$this->get_rekap_asisten($thnid, $p->prodi_id, $mk, $kelas, $dosen_mhs, $datemulai, $dateselesai, $view_);
								}
								?>
								<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
								<div class="page-break" id="footer"></div>
								<?php
							}
							
					} //will group by prodi for print
				}
			?>
		</div>
	</div>
</fieldset>
<?php $this->foot(); ?>