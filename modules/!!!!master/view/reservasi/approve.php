<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Approval</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Reservasi Ruang</a></li>
		  <li class="active"><a href="#">Approval</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class='block-box'>
			
			<form action="<?php echo $this->location('module/master/reservasi/approve') ?>" method='POST'>
			    <div class="input-group">
			        <input value="<?php echo $tgl_mulai ?>" type="text" class="input-daterange input-small form-control" name="start" placeholder="Tanggal Mulai" />
			        <span class="input-group-addon">s/d</span>
			        <input value="<?php echo $tgl_selesai ?>" type="text" class="input-daterange input-small form-control" name="end" placeholder="Tanggal Selesai" />
			    	<span class="input-group-btn">
			    		<button class="btn btn-primary button-group-addon"><i class="fa fa-search"></i> Cari</button>
			      	</span>
			    </div>
		    </form>
			
			<div style="margin-bottom: 10px">
				<i style="color: #f39c12" title="Belum diproses" class="fa fa-square"></i>
				<small class="margin">Belum Diproses</small>
				
				<i style="color: #16a085" title="Disetujui" class="fa fa-square"></i>
				<small class="margin">Disetujui</small>
				
				<i style="color: #e74c3c" title="Ditolak" class="fa fa-square"></i>
				<small class="margin">Ditolak</small>
			</div>
			
			<?php if($reservasi) : ?>
			<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th width="10%">No</th>
						<th>Kegiatan</th>
						<!-- <th>Pemesan</th> -->
					</tr>
				</thead>
				
				<tbody>
					<?php
						$no =1;
						foreach($reservasi as $key){
							if($key->is_valid == '0') $icon = '<i style="color: #f39c12" title="Belum diproses" class="fa fa-square"></i>';
							elseif($key->is_valid == '1') $icon = '<i style="color: #16a085" title="Disetujui" class="fa fa-square"></i>';
							else $icon = '<i style="color: #e74c3c" title="Ditolak" class="fa fa-square"></i>';
							echo "<tr>";
							echo "<td>".$no."</td>";
							echo '<td>';
								echo '<a style="cursor: pointer" onclick="detail_reservasi(\''.$key->jadwal_id.'\')">'.$icon.' ' .$key->kegiatan.'</a>';
								echo '<br><small style="color: #c0392b"><i class="fa fa-calendar-o"></i> '.date("M d, Y",strtotime($key->tgl_mulai));
								echo ' <i class="fa fa-clock-o"></i> '.substr($key->jam_mulai,0,5). ' - ' . substr($key->jam_selesai,0,5).'</small>';
							echo '</td>';
							// echo "<td></td>";
							echo "</tr>";
							$no++;
						}
					?>
				</tbody>
			</table>
			<?php else : ?>
				<div class="span3" align="center" style="margin-top:20px;">
				    <div class="well">Sorry, no content to show</div>
			    </div>
			<?php endif; ?>
		</div>
		
	</div>
	
	<div class="col-md-6">
		<div class='block-box detail-reservasi'>
			<i id="loading" class="fa fa-refresh fa-spin" style="position: absolute; display: none"></i>
			<h3 style="margin-top: 0; text-align: center">Reservasi Ruangan</h3>
			
		</div>
	</div>
</div>
<?php 
	$this->foot();
?>

<style type="text/css" media="screen">
	#ruang-field .btn-xs{
		margin-bottom: 3px;
		margin-left: 3px;
	}
</style>

<<script type="text/javascript" charset="utf-8">
	$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	
	$('.input-daterange').datepicker({
		format: 'yyyy-mm-dd', 
		viewMode: 2
	});
</script>




























