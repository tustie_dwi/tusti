<?php 	
 if(isset($posts)):	?>
<table class='table table-hover' id='example'>
	<thead>
		<tr>
			<th>Modified</th>
			<th>Content</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php	
		if($posts > 0){
			$i= 1;
			foreach ($posts as $dt): ?>
				<tr>
					<td width="15%">
						<small><i class="fa fa-clock-o">&nbsp;</i><?php echo $dt->last_update ?></small>
					</td>
					<td>
						<div class="media">
						  <?php if($dt->content_category=='slider'){ ?>	
						  <a class="pull-left" href="#">
							<img class="media-object img-thumbnail" src="<?php echo $this->config->file_url_view."/".$dt->file_loc; ?>" width="60" >
						  </a>
						  <?php } ?>
						  <div class="media-body">
							<b class="media-heading"><?php echo $dt->file_title; ?></b>
							<code>
								<?php 
								if($dt->file_type=='image/jpeg'){
									echo 'Image';
								}else{
									echo 'Video';
								} ?>
							</code>&nbsp;
							<?php if($dt->is_publish == '1') 
								echo '<span class="label label-success">Published</span>'; 
								else echo '<span class="label label-warning">Draft</span>';
								?>
							<br>
							<small><?php echo strip_tags($dt->file_file_note)."..."; ?></small>
						  </div>
						</div>
					</td>
					<td width="10%">
						<ul class='nav nav-pills' style='margin:0;'>
							<li class='dropdown'>
							  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
							  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
								<li>
								<a class='btn-edit-post' href="<?php echo $this->location('module/master/content/edit/file/'.$dt->file_id); ?>"><i class='fa fa-edit'></i> Edit</a>	
								</li>
								<li>
								<a class='btn-edit-post' onclick="delete_(this, 'file', '<?php echo $dt->file_id ?>')" href="javascript::"><i class='fa fa-trash-o'></i> Delete</a>	
								</li>
								<li>
								<a class='btn-edit-post' href="<?php echo $this->location('module/master/content/detail/file/'.$dt->file_id); ?>"><i class='fa fa-eye'></i> View</a>	
								</li>
							  </ul>
							</li>
						</ul>
					</td>
				</tr>
				
			<?php endforeach; 
		 }
	?>
	</tbody>
</table>	
 <?php
 else: 
 ?>
<div class="row">
<div class="col-md-12" align="center" style="margin-top:20px;">
    <div class="well">Sorry, no content to show</div>
</div>
</div>
<?php endif; ?>