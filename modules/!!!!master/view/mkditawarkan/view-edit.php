<?php 
if($posts !=""){
	$header		= "Edit Mata Kuliah";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$gettahunid		= $dt->tahun_akademik;
		$isblok			= $dt->is_blok;
		$ispraktikum	= $dt->is_praktikum;
		$namamk			= $dt->namamk;
		$fakultas_id	= $dt->fakultas_id;
		$parentid		= $dt->parent_id;
		$kuota			= $dt->kuota;
		$icon			= $dt->icon;
		$isonline		= $dt->is_online;
		$cabangid		= $dt->cabang_id;
	endforeach;
	
	$header.=" ".$namamk;
	
	if($parentid==""){
		$cekparent='1';
	}else{
		$cekparent='';
	}
		
	$edit			= "1";
}else{
	$header		= "Write New Mata Kuliah Ditawarkan";
	$id			= "";
	$fakultas_id= "";
	$gettahunid = "";
	$isblok		= "";
	$ispraktikum= "";
	$edit		= "";
	$isonline	= "1";
	$cabangid	= "";
	//$ceknew		= '1';
}
?>

<div class="panel panel-default">
  <div class="panel-heading"><?php echo $header; ?></div>
  <div class="panel-body ">
	<form method=post  name="form" id="form-mkditawarkan" class="form-horizontal" enctype="multipart/form-data">
		
		<div class="form-group">	
			<label class="col-sm-2 control-label">Fakultas</label>
			<div class="col-md-10">
				<select id="select_fakultas" class="e9 form-control" name="fakultas" >
					<option value="0" data-uri='1'>Select Fakultas</option>
					<?php if(count($get_fakultas)> 0) {
						foreach($get_fakultas as $dt) :
							echo "<option value='".$dt->fakultasid."' ";
							if(isset($fakultasid)){
								if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
									echo "selected";
								}
							}
							echo " >".ucWords($dt->keterangan)."</option>";
						endforeach;
					} ?>
				</select>
			</div>
		</div>
		
		<div class="form-group">	
			<label class="col-sm-2 control-label">Cabang</label>
			<div class="col-md-10">
				<select id="select_cabang" class="e9 form-control" name="cabang" >
					<option value="0">Select Cabang</option>
					<?php if(count($cabang)> 0) {
						foreach($cabang as $c) :
							echo "<option value='".$c->cabang_id."' ";
							if(isset($cabangid)){
								if($cabangid==$c->cabang_id){
									echo "selected";
								}
							}
							echo " >".ucWords($c->keterangan)."</option>";
						endforeach;
					} ?>
				</select>
			</div>
		</div>				
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Tahun Akademik</label>
			<div class="col-md-10">
				<select id="select_thn" name="tahun_akademik" class="e9 form-control" >
					<option value="0">Select Tahun Akademik</option>
					<?php if(count($thnakademik)> 0) {
						foreach($thnakademik as $th) :
							echo "<option value='".$th->tahun_akademik."' ";
							if(isset($gettahunid)){
								if($gettahunid==$th->tahun_akademik){
									echo "selected";
								}
							}
							echo " >".ucWords($th->thnakademik)."</option>";
						endforeach;
					} ?>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Mata Kuliah </label>
				<div class="col-md-10" id="mk">
					<input id="namamk" name='namamk' required='required' type='text' value ='<?php if(isset($namamk))echo $namamk ?>' class='col-md-6 form-control typeahead'>		
					<?php if($edit == '1') { ?>
					<input type="hidden" name='namamk' value ='<?php echo $namamk ?>' >
					<?php } ?>
				</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Kuota</label>
				<div class="col-md-10">
					<input id="kuota" name='kuota' required='required' type='text' value ='<?php if(isset($kuota))echo $kuota ?>' class='col-md-6 form-control'>		
				</div>
		</div>				

		<div class="form-group">
			<label class="col-sm-2 control-label">Icon</label>
				<div class="col-md-10">
					<?php if($edit==1) { ?>
					<?php if(isset($icon)) { ?>
						<div class='well'>
							<img src='<?php echo $this->config->file_url_view."/".$icon ?>' />
						</div>
					<?php } else { ?>
						<div class='well'>
							Belum terdapat icon untuk matakuliah ini
						</div>
					<?php } ?>
					<?php } ?>
					<input name='icon' type='file' class='col-md-6 form-control'>		
				</div>
		</div>
		
		<?php if($edit!=1) {?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Koordinator</label>
				<div class="col-md-10" >
					<input required='required' placeholder="Nama Dosen Koordinator" type='text' id ='dosenkoor' name ='dosenkoor' value ='<?php if(isset($dosenkoor))echo $dosenkoor ?>' class='col-md-6 form-control typeahead' >	
					<input type="hidden" name="iskoordinator" value="1"/>
				</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Pengampu </label>
				<div class="col-md-10">
					<input type="text" id ='dosenpengampu' class="form-control typeahead" name="dosenpengampu[]" value="" placeholder="Nama Dosen Pengampu" data-role="tagsinput" />
				</div>
		</div>
		<?php } else {?>
		<?php $x=1 ?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Pengampu </label>
			<div class="col-md-10">
			  <table class='table'>
			  <?php if(isset($pengampu)){ 
			  	   	foreach ($pengampu as $p) { ?>
			  		<tr>
					  <td><?php echo $p->nama ?></td>
					  <td>
					  	<input id="koor<?php echo $x; ?>" type="radio" name="koor" onclick="iskoor(<?php echo $x; ?>)" value="1" <?php if ($p->is_koordinator=="1") { echo "checked"; } ?>>
					    <span id="koorlist<?php echo $x; ?>">
					     <i id='benar<?php echo $x; ?>'>
					      <span id="rem<?php echo $x; ?>">
					      <?php if($p->is_koordinator=="1"){ ?><small><span class='label label-danger'><em>Koordinator</em></span></small><?php } ?>
					      </span>
					     </i>
		  			  	 <input type='hidden' id ='isbenarcheck<?php echo $x; ?>' name='isvalbenar[]' value='<?php if($p->is_koordinator=="1")echo $p->is_koordinator; ?>' >
		  			  	 <input type='hidden' id ='pengampuid<?php echo $x ?>' value='<?php echo $p->pengampu_id; ?>' >
		  			  	 <input type='hidden' id ='pengampukoor<?php echo $x ?>' value='<?php echo $p->is_koordinator; ?>' >
		  			  	</span>
		  			  </td>
		  			  <td><a href="#" onclick="deletePengampu(<?php echo $p->pengampu_id; ?>)" >Delete</a></td>
		  			</tr>
			  <?php $x++; }
			 		 }else { echo "Belum terdapat Pengampu untuk matakuliah ini "; } ?>
			  </table>
			  <div class="col-md-9">
			  <input type="text" id ='namedosenpengampu' class="form-control typeahead" name="dosenpengampu" value="" placeholder="Nama Dosen Pengampu" onkeypress="postpengampu(event)"/>
			  <input type="hidden" id ='dosenpengampuid' class="form-control typeahead" name="dosenpengampuid" value="" />
			  </div>
			  <a href="#" id="addDosenPengampu" class='btn btn-info'><i class='fa fa-plus'></i></a>						
			  
			</div>
		</div>
		<?php }?>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Is Course </label>
			<div class="col-md-10">
				<label class="checkbox">
				<input id="iscourse" type="checkbox" name="iscourse" value="1" <?php if ($isonline==0) { echo "checked"; } ?>>Ya
				</label>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">Is Blok </label>
			<div class="col-md-10">
				<label class="checkbox">
				<input id="isblok" onclick="validate()" type="checkbox" name="isblok" value="1" <?php if ($isblok==1) { echo "checked"; } ?>>Ya
				</label>
			</div>
		</div>
		
		<?php echo "<div id='form' "; 
			  if($isblok!='1'){
			  echo "style='display: none;' "; 
			  }
			  echo " > "; ?>
			<div class="form-group">
				<label class="col-sm-2 control-label">Blok dari </label>
				<div class="col-md-10">
					<select id="select_mk" disabled name="parent_mk" class="form-control e9" >
					<option value="0">Select Mata Kuliah</option>
					<?php
						foreach($getdatamk as $dk):
							echo "<option value='".$dk->mkid."' ";
							if($parentid==$dk->mkid){
								echo "selected";
							}
							echo " >".$dk->keterangan."</option>";
						endforeach;
					?>
					</select>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-md-10">
				<!-- <label class="checkbox"><input type="checkbox" name="ispraktikum" value="1" <?php if ($ispraktikum==1) { echo "checked"; } ?>>Ya</label><br> -->
				<input type="hidden" name="hidId" value="<?php echo $id;?>">
				<input type="hidden" id ="edit-cek" name="edit" value="<?php echo $edit;?>">	
				<?php if($edit == '1') { ?>
					<input type="hidden" name="tahun_akademik" value="<?php echo $gettahunid;?>">
					<input type="hidden" name="icon_loc" value="<?php echo $icon;?>">
					<?php if($cekparent!="1") { ?>
					<input type="hidden" name="parent_mk" value="<?php echo $parentid;?>">
					<?php } ?>
				<?php } ?>				
				<input type="submit" name="b_mkditawarkan" id="submit" value="Simpan" class="btn btn-primary">
			</div>
		</div>
		
	</form>
  </div>
</div>