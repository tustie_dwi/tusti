<div id="parameter" class="block-box">
	<form action="<?php echo $this->location('module/master/akademik/mkditawarkan'); ?>" role="form" id="param" method="POST">
		<div class="form-group">	
			<label class="control-label">Fakultas</label>
							
				<select id="pilih_fakultas" class="form-control e9" name="fakultas" >
				<option value="0" data-uri='1'>Select Fakultas</option>
				<?php if(count($get_fakultas)> 0) {
					foreach($get_fakultas as $dt) :
						echo "<option value='".$dt->fakultasid."' ";
						if(isset($fakultasid)){
							if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
								echo "selected";
							}
						}
						echo " >".ucWords($dt->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
		</div>
		
		<div class="form-group">	
			<label class="control-label">Cabang</label>					
				<select id="pilih_cabang" class="form-control e9" name="cabang" >
				<option value="0" data-uri='1'>Select Cabang</option>
				<?php if(count($cabang)> 0) {
					foreach($cabang as $c) :
						echo "<option value='".$c->cabang_id."' ";
						if(isset($cabangid)){
							if($cabangid==$c->cabang_id){
								echo "selected";
							}
						}
						echo " >".ucWords($c->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
		</div>
		
		<div class="form-group">	
			<label class="control-label">Tahun Akademik</label>						
				<select id="select_thnakademik" class="form-control e9" name="thn_akademik" >
				<option value="0" data-uri='1'>Select Tahun akademik</option>
				<?php if(count($thnakademik)> 0) {
					foreach($thnakademik as $th) :
						echo "<option value='".$th->tahun_akademik."' ";
						if(isset($thnakademikid)){
							if($thnakademikid==$th->tahun_akademik){
								echo "selected";
							}
						}
						echo " >".ucWords($th->thnakademik)."</option>";
					endforeach;
				} ?>
				</select>
		</div>
	</form>
</div>