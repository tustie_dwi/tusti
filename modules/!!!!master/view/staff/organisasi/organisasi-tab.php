<div class="row">
	<div class="col-md-7">
		<div class="panel panel-default">
			<div class="panel-heading">Organisasi</div>
			<div class="panel-body ">
				<?php if(isset($organisasi)){ ?>
					<table class="table table-condensed example">
						<thead>
							<tr>
								<th>Organisasi</th>
								<th>Periode</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($organisasi as $or){
								if($or->is_aktif==1) $aktif = "Aktif";
								else $aktif = "Tidak Aktif";
							?>
							<tr id="<?php echo $or->org_id ?>">
								<td>
									<?php 
										echo $or->nama_organisasi;
										echo "&nbsp;<label class='label label-success'>".ucwords($aktif)."</label>";
										echo "<br><code>".ucwords($or->sebagai)."</code>";
									?>
								</td>
								<td><?php echo date("d F Y", strtotime($or->periode_mulai))." - <br>".date("d F Y", strtotime($or->periode_selesai)) ?></td>
								<td>
									<ul class='nav nav-pills'>
										<li class='dropdown pull-right'>
										  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
										  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
											<li>
											<a href="javascript::" class="edit_organisasi" data-id="<?php echo $or->org_id; ?>"><i class="fa fa-edit"></i> Edit</a>
											<a href="javascript::" class="delete_organisasi" data-id="<?php echo $or->org_id; ?>"><i class="fa fa-trash-o"></i> Delete</a>
											</li>
										  </ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php }else{ ?>
					<div class="col-sm-12" align="center" style="margin-top:20px;">
					    <div class="well">Data tidak dapat ditemukan</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<form id="form-organisasi">
			<div class="panel panel-default">
				<div class="panel-heading">Data Pendukung</div>
				<div class="panel-body ">
					<div class="form-group">
						<label class="control-label">Nama Organisasi</label>								
						<input name="nama_organisasi" class="form-control" autocomplete="off" type="text" placeholder="Organisasi" value="<?php if(isset($edit_org)) echo $edit_org->nama_organisasi ?>">
					</div>
					<div class="form-group">
						<label class="control-label">Jabatan</label>								
						<input name="jabatan_sebagai" class="form-control" autocomplete="off" type="text" placeholder="Jabatan" value="<?php if(isset($edit_org)) echo $edit_org->sebagai ?>">
					</div>
					<div class="form-group">
						<label class="control-label">Periode</label>
						<div class="row">	
							<div class="col-sm-6">					
								<input name="periode_mulai" autocomplete="off" class="form-control form_datetime" type="text" placeholder="Tanggal Mulai" value="<?php if(isset($edit_org)) echo $edit_org->periode_mulai ?>">
							</div>
							<div class="col-sm-6">
								<input name="periode_selesai" autocomplete="off" class="form-control form_datetime" type="text" placeholder="Tanggal Selesai" value="<?php if(isset($edit_org)) echo $edit_org->periode_selesai ?>">
							</div>
						</div>
					</div>
					<div class="form-group checkbox">
						<label><input name="current" <?php if(isset($edit_org) && $edit_org->is_aktif==1)echo " checked "; ?> value="1" type="checkbox"><b>Current</b></label>
						<span class="help-block"><em>centang jika aktif sampai sekarang</em></span>
					</div>
					<div class="form-group">
						<label class="control-label"></label>	
						<input type="hidden" name="hidId" class="form-control" value="<?php if(isset($edit_org)) echo $edit_org->organisasi_id ?>">
						<input type="hidden" name="kar_id" class="form-control" value="<?php if(isset($hid_id)) echo $hid_id ?>">							
						<button type="submit" class="btn btn-primary">Data Valid & Save</button>
						<a href="<?php echo $this->location("module/master/staff/edit/".$id); ?>" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>