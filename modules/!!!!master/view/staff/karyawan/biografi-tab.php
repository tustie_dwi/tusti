<?php 
if($posts !=""){
	// var_dump($posts);
	// foreach ($posts as $dt):
		$hidId			= $posts->hid_id;
		$biografi		= $posts->biografi;
		$biografi_en	= $posts->biografi_en;
		$about			= $posts->about;
		$about_en		= $posts->about_en;
		$interest		= $posts->interest;
		$interest_en	= $posts->interest_en;
	// endforeach;
	
}else{
	$hidId			= "";
	$biografi		= "";
	$biografi_en	= "";
	$about			= "";
	$about_en		= "";
	$interest	    = "";
	$interest_en    = "";
}
?>
<div class="row">
	<form method="post" name="form" id="form_bio" enctype="multipart/form-data">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Biodata Pribadi</div>
				<div class="panel-body ">
					<div class="form-group">
						<label class="control-label">Biografi</label>								
						<textarea name="biografi" id="biografi" class="form-control ckeditor" placeholder="Biografi"><?php if(isset($biografi))echo $biografi?></textarea>
					</div>
					
					<div class="form-group">
						<label class="control-label">Biografi (en)</label>								
						<textarea name="biografi_eng" id="biografi_eng" class="form-control ckeditor" placeholder="Biografi"><?php if(isset($biografi_en))echo $biografi_en?></textarea>
					</div>	
					
					<div class="form-group">
						<label class="control-label">About</label>								
						<textarea name="about" id="about" class="form-control ckeditor" placeholder="About"><?php if(isset($about))echo $about?></textarea>
					</div>
					
					<div class="form-group">
						<label class="control-label">About (en)</label>								
						<textarea name="about_eng" id="about_eng" class="form-control ckeditor" placeholder="About"><?php if(isset($about_en))echo $about_en?></textarea>
					</div>		
					
					<div class="form-group">
						<label class="control-label">Interest</label>
						<input name="interest" class="form-control" value="<?php if(isset($interest))echo $interest?>" placeholder="Interest" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">Interest (en)</label>
						<input name="interest_eng" class="form-control" value="<?php if(isset($interest_en))echo $interest_en?>" placeholder="Interest" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label"></label>
						<input name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>" type="hidden">
						<button class="btn btn-primary" type="submit">Data Valid & Save</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>