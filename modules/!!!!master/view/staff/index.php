<?php $this->head(); 
$header="Staff/Dosen";
?>
<div class="row">
    
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/staff'); ?>">Staff/Dosen</a></li>
	  <li class="active"><a href="#">Data</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/master/staff/write'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Staff/Dosen</a> 
    </div>
	
	<div class="row">
		<div class="col-md-4">	
			<div id="parameter" class="block-box">
			   <div class="content">
				<form action="<?php echo $this->location('module/master/staff'); ?>" role="form" id="param" method="POST">
						
						<div class="form-group">
							<label class="control-label">Fakultas</label>								
							<select id="select_fakultas" class="e9 form-control" name="fakultas">
								<option value="-">Select Fakultas</option>
								<?php if(count($fakultas)> 0) {
									foreach($fakultas as $f) :
										echo "<option value='".$f->hid_id."' ";
											if(isset($fakultas_id)&&$fakultas_id==$f->hid_id){
												echo "selected";
											}
										echo " >".ucWords($f->keterangan)."</option>";
									endforeach;
								} ?>
							</select>
						</div>
						
						<div class="form-group">	
							<label class="control-label">Cabang</label>					
								<?php echo '<select id="pilih_cabang" class="form-control e9" name="cabang" >'; ?>
								<option value="0" data-uri='1'>Select Cabang</option>
								<?php if(count($cabang)> 0) {
									foreach($cabang as $c) :
										echo "<option value='".$c->cabang_id."' ";
										if(isset($cabangid)){
											if($cabangid==$c->cabang_id){
												echo "selected";
											}
										}
										echo " >".ucWords($c->keterangan)."</option>";
									endforeach;
								} ?>
								</select>
						</div>
						
					</form>
			  </div>
			</div>
		</div>
	
		<div class="col-md-8">			
			<div id="isi" class="block-box">
				 <?php
				 if(isset($status) and $status) : ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $statusmsg; ?>
					</div>
				<?php 
				endif; 
				
				if( isset($posts) ) :	?>
				<table class='table table-hover' id='example'>
					<thead>
						<tr>
							<th>&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
					<?php if($posts > 0){
						foreach ($posts as $dt): 
							if($dt->foto) $foto = $this->config->file_url_view."/".$dt->foto; 
							else $foto = $this->config->default_thumb_web;
						?>
						<tr>
							<td>
							<div class="col-md-9">
								<div class='media'>
									<a class="pull-left"><img src="<?php echo $foto;?>"  class='media-object img-thumbnail' width="64" height="64"></a>
							
								<div class="media-body">							
								<span class='text text-default'><strong>
									<a href="<?php echo $this->location('module/master/staff/detail/'.$dt->karyawan_id) ?>"><span class="text text-default"><b><?php echo $dt->nama; ?>
									<?php
									if($dt->gelar_awal){echo ", ". $dt->gelar_awal;}
									if($dt->gelar_akhir){echo ", ".$dt->gelar_akhir." ";}
									?></strong></span></b></span></a>
									<code><?php echo strToUpper($dt->is_nik).". ".$dt->nik ?></code>
									<span class="label label-info"><?php echo ucWords($dt->is_status) ?></span>
									<?php if($dt->pin) echo "<span class='label label-danger'>PIN ".$dt->pin."</span>";
									$ruang_dosen = $mdosen->get_ruang_karyawan($dt->karyawan_id); ?>
									<?php 
									if($ruang_dosen){
										foreach($ruang_dosen as $dt):
											echo "<code>".$dt->ruang_id."</code>";
										endforeach;
										
									}				
									?>
									<br>
									<small><em><?php echo $dt->fakultas ?>&nbsp;<span class="label label-success"><?php echo $dt->cabang_id ?></span></em></small>
								</div>
								</div>
							</div>
							<div class="col-md-3">
								<ul class='nav nav-pills'>
									<li class='dropdown pull-right'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/master/staff/edit/'.$dt->karyawan_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
										</li>
									  </ul>
									</li>
								</ul>
							</div>
							</td>
						</tr>
					<?php endforeach; 
						 } ?>
					</tbody>
					</table>
				
				 <?php else: 
				 ?>
			    
				    <div class="well">Sorry, no content to show</div>
			    <?php endif; ?>
		    </div>
		</div>
    </div>
</div>
<?php $this->foot(); ?>