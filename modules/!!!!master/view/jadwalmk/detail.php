<?php $this->head(); ?>

    
    <h2 class="title-page">GENERAL INFO</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/master/jadwalmk'); ?>">Jadwal Matakuliah</a></li>
	  <li class="active"><a href="#">General Info</a></li>
	</ol>
    <div class="breadcrumb-more-action">
     <?php if($user!="mahasiswa"&&$user!="dosen"){  ?>
    <a href="<?php echo $this->location('module/master/jadwalmk/writenew'); ?>" class="btn btn-primary pull-right">
    <i class="fa fa-pencil icon-white"></i> New Jadwal Mata Kuliah </a> 
    <?php } ?>
	
	
</div>


	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :
		 if($posts){
			foreach ($posts as $dt): ?>
				<div class="media">				
				  <img class="media-object  pull-left img-thumbnail" src="<?php if($dt->icon){ echo $this->config->file_url_view."/".$dt->icon; }else{ echo $this->asset('images/topic.jpg'); } ?>" width="200">
					<div class="media-body">
					  <h2><?php echo $dt->namamk ?> <span class="label label-default"><?php echo ucWords($dt->thnakademik) ?></span>&nbsp;<?php if($dt->is_online == '1'){ echo "<span class='label label-success'>Online</span>";}else{ echo '<span class="label label-danger">Course</span>'; } ?></h2>
					  
					 <span class="text text-danger"><i class="fa fa-user"></i> <?php echo $dt->pengampu?></span>
					 <i class="fa fa-bullseye"></i> Kelas <?php echo $dt->kelas ?>&nbsp;
								 <i class="fa fa-calendar-o"></i> <?php echo $dt->hari ?>, <?php echo date("H:i",strtotime($dt->jam_mulai))." - ".date("H:i",strtotime($dt->jam_selesai)) ?><br>
								 <i class="fa fa-map-marker"></i> <?php echo $dt->ruang. " <code>".$dt->ruang_id ?></code>
					</div>
			  </div>

			
	  <?php endforeach; 
			 } ?>
			<br>
			<div class='well'>
				<a class='btn-edit-post' href="<?php echo $this->location('module/master/jadwalmk/edit/'.$dt->jadwal_id) ?>">
				<button type='button' class='btn btn-info'><i class='fa fa-edit'></i> Edit Jadwal Mata Kuliah</button></a>
				<!--<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/test/writebyjadwal/'.$dt->jadwal_id) ?>">
				<button type='button' class='btn btn-info'><i class='fa fa-file-text-o'></i> Test</button></a>-->
			</div>
	<?php
	
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>

	
	
	

<?php $this->foot(); ?>