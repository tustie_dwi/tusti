<?php
$this->head();

if($posts !=""){
	$header		= "Edit Jadwal Mata Kuliah";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$namamk			= $dt->namamk;
		$fakultas_id	= $dt->fakultasid;
		$thnakademikid	= $dt->tahunakademik;
		$pengampuid		= $dt->pengampu_id;
		$prodiid		= $dt->prodi_id;
		$ruang			= $dt->ruang;
		$ruangedit		= $dt->ruangedit;
		$dosen			= $dt->pengampu;
		$hari			= $dt->hari;
		$jam			= $dt->jam_mulai;
		$jamselesai		= $dt->jam_selesai;
		$kelas			= $dt->kelas;
		$isaktif		= $dt->is_aktif;
		$isonlinecek	= $dt->is_online;
		$cabangid		= $dt->cabang_id;
		$mkditawarkanid = $dt->mkditawarkan_id;
	endforeach;
	 	$edit			= '1';
		
		if($isonlinecek=='1'){
			$isonline = 0;
		}
		else {
			$isonline = 1;
		}
}else{
	$header			= "Write New Jadwal Mata Kuliah";
	
	foreach ($read as $r) {
		$thnakademikid 	= $r->tahun_akademik;
		$fakultas_id	= $r->fakultasid;
		$cabangid		= $r->cabang_id;
		$namamk			= $r->namamk;
		$mkditawarkanid = $r->mkditawarkan_id;
	}
	
	$id				= "";
	$isaktif		= "";
	$isonline		= "";
	$prodiid		= "";
	$pengampuid		= "";
}


?>

    <div class="row">  
    <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/master/jadwalmk'); ?>">Jadwal Mata Kuliah</a></li>
	  <li class="active"><a href="#"><?php echo str_replace(' Jadwal Mata Kuliah','',str_replace('Write ','',$header));  ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if(isset($edit)) { ?>
		<a href="<?php echo $this->location('module/master/jadwalmk/writenew'); ?>" class="btn btn-primary">
    	<i class="fa fa-pencil icon-white"></i> New Jadwal Mata Kuliah </a>
    	<?php } ?> 
		<?php if(isset($write)) { ?>
		<a href="<?php echo $this->location('module/master/akademik/mkditawarkan'); ?>" class="btn btn-default" ><i class="fa fa-bars"></i> Mata Kuliah Ditawarkan List</a> 
		<?php } ?>
    </div>   
        <div class="col-md-12">
		
			<form method=post name="form" id="form" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Cabang</label>
					<div class="col-md-10">
						<?php echo '<select id="select_cabang" class="e9 form-control" name="cabang" >'; ?>
							<option value="0">Select Cabang</option>
							<?php if(count($cabang)> 0) {
								foreach($cabang as $c) :
									echo "<option value='".$c->cabang_id."' ";
									if($cabangid==$c->cabang_id){
										echo "selected";
									}
									echo " >".$c->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
				</div>
							
				<div class="form-group">	
					<label class="col-sm-2 control-label">Fakultas</label>
					<div class="col-md-10">
						<?php $uri_parent = $this->location('module/master/jadwalmk/tampilkan_prodi'); ?>
						<?php echo '<select id="select_fakultas" class="form-control e9" name="fakultas" uri_="'.$uri_parent.'">'; ?>
							<option value="" data-uri='1'>Select Fakultas</option>
							<?php if(count($fakultas)> 0) {
								foreach($fakultas as $f) :
									echo "<option value='".$f->fakultasid."' ";
									if($fakultas_id==$f->fakultasid){
										echo "selected";
									}
									echo " >".$f->keterangan."</option>";
								endforeach;
							} ?>
						</select>						
						<input type="hidden" id='hidfakultasid' value="<?php echo $fakultas_id ?>"/>
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Tahun Akademik</label>
					<div class="col-sm-10">
						<select name="thn_akademik" class='col-md-9 form-control e9'>
							<option value="0" class="sub_01">Please Select..</option>			
							<?php														
								foreach($thnakademik as $dt):
									echo "<option value='".$dt->tahun_akademik."' ";
									if($thnakademikid==$dt->tahun_akademik){
										echo "selected";
									}
									echo ">".$dt->tahun."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Prodi</label>
					<div class="col-sm-10">
						<select id='select_prodi' name="prodi" class='col-md-9 form-control e9'>
							<option value="0" class="sub_01">Select Prodi</option>			
							<?php														
								foreach($prodi as $dt):
									echo "<option value='".$dt->prodi_id."' ";
									if($prodiid==$dt->prodi_id){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					 <label   class="col-sm-2 control-label">Nama Mata Kuliah</label>
					 <div class="col-sm-10">
						<input required="required" disabled type=text id="namamk" class='col-md-9 form-control' value="<?php if(isset($namamk))echo $namamk; ?>">
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Dosen Pengampu</label>
					<div class="col-sm-10">
						<select id="dosenpengampu" name="dosen" class='col-md-9 form-control e9'>
							<option value="0" class="sub_01">Please Select..</option>			
							<?php														
								foreach($datapengampu as $dt):
									echo "<option value='".$dt->pengampu_id."' ";
									if($pengampuid==$dt->pengampu_id){
										echo "selected";
									}
									echo ">".$dt->nama."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
			
				<div class="form-group">	
					 <label   class="col-sm-2 control-label">Kelas</label>
					 <div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="kelas" name="kelas" value="<?php if(isset($kelas))echo $kelas; ?>">
					</div>
				</div>
				
				
				<?php if($cekonline=='0'){ ?>
					<!-- IS COURSE -->
				<div>
					<div class="form-group">
						<label   class="col-sm-2 control-label"></label>
						<div class="col-sm-10">
							<em><small>*Mata kuliah ini hanya bisa di tampilkan saat jam perkuliahan</small></em>
						</div>
					</div>
					
					<div class="form-group">	
						 <label   class="col-sm-2 control-label">Ruang</label>
						 <div class="col-sm-10">
							<input type=text id="ruang"  class='col-md-9 form-control typeahead' name="ruang" value="<?php if(isset($ruangedit))echo $ruangedit; ?>">
						</div>
					</div>
					
					<div class="form-group">	
						 <label   class="col-sm-2 control-label">Hari</label>
						 <div class="col-sm-10">
							<input type=text  class='col-md-9 form-control' name="hari" value="<?php if(isset($hari))echo $hari; ?>">
						</div>
					</div>
					
					<div class="form-group">	
						 <label   class="col-sm-2 control-label">Jam</label>
						 <div class="col-sm-3">
							<input type="text" name="jammulai" class="timepicker form-control" value="<?php if(isset($jam))echo $jam;?>"> 
							sampai 
							<input type="text" name="jamselesai" class="timepicker form-control" value="<?php if(isset($jamselesai))echo $jamselesai;?>">
						</div>
					</div>
				</div>
				<!-- END IS COURSE -->
				<?php } 
				else { ?>
				<!-- IS COURSE -->
				<?php echo "<div id='form_no_is_online' "; 
					  if($isonline!='1'){
					  echo "style='display: none;' "; 
					  }
					  echo " > "; ?>
					<div class="form-group">	
						 <label   class="col-sm-2 control-label">Ruang</label>
						 <div class="col-sm-10">
							<input type=text id="ruang"  class='col-md-9 form-control typeahead' name="ruang" value="<?php if(isset($ruangedit))echo $ruangedit; ?>">
						</div>
					</div>
					
					<div class="form-group">	
						 <label   class="col-sm-2 control-label">Hari</label>
						 <div class="col-sm-10">
							<input type=text  class='col-md-9 form-control' name="hari" value="<?php if(isset($hari))echo $hari; ?>">
						</div>
					</div>
					
					<div class="form-group">	
						 <label   class="col-sm-2 control-label">Jam</label>
						 <div class="col-sm-3">
							<input type="text" name="jammulai" class="timepicker form-control" value="<?php if(isset($jam))echo $jam;?>"> 
							sampai 
							<input type="text" name="jamselesai" class="timepicker form-control" value="<?php if(isset($jamselesai))echo $jamselesai;?>">
						</div>
					</div>
				</div>
				<!-- END IS COURSE -->
				
				<?php } ?>
				
				<div class="form-group">
					<label for="keterangan" class="col-sm-2 control-label">Is Aktif</label>
					 <div class="col-sm-10">
					 <label class="checkbox">
					 	<input type="checkbox" name="isaktif" value="1" <?php if ($isaktif==1) { echo "checked"; } ?>>Ya
					 	</label>
					</div>
				</div>
				<?php if($cekonline=='0'){ ?>
				<div class="form-group">	
				 	<label class="col-sm-2 control-label">Is Course </label>
					<div class="col-md-10">
						<label class="checkbox">
						<input disabled id="isonline" type="checkbox" value="1" checked } ?>Ya
						<input type="hidden" name="isonline" value="<?php echo $cekonline;?>">
						</label>
					</div>
				</div>
				<?php } 
				else { ?>
				<div class="form-group">	
				 	<label class="col-sm-2 control-label">Is Course </label>
					<div class="col-md-10">
						<label class="checkbox">
						<input id="isonline" onclick="validate()" type="checkbox" name="isonline" value="1" <?php if ($isonline==1) { echo "checked"; } ?>>Ya
						</label>
					</div>
				</div>	
				<?php } ?>
				<div class="form-group">	
				 	<label class="col-sm-2 control-label"></label>
					<div class="col-md-10">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="namamk" value="<?php if(isset($mkditawarkanid))echo $mkditawarkanid; ?>">
						<?php if(isset($edit)) { ?>
						<input type="hidden" id="hidprodi" name="prodi" value="<?php if(isset($prodiid))echo $prodiid; ?>">
						<?php } ?>				
						<input type="submit" name="b_jadwal" id="submit-jadwal" value="Submit" class="btn btn-primary">
					</div>
				</div>
				
				
			</form>
		</div>
	</div>
<?php
$this->foot();
?>