<?php $this->head(); ?>
<fieldset>
	<h2 class="title-page">Detail Yudisium</h2>
	
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/master/yudisium'); ?>">Yudisium</a></li>
	  <li class="active"><a href="#">Detail Yudisium</a></li>
	</ol>
	
	<div class="block-box">
		<div class="row">
			<div class="col-md-6">
				<table class="table table-condensed table-bordered">
					<tr>
						<th class="text-right">Judul :</th>
						<td><?php echo $yudisium->judul ?></td>
					</tr>
					<tr>
						<th class="text-right">Periode :</th>
						<td><?php echo $yudisium->periode ?></td>
					</tr>
					<tr>
						<th class="text-right">Tanggal :</th>
						<td><?php echo $yudisium->tgl_yudisium ?></td>
					</tr>
					<tr>
						<th class="text-right">Ruang :</th>
						<td><?php echo $yudisium->kode_ruang . " - " .$yudisium->ruang ?></td>
					</tr>
				</table>
			</div>
			<div class="col-md-6">
				<?php echo $yudisium->keterangan ?>
			</div>
		</div>	
	</div>
	
	<div class="block-box">
		<div class="row">
			<form action="<?php echo $this->location('module/master/yudisium/add_mhs') ?>" method="POST">
				<div class="col-md-1">
					<button class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
					<input value="<?php echo $yudisium_id ?>" type="hidden" name="yudisium_id">
				</div>
				<div class="col-md-11">
			    	<select class="form-control e9" name="mhs_id">
			    		<option value="-">Pilih Mahasiwa</option>
			    		<?php
							if($mahasiswa) :
								foreach($mahasiswa as $key){
									echo "<option value='".$key->mahasiswa_id."'>".$key->nama." - ". $key->nim. " (". $key->angkatan .")"."</option>";
								}
							endif;		    		
			    		?>
					</select>
				</div>
			</form>
		</div>
	</div>
	
	<div class="block-box">
		<table class='table table-hover' id='example'>
			<thead>
				<tr>
					<th>No</th>
					<th>NIM</th>
					<th>Nama</th>
					<th class="text-center">Action</th>
				</tr>
			</thead>
				<?php
					$no = 1;
					if($yudisium_mhs) :
						foreach($yudisium_mhs as $key){
				?>
					<tr>
						<td><?php echo $no; $no++; ?></td>
						<td><?php echo $key->nim ?></td>
						<td><?php echo $key->nama ?></td>
						<td class="text-center">
							<a onclick="return confirm('Apakah Anda ingin menghapus data ini?')" href="<?php echo $this->location('module/master/yudisium/del_mhs/'.$key->mhs_id.'/'.$yudisium_id) ?>" class="btn btn-danger btn-xs">
								<i class="fa fa-trash-o"></i> Hapus
							</a>
						</td>
					</tr>
				<?php
						}
					endif;
				?>
			<tbody>
			</tbody>
		</table>
	</div>
	
	
</fieldset>
<?php $this->foot(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".e9").select2();
		$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	});
</script>