<?php $this->head(); ?>
<fieldset>
	<h2 class="title-page">Daftar Yudisium</h2>
	
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/master/yudisium'); ?>">Yudisium</a></li>
	</ol>
	
	<div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/master/yudisium/add'); ?>" class="btn btn-primary">
			<i class="fa fa-pencil"></i> Tambah Data Yudisium</a>
	</div>
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	if( isset($posts) ) :	?>
		<div class='block-box'>
			<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>No</th>
						<th>Judul</th>
						<th>Keterangan</th>
						<th>Tahun Akademik</th>
						<th>Ruang</th>
						<th>Last Update</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$i = 1;
					if($posts > 0){
						foreach ($posts as $key): 
				?>
						
					<tr valign='top'>
						<td><?php echo $i; $i++; ?></td>
						<td><label class="label label-primary"><?php echo $key->periode ?></label> <?php echo $key->judul; ?></td>
						<td><?php echo $key->keterangan; ?></td>
						<td><?php echo $key->tahun ." - ". ucfirst($key->is_ganjil) . " ". $key->is_pendek; ?></td>
						<td><?php echo $key->kode_ruang . " - " .$key->ruang; ?></td>
						<td><?php echo $key->last_update; ?></td>
						<td>
							<a class="btn btn-xs btn-primary" href="<?php echo $this->location('module/master/yudisium/detail/'.$key->yudisium_id) ?>">
								<i class="fa fa-file"></i> Detail
							</a>
							<a href="<?php echo $this->location('module/master/yudisium/edit/'.$key->yudisium_id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit</a>
							<a onclick="return confirm('Apakah Anda ingin menghapus data ini?')" href="<?php echo $this->location('module/master/yudisium/del/'.$key->yudisium_id) ?>" class="btn btn-danger btn-xs">
								<i class="fa fa-trash-o"></i> Hapus
							</a>
						</td>
					</tr>
						
				<?php
						endforeach;
					}
				?>
				</tbody>
			</table>
		</div>
	<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</fieldset>
<?php $this->foot(); ?>