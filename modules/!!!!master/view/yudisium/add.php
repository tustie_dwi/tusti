<?php $this->head(); ?>
<fieldset>
	<h2 class="title-page">Tambah Yudisium</h2>
	
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/yudisium'); ?>">Yudisium</a></li>
	  <li class="active"><a href="#">Tambah Yudisium</a></li>
	</ol>
	
	<form class="form-horizontal" role="form" method="post" action="<?php echo $this->location('module/master/yudisium/save') ?>">
		<div class="panel panel-default">
		  	<div class="panel-heading">
		  		<i class="fa fa-plus"></i> Tambah Yudisium
		  	</div>
			<div class="panel-body">	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Judul</label>
				    <div class="col-sm-10">
				    	<input type="text" class="form-control" name="judul" required="">
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Tahun Akademik</label>
				    <div class="col-sm-10">
				    	<select class="form-control e9" name="tahun_akademik">
				    		<?php
				    			if($tahun) :
									foreach($tahun  as $key){
										echo "<option value='".$key->tahun_akademik."'>".$key->tahun." - ". ucfirst($key->is_ganjil) . " ". $key->is_pendek."</option>";
									}
								endif;
				    		?>
				    	</select>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Periode</label>
				    <div class="col-sm-10">
				    	<select class="form-control" name="periode">
				    		<option>1</option>
				    		<option>2</option>
				    		<option>3</option>
				    		<option>4</option>
				    		<option>5</option>
				    	</select>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Ruang</label>
				    <div class="col-sm-10">
				    	<select class="form-control e9" name="ruang">
				    		<?php
				    			if($ruang) :
									foreach($ruang  as $key){
										echo "<option value='".$key->ruang_id."'>".$key->kode_ruang . " - " .$key->keterangan."</option>";
									}
								endif;
				    		?>
				    	</select>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Tanggal Yudisium</label>
				    <div class="col-sm-10">
				    	<input name="tgl_yudisium" class="form_datetime form-control" value="" placeholder="YYYY-MM-DD" required="" type="text">
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Keterangan</label>
				    <div class="col-sm-10">
				    	<textarea class="form-control" name="keterangan"  required=""></textarea>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label"></label>
				    <div class="col-sm-10">
				      <button class="btn btn-primary">Simpan</button>
				      <a href="<?php echo $this->location('module/master/yudisium') ?>" class="btn btn-default">Batal</a>
				    </div>
			  	</div>
			</div>
		</div>
		
		<div class="panel panel-default">
		  	<div class="panel-heading">
		  		<i class="fa fa-user"></i> Tambah Mahasiswa
		  	</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
				    	<select class="form-control e9" id="mhs_id">
				    		<option value="-">Pilih Mahasiwa</option>
				    		<?php
								if($mahasiswa) :
									foreach($mahasiswa as $key){
										$isi = $key->nama." - ". $key->nim. " (". $key->angkatan .")";
										echo "<option value='".$key->mahasiswa_id."|". $isi ."'>".$isi."</option>";
									}
								endif;		    		
				    		?>
						</select>
						<br>
						<a onclick="add_mhs()" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
					</div>
					
					<div class="col-md-6">
						<div id="mhs-wrap" class="col-md-12">
						</div>
					</div>
				</div>	
			</div>
		</div>
	</form>
		
</fieldset>
<?php $this->foot(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".e9").select2();
		$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	});
	
	function add_mhs(){
		var text = $("#mhs_id").select2('val');
		$("#mhs-wrap").append(set_label(text));
	}
	function set_label(text){
		if(text == '-') return "";	
		var data = text.split("|");
		
		$("#mhs-"+data[0]).remove();
		var text = '<div data-id="'+data[0]+'" id="mhs-'+data[0]+'" class="label label-primary">'+data[1]+' <input name="mhs[]" value="'+data[0]+'" type="hidden">'+
						'<i style="cursor: pointer" class="del-mhs fa fa-times"></i>'+
					'</div>';
		return text;
	}
	
	$(document).on('click', ".del-mhs", function(){ //comment
		if(confirm('Apakah Anda ingin menghapus data ini?')){
			var id = $(this).closest(".label").data("id");
			
			$("#mhs-"+id).remove();
		}
	});
</script>
