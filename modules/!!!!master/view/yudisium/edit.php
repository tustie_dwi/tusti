<?php $this->head(); ?>
<fieldset>
	<h2 class="title-page">Edit Yudisium</h2>
	
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/yudisium'); ?>">Yudisium</a></li>
	  <li class="active"><a href="#">Edit Yudisium</a></li>
	</ol>
	
	<div class="panel panel-default">
	  	<div class="panel-heading">
	  		<i class="fa fa-pencil"></i> Edit Yudisium
	  	</div>
		<div class="panel-body">	
			<form class="form-horizontal" role="form" method="post" action="<?php echo $this->location('module/master/yudisium/editdata') ?>">
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Judul</label>
				    <div class="col-sm-10">
				    	<input value="<?php echo $yudisium->yudisium_id ?>" name="yudisium_id" type="hidden" >
				    	<input type="text" class="form-control" name="judul" value="<?php echo $yudisium->judul ?>">
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Tahun Akademik</label>
				    <div class="col-sm-10">
				    	<select class="form-control e9" name="tahun_akademik">
				    		<?php
				    			if($tahun) :
									foreach($tahun  as $key){
										if($key->tahun_akademik == $yudisium->tahun_akademik) $select = "selected";
										else $select ="";
										
										echo "<option ".$select." value='".$key->tahun_akademik."'>".$key->tahun." - ". ucfirst($key->is_ganjil) . " ". $key->is_pendek."</option>";
									}
								endif;
				    		?>
				    	</select>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Periode</label>
				    <div class="col-sm-10">
				    	<select class="form-control" name="periode">
				    		<option <?php if($yudisium->periode == '1') echo "selected"; ?>>1</option>
				    		<option <?php if($yudisium->periode == '2') echo "selected"; ?>>2</option>
				    		<option <?php if($yudisium->periode == '3') echo "selected"; ?>>3</option>
				    		<option <?php if($yudisium->periode == '4') echo "selected"; ?>>4</option>
				    		<option <?php if($yudisium->periode == '5') echo "selected"; ?>>5</option>
				    	</select>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Ruang</label>
				    <div class="col-sm-10">
				    	<select class="form-control e9" name="ruang">
				    		<?php
				    			if($ruang) :
									foreach($ruang  as $key){
										if($key->ruang_id == $yudisium->ruang_id) $select = "selected";
										else $select ="";
										
										echo "<option ".$select." value='".$key->ruang_id."'>".$key->kode_ruang . " - " .$key->keterangan."</option>";
									}
								endif;
				    		?>
				    	</select>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Tanggal Yudisium</label>
				    <div class="col-sm-10">
				    	<input name="tgl_yudisium" class="form_datetime form-control" value="<?php echo $yudisium->tgl_yudisium ?>" placeholder="YYYY-MM-DD" required="" type="text">
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label class="col-sm-2 control-label">Keterangan</label>
				    <div class="col-sm-10">
				    	<textarea class="form-control" name="keterangan"><?php echo $yudisium->keterangan ?></textarea>
				    </div>
			  	</div>
			  	
			  	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label"></label>
				    <div class="col-sm-10">
				      <button class="btn btn-primary">Simpan</button>
				      <a href="<?php echo $this->location('module/master/yudisium') ?>" class="btn btn-default">Batal</a>
				    </div>
			  	</div>
			</form>
		</div>
	</div>
		
</fieldset>
<?php $this->foot(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".e9").select2();
		$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	});
</script>
