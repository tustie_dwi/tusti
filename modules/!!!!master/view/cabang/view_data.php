<div class="block-box">
	<?php 	
	 if(isset($posts)):	?>
	<table class='table table-hover' id='example'>
		<thead>
			<tr>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php	
			if($posts > 0){
				$i= 1;
				foreach ($posts as $dt): ?>
					<tr>
						<td>
							<div class="col-md-10">
								<?php echo $dt->keterangan ?>&nbsp;<code><?php echo $dt->cabang_id ?></code>
							</div>
							<div class="col-md-2">
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/master/conf/cabang/edit/'.$dt->cabang_id); ?>"><i class='fa fa-edit'></i> Edit</a>	
										</li>
									  </ul>
									</li>
								</ul>
							</div>
						</td>
					</tr>					
				<?php endforeach; 
			 }
		?>
		</tbody>
	</table>	
	 <?php
	 else: 
	 ?>
	<div class="row">
	<div class="col-md-12" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
	</div>
	</div>
	<?php endif; ?>
</div>