<?php $this->head(); ?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('pjj'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/master/conf/cabang'); ?>">Cabang</a></li>
  <li class="active"><a href="#">data</a></li>
</ol>
<div class="breadcrumb-more-action">
</div>

<div class="row">
	<div id="isi" class="col-md-8">
		<?php
			$this->view('cabang/view_data.php', $data );
		?>
	</div>
	
	<div class="col-md-4">
    	<?php $this->view('cabang/sidebar.php', $data); ?>
	</div>
</div>
<?php $this->foot(); ?>