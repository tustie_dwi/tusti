<?php 
if(isset($posts_edit)){
	foreach ($posts_edit as $posts) {
		$cabangid 	= $posts->cabang_id;
		$keterangan = $posts->keterangan;
		$hidId 		= $posts->cabang_id;
	}
}
?>
<form method=post name="form" id="form">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $panel ?></div>
		<div class="panel-body ">
		  	
		  	<div class="form-group">
				<label class="control-label">Cabang Id</label>								
					<input name="cabangid" class="form-control" id="cabangid" value="<?php if(isset($cabangid))echo $cabangid?>" required="" type="text">
			</div>
			
			<div class="form-group">
				<label class="control-label">Keterangan</label>								
				<textarea class="form-control" id="keterangan" name="keterangan"><?php if(isset($keterangan))echo $keterangan?></textarea>
			</div>
			
			<div class="form-actions">
				<label class="control-label">&nbsp;</label>
				<input name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>" type="hidden">
				<a name="b_cabang" id="b_cabang" class="btn btn-primary" type="submit">Data Valid &amp; Save</a>
				<?php if(isset($posts_edit)){ ?>
				<a href="<?php echo $this->location('module/master/conf/cabang'); ?>" class="btn btn-default">Cancel</a>
				<?php } ?>
			</div>			
			
		</div>
	</div>
</form>