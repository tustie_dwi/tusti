<?php 
$this->head(); 

if($posts){
	foreach($posts as $dt):
		$id 		= $dt->wall_id;
		$judul 		= $dt->judul;
		$tgl		= $dt->tgl_publish;
		$keterangan	= $dt->keterangan;
	endforeach;
}else{
	$id 		= "";
	$judul 		= "";
	$tgl		= "";
	$keterangan	= "";
}


?>

<div class="container-fluid">
	<legend><a href="<?php echo $this->location('module/master/wallinfo'); ?>" class="btn btn-primary pull-right">
    <i class="icon-pencil icon-white"></i> Wall Information List</a> Write Wall Information
	</legend>
	
	<div class="block-box">
	<form method="post" action="<?php echo $this->location('module/master/wallinfo/save'); ?>">
		<div class="form-group">
				<label >Judul</label>
				<div class="controls">
					<input type="text" name="judul" class="form-control" value="<?php echo $judul; ?>" required>
					<input type="hidden" name="hidId" value="<?php echo $id;?>">
				</div>
			</div>
			<div class="form-group">
				<label >Keterangan</label>
				<div class="controls">
					<textarea name="keterangan" class="ckeditor" id="keterangan" rows='5'><?php echo $keterangan; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label >Tgl Ditayangkan</label>
				<div class="controls">
					<input type="text" name="tgl" class="date form-control" value="<?php echo $tgl; ?>" required>
					<span class="help-block"><em><small>Tanggal informasi tsb akan di tampilkan di wall information</small></em></span>
				</div>
			</div>
			<div class="form-group">
			<input type="submit" name="b_add" value=" Publish " class="btn btn-info">
			<input type="submit" name="b_draft" value=" Save to Draft " class="btn btn-warning">
		</div>
	</form>
	</div>
</div>
<?php $this->foot(); ?>