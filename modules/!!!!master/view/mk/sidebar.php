<div id="parameter" class="block-box">
	<form action="<?php echo $this->location('module/master/akademik/mk'); ?>" role="form" id="param" method="POST">
		<div class="form-group">	
			<label class="control-label">Fakultas</label>
							
				<select id="pilih_fakultas" class="form-control e9" name="fakultas" >
				<option value="0" data-uri='1'>Select Fakultas</option>
				<?php if(count($get_fakultas)> 0) {
					foreach($get_fakultas as $dt) :
						echo "<option value='".$dt->hid_id."' ";
						if(isset($fakultasid)){
							if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
								echo "selected";
							}
						}
						echo " >".ucWords($dt->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
		</div>
	</form>
</div>