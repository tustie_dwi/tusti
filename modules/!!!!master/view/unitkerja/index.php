<?php $this->head(); ?>

<style>
	.unit-list .btn{
		border : none;
		padding: 3px 5px;
		padding-left: 10px;
	}
	 .btn-edit-post{
		padding: inherit 0 !important;
	}
</style>

 
<div class="row">
		<div class="col-md-6">	
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="<?php echo $this->location('module/master/general/unit'); ?>">Unit Kerja</a></li>
			  <li class="active"><a href="#">Data</a></li>
			</ol>
				
		<div id="notif">

		</div>
		
		<div class="tree">
			<?php
				echo "<ul>";
				foreach ($fakultas as $key) {
					echo "<li><i class='fa fa-minus-square'></i> " . $key->keterangan . "<a href=''></a>";
					get_parent($key->fakultas_id, $parent, $child, $this->location('module/master/general/unit/'));
				}
				echo "</ul>";
			?>
		</div>
	
		</div>
			<div class="col-md-6">
				<?php echo $this->view("unitkerja/edit.php", $data); ?>
			</div>	
		</div>
<?php $this->foot(); ?>

<?php
		function get_parent($fakultas_id, $parent, $child, $uri){
			echo "<ul>";
			foreach($parent as $key){
				if($key->fakultas_id == $fakultas_id) {
					if($key->is_aktif != '1'){
						$aktif = "(tidak aktif)";
					}else {
						$aktif = "";
					}
					?>
						<li class="unit-<?php echo $key->unit_id ?> unit-list">
							<span><i class='fa fa-minus-square'></i><?php echo ' ' . $key->kode . ' - ' . $key->keterangan . " " . $aktif ?></span>
							
							<div class="btn-group">
							    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							      Action
							      <span class="fa fa-caret-down"></span>
							    </button>
							    <ul class="dropdown-menu">
							      <li><a class='' href='<?php echo $uri."edit/".$key->unit_id; ?>'><i class='fa fa-edit'></i> Edit</a></li>
							      <?php if(cek_parent($child, $key->hidId) == '1'){ ?>
									<li>
										<a data-id="<?php echo $key->unit_id ?>" data-uri="<?php echo $uri . "delete" ?>" data-kode="<?php echo $key->kode ?>" data-kategori="<?php echo $key->kategori ?>" class='btn-del' href='#'>
											<i class='fa fa-times'></i> Hapus
										</a>
									</li>
								  <?php } ?>	
							    </ul>
							</div>	
					<?php
					get_child($key->hidId, $child, $uri);
				}
			}
			echo "</ul>";
		} //end of function
		
		function get_child($parentid, $child, $uri){
			echo "<ul>";
			$no = 0;
			foreach($child as $key){
				if($key->parent_id == $parentid) { //parent_id itu adalah id milik parent, yang $key itu yang current
					if($key->is_aktif != '1'){
						$aktif = "(tidak aktif)";
					}else {
						$aktif = "";
					}
					?>
						<li class="unit-<?php echo $key->unit_id ?> unit-list">
							<span><i class='fa fa-minus-square'></i><?php echo ' ' . $key->kode . ' - ' . $key->keterangan . " " . $aktif ?></span>
							<div class="btn-group">
							    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							      Action
							      <span class="fa fa-caret-down"></span>
							    </button>
							    <ul class="dropdown-menu">
							      <li><a class='' href='<?php echo $uri."edit/".$key->unit_id; ?>'><i class='fa fa-edit'></i> Edit</a></li>
							      <?php if(cek_parent($child, $key->hidId) == '1'){ ?>
									<li>
										<a data-id="<?php echo $key->unit_id ?>" data-uri="<?php echo $uri . "delete/" . $key->unit_id ?>" data-kode="<?php echo $key->kode ?>" class='btn-del' href='#'>
											<i class='fa fa-times'></i> Hapus
										</a>
									</li>
								  <?php } ?>	
							    </ul>
							</div>
					<?php
					get_child($key->hidId, $child, $uri);
					$no++;
				}
			}
			echo "</ul>";
			if($no == 0) return;
		} //end of function
		
		function cek_parent($child, $parentid){
			$init = 1;
			foreach($child as $key){
				if($key->parent_id == $parentid) {
					$init = 0;
					break;
				}
			}
			return $init;
		}
	?>