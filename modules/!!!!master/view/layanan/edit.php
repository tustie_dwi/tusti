<?php 
if($posts){
	$header		= "Edit Jaminan Mutu";
	
	$id				= $dt->hid_id;
	$kategoriid		= $dt->kategori_id;	
	$fakultas_id	= $dt->fakultas_id;
	$judul			= $dt->judul;
	$icon			= $dt->icon;
	$ispublish		= $dt->is_publish;
	$cabangid		= $dt->cabang_id;

}else{
	$header		= "Write New Jaminan Mutu";
	
}
?>

<div class="panel panel-default">
  <div class="panel-heading"><?php echo $header; ?></div>
  <div class="panel-body ">
	<form method=post  name="form" id="form-gjm" enctype="multipart/form-data">
		
		<div class="form-group">	
			<label >Fakultas</label>
			<select id="select_fakultas" class="e9 form-control" name="fakultas" >
				<option value="0" data-uri='1'>Select Fakultas</option>
				<?php if($fakultas) {
					foreach($fakultas as $dt) :
						echo "<option value='".$dt->fakultas_id."' ";
						if(isset($fakultasid)){
							if($fakultasid==$dt->fakultas_id){
								echo "selected";
							}
						}
						echo " >".ucWords($dt->keterangan)."</option>";
					endforeach;
				} ?>
			</select>
		</div>
		
		<div class="form-group">	
			<label >Cabang</label>
			<select id="select_cabang" class="e9 form-control" name="cabang" >
				<option value="0">Select Cabang</option>
				<?php if(count($cabang)> 0) {
					foreach($cabang as $c) :
						echo "<option value='".$c->cabang_id."' ";
						if(isset($cabangid)){
							if($cabangid==$c->cabang_id){
								echo "selected";
							}
						}
						echo " >".ucWords($c->keterangan)."</option>";
					endforeach;
				} ?>
			</select>
		</div>				
		
		<div class="form-group">
			<label>Kategori</label>			
			<select id="select_thn" name="cmbkategori" class="e9 form-control" >
				<option value="0">Select Kategori</option>
				<?php if($kategori) {
					foreach($kategori as $dt) :
						echo "<option value='".$dt->kategori_id."' ";
						if(isset($kategoriid)){
							if($kategoriid==$dt->kategori_id){
								echo "selected";
							}
						}
						echo " >";
						if($dt->parent_id=='0') echo strtoUpper($dt->keterangan)."</option>";						
						else echo "  -- ".$dt->keterangan."</option>";
					endforeach;
				} ?>
			</select>			
		</div>
		
				
		<div class="form-group">
			<label>Judul</label>
			<input id="judul" name='judul' required='required' type='text' value ='<?php if(isset($judul))echo $judul ?>' class="form-control">	
		</div>	

		<div class="form-group">
			<label>Keterangan</label>
			<textarea name="keterangan" cols="5" class="form-control"></textarea>
		</div>

		<div class="form-group">
			<input name='icon' type='file' class='form-control'>					
		</div>
		<div class="form-group">
			<div class="checkbox">
				<label>
				  <input type="checkbox" name="chkpublish" value="1" <?php if(isset($ispublish) && ($ispublish==1)) echo "checked" ?>> Publish
				</label>
			  </div>
		</div>
		<div class="form-group">
				<input type=hidden name="hidid" value="<?php if(isset($id)) echo $id; ?>">
				<input type="submit" name="b_submit" value="  Submit " class="btn btn-primary">
				<a href="<?php echo $this->location("module/master/gjm"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>
			</div>
		</div>			
	</form>
  </div>
</div>