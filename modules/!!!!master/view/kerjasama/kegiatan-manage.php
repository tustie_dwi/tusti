<div class="col-md-9 right-pane" style="display: none" id="form-manage">
	<div class="panel panel-default" >
		  	<div class="panel-heading">
		  		<i class="fa fa-plus"></i> Tambah Kegiatan
		  	</div>
			<div class="panel-body">	
				<form class="form-horizontal" role="form" method="post" action="<?php echo $this->location('module/master/kerjasama/save_kegiatan') ?>">
				  	<div class="form-group">
				  		<input name="id" type="hidden">
					    <label class="col-sm-2 control-label">Instansi</label>
					    <div class="col-sm-10">
					    	<select id="instansi" class="form-control e9" name="instansi_id">
					    		<option value="-">Pilih Instansi</option>
					    		<?php
									if($instansi) :
										foreach($instansi as $key) :
											echo "<option value='".$key->instansi_id."'>".$key->nama_instansi."</option>";
										endforeach;
									endif;						    		
					    		?>
					    	</select>
					    </div>
				  	</div>
				  	
				  	<div class="form-group">
					    <label class="col-sm-2 control-label">Judul</label>
					    <div class="col-sm-10">
					    	<input class="form-control" name="judul" type="text">
					    </div>
				  	</div>
				  	
				  	<div class="form-group">
					    <label class="col-sm-2 control-label">Tanggal</label>
					    <div class="col-sm-4">
					    	<input data-format="YYYY-MM-DD" onchange="check_tgl()" class="form-control pick-date" name="tgl_mulai" type="text">
					    </div>
					    <div class="col-sm-4">
					    	<input data-format="YYYY-MM-DD" onchange="check_tgl()" class="form-control pick-date" name="tgl_selesai" type="text">
					    </div>
				  	</div>
				  	
				  	<div class="form-group">
					    <label class="col-sm-2 control-label">Ruang</label>
					    <div class="col-sm-10">
					    	<select id="ruang" name="ruang[]" class="form-control select2-multiple" multiple="multiple" >
								<?php
									if($ruang) :
										foreach($ruang as $key){
											echo "<option value='".$key->ruang_id."'>".$key->kode_ruang. ' - ' . $key->keterangan."</option>";
										}
									endif;
								?>
							</select>
					    </div>
				  	</div>
				  	
				  	<div class="form-group">
					    <label class="col-sm-2 control-label">Lokasi</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" name="lokasi"></textarea>
					    </div>
				  	</div>
				  	
				  	<div class="form-group">
					    <label class="col-sm-2 control-label">Keterangan</label>
					    <div class="col-sm-10">
					    	<textarea class="form-control" type="text" name="keterangan"></textarea>
					    </div>
				  	</div>
				  	
				  	<div class="form-group">
					    <label for="inputEmail3" class="col-sm-2 control-label"></label>
					    <div class="col-sm-10">
					      <button class="btn btn-primary">Simpan</button>
					      <a onclick="batal()" class="btn btn-default">Batal</a>
					    </div>
				  	</div>
				</form>
			</div>
		</div>
</div>