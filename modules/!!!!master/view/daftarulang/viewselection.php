<?php if( isset($mhs) ) :	?>
	<table class="table table-hover" id="example">
			<thead>
				<tr>						
					<th>&nbsp;</th>						
				</tr>
			</thead>
			<tbody>
	<?php
		$i = 1;
		if($mhs){
			foreach ($mhs as $dt): 
			if($dt->is_aktif=="aktif"){
				$label = "success";
			}
			else{
				$label = "warning";
			}
			
			if($dt->is_valid=="1"){
				$valid = "Valid";
				$url   = $this->location('module/master/mhs/detail/'.$dt->mhs_id);
			}
			else{
				$valid = "Not Valid";
				$url   = $this->location('module/master/daftar/detail/'.$dt->mhs_id);
			}
			
			if($dt->foto){ $foto=$dt->foto; }else{ $foto='upload/foto/no_foto.png';}
			?>
				<tr>
					<td>
						<div class="col-md-10">
							<div class="media">
								<a class="pull-left" style="text-decoration:none" href="<?php echo $this->location('module/master/mhs/detail/'.$dt->mhs_id); ?>" >
									<img class="media-object img-thumbnail" src="<?php echo $this->asset($foto); ?>"  width="40" >
								</a>
								<div class="media-body">
							  
									<?php 
									echo "<a href='".$url."' class='text text-default'><b>".$dt->nim."</b> ".ucWords(strToLower($dt->nama))."</a>";
									?>
									<span class='label label-<?php echo $label ?>'><?php echo ucWords($dt->is_aktif) ?></span>&nbsp;
									<code><small><?php echo $valid ?></small></code>
									<br>
									<small><span class="text text-danger"><?php echo $dt->prodi;?></span><br>
									<?php
									if($dt->alamat_malang){
									echo "<i class='fa fa-map-marker'></i> alamat malang : ".$dt->alamat_malang."<br>";
									}
									if($dt->alamat_ortu){
									echo "<i class='fa fa-map-marker'></i> alamat orangtua : ".$dt->alamat_ortu."<br>";
									}
									if($dt->telp){
										echo "<i class='fa fa-phone'></i> ".$dt->telp."&nbsp; ";
									}
									if($dt->hp){
										echo "<i class='fa fa-mobile'></i> ".$dt->hp."&nbsp; ";
									}
									if($dt->email){
										echo "<i class='fa fa-envelope'></i> ".$dt->email."&nbsp; ";
									}
									?>
									</small>
								</div>
							</div>
						</div>
						<?php if($dt->is_valid!="1"){ ?>
						<div class="col-md-2">
							<ul class='nav nav-pills'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href="javascript:" onclick="validate('<?php echo $dt->daftar_id ?>','<?php echo $dt->mhs_id ?>')"><i class='fa fa-check'></i> Validate</a>
									</li>
								  </ul>
								</li>
							</ul>
						</div>
						<?php } ?>
					</td>
				</tr>
				<?php
			 endforeach; 
		 } ?>
			</tbody>
	</table>
<?php
 else: 
 ?>
	
	<div class="well">Sorry, no content to show</div>
<?php endif; ?>