<?php if(isset($posts)&&$posts!==""): ?>
	<table class='table table-hover' id="example">
		<thead>
			<tr>
				<th>Detail</th>	
				<th>&nbsp;</th>	
			</tr>
		</thead>
		<tbody>
		<?php foreach ($posts as $p) { ?>
		<tr>
			<td>
				<?php
					echo ucwords($p->judul);
					echo " <small><label class='label label-success'>".$p->status."</label></small>";
					echo " <small><label class='label label-default'>".ucwords($p->thn_akademik)."</label></small><br>";
					 
				?>
			</td>
			<td>
				<ul class='nav nav-pills'>
					<li class='dropdown pull-right'>
					  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
					  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
						<li>	
							<a class='btn-edit-post' href="<?php echo $this->location('module/master/penelitian/edit/'.$p->penelitianid); ?>" ><i class='fa fa-edit'></i> Edit</a>
							<a class='btn-edit-post' href="<?php echo $this->location('module/master/penelitian/lihat/'.$p->penelitianid); ?>" ><i class='fa fa-search'></i> Lihat</a>
							<?php if($role=="student employee" || $role=="admin"){ ?>
							<a class='btn-edit-post' href="javascript:" onclick="delete_penelitian('<?php echo $p->penelitianid ?>')"><i class='fa fa-trash-o'></i> Hapus</a>
							<?php } ?>
						</li>
					  </ul>
					</li>
				</ul>
			</td>
		</tr>
		<?php } ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="well" align="center">
		No Data Available
	</div>
<?php endif; ?>