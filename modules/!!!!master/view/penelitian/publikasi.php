<input type="hidden" name="publikasi_id" class="form-control" value="<?php if(isset($publish)) echo $publish->publish_id ?>"/>
<div id="form-jurnal">
	<div class="form-group">
		<label class="control-label">Jenis Jurnal</label>								
		<select class="form-control e7" name="jenis_jurnal">
			<option value="0">Silahkan Pilih</option>
			<option value="nasional" <?php if(isset($publish)&&$publish->jurnal_jenis=="nasional") echo "selected"; ?>>Nasional</option>
			<option value="internasional" <?php if(isset($publish)&&$publish->jurnal_jenis=="internasional") echo "selected"; ?>>Internasional</option>
		</select>
	</div>
	
	<div class="form-group">
		<label class="control-label">Nama Jurnal</label>								
		<input type="text" name="nama_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_nama ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Link Jurnal</label>								
		<input type="text" name="link_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_link ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Periode Jurnal</label>								
		<input type="text" name="periode_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_periode ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Edisi Jurnal</label>								
		<input type="text" name="edisi_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_edisi ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Volume Jurnal</label>								
		<input type="text" name="volume_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_volume ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">ISSN Jurnal</label>								
		<input type="text" name="issn_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_issn ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Halaman Jurnal</label>								
		<input type="text" name="halaman_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_page ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Lokasi Jurnal</label>								
		<input type="text" name="lokasi_jurnal" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_lokasi ?>"/>
	</div>
</div>
<div id="form-seminar">
	<div class="form-group">
		<label class="control-label">Tanggal Pelaksanaan Seminar</label>								
		<input type="text" name="tgl_pelaksanaan_seminar" class="form-control form_datetime" autocomplete="off" value="<?php if(isset($publish)) echo $publish->tgl_publish ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Nama Seminar</label>								
		<input type="text" name="nama_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->nama_kegiatan ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Lokasi Seminar</label>								
		<input type="text" name="lokasi_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->lokasi_publish ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Jenis Jurnal</label>								
		<select class="form-control e7" name="jenis_jurnal_seminar">
			<option value="0">Silahkan Pilih</option>
			<!--<option value="proceeding" <?php //if(isset($publish)&&$publish->jurnal_jenis=="proceeding") echo "selected"; ?>>Proceeding</option>-->
			<option value="nasional" <?php if(isset($publish)&&$publish->jurnal_jenis=="nasional") echo "selected"; ?>>Nasional</option>
			<option value="internasional" <?php if(isset($publish)&&$publish->jurnal_jenis=="internasional") echo "selected"; ?>>Internasional</option>
		</select>
	</div>
	
	<div class="form-group">
		<label class="control-label">Nama Jurnal</label>								
		<input type="text" name="nama_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_nama ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Link Jurnal</label>								
		<input type="text" name="link_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_link ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Periode Jurnal</label>								
		<input type="text" name="periode_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_periode ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Edisi </label>								
		<input type="text" name="edisi_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_edisi ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">ISSN</label>								
		<input type="text" name="issn_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_issn ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Volume </label>								
		<input type="text" name="volume_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_volume ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Nama Editor </label>								
		<input type="text" name="editor_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->editor ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Halaman Jurnal</label>								
		<input type="text" name="halaman_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_page ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Penerbit Jurnal</label>								
		<input type="text" name="penerbit_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_penerbit ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Lokasi Jurnal</label>								
		<input type="text" name="lokasi_jurnal_seminar" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->jurnal_lokasi ?>"/>
	</div>
</div>
<div id="form-buku">
	<div class="form-group">
		<label class="control-label">Penerbit Buku</label>								
		<input type="text" name="penerbit_buku" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->buku_penerbit ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">ISBN</label>								
		<input type="text" name="isbn_buku" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->buku_isbn ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">Judul Buku</label>								
		<input type="text" name="judul_buku" class="form-control" autocomplete="off" value="<?php if(isset($publish)) echo $publish->buku_judul ?>"/>
	</div>
	<div class="form-group">
		<label class="control-label">File Buku (e-book)</label>	
		<div id="preview_file_publish" class="well">
			<?php
			if(isset($publish)&&isset($publish->buku_file)){
				$file_name = basename($publish->buku_file);
				echo $file_name; 
			?>
				<input type="hidden" name="file_loc_buku" id="file_loc_buku" value="<?php if(isset($publish->buku_file)) echo $publish->buku_file; ?>">
				&nbsp;<a href="javascript:" onclick="remove_selected('preview_file_publish')"><i class="fa fa-trash-o"></i></a>
			<?php
			}else echo "Belum ada dokumen pendukung";
			?>
		</div>							
		<input type="file" name="file_buku" class="form-control"/>
	</div>
</div>
		