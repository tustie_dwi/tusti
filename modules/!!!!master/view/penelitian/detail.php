<?php $this->head();?>
<h2 class="title-page">Penelitian dan Pengabdian</h2>
<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/master/penelitian'); ?>">Penelitian</a></li>
		  <li><a href="<?php echo $this->location('module/master/penelitian/lihat/'.$id); ?>">Lihat Penelitian</a>
		  </li>
		</ol>
		<div class="row">
			<div class="col-sm-6">
				<div class="block-box">
					<div class="header">Detail <?php echo ucwords($posts_single->kategori);?></div>
					<div class="content">
						<table>
							<tr>
								<td>Fakultas</td>
								<td>: <?php echo $posts_single->fakultas;?></td>
							</tr>
							<tr>
								<td>Cabang</td>
								<td>: <?php echo $posts_single->cabang; ?></td>
							</tr>
							<tr>
								<td>Judul <?php echo ucwords($posts_single->kategori); ?></td>
								<td>: <?php echo $posts_single->judul; ?></td>
							</tr>
							<tr>
								<td>Tahun Akademik</td>
								<td>: <?php echo ucwords($posts_single->thn_akademik); ?></td>
							</tr>
							<tr>
								<td>Keterangan <?php echo ucwords($posts_single->kategori); ?></td>
								<td>: <?php echo $posts_single->keterangan_penelitian; ?></td>
							</tr>
							<tr>
								<td>Sumber Dana</td>
								<td>: <?php echo $posts_single->inf_sumber_dana; ?></td>
							</tr>
							<tr>
								<td valign="top">Peserta</td>
								<td valign="top">: 
									<?php
									$i=1;
									foreach($peserta as $psrt){
										if($psrt->is_ketua==1) $ketua = "<code>Ketua</code>";
										else $ketua ="";
										echo "<br>".$i.". ".$psrt->nama." ".$ketua;
										$i++; 
									}
									?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="block-box">
					<div class="header">Detail Progress</div>
					<div class="content">
						<table>
							<?php
							$i=1;
							foreach($progress as $pr){ ?>
								<tr>
									<td><?php echo $i.". " ?></td>
									<td><?php echo $pr->status?></td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>Tanggal</td>
									<td>: <?php 
											$originalDate = substr($pr->tgl_pelaksanaan, 0,10);
											$newDate = date("d F Y", strtotime($originalDate));
											echo $newDate ?>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>Waktu</td>
									<td>: <?php echo substr($pr->tgl_pelaksanaan, 10,19);?>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>Tempat</td>
									<td>: <?php echo $pr->lokasi?></td>
								</tr>
							<?php
							$i++;
							}
							?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->foot(); ?>