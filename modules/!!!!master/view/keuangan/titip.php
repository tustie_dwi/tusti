<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Keuangan</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Keuangan</a></li>
		  <li class="active"><a href="#">Dosen Titip</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-8">
		<div class='block-box'>
			<?php
				echo "<h3 style='margin-top: 10px' class='text-center'><i class='fa fa-calendar'></i> ".$tahun_detail->tahun." - ".ucfirst($tahun_detail->is_ganjil). " ". $tahun_detail->is_pendek."</h3>";
			?>
		</div>
		<div class='block-box'>
			<div class="col-md-6">
			<select class="form-control" id="tahun">
				<option value='-'>Pilih Tahun Akademik</option>
				<?php
					if($tahun) :
						foreach($tahun as $key){
							echo "<option value='".$key->tahun_akademik."' ";
							if($key->tahun_akademik==$tahun_tmp) echo "selected";
							echo ">".$key->tahun." - ".ucfirst($key->is_ganjil). " ". $key->is_pendek."</option>";
						}
					endif;
				?>
			</select>
			</div>
			<?php if($titip) : ?>
			<table class='table table-hover example' style="font-size: 100%">
				<thead>
					<tr>
						<th>Dosen SK</th>
						<th>Dosen Titip</th>
						<th width="20%">Menu</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($titip as $key) : ?>
						<tr>
							<td><?php echo $key->dosen_sk_nama . '<br><code>'.$key->dosen_sk_nik.'</code>' ?></td>
							<td><?php echo $key->dosen_titip_nama . '<br><code>'.$key->dosen_titip_nik.'</code>' ?> <span class="label label-info"><?php echo $key->prodi?></span></td>
							<td>
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
											<a class='btn-edit-post btn-edit-titip' 
												data-dosen_sk="<?php echo $key->dosen_sk_karyawan_id ?>"
												data-dosen_titip="<?php echo $key->dosen_titip_karyawan_id ?>"
												data-id="<?php echo $key->titip_id ?>" 
												data-prodi="<?php echo $key->prodi ?>" 
												data-semester="<?php echo $key->tahun_akademik ?>"
											>
												<i class='fa fa-edit'></i> Edit
											</a>
											
											<a class='btn-edit-post' onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')" href='<?php echo $this->location("module/master/keuangan/del_titip/". $key->titip_id) ?>'>
												<i class='fa fa-trash-o'></i> Hapus
											</a>
										</li>
									  </ul>
									</li>
								</ul>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php else : ?>
				<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel panel-default">
			  	<div class="panel-heading">
			  		<i class="fa fa-plus"></i> Tambah Dosen Titip
			  	</div>
				<div class="panel-body">	
					<form role="form" method="post" action="<?php echo $this->location('module/master/keuangan/save_titip') ?>">	
						<div class="form-group">
							<label>Tahun Akademik</label>
							<select class="form-control" name="cmbtahun" id="cmbtahun">
								<?php
								if($tahun):
									foreach($tahun as $key):
										echo "<option value='".$key->id."' >".$key->tahun." ".$key->is_ganjil." ".$key->is_pendek."</option>";
									endforeach;
								endif;									
								?>
							</select>
						</div>
						<div class="form-group">
						    <label >Dosen SK</label>
						    <input id="id" name="titip" type="hidden">
						    <select class="form-control" name="dosen_sk" id="dosen_sk">
						    	<option value="-">Pilih Dosen</option>
							    <?php
									if($dosen){
										foreach($dosen as $key):
											echo "<option value='".$key->karyawan_id."'>".$key->nama."</option>";
										endforeach;
									}
								?>
							</select>
						</div>
						
						<div class="form-group">
						    <label >Dosen Titip</label>
						    <input name="titip_id" type="hidden">
						    <select class="form-control e9" name="dosen_titip" id="dosen_titip">
						    	<option value="-">Pilih Dosen Titip</option>
							    <?php
									if($dosen){
										foreach($dosen as $key):
											echo "<option value='".$key->karyawan_id."'>".$key->nama."</option>";
										endforeach;
									}
								?>
							</select>
						</div>
						
						<div class="form-group">
							<label>Prodi</label>
							<select class="form-control" name="cmbprodi" id="cmbprodi">
								<?php
								if($prodi):
									foreach($prodi as $key):
										echo "<option value='".$key->id."' >".$key->value."</option>";
									endforeach;
								endif;									
								?>
							</select>
						</div>
					
						<div class="form-group">
						    <button class="btn btn-primary">Simpan</button>
						    <a onclick="batal()" class="btn btn-default">Batal</a>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>
<?php $this->foot(); ?>