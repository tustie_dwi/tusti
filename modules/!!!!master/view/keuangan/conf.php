<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Keuangan</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Keuangan</a></li>
		  <li class="active"><a href="#">Konfigurasi</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-8">
		<div class='block-box'>
			<?php if($conf) : ?>
			<table class='table table-hover' id='example' style="font-size: 100%">
				<thead>
					<tr>
						<th width="10%">No.</th>
						<th>Kategori</th>
						<th width="20%">Menu</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($conf as $key) : ?>
						<tr>
							<td><?php echo $no; $no++ ?></td>
							<td>
								Golongan <?php echo $key->golongan . ' / ' . $key->pangkat . ' / ' . $key->jabatan . ' / ' . $key->jenjang_pendidikan ?>
								<br><?php echo "<i class='fa fa-money'></i> Rp. " . $key->tarif ?> <code>Potongan : <?php echo $key->potongan . '%' ?></code>
							</td>
							<td>
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
											<a class='btn-edit-post' 
												data-id="<?php echo $key->conf_id ?>"
												data-golongan="<?php echo $key->golongan ?>"
												data-jenjang_pendidikan="<?php echo $key->jenjang_pendidikan ?>"
												data-tarif="<?php echo $key->tarif ?>"
												data-potongan="<?php echo $key->potongan ?>"
												data-is_pns="<?php echo $key->is_pns ?>"
											>
												<i class='fa fa-edit'></i> Edit
											</a>
										</li>
									  </ul>
									</li>
								</ul>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php else : ?>
				<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel panel-default">
			  	<div class="panel-heading">
			  		<i class="fa fa-plus"></i> Tambah Kategori
			  	</div>
				<div class="panel-body">	
					<form role="form" method="post" action="<?php echo $this->location('module/master/keuangan/save_conf') ?>">
						<div class="form-group">
						    <label >Golongan</label>
						    <input name="conf_id" type="hidden">
						    <select class="form-control" name="golongan">
						    	<option value="">Pilih Golongan</option>
							    <?php
									if($golongan){
										foreach($golongan as $key):
											echo "<option value='".$key->golongan."'>".$key->golongan." - ". $key->pangkat."</option>";
										endforeach;
									}
								?>
							</select>
						</div>
						<div class="form-group">
						    <label >Jenjang Pendidikan</label>
						    <select name="jenjang_pendidikan" class="form-control">
						    	<option value="">Pilih Jenjang Pendidikan</option>
						    	<option value="D3">D3</option>
						    	<option value="D4">D4</option>
						    	<option value="S1">S1</option>
						    	<option value="S2">S2</option>
						    	<option value="S3">S3</option>
						    	<option value="Guru Besar">Guru Besar</option>
						    </select>
						</div>
						<div class="form-group">
						    <label >Tarif</label>
						    <input autocomplete="off" name="tarif" required="" type="text" class="form-control">
						</div>
						<div class="form-group">
						    <label >Potongan</label>
						    <div class="input-group">
								<input min="0" autocomplete="off" name="potongan" required="" type="number" type="text" class="form-control">
								<span class="input-group-addon">%</span>
							</div>
						    <input type="checkbox" name="is_pns" placeholder="Kategori Kerjasama"> PNS
						</div>
						<div class="form-group">
						    <button class="btn btn-primary">Simpan</button>
						    <a onclick="batal()" class="btn btn-default">Batal</a>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>
<?php $this->foot(); ?>