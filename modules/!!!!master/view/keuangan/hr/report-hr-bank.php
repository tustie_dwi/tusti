	<link rel="stylesheet" type="text/css" href="<?php echo $this->asset("css/v4/style-print.min.css");?>" />
	<style>
	.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead{ border-top:solid 1px #444;}
		.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{
			font-size:12px;
			border: solid 1px #444;
		}
	</style>
	<?php 
	$minfo=new model_honor();
	foreach($prodi as $row):
		$post_data = $mconf->get_rekap_absen_bayar_cetak($row->id, $mulai, $selesai, $kode,"",$selisih);
		if($post_data):
		?>
		<div id="header-report">
			<h6 style="text-align: left;">KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN<br>UNIVERSITAS BRAWIJAYA<br> 
			<span style="font-weight: bold; font-size: 0.8em;">PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER</span><br> 	
			<p>&nbsp;</p>		
				
			<table> 
				<tr valign="top"> 
					<td>
					<?php 
						$data_custom = explode("|", $custom);
						echo $data_custom[0];
						
						if(!isset($data_custom[1])) $data_custom[1] = '';
						if(!isset($data_custom[2])) $data_custom[2] = '';
						if(!isset($data_custom[3])) $data_custom[3] = '';
					?>
					<br>
					PROGRAM STUDI <?php echo strtoUpper($row->value); ?>
					</td>
				</tr>
				<!--<tr valign="top">
					<td>BULAN</td>
					<td>:</td>
					<td><?php //echo strToUpper($minfo->get_nama_bulan(date("m", strtotime($selesai)))) ." ". date("Y", strtotime($selesai)); ?></td>
				</tr> -->
			</table>
		</div>
		<div class="content-print row">
		<table class="table table-bordered" width="100%">
			<thead>				
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Rekening</th>
					<th>Jumlah</th>
				</tr>
			</thead>
			<tbody>
			<?php
					$i =0;
					
						foreach($post_data as $key) {
							$i++;
							if(($key->gelar_awal)&&($key->gelar_awal!="-")){
								$gawal= ", ".$key->gelar_awal;
							}else{
								$gawal="";
							}
							
							if(($key->gelar_akhir)&&($key->gelar_akhir!="-")){
								$gakhir= ", ".$key->gelar_akhir;
							}else{
								$gakhir="";
							}
									
						//if($mhs_tmp != ''){
							$dosen[$key->karyawan_id]['nama'] = $key->nama.$gawal.$gakhir;
							$dosen[$key->karyawan_id]['karyawan_id'] = $key->karyawan_id;
							$dosen[$key->karyawan_id]['golongan'] = $key->golongan;
							$dosen[$key->karyawan_id]['nik'] = $key->nik;
							$dosen[$key->karyawan_id]['rekening'] = $key->rekening;
							$dosen[$key->karyawan_id]['prodi'] = $key->prodi_id;
							$dosen[$key->karyawan_id]['pangkat'] = $key->pangkat;
							$dosen[$key->karyawan_id]['jabatan'] = $key->jabatan;
							$dosen[$key->karyawan_id]['pendidikan'] = $key->pendidikan_terakhir;
							$dosen[$key->karyawan_id]['kode'] = $key->bayar_id;
							$dosen[$key->karyawan_id]['mku'] = $key->is_mku;
							$dosen[$key->karyawan_id]['no'] = $i;
							if($key->is_nik=='nik') $dosen[$key->karyawan_id]['pns'] = 0;
							else $dosen[$key->karyawan_id]['pns'] = 1;
						//}
							$detail = $minfo->get_rekap_detail_bayar_cetak($row->id, $mulai, $selesai, $key->karyawan_id,$kode,"",$selisih);
							$rowspan = count($detail);						
							$dosen[$key->karyawan_id]['kolom'] = $rowspan;																

							$dosen_tmp = $key->karyawan_id;											
						}
						print_nilai($dosen, $mconf,$row->id, $mulai, $selesai, $kode,$selisih);						
					
					 ?>				 
			</tbody>
		</table>
		<div class="pull-right">
				<p>Malang, <?php echo date("d"). " ". $minfo->get_nama_bulan(date("m")) ." ". date("Y"); ?><br>
				<?php echo $data_custom[1]; ?></p>
				<p>&nbsp;</p><p>&nbsp;</p>
				<p><?php echo $data_custom[3]; ?><br>
				NIP. <?php echo $data_custom[2]; ?></p>
			</div>
		</div>
		<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
		<div class="page-break" id="footer"></div>
	<?php
	endif;
endforeach;

function print_nilai($dosen, $mconf, $prodi, $mulai, $selesai, $kode, $selisih){
		//$mconf = new model_conf();
		//rumus = (jumlah ttap muka ) * (jumlah sks - kewajiban mengajar) * tarif - pajak
		$k=0;
		$total_honor=0;
		$total_honor_pph=0;
		$total_honor_all=0;
		foreach($dosen as $key => $value){
		$k++;
			//$rows = $value['kolom'];
			/*if($selisih) $detail = $mconf->get_rekap_detail_absen_titip($prodiid, $mulai, $selesai, $value['karyawan_id'], $kode);
			else $detail = $mconf->get_rekap_detail_bayar($prodiid, $mulai, $selesai, $value['karyawan_id'], $kode);*/
			$detail = $mconf->get_rekap_detail_bayar_cetak($prodi, $mulai, $selesai, $value['karyawan_id'], $kode,"",$selisih);
			
				
			if($value['pns']==1) $strpns = "NIP";
			else $strpns = "NIK";
			
			if($detail):
				$j=0;
				$total_all=0;
				$total_bersih = 0;
				$total_sks = 0;
				$total_hadir = 0;
				foreach($detail as $dt){
					$j++;
					
					$tarif = $dt->satuan;
					$pph = $dt->pph;
					$satuan = $dt->satuan;
					
					$kewajiban = ($dt->jml_mgg * 6);
					
					
					 $sks_valid = $dt->sks;
					
					$total_sks = $total_sks + $sks_valid;
					$total_hadir = $total_hadir + $dt->hadir;
					
				
					$total = ($sks_valid * $dt->hadir) * $satuan;
					
										
					$wajib =  $dt->kewajiban;
					
					
					$total_pph = $dt->pph;
										
					$jumlah = ($total - $wajib) - $total_pph;
					$total_all = $total_all + $jumlah;					
					
					$total_bersih_sum = $total_all;
					
					/*--- total akhir --*/
					$total_honor = $total_honor + ($total-$wajib);
					$total_honor_pph = $total_honor_pph + $total_pph;
					$total_honor_all = $total_honor - $total_honor_pph;
					
				}
				
				echo "<tr><td>";
						echo $k.".";
						echo "</td><td>".$value['nama']."<small>";
						/*if($value['nik'] && ($value['nik']!='-')) echo "<br><b>".$strpns.". ".$value['nik']."</b>";
						if($value['golongan'] && ($value['golongan']!='-')) echo " <br>".$value['golongan'];
						if($value['pangkat'] && ($value['pangkat']!='-')) echo " / ".$value['pangkat'];
						if($value['jabatan'] && ($value['jabatan']!='-')) echo " / ".$value['jabatan'];*/
						
						echo "</small></td><td>".$value['rekening']."</td>";
						echo "<td align='right'><b>".$total_bersih_sum."</b></td>";	
						echo "</tr>";
			endif;				
			
		}
		
		/*echo "<tr valign='top'>";
			echo "<td colspan=3>Jumlah Honor Tidak Kena Pajak</td><td colspan=4 align='right'><b>".number_format($total_honor)."</b></td><td></td>";
		echo "</tr>";
		echo "<tr valign='top'>";
			echo "<td colspan=3>Jumlah Honor Kena Pajak</td><td colspan=4 align='right'><b>".number_format($total_honor_pph)."</b></td><td></td>";
		echo "</tr>";*/
		echo "<tr valign='top'>";
			echo "<td colspan=3>Jumlah</td><td align='right'><b>".$total_honor_all."</b></td>";
		echo "</tr>";
		echo "<tr valign='top'>";
			echo "<td colspan=4>&nbsp;</td>";
		echo "</tr>";
		
	}


?>
<script src="<?php echo $this->asset("js/jsread.js"); ?>"></script>

