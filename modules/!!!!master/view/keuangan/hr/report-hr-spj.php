	<link rel="stylesheet" type="text/css" href="<?php echo $this->asset("css/v4/style-print.min.css");?>" />
	<style>
	.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead{ border-top:solid 1px #444;}
		.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{
			font-size:12px;
			border: solid 1px #444;
		}
		table{ font-size:12px;
			border: solid 0px #fff; }
	</style>
	<?php 
	$minfo=new model_honor();
	foreach($prodi as $row):
		$post_data = $mconf->get_rekap_absen_bayar_cetak($row->id, $mulai, $selesai, $kode,"",$selisih);
		if($post_data):
		?>
		<div id="header-report">
			<h6 style="text-align: left;">KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN<br>UNIVERSITAS BRAWIJAYA<br> 
			<span style="font-weight: bold; font-size: 0.8em;">PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER</span><br> 	
			<p>&nbsp;</p>		
				
			<table> 
				<tr valign="top"> 
					<td>
					<?php 
						$data_custom = explode("|", $custom);
						echo $data_custom[0];
						
						if(!isset($data_custom[1])) $data_custom[1] = '';
						if(!isset($data_custom[2])) $data_custom[2] = '';
						if(!isset($data_custom[3])) $data_custom[3] = '';
					?>
					<br>
					PROGRAM STUDI <?php echo strtoUpper($row->value); ?>
					</td>
				</tr>
				<!--<tr valign="top">
					<td>BULAN</td>
					<td>:</td>
					<td><?php //echo strToUpper($minfo->get_nama_bulan(date("m", strtotime($selesai)))) ." ". date("Y", strtotime($selesai)); ?></td>
				</tr> -->
			</table>
		</div>
		<div class="content-print row">
		<table class="table table-bordered" width="100%">
			<thead>
				
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Gol</th>
					<th>Matakuliah</th>
					<th>Kelas</th>
					<th>SKS</th>			
					<th>Hadir</th>
					<th>Harga Satuan</th>
					<th>Jumlah</th>
					<th>PPh</th>
					<th colspan="2">Jumlah Diterima</th>
					<th>Tanda Tangan</th>
				</tr>
			</thead>
			<tbody>
			<?php
					$i =0;
					
						foreach($post_data as $key) {
							$i++;
							if(($key->gelar_awal)&&($key->gelar_awal!="-")){
								$gawal= ", ".$key->gelar_awal;
							}else{
								$gawal="";
							}
							
							if(($key->gelar_akhir)&&($key->gelar_akhir!="-")){
								$gakhir= ", ".$key->gelar_akhir;
							}else{
								$gakhir="";
							}
									
						//if($mhs_tmp != ''){
							$dosen[$key->karyawan_id]['nama'] = $key->nama.$gawal.$gakhir;
							$dosen[$key->karyawan_id]['karyawan_id'] = $key->karyawan_id;
							$dosen[$key->karyawan_id]['golongan'] = $key->golongan;
							$dosen[$key->karyawan_id]['nik'] = $key->nik;
							$dosen[$key->karyawan_id]['prodi'] = $key->prodi_id;
							$dosen[$key->karyawan_id]['pangkat'] = $key->pangkat;
							$dosen[$key->karyawan_id]['jabatan'] = $key->jabatan;
							$dosen[$key->karyawan_id]['pendidikan'] = $key->pendidikan_terakhir;
							$dosen[$key->karyawan_id]['kode'] = $key->bayar_id;
							$dosen[$key->karyawan_id]['rekening'] = $key->rekening;
							$dosen[$key->karyawan_id]['mku'] = $key->is_mku;
							$dosen[$key->karyawan_id]['no'] = $i;
							if($key->is_nik=='nik') $dosen[$key->karyawan_id]['pns'] = 0;
							else $dosen[$key->karyawan_id]['pns'] = 1;
						//}
							$detail = $minfo->get_rekap_detail_bayar_cetak($row->id, $mulai, $selesai, $key->karyawan_id,$kode,"",$selisih);
							$rowspan = count($detail);						
							$dosen[$key->karyawan_id]['kolom'] = $rowspan;																

							$dosen_tmp = $key->karyawan_id;											
						}
						print_nilai($dosen, $mconf,$row->id, $mulai, $selesai, $kode,$selisih);						
					
					 ?>				 
			</tbody>
		</table>
		<div class="pull-right">
				<p>Malang, <?php echo date("d"). " ". $minfo->get_nama_bulan(date("m")) ." ". date("Y"); ?><br>
				<?php echo $data_custom[1]; ?></p>
				<p>&nbsp;</p><p>&nbsp;</p>
				<p><?php echo $data_custom[3]; ?><br>
				NIP. <?php echo $data_custom[2]; ?></p>
			</div>
		</div>
		<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
		<div class="page-break" id="footer"></div>
	<?php
	endif;
endforeach;

function print_nilai($dosen, $mconf, $prodi, $mulai, $selesai, $kode,$selisih){
		//$mconf = new model_conf();
		//rumus = (jumlah ttap muka ) * (jumlah sks - kewajiban mengajar) * tarif - pajak
		$k=0;
		$total_honor=0;
		$total_honor_pph=0;
		$total_honor_all=0;
		
		$m=0;
		foreach($dosen as $key => $value){
			$k++;
			
			
			
			if($k % 2){
				$style= "";
			}else{
				$style= "style='text-align:left'";
			}
		
			$rows = $value['kolom'];
			$detail = $mconf->get_rekap_detail_bayar_cetak($prodi, $mulai, $selesai, $value['karyawan_id'], $kode,"",$selisih);
			
				
			if($value['pns']==1) $strpns = "NIP";
			else $strpns = "NIK";
			
			if($detail):
				$m++;
				$j=0;
				$total_all=0;
				$total_bersih = 0;
				$total_sks = 0;
				$total_hadir = 0;
				foreach($detail as $dt){
					$j++;
					
					$tarif = $dt->satuan;
					$pph = $dt->pph;
					$satuan = $dt->satuan;
					
					$kewajiban = ($dt->jml_mgg * 6);
					
					
					if($j==1){
						echo "<tr><td rowspan='".$rows."' >";
						echo $m.".";
						echo "</td><td rowspan='".$rows."'>".$value['nama']."<small>";
						if($value['nik'] && ($value['nik']!='-')) echo "<br><b>".$strpns.". ".$value['nik']."</b>";
						/*if($value['golongan'] && ($value['golongan']!='-')) echo " <br>".$value['golongan'];
						if($value['pangkat'] && ($value['pangkat']!='-')) echo " / ".$value['pangkat'];
						if($value['jabatan'] && ($value['jabatan']!='-')) echo " / ".$value['jabatan'];*/
						
						echo "</small></td>";
						echo "<td rowspan='".$rows."'>".$value['golongan']."</td>";
					}
					
					$sks_valid = $dt->sks;
					
					$total_sks = $total_sks + $sks_valid;
					$total_hadir = $total_hadir + $dt->hadir;
					
					//$total = (($sks_valid * $dt->jml) - $kewajiban) * $satuan;
					$total = ($sks_valid * $dt->hadir) * $satuan;
					$wajib =  $dt->kewajiban;
					
					if($wajib > 0){
						//$kehadiran = (($sks_valid * $dt->hadir) - ($dt->jml_mgg * 6))/$sks_valid;
						$kehadiran = (($sks_valid * $dt->hadir) - ($dt->potongan));
					}else{
						$kehadiran = $dt->hadir;
					}
									
					$kehadiran = $dt->hadir;
					echo "<td><b>".$dt->kode_mk. "</b> - ".$dt->nama_mk ;
					if($dt->praktikum==1) echo "<small><em> *</em></small>";
					echo "</td>";
					echo "<td align='center'>".$dt->kelas_id."</td>";
					echo "<td align='center'>".$sks_valid."</td>";
					echo "<td align='center'>".$kehadiran."</td>";
					//echo "<td><b>".$kewajiban."</b><br><small>*) ".$numWeeks." mgg</small></td>";					
					echo "<td align='right'>".number_format($satuan)."</td>";
					echo "<td align='right'>".number_format($total-$wajib)."</td>";
										
							
					
					$total_pph = $dt->pph;
										
					$jumlah = ($total - $wajib) - $total_pph;
					$total_all = $total_all + $jumlah;					
					
					$total_bersih_sum = $total_all;
					
					/*--- total akhir --*/
					$total_honor = $total_honor + ($total-$wajib);
					$total_honor_pph = $total_honor_pph + $total_pph;
					$total_honor_all = $total_honor - $total_honor_pph;
					
					
					echo "<td align='right'>".number_format($total_pph)."</td>";	
					echo "<td align='right'>".number_format($jumlah)."</td>";
						if($j==$rows):							
							echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";							
						else:							
							echo "<td></td>";	
						endif;
						if($j==1):
							echo "<td rowspan='".$rows."' ".$style." >".$m.". </td>";
						endif;						
					echo "</tr>";
				}
				
				
			endif;	

			
			
		}
		
		echo "<tr valign='top'>";
			echo "<td colspan=8 align='right'><b>Jumlah</b></td><td align='right'><b>".number_format($total_honor)."</b></td>";
		
			echo "<td align='right'><b>".number_format($total_honor_pph)."</b></td>";
		
			echo "<td colspan='2' align='right'><b>".number_format($total_honor_all)."</b></td>";
		echo "</tr>";
		
	}


?>


