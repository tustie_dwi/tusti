	<link rel="stylesheet" type="text/css" href="<?php echo $this->asset("css/v4/style-print.min.css");?>" />
	<style>
	.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead{ border-top:solid 1px #444;}
		.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{
			font-size:12px;
			border: solid 1px #444;
		}
		.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td { padding:4px; line-height:1;}
	</style>
	<?php $minfo=new model_honor();?>
	<div class="content-print row" style="margin-left:10px; margin-right:10px;">
		<?php
				$i = 1;
				
					foreach($posts_data as $key) {
						if(($key->gelar_awal)&&($key->gelar_awal!="-")){
							$gawal= ", ".$key->gelar_awal;
						}else{
							$gawal="";
						}
						
						if(($key->gelar_akhir)&&($key->gelar_akhir!="-")){
							$gakhir= ", ".$key->gelar_akhir;
						}else{
							$gakhir="";
						}
						$dosen[$key->karyawan_id]['nama'] = $key->nama.$gawal.$gakhir;
						$dosen[$key->karyawan_id]['karyawan_id'] = $key->karyawan_id;
						$dosen[$key->karyawan_id]['golongan'] = $key->golongan;
						$dosen[$key->karyawan_id]['nik'] = $key->nik;
						$dosen[$key->karyawan_id]['prodi'] = $key->prodi_id;
						$dosen[$key->karyawan_id]['pangkat'] = $key->pangkat;
						$dosen[$key->karyawan_id]['jabatan'] = $key->jabatan;
						$dosen[$key->karyawan_id]['pendidikan'] = $key->pendidikan_terakhir;
						$dosen[$key->karyawan_id]['kode'] = $key->bayar_id;
						$dosen[$key->karyawan_id]['mku'] = $key->is_mku;
						if($key->is_nik=='nik') $dosen[$key->karyawan_id]['pns'] = 0;
						else $dosen[$key->karyawan_id]['pns'] = 1;
					//}
						$detail = $minfo->get_rekap_detail_bayar_cetak_amplop($prodiid, $mulai, $selesai, $key->karyawan_id);
						$rowspan = count($detail);						
						$dosen[$key->karyawan_id]['kolom'] = $rowspan;																

						$dosen_tmp = $key->karyawan_id;											
					}
					print_hr($dosen, $mconf,$prodiid, $mulai, $selesai, $kode);		
				?>	
						
		
<?php	

function print_hr($dosen, $mconf, $prodiid, $mulai, $selesai, $kode){
		//$mconf = new model_conf();
		//rumus = (jumlah ttap muka ) * (jumlah sks - kewajiban mengajar) * tarif - pajak
		$k=0;
		$total_honor_all_sum =0;
		foreach($dosen as $key => $value){
			$total_honor=0;
			$total_honor_pph=0;
			$total_honor_wajib=0;
			$total_honor_all=0;
			$k++;
			
			if($k % 2){
				$style= "";
			}else{
				$style= "style='text-align:center'";
			}
		
			$rows = $value['kolom'];
			$detail = $mconf->get_rekap_detail_bayar_cetak_amplop($prodiid, $mulai, $selesai, $value['karyawan_id']);			
			?>
			<table style="width: 100%;"> 
				<tr> 
					<td valign="top"> 
						<h6 style="text-align: left;">KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN<br>UNIVERSITAS BRAWIJAYA<br> 
						<span style="font-weight: bold; font-size: 0.8em;">PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER</span><br> 
						<span style="font-weight: normal; font-size: 0.8em;">Jl.Veteran, Malang Telp. 0341-577911, Fax 0341-577911, Email: ptiik@ub.ac.id</span> 
						<hr> 
						</h6> 
					</td> 
				</tr> 
			</table>
			<?php echo $value['nama']; ?>
			</br>
			<table class="table table-bordered" style="padding:2px">
				<tr>
					<th>No</th>
					<th>Nama MK</th>
					<th>Jumlah</th>	
					<th>Wajib</th>
					<th>PPh</th>					
					<th>Jumlah Diterima</th>		
				</tr>
			
			<?php
			
			
			if($detail):
				
				$j=0;
				
				$m=0;
				$total_all=0;
				$total_bersih = 0;
				$total_sks = 0;
				$total_hadir = 0;
				$z=1;
				foreach($detail as $dt){
					$j++;
					
					$m++;
					
					if($dt->kewajiban==0):
					echo "<tr>";
					$tarif = $dt->satuan;
					$pph = $dt->pph;
					$satuan = $dt->satuan;
					
					$kewajiban = ($dt->jml_mgg * 6);
					
					$total_hadir = $total_hadir + $dt->hadir;
					
					 $sks_valid = $dt->sks;
					
					$total_sks = $total_sks + $sks_valid;
					$total_hadir = $total_hadir + $dt->hadir;
					
					//$total = (($sks_valid * $dt->jml) - $kewajiban) * $satuan;
					//$total = ($sks_valid * $dt->hadir) * $satuan;
					$total = $dt->total;
					$wajib =  $dt->kewajiban;
					
					//$total = (($sks_valid * $dt->jml) - $kewajiban) * $satuan;
								
					$total_pph = $dt->pph;
										
					$jumlah = ($total - $wajib) - $total_pph;
					$total_all = $total_all + $jumlah;					
					
					$total_bersih_sum = $total_all;
					
					/*--- total akhir --*/
					$total_honor = $total_honor + $total;
					$total_honor_pph = $total_honor_pph + $total_pph;
					$total_honor_all = $total_honor_all + $jumlah;
					$total_honor_wajib = $total_honor_wajib + $wajib;
					
					echo "<td>".$z++."</td>";
					echo "<td><b>".$dt->kode_mk. "</b> - ".$dt->nama_mk . " - ". $dt->kelas_id." (".$dt->prodi_id.")";
					if($dt->praktikum==1) echo "<small><em> * </em></small>";
					
					/*echo "<td>".$dt->kelas_id."</td>";
					echo "<td>".$dt->sks."</td>";
					echo "<td>".$dt->jml."</td>";
					echo "<td><b>".$kewajiban."</b><br><small>*) ".$numWeeks." mgg</small></td>";					
					echo "<td align='right'>".number_format($satuan)."</td>";*/
					echo "<td align='right'>".number_format($total)."</td>";
					echo "<td align='right'>".number_format($wajib)."</td>";
					echo "<td align='right'>".number_format($total_pph)."</td>";	
					echo "<td align='right'>".number_format($jumlah)."</td>";
						
					echo "</tr>";
					endif;
				}
				
				$total_honor_all_sum = $total_honor_all_sum + $total_honor_all;
			endif;
			?>
			<tr>
					<th colspan="2">Total</th>
					
					<th><div class="pull-right">Rp. <?php echo number_format($total_honor);?></div></th>	
					<th><div class="pull-right">Rp. <?php echo number_format($total_honor_wajib);?></div></th>					
					<th><div class="pull-right">Rp. <?php echo number_format($total_honor_pph);?></div></th>
					<th><div class="pull-right">Rp. <?php echo number_format($total_honor_all);?></div></th>	
				</tr>
			</table>
			<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
			<div class="page-break" id="footer"></div>
			<?php
			
		}
		
		echo "<h1>Total Rp. ".number_format($total_honor_all_sum)."</h1>";
		
	}


?>


