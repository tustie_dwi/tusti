 
   <div class="block-box">
	<form method="post" action="<?php echo $this->location('module/master/keuangan/rekap') ?>">		
		<div class="control-group">
			<label class="form-label"></label>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<input type="text" name="tmulai" placeholder="Tgl Mulai" class="date form-control" id="input-tgl-mulai" value="<?php echo $mulai; ?>">
					</div>
					<div class="col-md-6">
						<input type="text" name="tselesai" placeholder="Tgl Selesai" class="date form-control" id="input-tgl-selesai" value="<?php echo $selesai; ?>">
					</div>
				</div>
			</div>
		</div>
		
		<button class="btn btn-primary"> <i class='fa fa-search'></i> Cari</button>
	</form>
</div>
