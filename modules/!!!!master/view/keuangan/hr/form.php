 
   <div class="block-box">
	<form method="post" action="<?php echo $this->location('module/master/keuangan') ?>">		
		
		<div class="control-group">
			<label class="control-label">Prodi</label>
			<div class="form-group">
				<select class="form-control" name="cmbprodi">
					<option value="">Semua Prodi</option>
					<?php 
					if($prodi):
						foreach($prodi as $dt):
							?>
							<option value="<?php echo $dt->id ?>" <?php if($prodiid==$dt->id) echo "selected"; ?>><?php echo $dt->value ?></option>
							<?php
						endforeach;
					endif;
					?>
				</select>
			</div>
		</div>
		
		<div class="control-group">
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<input class="form-control date" placeholder="Tgl Mulai" type="text" name="tmulai" id="input-tgl-mulai" value="<?php echo $mulai; ?>">
					</div>
					<div class="col-md-6">
						<input class="form-control date" placeholder="Tgl Selesai" type="text" name="tselesai" id="input-tgl-selesai" value="<?php echo $selesai; ?>">
					</div>
				</div>
			</div>
		</div>
		
		<button class="btn btn-primary"> <i class='fa fa-search'></i> Cari</button>
	</form>
</div>
