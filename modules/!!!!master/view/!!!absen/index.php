<?php $this->head();  $header="Absen Dosen"; ?>

<h2 class="title-page">Absensi <span class="title-tipe">Dosen</span></h2>
<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#"><?php echo $header;?></a></li>
		  <li class="active"><a href="#">Data</a></li>
		</ol>
		
		<div class="breadcrumb-more-action">
		<!-- <a href="<?php echo $this->location('module/akademik/absen/write'); ?>" class="btn pull-right">
		<i class="fa fa-plus"></i> Tambah Absen</a> -->
		</div>
		<div class="row">
			<div class="col-md-5" id="search-box">
				<div class="block-box">
					<i id="loading" class="fa fa-refresh fa-spin" style="position: absolute; right: 20px; display: none"></i>
				 	<form id="form-absen" role="form" method="POST">
						<div class="form-group">
							<input name="tipe" value="dosen" type="hidden">
							<label >Semester</label>
						    <select class="e9 form-control" id="inp_tahun" name="tahun">
						    	<option id="main-tahun" value="0">Pilih Tahun Akademik</option>
						    	<?php
						    		if($tahunAkademik) :
										foreach($tahunAkademik as $key) {
											if($key->is_pendek == '') $is_pendek = '';
											else $is_pendek = " (Pendek)";
											$isi = $key->tahun. ' - ' . ucfirst($key->is_ganjil) . $is_pendek;
											echo "<option value='".$key->tahun_akademik."'>".$isi ."</option>";
										}
									endif;
						    	?>
						    </select>
						</div>
						<div class="form-group">
							<label >Prodi</label>
						    <select class="e9 form-control" id="inp_prodi" name="inp_prodi">
						    	<option value='0'>Pilih Prodi</option>
						    	<?php
						    		if($prodi) :
										foreach($prodi as $key) {
											echo "<option value='".$key->prodi_id."'>".$key->keterangan ."</option>";
										}
									endif;
						    	?>
						    </select>
						</div>
						<div class="form-group">
							<label >Mata Kuliah</label>
						    <select class="e9 form-control" id="inp_mk" name="mk">
						    	<option value='0'>Pilih Matakuliah</option>
						    </select>
						</div>
						<div class="form-group">
							<label >Nama <span class="title-tipe">Dosen</span></label>
						    <select class="e9 form-control" id="inp_dosen" name="dosen">
						    	<option value='0'>Pilih Dosen/Mahasiswa</option>
						    	<?php 
						    	if($dosen) :
									echo "<option value='all'>Semua Dosen</option>";
									foreach($dosen as $key){
										$isi = $key->nama. ' - ' . $key->nik;
										echo "<option value='".$key->karyawan_id."'>".$isi ."</option>";
									}
								else :
									echo "<option value='all'>Dosen tidak ditemukan</option>";
								endif;
						    	?>
						    </select>
						</div>
						<div class="form-group">
							<label >Periode</label>
							<div class="row">
								<div class="col-sm-6">
									<input name="tglMulai" class="form-control" autocomplete="off" type="text" id="dateMulai" data-format="YYYY-MM-DD" placeholder="Tanggal Mulai" required="" name="tanggal_lahir">
								</div>
								<div class="col-sm-6">
									<input name="tglSelesai" class="form-control" autocomplete="off" type="text" id="dateSelesai" data-format="YYYY-MM-DD" placeholder="Tanggal Selesai" required="" name="tanggal_lahir">
						    	</div>
						    </div>
						</div>
						
						<a onclick="get_absen()" class="btn btn-primary"><i class="fa fa-search"></i> Lihat Absensi</a>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->foot(); ?>