jQuery(".tagMhs").tagsManager({
	prefilled: $('.tmpmhs').val(),
	deleteTagsOnBackspace: true,
	preventSubmitOnEnter: true,
	typeahead: true,   
	hiddenTagListName: 'hidMhs'
});
$(function() {
	$('#writeTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});
	$(".tagMhs").autocomplete({ 
		source: base_url + "module/akademik/conf/mhs",
		minLength: 0, 
		select: function(event, ui) { 
			$('.tmpmhs').val(ui.item.id); 
			$('.tagMhs').val(ui.item.value);
		} 
	});
	  $( ".date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	  
});
	
	
