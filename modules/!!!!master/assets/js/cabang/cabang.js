$(document).ready(function() {
	
})

$("#b_cabang").click(function(e){
	var cabang = $('#cabangid').val();
	var keterangan = $('#keterangan').val();
	if(cabang.length > 0 && keterangan.length > 0){
		submit_(e);
	}
	else{
		alert('Lengkapi Data Anda!');
	}
	
});

function submit_(e){
	var postData = new FormData($('#form')[0]);
	$.ajax({
		url : base_url + "module/master/conf/save_cabang",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			// alert(data);
			alert('Data berhasil di simpan!');
			location.reload();
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
			// alert(errorThrown);
		    alert ('Proses Simpan Gagal!');      
		    },
		    cache: false,
			contentType: false,
			processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
	
}
