$(document).ready(function(){
	var temp = '';
	var instansi = '';
	
	set_fakultas();
	get_fakultas();
	get_data_instansi();
});

function tambah(){
	$("#data-wrap").hide();
	$("#add-wrap").show();
}

function batal(){
	$("#data-wrap").show();
	$("#add-wrap").hide();
}

function set_fakultas(){
	for	(index = 0; index < fakultas.length; index++) {
	    temp = fakultas[index];
	    data = temp.split("|");
	    
	   $("#select-fakultas").append('<option value="'+data[0]+'">'+data[1]+'</option>');
	}
}

function get_instansi_by_fakulas(id){
	if(instansi[id]){
		$("#select-instansi").html('<option value="">Pilih Instansi</option>');
		for(var i=0; i<instansi[id].length; i++){
			$("#select-instansi").append('<option value="'+instansi[id][i]["instansi_id"]+'">'+instansi[id][i]["instansi"]+'</option>');
		}
	}
	else{
		$("#select-instansi").html('<option value="">Instansi tidak ditemukan</option>');
	}
}

function get_kegiatan_by_instansi(id){
	var URL = base_url + 'module/master/kerjasama/get_kegiatan_by_instansi';
	$(".loading").show();
	$.ajax({
	    url : URL,
	    type: "POST",
	    dataType : "html",
	    data : $.param({
	    	ins : id
	    }),
	    success:function(msg) 
	    {
	    	$("#select-kegiatan").html(msg);
	    }
    });
    $(".loading").fadeOut();
}

//side js

function get_dokumen_data(id){
	var URL = base_url + 'module/master/kerjasama/get_all_data';
	$.ajax({
	    url : URL,
	    type: "POST",
	    dataType : "html",
	    data : $.param({
	    	kegiatan : id
	    }),
	    success:function(msg) 
	    {
	    	$("#all-data-wrap").html(msg);
	    }
    });
}


function get_fakultas(){
	$("#fakultas-wrap").html("");
	for	(index = 0; index < fakultas.length; index++) {
	    temp = fakultas[index];
	    data = temp.split("|");
	    
	    $("#fakultas-wrap").append('<a dataid="'+data[0]+'" onclick="get_kegiatan(\''+data[0]+'\')" href="#" class="list-group-item">'+data[1]+'</a>');
	}
}

function hover(id){
	$("#fakultas-wrap a").attr("class","list-group-item");
	$("#fakultas-wrap a[dataid='"+id+"']").attr("class","list-group-item active");
}

function get_detail(id){
	hover(id);
	var URL = base_url + 'module/master/kerjasama/get_all_kegiatan';
	$.ajax({
	    url : URL,
	    type: "POST",
	    dataType : "html",
	    data : $.param({
	    	instansi : id
	    }),
	    success:function(msg) 
	    {
	    	get_dokumen_data(id+"|ins");
	    	$("#data-wrap").html(msg);
	    }
    });
}

function get_kegiatan(id){
	hover(id);
	$("#fakultas-wrap").html('<a onclick="get_fakultas()" href="#" class="list-group-item"><i class="fa fa-arrow-circle-o-up"></i> Daftar Fakultas ...</a>');
	if(instansi[id]){
		for(var i=0; i<instansi[id].length; i++){
			$("#fakultas-wrap").append('<a dataid="'+instansi[id][i]["instansi_id"]+'" onclick="get_detail(\''+instansi[id][i]["instansi_id"]+'\')" href="#" class="list-group-item">'+instansi[id][i]["instansi"]+'</a>');
		}
	}
	else{
		console.log("empty");
	}
}

function get_data_instansi(){
	var URL = base_url + 'module/master/kerjasama/get_fakultas_instansi';
	$.ajax({
	    url : URL,
	    type: "POST",
	    dataType : "json",
	    async: false,
	    success:function(msg) 
	    {
	    	instansi = msg;
	    },
	    cache: false,
	    contentType: false,
	    processData: false
    });
    
}

