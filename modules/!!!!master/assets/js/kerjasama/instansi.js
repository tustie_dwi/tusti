function tambah(){
	$(".null-option").attr("selected","");
	$("[name='id'], [name='nama_instansi'], [name='contact_person'], [name='telp']").val('');
	$("[name='alamat'], [name='keterangan']").html('');
	
	$("#form-manage").show();
	$("#form-data").hide();
}

function batal(){
	$("#form-manage").hide();
	$("#form-data").show();
}

$(".btn-edit-instansi").click(function(){
	tambah();
	$("[name='id']").val($(this).data("id"));
	$("[name='kategori_id']").find("[value='"+$(this).data("kategori_id")+"']").attr("selected","");
	$("[name='fakultas_id']").find("[value='"+$(this).data("fakultas_id")+"']").attr("selected","");
	$("[name='nama_instansi']").val($(this).data("nama_instansi"));
	$("[name='contact_person']").val($(this).data("contact_person"));
	$("[name='telp']").val($(this).data("telp"));
	$("[name='alamat']").html($(this).data("alamat"));
	$("[name='keterangan']").html($(this).data("keterangan"));
});

$(document).on("change","[name='fakultas'], [name='kategori']", function(){
	document.getElementById("form-instansi").submit();
})
