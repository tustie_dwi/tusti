function edit_kategori(keterangan, id){
	$("[name='id_kategori']").val(id);
	$("[name='keterangan']").val(keterangan);
	
	$(".panel-heading").html('<i class="fa fa-pencil"></i> Ubah Kategori');
}

function batal_kategori(){
	$("[name='id_kategori']").val("");
	$("[name='keterangan']").val("");
	$(".panel-heading").html('<i class="fa fa-plus"></i> Tambah Kategori');
}
