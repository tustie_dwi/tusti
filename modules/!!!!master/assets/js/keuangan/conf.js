$(document).on("click",".btn-edit-post", function(){
	$("[name=conf_id]").val($(this).data("id"));
	$("[value="+$(this).data("golongan")+"]").attr('selected','');
	$("[value="+$(this).data("jenjang_pendidikan")+"]").attr('selected','');
	$("[name=tarif]").val($(this).data("tarif"));
	$("[name=potongan]").val($(this).data("potongan"));
	
	if($(this).data('is_pns') == 1) $('[name="is_pns"]').attr("checked",'');
	else $('[name="is_pns"]').removeAttr("checked");
})

function batal(){
	$("[name=conf_id]").val('');
	$("[value]").removeAttr('selected');
	$("[name=tarif]").val('');
	$("[name=potongan]").val('');
	
	$('[name="is_pns"]').removeAttr("checked");
}
