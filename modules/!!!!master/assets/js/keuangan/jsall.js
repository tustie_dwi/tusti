$(function() {
	$("#checkAll").click(function () {
		 $('input:checkbox').not(this).prop('checked', this.checked);
	});
	
	 $(".cmb-rekap").change(function(){
		
		var kode = $(document.getElementById("cmbrekap")).val();	
		//alert(kode);
		$.ajax({
			type:"POST",
			dataType:"html",
			url : base_url + 'module/master/report/view_hr',
			data : $.param({				
				kode:kode	
			}),
			success : function(msg) {
				$("#content-print").html(msg);
			}		
		});
			
	 });
	
	$(".btn-cetak-hr-bank-selisih").click(function(){
		
		var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/keuangan/rekap',
				data : $.param({
					tmulai : mulai,
					tselesai: selesai,
					cmbprodi: prodi,
					kode:kode,
					cetak	: cetak			
				}),
				success : function(msg) {
					if(kode){
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/11/'+custom,"_blank");		
					}else{
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/11/'+custom,"_blank");
					}					
					
				}
			});
			
	 });
	  
	$(".btn-cetak-hr-bank").click(function(){
		
		var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/keuangan/rekap',
				data : $.param({
					tmulai : mulai,
					tselesai: selesai,
					cmbprodi: prodi,
					kode:kode,
					cetak	: cetak			
				}),
				success : function(msg) {
					if(kode){
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/10/'+custom,"_blank");		
					}else{
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/10/'+custom,"_blank");
					}					
					
				}
			});
			
	  });
	  
	
	 $(".btn-cetak-hr-excel").click(function(){
		
		var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/keuangan/rekap',
				data : $.param({
					tmulai : mulai,
					tselesai: selesai,
					cmbprodi: prodi,
					kode:kode,
					cetak	: cetak			
				}),
				success : function(msg) {
					if(kode){
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/4/'+custom,"_blank");		
					}else{
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/4/'+custom,"_blank");
					}					
					
				}
			});
			
	  });
	  
	  $(".btn-cetak-hr-titip").click(function(){
	  	var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/keuangan/rekap',
			data : $.param({
				tmulai : mulai,
				tselesai: selesai,
				cmbprodi: prodi,
				kode:kode,
				cetak	: cetak			
			}),
			success : function(msg) {
				if(kode){
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/6/'+custom,"_blank");		
				}else{
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/6/'+custom,"_blank");
				}					
				
			}
		});
	  });
	  
	  $(".btn-cetak-selisih-spj").click(function(){
	  	var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/keuangan/rekap',
			data : $.param({
				tmulai : mulai,
				tselesai: selesai,
				cmbprodi: prodi,
				kode:kode,
				cetak	: cetak			
			}),
			success : function(msg) {
				if(kode){
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/7/'+custom,"_blank");		
				}else{
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/7/'+custom,"_blank");
				}					
				
			}
		});
	  });
	  
	   $(".btn-cetak-selisih-hr").click(function(){
	  	var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/keuangan/rekap',
			data : $.param({
				tmulai : mulai,
				tselesai: selesai,
				cmbprodi: prodi,
				kode:kode,
				cetak	: cetak			
			}),
			success : function(msg) {
				if(kode){
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/8/'+custom,"_blank");		
				}else{
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/8/'+custom,"_blank");
				}					
				
			}
		});
	  });
	  
	  $(".btn-cetak-selisih-mku").click(function(){
	  	var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/keuangan/rekap',
			data : $.param({
				tmulai : mulai,
				tselesai: selesai,
				cmbprodi: prodi,
				kode:kode,
				cetak	: cetak			
			}),
			success : function(msg) {
				if(kode){
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/9/'+custom,"_blank");		
				}else{
					window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/9/'+custom,"_blank");
				}					
				
			}
		});
	  });
	  
	   $(".btn-cetak-hr-mku").click(function(){
		
		var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		// var custom = '';
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/keuangan/rekap',
				data : $.param({
					tmulai : mulai,
					tselesai: selesai,
					cmbprodi: prodi,
					kode:kode,
					cetak	: cetak			
				}),
				success : function(msg) {
					if(kode){
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/5/'+custom,"_blank");		
					}else{
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/5/'+custom,"_blank");
					}					
					
				}
			});
			
	  });
	
	 $(".btn-cetak-hr").click(function(){
		
		var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		// var custom = '';
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/keuangan/rekap',
				data : $.param({
					tmulai : mulai,
					tselesai: selesai,
					cmbprodi: prodi,
					kode:kode,
					cetak	: cetak			
				}),
				success : function(msg) {
					if(kode){
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/1/'+custom,"_blank");		
					}else{
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/1/'+custom,"_blank");
					}					
					
				}
			});
			
	  });
	  
	  $(".btn-cetak-spj").click(function(){
		var karyawan_ = $("#cmbmulti").select2("val");
		var judul_ = $("[name='custom_judul']").val();
		var jabatan_ = $("[name='custom_jabatan']").val();
		var custom = judul_ + "|" + jabatan_ + "|" + karyawan_;
		// var custom = '';
		
		var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
		
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/keuangan/rekap',
				data : $.param({
					tmulai : mulai,
					tselesai: selesai,
					cmbprodi: prodi,
					kode:kode	
				}),
				success : function(msg) {
					
						//$("#content-print").html(msg);
						//window.print();	
					if(kode){
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/3/'+custom,"_blank");		
					}else{
						window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/3/'+custom,"_blank");
					}					
					
				}
			});
			
	  });
	  
	  $(".btn-cetak-amplop-hr").click(function(){
		
		var mulai = $(document.getElementById("mulai")).val();
		var selesai = $(document.getElementById("selesai")).val();
		var prodi = $(document.getElementById("prodi")).val();
		var kode = $(document.getElementById("tmp")).val();
		var cetak = 'ok';
	
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/keuangan/rekap',
				data : $.param({
					tmulai : mulai,
					tselesai: selesai,
					cmbprodi: prodi,
					cetak	: cetak			
				}),
				success : function(msg) {
					
						
						if(kode){
							window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/'+kode+'/2',"_blank");		
						}else{
							window.open(base_url + 'module/master/keuangan/rekap/1/'+mulai+'/'+selesai+'/1/2',"_blank");
						}
					
				}
			});
			
	  });
	
	$('#writeTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
  });
  
  $('#jadwalform').hide();
  $("#mainform #form-report-koordinator").hide();
  $("#form-report-view").hide();
  
   $('#myModal').modal('hide', function(){
	//var hari = $('.hari').attr('value');
   });
   
   
   $('#registerModal').modal({
		show: true,
		remote: base_url + "/registrasi/msg"
	});
   
   
   $("[data-toggle=tooltip]").tooltip();
  
	$(".pop").each(function() {
		var $pElem= $(this);
		$pElem.popover(
			{
			  title: getPopTitle($pElem.attr("id")),
			  content: getPopContent($pElem.attr("id")),
			  trigger:'hover'
			}
		);
	});
					
	function getPopTitle(target) {
		return $("#" + target + "_content > div.popTitle").html();
	};
			
	function getPopContent(target) {
		return $("#" + target + "_content > div.popContent").html();
	};

   
   $(".btn-delete-post").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/deletemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	
	$(".btn-delete-post-kegiatan").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
		
	$( "#date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$( ".date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	
	$(".dosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
		} 
	});  

	$(".ndosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ndosenid').val(ui.item.id); 
			$('#ndosen').val(ui.item.value);
		} 
	});  		

	$(".pengampu").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#pengampuid').val(ui.item.id); 
			$('#pengampu').val(ui.item.value);
		} 
	});  			
	
	$("#dosen").autocomplete({ 
		source: base_url + "/module/akademik/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
			$('#frmDosen').submit(); 
		} 
	});  			
	
		
	$(".hari").autocomplete({ 
		source: base_url + "/module/masterdata/conf/hari",
		minLength: 0, 
		select: function(event, ui) { 
			$('#hariid').val(ui.item.id); 
			$('#hari').val(ui.item.value);
		} 
	});  
		
	
	
	$("#ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".blokwaktu").autocomplete({ 
		source: base_url + "/module/penjadwalan/conf/blokwaktu",
		minLength: 0, 
		select: function(event, ui) { 
			$('#blokid').val(ui.item.id); 
			$('#blok').val(ui.item.value);
		} 
	});  
	
	$(".jammulai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jammulai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jammulaiid').val(ui.item.id); 
			$('#jammulai').val(ui.item.value);
		} 
	});  
	
	$(".jamselesai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jamselesai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jamselesaiid').val(ui.item.id); 
			$('#jamselesai').val(ui.item.value);
		} 
	});  
	
	$(".prodi").autocomplete({ 
		source: base_url + "/module/masterdata/conf/prodi",
		minLength: 0, 
		select: function(event, ui) { 
			$('#prodiid').val(ui.item.id); 
			$('#prodi').val(ui.item.value);
		} 
	});  
	
	$("#mkditawarkan").autocomplete({ 
		source: base_url + "/module/akademik/conf/mkditawarkan/1",
		minLength: 0, 
		select: function(event, ui) { 
			$('#mkditawarkanid').val(ui.item.id); 
			$('#mkditawarkan').val(ui.item.value);
		} 
	});  
	
	
	$(".cmbmulti").select2();	
	$("#cmbmulti").select2();
	$("#cmbunit").select2();
	$("#cmbruang").select2();
	
			
	// $(".e7").select2({		
		// placeholder: "Search for mahasiswa",
		// minimumInputLength: 1,
		// multiple:true,
		// ajax: {
			// //url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
			// url:base_url + "/module/masterdata/conf/mahasiswa",
			// dataType: 'json',
			// type: "GET",
			// data: function (term) { // page is the one-based page number tracked by Select2
				// return {
					// q: term, //search term
					// page: 100
				// };
			// },
			// results: function (data, page) {
// 				
				// return {results: data.results, more: false};
			// }
		// },
		// formatResult: optionFormatResult, // omitted for brevity, see the source of this page
		// formatSelection: optionFormatSelection,
// 		
	// });
	
	$("#approve").click(function(){
	  if ($('.pic').attr('disabled') == "disabled" ) {
		$('.pic').removeAttr('disabled'); 
      }else {
		$('.pic').removeAttr('disabled'); 
      }
	});
	
	$(".reject").click(function(){
	  if ($('.pic').attr('disabled') == "disabled" ) {
		$(".pic").attr('disabled', 'disabled');    
      }else {
		$(".pic").attr('disabled', 'disabled');      
      }
	});
	
	$(".tagObjek").tagsManager({		
		deleteTagsOnBackspace: true,
		prefilled: $('.tmpobjek').val(),
        preventSubmitOnEnter: true,
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidObjek'
      });  
	  
	  $(".tagMhsKKNP").tagsManager({		
		prefilled: $('.tmpmhskknp').val(),
		deleteTagsOnBackspace: true,
        preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/masterdata/conf/pesertamhs",
		AjaxPush: base_url + "/module/masterdata/conf/pesertamhs/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidMhsKKNP'
      });
	  
	 
		
});

function optionFormatResult(mhs) {	
        var markup = "<table class='movie-result'><tr>";       
        markup += "<td>"+mhs.text+"";         
        markup += "</td></tr></table>"

        return markup;
    }

function optionFormatSelection(mhs) {	
	return mhs.text;
}

$(".e9").select2();

	
