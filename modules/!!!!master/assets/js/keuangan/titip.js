$(document).ready(function(){
	$("#dosen_sk, #dosen_titip, #tahun").select2({
         matcher: function(term, text) { return text.toUpperCase().indexOf(term.toUpperCase())==0; }
    });
    
    $("#tahun").change(function(){
    	var tahun = $("#tahun").select2("val");
		
    	$("#tahun").select2("val", tahun);
		
    	window.location.href = base_url + 'module/master/keuangan/titip/' + tahun;
    });
    
    $(".btn-edit-titip").click(function(){
    	var dosen_sk = $(this).data("dosen_sk");
    	var dosen_titip = $(this).data("dosen_titip");
    	var id = $(this).data("id");
		var prodi = $(this).data("prodi");
		var semester = $(this).data("semester");
		
		//alert(semester);
    	
    	$("#dosen_sk").select2("val", dosen_sk);
    	$("#dosen_titip").select2("val", dosen_titip);
		$("#cmbprodi").val(prodi);
		$("#cmbtahun").val(semester);
		$("#id").val(id);    	
    	//console.log(dosen_sk + ' ' + dosen_titip);
    });
});

function batal(){
	$("#dosen_sk").select2("val", "-");
    $("#dosen_titip").select2("val", "-");
    $("#id").val("");    
}
