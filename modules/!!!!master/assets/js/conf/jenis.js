$(document).ready(function(){
	
})

$(document).on('click', 'input[name="b_save"]', function() {
  $('#form').submit(function (e) {
  	var ket = $('input[name="keterangan"]').val();
  	if(ket.length!=0){
  		var formData = new FormData($(this)[0]);
		var URL = base_url + 'module/master/jenis/saveToDB';
	      $.ajax({
	        url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data) 
	        {
	        	alert(data);
	        	location.href=base_url + 'module/master/jenis';
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
  	}else{
  		alert("Lengkapi data anda");
  	}
  });
});