$(document).ready(function(){
	//=======================tab selected=====================//
	$('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
    	
        var id = $(e.target).attr("href").substr(1);
        // alert(id);
        window.location.hash = id;
    });

    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    
    $('#myTab a[href="' + hash + '"]').tab('show');
	//=======================tab selected=====================//
	
	$(".close_btn").click(function(){
		$(".library_attach").hide();
		$("#parent").val("0");
		$("#par").val("0");
		$("#id").val("");
		$("#folder_name").val("");
		//location.href = base_url + 'module/filemanager/folder';
	});
	
	$(".close_btn_file_upload").click(function(){
		var folder 	= document.getElementById("folder");
		var folderid	= $(folder).val();
		var kar_id = $("#kar_id").val();
		$("#folder").val("0");
		$("#folder_loc").val("");
		
		view_content(folderid,kar_id);
		//location.href = base_url + 'module/filemanager/folder';
	});
	
	$("#newfolder_form").submit(function(e){
		var name = $("#folder_name").val();
		if(name.trim().length > 0){
			var formData = new FormData($(this)[0]);
			var URL = base_url + 'module/master/staff/save_folder';
			$.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.reload();
		        },
		        error: function(msg) 
		        {
		            alert ("ERROR!");
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});
			e.preventDefault(); //STOP default action
			return false;
		}
		else{
			alert("Gagal membuat folder baru.");
		}
	});
	
	
	
	//file
	var countfile = 1;
	$("#add_file_form").click(function(){
		countfile += 1;
		var newfile = "<br><div class='row'><div class='col-sm-12'><div class='form-group'>";
			newfile += "<input type='text' required='required' name='title[]' autocomplete='off' class='form-control' id='title' value='-' />";
			newfile += "</div>";
			newfile += "<div class='form-group'>";
		    newfile += "<textarea class='form-control' name='keterangan[]' id='keterangan' placeholder='Keterangan'></textarea>";
		    newfile += "</div>";
			newfile += "<div class='form-group'>";
		    newfile += "<input type='file' required='required' name='uploads[]' multiple='multiple' autocomplete='off' id='uploads'/>";
		    newfile += "</div>";
		    newfile += "</div>";
		$("#newform").append(newfile);
		$('#count_new_file').val(countfile);
	});
	
	$("#newfile_form").submit(function(e){
		var folder 	= document.getElementById("folder");
		var folderid	= $(folder).val();
				
		var formData = new FormData($(this)[0]);
		var URL = base_url + 'module/master/staff/save_file';
		$.ajax({
            url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert(msg);
	            location.reload();
				//view_content(folderid);
	           // location.href = base_url + 'module/filemanager/folder';
	        },
	        error: function(msg) 
	        {
	            alert ("ERROR!");     
				//view_content(folderid);
	            //location.href = base_url + 'module/filemanager/folder';
	        },
	        cache: false,
	        contentType: false,
	        processData: false
		});
		e.preventDefault(); //STOP default action
		return false;
	});
	
	
	$(".view_content").on("click", function (e) {
		e.preventDefault();
		setTimeout(function(){
			var type = $('#myTab .active a').attr('href').substr(1);
			window.location.hash = type;
			var hash = window.location.hash;
			$('#myTab a[href="' + hash + '"]').tab('show');
		})
    });
});

function newfolder_parent(id){
	$("#parent").val(id);
};

function remove_selected(){
	$('.document-place').empty();
}

function newfile_parent(id, loc){
	//alert(loc);
	$("#folder").val(id);
	$("#folder_loc").val(loc);
};

function rename(id,name,aktif){
	// alert(id+"\n"+name);
	$("#id").val(id);
	// alert($("#id").val());
	$("#folder_name").val(name);
	if(aktif == '1'){
		$("#aktif").prop('checked', true);
	}
};

function view_content(id,karyawan){
	// alert(id+"\n"+karyawan);
	//$("#par").val(parent);
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/master/staff/view_content',
		data : $.param({
			id : id,
			karyawan : karyawan
		}),
		success : function(msg) {
			// alert(msg);
			$(".content-table").show();
			$(".content-file").hide();
			$(".view_content").html(msg);
			var type = $('#myTab .active a').attr('href').substr(1);
			//alert(type);
			if(type!="dokumen"){
				$(".library_attach").show();
			}
			$('.example').dataTable({
			  	"bDestroy": true,
				"fnInitComplete": function(oSettings, json) {
				  //alert( 'DataTables has finished its initialisation.' );
				}
			});
		}
	});
};

function doView(id, $folder){	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/staff/files/'+id,
			success : function(msg) {	
				$(".content-table").hide();
				$(".content-file").show();
				$(".content-file").html(msg);
				//location.reload();				
			}
		});
};

function doDelete(i,jenis_del) {
	var del_id = i;
	var jenis_del = jenis_del;
	var x = confirm("Anda yakin menonaktifkan folder ini?");
 	if (x){
 		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/staff/delete',
			data : $.param({
				delete_id : del_id,
				jenis_del : jenis_del
			}),
			success : function(msg) {
				//alert(msg);
				if (msg) {
					alert("Data Berhasil Terhapus!");
					if(jenis_del == 'file'){
						$(".file"+del_id).fadeOut();
					}
					else if(jenis_del == 'folder'){
						location.reload();
					}
				}else {
					alert('gagal');
				}
			}
		});
	}
	else {
	   location.reload();
	}
};

$(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
        	$(this).find("i").removeClass("fa-folder-open-o");
        	$(this).find("i").addClass("fa-folder-o");
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
        	$(this).find("i").removeClass("fa-folder-o");
        	$(this).find("i").addClass("fa-folder-open-o");
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
	
	
   var treeHeight=$(window).height()-$('header').height()-200;
   $('.tree > ul > li.parent_li').css('max-height',treeHeight);
});


