$(document).ready(function() {
	$(".e9").select2();
	
	if( typeof $('#edit-cek').val() !== 'undefined' ){
		jQuery("#dosenpengampu").tagsManager();
	}
	
	//-----------------------------------------------------------------
	var fakultas_id_ = $('#select_fakultas').val();
	$.ajax({
		type : "POST",
		dataType: "json",
		url: base_url + "module/master/conf/namamk_from_matakuliah",
		data : $.param({
			fakultasid : fakultas_id_
		}),
		success: function(data){
			$('#namamk').prop('disabled', false);
			$('#namamk').autocomplete(
			{
				source: data,
				minLength: 0,
				select: function(event, ui) { 
				$('#namamk').val(ui.item.value);
				}     
			});
		},
		error: function(){
			$('#namamk').prop('disabled', true);
		}
	});
	
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/master/akademik/blok_mk',
		data : $.param({
			fakultas_id : fakultas_id_
		}),
		success : function(msg) {
			if (msg == '') {
				$("#select_mk").attr("disabled", true);
				$("#form").fadeOut();
				$("#isblok").attr("checked", false);
				
			} else {
				$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
				$("#select_mk").removeAttr("disabled");
				$("#select_mk").html(msg);
				$("#select_mk").select2('enable', true);
			}
		}
	});
	
	//-----------------------------------------------------------------
});

$("#select_fakultas").change(function() {
	var fakultas_id_ = $(this).val();
	$.ajax({
		type : "POST",
		dataType: "json",
		url: base_url + "/module/master/conf/namamk_from_matakuliah",
		data : $.param({
			fakultasid : fakultas_id_
		}),
		success: function(data){
			$('#namamk').prop('disabled', false);
			$('#namamk').autocomplete(
			{
				source: data,
				minLength: 0,
				select: function(event, ui) { 
				$('#namamk').val(ui.item.value);
				}    
			});
		},
		error: function(){
			$('#namamk').prop('disabled', true);
		}
	});
	
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/master/akademik/blok_mk',
		data : $.param({
			fakultas_id : fakultas_id_
		}),
		success : function(msg) {
			// alert(msg);
			if (msg == '') {
				$("#select_mk").attr("disabled", true);
				$("#form").fadeOut();
				$("#isblok").attr("checked", false);
				
			} else {
				$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
				$("#select_mk").removeAttr("disabled");
				$("#select_mk").html(msg);
				$("#select_mk").select2('enable', true);
			}
		}
	});
});

if( typeof $('#edit-cek').val() !== 'undefined' ){
	$("#dosenkoor").autocomplete({ 
		source: base_url + "/module/master/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenkoor').val(ui.item.value);
		} 
	});
	
	$("#dosenpengampu").autocomplete({ 
		source: base_url + "/module/master/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenpengampu').val(ui.item.value);
			$('#dosenpengampuid').val(ui.item.id);
		} 
	});		
	
	$("#namedosenpengampu").autocomplete({ 
		source: base_url + "/module/master/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenpengampu').val(ui.item.value);
			$('#dosenpengampuid').val(ui.item.id);
		} 
	});
}

function koor_pengampu(pengampu_id, koor){
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + "module/master/akademik/koor_pengampu",
		data : $.param({
			pengampuid : pengampu_id,
			iskoor	   : koor
		}),
		success : function(msg) {
			//location.reload();
		}
	});
}

function deletePengampu(i){
	var x = confirm("Are you sure you want to delete?");
 	if (x){
 		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + "module/master/akademik/delete_pengampu",
			data : $.param({
				delete_id : i
			}),
			success : function(msg) {
				alert(msg);
				location.reload();
			}
		});
 	}
}

function postpengampu(e){
	if (e.keyCode == 13) {
        var karyawan_id = $('#dosenpengampuid').val();
		var mk_id = $('input:hidden[name="hidId"]').val();
		addPengampu(karyawan_id, mk_id)
    }
}

function iskoor(id){
	var radio_length = $('input:radio[name="koor"]').length;
	for(i=1;i<=radio_length;i++){
		isrem(i);
	}
	var output='';
	output += "<i id='rem"+id+"'><small><span class='label label-danger'>Koordinator</span></small></i>";
    $("#benar"+id).html(output);
    $("#isbenarcheck"+id).val('1');
}

function isrem(id){
	$("#rem"+id).remove();
	$("#isbenarcheck"+id).val('');
}

function addPengampu(karyawan_id, mk_id){
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + "module/master/akademik/add_pengampu",
		data : $.param({
			karyawan_id : karyawan_id,
			mk_id		: mk_id
		}),
		success : function(msg) {
			location.reload();
		}
	});
}

$("#select_thnakademik").change(function() {
	$("#param").submit(); //SUBMIT FORM
});

$(document).on("keypress", 'form', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        return false;
    }
});

function validate() {
	if (document.getElementById('isblok').checked) {
    	$("#form").fadeIn();
    } 
    else {
       $("#form").fadeOut();
    }
}

function delete_mk_(e, mkid){
	var parent = $(e).parents('.tr-'+mkid);
	// parent.remove();
	var x = confirm("Anda yakin menghapus data ini?");
 	if (x){
 		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + "module/master/akademik/delete_mkditawarkan",
			data : $.param({
				mkid : mkid
			}),
			success : function(msg) {
				if(msg=='sukses'){
					alert('Data berhasil dihapus');
					parent.remove();
				}
			}
		});
 	}
}

$("#submit").click(function(e){			
	var fakultas = $('#select_fakultas').val();
	var select_thn = $('#select_thn').val();
	var namamk = $('#namamk').val().length;
	var kuota = $('#kuota').val().length;
	
	if(fakultas!=0 && select_thn!=0 && namamk > 0 && kuota > 0){
		var edit = $('#edit-cek').val();
		if(edit!='1'){
			var uri = base_url + "module/master/akademik/save_mkditawarkan_ToDB";
		}
		else{
			var uri = base_url + "module/master/akademik/save_editmkditawarkan_ToDB";
		}
 		
		//------SET KOORDINATOR---------------------
		var radio_length = $('input:radio[name="koor"]').length;
		for(i=1;i<=radio_length;i++){
			if($('#koor'+i+':checked').val())var pengampu_id = $('#pengampuid'+i).val();
			if($('#pengampukoor'+i).val()=='1')var last_koor_pengampu_id = $('#pengampuid'+i).val();
		}
		koor_pengampu(last_koor_pengampu_id, '0');
		koor_pengampu(pengampu_id, '1');
		
		//------END SET KOORDINATOR---------------------
		var postData = new FormData($('#form-mkditawarkan')[0]);
		$.ajax({
			url : uri,
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
			    // alert ('Proses Simpan Berhasil!');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Proses Simpan Gagal!');      
			    },
			    cache: false,
				contentType: false,
				processData: false
		  });
		e.preventDefault(); //STOP default action
		return false;
	}
	else{
		alert("Lengkapi Data Anda");
	}
});