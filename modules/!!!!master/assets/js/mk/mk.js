$(document).ready(function(){
	$(".e9").select2();
	
	var fakultas_id_ = $('#select_fakultas').val();
	if(fakultas_id_=='0'){
		$("#namamk").attr("disabled", true);
	}else{
		$("#namamk").removeAttr("disabled");
		datalist_edit(fakultas_id_);
	} 
})

$("#pilih_fakultas").change(function() {
	$("#param").submit(); //SUBMIT FORM
});

var select = document.getElementById("strata");
var c = $("input[name='strata_edit']").val();
for(var i = 0;i < select.options.length;i++){
    if(select.options[i].value == c ){
       select.options[i].selected = true;
        $("#strata").select2("val", c);
    }
}

$("#select_fakultas").change(function() {
	var fakultas_id_ = $(this).val();
	
	if(fakultas_id_=='0'){
		$("#namamk").attr("disabled", true);
	}else{
		$("#namamk").removeAttr("disabled");
		datalist_edit(fakultas_id_);
	} 
});

function datalist_edit(fakultas_id_){
	$.ajax({
	  type : "POST",
	  dataType: "json",
      url: base_url + "/module/master/conf/namamk",
      data : $.param({
		fakultasid : fakultas_id_
	}),
       success: function(data){
	       if(data!='failed'){
		       $('#namamk').autocomplete(
		       	{
		       	source: data,
		       	minLength: 0,
		       	select: function(event, ui) { 
				$('#namamk').val(ui.item.value);
				}    
		       	});
	       }else{
	       	 $('#namamk').autocomplete(
		       	{
		       	source: data,
		       	minLength: 0,
		       	select: function(event, ui) { 
				$('#namamk').val(ui.item.value);
				}    
		       	});
	       }
       }
    });
}

$("#submitmk").click(function(e){
	var namamk = $('#namamk').val().length;
	var namamkeng = $('#namamkeng').val().length;
	var kodemk = $('#kodemk').val().length;
	var kurikulum = $('#kurikulum').val().length;
	var sks = $('#sks').val().length;

	if(namamk > 0 && namamkeng > 0 && kodemk > 0 && kurikulum > 0 && sks > 0){
	  var postData = new FormData($('#form')[0]);
		$.ajax({
			url : base_url + "module/master/akademik/saveToDBmk",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Proses Simpan Gagal!');      
			    },
			    cache: false,
				contentType: false,
				processData: false
		  });
		e.preventDefault(); //STOP default action
		return false;
	}
	else{
		alert("Lengkapi Data Anda");
	}
});
