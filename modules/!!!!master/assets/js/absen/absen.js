$(document).ready(function() {
	$('.e9').select2();

	$("select[name='jadwal-mk']").change(function(){
		var jadwal = $(this).val();
		var start = $('option:selected', this).attr('start');
		var finish = $('option:selected', this).attr('finish');
		var kelas = $('option:selected', this).attr('kelas');
		$("input[name='jam_mulai']").val(start);
		$("input[name='jam_selesai']").val(finish);
		$("input[name='kelas']").val(kelas);
	});
	
	$("#form-absen-dosen").submit(function(e){
		var postData = new FormData($(this)[0]);
		$.ajax({
			url : base_url + "module/master/report/save_absen",
			type: "POST",
			data : postData,
			async: false,
			success:function(msg) 
			{
				alert(msg);
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Proses Gagal!');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault();
		return false;
	});
	
	$("#form-absen-asisten").submit(function(e){
		var postData = new FormData($(this)[0]);
		$.ajax({
			url : base_url + "module/master/report/save_absen_asisten",
			type: "POST",
			data : postData,
			async: false,
			success:function(msg) 
			{
				alert(msg);
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Proses Gagal!');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault();
		return false;
	});

	var edit = $("input[name='hidId']").length;
	var date_index = $("#dateMulai").length;
	if(edit || date_index){
		$('#dateMulai, #dateSelesai, .date').datetimepicker({
		  pickTime : false,
		  format : 'YYYY-MM-DD'
		});
		
		$('.time').datetimepicker({
		  pickTime : true,
		  pickDate : false,
		  format : 'HH:mm:s'
		});
	};
	
	$(".close, .close_btn").click(function(){
		// $("select[name='jadwal-mk']").select2("val","0");
		$("input[name='tgl_pertemuan']").val("");
		$("input[name='jam_mulai']").val("");
		$("input[name='jam_selesai']").val("");
		$("input[name='materi']").val("");
		// $("input[name='total']").val("");
		// $("select[name='sesi']").select2("val","0");
		$("input[name='hidId_absen']").val("");
		$("input[name='hidId_absendosen']").val("");
		$("input[name='dosen']").val("");
		$("input[name='hidId_asistenabsen']").val("");
		$("input[name='asisten']").val("");
	});
	
	if($("#content_print").length){
		$("button.btn_cetak, .action_btn, .tambah_absen").hide();
		 
		var printContents = document.getElementById("cetak").innerHTML;
	    var originalContents = document.body.innerHTML;
	
	    document.body.innerHTML = printContents;
	    window.print();
	    document.body.innerHTML = originalContents;
	}
});
$(document).on('change', '#inp_tahun', function(){
	var thn_id = $(this).val();
	var tipe = $("[name=tipe]").val();
	if(tipe=="mahasiswa"){
		var id = $("[name=mahasiswaid]").val();
	}else var id = $("[name=dosenid]").val();
	
	// ajax_dosen(thn_id, tipe, '');
	
	$.ajax({
        url : base_url + 'module/master/report/get_mk',
        type: "POST",
        dataType : "HTML",
        data : $.param({
        	tahunId : thn_id, 
        	tipe : tipe,
        	id : id
        }),
        success:function(msg) 
        {
        	// alert(msg);
            $("#inp_mk").html(msg);
            filter_prodi();
            
            // $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	// $("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
	
});

$(document).on( 'change', '#inp_mk', function(){
	var id = $(this).val();
	var tipe_ = $("[name=tipe]").val();
	ajax_dosen('', tipe_, id);
});

function ajax_dosen(thn_id, tipe, mkid){
	$.ajax({
        url : base_url + 'module/master/report/get_dosen',
        type: "POST",
        dataType : "HTML",
        data : $.param({
        	tahunId : thn_id, 
        	tipe : tipe,
        	periodeId : mkid
        }),
        success:function(msg) 
        {
        	// alert(msg);
           $("#inp_data").html(msg);
            // filter_prodi();
            // $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	// $("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
}

function filter_prodi(){
	var prodi = $("#inp_prodi").val();
	
	$.each($("#inp_mk").find("[data-prodi]"), function(){
		if($(this).text().indexOf("Pilih Matakuliah") < 0 && prodi !== '0'){
			if($(this).data("prodi") == prodi) {
				$(this).show();//removeAttr("disabled");
			}
			else $(this).hide();//attr("disabled","disabled");
		}
		else{
			$(this).show();//removeAttr("disabled");
		}
	});
}

function get_absen(){
	var thnid = $("#inp_tahun").val();
	var prodiid = $("#inp_prodi").val();
	var mk = $("#inp_mk").val().split("|")[3];
	var kelas = $("#inp_mk").val().split("|")[1];
	var data = $("#inp_data").val();
	var type = $("input[name='tipe']").val();
	
	//alert(type);
	var datemulai = $("#dateMulai").val();
	var dateselesai = $("#dateSelesai").val();
	
	if(datemulai.length!=0 && dateselesai.length!=0){	
		var form = document.createElement("form");
		var input = document.createElement("input");
		var input2 = document.createElement("input");
		var input3 = document.createElement("input");
		var input4 = document.createElement("input");
		var input5 = document.createElement("input");
		var input6 = document.createElement("input");
		var input7 = document.createElement("input");
		var input8 = document.createElement("input");
		
		form.action = base_url + 'module/master/report/detail';
		form.method = "post"
		
		input.name = "thnid";
		input.value = thnid;
		form.appendChild(input);
		
		input2.name = "prodi";
		input2.value = prodiid;
		form.appendChild(input2);
		
		input3.name = "mk";
		input3.value = mk;
		form.appendChild(input3);
		
		input4.name = "data";
		input4.value = data;
		form.appendChild(input4);
		
		input5.name = "datemulai";
		input5.value = datemulai;
		form.appendChild(input5);
		
		input6.name = "dateselesai";
		input6.value = dateselesai;
		form.appendChild(input6);
		
		input7.name = "kelas";
		input7.value = kelas;
		form.appendChild(input7);
		
		input8.name = "type";
		input8.value = type;
		form.appendChild(input8);
		
		document.body.appendChild(form);
		form.submit();
	}else{
		alert("Lengkapi Data Anda!");
	}
}

function cetak(thn, mk, prodi, kls, dosen, tgl_mulai, tgl_selesai, type)
{
	/*var form = document.createElement("form");
	var input = document.createElement("input");
	var input2 = document.createElement("input");
	var input3 = document.createElement("input");
	var input4 = document.createElement("input");
	var input5 = document.createElement("input");
	var input6 = document.createElement("input");
	var input7 = document.createElement("input");
	
	form.action = base_url + 'module/akademik/absen/detail';
	form.method = "post"
	
	input.name = "thnid";
	input.value = thn;
	form.appendChild(input);
	
	input2.name = "mk";
	input2.value = mk;
	form.appendChild(input2);
	
	input3.name = "dosen";
	input3.value = dosen;
	form.appendChild(input3);
	
	input4.name = "datemulai";
	input4.value = tgl_mulai;
	form.appendChild(input4);
	
	input5.name = "dateselesai";
	input5.value = tgl_selesai;
	form.appendChild(input5);
	
	input6.name = "kelas";
	input6.value = kls;
	form.appendChild(input6);
	
	input7.name = "view";
	input7.value = "cetak";
	form.appendChild(input7);
	
	document.body.appendChild(form);
	//form.submit();
	//window.open(form.submit(),"_blank");	*/
	
	if(! dosen) dosen = "-";
	if(! thn) thn = "-";
	if(! kls) kls = "-";
	if(! prodi) prodi = "-";
	if(! mk) mk = "-";
	if(type=="dosen"){
		url_cetak = 'module/master/report/get_rekap/';
	}
	else{
		url_cetak = 'module/master/report/get_rekap_asisten/';
	}
	//alert(dosen);
	// alert(thn);	
		$.ajax({
					
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/report/detail',
				data : $.param({
					thnid : thn,
					mk: mk,
					data: dosen,
					datemulai:tgl_mulai,
					dateselesai	: tgl_selesai,
					kelas	: kls,
					prodi	: prodi,
					type 	: type,
					view_	: 'cetak',
				}),
				success : function(msg) {			
					window.open(base_url + url_cetak+thn+'/'+prodi+'/'+mk+'/'+kls+'/'+dosen+'/'+tgl_mulai+'/'+tgl_selesai+'/cetak',"_blank");			
				}
			});	
};

function add_absen(karyawan,dosen,jadwal){
	$("#myModalLabel").html("Tambah Absen");
	$("input[name='dosen']").val(dosen);
	// alert(karyawan);
	$.ajax({
        url : base_url + 'module/master/report/get_jadwal',
        type: "POST",
        dataType : "html",
        data : $.param({
        	karyawan : karyawan
        }),
        success:function(data) 
        {
        	// alert(data);
        	$("select[name='jadwal-mk']").html(data);
        	$("select[name='jadwal-mk']").select2("val",jadwal);
        }
    });
    
    $('#dateMulai, #dateSelesai, .date').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
	
	$('.time').datetimepicker({
	  pickTime : true,
	  pickDate : false,
	  format : 'HH:mm:s'
	});
}

function edit_absen(absen,karyawan){
	$("#myModalLabel").html("Edit Absen");
	$.ajax({
        url : base_url + 'module/master/report/get_jadwal',
        type: "POST",
        dataType : "html",
        data : $.param({
        	karyawan : karyawan
        }),
        success:function(data) 
        {
        	$("select[name='jadwal-mk']").html(data);
        }
    });
	$.ajax({
        url : base_url + 'module/master/report/get_absen',
        type: "POST",
        dataType : "json",
        data : $.param({
        	absen : absen
        }),
        success:function(data) 
        {
        	$.each(data, function(i, f){
        		// alert(f.sesi);
				$("select[name='jadwal-mk']").select2("val",f.jadwal_id);
				$("input[name='tgl_pertemuan']").val(f.tgl_pertemuan);
				$("input[name='jam_mulai']").val(f.jam_masuk);
				$("input[name='jam_selesai']").val(f.jam_selesai);
				$("input[name='materi']").val(f.materi);
				$("input[name='total']").val(f.total);
				$("select[name='sesi']").select2("val",f.sesi);
				$("input[name='hidId_absen']").val(f.hidId);
				$("input[name='hidId_absendosen']").val(f.absendosen_id);
				$("input[name='dosen']").val(f.dosen_id);
			});
        }
    });
    $('#dateMulai, #dateSelesai, .date').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
	
	$('.time').datetimepicker({
	  pickTime : true,
	  pickDate : false,
	  format : 'HH:mm:s'
	});
};

function delete_absen(absen){
	var x =  confirm("Apakah Anda yakin ingin menghapus data ini?");
	if(x){
		$.ajax({
	        url : base_url + 'module/master/report/hapus_absen',
	        type: "POST",
	        dataType : "html",
	        data : $.param({
	        	absen : absen
	        }),
	        success:function(msg) 
	        {
	        	alert(msg);
	        	location.reload();
	        }
	    });
   }
}

function add_absen_asisten(mhs,asisten,jadwal){
	$("#myModal_asistenLabel").html("Tambah Absen");
	$("input[name='asisten']").val(asisten);
	$.ajax({
        url : base_url + 'module/master/report/get_jadwal_asisten',
        type: "POST",
        dataType : "html",
        data : $.param({
        	mhs : mhs,
        	asisten : asisten
        }),
        success:function(data) 
        {
        	//alert(data);
        	$("select[name='jadwal-mk']").html(data);
        	$("select[name='jadwal-mk']").select2("val",jadwal);
        	
        	var jadwal_id = $("select[name='jadwal-mk']").val();
			var start = $('option:selected', "select[name='jadwal-mk']").attr('start');
			var finish = $('option:selected', "select[name='jadwal-mk']").attr('finish');
			var kelas = $('option:selected', "select[name='jadwal-mk']").attr('kelas');
			$("input[name='jam_mulai']").val(start);
			$("input[name='jam_selesai']").val(finish);
			$("input[name='kelas']").val(kelas);
        }
    });
	
	$('#dateMulai, #dateSelesai, .date').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
	
	$('.time').datetimepicker({
	  pickTime : true,
	  pickDate : false,
	  format : 'HH:mm:s'
	});
}

function edit_absen_asisten(mhs,asisten,jadwal,absen){
	$("#myModal_asistenLabel").html("Edit Absen");
	$("input[name='asisten']").val(asisten);
	$.ajax({
        url : base_url + 'module/master/report/get_jadwal_asisten',
        type: "POST",
        dataType : "html",
        data : $.param({
        	mhs : mhs,
        	asisten : asisten
        }),
        success:function(data) 
        {
        	//alert(data);
        	$("select[name='jadwal-mk']").html(data);
        	$("select[name='jadwal-mk']").select2("val",jadwal);
        }
    });
	
	$.ajax({
        url : base_url + 'module/master/report/get_absen_asisten',
        type: "POST",
        dataType : "json",
        data : $.param({
        	absen : absen
        }),
        success:function(data) 
        {
        	//alert(data);
        	$.each(data, function(i, f){
        		//alert(f.materi);
				$("select[name='jadwal-mk']").select2("val",f.jadwal_id);
				$("input[name='tgl_pertemuan']").val(f.tgl_pertemuan);
				$("input[name='jam_mulai']").val(f.jam_masuk);
				$("input[name='jam_selesai']").val(f.jam_selesai);
				$("input[name='materi']").val(f.materi);
				$("input[name='total']").val(f.total);
				$("select[name='sesi']").select2("val",f.sesi);
				$("input[name='hidId_absen']").val(f.absen_id);
				$("input[name='hidId_asistenabsen']").val(f.jadwal_asisten_id);
				$("input[name='asisten']").val(f.asisten_id);
				$("input[name='kelas']").val(f.kelas);
				$("select[name='kelompok']").select2("val",f.kelompok_id);
			});
        }
    });
	
	$('#dateMulai, #dateSelesai, .date').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
	
	$('.time').datetimepicker({
	  pickTime : true,
	  pickDate : false,
	  format : 'HH:mm:s'
	});
}

function delete_absen_asisten(absen){
	var x =  confirm("Apakah Anda yakin ingin menghapus data ini?");
	if(x){
		$.ajax({
	        url : base_url + 'module/master/report/hapus_absen_asisten',
	        type: "POST",
	        dataType : "html",
	        data : $.param({
	        	absen : absen
	        }),
	        success:function(msg) 
	        {
	        	alert(msg);
	        	location.reload();
	        }
	    });
   }
}

$(document).on('change','#inp_prodi', function(){
	filter_prodi();
});