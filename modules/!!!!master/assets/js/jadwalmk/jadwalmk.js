$(document).ready(function() {
	$(".e9").select2();
	var fkid			= document.getElementById("hidfakultasid");
	var fakultas_id_	= $(fkid).val();
	
	var pdid			= document.getElementById("hidprodi");
	var prodi_id_		= $(pdid).val();
	
	var cabang			= document.getElementById("select_cabang");
	var cabang_id_		= $(cabang).val();
	
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/master/jadwalmk/get_prodi_by_fakultas',
		data : $.param({
			fakultas_id : fakultas_id_,
			prodi_id : prodi_id_
		}),
		success : function(msg) {
			if (msg == '') {
				
			} else {
				$("#select_prodi").html(msg);
			}
		}
	});
	
	$.ajax({
			type : "POST",
			dataType : "json",
			url : base_url + "/module/master/conf/ruang",
			data : $.param({
				cabang_id : cabang_id_
			}),
			success : function(msg) {
				if (msg == '') {
					
				} else {
					$("#ruang").autocomplete({ 
						source: msg,
						minLength: 0, 
						select: function(event, ui) { 
							$('#ruang').val(ui.item.value);
						} 
					});
				}
			}
	});
	
	$("#select_cabang").change(function() {
		var cabang_id_		= $(this).val();
		
		$.ajax({
			type : "POST",
			dataType : "json",
			url : base_url + "/module/master/conf/ruang",
			data : $.param({
				cabang_id : cabang_id_
			}),
			success : function(msg) {
				if (msg == '') {
					
				} else {
					$("#ruang").autocomplete({ 
						source: msg,
						minLength: 0, 
						select: function(event, ui) { 
							$('#ruang').val(ui.item.value);
						} 
					});
				}
			}
		});
	});
	
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		//alert(fakultas_id_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/jadwalmk/get_prodi_by_fakultas',
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					//$("#select_thn").html(msg);
					// alert("tes");
				} else {
					$("#select_prodi").removeAttr("disabled");
					//$("#addnamamk").removeAttr("disabled");
					$("#select_prodi").html('<option value="0">Select Prodi</option>');
					$("#select_prodi").html(msg);
				}
			}
		});
		
		//----------------ajax dosen pengampu---------------------------------------
		var uri2 = base_url + "/module/master/jadwalmk/get_dosenbyfakultas";
		//alert(uri2);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri2,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					//$("#select_thn").html(msg);
					// alert("tes");
				} else {
					$("#select_pengampu").html('<option value="0">Select Dosen Pengampu</option>');
					$("#select_pengampu").removeAttr("disabled");
					$("#select_pengampu").html(msg);
				}
			}
		});
		//----------------ajax dosen pengampu---------------------------------------
		
	});

	
	$("#submit-jadwal").click(function(e){
		var thn_akademik = $('#thn_akademik').val();
		var select_prodi = $('#select_prodi').val();
		var dosenpengampu = $('#dosenpengampu').val();
		var namamk = $('#namamk').val().length;
		var kelas = $('#kelas').val().length;

		if(thn_akademik!=0 && select_prodi!=0 && dosenpengampu!=0 && namamk > 0 && kelas > 0){
	      var postData = $('#form').serializeArray();
          $.ajax({
            url : base_url + "module/master/jadwalmk/saveToDB",
	        type: "POST",
	        data : postData,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Success!');
	            //location.reload();
	            location.href=base_url + "module/master/jadwalmk/index";
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        }
	      });
	    e.preventDefault(); //STOP default action
		}
		else{
			alert("Please fill the form");
		}
	});
	
});

function validate() {
	if (document.getElementById('isonline').checked) {
    	$("#form_no_is_online").fadeIn();
    	//$('#form').removeAttr('style');  
    } 
    else {
       $("#form_no_is_online").fadeOut();
       //$("#form").attr("style", "display: none;");
    }
}