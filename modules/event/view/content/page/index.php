<?php $this->head(); ?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/event/content/page'); ?>">Content</a></li>
  <li class="active"><a href="#">data</a></li>
</ol>
<div class="breadcrumb-more-action">
	<?php if($user!="mahasiswa"){ //if($user!="mahasiswa"&&$user!="dosen"){ ?>
	<a href="<?php echo $this->location('module/event/content/write/page'); ?>" class="btn btn-primary">
	<i class="fa fa-pencil icon-white"></i> New Content</a> 
	<?php } ?>
</div>

<div class="row">
	<div class="col-md-4">
    	<?php $this->view('content/page/sidebar.php', $data); ?>
	</div>
	
	<div id="isi" class="col-md-8">
		<?php
		if($content_view=='content'){
			$this->view('content/page/view-content.php', $data );
		}
		elseif($content_view=='upload'){
			$this->view('content/page/view-file.php', $data );
		}
		?>
	</div>
</div>
<?php $this->foot(); ?>