<form method=post name="form-setting" id="form-setting">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-pencil icon-white"> </i>New Content Category</div>
		<div class="panel-body ">
			
			<div class="form-group">	
				<label class="control-label">Category</label>					
				<select id="category" class="form-control e9" name="category" >
				<option value="0">Select category</option>
				<option value="content">Content</option>
				<option value="upload">Upload</option>
				<option value="menu">Menu</option>
				</select>
			</div>
			
			<div class="form-group">
				<label class="control-label">Content</label>
				<input name="content" id="content" class="form-control" value="" required="" type="text">
			</div>
			
			<div class="form-group">
				<label class="control-label">Note</label>
				<textarea name="note" id="note" class="form-control"></textarea>
			</div>
			
			<div class="form-actions">
				<input name="hidcategory" value="" type="hidden">
				<input name="hidcontent" value="" type="hidden">
				<input name="b_content_category" id="b_content_category" value=" Data Valid &amp; Save " class="btn btn-primary" type="submit">&nbsp;
				<a href="<?php echo $this->location('module/event/content/setting'); ?>" class="btn category-content-cancel btn-default" style="display: none">Cancel</a>
			</div>
			
		</div>
	</div>
</form>