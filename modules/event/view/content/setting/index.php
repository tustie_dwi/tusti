<?php $this->head(); ?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/event/content'); ?>">Content</a></li>
  <li class="active"><a href="<?php echo $this->location('module/event/content/setting'); ?>">Setting</a></li>
</ol>
<div class="breadcrumb-more-action">
</div>

<div class="row">
	<div id="isi" class="col-md-7">
		<?php 	
		 if(isset($posts)):	?>
		<table class='table table-hover' id='example'>
			<thead>
				<tr>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			<?php	
				if($posts > 0){
					$i= 1;
					foreach ($posts as $dt): ?>
						<tr>
							<td>
							<div class="col-md-9">
								<?php echo $dt->content_category ?>&nbsp;
								<span class="label label-success"><?php echo $dt->category ?></span>&nbsp;
								<span class="label label-default"><small><?php echo $dt->note ?></small></span>
							</div>
							
							<div class="col-md-3">
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
										<a class='btn-edit-post' onclick="edit_category('<?php echo $dt->content_category ?>', '<?php echo $dt->category ?>', '<?php echo $dt->note ?>')" href="javascript::>"><i class='fa fa-edit'></i> Edit</a>	
										</li>
									  </ul>
									</li>
								</ul>
							</div>
							</td>
						</tr>
					<?php endforeach; 
				 }
			?>
			</tbody>
		</table>	
		 <?php
		 else: 
		 ?>
		<div class="row">
	    <div class="col-md-12" align="center" style="margin-top:20px;">
		    <div class="well">Sorry, no content to show</div>
	    </div>
	    </div>
	    <?php endif; ?>
	</div>
	
	<div class="col-md-5">
		<?php $this->view('content/setting/edit.php', $data ); ?>
	</div>
	
</div>
<?php $this->foot(); ?>