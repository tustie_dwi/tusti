<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li class="active"><a>Kelola Peserta</a></li>
	</ol>
	<div class="row">
		<div class="col-md-7 block-box">
					<h3 class="panel-title">Daftar Peserta Kegiatan : <?php echo $kegiatan->nama."@".$kegiatan->lokasi ?></h3>
					<div id="jenis_peserta" style="padding:10px 0 10px 0;vertical-align: middle;">
						<a class="btn" style="border-left: 3px solid #262626;" data-toggle="modal" data-target="#tambah_jenis_peserta">Jenis Peserta : </a>
						<?php 
							if(!$jenis_peserta_kegiatan){}
							else
							foreach ($jenis_peserta_kegiatan as $key => $value) {
								echo "<span class='label label-default' style='margin:0px 3px'>".$value->keterangan." <a href='#' onclick='deleteJenisPeserta(\"".$value->jenis_peserta."\",this)' style='opacity:0.6;color:white;'>X</a></span>";
							}
						?>
					</div>
				
					<table class="table tabel-hasil" id="example">
						<thead>
							<th>&nbsp;</th>
						</thead>
						<tbody id="table_kegiatan_peserta">
							<?php
								if (!$peserta_kegiatan){}
								else
									foreach ($peserta_kegiatan as $peserta){
										switch($peserta->st_dok){
											case '0': $str='Submitted';break;
											case '1': $str='Accepted';break;
											case '2': $str='Under Review';break;
											case '3': $str='Rejected';break;
											default:$str='none';
											break;
										}
									
										echo "<tr><td>";
											echo '<div class="col-md-9">';
												echo "<b>".$peserta->nama.' '.$peserta->gelar_awal.' '. $peserta->gelar_akhir."</b><span class='label label-default'>".$peserta->jenis_peserta."</span>&nbsp;<span class='label label-danger'>PIN ".$peserta->pin."</span>";
												if($peserta->st_dok) echo "&nbsp;<span class='label label-success'>".$str."</span>";
												echo "<br><small>".$peserta->alamat."&nbsp;<i class='fa fa-envelope-o'></i> ".$peserta->email."&nbsp;<i class='fa fa-mobile'></i> ".$peserta->telp."<i class='fa fa-map-marker'></i> ".$peserta->negara."</small></div>";
											echo '<div class="col-md-3">';
										echo "<ul class='nav nav-pills' style='margin:0;'>
													<li class='dropdown'>
													  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
													  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
														<li>
															<a class='btn-edit-post' href=".$this->location('module/event/kegiatan/peserta/'.$kegiatan->kegiatanid.'/'.$peserta->pesertaid)."><i class='fa fa-user'></i> Tagihan</a>	
														</li>
														<li>
															<a class='btn-edit-post' href=".$this->location('module/event/kegiatan/berkas/'.$kegiatan->kegiatanid.'/'.$peserta->pesertaid)."><i class='fa fa-file-o'></i> Berkas</a>	
														</li>
														<li>
															<a class='btn-edit-post' href=".$this->location('module/event/kegiatan/tahap/'.$kegiatan->kegiatanid.'/'.$peserta->pesertaid)."><i class='fa fa-gear'></i> Tahap</a>	
														</li>
														<li>
															<a class='btn-edit-post' href=".$this->location('module/event/kegiatan/nilai/'.$kegiatan->kegiatanid.'/'.$peserta->pesertaid)."><i class='fa fa-bar-chart'></i> Nilai</a>	
														</li>
														<li>
															<a class='btn-edit-post' data-param='".base64_encode(json_encode($peserta))."' onclick='editPesertaKegiatan(this)' href='javascript::'><i class='fa fa-edit'></i> Edit</a>	
														</li>
														<li>
														<a class='btn-edit-post' onclick='deletePesertaaKegiatan(\"".$peserta->peserta_id."\",this)' href='javascript::'><i class='fa fa-trash-o'></i> Delete</a>	
														</li>
													  </ul>
													</li>
												</ul>";
									echo '</div>';
									
									echo"
										</td>
									</tr>";
									
										
									} 
							?>
						</tbody>
					</table>
		</div>
		<div class="col-md-5">
			<form id="form_kegiatan_peserta">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">Form Peserta</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input type="text" id="nama" name="nama" class="form-control" required="">
							<input type="hidden" class="form-control" id="kegiatan_id" name="kegiatan_id" value="<?php echo $kegiatan->kegiatan_id; ?>"/>
							<input type="hidden" class="form-control" id="kegiatanid" name="kegiatanid" value="<?php echo $kegiatan->kegiatanid; ?>"/>
							<input type="hidden" class="form-control" id="peserta_id" name="peserta_id" value="<?php echo $kegiatan->peserta_id; ?>" disabled=""/>
						</div>
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-md-6">
								<label>First Name</label>
								<input type="text" id="first_name" name="first_name" class="form-control" required="">
							</div>
							<div class="col-md-6">
								<label>Last Name</label>
								<input type="text" id="last_name" name="last_name" class="form-control" >
							</div>
						</div>
						<div class="row" style="margin-bottom: 10px;">
							<div class="col-md-6">
								<label>Gelar Awal</label>
								<input type="text" id="gelar_awal" name="gelar_awal" class="form-control">
							</div>
							<div class="col-md-6">
								<label>Gelar Akhir</label>
								<input type="text" id="gelar_akhir" name="gelar_akhir" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label>E-Mail</label>
							<input type="email" id="email" name="email" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Telp</label>
							<input type="text" id="telp" name="telp" class="form-control">
						</div>
						<div class="form-group">
							<label>Hp</label>
							<input type="text" id="hp" name="hp" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<textarea class="form-control" id="alamat" name="alamat" required=""></textarea>
						</div>
						<div class="form-group">
							<label>Negara</label>
							<select class="form-control select" name="negara" id="negara">
								<?php
		      						foreach ($negara as $value) {
										echo "<option value='".$value->negara."'>".$value->negara."</option>";	  
									}
		      					?>
							</select>
						</div>
						<div class="form-group">
							<label>Total Tagihan</label>
							<input type="number" id="total_tagihan" value="0" name="total_tagihan" class="form-control">
						</div>
						<div class="form-group">
							<label>Total Bayar</label>
							<input type="number" id="total_bayar" value="0" name="total_bayar" class="form-control">
						</div>
						
						<div class="form-group">
							<label>Instansi</label>
							<input type="text" id="instansi" name="instansi" class="form-control">
						</div>
						<div class="form-group">
							<label>Alamat Instansi</label>
							<textarea class="form-control" id="alamat_instansi" name="alamat_instansi"></textarea>
						</div>
						<div class="form-group">
							<label>Tgl Registrasi</label>
							<div class='input-group date' >
								<input type='text' class="form-control date" name="tgl_registrasi" id='tgl_registrasi' required=""/>
								<span class="input-group-addon"><span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label>Jenis Peserta</label>
							<select id="jenis_peserta_peserta" name="jenis_peserta" class="form-control" required="">
								<?php
		      						foreach ($jenis_peserta_kegiatan as $key => $value) {
										echo "<option id='".$value->jenis_peserta."' value='".$value->jenis_peserta."'>".$value->keterangan."</option>";	  
									}
		      					?>
							</select>
						</div>
						<div class="form-group">
							<label>Jenis Pendaftaran</label>
							<select id="metode_pendaftaran" name="jenis_pendaftaran" class="form-control" required="">
								<option value="online">On Line</option>
								<option value="offsite">Off site</option>
							</select>
						</div>
						<div class="form-group">
							<label>Metode Pembayaran</label>
							<select id="metode_pembayaran" name="metode_pembayaran" class="form-control" required="">
								<option value="transfer">Transfer</option>
								<option value="tunai">Tunai</option>
							</select>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<textarea class="form-control" id="keterangan" name="keterangan"></textarea>
						</div>
						<div class="form-group">
							<label for="is_valid">Valid?</label><br/>
							<input type="checkbox" name="is_valid" id="is_valid" />
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary" type="button" onclick="simpanPesertaKegiatan()">Simpan</button>
						<button class="btn btn-warning" type="button" onclick="cancelPesertaKegiatan()">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal fade" id="tambah_jenis_peserta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	  		<form class="form-horizontal" id="form_jenis_peserta">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        			<h4 class="modal-title" >Tambah Jenis Peserta</h4>
	      			</div>
	      			<div class="modal-body">
	      				<label>Pilih Jenis Peserta</label>
		      				<select class="form-control" id="jenis_peserta" name="jenis_peserta" onchange='yeah(this.options[this.selectedIndex].text,this.value)'>
		      					<?php
		      						foreach ($jenis_peserta as $key => $value) {
										echo "<option value='".$value->jenis_peserta."'>".$value->keterangan."</option>";	  
									}
		      					?>
		      				</select>
		      				<input type="hidden" name="kegiatan_id" id="kegiatan_id_jenis_peserta" value="<?php echo $kegiatan->kegiatan_id; ?>">
	      			</div>
	      			<div class="modal-footer">
	      				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        			<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="submit_jenis_peserta()">Tambah</button>
	      			</div>
	      		</div>
	      	</form>
	    </div>
	</div>
<?php $this->foot(); ?>