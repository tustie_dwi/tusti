<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li class="active"><a>Komponen Nilai Kegiatan</a></li>
	</ol>
	<div class="row">
		<div class="col-md-12" id="list-nilai">
			<div class="panel">
				<div class="panel-heading">
					<button type="button" class="btn btn-default btn-xs pull-right" onclick="tambahSubNilai(0,this)"><i class="fa fa-plus"></i> Komponen Nilai</button>
					<h4 class="panel-title">Komponen Nilai</h4>
				</div>
				<div class="panel-body">
					<div class="list-group" style='margin:0px' id="parentid_0">
						<?php
							if (!$kegiatan_nilai){}
							else drawTree("0",$kegiatan_nilai);
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5" id="form_nilai" style="display: none" >
			<div class="panel">
				<form id='form_nilai_kegiatan'>
				<div class="panel-heading">
					<h4 class="panel-title">Form Komponen Nilai</h4>
				</div>
				<div class="panel-body">
					<div class='form-group'>
						<label>Nilai Judul</label>
						<input type="text" class="form-control" name="judul" id="judul" required="">
						<input type="hidden" class="form-control" name="parent_id" id="parent_id" required="">
						<input type="hidden" class="form-control" name="kegiatan_id" id="kegiatan_id" value="<?php echo $kegiatan->kegiatan_id; ?>">
						<input type="hidden" class="form-control" name="urut" id="urut" disabled="">
						<input type="hidden" class="form-control" name="komponen_nilai_id" id="komponen_nilai_id" disabled="">
					</div>
					<div class="form-group">
						<label>Keterangan</label>
						<input type="text" class="form-control" name="keterangan" id="keterangan" required="">
					</div>
					<div class="form-group">
						<label for="is_aktif">Aktif ?</label><br/>
						<input type="checkbox" name="is_aktif" id="is_aktif" />
					</div>
				</div>
				<div class='panel-footer'>
					<button class='btn btn-primary' onclick="submitSubNilai()" type="button">Simpan</button>
					<button class='btn btn-default' onclick="cancelNilai()" type="button">Cancel</button>
				</div>
				</form>
			</div>
		</div>
	</div>
<?php $this->foot(); ?>
<?php 
function drawTree($parent_id, $kegiatan_nilai){
	foreach ($kegiatan_nilai as $key => $value) {
		if($value->parent_id == $parent_id){
			echo "<div class='list-group-item' style='padding:15px'>
					<a data-toggle='collapse' href='#parentid_".str_replace(".", "_", $value->urut)."'><h4 style=''>".$value->judul."</h4></a>
					<p>".$value->keterangan."</p>
					<a class='btn-default btn-xs' onclick='tambahSubNilai(\"".$value->urut."\")'><i class='fa fa-plus'></i></a>
					<a class='btn-default btn-xs edit' onclick='editSubNilai(\"".$value->urut."\",\"".$value->komponen_nilai_id."\",\"".$value->judul."\",\"".$value->keterangan."\",\"".$value->is_aktif."\",\"".$value->parent_id."\",this)'><i class='fa fa-pencil'></i></a>
					<a class='btn-default btn-xs' onclick='deleteSubNilai(\"".$value->komponen_nilai_id."\",this)'><i class='fa fa-remove'></i></a>
					<div class='list-group collapse in' style='margin:0px;' id='parentid_".str_replace(".", "_", $value->urut)."'>";
			drawTree($value->urut,$kegiatan_nilai);
			echo "</div></div>";
		}
	}
}
?>