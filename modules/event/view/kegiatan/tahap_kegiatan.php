<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li class="active"><a>Kelola Tahapan Kegiatan</a></li>
	</ol>
	<div class="row">
		<div class="col-md-12" id="list-tahap">
			<div class="panel">
				<div class="panel-heading">
					<button type="button" class="btn btn-default btn-xs pull-right" onclick="tambahSubTahap(0,this)"><i class="fa fa-plus"></i>Tahapan</button>
					<h3 class='panel-title'>Tahap Kegiatan : <?php echo $kegiatan->nama."@".$kegiatan->lokasi ?></h3>
				</div>
				<div class="panel-body">
					<div class="list-group" style='margin:0px' id="parentid_0">
						<?php
							if (!$kegiatan_tahap){}
							else drawTree("0",$kegiatan_tahap);
						?>
					</div>
				</div>
			</div>
		</div>
		<div class='col-md-5' id="form_tahap" style="display: none;">
			<div class='panel'>
				<form id='form_tahap_kegiatan'>
				<div class='panel-heading'>
					<h4 class='panel-title'>Form Tahap</h4>
				</div>
				<div class='panel-body'>
					<div class='form-group'>
						<label>Keterangan Tahap</label>
						<input type="text" class="form-control" name="keterangan" id="keterangan" required="">
						<input type="hidden" class="form-control" name="parent_id" id="parent_id" required="">
						<input type="hidden" class="form-control" name="kegiatan_id" id="kegiatan_id" value="<?php echo $kegiatan->kegiatan_id; ?>">
						<input type="hidden" class="form-control" name="urut" id="urut" disabled="">
						<input type="hidden" class="form-control" name="tahap_id" id="tahap_id" disabled="">
						<input type="hidden" class="form-control" name="kegiatanid" id="kegiatanid" value="<?php echo $kegiatan->kegiatanid; ?>">
					</div>
				</div>
				<div class='panel-footer'>
					<button class='btn btn-primary' onclick="submitSubTahap()" type="button">Simpan</button>
					<button class='btn btn-default' onclick="cancelTahap()" type="button">Cancel</button>
				</div>
				</form>
			</div>
		</div>
	</div>
<?php $this->foot(); ?>
<?php 
function drawTree($parent_id, $kegiatan_tahap){
	foreach ($kegiatan_tahap as $key => $value) {
		if($value->parent_id == $parent_id){
			echo "<div class='list-group-item' style='padding:15px'>
					<a data-toggle='collapse' href='#parentid_".str_replace(".", "_", $value->urut)."'><h4 style=''>".$value->keterangan."</h4></a>
					<a class='btn-default btn-xs' onclick='tambahSubTahap(\"".$value->urut."\")'><i class='fa fa-plus'></i></a>
					<a class='btn-default btn-xs' onclick='editSubTahap(\"".$value->urut."\",\"".$value->tahap_id."\",\"".$value->keterangan."\",\"".$value->parent_id."\", this)'><i class='fa fa-pencil'></i></a>
					<a class='btn-default btn-xs' onclick='deleteSubTahap(\"".$value->tahap_id."\", this)'><i class='fa fa-remove'></i></a>
					<div class='list-group collapse in' style='margin:0px;' id='parentid_".str_replace(".", "_", $value->urut)."'>";
			drawTree($value->urut,$kegiatan_tahap);
			echo "</div></div>";
		}
	}
}
?>
