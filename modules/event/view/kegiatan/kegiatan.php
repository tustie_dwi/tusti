<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  <li class="active"><a>Agenda Kegiatan</a></li>
	</ol>
	<div class="row">
		<div class="col-md-7 block-box">
			<table class="table">
				<thead>
					<th>&nbsp;</th>
				</thead>
				<tbody id="table_kegiatan">
					<?php
					$baseUrl = $this->location('module/event/kegiatan/');
					if($kegiatan)drawTree("0",$kegiatan,$baseUrl,0);
					
					function drawTree($parent_id, $kegiatan, $baseUrl, $margin_left){
						foreach ($kegiatan as $key => $value) {
							if($value->parent_id == $parent_id){
							echo "<tr><td style='padding-left:".$margin_left."px;'>";
							echo '<div class="pull-right">';
								echo "<ul class='nav nav-pills' style='margin:0;'>
											<li class='dropdown'>
											  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
											  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
												<li>
													<a class='btn-edit-post' href=".$baseUrl.'/peserta/'.$value->kegiatanid."><i class='fa fa-user'></i> Peserta</a>	
												</li>
												<li>
													<a class='btn-edit-post' href=".$baseUrl.'/biaya/'.$value->kegiatanid."><i class='fa fa-money'></i> Biaya</a>	
												</li>
												<li>
													<a class='btn-edit-post' href=".$baseUrl.'/tahap/'.$value->kegiatanid."><i class='fa fa-gear'></i> Tahap</a>	
												</li>
												<li>
													<a class='btn-edit-post' href=".$baseUrl.'/nilai/'.$value->kegiatanid."><i class='fa fa-bar-chart'></i> Nilai</a>	
												</li>
												<li>
													<a class='btn-edit-post' href=".$baseUrl.'/faq/'.$value->kegiatanid."><i class='fa fa-question-circle'></i> FAQ</a>	
												</li>
												<li>
													<a class='btn-edit-post' data-param='".base64_encode(json_encode($value))."' onclick='editKegiatan(this)' href='javascript::'><i class='fa fa-edit'></i> Edit</a>	
												</li>
												<li>
													<a class='btn-edit-post' onclick='hapusKegiatan(\"".$value->kegiatan_id."\",this)' href='javascript::'><i class='fa fa-trash-o'></i> Delete</a>	
												</li>
											  </ul>
											</li>
										</ul>";
							echo '</div>';
							echo "<b>".$value->nama."</b>";
							echo "<br><small>".$value->lokasi."&nbsp;<i class='fa fa-calendar'></i> ".$value->tgl_mulai."</small>";
							echo"</td></tr>";
							drawTree($value->kegiatan_id,$kegiatan, $baseUrl, $margin_left+20);
							}
						}
					}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-5">	
			<form id="form_kegiatan" action="<?php echo $this->location('module/event/kegiatan/submit_kegiatan'); ?>" method="post" enctype="multipart/form-data">
			<div class="panel">
				<div class="panel-heading" style="  padding: 10px; border-bottom: 1px solid #eee;">
					<h4 class="panel-title">Form Kegiatan</h4>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>Jenis Kegiatan</label>
						<select class="form-control select2" id="jenis_kegiatan" name="jenis_kegiatan" required="">
							<?php
								foreach ($jenis_kegiatan as $key => $value) {
									echo "<option value='".$value->jenis_kegiatan_id."' >".$value->keterangan."</option>";
								} 
							?>
						</select>
					</div>
					<div class="form-group">
						<label>Unit ID</label>
						<select class="form-control select2" id="unit_id" name="unit_id" required="">
							<?php
								foreach ($unit_id as $key => $value) {
									echo "<option value='".$value->unit_id."' >".$value->keterangan."</option>";
								} 
							?>
						</select>
					</div>
					<div class="form-group">
						<label>Kagiatan Unit</label>
						<select class="form-control select2" id="parent_id" name="parent_id" required="">
							<option value="0">Kegiatan Baru</option>
							<?php
								foreach ($kegiatan as $key => $value) {
									echo "<option value='".$value->kegiatan_id."' >Kegiatan : ".$value->nama." @ ".$value->lokasi."</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label>Nama Kegiatan</label>
						<input type="text" class="form-control" id="nama" name="nama" required="" />
						<input type="hidden" class="form-control" id="kegiatan_id" name="kegiatan_id" disabled=""/>
					</div>
					<div class="form-group">
						<label>Lokasi</label>
						<input type="text" class="form-control" id="lokasi" name="lokasi" required=""/>
					</div>
					<div class="form-group">
						<label>Kode Kegiatan</label>
						<input type="text" class="form-control" id="kode_kegiatan" name="kode_kegiatan" required=""/>
					</div>
					<div class="form-group">
						<label for="is_aktif">Publish ?</label><br>
						<input type="checkbox" name="is_aktif" id="is_aktif" value="pub"/>
					</div>
					<div class="form-group" style="margin-left:-15px; margin-right:-15px;">
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Mulai</label>
								<div class='input-group' >
									<input type='text' class="form-control date" name="tgl_mulai" id='tgl_mulai' required=""/>
									<span class="input-group-addon"><span class="fa fa-calendar"></span>
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tanggal Selesai</label>
								<div class='input-group' >
									<input type='text' class="form-control date" name="tgl_selesai" id='tgl_selesai' required=""/>
									<span class="input-group-addon"><span class="fa fa-calendar"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="file">Logo Kegiatan</label><br>
						<input type="file" class="form-control" name="file" id="file"/>
						<input type="hidden" name="logo" id="logo"/>
					</div>
					<div class="form-group">
						<label for="is_aktif">Keterangan</label><br>
						<textarea type="file" class="form-control" name="keterangan" id="keterangan"></textarea>
					</div>
					<hr>
					<div class="form-group">
						<label for="is_aktif">Host Name</label><br>
						<input type="text" class="form-control" name="host_name" id="host_name"/>
					</div>
					<div class="form-group">
						<label for="is_aktif_host_name">Aktif Host Name?</label>
						<input type="checkbox" name="is_aktif_host_name" id="is_aktif_host_name"/>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary btn-md" type="submit"> <!-- onclick="simpanKegiatan()" -->
						Simpan
					</button>
					<button class="btn btn-warning btn-md" type="button" onclick="cancelKegiatan()">Cancel</button>
				</div>
			</div>
			</form>
		</div>
	</div>
<?php $this->foot(); ?>
