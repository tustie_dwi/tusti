<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  <li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  <li class="active"><a>F.A.Q</a></li>
	</ol>
	<div class="row">
		<div class="col-md-12" id="list-faq">
			<div class="panel">
				<div class="panel-heading">
					<button type="button" class="btn btn-default btn-xs pull-right" onclick="tambahfaq()"><i class="fa fa-plus"></i> Tambah F.A.Q</button>
					<h4 class="panel-title">FAQ</h4>
				</div>
				<div class="panel-body">
					<div class="list-group" style='margin: 5px;' id="list-group-faq">
						<?php
							if($kegiatan_faq)
							foreach ($kegiatan_faq as $key => $value) {
								if($value->parent_id==0){
									echo "<div class='list-group-item'>
											<small class='pull-right'>".$value->comment_post."</small>
									    	<h4 class='list-group-item-heading'>".$value->title."<small> by ".$value->user_name ."</small></h4>
									    	<small class='pull-right'>".(($value->is_approve==1)?'disetujui':'tidak disetujui')."</small>
									    	<p class='list-group-item-text'>". $value->comment ."<br/>
									    		<button type='button' class='btn btn-primary btn-xs jawab' onclick='replayfaq(\"".$value->comment_id."\",this)'> <i class='fa fa-reply'></i> jawab</button>
									    		<button type='button' class='btn btn-warning btn-xs edit' onclick='editfaq(\"".base64_encode(json_encode($value))."\", this)'> <i class='fa fa-edit'></i> edit</button>
									    		<button type='button' class='btn btn-danger btn-xs delete' onclick='deletefaq(\"".$value->comment_id."\", this)'> <i class='fa fa-trash-o'></i> hapus</button>
									    	</p>
									    	<div class='list-group' style='margin:0px; padding-left:60px; padding-top:10px; margin-right: -16px; margin-bottom: -11px;  margin-top: -7px;'>";
											foreach ($kegiatan_faq as $k => $val) {
									    		if($val->parent_id == $value->comment_id){
									    			echo "<div class='list-group-item'>
															<small class='pull-right'>".$val->comment_post."</small>
														   	<h4 class='list-group-item-heading'>".$val->title."<small> by ". $val->user_name."</small></h4>
														   	<small class='pull-right'>".(($val->is_approve==1)?'disetujui':'tidak disetujui')."</small>
														   	<p class='list-group-item-text'>
														   		".$val->comment."<br/>
													    		<button type='button' class='btn btn-warning btn-xs edit' onclick='editfaq(\"".base64_encode(json_encode($val))."\", this)'><i class='fa fa-edit'></i> edit</button> 
													    		<button type='button' class='btn btn-danger btn-xs delete' onclick='deletefaq(\"".$val->comment_id ."\", this)'><i class='fa fa-trash-o'></i> hapus</button>
													    	</p>
													    </div>";
													}
												}
										echo "</div></div>";
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5" id="form_faq" style="display: none" >
			<div class="panel">
				<form id='form_faq_kegiatan'>
					<div class="panel-heading">
						<h4 class="panel-title" id="form_faq_title"></h4>
					</div>
					<div class="panel-body">
						<div class='form-group'>
							<label>Title</label>
							<input type="text" class="form-control" required="" name="title" id="title"/>
							<input type="hidden" class="form-control" name="kegiatan_id" id="kegiatan_id" value="<?php echo $kegiatan->kegiatan_id; ?>">
							<input type="hidden" class="form-control" name="comment_id" id="comment_id">
							<input type="hidden" class="form-control" name="parent_id" id="parent_id">
						</div>
						<div class='form-group'>
							<label>Deskripsi</label>
							<textarea type="text" class="form-control" required="" name="comment" id="comment"></textarea>
						</div>
						<div class='form-group'>
							<label for="is_approve">Approve ?</label>
							<input type="checkbox" name="is_approve" id="is_approve" />
						</div>
					</div>
					<div class="panel-footer">
						<button class='btn btn-primary' onclick="submitFaq()" type="button">Simpan</button>
						<button class='btn btn-default' onclick="cancelfaq()" type="button">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php $this->foot(); ?>