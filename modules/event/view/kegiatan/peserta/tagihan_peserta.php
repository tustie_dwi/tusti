<?php $this->head();?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  <li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  <li><a href="<?php echo $this->location('module/event/kegiatan/peserta/'.$kegiatan->kegiatanid); ?>">Kelola Peserta</a></li>
	  <li class="active"><a>Kelola Tagihan</a></li>
	</ol>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Tagihan : <?php echo $peserta->gelar_awal.' '.$peserta->nama.' '.$peserta->gelar_akhir." untuk kegiatan ".$kegiatan->nama.'@'.$kegiatan->lokasi ?></h3>
				</div>
				<div class="panel-body">
					<table class="table tabel-hasil" id="example">
						<thead>
							<th>Nama Biaya</th>
							<th>Jumlah</th>
							<th>Harga @</th>
							<th>Total</th>
							<th>#</th>
						</thead>
						<tbody id="table_tagihan_peserta">
							<?php
								if(!$biaya_tagihan_peserta){}
								else{
									foreach ($biaya_tagihan_peserta as $key => $value) {
										echo "<tr>
											  	  <td>".$value->nama_biaya."</td>
												  <td>".$value->inf_jumlah."</td>
												  <td>".$value->harga."</td>
												  <td>".$value->total."</td>
												  <td>
												  	<a class='label label-warning' onclick='editTagihanPeserta(\"".$value->tagihan_id."\",\"".$value->komponen_id."\",\"".$value->inf_jumlah."\",\"".$value->is_bayar."\",\"".$value->tgl_bayar."\",this)' > <i class='fa fa-pencil'></i> Rubah</a>
												  	<a class='label label-danger' onclick='deleteTagihanPeserta(\"".$value->tagihan_id."\",this)' > <i class='fa fa-remove'></i> Hapus</a>
												  </td>
											  </tr>";
									}
								} 
							?>
						</tbody>
					</table>					
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<form id="form_tagihan_peserta">
			<div class="panel">
				<div class="panel-heading">
					<a class='btn btn-primary btn-xs pull-right' href='<?php echo $this->location('module/event/kegiatan/biaya/'.$kegiatan->kegiatanid); ?>'><i class='fa fa-gear'></i> kelola biaya kegiatan</a>
					<h3 class="panel-title">
						Form Tagihan
					</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>Tagihan</label>
						<select class="form-control" id="komponen_id" name="komponen_id" required="">
							<?php
								if(!$biaya_kegiatan){}
								else
									foreach ($biaya_kegiatan as $key => $value) {
										echo "<option value='".$value->komponen_id."'>".$value->nama_biaya." : Rp ".$value->harga."</option>";
									}
							?>
						</select>
						<input type="hidden" name="peserta_id" id="peserta_id" value="<?php echo $peserta->peserta_id; ?>">
						<input type="hidden" name="tagihan_id" id="tagihan_id" value="" disabled="">
					</div>
					<div class="form-group">
						<label>Jumlah</label>
						<input type="number" class="form-control" id="inf_jumlah" name="inf_jumlah" required="">
					</div>
					<div class="row">
						<div class="col-md-8">
							<label>Tgl Bayar</label>
							<div class='input-group' >
								<input type='text' class="form-control date" name="tgl_bayar" id='tgl_bayar' required=""/>
								<span class="input-group-addon"><span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
						<div class="col-md-4">
							<center>
								<label for="is_bayar">Sudah Bayar?</label><br/>
								<input type="checkbox" aria-label="..." id="is_bayar" name="is_bayar">
							</center>
						</div>
					</div>
				</div>
				<div class="panel-footer">
	        		<button type="button" class="btn btn-primary" onclick="submit_tagihan_peserta()" >Simpan</button>
	        		<button type="button" class="btn btn-default" onclick="cancel_tagihan_peserta()" >Cancel</button>
	      		</div>
			</div>
			</form>
		</div>
	</div>
<?php $this->foot(); ?>