<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li><a href="<?php echo $this->location('module/event/kegiatan/peserta/'.$kegiatan->kegiatanid); ?>">Kelola Peserta</a></li>
	  	<li class="active"><a>Peserta Nilai</a></li>
	</ol>
	<div class="col-md-7">
		<div class="panel">
			<div class="panel-heading">
				<h4 class="panel-title">
					Nilai Peserta
				</h4>
			</div>
			<div class="panel-body">
				<table class="table tabel-hasil">
					<thead>
						<th>Keterangan Tahap</th>
						<th>Komponen Nilai</th>
						<th>Nilai</th>
						<th>#</th>
					</thead>
					<tbody id="table_nilai_peserta">
						<?php
							if(!$peserta_nilai){}
							else
							foreach ($peserta_nilai as $key => $value) {
								echo "<tr>
									    <td>
											<h5>".$value->keteranganTahap."</h5>
											<p>".$value->catatan."</p>
									 	</td>
									 	<td>
									 		<h5>".$value->judul."</h5>
											<p>".$value->keteranganKomponenNilai."</p>
									 	</td>
									 	<td>
									 		".$value->nilai."
									 	</td>
									 	<td>
									 		<a class='label label-warning' onclick='editPesertaNilai(\"".$value->nilai_peserta_id."\",\"".$value->komponen_nilai_id."\",\"".$value->peserta_tahap_id."\",\"".$value->nilai."\",\"".$value->uraian."\",\"".$value->dinilai_oleh."\",\"".$value->karyawan_id."\",this)'><i class='fa fa-pencil'></i> Rubah</a>
									 		<a class='label label-danger' onclick='deletePesertaNilai(\"".$value->nilai_peserta_id."\",this)'><i class='fa fa-remove'></i> Delete</a>
									 	</td>
									 </tr>";
							} 
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel">
			<form id="form_peserta_nilai">
			<div class="panel-heading">
				<h4 class="panel-title">
					Form Penilaian Tahap Peserta
				</h4>
			</div>
			<div class="panel-body">
				<div class="form-group">
					<a class="label label-primary pull-right" href="<?php echo $this->location('module/event/kegiatan/nilai/'.$kegiatan->kegiatanid); ?>"><i class="fa fa-bar-chart"></i> Komponen Nilai</a>
					<label>Komponen Nilai</label>
					<select class="form-control" name="komponen_nilai_id" id="komponen_nilai_id" required="">
						<?php
							foreach ($kegiatan_nilai as $key => $value) {
								if($value->is_aktif==1)
									echo "<option value='".$value->komponen_nilai_id."'>".$value->judul." : ".$value->keterangan."</option>";
							}
						?>
					</select>
					<input type="hidden" name="nilai_peserta_id" id="nilai_peserta_id" disabled="">
				</div>
				<div class="form-group">
					<a class="label label-primary pull-right" href="<?php echo $this->location('module/event/kegiatan/tahap/'.$kegiatan->kegiatanid.'/'.$peserta->pesertaid); ?>"><i class="fa fa-cog"></i> Tahap Peserta</a>
					<label>Tahap Peserta</label>
					<select class="form-control" name="peserta_tahap_id" id="peserta_tahap_id" required="">
						<?php
							foreach ($peserta_tahap as $key => $value) {
								echo "<option value='".$value->peserta_tahap_id."'>".$value->keterangan." : ".$value->catatan."</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label>Nilai</label>
					<input type="number" class="form-control" name="nilai" id="nilai" required="">
				</div>
				<div class="form-group">
					<label>Uraian</label>
					<textarea type="text" class="form-control" name="uraian" id="uraian" required=""></textarea>
				</div>
				<div class="form-group">
					<label>Di Nilai Oleh</label>
					<input type="text" class="form-control" name="dinilai_oleh" id="dinilai_oleh" required="">
					<input type="hidden" class="form-control" name="karyawan_id" id="karyawan_id">
				</div>
			</div>
			<div class="panel-footer">
				<button type="button" class="btn btn-primary" onclick="submitPesertaNilai()">Simpan</button>
				<button type="button" class="btn btn-default" onclick="cancelPesertaNilai()">Cancel</button>
			</div>
			</form>
		</div>
	</div>
<?php $this->foot(); ?>