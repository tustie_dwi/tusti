$(document).ready(function(){
	$(".tabel-hasil").DataTable();
});
var new_peserta = new_biaya = false;
var elementPeserta, elementBiaya=null;
function editJenisPeserta (jenis_peserta, keterangan,el){
	$("#jenis_peserta").val(jenis_peserta);
	$("#jenis_peserta").attr('disabled','disabled');
	$("#jenis_peserta_hidden").val(jenis_peserta);
	$("#jenis_peserta_hidden").removeAttr('disabled');
	$("#keterangan").val(keterangan);
	new_peserta = false;
	elementPeserta = el;
	$('#tambah_jenis_peserta').modal('show');
}
function deleteJenisPeserta (jenis_peserta,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/master/delete_jenis_peserta",
	  	data: $.param({jenis_peserta:jenis_peserta}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().remove();
	  	},
	 	error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function tambahJenisPeserta (){
	$("#jenis_peserta").val("");
	$("#jenis_peserta").removeAttr('disabled');
	$("#jenis_peserta_hidden").val("");
	$("#jenis_peserta_hidden").attr('disabled','disabled');
	$("#keterangan").val("");
	new_peserta = true;
}

function editJenisBiaya(jenis_biaya, jenis_peserta, keterangan, el){
	$("#jenis_biaya").val(jenis_biaya);
	$("#jenis_biaya").attr('disabled','disabled');
	$("#jenis_biaya_hidden").val(jenis_biaya);
	$("#jenis_biaya_hidden").removeAttr('disabled');
	$("#keterangan_biaya").val(keterangan);
	$.ajax({
	  	url: base_url+"module/event/master/get_jenis_peserta",
	  	success:function(data, textStatus, jqXHR) {
	  		var hasil = JSON.parse(data);
	  		$('#select_peserta').empty();
	  		$.each(hasil, function (index, val){
	  			if(val.jenis_peserta == jenis_peserta)
	  				$('#select_peserta').prepend("<option selected value='"+val.jenis_peserta+"'>"+val.keterangan+"</option>");
	  			else
	  				$('#select_peserta').prepend("<option value='"+val.jenis_peserta+"'>"+val.keterangan+"</option>");
	  		});
	  	}
	});
	new_biaya = false;
	elementBiaya = el;
	$('#tambah_jenis_biaya').modal('show');
}
function deleteJenisBiaya(jenis_biaya,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/master/delete_jenis_biaya",
	  	data: $.param({jenis_biaya:jenis_biaya}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().remove();
	  	},
	 	error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function tambahJenisBiaya(){
	$("#jenis_biaya").val("");
	$("#jenis_biaya").removeAttr('disabled');
	$("#jenis_biaya_hidden").val("");
	$("#jenis_biaya_hidden").attr('disabled','disabled');
	$("#keterangan_biaya").val("");
	$.ajax({
	  	url: base_url+"module/event/master/get_jenis_peserta",
	  	success:function(data, textStatus, jqXHR) {
	  		var hasil = JSON.parse(data);
	  		$('#select_peserta').empty();
	  		$.each(hasil, function (index, val){
	  			$('#select_peserta').prepend("<option value='"+val.jenis_peserta+"'>"+val.keterangan+"</option>");
	  		});
	  	}
	});
	new_biaya = true;
}

$(document).on('click','#submit_jenis_biaya',function(){
	var cekvalid = true;
	$('#form_jenis_biaya :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/master/submit_jenis_biaya",
		  	data: $("#form_jenis_biaya").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		$('#table_jenis_biaya').prepend("<tr>"+
		  			"<td>"+hasil.keterangan_peserta+"</td>"+
		  			"<td>"+hasil.keterangan_biaya+"</td>"+
		  			"<td>"+
						"<a class='label label-danger pull-right' onclick='deleteJenisBiaya(\""+hasil.jenis_biaya+"\",this)'><i class='fa fa-remove'></i> Hapus</a>"+
						"<a class='label label-warning pull-right' "+
						 "onclick='editJenisBiaya(\""+hasil.jenis_biaya+"\",\""+hasil.jenis_peserta+"\",\""+hasil.keterangan_biaya+"\",this)'>"+
						"<i class='fa fa-pencil'></i> Edit</a>"+
					"</td></tr>");
				if(!new_biaya)
					$(elementBiaya).parent().parent().remove();
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
});
$(document).on('click',"#submit_jenis_peserta",function(){
	var cekvalid = true;
	$('#form_jenis_peserta :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/master/submit_jenis_peserta",
		  	data: $("#form_jenis_peserta").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		$("#form_jenis_peserta")[0].reset();
					$('#list_jenis_peserta').prepend("<li class='list-group-item'>"+
					"<a class='badge' onclick='deleteJenisPeserta(\""+hasil.jenis_peserta+"\",this)'><i class='fa fa-remove'></i></a>"+
					"<a class='badge' onclick='editJenisPeserta(\""+hasil.jenis_peserta+"\",\""+hasil.keterangan+"\",this)'><i class='fa fa-pencil'></i></a>"+
					hasil.keterangan+"</li>");
				if(!new_peserta)
					$(elementPeserta).parent().remove();
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
});
