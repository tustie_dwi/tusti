$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
	$(".select").select2();
});
var edit_peserta = false;
var elementPeserta = null;
var jenis_peserta = "alumni";
var keterangan_peserta = "Alumni";
function editPesertaKegiatan(el){
	var value = JSON.parse(Base64.decode($(el).data('param')));
	$('#peserta_id').val(value.peserta_id);
	$('#nama').val(value.nama);
	$('#first_name').val(value.first_name);
	$('#last_name').val(value.last_name);
	$('#gelar_awal').val(value.gelar_awal);
	$('#gelar_akhir').val(value.gelar_akhir);
	$('#email').val(value.email);
	$('#negara').val(value.negara);
	if(value.is_valid==1)
		$('#is_valid').attr("checked","checked");
	else
		$('#is_valid').removeAttr("checked");
	$('#keterangan').val(value.keterangan);
	var t = value.tgl_registrasi.split(/[- :]/);
	$('#tgl_registrasi').val(t[2]+"-"+t[1]+"-"+t[0]);
	
	$('#jenis_pendaftaran').val(value.jenis_pendaftaran);
	$('#metode_pembayaran').val(value.metode_pembayaran);
	$('#total_tagihan').val(value.total_tagihan);
	$('#total_bayar').val(value.total_bayar);
	$('#telp').val(value.telp);
	$('#hp').val(value.hp);
	$('#alamat').val(value.alamat);
	$('#instansi').val(value.instansi);
	$('#alamat_instansi').val(value.alamat_instansi);
	
	$('#peserta_id').removeAttr('disabled');
	elementPeserta = el;
	edit_peserta = true;
}
function deletePesertaaKegiatan(peserta_id, el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_kegiatan_peserta",
	  	data: $.param({peserta_id:peserta_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().parent().parent().parent().parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function simpanPesertaKegiatan(){
	var cekvalid = true;
	$('#form_kegiatan_peserta :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_kegiatan_peserta",
		  	data: $("#form_kegiatan_peserta").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		$('#table_kegiatan_peserta').prepend(
		  			"<tr><td><div class='col-md-9'><b>"+hasil.nama+" "+hasil.gelar_awal+" "+hasil.gelar_akhir+"</b><span class='label label-default'>".hasil.jenis_peserta."</span>&nbsp;<span class='label label-danger'>PIN ".hasil.pin."</span><br>"+
		  			"<small>"+hasil.alamat+"&nbsp;<i class='fa fa-envelope-o'></i> "+
		  			""+hasil.email+"&nbsp;<i class='fa fa-mobile'></i> "+
		  			""+hasil.telp+" <i class='fa fa-map-marker'></i> ".hasil.negara."</small></div>"+
		  			"<div class='col-md-3'><ul class='nav nav-pills' style='margin:0;'><li class='dropdown'><a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>"+
		  				"<ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>"+
		  				"<li><a class='btn-edit-post' href='"+base_url+"module/event/kegiatan/peserta/"+$('#kegiatanid').val()+"/"+hasil.pesertaid+"'><i class='fa fa-user'></i> Tagihan</a></li>"+
		  				"<li><a class='btn-edit-post' href='"+base_url+"module/event/kegiatan/berkas/"+$('#kegiatanid').val()+"/"+hasil.pesertaid+"'><i class='fa fa-file-o'></i> Berkas</a></li>"+
		  				"<li><a class='btn-edit-post' href='"+base_url+"module/event/kegiatan/tahap/"+$('#kegiatanid').val()+"/"+hasil.pesertaid+"'><i class='fa fa-gear'></i> Berkas</a></li>"+
		  				"<li><a class='btn-edit-post' href='"+base_url+"module/event/kegiatan/nilai/"+$('#kegiatanid').val()+"/"+hasil.pesertaid+"'><i class='fa fa-bar-chart'></i> Berkas</a></li>"+
		  				"<li><a class='btn-edit-post' data-param='"+Base64.encode(data)+"' onclick='editPesertaKegiatan(this)'><i class='fa fa-edit'></i> Edit</a></li>"+
		  				"<li><a class='btn-edit-post' onclick='deletePesertaaKegiatan(\""+hasil.peserta_id+"\",this)'><i class='fa fa-trash-o'></i> Delete</a></li>"+
		  			"</ul></li></ul></td></tr>");	  
				$("#form_kegiatan_peserta")[0].reset();
				if(edit_peserta) $(elementPeserta).parent().parent().parent().parent().parent().parent().remove();
				$('#peserta_id').val("");
				$('#peserta_id').attr('disabled','disabled');
				edit_peserta = false;
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function cancelPesertaKegiatan(){
	$("#form_kegiatan_peserta")[0].reset();
	$('#peserta_id').val("");
	$('#peserta_id').attr('disabled','disabled');
	edit_peserta = false;
}
function yeah(a,b){
	jenis_peserta = b;
	keterangan_peserta = a;
}
function submit_jenis_peserta(){
	console.log($('#jenis_peserta :selected').val());
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/submit_jenis_peserta_kegiatan",
	  	data: $("#form_jenis_peserta").serialize(),
	  	success:function(data, textStatus, jqXHR) {
			$("#form_jenis_peserta")[0].reset();
			$('#jenis_peserta').append("<span class='label label-default' style='margin:0px 3px'>"+keterangan_peserta+" <a href='' style='opacity:0.6;color:white;' onclick='deleteJenisPeserta(\""+jenis_peserta+"\",this)'>X</a></span>");
			$('#jenis_peserta_peserta').append("<option id='"+jenis_peserta+"' value='"+jenis_peserta+"'>"+keterangan_peserta+"</option>");
	  	},
	 	error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function deleteJenisPeserta(jenis_peserta,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_kegiatan_jenis_peserta",
	  	data: $.param({kegiatan_id:$('#kegiatan_id').val(), jenis_peserta:jenis_peserta}),
	  	success:function(data, textStatus, jqXHR) {
			$('#'+jenis_peserta).remove();
			$(el).parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
