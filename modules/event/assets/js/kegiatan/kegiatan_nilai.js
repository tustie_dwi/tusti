$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
});
var edit_nilai = false;
var cur_nilai = null;
function cancelNilai(){
	$('#form_nilai').slideUp( "fast",function (){
		$('#list-nilai').removeClass("col-md-7");		
	});
	$('#form_nilai_kegiatan')[0].reset();
	edit_nilai = false;
}
function tambahSubNilai(parent_id){
	$('#list-nilai').addClass("col-md-7");
	$('#form_nilai').slideDown( "fast" );
	
	$('#parent_id').val(parent_id);
	
	$('#komponen_nilai_id').attr("disabled","disabled");
	$('#urut').attr("disabled","disabled");
}
function editSubNilai(urut, komponen_nilai_id, judul, keterangan, is_aktif, parent_id, el){
	$('#list-nilai').addClass("col-md-7");
	$('#form_nilai').slideDown( "fast" );
	
	$('#parent_id').val(parent_id);
	$('#judul').val(judul);
	$('#keterangan').val(keterangan);
	if(is_aktif==0)
		$('#is_aktif').removeAttr("checked");
	else
		$('#is_aktif').attr("checked","");
	$('#komponen_nilai_id').val(komponen_nilai_id);
	$('#komponen_nilai_id').removeAttr("disabled");
	$('#urut').val(urut);
	$('#urut').removeAttr("disabled");
	cur_nilai = el;
	edit_nilai = true;
}
function deleteSubNilai(komponen_nilai_id,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_komponen_nilai_kegiatan",
	  	data: $.param({komponen_nilai_id:komponen_nilai_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function submitSubNilai(){
	var cekvalid = true;
	$('#form_tahap_kegiatan :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_komponen_nilai_kegiatan",
		  	data: $("#form_nilai_kegiatan").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
				if(edit_nilai){
					$(cur_nilai).parent().find('h4').first().text(hasil.judul);
					$(cur_nilai).parent().find('p').first().text(hasil.keterangan);
					$(cur_nilai).parent().find('a.edit').replaceWith("<a class='btn-default btn-xs edit' onclick='editSubNilai(\""+hasil.urut+"\",\""+hasil.komponen_nilai_id+"\",\""+hasil.judul+"\",\""+hasil.keterangan+"\",\""+hasil.is_aktif+"\",\""+hasil.parent_id+"\", this)'><i class='fa fa-pencil'></i></a>");
				}
				else {
					$('#parentid_'+hasil.parent_id.replace(/\./g,'_')).append("<div class='list-group-item' style='padding:15px'>"+
						"<a data-toggle='collapse' href='#parentid_"+hasil.urut.replace(/\./g,'_')+"'><h4 style=''>"+hasil.judul+"</h4></a>"+
						"<p>"+hasil.keterangan+"</p>"+
						"<a class='btn-default btn-xs' onclick='tambahSubNilai(\""+hasil.urut+"\")'><i class='fa fa-plus'></i></a>"+
						"<a class='btn-default btn-xs edit' onclick='editSubNilai(\""+hasil.urut+"\",\""+hasil.komponen_nilai_id+"\",\""+hasil.judul+"\",\""+hasil.keterangan+"\",\""+hasil.is_aktif+"\",\""+hasil.parent_id+"\", this)'><i class='fa fa-pencil'></i></a>"+
						"<a class='btn-default btn-xs' onclick='deleteSubNilai(\""+hasil.komponen_nilai_id+"\", this)'><i class='fa fa-remove'></i></a>"+
						"<div class='list-group collapse in' style='margin:0px' id='parentid_"+hasil.urut.replace(/\./g,'_')+"'></div></div>"
					);
				}
		  		cancelNilai();
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
