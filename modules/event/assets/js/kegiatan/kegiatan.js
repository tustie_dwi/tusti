$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
	$(".select2").select2();
});
var edit_kegiatan = false;
var elementKegiatan = null
function editKegiatan(el){
	var value = JSON.parse(Base64.decode($(el).data('param')));
	$('#nama').val(value.nama);
	$('#lokasi').val(value.lokasi);
	$('#jenis_kegiatan').select2('val',value.jenis_kegiatan);
	$('#unit_id').select2('val',value.unit_id);
	$('#kode_kegiatan').val(value.kode_kegiatan);
	var t = value.tgl_mulai.split(/[- :]/);
	$('#tgl_mulai').val(t[2]+"-"+t[1]+"-"+t[0]);
	var t = value.tgl_selesai.split(/[- :]/);
	$('#tgl_selesai').val(t[2]+"-"+t[1]+"-"+t[0]);
	$('#kegiatan_id').val(value.kegiatan_id);
	$('#host_name').val(value.host_name);
	$('#parent_id').select2('val',value.parent_id);
	$('#keterangan').val(value.keterangan);
	if(value.is_aktif==1)
		$('#is_aktif').attr("checked","checked");
	else
		$('#is_aktif').removeAttr("checked");
		
	if(value.is_aktif==1)
		$('#is_aktif_host_name').attr("checked","checked");
	else
		$('#is_aktif_host_name').removeAttr("checked");
	$('#kegiatan_id').removeAttr('disabled');
	$('#logo').val(value.logo);
	edit_kegiatan = true;
	elementKegiatan = el;
}
function hapusKegiatan(kegiatan_id,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_kegiatan",
	  	data: $.param({kegiatan_id:kegiatan_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().parent().parent().parent().parent().remove();
	  	},
	 	error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function simpanKegiatan(){
	var cekvalid = true;
	$('#form_kegiatan :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_kegiatan",
		  	data: $("#form_kegiatan").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		$('#table_kegiatan').prepend("<tr>"+
		  			"<td>"+hasil.nama+"</td>"+
		  			"<td>"+hasil.lokasi+"</td>"+
		  			"<td>"+hasil.tgl_mulai+"</td>"+
		  			"<td>"+hasil.tgl_selesai+"</td>"+
		  			"<td style='min-width:180px'>"+
						"<a class='label label-success pull-right' href='"+base_url+"module/event/kegiatan/peserta/"+hasil.kegiatanid+"'><i class='fa fa-gear'></i> Peserta</a>"+
						"<a class='label label-primary pull-right' href='"+base_url+"module/event/kegiatan/biaya/"+hasil.kegiatanid+"'><i class='fa fa-gear'></i> Biaya</a>"+
						"<a class='label label-info pull-right' href='"+base_url+"module/event/kegiatan/tahap/"+hasil.kegiatanid+"'><i class='fa fa-gear'></i> Tahap</a><br>"+
						"<a class='label label-warning pull-right' onclick='editKegiatan(\""+hasil.kegiatan_id+"\",\""+hasil.nama+"\",\""+hasil.lokasi+"\",\""+hasil.tgl_mulai+"\",\""+hasil.tgl_selesai+"\",\""+hasil.is_aktif+"\",\""+hasil.jenis_kegiatan+"\",\""+hasil.unit_id+"\",\""+hasil.kode_kegiatan+"\",\""+hasil.logo+"\", \""+hasil.host_name+"\",\""+hasil.is_aktif_host_name+"\",\""+hasil.keterangan+"\",\""+hasil.parent_id+"\",this)'><i class='fa fa-pencil'></i> edit</a>"+
						"<a class='label label-danger pull-right' onclick='hapusKegiatan(\""+hasil.kegiatan_id+"\",el)'><i class='fa fa-remove'></i> delete</a>"+
					"</td></tr>");
					$('#nama').val("");
					$('#lokasi').val("");
					$('#keterangan').val("");
					$('#tgl_mulai').val("");
					$('#tgl_selesai').val("");
					$('#kegiatan_id').attr('disabled','disabled');
					$('#kegiatan_id').val("");
					$('#is_aktif').removeAttr('checked');
					if(edit_kegiatan)
						$(elementKegiatan).parent().parent().parent().parent().parent().parent().remove();
					edit_kegiatan = false;
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function cancelKegiatan (){
	$("#form_kegiatan")[0].reset();
	
	$('#kegiatan_id').attr('disabled','disabled');
	$('#kegiatan_id').val("");
	
	$('#is_aktif').removeAttr('checked');
	$('#is_aktif_host_name').removeAttr("checked");
	edit_kegiatan = false;
}
