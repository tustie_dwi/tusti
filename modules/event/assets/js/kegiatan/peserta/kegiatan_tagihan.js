$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
});
var edit_tagihan = false;
var elementTagihan = null;
function cancel_tagihan_peserta(){
	$("#form_tagihan_peserta")[0].reset();
	$('#tagihan_id').val("");
	$('#tagihan_id').attr('disabled','disabled');
	$('#is_bayar').removeAttr('checked');
	edit_tagihan = false;
}
function submit_tagihan_peserta(){
	var cekvalid = true;
	$('#form_tagihan_peserta :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_tagihan_peserta",
		  	data: $("#form_tagihan_peserta").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		$('#table_tagihan_peserta').prepend(
		  			"<tr><td>"+hasil.nama_biaya+"</td>"+
		  			"<td>"+hasil.inf_jumlah+"</td>"+
		  			"<td>"+hasil.harga+"</td>"+
		  			"<td>"+hasil.total+"</td>"+
		  			"<td>"+
		  				"<a class='label label-warning' onclick='editTagihanPeserta(\""+hasil.tagihan_id+"\",\""+hasil.komponen_id+"\",\""+hasil.inf_jumlah+"\",\""+hasil.is_bayar+"\",\""+hasil.tgl_bayar+"\",this)' > <i class='fa fa-pencil'></i> Rubah</a>"+
						" <a class='label label-danger' onclick='deleteTagihanPeserta(\""+hasil.tagihan_id+"\",this)' > <i class='fa fa-remove'></i> Hapus</a>"+
		  			"</td>"+
				"</tr>");	  
				$("#form_tagihan_peserta")[0].reset();
				if(edit_tagihan) $(elementTagihan).parent().parent().remove();
				$('#tagihan_id').val("");
				$('#tagihan_id').attr('disabled','disabled');
				$('#is_bayar').removeAttr('checked');
				edit_tagihan = false;
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function editTagihanPeserta(tagihan_id, komponen_id, inf_jumlah, is_bayar, tgl_bayar, el){
	$('#tagihan_id').val(tagihan_id);
	$('#tagihan_id').removeAttr('disabled');
	$('#komponen_id').val(komponen_id);
	$('#inf_jumlah').val(inf_jumlah);
	if(is_bayar==1)
		$('#is_bayar').attr("checked","");
	else
		$('#is_bayar').removeAttr("checked");
	var t = tgl_bayar.split(/[- :]/);
	$('#tgl_bayar').val(t[2]+"-"+t[1]+"-"+t[0]);
	elementTagihan = el;
	edit_tagihan = true;
}
function deleteTagihanPeserta(tagihan_id, el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_tagihan_peserta",
	  	data: $.param({tagihan_id:tagihan_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
