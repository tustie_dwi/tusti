$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
	$("#dinilai_oleh").autocomplete({
		source:base_url+"module/event/kegiatan/penilai",
		minLength: 0,
		select : function (event, ui) {
			$('#karyawan_id').val(ui.item.karyawan_id);
		}
	});
});
edit_nilai= false;
elementNilai = null;
function cancelPesertaNilai(){
	$("#form_peserta_nilai")[0].reset();
	
	$('#nilai_peserta_id').val("");
	$('#nilai_peserta_id').attr('disabled','disabled')
	edit_nilai = false;
}
function submitPesertaNilai(){
	var cekvalid = true;
	$('#form_peserta_nilai :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_nilai_peserta",
		  	data: $("#form_peserta_nilai").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
				$('#table_nilai_peserta').prepend("<tr><td>"+
					"<h5>"+hasil.keteranganTahap+"</h5>"+
					"<p>"+hasil.catatan+"</p>"+
					"</td><td>"+
						"<h5>"+hasil.judul+"</h5>"+
						"<p>"+hasil.keteranganKomponenNilai+"</p></td>"+
					"<td>"+hasil.nilai+"</td>"+
					"<td>"+
						"<a class='label label-warning' onclick='editPesertaNilai(\""+hasil.nilai_peserta_id+"\",\""+hasil.komponen_nilai_id+"\",\""+hasil.peserta_tahap_id+"\",\""+hasil.nilai+"\",\""+hasil.uraian+"\",\""+hasil.dinilai_oleh+"\",\""+hasil.karyawan_id+"\",this)'><i class='fa fa-pencil'></i> Rubah</a>"+
						"<a class='label label-danger' onclick='deletePesertaNilai(\""+hasil.nilai_peserta_id+"\",this)'><i class='fa fa-remove'></i> Delete</a>"+
					"</td>"+
					"</td></tr>");
				if(edit_nilai) $(elementNilai).parent().parent().remove();
				cancelPesertaNilai();
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function editPesertaNilai(nilai_peserta_id, komponen_nilai_id, peserta_tahap_id, nilai, uraian, dinilai_oleh, karyawan_id, el){
	$("#nilai_peserta_id").val(nilai_peserta_id);
	$("#nilai_peserta_id").removeAttr("disabled");
	
	$("#komponen_nilai_id").val(komponen_nilai_id);
	$("#peserta_tahap_id").val(peserta_tahap_id);
	$("#nilai").val(nilai);
	$("#uraian").val(uraian);
	$("#dinilai_oleh").val(dinilai_oleh);
	$("#karyawan_id").val(karyawan_id);
	
	edit_nilai = true;
	elementNilai = el;
}
function deletePesertaNilai(nilai_peserta_id,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_nilai_peserta",
	  	data: $.param({nilai_peserta_id:nilai_peserta_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}

