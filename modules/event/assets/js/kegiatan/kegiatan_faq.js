$(document).ready(function(){
	CKEDITOR.replace( 'comment' );
});
var edit_faq = false; 
var is_answer = false;
var cur_element = null;
function cancelfaq(){
	$('#form_faq').slideUp( "fast",function (){
		$('#list-faq').removeClass("col-md-7");		
	});
	$('#form_faq_kegiatan')[0].reset();
	CKEDITOR.instances['comment'].setData("");
	edit_faq = false;
	is_answer = false;
}
function tambahfaq(){
	$('#list-faq').addClass("col-md-7");
	$('#form_faq').slideDown( "fast" );
	
	$('#form_faq_title').text("Tambah Pertanyaan F.A.Q");
	
	$('#comment_id').val("");
	$('#comment_id').attr("disabled","disabled");
	$('#parent_id').val("");
	$('#parent_id').attr("disabled","disabled");
	edit_faq = false;
	is_answer = false;
}
function replayfaq(parent_id, el){
	$('#list-faq').addClass("col-md-7");
	$('#form_faq').slideDown( "fast" );
	
	$('#form_faq_title').text("Jawab Pertanyaan F.A.Q");
	
	$('#comment_id').val("");
	$('#comment_id').attr("disabled","disabled");
	$('#parent_id').val(parent_id);
	$('#parent_id').removeAttr("disabled");
	edit_faq = false;
	is_answer = true;
	cur_element = el;
}
function editfaq(data, el){
	$('#list-faq').addClass("col-md-7");
	$('#form_faq').slideDown( "fast" );
	
	var parsing = JSON.parse(Base64.decode(data));
	$('#form_faq_title').text("Edit F.A.Q");
	$('#title').val(parsing.title);
	$('#comment').val(parsing.comment);
	if(parsing.is_approve==0)
		$('#is_approve').removeAttr("checked");
	else
		$('#is_approve').attr("checked","");
	CKEDITOR.instances['comment'].setData(parsing.comment);
	
	$('#comment_id').val(parsing.comment_id);
	$('#comment_id').removeAttr("disabled");
	$('#parent_id').val(parsing.parent_id);
	$('#parent_id').removeAttr("disabled");
	
	edit_faq = true;
	cur_element = el;
}
function submitFaq(){
	var cekvalid = true;
	$('#form_faq_kegiatan :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		var param = {
			'title':$('#title').val(),
	  		'kegiatan_id':$('#kegiatan_id').val(),
	  		'parent_id':$('#parent_id').val(),
	  		'comment': CKEDITOR.instances['comment'].getData()
		}
		if ($('#is_approve').is(":checked")) param.is_approve = true;
		if(!$('#comment_id').prop('disabled')) param.comment_id = $('#comment_id').val();
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_faq",
		  	data: $.param(param),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		var template = "<div class='list-group-item'>"+
					"<small class='pull-right'>"+hasil.comment_post+"</small>"+
					"<h4 class='list-group-item-heading'>"+hasil.title+"<small> by "+hasil.user_name+"</small></h4>"+
					"<small class='pull-right'>"+((hasil.is_approve==1)?'disetujui':'tidak disetujui')+"</small>"+
					"<p class='list-group-item-text'>"+ hasil.comment +"<br/>"+
						"<button type='button' class='btn btn-warning btn-xs edit' onclick='editfaq(\""+Base64.encode(data)+"\", this)'><i class='fa fa-edit'></i> edit</button> "+
						"<button type='button' class='btn btn-danger btn-xs delete' onclick='deletefaq(\""+hasil.comment_id+"\", this)'><i class='fa fa-trash-o'></i> hapus</button> "+
					"</p>"+
					"</div>";
		  		if(edit_faq){//edit pertanyaan
		  			var root = $(cur_element).parent().clone();
		  			var templates = $(cur_element).parent();
		  			templates.before(template);
		  			if(root.find('.jawab').length>0){
		  				templates.prev().find('.edit').first().before("<button type='button' class='btn btn-primary btn-xs jawab' onclick='replayfaq(\""+hasil.comment_id+"\",this)'><i class='fa fa-reply'></i> jawab</button> ");
		  				templates.prev().append(root.find('.list-group').clone());
		  			}
		  			templates.remove();
		  		}else{
		  			if(is_answer){//tambah jawaban
		  				$(cur_element).parent().find('.list-group').prepend(template);
		  			}else{//tambah pertanyaan
		  				var a = $('#list-group-faq').prepend(template);
		  				a.find('.edit').first().before("<button type='button' class='btn btn-primary btn-xs jawab' onclick='replayfaq(\""+hasil.comment_id+"\",this)'><i class='fa fa-reply'></i> jawab</button> ");
		  				a.find('.list-group-item').first().append("<div class='list-group' style='margin:0px; padding-left:60px; padding-top:10px;margin-right: -16px; margin-bottom: -11px;  margin-top: -7px;'></div>");
		  			}
		  		}
		  		cancelfaq();
		  	},
		  	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function deletefaq(comment_id,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_faq",
	  	data: $.param({comment_id:comment_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
