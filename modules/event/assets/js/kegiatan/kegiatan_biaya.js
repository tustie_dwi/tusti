$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
});
var edit_biaya = false;
var elementBiaya = null
function editBiayaKegiatan(el){
	var value = JSON.parse(Base64.decode($(el).data('param')));
	$('#nama_biaya').val(value.nama_biaya);
	$('#harga').val(value.harga);
	var t = value.tgl_mulai.split(/[- :]/);
	$('#tgl_mulai').val(t[2]+"-"+t[1]+"-"+t[0]);
	var t = value.tgl_selesai.split(/[- :]/);
	$('#tgl_selesai').val(t[2]+"-"+t[1]+"-"+t[0]);
	if(is_aktif==1)
		$('#is_aktif').attr("checked","");
	else
		$('#is_aktif').removeAttr("checked");
	$('#jenis_biaya').val(value.jenis_biaya);
	$('#satuan').val(value.satuan);
	$('#keterangan').val(value.keterangan);
	$('#komponen_id').val(value.komponen_id);
	$('#komponen_id').removeAttr('disabled');
	edit_biaya = true;
	elementBiaya = el;
}
function hapusBiayaKegiatan(komponen_id,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_kegiatan_biaya",
	  	data: $.param({komponen_id:komponen_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function simpanBiayaKegiatan(){
	var cekvalid = true;
	$('#form_kegiatan_biaya :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_kegiatan_biaya",
		  	data: $("#form_kegiatan_biaya").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		$('#table_kegiatan_biaya').prepend(
		  			"<tr><td>"+hasil.nama_biaya+"</td>"+
		  			"<td>"+hasil.keteranganBiaya+"</td>"+
		  			"<td>"+hasil.harga+"</td>"+
		  			"<td>"+hasil.tgl_mulai+"</td>"+
		  			"<td>"+hasil.tgl_selesai+"</td>"+
		  			"<td style='min-width:120px'>"+
		  				"<a class='label label-warning' data-param='"+Base64.encode(data)+"' onclick='editBiayaKegiatan(this)'><i class='fa fa-pencil'></i> Rubah</a> "+
		  				"<a class='label label-danger'  onclick='hapusBiayaKegiatan(\""+hasil.komponen_id+"\",this)'><i class='fa fa-remove'></i> Hapus</a>"+
		  			"</td>"+
				"</tr>");	  
				$("#form_kegiatan_biaya")[0].reset();
				if(edit_biaya)
					$(elementBiaya).parent().parent().remove();
				edit_biaya = false;
				$('#komponen_id').attr('disabled','disabled');
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function cancelBiayaKegiatan(){
	$("#form_kegiatan_biaya")[0].reset();
	edit_biaya = false;
	$('#komponen_id').attr('disabled','disabled');
}
