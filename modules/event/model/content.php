<?php
class model_content extends model {
	
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	/*
	 * SETTING===================================================
	 * */
	 
	 function get_event($unit=NULL){
		$sql = "SELECT
					db_ptiik_event.tbl_kegiatan.kegiatan_id,
					db_ptiik_event.tbl_kegiatan.nama,
					db_ptiik_event.tbl_kegiatan.jenis_kegiatan,
					db_ptiik_event.tbl_kegiatan.unit_id
				FROM
					db_ptiik_event.tbl_kegiatan
				WHERE 1 ";
		if($unit) $sql.= " AND db_ptiik_event.tbl_kegiatan.unit_id='$unit' ";
		$sql.=" ORDER BY
				db_ptiik_event.tbl_kegiatan.tgl_selesai DESC ";
		return $this->db->query($sql);
	 }
	
	function read_content_category($param=NULL,$cat=NULL){
		$sql = "SELECT * FROM db_ptiik_coms.coms_content_category WHERE 1";
		
		if($cat){
			$sql .= " AND category = '".$cat."' ";
		}
		
		if($param=='content'){
			$sql .= " AND category = 'content' ";
		}
		
		if($param=='read-by-category'){
			$sql .= " GROUP BY category ";
		}

		// echo $sql;
		return $this->db->query($sql);
	}
	
	function update_content_category($content_category=NULL,$note=NULL,$category=NULL,$content_cparam=NULL,$category_param=NULL){
		$sql = "UPDATE db_ptiik_coms.coms_content_category 
				SET 
				content_category = '".$content_category."',
				note = '".$note."',
				category = '".$category."' 
				WHERE content_category = '".$content_cparam."' AND category = '".$category_param."' 
			   ";
		return $this->db->query($sql);
	}
	
	function replace_content_category($datanya){
		return $this->db->replace('db_ptiik_coms`.`coms_content_category',$datanya);
	}
	
	function replace_content_tag($datanya){
		return $this->db->replace('db_ptiik_coms`.`coms_content_tag',$datanya);
	}
	
	/*
	 * END SETTING========================== START COMS_CONTENT
	 * */
	
	function read_content($id=NULL,$fakultas=NULL,$cabang=NULL,$unit=NULL,$category=NULL,$parent=NULL,$lang=NULL,$lang_type=NULL, $event=NULL){
		$sql = "SELECT DISTINCT 
				mid(md5(coms_content.content_id),6,6) as content_id,
				coms_content.content_id as contentid,				
				coms_content.content_category,
				coms_content_category.category,
				coms_content.menu_order,
				coms_content.unit_id,
				coms_content.fakultas_id,
				coms_content.cabang_id,
				coms_content.event_id,
				coms_content.content_title,
				coms_content.content_excerpt,
				coms_content.content,
				SUBSTRING(coms_content.content, 1,100) AS content_content,
				coms_content.content_link,
				coms_content.content_data,
				coms_content.content_status,
				coms_content.content_lang,
				coms_content.content_hit,
				coms_content.content_author,
				coms_content.content_upload,
				coms_content.content_comment,
				coms_content.content_thumb_img,
				coms_content.is_sticky,
				coms_content.user_id,
				coms_content.last_update,				
				mid(md5(coms_content.parent_id),6,6) as parent_id,
				coms_content.parent_id as parentid
				FROM db_ptiik_coms.coms_content
				INNER JOIN db_ptiik_coms.coms_content_category ON coms_content_category.content_category = coms_content.content_category 
				WHERE 1";
		
		if($id){
			if($parent=='child'){
				$sql .= " AND (coms_content.parent_id!= 0 AND (mid(md5(coms_content.parent_id),6,6) = '".$id."' OR coms_content.parent_id='$id') ) ";
			}else{
				$sql .= " AND (mid(md5(coms_content.content_id),6,6) = '".$id."' OR coms_content.content_id='$id') ";
			}
		}
		
		if($fakultas){
			$sql .= " AND coms_content.fakultas_id = '".$fakultas."' ";
		}
		
		if($cabang){
			$sql .= " AND coms_content.cabang_id = '".$cabang."' ";
		}
		
		if($unit=='0'){
			$sql .= " AND coms_content.unit_id = '0' ";
		}elseif($unit!=''){
			$sql .= " AND coms_content.unit_id = '".$unit."' ";
		}
		
		if($category){
			$sql .= " AND coms_content.content_category = '".$category."' ";
		}
		
		if($event){
			$sql .= " AND coms_content.event_id = '".$event."' ";
		}
		
		if($lang){
			if($lang_type=='IN'){
				$sql .= " AND coms_content.content_lang IN (".$lang.") ";
			}else{
				$sql .= " AND coms_content.content_lang = '".$lang."' ";
			}
			
		}
		
		if($parent=='parent'){
			$sql .= " AND coms_content.parent_id = 0 ";
		}
		if($parent=='child'){
			$sql .= " AND coms_content.parent_id != 0 ";
		}
		
		// echo $sql."<br>";
		if($id && $parent!='child') return $this->db->getRow($sql);
		else	return $this->db->query($sql);
	}
	
	function read_content_tag($id,$param){
		$sql = "SELECT 
				coms_content_tag.content_id,
				coms_content_tag.tag
				FROM db_ptiik_coms.coms_content_tag 
				WHERE 1
			   ";
		
		if($id){
			if($param=='md5'){
				$sql .= " AND mid(md5(coms_content_tag.content_id),6,6) = '".$id."' ";
			}else{
				$sql .= " AND coms_content_tag.content_id = '".$id."' ";
			}
		}
		
		return $this->db->query($sql);
	}
	
	function delete_tag($id=NULL, $tag=NULL,$param=NULL){
		$sql = "DELETE FROM db_ptiik_coms.coms_content_tag WHERE 1 ";
		
		if($id){
			if($param=='md5'){
				$sql .= " AND mid(md5(coms_content_tag.content_id),6,6) = '".$id."' ";
			}else{
				$sql .= " AND coms_content_tag.content_id= '".$id."' ";
			}
		}
		
		if($tag){
			$sql .= " AND coms_content_tag.tag = '".$tag."' ";
		}
		
		return $this->db->query($sql);
	}
	
	function delete_content($id){
		$sql_child = "DELETE FROM db_ptiik_coms.coms_content WHERE mid(md5(coms_content.parent_id),6,6) = '".$id."'";
		$this->db->query($sql_child);
		$sql_tag = "DELETE FROM db_ptiik_coms.coms_content_tag WHERE mid(md5(coms_content_tag.content_id),6,6) = '".$id."'";
		$this->db->query($sql_tag);
		$sql_file_attach = "DELETE FROM db_ptiik_coms.coms_content_upload WHERE mid(md5(coms_content_upload.content_id),6,6) = '".$id."' AND coms_content_upload.kategori = 'file' ";
		$this->db->query($sql_file_attach);
		$sql = "DELETE FROM db_ptiik_coms.coms_content WHERE mid(md5(coms_content.content_id),6,6) = '".$id."'";
		return $this->db->query($sql);
	}
	
	function get_content_id($id, $byparent){
		$sql = "SELECT 
				coms_content.content_id,
				mid(md5(coms_content.content_id),6,6) as content_id_md5
				FROM db_ptiik_coms.coms_content 
				WHERE 1";
		
		if($id){
			if($byparent=='parent'){
				$sql .= " AND mid(md5(coms_content.parent_id),6,6) = '".$id."' ";
				$dt = $this->db->query($sql);
			}else{
				$sql .= " AND mid(md5(coms_content.content_id),6,6) = '".$id."' ";
				$dt = $this->db->getRow( $sql );
			}
			
		}
		return $dt;
	}
	
	function get_file_parent_id($id, $byparent){
		$sql = "SELECT 
				coms_file.file_id,
				mid(md5(coms_file.file_id),6,6) as content_id_md5
				FROM db_ptiik_coms.coms_file 
				WHERE 1";
		
		if($id){
			if($byparent=='parent'){
				$sql .= " AND mid(md5(coms_file.parent_id),6,6) = '".$id."' ";
				$dt = $this->db->query($sql);
			}else{
				$sql .= " AND mid(md5(coms_file.file_id),6,6) = '".$id."' ";
				$dt = $this->db->getRow( $sql );
			}
			
		}
		
		return $dt;
	}
	
	function get_content_reg_number($lang=NULL){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(content_id,6) AS 
				unsigned)), 0) + 1 AS unsigned)),6)) as `data` 
				FROM db_ptiik_coms.coms_content WHERE left(content_id,6) = '".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_content($datanya){
		return $this->db->replace('db_ptiik_coms`.`coms_content',$datanya);
	}
	
	/*
	 * END COMS_CONTENT========================== START COMS_CONTENT_UPLOAD
	 * */
	function get_content_upload($id=NULL,$param=NULL,$kategori=NULL){
		$sql = "SELECT 
				mid(md5(coms_content_upload.upload_id),6,6) as upload_id,
				coms_content_upload.upload_id as uploadid,
				
				mid(md5(coms_content_upload.content_id),6,6) as content_id,
				coms_content_upload.content_id as contentid,
				
				mid(md5(coms_content_upload.file_id),6,6) as file_id,
				coms_content_upload.file_id as fileid,
				
				coms_content_upload.title,
				coms_content_upload.note,
				coms_content_upload.file_loc,
				coms_content_upload.file_type,
				coms_content_upload.kategori
				
				FROM db_ptiik_coms.coms_content_upload
				WHERE 1";
		
		if($id){
			if($param=='content'){
				$sql .= " AND mid(md5(coms_content_upload.content_id),6,6) = '".$id."' ";
			}
		}
		
		if($kategori){
			$sql .= " AND coms_content_upload.kategori = '".$kategori."' ";
		}
		
		return $this->db->query($sql);
	}
	
	function delete_content_upload($id){
		$sql = "DELETE FROM db_ptiik_coms.coms_content_upload WHERE mid(md5(coms_content_upload.upload_id),6,6) = '".$id."'";
		return $this->db->query($sql);
	}
	
	function get_content_upload_reg_number(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(upload_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_coms.coms_content_upload"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_content_upload($datanya){
		return $this->db->replace('db_ptiik_coms`.`coms_content_upload',$datanya);
	}
	
	/*
	 * END COMS_CONTENT_UPLOAD========================== START COMS_FILE
	 * */
	function read_file($id=NULL,$fakultas=NULL,$cabang=NULL,$unit=NULL,$category=NULL,$lang=NULL, $parent=NULL, $event=NULL){
		$sql = "SELECT DISTINCT 
				mid(md5(coms_file.file_id),6,6) as file_id,
				mid(md5(coms_file.file_id),6,6) as content_id,
				coms_file.file_id as contentid,
				coms_file.file_id as fileid,
				coms_file.file_lang,
				coms_file.content_category,
				coms_content_category.category,
				coms_file.unit_id,
				coms_file.fakultas_id,
				coms_file.cabang_id,
				coms_file.event_id,
				coms_file.file_title,
				coms_file.file_title as content_title,
				coms_file.file_note,
				SUBSTRING(coms_file.file_note, 1,100) AS file_file_note,
				coms_file.file_data,
				coms_file.file_group,
				coms_file.file_type,
				coms_file.file_name,
				coms_file.file_size,
				coms_file.file_loc,
				coms_file.is_publish,
				coms_file.user_id,
				coms_file.last_update,
				mid(md5(coms_file.parent_id),6,6) as parent_id,
				coms_file.parent_id as parentid
				FROM db_ptiik_coms.coms_file
				LEFT JOIN db_ptiik_coms.coms_content_category ON coms_content_category.content_category = coms_file.content_category 
				WHERE 1";
		
		if($id){
			$sql .= " AND mid(md5(coms_file.file_id),6,6) = '".$id."' ";
		}
		
		if($fakultas){
			$sql .= " AND coms_file.fakultas_id = '".$fakultas."' ";
		}
		
		if($cabang){
			$sql .= " AND coms_file.cabang_id = '".$cabang."' ";
		}
		
		if($unit){
			$sql .= " AND coms_file.unit_id = '".$unit."' ";
		}
		
		if($event){
			$sql .= " AND coms_file.event_id = '".$event."' ";
		}
		
		if($lang){
			//$sql .= " AND (coms_file.file_lang IN (".$lang.")  OR coms_file.file_lang='$lang' )";
			$sql.= " AND coms_file.file_lang='$lang' ";
		}
		
		if($category){
			$sql .= " AND coms_file.content_category = '".$category."' ";
		}
		if($parent){
			$sql .= " AND coms_file.parent_id = '0' ";
		}
		//echo $sql;	
		if($id) return $this->db->getRow($sql);
		else return $this->db->query($sql);
	}
	
	function delete_file($id){
		$sql = "DELETE FROM db_ptiik_coms.coms_file WHERE mid(md5(coms_file.file_id),6,6) = '".$id."'";
		return $this->db->query($sql);
	}
	
	function get_file_reg_number(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,6) AS 
				unsigned)), 0) + 1 AS unsigned)),6)) as `data` 
				FROM db_ptiik_coms.coms_file WHERE left(file_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_file_upload($datanya){
		return $this->db->replace('db_ptiik_coms`.`coms_file',$datanya);
	}
	
}
?>