<?php
class event_master extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		
	}
	
	function biaya($jenisbiaya=null){
		$mmaster = new model_master();
		$data['jenis_biaya'] = $mmaster->get_jenis_biaya();
		$data['jenis_peserta'] = $mmaster->get_jenis_peserta();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/master/master.js');
		
		$this->view('master/master_jenis.php',$data);
	}
	function submit_jenis_peserta(){
		$mmaster = new model_master();
		$mmaster->simpan_jenis_peserta($_POST);
		echo json_encode($_POST);
	}
	function delete_jenis_peserta(){
		$mmaster = new model_master();
		$mmaster->hapus_jenis_peserta($_POST['jenis_peserta']);
	}
	function get_jenis_peserta(){
		$mmaster = new model_master();
		$mmaster->get_jenis_peserta();
		echo json_encode($mmaster->get_jenis_peserta());
	}
	function submit_jenis_biaya(){
		$mmaster = new model_master();
		$mmaster->simpan_jenis_biaya($_POST);
		$hasil = $mmaster->get_jenis_biaya($_POST['jenis_biaya']);
		echo json_encode($hasil);
	}
	function delete_jenis_biaya(){
		$mmaster = new model_master();
		$mmaster->hapus_jenis_biaya($_POST['jenis_biaya']);
	}
	
	
	

}

?>