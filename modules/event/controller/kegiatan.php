<?php
class event_kegiatan extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	//kegiatan
	function index(){
		$mmaster = new model_master();
		$data['kegiatan'] = $mmaster->get_kegiatan();
		$data['jenis_kegiatan'] = $mmaster->get_jenis_kegitan();
		$data['unit_id'] = $mmaster->get_unit_id();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('admin/js/moment.js');
		$this->coms->add_script('admin/js/bootstrap-datetimepicker.js');
		$this->coms->add_script('admin/css/bootstrap-datetimepicker.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->add_script('js/kegiatan/kegiatan.js');
		$this->add_script('js/base64.js');
		
		$this->view('kegiatan/kegiatan.php',$data);
	}
	function submit_kegiatan(){
		$mmaster = new model_master();
		$kegiatan_id = (isset($_POST['kegiatan_id']))?$_POST['kegiatan_id']:$mmaster->get_nextid_kegiatan();
		$data = array (
			"kegiatan_id"=>$kegiatan_id,
			"nama"=>$_POST['nama'],
			"lokasi"=>$_POST['lokasi'],
			"tgl_mulai"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_mulai']),'Y-m-d H:i:s'),
			"tgl_selesai"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_selesai']),'Y-m-d H:i:s'),
			"is_aktif"=>(isset($_POST['is_aktif']))?1:0,
			"jenis_kegiatan"=>$_POST['jenis_kegiatan'],
			"unit_id"=>$_POST['unit_id'],
			"kode_kegiatan"=>$_POST['kode_kegiatan'],
			"parent_id"=>$_POST['parent_id'],
			"keterangan"=>$_POST['keterangan'],
			"logo"=>$_POST['logo'],
			"user_id"=>$this->coms->authenticatedUser->id,
			"last_update"=> date('Y-m-d H:i:s')
		);
		$comsData = $mmaster->get_coms_site_kegiatan($kegiatan_id);
		if(!$comsData){
			$comsData = array(
				'site_id'=>$mmaster->get_nextid_com_site(),
				'host_name'=>$_POST['host_name'],
				'event_id'=>$kegiatan_id,
				'unit_id'=>$_POST['unit_id'],
				"user_id"=>$this->coms->authenticatedUser->id,
				"last_update"=> date('Y-m-d H:i:s'),
				'is_aktif'=>(isset($_POST['is_aktif_host_name']))?1:0,
			);
		} else{
			$comsData = json_decode(json_encode($comsData), true);
			$comsData['host_name'] = $_POST['host_name'];
			$comsData['is_aktif'] = (isset($_POST['is_aktif_host_name']))?1:0;
			$comsData['event_id']= $kegiatan_id;
			$comsData['unit_id']= $_POST['unit_id'];
			$comsData['last_update']= date('Y-m-d H:i:s');
			$comsData['user_id']= $this->coms->authenticatedUser->id;
		}
		//upload file
		$cekicon = 'no error';
		if(isset($_FILES['file'])){
			foreach ($_FILES['file'] as $id => $icon) {
				if($id == 'error'){
					if($icon!=0){
						$cekicon = 'error';
					}
				}
			}
			if($cekicon!='error'){
				foreach ($_FILES['file'] as $id => $icon) {
					$brokeext = $this->get_extension($icon);
					if($id == 'name'){
						$ext 	= $brokeext;
						$file	= $icon;
					}
					if($id == 'type'){
						$file_type = $_FILES['file']['type'];
					}
					if($id == 'size'){
						$file_size = $_FILES['file']['size'];
					}
					if($id == 'tmp_name'){
						$file_tmp_name = $_FILES['file']['tmp_name'];
					}
				}
				
				$upload_dir = 'assets/upload/file/event/kegiatan/'.date('Y'). '-' .date('m').'/';		
				$upload_dir_db = 'upload/file/event/kegiatan/'.date('Y'). '-' .date('m').'/';
				
				$file_loc = $upload_dir_db . $file;
				
				$this->upload_file($upload_dir, $file_tmp_name, $file, $file, $file_size);
				
 				$data['logo']= $file_loc."/".$file;
				$mmaster->simpan_kegiatan($data);
				$mmaster->simpan_coms_site($comsData);
				
				$this->redirect('module/event/kegiatan');
			}
			else {
				$mmaster->simpan_kegiatan($data);
				$mmaster->simpan_coms_site($comsData);
				$this->redirect('module/event/kegiatan');
			}
		}
		$hasil = $mmaster->get_kegiatan($kegiatan_id);
		echo json_encode($hasil);
	}
	function delete_kegiatan(){
		$mmaster = new model_master();
		$mmaster->hapus_kegiatan($_POST['kegiatan_id']);
	}
	function get_nama_kegiatan(){
		$mmaster = new model_master();
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$data = $mmaster->get_nama_kegiatan($str);
		$json_response = json_encode($data);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
	}
	//penilai
	function penilai(){
		$mmaster = new model_master();
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$data = $mmaster->get_pegawai($str);
		$json_response = json_encode($data);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
	}
	//kegiatan biaya
	function biaya($kegiatanid){
		$mmaster = new model_master();
		$data['kegiatan'] = $mmaster->get_kegiatanDet($kegiatanid);
		$data['jenis_biaya'] = $mmaster->get_jenis_biaya();
		$data['biaya_kegiatan'] = $mmaster->get_biaya_komponen($kegiatanid);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('admin/js/moment.js');
		$this->coms->add_script('admin/js/bootstrap-datetimepicker.js');
		$this->coms->add_script('admin/css/bootstrap-datetimepicker.css');
		
		$this->add_script('js/base64.js');
		$this->add_script('js/kegiatan/kegiatan_biaya.js');
		
		$this->view('kegiatan/biaya_kegiatan.php',$data);
	}
	function submit_kegiatan_biaya (){
		$mmaster = new model_master();
		$komponen_id = (isset($_POST['komponen_id']))?$_POST['komponen_id']:$mmaster->get_nextid_biaya_komponen();
		$data = array (
			"komponen_id"=>$komponen_id,
			"kegiatan_id"=>$_POST['kegiatan_id'],
			"jenis_biaya"=>$_POST['jenis_biaya'],
			"nama_biaya"=>$_POST['nama_biaya'],
			"satuan"=>$_POST['satuan'],
			"keterangan"=>$_POST['keterangan'],
			"harga"=>$_POST['harga'],
			"tgl_mulai"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_mulai']),'Y-m-d H:i:s'),
			"tgl_selesai"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_selesai']),'Y-m-d H:i:s'),
			"is_aktif"=>(isset($_POST['is_aktif']))?1:0
		);
		$mmaster->simpan_kegiatan_biaya($data);
		$hasil = $mmaster->get_biaya_komponen(null,$komponen_id);
		echo json_encode($hasil);
	}
	function delete_kegiatan_biaya(){
		$mmaster = new model_master();
		$mmaster->hapus_kegiatan_biaya($_POST['komponen_id']);
	}
	//kegiatan peserta
	function peserta($kegiatanid, $pesertaid=null){
		$mmaster = new model_master();
		$data['kegiatan'] = $mmaster->get_kegiatanDet($kegiatanid);
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('admin/js/moment.js');
		$this->coms->add_script('admin/js/bootstrap-datetimepicker.js');
		$this->coms->add_script('admin/css/bootstrap-datetimepicker.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		if(!$pesertaid){
			$data['peserta_kegiatan'] = $mmaster->get_peserta($kegiatanid);
			$data['jenis_peserta'] = $mmaster->get_jenis_peserta();
			$data['negara'] = $mmaster->get_negara();
			$data['jenis_peserta_kegiatan'] = $mmaster->get_jenis_peserta_kegiatan($kegiatanid);
			
			$this->add_script('js/base64.js');
			$this->add_script('js/kegiatan/kegiatan_peserta.js');
			
			$this->view('kegiatan/peserta_kegiatan.php',$data);
		}
		else{
			$data['peserta'] = $mmaster->get_pesertaDet($pesertaid);
			$data['biaya_tagihan_peserta'] = $mmaster->get_tagihan_peserta($pesertaid);
			$data['biaya_kegiatan'] = $mmaster->get_biaya_komponen($kegiatanid);
			
			$this->add_script('js/kegiatan/peserta/kegiatan_tagihan.js');
			$this->view('kegiatan/peserta/tagihan_peserta.php',$data);
		}
	}
	function submit_jenis_peserta_kegiatan(){
		$mmaster = new model_master();
		$mmaster->simpan_kegiatan_jenis_peserta($_POST);
	}
	function submit_kegiatan_peserta(){
		$mmaster = new model_master();
		$peserta_id = (isset($_POST['peserta_id']))?$_POST['peserta_id']:$mmaster->get_nextid_peserta_kegiatan();
		$data = array (
			"peserta_id"=>$peserta_id,
			"kegiatan_id"=>$_POST['kegiatan_id'],
			"jenis_peserta"=>$_POST['jenis_peserta'],
			"nama"=>$_POST['nama'],
			"first_name"=>$_POST['first_name'],
			"last_name"=>$_POST['last_name'],
			"gelar_awal"=>$_POST['gelar_awal'],
			"gelar_akhir"=>$_POST['gelar_akhir'],
			"email"=>$_POST['email'],
			"negara"=>$_POST['negara'],
			"keterangan"=>$_POST['keterangan'],
			"tgl_registrasi"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_registrasi']),'Y-m-d H:i:s'),
			"jenis_pendaftaran"=>$_POST['jenis_pendaftaran'],
			"metode_pembayaran"=>$_POST['metode_pembayaran'],
			"total_tagihan"=>$_POST['total_tagihan'],
			"total_bayar"=>$_POST['total_bayar'],
			"telp"=>$_POST['telp'],
			"hp"=>$_POST['hp'],
			"alamat"=>$_POST['alamat'],
			"instansi"=>$_POST['instansi'],
			"alamat_instansi"=>$_POST['alamat_instansi'],
			"is_valid"=>(isset($_POST['is_valid']))?1:0
		);
		$mmaster->simpan_kegiatan_peserta($data);
		$hasil = $mmaster->get_peserta(null,$peserta_id);
		echo json_encode($hasil);
	}
	function delete_kegiatan_jenis_peserta(){
		$mmaster = new model_master();
		$mmaster->hapus_kegiatan_jenis_peserta($_POST['kegiatan_id'],$_POST['jenis_peserta']);
	}
	function delete_kegiatan_peserta(){
		$mmaster = new model_master();
		$mmaster->hapus_kegiatan_peserta($_POST['peserta_id']);
	}
	//tagihan peserta
	function submit_tagihan_peserta (){
		$mmaster = new model_master();
		$tagihan_id = (isset($_POST['tagihan_id']))?$_POST['tagihan_id']:$mmaster->get_nextid_peserta_tagihan();
		$data = array (
			"tagihan_id"=>$tagihan_id,
			"peserta_id"=>$_POST['peserta_id'],
			"komponen_id"=>$_POST['komponen_id'],
			"inf_jumlah"=>$_POST['inf_jumlah'],
			"is_bayar"=>(isset($_POST['is_bayar']))?1:0,
			"tgl_bayar"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_bayar']),'Y-m-d H:i:s'),
		);
		$mmaster->simpan_tagihan_peserta($data);
		$hasil = $mmaster->get_tagihan_peserta(null,$tagihan_id);
		echo json_encode($hasil);
	}
	function delete_tagihan_peserta(){
		$mmaster = new model_master();
		$mmaster->hapus_tagihan_peserta($_POST['tagihan_id']);
	}
	//berkas kegiatan
	function berkas($kegiatanid=null, $pesertaid=null){
		if(!$kegiatanid && !$pesertaid)
			$this->redirect('module/event/kegiatan/');
		if(!$pesertaid)
			$this->redirect('module/event/kegiatan/peserta/'.$kegiatanid);
		$mmaster = new model_master();
		$data['kegiatan'] = $mmaster->get_kegiatanDet($kegiatanid);
		$data['peserta'] = $mmaster->get_pesertaDet($pesertaid);
		$data['peserta_upload'] = $mmaster->get_upload_pesertaDet($pesertaid);
		if(!$data['peserta_upload']){}
		else
			foreach ($data['peserta_upload'] as $key => $value) {
				$value->reviews = $mmaster->get_dokumen_review($value->upload_id);
			}
		$data['dokumen_kategori'] = $mmaster->get_dokumen_kategori();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('admin/js/moment.js');
		$this->coms->add_script('admin/js/bootstrap-datetimepicker.js');
		$this->coms->add_script('admin/css/bootstrap-datetimepicker.css');
		
		$this->add_script('js/kegiatan/peserta/kegiatan_upload.js');
		$this->view('kegiatan/peserta/upload_peserta.php',$data);
	}
	function get_kategori_dokumen(){
		$mmaster = new model_master();
		echo json_encode($mmaster->get_dokumen_kategori());
	}
	function submit_kategori_dokumen(){
		$mmaster = new model_master();
		$mmaster->simpan_kategori_dokumen($_POST);
		
		echo json_encode($mmaster->get_dokumen_kategori());
	}
	function delete_kategori_dokumen(){
		$mmaster = new model_master();
		$mmaster->hapus_kategori_dokumen($_POST['kategori_dokumen']);
	}
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	function submit_peserta_upload($kegiatanid=null, $pesertaid=null){
		if(!$kegiatanid && !$pesertaid)
			$this->redirect('module/event/kegiatan/');
		if(!$pesertaid)
			$this->redirect('module/event/kegiatan/peserta/'.$kegiatanid);
		$mmaster = new model_master();
		$upload_id = (isset($_POST['upload_id']))?$_POST['upload_id']:$mmaster->get_nextid_peserta_upload();
		$data = array (
			'upload_id'=>$upload_id,
			'peserta_id'=>$_POST['peserta_id'],
			'kategori_dokumen'=>$_POST['kategori_dokumen'],
			'is_valid'=>(isset($_POST['is_valid']))?1:0,
			'user_id'=>$this->coms->authenticatedUser->id,
			'tgl_upload'=>date('Y-m-d H:i:s'),
			'last_update'=>date('Y-m-d H:i:s')
		);
		//curl
		$cekicon = 'no error';
		if(isset($_FILES['file'])){
			foreach ($_FILES['file'] as $id => $icon) {
				if($id == 'error'){
					if($icon!=0){
						$cekicon = 'error';
					}
				}
			}
			//------------------UPLOAD FILE START---------------------------------------------
			if($cekicon!='error'){
				foreach ($_FILES['file'] as $id => $icon) {
					$brokeext = $this->get_extension($icon);
					if($id == 'name'){
						$ext 	= $brokeext;
						$file	= $icon;
					}
					if($id == 'type'){
						$file_type = $_FILES['file']['type'];
					}
					if($id == 'size'){
						$file_size = $_FILES['file']['size'];
					}
					if($id == 'tmp_name'){
						$file_tmp_name = $_FILES['file']['tmp_name'];
					}
				}
				
				$upload_dir = 'assets/upload/file/event/peserta/'.date('Y'). '-' .date('m').'/';		
				$upload_dir_db = 'upload/file/event/peserta/'.date('Y'). '-' .date('m').'/';
			
				$file_loc = $upload_dir_db . $file;
				
				$this->upload_file($upload_dir, $file_tmp_name, $file, $file, $file_size);
				$data['file_loc']= $file_loc;
			    $data['file_name']= $file;
				$data['file_size']= $file_size;
				$data['file_type']= $file_type;
				if (isset($_POST['upload_id']))	$mmaster->update_peserta_upload($data);
				else $mmaster->simpan_peserta_upload($data);
				
				$this->redirect('module/event/kegiatan/berkas/'.$kegiatanid.'/'.$pesertaid);
			}
			else {
				if (isset($_POST['upload_id']))
					$mmaster->update_peserta_upload($data);
				else
					$mmaster->simpan_peserta_upload($data);
				$this->redirect('module/event/kegiatan/berkas/'.$kegiatanid.'/'.$pesertaid);
			}
		}
	}
	function delete_peserta_upload(){
		$mmaster = new model_master();
		$mmaster->hapus_peserta_upload($_POST['upload_id']);
	}
	function submit_dokumen_review(){
		$mmaster = new model_master();
		$review_id = (isset($_POST["review_id"]))?$_POST["review_id"]:$mmaster->get_nextid_dokumen_review();
		$data = array(
			'review_id'=>$review_id,
			'upload_id'=>$_POST['upload_id'],
			'review_by'=>$_POST['review_by'],
			'tgl_review'=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_review']),'Y-m-d H:i:s'),
			'hasil_review'=>$_POST['hasil_review'],
			'st_review'=>$_POST['st_review'],
			'catatan'=>$_POST['catatan'],
			'user_id'=>$this->coms->authenticatedUser->id,
			'last_update'=>date('Y-m-d H:i:s')
		);
		$mmaster->simpan_dokumen_review($data);
		echo json_encode($mmaster->get_dokumen_review($_POST['upload_id'],$review_id));
	}
	function delete_dokumen_review (){
		$mmaster = new model_master();
		$mmaster->hapus_dokumen_review($_POST['review_id']);
	}
	//tahap
	function tahap($kegiatanid, $pesertaid=null){
		$mmaster = new model_master();
		$data['kegiatan'] = $mmaster->get_kegiatanDet($kegiatanid);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('admin/js/moment.js');
		$this->coms->add_script('admin/js/bootstrap-datetimepicker.js');
		$this->coms->add_script('admin/css/bootstrap-datetimepicker.css');
		if(!$pesertaid) {
			$data['kegiatan_tahap'] = $mmaster->get_kegiatan_tahapDet($kegiatanid);
			
			$this->add_script('js/kegiatan/kegiatan_tahap.js');
			$this->view('kegiatan/tahap_kegiatan.php',$data);
		}
		else{
			$data['peserta'] = $mmaster->get_pesertaDet($pesertaid);
			$data['tahap_kegiatan'] = $mmaster->get_kegiatan_tahapDet($kegiatanid);
			$data['peserta_tahap'] = $mmaster->get_kegiatan_tahap_pesertaDet($pesertaid);
			
			$this->add_script('js/kegiatan/peserta/peserta_tahap.js');
			$this->view('kegiatan/peserta/tahap_peserta.php',$data);
		}
	}
	function submit_tahap_peserta(){
		$mmaster = new model_master();
		$peserta_tahap_id = (isset($_POST['peserta_tahap_id']))?$_POST['peserta_tahap_id']:$mmaster->get_nextid_peserta_tahap();
		$data = array(
			"peserta_tahap_id" => $peserta_tahap_id,
			"tahap_id" => $_POST['tahap_id'],
			"peserta_id"=>$_POST['peserta_id'],
			"catatan" => $_POST['catatan'],
			"is_aktif" => (isset($_POST['is_aktif']))?1:0,
			"user_id" => $this->coms->authenticatedUser->id,
			"last_update" => date('Y-m-d H:i:s')
		);
		
		$mmaster->simpan_tahap_peserta($data);
		echo json_encode($mmaster->get_kegiatan_tahap_peserta($peserta_tahap_id));
	}
	function submit_tahap_kegiatan(){
		$mmaster = new model_master();
		$tahap_id = (isset($_POST['tahap_id']))?$_POST['tahap_id']:$mmaster->get_nextid_kegiatan_tahap();
		$urut = (isset($_POST['urut']))?$_POST['urut']:$mmaster->get_nexturut_tahap($_POST['parent_id'],$_POST['kegiatan_id']);
		$data = array(
			'tahap_id'=>$tahap_id,
			'kegiatan_id'=>$_POST['kegiatan_id'],
			'keterangan'=>$_POST['keterangan'],
			'parent_id'=>$_POST['parent_id'],
			'urut'=>$urut,			
		);
		$mmaster->simpan_tahap_kegiatan($data);
		echo json_encode($mmaster->get_tahap_kegiatan($tahap_id));
	}
	function delete_tahap_peserta(){
		$mmaster = new model_master();
		$mmaster->hapus_tahap_peserta($_POST['peserta_tahap_id']);
	}
	function delete_tahap_kegiatan (){
		$mmaster = new model_master();
		$mmaster->hapus_tahap_kegiatan($_POST['tahap_id']);
	}
	//nilai
	function nilai($kegiatanid, $pesertaid=null){
		$mmaster = new model_master();
		$data['kegiatan'] = $mmaster->get_kegiatanDet($kegiatanid);
		$data['kegiatan_nilai'] = $mmaster->get_kegiatan_nilaiDet($kegiatanid);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('admin/js/moment.js');
		$this->coms->add_script('admin/js/bootstrap-datetimepicker.js');
		$this->coms->add_script('admin/css/bootstrap-datetimepicker.css');
		
		if(!$pesertaid) {
			$this->add_script('js/kegiatan/kegiatan_nilai.js');
			$this->view('kegiatan/nilai_kegiatan.php',$data);
		}
		else{
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_script('js/jquery.tokeninput.js');
			
			//$data['pegawai'] = $mmaster->get_pegawai();
			$data['peserta'] = $mmaster->get_pesertaDet($pesertaid);
			$data['peserta_nilai'] = $mmaster->get_peserta_nilaiDet($pesertaid);
			
			$data['peserta_tahap'] = $mmaster->get_kegiatan_tahap_pesertaDet($pesertaid);
			$this->add_script('js/kegiatan/peserta/peserta_nilai.js');
			$this->view('kegiatan/peserta/nilai_peserta.php',$data);
		}
	}
	function submit_nilai_peserta(){
		$mmaster = new model_master();
		$nilai_peserta_id = (isset($_POST['nilai_peserta_id']))?$_POST['nilai_peserta_id']:$mmaster->get_nextid_peserta_nilai();
		$data = array(
			'nilai_peserta_id'=>$nilai_peserta_id,
			'komponen_nilai_id'=>$_POST['komponen_nilai_id'],
			'peserta_tahap_id'=>$_POST['peserta_tahap_id'],
			'nilai'=>$_POST['nilai'],
			'uraian'=>$_POST['uraian'],
			'dinilai_oleh'=>$_POST['dinilai_oleh'],
			'karyawan_id'=>$_POST['karyawan_id'],
			"user_id" => $this->coms->authenticatedUser->id,
			"last_update" => date('Y-m-d H:i:s')
		);
		$mmaster->simpan_peserta_nilai($data);
		echo json_encode($mmaster->get_peserta_nilai($nilai_peserta_id));
	}
	function submit_komponen_nilai_kegiatan(){
		$mmaster = new model_master();
		$komponen_nilai_id = (isset($_POST['komponen_nilai_id']))?$_POST['komponen_nilai_id']:$mmaster->get_nextid_kegiatan_nilai();
		$urut = (isset($_POST['urut']))?$_POST['urut']:$mmaster->get_nexturut_nilai($_POST['parent_id'],$_POST['kegiatan_id']);
		$data = array(
			'komponen_nilai_id'=>$komponen_nilai_id,
			'kegiatan_id'=>$_POST['kegiatan_id'],
			'judul'=>$_POST['judul'],
			'keterangan'=>$_POST['keterangan'],
			'is_aktif'=>(isset($_POST['is_aktif']))?1:0,
			'parent_id'=>$_POST['parent_id'],
			'urut'=>$urut
		);
		$mmaster->simpan_komponen_nilai_kegiatan($data);
		echo json_encode($mmaster->get_kegiatan_nilai($komponen_nilai_id));
	}
	function delete_nilai_peserta(){
		$mmaster = new model_master();
		$mmaster->hapus_peserta_nilai($_POST['nilai_peserta_id']);
	}
	function delete_komponen_nilai_kegiatan(){
		$mmaster = new model_master();
		$mmaster->hapus_komponen_nilai_kegiatan($_POST['komponen_nilai_id']);
	}
	//faq
	function faq($kegiatanid){
		$mmaster = new model_master();
		$data['kegiatan'] = $mmaster->get_kegiatanDet($kegiatanid);
		$data['kegiatan_faq'] = $mmaster->get_faqDet($kegiatanid);
		
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->add_script('js/base64.js');	
		$this->add_script('js/kegiatan/kegiatan_faq.js');	
		$this->view('kegiatan/faq_kegiatan.php',$data);
	}
	function submit_faq(){
		$mmaster = new model_master();
		$comment_id = (isset($_POST['comment_id']))?$_POST['comment_id']:$mmaster->get_nextid_faq();
		$parent_id = (isset($_POST['parent_id']))?$_POST['parent_id']:'';
		$data = array(
			'comment_id'=>$comment_id,
			'content_id'=>'-',
			'event_id'=>$_POST['kegiatan_id'],
			'email'=>'',
			'user_name'=>$this->coms->authenticatedUser->username,
			'title'=>$_POST['title'],
			'comment'=>$_POST['comment'],
			'parent_id'=>$parent_id,
			'comment_post'=>(isset($_POST['comment_id']))?$mmaster->get_faqDet(null,null,$_POST['comment_id'])->comment_post : date('Y-m-d H:i:s'),
			'is_approve'=>(isset($_POST['is_approve']))?1:0,
			'user_id'=>$this->coms->authenticatedUser->id,
			'category'=>'faq',
			'last_update'=>date('Y-m-d H:i:s')
		);
		// echo json_encode($data);
		$mmaster->simpan_faq($data);
		echo json_encode($mmaster->get_faqDet(null,null,$comment_id));
	}
	function delete_faq(){
		$mmaster = new model_master();
		$mmaster->hapus_faq($_POST['comment_id']);
	}
	function cek (){
		echo json_encode($this->coms->authenticatedUser);
	}
}
?>