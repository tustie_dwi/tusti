function new_mark(mhs_id, key_id, kategori, link_id, kat_nilai){
	$("#newMark").modal('show');
	
	mkName = $(".tugasTitle-"+key_id).text();
	mhsName = $(".mhsName-"+mhs_id).text();
	
	$(".tugas-wrap").html(mkName);
	$(".mhs-wrap").html('<i class="fa fa-user"></i> ' + mhsName);
	
	$("#mhs_id").val(mhs_id);
	$("#tugas_id").val(key_id);
	$("#kategori").val(kategori);
	$("#kategori_nilai").val(kat_nilai);
	$("#link_id").val(link_id);
	
	$("#skor, #total_skor").val('');
};

$(".e9").select2();

$(document).on('change','.e9', function(){
	// alert($(this).val());
	// $(".table-data > tbody > tr").hide();
	// $(".table-data > tbody > ."+$(this).val()).show();
	document.getElementById("form-jadwal").submit();
});

function add_final_grade(mk){
		var judul		= "Nilai KHS";
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/elearning/progress/add',
				data : $.param({
					mkd : mk,
					judul: judul		
				}),
				success : function(msg) {
				
					if (msg == '') {
					} else {						
						location.reload();
					}
				}
			});
	 e.preventDefault(); //STOP default action
	return false;
}

//remove coponent
function remove_btn(){
	var note = $("#btn-remove").text();
	if(note.indexOf('Remove Grade') > 0) $("#btn-remove").html('<i class="fa fa-trash-o"></i> Close');
	else $("#btn-remove").html('<i class="fa fa-trash-o"></i> Remove Grade');
	
	$("[name^=komponen]").fadeToggle();
}

function remove_component(komponen_id){
	if(confirm('Are you sure wanna remove this course component?')){
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/elearning/progress/del',
			data : $.param({
				komponen : komponen_id
			}),
			success : function(msg) {
				location.reload();
			}
		});
	}
}

$(document).ready(function(){
	
	var d = new Date();
	tgl = d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
	
	$('#newMark').on('shown.bs.modal', function (e) {
		$("#init_enter").val('1');
	});
	
	$('#newMark').on('hide.bs.modal', function (e) {
	   $("#init_enter").val('0');
	});
	
	$("body").keyup(function(event){
		init = $("#init_enter").val();
	    if(event.keyCode == 13 && init == 1){
	        skor = $("#skor").val();
	        total_skor = $("#total_skor").val();
	        
	        if(!skor || !total_skor) alert('Please insert data completely !!');
	        else{
	        	var formData = new FormData($('#formNewMark')[0]);
	    		var URL = base_url + 'module/elearning/progress/new_mark';
	    		
	    		$.ajax({
		            url : URL,
			        type: "POST",
			        dataType : "HTML",
			        data : formData,
			        async: false,
			        success:function(msg) 
			        {
			            $("#newMark").modal('hide');
			            mhs_id = $("#mhs_id").val();
			            tugas_id = $("#tugas_id").val();
			            
			            cell_data = '<span class="btn-tooltip" data-placement="left" title="Not Turn In<br>'+tgl+'"><span class="text text-warning">'+skor+'/'+total_skor+'</span></span>';
			            $(".cell-data-"+mhs_id+tugas_id).html(cell_data);
			        },
			        error: function(msg) 
			        {
			            alert('Oops, process failed. Please try again !!')   
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });
	        }
	    }
	});
	
	
}); //end of qQuery functions
