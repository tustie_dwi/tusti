$(document).ready(function(){
	$(".inp-note, .inp-comment, .loading, .btn-menu, .edit-post, .edit-comment, .comment-hide").hide();

	$("#post-inp").click(function(){
		$("#post-inp").hide();
		$(".inp-note").show();
		$("#isi").attr('autofocus','');
	});
		
	$("#cancel-note").click(function(){
		$("#attach-wrap-note").html("");
		$(".inp-note").hide();
		$("#post-inp").show();
	});
	
	$('#btn-submit-note').click(function (e) {
    	var isi_ = $("#isi").val().length;
    	var mkd_id_ = $("#mkd_id").val().length;
    	if(isi_ > 1 && mkd_id_ > 1){
	    	var formData = new FormData($('#form-note')[0]);
	    	var URL = base_url + 'module/elearning/home/save_note';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.reload();
		        },
		        error: function(msg) 
		        {
		            alert (msg);  
		            location.reload();    
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
		}
		else {
			alert("Note and course can't be blank!!");
		}
	});
	
	$(".btn-edit-post").click(function(){
		var id = $(this).data('id');
		
		var formData = new FormData($('.form-edit-post-'+id)[0]);
    	var URL = base_url + 'module/elearning/home/edit_post';
          $.ajax({
            url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            location.reload();
	        },
	        error: function(msg) 
	        {
	            location.reload();    
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});	
	
	$("#uploadBtn").change(function(){
		//$('.label-file').remove();
		var file_name = document.getElementById("uploadBtn").files;
		for (var i = 0; i < file_name.length; i++){
			var filename = "'"+file_name[i].name+"'";
			
			input_url = ' <span class="label label-success label-file" data_id="'+ file_name[i].name +'"><i class="fa fa-file-text-o"></i> '+file_name[i].name;
			input_url += ' <i onclick="del_file('+filename+')" title="Delete" class="fa fa-times" style="cursor: pointer"></i></span>';
			
			$("#attach-wrap-note").append(input_url);
			/*$("#attach-wrap-note").append('<span class="label label-success label-file" data-id="'+ file_name[i].name +'"><i class="fa fa-file-text-o"></i> '+file_name[i].name+' <i title="Delete" class="fa fa-times" style="cursor: pointer" onClick="del_file('''+ file_name[i].name +''')"></i></span> ');*/
		}
	});
	
	
	
	$("#uploads").change(function(){
		//$('.label-file').remove();
		var file_name = document.getElementById("uploads").files;
		for (var i = 0; i < file_name.length; i++){
			var filename = "'"+file_name[i].name+"'";
			
			input_url = ' <span class="label label-success label-file" data_id="'+ file_name[i].name +'"><i class="fa fa-file-text-o"></i> '+file_name[i].name;
			input_url += ' <i onclick="del_file('+filename+')" title="Delete" class="fa fa-times" style="cursor: pointer"></i></span>';
			
			$("#attach-wrap-assignment").append(input_url);
			//$("#attach-wrap-assignment").append('<span class="label label-success label-file" ><i class="fa fa-file-text-o"></i> '+file_name[i].name+'</span> ');
		}
	});
	
	$('#add_url').focus(function(){
		$('#add_url').val("http://");
	});
	
	$(".nav-notification > tbody > tr").hover(
		function(){
			$(this).find('.icon-del').css("opacity","1");
		},
		function(){
			$(this).find('.icon-del').css("opacity","0");
		}
	);
	
	//syllabus
	$(".btn-collapse").click(function(){
		// /$(".panel-collapse").collapse("hide");
		//$(".btn-collapse").html("show");		
		var idcollapse = $(this).attr("href");
		var kls = $(idcollapse).attr("class");
		
		if(kls=="panel-collapse collapse"){
			$(this).html("close");
			$("#form-silabus"+idcollapse.substr(1)).hide();
			$("button[name='b_silabus"+idcollapse.substr(1)+"']").hide();
			$("button[name='b_cancel"+idcollapse.substr(1)+"']").hide();
			var collapse_title = $(this).closest(".panel-title").find("span.collapse-title").html();
			if(collapse_title=="Course Overview"){ 				
			}
			else{
				get_komponen(idcollapse.substr(1));
			}
		}
		else{
			$(this).html("show");
			$("#label-silabus"+idcollapse.substr(1)).show();
		}
	});
	
	$("button[name='edit_sil']").click(function(){
		var id = $(this).data("uri");
		$("#form-silabus"+id).show();
		$("button[name='b_silabus"+id+"']").show();
		$("button[name='b_cancel"+id+"']").show();
		$("#label-silabus"+id).hide();
	});
	
	$("button.cancel").click(function(){
		var id = $(this).data("uri");
		$("#form-silabus"+id).hide();
		$("button[name='b_silabus"+id+"']").hide();
		$("button[name='b_cancel"+id+"']").hide();
		$("#label-silabus"+id).show();
	});
	
	$("select[name='unit_course']").change(function(){
		var data = $(this).val();
		
		if(data==0){
			data = "";
		}
		else{
			var i = $('option:selected', this).data('i');
		}

		get_materi(data,i);
	});
	
}); //end of jquery

$(document).on( 'change', '#select_jadwal', function(){
	var jadwalid =  $(this).val();
	var output	 = '';
	if(jadwalid!=0){
		$.ajax({
	        url : base_url + 'module/elearning/course/get_member',
	        type: "POST",
	        dataType : "html",
	        data : $.param({
	        	jadwalid : jadwalid
	        }),
	        success:function(data) 
	        {
	        	$('.member-li').html(data);
	        }
	    });
   }
});

$(document).on( 'change', 'select[name="jadwal"]', function(){

	var mk_id	= $(this).find('option:selected').data('mk');
	
	$.ajax({
        url : base_url + 'module/elearning/quiz/get_materi',
        type: "POST",
        dataType : "JSON",
        data : $.param({
        	mk_id : mk_id
        }),
        success:function(data) 
        {
        	var output = '';
        	if(JSON.stringify(data)!='""'){
        		output += '<option value="0">No Topic</option>';
        		$.each(data ,function(i,d) {
        			output += '<option value='+d.materi_id+'>'+d.judul+'</option>';
        		});
        		
        		$('select[name="select-materi"]').html(output);
        		$('.material-assign').fadeIn();
        	}else{
        		$('.material-assign').fadeOut();
        	}
        }
    });
});


function del_file(file){		
		$(".label-file[data_id='"+file+"']").fadeOut();
		$(".label-file[data_id='"+file+"']").remove();
	}
	

//-------------------------------------------- notifikation
function redirect_notif(url_, notif_id_){

	var uri = base_url + 'module/elearning/home/update_read';
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({notif_id : notif_id_}),
        success:function(msg) 
        {
            window.location = url_;
        },
        error: function(msg) 
        {
			 window.location = url_;
            //location.reload();    
        }
    });
}

function del_notifikasi(not_id){
	$(".nav-notification > tbody > tr[id='"+not_id+"']").fadeOut();
	var uri = base_url + 'module/elearning/home/delete_notifikasi';
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({notif_id : not_id}),
        success:function(msg) 
        {
            // $(".box").html(msg);
        },
        error: function(msg) 
        {
            location.reload();    
        }
    });
}
// ----------------------------------- Materi MK --------------------------------------------------//
function view_materi(jadwal,mk,materi){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/elearning/course/topic/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function get_materi(materi, i){
	var form = document.createElement("form");
    var input = document.createElement("input");

	form.action = window.location.href;
	form.method = "post";
	
	input.name = "materi_id";
	input.value = materi;
	form.appendChild(input);
	
	//alert(window.location.href);
	if(i){
		 var input2 = document.createElement("input");
		 input2.name = "nomer";
		 input2.value = i;
		 form.appendChild(input2);
	}
	
	document.body.appendChild(form);
	form.submit();
}

function view_file(jadwal,mk,materi,file){
	var form = document.createElement("form");
    var input = document.createElement("input");	
	
	form.action = base_url + "module/elearning/course/topic/"+mk+"/"+materi+"/"+file;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
	
	input.name = "action";
	input.value = "view";
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function edit_materi(jadwal,mk,materi){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/elearning/course/topic/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
	
	input.name = "action";
	input.value = "edit";
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function edit_file(jadwal,mk,materi,file){

	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/elearning/course/topic/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
	
	input.name = "action";
	input.value = "file";
	form.appendChild(input);
	
	input.name = "file_id";
	input.value = file;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function add_sub_materi(jadwal,mk,materi){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/elearning/course/topic/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
	
	input.name = "action";
	input.value = "add sub";
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};


function add_file_materi(jadwal,mk,materi){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/elearning/course/topic/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
	
	input.name = "action";
	input.value = "add file";
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};
function add_materi(jadwal,mk){
	
	var form = document.createElement("form");
    var input = document.createElement("input");
    var input2 = document.createElement("input");
    var input3 = document.createElement("input");

	form.action = base_url + "module/elearning/course/topic/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
		
	input.name = "action";
	input.value = "add";
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function submit_topik_content(i, mk){
	var publish = i;
    $('#form-save-topik').submit(function (e) {
    	var formData = new FormData($(this)[0]);
    	if(publish=='publish'){
	   		formData.append("b_savepublish", "1");
	   	}else{
	   		formData.append("b_draft", "1");
	   	}
    	var URL = base_url + 'module/elearning/course/save_topic';
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert (msg);
	            location.href = base_url + 'module/elearning/course/syllabus/'+mk;
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload failed');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
};

function submit_file_content(i, mk){
	var publish = i;
	
    $('#form-save-file').submit(function (e) {
    	var judul = $("#judul").val();
    	if(judul.length > 0){
	    	var formData = new FormData($(this)[0]);
	    	if(publish=='publish'){
		   		formData.append("b_savepublish", "1");
		   	}else{
		   		formData.append("b_draft", "1");
		   	}
	    	var URL = base_url + 'module/elearning/course/save_upload_file';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
					alert ('Update success');    
		            location.href = base_url + 'module/elearning/course/syllabus/'+mk;
		        },
		        error: function(msg) 
		        {
		            alert ('Update failed');      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else alert("Please fill in the title");
	});
};

function submit_edit_file_content(i, mk){
	var publish = i;
	// alert(i);
    $('#upload-form-edit-file').submit(function (e) {
    	var judul = $("#judul").val();
    	if(judul.length > 0){
	    	var formData = new FormData($(this)[0]);
	    	if(publish=='publish'){
		   		formData.append("b_savepublish", "1");
		   	}else{
		   		formData.append("b_draft", "1");
		   	}
	    	var URL = base_url + 'module/elearning/course/save_topic_file';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
					 location.href = base_url + 'module/elearning/course/syllabus/'+mk;
		        },
		        error: function(msg) 
		        {
		            alert ('Update failed');      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else alert("Please fill in the title");
	});
};

function do_del_topic(i) {
  	var x = confirm("Are you sure you want to delete?");
 	if (x){
    	var a=document.getElementsByClassName("deleted"+i);
  		var del_id=a[0].getAttribute('value');
  		var url=a[0].getAttribute('uri');
		
	  	$.ajax({
				type : "POST",
				dataType : "html",
				url : url,
				data : $.param({
					delete_id : del_id
				}),
				success : function(msg) {
					
					if (msg) {
						$("#del_materi"+i).fadeOut();
					} else {
						alert('Failed');
					}
					
				}
		});
	}
  else {
  	location.reload();
  }
  	
}


// -------------------------------------------- End Materi MK ---------------------------------//

//--------------------------------------------------------- post manage
$(document).on("mouseenter", ".li-post", function(){ //show menu edit post
	var id = $(this).data('id');
	$('.btn-menu').hide();
	$(".btn-menu[id='"+id+"']").fadeIn();
});

$(document).on("mouseleave", ".li-post", function(){ //hide menu edit post
	$('.btn-menu').hide();
});
	
$(document).on("click", ".btn-post-comment", function(){ //save_comment
	var post_id_ = $(this).data('postid');
	var nilai_ = $(".nilai-comment[postid='"+post_id_+"']").val();
	var foto_ = $("#foto").val();
	var hidId = $(this).data('id');
	var link_id_ = 'elearning/home/show_post/'+hidId;
	
	if(nilai_ != ''){
		var uri = base_url + 'module/elearning/home/save_comment';
		$.ajax({
            url : uri,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({post_id : post_id_, isi : nilai_, foto : foto_, link_id : link_id_}),
	        success:function(msg) 
	        {
	            $(".comment-wrap-"+hidId).append(msg);
	            $(".edit-comment").hide();
	            $(".nilai-comment").val('');
	            // location.reload();
	        },
	        error: function(msg) 
	        {
	            // alert (msg);  
	            location.reload();    
	        }
	    });
	}		
});

$(document).on("mouseenter", ".btn-tooltip", function(){ //show tooltip
	$(this).tooltip('show');
});

$(document).on("click", ".btn-comment", function(){ //show comment input
	var soal_id = $(this).data('postid');
	$(".inp-comment[data-postid='"+soal_id+"']").slideToggle();
});

function delete_post(post_id_){ //hapus post
	var URL = base_url + 'module/elearning/home/delete_post';
	if(confirm('Are you sure want to delete your post?')){
	    $.ajax({
	        url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({post_id : post_id_}),
	        success:function(msg) 
	        {
	        	// alert(msg);
	        	$(".post-box-"+post_id_).fadeOut();
	        	// location.reload();
	        },
	        error: function(msg) 
	        {
	            location.reload();    
	        },
	        processData: false
	    });
   }
}

function edit_post(post_id){
	cancel_edit_post();
	$(".edit-post[id='"+post_id+"']").fadeIn();
}

function cancel_edit_post(){
	$(".edit-post").fadeOut();
}

function more_post(){
	var URL = base_url + 'module/elearning/home/add_post';
	var page = $('#page').val();
	page = parseInt(page) + 1;
	$('#page').val(page);
	$.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : $.param({page_ : page}),
        success:function(msg) 
        {
        	if(msg == 'habis'){
        		$('.btn-more-post').fadeOut();
        	}
        	else{
	        	$(".view-content").append(msg);
	        	$(".inp-note, .inp-comment, .loading, .btn-menu, .edit-post, .edit-comment, .comment-hide").hide();
        	}
        },
        error: function(msg) 
        {
            location.reload();    
        },
        processData: false
    });
}

function more_post_group(mkid){
	var URL = base_url + 'module/elearning/course/add_post/'+mkid;
	var page = $('#page').val();
	page = parseInt(page) + 1;
	$('#page').val(page);
	$.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : $.param({page_ : page}),
        success:function(msg) 
        {
        	if(msg == 'habis'){
        		$('.btn-more-post').fadeOut();
        	}
        	else{
	        	$(".view-content").append(msg);
	        	$(".inp-note, .inp-comment, .loading, .btn-menu, .edit-post, .edit-comment, .comment-hide").hide();
        	}
        },
        error: function(msg) 
        {
            location.reload();    
        },
        processData: false
    });
}

//---------------------------------------------------------- comment manage

function del_comment(post_id_){
	var URL = base_url + 'module/elearning/home/delete_comment';
	if(confirm('Are you sure want to delete your post?')){
		
	    $.ajax({
	        url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({post_id : post_id_}),
	        success:function(msg) 
	        {
	        	$(".box-comment-"+post_id_).fadeOut();
	        	// location.reload();
	        },
	        error: function(msg) 
	        {
	            location.reload();    
	        },
	        processData: false
	    });
   }
}

function show_all_comment(post_id){
	$('.comment-'+post_id).fadeIn();
	$('.btn-show-more-'+post_id).hide();
}

function edit_comment(comment_id){
	$(".form-edit-post-"+comment_id).fadeIn();
	$(".isi-edit-post-"+comment_id).hide();
}

function cancel_edit_comment(comment_id){
	$(".form-edit-post-"+comment_id).hide();
	$(".isi-edit-post-"+comment_id).fadeIn();
}

function save_edit_comment(comment_id){
	var formData = new FormData($('.form-edit-post-'+comment_id)[0]);
	var URL = base_url + 'module/elearning/home/edit_post';
      $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : formData,
        async: false,
        success:function(msg) 
        {
        	cancel_edit_comment(comment_id);
        	$(".isi-edit-post-"+comment_id).html($(".form-edit-post-"+comment_id).find("textarea").val());
            // location.reload();
        },
        error: function(msg) 
        {
            // location.reload();    
        },
        cache: false,
        contentType: false,
        processData: false
    });
    e.preventDefault(); //STOP default action
    return false;
}

//---------------------------------------------------------- add folder
function get_folder(folder_id_){
	$(".loading").show();
	var URL = base_url + 'module/elearning/home/get_library';
    $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : $.param({folder_id : folder_id_}),
        success:function(msg) 
        {
        	$(".folder-wrap").html(msg);
        	$(".loading").hide();
        },
        error: function(msg) 
        {
            location.reload();    
        },
        processData: false
    });
}

function folder_back(){
	$(".loading").show();
	var folder_id_ = $("#folder_back_parent_id").val();
	if(folder_id_ != '0'){
		var URL = base_url + 'module/elearning/home/folder_back';
	    $.ajax({
	        url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({folder_id : folder_id_}),
	        success:function(msg) 
	        {
	        	$(".folder-wrap").html(msg);
	        	$(".loading").hide();
	        },
	        error: function(msg) 
	        {
	            location.reload();    
	        },
	        processData: false
	    });
   }
   else{
   		$('#modal-library').modal('hide');
   }
}


//-------------------------------------------------- add link
function tab_val(link){
	$("#tab-link").val(link);
}

function get_new_url(){
	var tab_link = $("#tab-link").val();
	var url_ = $('#add_url').val();
	if(cek_url_val(url_)){
		var url_param = "'"+url_+"'";
		var input_url = "<input value='"+url_+"' name='link[]' type='hidden' />";
		$('#add_url').val('');
		$('#link-wrap-'+tab_link).append(input_url);
			
		input_url = ' <span class="label label-success label-link" data-val="'+url_+'"><i class="fa fa-link"></i> '+url_;
		input_url += ' <i onclick="delete_link('+url_param+')" title="Delete" class="fa fa-times" style="cursor: pointer"></i></span>';
		
		$("#attach-wrap-"+tab_link).append(input_url);
		$('#modal-link').modal('hide');
	}
}

function delete_link(url){
	$(".label-link[data-val='"+url+"']").remove();
	$("input[value='"+url+"']").remove();
}

function cek_url_val(url){
	var is_ada = 0;
	$(".label-link").each(function(){
		if(url == $(this).data('val')) is_ada = 1;
	});
	
	if(is_ada == 1) return false;
	else return true;
}


//----------------------------------------------add library
function add_library(file_loc, file_name){
	var tab_link = $("#tab-link").val();
	if(cek_library_val(file_loc)){
		var url_param = "'"+file_loc+"'";
		var input_url = "<input value='"+file_loc+"' name='library[]' type='hidden' />";
		$('#add_url').val('');
		$('#link-wrap-'+tab_link).append(input_url);
		input_url = ' <span class="label label-success label-library" data-loc="'+file_loc+'"><i class="fa fa-archive"></i> '+file_name;
		input_url += ' <i onclick="delete_library('+url_param+')" title="Delete" class="fa fa-times" style="cursor: pointer"></i></span>';
		
		$("#attach-wrap-"+tab_link).append(input_url);
		$('#modal-library').modal('hide');
	}
	else{
		alert('File has been attached!!');
	}
}

function cek_library_val(file_loc){
	var is_ada = 0;
	$(".label-library").each(function(){
		if(file_loc == $(this).data('loc')) is_ada = 1;
	});
	
	if(is_ada == 1) return false;
	else return true;
}

function delete_library(url){
	$(".label-library[data-loc='"+url+"']").remove();
	$("input[value='"+url+"']").remove();
}

//-------------------------------------------- url mk
function url_mk_(id){
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/elearning/home/index/'+id,
		data : $.param({ mkid : id }),
		success : function(msg) {
			
			if(msg){	
				$(".view-content").html(msg);				
			}else{
				return false;
			}			
		},
		cache: false,
		contentType: false,
		processData: false
	});
	 e.preventDefault(); //STOP default action
	return false;
}

$(document).on('click', '.load-quiz', function() {
	$.ajax({
        url : base_url + 'module/elearning/quiz/get_quiz',
        type: "POST",
        dataType : "json",
        // data : $.param({
        	// mkid : mkid
        // }),
        success:function(data) 
        {
        	var t = $('#example').DataTable();
        	if(data!=""){
        		t.clear();
        		$.each(data ,function(i,d) {
        			var output = '';
        			output += '<i class="fa fa-file-o"></i>&nbsp;'+d.judul+'&nbsp;on&nbsp;'+d.namamk;
        			output += '<br>';
        			output += '<a href="'+base_url+'module/elearning/quiz/edit/'+d.test_id+'">edit</a>';
	        		t.row.add( [
			            output
			        ] ).draw();
        		});
        		$('#QuizBankModal').modal('show');
        	}else{
        		alert("No Data Available");
        		t.clear();
        	}
        }
    });
});

//------------------------------------------------------------------------------download file
function Download(i){
	var x = confirm("Are you sure you want to download file "+i+" ?");
 	if(x){
	  	window.location.href = base_url+'module/elearning/home/download/'+i;
 	}
};

function doView(id){
	var file_id_ = id;
	var action_ = "view";
	var url = base_url + 'module/elearning/home/log_view';

	$.ajax({
		type :"POST",
		dataType : "HTML",
		url : url,
		async: false,
		data : $.param({
			file_id : file_id_,
			action : action_
		}),
		success : function(msg){
			//alert(msg);
		}
	});
};

function doDownload(id) {
	var file_id_ = id;
	var action_ = "download";
	var url = base_url + 'module/elearning/home/log_download';
	//alert(file_id_);
	// alert(action_);
	// alert(URL);
	$.ajax({
		type :"POST",
		dataType : "HTML",
		url : url,
		async: false,
		data : $.param({
			file_id : file_id_,
			action : action_
		}),
		success : function(msg){
			//alert(msg);
		}
	});
};

// ---- syllabus --//
$("#form-content-silabus").hide();

function get_komponen(kmp_id){
	var komponenid	= kmp_id;
	
	var mk 	= document.getElementById("mkid");
	var mkid= $(mk).val();
	
	var jadwal 	= document.getElementById("jadwalid");
	var jadwalid= $(jadwal).val();
	
	var tes = $( "textarea[name='silabus']").length;
	if(tes){
		$( "textarea[name='silabus']").val('');
		CKEDITOR.instances['silabus'+komponenid].setData("");
		$( "input[name='silabusid']").val('');
		$( "input[name='detailid']").val('');
	}
		$( ".silabus"+komponenid).html(" - ");	
	
	
	var aaa = $.post(
		base_url + "module/elearning/course/get_frm_silabus",
        {
			cmbkomponen : komponenid,
			mkid		: mkid,
			jadwal		: jadwalid
		},"json");
	aaa.done(
		function(data){
			var obj = jQuery.parseJSON(data);
			if(tes){
				CKEDITOR.instances['silabus'+komponenid].setData(obj.keterangan);
				$( "textarea[name='silabus']").html(obj.keterangan);
				$( "input[name='silabusid']").val(obj.id);
				$( "input[name='detailid']").val(obj.detail);
				
			}
				$( ".silabus"+komponenid).html(obj.keterangan);	
    	});
    	$("#form-content-silabus").fadeIn(100);
};

function submit_silabus_content(id){
    $('#silabus-form'+id).submit(function (e) {
    	var postData = $(this).serializeArray();
    	var formURL = base_url + 'module/elearning/course/save_silabus';
    	var ket = $("#cmbkomponen").val();
    	
          $.ajax({
            url : formURL,
	        type: "POST",
	        data : postData,
	        success:function(msg) 
	        {
	            alert (msg);
	            location.reload();  
	        },
	        error: function(msg) 
	        { 
	            alert ("Update silabus gagal!");    
	        }
	      });
	    e.preventDefault(); //STOP default action
	});
};

function detail_krs(id){
	
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/elearning/report/detail_krs/";
	form.method = "post"
	
	input.name = "mhs_id";
	input.value = id;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
}
// $('.nav-syllabus').each(function(e){
	// var type=$(this).data('syllabus') + 1;
	// $(this).find('p').css('color','#7E7E7E');
// });
