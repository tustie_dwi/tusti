$(".btn-tooltip").mouseenter(function(){
	$(".btn-tooltip").tooltip('show');
});

$('.show-calendar-it').click(function(e){
	e.preventDefault();
	$(this).parent('small').parent('div').parent('td').children('.hide-it').fadeIn().removeClass('hide-it');
	$(this).hide();
	$('#calendarModal').hide();
	});
$('#calendarModal').hide();
	
$('#btn-modal-close').click(function(e){
	e.preventDefault();	
	$('#calendarModal').hide();
});

$('#btn-modal-close-manage').click(function(e){
	$('#cal-detail').show();
	$('#cal-edit').hide();
});

$("#btn-ubah-kegiatan").click(function(){
	$('#cal-edit').show();
	$('#cal-detail').hide();
});

//calendar
var is_detail = 0;

$('.popup-form').click(function(e){
	$('#calendarModal').show();
	e.preventDefault();
	var $year=$(this).data('calendaryear');
	var $month=$(this).data('calendarmonth');
	var $day=$(this).data('calendarday')
	var day = ("0" + $day).slice(-2);
    var month = ("0" + ($month)).slice(-2);
	var today = $year+"-"+(month)+"-"+(day);
	$('#calendarModal .input-datetime').val(today);
	$('#calendarModal .input-datetime').datepicker();
	//$('#calendarModal').modal('toggle');
	$('#calendarModal').show();	
	if(is_detail == 1){
		is_detail = 0;
	}
	else{
		$('#cal-add').show();
		$('#cal-edit, #cal-detail').hide();
	}
	});
	
$(".label-aktifitas").click(function(){
	is_detail = 1;
	$('#cal-add, #cal-edit').hide();
	$('#cal-detail').show();	
	
	$("#info-judul").html(": "+$(this).data("judul"));
	$("#info-waktu").html(": "+$(this).data("tgl") + " " + $(this).data("jam_mulai") + " s/d " + $(this).data("tgl_selesai") + " " + $(this).data("jam_selesai")  );
	$("#info-ruang").html(": "+$(this).data("ruang"));
	$("#info-lokasi").html(": "+$(this).data("lokasi"));
	$("#info-jenis_kegiatan").html(": "+$(this).data("keterangan_jenis_kegiatan"));
	
	var tes = [];
	
	$.each($(this).data("ruang").split(", ") ,function(key, value){
		console.log(value);
		tes.push(value);
	});
	
	console.log(tes);
	$("#ruang").select2("val", tes);
	
	$("[name='edit_jenis_kegiatan']").find("[value='"+$(this).data("jenis_kegiatan")+"']").attr("selected","");
	//$("#edit_ruang").find("[value='"+$(this).data("ruang")+"']").attr("selected","");
	$("[name='time']").val($(this).data("time"));
	$("[name='edit_judul']").val($(this).data("judul"));
	$("[name='edit_tgl_selesai']").val($(this).data("tgl_selesai"));
	$("[name='edit_lokasi']").html($(this).data("lokasi"));
	$("[name='edit_jam_mulai']").val($(this).data("jam_mulai"));
	$("[name='edit_jam_selesai']").val($(this).data("jam_selesai"));
});

function load_select_ruang(ruang_){
	var URL = base_url + 'module/elearning/report/load_select_ruangan';
    $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : $.param({ruang : ruang_}),
        success:function(msg) 
        {
            $("#edit-ruang").html(msg);
        }
    });
}


function save_aktifitas(){
	var formData = new FormData($("#form-aktifitas")[0]);
	var URL = base_url + 'module/elearning/report/add_kegiatan';
	$(".loading").show();
    $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : formData,
        async: false,
        success:function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();
        },
        error: function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();    
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function edit_aktifitas(){
	var formData = new FormData($("#form-edit-aktifitas")[0]);
	var URL = base_url + 'module/elearning/report/edit_kegiatan';
	$(".loading").show();
    $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : formData,
        async: false,
        success:function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();
        },
        error: function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();    
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function hapus_aktifitas(id_){
	if(confirm('Are your sure wanna delete this activity?')){
		// var formData = new FormData($("#form-edit-aktifitas")[0]);
		var URL = base_url + 'home/hapus_kegiatan';
		$(".loading").show();
	    $.ajax({
	        url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({id : id_}),
	        success:function(msg) 
	        {
	        	$(".loading").fadeOut();
	            location.reload();
	        },
	        error: function(msg) 
	        {
	        	$(".loading").fadeOut();
	            location.reload();    
	        }
	    });
   }
}

$("#select-tahun, #select-bulan").change(function(){
	document.getElementById("form-select").submit();
});

$(document).ready(function(){
	$('#calendarModal').hide();
});

function check_tgl(){
	if($("[name='tanggal_sampai']").val() != '') {
		var start = new Date($("[name='tanggal']").val());
		var end = new Date($("[name='tanggal_sampai']").val());

		if(end < start) $("[name='tanggal_sampai']").val('');
	}
}

function check_tgl_edit(){
	if($("[name='edit_tgl_selesai']").val() != '') {
		var start = new Date($("[name='edit_tanggal']").val());
		var end = new Date($("[name='edit_tgl_selesai']").val());

		if(end < start) $("[name='edit_tgl_selesai']").val('');
	}
}

$(function () {
    $('.pick-date').datetimepicker({
        pickDate: true,
        pickTime: false
    });
});


$(function () {
    $('.pick-time').datetimepicker({
        pickDate: false
    });
});




