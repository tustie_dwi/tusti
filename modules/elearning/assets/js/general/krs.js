$(document).ready(function() {		
	//--- POST SELEKSI DARI PARAMETER ---//
	$('#parameter').change(function() {
		$("#param").submit();
	});
	//--- END POST SELEKSI DARI PARAMETER ---//
	
	$(".drop-mk").click(function(){
		var uri = base_url + 'module/elearning/krs/del';
		var id_ = $(this).data('id');
		var mk = $(this).data('mk');
		
		if(confirm('Are you sure delete this course ' + mk + ' ?')){
			
			$.ajax({
				type : 'POST',
				dataType : 'html',
				url : uri,
				data : $.param({krs_id : id_}),
				success:function(msg){
					window.location.href = base_url + "module/elearning/krs/";
					alert('Mata kuliah berhasil dibatalkan');
				}
			});
			
		}
	});
	
	$(".add-krs").click(function(){
		var sks_cur = $("#sks-curr").val();
		var sks_max = $("#sks-max").val();
		
		// alert(sks_cur + ' ' + sks_max);
		
		if(Number(sks_cur) < Number(sks_max)){
			window.location.href = base_url + "module/elearning/krs/add/";
		}
		else{
			alert('Maaf, jumlah sks yang Anda ambil sudah penuh');
		}
	});	
	
	$("#btn-approve").click(function(){
		document.getElementById('form-approve').submit();
	});
	
	$(".btn-btl-krs").click(function(){
		var uri = base_url + "module/elearning/krs/batal_krs";
		var id = $(this).data('id');
		var hidId_ = $(this).data('hidid');
		var mhsid = $("#mhsid").val();
		
		if(confirm('Apakah Anda yakin ingin membatalkan mata kuliah ini?')){
			$.ajax({
				type : 'POST',
				dataType : 'html',
				url : uri,
				data : $.param({krsid : id, hidId : hidId_}),
				success:function(msg){
					window.location.href = base_url + "module/elearning/krs/approve/" + mhsid;
				}
			});
		}
	});	
	
	
});



function add(jadwal_id_){
	var uri = base_url + 'module/elearning/krs/save';
	
	$.ajax({
		type : 'POST',
		dataType : 'html',
		url : uri,
		data : $.param({jadwal_id : jadwal_id_}),
		success:function(msg){
			if(msg == 'ok'){
				$("#warning").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>Ok, mata kuliah berhasil diambil.</div>');
				window.location.href = base_url + "module/elearning/krs/index/ok";
			}
			else if(msg == 'full'){
				$("#warning").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Maaf, kuota kelas yang Anda pilih tidak mencukupi.</div>');
			}
			else if(msg == 'sks_penuh'){
				$("#warning").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Maaf, jumlah sks Anda tidak mencukupi untuk mengambil mata kuliah ini.</div>');
			}
			else if(msg == 'sks_max_penuh'){
				$("#warning").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Maaf, jumlah sks maksimal Anda sudah penuh.</div>');
			}
			else if(msg == 'prasyarat tdk memenuhi'){
				$("#warning").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>Maaf, mata kuliah prasyarat belum diambil.</div>');
			}
		}
	});
};