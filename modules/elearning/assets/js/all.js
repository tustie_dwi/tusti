$(document).on("mouseenter", ".btn-tooltip", function(){ //show tooltip
	$(this).tooltip({
		'html' : true
	});
	
	$(this).tooltip('show');
});

$(function(){
	$('.btn-update-password').click(function(){
		
		if($('#input-password').val().trim() == '') {
			alert('Password must not be blank!');
			return false;
		}
		
		if($('#input-password').val().trim() != $('#input-password2').val().trim()) {
			alert('Password and Conform New Password must be the same');
			return false;
		}

		btn = $(this);
		btn.button('loading');	
		$('#status-password').html('&nbsp;');
		//alert( $(this).parents('td').siblings('.col-status').children('.label').html() );
		
		$.post(
			base_url + 'user/updatepassword',
			$("#form-update-password").serialize(),
			function(data){
				
				btn.button('reset');
				
				if(data.status.trim() == "OK") {
					$('#status-password').html(data.modified);
				}
				else {
					$('#status-password').html('&nbsp;');
					alert(data.error);
				}
				
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
		});
		
		return false;
	});
});

function redirect_notif(url_, notif_id_){

	var uri = base_url + 'module/elearning/home/update_read';
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({notif_id : notif_id_}),
        success:function(msg) 
        {
            window.location = url_;
        },
        error: function(msg) 
        {
            location.reload();    
        }
    });
}