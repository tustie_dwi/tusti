	var countfile = 1;
	function addfile(){
		countfile += 1;
			var newfile = "<div class='row'><div class='col-sm-12'><div class='form-group'>";
				/*newfile += "<input type='text' required='required' name='title[]' autocomplete='off' class='form-control' id='title' value='-' /></div></div>";
				newfile += "<div class='col-sm-6'><div class='form-group'>";*/
				newfile += "<input type='file' required='required' name='uploads[]' multiple='multiple' autocomplete='off'";
				newfile += "id='uploads'/></div></div></div>";
				
			$("#newform").append(newfile);
			$('#count_new_file').val(countfile);
	}
	
	function newfile_parent(id, loc){		
		$("#folder").val(id);
		$("#folder_loc").val(loc);
	}
	
	function newfolder_parent(id, mk){		
		$("#parent").val(id);
		$("#mk").val(mk);
	}
	
	function savefolder(){		
		var mk 		= document.getElementById("group");
		var mkid	= $(mk).val();
		
		var name 	= $("#folder_name").val();
		var URL 	= base_url + 'module/elearning/folder/save';
					
		$.post(
			
			base_url + 'module/elearning/folder/save',
			$('#newfolder_form').serialize(),
			
			function(data){
				alert(data);				
				location.href = base_url + 'module/elearning/course/folder/'+mkid;
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
				location.href = base_url + 'module/elearning/course/folder/'+mkid;
		});		
		return false;			
	}
	
	function savefiles(){		
		var mk 		= document.getElementById("group");
		var mkid	= $(mk).val();
		
		var folder 	= document.getElementById("folder");
		var folderid= $(folder).val();
		
		$.ajax( {
			  url: base_url + 'module/elearning/folder/save_file',
			  type: 'POST',
			  data: new FormData($("#newfile_form")[0]),
			  success : function(msg) {
					alert(msg);
					view_content(folderid);
					//location.href = base_url + 'module/elearning/course/folder/'+mkid;
				},
			  processData: false,
			  contentType: false
			} );
		
		return false;
		
	}
	
	function view_content(id, nama){		
	
		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/elearning/folder/view_content',
			data : $.param({
				id : id,
				name:nama
			}),
			success : function(msg) {
				$("#view_content").html(msg);
			}
		});
	}
	
	
	function doDelete(i,jenis_del) {
		var mk 		= document.getElementById("group");
		var mkid	= $(mk).val();
		
		var del_id = i;
		var jenis_del = jenis_del;
		var x = confirm("Are you sure you want to delete?");
		if (x){
			$.ajax({
				type : "POST",
				dataType : "html",
				url : base_url + 'module/elearning/folder/delete',
				data : $.param({
					delete_id : del_id,
					jenis_del : jenis_del
				}),
				success : function(msg) {
					if (msg) {
						alert("Data Berhasil Terhapus!");
						if(jenis_del == 'file'){
							$(".file"+del_id).fadeOut();
						}
						else if(jenis_del == 'folder'){
							 location.href = base_url + 'module/elearning/course/folder/'+mkid;
						}
					}else {
						alert('gagal');
					}
				}
			});
		}
		else {
			view_content(del_id);
		   //location.reload();
		}
	};
	
	function doView(id, $folder){
		
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/elearning/folder/views/'+id,
			success : function(msg) {	
				$("#view_content").html(msg);	
			}
		});
	}
	
$(function(){
	$('.btn-toggle-status-user').click(function(){
		uid = $(this).data('uid'); // alert(uid);
		label = $(this).find('.label-status'); // alert(label);
		btn = $(this);
		btn.button('loading');	
		
		$.post(
			base_url + 'module/elearning/folder/togglestatus',
			{id: uid},
			function(data){				
				btn.button('reset');
				
				if(data.status.trim() == "OK") {
					$(".label-status").html(data.ustatus);					
				}
				else alert(data.error);
				
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
		});
		
		return false;
	});
});	