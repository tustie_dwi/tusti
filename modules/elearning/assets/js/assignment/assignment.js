$(document).ready(function(){
	$(".form_datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss', 
		showSecond: true
	});
	
	$("#load").click(function(){
		$('#example').DataTable();
	});
	
	
	$("#detail_form").hide();
	$("#assignment_title").focus(function(){
		$("#detail_form").show();
	});
	$(".e9").select2();
	
	$("#select_jadwal").change(function (){
		var jadwal_id_ = $("#select_jadwal").val();
		var uri = $("#select_jadwal").data("uri");
		//alert(jadwal_id);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				jadwal_id : jadwal_id_
			}),
			success : function(msg) {
				if (msg == "<option value='0'>Select Materi</option>") {
					$("#select_materi").html('<option value="0">Select Materi</option>');
					$("#select_materi").select2("enable", false);
					$("#select_materi").attr("disabled","disabled");
					alert("No Materi Found!");
				} else {
					$("#select_materi").removeAttr("disabled");
					$("#select_materi").html(msg);
					$("#select_materi").select2("enable", true);
					
				}
			}
		});
	});
	
	 $('#form-assignment').submit(function (e) {
    	var jadwal_id = $("#select_jadwal").val().length;
    	var instruction = $("#instruction").val().length;
    	var describe = $("#describe").val().length;
    	var start_date = $("#start_date").val().length;
    	var due_date = $("#due_date").val().length;
    	
    	if(jadwal_id > 1 && instruction > 1 && describe > 1 && start_date > 1 && due_date > 1){
	    	var formData = new FormData($(this)[0]);
	    	var URL = base_url + 'module/elearning/home/save_tugas';
	        $.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.reload();
		        },
		        error: function(msg) 
		        {
		            alert (msg);  
		            location.reload();    
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		}
		else {
			alert("Schedule can't be blank!!");
		}
		e.preventDefault(); //STOP default action
	    return false;
	});
	
	$('#upload-assignment').submit(function (e) {
    	var catatan = $("#catatan").val().length;
    	//var upload = $("#uploads").val().length;
    	if(catatan > 1){
	    	var formData = new FormData($(this)[0]);
	    	var URL = base_url + 'module/elearning/assignment/save_answer';
	    	
	          $.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(post_id) 
		        {
		            //alert (post_id); 
		            var URL_file = base_url + 'module/elearning/home/saveAttach_Response/'+post_id;
		            //alert(URL_file);
		            if(post_id !== "Sorry, your data failed to save!"){
		            	 $.ajax({
				            url : URL_file,
					        type: "POST",
					        dataType : "HTML",
					        data : formData,
					        async: false,
					        success:function(msg) 
					        {
					        	alert(msg);
					        	location.reload();  
					        },
					        cache: false,
					        contentType: false,
					        processData: false
				        });
					    e.preventDefault(); //STOP default action
					    return false;
		            }
		        },
		        error: function(post_id) 
		        {
		            alert (post_id);  
		            location.reload();    
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
		}
		else {
			alert("Note and file can't be blank!!");
			location.reload();
		}
	});
		
	$('#nilai-form').submit(function (e) {
		var skor = $("#skor").val().length;
		var total = $("#total_skor").val().length;
		if(skor > 1 && total > 1){
			var formData = new FormData($(this)[0]);
	    	var URL = base_url + 'module/elearning/assignment/skor';
	        $.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.reload();
		        },
		        error: function(msg) 
		        {
		            alert (msg);  
		            location.reload();    
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
			e.preventDefault(); //STOP default action
		    return false;
		}
		else {
			alert("form can't be blank!!");
			location.reload();
		}
	});
	
	$('#upload-comment-form').submit(function (e) {
		var isi = $("#isi").val();
		var parent_id = $("#parent_id").val();
		if(isi.trim() !== "" && parent_id !== 0){
			var formData = new FormData($(this)[0]);
	    	var URL = base_url + 'module/elearning/home/save_comment/assignment';
	        $.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.reload();
		        },
		        error: function(msg) 
		        {
		            alert (msg);  
		            location.reload();    
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
			e.preventDefault(); //STOP default action
		    return false;
		}
		else {
			alert("form can't be blank!!");
			location.reload();
		}
	});
	
	$(document).on('click', '#load-assignment', function() {
	$.ajax({
        url : base_url + 'module/elearning/assignment/get_all_assignment_record',
        type: "POST",
        dataType : "json",
        success:function(data) 
        {
        	// alert(data);
        	var t = $('#assignment-tbl').DataTable();
        	if(data!=""){
        		t.clear();
        		$.each(data ,function(i,d) {
	        		t.row.add( [
			            d.judul, d.mk, d.kelas, "<a href='#' data-dismiss='modal' onclick='load_assignment(\""+d.tgs_id+"\")'>load</a>"
			        ] ).draw();
        		});
        		$('#modal-load-tugas').modal('show');
        	}else{
        		alert("No Data Available");
        		t.clear();
        	}
        }
    });
});
	
});

function validate_tanggal(){
	var tgl_mulai_ = $('#start_date').val();
	var tgl_selesai_ = $('#due_date').val();

	var tgl_mulai = tgl_mulai_.substr(0, 10);
	var tgl_selesai = tgl_selesai_.substr(0, 10);
	
	if(tgl_selesai_!=""){
		if(tgl_mulai_ > tgl_selesai_) {
			alert('Due date must be smaller than start date!');
			$('#due_date').val('');
		}else if(tgl_selesai_=='') {
			$('#due_date').val('');
		}
		else{
			
		}
	}
};

function show_form(){
	$("#nilai").hide();
	$("#nilai-form").show();
};


function doDownloadTugas(id) {
	var file_id_ = id;
	var action_ = "download_tugas";
	var url = base_url + 'module/elearning/home/log_download';
	//alert(file_id_);
	// alert(action_);
	// alert(URL);
	$.ajax({
		type :"POST",
		dataType : "HTML",
		url : url,
		async: false,
		data : $.param({
			file_id : file_id_,
			action : action_
		}),
		success : function(msg){
			//alert(msg);
		}
	});
};

function load_assignment(id){
	var id = id;
	var url = base_url + 'module/elearning/assignment/load_assignment';
	$.ajax({
		type :"POST",
		dataType : "json",
		url : url,
		async: false,
		data : $.param({
			id : id
		}),
		success : function(msg){
				$("#assignment_title").val(msg.judul);
				$("#start_date").val(msg.tgl_mulai);
				$("#due_date").val(msg.tgl_selesai);
				$("#instruction").val(msg.instruksi);
				$("#describe").val(msg.keterangan);
				$("#detail_form").show();
		}
	});
};
