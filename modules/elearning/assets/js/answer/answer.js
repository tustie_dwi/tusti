$(document).ready(function() {
	$("[class^='page-']").hide();
	$("[class='page-1']").show();
	
	$(".jwb").change(function(){
		var isi = $(this).val();
		var soal_id = $(this).data('jwb');
		var inp = soal_id +'|0|3|'+isi;
		
		$("[inp='"+soal_id+"']").val(inp);
	});
});

function change_question(page){
	$("[class^='page-']").hide();
	$("[class='page-"+page+"']").show();
	
	$("[id^='soal-page-']").removeAttr('class');
	$("[id='soal-page-"+page+"']").attr('class', 'active');
}

function form_submit(){
	if(confirm('Are you sure want to submit your test?')) document.getElementById('form-answer').submit();
}

function submit_mark(){
	if(confirm('Are you sure want to marked this submission?')) document.getElementById('form-mark').submit();
}

