var last_selected = '';
$(document).ready(function() {
	$(".form_datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii:ss', 
		showSecond: true
	});
	$(".e9").select2();
	
	$('.type-assign').hide();
	$('.material-assign').hide();

	last_selected = $('.typetest-question-1').find('option:selected').val();
	
	var edit = $('input[name="hidId"]').val();
	
	if(!edit){
		/*-----If question number 1 is selected-------*/
		var li_id = $('.menu-question-number li.active').attr('id');
		if(li_id=='test-number-1'){
			$('.remove-question').hide();
		}
	}
	
	if(edit){//----EDIT---------
		
		var li_num = $('.menu-question-number li').length;
		if(li_num==1){
			$('.remove-question').hide();
		}else{
			$.each($('.remove-question'), function(i,r){
				if((i+1)==1){
					$(this).show();
				}else $(this).hide();
			});
		}
		
		/*--- set type ---*/
		var etype = $('input[name="test-type"]:checked').attr('class');
		var ejadwal = $('.data-quiz-type').data("jadwal");
		var emk = $('.data-quiz-type').data("mk");
		var emateri = $('.data-quiz-type').data("materi");
		$.ajax({
	        url : base_url + 'module/elearning/quiz/get_jadwal',
	        type: "POST",
	        dataType : "html",
	        data : $.param({
	        	type : etype,
	        	jadwalid : ejadwal,
	        	mkid : emk
	        }),
	        success:function(data) 
	        {
	        	if (data == '') {
					$('.type-assign').hide();
				} else {
					if(etype=='exam-type'){
						$('.type-text').text('Exam');
					}else{
						$('.type-text').text('Schedule');
					}
					$("select[name='select-jadwal']").html(data);
					if(etype!='quiz-type') $("select[name='select-jadwal']").select2().select2("val",emk);
					else $("select[name='select-jadwal']").select2().select2("val",ejadwal);
					
					$('.type-assign').fadeIn();
				}
	        }
	    });
	    
	    $.ajax({
	        url : base_url + 'module/elearning/quiz/get_materi',
	        type: "POST",
	        dataType : "JSON",
	        data : $.param({
	        	mk_id : emk
	        }),
	        success:function(data) 
	        {
	        	var output = '';
	        	if(JSON.stringify(data)!='""'){
	        		output += '<option value="0">No Topic</option>';
	        		$.each(data ,function(i,d) {
	        			output += '<option value=\''+d.materi_id+'\'';
	        			if(d.materi_id==emateri){
	        				output += 'selected';
	        			}
	        			output += '>'+d.judul+'</option>';
	        		});
	        		
	        		$('select[name="select-materi"]').html(output);
	        		$("select[name='select-materi']").select2().select2("val",emateri);
	        		$('.material-assign').fadeIn();
	        	}else{
	        		$('.material-assign').fadeOut();
	        	}
	        }
	    });
	    
	}
});

/*-----select bank soal--------------*/
$(document).on('click', '.load-question', function() {
	var type = $('input[name="test-type"]:checked').val();
	var mkid = $('select[name="select-jadwal"]').find(":selected").data('mk');
	if(!type){
		alert("Please select quiz type first.");
	}
	else{
		if(!mkid){
			alert("Please select course first.");
		}
		else{
			$.ajax({
		        url : base_url + 'module/elearning/quiz/get_bank_soal',
		        type: "POST",
		        dataType : "json",
		        data : $.param({
		        	mkid : mkid
		        }),
		        success:function(data) 
		        {
		        	var t = $('#example').DataTable();
		        	if(data!=""){
		        		t.clear();
		        		$.each(data ,function(i,d) {
		        			$('.modal-title').html('Previous Question For '+d.namamk);
		        			var output = '';
		        			output += d.pertanyaan+'<br><small style="color:grey">'+d.kategori;
		        			if(d.count_answer!='1'){
		        				output += '&nbsp;('+d.count_answer+'&nbsp;answers)';
		        			}
		        			output += '</small><br>';
		        			output += '<a href="javascript::" onclick="new_questBank(\''+d.soal_id+'\',\''+d.kathid_id+'\',\''+d.pertanyaan+'\',\''+d.kategori+'\',\''+d.parameter_input+'\')">Load This Question</a>';
			        		t.row.add( [
					            output
					        ] ).draw();
		        		});
		        		$('#QuestionBankModal').modal('show');
		        	}else{
		        		alert("No Data Available");
		        		t.clear();
		        	}
		        }
		    });
		}
	}
});

/*-----select test type--------------*/
$(document).on( 'change', 'input[name="test-type"]', function(){
	var type = $(this).attr('class');
	$.ajax({
        url : base_url + 'module/elearning/quiz/get_jadwal',
        type: "POST",
        dataType : "html",
        data : $.param({
        	type : type
        }),
        success:function(data) 
        {
        	if (data == '') {
				$('.type-assign').hide();
			} else {
				if(type=='exam-type'){
					$('.type-text').text('Exam');
				}else{
					$('.type-text').text('Schedule');
				}
				$("select[name='select-jadwal']").html(data);
				$('.type-assign').fadeIn();
			}
        }
    });
});

/*-----Select Question-------------------*/
$(document).on( 'click', '.menu-question-number li', function(){
	var li_id = $(this).attr('id');
	if(li_id=='test-number-1'){
		$('#remove-question').hide();
	}else{$('#remove-question').show();}
	
	var qn = $(this).attr('id').split('-')[2];
	var qi = $('.menu-question-number li').length;
	for(var i = 1;i <= qi; i++){
		$('#test-number-'+i).removeClass('active');
		hide_quest(i);
	};
	$(this).addClass('active');
	show_quest(qn);
});

/*-----New Question by Bank-------------------*/
function new_questBank(soalid,kathid_id,pertanyaan,kategori,parameter_input){
	var qi = $('.menu-question-number li').length;
	var next_qi = qi+1;
	for(var i = 1;i <= qi; i++){
		$('#test-number-'+i).removeClass('active bottom-child');
		hide_quest(i);
	}
	$('.remove-question-box').append('<a class="btn btn-default remove-question remove-question-'+next_qi+'">Remove Question</a>');
	$('.menu-question-number').append('<li class="bottom-child active" id="test-number-'+next_qi+'"><a href="javascript::" id="link-question-'+next_qi+'">'+next_qi+'</a></li>');
	
	var selectbox = '';
	$.ajax({
        url : base_url + 'module/elearning/quiz/get_test_type',
        type: "POST",
        dataType : "html",
        data : $.param({
        	kathid_id : kathid_id
        }),
        success:function(data) 
        {
        	selectbox += '<select class="form-control typetest-question-'+next_qi+'" name="select-test[]">';
        	selectbox += data;
        	selectbox += '</select>';
        	$('.select-box').append(selectbox);
        }
    });
	$('.points-box').append('<input type="text" class="form-control point-question-'+next_qi+'" required="required" name="point[]" />');
	$('.question-box').append('<textarea name="question[]" class="form-control question-question-'+next_qi+'">'+pertanyaan+'</textarea>');
	
	var randomanswer = '';
	randomanswer += '<label class="randomanswer-question-'+next_qi+'">';
	randomanswer += '<input type="checkbox" value="" name="random-answer-q'+next_qi+'">';
	randomanswer += 'Random Response';
	randomanswer += '</label>';
	$('.randomanswer-box').append(randomanswer);
		
	if(kategori=='essay'){
		$('.randomanswer-question-'+next_qi+'').hide();
	}
	
	var answer = '';
	var count = 0;
	$.ajax({
        url : base_url + 'module/elearning/quiz/get_bank_jawab',
        type: "POST",
        dataType : "json",
        data : $.param({
        	soalid : soalid
        }),
        success:function(data) 
        {
        	answer += '<span class="area answer-option-area-'+next_qi+'">';
        	$.each(data ,function(i,d) {
        		answer += '<div class="radio radio-q'+next_qi+'">';
        		if(parameter_input!='textarea'){
				  answer += '<label>';	  
				  answer += '<input type="'+parameter_input+'" name="options-q'+next_qi+'" value="q'+next_qi+'-'+(i+1)+'"';
				  if(d.is_benar=='1'){
				  answer += ' checked ';
				  }
				  answer += 'class="option-check">';
				  answer += '</label>';
				  answer += '<input type="text" name="answer-q'+next_qi+'[]" class="answer" value="'+d.keterangan+'">';
				  answer += '<input type="text" name="skor-q'+next_qi+'[]" placeholder="score" class="score" value="'+d.skor+'">';
				  answer += '<input type="hidden" name="isbenar-q'+next_qi+'[]" class="isright is-benar-q'+next_qi+'-'+(i+1)+'" value="'+d.is_benar+'">';
				  answer += '<span class="isoption is-option-q'+next_qi+'-'+(i+1)+'">';
				  if(d.is_benar=='1'){
				     answer += '&nbsp;<span class="label label-info">Right Answer</span>';
				  }
				  answer += '</span>';
				  if((i+1)!=1){
				     answer += '<a href="javascript::" class="remove-answer remove-answer-q'+next_qi+'-'+(i+1)+'"> Remove Answer</a>';
				  }
				}else{
					$('.randomanswer-question-'+next_qi).hide();
					answer += '<textarea name="answer-q'+next_qi+'[]" placeholder="Answer" class="answer form-control">'+d.keterangan+'</textarea>';
					$('.response-'+next_qi).hide();
				}
				answer += '</div>';
				count = count + 1;
        	});
        	answer += '<input type="hidden" name="count-answer[]" value="'+count+'" class="answercount answer-count-area-'+next_qi+'"/>';
        	answer += '</span>';
			$('.answer-option-area').append(answer);
        }
    });
    
	$('.add-response-box').append('<a href="javascript::" class="btn btn-danger add-response response-'+next_qi+'">Add Response</a>');
	last_selected = kathid_id;
	$('#QuestionBankModal').modal('hide');
}

/*-----New Question-------------------*/
$(document).on( 'click', '.add-question', function(){
	var qi = $('.menu-question-number li').length;
	var next_qi = qi+1;
	for(var i = 1;i <= qi; i++){
		$('#test-number-'+i).removeClass('active bottom-child');
		hide_quest(i);
	}
	$('.remove-question-box').append('<a class="btn btn-default remove-question remove-question-'+next_qi+'">Remove Question</a>');
	$('.menu-question-number').append('<li class="bottom-child active" id="test-number-'+next_qi+'"><a href="javascript::" id="link-question-'+next_qi+'">'+next_qi+'</a></li>');
	var selectbox = '';
	$.ajax({
        url : base_url + 'module/elearning/quiz/get_test_type',
        type: "POST",
        dataType : "html",
        success:function(data) 
        {
        	selectbox += '<select class="form-control typetest-question-'+next_qi+'" name="select-test[]">';
        	selectbox += data;
        	selectbox += '</select>';
        	$('.select-box').append(selectbox);
        }
    });
	$('.points-box').append('<input type="text" class="form-control point-question-'+next_qi+'" required="required" name="point[]" />');
	$('.question-box').append('<textarea name="question[]" class="form-control question-question-'+next_qi+'"></textarea>');
	
	var randomanswer = '';
	randomanswer += '<label class="randomanswer-question-'+next_qi+'">';
	randomanswer += '<input type="checkbox" value="" name="random-answer-q'+next_qi+'">';
	randomanswer += 'Random Response';
	randomanswer += '</label>';
	$('.randomanswer-box').append(randomanswer);
	
	var answer = '';
	answer += '<span class="area answer-option-area-'+next_qi+'">';
	answer += '<input type="hidden" name="count-answer[]" value="1" class="answercount answer-count-area-'+next_qi+'"/>';
	answer += '<div class="radio radio-q'+next_qi+'">';
	answer += '<label>';
	answer += '<input type="radio" name="options-q'+next_qi+'" value="q'+next_qi+'-1" class="option-check">';
	answer += '</label>';
	answer += '<input type="text" name="answer-q'+next_qi+'[]" class="answer">';
	answer += '<input type="text" name="skor-q'+next_qi+'[]" placeholder="score" class="score">';
	answer += '<input type="hidden" name="isbenar-q'+next_qi+'[]" class="isright is-benar-q'+next_qi+'-1">';
	answer += '<span class="isoption is-option-q'+next_qi+'-1"></span>';
	answer += '</div>';
	answer += '</span>';
	$('.answer-option-area').append(answer);
	
	$('.add-response-box').append('<a href="javascript::" class="btn btn-danger add-response response-'+next_qi+'">Add Response</a>');
	last_selected = 'eb430fe'; //---new select
});

function show_quest(i){
	var li_list = $('.menu-question-number li').length;
	var active = $('.menu-question-number li.active').attr('id').split('-')[2];
	var type = $('.typetest-question-'+active).find(":selected").data('input');
	$('.typetest-question-'+i).show();
	$('.point-question-'+i).show();
	if(li_list>1){
		$('.remove-question-'+i).show();
	}else{
		$('.remove-question-'+i).hide();
	}
	$('.question-question-'+i).show();
	$('.answer-option-area-'+i).show();
	$('.answer-count-area-'+i).show();
	if(type!='textarea'){
		$('.response-'+i).show();
		$('.randomanswer-question-'+i).show();
	}
	last_selected = $('.typetest-question-'+i).find('option:selected').val();
}

function hide_quest(i){
	$('.typetest-question-'+i).hide();
	$('.point-question-'+i).hide();
	$('.remove-question-'+i).hide();
	$('.question-question-'+i).hide();
	$('.randomanswer-question-'+i).hide();
	$('.answer-option-area-'+i).hide();
	$('.answer-count-area-'+i).hide();
	$('.response-'+i).hide();
}

/*-----If select exam/schedule-------*/
$(document).on( 'change', 'select[name="select-jadwal"]', function(){
	var mk_id	= $(this).find('option:selected').data('mk');
	$.ajax({
        url : base_url + 'module/elearning/quiz/get_materi',
        type: "POST",
        dataType : "JSON",
        data : $.param({
        	mk_id : mk_id
        }),
        success:function(data) 
        {
        	var output = '';
        	if(JSON.stringify(data)!='""'){
        		output += '<option value="0">No Topic</option>';
        		$.each(data ,function(i,d) {
        			output += '<option value='+d.materi_id+'>'+d.judul+'</option>';
        		});
        		
        		$('select[name="select-materi"]').html(output);
        		$('.material-assign').fadeIn();
        	}else{
        		$('.material-assign').fadeOut();
        	}
        }
    });
});

/*-----If select new type answer-------*/
$(document).on( 'change', 'select[name="select-test[]"]', function(){
	// alert(last_selected);
	var active = $('.menu-question-number li.active').attr('id').split('-')[2];
	var sum_answer = 0;
	for(var i=0;i<$('.answer-option-area-'+active).find('.answer').length;i++){
		var answer = $('.answer-option-area-'+active).find('.answer');
		sum_answer = sum_answer + ($(answer[i]).val().length);
	}
	if(sum_answer>0){
		var x = confirm('Do you really want to change the question type? \nIf you select \'Ok\' existing question responses will be lost.');
		if(x){
			change_question_type(active, this);
			last_selected = $(this).val();
		}else{
			$(this).val(last_selected);
		}
	}else{
		change_question_type(active, this);
		last_selected = $(this).val();
	}
});

function change_question_type(active, e){
	var output = '';
	$('.answer-option-area-'+active).empty();
	var type = $(e).find('option:selected').data('input');
	output += '<input type="hidden" name="count-answer[]" value="1" class="answercount answer-count-area-'+active+'"/>';
	output += '<div class="radio radio-q'+active+'">';
	if(type!='textarea'){
		$('.randomanswer-question-'+active).show();
		output += '<label><input type="'+type+'" name="options-q'+active+'" value="q'+active+'-1" class="option-check"></label>';
		output += '<input type="text" name="answer-q'+active+'[]" class="answer">';
		output += '<input type="text" name="skor-q'+active+'[]" placeholder="score" class="score">';
		output += '<input type="hidden" name="isbenar-q'+active+'[]" class="isright is-benar-q'+active+'-1">';
		output += '<span class="isoption is-option-q'+active+'-1"></span>';
		$('.response-'+active).show();
	}else{
		$('.randomanswer-question-'+active).hide();
		output += '<textarea name="answer-q'+active+'[]" placeholder="Answer" class="answer form-control"></textarea>';
		$('.response-'+active).hide();
	}
	output += '</div>';
	$('.answer-option-area-'+active).html(output);
}

/*-----Give right answer label-------*/
$(document).on( 'click', '.option-check', function(){
	var active = $('.menu-question-number li.active').attr('id').split('-')[2];
	var type = $('.typetest-question-'+active).find(":selected").data('input');
	if(type=='radio'){
		var options_length = $('.option-check').length;
		for(i=1;i<=options_length;i++){
			$('.is-option-q'+active+'-'+i).empty();
			$('.is-benar-q'+active+'-'+i).val('0');
		}
		var id = $(this).val().split('-')[1];
		$('.is-option-q'+active+'-'+id).html('&nbsp;<span class="label label-info">Right Answer</span>');
		$('.is-benar-q'+active+'-'+id).val('1');
	}
	else if(type=='checkbox'){
		var id = $(this).val().split('-')[1];
		if( $(this).is(':checked') ){
			$('.is-option-q'+active+'-'+id).html('&nbsp;<span class="label label-info">Right Answer</span>');
			$('.is-benar-q'+active+'-'+id).val('1');
		}else{
			$('.is-option-q'+active+'-'+id).empty();
			$('.is-benar-q'+active+'-'+id).val('0');
		}
	}
});

/*-----Add new answer-------*/
$(document).on( 'click', '.add-response', function(){
	var output = '';
	var active = $('.menu-question-number li.active').attr('id').split('-')[2];
	var answer_length = $('.radio-q'+active).length;
	var type = $('.typetest-question-'+active).find(":selected").data('input');
	
	var answercount = $('.answer-count-area-'+active).val();
	$('.answer-count-area-'+active).val((Number(answercount)+1));
	output += '<div class="radio radio-q'+active+'">';
	output += '<label><input type="'+type+'" name="options-q'+active+'" value="q'+active+'-'+(answer_length+1)+'" class="option-check"></label>';
	output += '<input type="text" name="answer-q'+active+'[]" class="answer">';
	output += '<input type="text" name="skor-q'+active+'[]" placeholder="score" class="score">';
	output += '<span class="isoption is-option-q'+active+'-'+(answer_length+1)+'"></span>';
	output += '<a href="javascript::" class="remove-answer remove-answer-q'+active+'-'+(answer_length+1)+'"> Remove Answer</a>';
	output += '<input type="hidden" name="isbenar-q'+active+'[]" class="isright is-benar-q'+active+'-'+(answer_length+1)+'">';
	output += '</div>';
	$('.answer-option-area-'+active).append(output);
});

/*-----Delete Answer--------*/
$(document).on( 'click', '.remove-answer', function(){
	var active = $('.menu-question-number li.active').attr('id').split('-')[2];
	var type = $('.typetest-question-'+active).find(":selected").data('input');
	var areaclass = $('.answer-option-area-'+active).attr('class').split(' ')[1];
	var area = $('.'+areaclass)
	$(this).parent().remove();
	
	var answercount = $('.answer-count-area-'+active).val();
	$('.answer-count-area-'+active).val((Number(answercount)-1));
	
	for(var i=0;i<$(area).find('.radio').length;i++){
		var radio = $(area).find('.radio');
		$(radio[i]).removeAttr('class');
		$(radio[i]).addClass('radio radio-q'+active);
	}
	for( var y=0;y<$(area).find('input[type="'+type+'"]').length;y++ ){
		var optradio = $(area).find('input[type="'+type+'"]');
		var value = 'q'+active+'-'+(y+1)+'';
		var name  = 'options-q'+active;
		$(optradio[y]).val(value);
		$(optradio[y]).attr('name', name);
	}
	$.each($(area).find('.isright'), function(z, e) {
		$(e).removeAttr('class');
		$(e).addClass('isright is-benar-q'+active+'-'+(z+1)+'');
	});
	$.each($(area).find('.isoption'), function(a, e) {
		$(e).removeAttr('class');
		$(e).addClass('isoption is-option-q'+active+'-'+(a+1)+'');
	});
	$.each($(area).find('.remove-answer'), function(b, e) {
		$(e).removeAttr('class');
		$(e).addClass('remove-answer remove-answer-q'+active+'-'+(b+2)+'');
	});
});

/*-----Delete Question--------*/
$(document).on( 'click', '.remove-question', function(){
	var middle = 0;
	var active = $('.menu-question-number li.active').attr('id').split('-')[2];
	var list   = $('.menu-question-number li.active').attr('class').split(' ');
	
	if(jQuery.inArray('bottom-child',list) == 0){
	    var del_li_pos = 'bottom';
	}
	else if(jQuery.inArray('top-child',list) == 0){
		var del_li_pos = 'top';
	}
	else{
		var del_li_pos = 'middle';
	}

	$('#test-number-'+active).remove();
	$('.typetest-question-'+active).remove();
	$('.point-question-'+active).remove();
	$('.remove-question-'+active).remove();
	$('.question-question-'+active).remove();
	$('.randomanswer-question-'+active).remove();
	$('.answer-option-area-'+active).remove();
	$('.answer-count-area-'+active).remove();
	$('.response-'+active).remove();
	
	$.each( $('.menu-question-number li'), function(i, f) {
		$(f).removeAttr('id');
		$(f).attr('id','test-number-'+(i+1));
		$(f).find('a').removeAttr('id');
		$(f).find('a').attr('id','link-question-'+(i+1));
		$(f).find('a').text(i+1);
	});
	$('#test-number-'+active).addClass('active');
	
	$.each( $('.select-box select'), function(i, f) {
		$(f).removeAttr('class');
		$(f).addClass('form-control typetest-question-'+(i+1));
	});
	
	$.each( $('.points-box input'), function(i, f) {
		$(f).removeAttr('class');
		$(f).addClass('form-control point-question-'+(i+1));
	});
	
	$.each( $('.remove-question-box a'), function(i, f) {
		$(f).removeAttr('class');
		$(f).addClass('btn btn-default remove-question remove-question-'+(i+1)+'');
	});
	
	$.each( $('.question-box textarea'), function(i, f) {
		$(f).removeAttr('class');
		$(f).addClass('form-control question-question-'+(i+1));
	});
	
	$.each( $('.randomanswer-box label'), function(i, f) {
		$(f).removeAttr('class');
		$(f).addClass('randomanswer-question-'+(i+1));
	});
	
	$.each( $('.randomanswer-box input'), function(i, f) {//------------------------------------------
		$(f).attr('name', 'random-answer-q'+(i+1));
	});
	
	$.each( $('.add-response-box a'), function(i, f) {
		$(f).removeAttr('class');
		$(f).addClass('btn btn-danger add-response response-'+(i+1));
	});
		
	if(del_li_pos=='bottom'){
		$.each( $('.area'), function(i, f) {
			$(f).removeAttr('class');
			$(f).addClass('area answer-option-area-'+(i+1));
			
			$.each( $('.radio'), function(i, f) {
				$(f).removeAttr('class');
				$(f).addClass('radio radio-q'+(i+1));
			});
		});
		
		var type = $('.typetest-question-'+(active-1)).find(":selected").data('input');
		var position = $('#test-number-'+(active-1)).attr('class');
		if(position=='top-child'){
			$('#test-number-'+(active-1)).addClass('active');
		}else{
			$('#test-number-'+(active-1)).addClass('bottom-child active');
		}
		
		show_quest((active-1));
	}
	else{
		var type = $('.typetest-question-'+active).find(":selected").data('input');
		var area = $('.area');
		for(var i=0;i<area.length;i++){
			$(area[i]).removeAttr('class');
			$(area[i]).addClass('area answer-option-area-'+(i+1));
			
			for( var b=0;b<$(area[i]).find('.answercount').length;b++ ){
				var acount = $(area[i]).find('.answercount');
				$(acount[b]).removeAttr('class');
				$(acount[b]).addClass('answercount answer-count-area-'+(i+1));
			}
			for( var x=0;x<$(area[i]).find('.radio').length;x++ ){
				var radio = $(area[i]).find('.radio');
				$(radio[x]).removeAttr('class');
				$(radio[x]).addClass('radio radio-q'+(i+1));
			}
			for( var c=0;c<$(area[i]).find('.answer').length;c++ ){
				var answername = $(area[i]).find('.answer');
				var aname  = 'answer-q'+(i+1)+'[]';
				$(answername[c]).attr('name', aname);
			}
			for( var d=0;d<$(area[i]).find('.score').length;d++ ){
				var scorename = $(area[i]).find('.score');
				var sname  = 'skor-q'+(i+1)+'[]';
				$(scorename[d]).attr('name', sname);
			}
			for( var y=0;y<$(area[i]).find('input[type="'+type+'"]').length;y++ ){
				var optradio = $(area[i]).find('input[type="'+type+'"]');
				var value = 'q'+(i+1)+'-'+(y+1)+'';
				var name  = 'options-q'+(i+1);
				$(optradio[y]).val(value);
				$(optradio[y]).attr('name', name);
			}
			for( var z=0;z<$(area[i]).find('.isright').length;z++ ){
				var isright = $(area[i]).find('.isright');
				var irname  = 'isbenar-q'+(i+1)+'[]';
				$(isright[z]).removeAttr('class');
				$(isright[z]).addClass('isright is-benar-q'+(i+1)+'-'+(z+1)+'');
				$(isright[z]).attr('name', irname);
			}
			for( var a=0;a<$(area[i]).find('.isoption').length;a++ ){
				var isoption = $(area[i]).find('.isoption');
				$(isoption[a]).removeAttr('class');
				$(isoption[a]).addClass('isoption is-option-q'+(i+1)+'-'+(a+1)+'');
			}
			for( var e=0;e<$(area[i]).find('.remove-answer').length;e++ ){
				var remo = $(area[i]).find('.remove-answer');
				$(remo[e]).removeAttr('class');
				$(remo[e]).addClass('remove-answer remove-answer-q'+(i+1)+'-'+(e+2)+'');
			}
		}
		
		if(del_li_pos=='top'){
			show_quest(active);
			$('#test-number-'+(active)).removeAttr('class');
			$('#test-number-'+(active)).addClass('top-child active');
		}else{
			show_quest(active);
		}
		
	}
});

$(document).on('blur', 'input[name="finish-date"]', function() {
	var tgl_mulai_ = $('input[name="start-date"]').val();
	var tgl_selesai_ = $(this).val();
	
	if(tgl_mulai_ > tgl_selesai_) {
		alert('Date unavailable');
		$(this).val('');
	}else if(tgl_selesai_=='') {
		$(this).val('');
	}
	else {
		
	}
});

function save(i){
		$('#form-quiz').submit(function (e) {
		var formData = new FormData($(this)[0]);
		var URL = base_url + 'module/elearning/quiz/save';
		formData.append("is_publish", i);
	      $.ajax({
	        url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	        	if(data=='Select quiz type first'){
	        		alert(data);
	        	}
	        	else{
	        		// alert(data);
	        		alert('Upload Success');
	        		location.href=base_url + 'module/elearning';
	        	}
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
}