$(document).ready(function(){

  $('#tagihan-wrap, #bayar-inp').hide();
  $('.alert').delay( 800 ).slideUp();

  $("#add-tagihan").click(function(){
    $('.main-wrap').slideUp();
    $('#tagihan-wrap').slideDown();
  });

  $("#cara_bayar").change(function(){
    var nilai = $(this).val();
    if(nilai == 'tunai') {
      $("#no_kartu").attr('disabled','');
      $("#no_kartu").val('');
    }
    else $("#no_kartu").removeAttr('disabled');
  });

  $("#btn-close").click(function(){
    $('.main-wrap').slideDown();
    $('#tagihan-wrap').slideUp();
  });

  $("#add-bayar").click(function(){
    $('#bayar-table').slideUp();
    $('#bayar-inp').slideDown();
  });

  $("#batal-bayar").click(function(){
    $('#bayar-table').slideDown();
    $('#bayar-inp').slideUp();
  });

  $(".row-data").mouseenter(function(){
    var id = $(this).data('id');
    $(".pop[data-id='"+id+"']").popover('show');
  });

  $(".row-data").mouseleave(function(){
    var id = $(this).data('id');
    $(".pop[data-id='"+id+"']").popover('hide');
  });

  $("#add-item").click(function(){
    var nilai = $("#id-item").val();
    var data = nilai.split("|");
    var number = parseInt($('#number').val());

    if(nilai != '' && cek_item(data[0])) {
      $("#number").val(number+1);
      var tes = '<tr class="row-item" data-id="'+data[0]+'"><td><strong class="number-item">'+ number + '.</strong> ' + data[1];

      tes += '<input name="item_id[]" value="'+data[0]+'" type="hidden" ><input name="harga[]" value="'+data[2]+'" type="hidden" ></td>';
      tes += '<td style="padding-left: 20px;"><input name="qty[]" type="number" class="form-control coba item input-small" value="0" min="0" data-id="'+data[0]+'" data-harga="'+data[2]+'"></td>';
      tes += '<td style="padding-right: 20px; padding-left: 20px"><div class="input-group"><input name="disc[]" step="5" type="number" class="form-control diskon input-small" value="0" style="width: 100%" min="0" max="100" data-id="'+data[0]+'" ><span class="input-group-addon">%</span></div></td>';
      tes += '<td style="padding-left: 20px">'+rp(parseInt(data[2]))+'</td>';
      tes += '<td><span class="diskon-total" data-id="'+data[0]+'">Rp. 0</span><input name="total_disc[]" class="diskon-total-inp" value="0" data-id="'+data[0]+'" type="hidden"/></td>';
      tes += '<td><span class="sub-total" data-id="'+data[0]+'">Rp. 0</span><input name="total_tagihan[]" class="sub-total-inp" value="0" data-id="'+data[0]+'" type="hidden"/></td>';
      tes += '<td><span class="all-total" data-id="'+data[0]+'">Rp. 0</span><input class="all-total-inp" value="0" data-id="'+data[0]+'" type="hidden"/></td>';
      tes += '<td class="text-center"><a href="#" class="btn-danger btn del-item" data-id="'+data[0]+'"><i class="fa fa-trash-o"></i></a></td></tr>';

      $("#table-item").append(tes);
      $("#notif").remove();

      $('.item').change(function(){
        var id = $(this).data('id');
        var harga = $(this).data('harga');
        var jml = $(this).val();
        var total = 0;
        var diskon_inp = parseInt($(".diskon[data-id='"+id+"']").val());

        $(".sub-total[data-id='"+id+"']").html(rp(harga*jml));
        $(".sub-total-inp[data-id='"+id+"']").val(harga*jml);

        var sub_total = $(".sub-total-inp[data-id='"+id+"']").val(); 
        var diskon = $(".diskon-total-inp[data-id='"+id+"']").val(); 

        $(".diskon-total[data-id='"+id+"']").html(rp(sub_total*diskon_inp/100));
        $(".diskon-total-inp[data-id='"+id+"']").val(sub_total*diskon_inp/100);

        $(".all-total[data-id='"+id+"']").html(rp(sub_total - diskon));
        $(".all-total-inp[data-id='"+id+"']").val(sub_total - diskon);

      });

      $('.diskon').change(function(){
        var id = $(this).data('id');
        var jml = $(this).val();
        var total = 0;

        var sub_total = $(".sub-total-inp[data-id='"+id+"']").val();

        if(sub_total == 0) $(this).val(0);
        else{
          $(".diskon-total[data-id='"+id+"']").html(rp(sub_total*jml/100));
          $(".diskon-total-inp[data-id='"+id+"']").val(sub_total*jml/100);

          var diskon = $(".diskon-total-inp[data-id='"+id+"']").val(); 

          $(".all-total[data-id='"+id+"']").html(rp(sub_total - diskon));
          $(".all-total-inp[data-id='"+id+"']").val(sub_total - diskon);

          update_total(id);
        }
      });

      $(".del-item").click(function(){
        var id = $(this).data('id');
        var total = 0;
        $(".row-item[data-id='"+id+"']").remove();

        var number = 1;
        $(".number-item").each(function(){
          $(this).html(number+'. ');
          number++;
        });
        $("#number").val(number);

        update_total(id);
      });
    }
    update_total();
  }); //end add-item      

});

function sub_total(){
  key = String.fromCharCode( key );
  alert(key);
}

function cek_item(id){
  var is_bentrok = 0;
  $('.row-item').each(function(){
    id_temp = $(this).data('id');
    if(id_temp == id) {
      is_bentrok = 1;
    }
  });

  if(is_bentrok == 0) return true;
  else return false;
}

function update_total(id){
  var total = 0;
  $('.all-total-inp').each(function(){
    total = total + parseInt($(this).val());
  });

  $('.total').html('<h4>' + rp(total) + '</h4>');
  $('.total-inp').val(total);
}

function rp(n) {
    return "Rp. " + n.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
}