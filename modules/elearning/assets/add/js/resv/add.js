$(document).ready(function(){
	$(".btn-room").click(function(){
		$('#nama').html('<i class="fa fa-tags"></i> ' + $(this).data('nama'));
        $('#harga').html('<i class="fa fa-bookmark fa-fw"></i> <strong>Harga</strong> : ' + $(this).data('hargarp'));
        $('#note').html($(this).data('note'));
        $('#itemid').val($(this).data('itemid'));
        $('#harga_inp').val($(this).data('harga'));


        var url = window.location.href.split('/');
        var baseUrl = url[0] + '//' + url[2] + '/' + url[3] + '/assets/pic/'; 
        var foto = $(this).data('foto');
        $("#foto").attr('src', baseUrl + foto);

		$('#room-wrap').slideToggle(500);
		$('#input-wrap').slideToggle(500);
	});
});