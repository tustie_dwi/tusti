$(document).ready(function(){
  $('#max').popover({
    'html' : true
  });

  $('.cal-2, #close, #form-add').hide();

  $('#add').click(function(){
    $('#add').hide();
    $('#form-add').slideDown();
    $('#close').show();
  });

  $('#close').click(function(){
    $('#close').hide();
    $('#form-add').slideUp();
    $('#add').show();
  });

  $('.inp_val').change(function(){
    var tahun_ = $('#tahun_inp').val();
    var bulan_ = $('#bulan').val();
    var kategori_ = $('#kategori').val();
    var uri = $("#uri").val();

    if(tahun_ != '' && bulan_ != '' && kategori_ != ''){
      $.ajax({
        type : 'POST',
        dataType : 'html',
        url : uri,
        data : $.param({tahun : tahun_, bulan : bulan_, kategori : kategori_}),
        success:function(msg){
          var nilai = msg.split("|");

          $("#debit").val(nilai[0]);
          $("#kredit").val(nilai[1]);
          $("#balance").val(nilai[2]);
        }
      });
    }

  });

  $('.alert').delay( 800 ).slideUp();

  $('.inp').change(function(){
    document.getElementById('form_inp').submit();
  });

});