$(document).ready(function(){      
  $(".page[data-page!='1']").hide();

  $('#prev').click(function(){
    var page = parseInt($("#page_now").val());
    if(page > 1){
      page = page - 1;
      $("#page_now").val(page);
      $(".page").hide();
      $(".page[data-page='"+page+"']").fadeIn();
    }
  });
  $('#next').click(function(){
    var page = parseInt($("#page_now").val());
    var max = parseInt($("#page_max").val());
    if(page < max){
      page = page + 1;
      $("#page_now").val(page);
      $(".page").hide();
      $(".page[data-page='"+page+"']").fadeIn();
    }
  });

  $('.alert').delay( 800 ).slideUp();

  $('.kamar-link').click(function(){
    var uri = $("#url").val();
    var kamar_id_ = $(this).data('id');

    $.ajax({
      type : 'POST',
      dataType :'html',
      url : uri,
      data : $.param({kamar_id : kamar_id_}),
      success:function(msg){
        $('#daftar-wrap').html(msg);
      }
    }); //end of ajax

    $(".page_2[data-page!='1']").hide();
    $('#prev_2').click(function(){
      var page = parseInt($("#page_now_2").val());
      if(page > 1){
        page = page - 1;
        $("#page_now_2").val(page);
        $(".page_2").hide();
        $(".page_2[data-page='"+page+"']").fadeIn();
      }
    });
    $('#next_2').click(function(){
      var page = parseInt($("#page_now_2").val());
      var max = parseInt($("#page_max_2").val());
      if(page < max){
        page = page + 1;
        $("#page_now_2").val(page);
        $(".page_2").hide();
        $(".page_2[data-page='"+page+"']").fadeIn();
      }
    });
  }); //enf of kamar-liink

});

document.onkeydown = checkKey;
function checkKey(e) {

    e = e || window.event;

    if (e.keyCode == '37') {
      var page = parseInt($("#page_now_2").val());
      if(page > 1){
        page = page - 1;
        $("#page_now_2").val(page);
        $(".page_2").hide();
        $(".page_2[data-page='"+page+"']").fadeIn();
      }
    }
    else if (e.keyCode == '39') {
      var page = parseInt($("#page_now_2").val());
      var max = parseInt($("#page_max_2").val());
      if(page < max){
        page = page + 1;
        $("#page_now_2").val(page);
        $(".page_2").hide();
        $(".page_2[data-page='"+page+"']").fadeIn();
      }
    }
}