$(document).ready(function(){
	$(".child").hide();
	$(".expand").show();

	$('.parent').click(function(){
		var parent_id = $(this).data('parent');

		$("[data-child='"+parent_id+"']").slideToggle();
		
		var caret = $("[data-parent='"+parent_id+"'] i").attr('class');
		if(caret == 'fa fa-caret-down pull-right') $("[data-parent='"+parent_id+"'] i").attr('class','fa fa-caret-right pull-right');
		else $("[data-parent='"+parent_id+"'] i").attr('class','fa fa-caret-down pull-right');
	});
});