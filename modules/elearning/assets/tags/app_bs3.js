var url = window.location.href.split('/');
var baseUrl = url[0] + '//' + url[2] + '/' + url[3] + '/';


elt = $('.example_objects_as_tags > > input');
elt.tagsinput({
  itemValue: 'value',
  itemText: 'text'
});

// elt.tagsinput('add', { "value": 1 , "text": "Amsterdam"   , "continent": "Europe"    });
// elt.tagsinput('add', { "value": 4 , "text": "Washington"  , "continent": "America"   });
// elt.tagsinput('add', { "value": 7 , "text": "Sydney"      , "continent": "Australia" });
// elt.tagsinput('add', { "value": 10, "text": "Beijing"     , "continent": "Asia"      });
// elt.tagsinput('add', { "value": 13, "text": "Cairo"       , "continent": "Africa"    });

elt.tagsinput('input').typeahead({
  valueKey: 'text',
  prefetch: baseUrl + 'setting/c_akses/get_menu/',
  template: '<p>{{text}}</p>',                                       
  engine: Hogan

}).bind('typeahead:selected', $.proxy(function (obj, datum) {  
	this.tagsinput('add', datum);
	this.tagsinput('input').typeahead('setQuery', '');
}, elt));
