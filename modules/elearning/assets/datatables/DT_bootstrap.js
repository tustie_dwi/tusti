//* Set the defaults for DataTables initialisation */
$.extend( true, $.fn.dataTable.defaults, {
	/*"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
	"sPaginationType": "bootstrap",
	"oLanguage": {
		"sLengthMenu": "_MENU_ records per page"
	}*/
	"sDom": "<'row'<'col-sm-12'<'pull-right'f><'pull-left'l>r<'clearfix'>>>t<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>",
    "sPaginationType": "bs_normal",
    "oLanguage": {
        "sLengthMenu": "Show _MENU_ Rows",
        "sSearch": ""
    }
} );





/* Default class modification */
$.extend( $.fn.dataTableExt.oStdClasses, {
	/*"sWrapper": "dataTables_wrapper form-inline",
	"sFilterInput": "form-control",
	"sLengthSelect": "form-control"*/
	"sWrapper": "dataTables_wrapper form-inline"
} );


/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
	return {
		"iStart":         oSettings._iDisplayStart,
		"iEnd":           oSettings.fnDisplayEnd(),
		"iLength":        oSettings._iDisplayLength,
		"iTotal":         oSettings.fnRecordsTotal(),
		"iFilteredTotal": oSettings.fnRecordsDisplay(),
		"iPage":          oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
		"iTotalPages":    oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	};
};


/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
	"bootstrap": {
		"fnInit": function( oSettings, nPaging, fnDraw ) {
			var oLang = oSettings.oLanguage.oPaginate;
			var fnClickHandler = function ( e ) {
				e.preventDefault();
				if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
					fnDraw( oSettings );
				}
			};

			$(nPaging).append(
				'<ul class="pagination">'+
					'<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
					'<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
				'</ul>'
			);
			var els = $('a', nPaging);
			$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
			$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
		},

		"fnUpdate": function ( oSettings, fnDraw ) {
			var iListLength = 5;
			var oPaging = oSettings.oInstance.fnPagingInfo();
			var an = oSettings.aanFeatures.p;
			var i, ien, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

			if ( oPaging.iTotalPages < iListLength) {
				iStart = 1;
				iEnd = oPaging.iTotalPages;
			}
			else if ( oPaging.iPage <= iHalf ) {
				iStart = 1;
				iEnd = iListLength;
			} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
				iStart = oPaging.iTotalPages - iListLength + 1;
				iEnd = oPaging.iTotalPages;
			} else {
				iStart = oPaging.iPage - iHalf + 1;
				iEnd = iStart + iListLength - 1;
			}

			for ( i=0, ien=an.length ; i<ien ; i++ ) {
				// Remove the middle elements
				$('li:gt(0)', an[i]).filter(':not(:last)').remove();

				// Add the new list items and their event handlers
				for ( j=iStart ; j<=iEnd ; j++ ) {
					sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
					$('<li '+sClass+'><a href="#">'+j+'</a></li>')
						.insertBefore( $('li:last', an[i])[0] )
						.bind('click', function (e) {
							e.preventDefault();
							oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
							fnDraw( oSettings );
						} );
				}

				// Add / remove disabled classes from the static elements
				if ( oPaging.iPage === 0 ) {
					$('li:first', an[i]).addClass('disabled');
				} else {
					$('li:first', an[i]).removeClass('disabled');
				}

				if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
					$('li:last', an[i]).addClass('disabled');
				} else {
					$('li:last', an[i]).removeClass('disabled');
				}
			}
		}
	}
} );


/*
 * TableTools Bootstrap compatibility
 * Required TableTools 2.1+
 */
if ( $.fn.DataTable.TableTools ) {
	// Set the classes that TableTools uses to something suitable for Bootstrap
	$.extend( true, $.fn.DataTable.TableTools.classes, {
		"container": "DTTT btn-group",
		"buttons": {
			"normal": "btn btn-default",
			"disabled": "disabled"
		},
		"collection": {
			"container": "DTTT_dropdown dropdown-menu",
			"buttons": {
				"normal": "",
				"disabled": "disabled"
			}
		},
		"print": {
			"info": "DTTT_print_info modal"
		},
		"select": {
			"row": "active"
		}
	} );

	// Have the collection use a bootstrap compatible dropdown
	$.extend( true, $.fn.DataTable.TableTools.DEFAULTS.oTags, {
		"collection": {
			"container": "ul",
			"button": "li",
			"liner": "a"
		}
	} );
}



var oTable;

/* Table initialisation */
$(document).ready(function() {
	 $("#example tbody tr").click( function( e ) {
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }
        else {
            oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
        }
    });
	
	$(".btn-delete-post").click(function(){
	
		var pid = $(this).parents('tr').data("id");
		var mod	= $(this).parents('table').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('tr');
		
		var anSelected = fnGetSelected( oTable );
        if ( anSelected.length !== 0 ) {
            oTable.fnDeleteRow( anSelected[0] );
        }
				
		$.post(
			base_url + mod + '/delete/'+pid,
			
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	$('.myModal').modal({
		keyboard: false
	});
	
	oTable = $('#example').dataTable( {
		//"sDom": "<'row'<'&nbsp;'><'span3'l><'span6'f>r>t<'row'<'span9'i><'span9'p>>",
		/*"sDom": "<'row'<'col-md-3'f><'col-md-9'>r><'table-responsive't><'row'<'col-md-6'><'col-md-6'p>>",
		"sPaginationType": "bootstrap",
		"aaSorting": [[0, 'desc']],
		"oLanguage": {
			"sLengthMenu": "_MENU_ records",
			"sSearch": ""
		}*/
		
		//"sDom": "<'row'<'col-sm-12'<'pull-right'f><'pull-left'>r<'clearfix'>>>t<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>",
		"sDom": "<'row'<'col-md-7'><'col-md-5'f>><'table-responsive't><'row'<'col-md-12'<'pagination-custom'p>>>",
		"sPaginationType": "bootstrap",
			"aaSorting": [[0, 'desc']],
			"oLanguage": {
				"sLengthMenu": "Show _MENU_ Rows",
				"sSearch": ""
			}
	} );
	
	
	$('.example').dataTable( {
		//"sDom": "<'row'<'&nbsp;'><'span3'l><'span6'f>r>t<'row'<'span9'i><'span9'p>>",
		//"sDom": "<'row-fluid'<'span3'l><'span9'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sDom": "<'row'<'col-md-7'><'col-md-5'f>><'table-responsive't><'row'<'col-md-12'<'pagination-custom'p>>>",//<'row'<'col-md-6'><'col-md-6'p>>",
		"sPaginationType": "bootstrap",
		"aaSorting": [[0, 'desc']],
		"oLanguage": {
			"sLengthMenu": "_MENU_ records",
			"sSearch": ""
		}
	} );
	
	$('.file-materi').dataTable( {
		"sDom": "<'row'><'table't><'row'<'col-md-12'<'pagination-custom'p>>>",
		"sPaginationType": "bootstrap",
		"aaSorting": [[0, 'desc']],
		"oLanguage": {
			"sLengthMenu": "_MENU_ records",
			"sSearch": ""
		},
		"aLengthMenu": [[5, 10, 15, 25, 50, 100 , -1], [5, 10, 15, 25, 50, 100, "All"]],
		"iDisplayLength" : 5
	} );
	
	$('.page').dataTable( {
		//"sDom": "<'row'<'col-md-8'><'col-md-2'f>><'table-responsive't><'row'<'col-md-6'><'col-md-6'p>>",
		"sDom": "<'row'<'col-md-7'><'col-md-5'f>><'table-responsive't><'row'<'col-md-12'<'pagination-custom'p>>>",
		"sPaginationType": "bootstrap",
		"aaSorting": [[0, 'desc']],
		"oLanguage": {
			"sLengthMenu": "_MENU_ records",
			"sSearch": ""
		}
	} );
	
	/*$('.dataTable').dataTable({
		"sDom": "<'row-fluid'<'span3'l><'span9'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave": true,
		"oLanguage": {
			"sLengthMenu": "_MENU_ records per page"
		},
		"oTableTools": {
			"sSwfPath": "http://cdnjs.cloudflare.com/ajax/libs/datatables-tabletools/2.1.4/swf/copy_csv_xls_pdf.swf",
			"aButtons": [ "copy", "csv", "xls", "pdf", "print" ]
		}
	});*/

	
	$('#smenu').dataTable( {
		//"sDom": "<'row'<'&nbsp;'><'span6'l><'span3'f>r>t<'row'<'span3'i><'span9'p>>",
		//"sDom": "<'row-fluid'<'span3'l><'span9'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sDom": "<'row'<'col-md-3'l><'col-md-9'f>r><'table-responsive't><'row'<'col-md-6'i><'col-md-6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ records",
			"sSearch": "",
			"aaSorting": [[0, 'desc']]
		}
	} );
	$('.dataTables_length select,.dataTables_filter input').addClass('form-control');
} );

/* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
    return oTableLocal.$('tr.row_selected');
}
