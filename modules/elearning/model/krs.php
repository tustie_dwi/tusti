<?php
class model_krs extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_mkd($mkd_id=NULL, $fak_id=NULL, $cabang_id=NULL, $prodi_id=NULL){ //melihat detail mkd yang bisa dipilih
		$sql = "SELECT 
					DISTINCT
					mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) mkditawarkan_id, 
					vw_jadwal_mkd.keterangan,
					vw_jadwal_mkd.kode_mk,
					vw_jadwal_mkd.kelas, 
					vw_jadwal_mkd.cabang_id, 
					vw_jadwal_mkd.fakultas_id,
					vw_jadwal_mkd.prodi_id, 
					vw_jadwal_mkd.nama_dosen,
					vw_jadwal_mkd.gelar_awal,
					vw_jadwal_mkd.gelar_akhir,
					vw_jadwal_mkd.kuota,
					vw_kuota.jml
				FROM db_ptiik_apps.vw_jadwal_mkd
				LEFT JOIN db_ptiik_apps.vw_kuota ON vw_kuota.jadwal_id = vw_jadwal_mkd.jadwal_id
				WHERE vw_jadwal_mkd.is_praktikum = 0
				";
		
		if($fak_id) $sql .= " AND vw_jadwal_mkd.fakultas_id = '".$fak_id."'";
		if($cabang_id) $sql .= " AND vw_jadwal_mkd.cabang_id = '".$cabang_id."'";
		if($prodi_id) $sql .= " AND vw_jadwal_mkd.prodi_id = '".$prodi_id."'";
		
		if($mkd_id) $sql .= " AND mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '".$mkd_id."'";
		
		$sql .= " ORDER BY vw_jadwal_mkd.keterangan ASC, vw_jadwal_mkd.nama_dosen ASC, vw_jadwal_mkd.kelas ASC";
		
		return $this->db->query($sql);
	}
	
	function get_mk_for_select($fak_id=NULL, $cabang_id=NULL, $prodi_id=NULL){ //melihat list mkd yang di select option
		$sql = "SELECT DISTINCT
					MID(MD5(vw_jadwal_mkd.mkditawarkan_id),9,7) mkditawarkan_id,
					vw_jadwal_mkd.keterangan
				FROM db_ptiik_apps.vw_jadwal_mkd
				WHERE 1 = 1
				";
		if($fak_id!=NULL) $sql .= " AND vw_jadwal_mkd.fakultas_id = '".$fak_id."'";
		if($cabang_id!=NULL) $sql .= " AND vw_jadwal_mkd.cabang_id = '".$cabang_id."'";
		if($prodi_id) $sql .= " AND vw_jadwal_mkd.prodi_id = '".$prodi_id."'";
		
		$sql .= " ORDER by vw_jadwal_mkd.keterangan";
		
		return $this->db->query($sql);
	}
	
	function get_fakultas_id_by_mhs($mhs_id=NULL){ //mendapatkan fakultas_id dari mhs_id yang diinputkan
		$sql = "SELECT tbl_prodi.fakultas_id, tbl_mahasiswa.cabang_id, tbl_mahasiswa.prodi_id
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				WHERE mahasiswa_id = '".$mhs_id."'";
		
		$dt = $this->db->getRow( $sql );
		return $dt;
	}
	
	function get_sks_curr($mhsid){
		$sql = "SELECT MAX(tbl_mhs_ipk.inf_semester) semester FROM db_ptiik_apps.tbl_mhs_ipk WHERE mahasiswa_id = '$mhsid'";
		$data = $this->db->getRow($sql);
		return $data->semester;
	}
	
	function get_krs_id(){ //mendapatkan krs_id yang baru
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(krs_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_krs_tmp WHERE left(krs_id,6)='".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_tahunakademik_aktif(){ //mendapatkan tahun akademik yang aktif saat ini
		$sql = "SELECT tbl_tahunakademik.tahun_akademik 
			FROM `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik 
			WHERE tbl_tahunakademik.is_aktif = '1'";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->tahun_akademik;
		return $strresult;
	}
	
	function get_mk_detail($mkditawarkan_id){
		$sql = "SELECT 
					vw_jadwal_mkd.mkditawarkan_id,
					vw_jadwal_mkd.jadwal_id,
					vw_jadwal_mkd.keterangan,
					vw_jadwal_mkd.kode_mk,
					vw_jadwal_mkd.kelas,
					vw_jadwal_mkd.sks
				FROM db_ptiik_apps.vw_jadwal_mkd
				WHERE MID(MD5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '$mkditawarkan_id'";
				
		return $this->db->getRow($sql);
	}
	
	function add_krs($data=NULL){ //simpan krs
		$this->db->replace('db_ptiik_apps`.`tbl_krs_tmp',$data);
	}
	
	function get_jadwal_by_mk($mkd, $kelas){
		$sql = "SELECT vw_jadwal_mkd.jadwal_id
				FROM db_ptiik_apps.vw_jadwal_mkd
				WHERE vw_jadwal_mkd.mkditawarkan_id = '$mkd' AND vw_jadwal_mkd.kelas = '$kelas'";
		$data = $this->db->getRow($sql);
		return $data->jadwal_id;
	}
	
	function add_mhs_mkd($data=NULL){ //simpan krs
		$this->db->replace('db_ptiik_apps`.`vw_mhs_mkd',$data);
	}
	
	/*function del_mkd_mhs($mhs_id, $mkd, $kelas){
		
		$sql = "DELETE FROM db_ptiik_apps.vw_mhs_mkd WHERE mahasiswa_id = '$mhs_id' AND MID(MD5(mkditawarkan_id),9,7)  = '$mkd' AND kelas = '$kelas'";
		$this->db->query($sql);
	}*/
	
	function del_mkd_mhs($id){
		
		$sql = "DELETE FROM db_ptiik_apps.tbl_krs_tmp WHERE MID(MD5(krs_id),9,7)  = '$id' ";
		$this->db->query($sql);
	}
	
	function get_tgl_krs($tgl=NULL){ //mendapatkan tgl_mulai dan tgl_selesainya KRS-an
		$sql ="SELECT 
				tbl_kalenderakademik.tgl_mulai tgl_mulai, 
				tbl_kalenderakademik.tgl_selesai tgl_selesai
			FROM `db_ptiik_apps`.`tbl_kalenderakademik` tbl_kalenderakademik
			LEFT JOIN `db_ptiik_apps`.`tbl_jeniskegiatan` tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_kalenderakademik.jenis_kegiatan_id
			WHERE tbl_kalenderakademik.is_aktif = '1'
				AND lcase(tbl_jeniskegiatan.keterangan) = 'krs'";
			if($tgl) $sql.="AND ('".$tgl."' BETWEEN db_ptiik_apps.tbl_kalenderakademik.tgl_mulai AND db_ptiik_apps.tbl_kalenderakademik.tgl_selesai)";
			
			return $this->db->getRow($sql);
	}
	
	function  get_krs_mhs($mhs_id=NULL, $semester=NULL){
		$sql = "SELECT 
					 mid(md5(tbl_krs_tmp.krs_id),9,7) AS id, 
					 mid(md5(tbl_krs_tmp.mahasiswa_id),9,7) AS mid, 
					tbl_krs_tmp.krs_id,
					tbl_krs_tmp.mahasiswa_id,
					tbl_krs_tmp.mkditawarkan_id,
					tbl_krs_tmp.nama_mk as keterangan,
					tbl_krs_tmp.kode_mk,
					tbl_krs_tmp.is_approve,
					tbl_krs_tmp.sks,
					tbl_krs_tmp.tahun_akademik,
					tbl_krs_tmp.kelas,
					tbl_tahunakademik.is_aktif
					FROM
					db_ptiik_apps.tbl_krs_tmp
					INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_krs_tmp.tahun_akademik = tbl_tahunakademik.tahun_akademik
					 WHERE tbl_tahunakademik.is_aktif = '1' 
					";
		if($mhs_id) $sql.= " AND (tbl_krs_tmp.mahasiswa_id = '$mhs_id' OR (mid(md5(tbl_krs_tmp.mahasiswa_id),9,7) = '$mhs_id') ) ";
		
		if($semester) $sql.= " AND (tbl_krs_tmp.tahun_akademik = '$semester' ) ";
		
		return $this->db->query($sql);
	}
	
	function get_mhs_by_pa($dosenid){ //mendapatkan mahasiswa yg diampu oleh dosen yg login
		$sql = "SELECT 
					tbl_mahasiswa.nama, 
					tbl_mahasiswa.nim, 
					tbl_mahasiswa.angkatan, 
					tbl_mahasiswa.tmp_lahir, 
					tbl_mahasiswa.tgl_lahir, 
					tbl_mahasiswa.alamat,
					tbl_mahasiswa.hp,
					mid(md5(tbl_mahasiswa.mahasiswa_id),9,7) mahasiswa_id,					
					tbl_mahasiswa.foto as foto_mhs,
					tbl_mahasiswa.is_aktif as status,
					(SELECT tbl_prodi.keterangan FROM db_ptiik_apps.tbl_prodi tbl_prodi WHERE tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id) as prodi,
					(SELECT tbl_prodi.strata FROM db_ptiik_apps.tbl_prodi tbl_prodi WHERE tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id) as strata,
					(SELECT tbl_cabang.keterangan FROM db_ptiik_apps.tbl_cabang tbl_cabang WHERE tbl_cabang.cabang_id = tbl_mahasiswa.cabang_id) as cabang
				FROM `db_ptiik_apps`.`tbl_dosen_pembimbing`
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa`ON  tbl_mahasiswa.mahasiswa_id = tbl_dosen_pembimbing.mahasiswa_id
				WHERE tbl_dosen_pembimbing.karyawan_id = '".$dosenid."'
				ORDER by tbl_mahasiswa.nama";
		return $this->db->query($sql);
	}
	
	function get_ip_terakhir($mhs_id){ //mendapatkan semester mhs saat ini
		$sql = "SELECT tbl_mhs_ipk.total_sks as `jml_sks`,tbl_mhs_ipk.ip, tbl_mhs_ipk.ipk
				FROM `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk
				WHERE (mid(md5(tbl_mhs_ipk.mahasiswa_id),9,7) = '".$mhs_id."' OR tbl_mhs_ipk.mahasiswa_id='$mhs_id')
				ORDER BY tbl_mhs_ipk.tahun_akademik DESC
				LIMIT 0,1";
		
		return $this->db->getRow($sql);
	}
	
	function rekap_ipk($mhs_id=NULL){
		$sql = "SELECT tbl_mhs_ipk.ip
				FROM `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_mhs_ipk.tahun_akademik
				WHERE tbl_tahunakademik.is_aktif <> 1 AND tbl_mhs_ipk.mahasiswa_id = '".$mhs_id."'
				ORDER BY tbl_mhs_ipk.tahun_akademik DESC
				LIMIT 0,1";
		$data = $this->db->getRow($sql);
		if($data) return $data;
		else {
			$sql = "SELECT 
					tbl_krs.inf_semester, 
					SUM(tbl_matakuliah.sks * tbl_krs.inf_bobot)/SUM(tbl_matakuliah.sks) ip, 
					SUM(tbl_matakuliah.sks) jml_sks
				FROM `db_ptiik_apps`.`tbl_krs` tbl_krs
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_krs.mkditawarkan_id
				LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik =  tbl_krs.inf_semester
				WHERE tbl_krs.mahasiswa_id = '".$mhs_id."' AND tbl_tahunakademik.is_aktif <> 1
				GROUP BY tbl_krs.inf_semester
				ORDER BY tbl_mkditawarkan.tahun_akademik DESC";		
				
			$data = $this->db->query($sql);
		
			
			$ipk = 0;
			$total_ip = 0;
			$count = 0;
			foreach($data as $key){ //proses hitung ipk
				if($key->ip != ''){
					$count++;
					$total_ip += $key->ip;
					$ipk = $total_ip / $count;
					
					$data_save = array( //simpan ke tabel mhs_ipk
						'mahasiswa_id' => $mhs_id,
						'tahun_akademik' => $key->inf_semester,
						'inf_semester' => ($count),
						'ipk' => $ipk,
						'ip' => $key->ip,
						'total_sks' => $key->jml_sks
					);	
					
					$this->db->replace('db_ptiik_apps`.`tbl_mhs_ipk', $data_save);
				}
			}
		}
	}
	
	function get_mhsid($mhs_id){
		$sql = "SELECT mahasiswa_id 
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa 
				WHERE mid(md5(tbl_mahasiswa.mahasiswa_id),9,7) = '".$mhs_id."'";
		
		$data = $this->db->getRow($sql);
		return $data->mahasiswa_id;
	}
	
	
}

?>