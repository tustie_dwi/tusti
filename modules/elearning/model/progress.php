<?php
class model_progress extends model {
	private $coms;
	
	function get_persen_progress($mhs=NULL, $mk=NULL){
		$sql = "SELECT
				tbl_post_nilai.mkditawarkan_id,
				tbl_post_nilai_mhs.mahasiswa_id,
				Count(tbl_post_nilai_mhs.total_skor) as `total`
				FROM
				db_ptiik_apps.tbl_post_nilai
				LEFT JOIN db_ptiik_apps.tbl_post_nilai_mhs ON tbl_post_nilai_mhs.post_nilai_id = tbl_post_nilai.post_nilai_id
				where tbl_post_nilai.kategori_nilai <> 'khs' ";
		if($mhs) $sql.= " AND tbl_post_nilai_mhs.mahasiswa_id = '$mhs' ";
		if($mk) $sql.= " AND MID(MD5(tbl_post_nilai.mkditawarkan_id),9,7) = '$mk' ";
		
		$sql.= "GROUP BY
				tbl_post_nilai.mkditawarkan_id,
				tbl_post_nilai_mhs.mahasiswa_id";
		
		return $this->db->getRow($sql);

	}
	
	// function get_progress($mkd){
		// $sql = "SELECT DISTINCT
					// tbl_post_nilai.post_nilai_id,
					// tbl_post_nilai.judul,
					// vw_mhs_mkd.nama,
					// vw_mhs_mkd.foto,
					// vw_mhs_mkd.mahasiswa_id,
					// tbl_post_nilai_mhs.link_id,
					// tbl_post_nilai_mhs.tbl_name as `kategori_nilai`,
					// tbl_post_nilai_mhs.skor,
					// tbl_post_nilai_mhs.total_skor,
					// tbl_post_nilai.kategori_nilai as `kategori`,
					// tbl_post_nilai_mhs.status,
					// tbl_post_nilai_mhs.tgl_post tgl_pengumpulan
				// FROM db_ptiik_apps.tbl_post_nilai
				// LEFT JOIN db_ptiik_apps.vw_mhs_mkd ON vw_mhs_mkd.mkditawarkan_id = tbl_post_nilai.mkditawarkan_id
				// INNER JOIN db_coms.coms_user ON vw_mhs_mkd.mahasiswa_id = coms_user.mahasiswa_id
				// LEFT JOIN db_ptiik_apps.tbl_post_nilai_mhs ON tbl_post_nilai_mhs.post_nilai_id = tbl_post_nilai.post_nilai_id AND tbl_post_nilai_mhs.mahasiswa_id = vw_mhs_mkd.mahasiswa_id
				// WHERE MID(MD5(tbl_post_nilai.mkditawarkan_id),9,7) = '$mkd'
				// ORDER BY tbl_post_nilai.tgl_mulai DESC";
// 		
		// return $this->db->query($sql);
	// }
	
	function get_progress($mkd, $jadwal){
		$sql = "SELECT 
					DISTINCT tbl_post_nilai.post_nilai_id, 
					tbl_post_nilai.judul, 
					vw_mhs_mkd.nama, 
					vw_mhs_mkd.foto,
					vw_mhs_mkd.mahasiswa_id, 
					tbl_post_nilai_mhs.link_id, 
					tbl_post_nilai_mhs.tbl_name AS `kategori_nilai`, 
					tbl_post_nilai_mhs.skor, 
					tbl_post_nilai_mhs.total_skor, 
					tbl_post_nilai.kategori_nilai AS `kategori`, 
					tbl_post_nilai_mhs.status, 
					tbl_post_nilai_mhs.tgl_post tgl_pengumpulan
				FROM db_ptiik_apps.tbl_post_nilai 
				LEFT JOIN db_ptiik_apps.vw_mhs_mkd ON vw_mhs_mkd.mkditawarkan_id = tbl_post_nilai.mkditawarkan_id 
				INNER JOIN db_coms.coms_user ON vw_mhs_mkd.mahasiswa_id = coms_user.mahasiswa_id 
				LEFT JOIN db_ptiik_apps.tbl_post_nilai_mhs ON tbl_post_nilai_mhs.post_nilai_id = tbl_post_nilai.post_nilai_id 
					AND tbl_post_nilai_mhs.mahasiswa_id = vw_mhs_mkd.mahasiswa_id 
				WHERE MID(MD5(tbl_post_nilai.mkditawarkan_id),9,7) = '$mkd' 
					AND vw_mhs_mkd.jadwal_id = '$jadwal'
					AND (tbl_post_nilai.jadwal_id = '$jadwal' OR ISNULL(tbl_post_nilai.jadwal_id) OR tbl_post_nilai.jadwal_id)
				ORDER BY tbl_post_nilai.tgl_mulai DESC";
				
		return $this->db->query($sql);
	}
	
	function get_jadwal_by_mhs($mhs, $jadwal){
		$sql = "SELECT DISTINCT
					vw_mhs_mkd.jadwal_id, 
					vw_mhs_mkd.keterangan as namamk, 
					vw_mhs_mkd.kelas,
					tbl_prodi.keterangan prodi
				FROM db_ptiik_apps.vw_mhs_mkd 
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = vw_mhs_mkd.prodi_id
				WHERE vw_mhs_mkd.is_praktikum='0' AND vw_mhs_mkd.mahasiswa_id = '$mhs' 
					AND MID(MD5(vw_mhs_mkd.mkditawarkan_id),9,7) = '$jadwal'";
		
		return $this->db->query($sql);
	}
	
	function get_jadwal($staff, $mkid){
		$sql = "SELECT 
					vw_dosen_mkd.jadwal_id, 
					vw_dosen_mkd.namamk, 
					vw_dosen_mkd.kelas,
					tbl_prodi.keterangan prodi
				FROM db_ptiik_apps.vw_dosen_mkd 
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = vw_dosen_mkd.prodi_id
				WHERE vw_dosen_mkd.karyawan_id = '$staff' AND MID(MD5(vw_dosen_mkd.mkditawarkan_id),9,7) = '$mkid'
				GROUP BY vw_dosen_mkd.namamk, vw_dosen_mkd.kelas";
		return $this->db->query($sql);
	}
	
	function get_mkd_name($mkd, $role){
		if($role == 'mahasiswa'){
			$sql = "SELECT vw_mhs_mkd.keterangan, vw_mhs_mkd.mkditawarkan_id
					FROM db_ptiik_apps.vw_mhs_mkd
					WHERE MID(MD5(mkditawarkan_id),9,7) = '$mkd'
					LIMIT 0,1";
		}
		else{
			$sql = "SELECT vw_dosen_mkd.namamk keterangan, vw_dosen_mkd.mkditawarkan_id 
					FROM db_ptiik_apps.vw_dosen_mkd 
					WHERE MID(MD5(vw_dosen_mkd.mkditawarkan_id),9,7) = '$mkd' 
					LIMIT 0,1";
		}
		
		// echo $sql;
		return $this->db->getRow($sql);
	}
	
	function get_progress_id(){
		$sql="SELECT concat('AD".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(post_nilai_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_post_nilai WHERE left(post_nilai_id,8)='AD".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_judul_progress($mkd, $jadwal){
		$sql = "SELECT DISTINCT
					tbl_post_nilai.post_nilai_id,
					tbl_post_nilai.judul,
					tbl_post_nilai.tgl_mulai,
					tbl_post_nilai.tgl_selesai,
					tbl_post_nilai.kategori_nilai
				FROM db_ptiik_apps.tbl_post_nilai
				LEFT JOIN db_ptiik_apps.vw_mhs_mkd ON vw_mhs_mkd.mkditawarkan_id = tbl_post_nilai.mkditawarkan_id
				WHERE MID(MD5(tbl_post_nilai.mkditawarkan_id),9,7) = '$mkd' 
					AND (tbl_post_nilai.jadwal_id = '$jadwal' OR ISNULL(tbl_post_nilai.jadwal_id) OR tbl_post_nilai.jadwal_id)
				ORDER BY tbl_post_nilai.tgl_mulai DESC";
				
		return $this->db->query($sql);
	}
	
	function get_progress_khs($mkd){
		$sql = "SELECT DISTINCT
					tbl_post_nilai.post_nilai_id
				FROM db_ptiik_apps.tbl_post_nilai
				
				WHERE MID(MD5(tbl_post_nilai.mkditawarkan_id),9,7) = '$mkd' AND kategori_nilai='khs'";
		return $this->db->getRow($sql);
	}
	
	function save_grade($data){
		$this->db->replace('db_ptiik_apps`.`tbl_post_nilai', $data);
	}
	
	function new_mark($id){
		$mhs_id = $_POST['mhs_id'];
		$tugas_id = $_POST['tugas_id'];
		$skor = $_POST['skor'];
		$total_skor = $_POST['total_skor'];
		
		$sql = "SELECT tbl_post_nilai_mhs.nilai_id,
					tbl_post_nilai_mhs.mahasiswa_id,
					tbl_post_nilai_mhs.post_nilai_id,
					tbl_post_nilai_mhs.link_id,
					tbl_post_nilai_mhs.tbl_name,
					tbl_post_nilai_mhs.skor,
					tbl_post_nilai_mhs.total_skor,
					tbl_post_nilai_mhs.tgl_post,
					tbl_post_nilai_mhs.`status`,
					tbl_post_nilai_mhs.user_id,
					tbl_post_nilai_mhs.last_update,
					tbl_post_nilai_mhs.is_proses,
					tbl_post_nilai.mkditawarkan_id
				FROM db_ptiik_apps.tbl_post_nilai_mhs
					INNER JOIN db_ptiik_apps.tbl_post_nilai ON tbl_post_nilai_mhs.post_nilai_id = tbl_post_nilai.post_nilai_id
				WHERE tbl_post_nilai_mhs.mahasiswa_id = '$mhs_id' AND tbl_post_nilai_mhs.post_nilai_id = '$tugas_id'";
		
		$data = $this->db->getRow($sql);
		
		if(isset($data)){ //update nilai
			$sql = "UPDATE db_ptiik_apps.tbl_post_nilai_mhs
					SET skor = '$skor', total_skor = '$total_skor'
					WHERE mahasiswa_id = '$mhs_id' AND post_nilai_id = '$tugas_id'";
			$this->db->query($sql);
			
			if(($_POST['kategori_nilai']=='khs')) $this->update_khs($skor, $total_skor, $_POST['kategori_nilai'],$mhs_id, $data->mkditawarkan_id, $id );
			
			if(! empty($_POST['kategori'])) $this->update_nilai($_POST['kategori'], $_POST['link_id'], $skor, $total_skor, $_POST['kategori_nilai'],$mhs_id, $data->mkditawarkan_id,$id);
		}
		
		else{ //insert new mark
			$data = array(
				'nilai_id' => $this->get_nilai_id(),
				'mahasiswa_id' => $mhs_id,
				'post_nilai_id' => $tugas_id,
				'skor' => $skor,
				'total_skor' => $total_skor,
				'tgl_post' => date('Y-m-d H:i:s'),
				'status' => 'Turn In',
				'user_id' => $id,
				'last_update' => date('Y-m-d H:i:s'),
				'is_proses' => '1'
			);
			$this->db->replace('db_ptiik_apps`.`tbl_post_nilai_mhs', $data);
		}
		
		
	} //end of function new mark
	
	function update_nilai($kategori, $link_id, $skor, $total_skor, $kategori_nilai, $mhs, $mk, $id){
		if($kategori == 'tbl_test_hasil'){
			$sql = "UPDATE db_ptiik_apps.tbl_test_hasil SET total_skor = '$skor' WHERE hasil_id = '$link_id'";
		}
		elseif($kategori == 'tbl_tugas_mhs'){
			$sql = "UPDATE db_ptiik_apps.tbl_tugas_mhs SET skor = '$skor', total_skor = '$total_skor' WHERE upload_id = '$link_id'";
		}
		$this->db->query($sql);
		
		if($kategori_nilai=='khs') $this->update_khs($skor, $total_skor, $kategori_nilai, $mhs, $mk, $id);
	}
	
	function update_khs($skor, $total_skor, $kategori_nilai, $mhs, $mk, $id){
		$nilai_huruf = $this->convertTohuruf($skor);
		$nilai_bobot = $this->convertToBobot($skor);

		$sql = "UPDATE db_ptiik_apps.tbl_krs SET nilai_akhir = '$skor', inf_bobot = '$nilai_bobot', inf_huruf='$nilai_huruf',user_id= '$id',
				last_update='".date('Y-m-d H:i:s')."' WHERE mahasiswa_id = '$mhs' AND mkditawarkan_id = '$mk'";
		
		$this->db->query($sql);
	}
	
	function convertToBobot($angka){
		if($angka > 80){
			$huruf = '4';
		}
		elseif($angka > 75 && $angka <= 80){ 
			$huruf = '3';
		}
		elseif($angka > 69 && $angka <= 75){
			$huruf = '3';
		}
		elseif($angka > 60 && $angka <= 69){
			$huruf = '2';
		}
		elseif($angka > 55 && $angka <= 60){
			$huruf = '2';
		}
		elseif($angka > 50 && $angka <= 55){
			$huruf = '1';
		}
		elseif($angka > 44 && $angka <= 50){
			$huruf = '1';
		}
		elseif($angka <= 44){
			$huruf = '0';
		}
		return $huruf;
	}
	
	function convertTohuruf($angka){
		if($angka > 80){
			$huruf = 'A';
		}
		elseif($angka > 75 && $angka <= 80){ 
			$huruf = 'B+';
		}
		elseif($angka > 69 && $angka <= 75){
			$huruf = 'B';
		}
		elseif($angka > 60 && $angka <= 69){
			$huruf = 'C+';
		}
		elseif($angka > 55 && $angka <= 60){
			$huruf = 'C';
		}
		elseif($angka > 50 && $angka <= 55){
			$huruf = 'D+';
		}
		elseif($angka > 44 && $angka <= 50){
			$huruf = 'D';
		}
		elseif($angka <= 44){
			$huruf = 'E';
		}
		return $huruf;
	}
	
	function get_nilai_id(){
		$sql="SELECT concat('AD".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(nilai_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_post_nilai_mhs WHERE left(nilai_id,8)='AD".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function del_komponen($komponen){
		$sql = "DELETE FROM db_ptiik_apps.tbl_post_nilai_mhs WHERE post_nilai_id = '$komponen'";
		$this->db->query($sql);
		$sql = "DELETE FROM db_ptiik_apps.tbl_post_nilai WHERE post_nilai_id = '$komponen'";
		$this->db->query($sql);
	}
	
	
	
	
	
	
	
	
	
}
?>