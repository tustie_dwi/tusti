<?php
class model_folder extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	/* folder */
		function read($id=NULL) {		
		$sql = "SELECT MID( MD5( tbl_media_library_folder.id), 9,7) as folder_id,
					   tbl_media_library_folder.*
				FROM db_ptiik_apps.tbl_media_library_folder
				WHERE 1
			   ";
		if($id){
			$sql .= " AND tbl_media_library_folder.parent_id = '".$id."'";
		}
		else{
			$sql .= " AND tbl_media_library_folder.parent_id = '0'";
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function folder_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_media_library_folder";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_folder($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_media_library_folder', $data);
	}
	
	/* file */
	function read_file($id=NULL, $file=NULL) {		
		$sql = "SELECT MID( MD5( tbl_media_library.file_id), 9,7) as id, tbl_media_library.*
				FROM db_ptiik_apps.tbl_media_library
				LEFT JOIN db_ptiik_apps.tbl_media_library_folder ON tbl_media_library_folder.id = tbl_media_library.folder_id
				WHERE 1
			   ";
		if($id){
			$sql .= " AND tbl_media_library.folder_id = '".$id."'";
		}
		else{
			if($file){
				$sql .= " AND tbl_media_library.file_id = '".$file."'";
			}else{	
				$sql .= " AND tbl_media_library.folder_id = '0'";
			}
		}		
		
		if($file){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
	}
	
	
	
	function file_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_media_library";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_id_from_parent($folder_id){
		$sql = "SELECT MID( MD5(tbl_media_library_folder.id), 9,7) as id
				FROM db_ptiik_apps.tbl_media_library_folder
				WHERE 1";
		if($folder_id!=""){
			$sql .= " AND (MID( MD5(parent_id), 9,7) = '".$folder_id."' OR parent_id='".$folder_id."')";
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_folder($id=NULL){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_media_library_folder
				WHERE (MID( MD5(id), 9,7) = '".$id."' OR  id='".$id."')";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_folder_name($folder_id=NULL){
		$sql = "SELECT tbl_media_library_folder.folder_name, parent_id
				FROM db_ptiik_apps.tbl_media_library_folder
				WHERE 1";
		if($folder_id){
			$sql .= " AND tbl_media_library_folder.id = '".$folder_id."'";
		}
		$dt = $this->db->getRow( $sql );
		return $dt;
	}
	
	function replace_file($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_media_library', $data);
	}
	
	function delete_file($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_media_library
				WHERE MID( MD5(file_id), 9,7) = '".$id."'";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_file_by_folder($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_media_library
				WHERE MID( MD5(folder_id), 9,7) = '".$id."'";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function rename_folder($id, $folder_name){
		$sql = "UPDATE db_ptiik_apps.tbl_media_library_folder
				SET folder_name = '".$folder_name."'
				WHERE id = '".$id."'";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	public function toggleStatus($id) {
		$sql = "UPDATE db_ptiik_apps.tbl_media_library_folder SET is_available = CASE is_available WHEN 0 THEN 1 ELSE 0 END WHERE id = '$id'";
		$result = $this->db->query( $sql );
		if( !$result )
			$this->error = $this->db->getLastError();
			
		$sql = "SELECT is_available FROM db_ptiik_apps.tbl_media_library_folder WHERE id = '$id'";
		$this->status = $this->db->getVar($sql);	
			
		return $result;
	}
}
?>