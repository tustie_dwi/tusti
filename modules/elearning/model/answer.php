<?php
class model_answer extends model {
	private $coms;
	
	function cek_test($quiz_id, $mhs_id){
		$sql = "SELECT * FROM db_ptiik_apps.tbl_test_hasil WHERE mahasiswa_id = '$mhs_id' AND mid(md5(test_id),9,7) = '$quiz_id'";
		
		return $this->db->getRow($sql);
	}
	
	function get_mkd_dosen($staff_id, $mkd){
		$sql = "SELECT vw_dosen_mkd.jadwal_id FROM db_ptiik_apps.vw_dosen_mkd WHERE vw_dosen_mkd.karyawan_id = '$staff_id' AND vw_dosen_mkd.mkditawarkan_id = '$mkd'";
		return $this->db->query($sql);
	}
	
	function get_mhs_by_test($quiz_id, $mkd){
		$sql = "SELECT DISTINCT 
					vw_mhs_mkd.nama,
					vw_mhs_mkd.kelas,
					vw_mhs_mkd.prodi_id,
					vw_mhs_mkd.icon, 
					vw_mhs_mkd.foto,
					db_coms.coms_user.id as user_id,
					tbl_test_hasil.is_mark,
					MID(MD5(tbl_test_hasil.hasil_id),9,7) hasil_id,
					MID(MD5(vw_mhs_mkd.mahasiswa_id),9,7) mhs_id
				FROM db_ptiik_apps.tbl_test
				LEFT JOIN db_ptiik_apps.vw_mhs_mkd ON vw_mhs_mkd.mkditawarkan_id = tbl_test.mkditawarkan_id
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_mhs_mkd.mahasiswa_id = db_coms.coms_user.mahasiswa_id
				LEFT JOIN db_ptiik_apps.tbl_test_hasil ON tbl_test_hasil.test_id = tbl_test.test_id AND tbl_test_hasil.mahasiswa_id = vw_mhs_mkd.mahasiswa_id
				WHERE MID(MD5(tbl_test.test_id),9,7) = '$quiz_id'";
		
		$sql .= " AND vw_mhs_mkd.jadwal_id IN (";
		foreach ($mkd as $key) {
			$sql .= "'$key->jadwal_id',";
		}
		$sql .= "'-')";
		return $this->db->query($sql);
	}
	
	function get_mhs_by_jadwal($quiz_id){
		$sql = "SELECT DISTINCT 
					vw_mhs_mkd.nama,
					vw_mhs_mkd.kelas,
					vw_mhs_mkd.prodi_id,
					vw_mhs_mkd.icon, 
					vw_mhs_mkd.foto,
					db_coms.coms_user.id as user_id,
					tbl_test_hasil.is_mark,
					MID(MD5(tbl_test_hasil.hasil_id),9,7) hasil_id,
					MID(MD5(vw_mhs_mkd.mahasiswa_id),9,7) mhs_id
				FROM db_ptiik_apps.tbl_test
				LEFT JOIN db_ptiik_apps.vw_mhs_mkd ON vw_mhs_mkd.mkditawarkan_id = tbl_test.mkditawarkan_id AND vw_mhs_mkd.jadwal_id = tbl_test.jadwal_id
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_mhs_mkd.mahasiswa_id = db_coms.coms_user.mahasiswa_id
				LEFT JOIN db_ptiik_apps.tbl_test_hasil ON tbl_test_hasil.test_id = tbl_test.test_id AND tbl_test_hasil.mahasiswa_id = vw_mhs_mkd.mahasiswa_id
				WHERE tbl_test.test_id = '$quiz_id'";
		return $this->db->query($sql);
	}
	
	function get_jawaban_mhs($mhs_id, $quiz_id){
		if(empty($mhs_id)) return NULL;
		else{
			$sql = "SELECT 
						tbl_test_hasil.hasil_id,
						tbl_test_hasil.tgl_test,
						tbl_test_hasil.jam_mulai,
						tbl_test_hasil.jam_selesai,
						tbl_test_hasil.max_post,
						tbl_test_hasil.total_skor,
						tbl_test_hasil.is_mark,
						tbl_mahasiswa.nama,
						tbl_mahasiswa.foto,
						mid(md5(tbl_mahasiswa.mahasiswa_id),9,7) nim
					FROM db_ptiik_apps.tbl_test_hasil
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_test_hasil.mahasiswa_id
					WHERE MID(MD5(tbl_test_hasil.mahasiswa_id),9,7) = '$mhs_id'
						AND MID(MD5(tbl_test_hasil.test_id),9,7) = '$quiz_id'";
			
			$data = $this->db->getRow($sql);
			if(empty($data)) return 'jawaban kosong';
			else return $data;
		}
	}
	
	function get_hasil($hasil_id){
		$sql = "SELECT jawaban_id, soal_id, inf_jawab, inf_skor
				FROM db_ptiik_apps.tbl_test_hasil_detail
				WHERE hasil_id = '$hasil_id'";
		return $this->db->query($sql);
	}
	
	function update_skor(){
		$hasil_id = explode('|', $_POST['xvsvd']);
		$hasil_id = $hasil_id[0];
		$skor = 0;
		foreach($_POST['skor'] as $key){
			$skor += $key;
		}
		$sql = "UPDATE db_ptiik_apps.tbl_test_hasil SET total_skor = '$skor', is_mark = '1' WHERE mid(md5(hasil_id),9,7) = '$hasil_id'";
		$this->db->query($sql);
		
		$this->update_post_nilai($skor, '100', $hasil_id);
	}
	
	function update_post_nilai($skor=NULL, $total=NULL, $id=NULL){
		$sql = "SELECT hasil_id FROM db_ptiik_apps.`tbl_test_hasil` WHERE MID( MD5(hasil_id) , 9,7 ) = '".$id."' ";
		$row = $this->db->getRow( $sql );
		
		if($row){
			$nilai_id = 'QU'.$row->hasil_id;
			
			$sqlupdate= "UPDATE db_ptiik_apps.`tbl_post_nilai_mhs`
				   SET status='Marked', skor = '".$skor."', total_skor = '".$total."'  
				   WHERE nilai_id = '".$nilai_id."'
				 "; 
			$result = $this->db->query( $sqlupdate );
			if(isset($result)){				
				return TRUE;
				
			}
		}
	}
	
	function get_quiz_owner($test_id){
		$sql = "SELECT coms_user.id, coms_user.name nama
				FROM db_ptiik_apps.tbl_test 
				LEFT JOIN db_coms.coms_user ON tbl_test.user_id = coms_user.id
				WHERE tbl_test.test_id = '$test_id'";
		return $this->db->getRow($sql);
	}
	
	function get_submission_owner($hasil_id){
		$sql = "SELECT coms_user.id, coms_user.name nama 
				FROM db_ptiik_apps.tbl_test_hasil 
				LEFT JOIN db_coms.coms_user ON tbl_test_hasil.user_id = coms_user.id
				WHERE MID(MD5(tbl_test_hasil.hasil_id),9,7) = '$hasil_id'";
				echo $sql;
		return $this->db->getRow($sql);
	}
	//---------------------------------------------- test manage
	
	function get_test($quiz_id){
		$sql = "SELECT time_limit, judul, keterangan, tgl_selesai, tgl_mulai, is_random, test_id, mkditawarkan_id, jadwal_id, instruksi, time_limit
				FROM db_ptiik_apps.tbl_test
				WHERE mid(md5(test_id),9,7) = '".$quiz_id."'";
		return $this->db->getRow($sql);
	}
	
	function get_soal($quiz_id){
		$sql = "SELECT tbl_test_soal.pertanyaan, tbl_test_kategori.keterangan, tbl_test_soal.soal_id
				FROM db_ptiik_apps.tbl_test_soal
				LEFT JOIN db_ptiik_apps.tbl_test_kategori ON tbl_test_kategori.kategori_id = tbl_test_soal.kategori_id
				WHERE mid(md5(tbl_test_soal.test_id),9,7) = '".$quiz_id."'";
		return $this->db->query($sql);
	}
	
	function get_jawaban($quiz_id){
		$sql ="SELECT tbl_test_jawab.jawaban_id, tbl_test_jawab.keterangan, tbl_test_jawab.is_benar, tbl_test_jawab.skor, tbl_test_soal.soal_id
				FROM db_ptiik_apps.tbl_test_soal
				INNER JOIN db_ptiik_apps.tbl_test_jawab ON tbl_test_jawab.soal_id = tbl_test_soal.soal_id
				WHERE mid(md5(tbl_test_soal.test_id),9,7) = '".$quiz_id."'";
		return $this->db->query($sql);
	}
	
	function get_hasil_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(hasil_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_test_hasil WHERE left(hasil_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_detail_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(detail_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_test_hasil_detail WHERE left(detail_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_skor($jawaban_id){
		$sql = "SELECT skor FROM db_ptiik_apps.tbl_test_jawab WHERE jawaban_id = '".$jawaban_id."'";
		$strresult = $this->db->getRow( $sql );
		if(isset($strresult->skor)) return $strresult->skor;
		else return 0;
	}
	
	function save_answer($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_test_hasil_detail',$data)) return TRUE;
		else return FALSE;
	}
	
	function save($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_test_hasil',$data)) return TRUE;
		else return FALSE;
	}
	
	
}
?>