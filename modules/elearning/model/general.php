<?php
class model_general extends model {
	private $coms;
	
	function get_user_profile($user_id=NULL){
		$sql = "SELECT
				db_coms.coms_user.id,
				db_coms.coms_user.`name`,
				db_coms.coms_user.email,
				db_coms.coms_user.username,
				db_coms.coms_user.biografi,
				db_coms.coms_user.`password`,
				db_coms.coms_user.`level`,
				db_coms.coms_user.`status`,
				db_coms.coms_user.karyawan_id,
				db_coms.coms_user.mahasiswa_id,
				db_coms.coms_user.fakultas_id,
				db_coms.coms_user.foto,
				db_coms.coms_level.description as `role`
				FROM
				db_coms.coms_user
				INNER JOIN db_coms.coms_level ON db_coms.coms_user.`level` = db_coms.coms_level.`level` WHERE db_coms.coms_user.id='$user_id' ";
		return $this->db->getRow($sql);
	}
	
	//notifikasi-----------------------------------------------------
	
	function get_notifikasi_new($user_id){
		$sql = "SELECT 
					mid(md5(notifikasi_id),9,7) hidId,
					keterangan, tgl_notifikasi, is_read, user_from_name, kategori, link_id
				FROM db_ptiik_apps.tbl_post_notifikasi
				WHERE user_to = '$user_id'
				ORDER BY tgl_notifikasi DESC
				LIMIT 0,8";
		return $this->db->query($sql);
	}
	
	function get_notifikasi_all($user_id, $year=NULL){
		if($year == '') $year = date('Y');
		$sql = "SELECT 
					mid(md5(notifikasi_id),9,7) hidId,
					keterangan, tgl_notifikasi, 
					is_read, 
					user_from_name, 
					kategori, 
					link_id,
					tgl_notifikasi,
					tgl_baca
				FROM db_ptiik_apps.tbl_post_notifikasi
				WHERE user_to = '$user_id' AND YEAR(tgl_notifikasi) = '$year'
				ORDER BY tgl_notifikasi DESC";
		return $this->db->query($sql);
	}
	
	function get_jml_notifikasi($user_id){
		$sql = "SELECT count(notifikasi_id) jml
				FROM db_ptiik_apps.tbl_post_notifikasi
				WHERE user_to = '$user_id' AND is_read = '0'";
				
		$strresult = $this->db->getRow( $sql );
		return $strresult->jml;
	}
	
	function get_notifikasi_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(notifikasi_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_post_notifikasi WHERE left(notifikasi_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		
		return $strresult->data;
	}
	
	function get_notif_dosen($mkd){
		$sql = "SELECT DISTINCT vw_dosen_mkd.nama_dosen nama, coms_user.id
				FROM db_ptiik_apps.vw_dosen_mkd 
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_dosen_mkd.karyawan_id = db_coms.coms_user.karyawan_id
				WHERE vw_dosen_mkd.mkditawarkan_id = '$mkd' OR vw_dosen_mkd.jadwal_id = '$mkd'";
		return $this->db->query($sql); 
	}
	
	function get_notif_mhs($mkd){
		$sql = "SELECT DISTINCT vw_mhs_mkd.nama, coms_user.id
				FROM db_ptiik_apps.vw_mhs_mkd 
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_mhs_mkd.mahasiswa_id = db_coms.coms_user.mahasiswa_id
				WHERE vw_mhs_mkd.mkditawarkan_id = '$mkd' OR vw_mhs_mkd.jadwal_id = '$mkd'";
		return $this->db->query($sql); 
	}
	
	function get_notif_dosen_by_mk($mkd=NULL, $mhs=NULL){
		$sql = "SELECT DISTINCT vw_dosen_mkd.nama_dosen nama, coms_user.id
				FROM db_ptiik_apps.vw_dosen_mkd 
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_dosen_mkd.karyawan_id = db_coms.coms_user.karyawan_id
				INNER JOIN db_ptiik_apps.vw_mhs_mkd ON db_ptiik_apps.vw_dosen_mkd.mkditawarkan_id = db_ptiik_apps.vw_mhs_mkd.mkditawarkan_id AND db_ptiik_apps.vw_dosen_mkd.kelas = db_ptiik_apps.vw_mhs_mkd.kelas AND db_ptiik_apps.vw_dosen_mkd.prodi_id = db_ptiik_apps.vw_mhs_mkd.prodi_id
				WHERE (vw_dosen_mkd.mkditawarkan_id = '$mkd' OR vw_dosen_mkd.jadwal_id = '$mkd') AND db_ptiik_apps.vw_mhs_mkd.mahasiswa_id='$mhs'";
		
		return $this->db->query($sql); 
	}
	
	function get_notif_mhs_by_mk($mkd=NULL, $staff=NULL){
		$sql= "SELECT DISTINCT vw_mhs_mkd.nama, coms_user.id FROM db_ptiik_apps.vw_mhs_mkd 
				INNER JOIN  db_ptiik_apps.vw_dosen_mkd ON vw_mhs_mkd.mkditawarkan_id = vw_dosen_mkd.mkditawarkan_id AND vw_mhs_mkd.kelas = vw_dosen_mkd.kelas AND vw_mhs_mkd.prodi_id = vw_dosen_mkd.prodi_id
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_mhs_mkd.mahasiswa_id = db_coms.coms_user.mahasiswa_id
				WHERE (vw_mhs_mkd.mkditawarkan_id = '$mkd' OR vw_mhs_mkd.jadwal_id = '$mkd') AND vw_dosen_mkd.karyawan_id='$staff'
				";
		return $this->db->query($sql); 
	}
	
	function get_notif_by_comment($post_id){
		$sql = "SELECT DISTINCT tbl_post.user_id id, coms_user.name nama
				FROM db_ptiik_apps.tbl_post
				LEFT JOIN db_ptiik_apps.tbl_post_share ON tbl_post_share.id_post = tbl_post.post_id
				INNER JOIN db_coms.coms_user ON tbl_post.user_id = coms_user.id
				WHERE tbl_post.post_id = '$post_id' OR tbl_post.parent_id = '$post_id'";
		return $this->db->query($sql); 
	}
	
	function add_notif($data){
		$this->db->replace('db_ptiik_apps`.`tbl_post_notifikasi',$data);
	}
	
	function update_read(){
		$notif_id = $_POST['notif_id'];
		$tgl = date('Y-m-d H:i:s');
		$sql = "UPDATE db_ptiik_apps.tbl_post_notifikasi SET is_read='1', tgl_baca = '$tgl' WHERE mid(md5(notifikasi_id),9,7) = '$notif_id'";
		$this->db->query($sql); 
	}
	
	function delete_notifikasi($user_id){
		$notif_id = $_POST['notif_id'];
		
		$sql = "DELETE FROM db_ptiik_apps.tbl_post_notifikasi 
				WHERE mid(md5(notifikasi_id),9,7) = '$notif_id' 
				AND user_to = '$user_id'";
				
		$this->db->query($sql);
	}
	
	//--------------------------------------------------------------------------
	function get_mkd_dosen($dosen_id=NULL, $mkid=NULL){
		$sql = "SELECT DISTINCT  mid(md5(vw_dosen_mkd.karyawan_id),9,7) AS id, 
					vw_dosen_mkd.karyawan_id, 
					vw_dosen_mkd.nama_dosen nama, 
					vw_dosen_mkd.mkditawarkan_id, 
					vw_dosen_mkd.icon, vw_dosen_mkd.foto, 
					vw_dosen_mkd.namamk keterangan, 
					mid(md5(vw_dosen_mkd.mkditawarkan_id),9,7) hidId,
					db_coms.coms_user.id as user_id
				FROM
				db_ptiik_apps.vw_dosen_mkd
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_dosen_mkd.karyawan_id = db_coms.coms_user.karyawan_id
				WHERE 1 ";
				
		if($dosen_id) $sql .= " AND (vw_dosen_mkd.karyawan_id = '$dosen_id'  OR (mid(md5(vw_dosen_mkd.karyawan_id),9,7) ='$dosen_id' ))  ";
		
		if(($mkid) && ($dosen_id)):
			$sql .= "  AND  mid(md5(vw_dosen_mkd.mkditawarkan_id),9,7) = '$mkid' ";		
			
			return $this->db->getRow($sql); 
		else:
			if($mkid) $sql .= "  AND  mid(md5(vw_dosen_mkd.mkditawarkan_id),9,7) = '$mkid' ";
			
			$sql .= " ORDER BY vw_dosen_mkd.namamk  ASC";
			
			return $this->db->query($sql); 
		endif;
	}
	
	function  get_jml_mhs_dosen($dosen_id=NULL){
		$sql = "SELECT
				vw_dosen_mkd.karyawan_id,
				Count(vw_mhs_mkd.mahasiswa_id) as `jml`
				FROM
				db_ptiik_apps.vw_mhs_mkd
				INNER JOIN db_ptiik_apps.vw_dosen_mkd ON vw_mhs_mkd.jadwal_id = vw_dosen_mkd.jadwal_id WHERE 1 
				";
		if($dosen_id) $sql .= " AND (vw_dosen_mkd.karyawan_id = '$dosen_id'  OR (mid(md5(vw_dosen_mkd.karyawan_id),9,7) ='$dosen_id' ))  ";
		
		$sql.= "GROUP BY
				vw_dosen_mkd.karyawan_id ";
		return $this->db->getRow($sql); 
	}
	
	function get_jadwal_dosen($dosen_id=NULL, $mkid=NULL){
		$sql = "SELECT DISTINCT  mid(md5(vw_dosen_mkd.karyawan_id),9,7) AS id, 
					vw_dosen_mkd.karyawan_id, 
					vw_dosen_mkd.nama_dosen nama, 
					vw_dosen_mkd.jadwal_id, 
					vw_dosen_mkd.mkditawarkan_id, 
					vw_dosen_mkd.icon, vw_dosen_mkd.foto, 
					vw_dosen_mkd.namamk keterangan, 
					mid(md5(vw_dosen_mkd.mkditawarkan_id),9,7) as mkid,
					mid(md5(vw_dosen_mkd.mkditawarkan_id),9,7) as hidId,
					db_coms.coms_user.id as user_id
				FROM
				db_ptiik_apps.vw_dosen_mkd
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_dosen_mkd.karyawan_id = db_coms.coms_user.karyawan_id
				WHERE 1 ";
				
		if($dosen_id) $sql .= " AND (vw_dosen_mkd.karyawan_id = '$dosen_id'  OR (mid(md5(vw_dosen_mkd.karyawan_id),9,7) ='$dosen_id' ))  ";		
		
		if($mkid) $sql .= "  AND  mid(md5(vw_dosen_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		$sql .= " ORDER BY vw_dosen_mkd.nama_dosen ASC";
		
		return $this->db->query($sql); 
		
	}
	
	function get_mkd_mhs($mhs_id=NULL, $mkid=NULL){
		
		$sql = "SELECT DISTINCT mid(md5(vw_mhs_mkd.mahasiswa_id),9,7) AS id, 
					vw_mhs_mkd.mahasiswa_id, 
					vw_mhs_mkd.icon, 
					vw_mhs_mkd.foto, 
					vw_mhs_mkd.nama, 
					vw_mhs_mkd.mkditawarkan_id, 
					vw_mhs_mkd.keterangan, 
					mid(md5(vw_mhs_mkd.mkditawarkan_id),9,7) hidId, 
					db_coms.coms_user.id as user_id
				FROM
				db_ptiik_apps.vw_mhs_mkd
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_mhs_mkd.mahasiswa_id = db_coms.coms_user.mahasiswa_id WHERE 1 ";
		
		if($mhs_id) $sql .= " AND (vw_mhs_mkd.mahasiswa_id = '$mhs_id' OR (mid(md5(vw_mhs_mkd.mahasiswa_id),9,7) = '$mhs_id') ) ";
		
		if(($mkid) && ($mhs_id)):
			$sql .= "  AND  mid(md5(vw_mhs_mkd.mkditawarkan_id),9,7) = '$mkid' ";
			
			return $this->db->getRow($sql); 
		else:
			if($mkid) $sql .= " AND  mid(md5(vw_mhs_mkd.mkditawarkan_id),9,7) = '$mkid' ";
			
			$sql .= " ORDER BY vw_mhs_mkd.nama ASC ";
			
			return $this->db->query($sql); 
		endif;
		
	}
	
	function get_jadwal_mhs($mhs_id=NULL, $mkid=NULL, $jadwal=NULL, $data=NULL){
		
		$sql = "SELECT DISTINCT mid(md5(vw_mhs_mkd.mahasiswa_id),9,7) AS id, 
					vw_mhs_mkd.mahasiswa_id, 
					vw_mhs_mkd.icon, 
					vw_mhs_mkd.foto, 
					vw_mhs_mkd.nama, 
					vw_mhs_mkd.jadwal_id, 
					vw_mhs_mkd.mkditawarkan_id, 
					vw_mhs_mkd.keterangan, 
					mid(md5(vw_mhs_mkd.mkditawarkan_id),9,7) as mkid,
					mid(md5(vw_mhs_mkd.mkditawarkan_id),9,7) as hidId, 
					db_coms.coms_user.id as user_id
				FROM
				db_ptiik_apps.vw_mhs_mkd
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.vw_mhs_mkd.mahasiswa_id = db_coms.coms_user.mahasiswa_id WHERE 1 ";
				
		if($mhs_id) $sql .= " AND (vw_mhs_mkd.mahasiswa_id = '$mhs_id' OR (mid(md5(vw_mhs_mkd.mahasiswa_id),9,7) = '$mhs_id') ) ";		
	
		if($mkid) $sql .= " AND  mid(md5(vw_mhs_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		if($jadwal) $sql .= " AND  mid(md5(vw_mhs_mkd.jadwal_id),9,7) = '$jadwal' ";
		
		if($data){
			$sql.= " AND mid(md5(vw_mhs_mkd.jadwal_id),9,7) IN (";
				$jml = count($data);
				
				$i=0;
				foreach ($data as $key):
					$i++;
					
					$sql .= "'".$key->jadwal_id . "'";
					if($i!=$jml) $sql.= ",";
				endforeach;		
			$sql.= ") ";
		}
		
		$sql .= " ORDER BY vw_mhs_mkd.mahasiswa_id ASC, vw_mhs_mkd.nama ASC ";
	
		return $this->db->query($sql); 
	
	}
	
	function get_jadwal_by_mhs($mhs=NULL, $mkid=NULL){
		$sql = "SELECT  DISTINCT 
				MID( MD5( vw_jadwal_mkd.jadwal_id ) , 9,7 ) AS jadwal_id,
				MID( MD5( vw_jadwal_mkd.mkditawarkan_id ) , 9,7 ) AS mkid,
				vw_jadwal_mkd.mkditawarkan_id,
				vw_jadwal_mkd.kelas,
				vw_jadwal_mkd.keterangan AS namamk,
				vw_jadwal_mkd.nama_dosen AS nama,
				CONCAT (vw_jadwal_mkd.keterangan , ' ' , vw_jadwal_mkd.kelas , ' - [', tbl_prodi.keterangan, ']') AS jadwal
				FROM
				db_ptiik_apps.vw_jadwal_mkd
				LEFT JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = vw_jadwal_mkd.prodi_id
				INNER JOIN db_ptiik_apps.vw_mhs_mkd ON vw_jadwal_mkd.jadwal_id = vw_mhs_mkd.jadwal_id  WHERE vw_jadwal_mkd.is_praktikum='0'  
				";
		if($mhs) $sql .= " AND vw_mhs_mkd.mahasiswa_id = '$mhs'";
		
		if($mkid)  $sql .= " AND  mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_post_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(post_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_post WHERE left(post_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		
		return $strresult->data;
	}
	
	function save_post($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_post',$data)) return TRUE;
		return FALSE;
	}
	
	function save_share($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_post_share',$data)) return TRUE;
		return FALSE;
	}
	
	function get_post($mkid=NULL, $memberid=NULL, $kategori=NULL, $page=1, $mkd=NULL, $jadwal=NULL){
		$offset = 10;
		$start = ($page-1) * $offset;
		
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_post.post_id,
					MID(MD5(db_ptiik_apps.tbl_post.post_id),9,7) hidId,
					db_ptiik_apps.tbl_post.isi,
					db_ptiik_apps.tbl_post.last_update,
					db_ptiik_apps.vw_jadwal_mkd.keterangan AS mkd,
					db_ptiik_apps.tbl_post.kategori,
					db_ptiik_apps.tbl_post.user_id,
					MID(MD5(db_ptiik_apps.tbl_post.link_id),9,7) AS links,
					MID(MD5(db_ptiik_apps.tbl_post_share.id_user),9,7) AS id_user,
					db_coms.coms_user.`name` AS `user_name`,db_coms.coms_user.foto,
					GROUP_CONCAT(DISTINCT tbl_post_attach.attach, '|', tbl_post_attach.jenis) attach,
					GROUP_CONCAT(DISTINCT user_comment.foto, '>>', user_comment.name, '>>', tbl_comment.post_id, '>>', tbl_comment.user_id, '>>', tbl_comment.isi, '>>', tbl_comment.last_update ORDER BY tbl_comment.last_update ASC  SEPARATOR '||') comment
				FROM
					db_ptiik_apps.tbl_post
				LEFT JOIN db_ptiik_apps.tbl_post_attach ON tbl_post.post_id = tbl_post_attach.post_id
				LEFT JOIN db_ptiik_apps.tbl_post_share ON db_ptiik_apps.tbl_post_share.id_post = db_ptiik_apps.tbl_post.post_id
				LEFT JOIN db_ptiik_apps.vw_jadwal_mkd ON db_ptiik_apps.vw_jadwal_mkd.mkditawarkan_id = db_ptiik_apps.tbl_post_share.id_user OR db_ptiik_apps.vw_jadwal_mkd.jadwal_id = db_ptiik_apps.tbl_post_share.id_user
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.tbl_post.user_id = db_coms.coms_user.id
				LEFT JOIN db_ptiik_apps.tbl_post tbl_comment ON tbl_comment.parent_id = tbl_post.post_id AND tbl_comment.isi <> ''
				LEFT JOIN db_coms.coms_user user_comment ON tbl_comment.user_id = user_comment.id
				WHERE 1
				";
		
		if($mkd){
			$sql .= " AND tbl_post_share.id_user IN (";
			if(count($mkd) > 1):
				foreach ($mkd as $key):
					$sql .= "'".$key->mkditawarkan_id . "',";
				endforeach;
			else:
				if(isset($mkd->mkditawarkan_id)):
					$sql .= "'".$mkd->mkditawarkan_id . "',";
				else:
					foreach ($mkd as $key):
						$sql .= "'".$key->mkditawarkan_id . "',";
					endforeach;
				endif;
			endif;
			
			if($jadwal){				
				foreach ($jadwal as $key):
					$sql .= "'".$key->jadwal_id . "',";
				endforeach;			
			}		
			$sql .= "'-')";
		}else{
			$sql .= " AND tbl_post_share.id_user IN ('-')";
		}	
		
		
		if($kategori) $sql .="  AND tbl_post.kategori IN (".$kategori.")";
		
		if($mkid) $sql .=" AND mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		if($memberid) $sql .= " AND (coms_user.mahasiswa_id = '$memberid' OR (mid(md5(coms_user.mahasiswa_id),9,7) = '$memberid') ) ";
		
		$sql .= " GROUP BY tbl_post.post_id ORDER BY tbl_post.last_update DESC LIMIT " . $start . "," . $offset;
		
		return $this->db->query($sql);
		
		
	}
	
	function get_post_detail($post_id){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_post.post_id,
					MID(MD5(db_ptiik_apps.tbl_post.post_id),9,7) hidId,
					db_ptiik_apps.tbl_post.isi,
					db_ptiik_apps.tbl_post.last_update,
					db_ptiik_apps.vw_jadwal_mkd.keterangan AS mkd,
					db_ptiik_apps.tbl_post.kategori,
					db_ptiik_apps.tbl_post.user_id,
					MID(MD5(db_ptiik_apps.tbl_post.link_id),9,7) AS links,
					MID(MD5(db_ptiik_apps.tbl_post_share.id_user),9,7) AS id_user,
					db_coms.coms_user.`name` AS `user_name`,db_coms.coms_user.foto,
					GROUP_CONCAT(DISTINCT tbl_post_attach.attach, '|', tbl_post_attach.jenis) attach,
					GROUP_CONCAT(DISTINCT user_comment.foto, '>>', user_comment.name, '>>', tbl_comment.post_id, '>>', tbl_comment.user_id, '>>', tbl_comment.isi, '>>', tbl_comment.last_update ORDER BY tbl_comment.last_update ASC  SEPARATOR '||') comment
				FROM
					db_ptiik_apps.tbl_post
				LEFT JOIN db_ptiik_apps.tbl_post_attach ON tbl_post.post_id = tbl_post_attach.post_id
				LEFT JOIN db_ptiik_apps.tbl_post_share ON db_ptiik_apps.tbl_post_share.id_post = db_ptiik_apps.tbl_post.post_id
				LEFT JOIN db_ptiik_apps.vw_jadwal_mkd ON db_ptiik_apps.vw_jadwal_mkd.mkditawarkan_id = db_ptiik_apps.tbl_post_share.id_user OR db_ptiik_apps.vw_jadwal_mkd.jadwal_id = db_ptiik_apps.tbl_post_share.id_user
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.tbl_post.user_id = db_coms.coms_user.id
				LEFT JOIN db_ptiik_apps.tbl_post tbl_comment ON tbl_comment.parent_id = tbl_post.post_id AND tbl_comment.isi <> ''
				LEFT JOIN db_coms.coms_user user_comment ON tbl_comment.user_id = user_comment.id
				WHERE mid(md5(db_ptiik_apps.tbl_post.post_id),9,7) = '$post_id'";
		
		return $this->db->query($sql);
	}
	
	function get_comment($mkid=NULL, $memberid=NULL, $jadwal=NULL){
		$sql = "SELECT DISTINCT tbl_post.parent_id post_id, mid(md5(tbl_post.post_id),9,7) as comment_id, tbl_post.isi, tbl_post.last_update, 
					vw_jadwal_mkd.keterangan mkd, tbl_post.kategori, tbl_post.user_id, 	db_coms.coms_user.`name` AS `user_name`,
					db_coms.coms_user.foto
				FROM db_ptiik_apps.tbl_post
				LEFT JOIN db_ptiik_apps.tbl_post_share ON db_ptiik_apps.tbl_post.parent_id = db_ptiik_apps.tbl_post_share.id_post
				LEFT JOIN db_ptiik_apps.vw_jadwal_mkd ON vw_jadwal_mkd.mkditawarkan_id = tbl_post_share.id_user OR vw_jadwal_mkd.jadwal_id = tbl_post_share.id_user
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.tbl_post.user_id = db_coms.coms_user.id
				WHERE tbl_post.kategori  IN ('comment')
				";
		if($mkid) $sql .=" AND mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		if($memberid) $sql .= " AND (coms_user.mahasiswa_id = '$memberid' OR (mid(md5(coms_user.mahasiswa_id),9,7) = '$memberid') ) ";
		
		if($jadwal) $sql .= " AND mid(md5(vw_jadwal_mkd.jadwal_id),9,7) = '$jadwal' ";
		
		$sql .=" ORDER BY tbl_post.last_update DESC";
		
		return $this->db->query($sql);
	}
	
	
	function get_progress($mkid=NULL, $mhs_id=NULL){
		/*$sql = "SELECT DISTINCT tbl_post.parent_id post_id, tbl_post.isi, tbl_post.last_update, vw_jadwal_mkd.keterangan mkd, tbl_post.kategori, tbl_post.user_id, db_coms.coms_user.`name` AS `user_name`,
					db_coms.coms_user.foto
				FROM db_ptiik_apps.tbl_post
				LEFT JOIN db_ptiik_apps.tbl_post_share ON tbl_post_share.id_post = tbl_post.parent_id
				LEFT JOIN db_ptiik_apps.vw_jadwal_mkd ON vw_jadwal_mkd.mkditawarkan_id = tbl_post_share.id_user OR vw_jadwal_mkd.jadwal_id = tbl_post_share.id_user
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.tbl_post.user_id = db_coms.coms_user.id
				WHERE tbl_post.kategori  IN ('response')
				";
		if($mkid) $sql .=" AND mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		if($memberid) $sql .= " AND (coms_user.mahasiswa_id = '$memberid' OR (mid(md5(coms_user.mahasiswa_id),9,7) = '$memberid') ) ";
		
		if($parent) $sql .=" AND tbl_post.parent_id = '$parent' ";
		
		$sql .=" ORDER BY tbl_post.last_update DESC";*/
		$sql_tugas = "SELECT
							
								tbl_tugas.judul
								FROM
								db_ptiik_apps.tbl_tugas_mhs
								INNER JOIN db_ptiik_apps.tbl_tugas ON tbl_tugas.tugas_id = tbl_tugas_mhs.tugas_id
								INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_tugas.jadwal_id = tbl_jadwalmk.jadwal_id
								WHERE tbl_tugas_mhs.tgl_upload IS NOT NULL  AND  (tbl_tugas_mhs.mahasiswa_id = '$mhs_id' OR (mid(md5(tbl_tugas_mhs.mahasiswa_id),9,7) = '$mhs_id')) AND 
									(mid(md5(tbl_jadwalmk.mkditawarkan_id),9,7)='$mkid' OR (mid(md5(tbl_tugas.mkditawarkan_id),9,7)='$mkid') )					
						";
		
		
		$sql_test = " SELECT
						
						tbl_test.judul
						FROM
						db_ptiik_apps.tbl_test_hasil
						INNER JOIN db_ptiik_apps.tbl_test ON tbl_test.test_id = tbl_test_hasil.test_id
						LEFT JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_test.jadwal_id = tbl_jadwalmk.jadwal_id
						WHERE tbl_test_hasil.tgl_test IS NOT NULL AND  
							(tbl_test_hasil.mahasiswa_id = '$mhs_id' OR (mid(md5(tbl_test_hasil.mahasiswa_id),9,7) = '$mhs_id'))
						AND 
							(mid(md5(tbl_jadwalmk.mkditawarkan_id),9,7)='$mkid' OR (mid(md5(tbl_test.mkditawarkan_id),9,7)='$mkid') ) ";
					
					
		$sql = "SELECT count(judul) as `total` 
				FROM(
					$sql_tugas
					UNION
					$sql_test					
				) progres 
				";
		
		return $this->db->getRow($sql);
	}
	
	
	function get_progress_mhs($mkid=NULL, $mhs_id=NULL, $parent=NULL){
		/*$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_tugas_mhs.skor,
					db_ptiik_apps.tbl_tugas_mhs.total_skor,
					db_ptiik_apps.tbl_tugas_mhs.mahasiswa_id
					FROM
					db_ptiik_apps.tbl_tugas_mhs
					INNER JOIN db_ptiik_apps.tbl_post ON db_ptiik_apps.tbl_tugas_mhs.tugas_id = db_ptiik_apps.tbl_post.link_id
					INNER JOIN db_ptiik_apps.tbl_post_share ON db_ptiik_apps.tbl_post.post_id = db_ptiik_apps.tbl_post_share.id_post
					INNER JOIN db_ptiik_apps.vw_mhs_mkd ON db_ptiik_apps.tbl_post_share.id_user = db_ptiik_apps.vw_mhs_mkd.jadwal_id AND db_ptiik_apps.tbl_tugas_mhs.mahasiswa_id = db_ptiik_apps.vw_mhs_mkd.mahasiswa_id
					";
		if($mkid) $sql .=" AND mid(md5(vw_mhs_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		if($memberid) $sql .= " AND (vw_mhs_mkd.mahasiswa_id = '$memberid' OR (mid(md5(vw_mhs_mkd.mahasiswa_id),9,7) = '$memberid') ) ";
		
		if($parent) $sql .=" AND tbl_post.post_id = '$parent' ";
		
		$sql .= " ORDER BY db_ptiik_apps.tbl_tugas_mhs.last_update DESC";
		
		return $this->db->getRow($sql);*/
		$sql_tugas = "SELECT
							tbl_tugas.tugas_id as `id`,
								tbl_tugas.judul,
								CONCAT(tbl_tugas_mhs.skor , '-' , tbl_tugas_mhs.total_skor) AS nilai,
								DATE(tbl_tugas_mhs.tgl_upload) AS tgl,
								DATE(tbl_tugas.tgl_mulai) AS tgl_mulai
								FROM
								db_ptiik_apps.tbl_tugas_mhs
								INNER JOIN db_ptiik_apps.tbl_tugas ON tbl_tugas.tugas_id = tbl_tugas_mhs.tugas_id
								INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_tugas.jadwal_id = tbl_jadwalmk.jadwal_id
								WHERE (tbl_tugas_mhs.mahasiswa_id = '$mhs_id' OR (mid(md5(tbl_tugas_mhs.mahasiswa_id),9,7) = '$mhs_id')) AND 
									(mid(md5(tbl_jadwalmk.mkditawarkan_id),9,7)='$mkid' OR (mid(md5(tbl_tugas.mkditawarkan_id),9,7)='$mkid') )					
						";
		if($parent) $sql_tugas.= " AND tbl_tugas.tugas_id = '$parent' ";
		
		$sql_test = " SELECT
						tbl_test.test_id as `id`,
						tbl_test.judul,
						CONCAT(tbl_test_hasil.total_skor , '-100.00') AS nilai,
						DATE(tbl_test_hasil.tgl_test) AS tgl,
						DATE(tbl_test.tgl_mulai) AS tgl_mulai
						FROM
						db_ptiik_apps.tbl_test_hasil
						INNER JOIN db_ptiik_apps.tbl_test ON tbl_test.test_id = tbl_test_hasil.test_id
						LEFT JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_test.jadwal_id = tbl_jadwalmk.jadwal_id
						WHERE 
							(tbl_test_hasil.mahasiswa_id = '$mhs_id' OR (mid(md5(tbl_test_hasil.mahasiswa_id),9,7) = '$mhs_id'))
						AND 
							(mid(md5(tbl_jadwalmk.mkditawarkan_id),9,7)='$mkid' OR (mid(md5(tbl_test.mkditawarkan_id),9,7)='$mkid') ) ";
				if($parent) $sql_test.= " AND tbl_test.test_id = '$parent' ";			
					
		$sql = "SELECT *
				FROM(
					$sql_tugas
					UNION
					$sql_test					
				) progres 
				ORDER BY progres.tgl_mulai";
		
		if($parent) return $this->db->getRow($sql);
		
		else return $this->db->query($sql);
	
	}
	
	
	function get_post_assign($mkid=NULL){
		/*$sqlx = "SELECT DISTINCT
					db_ptiik_apps.tbl_post.post_id,
					db_ptiik_apps.tbl_post.link_id,
					db_ptiik_apps.tbl_post.isi,
					db_ptiik_apps.tbl_post.last_update,
					db_ptiik_apps.vw_jadwal_mkd.keterangan AS mkd,
					db_ptiik_apps.tbl_post.kategori,
					db_ptiik_apps.tbl_post.user_id,
					mid(md5(db_ptiik_apps.tbl_post.link_id),9,7) as links,
					mid(md5(db_ptiik_apps.tbl_post_share.id_user),9,7) as id_user,
					db_coms.coms_user.`name` AS `user_name`,db_coms.coms_user.foto,
					GROUP_CONCAT(DISTINCT tbl_post_attach.attach, '|', tbl_post_attach.jenis) attach
				FROM
					db_ptiik_apps.tbl_post
				LEFT JOIN db_ptiik_apps.tbl_post_attach ON tbl_post.post_id = tbl_post_attach.post_id
				LEFT JOIN db_ptiik_apps.tbl_post_share ON db_ptiik_apps.tbl_post_share.id_post = db_ptiik_apps.tbl_post.post_id
				LEFT JOIN db_ptiik_apps.vw_jadwal_mkd ON db_ptiik_apps.vw_jadwal_mkd.mkditawarkan_id = db_ptiik_apps.tbl_post_share.id_user OR db_ptiik_apps.vw_jadwal_mkd.jadwal_id = db_ptiik_apps.tbl_post_share.id_user
				INNER JOIN db_coms.coms_user ON db_ptiik_apps.tbl_post.user_id = db_coms.coms_user.id
				WHERE tbl_post.kategori  IN ('assignment', 'quiz')
				";
		if($mkid) $sqlx .=" AND mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '$mkid' ";
		
		if($memberid) $sqlx .= " AND (coms_user.mahasiswa_id = '$memberid' OR (mid(md5(coms_user.mahasiswa_id),9,7) = '$memberid') ) ";
			
		$sqlx .= " GROUP BY tbl_post.post_id ORDER BY tbl_post.last_update DESC";*/
		
		$sql = "SELECT DISTINCT *
				FROM(
					SELECT
						tbl_tugas.tugas_id as `id`,
							tbl_tugas.judul,
							tbl_tugas.mkditawarkan_id,
							tbl_tugas.jadwal_id,
							DATE(tbl_tugas.tgl_mulai) AS tgl_mulai,
							tbl_jadwalmk.mkditawarkan_id as `mkid`
							FROM
							 db_ptiik_apps.tbl_tugas 
							LEFT JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_tugas.jadwal_id = tbl_jadwalmk.jadwal_id
							WHERE 
								(mid(md5(tbl_jadwalmk.mkditawarkan_id),9,7)='$mkid' OR (mid(md5(tbl_tugas.mkditawarkan_id),9,7)='$mkid') )
					UNION
				
					SELECT
						tbl_test.test_id as `id`,
						tbl_test.judul,
						tbl_test.mkditawarkan_id,
						tbl_test.jadwal_id,
						DATE(tbl_test.tgl_mulai) AS tgl_mulai,
						tbl_jadwalmk.mkditawarkan_id as `mkid`
						FROM
						db_ptiik_apps.tbl_test 
						LEFT JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_test.jadwal_id = tbl_jadwalmk.jadwal_id
						WHERE 
							(mid(md5(tbl_jadwalmk.mkditawarkan_id),9,7)='$mkid' OR (mid(md5(tbl_test.mkditawarkan_id),9,7)='$mkid') )
				) progres
				ORDER BY progres.tgl_mulai";
		
		return $this->db->query($sql);
	}
	
	function get_progress_mahasiswa($mhs_id){
		$sql = "SELECT *
				FROM(
					SELECT 
						tbl_tugas.judul,
						CONCAT(tbl_tugas_mhs.skor , '-' , tbl_tugas_mhs.total_skor) nilai, 
						DATE(tbl_tugas_mhs.tgl_upload) tgl,
						DATE(tbl_tugas.tgl_mulai) tgl_mulai
					FROM db_ptiik_apps.tbl_tugas_mhs
					INNER JOIN db_ptiik_apps.tbl_tugas ON tbl_tugas.tugas_id = tbl_tugas_mhs.tugas_id
					WHERE (tbl_tugas_mhs.mahasiswa_id = '$mhs_id' OR (mid(md5(tbl_tugas_mhs.mahasiswa_id),9,7) = '$mhs_id'))
				
					UNION
				
					SELECT 
						tbl_test.judul,
						CONCAT(tbl_test_hasil.total_skor , '-100.00') nilai, 
						DATE(tbl_test_hasil.tgl_test) tgl,
						DATE(tbl_test.tgl_mulai) tgl_mulai
					FROM db_ptiik_apps.tbl_test_hasil
					INNER JOIN db_ptiik_apps.tbl_test ON tbl_test.test_id = tbl_test_hasil.test_id
					WHERE (tbl_test_hasil.mahasiswa_id = '$mhs_id' OR (mid(md5(tbl_test_hasil.mahasiswa_id),9,7) = '$mhs_id'))
				) progres
				ORDER BY progres.tgl_mulai";
		return $this->db->query($sql);
	}
	
	function mahasiswa_list($jadwal=NULL,$mhs=NULL){
		$sql = "SELECT DISTINCT vw_mhs_mkd.nama,
					   vw_mhs_mkd.mahasiswa_id,
					   mid(md5(vw_mhs_mkd.mahasiswa_id),9,7) as id
				FROM db_ptiik_apps.vw_mhs_mkd
				INNER JOIN db_coms.coms_user ON vw_mhs_mkd.mahasiswa_id = coms_user.mahasiswa_id
				WHERE 1
			   ";
		
		if($jadwal){
			$sql .= " AND mid(md5(vw_mhs_mkd.jadwal_id),9,7) = '".$jadwal."'";
		}
		if($mhs){
			$sql .= " AND mid(md5(vw_mhs_mkd.mahasiswa_id),9,7) = '".$mhs."'";
		}
		$sql .="ORDER BY vw_mhs_mkd.mahasiswa_id";
		
		return $this->db->query($sql);
	}
	
	function get_folder($mkid=NULL, $user=NULL, $parent=NULL){
		$sql = "SELECT
					mid(md5(tbl_media_library_folder.parent_id),9,7) as id, 
					tbl_media_library_folder.id,
					tbl_media_library_folder.folder_name,
					tbl_media_library_folder.parent_id, 
					tbl_media_library_folder.mkditawarkan_id, 
					tbl_media_library_folder.user_id, 
					tbl_media_library_folder.last_update, 
					tbl_media_library_folder.is_available
					FROM
					db_ptiik_apps.tbl_media_library_folder WHERE 1 ";
					
		if($mkid) $sql .=" AND mid(md5(tbl_media_library_folder.mkditawarkan_id),9,7) = '$mkid' ";
		if($user) $sql .=" AND  tbl_media_library_folder.user_id = '$user' ";
		
		if($parent):
			$sql .=" AND ( mid(md5(tbl_media_library_folder.parent_id),9,7) = '$parent' OR tbl_media_library_folder.parent_id = '$parent')";
		else:
			$sql .=" AND tbl_media_library_folder.parent_id = '0' ";
		endif;
		
		
		return $this->db->query($sql);
	}
	
	//--------------------------- file model
	
	function update_avatar($id, $file, $role){
		$sql = "UPDATE db_coms.coms_user SET foto = '$file' WHERE id = '$id'";
		$this->db->query($sql);
		
		$sql = "SELECT karyawan_id, mahasiswa_id FROM db_coms.coms_user WHERE id = '$id'";
		$data = $this->db->getRow($sql);
		
		$karyawan_id = $data->karyawan_id;
		$mahasiswa_id = $data->mahasiswa_id;
		
		if($role == 'dosen'){
			$sql = "UPDATE db_ptiik_apps.tbl_karyawan SET foto = '$file' WHERE karyawan_id = '$karyawan_id'";
			$this->db->query($sql);
		}
		else{
			$sql = "UPDATE db_ptiik_apps.tbl_mahasiswa SET foto = '$file' WHERE mahasiswa_id = '$mahasiswa_id'";
			$this->db->query($sql);
		}
	}
	
	function  update_profile($data, $id){
		return $this->db->update('db_coms`.`coms_user',$data, $id);
	}
	
	function get_file_ext($file_ext){
		$sql = "SELECT max_size FROM db_ptiik_apps.tbl_jenisfile WHERE extention LIKE '%$file_ext%' AND keterangan = 'Image'";
		$data = $this->db->getRow($sql);
		if(isset($data->max_size)) return $data->max_size;
		else return 0;		
	}
	
	function get_file($folder_id){
		$sql = "SELECT
					tbl_media_library.jenis_file,
					tbl_media_library.file_name,
					tbl_media_library.file_loc
				FROM db_ptiik_apps.tbl_media_library
				WHERE tbl_media_library.folder_id = '".$folder_id."'";
		return $this->db->query($sql);
	}
	
	function get_folder_parent_id($folder_id){
		$sql = "SELECT tbl_media_library_folder.parent_id 
				FROM db_ptiik_apps.tbl_media_library_folder 
				WHERE tbl_media_library_folder.id = '".$folder_id."'";
		$strresult = $this->db->getRow( $sql );
		return $strresult->parent_id;
	}
	
	//file upload
	function cek_file_type($file_ext){
		$sql = "SELECT jenis_file_id, kode_jenis, max_size
				FROM db_ptiik_apps.tbl_jenisfile
				WHERE extention LIKE '%".$file_ext."%'";
		return $this->db->getRow($sql);
	}
	
	function get_file_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_file WHERE left(file_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_file_log($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_file',$data)) return TRUE;
		return FALSE;
	}
	
	//------------------------ post model
	
	function save_post_attach($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_post_attach',$data)) return TRUE;
		return FALSE;
	}
	
	function delete_post($post_id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_post_share WHERE MID(MD5(tbl_post_share.id_post),9,7) = '".$post_id."'";
		$this->db->query($sql);
		
		$sql = "DELETE FROM db_ptiik_apps.tbl_post_attach WHERE MID(MD5(tbl_post_attach.post_id),9,7) = '".$post_id."'";
		$this->db->query($sql);
		
		$sql = "DELETE FROM db_ptiik_apps.tbl_post WHERE MID(MD5(tbl_post.post_id),9,7) = '".$post_id."'";
		$this->db->query($sql);
	}
	
	function delete_comment($post_id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_post WHERE MID(MD5(tbl_post.post_id),9,7) = '".$post_id."'";
		$this->db->query($sql);
	}
	
	function edit_post(){
		$post_id = $_POST['id'];
		$tipe = $_POST['tipe'];
		
		if($tipe == 'note') $isi = $_POST['isi'];
		else $isi = $_POST['judul'] . '|' . $_POST['isi'] . '|' . $_POST['tgl'];
		
		$sql = "UPDATE db_ptiik_apps.tbl_post SET isi = '".$isi."' WHERE MID(MD5(tbl_post.post_id),9,7) = '".$post_id."'";
		$this->db->query($sql);
	}
	
	function get_attach_media_library($location=NULL){
		$sql = "SELECT *
				FROM db_ptiik_apps.tbl_media_library
				WHERE 1";
		if($location){
			$sql .= " AND tbl_media_library.file_loc ='".$location."'";	
		}
		$result = $this->db->getRow( $sql );
		return $result;
	}
	
	
	function replace_post_nilai($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_post_nilai',$data)) return TRUE;
		return FALSE;
	}
	
	function save_post_nilai_mhs($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_post_nilai_mhs',$data)) return TRUE;
		return FALSE;
	}
	
}
?>