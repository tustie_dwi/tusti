<?php
class model_pembimbing extends model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public $error;
	public $id;
	public $modified;
	
	function read($staff_id=NULL , $mhsid=NULL){
		$sql = "SELECT MID( MD5(tbl_dosen_pembimbing.mahasiswa_id), 6, 6) as mhs_id,
				MID( MD5(tbl_dosen_pembimbing.karyawan_id), 6, 6) as kar_id,
				tbl_dosen_pembimbing.tahun_akademik,
				tbl_dosen_pembimbing.karyawan_id,
				tbl_dosen_pembimbing.mahasiswa_id,
				tbl_mahasiswa.nim,
				tbl_mahasiswa.nama as nama,
				tbl_mahasiswa.foto as foto_mhs,
				tbl_mahasiswa.angkatan as angkatan,
				tbl_mahasiswa.is_aktif as status,
				(SELECT tbl_prodi.keterangan FROM db_ptiik_apps.tbl_prodi tbl_prodi WHERE tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id) as prodi,
				(SELECT tbl_prodi.strata FROM db_ptiik_apps.tbl_prodi tbl_prodi WHERE tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id) as strata,
				(SELECT tbl_cabang.keterangan FROM db_ptiik_apps.tbl_cabang tbl_cabang WHERE tbl_cabang.cabang_id = tbl_mahasiswa.cabang_id) as cabang,
				MID( MD5(tbl_karyawan.fakultas_id), 6, 6) as fakultas_id,
				CONCAT (IFNULL(tbl_karyawan.gelar_awal,''),' ',tbl_karyawan.nama,' ',IFNULL(tbl_karyawan.gelar_akhir,'')) as dosen,
				tbl_karyawan.alamat as alamat_dosen,
				tbl_karyawan.telp as tlp_dosen,
				tbl_karyawan.foto as foto_dosen,
				tbl_karyawan.email as email_dosen,
				tbl_karyawan.nik,
				(SELECT CONCAT (tbl_tahunakademik.tahun,' ',tbl_tahunakademik.is_ganjil,' ',tbl_tahunakademik.is_pendek) FROM `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik WHERE tbl_tahunakademik.tahun_akademik = tbl_dosen_pembimbing.tahun_akademik) as tahun_mulai";
		if($staff_id && $mhsid){ //detail
			$sql .= " ,
				tbl_mhs_ipk.inf_semester as semester,
				tbl_mhs_ipk.total_sks as sks,
				tbl_mhs_ipk.ip as ip,
				tbl_mhs_ipk.ipk as ipk,
				tbl_mhs_ipk.tahun_akademik as thn_akademik,
				(SELECT CONCAT (tbl_tahunakademik.tahun,' ',tbl_tahunakademik.is_ganjil,' ',tbl_tahunakademik.is_pendek) FROM `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik WHERE tbl_tahunakademik.tahun_akademik = tbl_mhs_ipk.tahun_akademik) as tahun
				";
		}
			$sql .= " 
				FROM `db_ptiik_apps`.`tbl_dosen_pembimbing` tbl_dosen_pembimbing
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa 
					ON tbl_dosen_pembimbing.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
				LEFT JOIN `db_ptiik_apps`.`tbl_karyawan` tbl_karyawan 
					ON tbl_dosen_pembimbing.karyawan_id = tbl_karyawan.karyawan_id
				";
		if($staff_id && $mhsid){ //detail
			$sql .= " 
				LEFT JOIN `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk 
					ON tbl_dosen_pembimbing.mahasiswa_id = tbl_mhs_ipk.mahasiswa_id";
		} 
			$sql .= " WHERE 1";
		if($staff_id){ //view mhs by pembimbing
			$sql .= " AND tbl_dosen_pembimbing.karyawan_id = '".$staff_id."'";
		}
		if($mhsid){ //view pembimbing by mhs
			$sql .= " AND tbl_dosen_pembimbing.mahasiswa_id = '".$mhsid."'";
		}
			$sql .= " ORDER BY tbl_mahasiswa.nim ASC";
		
		
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	function get_data_akademik($mhs=NULL){
		$sql = "SELECT
					MID( MD5(vw_khs_mhs.mahasiswa_id), 6, 6) as mhs_id,
					Sum(vw_khs_mhs.sks) as `sks`, 
					Sum(vw_khs_mhs.sks*vw_khs_mhs.inf_bobot) AS sks_nilai,
					((SUM(vw_khs_mhs.sks*vw_khs_mhs.inf_bobot)) / SUM(vw_khs_mhs.sks)) AS `ipk`,
					(SELECT CONCAT (tbl_tahunakademik.tahun,' ',tbl_tahunakademik.is_ganjil,' ',tbl_tahunakademik.is_pendek) FROM `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik WHERE tbl_tahunakademik.tahun_akademik = vw_khs_mhs.tahun_akademik) as tahun,
					vw_khs_mhs.mahasiswa_id,
					vw_khs_mhs.tahun_akademik
					FROM db_ptiik_apps.`vw_khs_mhs` WHERE vw_khs_mhs.mahasiswa_id = '".$mhs."' 
					GROUP BY
					vw_khs_mhs.mahasiswa_id,
					vw_khs_mhs.tahun_akademik ORDER BY vw_khs_mhs.tahun_akademik ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_data_mhs($mhsid=NULL){
		$sql = "SELECT mid(md5(`tbl_mahasiswa`.`mahasiswa_id`),6,6) as mhs_id,
					   tbl_mahasiswa.*,
					   `db_ptiik_apps`.`tbl_prodi`.keterangan as prodi, 
					   tbl_fakultas.keterangan as fakultas
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi`
					ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas`
					ON tbl_prodi.fakultas_id = tbl_fakultas.fakultas_id WHERE 1 = 1";
		if($mhsid){
			$sql=$sql . " AND (mid(md5(`tbl_mahasiswa`.`mahasiswa_id`),6,6)='".$mhsid."' OR `tbl_mahasiswa`.`mahasiswa_id` = '".$mhsid."') ";
		}
		$sql = $sql . " ORDER BY `tbl_mahasiswa`.`mahasiswa_id` ASC";
			
		$result = $this->db->getRow( $sql );	
		return $result;
	}

}
?>