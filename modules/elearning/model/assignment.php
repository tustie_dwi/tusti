<?php
class model_assignment extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	//=== TUGAS ===//
	function read_tugas($id=NULL,$comment=NULL){
		$sql = "SELECT
				    MID( MD5( tbl_tugas.tugas_id ) , 9,7 ) AS tugasid,
				    MID( MD5( tbl_tugas.jadwal_id ) , 9,7 ) AS jadwalid,
				    MID( MD5( tbl_tugas.materi_id ) , 9,7 ) AS materiid,
				    db_ptiik_apps.tbl_tugas.jadwal_id,
				    db_ptiik_apps.tbl_tugas.tugas_id,
				    db_ptiik_apps.tbl_materimk.judul AS materi,
				    db_ptiik_apps.tbl_tugas.materi_id,
				    MID( MD5(db_ptiik_apps.tbl_materimk.mkditawarkan_id) , 9, 7 ) AS mk,
				    db_ptiik_apps.vw_mk_by_dosen.namamk,
				    db_ptiik_apps.vw_mk_by_dosen.kode_mk,
				    db_ptiik_apps.tbl_tugas.judul,
				    db_ptiik_apps.tbl_tugas.instruksi,
				    db_ptiik_apps.tbl_tugas.keterangan,
				    db_ptiik_apps.tbl_tugas.tgl_mulai,
				    db_ptiik_apps.tbl_tugas.tgl_selesai,
				    db_ptiik_apps.tbl_tugas.user_id AS userid,
				    db_ptiik_apps.tbl_tugas.last_update,
				    db_coms.coms_user.`name` AS `user`,
				    db_ptiik_apps.tbl_materimk.mkditawarkan_id,					
				    tbl_post.post_id,
				    tbl_post_attach.attach,
				    tbl_post_attach.jenis,
				    tbl_post_attach.urut,
				    CASE WHEN jenis = 'link' THEN NULL ELSE tbl_file.file_name END as file_name,
				    CASE WHEN jenis = 'link' THEN NULL ELSE tbl_file.file_size END as file_size,
				    CASE WHEN jenis = 'link' THEN NULL ELSE tbl_file.file_loc END as file_loc
				FROM db_ptiik_apps.tbl_tugas
				LEFT JOIN db_ptiik_apps.tbl_materimk ON db_ptiik_apps.tbl_materimk.materi_id = db_ptiik_apps.tbl_tugas.materi_id
				LEFT JOIN db_ptiik_apps.vw_mk_by_dosen ON db_ptiik_apps.vw_mk_by_dosen.jadwal_id = db_ptiik_apps.tbl_tugas.jadwal_id
				LEFT JOIN db_coms.coms_user ON db_ptiik_apps.tbl_tugas.user_id = db_coms.coms_user.id
				LEFT JOIN db_ptiik_apps.tbl_post ON tbl_tugas.tugas_id = tbl_post.link_id
				LEFT JOIN db_ptiik_apps.tbl_post_attach ON tbl_post.post_id = tbl_post_attach.post_id
				LEFT JOIN db_ptiik_apps.tbl_file ON tbl_file.tugas_id = tbl_tugas.tugas_id AND tbl_file.file_loc = attach
				WHERE 1 = 1 
				AND tbl_post.kategori = 'assignment'
			   ";
		if($id){
			$sql .= " AND MID( MD5(db_ptiik_apps.`tbl_tugas`.tugas_id) , 9,7 ) = '".$id."'";
		}
		$result = $this->db->query( $sql );
		// echo $sql;
		
		return $result;
	}
	
	function get_jadwal($id_dosen=NULL, $mkid=NULL, $mhs=NULL){
		$sql = "SELECT MID( MD5( vw_jadwal_mkd.jadwal_id ) , 9,7 ) as jadwal_id,
					   MID( MD5( vw_jadwal_mkd.mkditawarkan_id ) , 9,7 ) as mkid,
					   vw_jadwal_mkd.mkditawarkan_id, 
					   vw_jadwal_mkd.kelas, 
					   vw_jadwal_mkd.keterangan as namamk,
					   vw_jadwal_mkd.nama_dosen as nama,
					  CONCAT (vw_jadwal_mkd.keterangan , ' ' , vw_jadwal_mkd.kelas , ' - [', tbl_prodi.keterangan, ']') as jadwal
				FROM `db_ptiik_apps`.`vw_jadwal_mkd`
				LEFT JOIN `db_ptiik_apps`.tbl_prodi ON tbl_prodi.prodi_id = vw_jadwal_mkd.prodi_id
				WHERE  1
				";
		if($id_dosen){
			$sql .= " AND vw_jadwal_mkd.karyawan_id = '".$id_dosen."'";
		}
		
		if($mkid){
			$sql .= " AND  mid(md5(vw_jadwal_mkd.mkditawarkan_id),9,7) = '$mkid'";
		}
		$result = $this->db->query( $sql );

		return $result;
	}
	
	
	
	function tugas_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tugas_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_tugas WHERE left(tugas_id,6)='".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_jadwal_id_by_md5($id=NULL){
		$sql= "SELECT jadwal_id as jadwal_id
		       FROM db_ptiik_apps.`tbl_jadwalmk` 
		       WHERE mid(md5(`jadwal_id`),9,7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->jadwal_id;
			return $strresult;
		}
	}
	
	function cek_new_tugas_by_judul($ket=NULL){
		$sql = "SELECT judul 
				from `db_ptiik_apps`.`tbl_tugas` 
				where judul = '".$ket."' ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_id_tugas_by_id($ket=NULL){
		$sql = "SELECT tugas_id 
				from `db_ptiik_apps`.`tbl_tugas` 
				where judul = '".$ket."' ";
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->tugas_id;
			}
			return $id;
		}
	}
	
	function get_tugas_submitted_all($user_id = NULL, $id = NULL){
		$sql = "SELECT MID( MD5(db_ptiik_apps.`tbl_tugas`.tugas_id) , 9,7 ) as tgs_id,
					   tbl_tugas.judul,
					   tbl_tugas.keterangan,
					   tbl_tugas.instruksi,
					   tbl_tugas.jadwal_id,
					   tbl_tugas.tgl_mulai,
					   tbl_tugas.tgl_selesai,
					   vw_jadwal_mkd.keterangan as mk,
					   vw_jadwal_mkd.kelas						
				FROM `db_ptiik_apps`.`tbl_tugas`
				LEFT JOIN `db_ptiik_apps`.`vw_jadwal_mkd` ON tbl_tugas.jadwal_id = vw_jadwal_mkd.jadwal_id
				WHERE user_id = '".$user_id."'";
		if($id){
			$sql .= " AND MID( MD5(db_ptiik_apps.`tbl_tugas`.tugas_id) , 9,7 ) = '".$id."'";
			$sql .= " ORDER BY tbl_tugas.last_update DESC ";
			// echo $sql;
			$result = $this->db->getRow( $sql );
		}
		else{
			$sql .= " ORDER BY tbl_tugas.last_update DESC ";
			$result = $this->db->query( $sql );
		}
		// echo $sql;
		return $result;
	}
	
	function replace_tugas($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tugas',$datanya);
	}
	/*function replace_post($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_post',$datanya);
	}
	function replace_share($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_post_share',$datanya);
	}*/
	
	//=== TUGAS ===//
	
	//=== TUGAS MHS ===//
	function read_tugas_mhs($id=NULL,$upload=NULL, $mhs=NULL, $comment=NULL){
		$sql = "SELECT tbl_tugas_mhs.mahasiswa_id as mhs_id,
				tbl_mahasiswa.nama as nama_mhs,
				tbl_mahasiswa.nim,
                tbl_tugas_mhs.tugas_id as tgs_id,
                MID( MD5(tbl_tugas_mhs.tugas_id) , 9,7 ) as tugas_id,
                tbl_tugas_mhs.upload_id as uploadid,
                MID( MD5(tbl_tugas_mhs.upload_id) , 9,7 ) as upload,
                tbl_tugas.judul as judul_tugas,
                tbl_tugas.instruksi as instruksi,
                tbl_tugas.keterangan as keterangan,
                tbl_tugas_mhs.tgl_upload,
                tbl_tugas_mhs.catatan,
                tbl_file.file_loc as tugas_loc,
                MID( MD5(tbl_file.file_id) , 9,7 ) as fileid,
                tbl_file.file_name as file_name,
                tbl_file.file_size as file_size,
                tbl_tugas_mhs.skor,
                tbl_tugas_mhs.total_skor";
    if($comment){
		$sql .= ",tbl_post.post_id";
	}
      $sql .= " FROM db_ptiik_apps.`tbl_tugas_mhs`
                LEFT JOIN db_ptiik_apps.tbl_tugas ON tbl_tugas.tugas_id = tbl_tugas_mhs.tugas_id
                LEFT JOIN db_ptiik_apps.tbl_file ON tbl_file.upload_id = tbl_tugas_mhs.upload_id
                LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_tugas_mhs.mahasiswa_id";
    if($comment){
		$sql .= " LEFT JOIN db_ptiik_apps.tbl_post ON tbl_tugas_mhs.upload_id = tbl_post.link_id";
	}
	  $sql .= " WHERE 1 = 1
				";
		if($comment){
			$sql .= " AND tbl_post.kategori = 'response'";
		}
		if($id){
			$sql = $sql . "	AND MID( MD5(tbl_tugas_mhs.tugas_id) , 9,7 ) = '".$id."'";
		}
		
		if($upload){
			$sql = $sql . "	AND MID( MD5(tbl_tugas_mhs.upload_id) , 9,7 ) = '".$upload."'";
		}
		
		if($mhs){
			$sql = $sql . " AND MID( MD5(tbl_tugas_mhs.mahasiswa_id) , 9,7 ) = '".$mhs."' ";
		}
		
		$sql = $sql. " ORDER BY tbl_tugas_mhs.last_update DESC ";
		
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;
	}
	
	function get_md5_mhs_id($id=NULL){
		$sql= "SELECT  mid(md5(`mahasiswa_id`),9,7) as mhs_id
		       FROM db_ptiik_apps.`tbl_mahasiswa` 
		       WHERE mahasiswa_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->mhs_id;
			return $strresult;
		}
	}
	
	function upload_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(upload_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_tugas_mhs WHERE left(upload_id,6) = '".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_mk_by_jadwal($id=NULL){
		$sql = "SELECT mkditawarkan_id 
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				";
		
		if($id){
			$sql .= "WHERE MID( MD5( `jadwal_id` ) , 9,7 ) = '".$id."'";
		}
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->mkditawarkan_id;
		return $strresult;
	}
	
	function get_uploadid_by_mhs_tgs($id=NULL,$tgs=NULL){
		$sql = "SELECT upload_id 
				FROM `db_ptiik_apps`.`tbl_tugas_mhs`
				WHERE 1";
		
		if($id){
			$sql .= " AND (tbl_tugas_mhs.mahasiswa_id = '".$id."' OR MID( MD5(tbl_tugas_mhs.mahasiswa_id) , 9,7 )  = '".$id."')";
		}
		if($tgs){
			$sql .= " AND (tbl_tugas_mhs.tugas_id = '".$tgs."' OR MID( MD5(tbl_tugas_mhs.tugas_id) , 9,7 )  = '".$id."')";
		}
		//echo $sql;
		$dt = $this->db->getRow( $sql );
		if($dt):
			$strresult = $dt->upload_id;
		else:
			$strresult = $this->upload_id();
		endif;
		return $strresult;
	}		
	
	function get_tugas_id_by_md5($id=NULL){
		$sql= "SELECT tugas_id as tugas_id
		       FROM db_ptiik_apps.`tbl_tugas` 
		       WHERE mid(md5(`tugas_id`),9,7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->tugas_id;
			return $strresult;
		}
	}
	
	function replace_tugas_mhs($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tugas_mhs',$datanya);
	}
	
	function update_skor($skor=NULL, $total=NULL, $id=NULL){
		$sql= "UPDATE db_ptiik_apps.`tbl_tugas_mhs`
			   SET skor = '".$skor."', total_skor = '".$total."'  
		       WHERE MID( MD5(upload_id) , 9,7 ) = '".$id."'
			 "; 
		$result = $this->db->query( $sql );
		if(isset($result)){
			$this->update_post_nilai($skor, $total, $id);
			
			return TRUE;
			
		}
	}
	//=== TUGAS MHS ===//
	
	//=== POST ===//
	function get_post_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(post_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_post WHERE left(post_id,6)='".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function update_post_nilai($skor=NULL, $total=NULL, $id=NULL){
		$sql = "SELECT upload_id FROM db_ptiik_apps.`tbl_tugas_mhs` WHERE MID( MD5(upload_id) , 9,7 ) = '".$id."' ";
		$row = $this->db->getRow( $sql );
		
		if($row){
			$nilai_id = 'TG'.$row->upload_id;
			// return $nilai_id;
			$sqlupdate= "UPDATE `db_ptiik_apps`.`tbl_post_nilai_mhs`
				   SET status='Marked', skor = '".$skor."', total_skor = '".$total."'  
				   WHERE nilai_id = '".$nilai_id."'
				 "; 
			$result = $this->db->query( $sqlupdate );
			if(isset($result)){				
				return TRUE;
				
			}
		}
	}
}
?>