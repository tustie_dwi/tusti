<?php
class model_event extends model {
	
	function get_aktifitasId(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(aktifitas_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_aktifitas WHERE left(aktifitas_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_aktifitas($data){
		if(! $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$data)) echo mysql_error();
	}
	
	
	
	function get_ruangan($fak=NULL, $cabang=NULL){
		$sql = "SELECT tbl_ruang.ruang_id, tbl_ruang.kode_ruang, tbl_ruang.keterangan
				FROM db_ptiik_apps.tbl_ruang
				WHERE tbl_ruang.cabang_id = '$cabang' AND tbl_ruang.fakultas_id = '$fak'";
				
		return $this->db->query($sql);
	}
	
	function get_jeniskegiatan(){
		$sql = "SELECT tbl_jeniskegiatan.jenis_kegiatan_id, tbl_jeniskegiatan.keterangan
				FROM db_ptiik_apps.tbl_jeniskegiatan";
		return $this->db->query($sql);
	}
	
	function edit_aktifitas($mulai, $selesai, $tgl_selesai, $ruang){
		$id = $_POST['time'];
		$judul = $_POST['edit_judul'];
		$lokasi = $_POST['edit_lokasi'];
		$jenis = $_POST['edit_jenis_kegiatan'];
		
		$sql = "UPDATE db_ptiik_apps.tbl_aktifitas 
				SET judul = '$judul', 
					jam_mulai = '$mulai', 
					jam_selesai = '$selesai', 
					lokasi='$lokasi', 
					tgl_selesai = '$tgl_selesai',
					inf_ruang = '$ruang',
					jenis_kegiatan_id = '$jenis'
				WHERE MID(MD5(tbl_aktifitas.aktifitas_id),8,6) = '$id'";
				
			// echo $sql;
		$this->db->query($sql);
	}
	
	function hapus_aktifitas(){
		$id = $_POST['id'];
		$sql = "DELETE FROM db_ptiik_apps.tbl_aktifitas 
				WHERE MID(MD5(tbl_aktifitas.aktifitas_id),8,6) = '$id'";
		$this->db->query($sql);
	}
	
		
	function get_reg_aktifitas(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(aktifitas_id,6) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_aktifitas
			WHERE left(aktifitas_id,6)='".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	
	function replace_aktifitas($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$datanya);
	}
	
	function clean(){
		 $sql = "DELETE FROM `db_ptiik_apps`.`tbl_status`";
		 $this->db->query( $sql );
	}
	
	function delete($id){
		 $sql = "DELETE FROM `db_ptiik_apps`.`tbl_status` WHERE `tbl_status`.`status_id` = '".$id."'";
		 $this->db->query( $sql );
	}
	
	function del_comment($id){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_status` SET `tbl_status`.`is_delete` = 1 WHERE mid(md5(`tbl_status`.`status_id`),6,6) = '".$id."'";
		$this->db->query( $sql );
	}
	
	function del_status($id){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_status` SET `tbl_status`.`is_delete` = 1 WHERE mid(md5(`tbl_status`.`status_id`),6,6) = '".$id."'";
		$this->db->query( $sql );
	}
	
	function del_parent($id){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_status` SET `tbl_status`.`is_delete` = 1 WHERE mid(md5(`tbl_status`.`parent_id`),6,6) = '".$id."'";
		$this->db->query( $sql );
	}
	
	
	function get_aktifitas($jenis=NULL, $tgl=NULL, $id = NULL){
		$sql= "SELECT
				mid(md5(tbl_aktifitas.aktifitas_id),6,6) AS aktifitas_id,
				db_ptiik_apps.tbl_aktifitas.aktifitas_id AS hid_id,
				db_ptiik_apps.tbl_aktifitas.karyawan_id,
				db_ptiik_apps.tbl_aktifitas.user_id,
				db_ptiik_apps.tbl_aktifitas.mahasiswa_id,
				db_ptiik_apps.tbl_aktifitas.hari,
				db_ptiik_apps.tbl_aktifitas.tgl,
				db_ptiik_apps.tbl_aktifitas.tgl_selesai,
				TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_mulai,'%H:%i') as `jam_mulai`,
				TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_selesai,'%H:%i') as `jam_selesai`,
				db_ptiik_apps.tbl_aktifitas.judul,
				db_ptiik_apps.tbl_aktifitas.nama_mk,
				db_ptiik_apps.tbl_aktifitas.kode_mk,
				db_ptiik_apps.tbl_aktifitas.catatan,
				db_ptiik_apps.tbl_aktifitas.is_finish,
				db_ptiik_apps.tbl_aktifitas.inf_kategori,
				coms_user.`name` AS nama_user,
				db_ptiik_apps.tbl_aktifitas.inf_ruang,
				db_ptiik_apps.tbl_aktifitas.lokasi,
				tbl_aktifitas.jenis_kegiatan_id,
				db_ptiik_apps.tbl_jeniskegiatan.keterangan AS `jenis_kegiatan`
			FROM
					db_ptiik_apps.tbl_aktifitas
				LEFT JOIN db_coms.coms_user ON db_ptiik_apps.tbl_aktifitas.user_id = db_coms.coms_user.id
				LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan ON db_ptiik_apps.tbl_aktifitas.jenis_kegiatan_id = db_ptiik_apps.tbl_jeniskegiatan.jenis_kegiatan_id
					WHERE 1 = 1
				 ";
							
		if($jenis){
			$sql = $sql. " AND tbl_aktifitas.inf_kategori='".$jenis."' ";
		}
		
		if($tgl){
			$sql = $sql. " AND ('".$tgl."' BETWEEN tbl_aktifitas.tgl AND tbl_aktifitas.tgl_selesai) ";					  
		}
		
		if($id){
			$sql = $sql. " AND (tbl_aktifitas.user_id='".$id."' OR tbl_aktifitas.karyawan_id='".$id."' OR tbl_aktifitas.mahasiswa_id='".$id."') ";
		}
		
		$sql = $sql. " ORDER BY tbl_aktifitas.tgl DESC, tbl_aktifitas.jam_mulai DESC ";
	
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function finish_tasks($id=NULL,$val=NULL){
		$sql= "UPDATE db_ptiik_apps.tbl_aktifitas tbl_aktifitas
			   SET is_finish = '".$val."'
			   WHERE mid(md5(tbl_aktifitas.aktifitas_id),6,6) = '".$id."'";
				
		$result = $this->db->query( $sql );
		
		return $result;
	}
	function delete_tasks($id=NULL){
		$sql= "DELETE FROM db_ptiik_apps.tbl_aktifitas
			   WHERE mid(md5(db_ptiik_apps.tbl_aktifitas.aktifitas_id),6,6) = '".$id."'";
				
		if($this->db->query( $sql )){
			return TRUE;
		}
	}
	
	function getEvent($day=NULL,$month=NULL,$year=NULL, $runday=NULL, $user=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$this->addNol($month)."-".$this->addNol(($day+1));
		
		$hari=$this->get_namahari($runday);
		
		$sqlx = "SELECT DISTINCT mid(md5(agenda_id),5,5) as `id`, agenda_id, judul, 
					keterangan, (SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_agenda.jenis_kegiatan_id) as `jenis`,
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%H:%i') as `jam_mulai`, 
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_selesai, '%H:%i') as `jam_selesai`,
					DATE_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%d') as `ds`, 
					DATE_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_selesai, '%d') as `de` 
				FROM `db_ptiik_apps`.`tbl_agenda` 
					WHERE `tbl_agenda`.`status_agenda` = 'publish' AND `tbl_agenda`.`is_delete` = '0' AND (((tgl_mulai like '".$tgl."%' OR  tgl_selesai like '".$tgl."%')  AND inf_hari like '%".$hari."%' )
						OR ((date_format(tgl_mulai,'%Y-%m-%d') < '".$tgl."' AND date_format(tgl_selesai,'%Y-%m-%d') > '".$tgl."')  AND inf_hari like '%".$hari."%' )) ";
		$sqlx = $sqlx . " ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%H:%i') ASC ";
		
		$sql = "SELECT DISTINCT
				MID(MD5(db_ptiik_apps.tbl_aktifitas.aktifitas_id),8,6) id, 
				db_ptiik_apps.tbl_aktifitas.tgl,
				db_ptiik_apps.tbl_aktifitas.tgl_selesai,
				TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_mulai,'%H:%i') as `jam_mulai`, 
				TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_selesai,'%H:%i') as `jam_selesai`, 
				db_ptiik_apps.tbl_aktifitas.judul,
				db_ptiik_apps.tbl_aktifitas.nama_mk,
				db_ptiik_apps.tbl_aktifitas.kode_mk,
				db_ptiik_apps.tbl_aktifitas.catatan  as `keterangan`,
				db_ptiik_apps.tbl_aktifitas.inf_ruang as `ruang`,
				db_ptiik_apps.tbl_aktifitas.lokasi,
				db_ptiik_apps.tbl_aktifitas.inf_kategori,
				db_ptiik_apps.tbl_aktifitas.jenis_kegiatan_id,
				(SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_aktifitas.jenis_kegiatan_id) as `jenis`,
				db_ptiik_apps.tbl_aktifitas.agenda_id,
				tbl_jeniskegiatan.keterangan jenis_kegiatan
				FROM
				db_ptiik_apps.tbl_aktifitas
				LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_aktifitas.jenis_kegiatan_id
				WHERE
				('".$tgl."' BETWEEN tbl_aktifitas.tgl AND tbl_aktifitas.tgl_selesai) ";
		if($user){
			$sql = $sql. " AND (tbl_aktifitas.user_id='".$user."' OR tbl_aktifitas.karyawan_id='".$user."' OR tbl_aktifitas.mahasiswa_id='".$user."') ";
		}
		$sql = $sql. " ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_aktifitas`.jam_mulai,'%H:%i') ASC ";
		
		return $this->db->query( $sql );
	
	}
	
	function get_all_event($user=NULL, $bulan=NULL, $tahun=NULL){
		$sql = "SELECT DISTINCT
					MID(MD5(tbl_aktifitas.aktifitas_id),8,6) id, 
					tbl_aktifitas.tgl,
					tbl_aktifitas.tgl_selesai,
					TIME_FORMAT(tbl_aktifitas.jam_mulai,'%H:%i') AS `jam_mulai`, 
					TIME_FORMAT(tbl_aktifitas.jam_selesai,'%H:%i') AS `jam_selesai`, 
					tbl_aktifitas.judul,
					GROUP_CONCAT(' ', tbl_ruang.kode_ruang, ' - ', tbl_ruang.keterangan) ruang_ket,
					GROUP_CONCAT(tbl_ruang.kode_ruang) ruang,
					tbl_aktifitas.lokasi,
					tbl_aktifitas.inf_kategori,
					tbl_aktifitas.jenis_kegiatan_id,
					tbl_jeniskegiatan.keterangan jenis_kegiatan
				FROM db_ptiik_apps.tbl_aktifitas
				LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_aktifitas.jenis_kegiatan_id
				LEFT JOIN db_ptiik_apps.tbl_ruang ON tbl_aktifitas.inf_ruang LIKE CONCAT('%',tbl_ruang.ruang_id,'%')
				WHERE (tbl_aktifitas.user_id='$user' OR tbl_aktifitas.karyawan_id='$user' OR tbl_aktifitas.mahasiswa_id='$user')
					AND $bulan BETWEEN MONTH(tbl_aktifitas.tgl) AND MONTH(tbl_aktifitas.tgl_selesai) 
					AND (YEAR(tbl_aktifitas.tgl) = $tahun OR YEAR(tbl_aktifitas.tgl_selesai) = $tahun)
				GROUP BY tbl_aktifitas.aktifitas_id 
				ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_aktifitas`.jam_mulai,'%H:%i') ASC ";
		
		return $this->db->query( $sql );
	}
	
	function get_namahari($runday){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
			case '7': $hari='minggu'; break;
		}
		
		return $hari;
	}
	
	function potong_kalimat ($content, $length) {
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
	function addnol($string){
		if(strlen($string)==1){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	
}

?>