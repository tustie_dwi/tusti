<?php
class model_quiz extends model {
	private $coms;
	
	public function __construct() {
		parent::__construct();	
	}
	
	function read($testid=NULL,$staff=NULL){
		$sql = "SELECT  mid(md5(`tbl_test`.`test_id`),9,7) as `test_id`,
				`tbl_test`.`test_id` as `hid_id`,
				mid(md5(`tbl_test`.`mkditawarkan_id`),9,7) as `mkditawarkan_id`,
				mid(md5(`tbl_test`.`materi_id`),9,7) as `materi_id`,
				mid(md5(`tbl_test`.`jadwal_id`),9,7) as `jadwal_id`,
				`tbl_test`.`judul`,
				`tbl_test`.`time_limit`,
				`tbl_test`.`tgl_mulai`,
				`tbl_test`.`tgl_selesai`,
				`tbl_test`.`instruksi`,
				`tbl_test`.`keterangan`,
				`tbl_test`.`is_random`,
				`tbl_test`.`is_publish`,
				`tbl_materimk`.`judul` as `materi`,
                `tbl_namamk`.`keterangan` as `namamk`,
				`tbl_test`.`last_update`,
              	substring(`tbl_test`.`last_update`, 1,10) as `YMD`,
        	  	substring(`tbl_test`.`last_update`, 12,8) as `waktu`,
        	  	
        	  	(SELECT a.username FROM `db_coms`.`coms_user` as a WHERE a.id =  `tbl_test`.user_id) as user
				
				FROM `db_ptiik_apps`.`tbl_test` 
                LEFT JOIN `db_ptiik_apps`.`tbl_materimk` ON `tbl_materimk`.`materi_id` = `tbl_test`.`materi_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` ON `tbl_mkditawarkan`.`mkditawarkan_id` = `tbl_test`.`mkditawarkan_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` ON `tbl_matakuliah`.`matakuliah_id` = `tbl_mkditawarkan`.`matakuliah_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_pengampu` ON `tbl_pengampu`.`mkditawarkan_id` = `tbl_mkditawarkan`.`mkditawarkan_id`
				WHERE 1
			   ";
	  	
	  	if($testid!=""){
			$sql = $sql . " AND mid(md5(`tbl_test`.`test_id`),9,7) = '".$testid."' ";
		}
		
		if($staff!=""){
			$sql = $sql . " AND `tbl_pengampu`.`karyawan_id` = '".$staff."' ";
		}
	  	
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_soal($testid=NULL){
		$sql = "SELECT MID( MD5(tbl_test_soal.soal_id), 9, 7) as soal_id,
					tbl_test_soal.soal_id as hid_id,
					tbl_test_soal.jadwal_id,
					tbl_test_soal.materi_id,
					tbl_test_soal.mkditawarkan_id,
					tbl_test_soal.test_id,
					MID( MD5(tbl_test_soal.test_id), 9, 7) as testid,
					tbl_test_soal.poin,
					tbl_test_soal.pertanyaan,
					tbl_test_soal.kategori_id,
					MID( MD5(tbl_test_soal.kategori_id), 9, 7) as kategoriid,
					tbl_test.judul as judul_tes,
				    tbl_namamk.keterangan as mata_kuliah,
				    (select judul from `db_ptiik_apps`.`tbl_materimk` where materi_id = tbl_test_soal.materi_id) as materi,
				    (select concat(mata_kuliah, ' ', kelas) from `db_ptiik_apps`.`tbl_jadwalmk` where jadwal_id = tbl_test_soal.jadwal_id) as jadwal,
				    tbl_test_kategori.keterangan as kategori,
				    tbl_test_kategori.parameter_input as input,
				    (select username from coms.coms_user where id = tbl_test_soal.user_id) as user,
                    tbl_test_jawab.is_random as random_answer,
                    COUNT(`tbl_test_jawab`.`jawaban_id`) as answernum
				FROM `db_ptiik_apps`.`tbl_test_soal`,
				     `db_ptiik_apps`.`tbl_mkditawarkan`,
				     `db_ptiik_apps`.`tbl_matakuliah`,
				     `db_ptiik_apps`.`tbl_namamk`,
				     `db_ptiik_apps`.`tbl_test_kategori`,
				     `db_ptiik_apps`.`tbl_test`,
                     `db_ptiik_apps`.`tbl_test_jawab`
				WHERE tbl_test_soal.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
				      AND tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				      AND tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				      AND tbl_test_soal.kategori_id = tbl_test_kategori.kategori_id
				      AND tbl_test_soal.test_id = tbl_test.test_id
                      AND tbl_test_soal.soal_id = tbl_test_jawab.soal_id 
			   ";
	  	
	  	if($testid!=""){
			$sql = $sql . " AND mid(md5(`tbl_test_soal`.`test_id`),9,7) = '".$testid."' ";
		}
	  	
	  	$sql = $sql . " GROUP BY tbl_test_soal.soal_id ";
	  	
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_jawaban($testid=NULL){
		$sql = "SELECT MID( MD5(`tbl_test_jawab`.`jawaban_id`), 9, 7) as `jawaban_id`,
				`tbl_test_jawab`.`jawaban_id` as `hidId`,
				`tbl_test_jawab`.`soal_id` as `hidIdSoal`,
				MID( MD5(`tbl_test_jawab`.`soal_id`), 9, 7) as `soal_id`,
				`tbl_test_jawab`.`keterangan`,
				`tbl_test_jawab`.`is_benar`,
				`tbl_test_jawab`.`skor`,
				`tbl_test_jawab`.`urut`,
				`tbl_test_jawab`.`is_random`
				FROM `db_ptiik_apps`.`tbl_test_jawab`
				LEFT JOIN `db_ptiik_apps`.`tbl_test_soal` ON `tbl_test_soal`.`soal_id` = `tbl_test_jawab`.`soal_id`
				WHERE 1
			   ";
	  	
	  	if($testid!=""){
			$sql = $sql . " AND mid(md5(`tbl_test_soal`.`test_id`),9,7) = '".$testid."' ";
		}
	  	
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_kategori($id=NULL){
		$sql = "SELECT MID( MD5(kategori_id), 9, 7) as kat_id,
					   kategori_id as hid_id,
					   keterangan as kategori,
					   parameter_input as jenis_input
				FROM `db_ptiik_apps`.`tbl_test_kategori`";
		if($id!=""){
			$sql = $sql . " WHERE  MID( MD5(kategori_id), 9, 7) = '".$id."' ";
		}
	
		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_jadwal($cek=NULL,$staffid=NULL,$type=NULL){
		$sql = "SELECT MID( MD5( jadwal_id ) , 9,7 ) as jadwal_id,
					   MID( MD5( mkditawarkan_id ) , 9,7 ) as mkid,
					   mkditawarkan_id, 
					   kelas, 
					   prodi_id,
					   namamk,
					   nama, ";
		if($type=="exam-type"){
		$sql.= " namamk as jadwal ";	
		}else{
		$sql.= " CONCAT (namamk , ' ' , kelas) as jadwal ";		
		}			   		  
		$sql.= " FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				WHERE `vw_mk_by_dosen`.`is_aktif`= '1'
				";
				
		if($cek==""){
			$sql .= " AND vw_mk_by_dosen.karyawan_id = '".$staffid."'";
		}
		else {
			$sql=$sql . " AND MID( MD5( vw_mk_by_dosen.karyawan_id ) , 9, 7 ) = '".$staffid."' ";
		}
		if($type=="exam-type"){
			$sql=$sql . " GROUP BY mkditawarkan_id ";
		}	
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_materi($cek=NULL,$mkid=NULL){
		$sql = "SELECT MID( MD5( `materi_id` ) , 9, 7 ) as `materi_id`, `judul` 
			    FROM db_ptiik_apps.`tbl_materimk` 
			    WHERE `parent_id` =  '0' AND db_ptiik_apps.tbl_materimk.is_valid = 1
				";
				
		if($cek==""){
			$sql=$sql . " AND `mkditawarkan_id` = '".$id."' ";
		}
		else {
			$sql=$sql . " AND MID( MD5( `mkditawarkan_id` ) , 9, 7 ) = '".$mkid."' ";
		}
		
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_mkid_md5($mkid=NULL){
		$sql = "SELECT tbl_mkditawarkan.mkditawarkan_id as data 
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
				WHERE mid(md5(tbl_mkditawarkan.mkditawarkan_id),9,7) = '".$mkid."'";
				
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_jadwalid_md5($jadwalid=NULL){
		$sql = "SELECT tbl_jadwalmk.jadwal_id, tbl_jadwalmk.mkditawarkan_id 
			    FROM db_ptiik_apps.`tbl_jadwalmk` 
			    WHERE mid(md5(tbl_jadwalmk.jadwal_id),9,7) = '".$jadwalid."'";
		
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_materiid_md5($materiid=NULL){
		$sql = "SELECT tbl_materimk.materi_id as data 
				FROM `db_ptiik_apps`.`tbl_materimk` 
				WHERE mid(md5(tbl_materimk.materi_id),9,7) = '".$materiid."'";
				
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_test_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(test_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_test"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_test($datanya){
		if($this->db->replace('db_ptiik_apps`.`tbl_test',$datanya))return TRUE;
		else return FALSE;
	}
	
	function get_soal_id($semester=NULL){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(soal_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_test_soal`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_soal($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_soal',$datanya);
	}
	
	function get_jawabid($id=NULL, $soalid=NULL){
		$sql= "SELECT jawaban_id 
		       FROM `db_ptiik_apps`.`tbl_test_jawab`
		       WHERE MID( MD5(jawaban_id), 9, 7) = '".$id."' AND soal_id = '".$soalid."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if($dt){
			$strresult = $dt->jawaban_id;
		}
		else{
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jawaban_id,4) AS 
				  unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				  FROM db_ptiik_apps.tbl_test_jawab";
			
			$dt = $this->db->getRow( $sql );
		
			$strresult = $dt->data;
		}
		return $strresult;
	}
	
	function replace_jawaban($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_jawab',$datanya);
	}
	
	function get_post_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(post_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_post"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_post($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_post',$datanya);
	}
	
	function replace_post_share($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_post_share',$datanya);
	}
	
	function get_namamkid($mkid=NULL){
		$sql = "SELECT 
				(SELECT `db_ptiik_apps`.`tbl_namamk`.namamk_id
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah` 
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk_id
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
				WHERE mkditawarkan_id = '".$mkid."'
			   ";
			   
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->namamk_id;
	
		return $strresult;}
	}
	
	function replace_bank_soal($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_bank_soal',$datanya);
	}
	
	function replace_bank_jawab($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_bank_jawab',$datanya);
	}
	
	function get_bank_soal($mkid=NULL){
		$sql = "SELECT MID( MD5(`tbl_bank_soal`.`soal_id`), 9, 7) as `soal_id`,
				`tbl_bank_soal`.`soal_id` as `hidId`,
				`tbl_bank_soal`.`jadwal_id`,
				`tbl_bank_soal`.`materi_id`,
				`tbl_bank_soal`.`mkditawarkan_id`,
				`tbl_bank_soal`.`namamk_id`,
				`tbl_namamk`.`keterangan` as `namamk`,
				`tbl_bank_soal`.`kategori_id`,
				MID( MD5(`tbl_bank_soal`.`kategori_id`), 9, 7) as kathid_id,
				`tbl_bank_soal`.`pertanyaan`,
				`tbl_bank_soal`.`user_id`,
				`tbl_bank_soal`.`last_update`,
				
				`tbl_namamk`.`keterangan` as `namamk`,
				`tbl_test_kategori`.`keterangan` as `kategori`,
				`tbl_test_kategori`.`parameter_input`,
                CASE
                  WHEN `tbl_test_kategori`.`kategori_id`
                  THEN COUNT(`tbl_bank_jawab`.`jawaban_id`)
                  ELSE '0'
                END as `count_answer`
				FROM `db_ptiik_apps`.`tbl_bank_soal` 
				LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON `tbl_namamk`.`namamk_id` = `tbl_bank_soal`.`namamk_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_test_kategori` ON `tbl_test_kategori`.`kategori_id` = `tbl_bank_soal`.`kategori_id`
                                LEFT JOIN `db_ptiik_apps`.`tbl_bank_jawab` ON `tbl_bank_jawab`.`soal_id` = `tbl_bank_soal`.`soal_id`
				WHERE 1
				";
		if($mkid!=""){
			$sql = $sql . " AND MID( MD5(`tbl_bank_soal`.`mkditawarkan_id`), 9, 7) = '".$mkid."' ";
		}
		$sql = $sql . " GROUP BY `tbl_bank_soal`.`soal_id` ";
		$result = $this->db->query($sql);
		return $result;
	}
	
	function get_bank_jawab($soalid=NULL){
		$sql = "SELECT 
				MID( MD5(`tbl_bank_jawab`.`jawaban_id`), 9, 7) as `jawaban_id`,
				`tbl_bank_jawab`.`jawaban_id` as hidId,
				`tbl_bank_jawab`.`keterangan`,
				`tbl_bank_jawab`.`is_benar`,
				`tbl_bank_jawab`.`skor`
				FROM `db_ptiik_apps`.`tbl_bank_jawab` 
				WHERE 1
				";
		if($soalid!=""){
			$sql = $sql . " AND MID( MD5(`tbl_bank_jawab`.`soal_id`), 9, 7) = '".$soalid."' ";
		}
		$result = $this->db->query($sql);
		return $result;
	}
	
}
?>