<?php
class elearning_course extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($mkid=NULL, $kategori=NULL, $memberid=NULL, $jadwal=NULL, $materi=NULL, $file_id=NULL, $action=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		
		if(isset($mkdi)) $data['mkid'] = $mkid;
		else $data['mkid'] = "-";
		
		$m_general	  = new model_general();
		$m_assignment = new model_assignment();
		$mquiz		  = new model_quiz();
		
		$key 		= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $key->foto;
		$data['jadwal_id']	= $jadwal;
		$data['materi_id']	= $materi;
		$data['file_id']	= $file_id;
		$data['straction'] 	= $action;
			
		
		if(! empty($staff)):
			$data['mkd'] = $m_general->get_mkd_dosen($staff, $mkid);
			$data['jadwal'] = $m_assignment->get_jadwal($staff, $mkid);
		else:
			$data['mkd'] = $m_general->get_mkd_mhs($mhs_id, $mkid);
			$data['jadwal'] = $m_general->get_jadwal_by_mhs($mhs_id, $mkid);
		endif;
		
		if(! empty($staff)) $data['jadwalmkd'] = $m_general->get_jadwal_dosen($staff, $mkid);
		else $data['jadwalmkd'] = $m_general->get_jadwal_mhs($mhs_id, $mkid);
		
		$data['jml_notifikasi'] = $m_general->get_jml_notifikasi($data['user_id']);
		$data['notifikasi'] = $m_general->get_notifikasi_new($data['user_id']);
		
		$data['post'] 	= $m_general->get_post($mkid, $memberid,"'note','assignment','quiz'", 1,$data['mkd'], $data['jadwalmkd']);
		$data['comment'] = $m_general->get_comment($mkid, $memberid);
		$data['kategori'] = $kategori;
		$data['memberid'] = $memberid;
		$data['groups']   = $m_general->get_mkd_mhs($memberid);
			
		//$data['library'] = $m_general->get_folder($mkid,$this->coms->authenticatedUser->id,'');
		$data['library'] = $m_general->get_folder('',$this->coms->authenticatedUser->id,'');
				
		if($memberid) $data['member'] 	= $m_general->get_mkd_mhs($memberid, $mkid);
		
		//tugas
				
		$this->add_style('select2/select2.css');
		$this->add_style('player/skin/all-skins.css');
		$this->add_style('player/skin/minimalist.css');
		$this->add_script('select2/select2.js');
		$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/jquery.dataTables.js');	
		$this->add_script('datatables/DT_bootstrap.js');
		$this->add_script('js/assignment/assignment.js');
		if($kategori!='folder')	$this->add_script('js/general/general.js');
		if($kategori=='topic') $this->add_script('bootstrap/js/jasny-bootstrap.min.js');
		$this->add_script('player/flowplayer.js');
		$this->add_script('js/folder/folder.js');
		
				
	
		
		if($memberid): 
			switch($kategori){
				case "member":
					$this->view('course/detail_member.php',$data);
				break;
				case "progress";									
					$this->view('course/progress_member.php',$data);
				break;
			}
			
		else: $this->view('course/index.php',$data);
		endif;
	}
	
	function get_member(){
				
		$m_general	= new model_general();
		
		$jadwalid	= $_POST['jadwalid'];
		
	
		$result = $m_general->get_jadwal_mhs('','',$jadwalid);
				
		if($result){
			foreach($result as $dt){ 
				?>
				<li class="list-group-item">
					<div class="row">							
						<div class="col-md-8">
							<div class="col-sm-2">
								<img src="<?php if($dt->foto) echo $this->config->file_url_view.'/'.$dt->foto; else  echo $this->config->default_pic ?>" class="img-avatar" />
							</div>
							<div class="col-sm-10">
								<a href="<?php echo $this->location('module/elearning/course/member/'. $dt->hidId.'/'.$dt->id) ?>"> <?php echo $dt->nama; ?></a>
								<small>Student
								<br><?php echo $dt->mahasiswa_id; ?>
								</small>
							</div>
						</div>
						<div class="col-md-2"><a href="<?php echo $this->location('module/elearning/course/member/'. $dt->hidId.'/'.$dt->id) ?>">Progress</a></div>
						<div class="col-md-2"><a href="#">More</a></div>
					</div>
				</li>
				<?php
			}
		}
	}
	
	function convert_date($date){
		$date = explode('-', $date);
		return substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}
	
	function add_post($mkid=NULL, $kategori=NULL, $memberid=NULL){
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;		
		$data['mkid'] = $mkid;
		
		$m_general	  = new model_general();
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] 	= $key->foto;
		
		$data['post'] 	= $m_general->get_post($mkid, $memberid,"'note','assignment','quiz'", $_POST['page_']);
		
		if(empty($data['post'])) echo "habis";
		else{
			$this->add_style('player/skin/all-skins.css');
			$this->add_style('player/skin/minimalist.css');
			$this->add_script('js/assignment/assignment.js');
			$this->add_script('js/general/general.js');
			$this->add_script('player/flowplayer.js');
		
			$this->view('home/view-content.php',$data);
		}
	}
	
	function groups($mkid=NULL){
		$this->index($mkid);
	}
	
	function folder($mkid=NULL){
		$this->index($mkid,'folder');
	}
	
	function topic($mkid=NULL, $materi=NULL, $file=NULL){
		if(isset($_POST['jadwal_id'])){
			$jadwalid 	= $_POST['jadwal_id'];
		}else{			
			$jadwalid	= "";
		}
		
		if(isset($_POST['action'])){
			$action = $_POST['action'];
		}else{			
			$action	= "";
		}
		
		if(isset($_POST['file_id'])){
			$file_id = $_POST['file_id'];
		}else{			
			$file_id	= $file;
		}
		
		$this->index($mkid,'topic',"", $jadwalid, $materi, $file_id, $action);
	}
	
	function syllabus($mkid=NULL){
		$this->index($mkid,'syllabus');
	}
	
	function member($mkid=NULL, $memberid=NULL){
		if($memberid) $this->index($mkid,'member',$memberid); //$this->detail_member($mkid, $memberid); //$this->index($mkid,'member',$memberid);
		
		else $this->index($mkid,'member');
	}
	
	function detail_member($mkid=NULL, $memberid=NULL){
	
	}
	
	function progress($mkid=NULL, $memberid=NULL){
		if($memberid) $this->index($mkid,'progress',$memberid); 		
		else $this->index($mkid,'progress');
	}
	
	function chart($mkid=NULL, $memberid=NULL){
		$mconf = new model_general();
		$post  = $mconf->get_post_assign($mkid);
		
		$return_arr = array();
		foreach($post as $row){
			$isi = explode("|",$row->isi); 
			$return_arr[] = $isi[0];		
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function del_file_topic($str=NULL){
		$mmateri = new model_course();
		
		if (!$str){
			$id = $_POST['delete_id'];
		}
		else $id=$str;
		
		$mmateri->delete_file($id);
		echo "Delete success";
	}
	
	function del_topic($mat=NULL){
		$mmateri = new model_course();
		if (!$mat){
			$id = $_POST['delete_id'];
		}
		else $id=$mat;
		// echo $id;
		// exit();
		$getid = $mmateri->get_id_from_parent($id);
		$delpar = $mmateri->delete($id);
		if(isset($getid)){
			foreach ($getid as $c) {
				$matid=$c->materi_id;
				$this->delete($matid);
			}
		}
		echo "Delete success";
	}
	
	function detailmateri($mk, $id, $p, $i=NULL, $jadwal_id=NULL, $mat_id=NULL, $data){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mmateri = new model_course();
		$materi = $mmateri->get_materi($mk, $id);
		$par = $p; //ambil urutan looping parent
		
		if($materi){
			$i+=1;//jumlahnya nambah tiap manggil detail materi atau bila punya child
			//$tab = str_repeat("&nbsp;&nbsp;&nbsp;", $i);//tab untuk child
			$tab = str_repeat("", $i);//tab untuk child
			$a=1;
			foreach ($materi as $dt):
				$n=$par.$a;//urutan parent+self
				// $i++;
				// echo $i;
				?>
					<tr id="del_materi<?php echo $dt->materi_id ?>" class="nav-syllabus" data-syllabus="<?php echo $i;?>">							
												
						<?php $uri_location = $this->location('module/elearning/course/del_topic'); ?>
						<td style='min-width: 80px;'>        
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
									<a class='dropdown-toggle link-syllabus' data-syllabuschild="<?php echo $i; ?>" id='drop4' role='button' data-toggle='dropdown' href='#'><?php echo $tab.$par.".".$a.". ".$dt->judul;?>&nbsp;&nbsp;</a>
										<ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
											<li>
												<!-- <a style='cursor:pointer' onclick="get_materi('<?php echo $dt->materi_id ?>')"><i class='fa fa-search'></i> View</a> -->
												<!-- <a style='cursor:pointer' onclick="view_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-search'></i> View</a>	 -->
											</li>
											
											<?php
												if($role!='mahasiswa'){
												?>
												 <!-- <li class="divider"></li> -->
												<li>
													<a style='cursor:pointer' onclick="add_sub_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-plus'></i> Add Sub Topic</a>	
												</li>
												<li>
													<a style='cursor:pointer' onclick="add_file_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-file-o'></i> Add File</a>
												</li>
												 <li class="divider"></li>
													<li><a style='cursor:pointer' onclick="edit_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-pencil'></i> Edit</a></li>
												<li><a class='btn-edit-post' href="#" onclick="do_del_topic(<?php echo $dt->materi_id ?>)"><i class='fa fa-trash-o'></i> Delete</a>		
												<input type='hidden' class='deleted<?php echo $dt->materi_id ?>' value="<?php echo $dt->materi_id ?>" uri = "<?php echo $uri_location ?>"/>				
												</li>
												<?php
												}
												?>	
										</ul>
								</li>
							</ul>
							<?php if($mat_id!=""){ ?>
								<?php 
									echo $dt->keterangan;
									$data['materi_id'] = $dt->materiid;
									$data['view_silabus'] = 1;
									$this->view('course/topic.php', $data);
								?>
							<?php } ?>
						</td>
					</tr>
				<?php
				$a++;
				$cont = $par.".".($a-1);
				echo $this->detailmateri($dt->mkditawarkan_id, $dt->materi_id,$cont, $i, $jadwal_id, $mat_id, $data);
			endforeach;
		}
	}
	
	
	function save_topic_file(){
		$user	= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$mpost 	= new model_course();
		
		$file_id 			= $_POST['hidfileId'];
		
		$materi_id			= $_POST['materi_id'];
		$mkditawarkan_id	= $_POST['mkditawarkan_id'];
		
	
		$result = $this->upload_file($file_id,'',''); //urutan hasilnya array($file_name,$file_type,$file_loc,$file_size,$file_content,$jenis_file_id)
		
		if($result == FALSE){
			echo "Upload file gagal!";
		}
			elseif($result == "not_upload"){
				$file_name			= $_POST['file_name'];
				$file_type			= $_POST['file_type'];
				$file_loc			= $_POST['file_loc'];
				$file_size			= $_POST['file_size'];
				$file_content		= NULL;
				
				$tgl_upload			= $_POST['tgl_upload'];
				$upload_by			= $_POST['upload_by'];
				$jenis_file_id		= $_POST['jenis_file_id'];
			}
			else{
				for($j=0;$j<count($result);$j++){
					switch($j){
						case 0: $file_name = $result [$j];
							    break;
						case 1: $file_type = $result [$j];
							    break;
						case 2: $file_loc = $result [$j];
							    break;
						case 3: $file_size = $result [$j];
							    break;
						case 4: $file_content = $result [$j];
							    break;
						case 5: $jenis_file_id = $result [$j];
							    break;
					}
				}
				$tgl_upload = date("Y-m-d H:i:s");
				$upload_by 	= $this->coms->authenticatedUser->username;
			}
			
			if(isset($_POST['b_savepublish'])){
				$ispublish = 1;
			}
			elseif(isset($_POST['b_draft'])){
				$ispublish = 0;
			}
			
			$judul				= $_POST['judul'];
			$keterangan			= $_POST['keterangan'];
			$user				= $this->coms->authenticatedUser->id;
			$lastupdate			= date("Y-m-d H:i:s");
			
				if(($judul)){
				$datanya 	= Array(		'file_id'=>$file_id,
											'jenis_file_id'=>$jenis_file_id,
											'materi_id'=>$materi_id,
											'mkditawarkan_id'=>$mkditawarkan_id,
										    'judul'=>$judul,
										    'keterangan'=>$keterangan,
										    'file_name'=>$file_name,
										    'file_type'=>$file_type,
										    'file_loc'=>$file_loc,
										    'file_size'=>$file_size,
										    'file_content'=>$file_content,
										    'tgl_upload'=>$tgl_upload,
										    'upload_by'=>$upload_by,
										    'is_publish'=>$ispublish,
										    'user_id'=>$user,
										    'last_update'=>$lastupdate
											);
				$mpost->replace_file($datanya);
				echo "Upload success";
				exit();
			}
	}
	
	
	function save_upload_file(){
		$m_general	= new model_general();
		$id_user = $this->coms->authenticatedUser->id;
		$uploadby = $this->coms->authenticatedUser->username;
		$tgl_upload = date("Y-m-d H:i:s");
		$month = date('m');
		$year = date('Y');
		
		if(isset($_POST['b_savepublish'])){
			$ispublish = 1;
		}
		elseif(isset($_POST['b_draft'])){
			$ispublish = 0;
		}
		
		
		$materi_id			= $_POST['materi_id'];
		$mkditawarkan_id	= $_POST['mkditawarkan_id'];
		$isi				= $_POST['keterangan'];
		$judul				= $_POST['judul'];
		
		if(isset($_POST['library'])){
			$library			= $_POST['library'];
			
			foreach($library as $key){
				$data_library			= $m_general->get_attach_media_library($key);
				$library_name 			= $data_library->file_name;
				$library_file_type 		= $data_library->file_type;
				$library_file_loc 		= $key;
				$library_file_size 		= $data_library->file_size;
				$library_file_ext 		= $this->get_file_ext($library_name);
				$library_jenis_file_id 	= $m_general->cek_file_type($library_file_ext)->jenis_file_id;
				
				$data = Array(
							'file_id'=> $m_general->get_file_id(),
							'jenis_file_id' => $library_jenis_file_id,
						    'file_name'=>$library_name,
						    'file_type'=>$library_file_type,
						    'file_loc'=>$library_file_loc,
						    'file_size'=>$library_file_size,
						    'materi_id'=>$materi_id,
							'mkditawarkan_id'=>$mkditawarkan_id,
							 'judul'=>$judul,
						    'keterangan' => $isi,
						    'tgl_upload'=>$tgl_upload,
						    'upload_by'=>$uploadby,
							'is_publish'=>$ispublish,
						    'user_id'=>$id_user,
						    'last_update'=>date('Y-m-d H:i:s')
						);
				$m_general->save_file_log($data);
			}
		}
		
		if(isset($_FILES["file"]['name'])){
			
			foreach($_FILES["file"]['name'] as $id => $name){
				if(! empty($name)){
				
					$maxfilesize = 0;
					$file_ext = $this->get_file_ext($name);
					$init_file = $m_general->cek_file_type($file_ext);
					if(isset($init_file->max_size)){
						$maxfilesize = $init_file->max_size;
						$jenis_file_id = $init_file->jenis_file_id;
					}
					
					$file = "";
					$file_type=$_FILES["file"]["type"][$id] ; 
					$file_size=$_FILES["file"]["size"][$id] ; 
					$upload_dir = 'assets/upload/file/'. $id_user . DIRECTORY_SEPARATOR . 'attach'. DIRECTORY_SEPARATOR .$year. '-' .$month . DIRECTORY_SEPARATOR ;		
					$upload_dir_db = 'upload/file/'. $id_user . DIRECTORY_SEPARATOR . 'attach'. DIRECTORY_SEPARATOR .$year. '-' .$month . DIRECTORY_SEPARATOR ;
		
					if (!file_exists($upload_dir)) {
						mkdir($upload_dir, 0777, true);
						chmod($upload_dir, 0777);
					}
					$file_loc = $upload_dir_db . $name;
					
					if ($file_size <= $maxfilesize AND isset($init_file->max_size)){
						//if(file_exists($upload_dir . $name)) unlink($upload_dir . $name);			
						if($_SERVER['REQUEST_METHOD'] == 'POST') {
							//------UPLOAD USING CURL----------------
							//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
							$url	     = $this->config->file_url;
							$filedata    = $_FILES['file']['tmp_name'][$id];
							$filename    = $name;
							$filenamenew = $name;
							$filesize    = $file_size;
						
							$headers = array("Content-Type:multipart/form-data");
							$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
							$ch = curl_init();
							$options = array(
								CURLOPT_URL => $url,
								CURLOPT_HEADER => true,
								CURLOPT_POST => 1,
								CURLOPT_HTTPHEADER => $headers,
								CURLOPT_POSTFIELDS => $postfields,
								CURLOPT_INFILESIZE => $filesize,
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
								CURLOPT_SSL_VERIFYPEER => false,
								CURLOPT_SSL_VERIFYHOST => 2
							); // cURL options 
							curl_setopt_array($ch, $options);
							curl_exec($ch);
							if(!curl_errno($ch))
							{
								$info = curl_getinfo($ch);
								if ($info['http_code'] == 200)
								   $errmsg = "File uploaded successfully";
							}
							else
							{
								$errmsg = curl_error($ch);
							}
							curl_close($ch);
							//------UPLOAD USING CURL----------------
							//rename($_FILES['file']['tmp_name'][$id], $upload_dir . $name);
						}
		
						$data = Array(
							'file_id'=> $m_general->get_file_id(),
							'jenis_file_id' => $jenis_file_id,
						    'file_name'=>$name,
						    'file_type'=>$file_type,
						    'file_loc'=>$file_loc,
						    'file_size'=>$file_size,
							 'judul'=>$judul,
						    'materi_id'=>$materi_id,
							'mkditawarkan_id'=>$mkditawarkan_id,
						    'keterangan' => $isi,
						    'tgl_upload'=>$tgl_upload,
						    'upload_by'=>$uploadby,
							'is_publish'=>$ispublish,
						    'user_id'=>$id_user,
						    'last_update'=>date('Y-m-d H:i:s')
						);
						$m_general->save_file_log($data);
						
						
					} //end id isempty
				}
			} //end of foreach
			return ($id+1);
		}
		else{
			return 0;	
		}
	
	} //end of function 
	
	function save_topic(){
				
		$user	= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$mpost 	= new model_course();

		if(isset($_POST['id'])&& $_POST['id']) $materiid = $_POST['id'];
		else $materiid = $mpost->get_id();
		
		if(isset($_POST['hidid']))	$mk = $_POST['hidid'];
		else $mk = "";
		
		if(isset($_POST['iscampus'])) $campusonly	= $_POST['iscampus'];
		else $campusonly	= 0;
		
		if(isset($_POST['cmbmateri'])) $parent	= $_POST['cmbmateri'];
		else $parent	= 0;
		
		if(isset($_POST['b_savepublish'])){
			$ispublish	= 1;
		}
		elseif(isset($_POST['b_draft'])){
			$ispublish	= 0;
		}
		$ket = $_POST['keterangan_topik'];
		$url  = trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($_POST['judul']))))));
		
		$result = $this->upload_file($materiid,'icon'); //urutan hasilnya array($file_name,$file_type,$file_loc,$file_size,$file_content,$jenis_file_id)
		if($result == FALSE){
			echo "Upload failed!";
		}
		elseif($result == "not_upload"){
			if(isset($_POST['uploads'])){
				$icon_loc	= $_POST['uploads'];
			}
			else $icon_loc = NULL;
		}
		else{
			for($j=0;$j<count($result);$j++){
				switch($j){
					case 0: $icon_name = $result [$j];
						    break;
					case 1: $icon_type = $result [$j];
						    break;
					case 2: $icon_loc = $result [$j];
						    break;
					case 3: $icon_size = $result [$j];
						    break;
					case 4: $icon_content = $result [$j];
						    break;
					case 5: //$jenis_icon_id = $result [$j];
						    break;
				}
			}
		}

		$save = $mpost->save($mk, $_POST['judul'], $ket, $ispublish, $_POST['urut'], $parent,$_POST['cmbstatus'], $campusonly,$icon_loc, $url, $materiid, $user, $lastupdate);
	
		if($save!=FALSE){
			echo "Upload topic success";
		}
	}
	
	//========FUNGSI UPLOAD SINGLE FILE START========//
	//call upload_file(file_id,icon/not)
	function upload_file($file_id = NULL,$icon = NULL){
		ob_start();
		$mfile = new model_course();
		
		foreach ($_FILES['uploads'] as $id => $file) {
			if($id == 'error'){
				if($file!=0){
					$cekfile = 'error';
				}
				else $cekfile = 'success';
			}
		}
		
		if($cekfile!='error'){
				$month = date('m');
				$year = date('Y');
				$name = $_FILES['uploads']['name'];
				$ext	= $this->get_file_ext($name);
				$seleksi_ext = $mfile->get_ext($ext);
				if($seleksi_ext!=NULL){
						$jenis_file_id 	= $seleksi_ext->jenis_file_id;
						$jenis			= $seleksi_ext->keterangan;
						$maxfilesize	= $seleksi_ext->max_size;
						
						switch(strToLower($jenis)){
							case 'image':
								$extpath = "image";
							break;
							case 'presentation':
								$extpath = "presentation";
							break;
							case 'document':
								$extpath = "document";
							break;
							case 'video':
								$extpath = "video";
							break;
							case 'spreadsheet':
								$extpath = "spreadsheet";
							break;
						}
				}
				else{
						return FALSE;
				}
// 				
				
				$file_type=$_FILES["uploads"]["type"]; 
				$file_size=$_FILES["uploads"]["size"]; 
				if(isset($icon)&& $icon!=''){
					$allowed_ext = array('jpg','jpeg','png','gif');
					$ceknamafile = $mfile->cek_nama_icon($name);
					$upload_dir = 'assets/upload/icon/materi/';
					$upload_dir_db = 'upload/icon/materi/';
				}
				else{
					$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx', 'webm');
					$ceknamafile = $mfile->cek_nama_file($name);
					$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
					$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
				}
				
				
				// if (!file_exists($upload_dir)) {
							// mkdir($upload_dir, 0777, true);
							// chmod($upload_dir, 0777);
				// }
				$file_name = $file_id.".".$ext;
				$file_loc = $upload_dir_db . $file_name;
				if(!in_array($ext,$allowed_ext)){
						return FALSE;
				}
				elseif (($ceknamafile==NULL) && ($file_size <= $maxfilesize)){
				
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
						//------UPLOAD USING CURL----------------
						//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
						$url	     = $this->config->file_url;
						$filedata    = $_FILES['uploads']['tmp_name'];
						$filename    = $file_name;
						$filenamenew = $file_name;
						$filesize    = $file_size;
					
						$headers = array("Content-Type:multipart/form-data");
						$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
						$ch = curl_init();
						$options = array(
							CURLOPT_URL => $url,
							CURLOPT_HEADER => true,
							CURLOPT_POST => 1,
							CURLOPT_HTTPHEADER => $headers,
							CURLOPT_POSTFIELDS => $postfields,
							CURLOPT_INFILESIZE => $filesize,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => 2
						); // cURL options 
						curl_setopt_array($ch, $options);
						curl_exec($ch);
						if(!curl_errno($ch))
						{
							$info = curl_getinfo($ch);
							if ($info['http_code'] == 200)
							   $errmsg = "File uploaded successfully";
						}
						else
						{
							$errmsg = curl_error($ch);
						}
						curl_close($ch);
						//------UPLOAD USING CURL----------------
					
						//rename($_FILES['uploads']['tmp_name'], $upload_dir . $file_name);
					}
				}
				$file_content		= NULL;
				$hasil = array($file_name,$file_type,$file_loc,$file_size,$file_content,$jenis_file_id); 
				return $hasil;
		}
		else{
			return "not_upload";
		}
	}
	//========FUNGSI UPLOAD FILE END========//
	
	function get_file_ext($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_title($file_name){
		$name = explode('.', $file_name);
		array_pop($name);
		return implode('.', $name);
	}
	
	
	function download($id=NULL){
				
		$mfile = new model_file();
		
		$path = $this->config->file_url_view.'/'.$mfile->get_locfile($id);	
			
		if (file_exists($path)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename=' . urlencode(basename($path)));
			// header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path));
			ob_clean();
			flush();
			readfile($path);
			exit;
		}else{
			die('File Not Found');
		}
	}
	
	function get_frm_silabus(){
		$mpost = new model_course();
		
		$mkid		= $_POST['mkid'];
		$komponen	= $_POST['cmbkomponen'];
		
		$silabus = $mpost->get_silabus($mkid, $komponen);
		$result = array();
		$result['keterangan'] = $silabus->keterangan;
		$result['id']			= $silabus->silabus_id;
		$result['detail']		= $silabus->detail_id;
		echo json_encode($result);
		
	}
	
	function save_silabus(){
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$mpost 		= new model_course();
		
		$mkid		= $_POST['hidid'];
		$komponen	= $_POST['cmbkomponen'];
		$keterangan	= $_POST['silabus'];
		
		$silabusid		= $mpost->get_silabus_reg_id($mkid);
		$data_silabus 	= Array('silabus_id'=>$silabusid, 'mkditawarkan_id'=>$mkid);
		$mpost->replace_silabus($data_silabus);
		
		$detailid		= $mpost->get_silabus_detail($silabusid, $komponen);
		
		// echo $mkid."\n".$komponen."\n".$keterangan;
		 if($keterangan){
				
			$data_detail 	= Array('detail_id'=>$detailid, 'silabus_id'=>$silabusid,
								 'komponen_id'=>$komponen, 'keterangan'=>$keterangan, 'user_id'=>$user, 'last_update'=>$lastupdate);
			$mpost->replace_detail($data_detail);
			
			echo "Update sucess";
			exit();
		 }
		
	}
	
	
}
?>