<?php
class elearning_progress extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
		
	function views($mkid=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$m_general	  = new model_general();
		$m_progress	  = new model_progress();
			
		$key 			= $m_general->get_user_profile($data['user_id']);
		$data['foto']   = $key->foto;
		$data['mkd_name'] = $m_progress->get_mkd_name($mkid, $role);
		$data['khs'] = $m_progress->get_progress_khs($mkid);
		
		if($role == 'dosen') $data['list_jadwal'] = $m_progress->get_jadwal($staff, $mkid);
		elseif($role == 'mahasiswa') $data['list_jadwal'] = $m_progress->get_jadwal_by_mhs($mhs_id, $mkid);
		
		if(!isset($_POST['jadwal'])) {
			if(isset($data['list_jadwal'][0]->jadwal_id)) $jadwal = $data['list_jadwal'][0]->jadwal_id;
			else $jadwal = '';
		}
		else{
			$jadwal = $_POST['jadwal'];
		}
		
		$data['tugas'] = $m_progress->get_judul_progress($mkid, $jadwal);
		$data['mhs_progress'] = $m_progress->get_progress($mkid, $jadwal);
		$data['mkid'] 	= $mkid;
		$data['jadwal'] = $jadwal;
		
		$this->add_style('bootstrap/css/add.css');
		$this->add_style('select2/select2.css');
		$this->add_style('player/skin/all-skins.css');
		$this->add_style('player/skin/minimalist.css');
		$this->add_script('select2/select2.js');
		$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/jquery.dataTables.js');	
		$this->add_script('datatables/DT_bootstrap.js');	
		
		$this->add_script('js/progress/progress.js');	
		
		$this->view('progress/index.php',$data);
	}
	
	function add(){
		$m_progress	  = new model_progress();
		if($_POST['judul']=='Nilai KHS'){
			$mulai = "0000-00-00 00:00";
			$kategori = "khs";
		}else{
			$mulai = date('Y-m-d H:i:s');
			$kategori = "addition";
		}
		
		$data = array(
			'post_nilai_id' => $m_progress->get_progress_id(),
			'mkditawarkan_id' => $_POST['mkd'],
			'judul' => $_POST['judul'],
			'jadwal_id' => $_POST['jadwal'],
			'tgl_mulai' => $mulai,
			'kategori_nilai' => $kategori,
			'user_id' => $this->coms->authenticatedUser->id,
			'last_update' => date('Y-m-d H:i:s'),
			'is_proses' => 0
		);
		
		$m_progress->save_grade($data);
		
		$this->redirect('module/elearning/progress/views/'.substr(md5($_POST['mkd']),8,7));
	}
	
	
	function new_mark(){
		$m_progress	  = new model_progress();
		$m_progress->new_mark($this->coms->authenticatedUser->id);
	}
	
	function del(){
		$m_progress	  = new model_progress();
		$m_progress->del_komponen($_POST['komponen']);
	}
	
	
	
	
	
	
	
	
	
	
	
		
	
}
?>