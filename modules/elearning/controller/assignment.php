<?php
class elearning_assignment extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){}
	
	function detail($tugas_id=NULL, $jadwal=NULL, $mhs=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$m_assignment = new model_assignment();
		$m_general = new model_general();
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] 	= $key->foto;
		
		$data['tugas'] 	= $m_assignment->read_tugas($tugas_id);
		$data['library'] = $m_general->get_folder('',$this->coms->authenticatedUser->id,'');
		$data['comment_list'] = $m_general->get_comment();
		
		$m_general		  = new model_general();
		$data['jml_notifikasi'] = $m_general->get_jml_notifikasi($data['user_id']);
		$data['notifikasi'] = $m_general->get_notifikasi_new($data['user_id']);
		
		if($role == "dosen"){
			$data['list_mhs'] = $m_general->mahasiswa_list($jadwal);
			$data['tugas_id'] = $tugas_id;
			$data['jadwal_id'] = $jadwal;
			$data['mhs_id'] = $mhs;
			if($mhs){
				$data['data_mhs'] = $m_general->mahasiswa_list($jadwal,$mhs);
				$data['posts_mhs'] = $m_assignment->read_tugas_mhs($tugas_id, "", $mhs);
				$data['comment'] = $m_assignment->read_tugas_mhs($tugas_id,"",$mhs,"comment");
			}
		}
		elseif($role == "mahasiswa"){
			$mhsid = $m_assignment->get_md5_mhs_id($mhs_id);
			$data['tugas_id'] = $tugas_id;
			$data['jadwal_id'] = $jadwal;
			$data['mhs_id'] = $mhs_id;
			$data['tgs_mhs'] = $m_assignment->read_tugas_mhs($tugas_id,"",$mhsid);
			$data['mkid'] = $m_assignment->get_mk_by_jadwal($jadwal);
			$data['data_mhs'] = $m_general->mahasiswa_list($jadwal,$mhsid);
			$data['comment'] = $m_assignment->read_tugas_mhs($tugas_id,"",$mhsid,"comment");
		}
		
		$this->add_style('select2/select2.css');
		$this->add_script('select2/select2.js');
		$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_script('js/assignment/assignment.js');
		$this->add_script('js/general/general.js');
		
		$this->view('assignment/index.php', $data);
	}

	function convert_date($date){
		$date = explode('-', $date);
		return substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}

	function load_assignment(){
		$id = $_POST['id'];
		if(isset($id)){
			$m_assignment = new model_assignment();
			$user = $this->coms->authenticatedUser->id;
			$data = $m_assignment->get_tugas_submitted_all($user,$id);
			echo json_encode($data);
		}
	}
	
	function get_all_assignment_record(){
		$m_assignment = new model_assignment();
		$tugas = $m_assignment->get_tugas_submitted_all($this->coms->authenticatedUser->id);
		if($tugas){
			$return_arr = array();
		
			foreach($tugas as $row){
				foreach ($row as $key => $value) {
					 $arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			echo json_encode($return_arr);
		}else{
			echo json_encode("");
		}
	}
	
	function formatSizeUnits($bytes){
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}

	function save_answer(){
		if($_POST['catatan']){
			$this->save_answerToDB();
			exit();
		}else{
			echo "Sorry, save failed!";
			exit();
		}
	}

	function save_answerToDB(){
		ob_start();
		$mfile		  	= new model_file();
		$m_assignment	= new model_assignment();
		$m_general		= new model_general();
		
		$lastupdate		= date("Y-m-d H:i:s");
		$user			= $this->coms->authenticatedUser->id;
		
		//$uploadid		= $m_assignment->upload_id(); //diganti select
		
		$catatan 		= $_POST['catatan'];
		$mhs_id			= $_POST['mhsid'];
		$tugasid_md5	= $_POST['tgsid'];
		$tugasid		= $m_assignment->get_tugas_id_by_md5($_POST['tgsid']);
		$post_id 		= $m_assignment->get_post_id();
		$uploadid		= $m_assignment->get_uploadid_by_mhs_tgs($mhs_id,$tugasid);
		
		if(isset($catatan)){
			$uploadby 	= $this->coms->authenticatedUser->username;
			
			$datanya 	= Array('upload_id'=>$uploadid,
								'mahasiswa_id'=>$mhs_id,
								'tugas_id'=>$tugasid,
								'tgl_upload'=>$lastupdate,
								'catatan'=>$catatan,
							    'user_id'=>$user,
							    'last_update'=>$lastupdate
								  );
			$m_assignment->replace_tugas_mhs($datanya);
			
			/* save skor to post nilai */
				$nilai_id = 'TG'.$uploadid;
				$post_nilai_id = 'TG'.$tugasid;
				
				$data_nilai = array(
					'nilai_id' => $nilai_id,
					'mahasiswa_id' => $mhs_id,
					'post_nilai_id' => $post_nilai_id,
					'link_id' =>$uploadid,
					'tbl_name'=>'tbl_tugas_mhs',
					'tgl_post' => date('Y-m-d H:i:s'),			
					'skor' => '0',
					'status'=>'Turn In',
					'user_id' => $user,
					'last_update' => date('Y-m-d H:i:s')
				);
				
				$m_general->save_post_nilai_mhs($data_nilai); 
			/* end */
			
			//$this->log_upload("upload_tugas", $fileid);
			
			echo $tugasid."/".$post_id."/".$uploadid;
		}
		else{
			echo "Failed, Please try again!";
			exit();
		}
	}

	function log_upload($action, $fileid){
		$mfile = new model_file();
		$user = $this->coms->authenticatedUser->id;
		$result = $mfile->log_data($user, $action, $fileid);
	}

	function skor(){
		$m_assignment	= new model_assignment();
		$skor = $_POST['skor'];
		$total = $_POST['total_skor'];
		$upload_id = $_POST['upload_id'];
		$skor = $m_assignment->update_skor($skor,$total,$upload_id);
		// echo $skor;
		if($skor==TRUE){
			echo "Grading Success!";
		}
		else echo "Grading Failed!";
	}
	
	
}
?>