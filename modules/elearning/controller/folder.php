<?php
class elearning_folder extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth');
	}
	
	function index(){
		$this->redirect('module/elearning/home');
		exit();
		
		$mfolder = new model_folder();
		
		$data['posts'] = $mfolder->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->add_script('js/folder.js');
		
		$this->view( 'folder/index.php', $data );
	}
	
		
	function groups($mkid=NULL, $kategori=NULL, $memberid=NULL){
		$mfolder = new model_folder();
		
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;		
		
		$m_general	  = new model_general();
		$m_assignment = new model_assignment();
		$mquiz		  = new model_quiz();
		
		$key 		  = $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $key->foto;
		
		
		if(! empty($staff)) $data['mkd'] = $m_general->get_mkd_dosen($staff, $mkid);
		else $data['mkd'] = $m_general->get_mkd_mhs($mhs_id, $mkid);
		$data['post'] 	= $m_general->get_post($mkid, $memberid);
		$data['comment'] = $m_general->get_comment($mkid, $memberid);
		$data['kategori'] = $kategori;
		$data['memberid'] = $memberid;
		
		if($memberid) $data['member'] 	= $m_general->get_mkd_mhs($memberid, $mkid);
		
		//tugas
		$data['jadwal'] = $m_assignment->get_jadwal($staff);
					
		$this->add_style('select2/select2.css');
		$this->add_script('select2/select2.js');
		$this->add_style('datepicker/css/datetimepicker.css');
		
		
		$data['posts'] = $mfolder->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->add_script('js/folder/folder.js');		
		
		$this->view( 'folder/index.php', $data );
	}
	
	function detailchild($id=NULL, $str=NULL){
		$mfolder = new model_folder();
		
		$child = $mfolder->read($id);
		
		$level = $this->coms->authenticatedUser->level; 
		
		echo "<ul>";
			if(isset($child)){ 
				$n = 1;
				$folder = $str;
				foreach ($child as $dt) {
					$folder = $str."/".$dt->folder_name;
						if(($level=="1")||($level=="4")){
				?>
						<li>							
							<span class='span text text-default' onclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span>
							<a title="rename" class='btn-edit-post btn-add' href="#" data-toggle="modal" data-target="#newfolder" onclick="rename('<?php echo $dt->id ?>','<?php echo $dt->folder_name ?>')"><span class='text'><i class="fa fa-gear"></i></span></a>
							<a title="create new folder" class='btn-edit-post btn-add' href="#" data-toggle="modal" data-target="#newfolder" onclick="newfolder_parent('<?php echo $dt->id ?>')"><span class='text'><i class="fa fa-plus"></i></span></a>
							<a title="upload file" class='btn-edit-post btn-add' href="#" data-toggle="modal" data-target="#newfile" onclick="newfile_parent('<?php echo $dt->id ?>','<?php echo $folder; ?>')"><span class='text'><i class="fa fa-cloud-upload"></i></span></a>
							<a title="delete folder" class='btn-edit-post btn-add' href="#" onclick="doDelete('<?php echo $dt->folder_id ?>','folder')"><span class='text'><i class="fa fa-trash-o"></i></span></a>
							<?php echo $this->detailchild( $dt->id, $folder) ?>
						</li>
					<?php 
					}else{
						?>
						<li><span class='span text text-default' onclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span>
						<?php echo $this->detailchild( $dt->id, $folder) ?></li>
						<?php
					}
				} 
			}
		echo "</ul>";
	}
	
	function save(){
		$mfolder = new model_folder();
		$id = isset($_POST['id']);		
		
		$parent = $_POST['parent'];
		$folder_name = $_POST['folder_name'];
		$mkid	= $_POST['mk'];
		$folder_id = $mfolder->folder_id();
		
		$datanya 	= Array(
							'id' => $folder_id,
							'folder_name' => $folder_name,
							'parent_id' => $parent,
							'is_available'=>1,
							'mkditawarkan_id'=>$mkid,
							'user_id'=>$this->coms->authenticatedUser->id,
							'last_update'=>date("Y-m-d H:i")
						   );
		$mfolder->replace_folder($datanya);
		
		if($mfolder->replace_folder($datanya)){
			echo "Create folder process success!";
		}else{
			echo "Create folder failed!";
		}
		exit();
		
	}
	
	function save_file(){
		$mfolder = new model_folder();
		
		$user	= $this->coms->authenticatedUser->id;
		
		$folder_id  = $_POST['folder'];
		$folder	= $_POST['folder_loc'];
	
		
		//$title		= $_POST['title'];
		$uploads	= $_FILES['uploads'];
		
		$i=0;
		foreach ($_FILES['uploads']['name'] as $id => $file){
			$lastupdate	= date("Y-m-d H:i:s");
			$month = date('m');
			$year  = date('Y');
			$name  = $_FILES['uploads']['name'][$id];
			$ext   = $this->get_extension($name);
			switch(strToLower($ext)){
				case 'jpg':
				case 'jpeg':
				case 'png':
				case 'gif':
					$jenisfile = "image";
					break;
				case 'doc':
				case 'ppt':
				case 'pdf':
				case 'xls':
				case 'docx':
				case 'pptx':
				case 'xlsx':
					$jenisfile = "document";
					break;
				case 'webm':
				case 'mp4':
					$jenisfile = "video";
			}
			$file_type=$_FILES["uploads"]["type"][$id];
			$file_size=$_FILES["uploads"]["size"][$id];
			
			$upload_dir = 'assets/upload/file/'.$user.'/library/'.$folder. "/";
			$upload_dir_db = 'upload/file/'.$user.'/library/'.$folder. "/";
			
			$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx', 'webm', 'mp4');
			
			if(!in_array($ext,$allowed_ext)){
				echo "Sorry, your data type is not allowed!";
				exit();
			}
			else{
				// if (!file_exists($upload_dir)) {
					// mkdir($upload_dir, 0777, true);
					// chmod($upload_dir, 0777);
				// }
				$file_loc = $upload_dir_db . $name;
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					//rename($_FILES['uploads']['tmp_name'][$id], $upload_dir . $name);
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $_FILES['uploads']['tmp_name'][$id];
					$filename    = $name;
					$filenamenew = $name;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
					$ch = curl_init();
					$options = array(
						CURLOPT_URL => $url,
						CURLOPT_HEADER => true,
						CURLOPT_POST => 1,
						CURLOPT_HTTPHEADER => $headers,
						CURLOPT_POSTFIELDS => $postfields,
						CURLOPT_INFILESIZE => $filesize,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
						CURLOPT_SSL_VERIFYPEER => false,
						CURLOPT_SSL_VERIFYHOST => 2
					); // cURL options 
					curl_setopt_array($ch, $options);
					curl_exec($ch);
					if(!curl_errno($ch))
					{
						$info = curl_getinfo($ch);
						if ($info['http_code'] == 200)
						   $errmsg = "File uploaded successfully";
					}
					else
					{
						$errmsg = curl_error($ch);
					}
					curl_close($ch);
					//------UPLOAD USING CURL----------------
				}
				
				$file_id = $mfolder->file_id();
				
				$datanya 	= Array('file_id' => $file_id,
									'file_loc' => $file_loc,
									'file_name' => $name,
									'jenis_file' => $jenisfile,
									'file_type' => $file_type,
									'file_size' => $file_size,
									'folder_id' => $folder_id,
									'user_id' => $user,
									'last_update' => $lastupdate
									);
				$mfolder->replace_file($datanya);
				
				//echo $title[$i]."\n".$name."\n".$file_type."\n".$file_size."\n".$jenisfile."\n";
				$i++;
			}
		}
		echo "Upload file Success!";
		exit();
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_title($file_name){
		$name = explode('.', $file_name);
		array_pop($name);
		return implode('.', $name);
	}
	
	function views($id=NULL, $folder=NULL){
		$mfolder = new model_folder();
		$data['detail']	= $mfolder->read_file("",$id);
		$this->view( 'folder/view.php', $data );
	}
	
	function download($id=NULL){
		$mfile = new model_folder();
		
		$dt=$mfile->read_file("",$id);
		
		$path = "assets/".$dt->file_loc;	
			
		if (file_exists($path)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename=' . urlencode(basename($path)));
			// header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path));
			ob_clean();
			flush();
			readfile($path);
			exit;
		}else{
			die('File Not Found');
		}
		
		
	}
	
	function view_content(){
		$mfolder = new model_folder();
		
		$folder_id = $_POST['id'];
		
		$nama	= $_POST['name'];
		
		$child = $mfolder->read($folder_id);
		$childfile = $mfolder->read_file($folder_id);
		
		$level = $this->coms->authenticatedUser->level; 
		?>
		
		<div class="row"  style="margin-left: 5px;">
			<div class="col-md-6">
				<h3><?php echo $nama ?></h3>
			</div>
			<div class="col-md-6">			
				<a title="upload file" class='btn btn-primary btn-edit-post btn-add pull-right' data-toggle="modal" data-target="#newfile" href="#" onclick="newfile_parent('<?php echo $folder_id ?>','<?php echo $nama; ?>')"><i class="fa fa-plus"></i> Add File</a>
			</div>
		</div>		
		<table class="table table-hover example">
			<thead>
				<tr>
					<th><small>Name</small></th>
					<th><small>Size</small></th>
					<th><small>Last Modified</small></th>
					<?php if(($level=="1")||($level=="4")){ ?>
					<th>&nbsp;</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
		<?php
		if(isset($child)){?>
			
			<?php
			foreach ($child as $dt) {?>
				<tr class="folder<?php echo $dt->folder_id ?>">
					<td><span class='span' style="cursor: pointer" ondblclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span></td>
					<td></td>
					<td></td>					
					<td>
						<span onclick="doDelete('<?php echo $dt->folder_id ?>','folder')"><i class="fa fa-trash-o" style="color: red; cursor: pointer"></i></span>
					</td>					
				</tr>
			<?php 
			} 
		}
		if(isset($childfile)){?>
			<?php
			foreach ($childfile as $d) {?>
				<tr class="file<?php echo $d->id ?>">
					<td>
						<a  href="#" onClick="doView('<?php echo $d->file_id;?>','<?php echo $d->folder_id;?>')";>
						<span style="cursor: pointer">
							<?php if($d->jenis_file == 'document'){ ?>
								<i class="fa fa-file-text-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'image'){ ?>
								<i class="fa fa-picture-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'video'){ ?>
								<i class="fa fa-video-camera"></i>&nbsp;
							<?php } ?>
							<span class="text text-default"><?php echo $d->file_name ?></span></a>
						</span>
					</td>
					<td><small><?php echo $this->formatSizeUnits($d->file_size) ?></small></td>
					<td><small><i class="fa fa-clock-o"></i>&nbsp;<?php echo $d->last_update ?></small></td>					
					<td>					
						<span onclick="doDelete('<?php echo $d->id ?>','file')"><i class="fa fa-trash-o" style="color: red; cursor: pointer"></i></span>						
					</td>
				</tr>
			<?php 
			} 
		}
		?>				
			</tbody>
		</table>
		<script>
			$(document).ready( function() {
			  $('.example').dataTable( {
				"fnInitComplete": function(oSettings, json) {
				  //alert( 'DataTables has finished its initialisation.' );
				}
			  } );
			} )

		</script>
		<?php
	}
	
	function formatSizeUnits($bytes){
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}
	
	function delete($folder_id=NULL){ //folder
		$mfolder = new model_folder();
		
		$jenis_del = $_POST['jenis_del'];
		
		if($jenis_del == 'file'){
			$id = $_POST['delete_id'];
			$del_file = $mfolder->delete_file($id);
			if($del_file == TRUE){
				echo "Delete file success!";
			}
		}
		else{
			
			if (!$folder_id){
				$id = $_POST['delete_id'];
			}
			else $id = $folder_id;
			
			//echo $id."\n";
			
			$get_child_id = $mfolder->get_id_from_parent($id);
			$del_parent = $mfolder->delete_folder($id);
			$cek_file_in_folder = $mfolder->read_file($id);
			if(isset($cek_file_in_folder)){
				$del_file = $mfolder->delete_file_by_folder($id);
			}
			if(isset($get_child_id)){
				foreach ($get_child_id as $c) {
					$folderid=$c->id;
					$this->delete($folderid);
				}
			}
			if($del_parent == TRUE) echo "Delete file success!";
		}
		
	}
	
	function togglestatus() {
		$id = $_POST['id'];
		$mfolder = new model_folder();
		$result = array();
		if($mfolder->toggleStatus($id)) {
			$result['status'] = "OK";
			if( $mfolder->status == 1 )
				$result['ustatus'] = "Enable";
			else $result['ustatus'] = "Disable";
		} else {
			$result['status'] = "NOK";
			$result['error'] = $mfolder->error;
		}
		echo json_encode($result);
	}
}
?>