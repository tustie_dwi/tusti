<?php
class elearning_krs extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 				
		
	}
	
	function krs_dosen_pa(){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$this->add_style('bootstrap/css/add.css');
		$this->add_style('select2/select2.css');
		$this->add_script('select2/select2.js');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/DT_bootstrap.js');	
		$this->add_script('js/assignment/assignment.js');
		$this->add_script('player/flowplayer.js');
		
		$this->add_script('data_table/js/jquery.dataTables.min.js');
		$this->add_script('data_table/js/datatables.js');
		
		$m_krs 		= new model_krs();
		
		$data['posts'] = $m_krs->get_mhs_by_pa($staff);
		
		$this->add_script('js/all.js');
		
		$this->view('krs/index-dosen.php', $data);
	}

	function cek_krs_date(){
		$m_krs 		= new model_krs();
		$tgl = $m_krs->get_tgl_krs(date('Y-m-d'));
		
		if(! $tgl) $this->redirect('module/elearning/home');
	}
	
	function index(){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		
		if($role=="dosen"){ $this->krs_dosen_pa(); }
		
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$this->add_style('bootstrap/css/add.css');
		$this->add_style('select2/select2.css');
		$this->add_script('select2/select2.js');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/DT_bootstrap.js');	
		$this->add_script('js/assignment/assignment.js');
		$this->add_script('player/flowplayer.js');
		
		$this->add_script('data_table/js/jquery.dataTables.min.js');
		$this->add_script('data_table/js/datatables.js');
		
		$m_krs 		= new model_krs();
		$m_general	= new model_general();
		
		if(isset($_POST['mkd_id'])){
			$mkd_id = $_POST['mkd_id'];
		}
		else{
			$mkd_id = NULL;
		}
		$data['mkd_id'] = $mkd_id;
		
		$data_mhs = $m_krs->get_fakultas_id_by_mhs($this->coms->authenticatedUser->mhsid);
		$data['list_mkd'] = $m_krs->get_mk_for_select($data_mhs->fakultas_id, $data_mhs->cabang_id, $data_mhs->prodi_id);
		$data['mkd'] = $m_krs->get_mkd($mkd_id, $data_mhs->fakultas_id, $data_mhs->cabang_id, $data_mhs->prodi_id);
		
		//$data['krs'] = $m_general->get_mkd_mhs($mhs_id);
		$data['krs'] = $m_krs->get_krs_mhs($mhs_id);
		
		$this->add_script('js/all.js');
		$this->view('krs/index.php', $data);
	}
	
	/*function convert_date($date){
		$date = explode('-', $date);
		return substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}*/
	
	function add($jadwal_id, $kelas){
		$m_krs 		= new model_krs();
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$id 	 = $this->coms->authenticatedUser->id;
		
		$m_general		  = new model_general();
		$mhs_data = $m_general->get_user_profile($id);
		
		$data_mhs = $m_krs->get_fakultas_id_by_mhs($this->coms->authenticatedUser->mhsid);
		
		$mk = $m_krs->get_mk_detail($jadwal_id);
		$data = array(
			'krs_id' => $m_krs->get_krs_id(),
			'mahasiswa_id' => $mhs_id,
			'mkditawarkan_id' => $mk->mkditawarkan_id,
			'nama_mk' => $mk->keterangan,
			'kode_mk' => $mk->kode_mk,
			'sks' => $mk->sks,
			'kelas' => $kelas,
			'nilai_akhir' => 0,
			'tahun_akademik' => $m_krs->get_tahunakademik_aktif(),
			'inf_semester' => $m_krs->get_sks_curr($mhs_id),
			'user_id' => $id,
			'last_update' => date('Y-m-d H:i:s')
		);
		
		// foreach ($data as $key => $value) {
			// echo $key . ' => ' . $value . '<br>';
		// }
		$m_krs->add_krs($data);
		
		/*$data = array(
			'mahasiswa_id' => $mhs_id,
			'foto' => $mhs_data->foto,
			'nama' => $mhs_data->name,
			'keterangan' => $mk->keterangan,
			'mkditawarkan_id' => $mk->mkditawarkan_id,
			'kelas' => $kelas,
			'prodi_id' => $data_mhs->prodi_id,
			'jadwal_id' => $m_krs->get_jadwal_by_mk($mk->mkditawarkan_id, $kelas),
			'is_praktikum' => 0
		);
		
		// echo '<hr>';
		// foreach ($data as $key => $value) {
			// echo $key . ' => ' . $value . '<br>';
		// }
		$m_krs->add_mhs_mkd($data);*/
		$this->redirect('module/elearning/krs');
	}

	/*function del_mkd($mkd, $kelas){
		$m_krs 		= new model_krs();
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		
		$m_krs->del_mkd_mhs($mhs_id, $mkd, $kelas);
		$this->redirect('module/elearning/krs');
	}*/
	
	function del_mkd($krs){
		$m_krs 		= new model_krs();
			
		$m_krs->del_mkd_mhs($krs);
		$this->redirect('module/elearning/krs');
	}
	
}
?>