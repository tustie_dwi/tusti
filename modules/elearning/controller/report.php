<?php
class elearning_report extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function cek_krs_date(){
		$m_krs 		= new model_krs();
		
		$tgl = $m_krs->get_tgl_krs(date('Y-m-d'));
		if($tgl) return true;
		else return false;
	}
	
	function index($mkid=NULL, $kategori=NULL, $data=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$m_general		  = new model_general();
		$m_assignment = new model_assignment();
		$mquiz		  = new model_quiz();
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $key->foto;
		$data['kategori'] = $kategori;
		
		//general
		if(! empty($staff)) $data['mkd'] = $m_general->get_mkd_dosen($staff);
		else $data['mkd'] = $m_general->get_mkd_mhs($mhs_id);
		
				
		if(! empty($staff)) $data['jadwal'] = $m_general->get_jadwal_dosen($staff);
		else $data['jadwal'] = $m_general->get_jadwal_mhs($mhs_id);
		
		$data['post'] = $m_general->get_post($mkid,"","'note','assignment','quiz'",1,$data['mkd'], $data['jadwal']);
		
		//note
		$data['library'] = $m_general->get_folder('',$this->coms->authenticatedUser->id,'');
		
		//tugas
		$data['jadwal'] = $m_assignment->get_jadwal($staff);
		
		$this->add_style('bootstrap/css/add.css');
		$this->add_style('select2/select2.css');
		$this->add_style('player/skin/all-skins.css');
		$this->add_style('player/skin/minimalist.css');
		$this->add_script('select2/select2.js');
		if($kategori!='event'):
			$this->add_style('datepicker/css/datetimepicker.css');
			$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
			$this->add_style('datatables/DT_bootstrap.css');
			$this->add_script('datatables/jquery.dataTables.js');	
			$this->add_script('datatables/DT_bootstrap.js');	
			$this->add_script('js/assignment/assignment.js');
		else :
			$this->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
			$this->add_script('js/bootstrap-timepicker.min.js'); //timepicker
			$this->add_script('js/jquery/jquery.tokeninput.js');
			
			$this->add_script('datepicker/js/moment-2.4.0.js');
			$this->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
				
			$this->add_script('js/jquery/tagmanager.js');
		endif;
		
		$this->add_script('js/general/general.js');
		$this->add_script('player/flowplayer.js');
		
		$this->view('home/index.php',$data);
	}
	
	function krs(){
		$user 	= $this->coms->authenticatedUser->role;
		$staff	= $this->coms->authenticatedUser->staffid;
		
		$m_krs = new model_krs(); 
		
		$data['krs'] = $m_krs->get_mhs_by_pa($staff);		
		
		$this->add_script('js/pembimbing.js');
		$this->index("", 'krs', $data);
	}
	
	function event(){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mevent = new model_event(); 
		
		if(isset($_POST['tahun'])) $tahun = $_POST['tahun'];
		else $tahun = date('Y');
		
		if(isset($_POST['bulan'])) $bulan = $_POST['bulan'];
		else $bulan = date('m');
		
		$data['year'] = $tahun;
		$data['month'] = $bulan;	

		$data['ruang'] = $mevent->get_ruangan($this->coms->authenticatedUser->fakultas, $this->coms->authenticatedUser->cabang);
		$data['jenis_kegiatan'] = $mevent->get_jeniskegiatan();
		
		if($role != 'mahasiswa') $data['all_event'] = $mevent->get_all_event($staff, $bulan, $tahun);
		else $data['all_event'] = $mevent->get_all_event($mhs, $bulan, $tahun);
		
		$this->add_script('jquery/tagmanager.js');
		
		$this->add_style('css/calendar.css');
		
		$this->add_script('js/general/event.js');
		$this->index("", 'event', $data);
	}
	
	function get_data_event($data=NULL, $list_day=NULL, $month=NULL, $year=NULL, $running_day=NULL){
		if(count($data)>0){
		 $i=0;
		 foreach($data as $row):
			 $paymentDate  = strtotime($year."-".$month."-".$list_day." 00:00:00");
			 
			 $contractDateBegin = strtotime($row->tgl." 00:00:00");
			 $contractDateEnd  = strtotime($row->tgl_selesai." 00:00:00");
			 
			 
			if($paymentDate >= $contractDateBegin && $paymentDate <= $contractDateEnd){
			 	
				$i++;
				
				$icon = $note = $sclass = $tclass = '';
				$this->cetak_label($row, $icon, $note, $sclass, $tclass);
				
				$data['row'] = $row;
				$data['icon'] = $icon;
				$data['note'] = $note;
				$data['sclass'] = $sclass;
				$data['tclass'] = $tclass;
				$data['i'] = $i;
				
				$this->view('home/label-calendar.php', $data );
			 }
			
		 endforeach;
		 if($i>1)echo '<div style="text-align:left"><small><a href="javascript::" class="show-calendar-it" data-mincal="0" data-maxcal="'.$i.'" class="text text-info"><span class="fa fa-angle-down"></span> More</a></small></div>';
		}
	}
	
	function khs(){
		$mkhs = new model_khs();
		
		$user = $this->coms->authenticatedUser->role;
		$staff	= $this->coms->authenticatedUser->staffid;
		 
		if($user=="mahasiswa" || $user=="dosen" || $user=="admin"){
			
			$data['user']=$user;
			if(isset($_POST['mahasiswa_id'])){
				$mhsid = $_POST['mahasiswa_id'];
			}
			else{
				$mhsid	= $this->coms->authenticatedUser->mhsid;
			}
			
			if(isset($_POST['cmbsemester'])&&($_POST['cmbsemester'])){
				$semester = $_POST['cmbsemester'];			
			}else{
				$semester = $mkhs->get_semester_aktif();
			}
			
						
			$data['semester']	= $mkhs->get_semester($mhsid);
			$data['semesterid'] = $semester;
			$data['mhsid'] = $mhsid;
			$data['staff_id'] = $staff;
			
			$data['post_khs']	= $mkhs->read($semester, $mhsid, '', '');
			$data['identitas']	= $mkhs->read($semester, $mhsid, '1', ''); //1 adalah syarat saja supaya tidak null
			$data['semesterke']	= $mkhs->read('', $mhsid, '', '1'); //1 adalah syarat saja supaya tidak null			
			
			$this->index("", 'khs', $data);
		}
	}
	
	function adviser(){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpemb 	= new model_pembimbing();
		$data['staff'] = $staff;
		$data['mhs'] = $mhs;
		$data['post_data'] = $mpemb->read($staff,$mhs);
		
		if($staff){ //khusus halaman dosen
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->add_script('js/pembimbing.js');
		}
		
		$this->index("", 'pembimbing', $data);
	}
	
	function detail_adviser(){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mhsid = $_POST['mhs_id'];
		$data['staff'] = $staff;
		$data['mhs'] = $mhs;
		
		$mpemb 	= new model_pembimbing();
		
		
		$data['post_data'] = $mpemb->get_data_akademik($mhsid);
		$data['biodata'] = $mpemb->get_data_mhs($mhsid);
		
		$this->add_script('js/pembimbing.js');
		
		$this->index("", 'detail_adviser', $data);
		
		//$this->view("pembimbing/detail.php", $data);
	}
	
	function detail_krs(){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		
		$data['staff'] = $staff;
		$data['mhs'] = $mhs;
		
		$mkrs = new model_krs();
		$mkhs = new model_khs();
		
		if(isset($_POST['cmbsemester'])&&($_POST['cmbsemester'])){
			$semester = $_POST['cmbsemester'];			
		}else{
			$semester = $mkhs->get_semester_aktif();
		}
		
		$mhsid = $mkrs->get_mhsid($_POST['mhs_id']);
		
		//$mkrs->rekap_ipk($mhsid);
		
		$data['semester']	= $mkhs->get_semester($mhsid);
		$data['semesterid'] = $semester;
		$data['mhsid'] 		= $mhsid;
		$data['staff_id'] 	= $staff;
		
		$data['post_khs']	= $mkhs->read($semester, $mhsid, '', '');
		$data['identitas']	= $mkhs->read($semester, $mhsid, '1', ''); //1 adalah syarat saja supaya tidak null
		$data['semesterke']	= $mkhs->read('', $mhsid, '', '1'); //1 adalah syarat saja supaya tidak null		
		
		
		$data['posts'] 		= $mkrs->get_krs_mhs($mhsid, $semester);
		$data['ip'] 		= $mkrs->get_ip_terakhir($mhsid);		
		
		$this->add_script('js/general/krs.js');
		
		
		$this->index("", 'detail_krs', $data);
	}
	
	function convertTohuruf($angka){
		if($angka > 80){
			$huruf = 'A';
		}
		elseif($angka > 75 && $angka <= 80){ 
			$huruf = 'B+';
		}
		elseif($angka > 69 && $angka <= 75){
			$huruf = 'B';
		}
		elseif($angka > 60 && $angka <= 69){
			$huruf = 'C+';
		}
		elseif($angka > 55 && $angka <= 60){
			$huruf = 'C';
		}
		elseif($angka > 50 && $angka <= 55){
			$huruf = 'D+';
		}
		elseif($angka > 44 && $angka <= 50){
			$huruf = 'D';
		}
		elseif($angka <= 44){
			$huruf = 'E';
		}
		return $huruf;
	}
	
	function IPK($mhsid){
		$mkhs = new model_khs();
		$ipk = $mkhs->get_sks_all_count($mhsid);
		if(isset($ipk)){
			foreach($ipk as $dt){
				$hasil = $dt->IPK;
				return $hasil;
			}
		}
	}
	
	function kekata($x) { 
		$x = abs($x);
		$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"); 
		$temp = "";
		if ($x <12) { 
			$temp = "". $angka[$x];
		} else if ($x <20) { 
			$temp = kekata($x - 10). " belas";
		} 
		return $temp; 
	}
	
	function get_data_calendar($list_day=NULL, $month=NULL, $year=NULL, $running_day=NULL){
		echo "<br>";
		$mevent = new model_event();
		
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
	
		
		if($role=='mahasiswa'){
			$data = $mevent->getEvent($list_day, $month, $year, $running_day,$mhs);
		}else{
			$data = $mevent->getEvent($list_day, $month, $year, $running_day,$staff);
		}
		
		if($data){
		 $i=0;
		 foreach($data as $row):
			$i++;
			
			$icon = $note = $sclass = $tclass = '';
			$this->cetak_label($row, $icon, $note, $sclass, $tclass);
			
			$data['row'] = $row;
			$data['icon'] = $icon;
			$data['note'] = $note;
			$data['sclass'] = $sclass;
			$data['tclass'] = $tclass;
			$data['i'] = $i;
			$this->view('home/label-calendar.php', $data );
			
		 endforeach;
		 // if($i>1)echo '<div style="text-align:left"><small><a href="javascript::" class="show-calendar-it" data-mincal="0" data-maxcal="'.$i.'" class="text text-info"><span class="fa fa-angle-down"></span> More</a></small></div>';
		}	
	}
	
	function add_kegiatan(){
		$mevent = new model_event();	
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$ruang = '';
		if(isset($_POST['ruang']))
		foreach($_POST['ruang'] as $x) $ruang = $ruang . ', ' . $x;
		
		if(empty($_POST['tanggal_sampai'])) $tgl_selesai = $_POST['tanggal'] ;
		else $tgl_selesai = $this->convert_date_format($_POST['tanggal_sampai']);
		
		$data = array(
			'aktifitas_id' => $mevent->get_aktifitasId(),
			'karyawan_id' => $staff,
			'mahasiswa_id' => $mhs,
			'user_id' => $user,
			'tgl' => $_POST['tanggal'],
			'tgl_selesai' => $tgl_selesai,
			'jam_mulai' => $this->convert_am($_POST['jam_mulai']),
			'jam_selesai' => $jam_selesai = $this->convert_am($_POST['jam_mulai']),
			'judul' => $_POST['judul'],
			'lokasi' => $_POST['lokasi'],
			'jenis_kegiatan_id' => $_POST['jenis_kegiatan'],
			'inf_ruang' => substr($ruang,2)
		);
		
	
		
		$mevent->save_aktifitas($data);
	}

	function convert_date_format($date){
		$data = explode("/", $date);
		if(isset($data[2])) return $data[2].'-'.$data[0].'-'.$data[1];
		else return $date;
	}
	
	function convert_am($time){
		if(empty($time)) return "";
		$data = explode(":", $time);
		
		if(substr($time,-2) == 'PM') $am = 12;
		else $am = 0;
		
		return (substr($data[0],0,2) + $am) . ':' . substr($data[1], 0,2) . ':00';
	}
	
	function edit_kegiatan(){
		$ruang = '';
		if(isset($_POST['edit_ruang']))
			foreach($_POST['edit_ruang'] as $x) $ruang = $ruang . ', ' . $x;
		
		$mulai = $this->convert_am($_POST['edit_jam_mulai']);
		$selesai = $this->convert_am($_POST['edit_jam_selesai']);
		$tgl_selesai = $this->convert_date_format($_POST['edit_tgl_selesai']);
		
		$mevent = new model_event();	
		
		$mevent->edit_aktifitas($mulai, $selesai, $tgl_selesai, substr($ruang,2));
	}
	
	function hapus_kegiatan(){
		$mevent = new model_event();	
		$mevent->hapus_aktifitas();
	}
	
	function cetak_label($row, &$icon, &$note, &$sclass, &$tclass){
		$mevent = new model_event();
		switch (strToLower($row->jenis_kegiatan)){
			case 'konseling':
				$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'konseling';
				$tclass= 'text-putih';
			break;
			
			case 'rapat':
				$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'rapat';
				$tclass= 'text-putih';
			break;
			
			case 'kemahasiswaan':
				$icon = "";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'mhs';
				$tclass= 'text-putih';
			break;
			
			case 'kuliah tamu':
				$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'kuliahtamu';
				$tclass= 'text-putih';
			break;
			
			case 'wisuda':
				$icon = "<i class='fa fa-star'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'wisuda';
				$tclass= 'text-putih';
			break;
			
			case 'kunjungan':
				$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'kunjungan';
				$tclass= 'text-putih';
			break;
			
			case 'rekrutmen':
				$icon = "<i class='fa fa-signal'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'rekrutmen';
				$tclass= 'text-putih';
			break;
			
			case 'penelitian':
				$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'success';
			break;
			
			case 'pelatihan':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'pelatihan';
				$tclass= 'text-putih';
			break;
			
			case 'workshop':
				$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'workshop';
				$tclass= 'text-putih';
			break;
			case 'seminar':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'seminar';
				$tclass= 'text-putih';
			break;
			case 'uts':
				$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
				$note = $row->jenis;	
				$sclass= 'info';	
				$tclass= 'text-info';									
			break;
			case 'uas':
				$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
				$note = $row->jenis;	
				$sclass= 'info';	
				$tclass= 'text-putih';
			break;
			case 'lain-lain':
				$icon = "";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'success';
				$tclass= 'text-putih';
			break;
			case 'olahraga dan seni':
				$icon = "<i class='fa fa-flag'></i>&nbsp;";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'porseni';
				$tclass= 'text-putih';
			break;
			default:
				$icon = "";
				$note = $mevent->potong_kalimat($row->judul,5);
				$sclass= 'info';
				$tclass= 'text-putih';
			break;
		}
	}
	
	
	
}
?>