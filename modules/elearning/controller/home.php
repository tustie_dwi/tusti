<?php
class elearning_home extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function cek_krs_date(){
		$m_krs 		= new model_krs();
		/*$tgl 		= $m_krs->get_tgl_krs();
		
		$paymentDate = date('Y-m-d');
	    $paymentDate=date('Y-m-d', strtotime($paymentDate));;
	    echo $paymentDate; // echos today! 
	    $contractDateBegin = date('Y-m-d', strtotime($tgl->tgl_mulai));
	    $contractDateEnd = date('Y-m-d', strtotime($tgl->tgl_selesai));
	
	    if (! ($paymentDate > $contractDateBegin) && ! ($paymentDate < $contractDateEnd)){
	      return FALSE;
	    }
		else return TRUE;*/
		
		$tgl = $m_krs->get_tgl_krs(date('Y-m-d'));
		if($tgl) return true;
		else return false;
	}
	
	function profile($str=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$data['tmp']	= $str;
		
		$m_general		  = new model_general();
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $key->foto;
		
		$data['m_general'] = $m_general;
		
		if(! empty($staff)):
			$data['mkd'] = $m_general->get_mkd_dosen($staff);
			$data['mhs'] = $m_general->get_jml_mhs_dosen($staff);
		else:
			$data['mkd'] = $m_general->get_mkd_mhs($mhs_id);
			$data['mhs'] = "0";
		endif;
		
		$this->add_script('js/all.js');
		
		$this->view('home/profile.php', $data);
		
	}
	
	function settings($str=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$data['tmp']  = $str;
		
		$m_general	  = new model_general();
		
		$data['key']  = $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $data['key']->foto;
		
		$this->add_script('js/all.js');
		$this->add_script('bootstrap/js/jasny-bootstrap.min.js');
		
		$this->view('home/settings.php', $data);
		
	}
	
	function update_profile(){
		$user_id = $this->coms->authenticatedUser->id;
		$m_general		  = new model_general();
		
		if(isset($_POST['email'])):
			$datanya= array('email'=>$_POST['email'], 'name'=>$_POST['nama']);
			$idnya	= array('id'=>$user_id);
			
			$m_general->update_profile($datanya, $idnya);
		endif;
		
		$this->settings();
	}

	
	function index($mkid=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$m_general		  = new model_general();
		$m_assignment = new model_assignment();
		$mquiz		  = new model_quiz();
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $key->foto;
		
		//general
		if(! empty($staff)) $data['mkd'] = $m_general->get_mkd_dosen($staff);
		else $data['mkd'] = $m_general->get_mkd_mhs($mhs_id);
		
		if(! empty($staff)) $data['jadwals'] = $m_general->get_jadwal_dosen($staff);
		else $data['jadwals'] = $m_general->get_jadwal_mhs($mhs_id);
		
		$data['post'] = $m_general->get_post($mkid,"","'note','assignment','quiz'",1,$data['mkd'], $data['jadwals']);
		
		//note
		$data['library'] = $m_general->get_folder('',$this->coms->authenticatedUser->id,'');
		
		//tugas
		$data['jadwal'] = $m_assignment->get_jadwal($staff);
		//$data['load_tugas'] = $m_assignment->get_tugas_submitted_all($this->coms->authenticatedUser->id);
	
		// $data['makul']	= $mquiz->get_jadwal('',$staff);
		
		$this->add_style('bootstrap/css/add.css');
		$this->add_style('select2/select2.css');
		$this->add_style('player/skin/all-skins.css');
		$this->add_style('player/skin/minimalist.css');
		$this->add_script('select2/select2.js');
		/*$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/jquery.dataTables.js');	
		$this->add_script('datatables/DT_bootstrap.js');	*/
		
		$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/jquery.dataTables.js');	
		$this->add_script('datatables/DT_bootstrap.js');
		
		$this->add_script('js/assignment/assignment.js');
		$this->add_script('js/general/general.js');
		$this->add_script('player/flowplayer.js');
				
		$this->view('home/index.php',$data);
	}
	
	function show_post($post_id){
		$m_general		  = new model_general();
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		$data['foto']    = $this->coms->authenticatedUser->foto;
		$data['post'] = $m_general->get_post_detail($post_id);
		
		if($data['post']):		
			$data['jml_notifikasi'] = $m_general->get_jml_notifikasi($data['user_id']);
			$data['notifikasi'] = $m_general->get_notifikasi_new($data['user_id']);
			
			$this->add_style('bootstrap/css/add.css');
			$this->add_style('select2/select2.css');
			$this->add_style('player/skin/all-skins.css');
			$this->add_style('player/skin/minimalist.css');
			$this->add_script('select2/select2.js');
			$this->add_style('datepicker/css/datetimepicker.css');
			$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
			$this->add_style('datatables/DT_bootstrap.css');
			$this->add_script('datatables/jquery.dataTables.js');	
			$this->add_script('datatables/DT_bootstrap.js');	
			$this->add_script('js/assignment/assignment.js');
			$this->add_script('js/general/general.js');
			$this->add_script('player/flowplayer.js');
	
			$this->view('home/detail_post.php',$data);
		endif;
	}
	
	function add_post($mkid=NULL){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		$data['foto']    = $this->coms->authenticatedUser->foto;

		$m_general		  = new model_general();
		
		//general
		if(! empty($staff)) $data['mkd'] = $m_general->get_mkd_dosen($staff);
		else $data['mkd'] = $m_general->get_mkd_mhs($mhs_id);
		$data['post'] = $m_general->get_post($mkid,"","'note','assignment','quiz'", $_POST['page_']);
		
		if(empty($data['post'])) echo "habis";
		else{
			$this->add_style('player/skin/all-skins.css');
			$this->add_style('player/skin/minimalist.css');
			$this->add_style('datatables/DT_bootstrap.css');
			$this->add_script('datatables/jquery.dataTables.js');	
			$this->add_script('datatables/DT_bootstrap.js');	
			$this->add_script('js/general/general.js');
			$this->add_script('player/flowplayer.js');
			
			$this->view('home/view-content.php',$data);
		}
	}
	
	function save_post($postid=NULL, $kategori=NULL, $linkid=NULL, $userid=NULL, $isi=NULL, $parentid=NULL, $lastupdate=NULL, $mkd=NULL, $url=NULL, $is_notif=1){
		$m_general	= new model_general();
		$data = array(
			'post_id' => $postid,
			'kategori' => $kategori,
			'link_id' => $linkid,
			'user_id' => $userid,
			'isi' => $isi,
			'parent_id' => $parentid,
			'last_update' => $lastupdate
		);
		if($m_general->save_post($data)){
			if($is_notif == 1) $this->add_notif_mkd($postid, $mkd, $kategori, $url);
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	function delete_post(){
		$post_id = $_POST['post_id'];
		$m_general	= new model_general();
		$m_general->delete_post($post_id);
	}
	
	function delete_comment(){
		$post_id = $_POST['post_id'];
		$m_general	= new model_general();
		$m_general->delete_comment($post_id);
		echo "success";
	}
	
	function save_share($iduser=NULL, $idpost=NULL, $initshare=NULL){
		$m_general	= new model_general();
		$share 		= Array('id_user' => $iduser,
							'id_post' => $idpost,
							'init_share' => $initshare
						   );
		if($m_general->save_share($share)){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function edit_post(){
		$m_general	= new model_general();
		$m_general->edit_post();
	}

	function save_attach($isi=NULL, $postid=NULL, $link=NULL, $mk_id=NULL, $tugasid=NULL, $library=NULL){
		$count = $this->upload_file($isi, $postid, $mk_id, $tugasid,"",$library);
		if(isset($link)&&$link!=""){
			$this->save_link($link, $postid, ($count+1));
		}
		if(isset($library)&&$library!=""){
			$count = (count($library) + ($count+1));
			$this->save_library($library, $postid, ($count), $isi, $mk_id, $tugasid);
		}
	}
	
	function log_view(){
		$mfile = new model_file();
		$user = $this->coms->authenticatedUser->id;
		$action = $_POST['action'];
		$fileid = $_POST['file_id'];
		$result = $mfile->log_data($user, $action, $fileid);
		if($result == TRUE){
			echo "BERHASIL";
		}
		else echo "GAGAL";
	}
	
	function log_download(){
		$mfile = new model_file();
		$user = $this->coms->authenticatedUser->id;
		$action = $_POST['action'];
		$fileid = $_POST['file_id'];
		$result = $mfile->log_data($user, $action, $fileid);
		if($result == TRUE){
			echo "BERHASIL";
		}
		else echo "GAGAL";
	}

	function document($file_name){
		$mfile = new model_file();
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		$data['foto']    = $this->coms->authenticatedUser->foto;
		$data['path'] = $this->config->file_url_view.'/'.$mfile->get_locfile($file_name);	
		
		$this->add_style('select2/select2.css');
		$this->add_style('player/skin/all-skins.css');
		$this->add_style('player/skin/minimalist.css');
		$this->add_script('select2/select2.js');
		$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/jquery.dataTables.js');	
		$this->add_script('datatables/DT_bootstrap.js');	
		$this->add_script('js/assignment/assignment.js');
		$this->add_script('js/general/general.js');
		$this->add_script('player/flowplayer.js');
		
		$this->view('home/document.php',$data);
	}

	//=============DOWNLOAD===============//
	function getHeaders($url){
	  $ch = curl_init($url);
	  curl_setopt( $ch, CURLOPT_NOBODY, true );
	  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
	  curl_setopt( $ch, CURLOPT_HEADER, false );
	  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	  curl_setopt( $ch, CURLOPT_MAXREDIRS, 3 );
	  curl_exec( $ch );
	  $headers = curl_getinfo( $ch );
	  curl_close( $ch );
	
	  return $headers;
	}
	
	function take_file($url, $path){
	
		$output_filename = basename($url);

		$resource = curl_init();
		curl_setopt($resource, CURLOPT_URL, $url);
		curl_setopt($resource, CURLOPT_HEADER, 0);
		curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($resource, CURLOPT_BINARYTRANSFER, 1);
		$file = curl_exec($resource);
		curl_close($resource);
		
		$xpath = "assets/upload/".$output_filename;
		
		unlink($xpath);

		$fh = fopen($xpath, 'x');
		fwrite($fh, $file);
		fclose($fh);		
		
		header('Content-Type: application/octet-stream');
		header('Content-Type: application/force-download');
		header('Content-Disposition: attachment; filename=' . $output_filename);	
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($xpath));
		ob_clean();
		flush();
		readfile($xpath);
		unlink($xpath);
		exit;
		
	}
	
	function download($filename){
		$mfile = new model_file();
		
		//$path = $this->config->file_url_view."/".$mfile->get_locfile($filename);	
		
		
		$path = "/".$filename;
		$url = $this->config->file_url_view."/".$mfile->get_locfile($filename);
		
		//echo $url.$path."<br>";
		$headers = $this->getHeaders($url);
		

		//echo $headers['http_code'];
		if ($headers['http_code'] === 200) {
		  if ($this->take_file($url, $path)){
		    echo 'Download complete!';
		  }
		  else{
		  	echo 'Failed!';
		  }
		}
		else{
			echo "File Not Found!";
		}
	}
	
	//================================NOTE START================================//
	function save_note (){
		$m_general	= new model_general();
		$post_id = $m_general->get_post_id();
		$this->save_post($post_id,'note','',$this->coms->authenticatedUser->id,$_POST['isi'],'',date('Y-m-d H:i:s'), $_POST['mkd_id'],$this->location('module/elearning/home/show_post/'.substr(md5($post_id),8,7)));
		$data = array(
			'id_user' => $_POST['mkd_id'],
			'id_post' => $post_id,
			'init_share' => 'mkd'
		);
		if($m_general->save_share($data)) {
			ob_start();
			$count = $this->upload_file($_POST['isi'], $post_id);
			
			if(isset($_POST['link'])){
				$this->save_link($_POST['link'], $post_id, ($count+1));
			}
			if(isset($_POST['library'])){
				$count = (count($_POST['library']) + $count);
				$this->save_library($_POST['library'], $post_id, ($count+1));
			}
			echo "Your note has been posted";
		}
		else echo "Sorry, your note failed to posted. Please repeat again";
		
		// $this->add_notif_mkd($post_id, $_POST['mkd_id'], 'note', $this->location('module/elearning/home/show_post/'.substr(md5($post_id),8,7)) );
	}
	
	function save_library($library=NULL, $post_id=NULL, $urut=NULL, $isi=NULL, $mk_id=NULL, $tugasid=NULL){
		$m_general	= new model_general();
		foreach($library as $key){
			$data = array(
				'post_id' => $post_id,
				'attach' => $key,
				'jenis' => 'library',
				'urut' => $urut
			);
			$m_general->save_post_attach($data);
			$urut++;
			
			// $location = $key;
			// $library = $this->upload_file($isi,$post_id,$mk_id,$tugasid,"",$location);
		}
	}
	
	function save_link($link, $post_id, $urut){
		$m_general	= new model_general();
		foreach($link as $key){
			$data = array(
				'post_id' => $post_id,
				'attach' => $key,
				'jenis' => 'link',
				'urut' => $urut
			);
			$m_general->save_post_attach($data);
			$urut++;
		}
	}
	
	function update_avatar($str=NULL){
			
		if(isset($_FILES["file"]['name'])){
			$name = $_FILES["file"]['name'];
			$id = $this->coms->authenticatedUser->id;
			$role = $this->coms->authenticatedUser->role;
			
			$m_general	= new model_general();
			$file_ext = $this->get_file_ext($name);
			$max_size = $m_general->get_file_ext($file_ext);
			
			$name = $id.'.'.$file_ext;
			$file_type=$_FILES["file"]["type"] ; 
			$file_size=$_FILES["file"]["size"] ; 
			
			$upload_dir = 'assets/upload/foto/';		
			$upload_dir_db = 'upload/foto/';
			
			if($file_size <= $max_size){
				/*if (!file_exists($upload_dir)) {
					mkdir($upload_dir, 0777, true);
					chmod($upload_dir, 0777);
				}*/
				
				if(file_exists($upload_dir . $name)) unlink($upload_dir . $name);			
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $_FILES['file']['tmp_name'];
					$filename    = $name;
					$filenamenew = $name;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		            $ch = curl_init();
		            $options = array(
		                CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                	CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
		            ); // cURL options 
					curl_setopt_array($ch, $options);
			        curl_exec($ch);
			        if(!curl_errno($ch))
			        {
			            $info = curl_getinfo($ch);
			            if ($info['http_code'] == 200)
			               $errmsg = "File uploaded successfully";
			        }
			        else
			        {
			            $errmsg = curl_error($ch);
			        }
			        curl_close($ch);
					//------UPLOAD USING CURL----------------
					
					// rename($_FILES['file']['tmp_name'], $upload_dir . $name);
				}
				
				$fileloc = $upload_dir_db . $name;
				
				$m_general->update_avatar($id, $fileloc, $role);
			}
		}
		if($str): 
			if($str=='profile') $this->profile();
			else $this->settings();
		else: $this->redirect('module/elearning'); endif;
	}
	
	function upload_file($isi,$post_id,$mkd_id=NULL,$tugasid=NULL,$uploadid=NULL,$library=NULL){
		$m_general	= new model_general();
		$id_user = $this->coms->authenticatedUser->id;
		$uploadby = $this->coms->authenticatedUser->username;
		$tgl_upload = date("Y-m-d H:i:s");
		$month = date('m');
		$year = date('Y');
		if(!isset($mkd_id)){
			$mkd_id = NULL;
		}
		if(!isset($tugasid)){
			$tugasid = NULL;
		}
		if(!isset($uploadid)){
			$uploadid = NULL;
		}
		
		if($library){
			foreach($library as $key){
				$data_library			= $m_general->get_attach_media_library($key);
				$library_name 			= $data_library->file_name;
				$library_file_type 		= $data_library->file_type;
				$library_file_loc 		= $key;
				$library_file_size 		= $data_library->file_size;
				$library_file_ext 		= $this->get_file_ext($library_name);
				$library_jenis_file_id 	= $m_general->cek_file_type($library_file_ext)->jenis_file_id;
				//exit();
				$data = Array(
							'file_id'=> $m_general->get_file_id(),
							'jenis_file_id' => $library_jenis_file_id,
						    'file_name'=>$library_name,
						    'file_type'=>$library_file_type,
						    'file_loc'=>$library_file_loc,
						    'file_size'=>$library_file_size,
						    'mkditawarkan_id' =>$mkd_id,
						    'tugas_id' =>$tugasid,
						    'upload_id' =>$uploadid,
						    'keterangan' => $isi,
						    'tgl_upload'=>$tgl_upload,
						    'upload_by'=>$uploadby,
						    'user_id'=>$id_user,
						    'last_update'=>date('Y-m-d H:i:s')
						);
				$m_general->save_file_log($data);
			}
		}
		
		if(isset($_FILES["file"]['name'])){
			 // echo count($_FILES["file"]['name']);
			 // exit();
			foreach($_FILES["file"]['name'] as $id => $name){
				if(! empty($name)){
					//echo $name . ' ';
					$maxfilesize = 0;
					$file_ext = $this->get_file_ext($name);
					$init_file = $m_general->cek_file_type($file_ext);
					if(isset($init_file->max_size)){
						$maxfilesize = $init_file->max_size;
						$jenis_file_id = $init_file->jenis_file_id;
					}
					
					$file = "";
					$file_type=$_FILES["file"]["type"][$id] ; 
					$file_size=$_FILES["file"]["size"][$id] ; 
					$upload_dir = 'assets/upload/file/'. $id_user . DIRECTORY_SEPARATOR . 'attach'. DIRECTORY_SEPARATOR .$year. '-' .$month . DIRECTORY_SEPARATOR ;		
					$upload_dir_db = 'upload/file/'. $id_user . DIRECTORY_SEPARATOR . 'attach'. DIRECTORY_SEPARATOR .$year. '-' .$month . DIRECTORY_SEPARATOR ;
		
					// if (!file_exists($upload_dir)) {
						// mkdir($upload_dir, 0777, true);
						// chmod($upload_dir, 0777);
					// }
					$file_loc = $upload_dir_db . $name;
					
					if ($file_size <= $maxfilesize AND isset($init_file->max_size)){
						if(file_exists($upload_dir . $name)) unlink($upload_dir . $name);			
						if($_SERVER['REQUEST_METHOD'] == 'POST') {
							//------UPLOAD USING CURL----------------
							//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
							$url	     = $this->config->file_url;
							$filedata    = $_FILES['file']['tmp_name'][$id];
							$filename    = $name;
							$filenamenew = $name;
							$filesize    = $file_size;
						
							$headers = array("Content-Type:multipart/form-data");
							$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
				            $ch = curl_init();
				            $options = array(
				                CURLOPT_URL => $url,
				                CURLOPT_HEADER => true,
								CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
				                CURLOPT_POST => 1,
				                CURLOPT_HTTPHEADER => $headers,
				                CURLOPT_POSTFIELDS => $postfields,
				                CURLOPT_INFILESIZE => $filesize,
				                CURLOPT_RETURNTRANSFER => true,
				                CURLOPT_SSL_VERIFYPEER => false,
				  				CURLOPT_SSL_VERIFYHOST => 2
				            ); // cURL options 
							curl_setopt_array($ch, $options);
					        curl_exec($ch);
					        if(!curl_errno($ch))
					        {
					            $info = curl_getinfo($ch);
					            if ($info['http_code'] == 200)
					               $errmsg = "File uploaded successfully";
					        }
					        else
					        {
					            $errmsg = curl_error($ch);
					        }
					        curl_close($ch);
							//------UPLOAD USING CURL----------------
							// rename($_FILES['file']['tmp_name'][$id], $upload_dir . $name);
						}
		
						$data = Array(
							'file_id'=> $m_general->get_file_id(),
							'jenis_file_id' => $jenis_file_id,
						    'file_name'=>$name,
						    'file_type'=>$file_type,
						    'file_loc'=>$file_loc,
						    'file_size'=>$file_size,
						    'mkditawarkan_id' =>$mkd_id,
						    'tugas_id' =>$tugasid,
						    'upload_id' =>$uploadid,
						    'keterangan' => $isi,
						    'tgl_upload'=>$tgl_upload,
						    'upload_by'=>$uploadby,
						    'user_id'=>$id_user,
						    'last_update'=>date('Y-m-d H:i:s')
						);
						$m_general->save_file_log($data);
						
						$data = array(
							'post_id' => $post_id,
							'attach' => $file_loc,
							'jenis' => 'file',
							'urut' => ($id + 1)
						);
						$m_general->save_post_attach($data);
					} //end id isempty
				}
			} //end of foreach
			return ($id+1);
		}
		else{
			return 0;	
		}
		
	} //end of function 
	
	function get_file_ext($name){
		$ext = explode(".", $name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function save_comment($assigment=NULL){
		$m_general	= new model_general();
		$post_id = $m_general->get_post_id();
		$link_id = $_POST['link_id'];
		
		$user = $m_general->get_notif_by_comment($_POST['post_id']);
		if($user){
			foreach($user as $key){
				$name = $this->coms->authenticatedUser->name;
				$id   = $this->coms->authenticatedUser->id;
				if($key->id != $id){
					$data = array(
						'notifikasi_id' => $m_general->get_notifikasi_id(),
						'kategori' => 'comment',
						'post_id' => $_POST['post_id'],
						'link_id' => $link_id,
						'user_from_name' => $name,
						'user_from' => $id,
						'user_to_name' => $key->nama,
						'user_to' => $key->id,
						'keterangan' => 'posting new comment',
						'tgl_notifikasi' => date('Y-m-d H:i:s')
					);
					$m_general->add_notif($data);
				}
			}
		}
		$save_comment = $this->save_post($post_id, 'comment', "", $this->coms->authenticatedUser->id, $_POST['isi'], $_POST['post_id'], date('Y-m-d H:i:s'));
		
		if(isset($assigment)){
			if($save_comment == TRUE) echo "Your note has been posted";
			else echo "Sorry, your note failed to posted. Please repeat again";
		}
		else{
			$this->view('home/view-comment.php');
			$comment = $_POST['foto'] . '>>Me>>' . $post_id . '>>->>' . $_POST['isi'] . '>>' . date('Y-m-d H:i:s');
			print_comment($comment, '-','', substr(md5($_POST['post_id']),8,7));
		}
	}
	
	function folder_back(){
		$m_general	= new model_general();
		$folder_id = $m_general->get_folder_parent_id($_POST['folder_id']);
		$this->get_library($folder_id);
	}
	
	function get_library($folder_id=NULL){
		if($folder_id == NULL) $folder_id = $_POST['folder_id'];
		
		$m_general	= new model_general();
		$folder = $m_general->get_folder('',$this->coms->authenticatedUser->id, $folder_id);
		$file = $m_general->get_file($folder_id);
		
		$is_ada = 0;
		echo '<input id="folder_back_parent_id" value="'.$folder_id.'" type="hidden" />';
		if($folder){
			$is_ada = 1;
			foreach ($folder as $key) { ?>
				<div class="col-md-3" style="padding: 0px 5px 0px 5px">
		  			<button title="<?php echo $key->folder_name ?>" class="btn btn-default" style="width: 100%; margin: 5px;" onclick="get_folder('<?php echo $key->id ?>')">
		  				<i class="fa fa-folder fa-3x"></i><br><?php echo $key->folder_name ?>
		  			</button>
		  		</div>
			<?php }
		}
		
		if($file){
			$is_ada = 1;
			foreach ($file as $key) { ?>
				<div class="col-md-3" style="padding: 0px 5px 0px 5px">
		  			<button onclick="add_library('<?php echo $key->file_loc ?>','<?php echo $key->file_name ?>')" title="<?php echo $key->file_name ?>" class="btn btn-default" style="width: 100%; margin: 5px;">
		  				<?php
		  					if($key->jenis_file == 'image') echo '<i class="fa fa-file-image-o fa-3x"></i>';
							elseif($key->jenis_file == 'document') echo '<i class="fa fa-file-zip-o fa-3x"></i>';
		  				?>
		  				<br><?php echo $key->file_name ?>
		  			</button>
		  		</div>
			<?php }
		}
		
		if($is_ada == 0) echo '<div class="col-md-3" style="padding: 0px 5px 0px 5px"><i class="fa fa-ban"></i> Empty folder !!</div>';
	}
	
	//================================TUGAS START================================//
	function save_tugas(){
		if((isset($_POST['assignment_title'])) && $_POST['assignment_title']!=""){
			$this->save_tugasToDB();
			exit();
		}else{
			echo "Process Failed!";
			exit();
		}
	}
	function save_tugasToDB(){
		ob_start();
		$m_assignment = new model_assignment();
		$m_general 	= new model_general();
		$mquiz		= new model_quiz();
		
		$tugasid		= $m_assignment->tugas_id();
		$post_id 		= $m_assignment->get_post_id();
		$action 		= "insert";			

		if(isset($_POST['jadwal'])){
			$jadwal_id	= $m_assignment->get_jadwal_id_by_md5($_POST['jadwal']);
			$mk_id	= $m_assignment->get_mk_by_jadwal($_POST['jadwal']);
		}
		
		if($_POST['select-materi']!='0'){
			$materiid	= $mquiz->get_materiid_md5($_POST['select-materi']);
		}else{
			$materiid	= null;
		}
		
		$judul		= $_POST['assignment_title'];
		$instruksi	= $_POST['instruction'];
		$keterangan	= $_POST['describe'];
		$tgl_mulai	= $_POST['start_date'];
		$tgl_selesai= $_POST['due_date'];
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");

		$ceknewtugas = $m_assignment->cek_new_tugas_by_judul($judul);
		$ceknewtugasbyid = $m_assignment->cek_id_tugas_by_id($judul);
		
		if(isset($judul) && isset($jadwal_id)){
			if(!isset($ceknewtugas)){
					$tugas 		= Array('tugas_id'=>$tugasid,  
										'jadwal_id'=>$jadwal_id,
										'materi_id'=>$materiid,
										'mkditawarkan_id'=>$mk_id,
										'judul'=>$judul,
										'instruksi'=>$instruksi, 
										'keterangan'=>$keterangan, 
										'tgl_mulai'=> $tgl_mulai, 
										'tgl_selesai'=> $tgl_selesai, 
										'user_id'=>$user,
										'last_update'=>$lastupdate
									   );
					$m_assignment->replace_tugas($tugas);
					
					/*-- post nilai --*/
					$post_nilai_id = "TG".$tugasid;
					
					$nilai 		= Array('post_nilai_id'=>$post_nilai_id, 
										'tugas_id'=>$tugasid, 					
										'jadwal_id'=>$jadwal_id, 
										'mkditawarkan_id'=>$mk_id,
										'judul'=>$judul,
										'kategori_nilai'=>'tugas',
										'tgl_mulai'=> $tgl_mulai, 
										'tgl_selesai'=> $tgl_selesai, 
										'user_id'=>$user,
										'last_update'=>$lastupdate
									   );
					$m_general->replace_post_nilai($nilai);
					/*-- end -- */
					
					$isi = $judul.'|'.$keterangan.'|'.$tgl_selesai;
					
					$link_url = $this->location('module/elearning/assignment/detail/'.substr(md5($tugasid),8,7));
					$this->save_post( $post_id,'assignment',$tugasid, $user, $isi, '',$lastupdate, $jadwal_id, $link_url );
					
					$mhs_list = $m_general->mahasiswa_list($_POST['jadwal']);
					if($mhs_list){
						foreach($mhs_list as $m){
							$mhsid 			= $m->mahasiswa_id;
							$uploadid		= $m_assignment->upload_id();
							$datanya 		= Array('upload_id'=>$uploadid,
													'mahasiswa_id'=>$mhsid,
													'tugas_id'=>$tugasid,
												    'user_id'=>$user,
												    'last_update'=>$lastupdate
													  );
							$m_assignment->replace_tugas_mhs($datanya);
							$post_id_response 		= $m_assignment->get_post_id();
							$this->save_post($post_id_response,'response',$uploadid,$user,"",$post_id,$lastupdate, $jadwal_id, $link_url, 0);
							
							/* save skor to post nilai */
							$nilai_id = 'TG'.$uploadid;
							$post_nilai_id = 'TG'.$tugasid;
							
							$data_nilai = array(
								'nilai_id' => $nilai_id,
								'mahasiswa_id' => $mhsid,
								'post_nilai_id' => $post_nilai_id,
								'link_id' =>$uploadid,
								'tbl_name'=>'tbl_tugas_mhs',
								'tgl_post' => date('Y-m-d H:i:s'),			
								'skor' => '0',
								'status'=>'Not Turn In',
								'user_id' => $user,
								'last_update' => date('Y-m-d H:i:s')
							);
							
							$m_general->save_post_nilai_mhs($data_nilai); 
						/* end */
						
			
						}
					}
					$save_share = $this->save_share($jadwal_id,$post_id,"jadwal");
					
					if($save_share == TRUE){
						if(isset($_POST['link'])){
							$link = $_POST['link'];
						}else $link = "";
						
						if(isset($_POST['library'])){
							$library = $_POST['library'];
						}else $library = "";
						
						$this->save_attach($judul, $post_id, $link, $mk_id, $tugasid, $library);
						echo "OK, your data has been saved!";
						exit();
					}
					else{
						echo "Failed, please try again!";
						exit();
					}
				}
				else {
					echo "Sorry, your data duplicate!";
					exit();
				}
		}
		else {
			echo "Failed, please try again!";
			exit();
		}
	}
	//================================TUGAS END================================//
	
	//---------------------------------notifikation
	function see_all(){
		$role    = $this->coms->authenticatedUser->role;
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;

		$m_general		  = new model_general();
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $key->foto;
		
		//general
		$data['jml_notifikasi'] = $m_general->get_jml_notifikasi($data['user_id']);
		$data['notifikasi'] = $m_general->get_notifikasi_new($data['user_id']);
		
		$data['not_data'] = $m_general->get_notifikasi_all($data['user_id']);
		
		$this->add_style('bootstrap/css/add.css');
		$this->add_style('bootstrap/css/new.css');
		$this->add_script('js/general/general.js');
		$this->add_script('js/general/all.js');
		
		$this->view('home/notification.php', $data);
	}
	
	function delete_notifikasi(){
		$m_general	= new model_general();
		$user_id = $this->coms->authenticatedUser->id;
		$m_general->delete_notifikasi($user_id);
	}
	
	function convert_date($date){
		$date = explode('-', $date);
		return substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}
	
	function update_read(){
		$m_general	= new model_general();
		$m_general->update_read();
	}
	
	function add_notif_mkd($post_id, $mkd, $kategori, $link_id){
		$m_general	= new model_general();
		
		$staff   = $this->coms->authenticatedUser->staffid;
		$mhs_id  = $this->coms->authenticatedUser->mhsid;
		$id = $this->coms->authenticatedUser->id;
		$name = $this->coms->authenticatedUser->name;
		
		if($kategori=='note'):
			$dosen = $m_general->get_notif_dosen_by_mk($mkd, $mhs_id);
			$mhs = $m_general->get_notif_mhs($mkd, $staff);
		else:
			$dosen = $m_general->get_notif_dosen($mkd);
			$mhs = $m_general->get_notif_mhs($mkd);
		endif;
		
		
		if($dosen){
			foreach($dosen as $key){
				if($key->id != $id){
					$data = array(
						'notifikasi_id' => $m_general->get_notifikasi_id(),
						'kategori' => $kategori,
						'post_id' => $post_id,
						'link_id' => $link_id,
						'user_from_name' => $name,
						'user_from' => $id,
						'user_to_name' => $key->nama,
						'user_to' => $key->id,
						'keterangan' => 'send new post',
						'tgl_notifikasi' => date('Y-m-d H:i:s')
					);
					$m_general->add_notif($data);
				}
			} //end of foreach dosen
		}
		
		if($mhs){
			foreach ($mhs as $key) {
				if($key->id != $id){
					$data = array(
						'notifikasi_id' => $m_general->get_notifikasi_id(),
						'kategori' => $kategori,
						'post_id' => $post_id,
						'link_id' => $link_id,
						'user_from_name' => $name,
						'user_from' => $id,
						'user_to_name' => $key->nama,
						'user_to' => $key->id,
						'keterangan' => 'send new post',
						'tgl_notifikasi' => date('Y-m-d H:i:s')
					);
					$m_general->add_notif($data);
				}
			} //end of foreach mhs
		}
	} //end of function add_notif
	
	//================================RESPONSE ASSIGNMENT START==============================//
	function saveAttach_Response($tugasid,$post_id,$uploadid){
		ob_start();
		$m_assignment 	= new model_assignment();
		$user			= $this->coms->authenticatedUser->id; 
		$lastupdate		= date("Y-m-d H:i:s");
		$parent 		= $_POST['post_parent_id'];
		$catatan 		= $_POST['catatan'];
		$mkd_id			= $_POST['mkid'];
		
		$save_post = $this->save_post($post_id,'response',$uploadid,$user,$catatan,$parent,$lastupdate);

		if($save_post == TRUE){
			$count = $this->upload_file("",$post_id,$mkd_id,$tugasid,$uploadid);
			if(isset($_POST['link'])){
				$this->save_link($_POST['link'], $post_id, ($count+1));
			}
			echo "OK, your data has been saved!";
			exit();
		}
		else {
			echo "Failed, please try again!";
			exit();
		}
	}
	//================================RESPONSE ASSIGNMENT END================================//
	function group(){
		$this->view('home/group.php');
	}
	
	
	
	
	
}
?>