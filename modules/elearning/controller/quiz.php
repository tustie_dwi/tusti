<?php
class elearning_quiz extends comsmodule {
	private $coms;
	
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){}
	
	function create(){
		$mquiz	= new model_quiz();
		$m_general= new model_general();
		
		$role   = $this->coms->authenticatedUser->role;
		$staff  = $this->coms->authenticatedUser->staffid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		// $data['user_id'] = $this->coms->authenticatedUser->fakultas;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] 	= $key->foto;
		
		$data['jml_notifikasi'] = $m_general->get_jml_notifikasi($data['user_id']);
		$data['notifikasi'] = $m_general->get_notifikasi_new($data['user_id']);
		
		$data['categoryList'] = $mquiz->get_kategori();
		
		$this->add_style('select2/select2.css');
		$this->add_script('select2/select2.js');
		$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_script('js/test/test.js');
		$this->add_script('js/general/general.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/jquery.dataTables.js');	
		$this->add_script('datatables/DT_bootstrap.js');	
		
		$this->view('quiz/create-quiz.php', $data);
	}
	
	function convert_date($date){
		$date = explode('-', $date);
		return substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}
	
	
	//---------------------------------------------------------------------------------
	function edit($id=NULL){
		// echo $staff  = $this->coms->authenticatedUser->staffid;
		$mquiz		= new model_quiz();
		$m_general 	= new model_general();
		
		$role   = $this->coms->authenticatedUser->role;
		$staff  = $this->coms->authenticatedUser->staffid;
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$key 		  = $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] = $key->foto;
		
		$data['categoryList'] = $mquiz->get_kategori();
		$data['posts'] = $mquiz->read($id,'');
		$data['soal'] = $mquiz->get_soal($id);
		$data['jawab'] = $mquiz->get_jawaban($id);
		
		$this->add_style('select2/select2.css');
		$this->add_script('select2/select2.js');
		$this->add_style('datepicker/css/datetimepicker.css');
		$this->add_script('datepicker/js/bootstrap-datetimepicker.js');
		$this->add_script('js/test/test.js');
		$this->add_style('datatables/DT_bootstrap.css');
		$this->add_script('datatables/jquery.dataTables.js');	
		$this->add_script('datatables/DT_bootstrap.js');
		$this->view('quiz/create-quiz.php', $data);
	}
	
	//---------------------------------------------------------------------------------
	function save(){
		if(isset($_POST['test-type'])!=""){
			$this->saveToDB();
			exit();
		}else{
			echo "Select quiz type first";
			exit;
		}
	}
		
	function saveToDB(){
		$mquiz		= new model_quiz();
		$m_general = new model_general();
		
		$title 		= $_POST['title'];
		$timelimit	= $_POST['time-limit'];
		$instruction= $_POST['instruction'];
		$keterangan = $_POST['about-quiz'];
		$timelimit	= $_POST['time-limit'];
		$start_date = $_POST['start-date'];
		$finish_date= $_POST['finish-date'];
		$is_publish = $_POST['is_publish'];
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		if(isset($_POST['random-test'])){
			$soal_is_random = 1;
		}else $soal_is_random = 0;
		
		$test_type = $_POST['test-type'];
		if($test_type=='exam'){
			$mkid		= $mquiz->get_mkid_md5($_POST['select-jadwal']);
			$jadwalid	= null;
		}else{
			$jadwal	= $mquiz->get_jadwalid_md5($_POST['select-jadwal']);
			foreach ($jadwal as $j) {
				$jadwalid = $j->jadwal_id;
				$mkid = $j->mkditawarkan_id;
			}
		}
		
		if($_POST['select-materi']!='0'){
			$materiid	= $mquiz->get_materiid_md5($_POST['select-materi']);
		}else{
			$materiid	= null;
		}
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$testid	= $_POST['hidId'];
			$action = "update";
		}else{
			$testid	= $mquiz->get_test_id();
			$action = "insert";
		}
		
		
		$datatest 	= Array(
						'test_id'=>$testid,
						'jadwal_id'=>$jadwalid, 
						'materi_id'=>$materiid, 
						'mkditawarkan_id'=>$mkid,
						'judul'=>$title,
						'tgl_mulai'=>$start_date,
						'tgl_selesai'=>$finish_date,    
						'instruksi'=>$instruction, 
						'keterangan'=>$keterangan, 
						'is_random'=>$soal_is_random,
						'is_publish'=>$is_publish,
						'time_limit' => $timelimit,
						'user_id'=>$userid, 
						'last_update'=>$lastupdate
						);
		
		if( $mquiz->replace_test($datatest)==TRUE ){
			
			if($action == "insert"):
			  $post_id = $mquiz->get_post_id();
			  $datapost 	= Array(
						'post_id'=>$post_id,
						'kategori'=>'quiz', 
						'link_id'=>$testid, 
						'user_id'=>$userid, 
						'isi'=>$title.' | '.$instruction.' | '.$finish_date,
						'last_update'=>$lastupdate
						);
			  $mquiz->replace_post($datapost);
			  
			  /*-- post nilai --*/
				$post_nilai_id = "QU".$testid;
				
				$nilai 		= Array('post_nilai_id'=>$post_nilai_id, 
									'test_id'=>$testid, 					
									'jadwal_id'=>$jadwal_id, 
									'mkditawarkan_id'=>$mkid,
									'judul'=>$title,
									'kategori_nilai'=>'quiz',
									'tgl_mulai'=>$start_date,
									'tgl_selesai'=>$finish_date,   
									'user_id'=>$user,
									'last_update'=>$lastupdate
								   );
				$m_general->replace_post_nilai($nilai);
				/*-- end -- */
					
			
			  $datapostshare 	= Array(
						'id_user'=>$mkid,
						'id_post'=>$post_id, 
						'init_share'=>'quiz'
						);
			  $mquiz->replace_post_share($datapostshare);
			endif;
			
			$this->saveSoalToDb($testid,$jadwalid,$materiid,$mkid,$userid,$lastupdate);
		}else {
			echo "Upload Test Failed";
			exit;
		}
		
	}
	
	function saveSoalToDb($testid,$jadwalid,$materiid,$mkid,$userid,$lastupdate){
		$mquiz		 = new model_quiz();
		$answertype  = $_POST['select-test'];
		$point		 = $_POST['point'];
		$question	 = $_POST['question'];
		$countanswer = $_POST['count-answer'];
		
		  $array=0;
		  $arr=0;
		  for ($i=0; $i < count($answertype); $i++) {
		  	
			if(isset($_POST['hidIdSoal'][$i])&&$_POST['hidIdSoal'][$i]!=""){
				$action 			= "update";
			}else{
				$action 			= "insert";			
			}
			
		  	if($action=='insert'){
		  	  $soal_id	= $mquiz->get_soal_id();
			}else{
			  $soal_id	= $_POST['hidIdSoal'][$i];
			}
			
		    $kategori    = $mquiz->get_kategori($answertype[$i]);
		    foreach ($kategori as $k) {
			  $kategori_id	= $k->hid_id;
			  $jenis_input  = $k->jenis_input;
		    }
			
			$datasoal 	= Array(
					'soal_id'=>$soal_id, 
					'test_id'=>$testid, 
					'jadwal_id'=>$jadwalid, 
					'materi_id'=>$materiid, 
					'mkditawarkan_id'=>$mkid, 
					'kategori_id'=>$kategori_id, 
					'pertanyaan'=>$question[$i],
					'poin' => $point[$i], 
					'user_id'=> $userid, 
					'last_update'=>$lastupdate
				);
			if( $mquiz->replace_soal($datasoal)==TRUE ){
				
			  if($action=='insert'){
				$namamkid = $mquiz->get_namamkid($mkid);
				$databanksoal 	= Array(
						  'soal_id'=>$soal_id, 
						  'jadwal_id'=>$jadwalid, 
						  'materi_id'=>$materiid, 
						  'mkditawarkan_id'=>$mkid,
						  'namamk_id'=>$namamkid, 
						  'kategori_id'=>$kategori_id, 
						  'pertanyaan'=>$question[$i], 
						  'user_id'=> $userid, 
						  'last_update'=>$lastupdate
						  );
				$mquiz->replace_bank_soal($databanksoal);
			  }
				
				$arr = $this->savejawabanToDb($soal_id,$action,$jenis_input,$countanswer[$i],$i,($arr+$array));
			}else {
				echo "Upload Question Failed";
				exit;
			} 
		  }
	}
	
	function savejawabanToDb($soal_id,$action,$jenis_input,$countanswer,$i,$array){
		$mquiz		= new model_quiz();
		$answer		= $_POST['answer-q'.($i+1)];
		
		if($jenis_input!='textarea'){
		  if($_POST['skor-q'.($i+1)]&&isset($_POST['skor-q'.($i+1)])){
			 $skor		= $_POST['skor-q'.($i+1)];
		  }else $skor = '';
		
		  if($_POST['isbenar-q'.($i+1)]&&isset($_POST['isbenar-q'.($i+1)])){
		 	 $isbenar	= $_POST['isbenar-q'.($i+1)];
		  }else $isbenar = '';
		
		  if(isset($_POST['random-answer-q'.($i+1)])){
			 $is_random = 1;
		  }else $is_random = 0;
		}
		
		  for ($x=0; $x < $countanswer; $x++) {
			
			if($action=='insert'){
		  	  $jawab_id	= $mquiz->get_jawabid("", $soal_id);
			}else{
			  $jawab_id	= $_POST['hidIdJawab'][$array];
			}
			
			if($jenis_input!='textarea'){
			  $datajawaban 	= Array(
					'jawaban_id'=>$jawab_id, 
					'soal_id'=>$soal_id, 
					'keterangan'=>$answer[$x], 
					'is_benar'=>$isbenar[$x], 
					'skor'=>$skor[$x], 
					'urut'=>($x+1),
					'is_random'=>$is_random
			  );
			}else{
				$datajawaban 	= Array(
					'jawaban_id'=>$jawab_id, 
					'soal_id'=>$soal_id, 
					'keterangan'=>$answer[$x], 
					'is_benar'=>'0', 
					'skor'=>'0', 
					'urut'=>($x+1),
					'is_random'=>'0'
			  );
			}
			// echo "<br>".var_dump($datajawaban)."<br>";
			if( $mquiz->replace_jawaban($datajawaban)==TRUE ){
				echo "Upload Succes";
			  if($action=='insert'){
				$databankjawab  	= Array(
								    'jawaban_id'=>$jawab_id, 
									'soal_id'=>$soal_id, 
									'keterangan'=>$answer[$x], 
									'is_benar'=>$isbenar[$x], 
									'skor'=>$skor[$x],
									'urut'=>($x+1)
									);
				$mquiz->replace_bank_jawab($databankjawab);
			  }
			}else {
				echo "Upload answer Failed";
				exit;
			} 
			$array += 1;
		  }
		  return $array;
	}
	//---------------------------------------------------------------------------------
	
	function get_quiz(){
		$mquiz	= new model_quiz();
		$staff  = $this->coms->authenticatedUser->staffid;
		$quiz = $mquiz->read('',$staff);
		if($quiz){
			$return_arr = array();
		
			foreach($quiz as $row){
				foreach ($row as $key => $value) {
					 $arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			echo json_encode($return_arr);
		}else{
			echo json_encode("");
		}
	}
	
	function get_jadwal(){
		$mquiz	= new model_quiz();
		$staff  = $this->coms->authenticatedUser->staffid;
		$type	= $_POST['type'];
		$jadwal	= $mquiz->get_jadwal('',$staff, $type);
		
		if(isset($_POST['jadwalid'])){
			$jadwalid = $_POST['jadwalid'];
		}
		
		if(isset($_POST['mkid'])){
			$mkid = $_POST['mkid'];
		}
		
		echo "<option value='0'>No schedule</option>";		
		foreach($jadwal as $j):
			echo "<option value= ";
			if($type=='exam-type'){
				echo " '".$j->mkid."' ";
			}else{
				echo " '".$j->jadwal_id."' ";
			}
			echo " data-mk='".$j->mkid."' ";
			
			if($type=='exam-type'){
				if(isset($mkid)&&$mkid == $j->mkid){
					echo "selected";
				}
			}else{
				if(isset($jadwalid)&&$jadwalid == $j->jadwal_id){
					echo "selected";
				}
			}
			
			
			echo ">".$j->jadwal;
			if($type!='exam-type'){
				echo "&nbsp;[".$j->prodi_id."]";
			}
			echo "</option>";
		endforeach;
		
	}
	
	function get_materi(){
		$mquiz	= new model_quiz();
		$mkid 	= $_POST['mk_id'];
		$materi = $mquiz->get_materi('md5',$mkid);
		if($materi){
			$return_arr = array();
		
			foreach($materi as $row){
				foreach ($row as $key => $value) {
					 $arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			echo json_encode($return_arr);
		}else{
			echo json_encode("");
		}
	}
	
	function get_test_type(){
		$mquiz	= new model_quiz();
		
		$categoryList = $mquiz->get_kategori();
		
		if(isset($_POST['kathid_id'])){
			$categoryid = $_POST['kathid_id'];
		}
		
		foreach($categoryList as $c):
			echo "<option value='".$c->kat_id."' data-input='".$c->jenis_input."' ";
			if(isset($categoryid)&&$categoryid == $c->kat_id){
				echo "selected";
			}
			echo ">".$c->kategori."</option>";
		endforeach;
	}
	
	function get_bank_soal(){
		$mquiz	= new model_quiz();
		$mkid 	= $_POST['mkid'];
		$bank   = $mquiz->get_bank_soal($mkid);
		if($bank){
			$return_arr = array();
		
			foreach($bank as $row){
				foreach ($row as $key => $value) {
					 $arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			echo json_encode($return_arr);
		}else{
			echo json_encode("");
		}
	}
	
	function get_bank_jawab(){
		$mquiz	= new model_quiz();
		$soalid 	= $_POST['soalid'];
		$bank   = $mquiz->get_bank_jawab($soalid);
		if($bank){
			$return_arr = array();
		
			foreach($bank as $row){
				foreach ($row as $key => $value) {
					 $arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			echo json_encode($return_arr);
		}else{
			echo json_encode("");
		}
	}
	
}
?>