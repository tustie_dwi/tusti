<?php
class elearning_answer extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($quiz_id, $mhs_id=NULL){
		$data['user']	 = $this->coms->authenticatedUser->name;
		$data['role'] 	 = $this->coms->authenticatedUser->role;
		$data['user_id'] = $this->coms->authenticatedUser->id;
		//$data['foto']    = $this->coms->authenticatedUser->foto;
		
		$this->add_script('js/answer/answer.js');
		$this->add_script('js/all.js');
		$this->add_style('bootstrap/css/add.css');
		
		$m_answer = new model_answer();
		$data['test'] = $m_answer->get_test($quiz_id);
		$data['soal'] = $m_answer->get_soal($quiz_id);
		$data['jawaban'] = $m_answer->get_jawaban($quiz_id);
		$data['quiz_id'] = $quiz_id;
		
		$m_general		= new model_general();
		
		$key 			= $m_general->get_user_profile($data['user_id']);
		
		$data['foto'] 	= $key->foto;
		
		$data['notifikasi'] 	= $m_general->get_notifikasi_new($data['user_id']);
		$data['jml_notifikasi'] = $m_general->get_jml_notifikasi($data['user_id']);
		
		if($data['role'] == 'mahasiswa'){
			$data['hasil'] = $m_answer->cek_test($quiz_id, $this->coms->authenticatedUser->mhsid);
			if(isset($data['hasil'])) $this->view('answer/result.php', $data);
			else $this->view('answer/index.php', $data);
		}
		else{
			$staff   = $this->coms->authenticatedUser->staffid;
			$mkd = $m_answer->get_mkd_dosen($staff, $data['test']->mkditawarkan_id);
			$data['jawaban_mhs'] = $m_answer->get_jawaban_mhs($mhs_id, $quiz_id);
			
			if(isset($data['jawaban_mhs']->hasil_id)){
				$hasil_id = $data['jawaban_mhs']->hasil_id;
				$data['detail_jawaban'] = $m_answer->get_hasil($hasil_id);
			}
			
			if(empty($data['test']->jadwal_id)) $data['mhs'] = $m_answer->get_mhs_by_test($quiz_id, $mkd);
			else $data['mhs'] = $m_answer->get_mhs_by_jadwal($quiz_id);
			$this->view('answer/mark.php', $data);
		}
	}

	function convert_date($date){
		$date = explode('-', $date);
		return substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}
	
	function save(){
		$m_answer = new model_answer();
		$m_general	= new model_general();
		$hasil_id = $m_answer->get_hasil_id();
		
		$name = $this->coms->authenticatedUser->name;
		$mhs_id = $this->coms->authenticatedUser->mhsid;
		$id = $this->coms->authenticatedUser->id;
		
		$walk = 0; $max_post = 0; $total_skor = 0;
		foreach ($_POST['jwb'] as $key) {
			if(! empty($key)){
				$x = explode('|', $key);
				if($walk != $x[0]) { $max_post++; }
				$walk = $x[0];
				
				$skor = $m_answer->get_skor($x[1]);
				$total_skor += $skor;
				$data = array(
					'detail_id' => $m_answer->get_detail_id(),
					'jawaban_id' => $x[1],
					'hasil_id' => $hasil_id,
					'soal_id' => $x[0],
					'inf_jawab' => $x[3],
					'inf_skor' => $skor
				);
				
				$m_answer->save_answer($data);
			}
		}
		
		$data = array(
			'hasil_id' => $hasil_id,
			'mahasiswa_id' => $mhs_id,
			'test_id' => $_POST['xvsvd'],
			'tgl_test' => date('Y-m-d'),
			'jam_mulai' => $_POST['rtgrh'],
			'jam_selesai' => date('H:i:s'),
			'max_post' => $max_post,
			'total_skor' => $total_skor,
			'user_id' => $id,
			'last_update' => date('Y-m-d H:i:s')
		);
		$m_answer->save($data); 
		
		/* save skor to post nilai */
			$post_nilai_id = 'QU'.$_POST['xvsvd'];
			$nilai_id = 'QU'.$hasil_id;
			
			$data_nilai = array(
				'nilai_id' => $nilai_id,
				'mahasiswa_id' => $mhs_id,
				'post_nilai_id' => $post_nilai_id,
				'link_id' =>$hasil_id,
				'tbl_name'=>'tbl_test_hasil',
				'tgl_post' => date('Y-m-d H:i:s'),			
				'skor' => $total_skor,
				'status'=>'Turn In',
				'user_id' => $id,
				'last_update' => date('Y-m-d H:i:s')
			);
			
			$m_general->save_post_nilai_mhs($data_nilai); 
		/* end */
		
		$test_id = substr(md5($_POST['xvsvd']),8,7);
		$mahasiswa_id = substr(md5($mhs_id),8,7);
		$url = $this->location('module/elearning/answer/index/'.$test_id.'/'.$mahasiswa_id);
		
		$tujuan = $m_answer->get_quiz_owner($_POST['xvsvd']);
		$data = array(
			'notifikasi_id' => $m_general->get_notifikasi_id(),
			'kategori' => 'submission',
			'post_id' => '',
			'link_id' => $url,
			'user_from_name' => $name,
			'user_from' => $id,
			'user_to_name' => $tujuan->nama,
			'user_to' => $tujuan->id,
			'keterangan' => 'send quiz submission',
			'tgl_notifikasi' => date('Y-m-d H:i:s')
		);
		$m_general->add_notif($data);
		
		$this->redirect('module/elearning/answer/index/'.$test_id);
	}
	
	function update_skor(){
		$m_answer = new model_answer();
		$m_general	= new model_general();
		
		$name = $this->coms->authenticatedUser->name;
		$id = $this->coms->authenticatedUser->id;
		
		$m_answer->update_skor();
		
		//add notifikasi
		$hasil_id = explode('|', $_POST['xvsvd']);
		
		$url = $this->location('module/elearning/answer/index/'.$hasil_id[1]);
		
		$tujuan = $m_answer->get_submission_owner($hasil_id[0]);
		$data = array(
			'notifikasi_id' => $m_general->get_notifikasi_id(),
			'kategori' => 'mark',
			'post_id' => '',
			'link_id' => $url,
			'user_from_name' => $name,
			'user_from' => $id,
			'user_to_name' => $tujuan->nama,
			'user_to' => $tujuan->id,
			'keterangan' => 'has marked your submission',
			'tgl_notifikasi' => date('Y-m-d H:i:s')
		);
		$m_general->add_notif($data);
		
		$this->redirect('module/elearning/answer/index/'.$hasil_id[1].'/'.$hasil_id[2]);
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
?>