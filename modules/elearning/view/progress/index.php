<?php echo $this->view('header.php',$data) ?>

<!-- Begin Body -->
<input id="init_enter" value="0" class="hidden" />
<div class="container">
	<div class="row">
		<div class="box box-white full-radius" style="height: auto">
			<h3 class="no-margin">
				<i class="fa fa-tasks"></i> Student's Progress /
				<small><?php if($mkd_name) echo $mkd_name->keterangan ?></small>
				
				<?php if(($role == 'dosen')&&($mhs_progress)){ ?>
					<button data-toggle="modal" data-target="#addGrade" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Add Grade</button>&nbsp;
					<button id="btn-remove" onclick="remove_btn()" style="margin-right: 5px" class="btn btn-danger btn-sm pull-right"><i class="fa fa-trash-o"></i> Remove Grade</button>&nbsp;
				<?php } ?>
			</h3>
			<?php if(($role == 'dosen') && (! $khs) && ($mhs_progress)){ ?>
			<button class="btn btn-danger btn-sm pull-right" onclick="add_final_grade('<?php if($mkd_name) echo $mkd_name->mkditawarkan_id ?>')"><i class="fa fa-plus"></i> Add Final Grade (KHS)</button>
			
			<form id="form-jadwal" method="post" action="<?php echo $this->location('module/elearning/progress/views/'.$mkid) ?>">
					<select class="e9 form-control" name="jadwal" required="required" id="select_jadwal">
						<option>Pilih Kelas</option>
						<?php
							if($list_jadwal) :
								foreach($list_jadwal as $key) :
									if($jadwal == $key->jadwal_id) $sel = "selected";
									else $sel = '';
									echo "<option ".$sel." value='".$key->jadwal_id."'>".$key->namamk.' - '.$key->kelas.' ['.$key->prodi.']'."</option>";
								endforeach;
							endif;
						?>
					</select>
				</form>
			<?php } 
			
			if($list_jadwal && (count($list_jadwal) > 0)):
			?>
			<hr>
			<div class="row">
				<div class="col-md-6" style="margin-bottom:20px;">	
					<form id="form-jadwal" method="post" action="<?php echo $this->location('module/elearning/progress/views/'.$mkid) ?>">
						<select class="e9" name="jadwal" required="required" id="select_jadwal">
							<option>Select Group</option>
							<?php
								
								foreach($list_jadwal as $key) :
									if ($key->jadwal_id):
										if($jadwal == $key->jadwal_id) $sel = "selected";
										else $sel = '';
										echo "<option ".$sel." value='".$key->jadwal_id."'>".$key->namamk.' - '.$key->kelas.' ['.$key->prodi.']'."</option>";
									endif;
								endforeach;
								
							?>
						</select>
					</form>
				</div>
			</div>
			<?php
			endif;
			if($mhs_progress) :?>
			
			<table class="table table-condensed table-hover table-striped table-bordered">
				<thead class="text-center bolder">
					<tr>
						<?php if($tugas){
							echo "<td width='25%'>Student</td><td>Total</td>";
							$colspan = 2;
							if((! $khs)) $jml = count($tugas);
							else $jml = (count($tugas) - 1) ;
							
							foreach($tugas as $key) { $colspan++; ?>
							<td <?php  if($key->kategori_nilai == 'khs') echo 'class="warning"'; ?>>
								
								<?php if($key->kategori_nilai == 'addition') : ?>
									<label>
										<input style="display: none" name="komponen[]" onclick="remove_component(this.value)" value="<?php echo $key->post_nilai_id ?>" type="radio" value=""> 
									</label>
								<?php endif ?>
								
								<span class="btn-tooltip" data-placement="top" title="<?php echo ucfirst($key->kategori_nilai) .  '<br>' . $key->tgl_mulai . '<br>' . $key->tgl_selesai ?>">								
									<span class="tugasTitle-<?php echo $key->post_nilai_id ?>"><?php echo ucfirst($key->judul) ?></span>
								</span>
							</td>
						<?php }}else{
						$colspan=2;
						}?>
					</tr>
				</thead>
				<tbody>
					<?php 
						$mhs_tmp = $nama = '';
						$nilai = array();
						
						if($mhs_progress){							
							
							foreach($mhs_progress as $key) {
								
								//if($mhs_tmp != ''){
									$mhs[$key->mahasiswa_id]['nama'] = $key->nama;
									$mhs[$key->mahasiswa_id]['mahasiswa_id'] = $key->mahasiswa_id;
									$mhs[$key->mahasiswa_id]['foto'] = $key->foto;
									$mhs[$key->mahasiswa_id]['mk'] = $mkid;
								//}
								
								$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['judul'] = $key->judul;
								$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['skor'] = $key->skor . '/' . $key->total_skor;
								if($key->skor)	$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['skor_nilai'] = ($key->skor / $key->total_skor);
								else $mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['skor_nilai'] = 0;
								$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['status'] = $key->status;
								$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['tgl'] = $key->tgl_pengumpulan;
								$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['kategori'] = $key->kategori_nilai;
								$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['kategori_nilai'] = $key->kategori;
								$mhs[$key->mahasiswa_id]['nilai'][$key->post_nilai_id]['link_id'] = $key->link_id;

								$mhs_tmp = $key->mahasiswa_id;		
							}						
							
							print_nilai($mhs, $role, $this->config->file_url_view.'/', $jml);
						}
					?>
				</thead>
			</tbody>
			</table>
			<?php
			else:
				echo "<br><div class='well'>Sorry, no content to show.</div>";
			endif;
			?>
		</div>
	</div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	function print_nilai($nilai, $role, $url, $i){
		$mconf = new model_progress();
		
		foreach($nilai as $key => $value){
			if(empty($value['foto'])) $foto = $this->config->default_pic;
			else $foto = $url. $value['foto'];
			
			
			$persen = $mconf->get_persen_progress($value['mahasiswa_id'], $value['mk'])->total;
			
			echo "<tr>";
				
				echo "<td>
						<img class='img-ava-sm' src='$foto' width='30' height='30'>
						<span class='btn-tooltip mhsName-".$value['mahasiswa_id']."' data-placement='right' title='NIM : ".$value['mahasiswa_id']."'>".$value['nama']."</span>
					  </td>";
				echo "<td align='center'>".number_format((($persen/$i)*100),0) ." %</td>";
				
				$nilai_khs =0;
				
				foreach($value['nilai'] as $key_nilai => $val_nilai){
					$cell_id = $value['mahasiswa_id'] . $key_nilai;
					$mhs_id  = $value['mahasiswa_id'];
					$kategori_nilai  = $val_nilai['kategori_nilai'];
					$kategori = $val_nilai['kategori'];
					$tugas_id = $val_nilai['link_id'];
					
					if($val_nilai['skor_nilai']):
						$nilai_k = $val_nilai['skor_nilai'];
					else:
						$nilai_k = 0;
					endif;
					
					$nilai_khs = $nilai_khs + ($nilai_k * 100);
					
					
					if($kategori_nilai=='khs'):
						$class = "warning";
						if($val_nilai['skor'] == '/') $val_nilai['skor'] = "<span class='text text-danger'>".(number_format(($nilai_khs)/$i, 2))."/100 </span>";
						else $val_nilai['skor'] = '<span class="btn-tooltip" data-placement="left" title="'.$val_nilai['kategori_nilai'] . '<br>' . $val_nilai['tgl'].'"><span class="text text-danger">'.$val_nilai['skor'].'</span></span>';
					else:
						$class= "";
						if($val_nilai['skor'] == '/') $val_nilai['skor'] = '<button class="btn btn-info btn-xs">Mark</button>';
						else $val_nilai['skor'] = '<span class="btn-tooltip" data-placement="left" title="'.$val_nilai['status'] . '<br>' . $val_nilai['tgl'].'">'.$val_nilai['skor'].'</span>';
					endif;
					
					echo "<td class='text-center ".$class."'><span ";
					
					if($role == 'dosen') echo "onclick=\"new_mark('$mhs_id','$key_nilai','$kategori','$tugas_id','$kategori_nilai')\"";
					echo " class='cell-data-$cell_id'>".$val_nilai['skor']."</span></td>";
				}
				
				
			echo "</tr>";
		}
	}
	
	// echo "<pre>".print_r($mhs, true)."</pre>";
?>

<?php 
	echo $this->view('footer.php');	
?>

<?php if($role == 'dosen') { ?>
	<div class="modal fade" id="addGrade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Add Grade</h4>
	      </div>
	      <form role="form" action="<?php echo $this->location('module/elearning/progress/add') ?>" method="post">
		      <div class="modal-body">
				  <div class="form-group">
				    <label>Grade's Name</label>
				    <input name="mkd" class="form-control" value="<?php echo $mkd_name->mkditawarkan_id ?>" type="hidden">
				    <input name="judul" type="text" class="form-control" placeholder="Enter name">
				  </div>
				 
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Save changes</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
	
	<div class="modal fade" id="newMark" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" onkeypress="return runScript(event)">
	  <div class="modal-dialog" style="width: 500px">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel"><?php echo $mkd_name->keterangan ?> - <span class="tugas-wrap"></span></h4>
	      </div>
	      <form role="form" id="formNewMark" method="post">
		      <div class="modal-body">
				  <div class="form-group">
				    <label class="mhs-wrap"></label>
				    <input id="mhs_id" name="mhs_id" class="form-control hidden">
				    <input id="tugas_id" name="tugas_id" class="form-control hidden">
				    <input id="link_id" name="link_id" class="form-control hidden">
				    <input id="kategori" name="kategori" class="form-control hidden">
					 <input id="kategori_nilai" name="kategori_nilai" class="form-control hidden">
				    
				    <div class="row">
					    <div class="col-md-6">
					    	<input id="skor" autofocus="on" name="skor" type="text" class="form-control" placeholder="Skor" autocomplete="off">
					    </div>
					    <div class="col-md-6">
					    	<input id="total_skor" name="total_skor" type="text" class="form-control" placeholder="Max Skor" autocomplete="off">
					    </div>
				    </div>
				  </div>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
<?php } ?>



