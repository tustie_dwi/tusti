<?php echo $this->view('header.php',$data) ?>
<div class="box box-white" style="height: auto" id="view_content">	        		
	<div class="head-box">
		<div class="row"  style="margin-left: 5px;">
			<h3>Folders</h3>
			<div class="pull-right">
				<a title="create new folder" class='btn btn-primary btn-edit-post btn-add' data-toggle="modal" data-target="#newfolder"  onclick="return newfolder_parent('0','<?php echo $mkd->mkditawarkan_id ?>')" href="#"><i class="fa fa-plus"></i> Add Folder</a>
			</div>
		</div>
		<div class="action-group" style="margin-left: 5px;">
			<ul class="list-group content-li">
				<?php 
				$mconf = new model_general(); 
										
				$folder = $mconf->get_folder($mkd->hidId);
				if($folder):
					foreach($folder as $dt):
					?>
						<li class="list-group-item">
							<div class="row">							
								<div class="col-md-8">								
									<span style="cursor: pointer" ondblclick="view_content('<?php echo $dt->id ?>','<?php echo $dt->folder_name; ?>')"> <i class="fa fa-folder-o"></i> <?php echo $dt->folder_name; ?></span>
								</div>
								<div class="col-md-2"><a href="#">Disable</a></div>
								<div class="col-md-2"><a href="#">Remove</a></div>
							</div>
						</li>
					<?php
					endforeach;	
				else: echo '<li class="list-group-item">Sorry, there is no content to show.</li>';
				endif;				
				?>
			 </ul>
		</div>
	</div>
	
</div>
	

<!-- Modal Folder-->
<div class="modal fade" id="newfolder" tabindex="-1" role="dialog" aria-labelledby="newfolderLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_btn" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="newfolderLabel">Edit/Create Folder</h4>
      </div>
      <form id="newfolder_form">
	      <div class="modal-body">
        	  <div class="form-group">
	        	<label for="folder_name">Folder Name</label>
	        	<input type="text" name="folder_name" autocomplete="off" class="form-control" id="folder_name"/>
	        	<input type="hidden" name="parent" class="form-control" id="parent" value="0"/>
        	  </div>
	      </div>
	      <div class="modal-footer">
			<input type="hidden" name="id" class="form-control" id="id"/>
	        <button type="button" class="btn btn-default close_btn" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
       </form>
    </div>
  </div>
</div>

<!-- Modal File-->
<div class="modal fade" id="newfile" tabindex="-1" role="dialog" aria-labelledby="newfolderLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_btn_file_upload" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="newfolderLabel">Upload New File</h4>
      </div>
      <form id="newfile_form" enctype="multipart/form-data">
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="col-sm-12">
	      			<button type="button" id="add_file_form"><i class="fa fa-plus"></i></button>
      			</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-sm-6">
		        	<div class="form-group">			        
			        	<input type="text" name="title[]" required="required" autocomplete="off" class="form-control" id="title" value="-" />
		        	</div>
	        	</div>
	        	<div class="col-sm-6">
		        	<div class="form-group">
			        	<input type="file" name="uploads[]" required="required" autocomplete="off" class="span12" id="uploads"/>
		        	</div>
	        	</div>	
			</div>     
	        	<div id="newform">
	        		
	        	</div>
        	   	
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="count_new_file" class="form-control" id="count_new_file" value="1"/>
	      	<input type="hidden" name="folder" class="form-control" id="folder" value="0"/>
			<input type="hidden" name="folder_loc" class="form-control" id="folder_loc"/>
	        <button type="button" class="btn btn-default close_btn_file_upload" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
       </form>
    </div>
  </div>
<?php echo $this->view('footer.php',$data) ?>