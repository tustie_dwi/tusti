<div class="head-box">

		
		
<div class="row" style="margin-left: 5px;">
<?php 
	 if( isset($detail) ) {	
		?>		
			<div class="col-md-12">
				<div class="col-md-6"><h3><?php echo $detail->file_name; ?></h3></div>
				<div class="col-md-6">
				<a onclick="doDownload('<?php echo $detail->file_id ?>')" href="<?php echo $this->location('module/elearning/folder/download/'.$detail->file_id); ?>" class="btn btn-default pull-right">
				<i class="fa fa-download"></i> Download This File</a></div>							
			</div>
			
			<div class="col-md-12">			
					
				<?php switch ($detail->jenis_file) {
					case 'video':
						echo '<video width="480" style="border: 1px solid #444; background : #333" height="360" id="video" preload="none" title="'.$detail->judul.'" controls>
									<source src="'.$this->config->file_url_view.'/'.$detail->file_loc.'" type="video/webm">
									<source src="'.$this->config->file_url_view.'/'.$detail->file_loc.'" type="video/mp4">
									<source src="'.$this->config->file_url_view.'/'.$detail->file_loc.'" type="video/ogg">
								</video>';
						break;
					
					case 'image':
						echo '<img src="'.$this->config->file_url_view.'/'.$detail->file_loc.'"  class="thumbnail"/>';
						break;
						
					case 'presentation' || 'document' || 'spreadsheet':
						echo '<iframe src="//docs.google.com/viewer?url='.$this->asset($detail->file_loc).'&embedded=true" width="550"
								height="780"
								style="border: none;"></iframe>';
						break;
				} ?>								
						
			</div>	
	
		
				
		<?php
		
	 }
	 else{ 
	 ?>		
		<div class="well">Sorry, no content to show</div>
    <?php } ?>
	</div>
</div>