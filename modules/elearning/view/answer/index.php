<?php echo $this->view('header.php', $data); ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="form form-quiz">
					<div class="row">
					<div class="col-md-8 content-quiz">
						<div class="header-quiz">
							<div class="row">
								<div class="col-md-7">
									<!-- <input type="text" class="form-control" disabled value="<?php echo $test->judul ?>"/> -->
									<h2 class="no-margin"><i class="fa fa-sliders"></i> <?php echo $test->judul ?></h2>
								</div>
								<div class="col-md-5">
									<h2 class="no-margin pull-right"><i class="fa fa-clock-o"></i> <?php echo $test->time_limit . 'minutes' ?></h2>
								</div>
							</div>
						</div>
						<div class="main-quiz">
							<div class="row">
								<div class="col-md-2 main-quiz-menu"> <!-- wrap left -->
									<div class="title-menu-question">Questions</div>
									<ul class="menu-question-number">
										<?php $no=1; foreach($soal as $key) { ?>
											<li <?php if($no == 1) echo 'class="active"'; ?> id="soal-page-<?php echo $no; ?>"><a href="#" onclick="change_question('<?php echo $no; ?>')"><?php echo $no; ?></a></li>
										<?php $no++; } ?>
									</ul>
									
								</div>
								<div class="col-md-10 main-quiz-question"> <!-- wrap middle/list soal -->
									
									<form action="<?php echo $this->location('module/elearning/answer/save') ?>" method="POST" id="form-answer">
										<input type="hidden" name="xvsvd" value="<?php echo $test->test_id ?>" />
										<input type="hidden" name="rtgrh" value="<?php echo date('H:i:s') ?>" />
										<?php $no=1; foreach($soal as $key) { ?>
											<span class="page-<?php echo $no ?>">
											<div class="question-area" style="margin-top: 10px">
												<label>Question <?php echo $no ?></label><br>
												<?php echo $key->pertanyaan ?>
											</div>
											
											<div class="answer-area">
												<label>Response</label>
												<?php 
													if($key->keterangan != 'essay') cetak_jawaban($jawaban, $key->soal_id, $key->keterangan); 
													else echo '<textarea data-jwb="'.$key->soal_id.'" class="form-control jwb"></textarea>';
												?>
												<input type="hidden" inp="<?php echo $key->soal_id ?>" name="jwb[]" />
											</div>
											</span>
										<?php $no++; } ?>
									</form>
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="col-md-4 sidebar-quiz"> <!-- wrap right -->
						<div class="quiz-assign">
							<button onclick="form_submit()" class="btn btn-info btn-block">Assign <i class="fa fa-check-square-o"></i></button>
						</div>
						<p class="quiz-divider"></p>
						<div class="about-quiz">
							<label>Instruction</label>
							<p><?php echo $test->keterangan ?></p>
						</div>
						
						<!-- <p class="quiz-divider"></p> -->
						<!-- <div class="about-quiz">
							<label>Quiz Option</label>
							<div class="checkbox">
							  <label>
							    <input type="checkbox" disabled <?php if($test->is_random == '1') echo "checked" ?>>
							    Random Question
							  </label>
							</div>
						</div> -->
					</div>
					</div>
				</div> <!-- end of form_quiz -->
			</div>
		</div>
	</div>
</div>

<?php 
	echo $this->view('footer.php', $data); 
	function cetak_jawaban($jawaban, $soal_id, $tipe){
		foreach($jawaban as $key) {
			if($soal_id == $key->soal_id) {
				if($tipe == 'multiple choice') {echo '<div class="radio">'; $tipe_ = 1; }
				else {echo '<div class="checkbox">'; $tipe_ = 2;}
				echo '<label>';
				$val = $soal_id.'|'.$key->jawaban_id.'|'. $tipe_.'|'.$key->keterangan;
			  	if($tipe == 'multiple choice') echo '<input name="jwb[]" type="radio" value="'.$val.'">';
				else echo '<input name="jwb[]" type="checkbox" value="'.$val.'">';
			    echo $key->keterangan;
				echo '</label>';
				echo '</div>';
			} 
		} 
	}
?>
