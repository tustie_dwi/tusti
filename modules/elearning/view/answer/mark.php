<?php echo $this->view('header.php', $data); ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
	        	<div class="box box-white" style="height: auto">
	        		
	        		<div class="head-box">
	        			<div class="row">
	        				<div class="col-sm-2"><i class="fa fa-3x fa-file-zip-o"></i></div>
	        				<div class="col-sm-10">
	        					<div class="" id="nick-wrap" style="margin-left: 10px; margin-top: 2px">
									<a href="<?php echo $this->location('module/elearning/answer/index/'.$quiz_id) ?>" class="text-left"><?php echo $test->judul ?></a>
									<span style="color: #9290B0"><br>Due : <?php echo $test->tgl_selesai ?></span>
								</div>
	        				</div>
	        			</div>
						<div class="action-group">
							<ul class="list-group content-li">
								<?php foreach($mhs as $key) { ?>
								<li class="list-group-item">
									<a href="<?php echo $this->location('module/elearning/answer/index/'.$quiz_id.'/'.$key->mhs_id) ?>">
										<!-- <span class="fa fa-square blue"></span> -->
										<?php
											if(empty($key->foto)) $foto = $this->config->default_pic;
											else $foto = $this->config->file_url_view.'/'.$key->foto;
										?>
										<img width="30" height="30" src="<?php echo $foto ?>"> 
										<?php echo $key->nama ?>
										<span class="pull-right">
											<?php 
												if($key->is_mark == 1) echo '<i style="margin: 0" class="green fa fa-check-square btn-tooltip" title="Marked" data-placement="top"></i>';
												else echo '<i style="margin: 0" class="red fa fa-check-square btn-tooltip" title="Unmarked" data-placement="top"></i>';
												
												echo " ";
												
												if(! empty($key->hasil_id)) echo '<i style="margin: 0" class="green fa fa-circle-o btn-tooltip" title="Submitted" data-placement="top"></i>';
												else echo '<i style="margin: 0" class="red fa fa-circle-o btn-tooltip" title="Unsubmitted" data-placement="top"></i>';
											?>
										</span>
									</a>
								</li>
							 	<?php } ?>
							 </ul>
						</div>
	          		</div>
	        	</div>
	      	</div>
	      			 	
		</div> <!-- end of main row -->
  		</div>
  		
  		<?php 
  			if(!empty($jawaban_mhs)) $this->view('answer/jawaban.php', $data); 
			else $this->view('answer/detail_quiz.php', $data); 
  		?>
  		
	</div>
</div>

<?php 
	echo $this->view('footer.php', $data); 
?>
