<?php echo $this->view('header.php', $data); ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-white" style="height: auto; margin-top: 50px">
				<h3 class="page-header"><i class="fa fa-question-circle"></i> <?php echo $test->judul ?>
					<?php 
						if($hasil->is_mark == 1) echo '<span class="label label-success pull-right"><i class="fa fa-check"></i> Marked</span>';
						else echo '<span class="label label-warning pull-right"><i class="fa fa-clock-o"></i> Unmarked</span>';
					?>
				</h3>
				<p><i class="fa fa-calendar"></i> Date start : <?php echo $hasil->tgl_test ?> | <i class="fa fa-clock-o"></i> Time start : <?php echo $hasil->jam_mulai ?> | <i class="fa fa-bell"></i> Time finished : <?php echo $hasil->jam_selesai ?></p>
				<p>Question answered : <?php echo $hasil->max_post ?></p>
				
				<h3><i class="fa fa-check"></i> Your score is <?php echo $hasil->total_skor ?>
					<a class="pull-right btn-tooltip" data-placement="bottom" title="Back" href="<?php echo $this->location() ?>">
						<i class="fa fa-reply-all"></i>
					</a>
				</h3>
			</div>
		</div>
	</div>
</div>

<?php 
	echo $this->view('footer.php', $data); 
?>
