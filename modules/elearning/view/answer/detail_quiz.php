<div class="col-md-8 content-quiz box box-white form form-quiz">
	<div class="header-quiz" style="padding: 10px 5px;">
		<div class="row">
			<div class="col-md-12">
				<h3 class="no-margin">
					<i class="fa fa-sliders fa-lg"></i> <?php echo $test->judul ?>
					<small class="pull-right">Quiz Detail</small>
				</h3>
			</div>
		</div>
	</div>
	<div class="col-md-12">
	 	<h4>Start date <small>: <?php echo $test->tgl_mulai ?></small></h4>
	 	<h4>Due date <small>: <?php echo $test->tgl_selesai ?></small></h4>
	 	<h4>Time limit <small>: <?php echo $test->time_limit ?> minutes</small></h4>
	 	<h4>About quiz <small>: <?php echo $test->keterangan ?> </small></h4>
	 	<h4>Instruction <small>: <?php echo $test->instruksi ?> </small></h4>
	</div>
</div>

<?php 
	function cetak_jawaban($jawaban, $soal_id, $tipe){
		foreach($jawaban as $key) {
			if($soal_id == $key->soal_id) {
				if($tipe == 'multiple choice') {echo '<div class="radio">'; $tipe_ = 1; }
				else {echo '<div class="checkbox">'; $tipe_ = 2;}
				echo '<label>';
				$val = $soal_id.'|'.$key->jawaban_id.'|'. $tipe_.'|'.$key->keterangan;
			  	if($tipe == 'multiple choice') echo '<input name="jwb[]" type="radio" value="'.$val.'">';
				else echo '<input name="jwb[]" type="checkbox" value="'.$val.'">';
			    echo $key->keterangan;
				echo '</label>';
				echo '</div>';
			} 
		} 
	}
?>