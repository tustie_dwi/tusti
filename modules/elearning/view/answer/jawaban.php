<script type="text/javascript">
	function update_skor(skor, soal_id){
		var els=document.getElementById(soal_id);
		els.value = skor;
	}
</script>
<?php if(isset($jawaban_mhs->nama)) { ?>
	<div class="col-md-8 content-quiz box box-white form form-quiz">
		<div class="header-quiz" style="padding: 10px 5px;">
			<div class="row">
				<div class="col-md-12" style="margin-top: -5px">
					<h3 class="no-margin">
						<?php
							if(empty($jawaban_mhs->foto)) $foto = $this->config->default_pic;
							else $foto = $this->config->file_url_view.'/'.$jawaban_mhs->foto;
						?>
						<img class="img-ava" width="50" height="50" src="<?php echo $foto ?>">
						<?php echo $jawaban_mhs->nama ?>
						
						<?php if($jawaban_mhs->is_mark == 0){ ?>
							<button onclick="submit_mark();" class="btn btn-success pull-right bold"><i class=" fa fa-check"></i> Mark</button>
						<?php } else{ ?>
							<form class="pull-right bold" action="<?php echo $this->location('module/elearning/answer/update_skor') ?>" method="POST">
								<button class="btn btn-primary pull-right"><i class=" fa fa-history"></i> Remark</button>
								<span class="pull-right bold" style="margin-right: 10px">
									<div class="input-group" style="width: 130px">
								      <span class="input-group-addon">Score</span>
								      <input type="hidden" name="xvsvd" value="<?php echo substr(md5($jawaban_mhs->hasil_id),8,7) . '|' . $quiz_id . '|' . $jawaban_mhs->nim?>" />
								      <input name="skor[]" type="text" class="form-control" value="<?php echo $jawaban_mhs->total_skor ?>" >
								    </div>
								</span>
							</form>
						<?php } ?>
					</h3>
				</div>
			</div>
		</div>
		<div class="main-quiz">
			<div class="row">
				<div class="col-md-2 main-quiz-menu"> <!-- wrap left -->
					<div class="title-menu-question">Questions</div>
					<ul class="menu-question-number">
						<?php $no=1; foreach($soal as $key) { ?>
							<li <?php if($no == 1) echo 'class="active"'; ?> id="soal-page-<?php echo $no; ?>"><a href="#" onclick="change_question('<?php echo $no; ?>')"><?php echo $no; ?></a></li>
						<?php $no++; } ?>
					</ul>
					
				</div>
				<div class="col-md-10 main-quiz-question"> <!-- wrap middle/list soal -->
					
					<form action="<?php echo $this->location('module/elearning/answer/update_skor') ?>" method="POST" id="form-mark">
						<input type="hidden" name="xvsvd" value="<?php echo substr(md5($jawaban_mhs->hasil_id),8,7) . '|' . $quiz_id . '|' . $jawaban_mhs->nim?>" />
						<?php $no=1; foreach($soal as $key) { ?>
							<span class="page-<?php echo $no ?>">
								
								<?php if($jawaban_mhs->is_mark == 0){ ?>
									<span class="pull-right">
										<div class="input-group" style="width: 150px">
									      <span class="input-group-addon">Score</span>
									      <input name="skor[]" id="<?php echo $key->soal_id ?>" type="text" class="form-control" value="0" >
									    </div>
									</span>
								<?php } ?>
								<div class="question-area" style="margin-top: 10px">
									<label>Question <?php echo $no ?></label><br>
									<?php echo $key->pertanyaan ?>
								</div>
								
								<div class="answer-area">
									<label>Response</label>
									<?php 
										if($key->keterangan != 'essay') cetak_jawaban($jawaban, $key->soal_id, $key->keterangan, $detail_jawaban); 
										else echo '<textarea disabled class="form-control jwb">'.print_hasil($detail_jawaban, $key->soal_id, '', 'textarea').'</textarea>';
									?>
								</div>
							</span>
						<?php $no++; } ?>
					</form>
				</div>
				
			</div>
		</div>
	</div>
<?php } else{ ?>
	<div class="col-md-8 content-quiz box box-white form form-quiz">
	<div class="header-quiz" style="padding: 10px 5px;">
		<div class="row">
			<div class="col-md-12">
				<h3 class="no-margin">
					<i class="fa fa-meh-o fa-lg"></i> There's no submission
				</h3>
			</div>
		</div>
	</div>
<?php } ?>
<?php 
	function cetak_jawaban($jawaban, $soal_id, $tipe, $detail_jawaban){
		$total_skor = 0;
		foreach($jawaban as $key) {
			if($soal_id == $key->soal_id) {
				$skor = print_hasil($detail_jawaban, $soal_id, $key->jawaban_id, 'checked');
				
				if(! empty($skor)) {
					$checked = 'checked';
					$total_skor += $skor;
				}
				else $checked = '';
				
				if($tipe == 'multiple choice') {echo '<div class="radio">'; $tipe_ = 1; }
				else {echo '<div class="checkbox">'; $tipe_ = 2;}
				echo '<label>';
				  	if($tipe == 'multiple choice') echo '<input $checked type="radio" disabled>';
					else echo '<input '.$checked.' type="checkbox" disabled>';
				    echo $key->keterangan;
					if($key->is_benar == 1) echo " <span class='label label-success'>Correct</span>";;
				echo '</label>';
				echo '</div>';
			} 
		} 
		
		echo "<script language='javascript'> update_skor('".$total_skor."','".$soal_id."') </script>";
	}
	
	function print_hasil($jawaban, $soal_id, $jawaban_id, $tipe){
		foreach ($jawaban as $key) {
			if($key->soal_id == $soal_id AND $key->jawaban_id == $jawaban_id) {
				if($tipe == 'checked') return $key->inf_skor;
			}
			
			if($key->soal_id == $soal_id){
				if($tipe == 'textarea') return $key->inf_jawab;
			}
		}
	}
?>

