<?php
if(isset($mhs_id)&&(isset($posts_mhs)||isset($data_mhs))){ 
	if(isset($posts_mhs)){
		foreach($posts_mhs as $dt){
			$tgl_upload = $dt->tgl_upload;
			$uploadid 	= $dt->upload;
			$nama 		= $dt->nama_mhs;
			$catatan 	= $dt->catatan;
			$skor 		= $dt->skor;
			$total_skor	= $dt->total_skor;
		}
	}
	else{
		foreach($data_mhs as $dt){
			$nama 		= $dt->nama;
		}
	}
	
	if(!isset($catatan)){ 
		$status = "Not turned in";
	}
	elseif(isset($catatan)&&!isset($skor)){
		$status = "Not graded";
	}
	elseif(isset($catatan)&&isset($skor)){
		$status = "Graded";
	}
}
?>

<?php if(isset($mhs_id)&&isset($nama)){ ?>
<div class="box box-white" style="padding:15px 15px 15px 15px; height: auto; border-bottom: 1px solid #DDDDDD ;margin-bottom: 0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
	<div class="row">
		<div class="col-sm-1">
			<img src="<?php echo $this->config->default_pic ?>" class="img-avatar" />
		</div>
		<div class="col-sm-5">
			<div class="" id="nick-wrap" style="margin-left: 10px; margin-top: 2px">
				<div class="text-left"><?php echo ucwords($nama) ?></div>
				<?php if(!isset($catatan)){ ?>
					<div style="color: #9290B0"><?php echo $status ?></div>
				<?php }else{ ?>
					<div class="text-left" style="color: #999999"><?php echo "<small>Submitted on ".$tgl_upload."</small>" ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="col-sm-6" style="text-align: right">
			<?php if(isset($skor)&&isset($total_skor)&&isset($catatan)&&$skor!='0.00'){
					$form = "none";
					echo "<div id='nilai'>";
					echo $skor." / ".$total_skor;
					echo " <a href='#' class='btn btn-link' onclick='show_form()'><i class='fa fa-edit'></i></a>";
					echo "</div>";
				   }else{
				  	$form = "block";
				   } 
			?>
				<form id="nilai-form" style="display: <?php echo $form ?>">
					<div class="row">
						<input type="hidden" class="form-control" id="upload_id" name="upload_id" value="<?php if(isset($uploadid)&&isset($catatan)) echo $uploadid ?>"/>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="skor" name="skor" value="<?php if(isset($skor)&&isset($catatan)) echo $skor ?>"/>
						</div>
						<div class="col-sm-1">
							<p style="font-size: 20px;">/</p>
						</div>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="total_skor" name="total_skor" value="<?php if(isset($total_skor)&&isset($catatan)) echo $total_skor ?>"/>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-success">Grade</button>
						</div>
					</div>
				</form>
			
		</div>
	</div>
</div>
<div class="box box-white" style="padding:15px 15px 15px 15px; height: auto; margin-bottom: 0px; border-bottom: 1px solid #DDDDDD; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
	<?php if(!isset($catatan)){
			echo ucwords($nama)." has not been turned in yet!";
		  }else{ ?>
			<div class="text-left">
				
				<?php echo ucwords($catatan) ?>
				<?php
				if(isset($posts_mhs)){ ?>
					<p>
						<small>
					<?php foreach($posts_mhs as $dt){ 
							if($dt->file_name){ ?>
								<em><br>
									<?php echo "File&nbsp;&nbsp;&nbsp;: ".$dt->file_name ?><br>
									<?php echo "Size&nbsp;&nbsp;: ".$this->formatSizeUnits($dt->file_size) ?><br>
									<a href="#" class="btn btn-default">Download This File</a>
								</em><br>
					 <?php }
						} ?> 
						</small>
					</p>
				<?php
				}
				?>
			</div>
	<?php } ?>
</div>
<?php } ?>