<?php echo $this->view('header.php', $data);
	if(isset($tugas)){
		foreach($tugas as $dt){
			$judul 			= $dt->judul;
			$tgl_selesai 	= $dt->tgl_selesai;
			$instruksi 		= $dt->instruksi;
			$keterangan		= $dt->keterangan;
			$parent			= $dt->post_id;
			$jenis			= $dt->jenis;
			$materi			= $dt->materi;
		}
	}
?>

<!-- Begin Body -->
<div class="container">
	<!-- Begin Content -->
	<div class="row">
		<!-- Begin Sidebar -->
		<?php 
			$data['judul'] = $judul;
			$data['tgl_selesai'] = $tgl_selesai;
			$this->view("assignment/sidebar.php", $data); 
		?>
      	<!-- End Sidebar -->
      	
      	<!-- Begin Content Primary-->
      	<div class="col-sm-8">
      		<!-- Begin Detail Assignment -->
			<div class="row">
				<div class="col-md-12">
					<div class="box box-white" style="padding:15px 15px 15px 15px; height: auto; border-bottom: 1px solid #DDDDDD ;margin-bottom: 0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
						<span class="text-left"><b><?php echo ucwords($judul)?></b></span>
					</div>
					<div class="box box-white" style="padding:15px 15px 15px 15px; height: auto; border-top-left-radius: 0px; border-top-right-radius: 0px;">
						<div class="text-left">
							<small><em>Topic&nbsp;:</em></small><br>
							<?php 
							if($materi) echo ucwords($materi);
							else echo "No Topic";
							?>
						</div>
						
						<div class="text-left">
							<br><small><em>Instruction&nbsp;</em></small><br>
							<?php echo ucwords($instruksi)?>
						</div>
						<div class="text-left">
							<br><small><em>Description&nbsp;</em></small><br>
							<?php echo ucwords($keterangan)?>
						</div>
						<?php if(isset($jenis)){ ?>
							<div class="text-left">
							<br><small><em>Attachment&nbsp;</em></small>
							<?php 
								if(isset($tugas)){
								echo "<table class='table'>";
								echo "<tbody>";
									foreach($tugas as $dt){
										if($dt->jenis == "file" || $dt->jenis == "library"){
											echo "<tr><td style='width: 230px'><i class='fa fa-file-o'><small></i> ".$dt->file_name."</small></td>";
											echo "<td><small>".$this->formatSizeUnits($dt->file_size)."</small></td>";
											echo "<td><a href='#' onclick='Download(\"".$dt->file_name."\")' class='btn btn-default btn-xs'><small>Download This File</small></a></td></tr>";
										}
										else{
											echo "<tr><td><a href=".$dt->attach."><i class='fa fa-link'><small></i> ".$dt->attach."</small></a></td><td></td><td></td></tr>";
										}
									}
								echo "</tbody>";
								echo "</table>";
								}
							?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- End Detail Assignment -->
			<div class="row">
				<div class="col-md-12">
					<?php if($role == "dosen") {
							$this->view("assignment/dosen.php",$data);
						  }
						  elseif($role == "mahasiswa"){
						  	$data['parent'] = $parent;
						  	$this->view("assignment/mhs.php",$data);
						  }
					 ?>
					<!-- Start Comment -->
		         	<?php $this->view("assignment/comment.php", $data); ?>
		         	<!-- End Comment -->
				</div>
			</div>
      	</div>
      	<!-- Begin Content Primary-->
	</div>
	<!-- End Content -->
</div>
<!-- End Body -->

<?php echo $this->view('footer.php', $data) ?>