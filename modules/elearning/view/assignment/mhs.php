<?php
if(isset($tgs_mhs)){
	foreach($tgs_mhs as $dt){
		$uploadid 	= $dt->upload;
		$tgl_upload = $dt->tgl_upload;
		$catatan 	= $dt->catatan;
		$nama_mhs 	= $dt->nama_mhs;
		$skor 		= $dt->skor;
	}
}
if(isset($data_mhs)){
	foreach($data_mhs as $dt){
		$nama 		= $dt->nama;
	}
}

	if(!isset($catatan)){ 
		$status = "Not turned in";
	}
	elseif(isset($catatan)&&!isset($skor)){
		$status = "Awaiting grade";
	}
	elseif(isset($catatan)&&isset($skor)&&$skor!= "0.00"){
		$status = "Graded";
	} 
?>

<div class="box box-white" style="padding:15px 15px 15px 15px; height: auto; border-bottom: 1px solid #DDDDDD ;margin-bottom: 0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
	<div class="row">
		<div class="col-sm-1">
			<img src="<?php echo $this->config->default_pic ?>" class="img-avatar" />
		</div>
		<div class="col-sm-11">
			<div class="" id="nick-wrap" style="margin-left: 10px; margin-top: 2px">
				<div class="text-left">Hi, <?php echo $nama ?></div>
				<?php if(!isset($catatan)){ ?>
					<div style="color: #9290B0"><?php echo $status ?></div>
				<?php }else{ ?>
					<div class="text-left" style="color: #999999"><?php echo "<small>Submitted on ".$tgl_upload."</small>" ?></div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<div class="box box-white" style="padding:15px 15px 15px 15px; height: auto; margin-bottom: 0px; border-bottom: 1px solid #DDDDDD; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
	<?php if(!isset($catatan)&&$role=="mahasiswa"){
			$this->view('assignment/upload_answer.php', $data);
		  }else{ ?>
			<div class="text-left">
				<?php echo ucwords($catatan) ?>
				
				<?php
				if(isset($tgs_mhs)){ ?>
					<p>
						<small>
					<?php foreach($tgs_mhs as $dt){ 
							if($dt->file_name){ ?>
								<em><br>
									<?php echo "File&nbsp;&nbsp;&nbsp;: ".$dt->file_name ?><br>
									<?php echo "Size&nbsp;&nbsp;: ".$this->formatSizeUnits($dt->file_size) ?>
								</em><br>
							<?php }
						} ?> 
						</small>
					</p>
				<?php
				}
				?>
			</div>
	<?php } ?>
</div>