<?php
	if(isset($tgs_mhs)){
		foreach($tgs_mhs as $dt){
			$uploadid 	= $dt->upload;
			$skor 		= $dt->skor;
			$total_skor	= $dt->total_skor;
			$catatan 	= $dt->catatan;
		}
	}
	
	if(!isset($catatan)){ 
		$status = "Not turned in";
	}
	elseif(isset($catatan)&&isset($skor)&&$skor== "0.00"){
		$status = "Awaiting grade";
	}
	elseif(isset($catatan)&&isset($skor)&&$skor!= "0.00"){
		$status = "Graded";
	} 
?>

<!-- Begin Sidebar -->
<div class="col-md-4">
<div class="row">
	<div class="col-md-12">
    	<div class="box box-white" style="height: auto; border-bottom: 1px solid #DDDDDD ;margin-bottom: 0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
    		<div class="head-box">
    			<div class="row">
    				<div class="col-sm-3">
    					<i class="fa fa-file-o" style="font-size: 35px;margin-left: 10px;margin-top: 5px">
    					</i>
					</div>
    				<div class="col-sm-9" style="margin-left: -15px;">
						<div class="text-left"><a href="<?php echo $this->location('module/elearning/assignment/detail/'.$tugas_id."/".$jadwal_id) ?>"><b><?php echo ucwords($judul) ?></b></a></div>
      					<div class="text-left" style="color: #888888">
      						<small>
      							Due Date<br>
      							<?php echo $tgl_selesai ?>
      						</small>
      					</div>
      				</div>
      			</div>
      		</div>
    	</div>
    	<div class="box box-white" style="height: auto; border-top-left-radius: 0px; border-top-right-radius: 0px;">
    		<div class="head-box">
    			<div class="row">
    				<div class="col-sm-12">
    					<?php if($role == "mahasiswa"){ ?>
        					<div class="text-left">
        						<?php echo $status ?>
        						<div class="pull-right">
	        						<?php
	        						if(isset($skor)&&isset($total_skor)&&isset($catatan)&&$skor!= "0.00"){ 
	        							echo $skor." / ".$total_skor;
									}
									?>
	        					</div>
        					</div>
    					<?php }elseif($role == "dosen"){ ?>
    							<?php 
    							if(isset($list_mhs)){
    								  foreach($list_mhs as $l){ ?>
										<div class="row" style="margin-bottom: 5px;">
					        				<div class="col-sm-2"> 
					        					<img width="40px" src="<?php echo $this->config->default_pic ?>"/>
					        				</div>
					        				<div class="col-sm-10">
					        					<div class="" id="nick-wrap">
					        						<div class="text-left"><a href="<?php echo $this->location('module/elearning/assignment/detail/'.$tugas_id."/".$jadwal_id."/".$l->id."/") ?>" class="btn btn-link"><?php echo $l->nama ?></a></div>
					          					</div>
					          				</div>
					          			</div>
			          			<?php }
								} ?>
    					<?php } ?>
					</div>
      			</div>
      		</div>
    	</div>
  	</div>
</div>
</div>
<!-- End Sidebar -->