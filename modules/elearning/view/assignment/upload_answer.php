<form method="post" id="upload-assignment" class="form-horizontal">
	<div class="form-group">
		<div class="col-sm-12">
			<textarea class="form-control" name="catatan" id="catatan" required placeholder="Type your note here..."></textarea>
		</div>
	</div>
	<div class="form-group">
  		<div class="col-md-12" id="link-wrap-assignment"></div>
	  	<div class="col-md-12" id="attach-wrap-assignment" style="margin-bottom: 5px"></div>
	  	<div class="col-sm-8">
	  		<a data-toggle="modal" data-target="#modal-link" class="btn btn-success btn-xs pull-left" onclick="tab_val('assignment')"><i class="fa fa-link"></i></a> 
	  		<a accesskey="a" data-toggle="modal" data-target="#modal-library" style="margin: 0 2px" class="btn btn-success btn-xs pull-left btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Attach Library" onclick="tab_val('assignment')"><i class="fa fa-archive"></i></a> 
	  		<a class="fileUpload btn btn-success btn-xs pull-left">
			    <i class="fa fa-file-text-o"></i>
			    <input name="file[]" type="file" class="upload" id="uploads" multiple="multiple" />
			</a>
	  	</div>
  	</div>
	<div class="form-group">
		<div class="col-sm-12">
			<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
			<input type="hidden" name="mhsid" value="<?php echo $mhs_id;?>">
			<input type="hidden" name="tgsid" value="<?php echo $tugas_id;?>">
			<input type="hidden" name="post_parent_id" value="<?php echo $parent;?>">
			<button class="btn btn-info pull-right">Turn in Assignment</button>
			<a href="#" class="btn btn-link pull-right">Cancel</a>
		</div>
	</div>
</form>

<!-- Modal Attach Media Library -->
<div class="modal fade" id="modal-library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive"></i> Attach Library
	    		<span class="pull-right loading"><i class="fa fa-refresh fa-spin"></i> Loading</span>
	    	</h4>
	    </div>
    	<div class="modal-body" style="padding: 20px 0px">
    		<div class="row folder-wrap" style="margin: 0 10px; padding: 0px; padding-right: 10px">
    			<input id="folder_back_parent_id" value="0" type="hidden"/>
	    		<?php 
	    		if(isset($library)){
	    			foreach($library as $key){ ?>
			  		<div class="col-md-3" style="padding: 0px 5px 0px 5px">
			  			<button title="<?php echo $key->folder_name ?>" class="btn btn-default" style="width: 100%; margin: 5px;" onclick="get_folder('<?php echo $key->id ?>')">
			  				<i class="fa fa-folder fa-3x"></i><br><?php echo $key->folder_name ?>
			  			</button>
			  		</div>
			  	<?php }
				} ?>
		  	</div>
      	</div>
      	<div class="modal-footer">
      		<a type="button" class="btn btn-primary btn-sm pull-left" onclick="folder_back()"><i class="fa fa-arrow-circle-left"></i> Back</a>
      		
        	<a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
        	<a onclick="add_library()" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Attach</a>
      	</div>
    </div>
  </div>
</div>

<!-- Modal Attach Link -->
<div class="modal fade" id="modal-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-link"></i> Attach Link</h4>
      </div>
      <form role="form">
      	  <input type="hidden" id="tab-link"/>
	      <div class="modal-body">
			  <div class="form-group">
			    <input type="url" class="form-control" placeholder="Enter URL" required id="add_url">
			  </div>
	      </div>
	      <div class="modal-footer">
	        <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
	        <a onclick="get_new_url()" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Save changes</a>
	      </div>
      </form>
    </div>
  </div>
</div>