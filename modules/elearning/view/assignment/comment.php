<?php
if(isset($comment)){
	foreach($comment as $dt){
		$post_id = $dt->post_id;
	}
}
else if(isset($tugas)){
	foreach($tugas as $dt){
		$post_id = $dt->post_id;
	}
}

?>
<!-- Start Comment -->
<div class="row box box-gey post" style="background-color: #F3F5F6; padding:10px 10px 10px 10px; height: auto; margin-bottom: 50px; border-top-left-radius: 0px; border-top-right-radius: 0px;">
	<h4 style="color: #333333; margin-bottom: 25px">Comments</h4>
	<div class="li-comment">
	<?php
	if(isset($comment_list)){	
		foreach($comment_list as $key) {
			if($key->post_id == $post_id){
				
				?>
			<?php if($key->foto){$strimg= $this->config->file_url_view.'/'.$key->foto;}else{ $strimg=$this->config->default_pic;} ?>
			 	<div class="row box box-gey post no-shadow no-top-radius box-comment-<?php echo $key->comment_id ?>" style="border-top: solid 1px #cccccc">
					<div class="col-md-1" style="padding: 0px"><img src="<?php echo $strimg; ?>" class="img-avatar-sm pull-right" /></div>
					<div class="col-md-10">
						<span class="text-left">
							<a class="btn-link-sm">
							<?php 
	        					if($user_id == $key->user_id) echo "Me - ";
								else echo $key->user_name. " - ";
	        					convert_date($key->last_update) 
	        				?>
	        				</a>
			      		<span style="color: #383D48"><br>
			      			<p><?php echo $key->isi ?></p>
			      		</span>
			  		</div>
			  		<div class="col-md-1 pull-right">
			  			<?php
			  			if($user_id == $key->user_id){
			      			echo "<a href='#' onclick='del_comment(\"".$key->comment_id."\")'><i class='fa fa-times pull-right'></i></a>";
			      		}
			  			?>
			  		</div>
				</div>
				<?php
			}
		}
	}
	?>
	</div><br>
	<form method="post" id="upload-comment-form" class="form-horizontal">
		<div class="form-group">
			<div class="col-sm-12">
				<textarea class="form-control" name="isi" id="isi" placeholder="Type your comment here..."></textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<input type="hidden" name="link_id" id="link_id" value="<?php echo $this->location('module/elearning/assignment/detail/'.$tugas_id."/".$jadwal_id) ?>"/>
				<input type="hidden" name="post_id" id="parent_id" value="<?php echo $post_id ?>"/>
				<button class="btn btn-default pull-right">Add Comment</button>
			</div>
		</div>
	</form>
</div>
<!-- End Comment -->

<?php
function convert_date($date){
	$date = explode('-', $date);
	echo substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0))  . ' ' . $date[0]   . ' ' .  substr($date[2],2,6);
}
?>