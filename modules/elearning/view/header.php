<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>PJJ</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?php echo $this->location('modules/elearning/assets/bootstrap/css/style.min.css') ?>" rel="stylesheet">
		<link href="<?php echo $this->location('modules/elearning/assets/bootstrap/css/new.css') ?>" rel="stylesheet">
		<!-- <link href="<?php echo $this->location('modules/elearning/assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet"> -->
		<!-- <link href="<?php echo $this->location('modules/elearning/assets/bootstrap/css/add.css') ?>" rel="stylesheet"> -->
		<!-- <link href="<?php echo $this->location('modules/elearning/assets/bootstrap/css/ngengs.css') ?>" rel="stylesheet"> -->
		<!-- <link href="<?php echo $this->location('modules/elearning/assets/fa/css/font-awesome.min.css') ?>" rel="stylesheet"> -->
	    <?php $styles = $this->get_styles(); 
	        if(is_array( $styles )) : 
	    	
			foreach($styles as $s) : 
		?><link href="<?php echo $this->location($s); ?>" rel="stylesheet">
		<?php endforeach; endif; ?>
		<script>
			var base_url = '<?php echo $this->location(); ?>';
		</script>
	</head>
	<body>
	<?php
			$this->add_script('js/all.js');
			$role    = $this->coms->authenticatedUser->role;
			$staff   = $this->coms->authenticatedUser->staffid;
			$mhs_id  = $this->coms->authenticatedUser->mhsid;
			//$data['user']	 = $this->coms->authenticatedUser->name;
			$data['role'] 	 = $this->coms->authenticatedUser->role;
			$data['user_id'] = $this->coms->authenticatedUser->id;
			//$data['foto']    = $this->coms->authenticatedUser->foto;

			$m_general		  = new model_general();
			
			$jml_notifikasi = $m_general->get_jml_notifikasi($data['user_id']);
			$notifikasi = $m_general->get_notifikasi_new($data['user_id']);
			
			/*user data */
			$key = $m_general->get_user_profile($data['user_id']);
			
			if(! empty($staff)) $data_mk = $m_general->get_mkd_dosen($staff);
			else $data_mk = $m_general->get_mkd_mhs($mhs_id);
			
			$data['user']	 = $key->name;
			$data['foto']    = $key->foto;
			
	?>
	<header class="navbar navbar-default navbar-fixed-top" role="banner">
	  <div class="container">
	    <div class="navbar-header">
	      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	   		  <a class="navbar-brand" href="#"><img src="<?php echo $this->asset("images/pjj-logo-footer-2.png"); ?>" style="height:30px;width:100px;float:left;margin-right:5px;margin-top:-5.5px"></a>
	    </div>
	    <nav class="collapse navbar-collapse" role="navigation">
	      <ul class="nav navbar-nav">
			<li>
			<a href="<?php echo $this->location(); ?>">Home </a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Progress</a>
	      		<ul class="dropdown-menu">
					<?php 
					if($data_mk){
						foreach($data_mk as $key) { ?>
						    <li>
						    	<a href="<?php echo $this->location('module/elearning/progress/views/'. $key->hidId) ?>"  >
						    		<i class="fa fa-bookmark-o"></i> <?php echo $key->keterangan ?>
						    	</a>
						    </li>
					    <?php }
					}
					?>	
				</ul>
	        </li>
	        <!--<li>
	          <a href="#">Library</a>
	        </li>
	        <li>
	          <a href="#">About</a>
	        </li>-->
	      </ul>
	      <?php 
		  
			
			
			/*--- end --*/
		  
		  if($data['foto']){
				$strimg = $this->config->file_url_view."/".$data['foto'];
			}else{
				$strimg = $this->config->default_pic;
			}
		  ?>
	      <ul class="nav navbar-nav navbar-right">
	      	<li class="dropdown">
	      		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      			<span class="fa fa-bell-o icon-for-notification"></span>
	      			<?php if($jml_notifikasi > 0) { ?>
	      				<span class="count-notification-navbar"><?php echo $jml_notifikasi ?></span> 
	      			<?php } ?>
	      			<b class="caret" style="opacity:0"></b>
	      		</a>
	      		<ul class="dropdown-menu notif-menu">
	      			<?php if(isset($notifikasi)) { ?>
		      			<?php foreach($notifikasi as $key){ ?>
			      			<li <?php if($key->is_read == '0') echo 'class="notification-unreadi"' ?>>
			      				<a href="#" onclick="redirect_notif('<?php echo $key->link_id ?>','<?php echo $key->hidId ?>')">			      				
			      					<?php 
										echo '<strong>'.$key->user_from_name.'</strong> ';
										if(strlen($key->keterangan) < 50) echo $key->keterangan;
										else echo substr($key->keterangan,0,47) . ' ..';
										
										if($key->is_read == '0') echo "<span class='label label-success pull-right small icon-unread'>new</span>";
										
										switch($key->kategori){
											case 'comment': echo "<br><i class='fa fa-comment green'></i> New comment"; break;
											case 'note': echo "<br><i class='fa fa-pencil-square fa-lg purple'></i> New post"; break;
											case 'submission': echo "<br><i class='fa fa-file-zip-o red'></i> New submission"; break;
											case 'mark': echo "<br><i class='fa fa-check-square blue'></i> New result"; break;
											case 'assignment': echo "<br><i class='fa fa-check-circle blue'></i> New assignment"; break;
										}
										
										echo '<span class="blue"> - '.convert_date_header('2014-05-12').'</span>';
			      					?>
			      				</a>
			      			</li>
		      			<?php } ?>
		      			<li class="see-all-notification text-center"><a href="<?php echo $this->location('module/elearning/home/see_all') ?>" class="blue">
		      				See All <i class="fa fa-angle-down"></i></a>
		      			</li>
	      			<?php } else{ ?>
	      				<li class="see-all-notification text-center"><a href="#" class="red">
		      				No notification found</a>
		      			</li>
      				<?php } ?>
	      		</ul>
	      	</li>
	      	<li class="dropdown">
	      		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      			<img src="<?php echo $strimg ?>" style="height:30px;width:30px;float:left;margin-right:5px;margin-top:-5.5px">
	      			<?php  echo $data['user']; ?> <b class="caret"></b>
	      		</a>
	      		<ul class="dropdown-menu">
	      			<li><a href="<?php 	  echo $this->location('module/elearning/home/profile'); ?>">Profile</a></li>
	      			<li><a href="<?php 	  echo $this->location('module/elearning/home/settings'); ?>">Settings</a></li>
	      			<li><a href="<?php 
					  echo $this->location('auth/logoff/'.$data['user']); ?>">Logout</a></li>
	      		</ul>
	      	</li>
	      </ul>
	    </nav>
	  </div>
	</header>
	<?php
	function convert_date_header($date){
		$date = explode('-', $date);
		return substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}
	
	?>