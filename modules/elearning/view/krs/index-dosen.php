<?php 
	echo $this->view('header.php',$data);
	$this->add_script('bootstrap/js/jasny-bootstrap.min.js'); 
	

?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	?>
		<table class='table table-hover' id='example'>
				<thead>
					<tr>			
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
		
			<?php if($posts > 0){ ?>
				<?php foreach ($posts as $dt): ?>
					<tr>
						
						<td>
							<div class="col-md-9">
								<div class="media">
									<a class="pull-left" style="text-decoration:none" href="#" onClick = "detail(<?php echo $dt->mahasiswa_id?>)">
										<img class="media-object img-thumbnail" src="<?php echo $this->asset($foto); ?>"  width="50" height="50">
									</a>
								  <div class="media-body">
									<?php
									echo "<a href='".$this->location('module/content/krs/approve/' . $dt->mahasiswa_id) ."' class='text text-default'><b>".ucWords(strToLower($dt->nama))."</b></a>
										<small><span class='text text-danger'>".$dt->prodi."</span></small>
										&nbsp;<span class='label label-success'>".$dt->angkatan."</span><br>";
									echo $dt->nim;
									?>
								  </div>
								</div>
						     </div>
							<div class="col-md-3">
								<a class="btn btn-default pull-right" href="<?php echo $this->location('module/content/krs/approve/' . $dt->mahasiswa_id) ?>"><i class="fa fa-check"></i> Lihat KRS</a>
							</div>
						</td>
					</tr>
				 <?php endforeach; ?>
			 <?php } ?>
		</tbody></table>
	
	 <?php else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
			 	
			</div> <!-- end of main row -->
  		</div> <!-- end of main col-md-3 -->
      
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	echo $this->view('footer.php');
?>
<script type="text/javascript">
  $(document).ready(function() {
    $('.datatable').dataTable({}); 
    $('.datatable').each(function(){
      var datatable = $(this);
      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
      search_input.attr('placeholder', 'Search');
      search_input.addClass('form-control input-sm');
      // LENGTH - Inline-Form control
      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
      length_sel.addClass('form-control input-sm');
    });
  });

  function tes(id){
    alert(id);
  }
</script>



