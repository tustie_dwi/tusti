<?php 
	echo $this->view('header.php',$data);
	$this->add_script('bootstrap/js/jasny-bootstrap.min.js'); 
	
	$rep = array();
	if($krs > 0){
		foreach($krs as $key) array_push($rep, substr(md5($key->mkditawarkan_id),8,7) );
	}
?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="row">
		  		<div class="col-md-12">
			        <div class="panel">
			          <div class="panel-heading panel-dark">
			            <h3 class="panel-title">
			            	Course Plan 
			            	<i class="fa fa-bookmark pull-right"></i>
			            </h3>
			          </div>
			             <ul class="list-group content-li">
			             	<?php if($krs){ foreach($krs as $key) { ?>
							    <li class="list-group-item">
									<?php if($key->is_approve == 0) : ?>
							    	<a onclick="return confirm('Are you sure wanna drop this course?')" href="<?php echo $this->location('module/elearning/krs/del_mkd/'.$key->id ) ?>"  >
							    		<?php echo $key->keterangan ?>
							    		<i class="fa fa-trash-o pull-right red btn-tooltip" data-placement="left" title="Drop course"></i>
							    	</a>
									<?php else :?>
										<?php echo $key->keterangan ?>
							    		<i class="fa fa-check-square-o pull-right btn-tooltip" data-placement="left" title="Approved"></i>
									<?php endif ;?>
							    </li>
						    <?php }} else { echo ' <li class="list-group-item"><a href="">No content to show</a></li>'; } ?>
						 </ul>
			        </div>
			 	</div>
			 	
			</div> <!-- end of main row -->
  		</div> <!-- end of main col-md-3 -->
      	<div class="col-md-9">
			<div class="row">
				<div class="col-md-12" style="margin-bottom: 20px">
		        	<?php $this->view('krs/search-bar.php', $data); ?>
				</div>
				<div class="col-md-12">
			        <div class="box box-white">
			          <h3 class="page-header no-margin" style="margin-bottom: 10px">Course List</h3>
			          <table class="table table-hover table-condensed table-striped datatable table">
			            <thead>
			              <tr>
			                <th width="65%"></th>
			                <th width="30%"></th>
			                <th width="5%"></th>
			              </tr>
			            </thead>
			            <tbody>
			            	<?php $no = 1; if($mkd){ foreach($mkd as $key){ ?>
			            		<tr>
			            			<td>
			            				<?php 
			            					if(strlen($no) == 1) $no = '00' . $no;
											elseif(strlen($no) == 2) $no = '0' . $no;
											
			            					echo '<span class="hide">'.$no.'</span> '; $no++;
			            					echo '<strong>' . $key->kode_mk . '</strong> - ' .$key->keterangan;
			            					echo '<br>';
											echo '<span class="red"> <i class="fa fa-user"></i> ' . $key->gelar_awal . ' '. $key->nama_dosen . ' ' . $key->gelar_akhir . '</span>';
			            					echo '<br>';
			            					echo '<span class="label label-primary">' . $key->prodi_id . '</span> ';
			            					echo '<span class="label label-primary">' . $key->kelas . '</span>';
			            				?>
			            			</td>
			            			<td>
			            				<?php 
			            					$kuota_slide = round(($key->jml / $key->kuota) * 100,1);
											if($key->jml >= $key->kuota) {
												$empty = 0; $kuota_slide = 100;
											}
											else $empty = ($key->kuota - $key->jml);
			            				?>
			            				Capacity : <?php echo $key->kuota . ' people'; ?>
			            				<div class="progress">
											<div class="progress-bar progress-bar-<?php if($empty == 0) echo "danger"; else echo "success"; ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $kuota_slide ?>%;">
												<?php echo $empty . ' empty'?>
											</div>
										</div>
			            			</td>
			            			<td>
			            				<?php
			            					$is_ada = 0;
											str_replace($rep, '', $key->mkditawarkan_id, $is_ada);
										?>
										
										<?php if($is_ada == 0){ ?>
											<a href="<?php echo $this->location('module/elearning/krs/add/'.$key->mkditawarkan_id.'/'.$key->kelas) ?>">
			            						<button <?php if($empty == 1) { ?>onclick="alert()" <?php } ?> style="margin-top: 14px" class="btn btn-success btn-sm <?php if($empty == 0) echo "disabled"; ?>">Choose <i class=" fa fa-check"></i> </button>
			            					</a>
			            				<?php } else { ?>
			            					<button data-placement="left" title="Course has been added" style="margin-top: 14px" class="btn btn-danger disable btn-sm btn-tooltip">Disable <i class=" fa fa-ban"></i> </button>
			            				<?php } ?>
			            			</td>
			            		</tr>
			            	<?php }} ?>
			            </tbody>
			          </table>
			      </div>
			    </div>
			</div>
		</div>
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	echo $this->view('footer.php');
?>
<script type="text/javascript">
  $(document).ready(function() {
    $('.datatable').dataTable({}); 
    $('.datatable').each(function(){
      var datatable = $(this);
      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
      search_input.attr('placeholder', 'Search');
      search_input.addClass('form-control input-sm');
      // LENGTH - Inline-Form control
      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
      length_sel.addClass('form-control input-sm');
    });
  });

  function tes(id){
    alert(id);
  }
</script>



