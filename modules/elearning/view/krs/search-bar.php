<div class="box box-white" style="margin-bottom: 0; padding-top: 20px;">
	<form class="form-horizontal" role="form"method="post" action="<?php echo $this->location('module/elearning/krs') ?>">
	  <div class="form-group">
	    <div class="col-sm-12">
	    	<select class="e9" name="mkd_id">
	    		<option value="0">All course</option>
	    		<?php if($list_mkd){ foreach($list_mkd as $key) { ?>
	    			<option <?php if($mkd_id == $key->mkditawarkan_id) echo "selected"; ?> value="<?php echo $key->mkditawarkan_id ?>"><?php echo $key->keterangan ?></option>
	    		<?php }} ?>
	        </select>
	    </div>
	  </div>
	  
	  <div class="form-group">
	    <div class="col-sm-12">
	      <div class="input-group">
	          <button type="submit" class="btn btn-success" type="button">Search <i class="fa fa-search"></i></button>
	      </div>
	    </div>
	  </div>
	</form>
</div>
	
	