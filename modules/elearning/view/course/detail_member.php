<?php echo $this->view('header.php',$data) ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-white" style="height: auto">
				<div class="head-box">
					<div class="row">
					<div class="col-md-2"><img src="<?php if($member->foto) echo $this->config->file_url_view.'/'.$member->foto; else  echo $this->config->default_pic ?>" class="img img-thumbnail" /></div>
					<div class="col-md-10">	
						<div class="row">
							<h2><?php echo $member->nama; ?></h2>
							<div class="profile-desc">Student</div>							
						</div>
						<div class="row">
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php $jmlpost = count($post); $jmlcomment= count($comment); $tpostreplies=$jmlpost+$jmlcomment; ?><?php echo $tpostreplies;?></h3></span></b>
								<span style="color: #9290B0">Post & Replies</span>
							</div>
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php $jmlgroup = count($groups); ?><?php echo $jmlgroup;?></h3></span></b>
								<span style="color: #9290B0">Groups</span>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>				
  		</div> <!-- end of main col-md-12 -->
		<div class="col-md-4">			
			<div class="action-group box box-white">						
				<ul class="list-group content-li content-li-menu">
					<li class="list-group-item"><a href="#"> Profile Overview<span class="fa fa-chevron-right"></span></a></li>
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/progress/'. $member->hidId."/".$member->id) ?>"> Progress<span class="fa fa-chevron-right"></span></a></li>
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/member/'. $member->hidId."/".$member->id) ?>"> Activity<span class="fa fa-chevron-right"></span></a></li>
				 </ul>				
			</div>		
		</div>
      	<div class="col-md-8">
			<div class="row">
				<?php $data['detailmember']=true;echo $this->view('home/view-content.php', $data);?>
			</div>
		</div>
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	echo $this->view('footer.php');
	
	
?>





