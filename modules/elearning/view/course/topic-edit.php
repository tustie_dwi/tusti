<div class="box box-white" style="height: auto" id="view_content">	        		
	<div class="head-box">		
			<div class="row">
            	<div class="col-md-12">
					<h3 class="title-box" ><?php echo ucWords($straction) ?> Topic</h3>
                </div>
			</div>
			<?php
			
			
			$mconf = new model_course(); 
											
			$materi = $mconf->get_materi($mkd->mkditawarkan_id);
			$status	= $mconf->get_statusmateri();
			if($straction=='edit') $posts  = $mconf->get_materi($mkd->mkditawarkan_id,"", $materi_id);
			if($straction=="add sub") $post  = $mconf->get_materi($mkd->mkditawarkan_id,"", $materi_id);
			
			if($straction!="add file"):
			?>
			<div class="action-group" style="margin-left: 10px;">				
				<form  method="post" id="form-save-topik" action="<?php echo $this->location('module/elearning/course'); ?>" enctype="multipart/form-data">						
					 <div class="form-group">	
						<label>Sub Topic</label>
						<select name="cmbmateri" id="parent" class="form-control">
							<option value="0">None</option>			
							<?php
																
								foreach($materi as $materi):
									echo "<option  value='".$materi->materi_id."' ";
									if(isset($posts->parent_id)&&($posts->parent_id==$materi->materi_id)){
										echo "selected";
									}
									
									if(isset($post->materi_id)&&($post->materi_id==$materi->materi_id)){
										echo "selected";
									}
									echo ">".$materi->judul."</option>";
								endforeach;
							?>
						</select>
					</div>
					<div class="form-group">	
						<label></label>
						<input type="text" class="form-control" name='judul' id='judul_topik' required="required" value="<?php if(isset($posts->judul)) echo $posts->judul; ?>" placeholder="Title">
					</div>
					
					 <div class="form-group">	
						<label>Note</label>
						<textarea class="form-control ckeditor" name='keterangan_topik' id='keterangan'><?php if(isset($posts->keterangan)) echo $posts->keterangan; ?></textarea>
					</div>
					
					<div class="form-group">	
						<label>Status</label>							
						<select name="cmbstatus" id="parent" class="form-control">
							<?php								
								foreach($status as $dt):
									echo "<option  value='".$dt->status."' ";
									if(isset($posts->status) && ($posts->status==$dt->status)){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
						<input type="hidden" class="form-control" name='urut' id='urut_topik' required="required" value="<?php if(isset($posts->urut)) echo $posts->urut; ?>">							
					</div>
					<div class="checkbox">
						<label>
						 <input type="checkbox" name="iscampus" value="1" <?php if (isset($posts->is_campus) && ($posts->is_campus==1)) { echo "checked"; } ?>> Campus Only
						</label>
					  </div>
					
					<!--<div class="form-group">	
						<label>Icon</label>
						
								<?php 
								
									/*if(isset($posts->icon)){ ?>
										<div class='well'>
											<img style="width: 100px; height: auto;" src="<?php echo $this->asset($posts->icon); ?>"/>
											<input type="hidden" name="uploads" id="uploads" value="<?php if(isset($posts->icon)) echo $posts->icon; ?>">
										</div>
									<?php } else { ?>
										<div class='well'>
											<p>Not available</p>
										</div>
									<?php } */
								?>
								<input type="file" name="uploads" id="uploads">
						
					</div>-->					
					<div class="form-group">
						<div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
							<img src="<?php if(isset($posts->icon)) echo  $this->config->file_url_view.'/'.$posts->icon; else  echo $this->config->default_pic ?>">
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
							<div>
							<span class="btn btn-default btn-file"><span class="fileinput-new"><i class="fa fa-camera"></i> Select Image</span><span class="fileinput-exists"><i class="fa fa-edit"></i> Change</span>
							<input type="file" name="uploads" accept="image/*"></span>
							<span class="btn btn-file"><a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash-o"></i> Remove</a></span>
							</div>
						</div>
					</div>  
					 <div class="form-group">					 									
							<input type="hidden" name="hidid" id="mkid" value="<?php echo $mkd->mkditawarkan_id;?>">
							<input type="hidden" name="mkid" value="<?php if(isset($posts->mk)) echo $posts->mk;?>">
							<input type="hidden" name="id" value="<?php if(isset($posts->materi_id)) echo $posts->materi_id;?>">
							<input  type="submit" id="btn-submit-publish" name="b_savepublish" onclick="submit_topik_content('publish','<?php echo $mkd->hidId ?>')" value="Save and Publish" class="btn btn-primary">
							<input  type="submit" id="btn-submit-draft" name="b_draft" onclick="submit_topik_content('draft','<?php echo $mkd->hidId ?>')" value="Save as Draft" class="btn btn-warning">
							<span id="status-save" style="margin-left:1em;">&nbsp;</span>    			
					</div>				
				</form>
		</div>	
<?php
	else:
		$data['posts']  = $mconf->get_materi($mkd->mkditawarkan_id,"", $materi_id);
		$this->view('course/topic-file.php', $data);
	endif;
?>		
	</div>	
</div>
