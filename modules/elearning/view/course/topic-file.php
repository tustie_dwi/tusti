
			<?php
			
			
			$mconf = new model_course(); 
											
			$materi = $mconf->get_materi($mkd->mkditawarkan_id);
			$status	= $mconf->get_statusmateri();
			$posts  = $mconf->get_materi($mkd->mkditawarkan_id,"", $materi_id);
			?>
			<div class="action-group" style="margin-left: 10px;">				
				<form  method="post" id="form-save-file" action="<?php echo $this->location('module/elearning/course'); ?>" enctype="multipart/form-data">					
					
					 <div class="form-group">	
						<label>Topic</label>
						<select name="cmbmateri" class="form-control">
							
							<?php
																
								foreach($materi as $materi):
									echo "<option  value='".$materi->materi_id."' ";
																		
									if(isset($post->materi_id)&&($post->materi_id==$materi->materi_id)){
										echo "selected";
									}
									echo ">".$materi->judul."</option>";
								endforeach;
							?>
						</select>
					</div>
					<div class="form-group">	
						<label></label>
						<input type="text" class="form-control" name='judul' id='judul' required="required"  placeholder="Title">
					</div>
					 <div class="form-group">	
						<label>Note</label>
						<textarea class="form-control ckeditor" name='keterangan' id='keterangan'></textarea>
					</div>
					
					 <div class="form-group">
						<div class="col-md-12" id="link-wrap-note"></div>
						<div class="col-md-12" id="attach-wrap-note" style="margin-bottom: 5px"></div>
						<div class="col-md-12">
							<!--<a data-toggle="modal" data-target="#modal-link" class="btn btn-success btn-xs pull-left btn-tooltip white" data-toggle="tooltip" data-placement="bottom" title="Attach Link" onclick="tab_val('note')"><i class="fa fa-link"></i></a> -->
							<a accesskey="a" data-toggle="modal" data-target="#modal-library" style="margin: 0 2px" class="white btn btn-success btn-xs pull-left btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Attach Library" onclick="tab_val('note')"><i class="fa fa-archive"></i></a> 
							
							<a class="fileUpload btn btn-success btn-xs pull-left btn-tooltip white" data-toggle="tooltip" data-placement="bottom" title="Attach File">
								<i class="fa fa-file-text-o"></i>
								<input name="file[]" type="file" class="upload" id="uploadBtn" multiple="multiple" />
							</a>
							
							<a id="cancel-note" class="fileUpload btn btn-danger btn-xs pull-right btn-tooltip white" data-toggle="tooltip" data-placement="bottom" title="Cancel File"> 
								<i class="fa fa-ban"></i>						
							</a>							
						</div>							
					  </div> 
					  <div class="clearfix"></div>
					<div class="form-group">
							<p>&nbsp;</p>
							<label></label>
							<input type="hidden" name="materi_id" value="<?php if(isset($posts->materi_id)) echo $posts->materi_id;?>">
							<input type="hidden" name="mkditawarkan_id" value="<?php if(isset($mkd->mkditawarkan_id)) echo $mkd->mkditawarkan_id;?>">
							<input  type="submit" id="btn-submit-publish" name="b_savepublish" onclick="submit_file_content('publish','<?php echo $mkd->hidId ?>')" value="Save and Publish" class="btn btn-primary">
							<input  type="submit" id="btn-submit-draft" name="b_draft" onclick="submit_file_content('draft','<?php echo $mkd->hidId ?>')" value="Save as Draft" class="btn btn-warning">
							
							<span id="status-save" style="margin-left:1em;">&nbsp;</span>					
											
					</div>				
				</form>
		</div>	
