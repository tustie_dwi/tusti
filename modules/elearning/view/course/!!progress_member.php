<?php echo $this->view('header.php',$data) ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-white" style="height: auto">
				<div class="head-box">
					<div class="row">
					<div class="col-md-2"><img src="<?php if($member->foto) echo $this->location($member->foto); else  echo $this->location('modules/elearning/assets/pic/user.jpg') ?>" class="img img-thumbnail" /></div>
					<div class="col-md-10">	
						<div class="row">
							<h2><?php echo $member->nama; ?></h2>
							<span style="color: #9290B0">Student</span>							
						</div>
						<div class="row">
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php $jmlpost = count($post); $jmlcomment= count($comment); $tpostreplies=$jmlpost+$jmlcomment; ?><?php echo $tpostreplies;?></h3></span></b>
								<span style="color: #9290B0">Post & Replies</span>
							</div>
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php $jmlgroup = count($groups); ?><?php echo $jmlgroup;?></h3></span></b>
								<span style="color: #9290B0">Groups</span>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>				
  		</div> <!-- end of main col-md-12 -->
		
		
			
		<div class="col-md-4">			
			<div class="action-group box box-white">						
				<ul class="list-group content-li content-li-menu">
					<li class="list-group-item"><a href="#"> Profile Overview<span class="fa fa-chevron-right"></span></a></li>
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/progress/'. $member->hidId."/".$member->id) ?>"> Progress<span class="fa fa-chevron-right"></span></a></li>
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/member/'. $member->hidId."/".$member->id) ?>"> Activity<span class="fa fa-chevron-right"></span></a></li>
				 </ul>				
			</div>
			
			<div class="panel">
			  <div class="panel-heading panel-dark">
				<h3 class="panel-title">Lecturer </h3>
			  </div>
			   
			  <div class="box box-white row post">	
				<img src="<?php if($mkd->foto) echo $this->location($mkd->foto); else  echo $this->location('modules/elearning/assets/pic/user.jpg') ?>" class="img-avatar" />&nbsp;
				<span class="text text-info"> <?php echo $mkd->nama; ?></span>
			  </div>
			</div>			
		</div>
		
      	<div class="col-md-8">
			<div class="row">		
				<?php
				$mconf = new model_general();
								
				$data['post'] 		= $mconf->get_post_assign($member->hidId);
				$data['progress']	= $mconf->get_progress($member->hidId, $memberid);
				$data['persentase'] = (count($data['progress']) / count($data['post'])) * 100;
				
				$data['mkid'] = $member->hidId; $data['namamk']= $mkd->keterangan; //echo $this->view('home/view-progress.php', $data);?>
					<div class="col-md-12">	
						<div class="row post box box-white">
							<h3><?php echo $data['namamk']; ?></h3>
							<h4><?php echo $data['persentase']  ?> % <small style="color:#ddd"> Grade </small></h4>
							<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
						
						</div>
						<div class="clearfix message-footer no-shadow" style="margin-top: 0px; padding-left:10px;padding-bottom:20px;">
							<div class="subtext pull-right"><h4><?php echo count($data['progress'])."/".count($data['post']);?></h4></div>
							<div class="pull-left">
								<h4>Total</h4>
							</div>
						</div>
						
						<div class="row clearfix post box box-white shadow-center radius-bottom" style="margin-top:-10px;">	
							<ul class="list-box">
								<?php					
								if($data['post']):
									$series="";
									foreach($data['post'] as $dt):
										$progress= $mconf->get_progress_mhs($member->hidId, $memberid, $dt->post_id);
										
										?>
											<li><?php $isi = explode("|",$dt->isi); echo $isi[0]; ?></li>
										<?php
										 $kat[] = "'".$isi[0]."'";										 
										 if($progress): $series = $isi[0]; $skor[] =($progress->skor/$progress->total_skor)*100;									 
										 else: $series =$series; $skor[] =0;	
										 endif;										 
									endforeach;
								endif;
								?>
								</ul>													
						</div>
					</div>
			</div>
			<div class="row"><p>&nbsp;</p></div>
		</div>
		
		<div class="clearfix"></div>
	</div> <!-- end of main row -->
</div> <!-- end of container -->
<script src="<?php echo $this->location('modules/elearning/assets/bootstrap/js/jquery-1.10.2.min.js') ?>"></script>
<script src="<?php echo $this->location('modules/elearning/assets/js/general/highcharts.js') ?>"></script>
<script src="<?php echo $this->location('modules/elearning/assets/js/general/exporting.js')?>"></script>

<script>
	$(function () {
	$('#container').highcharts({
		
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: [								
				<?php echo join($kat, ',') ?>               
			]
		},
		yAxis: {
			min: 0,
			max:100,
			title: {
				text: '%'
			}
		},
		tooltip: {
			pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                shared: true
		},
		plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
		
		series: [{
			name:'<?php echo $series ?>',
			data:[<?php echo join($skor, ',')?>]
		}]
	});
});

</script>
	</body>
</html>
<?php 
	//echo $this->view('footer.php');	
?>


