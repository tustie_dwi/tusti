<div class="box box-white" style="height: auto" id="view_content">	        		
	<div class="head-box">	
		<div class="row">		
        	<div class="col-md-12">	
				<h3 class="title-box">Syllabus</h3>	
        	</div>			
		</div>
		<?php 
		$mconf = new model_course;
		$komponen = $mconf->get_komponen_silabus();
		?>
		<div class="action-group" style="margin-left: 5px;">
			  <div id="accordion">
				  	<div class="panel panel-success">
					  	<?php foreach($komponen as $dt): ?>
							    <div class="panel-heading" style="border-radius: 0px;">
							      <h4 class="panel-title" style="color: black">
							      	<span class="collapse-title"><?php echo $dt->keterangan; ?></span>
							      	<?php if($dt->keterangan!="Course Overview"){ ?>
							      	<small><a class="btn-collapse pull-right" data-toggle="collapse" href="#<?php echo $dt->komponen_id ?>">show</a></small>
							      	<?php } ?>
							      </h4>
							    </div>
							    <?php if($dt->keterangan!="Course Overview"){ ?>
							    <div id="<?php echo $dt->komponen_id ?>" class="panel-collapse collapse">
						    	<?php }else{ ?>
					    		<div id="<?php echo $dt->komponen_id ?>" class="panel-collapse collapse in">
					    		<?php } ?>
							      <div class="panel-body">
							      <?php if($role!="mahasiswa"){ ?>
							      	  <form role="form" method="post" id="silabus-form<?php echo $dt->komponen_id ?>" enctype="multipart/form-data" action="<?php echo $this->location('module/elearning/course/syllabus'); ?>">
										  <input type="hidden" id="hidid" name="hidid" value="<?php echo $mkd->mkditawarkan_id; ?>">
										  <input type="hidden" id="jadwalid" name="jadwalid" value="<?php echo $jadwal_id; ?>">
										  <input type="hidden" id="mkid" name="mkid" value="<?php echo $mkd->hidId; ?>">
								          <div id="form-content-silabus">
								          	<input type="hidden" name="cmbkomponen" value="<?php echo $dt->komponen_id ?>">
								          	<?php if($dt->keterangan!="Course Overview"){ ?>
											  <div class="form-group">
												<div id="label-silabus<?php echo $dt->komponen_id ?>">
													<p class="silabus<?php echo $dt->komponen_id ?>"></p>
													<button type="button" data-uri="<?php echo $dt->komponen_id ?>" class="btn btn-primary" name="edit_sil">Edit</button>
												</div>
												<div id="form-silabus<?php echo $dt->komponen_id ?>">
													<label>Note</label>
													<textarea class="form-control ckeditor" name='silabus' id="silabus<?php echo $dt->komponen_id ?>"></textarea>
													<input type="hidden" name="silabusid" value="">
													<input type="hidden" name="detailid" value="">
												</div>
											  </div>
											  <?php if($role!="mahasiswa"): ?>		
											  	<button type="submit" class="btn btn-primary"  name="b_silabus<?php echo $dt->komponen_id ?>" value="Save & Publish" onclick="submit_silabus_content('<?php echo $dt->komponen_id ?>')">Save & Publish</button>
											  	<button type="button" class="btn btn-info cancel" data-uri="<?php echo $dt->komponen_id ?>" name="b_cancel<?php echo $dt->komponen_id ?>">Cancel</button>
											  <?php endif; ?>
										  </div>
									  </form>
									   <?php }else{ ?>
								   	  	<?php										
								   	  		$this->view('course/topic.php', $data);
								   	  	?>
									   <?php } ?>
								<?php }else{ ?>
											<input type="hidden" id="jadwalid" name="jadwalid" value="<?php echo $jadwal_id; ?>">
										    <input type="hidden" id="mkid" name="mkid" value="<?php echo $mkd->hidId; ?>">
											<?php if($dt->keterangan!=="Course Overview"){ ?>
											<p class="silabus<?php echo $dt->komponen_id ?>"></p>
											<?php }else{ 										
										   	  		$this->view('course/topic.php', $data);
										   	      } ?>
								<?php } ?>
							      </div>
							    </div>
					    <?php endforeach; ?>
				    </div>
			  	  </div>
			</div>	
		</div>	
	</div>
</div>

