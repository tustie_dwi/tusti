<div class="box box-white" style="height: auto" id="view_content">	        		
	<div class="head-box">		
			<div class="row">
            	<div class="col-md-12">
					<h3 class="title-box" ><?php echo ucWords($straction) ?> File</h3>
                </div>
			</div>
			<?php
			
			
			$mconf = new model_course(); 
			$file = $mconf->get_file("",  $materi_id, $file_id);
											
			$materi = $mconf->get_materi($mkd->mkditawarkan_id);
			$status	= $mconf->get_statusmateri();
			if($straction=='edit') $posts  = $mconf->get_materi($mkd->mkditawarkan_id,"", $materi_id);
			if($straction=="add sub") $post  = $mconf->get_materi($mkd->mkditawarkan_id,"", $materi_id);
				
			?>
			<div class="action-group" style="margin-left: 10px;">				
				<form  method="post" id="upload-form-edit-file" action="<?php echo $this->location('module/elearning/course'); ?>" enctype="multipart/form-data">						
					
					<div class="form-group">	
						<label></label>
						<input type="text" class="form-control" name='judul' id='judul' required="required" value="<?php if(isset($file->judul)) echo $file->judul; ?>" placeholder="Title">
					</div>
					 
					 <div class="form-group">	
						<label>Note</label>
						<textarea class="form-control ckeditor" name='keterangan' id='keterangan'><?php if(isset($file->keterangan)) echo $file->keterangan; ?></textarea>
					</div>
					
					<div class="form-group">	
						<label>File</label>							
						<?php if(!isset($new)){
							if(isset($file->file_loc)){ ?>
								<div class='well'>
									<?php echo $file->file_name ?> *)
								</div>
							<?php } else { ?>
								<div class='well'>
									*) File belum dilampirkan
								</div>
							<?php } 
						}?>
						<input type="file" class="form-control" name="uploads" id="uploads">										
					</div>
					
					 <div class="form-group">							
							
							<input type="hidden" name="hidfileId" value="<?php if(isset($file->file_id)) echo $file->file_id;?>">
							<input type="hidden" name="jenis_file_id" value="<?php if(isset($file->jenis_file_id)) echo $file->jenis_file_id;?>">
							<input type="hidden" name="materi_id" value="<?php if(isset($file->materi_id)) echo $file->materi_id;?>">
							<input type="hidden" name="mkditawarkan_id" value="<?php if(isset($file->mkditawarkan_id)) echo $file->mkditawarkan_id;?>">
							<input type="hidden" name="file_name" value="<?php if(isset($file->file_name)) echo $file->file_name;?>">
							<input type="hidden" name="file_type" value="<?php if(isset($file->file_type)) echo $file->file_type;?>">
							<input type="hidden" name="file_loc" value="<?php if(isset($file->file_loc)) echo $file->file_loc;?>">
							<input type="hidden" name="file_size" value="<?php if(isset($file->file_size)) echo $file->file_size;?>">
							<!-- <input type="hidden" name="file_content" value="<?php //if(isset($file->file_content)) echo $file->file_content;?>"> -->
							<input type="hidden" name="tgl_upload" value="<?php if(isset($file->tgl_upload)) echo $file->tgl_upload;?>">
							<input type="hidden" name="upload_by" value="<?php if(isset($file->upload_by)) echo $file->upload_by;?>">
							<!-- <input type="hidden" name="is_publish" value="<?php if(isset($file->is_publish)) echo $file->is_publish;?>"> -->
							<input type="hidden" name="id" value="<?php if(isset($file->id)) echo $file->id;?>">
							<input type="hidden" name="edit" value="1">
							<input  type="submit" id="btn-submit" name="b_savepublish" value="Save and Publish" onclick="submit_edit_file_content('publish','<?php echo $mkd->hidId ?>')" class="btn btn-primary">
							<input  type="submit" id="btn-submit" name="b_draft" value="Save as Draft" onclick="submit_edit_file_content('draft','<?php echo $mkd->hidId ?>')" class="btn btn-warning">
							
							<span id="status-save" style="margin-left:1em;">&nbsp;</span>    
											
					</div>				
				</form>
		</div>		
	</div>	
</div>
