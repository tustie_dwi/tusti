
		<?php 
		
		// if($materi_id){
			// $materi_id = $materi_id;
		// }else if(isset($_POST['materi_id'])){
			// $materi_id = $_POST['materi_id'];
			// //$view_silabus = 1;
		// }
		
		if($materi_id):
		if(!isset($view_silabus)){
			
		?>
		<div class="box box-white" style="height: auto" id="view_content">	        		
		<div class="head-box">
		<?php 
		} 
		?>
		<div class="action-group" style="margin-left: -8px;margin-right: -8px">
		<?php
				$mconf = new model_course();
											
				$materi = $mconf->get_materi($mkd->mkditawarkan_id,"", $materi_id);
				
				if($file_id)	$rfile	= $mconf->get_file("", "",$file_id, $role);
				//else if (isset($_POST['materi_id'])) $file	= $mconf->get_file($mkd->mkditawarkan_id, $_POST['materi_id'],"", $role);				
				else $file	= $mconf->get_file($mkd->mkditawarkan_id, $materi_id,"", $role);
				
				
				
				
				if(!isset($view_silabus)){
				if(isset($materi) && ($materi)){
					$judul=$materi->judul;
					?>	
					<h3><?php echo $judul;?>
					<?php
					if($role!='mahasiswa'){
					?>
						<a style='cursor:pointer' onclick="edit_materi('<?php echo $jadwal_id ?>','<?php echo $materi->mk ?>','<?php echo $materi->id ?>');" class="btn btn-primary" style="margin:0px 5px">
						<i class="fa fa-pencil"></i> Edit Materi</a><?php
					}
					?>
					</h3>
					<p><?php echo $materi->keterangan;?></p>
				<?php }
				} 						
				if(isset($file) && ($file)):	
					?>
					<h4 style="margin-left:10px">File</h4>	
					<table class="table file-materi">
						<thead style="display: none">
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<?php $uri_loc = $this->location('module/elearning/course/del_file_topic'); ?>
						<?php
						foreach($file as $dt):								
						?>
							<tr id="del_materi<?php echo $dt->file_id ?>">
								<td><small><?php echo $dt->jenis;?></small></td>
								<td><a style='cursor:pointer' onclick="view_file('<?php echo $jadwal_id ?>','<?php echo $mkd->hidId ?>','<?php echo $materi_id ?>','<?php echo $dt->id ?>')"><?php	echo $dt->judul;?></a></td>	
								<td><small><?php if($dt->is_publish=='1'){ echo "Published"; } ?> </small></td>
								<td>
								<?php if($role!="mahasiswa"){ ?>
								<a  href="#" onclick="do_del_topic('<?php echo $dt->file_id ?>')"><i class='fa fa-trash-o'></i> </a>		
								<input type='hidden' class='deleted<?php echo $dt->file_id ?>' value="<?php echo $dt->file_id ?>" uri = "<?php echo $uri_loc ?>"/>
								<?php } ?>
								</td>										
							</tr>
							<?php								
						endforeach;
						?>						
						</tbody>
					</table>
					<?php
					endif;
						
						
					if(isset($rfile) && ($rfile)):
						echo "<h3>".$rfile->judul."";
						if($role!='mahasiswa'){
						?>
							<a style='cursor:pointer' onclick="edit_file('<?php echo $jadwal_id ?>','<?php echo $materi->mk ?>','<?php  echo $materi->id ?>', '<?php  echo $rfile->id; ?>')" class="btn btn-primary" style="margin:0px 5px">
								<i class="fa fa-pencil"></i> Edit File</a>
						<?php
						}
						
						echo "</h3>";
						echo "<p>".$rfile->keterangan."</p>";
						switch ($rfile->jenis){
							case 'Video':
								echo '<video width="480" height="360" id="video" preload="none" title="'.$rfile->judul.'" controls>
											<source src="'.$this->asset($rfile->file_loc).'" type="video/webm">
											<source src="'.$this->asset($rfile->file_loc).'" type="video/mp4">
											<source src="'.$this->asset($rfile->file_loc).'" type="video/ogg">
										</video>';
								break;
							
							case 'Image':
								echo '<img src="'.$this->asset($rfile->file_loc).'" width="480" height="360" class="img-thumbnail" />';
								break;
								
							case 'Presentation' || 'Document' || 'Spreadsheet':
								echo '<iframe src="https://docs.google.com/gview?url='.$this->asset($rfile->file_loc).'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';		
								
								break;
						}

						echo "<p><i class='fa fa-calendar'></i> ".date("M d, Y", strtotime($rfile->tgl_upload))."&nbsp; <i class='fa fa-clock-o'></i> ".date("H:i", strtotime($rfile->tgl_upload))." by ".$rfile->upload_by."</p>";
						echo '<a onclick=doDownload("'.$rfile->id.'") href="'.$this->location('module/elearning/course/download/'.$rfile->file_name).'" class="btn btn-default"><i class="fa fa-download"></i> Download This File</a>';
					endif;
				?>
			</div>
		<?php if(!isset($view_silabus)){ ?>
			</div>
	</div>
		<?php } ?>
		<?php
		else:?>
			<div class="row">
				<div class="col-md-6">
					
					<div class="form-group">
						<?php
							$mconf = new model_course(); 
							$topic_parent = $mconf->get_materi($mkd->mkditawarkan_id,'0');
						?>
						<select class="form-control" name="unit_course">
									<option value="0">Please Select</option>
							<?php $z=1;
								  foreach($topic_parent as $tp){ ?>
									<option value="<?php echo $tp->materi_id ?>" data-i="<?php echo $z ?>"
										<?php 
											if(isset($_POST['materi_id'])&&$tp->materi_id==$_POST['materi_id']){
												echo "selected";
											}
										?>
									>Unit <?php echo $z." ".ucwords($tp->judul); ?></option>
							<?php $z++;
								  } ?>
						</select>
					</div>	
				</div>
				<?php if($role!='mahasiswa') { ?><div class="col-md-6 pull-right">			
					<a title="create new topic" href="#" style="cursor:pointer" class='btn btn-primary pull-right'  onclick="return add_materi('<?php echo $jadwal_id ?>','<?php echo $mkd->hidId ?>');"><i class="fa fa-plus"></i> Add New</a>				
				</div>
				<?php } ?>
				
			</div>
            <div class="row">
			<div class="action-group">
				<table class='table table-nobordered'>
				<tbody>
					<?php
					if(isset($_POST['materi_id'])){
						$mat_id=$_POST['materi_id'];
					}else $mat_id="";		
					$topic = $mconf->get_materi($mkd->mkditawarkan_id,"","",$mat_id);

					if($topic){	
						
						$i=0;
						if(isset($_POST['nomer'])){
							$a=$_POST['nomer'];
						}else{
							$a=1;
						}
							
						foreach($topic as $dt):
						
							if($dt->parent_id=='0'){
								?>
								<tr id="del_materi<?php echo $dt->materi_id ?>" class="nav-syllabus" data-syllabus="<?php echo $i;?>">
									<?php $uri_location = $this->location('module/elearning/course/del_topic'); ?>
									<td>         
										<ul class='nav nav-pills' style='margin:0;'>
											<li class='dropdown'>
											  <a class='dropdown-toggle link-syllabus'  data-syllabuschild="<?php echo $i+1; ?>" id='drop4' role='button' data-toggle='dropdown' href='#'><?php	echo $a.". ".$dt->judul;?> </a>
											  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
												<li>
												<?php if($mat_id==""){ ?>
												<a style='cursor:pointer' onclick="get_materi('<?php echo $dt->materi_id ?>','<?php echo $a?>')"><i class='fa fa-search'></i> View</a>
												<!-- <a style='cursor:pointer' onclick="view_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-search'></i> View</a> -->
												<?php } ?>
												</li>
												<?php
												if($role!='mahasiswa'){
												?>
												<?php if($mat_id==""){ ?>
												 <li class="divider"></li>
											    <?php } ?>
												<li>
													<a style='cursor:pointer' onclick="add_sub_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-plus'></i> Add Sub Topic</a>	
												</li>
												<li>
													<a style='cursor:pointer' onclick="add_file_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-file-o'></i> Add File</a>
												</li>
												 <li class="divider"></li>
													<li><a style='cursor:pointer' onclick="edit_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-pencil'></i> Edit</a></li>
												<li><a class='btn-edit-post' href="#" onclick="do_del_topic(<?php echo $dt->materi_id ?>)"><i class='fa fa-trash-o'></i> Delete</a>		
												<input type='hidden' class='deleted<?php echo $dt->materi_id ?>' value="<?php echo $dt->materi_id ?>" uri = "<?php echo $uri_location ?>"/>				
												</li>
												<?php
												}
												?>											
											  </ul>
											</li>
										</ul>
										<?php if($mat_id!=""){ ?>
											<?php 
												echo $dt->keterangan;
												$data['materi_id'] = $dt->materiid;
												$data['view_silabus'] = 1;
												$this->view('course/topic.php', $data);
											?>
										<?php } ?>
									</td>								
								</tr>
								<?php
								$a++;									
								echo $this->detailmateri($dt->mkditawarkan_id, $dt->materi_id, $a-1, $i, $jadwal_id, $mat_id, $data);
							}
						
						endforeach;
						}else{
							echo "<tr><td>Sorry, no content to show</td></tr>";
						}
						?>
						
					</tbody>
				</table>
			</div>
            </div>
	<?php endif; ?>			
		


