<?php echo $this->view('header.php',$data) ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<?php if($mkd):?>
		<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
	        	<div class="box box-white" style="height: auto">
	        		
	        		<div class="head-box">
	        			<div class="row">
	        				<div class="col-sm-2"><img src="<?php echo $this->config->default_pic ?>" class="img-avatar" /></div>
	        				<div class="col-sm-10">
	        					<div class="" id="nick-wrap" style="margin-left: 10px; margin-top: 2px">
									<span class="text-left"><?php echo $mkd->keterangan; ?></span>
									<span style="color: #9290B0"><br>Course</span>
								</div>
	        				</div>
	        			</div>
						<div class="action-group">
							<ul class="list-group content-li">
								<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/groups/'. $mkd->hidId) ?>"><span class="fa fa-comment-o"></span> Posts<span class="fa fa-chevron-right"></span></a></li>
								<!--<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/topic/'. $mkd->hidId) ?>"><span class="fa fa-file-o"></span> Topic<span class="fa fa-chevron-right"></span></a></li>-->
								<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/syllabus/'. $mkd->hidId) ?>"><span class="fa fa-paperclip"></span> Syllabus<span class="fa fa-chevron-right"></span></a></li>
								<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/folder/'. $mkd->hidId) ?>"><span class="fa fa-folder-o"></span> Folder<span class="fa fa-chevron-right"></span></a></li>
								<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/member/'. $mkd->hidId) ?>"><span class="fa fa-users"></span> Members<span class="fa fa-chevron-right"></span></a></li>
							 </ul>
						</div>
	          		</div>
					
	        	</div>
	      	</div>
	      			 	
		</div> <!-- end of main row -->
  		</div> <!-- end of main col-md-3 -->
      	<div class="col-md-8">
			<div class="row">
				<?php 			
			
				if($kategori):	
					
					switch($kategori){
						case "topic":
							if($file_id):
								if($straction=='view'):
									echo $this->view('course/topic.php', $data);
								else:
									echo $this->view('course/topic-file-edit.php', $data);	
								endif;
							else:
								if($straction)	echo $this->view('course/topic-edit.php', $data);
								else echo $this->view('course/topic.php', $data);
							endif;
						break;
						case "syllabus":
							echo $this->view('course/syllabus.php', $data);
						break;
						case "folder":
							echo $this->view('course/folder.php', $data);
						break;
						case "member":
							if($memberid) echo $this->view('course/detail_member.php', $data);
							else echo $this->view('course/member.php', $data);
						break;	
						case "progress":
							echo $this->view('course/progress_member.php', $data);
						break;	
											
					}	
				else:
					?>				
					<div class="col-md-12" style="margin-bottom: 20px">
						<?php 
							if($role == 'dosen') $this->view('home/post-bar.php', $data);
							else $this->view('home/note-bar.php', $data);
						?>
					</div>
					<div class="view-content">
						<?php echo $this->view('home/view-content.php', $data);?>
					</div>
					<div class="col-md-12" style="padding: 15px; padding-top: 0px;">
						<input id="page" value="1" type="hidden"/>
						<button type="button" class="btn btn-info btn-sm btn-block btn-more-post" onclick="more_post_group('<?php echo $mkid ?>')">
							<i class="fa fa-chevron-down"></i> Show more
						</button>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php 
		else:
			echo '<div class="span12">
					<div class="well">
						<h4>Sorry, no content to show</h4>					   
					</div>
				</div>';
		endif; ?>
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	echo $this->view('footer.php');
	
	
?>

<!-- Modal Attach Link -->
<div class="modal fade" id="modal-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-link"></i> Attach Link</h4>
      </div>
      <form role="form">
      	  <input type="hidden" id="tab-link"/>
	      <div class="modal-body">
			  <div class="form-group">
			    <input type="url" class="form-control" placeholder="Enter URL" required id="add_url">
			  </div>
	      </div>
	      <div class="modal-footer">
	        <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
	        <a onclick="get_new_url()" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Attach</a>
	      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Attach Media Library -->
<div class="modal fade" id="modal-library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive"></i> Attach Library
	    		<span class="pull-right loading"><i class="fa fa-refresh fa-spin"></i> Loading</span>
	    	</h4>
	    </div>
    	<div class="modal-body" style="padding: 20px 0px">
    		<div class="row folder-wrap" style="margin: 0 10px; padding: 0px; padding-right: 10px">
    			<input id="folder_back_parent_id" value="0" type="hidden"/>
	    		<?php if($library):
				foreach($library as $key){ ?>
			  		<div class="col-md-3" style="padding: 0px 5px 0px 5px">
			  			<button title="<?php echo $key->folder_name ?>" class="btn btn-default" style="width: 100%; margin: 5px;" onclick="get_folder('<?php echo $key->id ?>')">
			  				<i class="fa fa-folder fa-3x"></i><br><?php echo $key->folder_name ?>
			  			</button>
			  		</div>
			  	<?php } endif; ?>
		  	</div>
      	</div>
      	<div class="modal-footer">
      		<a type="button" class="btn btn-primary btn-sm pull-left" onclick="folder_back()"><i class="fa fa-arrow-circle-left"></i> Back</a>
      		
        	<a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
        	<a onclick="add_library()" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Attach</a>
      	</div>
    </div>
  </div>
</div>









