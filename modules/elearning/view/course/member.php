<div class="box box-white" style="height: auto">	        		
	<div class="head-box">
		<div class="row"  style="margin-left: 5px;">
			<div class="col-md-6">
				<h3>Members</h3>
			</div>
			<div class="col-md-6 pull-right" style="margin-top:20px">
				<select class="e9" name="jadwal" required="required" id="select_jadwal">
					<option value='0'>Select Group</option>
					
					<?php 
						foreach($jadwal as $dt):
							echo "<option class='sub_".$dt->jadwal_id."' data-mk='".$dt->mkid."' value='".$dt->jadwal_id."'";
							echo ">".$dt->jadwal."</option>";
						endforeach;
					?>					
				</select>
			</div>
		</div>
		
		
		<div class="action-group view-member" style="margin-left: 5px;">
			<ul class="list-group content-li member-li">
				 
				<?php 
				
				$mconf = new model_general(); 
										
				$member = $mconf->get_jadwal_mhs("", $mkd->hidId,"",$jadwal);
				if($member):
					foreach($member as $dt):
					?>
						<li class="list-group-item">
							<div class="row">							
								<div class="col-md-8">
									<div class="col-sm-2">
										<img src="<?php if($dt->foto) echo $this->location($dt->foto); else  echo $this->location('modules/elearning/assets/pic/user.jpg') ?>" class="img-avatar" />
									</div>
									<div class="col-sm-10">
										<a href="<?php echo $this->location('module/elearning/course/member/'. $mkd->hidId.'/'.$dt->id) ?>"> <?php echo $dt->nama; ?></a>
										<small>Student
										<br><?php echo $dt->mahasiswa_id; ?>
										</small>
									</div>
								</div>
								<div class="col-md-2"><a href="<?php echo $this->location('module/elearning/course/member/'. $mkd->hidId.'/'.$dt->id) ?>">Progress</a></div>
								<div class="col-md-2"><a href="#">More</a></div>
							</div>
						</li>
					<?php
					endforeach;	
				else: echo '<li class="list-group-item">Sorry, there is no content to show.</li>';
				endif;				
				?>
				 
			 </ul>
			 </div>
		</div>
	</div>
</div>