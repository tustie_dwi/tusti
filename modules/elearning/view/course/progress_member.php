<?php echo $this->view('header.php',$data) ?>
<script src="<?php echo $this->location('modules/elearning/assets/bootstrap/js/jquery-1.10.2.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->location('modules/elearning/assets/moris/morris.css'); ?>">

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-white" style="height: auto">
				<div class="head-box">
					<div class="row">
					<div class="col-md-2"><img src="<?php if($member->foto) echo $this->location($member->foto); else  echo $this->location('modules/elearning/assets/pic/user.jpg') ?>" class="img img-thumbnail" /></div>
					<div class="col-md-10">	
						<div class="row">
							<h2><?php echo $member->nama; ?></h2>
							<div class="profile-desc">Student</div>									
						</div>
						<div class="row">
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php $jmlpost = count($post); $jmlcomment= count($comment); $tpostreplies=$jmlpost+$jmlcomment; ?><?php echo $tpostreplies;?></h3></span></b>
								<span style="color: #9290B0">Post & Replies</span>
							</div>
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php $jmlgroup = count($groups); ?><?php echo $jmlgroup;?></h3></span></b>
								<span style="color: #9290B0">Groups</span>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>				
  		</div> <!-- end of main col-md-12 -->
		
		
			
		<div class="col-md-4">			
			<div class="action-group box box-white">						
				<ul class="list-group content-li content-li-menu">
					<li class="list-group-item"><a href="#"> Profile Overview<span class="fa fa-chevron-right"></span></a></li>
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/progress/'. $member->hidId."/".$member->id) ?>"> Progress<span class="fa fa-chevron-right"></span></a></li>
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/member/'. $member->hidId."/".$member->id) ?>"> Activity<span class="fa fa-chevron-right"></span></a></li>
				 </ul>				
			</div>
			
			<div class="panel">
			  <div class="panel-heading panel-dark">
				<h3 class="panel-title">Lecturer </h3>
			  </div>
			   
			  <div class="box box-white row post">	
				<img src="<?php if($mkd->foto) echo $this->config->file_url_view.'/'.$mkd->foto; else  echo $this->config->default_pic ?>" height="50" class="img-avatar" />&nbsp;
				<span class="text text-info"> <?php echo $mkd->nama; ?></span>
			  </div>
			</div>			
		</div>
		
      	<div class="col-md-8">
			<div class="row">		
				<?php
				$mconf = new model_general();
								
				$data['post'] 		= $mconf->get_post_assign($member->hidId);
				$progres_mhs	= $mconf->get_progress($member->hidId, $memberid);
				$data['persentase'] = ($progres_mhs->total / count($data['post'])) * 100;
				$progres = $mconf->get_post_assign($member->hidId);
				
				$data['mkid'] = $member->hidId; $data['namamk']= $mkd->keterangan; //echo $this->view('home/view-progress.php', $data);?>
					<div class="col-md-12">	
						<div class="row post box box-white">
							<h3><?php echo $data['namamk']; ?></h3>
							<h4><?php echo $data['persentase']  ?> % <small style="color:#ddd"> Grade </small></h4>
							<!-- <div id="tes" style="min-width: 310px; height: 400px; margin: 0 auto"></div> -->
							<div class="chart" id="container-chart" style="height: 300px;"></div>
						</div>
						<div class="clearfix message-footer no-shadow" style="margin-top: 0px; padding-left:10px;padding-bottom:20px;">
							<div class="subtext pull-right"><h4><?php echo $progres_mhs->total."/".count($data['post']);?></h4></div>
							<div class="pull-left">
								<h4>Total</h4>
							</div>
						</div>
						
						<div class="row clearfix post box box-white shadow-center radius-bottom" style="margin-top:-10px;">	
							<ul class="list-box">
								<?php					
								/*if($data['post']):
									$series="";
									foreach($progres as $dt):
										$progress= $mconf->get_progress_mhs($member->hidId, $memberid, $dt->post_id);
										
										?>
											<li><?php $isi = explode("|",$dt->isi); echo $isi[0]; ?></li>
										<?php
										 $kat[] = "'".$isi[0]."'";										 
										 if($progress): $series = $isi[0]; $skor[] =($progress->skor/$progress->total_skor)*100;									 
										 else: $series =$series; $skor[] =0;	
										 endif;										 
									endforeach;
								endif;*/
								if($progres):
									foreach($data['post'] as $key){
										$progress= $mconf->get_progress_mhs($member->hidId, $memberid, $key->id);
										?>
										<li><div class="subtext pull-right"><?php 
										if($progress): 
										$nilai = explode('-', $progress->nilai); echo round($nilai[0],0)."/".round($nilai[1],0); 
										else: echo "0"; endif; ?></div>
											<div class="pull-left"><?php echo ucfirst($key->judul); ?></div></li>
										<?php
									}
								endif;
								?>
								</ul>													
						</div>
					</div>
			</div>
			<div class="row"><p>&nbsp;</p></div>
		</div>
		
		<div class="clearfix"></div>
	</div> <!-- end of main row -->
</div> <!-- end of container -->

<script>
    $(function() {
        "use strict";
        // AREA CHART
        var donut = new Morris.Bar({
            element: 'container-chart',
            resize: true,
            data: [
            	<?php
					foreach($progres as $dt){
						$key= $mconf->get_progress_mhs($member->hidId, $memberid, $dt->id);
						if($key):
							$nilai = explode('-', $key->nilai);
							$skor = ($nilai[0]/$nilai[1]) * 100;
							$persentaseskor = round($nilai[0],0)."/".round($nilai[1],0);
						else:
							$skor = 0;
							$persentaseskor = 0;
						endif;
						if($skor == 0) $dt->tgl_mulai = 'Unsubmitted';
						echo "{x: '".ucfirst($dt->judul)." : ". $dt->tgl_mulai ."', y: '".$persentaseskor."', value: ".$skor."},";
					}
				?>
            ],
            barColors: ['#4AA3DF'],
            xkey: 'x',
            xLabels: 'y',
            ykeys: ['value'],
            hideHover: 'auto',
			postUnits: '%',
			labels: ['Value '],
            ymax:100,
			ymin:0

        });
	});
</script>
<script type="text/javascript" src="<?php echo $this->location('modules/elearning/assets/moris/raphael-min.js') ?>"></script>
<script type="text/javascript" src="<?php echo $this->location('modules/elearning/assets/moris/morris.min.js') ?>"></script>
<?php 
	echo $this->view('footer.php');	
?>


