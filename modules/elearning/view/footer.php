<!-- script references -->
		<script src="<?php echo $this->location('modules/elearning/assets/bootstrap/js/jquery-1.10.2.min.js') ?>"></script>
		<script src="<?php echo $this->location('modules/elearning/assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?php echo $this->location('assets/ckeditor/ckeditor.js') ?>"></script>
		<?php $scripts = $this->get_scripts(); 
		foreach( $scripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
		<?php endforeach; ?>
		<script>			
		 $('.select2, .select2-multiple').select2();			 
		</script>		
	</body>
</html>