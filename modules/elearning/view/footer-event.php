<!-- script references -->
		<script src="<?php echo $this->location('modules/elearning/assets/jquery/jquery.min.js') ?>"></script>
		<script src="<?php echo $this->location('modules/elearning/assets/jquery/jquery-ui-1.8.16.custom.min.js') ?>"></script>
		<script src="<?php echo $this->location('modules/elearning/assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?php echo $this->location('assets/ckeditor/ckeditor.js') ?>"></script>		
		<?php $scripts = $this->get_scripts(); 
		foreach( $scripts as $s) : ?><script src="<?php echo $this->location($s); ?>"></script>
		<?php endforeach; ?>
		<script>			
		 $('.select2, .select2-multiple').select2();			 
		</script>
	  
	</body>
</html>