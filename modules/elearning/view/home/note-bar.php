
	
	<div class="box box-white" style="margin-bottom: 0; padding-top: 20px;">
		<form class="form-horizontal" role="form" id="form-note" method="post" action="<?php echo $this->location('module/elearning/home/save_note') ?>">
		  <div class="form-group" id="post-inp">
		    <div class="col-sm-12">
		      <input type="text" class="form-control" placeholder="Type your note here">
		    </div>
		  </div>
		  <span class="inp-note">
			  <div class="form-group">
			    <div class="col-sm-12">
			    	<textarea name="isi" id="isi" class="form-control" placeholder="Type your note here"></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-12">
			    	<select class="e9" name="mkd_id" required="required" id="mkd_id" >
			    		<option value="0">Select Course</option>
			    		<?php if($mkd):						
							if(count($mkd) > 1):
								foreach($mkd as $key) { ?>
								<option value="<?php echo $key->mkditawarkan_id ?>"><?php echo $key->keterangan ?></option>
							<?php } 
							else:
								if(isset($mkd->mkditawarkan_id)):
								?>
								<option value="<?php echo $mkd->mkditawarkan_id ?>"><?php echo $mkd->keterangan ?></option>
								<?php
								else:
									foreach($mkd as $key) { ?>
										<option value="<?php echo $key->mkditawarkan_id ?>"><?php echo $key->keterangan ?></option>
									<?php } 
								endif;
							endif;
						endif;
						?>
			    	</select>
			    </div>
			  </div>
			  <div class="form-group">
			  	<div class="col-md-12" id="link-wrap-note"></div>
			  	<div class="col-md-12" id="attach-wrap-note" style="margin-bottom: 5px"></div>
			  	<div class="col-sm-8">
			  		<a data-toggle="modal" data-target="#modal-link" class="btn btn-success btn-xs pull-left btn-tooltip white" data-toggle="tooltip" data-placement="bottom" title="Attach Link" onclick="tab_val('note')"><i class="fa fa-link"></i></a> 
			  		<a accesskey="a" data-toggle="modal" data-target="#modal-library" style="margin: 0 2px" class="white btn btn-success btn-xs pull-left btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Attach Library" onclick="tab_val('note')"><i class="fa fa-archive"></i></a> 
			  		
			  		<a class="white fileUpload btn btn-success btn-xs pull-left btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Attach File">
					    <i class="fa fa-file-text-o"></i>
					    <input name="file[]" type="file" class="upload" id="uploadBtn" multiple="multiple" />
					</a>
			  	</div>
			  	<div class="col-sm-4">
			      <a id="btn-submit-note" class="btn btn-info pull-right"><i class="fa fa-check-square"></i> Submit</a>
			      <a id="cancel-note" class="btn btn-link pull-right">Cancel</a>
			    </div>
		  	  </div>
	  	  </span>
		</form>
	</div>
	
	