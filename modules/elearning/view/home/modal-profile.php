<?php
?>
<div class="modal fade" id="modalProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-picture-o"></i> Update Photo Profile</h4>
      </div>
      
      <form enctype="multipart/form-data" role="form" method="post" action="<?php echo $this->location('module/elearning/home/update_avatar') ?>">
	      <div class="fileinput fileinput-new" data-provides="fileinput">
	      	<div class="modal-body">
  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
    <img src="<?php if($foto){
    	echo $this->config->file_url_view.'/'.$foto;
    	}else if($foto) echo $this->config->file_url_view.'/'.$foto;
     else  echo $this->config->default_pic;
      ?>">
  </div>
  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
  </div>
  <div>
	      <div class="modal-footer">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span>
    <span class="fileinput-exists">Change</span>
    <input  name="file" type="file"  accept="image/*"></span>
    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    <button type="submit" class="btn btn-primary fileinput-exists"><i class="fa fa-check-square"></i> Save changes</button>
	   </div>
  </div>
</div>
<!-- <div class="modal-body">
	        <div class="form-group">
			    <input name="file" type="file" >
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> Save changes</button>
	      </div> -->
      </form>
    </div>
  </div>
</div>