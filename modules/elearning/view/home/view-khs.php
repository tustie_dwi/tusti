
	<div class="post box row box box-gey ">	
		<h2 class="no-margin">KHS</h2><br>
		
		<form name="frmDosen" id="frmDosen" class="form-horizontal" method="post" role="form" action="<?php echo $this->location('module/elearning/report/khs')?>" >					
						
		 <div class="form-group">
			    <div class="col-sm-12">
					<select name="cmbsemester" onChange='form.submit();' class="e9">
						<option value="-">Semester</option>
						<?php
						foreach($semester as $dt):
							echo "<option value='".$dt->inf_semester."' ";
							if($semesterid==$dt->inf_semester){
								echo "selected";
							}
							echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
						endforeach;
						?>
					</select>
					<input type="hidden" name="mahasiswa_id" value="<?php  echo $mhsid; ?>" >
				</div>
			</div>
		
					
			</form>				
		<div class="clearfix"></div>						
	</div>	
	<div class="clearfix"></div>	
	<div class="row clearfix post box box-white">	
			<?php
			if(isset($post_khs)) :	
			?>
				<table>
					<?php if(isset($identitas)){
							foreach($identitas as $i){ ?>
					<tr>
						<td style="width: 150px;">Name</td>
						<td style="width: 250px;">: <?php echo $i->nama ?></td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 150px;">Angkatan</td>
						<td style="width: 150px;">: <?php echo $i->angkatan ?></td>
					</tr>
					<tr>
						<td style="width: 150px;">NIM</td>
						<td style="width: 150px;">: <?php echo $i->mahasiswa_id ?></td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">Semester</td>
						<td style="width: 150px;">: <?php 
							$j=1;
							if(isset($semesterke)){
								foreach($semesterke as $d){
									if($semesterid==$d->inf_semester) echo $j." (".$this->kekata($j).")";
									$j++;
								}
							}
						
						?>
						</td>
					</tr>
					<?php 	}
						  } ?>
				</table><br><br>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 50px;">No</th>
							<th>Course</th>
							<th>&nbsp;</th>
							<th style="width: 50px; text-align: center;">Class</th>
							<th style="width: 50px;">SKS</th>
							<th style="width: 50px;">Grade</th>
							<th style="width: 100px;">SKS x Grade</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 1;
							$nilai = 0;
							$sks = 0;
							$sks_lulus = 0;
							foreach($post_khs as $dt){ ?>
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo $dt->matakuliah ?></td>
									<td>&nbsp;</td>
									<td style="text-align: center;"><?php echo $dt->kelas ?></td>
									<td style="text-align: center;"><?php echo $dt->sks ?></td>
									<td style="text-align: center;"><?php echo $this->convertTohuruf($dt->nilai_akhir) ?></td>
									<td style="text-align: center;"><?php echo ($dt->sks * $dt->inf_bobot) ?></td>
								</tr>
						<?php $i++;
							  $nilai += ($dt->sks * $dt->inf_bobot);
							  $sks += $dt->sks;
							  if($this->convertTohuruf($dt->nilai_akhir)!='E'){
							  	$sks_lulus += $dt->sks;
							  }
							} ?>
								<tr>
									<td>&nbsp;</td>
									<td><strong>TOTAL</strong></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td style="text-align: center;"><strong><?php echo $sks ?></strong></td>
									<td>&nbsp;</td>
									<td style="text-align: center;"><strong><?php echo $nilai ?></strong></td>
								</tr>
								<tr>
									<td colspan="7"><div class="col-md-6">SKS beban : <?php echo $sks ?></div><div class="col-md-6">SKS lulus : <?php echo $sks_lulus ?></div></td>								
								</tr>
								<tr>
									<td colspan="7"><div class="col-md-6">SKS beban : IP Semester : <?php echo number_format(($nilai/$sks), 2)?></div>
									<div class="col-md-6">IP Kumulatif : <?php $ip_kom = number_format(($this->IPK($mhsid)), 2);
															 if($ip_kom != ""){
																echo number_format(($this->IPK($mhsid)), 2);
															 } else echo "0";
														?></div></td>
									
								</tr>
								
					</tbody>
				</table>
			<?php
			
			endif; 
			?>
	</div>

		