<?php
	$this->view('home/view-comment.php');	
?>
<input id="foto" value="<?php if($foto) echo $this->config->file_url_view.'/'.$foto; else  echo $this->config->default_pic ?>" type="hidden" />		

<ul class="col-md-12" <?php if(!isset($detailmember)){?>style="margin-top_: 20px; "<?php } ?>>
	<?php 
	if($post):
	
	foreach($post as $key) { 
		if($key->user_id):?>
	<?php if($key->foto){$strimg= $this->config->file_url_view.'/'.$key->foto;}else{ $strimg=$this->config->default_pic;} ?>
	<li class="li-post post-box-<?php echo $key->hidId ?>" style="box-shadow: 0 1px 5px 0 rgba(56,61,72,0.21);" data-id="<?php echo $key->hidId ?>">
		<div class="row box box-white post no-shadow" >
			<div class="col-md-1" style="padding: 0px; padding-top: 5px;">
				<img src="<?php echo $strimg ?>" class="img-avatar" />
			</div>
			
			<div class="col-md-11" style="padding-left: 10px; padding-right: 0">
				<span class="text-left"><a class="btn-link-sm">
					<?php 
						if($user_id == $key->user_id) echo "Me";
						else echo $key->user_name;
					?>
					</a> to <a class="btn-link-sm"><i class="fa fa-square"></i> <?php echo $key->mkd ?></a>
					
					<?php if($key->user_id == $user_id)  $is_edit = 1; else $is_edit = 0; ?>
					<?php if($is_edit == 1) { ?>
						<div class="btn-group btn-menu pull-right" id="<?php echo $key->hidId ?>" >
					      <i type="button" class="fa fa-cog dropdown-toggle btn-tooltip" data-placement="top" title="Menu" data-toggle="dropdown" style="color: #383D48; cursor: pointer"></i>
					      <ul class="dropdown-menu" role="menu">
					        <li><a onclick="edit_post('<?php echo $key->hidId ?>')"><i class="fa fa-pencil"></i> Edit</a></li>
					        <li><a onclick="delete_post('<?php echo $key->hidId ?>')"><i class="fa fa-trash-o"></i> Delete</a></li>
					      </ul>
					    </div>
				    <?php } ?>
				</span>
				<span style="color: #383D48"><br>
					<form action="#" role="form" style="margin-top: 5px" class="edit-post form-edit-post-<?php echo $key->hidId ?>" id="<?php echo $key->hidId ?>" method="post"> 
					  <input name="id" value="<?php echo $key->hidId ?>" type="hidden" >
					  <?php 
					  	$isi = $key->isi;
						$tipe = 'note';
					  	if($key->kategori != 'note') {
					  		$post_isi = explode('|', $key->isi);
							$isi = $post_isi[1];
							$tipe = 'assign';
					  ?>
						  <div class="form-group" style="margin-bottom: 5px">
						  	<input name="judul" class="form-control" value="<?php echo $post_isi[0] ?>">
						  </div>
						  <input name="tgl" value="<?php echo $post_isi[2] ?>" type="hidden">
					  <?php } ?>
					  
					  <input value="<?php echo $tipe ?>" name="tipe" type="hidden" />
					  <div class="form-group" style="margin-bottom: 5px">
					  	<textarea name="isi" class="form-control"><?php echo $isi ?></textarea>
					  </div>
					  
					  <div class="form-group">
						  <a type="submit" class="btn btn-info btn-xs btn-edit-post" data-id="<?php echo $key->hidId ?>"><i class="fa fa-check-square"></i> Save</a>
						  <a onclick="cancel_edit_post()" class="btn btn-danger btn-xs"><i class="fa fa-ban"></i> Cancel</a>
					  </div>
					</form>
						
					<?php					
						if($key->kategori == 'assignment'){
							$isi = explode('|', $key->isi); ?>
							<div class="btn-tugas-wrap">
								<a style="margin-bottom: 5px" class='btn btn-info btn-xs' href="<?php echo $this->location('module/elearning/assignment/detail/'.$key->links."/".$key->id_user) ?>"><i class='fa fa-pencil'></i> Turn In Assignment</a> <?php echo $isi[0] ?>
								<span class="pull-right btn-link-sm"><?php convert_date_all($isi[2]); ?></span>
								<br> <?php echo $isi[1]; ?>
							</div>
							<?php
						}
						elseif($key->kategori == 'quiz'){
							$isi = explode('|', $key->isi); ?>
							<div class="btn-tugas-wrap">
								<a style="margin-bottom: 5px" class='btn btn-danger btn-xs' href="<?php echo $this->location('module/elearning/answer/index/'. $key->links) ?>"><i class='fa fa-pencil'></i> Turn In Quiz</a> <?php echo $isi[0] ?>
								<span class="pull-right btn-link-sm"><?php convert_date_all($isi[2]); ?></span>
								<br> <?php echo $isi[1]; ?>
							</div>
							<?php
						}
						else echo "<p>".$key->isi."</p>";
						
						if(! empty($key->attach)){
							foreach(explode(',', $key->attach) as $attach){ ?>
								<div class="btn-tugas-wrap">
									<?php
										$att = explode('|', $attach);
										$file_name = explode('/', $att[0]);
										$file_name = end($file_name);
										
										if($att[1] == 'file' || $att[1] == 'library'){
											$file_name_js = "'" . $file_name . "'";
											$ext =get_file_ext($att[0]);
											switch(strToLower($ext)){
												case 'jpg':
												case 'jpeg':
												case 'png':
												case 'gif':
													$jenisfile = "image";
													echo "<img title='".$file_name."' height='100' src='".$this->config->file_url_view.'/'.$att[0]."'>";
													break;
												case 'doc':
												case 'docx':
													$jenisfile = "document";
													// echo "<img height='100' src='".$this->location('assets/upload/word.jpg')."'></a>";
													echo "<i class='fa fa-file-word-o fa-2x'></i> ". $file_name;
													break;
												case 'ppt':
												case 'pptx':
													$jenisfile = "document";
													// echo "<img height='100' src='".$this->location('assets/upload/ppt.jpg')."'>";
													echo "<i class='fa fa-file-powerpoint-o fa-2x'></i> ". $file_name;
													break;
												case 'pdf':
													$jenisfile = "document";
													// echo "<img height='100' src='".$this->location('assets/upload/pdf.jpg')."'></a>";
													echo "<i class='fa fa-file-pdf-o fa-2x'></i> ". $file_name;
													break;
												case 'xls':
												case 'xlsx':
													$jenisfile = "document";
													// echo "<img height='100' src='".$this->location('assets/upload/excel.jpg')."'></a>";
													echo "<i class='fa fa-file-excel-o fa-2x'></i> ";
													echo $file_name;
													break;
												case 'webm':
												case 'mp4':
													$jenisfile = "video";
													echo ' <div class="flowplayer"><video width="200" style="border: 1px solid #444; background : #333" height="100" id="video" preload="none" controls>
																<source src="'.$this->config->file_url_view.'/'.$att[0].'" type="video/webm">
																<source src="'.$this->config->file_url_view.'/'.$att[0].'" type="video/mp4">
																<source src="'.$this->config->file_url_view.'/'.$att[0].'" type="video/ogg">
															</video></div>';
											}
											
											if($jenisfile != 'video') echo '<i style="cursor: pointer" title="Download" onclick="Download('.$file_name_js.')" class="fa fa-cloud-download pull-right btn-tooltip" data-toggle="tooltip" data-placement="bottom"></i>';
											if($jenisfile != 'video' && $jenisfile != 'image'){
												echo '<a style="color: #383D48" href="'.$this->location('module/elearning/home/document/'. $file_name).'" target="blank">
														<i class="fa fa-clipboard pull-right btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Show"></i>
													 </a>';
											}
											elseif($jenisfile == 'image'){
												echo '<a style="color: #383D48" href="'.$this->location('assets/'. $att[0]).'" target="blank">
														<i class="fa fa-clipboard pull-right btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Show"></i></a>';
											}
											
										}
										elseif($att[1] == 'link'){
											// $tags=getMetaData($att[0]);
											echo "<a class='btn-link' href='".$att[0]."'><i class='fa fa-link'></i> ".$att[0]."</a>";
										}
									?>
								</div>
							<?php }
						}
					?>
				</span>
			</div>
		</div>
		
		<ul class="message-footer clearfix no-shadow" style="margin-top: 0px; padding-left: 20px;">
			<li class="time-ago-no-icon subtext pull-right"><i class="fa fa-calendar-o"></i> <?php convert_date($key->last_update) ?></li>
			<li class="pull-left">
				<a data-postid="<?php echo $key->post_id ?>" class="btn-link-sm btn-comment"><i class="fa fa-comment"></i> Reply</a>
			</li>
		</ul>
		
		<span class="comment-wrap-<?php echo $key->hidId ?>">
			<?php print_comment($key->comment, $user_id, $this->config->file_url_view.'/', $key->hidId) ?>
		</span>
		
		<div class="row box box-gey post no-top-radius no-shadow inp-comment" data-postid="<?php echo $key->post_id ?>">
			<div class="col-md-1" style="padding: 0px"><img src="<?php if($foto) echo $this->config->file_url_view.'/'.$foto; else  echo $this->config->default_pic ?>" class="img-avatar-sm pull-right" /></div>
			<div class="col-md-11">
				<form role="form">
				  <div class="form-group">
					<div class="input-group">
					  <input placeholder="Type your comment here" type="text" class="form-control nilai-comment" postid="<?php echo $key->post_id ?>">
					  <span class="input-group-btn">
						<button data-id="<?php echo $key->hidId ?>" data-postid="<?php echo $key->post_id ?>" class="btn btn-default btn-post-comment" type="button">Send <i class="fa fa-arrow-circle-right"></i></button>
					  </span>
					</div>
				  </div>
				</form>
			</div>
		</div>
		
		
	</li> <!-- end of post -->
	<?php 
			else:
				echo '<li class="li-post" style="box-shadow: 0 1px 5px 0 rgba(56,61,72,0.21);"><div class="row box box-white post no-shadow" >Sorry, there is no content to show.</div></li>';
			endif;
		}
	else:
		echo '<li class="li-post" style="box-shadow: 0 1px 5px 0 rgba(56,61,72,0.21);"><div class="row box box-white post no-shadow" >Sorry, there is no content to show.</div></li>';
	endif; ?>
</ul> <!-- end of ul post -->

      