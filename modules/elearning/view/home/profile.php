<?php echo $this->view('header.php',$data);

$key = $m_general->get_user_profile($user_id);
		$this->add_script('bootstrap/js/jasny-bootstrap.min.js');
?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
	<div class="col-md-12">
			<div class="box box-white full-radius" style="height: auto">
				<div class="head-box">
					<div class="row">
					<div class="col-md-2">
						<a href="#" class="btn-edit-profile" data-toggle="modal" data-target="#modalProfile">
							<figure>
								<img src="<?php if($key->foto) echo $this->config->file_url_view.'/'.$key->foto; else  echo $this->config->default_pic ?>" class="img img-thumbnail" />
							</figure>
							<div class="edit-profile-text">
								<span class="fa fa-camera"></span> Edit
							</div>
						</a>
						<?php 
						$data['key']=$key;
						$data['tmp'] = 'profile';
						
						echo $this->view('home/modal-profile.php',$data) ?>
								
					</div>
					<div class="col-md-10">	
						<div class="">
							<h2><?php echo $key->name; ?></h2>
							<div class="profile-desc"><?php 
							switch($key->role){
								case 'Mahasiswa': echo 'Student'; break;
								case 'Dosen': echo 'Lecturers'; break;
								default: echo $key->role;
							}
							?></div>							
						</div>
						<div class="row">
						<?php if($key->role=='Dosen'): ?>
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php echo $mhs->jml;?></h3></span></b>
								<span style="color: #9290B0">Student</span>
							</div>
						<?php endif; ?>
							<div class="col-md-2" style="text-align:center">
								<b><span class="text text-info"><h3><?php $jmlgroup = count($mkd); ?><?php echo $jmlgroup;?></h3></span></b>
								<span style="color: #9290B0">Groups</span>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>				
  		</div> <!-- end of main col-md-12 -->
  		
  		
  		<div class="col-md-4">
  				<div class="list-group profile-list">
				  <a class="list-group-item" href="#"><i class="fa fa-user fa-fw"></i> Profile Overview</a>
				  <a class="list-group-item" href="#"><i class="fa fa-book fa-fw"></i> Communities</a>
				</div>
  			
		        <div class="panel">
		          <div class="panel-heading panel-dark">
		            <h3 class="panel-title">Suggestion <i class="fa fa-bookmark pull-right"></i></h3>
		          </div>
		             <ul class="list-group content-li">
								<?php if($mkd){ foreach($mkd as $key) { ?>
						    <li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/groups/'. $key->hidId) ?>"  ><?php echo $key->keterangan ?></a></li>
					    <?php }} else { echo ' <li class="list-group-item"><a href="">No content to show</a></li>'; } ?>
					 </ul>
		        </div>
  		</div>
  		<div class="col-md-8">
  			<div class="box box-white full-radius">
  				<h3 class="title-nomargin">About Me</h3>
  				<div></div>  				
  				
  				
  				<hr>
  				<h3>Collections</h3>
  				<hr>
  				<h3>Other</h3>
  			</div>
  		</div>
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	echo $this->view('footer.php');
	
	
?>





