<?php
$mconf = new model_general();
				
$post 		= "";
				
?>

<div class="row box box-white post" >	
	<h3>User Photo</h3>
	
	<form enctype="multipart/form-data" role="form" method="post" action="<?php echo $this->location('module/elearning/home/update_avatar/photo') ?>">	     
		  
			<div class="form-group">
				<div class="fileinput fileinput-new" data-provides="fileinput">
				  <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
					<img src="<?php if($foto) echo $this->config->file_url_view.'/'.$foto; else  echo $this->config->default_pic ?>">
				  </div>
				  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
				  <div>
					<span class="btn btn-primary btn-file"><span class="fileinput-new"><i class="fa fa-camera"></i> Select Image</span><span class="fileinput-exists"><i class="fa fa-edit"></i> Change</span>
					<input type="file" name="file" accept="image/*"></span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash-o"></i> Remove</a>
					<button type="submit" class="btn btn-primary btn-submit-image fileinput-exists"><i class="fa fa-check-square"></i> Save changes</button>
				  </div>
				</div>
			</div>  
      </form>
	
	<div class="clearfix"></div>
		<hr>
		<h3>Personal Information</h3>
		<form method="post" action="<?php echo $this->location('module/elearning/home/update_profile') ?>">
			<div class="form-group">
				<label>Email</label>
				<input type="email" name="email" class="form-control" value="<?php echo $key->email ?>" required>
			</div>
			<div class="form-group">
				<label>Name</label>
				<input type="text" name="nama" class="form-control" value="<?php echo $key->name ?>" required>
			</div>
			<button type="submit" class="btn btn-primary">Save Personal Information</button>
		</form>
</div>	


		