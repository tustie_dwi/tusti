
	<div class="row clearfix post box box-white">	
			<h2 class="no-margin">KRS</h2><br>
	
		<table class='table table-hover' id='example'>
				<thead>
					<tr>			
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
		
			<?php
			if( isset($krs) ) :	
			
				foreach ($krs as $dt): 
				
				if($dt->foto_mhs){ $foto=$dt->foto_mhs; }else{ $foto='modules/elearning/assets/pic/user.jpg';}
				?>
					<tr>
						
						<td>
							<div class="media">
								<a class="pull-left" style="text-decoration:none" href="#" onClick = "detail_krs('<?php echo $dt->mahasiswa_id?>')">
									<img class="media-object img-thumbnail" src="<?php echo $this->location($foto); ?>"  width="50" height="50">
								</a>
							  <div class="media-body">
								<a href='#' onclick = "detail_krs('<?php echo $dt->mahasiswa_id?>')"><span class='text'><b><?php echo ucWords(strToLower($dt->nama))?></b></span></a>
									<small><span class='text text-danger'><?php echo $dt->prodi ?></span></small>
									&nbsp;<span class='label label-success'><?php echo $dt->angkatan ?></span><br>NIM. <?php echo $dt->nim ?>
								
							  </div>
							</div>
						</td>
					</tr>
				 <?php endforeach; ?>
			 <?php endif; ?>
		</tbody></table>
	
	</div>

		