
<div class="row box box-white post" >		
	<h3 class="title-box">Password</h3>
	<form action="<?php echo $this->location(); ?>" method="post" id="form-update-password">
		<input type="hidden" name="id" value="<?php echo $user_id; ?>" />
		<div class="form-group">
			<label>New Password</label>
			<input type="password" name="password" id="input-password" class="form-control" />
		</div>
		
		<div class="form-group">
			<label>Confirm New Password</label>
			<input type="password" name="password" id="input-password2" class="form-control" />
		</div>
		
		<input type="submit" value="Change Password" class="btn btn-primary btn-update-password" data-loading-text="Updating..." /><br>
		<span id="status-password" style="margin-left:1em;">&nbsp;</span>
	</form>
</div>	

