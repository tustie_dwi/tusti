<?php
	function print_comment($comment, $user_id, $url, $post_id){
		if(! empty($comment)){
			$no = 0;
			foreach (explode('||', $comment) as $key) {
				$data_comment = explode('>>', $key);
				
				$comment_id = substr(md5($data_comment[2]),8,7); 
				$no++;
				?>
					<div class="row box box-gey post no-top-radius no-shadow comment-<?php echo $post_id ?> box-comment-<?php echo $comment_id ?> <?php if($no > 3) echo ' comment-hide' ?>">
						<div class="col-md-1" style="padding: 0px"><img src="<?php echo $url.$data_comment[0]; ?>" class="img-avatar-sm pull-right" /></div>
						<div class="col-md-11" style="padding-right: 0px">
							<span class="text-left">
								<a class="btn-link-sm">
								<?php 
			    					if($user_id == $data_comment[3]) echo "Me - ";
									else echo $data_comment[1]. " - ";
			    					convert_date($data_comment[5]);
			    				?>
			    				</a>
			    				
			    				<?php if($user_id == $data_comment[3]) { ?>
				    				<span style="color : #383D48" class="pull-right">
				    					<i class="fa fa-edit btn-tooltip" onclick="edit_comment('<?php echo $comment_id ?>')" style="cursor: pointer" data-toggle="tooltip" data-placement="bottom" title="Edit"></i>
				    					<i onclick="del_comment('<?php echo $comment_id ?>')" class="fa fa-trash-o btn-tooltip" style="cursor: pointer" data-toggle="tooltip" data-placement="bottom" title="Delete"></i>
				    				</span>
			    				<?php } ?>
			    			<br>
				      		<span style="color: #383D48">
				      			
				      			<form action="#" role="form" style="margin-top: 5px; display: block;" class="edit-comment form-edit-post-<?php echo $comment_id ?>" method="post"> 
								  <input name="id" value="<?php echo $comment_id ?>" type="hidden">
								  <input name="tipe" value="note" type="hidden" />					  
								  <div class="form-group" style="margin-bottom: 5px">
								  	<textarea name="isi" class="form-control"><?php echo $data_comment[4] ?></textarea>
								  </div>
								  
								  <div class="form-group">
									  <a type="submit" class="btn btn-info btn-xs" onclick="save_edit_comment('<?php echo $comment_id ?>')"><i class="fa fa-check-square"></i> Save</a>
									  <a onclick="cancel_edit_comment('<?php echo $comment_id ?>')" class="btn btn-danger btn-xs"><i class="fa fa-ban"></i> Cancel</a>
								  </div>
								</form>
								
				      			<p class="isi-edit-post-<?php echo $comment_id ?>"><?php echo $data_comment[4]?></p>
				      		</span>
				  		</div>
					</div> 
				<?php
			}
			
			if($no > 3){ ?>
				<div class="row box box-gey post no-top-radius no-shadow btn-show-more-<?php echo $post_id ?>">
					<button type="button" class="btn btn-success btn-sm btn-block" onclick="show_all_comment('<?php echo $post_id ?>')">
						<i class="fa fa-chevron-down"></i> Show All Comment
					</button>
				</div>
			<?php }
		}
	}
	
	function convert_date($date){
		$date = explode('-', $date);
		echo substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0));
	}
	function convert_date_all($date){
		$date = explode('-', $date);
		echo 'Due ' . substr($date[2],0,2) . ' ' . date("M", mktime(0, 0, 0,$date[1] , 0, 0)) . ',';
		
		echo substr($date[2],3,5);
	}
	function getMetaData($url){
		// get meta tags
		$meta=get_meta_tags($url);
		// store page
		$page=file_get_contents($url);
		// find where the title CONTENT begins
		$titleStart=strpos($page,'<title>')+7;
		// find how long the title is
		$titleLength=strpos($page,'</title>')-$titleStart;
		// extract title from $page
		$meta['title']=substr($page,$titleStart,$titleLength);
		// return array of data
		return $meta;
	}
	
	function get_file_ext($name){
		$ext = explode(".", $name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
?>