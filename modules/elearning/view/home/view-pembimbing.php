	<div class="row clearfix post box box-white">	
	<h2 class="no-margin"><?php 
		if($staff) echo "Academic Consultation";
		else echo "Academic Adviser";
	?></h2>
	 <?php				
		 if( isset($post_data) ) :	?>			
			<table class="table table-hover example">
					<thead>
						<tr>							
							<th>&nbsp;</th>							
						</tr>
					</thead>
					<tbody>
					<?php 	
					if($post_data){
					foreach ($post_data as $dt):  ?>
						<tr>
							<td>
							<?php 
							if($staff){
								//if($dt->foto_mhs){ $foto=$dt->foto_mhs; }else{ $foto='modules/elearning/assets/pic/user.jpg';}
									if($dt->foto_mhs){ $foto=$this->config->file_url_view."/".$dt->foto_mhs; }else{ $foto=$this->config->default_pic;}
									?>
									
									<div class="media">
										<a class="pull-left" style="text-decoration:none" href="#" onClick = "detail(<?php echo $dt->mahasiswa_id?>)">
											<img class="media-object img-thumbnail" src="<?php echo $foto; ?>"  width="50" height="50">
										</a>
									  <div class="media-body">
										<?php
										echo "<a href='#' onClick = 'detail(".$dt->mahasiswa_id.")'><span class='text'><b>".ucWords(strToLower($dt->nama))."</b></span></a>
											<small><span class='text text-danger'>".$dt->prodi."</span></small>
											&nbsp;<span class='label label-success'>".$dt->angkatan."</span><br>NIM. ";
										echo $dt->nim. " <span class='label label-warning' style='font-weight:normal'>".ucWords($dt->tahun_mulai)."</span>";
										?>
									  </div>
									</div>
								
									
									<?php
									
								}else{
									//if($dt->foto_dosen){ $foto=$dt->foto_dosen; }else{ $foto='modules/elearning/assets/pic/user.jpg';} 
									if($dt->foto_dosen){ $foto=$this->config->file_url_view."/".$dt->foto_dosen; }else{ $foto=$this->config->default_pic;}
									?>
									
									<div class="media">
										<a class="pull-left" style="text-decoration:none" href="#" onClick = "#">
											<img class="media-object img-thumbnail" src="<?php echo $foto; ?>"  width="50" height="50">
										</a>
									  <div class="media-body">
										<?php
										echo "<b>".ucWords(strToLower($dt->dosen))."</b><br>";											
										echo $dt->nik. "&nbsp; <small> <span class='text text-info'> <i class='fa fa-envelope-o'></i> ".$dt->email_dosen." </span></small>
												<br><small> <span class='text text-warning'> 
										<i class='fa fa-map-marker'></i> ".ucWords($dt->alamat_dosen)."</span>&nbsp; <i class='fa fa-mobile'></i> ".$dt->tlp_dosen."</small>";
										?>
									  </div>
									</div>								
							
							
							<?php } ?>
							</td>
						</tr>
						<?php endforeach; 
						}?>
					</tbody>
			</table>
		<?php
		 else: 
		 ?>
		 	<div class="well">Sorry, no content to show</div>
		<?php endif; ?>
	</div>

		