<?php echo $this->view('header.php',$data);

		$this->add_script('bootstrap/js/jasny-bootstrap.min.js'); ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
	        	<div class="box box-white full-radius" style="height: auto">
	        		
	        		<div class="head-box">
	        			<div class="row">
	        				<div class="col-sm-3">
	        					<a href="#" class="btn-edit-profile" data-toggle="modal" data-target="#modalProfile">
		        					<figure>
		        						<img src="<?php if($foto) echo $this->config->file_url_view.'/'.$foto; else  echo $this->config->default_pic ?>" class="img-avatar" />
		        					</figure>
		        					<div class="edit-profile-text">
		        						<span class="fa fa-camera"></span> Edit
		        					</div>
	        					</a>
	        					<?php echo $this->view('home/modal-profile.php',$data) ?>
	        				</div>
	        				<div class="col-sm-9" id="nick-wrap">
	        					<div style="margin-top: 2px">
	        						<span class="text-left"><?php echo $user ?></span>
		          					<span style="color: #9290B0"><br>
		          						<?php 
		          							if($role == 'dosen') echo 'Lecturer';
											else echo "Student";
		          						?>
		          					</span>
	          					</div>
	          				</div>
	          			</div>
	          		</div>
	        	</div>
	      	</div>
	      	
	  		<div class="col-md-12">
		        <div class="panel">
		          <div class="panel-heading panel-dark">
		            <h3 class="panel-title">
		            	Course 
		            	<i class="fa fa-bookmark pull-right"></i>
		            	
		            	<?php if($role == 'mahasiswa') { ?>
		            	<?php if($this->cek_krs_date()){ ?>
			            	<a href="<?php echo $this->location('module/elearning/krs') ?>">
			            		<i class="fa fa-plus-circle pull-right btn-tooltip" data-placement="top" title="Insert KRS" style="color:white"></i>
			            	</a>
		            	<?php }} ?>
		            </h3>
		          </div>
		             <ul class="list-group content-li">
		             	<?php if($mkd){ foreach($mkd as $key) { ?>
						    <li class="list-group-item"><a href="<?php echo $this->location('module/elearning/course/groups/'. $key->hidId) ?>"  ><?php echo $key->keterangan ?></a></li>
					    <?php }} else { echo ' <li class="list-group-item"><a href="">No content to show</a></li>'; } ?>
					 </ul>
		        </div>
		 	</div>
			<?php if($role == 'mahasiswa') { ?>
				<div class="col-md-12">
					<div class="panel">
					  <div class="panel-heading panel-dark">
						<h3 class="panel-title">
							Others 
							<i class="fa fa-check-square-o pull-right"></i>	
						</h3>
					  </div>
						 <ul class="list-group content-li">		             
							   <li class="list-group-item"><a href="<?php echo $this->location('module/elearning/report/khs') ?>">KHS</a></li>
								<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/report/adviser') ?>">Academic Adviser</a></li>						   
						 </ul>
					</div>
				</div>
			<?php } ?>
			<?php if($role == 'dosen') { ?>
				<div class="col-md-12">
					<div class="panel">
					  <div class="panel-heading panel-dark">
						<h3 class="panel-title">
							Academic Consultation 
							<i class="fa fa-check-square-o pull-right"></i>	
						</h3>
					  </div>
						 <ul class="list-group content-li">		             
							 <li class="list-group-item"><a href="<?php echo $this->location('module/elearning/report/adviser') ?>">Student List</a></li>
							 <li class="list-group-item"><a href="<?php echo $this->location('module/elearning/report/krs') ?>">Course Plan</a></li>							 
						 </ul>
					</div>
				</div>
			<?php } ?>
			
			<div class="col-md-12">
					<div class="panel">
					  <div class="panel-heading panel-dark">
						<h3 class="panel-title">
							Others 
							<i class="fa fa-gears pull-right"></i>	
						</h3>
					  </div>
						 <ul class="list-group content-li">		             
							 <li class="list-group-item"><a href="<?php echo $this->location('module/elearning/report/event') ?>">Event</a></li>
							 <!--<li class="list-group-item"><a href="<?php //echo $this->location('module/elearning/report/krs') ?>">Course Plan</a></li>-->						 
						 </ul>
					</div>
				</div>
		 	
		</div> <!-- end of main row -->
  		</div> <!-- end of main col-md-3 -->
      	<div class="col-md-8">
			<?php if(isset($kategori)):?>
			
				<div class="row">
				
					<div class="view-content">
						<?php 
							switch($kategori){
								case 'event':
									 echo $this->view('home/view-event.php', $data);
								break;
								case 'khs':
									 echo $this->view('home/view-khs.php', $data);
								break;
								case 'krs':
									 echo $this->view('home/view-krs.php', $data);
								break;
								case 'pembimbing':
									 echo $this->view('home/view-pembimbing.php', $data);
								break;
								case 'detail_adviser':
									 echo $this->view('home/view-mhs-bimbingan.php', $data);
								break;
								case 'detail_krs':
									 echo $this->view('home/view-mhs-krs.php', $data);
								break;
							}
						?>
						
					</div>
					
				</div>

			<?php
			else:
			?>
				<div class="row">
					<div class="col-md-12" style="margin-bottom: 20px">
						<?php 
							if($role == 'dosen') $this->view('home/post-bar.php', $data);
							else $this->view('home/note-bar.php', $data);
						?>
					</div>
					<div class="view-content">
						<?php echo $this->view('home/view-content.php', $data);?>
					</div>
					<div class="col-md-12" style="padding: 15px; padding-top: 0px;">
						<input id="page" value="1" type="hidden"/>
						<button type="button" class="btn btn-info btn-sm btn-block btn-more-post" onclick="more_post()">
							<i class="fa fa-chevron-down"></i> Show more
						</button>
					</div>
				</div>
			<?php endif; ?>
		</div>
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	if(isset($kategori) && $kategori=='event') echo $this->view('footer-event.php');
	else echo $this->view('footer.php');
?>

<!-- Modal Attach Link -->
<div class="modal fade" id="modal-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-link"></i> Attach Link</h4>
      </div>
      <form role="form">
      	  <input type="hidden" id="tab-link"/>
	      <div class="modal-body">
			  <div class="form-group">
			    <input type="url" class="form-control" placeholder="Enter URL" required id="add_url">
			  </div>
	      </div>
	      <div class="modal-footer">
	        <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
	        <a onclick="get_new_url()" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Attach</a>
	      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Attach Media Library -->
<div class="modal fade" id="modal-library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive"></i> Attach Library
	    		<span class="pull-right loading"><i class="fa fa-refresh fa-spin"></i> Loading</span>
	    	</h4>
	    </div>
    	<div class="modal-body" style="padding: 20px 0px">
    		<div class="row folder-wrap" style="margin: 0 10px; padding: 0px; padding-right: 10px">
    			<input id="folder_back_parent_id" value="0" type="hidden"/>
	    		<?php if($library): foreach($library as $key){ ?>
			  		<div class="col-md-3" style="padding: 0px 5px 0px 5px">
			  			<button title="<?php echo $key->folder_name ?>" class="btn btn-default" style="width: 100%; margin: 5px;" onclick="get_folder('<?php echo $key->id ?>')">
			  				<i class="fa fa-folder fa-3x"></i><br><?php echo $key->folder_name ?>
			  			</button>
			  		</div>
			  	<?php } endif;?>
		  	</div>
      	</div>
      	<div class="modal-footer">
      		<a type="button" class="btn btn-primary btn-sm pull-left" onclick="folder_back()"><i class="fa fa-arrow-circle-left"></i> Back</a>
      		
        	<a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</a>
        	<a onclick="add_library()" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Attach</a>
      	</div>
    </div>
  </div>
</div>




