<?php echo $this->view('header.php',$data) ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
      	<div class="col-md-12">
			<div class="row">
				<div class="view-content">
					<?php echo $this->view('home/view-content.php', $data);?>
				</div>
				<div class="col-md-12" style="padding: 15px; padding-top: 0px;">
					<input id="page" value="1" type="hidden"/>
					<button type="button" class="btn btn-info btn-sm btn-block btn-more-post" onclick="more_post()">
						<i class="fa fa-chevron-down"></i> Show more
					</button>
				</div>
			</div>
		</div>
 </div> <!-- end of main row -->
</div> <!-- end of container -->
<?php 
	echo $this->view('footer.php');
?>


<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$('.btn-more-post').remove();
	});
</script>