<?php echo $this->view('header.php') ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-3">
		<div class="row">
			<div class="col-md-12">
	        	<div class="box box-white">
	        		<div class="head-box">
	        			<div class="row">
	        				<div class="col-sm-3"><img src="<?php echo $this->location('modules/elearning/assets/pic/user.jpg') ?>" class="img-avatar" /></div>
	        				<div class="col-sm-9">
	        					<div class="" id="nick-wrap" style="margin-left: 10px; margin-top: 2px">
	        			<span class="text-left">Code Igniter</span>
		          		<span style="color: #9290B0"><br>Group</span>
	          		</div>
	        				</div>
	        			</div>
	        		
	        		
	          		</div>
	          		<div class="action-group">
	          			<ul class="list-group content-li">
						    <li class="list-group-item"><a href="#"><span class="fa fa-comment-o"></span> Posts<span class="fa fa-chevron-right"></span></a></li>
						    <li class="list-group-item"><a href="#"><span class="fa fa-folder-o"></span> Folder<span class="fa fa-chevron-right"></span></a></li>
						    <li class="list-group-item"><a href="#"><span class="fa fa-users"></span> Members<span class="fa fa-chevron-right"></span></a></li>
						 </ul>
	          		</div>
	        	</div>
	      	</div>
	      	
	  		<div class="col-md-12">
		        <div class="panel">
		          <div class="panel-heading panel-dark">
		            <h3 class="panel-title">Mata Kuliah <i class="fa fa-bookmark pull-right"></i></h3>
		          </div>
		             <ul class="list-group content-li">
					    <li class="list-group-item"><a href="#">Pemrograman Web</a></li>
					    <li class="list-group-item"><a href="#">Algoritma dan Struktur Data</a></li>
					    <li class="list-group-item"><a href="#">Etika Profesi</a></li>
					    <li class="list-group-item"><a href="#">Sistem Basis Data</a></li>
					    <li class="list-group-item"><a href="#">Algoritma Avolusi</a></li>
					    <li class="list-group-item"><a href="#">Logika Fuzzy</a></li>
					 </ul>
		        </div>
		 	</div>
		 	
		</div> <!-- end of main row -->
  		</div> <!-- end of main col-md-3 -->
      	
      	<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
		        	<?php include('post-bar.php') ?>
				</div>
				
				<ul class="col-md-12" style="margin-top: 20px;">
					<li class="li-post box box-white post">
		        		<div class="col-md-1" style="padding: 0px; padding-top: 5px;">
							<img src="assets/pic/user.jpg" id="img-avatar" />
						</div>
		        		
		        		<div class="col-md-10" style="padding-left: 5px;">
		        			<span class="text-left"><a class="btn-link-sm">Me</a> to <a class="btn-link-sm"><i class="fa fa-square"></i> Pemrograman Basis Data</a></span>
			          		<span style="color: #9290B0"><br>
			          			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			          		</span>
		          		</div>
		          		
		          		<ul class="message-footer clearfix">
							<li class="time-ago-no-icon subtext pull-right">18 minutes ago</li>
			                <li class="bullet pull-left">•</li>
			                <li class="share-this share-stream message-icon subtext pull-left" title="Share" data-title="Share Post" data-template="share-stream-options" data-transposted="Posted" data-transme="Me">Reply</li>
			         	</ul>
			      	</li>
			      	
			      	<li class="li-post">
			        	<div class="row box box-white post">
			        		<div class="col-md-1" style="padding: 0px">
								<img src="assets/pic/user.jpg" id="img-avatar" />
							</div>
			        		
			        		<div class="col-md-10">
			        			<span class="text-left"><a class="btn-link-sm">Me</a> to <a class="btn-link-sm"><i class="fa fa-square"></i> Pemrograman Basis Data</a></span>
				          		<span style="color: #9290B0"><br>
				          			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				          		</span>
			          		</div>
			        	</div>
			      	</li>
		      	</ul>
			</div> <!-- end of main row -->
      	</div> <!-- end of main  col-md-9 -->
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php echo $this->view('footer.php') ?>