<?php echo $this->view('header.php',$data) ?>

<!-- Begin Body -->
<div class="container">
	<div class="row">
		
		<div class="col-md-4">			
			<div class="action-group box box-white">						
				<ul class="list-group content-li content-li-menu">
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/home/settings'); ?>"> Account<span class="fa fa-chevron-right"></span></a></li>
					<li class="list-group-item"><a href="<?php echo $this->location('module/elearning/home/settings/pwd'); ?>"> Password<span class="fa fa-chevron-right"></span></a></li>					
				 </ul>				
			</div>		
		</div>
      	<div class="col-md-8">
			<div class="row">
				<?php 
				if($tmp):				
					echo $this->view('home/view-password.php', $data);
				else:
					echo $this->view('home/view-account.php', $data);
				endif;
				?>
			</div>
		</div>
 </div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	echo $this->view('footer.php');
	
	
?>





