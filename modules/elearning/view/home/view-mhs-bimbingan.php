	<div class="row clearfix post box box-white">	
	<h2 class="no-margin">Student Detail</h2><br>
	 <?php				
		 if(isset($biodata) && ($biodata)){			
				
				
			if($biodata->foto){ $foto=$this->config->file_url_view."/".$biodata->foto; }else{ $foto=$this->config->default_pic;}
			
			if($biodata->jenis_kelamin == 'P'){
				$jenis_kelamin = 'Perempuan';
			}else $jenis_kelamin = 'Laki-laki';
					?>
					<div class="media">
						<a class="pull-left" style="text-decoration:none" href="#" >
							<img class="media-object img-thumbnail" src="<?php echo $foto; ?>"  width="80" >
						</a>
					  <div class="media-body">
							<?php
							echo "<h3>".ucWords(strToLower($biodata->nama))."
									<span class='label label-success'>".$biodata->angkatan."</span> <span class='label label-danger'>".ucWords($biodata->is_aktif)."</span>
									<span class='label label-default'>".$biodata->cabang_id."</span></h3>";
							echo "<code>NIM ".$biodata->nim."</code><small>&nbsp;<span class='text text-default'>".$biodata->fakultas."</span>&nbsp; <span class='text text-danger'>".$biodata->prodi."</span> <br>";
							echo "<i class='fa fa-map-marker'></i> ".$biodata->alamat."&nbsp; ";
							if($biodata->telp){
								echo "<i class='fa fa-phone'></i> ".$biodata->telp."&nbsp; ";
							}
							
							if($biodata->hp){
								echo "<i class='fa fa-mobile'></i> ".$biodata->hp."&nbsp; ";
							}
							
							if($biodata->email){
								echo "<i class='fa fa-envelope'></i> ".$biodata->email."&nbsp; ";
							}
							echo "</small>";
							?>
							<br>
							<?php if($biodata->nama_ortu){ ?>
							
							<h4><span class="text text-info"><?php echo ucWords($biodata->nama_ortu) ?></span> <small>orang tua</small></h4>
							<small>
							<?php if($biodata->alamat_ortu){
								echo "<i class='fa fa-map-marker'></i> ".$biodata->alamat_ortu."&nbsp; ";
							}
							
							if($biodata->telp_ortu){
								echo "<i class='fa fa-phone'></i> ".$biodata->telp_ortu."&nbsp; ";
							}
							
							if($biodata->hp_ortu){
								echo "<i class='fa fa-mobile'></i> ".$biodata->hp_ortu."&nbsp; ";
							}
							
							if($biodata->email_ortu){
								echo "<i class='fa fa-envelope'></i> ".$biodata->email_ortu."&nbsp; ";
							} ?>
							</small>
							<?php } ?>
							<?php
							if($post_data){
								?>
								<h3>Academic Information</h3>								
								<table class='table table-hover'>
								<thead>
									<tr>
										<th>Semester</th>
										<th>SKS</th>
										<th>IP</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									
										foreach ($post_data as $dt): 
										?>
											<tr id="post-<?php echo $dt->mhs_id; ?>" data-id="<?php echo $dt->mhs_id; ?>" valign=top>
												<?php if($staff){ ?>
													<td>
														<?php 
															echo "<a style='cursor: pointer;text-decoration:none;' onclick=khs('".$dt->tahun_akademik."','".$dt->mahasiswa_id."')>".$dt->tahun."</a>"; 
														?>
													</td>
													<td>
														<?php if($dt->sks!=""){
																echo $dt->sks;
															  }else echo "0"; ?>
													</td>
													<td>								
														<?php
															echo number_format($dt->ipk,2);
														?>
													</td>
													
												<?php } ?>
											</tr>
											<?php
											$i++;
										 endforeach; 
									 ?>
								</tbody>
								</table>
								<?php
							}
							
							?>
					  </div>
				</div>
					
		<?php	
			}
				
		?>
	</div>

		