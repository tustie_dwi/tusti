
	
	<div class="row clearfix post box box-white">	
			<h2 class="no-margin">KRS</h2><br>			
				<table>
					<?php if(isset($identitas)){
							foreach($identitas as $i){ ?>
					<tr>
						<td style="width: 150px;">Name</td>
						<td style="width: 250px;">: <?php echo $i->nama ?></td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 150px;">Angkatan</td>
						<td style="width: 150px;">: <?php echo $i->angkatan ?></td>
					</tr>
					<tr>
						<td style="width: 150px;">NIM</td>
						<td style="width: 150px;">: <?php echo $i->mahasiswa_id ?></td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">Semester</td>
						<td style="width: 150px;">: <?php 
							$j=1;
							if(isset($semesterke)){
								foreach($semesterke as $d){
									if($semesterid==$d->inf_semester) echo $j." (".$this->kekata($j).")";
									$j++;
								}
							}
						
						?>
						</td>
					</tr>
					<?php 	}
						  } ?>
				</table><p>&nbsp;</p>
				<?php
				if(isset($posts)) :	
				?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Course</th>
						</tr>
					</thead>
					<tbody>
						<?php
						
							$i = 1;
							$nilai = 0;
							$sks = 0;
							$sks_lulus = 0;
							foreach($posts as $dt){ ?>
								<tr>
									<td><?php echo $i ?>. </td>
									<td>
										<div class="col-md-7">
											<?php echo "<b>".$dt->kode_mk."</b> - ".$dt->keterangan;
												if($dt->is_approve == '1') echo '&nbsp;<span class="label label-success btn-tooltip" data-placement="top" title="Approved" style="color:white"><i class="fa fa-check"></i> </span>';
												else echo ' <span class="label label-danger">* Need Approval</span>';
											?>																			
											<br><small><span class="text text-info">Class <?php echo $dt->kelas ?></span></small>
										</div>
									
										<div class="col-md-2"><?php echo $dt->sks ?> sks </div>
									
										<div class="col-md-3">
											<?php if($dt->is_approve == 0){ ?>
												<label>
												  <input type="checkbox" name="krs_id[]" value="<?php echo $dt->krs_id ?>"> Approve
												</label>
											<?php } else{ ?>
												<a class="btn btn-danger btn-xs btn-btl-krs btn-tooltip" data-placement="top" title="Reject" data-hidid="<?php echo $dt->hidId ?>" data-id="<?php echo $dt->krs_id ?>"><i class="fa fa-trash-o"></i></a>
											<?php } ?>
										</div>
									</td>
								</tr>
						<?php $i++;
							  
							  $sks += $dt->sks;
							  
							} ?>
						<input id="mhsid" value="<?php echo $mhsid?>" type="hidden" name="mhsid" />
						<tr>
							<td colspan="3"><div class="col-md-2">Total SKS</div><div class="col-md-10">:&nbsp;&nbsp;<?php echo $sks ?> sks</div></td>
						</tr>
						<tr>
							<td colspan="3"><div class="col-md-2">SKS Max</div><div class="col-md-10">:&nbsp;&nbsp;<?php if(isset($ip->ip)) echo $ip->jml_sks; else echo "-"; ?> sks</div></td>
						</tr>
						<tr>
							<td colspan="3"><div class="col-md-2">IP</div><div class="col-md-10">:&nbsp;&nbsp;<?php 
								if(isset($ip->ip)) echo number_format($ip->ip,2);
								else echo "-";
							?></div></td>
						</tr>
						<tr>
							<td colspan="3">
							<div class="col-md-2">IPK</div>
							<div class="col-md-10">:&nbsp;&nbsp;<?php 
								if(isset($ip->ip)) echo number_format($ip->ipk,2);
								else echo "-";
							?></div>
							<div class="col-md-12"><p>&nbsp;</p>
							<a id="btn-approve" class="btn btn-primary"><i class="fa fa-check"></i> Process KRS</a>
							</div>
							</td>
						</tr>
						
					</tbody>
					</form>
				</table>
			<?php
			else:
				echo "Sorry, no content to show";
			endif; 
			?>
	</div>

		