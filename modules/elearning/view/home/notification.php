<?php echo $this->view('header.php',$data);

		$this->add_script('bootstrap/js/jasny-bootstrap.min.js');
		 ?>

<!-- Begin Body -->
<div class="container">
	<div class="row"><div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
	        	<div class="box box-white full-radius" style="height: auto">
	        		
	        		<div class="head-box">
	        			<div class="row">
	        				<div class="col-sm-3">
	        					<a href="#" class="btn-edit-profile" data-toggle="modal" data-target="#modalProfile">
		        					<figure>
		        						<img src="<?php if($foto) echo $this->location($foto); else  echo $this->location('modules/elearning/assets/pic/user.jpg') ?>" class="img-avatar" />
		        					</figure>
		        					<div class="edit-profile-text">
		        						<span class="fa fa-camera"></span> Edit
		        					</div>
	        					</a>
	        					<?php echo $this->view('home/modal-profile.php',$data) ?>
	        				</div>
	        				<div class="col-sm-9" id="nick-wrap">
	        					<div style="margin-top: 2px">
	        						<span class="text-left"><?php echo $user ?></span>
		          					<span style="color: #9290B0"><br>
		          						<?php 
		          							if($role == 'dosen') echo 'Lecturer';
											else echo "Student";
		          						?>
		          					</span>
	          					</div>
	          				</div>
	          			</div>
	          		</div>
	        	</div>
	      	</div>
	      	
	  		
		</div> <!-- end of main row -->
  		</div> <!-- end of main col-md-3 -->
      	<div class="col-md-8">
			<div class="row">
		<div class="box box-white" style="height: auto">
			<h2 class="page-header"><i class="fa fa-envelope"></i> All Notifications</h2>
			<table class="nav-notification table table-condensed table-hover">
			  <!-- <li class="active"><a href="#">Home</a></li> -->
			  <?php foreach($not_data as $key) { ?>
			  	<tr <?php if($key->is_read == 0) echo "class='active'"; ?> id="<?php echo $key->hidId ?>" >
			  		<td>
				  		<a href="#" onclick="redirect_notif('<?php echo $key->link_id ?>', '<?php echo $key->hidId ?>')" <?php if($key->is_read == 1) echo "class='grey'"; ?> >
				  			<?php 
				  				switch($key->kategori){
									case 'comment': echo "<i class='fa fa-comment green'></i> "; break;
									case 'note': echo "<i class='fa fa-pencil-square purple'></i> "; break;
									case 'submission': echo "<i class='fa fa-file-zip-o red'></i> "; break;
									case 'mark': echo "<i class='fa fa-check-square red'></i> "; break;
									case 'assignment': echo "<i class='fa fa-check-circle red'></i> "; break;
								}
								
				  				echo '<strong>' . $key->user_from_name . '</strong> ' . $key->keterangan;
								if($key->is_read == 0) echo "<span class='label label-success pull-right small'>new</span>";
							?>
				  		</a>
				  	</td>
				  	<td>
				  		<?php echo "<i  class='fa fa-times pull-right icon-del btn-tooltip' title='Delete' data-toggle='modal' data-placement='bottom' onclick=\"del_notifikasi('".$key->hidId."')\"></i>"; ?>
			  		</td>
			  	</tr>
			  <?php } ?>
			</table>
		</div>
	</div> <!-- end of main row -->
</div> <!-- end of container -->

<?php 
	echo $this->view('footer.php');
?>


