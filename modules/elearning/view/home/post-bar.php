<ul id="myTab" class="nav nav-tabs">
  <li style="border-radius: 3px 0 0 0" class="active"><a style="border-radius: 3px 0 0 0" href="#note" data-toggle="tab"><i class="fa fa-pencil"></i> Note</a></li>
  <li><a href="#assignment" data-toggle="tab"><i class="fa fa-check-circle"></i> Assignment</a></li>
  <li><a href="#quiz" data-toggle="tab"><i class="fa fa-question-circle"></i> Quiz</a></li>
</ul>
<div id="home" class="tab-content">
	
	<div class="tab-pane in active" id="note">
		<form class="form-horizontal" role="form" id="form-note" method="post" action="<?php echo $this->location('module/elearning/home/save_note') ?>">
		  <div class="form-group" id="post-inp">
		    <div class="col-sm-12">
		      <input type="text" class="form-control" placeholder="Type your note here">
		    </div>
		  </div>
		  <span class="inp-note">
			  <div class="form-group">
			    <div class="col-sm-12">
			    	<textarea name="isi" id="isi" class="form-control" placeholder="Type your note here"></textarea>
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-12">
			    	<select class="e9" name="mkd_id" required="required" id="mkd_id" >
			    		<option value="0">Select Course</option>
			    		<?php 
					
						if($mkd):						
							if(count($mkd) > 1):
							foreach($mkd as $key) { ?>
								<option value="<?php echo $key->mkditawarkan_id ?>"><?php echo $key->keterangan ?></option>
							<?php } 
							else:
								?>
								<option value="<?php echo $mkd->mkditawarkan_id ?>"><?php echo $mkd->keterangan ?></option>
								<?php
							endif;
						endif;
						?>
			    	</select>
			    </div>
			  </div>
			  <div class="form-group">
			  	<div class="col-md-12" id="link-wrap-note"></div>
			  	<div class="col-md-12" id="attach-wrap-note" style="margin-bottom: 5px"></div>
			  	<div class="col-sm-8">
			  		<a data-toggle="modal" data-target="#modal-link" class="btn btn-success btn-xs pull-left btn-tooltip white" data-toggle="tooltip" data-placement="bottom" title="Attach Link" onclick="tab_val('note')"><i class="fa fa-link"></i></a> 
			  		<a accesskey="a" data-toggle="modal" data-target="#modal-library" style="margin: 0 2px" class="white btn btn-success btn-xs pull-left btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Attach Library" onclick="tab_val('note')"><i class="fa fa-archive"></i></a> 
			  		
			  		<a class="fileUpload btn btn-success btn-xs pull-left btn-tooltip white" data-toggle="tooltip" data-placement="bottom" title="Attach File">
					    <i class="fa fa-file-text-o"></i>
					    <input name="file[]" type="file" class="upload" id="uploadBtn" multiple="multiple" />
					</a>
			  	</div>
			  	<div class="col-sm-4">
			      <a id="btn-submit-note" class="btn btn-info pull-right"><i class="fa fa-check-square"></i> Submit</a>
			      <a id="cancel-note" class="btn btn-link pull-right">Cancel</a>
			    </div>
		  	  </div>
	  	  </span>
		</form>
	</div>
	
	<div class="tab-pane fade" id="assignment">
		<form class="form-horizontal" role="form" id="form-assignment">
			  <div class="form-group">
			  	<div class="col-sm-8">
			      <input type="text" name="assignment_title" required="required" id="assignment_title" class="form-control" placeholder="Assignment title">
			    </div>
			    <div class="col-sm-4">
			    	<button type="button" id="load-assignment" class="btn btn-default"><i class="fa fa-file-zip-o"></i> Load Assignment</button>
			    </div>
			  </div>
			  <div id="detail_form">
				  <div class="form-group">
				  	<div class="col-sm-12">
						<select class="e9" name="jadwal" required="required" id="select_jadwal">
							<option value='0'>Select Schedule</option>
							<?php 
								foreach($jadwal as $dt):
									echo "<option class='sub_".$dt->jadwal_id."' data-mk='".$dt->mkid."' value='".$dt->jadwal_id."'";
									echo ">".$dt->jadwal."</option>";
								endforeach;
							?>					
						</select>
				    </div>
			  	  </div>
				  
				  <div class="material-assign form-group">
					<div class="col-sm-12">
						<select class="e9" name="select-materi">
							<option value="0">No Topic</option>
						</select>
					</div>
				</div>
				
			  	  <div class="form-group">
				  	<div class="col-sm-6">
				      <input type="text" required="required" class="form-control form_datetime" name="start_date" id="start_date" placeholder="Start date" onchange='validate_tanggal()'>
				    </div>
				    <div class="col-sm-6">
				      <input type="text" required="required" class="form-control form_datetime" name="due_date" id="due_date" onchange='validate_tanggal()' placeholder="Due date">
				    </div>
				  </div>
				  <div class="form-group">
				  	<div class="col-sm-12">
				      <textarea type="text" required="required" class="form-control" name="instruction" id="instruction" placeholder="Instruction"></textarea>
				    </div>
			  	  </div>
			  	  <div class="form-group">
				  	<div class="col-sm-12">
				      <textarea required="required" class="form-control" name="describe" id="describe" placeholder="Describe the assignment"></textarea>
				    </div>
			  	  </div>
			  	  <div class="form-group">
			  		<div class="col-md-12" id="link-wrap-assignment"></div>
				  	<div class="col-md-12" id="attach-wrap-assignment" style="margin-bottom: 5px"></div>
				  	<div class="col-sm-8">
				  		<a data-toggle="modal" data-target="#modal-link" class="btn btn-success btn-xs pull-left" onclick="tab_val('assignment')"><i class="fa fa-link"></i></a> 
				  		<a accesskey="a" data-toggle="modal" data-target="#modal-library" style="margin: 0 2px" class="btn btn-success btn-xs pull-left btn-tooltip" data-toggle="tooltip" data-placement="bottom" title="Attach Library" onclick="tab_val('assignment')"><i class="fa fa-archive"></i></a> 
				  		
				  		<a class="fileUpload btn btn-success btn-xs pull-left">
						    <i class="fa fa-file-text-o"></i>
						    <input name="file[]" type="file" class="upload" id="uploads" multiple="multiple" />
						</a>
				  	</div>
				  	<div class="col-sm-4">
				      <button type="submit" class="btn btn-info pull-right"><i class="fa fa-check-square"></i> Submit</button>
				      <a href="<?php $this->location(); ?>" class="btn btn-link pull-right">Cancel</a>
				    </div>
			  	  </div>
		  	  </div>
		</form>
	</div>
	
	<div class="tab-pane in" id="quiz">
		<form class="form-horizontal" role="form">
		  <div class="form-group">
		    <div class="col-sm-12">
		      <a href="<?php echo $this->location('module/elearning/quiz/create') ?>" class="btn btn-success"><i class="fa fa-pencil"></i> Create a Quiz</a>
		      or
		      <a class="btn btn-link load-quiz" href="javascript::"><i class="fa fa-file-zip-o"></i> Load a previously created Quiz</a>
		      <p class="quiz-divider"></p>
		    </div>
		  </div>
		</form>
	</div>
</div>


<!-- Modal Quiz -->
<div class="modal fade" id="QuizBankModal" tabindex="-1" role="dialog" aria-labelledby="QuizBankModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top: 100px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="QuizBankModalLabel">Previously Created Quiz</h4>
      </div>
      <div class="modal-body">
      	
      	<table id="example" class='table table-hover display'>
        <thead>
            <tr>
                <th></th>
            </tr>
        </thead>
		
   		</table>
		
      </div>
    </div>
  </div>
</div>

<!-- Modal Assignment -->
<div class="modal fade" id="modal-load-tugas" tabindex="-1" role="dialog" aria-labelledby="AssignmentModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top: 100px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="AssignmentModalLabel">Previously Created Assignment</h4>
      </div>
       <div class="modal-body">
      	 <table id="assignment-tbl" class='example table table-hover display'>
	        <thead>
	            <tr>
	                <th>Judul</th>
	                <th>Mata Kuliah</th>
	                <th>Kelas</th>
	                <th>&nbsp;</th>
	            </tr>
	        </thead>
   		</table>
      </div>
    </div>
  </div>
</div>