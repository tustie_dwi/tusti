<form method="POST" action="<?php echo $this->location('module/elearning/report/event') ?>" id="form-select">
<div class="row">
	<div class="col-md-6">
	<select class="form-control" id="select-bulan" name="bulan">
		<option <?php if($month==1) echo "selected"; ?> value="1">Jan</option>
		<option <?php if($month==2) echo "selected"; ?> value="2">Feb</option>
		<option <?php if($month==3) echo "selected"; ?> value="3">Mar</option>
		<option <?php if($month==4) echo "selected"; ?> value="4">Apr</option>
		<option <?php if($month==5) echo "selected"; ?> value="5">May</option>
		<option <?php if($month==6) echo "selected"; ?> value="6">Jun</option>
		<option <?php if($month==7) echo "selected"; ?> value="7">Jul</option>
		<option <?php if($month==8) echo "selected"; ?> value="8">Aug</option>
		<option <?php if($month==9) echo "selected"; ?> value="9">Sep</option>
		<option <?php if($month==10) echo "selected"; ?> value="10">Oct</option>
		<option <?php if($month==11) echo "selected"; ?> value="11">Nov</option>
		<option <?php if($month==12) echo "selected"; ?> value="12">Dec</option>
	</select>
	</div>
	
	<div class="col-md-6">
	<input name="tahun" id="select-tahun" class="form-control" type="number" value="<?php echo $year ?>" max="<?php echo date('Y') ?>" >
	</div>
</div>
</form>

<?php

$style	="calendar";  	
	
	$title = date('F Y',mktime(0,0,0,$month,1,$year));
	
	 if((substr($month, 0, 1)) == 0)
	 {
		 $tempMonth = substr($month, 1);                                                                                              
		 $month = $tempMonth;
	 }
	
	 // echo '<h5>'.$title.'</h5>';
	echo '<br><table width="100%" cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
	
	 $headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
	echo  '<thead><tr class="'. $style .'-row"><td width="14.285%" class="'. $style .'-day-head">'
		 .implode('</td><td width="14.285%" class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

	 /* days and weeks vars now ... */
	 $running_day = date('w',mktime(0,0,0,$month,1,$year));
	 $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	 $days_in_this_week = 1;
	 $day_counter = 0;
	 $dates_array = array();

	
	echo  '<tbody><tr class="'. $style .'-row">';
	   
	 for($x = 0; $x < $running_day; $x++):
		echo  '<td class="'. $style .'-day-np"> </td>';
		 $days_in_this_week++;
	 endfor;

	 $list_day=0;
	 $skip = false;
	 
	 
				
	 for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		 if($list_day == date("j",mktime(0,0,0,$month)))
		 {    
			echo  '<td class="popup-form '. $style .'-current-day" data-calendaryear="'.$year.'" data-calendarmonth="'.$month.'" data-calendarday="'.($day_counter+1).'">';
			 $txtstyle = $style .'-current-day';
		 }
		 else            
		 {    
			 if(($running_day == "0") || ($running_day == "6"))
			 {
				echo  '<td class="popup-form '. $style .'-weekend-day" data-calendaryear="'.$year.'" data-calendarmonth="'.$month.'" data-calendarday="'.($day_counter+1).'">';
				  $txtstyle = $style .'-weekend-day';
			 }
			 else
			 {
				echo  '<td class="popup-form '. $style .'-day" data-calendaryear="'.$year.'" data-calendarmonth="'.$month.'" data-calendarday="'.($day_counter+1).'">';  
				 $txtstyle = $style ;	
			 }
		 }
		
		echo  '<div class="'. $style .'-day-number">'.$list_day.'</div>';
		// echo  $this->get_data_calendar($list_day, $month, $year, $running_day);
		echo $this->get_data_event($all_event, $list_day, $month, $year, $running_day);	
		// echo  get_aktifitas($aktifitas, $list_day);	 
		// echo  $this->cetak_aktifitas($aktifitas, $list_day, $i);
		echo  '</td>';
		
		 if($running_day == 6):
			echo  '</tr>';
			 if(($day_counter+1) != $days_in_month):
				echo  '<tr class="'. $style .'-row">';
			 endif;
			 $running_day = -1;
			 $days_in_this_week = 0;
		 endif;
		 $days_in_this_week++; $running_day++; $day_counter++;
	 endfor;

	 if($days_in_this_week < 8) : //tgl kosong
		 for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			echo  '<td class="'. $style .'-day-np"></td>';
		 endfor;
	 endif;

	echo  '</tr>';
	echo  '</tbody></table>';
	?>

<?php
	function get_aktifitas($aktifitas, $tgl){
		if($aktifitas):
			echo "<br>";
			foreach($aktifitas as $key){
				if($key->tgl == $tgl) {
				echo $key->tgl_selesai;
				echo "<div style='margin-top: 2px'></div><span data-judul='$key->judul' data-jammulai='$key->jam_mulai' data-jamselesai='$key->jam_selesai'
						title='$key->jam_mulai - $key->jam_selesai' data-time='$key->id'
						style='xborder: 1px solid white;' class='label-aktifitas label label-success btn-tooltip'>".
						substr($key->judul,0, 15)."</span>";
				}
			}
			
			echo "<div style='margin-bottom: 5px'></div>";
		endif;
	}

?>
<div id="kegiatan-wrap"></div>

<div  id="calendarModal" >
	<span id="cal-add">
		<h4><i class="fa fa-plus"></i> Add Event</h4>
		<form role="form" method="POST" id="form-aktifitas" method="POST" aaction="<?php echo $this->location('home/add_kegiatan') ?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<input id="tanggal" name="tanggal" class="form-control input-datetime hasDatepicker form_date" type="date" readonly>
					</div>
					<div class="col-md-6">
						<input onchange="check_tgl()" name="tanggal_sampai" class="form-control pick-date" type="text" placeholder="Event End">
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label">Title</label>
				<input type="text" class="form-control" id="judul" name="judul">
			 </div>
			
			 <div class="form-group">
				<label class="control-label">Event Category</label>
				<select name="jenis_kegiatan" class="form-control">
					<option value="">Please Select</option>
					<?php
						if($jenis_kegiatan) :
							foreach($jenis_kegiatan as $key){
								echo "<option value='".$key->jenis_kegiatan_id."'>".$key->keterangan."</option>";
							}
						endif;
					?>
				</select>
			 </div>
			
			<div class="form-group">
				<label class="control-label">Room</label>
				<select name="ruang[]" class="select2-multiple" multiple="multiple" >
					<?php
						if($ruang) :
							foreach($ruang as $key){
								echo "<option value='".$key->ruang_id."'>".$key->kode_ruang. ' - ' . $key->keterangan."</option>";
							}
						endif;
					?>
				</select>
			 </div>
			 
			 <div class="form-group">
				<label class="control-label">Location</label>
				<textarea class="form-control" name="lokasi"></textarea>
			 </div>
			 
			  <div class="form-group">		
				<label class="control-label">Time</label>
				<div class="row">
					<div class="col-md-3">
						<input type="text" class="form-control pick-time" id="mulai" placeholder="00:00" name="jam_mulai">
					</div>
					<div class="col-md-3">
						<input type="text" class="form-control pick-time" id="selesai" placeholder="00:00" name="jam_selesai">		
					</div>		
				</div>			
			 </div>
			
			<div class="form-group">
	             <button onclick="save_aktifitas()" class="btn btn-primary">Save Changes <i class="loading fa fa-refresh fa-spin" style="display: none"></i></button>
				 <button type="button" class="btn btn-default" id="btn-modal-close">Close</button>
			</div>
	  </form>
  </span>
  
  <span id="cal-detail" style="display: none">
  	<h4>
  		<i class="fa fa-tasks"></i> Event Detail
  		<button id="btn-ubah-kegiatan" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Edit</button>
  	</h4>
  	<br>
  	<table class="table table-bordered">
  		<tr><td>Event</td><td id="info-judul"></td></tr>
  		<tr><td>Category</td><td id="info-jenis_kegiatan"></td></tr>
  		<tr><td>Date</td><td id="info-waktu"></td></tr>
  		<tr><td>Room</td><td id="info-ruang"></td></tr>
  		<tr><td>Location</td><td id="info-lokasi"></td></tr>
  	</table>
  </span>
  
  <span id="cal-edit" style="display: none">
  		<h4>
  			<i class="fa fa-pencil"></i> Edit Event
  		</h4>
		<form role="form" method="POST" id="form-edit-aktifitas" method="POST" aaction="<?php echo $this->location('home/edit_kegiatan') ?>">
			<div class="form-group">
				<input name="time" type="hidden" />
				<div class="row">
					<div class="col-md-6">
						<input name="edit_tanggal" class="form-control input-datetime hasDatepicker form_date" type="date" readonly>
					</div>
					<div class="col-md-6">
						<input onchange="check_tgl_edit()" name="edit_tgl_selesai" class="form-control pick-date" type="text" placeholder="Tanggal Selesai">
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label">Title</label>
				<input type="text" class="form-control" placeholder="Enter title" name="edit_judul">
			 </div>
			 
			 <div class="form-group">
				<label class="control-label">Event Category</label>
				<select name="edit_jenis_kegiatan" class="form-control">
					<option value="">Please Select</option>
					<?php
						if($jenis_kegiatan) :
							foreach($jenis_kegiatan as $key){
								echo "<option value='".$key->jenis_kegiatan_id."'>".$key->keterangan."</option>";
							}
						endif;
					?>
				</select>
			 </div>
			 
			 <div class="form-group" id="edit-ruang">
				<label class="control-label">Room</label>
				<select id="ruang" name="edit_ruang[]" class="select2-multiple" multiple="multiple" >
					<?php
						if($ruang) :
							foreach($ruang as $key){
								echo "<option value='".$key->ruang_id."'>".$key->kode_ruang. ' - ' . $key->keterangan."</option>";
							}
						endif;
					?>
				</select>
			 </div>
			 
			 <div class="form-group">
				<label class="control-label">Location</label>
				<textarea class="form-control" name="edit_lokasi"></textarea>
			 </div>
			 
			  <div class="form-group">		
				<label class="control-label">Time</label>
				<div class="row">
					<div class="col-md-3">
						<input type="text" class="form-control pick-time" placeholder="00:00" name="edit_jam_mulai">
					</div>
					<div class="col-md-3">
						<input type="text" class="form-control pick-time" placeholder="00:00" name="edit_jam_selesai">		
					</div>		
				</div>			
			 </div>
			
			<div class="form-group">
	             <button onclick="edit_aktifitas()" class="btn btn-primary">Edit Event <i class="fa fa-refresh fa-spin loading" style="display: none"></i></button>
				 <button type="button" class="btn btn-default" id="btn-modal-close-manage">Cancel</button>
			</div>
	  </form>
  </span>
</div>

