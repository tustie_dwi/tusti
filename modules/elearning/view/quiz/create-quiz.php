<?php echo $this->view('header.php', $data); ?>
<?php 
	if(isset($posts)){
		foreach ($posts as $p) {
			$title 			= $p->judul;
			$time_limit 	= $p->time_limit;
			$instruksi 		= $p->instruksi;
			$keterangan 	= $p->keterangan;
			$tgl_mulai		= $p->tgl_mulai;
			$tgl_selesai 	= $p->tgl_selesai;
			$is_random_quiz = $p->is_random;
			$jadwalid 		= $p->jadwal_id;
			$materi_id		= $p->materi_id;
			$mkid 			= $p->mkditawarkan_id;
			$hid_id 		= $p->hid_id;
		}
		if(isset($jadwalid)&&$jadwalid=='8f00b20'){$exam='1';}else{$quiz='1';}
		
		$data['soalnum'] = count($soal);
	}
	
?>
<!-- Begin Body -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">			
				<form id="form-quiz" class="form form-quiz">
					<div class="row">
					<div class="col-md-8 content-quiz">
						<div class="header-quiz">
							<div class="row">
								<div class="col-md-7">
									<input type="text" name="title" value="<?php if(isset($title))echo $title; ?>" class="form-control" placeholder="Title" required="required"/>
								</div>
								<div class="col-md-5">
									<div class="row">
									<div class="form-group">
    								<label class="col-sm-6 control-label">Time Limit</label>
								    <div class="col-sm-4">
								      <input type="text" name="time-limit" value="<?php if(isset($time_limit))echo $time_limit; ?>" class="form-control" placeholder="0" required="required">
								    </div>
								  </div>
								  </div>
								</div>
							</div>
						</div>
						<?php if(!isset($hid_id)){ ?>
						<div class="main-quiz">
							<div class="row">
								<div class="col-md-2 main-quiz-menu">
									<div class="title-menu-question">Questions</div>
									
									<ul class="menu-question-number">
										<li class="top-child active" id="test-number-1"><a href="javascript::" id="link-question-1">1</a></li>
									</ul>
									
									<div class="menu-btn">							
										<a href="javascript::" class="btn btn-default btn-block add-question">Add</a>
										<a href="javascript::" class="btn btn-info btn-block load-question">Load</a>
									</div>		
								</div>
								
								<div class="col-md-10 main-quiz-question">
									<div class="main-quiz-header">
										<div class="row">
											<div class="col-md-4">
												<div class="row">
													<div class="form-group">
													    <label for="inputEmail3" class="col-sm-4 control-label">Type</label>
													    <div class="col-sm-8 select-box">
													    <select class="form-control typetest-question-1" name="select-test[]">
														  <?php 
															foreach($categoryList as $cl):
																echo "<option value='".$cl->kat_id."' data-input='".$cl->jenis_input."' ";
																if(isset($categoryid)&&$categoryid == $cl->kat_id){
																	echo "selected";
																}
																echo ">".$cl->kategori."</option>";
															endforeach;
														?>		
														</select>  
													    </div>
													  </div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="form-group">
													    <label for="inputEmail3" class="col-sm-4 control-label">Points:</label>
													    <div class="col-sm-8 points-box">
													    <input type="text" class="form-control point-question-1" required="required" name="point[]" />
													    </div>
													  </div>
												</div>
											</div>
											<div class="col-md-4 remove-question-box">
												<a class="btn btn-default remove-question remove-question-1">Remove Question</a>
											</div>
										</div>
									</div>
									<div class="question-area">
										<label>Question</label>
										<span class="question-box">
										<textarea name="question[]" class="form-control question-question-1"></textarea>
										</span>
									</div>
									
									<div class="answer-area">
										<label>Response</label>
										<div class="checkbox">
										<span class="randomanswer-box">
										  <label class="randomanswer-question-1">
										    <input type="checkbox" value="" name="random-answer-q1">
										    Random Response
										  </label>
										</span>
										</div>
										
										<span class="answer-option-area">
										<span class="area answer-option-area-1">
										<input type="hidden" name="count-answer[]" value="1" class="answercount answer-count-area-1"/>
										<div class="radio radio-q1">
										  <label>
										    <input type="radio" name="options-q1" value="q1-1" class="option-check">
										  </label>
										    <input type="text" name="answer-q1[]" class="answer">
										    <input type="text" name="skor-q1[]" placeholder="score" class="score">
										    <input type="hidden" name="isbenar-q1[]" class="isright is-benar-q1-1">
										    <span class="isoption is-option-q1-1"></span>
										</div>
										</span>
										</span>
										
										<span class="add-response-box">
										<a href="javascript::" class="btn btn-danger add-response response-1">Add Response</a>
										</span>
									</div>
								</div>
								
							</div>
						</div>
						<?php }else{
							$this->view('quiz/quiz-body-edit.php', $data);
						} ?>
					</div>
					<div class="col-md-4 sidebar-quiz">
						<div class="quiz-assign">
							<input type="submit" name="b_savepublish" onclick="save('1')" id="submit-publish" value="Save and Publish" class="btn btn-info btn-block">
							<input type="submit" name="b_draft" onclick="save('0')" id="submit-draft" value="Save as Draft" class="btn btn-warning btn-block">
							<input type="hidden" name="hidId" value="<?php if(isset($hid_id))echo $hid_id; ?>" class="form-control" />
						</div>
						<p class="quiz-divider"></p>
						<div class="quiz-assign">
							<div>
							  <label><input type="radio" class="exam-type" name="test-type" value="exam" <?php if(isset($exam)&&$exam=='1')echo "checked" ?>/> Exam</label>
							  <label><input type="radio" class="quiz-type" name="test-type" value="quiz" <?php if(isset($quiz)&&$quiz=='1')echo "checked" ?>/> Quiz</label>
							  <?php if(isset($hid_id)){ ?>
							  	<input type="hidden" class="data-quiz-type" data-jadwal="<?php if(isset($jadwalid))echo $jadwalid; ?>" data-mk="<?php if(isset($mkid))echo $mkid; ?>" data-materi="<?php if(isset($materi_id))echo $materi_id; ?>"/>
							  <?php } ?>
							</div>
							<span class="type-assign">
							<label class="type-text">Schedule</label>
							<select class="e9" name="select-jadwal">
								<option value="0">No schedule</option>
							</select>
							</span>
						</div>
						<div class="quiz-assign material-assign">
							<label>Topic</label>
							<select class="e9" name="select-materi">
								<option value="0">No Topic</option>
							</select>
						</div>
						
						<p class="quiz-divider"></p>
						<div class="about-quiz">
							<label>Instruction</label>
							<input type="text" name="instruction" value="<?php if(isset($instruksi))echo $instruksi; ?>" class="form-control" />
							<p class="quiz-divider"></p>
							<label>About this Quiz</label>
							<textarea class="form-control" name="about-quiz"><?php if(isset($keterangan))echo $keterangan; ?></textarea>
							<p class="quiz-divider"></p>
							<label>Start date</label>
							<input type="text" name="start-date" value="<?php if(isset($tgl_mulai))echo $tgl_mulai; ?>" class="form-control form_datetime" />
							<label>Finished date</label>
							<input type="text" name="finish-date" value="<?php if(isset($tgl_selesai))echo $tgl_selesai; ?>" class="form-control form_datetime" />
						</div>
						
						<p class="quiz-divider"></p>
						<div class="about-quiz">
							<label>Quiz Option</label>
							<div class="checkbox">
							  <label>
							    <input type="checkbox" name="random-test" <?php if(isset($is_random_quiz)&&$is_random_quiz=='1')echo "checked"; ?> value="">
							    Random Question
							  </label>
							</div>
						</div>
					</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="QuestionBankModal" tabindex="-1" role="dialog" aria-labelledby="QuestionBankModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top: 100px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="QuestionBankModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      	
      	<table id="example" class='table table-hover display'>
        <thead>
            <tr>
                <th></th>
            </tr>
        </thead>
		
   		</table>
		
      </div>
    </div>
  </div>
</div>

<?php echo $this->view('footer.php', $data); ?>