<div class="main-quiz">
	<div class="row">
		<div class="col-md-2 main-quiz-menu">
			<div class="title-menu-question">Questions</div>
			
			<ul class="menu-question-number">
				<?php for ($i=1; $i <= $soalnum ; $i++){
				echo '<li ';
				if($i==1){
					echo 'class="top-child active"';
				}elseif($i==$soalnum){
					echo 'class="bottom-child"';
				}			
				echo ' id="test-number-'.$i.'"><a href="javascript::" id="link-question-'.$i.'">'.$i.'</a></li>';
				} ?>
			</ul>
			
			<div class="menu-btn">							
				<a href="javascript::" class="btn btn-default btn-block add-question">Add</a>
				<a href="javascript::" class="btn btn-info btn-block load-question">Load</a>
			</div>		
		</div>
		
		<div class="col-md-10 main-quiz-question">
			<div class="main-quiz-header">
				<div class="row">
					<div class="col-md-4">
						<div class="row">
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Type</label>
							    <div class="col-sm-8 select-box">
							    <?php for ($i=0; $i < $soalnum ; $i++){
									$categoryid = $soal[$i]->kategoriid;
							    ?>	
							    <select class="form-control typetest-question-<?php echo ($i+1) ?>" name="select-test[]" <?php if(($i+1)!=1)echo 'style="display: none;"' ?>>
								  <?php 
									foreach($categoryList as $cl):
										echo "<option value='".$cl->kat_id."' data-input='".$cl->jenis_input."' ";
										if(isset($categoryid)&&$categoryid == $cl->kat_id){
											echo "selected";
										}
										echo ">".$cl->kategori."</option>";
									endforeach;
								?>		
								</select>  
								<?php } ?>
							    </div>
							  </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="form-group">
							    <label for="inputEmail3" class="col-sm-4 control-label">Points:</label>
							    <div class="col-sm-8 points-box">
							    <?php for ($i=0; $i < $soalnum ; $i++){ ?>		
							    <input type="text" class="form-control point-question-<?php echo ($i+1) ?>" value="<?php echo $soal[$i]->poin ?>" required="required" name="point[]" <?php if(($i+1)!=1)echo 'style="display: none;"' ?>/>
							    <?php } ?>
							    </div>
							  </div>
						</div>
					</div>
					<div class="col-md-4 remove-question-box">
						<?php for ($i=0; $i < $soalnum ; $i++){ ?>		
						<a class="btn btn-default remove-question remove-question-<?php echo ($i+1) ?>">Remove Question</a>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="question-area">
				<label>Question</label>
				<span class="question-box">
				<?php for ($i=0; $i < $soalnum ; $i++){ ?>
				<textarea name="question[]" class="form-control question-question-<?php echo ($i+1) ?>" <?php if(($i+1)!=1)echo 'style="display: none;"' ?> ><?php echo $soal[$i]->pertanyaan; ?></textarea>
				<input type="hidden" name="hidIdSoal[]" value="<?php echo $soal[$i]->hid_id; ?>" />
				<?php } ?>
				</span>
			</div>
			
			<div class="answer-area">
				<label>Response</label>
				<div class="checkbox">
				<span class="randomanswer-box">
				  <?php for ($i=0; $i < $soalnum ; $i++){ ?>
				  <label class="randomanswer-question-<?php echo ($i+1) ?>" <?php if(($i+1)!=1)echo 'style="display: none;"' ?> >
				    <input type="checkbox" value="" name="random-answer-q<?php echo ($i+1) ?>" <?php if($soal[$i]->random_answer=='1')echo "checked" ?>>
				    Random Response
				  </label>
				  <?php } ?>
				</span>
				</div>
				
				<span class="answer-option-area">
				<?php $array = 0;for ($i=0; $i < $soalnum ; $i++){ ?>
				<span class="area answer-option-area-<?php echo ($i+1) ?>" <?php if(($i+1)!=1)echo 'style="display: none;"' ?>>
				<input type="hidden" name="count-answer[]" value="<?php echo $soal[$i]->answernum ?>" class="answercount answer-count-area-<?php echo ($i+1) ?>"/>
				<div class="radio radio-q<?php echo ($i+1) ?>">
				<?php for ($y=0; $y < $soal[$i]->answernum ; $y++){ ?>
				<input type="hidden" name="hidIdJawab[]" value="<?php echo $jawab[$array]->hidId; ?>" />
				<?php if($soal[$i]->input!='textarea'){ ?>
				  <label>
				    <input type="<?php echo $soal[$i]->input ?>" name="options-q<?php echo ($i+1) ?>" value="q<?php echo ($i+1) ?>-<?php echo ($y+1) ?>" class="option-check" <?php if($jawab[$array]->is_benar=='1')echo "checked" ?>>
				  </label>
				    <input type="text" name="answer-q<?php echo ($i+1) ?>[]" class="answer" value="<?php echo $jawab[$array]->keterangan ?>">
				    <input type="text" name="skor-q<?php echo ($i+1) ?>[]" placeholder="score" class="score" value="<?php echo $jawab[$array]->skor ?>">
				    <input type="hidden" name="isbenar-q<?php echo ($i+1) ?>[]" class="isright is-benar-q<?php echo ($i+1) ?>-<?php echo ($y+1) ?>" value="<?php echo $jawab[$array]->is_benar ?>">
				    <span class="isoption is-option-q<?php echo ($i+1) ?>-<?php echo ($y+1) ?>"><?php if($jawab[$array]->is_benar=='1')echo '&nbsp;<span class="label label-info">Right Answer</span>' ?></span><br>
				<?php }else{ ?>
					<textarea name="answer-q<?php echo ($i+1) ?>[]" placeholder="Answer" class="answer form-control"><?php echo $jawab[$array]->keterangan ?></textarea>
				<?php } ?>
				<?php $array += 1; } ?>	
				</div>
				</span>
				<?php } ?>
				</span>
				
				<span class="add-response-box">
				<?php for ($i=0; $i < $soalnum ; $i++){ ?>	
				<a href="javascript::" class="btn btn-danger add-response response-<?php echo ($i+1) ?>" <?php if($soal[$i]->input=='textarea')echo 'style="display: none;"' ?> <?php if(($i+1)!=1)echo 'style="display: none;"' ?> >Add Response</a>
				<?php } ?>
				</span>
			</div>
		</div>
		
	</div>
</div>