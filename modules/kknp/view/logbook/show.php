<div class="well">
	<h4 class="form-for-nilai-mhs"><?php echo $nama_mhs.'&nbsp;<span style="color:#c09853;"><small>'.$prodi_mhs.'</small></span><br>'; ?></h4>
	<br>
	<div class="form-group">
		<label class="col-sm-2 control-label">Pembimbing</label>
		<div class="col-sm-10">
			<select name="pembimbing" class="form-control">
			<option value="0">Pilih Pembimbing</option>
			<?php 
			if(count($pembimbing)>0):
				foreach ($pembimbing as $p) {
					if($p->nama!=''):
						echo "<option value='".$p->pembimbing_id."' ";
						if(isset($pembimbingid)){
							if($pembimbingid==$p->pembimbing_id){
								echo "selected";
							}
						}
						echo " >".ucWords($p->nama)."</option>";
					endif;
				}
			endif;
			?>
			</select>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Nama Kegiatan</label>
		<div class="col-sm-10">
			<input type="text" name="nama_kegiatan" class="nama_kegiatan form-control" value="" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Tanggal</label>
		<div class="col-sm-10">
			<input type="text" name="tgl_kegiatan" class="tgl_kegiatan form-control" value="" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Catatan</label>
		<div class="col-sm-10">
			<textarea name="catatan" class="form-control"></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-12">
			<input type="hidden" name="hiDMhs" class="form-control" value="<?php echo $mhs_kknp_id ?>" />
			<input type="hidden" name="hidId" class="form-control" value="" />
			<a href="javascript::" class="btn btn-danger pull-right" id="cancel-edit" style="margin-left:5px;display: none;">Cancel</a>
			<a href="javascript::" class="btn btn-primary pull-right" id="submit-logbook" type="submit">Submit</a>
		</div>
	</div>
	
	<div class="this-mhs-log">
		<?php if(isset($log)): ?>
		<table border="0" id="example" class="table table-hover">					
			<tbody>
				<?php 
				foreach ($log as $log) { ?>
				<tr class="tr-curr">				
					<td width="10%">
						<span class="nama-kegiatan-show"><?php echo $log->nama_kegiatan ?></span>&nbsp;
						<?php 
							$tgl = explode(' ', $log->tgl);
							$tgl = $tgl[0];
						?> 
						<span class="tgl-show label label-success"><?php echo $tgl ?></span><br><br>
					</td>
					<td width="15%">
						<?php echo $log->nama_pembimbing ?>
					</td>
					<td>
						<span class="catatan-show"><?php echo $log->catatan ?></span>
					</td>
					<td>
					<div class="col-md-1">
						<ul class="nav nav-pills">
							<li class="dropdown">
								<a class="dropdown-toggle btn btn-table" id="drop4" role="button" data-toggle="dropdown" href="#">Action <b class="caret"></b></a>
								<ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
									<li>
										<a href="javascript::" class="edit-logbook btn btn-default" data-id="<?php echo $log->hidId; ?>" data-pembimbing="<?php echo $log->pembimbing_id; ?>">
											<i class="fa fa-edit"></i> edit
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php endif; ?>
	</div>
	
</div>