<?php $this->head(); ?>
<?php 
if($pengajuan){
	foreach ($pengajuan as $p) {
		$pengajuan_id		= $p->pengajuan_id;
		$hidPId				= $p->hidId;
		$nama_perusahaan	= $p->nama_perusahaan;
		$alamat_perusahaan	= $p->alamat;
		$tgl_mulai			= $p->tgl_mulai;
		$tgl_selesai		= $p->tgl_selesai;
		$is_status			= $p->is_status;
		$mhs				= $p->nama_mhs;
	}	
}
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/kknp/info'); ?>">KKNP</a></li>
  <li class="active"><a href="#">Logbook</a></li>
</ol>
<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/kknp/info'); ?>" class="btn btn-primary">
	<i class="fa fa-list icon-white"></i> KKNP List</a> 
</div>

<div class="row" id="index-form">
	<div class="col-md-12">
		<form id="form" class="form-horizontal">
			<table id="example" class="table table-bordered">
			    <tbody>
			    	<tr>
			    		<td width="20%">Nama Perusahaan</td>
			    		<td><?php echo $nama_perusahaan ?></td>
			    	</tr>
			    	<tr>
			    		<td width="20%">Alamat Perusahaan</td>
			    		<td><?php echo $alamat_perusahaan ?></td>
			    	</tr>
			    	<tr>
			    		<td width="20%">Tanggal KKNP</td>
			    		<td><?php echo $tgl_mulai ?> s/d <?php echo $tgl_selesai ?></td>
			    	</tr>
			    	<tr>
			    		<td width="20%">Status</td>
			    		<td><?php echo $is_status ?></td>
			    	</tr>
			    	<tr>
			    		<td width="20%">Mahasiswa</td>
			    		<td><?php 
			    			$mhs = explode('@', $mhs);
							for ($i=0; $i < count($mhs) ; $i++) { 
								$mahasiswa = explode('|', $mhs[$i]);
								echo '<a href="javascript::" class="select-mhs" data-prodi="'.$mahasiswa[1].'" data-nama="'.$mahasiswa[0].'" data-aktif="'.$mahasiswa[2].'" data-mhs="'.$mahasiswa[3].'">';
								echo '<i class="fa fa-user">&nbsp;&nbsp;</i>'.$mahasiswa[0];
								if($mahasiswa[2]==0){
									echo '<sup><small>*</small></sup>';
								}
								echo '&nbsp;<span style="color:#c09853;"><small>'.$mahasiswa[1].'</small></span><br>';
								echo '</a>';
							}
			    		?></td>
			    	</tr>
			    </tbody>
			</table>
			
			<div id="selected-mhs-form"></div>
			
			<div class="form-group">
				<div class="col-sm-12">
					<input type="hidden" name="hidPId" class="hidPId form-control" value="<?php echo $hidPId ?>" />
				</div>
			</div>
			
		</form>
	</div>
</div>

<?php $this->foot(); ?>