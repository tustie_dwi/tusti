<?php if (isset($log)) : ?>	
	<?php for ($i=0; $i < count($mhs) ; $i++) {  
	$mahasiswa = explode('|', $mhs); 
	if($mahasiswa[2]==1):
	?>
	<div class="well">
		<h4 class="form-for-nilai-mhs col-md-10" style="padding-left: 0px;">
			<?php echo $mahasiswa[0].'&nbsp;<span style="color:#c09853;"><small>'.$mahasiswa[1].'</small></span><br>'; ?>
		</h4>
		<a href="<?php echo $this->location('module/kknp/logbook/add/'.$pengajuan_id_md5); ?>" class="btn btn-default pull-right">
		<i class="fa fa-list icon-white"></i> Show Logbook</a> 
		<table border="0" id="example" class="table table-hover">					
			<tbody>
				<?php 
				foreach ($log as $log) { ?>
				<tr class="tr-curr">				
					<td width="10%">
						<?php echo $log->nama_kegiatan ?>&nbsp;
						<?php 
							$tgl = explode(' ', $log->tgl);
							$tgl = $tgl[0];
						?> 
						<span class="label label-success"><?php echo $tgl ?></span>
					</td>
					<td width="15%">
						<?php echo $log->nama_pembimbing ?>
					</td>
					<td>
						<?php echo $log->catatan ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php 
	endif; 
	} ?>
<?php endif; ?>