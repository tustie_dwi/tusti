<?php $this->head(); ?>
<?php 
if($pengajuan){
	foreach ($pengajuan as $p) {
		$pengajuan_id		= $p->pengajuan_id;
		$hidPId				= $p->hidId;
		$nama_perusahaan	= $p->nama_perusahaan;
		$alamat_perusahaan	= $p->alamat;
		$tgl_mulai			= $p->tgl_mulai;
		$tgl_selesai		= $p->tgl_selesai;
		$is_status			= $p->is_status;
		$mhs				= $p->nama_mhs;
	}
	// print_r($data_nilai);
	$data_nilai = json_decode($data_nilai);
}
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/kknp/info'); ?>">KKNP</a></li>
  <li class="active"><a href="#">Nilai</a></li>
</ol>
<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/kknp/info'); ?>" class="btn btn-primary">
	<i class="fa fa-list icon-white"></i> KKNP List</a> 
</div>

<div class="row" id="index-form">
	<div class="col-md-12">
		<form id="form">
		<table id="example" class="table table-bordered">
		    <tbody>
		    	<tr>
		    		<td width="20%">Nama Perusahaan</td>
		    		<td><?php echo $nama_perusahaan ?></td>
		    	</tr>
		    	<tr>
		    		<td width="20%">Alamat Perusahaan</td>
		    		<td><?php echo $alamat_perusahaan ?></td>
		    	</tr>
		    	<tr>
		    		<td width="20%">Tanggal KKNP</td>
		    		<td><?php echo $tgl_mulai ?> s/d <?php echo $tgl_selesai ?></td>
		    	</tr>
		    	<tr>
		    		<td width="20%">Status</td>
		    		<td><?php echo $is_status ?></td>
		    	</tr>
		    	<tr>
		    		<td width="20%">Mahasiswa</td>
		    		<td><?php 
		    			$mhs = explode('@', $mhs);
						for ($i=0; $i < count($mhs) ; $i++) { 
							$mahasiswa = explode('|', $mhs[$i]);
							echo '<i class="fa fa-user">&nbsp;&nbsp;</i>'.$mahasiswa[0];
							if($mahasiswa[2]==0){
								echo '<sup><small>*</small></sup>';
							}
							echo '&nbsp;<span style="color:#c09853;"><small>'.$mahasiswa[1].'</small></span><br>';
						}
		    		?></td>
		    	</tr>
		    </tbody>
		</table>
		
		<div class="penilaian-content">
			<ul class="nav nav-tabs" role="tablist"style="margin-bottom: 10px;" id="myTab">
		  	<?php 
		  		$i = 0;
				$pembimbing = $data_nilai->{'pembimbing'};
				foreach ($pembimbing as $p) {
				  if($p->{'nama'} != ''):
					if($i==0){echo '<li class="active">';}else echo '<li>';  
					echo '<a href="#'.($p->{'pembimbing_ke'}).'" role="tab" data-toggle="tab">'.($p->{'nama'}).'</a></li>';
				  endif; 
				  $i++;
				} 
			?>
			</ul>
			
			<div class="tab-content">
		  	<?php 
		  		$i = 0;
				$pembimbing = $data_nilai->{'pembimbing'};
				$komponen	= $data_nilai->{'komponen'};
				$nilai_exist= $data_nilai->{'nilai'};
				// print_r($nilai_exist);
				foreach ($pembimbing as $p) {
				  if($p->{'nama'} != ''): 
					if($i==0){echo '<div class="tab-pane active" id="'.($p->{'pembimbing_ke'}).'">';}
					else echo '<div class="tab-pane" id="'.($p->{'pembimbing_ke'}).'">';
					?>
					<div class="tab-content-nilai-kknp">
						<?php for ($i=0; $i < count($mhs) ; $i++) {  
						$mahasiswa = explode('|', $mhs[$i]); 
						if($mahasiswa[2]==1):
						?>
						<div class="well">
							<h4 class="form-for-nilai-mhs"><?php echo $mahasiswa[0].'&nbsp;<span style="color:#c09853;"><small>'.$mahasiswa[1].'</small></span><br>'; ?></h4>
							<table border="0" id="example" class="table table-hover">					
								<tbody>
									<?php foreach ($komponen as $k) { ?>
									<tr class="tr-curr">				
										<td width="20%"><?php echo $k->keterangan ?></td>
										<td>:</td>
										<td>
											<?php $key = get_key_nilai($nilai_exist,$k->hidId,$mahasiswa[3],$p->pembimbing_id); ?>
											<input type="hidden" value="<?php echo $k->hidId ?>" name="komponen_id[]">
											<input type="hidden" value="<?php echo $mahasiswa[3] ?>" name="kknp_mhs_id[]">
											<input type="hidden" value="<?php echo $p->pembimbing_id ?>" name="pembimbing_id[]">
											<input type="text" class="input-nilai form-control" value="<?php if(isset($nilai_exist[$key]))echo $nilai_exist[$key]->nilai ?>" name="nilai[]">
											<input type="hidden" class="form-control" value="<?php if(isset($nilai_exist[$key]))echo $nilai_exist[$key]->inf_huruf ?>" name="inf_huruf[]">
											<input type="hidden" class="form-control" value="<?php if(isset($nilai_exist[$key]))echo $nilai_exist[$key]->hidId ?>" name="hidId[]">
											
										</td>
										<td style="width: 50%">
											<?php if(isset($nilai_exist[$key]))$label = bacaTotal($nilai_exist[$key]->nilai);else $label = 'label-danger'; ?>
											<h4 class="inf-huruf-show"><span class="label <?php echo $label ?>"> <?php if(isset($nilai_exist[$key]))echo $nilai_exist[$key]->inf_huruf;else echo "F"; ?></span></h4>										
										</td>
									</tr>
									<?php } ?>	
								</tbody>
							</table>
						</div>
						<?php 
						endif; 
						} ?>
					</div>
					<?php
					echo '</div>';
				  endif;
				  $i++; 
				} 
			?>
			</div>
		</div>
		
		<div class="form-group">
			<input type="hidden" name="hidPId" class="hidPId form-control" value="<?php echo $hidPId ?>" />
			<a href="javascript::" class="btn btn-primary" id="submit-nilai" type="submit">Submit</a>
		</div>
		
		</form>
	</div>
</div>

<?php 
function get_key_nilai($nilai_exist,$komponen_id,$mhs_kknp_id,$pembimbing_id){
	if(isset($nilai_exist)){
		foreach ($nilai_exist as $key => $value) {
			if( $komponen_id	== $value->komponen_id &&
			    $mhs_kknp_id	== $value->kknp_mhs_id &&
			    $pembimbing_id	== $value->pembimbing_id
			  ){
				return $key;
			}
		}
	}
}

function bacaTotal($total){
	if($total >= 80) $label = 'label-success';
	elseif($total >= 70) $label = 'label-primary';
	elseif($total >= 60) $label = 'label-warning';
	else $label = 'label-danger';
	return $label;
}

?>

<?php $this->foot(); ?>