<?php $this->head(); ?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li class="active"><a href="<?php echo $this->location('module/kknp/info'); ?>">KKNP</a></li>
</ol>
<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/kknp/info'); ?>" class="btn btn-primary">
	<i class="fa fa-list icon-white"></i> KKNP List</a> 
</div>

<div class="row" id="index-form">
	<div class="col-md-12">
		<!-- <div id="results"></div> -->
    	<?php  if( isset($posts) ) : ?>
    	
    	<div class="block-box">
			<form id="index-kknp-form">
				<div class="form-group">
					<input type="text" name="mhs" class="mhs-index form-control typeahead" value="" placeholder="Mahasiswa" />
				</div>
			</form>
			<div id="results"></div>
		</div>
    	
    	<?php else: ?>
    	
    	<div class="well" align="center">No Data Available</div>
    	
    	<?php endif; ?>
	</div>
</div>

<?php $this->foot(); ?>