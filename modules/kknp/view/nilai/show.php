<?php $data_nilai = json_decode($data_nilai); ?>
<div class="penilaian-content">
	<ul class="nav nav-tabs" role="tablist"style="margin-bottom: 10px;" id="myTab">
  	<?php 
  		$i = 0;
		$pembimbing = $data_nilai->{'pembimbing'};
		foreach ($pembimbing as $p) {
		  if($p->{'nama'} != ''):
			if($i==0){echo '<li class="active">';}else echo '<li>';  
			echo '<a href="#'.($p->{'pembimbing_ke'}).'" role="tab" data-toggle="tab">'.($p->{'nama'}).'</a></li>';
		  endif; 
		  $i++;
		} 
	?>
	</ul>
	
	<div class="tab-content">
  	<?php 
  		$i = 0;
		$pembimbing = $data_nilai->{'pembimbing'};
		$komponen	= $data_nilai->{'komponen'};
		$nilai_exist= $data_nilai->{'nilai'};
		// print_r($nilai_exist);
		foreach ($pembimbing as $p) {
		  if($p->{'nama'} != ''): 
			if($i==0){echo '<div class="tab-pane active" id="'.($p->{'pembimbing_ke'}).'">';}
			else echo '<div class="tab-pane" id="'.($p->{'pembimbing_ke'}).'">';
			?>
			<div class="tab-content-nilai-kknp">
				<?php for ($i=0; $i < count($mhs) ; $i++) {  
				$mahasiswa = explode('|', $mhs); 
				if($mahasiswa[2]==1):
				?>
				<div class="well">
					<h4 class="form-for-nilai-mhs"><?php echo $mahasiswa[0].'&nbsp;<span style="color:#c09853;"><small>'.$mahasiswa[1].'</small></span><br>'; ?></h4>
					<table border="0" id="example" class="table table-hover">					
						<tbody>
							<?php foreach ($komponen as $k) { ?>
							<tr class="tr-curr">				
								<td width="20%"><?php echo $k->keterangan ?></td>
								<td>:</td>
								<td>
									<?php $key = get_key_nilai($nilai_exist,$k->hidId,$mahasiswa[3],$p->pembimbing_id); ?>
									<?php if(isset($nilai_exist[$key]))echo $nilai_exist[$key]->nilai;else echo "-"; ?>
								</td>
								<td style="width: 50%">
									<?php if(isset($nilai_exist[$key]))$label = bacaTotal($nilai_exist[$key]->nilai);else $label = 'label-danger'; ?>
									<h4 class="inf-huruf-show"><span class="label <?php echo $label ?>"> <?php if(isset($nilai_exist[$key]))echo $nilai_exist[$key]->inf_huruf;else echo "F"; ?></span></h4>										
								</td>
							</tr>
							<?php } ?>	
						</tbody>
					</table>
				</div>
				<?php 
				endif; 
				} ?>
			</div>
			<?php
			echo '</div>';
		  endif;
		  $i++; 
		} 
	?>
	</div>
</div>
		
<?php 
function get_key_nilai($nilai_exist,$komponen_id,$mhs_kknp_id,$pembimbing_id){
	if(isset($nilai_exist)){
		foreach ($nilai_exist as $key => $value) {
			if( $komponen_id	== $value->komponen_id &&
			    $mhs_kknp_id	== $value->kknp_mhs_id &&
			    $pembimbing_id	== $value->pembimbing_id
			  ){
				return $key;
			}
		}
	}
}

function bacaTotal($total){
	if($total >= 80) $label = 'label-success';
	elseif($total >= 70) $label = 'label-primary';
	elseif($total >= 60) $label = 'label-warning';
	else $label = 'label-danger';
	return $label;
}

?>