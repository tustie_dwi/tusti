<?php 
if($sys=='new'){
	$sidebar_header = '<i class="fa fa-pencil"></i> New Komponen Nilai';
	$ket = '';$bobot = '';$urut = '';$hidId = '';$aktif = '';
}else{
	$sidebar_header = '<i class="fa fa-edit"></i> Edit Komponen Nilai';
	
	$edit_posts = json_decode($edit_posts); 
	foreach ($edit_posts as $edit) {
		$ket 	= $edit->keterangan;
		$bobot 	= $edit->bobot;
		$urut 	= $edit->urut;
		$hidId 	= $edit->hidId;
		$aktif 	= $edit->is_aktif;
		$parentid = $edit->hidId2;
	}
	
}
?>
<div class="panel panel-default">
	<div class="panel-heading"><?php echo $sidebar_header ?></div>
	<div class="panel-body ">
		<form id="form">		
			<div class="form-group">
			  <label>Keterangan</label>
			  <textarea id="keterangan" name="keterangan" class="form-control"><?php echo $ket ?></textarea>
			</div>
			
			<div class="form-group">
			  <label>Bobot</label>
			  <input type="text" value="<?php echo $bobot ?>" class="form-control" id="bobot" name="bobot" required="required">
			</div>
			
			<div class="form-group">
			  <label>Urut</label>
			  <input type="text" value="<?php echo $urut ?>" class="form-control" id="urut" name="urut" required="required">
			</div>
			
			<?php if(isset($posts)){ ?>
			<div class="form-group">
			  <label>Parent</label>
			  <select name="parent" class="form-control">
			  	<option value="0">Select Parent</option>
			  	<?php
			  		$posts = json_decode($posts); 
					foreach($posts as $p):
						echo "<option value='".$p->komponen_id."' ";
						if(isset($parentid) && $parentid==$p->hidId){
							echo "selected";
						}
						echo " >".$p->keterangan."</option>";
					endforeach;
				?>
			  </select>
			</div>
			<?php } ?>
			
			<div class="form-group">
				<label class="radio-inline" style="padding-left: 0px;">
				  <input name="is_aktif" value="1" type="checkbox" <?php if($aktif=='1') echo 'checked="checked"'; ?> > <b>Aktif</b>
				</label>
			</div>
			
			<div class="form-group">
			  <input type="hidden" name="hidId" value="<?php echo $hidId ?>" />
			  <a href="javascript::" class="btn btn-primary" id="submit-komponen" type="submit">Submit</a>
			  <?php if($sys!='new'){ ?>
			  <a class="btn btn-danger" href="<?php echo $this->location('module/kknp/komponen'); ?>">Cancel</a>
			  <?php } ?>
			</div>
					
		</form>
	</div>
</div>