<?php $this->head(); ?>
<style>
	.btn-edit{
		border : none;
		padding: 3px 5px;
		padding-left: 10px;
	}
</style>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/kknp/info'); ?>">KKNP</a></li>
  <li class="active"><a href="#">Komponen</a></li>
</ol>
<div class="breadcrumb-more-action">
</div>

<div class="row" id="index-form">
	<div class="col-md-7">
		<!-- <div id="results"></div> -->
    	<?php  if( isset($posts) ) : ?>
    	
    	<div class="block-box">
    		<div class="tree">
				<ul>
    		<?php 
    			$posts = json_decode($posts); 
				foreach($posts as $row): ?>
						<li>
							<span><i class='fa fa-minus-square'></i> <?php print_r($row->keterangan); ?></span>
							
							<div class="btn-group">
							    <button type="button" class="btn btn-default dropdown-toggle btn-edit" data-toggle="dropdown">
							      Action
							      <span class="fa fa-caret-down"></span>
							    </button>
							    <ul class="dropdown-menu">
							      <li><a class="" href="<?php echo $this->location('module/kknp/komponen/index/'.$row->komponen_id) ?>"><i class='fa fa-edit'></i> Edit</a></li>
							    </ul>
							</div>	
							
							<?php 
							if($row->child){
								child($row->child,$this->location()); 
							}	
							?>
						</li>
			<?php endforeach; ?>
 				</ul>
			</div>
		</div>
    	
    	<?php else: ?>
    	
    	<div class="well" align="center">No Data Available</div>
    	
    	<?php endif; ?>
	</div>
	
	<div class="col-md-5">
		<div class="block-box">
			<?php $this->view('komponen/sidebar.php', $data ); ?>
		</div>
	</div>
</div>

<?php 
function child($data,$this_location){
	$posts = ($data); 
	foreach($posts as $row):
		echo "<ul>";
		echo "<li>";
		echo "<span><i class='fa fa-minus-square'></i>&nbsp;".$row->keterangan."</span>";
		echo '	<div class="btn-group">
				    <button type="button" class="btn btn-default dropdown-toggle btn-edit" data-toggle="dropdown">
				      Action
				      <span class="fa fa-caret-down"></span>
				    </button>
				    <ul class="dropdown-menu">
				      <li><a class="" href="'.$this_location.'module/kknp/komponen/index/'.$row->komponen_id.'"><i class="fa fa-edit"></i> Edit</a></li>
				    </ul>
				</div>';
		// $this->child($d->jabatan_id);
		echo "</li>";
		echo "</ul>";
	endforeach;
}
?>

<?php $this->foot(); ?>