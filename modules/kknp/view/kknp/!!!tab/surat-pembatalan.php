<div class="form-group" style="margin-top: 15px;">
	<label class="col-sm-2 control-label">Mahasiswa Batal</label>
	<div class="col-sm-10">
		<input type="text" class="form-control typeahead" name="mhs_batal" id="mhs_batal" value="<?php if(isset($mhs_batal)) echo $mhs_batal ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">No Batal</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="no_batal" id="no_batal" value="<?php if(isset($no_batal)) echo $no_batal ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Batal</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="tgl_batal" id="tgl_batal" value="<?php if(isset($tgl_batal)) echo $tgl_batal ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Balasan Batal</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="tgl_batal_selesai" id="tgl_batal_selesai" value="<?php if(isset($tgl_batal_selesai)) echo $tgl_batal_selesai ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Isi Surat Batal</label>
	<div class="col-sm-10">
		<textarea name="isi_surat_batal" class="form-control ckeditor" rows="8" cols="40"></textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-10">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-surat-batal"><i class="fa fa-cloud-upload"></i> Upload File</button>
	</div>
</div>

<div class="modal fade" id="modal-surat-batal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload File Surat Batal</h4>
      </div>
      <div class="modal-body modal-body-surat-batal">
      	<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<a href="javascript::" class="btn btn-info" onclick="addFile(this, 'batal')"><i class="fa fa-plus"></i> Tambah File</a>
			</div>
		</div>
		
		<div class="form-file-surat-batal">
			<div class="form-group">
				<label class="col-sm-2 control-label">Judul</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="judul_file_surat_batal[]" />
				</div>
				<div class="col-sm-1">
					<a href="javascript::" class="btn btn-danger" onclick="delete_(this,'batal')"><i class="fa fa-minus"></i></a>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Keterangan</label>
				<div class="col-sm-9">
					<textarea class="form-control" name="keterangan_file_surat_batal[]"></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">File Batal</label>
				<div class="col-sm-10">
					<input type="file" class="form-control" name="file_surat_batal[]" />
					<label class="radio-inline" style="padding-left: 0px;">
						<input name="publish_file_surat_batal[]" value="1" type="checkbox" > <b>Publish File</b>
					</label>
				</div>
			</div>
		</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<input type="hidden" name="hidId6" value="<?php if(isset($hidId6)) echo $hidId6 ?>"/>
<input type="hidden" name="template_batal" value="" />
<input type="hidden" name="jenis_surat[]" value="batal" />