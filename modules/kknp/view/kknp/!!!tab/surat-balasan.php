<div class="form-group" style="margin-top: 15px;">
	<label class="col-sm-2 control-label">No Balasan</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="no_balasan" id="no_balasan" value="<?php if(isset($no_balasan)) echo $no_balasan ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Balasan</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="tgl_balasan" id="tgl_balasan" value="<?php if(isset($tgl_balasan)) echo $tgl_balasan ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Balasan Selesai</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="tgl_balasan_selesai" id="tgl_balasan_selesai" value="<?php if(isset($tgl_balasan_selesai)) echo $tgl_balasan_selesai ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Isi Surat Balasan</label>
	<div class="col-sm-10">
		<textarea name="isi_surat_balasan" class="form-control ckeditor" rows="8" cols="40"></textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-10">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-surat-balasan"><i class="fa fa-cloud-upload"></i> Upload File</button>
	</div>
</div>

<div class="modal fade" id="modal-surat-balasan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload File Surat Balasan</h4>
      </div>
      <div class="modal-body modal-body-surat-balasan">
      	<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<a href="javascript::" class="btn btn-info" onclick="addFile(this, 'balasan')"><i class="fa fa-plus"></i> Tambah File</a>
			</div>
		</div>
      	
      	<div class="form-file-surat-balasan">
      		<div class="form-group">
				<label class="col-sm-2 control-label">Judul</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="judul_file_surat_balasan[]" />
				</div>
				<div class="col-sm-1">
					<a href="javascript::" class="btn btn-danger" onclick="delete_(this,'balasan')"><i class="fa fa-minus"></i></a>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Keterangan</label>
				<div class="col-sm-9">
					<textarea class="form-control" name="keterangan_file_surat_balasan[]"></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">File Balasan</label>
				<div class="col-sm-10">
					<input type="file" class="form-control" name="file_surat_balasan[]" />
					<label class="radio-inline" style="padding-left: 0px;">
						<input name="publish_file_surat_balasan[]" value="1" type="checkbox" > <b>Publish File</b>
					</label>
				</div>
			</div>
		</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" name="hidId5" value="<?php if(isset($hidId5)) echo $hidId5 ?>"/>
<input type="hidden" name="template_balasan" value="" />
<input type="hidden" name="jenis_surat[]" value="balasan" />