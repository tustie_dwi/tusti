<div class="form-group" style="margin-top: 15px;">
	<label class="col-sm-2 control-label">No Surat</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="no_tugas" id="no_tugas" value="<?php if(isset($no_tugas)) echo $no_tugas ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Surat</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="tgl_tugas" id="tgl_tugas" value="<?php if(isset($tgl_tugas)) echo $tgl_tugas ?>"/>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Isi Surat</label>
	<div class="col-sm-10">
		<textarea name="isi_surat_tugas" class="form-control ckeditor" rows="8" cols="40"><?php echo form_surat($surat_tugas_kknp, $this->config->file_url_view) ?></textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-10">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-surat-tugas"><i class="fa fa-cloud-upload"></i> Upload File</button>
	</div>
</div>

<div class="modal fade" id="modal-surat-tugas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload File Surat Tugas</h4>
      </div>
      <div class="modal-body modal-body-surat-tugas">
      	<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<a href="javascript::" class="btn btn-info" onclick="addFile(this, 'tugas')"><i class="fa fa-plus"></i> Tambah File</a>
			</div>
		</div>
		
      	<div class="form-file-surat-tugas">
	      	<div class="form-group">
				<label class="col-sm-2 control-label">Judul</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="judul_file_surat_tugas[]" />
				</div>
				<div class="col-sm-1">
					<a href="javascript::" class="btn btn-danger" onclick="delete_(this,'tugas')"><i class="fa fa-minus"></i></a>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Keterangan</label>
				<div class="col-sm-9">
					<textarea class="form-control" name="keterangan_file_surat_tugas[]"></textarea>
				</div>
			</div>
			
	        <div class="form-group">
				<label class="col-sm-2 control-label">File Surat</label>
				<div class="col-sm-9">
					<input type="file" class="form-control" name="file_surat_tugas[]" />
					<label class="radio-inline" style="padding-left: 0px;">
						<input name="publish_file_surat_tugas[]" value="1" type="checkbox" > <b>Publish File</b>
					</label>
				</div>
			</div>
		</div>		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<input type="hidden" name="hidId3" value="<?php if(isset($hidId3)) echo $hidId3 ?>"/>
<input type="hidden" name="template_tugas" value="<?php echo $surat_tugas_kknp[0]->template_id ?>" />
<input type="hidden" name="jenis_surat[]" value="tugas" />