<?php 
$this->head(); 

if(isset($posts)){
	foreach ($posts as $p) {
		$hid_id1			= $p->hidId;
		$cabangid 			= $p->cabang_id;
		$fakultasid 		= $p->fakultas_id;
		$thnakademikid  	= $p->tahun_akademik;
		$perusahaan_name	= $p->nama_perusahaan;
		$perusahaan_alamat	= $p->alamat;
		$tgl_mulai			= $p->tgl_mulai;
		$tgl_selesai		= $p->tgl_selesai;
		$tgl_laporan		= $p->tgl_laporan;
		$is_status			= $p->is_status;
		$catatan			= $p->keterangan;
	}
	$data['pengajuan_id'] = $hid_id1;
}else{
	$data['pengajuan_id'] = NULL;
	$is_status = NULL;
}

?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/kknp/info'); ?>">KKNP</a></li>
  <li class="active"><a href="#">Pengajuan</a></li>
</ol>
<div class="breadcrumb-more-action">
	<?php if($header!='Pengajuan KKNP'){ ?>
	<a href="<?php echo $this->location('module/kknp/info/pengajuan'); ?>" class="btn btn-primary">
	<i class="fa fa-pencil icon-white"></i> New KKNP</a> 
	<?php } ?>
	<a href="<?php echo $this->location('module/kknp/info'); ?>" class="btn btn-primary">
	<i class="fa fa-list icon-white"></i> KKNP List</a> 
</div>

<div class="row" id="edit-form">
  <div class="col-md-12">
	<div class="block-box">
		<div id="results"></div>
		<form id="form-daftar-ulang" class="form-horizontal" enctype="multipart/form-data">
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Cabang</label>
				<div class="col-sm-10">
					<select name="cabang" class="form-control e9">
					<option value="0">Pilih Cabang</option>
					<?php 
					if(count($cabang)>0):
						foreach ($cabang as $c) {
							echo "<option value='".$c->cabang_id."' ";
							if(isset($cabangid)){
								if($cabangid==$c->cabang_id){
									echo "selected";
								}
							}
							echo " >".ucWords($c->keterangan)."</option>";
						}
					endif;
					?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Fakultas</label>
				<div class="col-sm-10">
					<select name="fakultas" class="form-control e9">
					<option value="0">Pilih Fakultas</option>
					<?php 
					if(count($fakultas)>0):
						foreach ($fakultas as $f) {
							echo "<option value='".$f->fakultas_id."' ";
							if(isset($fakultasid)){
								if($fakultasid==$f->fakultas_id){
									echo "selected";
								}
							}
							echo " >".ucWords($f->keterangan)."</option>";
						}
					endif;
					?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Semester Pengajuan</label>
				<div class="col-sm-10">
					<select name="thn_akademik" class="form-control e9">
					<option value="0">Pilih Tahun Akademik</option>
					<?php if(count($thnakademik)> 0) {
						foreach($thnakademik as $th) :
							echo "<option value='".$th->tahun_akademik."' ";
							if(isset($thnakademikid)){
								if($thnakademikid==$th->tahun_akademik){
									echo "selected";
								}
							}
							echo " >".ucWords($th->thnakademik)."</option>";
						endforeach;
					} ?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Nama Perusahaan</label>
				<div class="col-sm-10">
					<input type="text" required="required" class="form-control typeahead" name="perusahaan_name" id="perusahaan_name" value="<?php if(isset($perusahaan_name)) echo $perusahaan_name ?>"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Alamat Perusahaan</label>
				<div class="col-sm-10">
					<input type="text" required="required" class="form-control" name="perusahaan_alamat" id="perusahaan_alamat" value="<?php if(isset($perusahaan_alamat)) echo $perusahaan_alamat ?>"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Tanggal KKNP</label>
				<div class="col-md-5">
					<input type="text" placeholder="Tanggal Mulai" required="required" class="form-control" name="tgl_mulai" id="tgl_mulai" value="<?php if(isset($tgl_mulai)) echo $tgl_mulai ?>"/>
				</div>
				<div class="col-md-5">
					<input type="text" placeholder="Tanggal Selesai" required="required" class="form-control" name="tgl_selesai" id="tgl_selesai" value="<?php if(isset($tgl_selesai)) echo $tgl_selesai ?>"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Mahasiswa</label>
				<div class="col-sm-10">
					<input type="text" required="required" class="form-control typeahead" name="mhs_kknp" id="mhs_kknp" value="<?php if(isset($mhs_kknp)) echo $mhs_kknp ?>"/>
					<input type="hidden" class="form-control" id="existed-mhs-kknp" name="existed-mhs-kknp" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Dosen Pembimbing</label>
				<div class="col-sm-10">
					<input type="text" required="required" class="form-control typeahead" name="pembimbing_dalam" id="pembimbing_dalam" value="<?php if(isset($pembimbing_dalam)) echo $pembimbing_dalam ?>"/>
					<input type="hidden" class="form-control typeahead" name="pembimbing_dalam_id" id="pembimbing_dalam_id" value="<?php if(isset($pembimbing_dalam_id)) echo $pembimbing_dalam_id ?>" />
					<input type="hidden" name="hidId11" value="<?php if(isset($hid_id11))echo $hid_id11; ?>" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Pembimbing Lapangan</label>
				<div class="col-sm-10">
					<input type="text" class="form-control typeahead" name="pembimbing_luar" id="pembimbing_luar" value="<?php if(isset($pembimbing_luar)) echo $pembimbing_luar ?>"/>
					<input type="hidden" name="hidId12" value="<?php if(isset($hid_id12))echo $hid_id12; ?>" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Tanggal Laporan</label>
				<div class="col-sm-10">
					<input type="text" required="required" class="form-control" name="tgl_laporan" id="tgl_laporan" value="<?php if(isset($tgl_laporan)) echo $tgl_laporan ?>"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Status</label>
				<div class="col-sm-10">
					<select name="status" class="form-control e9">
						<option value="pengajuan" <?php if($is_status=='pengajuan')echo "selected";?> >Pengajuan</option>
						<option value="diterima" <?php if($is_status=='diterima')echo "selected";?>>Diterima</option>
						<option value="selesai" <?php if($is_status=='selesai')echo "selected";?>>Selesai</option>
						<option value="ditolak" <?php if($is_status=='ditolak')echo "selected";?>>Ditolak</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Objek</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="objek" id="objek" value="<?php if(isset($objek)) echo $objek ?>"/>
					<input type="hidden" class="form-control" id="existed-objek-kknp" name="existed-objek-kknp" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Catatan</label>
				<div class="col-sm-10">
					<textarea name="catatan" id="catatan" rows="8" cols="40" class="form-control"><?php if(isset($catatan)) echo $catatan ?></textarea>
				</div>
			</div>
			
			<ul class="nav nav-tabs" role="tablist">
			  <li role="presentation" class="active"><a href="#st-tgs-dosen" role="tab" data-toggle="tab">Surat Tugas Dosen</a></li>
			  <li role="presentation"><a href="#st-pengantar-prh" role="tab" data-toggle="tab">Surat Pengantar Perusahaan</a></li>
			  <li role="presentation"><a href="#st-balasan" role="tab" data-toggle="tab">Surat Balasan</a></li>
			  <li role="presentation"><a href="#st-batal" role="tab" data-toggle="tab">Surat Pembatalan</a></li>
			</ul>

			<div class="tab-content">
			  <div role="tabpanel" class="tab-pane active" id="st-tgs-dosen"><?php $this->view('kknp/tab/surat-tugas-dosen.php', $data) ?></div>
			  <div role="tabpanel" class="tab-pane" id="st-pengantar-prh"><?php $this->view('kknp/tab/surat-pengantar-perusahaan.php', $data) ?></div>
			  <div role="tabpanel" class="tab-pane" id="st-balasan"><?php $this->view('kknp/tab/surat-balasan.php', $data) ?></div>
			  <div role="tabpanel" class="tab-pane" id="st-batal"><?php $this->view('kknp/tab/surat-pembatalan.php', $data) ?></div>
			</div>
			
			<div class="form-group">
				<label class="control-label">&nbsp;</label><br>
				<input type="hidden" name="hidId1" value="<?php if(isset($hid_id1))echo $hid_id1; ?>" />
				<!-- <input type="hidden" name="hidId2" value="<?php if(isset($hid_id2))echo $hid_id2; ?>" /> -->
				<input type="hidden" name="tgl_pengajuan" value="<?php if(isset($tgl_pengajuan))echo $tgl_pengajuan; ?>" />
				<a href="javascript::" class="btn btn-primary" id="submit-submission" type="submit">Submit</a>
			</div>
		</form>
	</div>
  </div>
</div>

<?php
function form_surat($surat, $url_view){
	$kop = "<img src='".$url_view."/".$surat[0]->kop_surat."' />";
	$isi = $surat[0]->isi_surat;
	return $kop.$isi;
}
?>

<?php $this->foot(); ?>