<?php $this->head(); ?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li class="active"><a href="<?php echo $this->location('module/kknp/info'); ?>">KKNP</a></li>
</ol>
<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/kknp/info/pengajuan'); ?>" class="btn btn-primary">
	<i class="fa fa-pencil icon-white"></i> New KKNP</a> 
</div>

<div class="row" id="index-form">
	<div class="col-md-12">
		<!-- <div id="results"></div> -->
    	<?php  if( isset($posts) ) : ?>
    	
    	<div class="block-box">
			<table class='table table-hover' id='example' data-id='module/master/general/fakultas'>
				<thead>
					<tr>
						<th style="width: 13%">Last Modified</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>				
				</tbody>
			</table>
		</div>
    	
    	<div class="modal fade" id="modal-surat-view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content modal-lg">
		      <div id="print-area" class="modal-body modal-body-surat-view">
								
		      </div>
		      <div class="modal-footer">
		      	<!-- <button type="button" class="btn btn-info" data-dismiss="modal"><i class="fa fa-print">&nbsp;</i>Print</button> -->
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
    	
    	<?php else: ?>
    	
    	<div class="well" align="center">No Data Available</div>
    	
    	<?php endif; ?>
	</div>
</div>

<?php $this->foot(); ?>