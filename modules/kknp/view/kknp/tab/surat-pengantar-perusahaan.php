<div class="form-group" style="margin-top: 15px;">
	<label class="col-sm-2 control-label">No Pengantar</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="no_pengantar" id="no_pengantar" value="<?php if(isset($no_pengantar)) echo $no_pengantar ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Pengantar</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="tgl_pengantar" id="tgl_pengantar" value="<?php if(isset($tgl_pengantar)) echo $tgl_pengantar ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Tanggal Pengantar Selesai</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="tgl_pengantar_selesai" id="tgl_pengantar_selesai" value="<?php if(isset($tgl_pengantar_selesai)) echo $tgl_pengantar_selesai ?>"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Isi Surat Pengantar</label>
	<div class="col-sm-10">
		<textarea name="isi_surat_pengantar" class="form-control ckeditor" rows="8" cols="40"><?php echo form_surat($surat_pengantar_kknp, $this->config->file_url_view) ?></textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-10">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-surat-pengantar"><i class="fa fa-cloud-upload"></i> Upload File</button>
	</div>
</div>

<div class="modal fade" id="modal-surat-pengantar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload File Surat Pengantar</h4>
      </div>
      <div class="modal-body modal-body-surat-pengantar">
      	
      	<div class="form-file-surat-pengantar">
      		<div class="form-group">
				<label class="col-sm-2 control-label">Judul</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="judul_file_surat_pengantar" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">Keterangan</label>
				<div class="col-sm-9">
					<textarea class="form-control" name="keterangan_file_surat_pengantar"></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label">File Pengantar</label>
				<div class="col-sm-10">
					<div id="file_surat_pengantar_view" style="margin-bottom: 5px;"></div>
					<input type="file" class="form-control" name="file_surat_pengantar" />
					<label class="radio-inline" style="padding-left: 0px;">
						<input name="publish_file_surat_pengantar" value="1" type="checkbox" > <b>Publish File</b>
					</label>
					<input type="hidden" name="hidId8" value="<?php if(isset($hidId8)) echo $hidId8 ?>"/>
					<input type="hidden" name="loc_surat_pengantar" class="form-control" >
				</div>
			</div>
		</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<input type="hidden" name="hidId4" value="<?php if(isset($hidId4)) echo $hidId4 ?>"/>
<input type="hidden" name="template_pengantar" value="<?php echo $surat_pengantar_kknp[0]->template_id ?>" />
<input type="hidden" name="jenis_surat[]" value="pengantar" />
<textarea name="hidden-surat-pengantar" style="display: none"><?php echo form_surat($surat_pengantar_kknp, $this->config->file_url_view) ?></textarea>