$(document).ready(function() {

});

$('#submit-komponen').click(function(e) {
	var keterangan 		= $('#keterangan').val();
	var bobot	 	 	= $('#bobot').val();
	var urut	  		= $('#urut').val();
	
	if(keterangan.length!=0 && bobot.length!=0 && urut.length!=0){
		submit_komponen(e);
	}else{
		alert('Lengkapi data anda!');
	}
});

function submit_komponen(e){
	var postData = new FormData($('#form')[0]);
	$.ajax({
		url : base_url + "module/kknp/komponen/save",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			// alert(data);
			// location.reload();
			if(data=='sukses'){
				alert('Proses Simpan Berhasil!');
				location.reload();
			}
			else{
				alert('Proses Simpan Gagal!');
			}
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Proses Simpan Gagal!');      
	    },
	    cache: false,
		contentType: false,
		processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
}
