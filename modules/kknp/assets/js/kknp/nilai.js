$(document).ready(function() {
	if($('#index-form').length){
		$(".mhs-index").autocomplete({ 
			source: base_url + "/module/kknp/info/mhs",
			minLength: 3, 
			select: function(event, ui) { 
				$('.mhs-index').val(ui.item.value);
			} 
		});
	}
});

$(document).on('blur', '.mhs-index', function(){
	var mhs = $(this).val();
	if(mhs.length > 0){
		$.ajax({
			type : "POST",
			dataType: "HTML",
			async:   true,
			url: base_url + "module/kknp/nilai/get_nilai_by_mahasiswa",
			data : $.param({
				mhs : mhs
			}),
			success: function(data){
				$('#results').html( data );
			}	
		});
	}else{
		$('#results').html( '' );
	}
});

$(document).on('blur', '.input-nilai', function(){
	var e 			= $(this);
	var parent 		= e.parents('.tr-curr');
	var huruf_show 	= parent.find('.inf-huruf-show');
	var huruf_hidden= parent.find('[name="inf_huruf[]"]');
	var nilai 		= e.val();
	var inf_huruf 	= '';
	var label		= get_label(nilai);
	
	if( nilai > 0 && nilai <= 44 ){
		inf_huruf = 'E';
	}
	else if( nilai > 44 && nilai <= 50 ){
		inf_huruf = 'D';
	}
	else if(nilai > 50 && nilai <=55){
		inf_huruf = 'D+';
	}
	else if(nilai > 55 && nilai <=60){
		inf_huruf = 'C';
	}
	else if(nilai > 60 && nilai <=69){
		inf_huruf = 'C+';
	}
	else if(nilai > 69 && nilai <=75){
		inf_huruf = 'B';
	}
	else if(nilai > 75 && nilai <=80){
		inf_huruf = 'B+';
	}
	else if(nilai > 80 && nilai <=100){
		inf_huruf = 'A';
	}
	else{
		$(this).val('');
		inf_huruf = 'F';
	}
	
	huruf_hidden.val(inf_huruf);
	huruf_show.html('<span class="label '+label+'">'+inf_huruf+'</span>');
	
});

function get_label(nilai){
	var label = '';
	if(nilai >= 80) label = 'label-success';
	else if(nilai >= 70) label = 'label-primary';
	else if(nilai >= 60) label = 'label-warning';
	else label = 'label-danger';
	return label;
}

$('#submit-nilai').click(function(e) {
	var postData = new FormData($('#form')[0]);
	$.ajax({
		url : base_url + "module/kknp/nilai/save",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			// alert(data);
			if(data==''){
				alert('Proses Simpan Berhasil!');
				location.reload();		
			}
			else if(data=='gagal'){
				alert ('Proses Simpan Gagal!'); 
			}
				
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Proses Simpan Gagal!');      
	    },
	    cache: false,
		contentType: false,
		processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
});