self.addEventListener('message', function(e) {
	var data = e.data;
	var base_url = data.base_url;
	var url_prodi = base_url+"module/kknp/info/prodi_mhs";
	var url_dosen = base_url+"module/kknp/info/data_dosen";
	
	var nama_mhs = Array();
	var nim_mhs = Array();
	
	var mhs = data.mhs;
	var dosen = data.dosen;
	
	for(var i=0; i< mhs.length; i++){
		var data_m = mhs[i].split('-');
		nama_mhs.push( data_m[1].substring(1) );
		nim_mhs.push( data_m[0].substring(0, (data_m[0].length-1) ) );
	}
	
	var must_change_tugas 	= data.must_change_tugas;
	var changer_tugas		= data.changer_tugas;
	// var content_tugas		= data.content_tugas;
	
	//------TUGAS----------------------------
	var c_tugas = '';
	for(var i=0; i< nama_mhs.length; i++){
		var content_tugas		= data.content_tugas;
		
		var params = "nama="+nama_mhs[i]+"&nim="+nim_mhs[i];
		var xhr = new XMLHttpRequest();
		xhr.open("POST", url_prodi, false); //false mean async false
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.setRequestHeader("Content-length", params.length);
		xhr.setRequestHeader("Connection", "close");
	    xhr.send(params);
	    
	    content_tugas = content_tugas.replace("{Nama Mahasiswa}", nama_mhs[i]);
		content_tugas = content_tugas.replace("{NIM Mahasiswa}", nim_mhs[i]);
		content_tugas = content_tugas.replace("{Jurusan Mahasiswa}", xhr.responseText);
		for(var x=0; x < must_change_tugas.length; x++){
			content_tugas = content_tugas.replace(must_change_tugas[x], changer_tugas[x]);
		}
		
		var params_dosen = "nama="+dosen;
		xhr.open("POST", url_dosen, false); //false mean async false
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.setRequestHeader("Content-length", params_dosen.length);
		xhr.setRequestHeader("Connection", "close");
	    xhr.send(params_dosen);
		
		var data_d = JSON.parse(xhr.responseText);
		content_tugas = content_tugas.replace("{Nama Tenaga Pengajar}", data_d[0].nama);
		content_tugas = content_tugas.replace("{NIP Tenaga Pengajar}", data_d[0].nik);
		content_tugas = content_tugas.replace("{Pangkat Tenaga Pengajar}", data_d[0].golongan);
		content_tugas = content_tugas.replace("{Pekerjaan Tenaga Pengajar}", data_d[0].jabatan);
		
		
		c_tugas = c_tugas+content_tugas; 
	}
	
	//------PENGANTAR------------------------
	var content_pengantar		= data.content_pengantar;
	
	var table_pengantar = '';
	table_pengantar += '<table border="1" cellpadding="0" cellspacing="0" style="width:500px;font-size:11px;font-family:arial,helvetica,sans-serif"><tbody>';

	table_pengantar += '<tr>';
	table_pengantar += '<td><strong>NIM</strong></td>';
	table_pengantar += '<td><strong>NAMA</strong></td>';
	table_pengantar += '</tr>';
	
	for(var i=0; i< nama_mhs.length; i++){
		table_pengantar += '<tr>';
		table_pengantar += '<td>'+nim_mhs[i]+'</td>';
		table_pengantar += '<td>'+nama_mhs[i]+'</td>';
		table_pengantar += '</tr>';
	}
	table_pengantar += '</tbody></table>';

	content_pengantar = content_pengantar.replace("{Mhs}", table_pengantar);
	
	var must_change_pengantar 	= data.must_change_pengantar;
	var changer_pengantar		= data.changer_pengantar;
	
	for(var i=0; i < must_change_pengantar.length; i++){
		content_pengantar = content_pengantar.replace(must_change_pengantar[i], changer_pengantar[i]);
	}
	
	//------BATAL------------------------
	if( data.mhs_batal!='' ){
		var must_change_batal 	= data.must_change_batal;
		var changer_batal		= data.changer_batal;
		var mhs_batal			= data.mhs_batal;
		
		var nama_mhs_batal = Array();
		var nim_mhs_batal = Array();
		
		for(var i=0; i< mhs_batal.length; i++){
			var data_m = mhs_batal[i].split('-');
			nama_mhs_batal.push( data_m[1].substring(1) );
			nim_mhs_batal.push( data_m[0].substring(0, (data_m[0].length-1) ) );
		}
		
		var c_batal = ''; 
		for(var i=0; i< nama_mhs_batal.length; i++){
			var content_batal		= data.content_batal;
			
			var params = "nama="+nama_mhs_batal[i]+"&nim="+nim_mhs_batal[i];
			// var xhr = new XMLHttpRequest();
			xhr.open("POST", url_prodi, false); //false mean async false
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.setRequestHeader("Content-length", params.length);
			xhr.setRequestHeader("Connection", "close");
		    xhr.send(params);
		    
		    content_batal = content_batal.replace("{Nama Mahasiswa}", nama_mhs_batal[i]);
			content_batal = content_batal.replace("{NIM Mahasiswa}", nim_mhs_batal[i]);
			content_batal = content_batal.replace("{Jurusan Mahasiswa}", xhr.responseText);
			for(var x=0; x < must_change_tugas.length; x++){
				content_batal = content_batal.replace(must_change_batal[x], changer_batal[x]);
			}
			
			var params_dosen = "nama="+dosen;
			xhr.open("POST", url_dosen, false); //false mean async false
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.setRequestHeader("Content-length", params_dosen.length);
			xhr.setRequestHeader("Connection", "close");
		    xhr.send(params_dosen);
			
			var data_d = JSON.parse(xhr.responseText);
			content_batal = content_batal.replace("{Nama Tenaga Pengajar}", data_d[0].nama);
			content_batal = content_batal.replace("{NIP Tenaga Pengajar}", data_d[0].nik);
			content_batal = content_batal.replace("{Pangkat Tenaga Pengajar}", data_d[0].golongan);
			content_batal = content_batal.replace("{Pekerjaan Tenaga Pengajar}", data_d[0].jabatan);
			
			
			c_batal = c_batal+content_batal; 
			
		}
	}else c_batal = data.content_batal;
	
	self.postMessage({
					  'content_tugas' : c_tugas,
					  'content_pengantar' : content_pengantar,
					  'content_batal' : c_batal
					 });
}, false);