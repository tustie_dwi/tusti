$(document).ready(function(){
	if($('#index-form').length){
		$(".mhs-index").autocomplete({ 
			source: base_url + "/module/kknp/info/mhs",
			minLength: 3, 
			select: function(event, ui) { 
				$('.mhs-index').val(ui.item.value);
			} 
		});
	}
});

$(document).on('blur', '.mhs-index', function(){
	var mhs = $(this).val();
	if(mhs.length > 0){
		$.ajax({
			type : "POST",
			dataType: "HTML",
			async:   true,
			url: base_url + "module/kknp/logbook/get_log_by_mahasiswa",
			data : $.param({
				mhs : mhs
			}),
			success: function(data){
				$('#results').html( data );
			}	
		});
	}else{
		$('#results').html( '' );
	}
});

$(document).on('click', '.edit-logbook', function(){
	var e = $(this).parents('.tr-curr');
	
	var catatan = e.find('.catatan-show').text();
	var nama_kegiatan = e.find('.nama-kegiatan-show').text();
	var tgl = e.find('.tgl-show').text();
	var hidId = $(this).data('id');
	var pembimbingid = $(this).data('pembimbing');
	
	$('[name="pembimbing"]').val(pembimbingid);
	$('[name="nama_kegiatan"]').val(nama_kegiatan);
	$('[name="tgl_kegiatan"]').val(tgl);
	$('[name="catatan"]').val(catatan);
	$('[name="hidId"]').val(hidId);
	$('#cancel-edit').show();

	$('html, body').animate({
        scrollTop: $("#selected-mhs-form").offset().top
    }, 500);
});

$(document).on('click', '#cancel-edit', function(){
	$('[name="pembimbing"]').val(0);
	$('[name="nama_kegiatan"]').val('');
	$('[name="tgl_kegiatan"]').val('');
	$('[name="catatan"]').val('');
	$('[name="hidId"]').val('');
	$('#cancel-edit').hide();
});

$(document).on('click', '.select-mhs', function(){
	var mhs_kknp_id  = $(this).data('mhs');
	var is_aktif	 = $(this).data('aktif');
	var pengajuan_id = $('[name="hidPId"]').val();
	
	var nama		 = $(this).data('nama');
	var prodi		 = $(this).data('prodi');
	
	if(is_aktif==1){
		$.ajax({
			type : "POST",
			dataType: "HTML",
			async:   true,
			url: base_url + "module/kknp/logbook/show_selected",
			data : $.param({
				mhs_kknp_id : mhs_kknp_id,
				is_aktif :  is_aktif,
				pengajuan_id : pengajuan_id,
				nama : nama,
				prodi : prodi
			}),
			success: function(data){
				// alert(data);
				$('#selected-mhs-form').html(data);
				$('.tgl_kegiatan').datetimepicker({
				  pickTime : false,
				  format : 'YYYY-MM-DD'
				});
			}	
		});
	}else{
		var message = '';
		message += '<div class="well" align="center">';
		message += 'Mahasiswa Tidak Aktif KKNP';
		message += '</div>';
		$('#selected-mhs-form').html(message);
	}
});

$(document).on('click', '#submit-logbook', function(e){
	var pembimbing 			= $('[name="pembimbing"]').val();
	var nama_kegiatan 	= $('[name="nama_kegiatan"]').val();
	var tgl_kegiatan 	= $('[name="tgl_kegiatan"]').val();
	var catatan 		= $('[name="catatan"]').val();
	
	if( pembimbing!=0 && nama_kegiatan.length>0 && tgl_kegiatan.length>0 && catatan.length>0 ){
		submit_logbook(e);
	}else{
		alert('Lengkapi data anda!');
	}
});

function submit_logbook(e){
	var postData = new FormData($('#form')[0]);
	$.ajax({
		url : base_url + "module/kknp/logbook/save",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			// alert(data);
			// location.reload();
			if(data=='Proses Simpan Berhasil!'){
				alert(data);
				location.reload();
			}
			else{
				alert(data);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Proses Simpan Gagal!');      
	    },
	    cache: false,
		contentType: false,
		processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
}
