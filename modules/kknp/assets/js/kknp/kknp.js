$(document).ready(function() {
	console.log(base_url + "module/kknp/info/read_index");
	if($('#index-form').length){
		var table  = $('#example').DataTable();
		$.ajax({
			type : "POST",
			dataType: "JSON",
			url: base_url + "module/kknp/info/read_index",
			data : $.param({
				param		 : 'index'
			}),
			success: function(data){
				console.log(data);
				// $('#results').text(JSON.stringify(data));
				
				$.each(data, function(i,data){
					var td1 = '';
					var td2 = '';
					
					td1 += '<td>';
					td1 += '	<i class="fa fa-clock-o"></i> '+change_date(data.YMD)+'<br>'+(data.waktu).substring(0,5)+'<br>';
					td1 += '	<span style="color:#c09853;">'+data.user_name+'</span>';
					td1 += '</td>';
				
					td2 += '<td>';
					td2 += '	<div class="col-md-5"><b>'+data.nama_perusahaan+'</b>';
					
					$.each(data.pembimbing, function(i,p){
						if(p.kategori=='kknp'&&p.pembimbing_ke=='1'){
							td2 += '	<code>'+p.nama+'</code>';
						}
					});
					
					switch(data.is_status){
						case 'pengajuan' :
							td2 += '		<div class="label label-warning" align="center">'+data.is_status+'</div>';
						break;
						case 'diterima' :
							td2 += '		<div class="label label-info" align="center">'+data.is_status+'</div>';
						break;
						case 'selesai' :
							td2 += '		<div class="label label-success" align="center">'+data.is_status+'</div>';
						break;
						case 'ditolak' :
							td2 += '		<div class="label label-danger" align="center">'+data.is_status+'</div>';
						break;
					}
					td2 += '		<br>';
					td2 += '		<small>Pelaksanaan </small>: <span style="color:#c09853;">'+data.tgl_mulai+'</span> <small>s/d</small> <span style="color:#c09853;">'+data.tgl_selesai+'</span></small><br>';
					
					td2 += '		<small>';
					$.each(data.file_surat, function(i,r){
						switch(r.jenis_surat){
							case 'tugas' :
								td2 += 'Surat Tugas : <small><a href="javascript::" onclick="view_(\'surat\',\''+r.file_surat+'\')"><i class="fa fa-file-o"></i> </small></a><br>';
							break;
							case 'pengantar' :
								td2 += 'Surat Pengantar : <small><a href="javascript::" onclick="view_(\'surat\',\''+r.file_surat+'\')"><i class="fa fa-file-o"></i> </small></a><br>';
							break;
							case 'balasan' :
								td2 += 'Surat Balasan : <small><a href="javascript::" onclick="view_(\'surat\',\''+r.file_surat+'\')"><i class="fa fa-file-o"></i> </small></a><br>';
							break;
							case 'batal' :
								td2 += 'Surat Batal : <small><a href="javascript::" onclick="view_(\'surat\',\''+r.file_surat+'\')"><i class="fa fa-file-o"></i> </small></a><br>';
							break;
						}
					});
					td2 += '		</small>';
					td2 += '	</div>';
					
					td2 += '	<div class="col-md-5">';
					
					if(mhs != ''){
						var mhs = (data.nama_mhs).split('@');
						for(var i = 0;i<mhs.length;i++){
							var mahasiswa = mhs[i].split('|');
							td2 += '<i class="fa fa-user">&nbsp;&nbsp;</i>'+mahasiswa[0];
							if(mahasiswa[2]==0){
								td2 += '<sup><small>*</small></sup>';
							}
							td2 += ' <span style="color:#c09853;"><small>'+mahasiswa[1]+'</small></span><br>';
						}
					}
					
					td2 += '	</div>';
					
					td2 += '	<div class="col-md-2">';
					td2 += '		<ul class="nav nav-pills pull-right">';
					td2 += '		  <li class="dropdown">';
					td2 += '			<a class="dropdown-toggle btn btn-table" id="drop4" role="button" data-toggle="dropdown" href="#">Action <b class="caret"></b></a>';
					td2 += '			<ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">';
					td2 += '			  <li><a class="btn-edit-post" href="'+base_url+'module/kknp/info/edit/'+data.pengajuan_id+'"><i class="fa fa-edit"></i> Edit</a></li>';
					td2 += '			  <li><a class="btn-edit-post" href="'+base_url+'module/kknp/nilai/add/'+data.pengajuan_id+'"><i class="fa fa-plus"></i> Nilai</a></li>';
					td2 += '			  <li><a class="btn-edit-post" href="'+base_url+'module/kknp/logbook/add/'+data.pengajuan_id+'"><i class="fa fa-plus"></i> Logbook</a></li>';
					td2 += '			</ul>';
					td2 += '		  </li>';
					td2 += '		</ul>';
					td2 += '	</div>';
					td2 += '</td>';
					
					table.row.add( [td1, td2] ).draw();
				});
			}	
		});
	}
	else if($('#edit-form').length){
		$(".e9").select2();
			
		$('#tgl_mulai, #tgl_selesai, #tgl_laporan, #tgl_tugas, #tgl_pengantar, #tgl_pengantar_selesai, #tgl_balasan, #tgl_balasan_selesai, #tgl_batal, #tgl_batal_selesai').datetimepicker({
		  pickTime : false,
		  format : 'YYYY-MM-DD'
		});
		
		$("#mhs_kknp, #mhs_batal").autocomplete({ 
			source: base_url + "/module/kknp/info/mhs",
			minLength: 3, 
			select: function(event, ui) { 
				$('#mhs_kknp').val(ui.item.value);
			} 
		});
		
		$("#pembimbing_dalam").autocomplete({ 
			source: base_url + "/module/kknp/info/dosen",
			minLength: 3, 
			select: function(event, ui) { 
				$('#pembimbing_dalam').val(ui.item.value);
				$('#pembimbing_dalam_id').val(ui.item.id);
			} 
		});
		
		$("#perusahaan_name").autocomplete({ 
			source: base_url + "/module/kknp/info/company",
			minLength: 3, 
			select: function(event, ui) { 
				$('#perusahaan_name').val(ui.item.value);
				$('#perusahaan_alamat').val(ui.item.alamat);
			} 
		});
		
		var pengajuan_id = $('input[name="hidId1"]').val();
		if(pengajuan_id.length > 0 ){
			// alert('edit');
			var mhs_kknp = Array();
			var mhs_kknp_batal = Array();
			var objek_kknp = Array();
			$.ajax({
				type : "POST",
				dataType: "JSON",
				url: base_url + "module/kknp/info/get_data_edit",
				async:   false,
				data : $.param({
					pengajuan_id : pengajuan_id,
					param : 'pengajuan_id_noMD5'
				}),
				success: function(data){
					// $('#results').text( JSON.stringify(data) );
					if( (data.objek) != null ){
						$.each(data.objek, function(i,o){
							objek_kknp.push(o.keterangan);
						});
					}
					if( (data.mhs_aktif) != null ){
						$.each(data.mhs_aktif, function(i,r){
							mhs_kknp.push(r.nama_mhs);
						});
					}
					// $('#results').text(JSON.stringify(data.mhs_batal));
					if( (data.mhs_batal) != null ){
						$.each(data.mhs_batal, function(i,r){
							mhs_kknp_batal.push(r.nama_mhs);
						});
					}

					$.each(data.pembimbing, function(i,r){
						if(r.kategori == 'kknp'){
							$('#pembimbing_dalam').val(r.nama);
							$('#pembimbing_dalam_id').val(r.karyawan_id);
							$('input[name="hidId11"]').val(r.pembimbing_id);
						}else{
							$('#pembimbing_luar').val(r.nama);
							$('input[name="hidId12"]').val(r.pembimbing_id);
						}
					});
					// var file = data.file;
					$.each(data.surat, function(i,r){
						switch(r.jenis_surat){
							case 'tugas' :
								$('#no_tugas').val(r.no_surat);
								$('#tgl_tugas').val(r.tgl_surat);
								$('[name="isi_surat_tugas"]').val(r.isi_surat);
								$('[name="hidId3"]').val(r.hidId);
								$.each(data.file, function(i,f){
									if(f.surat_id==r.hidId){
										$('[name="hidId7"]').val(f.file_id);
										$('[name="judul_file_surat_tugas"]').val(f.judul);
										$('[name="keterangan_file_surat_tugas"]').val(f.keterangan);
										$('#file_surat_tugas_view').html('<img width="150px" heigth="150px" src="'+file_url_view+f.file_loc+'"/>');
										$('[name="loc_surat_tugas"]').val(f.file_loc);
										if(f.is_publish==1) $('[name="publish_file_surat_tugas"]').prop("checked", true);
									}
								});
								// CKEDITOR.instances['isi_surat_tugas'].setData(r.isi_surat);
							break;
							case 'pengantar' :
								$('#no_pengantar').val(r.no_surat);
								$('#tgl_pengantar').val(r.tgl_surat);
								$('#tgl_pengantar_selesai').val(r.tgl_surat_selesai);
								$('[name="isi_surat_pengantar"]').val(r.isi_surat);
								$('[name="hidId4"]').val(r.hidId);
								$.each(data.file, function(i,f){
									if(f.surat_id==r.hidId){
										$('[name="hidId8"]').val(f.file_id);
										$('[name="judul_file_surat_pengantar"]').val(f.judul);
										$('[name="keterangan_file_surat_pengantar"]').val(f.keterangan);
										$('#file_surat_pengantar_view').html('<img width="150px" heigth="150px" src="'+file_url_view+f.file_loc+'"/>');
										$('[name="loc_surat_pengantar"]').val(f.file_loc);
										if(f.is_publish==1) $('[name="publish_file_surat_pengantar"]').prop("checked", true);
									}
								});
								// CKEDITOR.instances['isi_surat_pengantar'].setData(r.isi_surat);
							break;
							case 'balasan' :
								$('#no_balasan').val(r.no_surat);
								$('#tgl_balasan').val(r.tgl_surat);
								$('#tgl_balasan_selesai').val(r.tgl_surat_selesai);
								$('[name="isi_surat_balasan"]').val(r.isi_surat);
								$('[name="hidId5"]').val(r.hidId);
								$.each(data.file, function(i,f){
									if(f.surat_id==r.hidId){
										$('[name="hidId9"]').val(f.file_id);
										$('[name="judul_file_surat_balasan"]').val(f.judul);
										$('[name="keterangan_file_surat_balasan"]').val(f.keterangan);
										$('#file_surat_balasan_view').html('<img width="150px" heigth="150px" src="'+file_url_view+f.file_loc+'"/>');
										$('[name="loc_surat_balasan"]').val(f.file_loc);
										if(f.is_publish==1) $('[name="publish_file_surat_balasan"]').prop("checked", true);
									}
								});
								// CKEDITOR.instances['isi_surat_balasan'].setData(r.isi_surat);
							break;
							case 'batal' :
								$('#no_batal').val(r.no_surat);
								$('#tgl_batal').val(r.tgl_surat);
								$('#tgl_batal_selesai').val(r.tgl_surat_selesai);
								$('[name="isi_surat_batal"]').val(r.isi_surat);
								$('[name="hidId6"]').val(r.hidId);
								$.each(data.file, function(i,f){
									if(f.surat_id==r.hidId){
										$('[name="hidId10"]').val(f.file_id);
										$('[name="judul_file_surat_batal"]').val(f.judul);
										$('[name="keterangan_file_surat_batal"]').val(f.keterangan);
										$('#file_surat_batal_view').html('<img width="150px" heigth="150px" src="'+file_url_view+f.file_loc+'"/>');
										$('[name="loc_surat_batal"]').val(f.file_loc);
										if(f.is_publish==1) $('[name="publish_file_surat_batal"]').prop("checked", true);
									}
								});
								// CKEDITOR.instances['isi_surat_balasan'].setData(r.isi_surat);
							break;
						}
					});
				}	
			});
			
			if(mhs_kknp.length!=0){
				$('#existed-mhs-kknp').val(mhs_kknp);
				jQuery("#mhs_kknp").tagsManager({prefilled: mhs_kknp});
			}else{
				jQuery("#mhs_kknp").tagsManager();
			}
			if(mhs_kknp_batal.length!=0){
				$('#existed-mhs-batal').val(mhs_kknp_batal);
				jQuery("#mhs_batal").tagsManager({prefilled: mhs_kknp_batal});
			}else{
				jQuery("#mhs_batal").tagsManager();
			}
			if(objek_kknp.length!=0){
				$('#existed-objek-kknp').val(objek_kknp);
				jQuery("#objek").tagsManager({prefilled: objek_kknp});
			}else{
				jQuery("#objek").tagsManager();
			}
		}else{
			// alert('new');
			jQuery("#mhs_kknp").tagsManager();
			jQuery("#mhs_batal").tagsManager();
			jQuery("#objek").tagsManager();
		}
		
	}
	
});

function view_(param,id){
	var output = '';
	if(param=='surat'){
		var url_ = base_url + "module/kknp/info/get_surat";
	};
	$.ajax({
			type : "POST",
			dataType: "JSON",
			url: url_,
			data : $.param({
				id : id
			}),
			success: function(data){
				output += data[0].isi_surat;
				$('.modal-body-surat-view').html(output)
				$('#modal-surat-view').modal('show');
			}	
		});
}

$(document).on('click', '.tm-tag-remove', function(){
	changer_surat();
});

$('#mhs_kknp, #mhs_batal, #pembimbing_dalam').on('blur', function(){
	changer_surat();
});

$('#no_tugas, #tgl_mulai, #tgl_selesai, #perusahaan_name, #objek, #no_pengantar').on('blur paste', function(){
	changer_surat();
})

function changer_surat(){
	var w;
	
	var mhs = ($('[name="hidden-mhs_kknp"]').val()).split(',');
	var mhs_batal = ($('[name="hidden-mhs_batal"]').val()).split(',');
	var dosen = $('#pembimbing_dalam').val();
	
	var no_tugas  	= $('#no_tugas').val();
	var no_pengantar= $('#no_pengantar').val();
	
	var tgl_mulai   = $('#tgl_mulai').val();
	var tgl_selesai = $('#tgl_selesai').val();
	var perusahaan	= $('#perusahaan_name').val();
	var objek		= $('[name="hidden-objek"]').val();
	// alert(objek);
	var content_tugas 	  = $('[name="hidden-surat-tugas"]').val();
	var must_change_tugas = [ "{Waktu KKN Mulai}", "{Waktu KKN Selesai}", "{Tempat KKN}", "{Obyek}" ];
	var changer_tugas	  = [tgl_mulai, tgl_selesai, perusahaan, objek];
	
	var content_pengantar  	  = $('[name="hidden-surat-pengantar"]').val();
	var must_change_pengantar = [ "{Perusahaan}", "{Obyek}", "{Pelaksanaan}" ];
	var changer_pengantar	  = [perusahaan, objek, tgl_mulai+" s/d "+tgl_selesai];
	
	var content_batal 	  = $('[name="hidden-surat-batal"]').val();
	var must_change_batal = [ "{Waktu KKN Mulai}", "{Waktu KKN Selesai}", "{Tempat KKN}", "{Obyek}" ];
	var changer_batal	  = [tgl_mulai, tgl_selesai, perusahaan, objek];
	
	if(typeof(Worker) !== "undefined") {
		var web_worker_url = base_url + "modules/kknp/assets/js/kknp/changer.js";
    	if(typeof(w) == "undefined") w = new Worker(web_worker_url);
    	
    	w.postMessage({
    				   'base_url': base_url, 'dosen': dosen, 'mhs': mhs, 'mhs_batal' : mhs_batal, 
    				   'must_change_tugas': must_change_tugas, 'changer_tugas': changer_tugas, 'content_tugas': content_tugas,
    				   'must_change_pengantar': must_change_pengantar, 'changer_pengantar': changer_pengantar, 'content_pengantar': content_pengantar,
    				   'must_change_batal' : must_change_batal, 'changer_batal' : changer_batal, 'content_batal' : content_batal
    				  });
    	
    	w.addEventListener('message', function(e) {
			var data = e.data;
			// alert(data.content_batal);
			// $('#results').html(data.content_pengantar);
			$('[name="isi_surat_tugas"]').val(data.content_tugas);
			CKEDITOR.instances['isi_surat_tugas'].setData(data.content_tugas);
			
			$('[name="isi_surat_pengantar"]').val(data.content_pengantar);
			CKEDITOR.instances['isi_surat_pengantar'].setData(data.content_pengantar);
			
			$('[name="isi_surat_batal"]').val(data.content_batal);
			CKEDITOR.instances['isi_surat_batal'].setData(data.content_batal);
		}, false);
		
    } else {
    	
    	var nama_mhs = Array();
		var nim_mhs = Array();
		
		for(var i=0; i< mhs.length; i++){
			var data_m = mhs[i].split('-');
			nama_mhs.push( data_m[1].substring(1) );
			nim_mhs.push( data_m[0].substring(0, (data_m[0].length-1) ) );
		}
    	
    	//------TUGAS----------------------------    	
    	var c_tugas = '';
		for(var i=0; i< nama_mhs.length; i++){
			var content_tugas 	  = $('[name="hidden-surat-tugas"]').val();
			$.ajax({
				type : "POST",
				dataType: "HTML",
				async:   false,
				url: base_url + "module/kknp/info/prodi_mhs",
				data : $.param({
					nama : nama_mhs[i],
					nim : nim_mhs[i]
				}),
				success: function(data){
					content_tugas = content_tugas.replace("{Nama Mahasiswa}", nama_mhs[i]);
					content_tugas = content_tugas.replace("{NIM Mahasiswa}", nim_mhs[i]);
					content_tugas = content_tugas.replace("{Jurusan Mahasiswa}", data);
					for(var x=0; x < must_change_tugas.length; x++){
						content_tugas = content_tugas.replace(must_change_tugas[x], changer_tugas[x]);
					}
				}	
			});
			
			$.ajax({
				type : "POST",
				dataType: "JSON",
				async:   false,
				url: base_url + "module/kknp/info/data_dosen",
				data : $.param({
					nama : dosen
				}),
				success: function(data){
					// alert(data[0].nama);
					content_tugas = content_tugas.replace("{Nama Tenaga Pengajar}", data[0].nama);
					content_tugas = content_tugas.replace("{NIP Tenaga Pengajar}", data[0].nik);
					content_tugas = content_tugas.replace("{Pangkat Tenaga Pengajar}", data[0].golongan);
					content_tugas = content_tugas.replace("{Pekerjaan Tenaga Pengajar}", data[0].jabatan);
				}	
			});
			c_tugas = c_tugas+content_tugas; 
		}
		
		$('[name="isi_surat_tugas"]').val(c_tugas);
		CKEDITOR.instances['isi_surat_tugas'].setData(c_tugas);
		
		//------PENGANTAR------------------------
		var table_pengantar = '';
		table_pengantar += '<table border="1" cellpadding="0" cellspacing="0" style="width:500px;font-size:11px;font-family:arial,helvetica,sans-serif"><tbody>';
	
		table_pengantar += '<tr>';
		table_pengantar += '<td><strong>NIM</strong></td>';
		table_pengantar += '<td><strong>NAMA</strong></td>';
		table_pengantar += '</tr>';
		
		for(var i=0; i< nama_mhs.length; i++){
			table_pengantar += '<tr>';
			table_pengantar += '<td>'+nim_mhs[i]+'</td>';
			table_pengantar += '<td>'+nama_mhs[i]+'</td>';
			table_pengantar += '</tr>';
		}
		table_pengantar += '</tbody></table>';

		content_pengantar = content_pengantar.replace("{Mhs}", table_pengantar);
		
		for(var i=0; i < must_change_pengantar.length; i++){
			content_pengantar = content_pengantar.replace(must_change_pengantar[i], changer_pengantar[i]);
		}
		
		$('[name="isi_surat_pengantar"]').val(content_pengantar);
		CKEDITOR.instances['isi_surat_pengantar'].setData(content_pengantar);
		
		//------BATAL------------------------
		var nama_mhs_batal = Array();
		var nim_mhs_batal = Array();
		
		for(var i=0; i< mhs_batal.length; i++){
			var data_m = mhs_batal[i].split('-');
			nama_mhs_batal.push( data_m[1].substring(1) );
			nim_mhs_batal.push( data_m[0].substring(0, (data_m[0].length-1) ) );
		}
		// alert(nama_mhs_batal);alert(nim_mhs_batal);
		var c_batal = ''; 
		for(var i=0; i< nama_mhs_batal.length; i++){
			var content_batal 	  = $('[name="hidden-surat-batal"]').val();
			$.ajax({
				type : "POST",
				dataType: "HTML",
				async:   false,
				url: base_url + "module/kknp/info/prodi_mhs",
				data : $.param({
					nama : nama_mhs_batal[i],
					nim : nim_mhs_batal[i]
				}),
				success: function(data){
					content_batal = content_batal.replace("{Nama Mahasiswa}", nama_mhs_batal[i]);
					content_batal = content_batal.replace("{NIM Mahasiswa}", nim_mhs_batal[i]);
					content_batal = content_batal.replace("{Jurusan Mahasiswa}", data);
					for(var x=0; x < must_change_batal.length; x++){
						content_batal = content_batal.replace(must_change_batal[x], changer_batal[x]);
					}
				}	
			});
			
			$.ajax({
				type : "POST",
				dataType: "JSON",
				async:   false,
				url: base_url + "module/kknp/info/data_dosen",
				data : $.param({
					nama : dosen
				}),
				success: function(data){
					// alert(data[0].nama);
					content_batal = content_batal.replace("{Nama Tenaga Pengajar}", data[0].nama);
					content_batal = content_batal.replace("{NIP Tenaga Pengajar}", data[0].nik);
					content_batal = content_batal.replace("{Pangkat Tenaga Pengajar}", data[0].golongan);
					content_batal = content_batal.replace("{Pekerjaan Tenaga Pengajar}", data[0].jabatan);
				}	
			});
			c_batal = c_batal+content_batal; 
		}
		
		$('[name="isi_surat_batal"]').val(c_batal);
		CKEDITOR.instances['isi_surat_batal'].setData(c_batal);
    }
}


function change_date(date){
	var date = date.split('-');
	var monthName = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
    var month_arr = Number(date[1])-1;
    var output = (String(monthName[month_arr])).substring(0,3)+' '+date[2]+', '+date[0];
	return output;
}

$('#submit-submission').click(function(e) {
	var p_name 		  = $('#perusahaan_name').val();
	var p_address	  = $('#perusahaan_alamat').val();
	var tgl_mulai	  = $('#tgl_mulai').val();
	var tgl_selesai	  = $('#tgl_selesai').val();
	var mhs	  		  = $('[name="mhs_kknp"]').val();
	var thn_akademik  = $('[name="thn_akademik"]').val();
	
	// changer_surat();
	
	if(p_name.length!=0 && p_address.length!=0 && tgl_mulai.length!=0 && tgl_selesai.length!=0 && mhs.length!=0 && thn_akademik.length!=0){
		submit_submission(e);
	}else{
		alert('Lengkapi data anda!');
	}
});

function submit_submission(e){
	$.each($('[name="jenis_surat[]"]'), function(i,data){
		var content = CKEDITOR.instances['isi_surat_'+($(data).val())].getData();
		$('[name="isi_surat_'+($(data).val())+'"]').val(content);
	})

	var postData = new FormData($('#form-daftar-ulang')[0]);
	$.ajax({
		url : base_url + "module/kknp/info/save_kknp",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			// alert(data);
			// location.reload();
			if(data=='Proses Simpan Berhasil!'){
				alert(data);
				location.reload();
			}
			else if(data=='wrong type'){
				alert('Jenis file yang di upload salah!');
			}
			else{
				alert(data);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Proses Simpan Gagal!');      
	    },
	    cache: false,
		contentType: false,
		processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
};