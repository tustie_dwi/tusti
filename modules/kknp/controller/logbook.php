<?php
class kknp_logbook extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mkknp 	= new model_kknpModules();
		$mlog	= new model_logbook();
		
		$data['posts']	= '';
		$data['header'] = 'Logbook KKNP';
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.new.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		// $this->coms->add_script('select/select2.js');
		// $this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/kknp/logbook.js');
		
		$this->view('logbook/index.php', $data );
		
	}
	
	function get_log_by_mahasiswa(){
		$mlog	= new model_logbook();
		$mnilai = new model_nilai();
		
		$mhs 		= $_POST['mhs'];
		$data_m 	= explode('-', $mhs);
		$nama_mhs 	= substr($data_m[1], 1);
		$nim_mhs 	= substr($data_m[0], 0, -1);
		
		$data_mhs_kknp	= $mnilai->get_kknp_mhs($nama_mhs,$nim_mhs,'1');
		if(isset($data_mhs_kknp)){
			$mhs_kknp_id	  = $data_mhs_kknp[0]->hid_id;
			$mhs_all		  = $data_mhs_kknp[0]->nama_mhs_all;
			$pengajuan_id	  = $data_mhs_kknp[0]->pengajuan_id;
			$pengajuan_id_md5 = $data_mhs_kknp[0]->pengajuan_id_md5;
			$param			  = 'pengajuan_id_noMD5';
			
			$data['pengajuan_id_md5'] = $pengajuan_id_md5;
			$data['mhs_kknp_id'] 	= $mhs_kknp_id;
			$data['mhs']			= $mhs_all;
			$data['log']			= $mlog->get_kknp_log_mhs($mhs_kknp_id);
			// echo $mhs_all;
			$this->view('logbook/show_view.php', $data );
		}
		
	}
	
	function add($pengajuanid=NULL){
		$mkknp 	= new model_kknpModules();
		$mlog	= new model_logbook();
		
		$data['pengajuan'] = $mkknp->read($pengajuanid);
		
		$data['posts']	= '';
		$data['header']	= 'Logbook KKNP';
		
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');		
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.css');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		
		$this->add_script('js/kknp/logbook.js');
		
		$this->view('logbook/write.php', $data );
		
	}
	
	function show_selected(){
		$mlog	= new model_logbook();
		$mkknp 	= new model_kknpModules();
		
		$mhs_kknp_id 	= $_POST['mhs_kknp_id'];
		$is_aktif 		= $_POST['is_aktif'];
		$pengajuan_id 	= $_POST['pengajuan_id'];
		
		$data['pembimbing'] 	= $mkknp->read_pembimbing($pengajuan_id,'pengajuan_id_noMD5');
		$data['mhs_kknp_id']	= $mhs_kknp_id;
		
		$data['nama_mhs']		= $_POST['nama'];
		$data['prodi_mhs']		= $_POST['prodi'];
		$data['log']			= $mlog->get_kknp_log_mhs($mhs_kknp_id);
		
		$this->view('logbook/show.php', $data );
		
	}
	
	function save(){
		$mlog	= new model_logbook();
		
		$userid		= $this->coms->authenticatedUser->id;
		$last_update= date("Y-m-d H:i:s"); 
		
		if(isset($_POST['hidId']) && $_POST['hidId']!=''){
			$log_id 	= $_POST['hidId'];
			$action		= 'edit';
		}else{
			$log_id 	= $mlog->get_log_id();
			$action		= 'new';
		}
		
		$kknp_mhs_id 		= $_POST['hiDMhs'];
		$pembimbing_kknp_id = $_POST['pembimbing'];
		$nama_kegiatan 		= $_POST['nama_kegiatan'];
		$tgl_kegiatan 		= $_POST['tgl_kegiatan'];
		$catatan 			= $_POST['catatan'];
		
		$data_log		 = array(
							'log_id'=>$log_id,
							'kknp_mhs_id'=>$kknp_mhs_id,
							'tgl'=>$tgl_kegiatan,
							'nama_kegiatan'=>$nama_kegiatan,
							'catatan'=>$catatan,
							'pembimbing_id'=>$pembimbing_kknp_id,
							'user_id'=>$userid,
							'last_update'=>$last_update
						   );
		if($mlog->replace_log($data_log)){
			echo "Proses Simpan Berhasil!";
		}else{
			echo "Proses Simpan Gagal!";
		}		
		
	}
	
}
?>