<?php
class kknp_info extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mkknp = new model_kknpModules();
		
		$data['posts']	= '';
		$data['header']	= 'KKNP';
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.new.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		// $this->coms->add_script('select/select2.js');
		// $this->coms->add_style('select/select2.css');
		$this->add_script('js/kknp/kknp.js');
		
		$this->view('kknp/index.php', $data );
		
	}
	
	function pengajuan(){
		
		$data['header']	= 'Pengajuan KKNP';
		$mkknp = new model_kknpModules();
		
		$data['thnakademik'] = $mkknp->get_thn_akademik();
		$data['fakultas']	 = $mkknp->get_from_unit_kerja('fakultas');
		$data['cabang']		 = $mkknp->get_cabang();
		
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')){
			$data['fakultasid'] = $this->coms->authenticatedUser->fakultas;
		}
		
		$data['surat_tugas_kknp'] = $mkknp->get_surat('Surat tugas','kknp');
		$data['surat_pengantar_kknp'] = $mkknp->get_surat('Surat pengantar','kknp');
		$data['surat_batal_kknp'] = $mkknp->get_surat('Surat batal','kknp');
		
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');		
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.css');
		$this->coms->add_script('js/jquery/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');

		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		
		$this->add_script('js/kknp/kknp.js');
		
		$this->view('kknp/edit.php', $data );
	}
	
	function edit($id){
		$mkknp		= new model_kknpModules();
		$data['header']	= 'Ubah Pengajuan KKNP';
		
		$data['thnakademik'] = $mkknp->get_thn_akademik();
		$data['fakultas']	 = $mkknp->get_from_unit_kerja('fakultas');
		$data['cabang']		 = $mkknp->get_cabang();
		
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')){
			$data['fakultasid'] = $this->coms->authenticatedUser->fakultas;
		}
		
		$data['posts'] = $mkknp->read($id);
		
		$data['surat_tugas_kknp'] = $mkknp->get_surat('Surat tugas','kknp');
		$data['surat_pengantar_kknp'] = $mkknp->get_surat('Surat pengantar','kknp');
		$data['surat_batal_kknp'] = $mkknp->get_surat('Surat batal','kknp');
		
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');		
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.css');
		$this->coms->add_script('js/jquery/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');

		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		
		$this->add_script('js/kknp/kknp.js');
		
		$this->view('kknp/edit.php', $data );
		
	}
	
	function save_kknp(){
		$mkknp		= new model_kknpModules();
		$userid		= $this->coms->authenticatedUser->id;
		$last_update= date("Y-m-d H:i:s"); 
		
		/*
		 * hidId1 = pengajuan_id;
		 * hidId2 = objek_id; //not used
		 * hidId3 = kknp_surat_id (tugas);
		 * hidId4 = kknp_surat_id (pengantar);
		 * hidId5 = kknp_surat_id (balasan);
		 * hidId6 = kknp_surat_id (batal);
		 * 
		 * hidId7 = file_id (tugas);
		 * hidId8 = file_id (pengantar);
		 * hidId9 = file_id (balasan);
		 * hidId10 = file_id (batal);
		 * 
		 * hidId11 = id_pembimbing_dalam;
		 * hidId12 = id_pembimbing_luar;
		 * */
		
		if(isset($_POST['hidId1']) && $_POST['hidId1']!=''){
			$pengajuan_id 	= $_POST['hidId1'];
			$tgl_pengajuan 	= $_POST['tgl_pengajuan'];
			$action			= 'edit';
		}else{
			$pengajuan_id = $mkknp->get_pengajuan_id();
			$tgl_pengajuan 	= $last_update;
			$action			= 'new';
		}
		
		$get_companyid 	= $this->company_save('from-save-kknp');
		
		$tgl_mulai	 	= $_POST['tgl_mulai'];
		$tgl_selesai 	= $_POST['tgl_selesai'];
		$mhsall 		= $_POST['hidden-mhs_kknp'];
		
		$status 		= $_POST['status'];
		$fakultas 		= $_POST['fakultas'];
		$cabang 		= $_POST['cabang'];
		$thnakademik	= $_POST['thn_akademik'];
		
		if(isset($_POST['tgl_laporan'])&&$_POST['tgl_laporan']!=''){
			$tgl_laporan = $_POST['tgl_laporan'];
		}else $tgl_laporan = NULL;
		
		$catatan = $_POST['catatan'];
		
		$data_kknp		 = array(
							'pengajuan_id'=>$pengajuan_id,
							'perusahaan_id'=>$get_companyid,
							'fakultas_id'=>$fakultas,
							'cabang_id'=>$cabang,
							'tahun_akademik'=>$thnakademik,
							'tgl_pengajuan'=>$tgl_pengajuan,
							'tgl_mulai'=>$tgl_mulai,
							'tgl_selesai'=>$tgl_selesai,
							'tgl_laporan'=>$tgl_laporan,
							'keterangan'=>$catatan,
							'user_id'=>$userid,
							'last_update'=>$last_update,
							'is_status'=>$status
						   );
		if($mkknp->replace_kknp($data_kknp)){
			
			$this->pembimbing_save($pengajuan_id);
			$this->mhs_save($mhsall, $pengajuan_id, '1', $action);
			$this->save_objek($pengajuan_id, $action);
			
			foreach ($_POST['jenis_surat'] as $jenis_surat) {
				if(isset($_POST['no_'.$jenis_surat]) && $_POST['no_'.$jenis_surat]!=''){
					$this->save_surat($pengajuan_id, $jenis_surat, $fakultas, $action);
				}
			}
			
			echo "Proses Simpan Berhasil!";
		}else{
			echo "Proses Simpan Gagal!";
		}
		
	}
	
	function save_surat($pengajuan_id, $jenis_surat, $fakultas, $action){
		$mkknp 		= new model_kknpModules();
		$userid		= $this->coms->authenticatedUser->id;
		$last_update= date("Y-m-d H:i:s"); 
		
		$no_surat 	= $_POST['no_'.$jenis_surat];
		$tgl_surat	= $_POST['tgl_'.$jenis_surat];
		$isi_surat	= $_POST['isi_surat_'.$jenis_surat];
		
		if($_POST['template_'.$jenis_surat]!=''){
			$template_id = $_POST['template_'.$jenis_surat];
		}else $template_id = NULL;
		
		$data_surat	 = array(
							'jenis_surat'=>$jenis_surat,
							'pengajuan_id'=>$pengajuan_id,
							'template_id'=>$template_id,
							'no_surat'=>$no_surat,
							'tgl_surat'=>$tgl_surat,
							'isi_surat'=>$isi_surat,
							// 'file_surat'=>$catatan,
							'user_id'=>$userid,
							'last_update'=>$last_update
						   );
		
		/*
		 * hidId3 = kknp_surat_id (tugas);
		 * hidId4 = kknp_surat_id (pengantar);
		 * hidId5 = kknp_surat_id (balasan);
		 * hidId6 = kknp_surat_id (batal);
		 * 
		 * hidId7 = file_id (tugas);
		 * hidId8 = file_id (pengantar);
		 * hidId9 = file_id (balasan);
		 * hidId10 = file_id (batal);
		 * 
		 * hidId11 = id_pembimbing_dalam;
		 * hidId12 = id_pembimbing_luar;
		 * */
		
		switch ($jenis_surat) {
			case 'tugas':
				if(isset($_POST['hidId3']) && $_POST['hidId3']!=''){
					$kknp_surat_id = $_POST['hidId3'];
				}else{
					$kknp_surat_id = $mkknp->get_kknp_surat_id();
				}
				
				if(isset($_POST['hidId7']) && $_POST['hidId7']!=''){
					$file_id = $_POST['hidId7'];
				}else{
					$file_id = $mkknp->get_file_id();
				}
				
				$data_tambahan = array('kknp_surat_id'=>$kknp_surat_id);
				$data_surat = $data_surat + $data_tambahan;
			break;
			case 'pengantar':
				$tgl_surat_selesai	= $_POST['tgl_'.$jenis_surat.'_selesai'];
				
				if(isset($_POST['hidId4']) && $_POST['hidId4']!=''){
					$kknp_surat_id = $_POST['hidId4'];
				}else{
					$kknp_surat_id = $mkknp->get_kknp_surat_id();
				}
				
				if(isset($_POST['hidId8']) && $_POST['hidId8']!=''){
					$file_id = $_POST['hidId8'];
				}else{
					$file_id = $mkknp->get_file_id();
				}
				
				$data_tambahan = array(
									   'kknp_surat_id'=>$kknp_surat_id,
									   'tgl_surat_selesai'=>$tgl_surat_selesai
									  );
				$data_surat = $data_surat + $data_tambahan;
			break;
			case 'balasan':
				$tgl_surat_selesai	= $_POST['tgl_'.$jenis_surat.'_selesai'];
				
				if(isset($_POST['hidId5']) && $_POST['hidId5']!=''){
					$kknp_surat_id = $_POST['hidId5'];
				}else{
					$kknp_surat_id = $mkknp->get_kknp_surat_id();
				}
				
				if(isset($_POST['hidId9']) && $_POST['hidId9']!=''){
					$file_id = $_POST['hidId9'];
				}else{
					$file_id = $mkknp->get_file_id();
				}
				
				$data_tambahan = array(
									   'kknp_surat_id'=>$kknp_surat_id,
									   'tgl_surat_selesai'=>$tgl_surat_selesai
									  );
				$data_surat = $data_surat + $data_tambahan;
			break;
			case 'batal':
				$tgl_surat_selesai	= $_POST['tgl_'.$jenis_surat.'_selesai'];
				
				// if(isset($_POST['hidden-mhs_batal'])&&$_POST['hidden-mhs_batal']!=''){
					$mhsall 			= $_POST['hidden-mhs_batal'];
					$this->mhs_save($mhsall, $pengajuan_id, '0', $action);
				// }

				if(isset($_POST['hidId6']) && $_POST['hidId6']!=''){
					$kknp_surat_id = $_POST['hidId6'];
				}else{
					$kknp_surat_id = $mkknp->get_kknp_surat_id();
				}
				
				if(isset($_POST['hidId10']) && $_POST['hidId10']!=''){
					$file_id = $_POST['hidId10'];
				}else{
					$file_id = $mkknp->get_file_id();
				}
				
				$data_tambahan = array(
									   'kknp_surat_id'=>$kknp_surat_id,
									   'tgl_surat_selesai'=>$tgl_surat_selesai
									  );
				$data_surat = $data_surat + $data_tambahan;
			break;
		}
		
		$file_surat = $this->save_file_surat($file_id,$pengajuan_id,$kknp_surat_id,$jenis_surat,$fakultas);
		$data_file_id = array('file_surat'=>$file_surat);
		$data_surat = $data_surat + $data_file_id;
		// var_dump($data_surat);
		// echo "\n\n";
		
		$mkknp->replace_surat($data_surat);
		
	}
	
	function save_file_surat($file_id,$pengajuan_id,$kknp_surat_id,$jenis_surat,$fakultas_id){
		$mkknp 		= new model_kknpModules();
		$userid		= $this->coms->authenticatedUser->id;
		$last_update= date("Y-m-d H:i:s"); 
		$month 		= date('m');
		$year 		= date('Y');
		
		if(isset($_POST['publish_file_surat_'.$jenis_surat])!=""){
			$is_publish	= $_POST['publish_file_surat_'.$jenis_surat];
		}else{
			$is_publish	= 0;
		}
		
		$judul_file = $_POST['judul_file_surat_'.$jenis_surat];
		$keterangan = $_POST['keterangan_file_surat_'.$jenis_surat];
		
		$data_file = array(
						   'file_id'=>$file_id,
						   'pengajuan_id'=>$pengajuan_id,
						   'surat_id'=>$kknp_surat_id,
						   'judul'=>$judul_file,
						   'keterangan'=>$keterangan,
						   'is_publish'=>$is_publish,
						   'user_id'=>$userid,
						   'last_update'=>$last_update
						  );
						  
		$cekicon = 'no error';
		if(isset($_FILES['file_surat_'.$jenis_surat])){
			foreach ($_FILES['file_surat_'.$jenis_surat] as $id => $icon) {
				if($id == 'error'){
					if($icon!=0){
						$cekicon = 'error';
					}
				}
			}
			
			
			//------------------UPLOAD FILE START---------------------------------------------
			if($cekicon!='error'){
				foreach ($_FILES['file_surat_'.$jenis_surat] as $id => $icon) {
					$brokeext			= $this->get_extension($icon);
					if($id == 'name'){
						$ext 	= $brokeext;
						$file	= $icon;
					}
					if($id == 'type'){
						$file_type = $_FILES['file_surat_'.$jenis_surat]['type'];
					}
					if($id == 'size'){
						$file_size = $_FILES['file_surat_'.$jenis_surat]['size'];
					}
					if($id == 'tmp_name'){
						$file_tmp_name = $_FILES['file_surat_'.$jenis_surat]['tmp_name'];
					}
				}
				
				$upload_dir = 'assets/upload/file/'.$fakultas_id.'/kknp/'.$year. '-' .$month.'/';		
				$upload_dir_db = 'upload/file/'.$fakultas_id.'/kknp/'.$year. '-' .$month.'/';	
				
				// if (!file_exists($upload_dir)) {
					// mkdir($upload_dir, 0777, true);
					// chmod($upload_dir, 0777);
				// }
				$file_loc = $upload_dir_db . $file;
				
				$allowed_ext = array('jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'pdf', 'xls', 'xlsx');
				if(!in_array($ext,$allowed_ext)){
					echo "wrong type";
					exit;
				}
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					// rename($file_tmp_name, $upload_dir . $file);
					
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $file_tmp_name;
					$filename    = $file;
					$filenamenew = $file;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		            $ch = curl_init();
		            $options = array(
		                CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
		            ); // cURL options 
					curl_setopt_array($ch, $options);
			        curl_exec($ch);
			        if(!curl_errno($ch))
			        {
			            $info = curl_getinfo($ch);
			            if ($info['http_code'] == 200):
			               $errmsg = "File uploaded successfully";
			               
						   $data_tambahan = array(
						   					      'file_loc'=>$file_loc,
											      'file_name'=>$file,
											      'file_size'=>$file_size,
											      'file_type'=>$file_type
						   						 );
							
						   $data_file = $data_file + $data_tambahan;
						   $mkknp->replace_file($data_file);
						   return $file_id;
						endif;
			        }
			        else
			        {
			            $errmsg = curl_error($ch);
			        }
			        curl_close($ch);
					//------UPLOAD USING CURL----------------
					// unlink($upload_dir . $file);
				}
			}
			else {
				$data_tambahan = array('file_loc'=>$_POST['loc_surat_'.$jenis_surat]);
			   	$data_file = $data_file + $data_tambahan;
				$mkknp->replace_file($data_file);
			 	return $file_id;
			}			
			//------------------UPLOAD FILE END-----------------------------------------------------
		}else{
			$data_tambahan = array('file_loc'=>$_POST['loc_surat_'.$jenis_surat]);
			$data_file = $data_file + $data_tambahan;
			$mkknp->replace_file($data_file);
			return $file_id;
		}
		
	}
	
	function save_objek($pengajuan_id, $action){
		$mkknp 		= new model_kknpModules();
		
		if(isset($_POST['hidden-objek']) && $_POST['hidden-objek']!=''):
			$objek		= explode(',', $_POST['hidden-objek']);
			
			foreach ($objek as $objek) {
				$obj = $mkknp->read_objek($pengajuan_id,'pengajuan_id_noMD5',$objek);
				if(isset($obj[0]->objek_id)){
					$objek_id	= $obj[0]->objek_id;
				}else{
					$objek_id	= $mkknp->get_objek_id();
				}
				
				$data_objek	 = array(
									'objek_id'=>$objek_id,
									'pengajuan_id'=>$pengajuan_id,
									'keterangan'=>$objek
								   );
				$mkknp->replace_kknp_objek($data_objek);
				
			}
		endif;
		
		if($action=='edit'){
			$data_ex 	= $_POST['existed-objek-kknp'];
			$objek 		= $_POST['hidden-objek'];
			$this->regex_objek($data_ex, $pengajuan_id, $objek);
		}
		
	}
	
	function regex_objek($data_ex, $pengajuan_id, $objek){
		$mkknp 			= new model_kknpModules();
		
		if(isset($objek) && $objek!=''):
			if($data_ex!=''):
				$existed_obj = explode(',', $data_ex);
				$data_ex	 = explode(',', $objek);
				$not_ex 	 = array_diff($existed_obj, $data_ex);
				// print_r($not_ex);
				if(count($not_ex)>0):
					foreach ($not_ex as $objek) {
						$obj = $mkknp->read_objek($pengajuan_id,'pengajuan_id_noMD5',$objek);
						if(isset($obj[0]->objek_id)) $objek_id	= $obj[0]->objek_id;
						
						$mkknp->delete_objek($objek_id,$pengajuan_id,'pengajuan_id_noMD5');
						
					}
				endif;
			endif;
		else:
			$mkknp->delete_objek(NULL,$pengajuan_id,'pengajuan_id_noMD5');
		endif;
	}
	
	function pembimbing_save($pengajuan_id){ 
		$mkknp 			= new model_kknpModules();
		
		/*
		 * hidId11 = id_pembimbing_dalam;
		 * hidId12 = id_pembimbing_luar;
		 */
		 $param = '';
		 for ($i=1; $i <= 2; $i++) {
		 	
			if($i==1){
				$param = 'dalam';
				if(isset($_POST['hidId11']) && $_POST['hidId11']!=''){
					$pembimbingid = $_POST['hidId11'];
				}else{
					$pembimbingid = $mkknp->get_pembimbing_id();
				}
				$kid_pembimbing  = $_POST['pembimbing_dalam_id'];
				$kategori = 'kknp';
				$pembimbingke = 1;
			}else{
				$param = 'luar';
				if(isset($_POST['hidId12']) && $_POST['hidId12']!=''){
					$pembimbingid = $_POST['hidId12'];
				}else{
					$pembimbingid = $mkknp->get_pembimbing_id();
				}
				$kid_pembimbing = NULL;
				$kategori = 'luar';
				$pembimbingke = 2;
			}
			
			$nama_pembimbing = $_POST['pembimbing_'.$param];
			
			$data_pembimbing = array(
									 'pembimbing_id'=>$pembimbingid,
									 'karyawan_id'=>$kid_pembimbing,
									 'pengajuan_id'=>$pengajuan_id,
									 'nama'=>$nama_pembimbing,
									 'kategori'=>$kategori,
									 'pembimbing_ke'=>$pembimbingke
						   			 );
			$mkknp->replace_kknp_pembimbing($data_pembimbing);
			
		 }
		 
	}
	
	function mhs_save($mhsall, $pengajuan_id, $status, $action){
		$mkknp 			= new model_kknpModules();
		$mhs = '';
		if(isset($mhsall)&&$mhsall!=''):
			$mhs = explode(',', $mhsall);
			foreach ($mhs as $m) {
				$data_m 	= explode('-', $m);
				$nama_mhs 	= substr($data_m[1], 1);
				$nim_mhs 	= substr($data_m[0], 0, -1);
				
				$get_mhs_id 	= $mkknp->get_mhs($nama_mhs, $nim_mhs);
				
				$kknp_mhs_id 	= $mkknp->get_kknp_mhs($pengajuan_id, $get_mhs_id[0]->hid_id, 'pengajuan_id_noMD5', NULL);
				if(isset($kknp_mhs_id[0]->hid_id)){
					$kknp_mhs_id	= $kknp_mhs_id[0]->hid_id;
				}else{
					$kknp_mhs_id	= $mkknp->get_mhs_kknp_id();
				}
				
					
				$data_mhs	 = array(
								'kknp_mhs_id'=>$kknp_mhs_id,
								'pengajuan_id'=>$pengajuan_id,
								'mahasiswa_id'=>$get_mhs_id[0]->hid_id,
								'is_aktif'=>$status
							   );
				$mkknp->replace_kknp_mhs($data_mhs);
				// var_dump($data_mhs);
			}
		endif;
		
		if($action=='edit'){
			if($status==1) $data_ex = $_POST['existed-mhs-kknp'];
			else $data_ex = $_POST['existed-mhs-batal'];
			$this->regex_mhs($mhsall, $pengajuan_id, $status, $data_ex, $mhs);
		}
	}
	
	function regex_mhs($mhsall, $pengajuan_id, $status, $data_ex, $mhs){
		$mkknp 			= new model_kknpModules();
		if(isset($mhsall)&&$mhsall!=''):
			if($data_ex!=''):
				$existed_mhs = explode(',', $data_ex);
				$not_ex 	 = array_diff($existed_mhs, $mhs);
				// print_r($existed_mhs);
				if(count($not_ex)>0):
					foreach ($not_ex as $m) {
						$data_m 	= explode('-', $m);
						$nama_mhs 	= substr($data_m[1], 1);
						$nim_mhs 	= substr($data_m[0], 0, -1);
						
						$get_mhs_id 	= $mkknp->get_mhs($nama_mhs, $nim_mhs);
						
						$kknp_mhs_id 	= $mkknp->get_kknp_mhs($pengajuan_id, $get_mhs_id[0]->hid_id, 'pengajuan_id_noMD5', NULL);
						$kknp_mhs_id    = $kknp_mhs_id[0]->hid_id;
						if($status!='1') $status = 'nol';
						$mkknp->delete_mhs($kknp_mhs_id,$pengajuan_id,$status,'pengajuan_id_noMD5');
					}
				endif;
			endif;
		else:
			if($status!='1') $status = 'nol';
			$mkknp->delete_mhs(NULL,$pengajuan_id,$status,'pengajuan_id_noMD5');
		endif;
		
	}
	
	function company_save($str=NULL){
		$mkknp 		= new model_kknpModules();
		
		$company_name = $_POST['perusahaan_name'];
		$company_address = $_POST['perusahaan_alamat'];
		
		if(isset($_POST['telp']) && $_POST['telp']!=''){
			$company_telp = $_POST['telp'];
		}else $company_telp = NULL;
		
		$perusahaan_id = $mkknp->get_company($company_name,$company_address);
		if(isset($perusahaan_id[0]->hid_id)){
			if($str=='from-save-kknp'){
				return $perusahaan_id[0]->hid_id;
			}			
		}else{
			$perusahaan_id = $mkknp->get_company_id();	
			$data_company	 = array(
							'perusahaan_id'=>$perusahaan_id,
							'nama'=>$company_name,
							'alamat'=>$company_address,
							'telp'=>$company_telp
						   );
			$mkknp->replace_perusahaan($data_company);	
		}
		
		return $perusahaan_id;
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function read_index(){
		$mkknp  = new model_kknpModules();
		
		if(isset($_POST['param'])&&$_POST['param']!=''){
			$param = $_POST['param'];
		}else $param = NULL;
		
		$result	= $mkknp->read();
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value){
				$arr[$key] = $value;				 
			}
			
			$file_surat = $this->read_surat_file($row->pengajuan_id,$param);
			$arr['file_surat'] = $file_surat;
			
			$pembimbing = $this->read_pembimbing($row->pengajuan_id,'pengajuan_id_MD5');
			$arr['pembimbing'] = $pembimbing;
			
			array_push($return_arr,$arr);
		}
		$json_response = json_encode($return_arr);
		echo $json_response;
		
	}
	
	function read_pembimbing($pengajuanid=NULL,$param=NULL){
		$mkknp  	= new model_kknpModules();
		
		$result		= $mkknp->read_pembimbing($pengajuanid,$param);
		$return_arr = array();
		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		return $return_arr;

	}
	
	function read_surat_file($pengajuanid=NULL,$param=NULL){
		$mkknp  	= new model_kknpModules();
		
		$result		= $mkknp->read_surat($pengajuanid,$param,NULL,'pengajuan_id_MD5');
		$return_arr = array();
		
		if($result) {
			foreach($result as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
		}
		
		return $return_arr;
	}
	
	function get_surat(){
		$mkknp  = new model_kknpModules();
		
		if(isset($_POST['id'])&&$_POST['id']!=''){
			$id = $_POST['id'];
		}else $id = NULL;
		
		$result		= $mkknp->read_surat(NULL,NULL,$id,'pengajuan_id_MD5');
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value){
				$arr[$key] = $value;				 
			}		
			array_push($return_arr,$arr);
		}
		$json_response = json_encode($return_arr);
		echo $json_response;
	}
	
	function mhs(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mkknp 		= new model_kknpModules();
		$result 	= $mkknp->get_mhs('','',$str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		echo $json_response;
	}
	
	function dosen(){
		$mkknp 		= new model_kknpModules();
		
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$result = $mkknp->get_dosen($str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		echo $json_response;
	}
	
	function company(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mkknp 		= new model_kknpModules();
		$result 	= $mkknp->get_company('','',$str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		echo $json_response;
	}
	
	function prodi_mhs(){
		$mkknp 		= new model_kknpModules();
		
		if(isset($_POST['nama'])){
			$nama = $_POST['nama'];
		}else $nama = NULL;
		
		if(isset($_POST['nim'])){
			$nim = $_POST['nim'];
		}else $nim = NULL;
		
		$result = $mkknp->data_mhs($nama,$nim);
		echo $result[0]->prodi_mhs;
		// echo $result;
	}
	
	function data_dosen(){
		$mkknp 		= new model_kknpModules();
		
		if(isset($_POST['nama']) && $_POST['nama']!= ''){
			$nama = $_POST['nama'];
			
			$result = $mkknp->get_data_dosen($nama);
			$return_arr = array();
			foreach($result as $key => $value){
					$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
			$json_response = json_encode($return_arr);
			echo $json_response;
			
		}else return NULL;
	}
	
	function get_data_edit(){
		$mkknp 		= new model_kknpModules();
		if(isset($_POST['pengajuan_id']) && $_POST['pengajuan_id']!= ''){
			$pengajuan_id 	= $_POST['pengajuan_id'];
			$param 			= $_POST['param'];

			$data 	= array();
			$result = array();
			
			$name[0] = 'objek';
			$name[1] = 'pembimbing';
			$name[2] = 'mhs_aktif';
			$name[3] = 'mhs_batal';
			$name[4] = 'surat';
			$name[5] = 'file';
			
			$data[0] = $mkknp->read_objek($pengajuan_id,$param);
			$data[1] = $mkknp->read_pembimbing($pengajuan_id,$param);
			$data[2] = $mkknp->get_kknp_mhs($pengajuan_id,NULL,$param,'1');
			$data[3] = $mkknp->get_kknp_mhs($pengajuan_id,NULL,$param,'nol');
			$data[4] = $mkknp->read_surat($pengajuan_id,NULL,NULL,$param);
			$data[5] = $mkknp->read_file($pengajuan_id,$param);
			
			$return_arr = array();
			
			
			for($i=0; $i < count($data); $i++){
				$arr = array();
				$res = array();
				if(count($data[$i])>0):
					foreach($data[$i] as $row){
						foreach ($row as $key => $value) {
							$arr[$key] = $value;				 
						}
						$res[] = $arr;
					}
					$result[$name[$i]] = $res;
				else:
					$result[$name[$i]] = NULL;
				endif;
			}
			
			array_push($return_arr,$result);
			
			$json_response = json_encode($result);
			echo $json_response;
		}else return NULL;
		
	}
	
	
}?>