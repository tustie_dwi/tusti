<?php
class kknp_komponen extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($id=NULL){
		$mkknp = new model_kknpModules();
		
		$data['posts']	= $this->read_index(NULL,'parent');
		$data['header']	= 'Komponen Nilai KKNP';
		
		if(!$id){
			$data['sys'] = 'new';
		}else {
			$data['sys'] = 'edit';
			$data['edit_posts']	= $this->read_index($id);
		}
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.new.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');	

		$this->add_script('js/kknp/komponen.js');
		
		$this->view('komponen/index.php', $data );
		
	}
	
	function save(){
		$mkknp = new model_kknpModules();
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		
		if(isset($_POST['hidId']) && $_POST['hidId']!=''){
			$komponen_id = $_POST['hidId'];
			$action = 'edit';
		}else{
			$komponen_id = $mkknp->get_komponen_id();
			$action = 'new';
		}
		
		$keterangan = $_POST['keterangan'];
		$bobot 		= $_POST['bobot'];
		$urut 		= $_POST['urut'];
		
		if(isset($_POST['is_aktif'])!=""){
			$is_aktif	= $_POST['is_aktif'];
		}else{
			$is_aktif	= 0;
		}
		
		if($_POST['parent']!='0'){
			$parentid = $mkknp->read_komponen($_POST['parent']);
			$parentid = $parentid[0]->hidId;
		}else $parentid = 0;
		
		$datanya 	= Array(
							'komponen_id'=>$komponen_id,
							'keterangan'=>$keterangan,
							'parent_id'=>$parentid,
							'is_aktif'=>$is_aktif,
							'urut'=>$urut,
							'bobot'=>$bobot,
							'user'=>$userid,
							'last_update'=>$lastupdate
							);
		if($mkknp->replace_kknp_komponen_nilai($datanya)){
			echo "sukses";
		}else echo "gagal";
		
	}
	
	function read_index($id=NULL,$parent=NULL){
		$mkknp = new model_kknpModules();
		
		$result = $mkknp->read_komponen($id,NULL,$parent);
		
		$return_arr = array();
		
		if($result):
			foreach($result as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				
				$child = $this->komponen_nilai_kknp_get_child($row->komponen_id);
				$arr['child'] = $child;
				
				array_push($return_arr,$arr);
			}
	
			$json_response = json_encode($return_arr);
			return $json_response;
		else:
			return NULL;
		endif;
	}
	
	function komponen_nilai_kknp_get_child($parentid=NULL){
		$mkknp = new model_kknpModules();
		
		$result		= $mkknp->read_komponen(NULL,$parentid,'child');
		if($result):
			$return_arr = array();
			foreach($result as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			return $return_arr;
		else:
			return NULL;
		endif;
	}
	
}?>