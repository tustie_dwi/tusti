<?php
class kknp_nilai extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mnilai = new model_nilai();
		
		$data['posts']	= '';
		$data['header'] = 'Nilai KKNP';
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.new.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		// $this->coms->add_script('select/select2.js');
		// $this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/kknp/nilai.js');
		
		$this->view('nilai/index.php', $data );
	}
	
	function add($pengajuanid=NULL){
		$mnilai = new model_nilai();
		$mkknp 	= new model_kknpModules();
		
		$data['header'] = 'Nilai KKNP';
		$data['pengajuan'] = $mkknp->read($pengajuanid);
		
		$data['data_nilai'] = $this->data_nilai($pengajuanid,'pengajuan_id_MD5');
		
		$this->add_script('js/kknp/nilai.js');
		
		$this->view('nilai/write.php', $data );
		
	}
	
	function get_nilai_by_mahasiswa(){
		$mnilai = new model_nilai();
		
		$mhs 		= $_POST['mhs'];
		$data_m 	= explode('-', $mhs);
		$nama_mhs 	= substr($data_m[1], 1);
		$nim_mhs 	= substr($data_m[0], 0, -1);
		
		$data_mhs_kknp	= $mnilai->get_kknp_mhs($nama_mhs,$nim_mhs,'1');
		if(isset($data_mhs_kknp)){
			$mhs_kknp_id	= $data_mhs_kknp[0]->hid_id;
			$mhs_all		= $data_mhs_kknp[0]->nama_mhs_all;
			$pengajuan_id	= $data_mhs_kknp[0]->pengajuan_id;
			$param			= 'pengajuan_id_noMD5';
			
			$data_nilai 	= $this->data_nilai($pengajuan_id,$param);
			
			$data['$mhs_kknp_id'] 	= $mhs_kknp_id;
			$data['data_nilai']		= $data_nilai;
			$data['mhs']			= $mhs_all;
			
			$this->view('nilai/show.php', $data );
			
		}
		
	}
	
	function data_nilai($pengajuan_id=NULL,$param=NULL){
		$mnilai = new model_nilai();
		$mkknp 	= new model_kknpModules();
		
		$data 	= array();
		$result = array();
		
		$name[0] = 'pembimbing';
		$name[1] = 'komponen';
		$name[2] = 'nilai';
		
		$data[0] = $mkknp->read_pembimbing($pengajuan_id,$param);
		$data[1] = $mkknp->read_komponen(NULL,NULL,'child','1');
		$data[2] = $mnilai->read($pengajuan_id,$param);
		
		$return_arr = array();
			
		for($i=0; $i < count($data); $i++){
			$arr = array();
			$res = array();
			if(count($data[$i])>0):
				foreach($data[$i] as $row){
					foreach ($row as $key => $value) {
						$arr[$key] = $value;				 
					}
					$res[] = $arr;
				}
				$result[$name[$i]] = $res;
			else:
				$result[$name[$i]] = NULL;
			endif;
		}
		
		array_push($return_arr,$result);
		
		$json_response = json_encode($result);
		return $json_response;
		
	}
	
	function save(){
		$mnilai = new model_nilai();
		$mkknp 	= new model_kknpModules();
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$komponen_id	= $_POST['komponen_id'];
		$kknp_mhs_id	= $_POST['kknp_mhs_id'];
		$pembimbing_id	= $_POST['pembimbing_id'];
		$nilai_txt 		= $_POST['nilai'];
		$inf_huruf		= $_POST['inf_huruf'];
		$hidId			= $_POST['hidId'];
		
		$blank = array('');
		$nilai_ = array_diff($nilai_txt,$blank);
		
		$rollback_id = array();
		
		foreach ($nilai_ as $key => $n) {
			
			if(isset($hidId[$key]) && $hidId[$key]!=''){
				$nilai_id = $hidId[$key];
			}else{
				$nilai_id = $mnilai->get_kknp_nilai_id();
			}
			
			$datanya = array(
   					      'nilai_id'=>$nilai_id,
					      'komponen_id'=>$komponen_id[$key],
					      'kknp_mhs_id'=>$kknp_mhs_id[$key],
					      'pembimbing_id'=>$pembimbing_id[$key],
					      'nilai'=>$nilai_txt[$key],
					      'inf_huruf'=>$inf_huruf[$key],
					      'user_id'=>$userid,
					      'last_update'=>$lastupdate
   						 );
			
			if($mnilai->replace_nilai($datanya)){
		   		// echo 'sukses';
		   		array_push($rollback_id, $nilai_id);
		   	}else{
		   		foreach ($rollback_id as $r):$mnilai->delete_nilai($r);endforeach;
		   		echo 'gagal';
				exit;
		   	}
			
		}
		// print_r($rollback_id);
		
	}
	
}?>