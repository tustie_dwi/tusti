<?php
class model_logbook extends model {
	
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function get_kknp_log_mhs($kknp_mhs_id=NULL){
		$sql = "SELECT
				MID(MD5(tbl_kknp_logbook.log_id),6,6) AS log_id,
				tbl_kknp_logbook.log_id AS hidId,
				tbl_kknp_logbook.kknp_mhs_id,
				tbl_kknp_logbook.tgl,
				tbl_kknp_logbook.nama_kegiatan,
				tbl_kknp_logbook.catatan,
				tbl_kknp_logbook.pembimbing_id,
				tbl_kknp_logbook.user_id,
				tbl_kknp_logbook.last_update,
				
				tbl_kknp_pembimbing.nama AS nama_pembimbing
				
				FROM db_ptiik_apps.tbl_kknp_logbook
				LEFT JOIN db_ptiik_apps.tbl_kknp_pembimbing ON tbl_kknp_pembimbing.pembimbing_id = tbl_kknp_logbook.pembimbing_id
				WHERE 1
			   ";
		
		if($kknp_mhs_id){
			$sql .= " AND tbl_kknp_logbook.kknp_mhs_id = '".$kknp_mhs_id."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
		
	}
	
	function replace_log($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_logbook',$datanya);
	}
	
	function get_log_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(log_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_logbook 
				where left(log_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
}
?>