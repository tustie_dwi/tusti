<?php
class model_kknpModules extends model {
	
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function get_thn_akademik(){
		$sql = "SELECT tahun_akademik, CONCAT(tahun , ' ' , is_ganjil , ' ' , is_pendek) as thnakademik FROM `db_ptiik_apps`.`tbl_tahunakademik` ORDER BY tahun_akademik DESC
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_surat($jenis_surat=NULL, $kategori=NULL){
		$sql = "SELECT tbl_surat_template.template_id,
				tbl_surat_template.kop_surat,
				tbl_surat_template.jenis_surat,
				tbl_surat_template.isi_surat,
				tbl_surat_template.kategori
				FROM db_ptiik_apps.tbl_surat_template
				WHERE 1
			   ";
		
		if($jenis_surat){
			$sql .= " AND tbl_surat_template.jenis_surat = '".$jenis_surat."' ";
		}
		
		if($kategori){
			$sql .= " AND tbl_surat_template.kategori = '".$kategori."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
		
	}
	
	function read_objek($pengajuanid=NULL,$param=NULL,$keterangan=NULL){
		$sql = "SELECT * FROM db_ptiik_apps.tbl_kknp_objek WHERE 1";
		
		if($pengajuanid){
			if($param=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_objek.pengajuan_id = '".$pengajuanid."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_objek.pengajuan_id),6,6) = '".$pengajuanid."' ";
			}
		}
		
		if($keterangan){
			$sql .= " AND tbl_kknp_objek.keterangan = '".$keterangan."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;	
		
	}
	
	function read_file($pengajuanid=NULL,$param=NULL){
		$sql = "SELECT * FROM db_ptiik_apps.tbl_kknp_file WHERE 1";
		
		if($pengajuanid){
			if($param=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_file.pengajuan_id = '".$pengajuanid."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_file.pengajuan_id),6,6) = '".$pengajuanid."' ";
			}
		}
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function read_pembimbing($pengajuanid=NULL,$param=NULL){
		$sql = "SELECT * FROM db_ptiik_apps.tbl_kknp_pembimbing WHERE 1";
		
		if($pengajuanid){
			if($param=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_pembimbing.pengajuan_id = '".$pengajuanid."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_pembimbing.pengajuan_id),6,6) = '".$pengajuanid."' ";
			}
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function read($pengajuanid=NULL){
		$sql = "SELECT 
				MID(MD5(tbl_kknp_pengajuan.pengajuan_id),6,6) AS pengajuan_id,
				tbl_kknp_pengajuan.pengajuan_id AS hidId,
				tbl_kknp_pengajuan.perusahaan_id,
				tbl_kknp_pengajuan.cabang_id,
				tbl_kknp_pengajuan.fakultas_id,
				tbl_kknp_pengajuan.tahun_akademik,
				tbl_kknp_pengajuan.tgl_pengajuan,
				tbl_kknp_pengajuan.tgl_mulai,
				tbl_kknp_pengajuan.tgl_selesai,
				tbl_kknp_pengajuan.tgl_laporan,
				tbl_kknp_pengajuan.is_status,
				tbl_kknp_pengajuan.keterangan,
				tbl_kknp_pengajuan.user_id,
				(SELECT coms_user.name FROM db_coms.coms_user WHERE coms_user.id = tbl_kknp_pengajuan.user_id) AS user_name,
				tbl_kknp_pengajuan.last_update,
				SUBSTRING(tbl_kknp_pengajuan.last_update, 1,10) AS YMD,
				SUBSTRING(tbl_kknp_pengajuan.last_update, 12,8) AS waktu,

				tbl_kknp_perusahaan.nama AS nama_perusahaan,
				tbl_kknp_perusahaan.alamat,
				
				
				GROUP_CONCAT( CONCAT(tbl_mahasiswa.nama, '|', tbl_unit_kerja.keterangan, '|', tbl_kknp_mahasiswa.is_aktif ,'|', tbl_kknp_mahasiswa.kknp_mhs_id) SEPARATOR '@') AS nama_mhs
				
				FROM db_ptiik_apps.tbl_kknp_pengajuan
				LEFT JOIN db_ptiik_apps.tbl_kknp_perusahaan ON tbl_kknp_pengajuan.perusahaan_id = tbl_kknp_perusahaan.perusahaan_id
				LEFT JOIN db_ptiik_apps.tbl_kknp_mahasiswa ON tbl_kknp_mahasiswa.pengajuan_id = tbl_kknp_pengajuan.pengajuan_id
				LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_kknp_mahasiswa.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_mahasiswa.prodi_id AND tbl_unit_kerja.kategori = 'prodi'
				WHERE 1
			   ";
		
		if($pengajuanid){
			$sql .= " AND MID(MD5(tbl_kknp_pengajuan.pengajuan_id),6,6) = '".$pengajuanid."' ";
		}
		
		$sql .= " GROUP BY tbl_kknp_pengajuan.pengajuan_id ";
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function read_surat($pengajuan_id=NULL,$param=NULL,$kknp_surat_id=NULL,$md5_pengajuan=NULL){
		$sql = "SELECT 
				MID(MD5(tbl_kknp_surat.kknp_surat_id),6,6) AS kknp_surat_id,
				tbl_kknp_surat.kknp_surat_id AS hidId,
				tbl_kknp_surat.jenis_surat,
				tbl_kknp_surat.pengajuan_id,
				tbl_kknp_surat.template_id,
				tbl_kknp_surat.no_surat,
				tbl_kknp_surat.tgl_surat,
				tbl_kknp_surat.tgl_surat_selesai,
				tbl_kknp_surat.isi_surat,
				MID(MD5(tbl_kknp_surat.file_surat),6,6) AS file_surat,
				tbl_kknp_surat.file_surat AS hidId2,
				tbl_kknp_surat.user_id,
				tbl_kknp_surat.last_update
				FROM db_ptiik_apps.tbl_kknp_surat 
				WHERE 1
			   ";
		
		if($kknp_surat_id){
			$sql .= " AND MID(MD5(tbl_kknp_surat.kknp_surat_id),6,6) = '".$kknp_surat_id."' ";
		}
		
		if($pengajuan_id){
			if($md5_pengajuan=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_surat.pengajuan_id = '".$pengajuan_id."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_surat.pengajuan_id),6,6) = '".$pengajuan_id."' ";
			}
			
		}
		
		if($param){
			if($param=='index'){
				$sql .= " AND tbl_kknp_surat.tgl_surat != '0000-00-00' ";
			}
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
		
	}
	
	function get_mhs($name=NULL,$nim=NULL,$str=NULL){
		$sql = "SELECT concat(nim, ' - ', nama) as value,
					   nim,
					   mahasiswa_id as hid_id,
					   MID( MD5(mahasiswa_id), 6, 6) as mhs_id 
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
		 		WHERE tbl_mahasiswa.is_aktif = 'aktif' ";
		
		if($name){
			$sql .= " AND tbl_mahasiswa.nama = '".$name."' ";
		}
		
		if($nim){
			$sql .= " AND tbl_mahasiswa.nim = '".$nim."' ";
		}
		
		if($str){
			$sql .= " AND tbl_mahasiswa.nama LIKE '%".$str."%' ";
		}
		
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_dosen($str=NULL){
		$sql = "SELECT karyawan_id as `id`, nama as `value`
				FROM `db_ptiik_apps`.`tbl_karyawan` WHERE nama LIKE '%".$str."%' AND is_status='dosen' AND is_aktif NOT IN ('keluar','meninggal') "; 
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_company($name=NULL,$addr=NULL,$str=NULL){
		$sql = "SELECT 
				MID( MD5(tbl_kknp_perusahaan.perusahaan_id), 6, 6) as perusahaan_id,
				tbl_kknp_perusahaan.perusahaan_id as hid_id,
				tbl_kknp_perusahaan.nama as value,
				tbl_kknp_perusahaan.alamat,
				tbl_kknp_perusahaan.telp
				FROM db_ptiik_apps.tbl_kknp_perusahaan 
				WHERE 1 ";
		
		if($name){
			$sql .= " AND tbl_kknp_perusahaan.nama = '".$name."' ";
		}
		
		if($addr){
			$sql .= " AND tbl_kknp_perusahaan.alamat = '".$addr."' ";
		}
		
		if($str){
			$sql .= " AND tbl_kknp_perusahaan.nama LIKE '%".$str."%' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_kknp_mhs($pengajuan_id=NULL, $mhs_id=NULL, $param=NULL, $aktif=NULL){
		$sql = "SELECT 
				MID( MD5(tbl_kknp_mahasiswa.kknp_mhs_id), 6, 6) as kknp_mhs_id,
				tbl_kknp_mahasiswa.kknp_mhs_id as hid_id,
				tbl_kknp_mahasiswa.pengajuan_id,
				tbl_kknp_mahasiswa.mahasiswa_id,
				tbl_kknp_mahasiswa.is_aktif,
				CONCAT(tbl_mahasiswa.nim, ' - ', tbl_mahasiswa.nama) AS nama_mhs
				FROM db_ptiik_apps.tbl_kknp_mahasiswa 
				LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_kknp_mahasiswa.mahasiswa_id
				WHERE 1 ";
		
		if($pengajuan_id){
			// $sql .= " AND tbl_kknp_mahasiswa.pengajuan_id = '".$pengajuan_id."' ";
			if($param=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_mahasiswa.pengajuan_id = '".$pengajuan_id."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_mahasiswa.pengajuan_id),6,6) = '".$pengajuanid."' ";
			}
		}
		
		if($mhs_id){
			$sql .= " AND tbl_kknp_mahasiswa.mahasiswa_id = '".$mhs_id."' ";
		}
		
		if($aktif){
			if($aktif=='1'){
				$sql .= " AND tbl_kknp_mahasiswa.is_aktif = '1' ";
			}else{
				$sql .= " AND tbl_kknp_mahasiswa.is_aktif = '0' ";
			}
			
			
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
		
	}
	
	function get_from_unit_kerja($param=NULL){
		$sql = "SELECT * FROM db_ptiik_apps.tbl_unit_kerja WHERE 1";
		
		if($param){
			$sql .= " AND tbl_unit_kerja.kategori = '".$param."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_cabang($cabangid=NULL){
		$sql = "SELECT * FROM db_ptiik_apps.tbl_cabang WHERE 1";
		
		if($cabangid){
			$sql .= " AND tbl_cabang.cabang_id = '".$cabangid."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function data_mhs($nama=NULL,$nim=NULL){
		$sql = "SELECT tbl_mahasiswa.* ,
				tbl_unit_kerja.keterangan AS prodi_mhs
				FROM db_ptiik_apps.tbl_mahasiswa 
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.kode = tbl_mahasiswa.prodi_id AND tbl_unit_kerja.kategori = 'prodi'
				WHERE 1";
		
		if($nama){
			$sql .= " AND tbl_mahasiswa.nama LIKE '%".$nama."%' ";
		}
		if($nim){
			$sql .= " AND tbl_mahasiswa.nim = '".$nim."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_data_dosen($nama=NULL){
		$sql = "SELECT 
				CASE
				  WHEN tbl_karyawan.gelar_awal IS NULL THEN CONCAT(tbl_karyawan.nama, ' ',tbl_karyawan.gelar_akhir)
				  ELSE CONCAT(tbl_karyawan.gelar_awal, ' ', tbl_karyawan.nama, ' ',tbl_karyawan.gelar_akhir)
				END AS nama,
				tbl_karyawan.nik,
				tbl_karyawan.golongan,
				
				CASE
				  WHEN tbl_master_jabatan.keterangan IS NULL THEN 'Dosen'
				  ELSE tbl_master_jabatan.keterangan
				END AS jabatan
				
				FROM db_ptiik_apps.tbl_karyawan
				LEFT JOIN db_ptiik_apps.tbl_karyawan_kenaikan ON tbl_karyawan.karyawan_id = tbl_karyawan_kenaikan.karyawan_id 
				AND tbl_karyawan_kenaikan.jenis = 'jabatan' 
				AND tbl_karyawan_kenaikan.is_aktif = '1'
				AND tbl_karyawan_kenaikan.periode_selesai >= '".date("Y-m-d")."'
				LEFT JOIN db_ptiik_apps.tbl_master_jabatan ON tbl_karyawan_kenaikan.jabatan_id = tbl_master_jabatan.jabatan_id
				WHERE 1
				";
		if($nama){
			$sql .= " AND tbl_karyawan.nama = '".$nama."' ";
		}
		$strresult = $this->db->getRow( $sql );
		return $strresult;
		
	}
	
	function delete_objek($objek_id=NULL,$pengajuan_id=NULL,$param=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_kknp_objek WHERE 1 ";
		
		if($objek_id){
			$sql .= " AND tbl_kknp_objek.objek_id = '".$objek_id."' ";
		}
		
		if($pengajuan_id){
			if($param=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_objek.pengajuan_id = '".$pengajuan_id."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_objek.pengajuan_id),6,6) = '".$pengajuan_id."' ";
			}
		}
		$this->db->query( $sql );
	}
	
	function delete_mhs($kknp_mhs_id=NULL,$pengajuan_id=NULL,$status=NULL,$param=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_kknp_mahasiswa WHERE 1 ";
		
		if($kknp_mhs_id){
			$sql .= " AND tbl_kknp_mahasiswa.kknp_mhs_id = '".$kknp_mhs_id."' ";
		}
		
		if($pengajuan_id){
			if($param=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_mahasiswa.pengajuan_id = '".$pengajuan_id."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_mahasiswa.pengajuan_id),6,6) = '".$pengajuan_id."' ";
			}
		}
		
		if($status){
			if($status=='1'){
				$sql .= " AND tbl_kknp_mahasiswa.is_aktif = '".$status."' ";
			}else{
				$sql .= " AND tbl_kknp_mahasiswa.is_aktif = '0' ";
			}
			
		}
		
		$this->db->query( $sql );
	}
	
	function get_pengajuan_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(pengajuan_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_pengajuan
				where left(pengajuan_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_company_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(perusahaan_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_perusahaan
				where left(perusahaan_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_mhs_kknp_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kknp_mhs_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_mahasiswa 
				where left(kknp_mhs_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_objek_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(objek_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_objek 
				where left(objek_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_kknp_surat_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kknp_surat_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_surat 
				where left(kknp_surat_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_file_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_file 
				where left(file_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_pembimbing_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(pembimbing_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_pembimbing 
				where left(pembimbing_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function replace_perusahaan($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_perusahaan',$datanya);
	}
	
	function replace_kknp($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_pengajuan',$datanya);
	}
	
	function replace_kknp_mhs($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_mahasiswa',$datanya);
	}
	
	function replace_kknp_objek($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_objek',$datanya);
	}
	
	function replace_surat($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_surat',$datanya);
	}
	
	function replace_file($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_file',$datanya);
	}
	function replace_kknp_pembimbing($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_pembimbing',$datanya);
	}
	
	/*
	 * KOMPONEN
	 * */
	
	function read_komponen($komponen_id=NULL,$parentid=NULL,$parent=NULL,$aktif=NULL){
		$sql = "SELECT MID(MD5(tbl_kknp_nilai_komponen.komponen_id),6,6) AS komponen_id,
				tbl_kknp_nilai_komponen.komponen_id AS hidId,
				tbl_kknp_nilai_komponen.keterangan,
				CASE
				  WHEN tbl_kknp_nilai_komponen.parent_id = 0 THEN 0
				  ELSE MID(MD5(tbl_kknp_nilai_komponen.parent_id),6,6)
				END AS parent_id,
				tbl_kknp_nilai_komponen.parent_id AS hidId2,
				tbl_kknp_nilai_komponen.is_aktif,
				tbl_kknp_nilai_komponen.urut,
				tbl_kknp_nilai_komponen.bobot,
				tbl_kknp_nilai_komponen.user,
				tbl_kknp_nilai_komponen.last_update,
				SUBSTRING(tbl_kknp_nilai_komponen.last_update, 1,10) AS YMD,
				SUBSTRING(tbl_kknp_nilai_komponen.last_update, 12,8) AS waktu
				FROM db_ptiik_apps.tbl_kknp_nilai_komponen
				WHERE 1
			   ";
		
		if($komponen_id){
			$sql .= " AND MID(MD5(tbl_kknp_nilai_komponen.komponen_id),6,6) = '".$komponen_id."' ";
		}
		
		if($parentid){
			$sql .= " AND MID(MD5(tbl_kknp_nilai_komponen.parent_id),6,6) = '".$parentid."' ";
		}
		
		if($aktif){
			$sql .= " AND tbl_kknp_nilai_komponen.is_aktif = '1' ";
		}
		
		if($parent){
			if($parent=='parent'){
				$sql .= " AND tbl_kknp_nilai_komponen.parent_id = 0 ";
			}else{
				$sql .= " AND tbl_kknp_nilai_komponen.parent_id != 0 ";
				$sql .= " ORDER BY tbl_kknp_nilai_komponen.urut ASC ";
			}
		}
		
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
		
	}
	
	function get_komponen_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(komponen_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_nilai_komponen 
				where left(komponen_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function replace_kknp_komponen_nilai($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_nilai_komponen',$datanya);
	}
	
} ?>