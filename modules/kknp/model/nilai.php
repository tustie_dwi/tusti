<?php
class model_nilai extends model {
	
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function read($pengajuanid=NULL,$param=NULL){
		$sql = "SELECT 
				MID(MD5(tbl_kknp_nilai.nilai_id),6,6) AS nilai_id,
				tbl_kknp_nilai.nilai_id AS hidId,
				tbl_kknp_nilai.komponen_id,
				tbl_kknp_nilai.kknp_mhs_id,
				tbl_kknp_nilai.pembimbing_id,
				tbl_kknp_nilai.nilai,
				tbl_kknp_nilai.inf_huruf,
				tbl_kknp_mahasiswa.pengajuan_id,
				tbl_kknp_nilai_komponen.keterangan AS komponen
				FROM db_ptiik_apps.tbl_kknp_nilai
				LEFT JOIN db_ptiik_apps.tbl_kknp_mahasiswa ON tbl_kknp_mahasiswa.kknp_mhs_id = tbl_kknp_nilai.kknp_mhs_id
				LEFT JOIN db_ptiik_apps.tbl_kknp_nilai_komponen ON tbl_kknp_nilai_komponen.komponen_id = tbl_kknp_nilai.komponen_id
				WHERE 1
			   ";
		
		if($pengajuanid){
			if($param=='pengajuan_id_noMD5'){
				$sql .= " AND tbl_kknp_mahasiswa.pengajuan_id = '".$pengajuanid."' ";
			}else{
				$sql .= " AND MID(MD5(tbl_kknp_mahasiswa.pengajuan_id),6,6) = '".$pengajuanid."' ";
			}
		}

		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_kknp_mhs($nama=NULL, $nim=NULL, $aktif=NULL){
		$sql = "SELECT 
				MID( MD5(tbl_kknp_mahasiswa.kknp_mhs_id), 6, 6) as kknp_mhs_id,
				tbl_kknp_mahasiswa.kknp_mhs_id as hid_id,
				MID( MD5(tbl_kknp_mahasiswa.pengajuan_id), 6, 6) as pengajuan_id_md5,
				tbl_kknp_mahasiswa.pengajuan_id,
				tbl_kknp_mahasiswa.mahasiswa_id,
				tbl_kknp_mahasiswa.is_aktif,
				CONCAT(tbl_mahasiswa.nim, ' - ', tbl_mahasiswa.nama) AS nama_mhs,
				GROUP_CONCAT( CONCAT(tbl_mahasiswa.nama, '|', tbl_unit_kerja.keterangan, '|', tbl_kknp_mahasiswa.is_aktif ,'|', tbl_kknp_mahasiswa.kknp_mhs_id) SEPARATOR '@') AS nama_mhs_all
				FROM db_ptiik_apps.tbl_kknp_mahasiswa 
				LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_kknp_mahasiswa.mahasiswa_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_mahasiswa.prodi_id AND tbl_unit_kerja.kategori = 'prodi'
				WHERE 1 ";
		
		if($nama){
			$sql .= " AND tbl_mahasiswa.nama LIKE '%".$nama."%' ";
		}
		
		if($nim){
			$sql .= " AND tbl_mahasiswa.nim = '".$nim."' ";
		}
		
		if($aktif){
			if($aktif=='1'){
				$sql .= " AND tbl_kknp_mahasiswa.is_aktif = '1' ";
			}else{
				$sql .= " AND tbl_kknp_mahasiswa.is_aktif = '0' ";
			}
		}
	
		$sql .= " GROUP BY tbl_kknp_mahasiswa.kknp_mhs_id ";		
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
		
	}
	
	function delete_nilai($nilai_id=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_kknp_nilai WHERE 1 ";
		
		if($nilai_id){
			$sql .= " AND tbl_kknp_nilai.nilai_id = '".$nilai_id."' ";
		}
		$this->db->query( $sql );
	}
	
	function get_kknp_nilai_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(nilai_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_nilai 
				where left(nilai_id,6) = '".date("Ym")."'"; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function replace_nilai($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_nilai',$datanya);
	}
	
}?>