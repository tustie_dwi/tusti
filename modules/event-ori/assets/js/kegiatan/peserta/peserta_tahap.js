$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
});
edit_tahap = false;
elementTahap = null;
function hapusPesertaTahap(peserta_tahap_id, el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_tahap_peserta",
	  	data: $.param({peserta_tahap_id:peserta_tahap_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function editPesertaTahap(peserta_tahap_id, tahap_id, catatan, is_aktif, el){
	$('#peserta_tahap_id').val(peserta_tahap_id);
	$('#peserta_tahap_id').removeAttr('disabled');
	$('#tahap_id').val(tahap_id);
	$('#catatan').val(catatan);
	if(is_aktif==1)
		$('#is_aktif').attr("checked","");
	else
		$('#is_aktif').removeAttr("checked");
	edit_tahap = true;
	elementTahap = el;
}
function cancelTahapPeserta(){
	$("#form_tahap_peserta")[0].reset();
	
	$('#peserta_tahap_id').val("");
	$('#peserta_tahap_id').attr('disabled','disabled')
	$('#is_aktif').removeAttr('checked');
	edit_tahap = false;
}
function submitTahapPeserta(){
	var cekvalid = true;
	$('#form_tahap_peserta :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_tahap_peserta",
		  	data: $("#form_tahap_peserta").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
				$('#table_tahap_peserta').prepend("<tr><td>"+
					"<a class='label label-danger pull-right' onclick='hapusPesertaTahap(\""+hasil.peserta_tahap_id+"\",this)'><i class='fa fa-remove'></i> Hapus</a>"+
					"<a class='label label-warning pull-right' onclick='editPesertaTahap(\""+hasil.peserta_tahap_id+"\", \""+hasil.tahap_id+"\",\""+hasil.catatan+"\",\""+hasil.is_aktif+"\",this)'><i class='fa fa-pencil'></i> Rubah</a>"+
					"<h5>"+hasil.keterangan+"</h5>"+
					"<p>"+hasil.catatan+"</p>"+
					"</td></tr>");
				if(edit_tahap) $(elementTahap).parent().parent().remove();
				cancelTahapPeserta();
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}