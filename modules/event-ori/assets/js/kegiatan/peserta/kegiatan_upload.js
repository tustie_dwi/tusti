$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
});
$('#search').keyup(function (){
	$.each($('li.media'), function (){
		if($( this ).html().indexOf($('#search').val()) <= -1)
			$( this ).hide();
		else
			$( this ).show();
	});
})
function cekDokumenKategori (){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/get_kategori_dokumen",
	  	success:function(data, textStatus, jqXHR) {
			var hasil = JSON.parse(data);
			$('#list_kategori_dokumen').empty();
			$.each(hasil, function(index,value){
				$('#list_kategori_dokumen').append("<li class='list-group-item'><a class='label label-danger pull-right' onclick='hapusDokumeKategori(\""+value.kategori_dokumen+"\",this)'><i class='fa fa-remove'></i> Hapus</a>"+
				"<a class='label label-warning pull-right' onclick='editDokumeKategori(\""+value.kategori_dokumen+"\",\""+value.keterangan+"\")'><i class='fa fa-pencil'></i> Rubah</a>"+value.keterangan+"</li>");	
			});
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function hapusDokumeKategori(kategori_dokumen,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_kategori_dokumen",
	  	data: $.param({kategori_dokumen:kategori_dokumen}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function editDokumeKategori(kategori_dokumen, keterangan){
	$('#kategori_dokumen_hidden').removeAttr("disabled");
	$('#kategori_dokumen_hidden').val(kategori_dokumen);
	console.log($('#kategori_dokumen_'));
	$('#kategori_dokumen_').attr("disabled","");
	$('#kategori_dokumen_').val(kategori_dokumen);
	$('#keterangan').val(keterangan);
}
function tambahDokumenKategori(){
	$.ajax({
		type: "POST",
	 	url: base_url+"module/event/kegiatan/submit_kategori_dokumen",
	 	data: $("#form_jenis_peserta").serialize(),
	 	success:function(data, textStatus, jqXHR) {
	 		var hasil = JSON.parse(data);
			$('#list_kategori_dokumen').empty();
			$.each(hasil, function(index,value){
				$('#list_kategori_dokumen').append("<li class='list-group-item'><a class='label label-danger pull-right' onclick='hapusDokumeKategori(\""+value.kategori_dokumen+"\",this)'><i class='fa fa-remove'></i> Hapus</a>"+
				"<a class='label label-warning pull-right' onclick='editDokumeKategori(\""+value.kategori_dokumen+"\",\""+value.keterangan+"\",this)'><i class='fa fa-pencil'></i> Rubah</a>"+value.keterangan+"</li>");	
			});
			cancelDokumenKategori();
	 	},
	 	error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function cancelDokumenKategori(){
	$("#form_jenis_peserta")[0].reset();
	$('#kategori_dokumen_hidden').attr("disabled","");
	$('#kategori_dokumen_').removeAttr("disabled");
}
function cancelPesertaUpload(){
	$("#form_upload")[0].reset();
	$('#upload_id').attr('disabled','disabled');
	$('#upload_id').val('');
}
function editPesertaUpload(upload_id,kategori_dokumen,is_valid){
	$('#upload_id').removeAttr('disabled');
	$('#upload_id').val(upload_id);
	if(is_valid==1)
		$('#is_valid').attr("checked","");
	else
		$('#is_valid').removeAttr("checked");
	$('#kategori_dokumen').val(kategori_dokumen);
}
function deletePesertaUpload(upload_id, el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_peserta_upload",
	  	data: $.param({upload_id:upload_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
var elementReview = null;
var edit_review = false;
function tambahReview(upload_id,el){
	$('#upload_id_review').val(upload_id);
	$('#modal_review').modal('show');
	elementReview = el;
	edit_review = false;
}
function cancelReview(){
	$("#form_review")[0].reset();
	$('#review_id').attr("disabled","");
	$('#modal_review').modal('hide');
}
function simpanReview(){
	var cekvalid = true;
	$('#form_review :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
			type: "POST",
		 	url: base_url+"module/event/kegiatan/submit_dokumen_review",
		 	data: $("#form_review").serialize(),
		 	success:function(data, textStatus, jqXHR) {
		 		var hasil = JSON.parse(data);
		 		$(elementReview).parent().parent().append(
		 			"<div class='media-body' style='padding:5px 20px; display: block; border-top: 1px solid #ddd;'>"+
						"<small class='pull-right'>"+hasil.tgl_review+"</small>"+
						"<h4 class='media-heading'>"+hasil.review_by+"</h4>"+
						"<p>"+hasil.hasil_review+"</p>"+
						"<a class='btn btn-danger btn-xs' onclick='deleteReview(\""+hasil.review_id+"\",this)'><i class='fa fa-times-circle-o'></i> </a> "+
						"<a class='btn btn-warning btn-xs' onclick='editReview(\""+hasil.review_id+"\", \""+hasil.review_by+"\",\""+tgl_review+"\",\""+hasil_review+"\",\""+hasil.st_review+"\",\""+hasil.catatan+"\",\""+hasil.upload_id+"\",this)'><i class='fa fa-edit'></i> </a>"+
					"</div>"
		 		);
		 		if(edit_review) $(elementReview).remove();
				cancelReview();
		 	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function editReview(review_id,review_by,tgl_review,hasil_review,st_review,catatan,upload_id,el){
	$('#upload_id_review').val(upload_id);
	$('#review_id').val(review_id);
	$('#review_id').removeAttr('disabled');
	edit_review=true;
	$('#review_by').val(review_by);
	var t = tgl_review.split(/[- :]/);
	$('#tgl_review').val(t[2]+"-"+t[1]+"-"+t[0]);
	$('#hasil_review').val(hasil_review);
	$('#st_review').val(st_review);
	$('#catatan').val(catatan);
	
	$('#modal_review').modal('show');
	elementReview = $(el).parent();
	
}
function deleteReview (review_id, el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_dokumen_review",
	  	data: $.param({review_id:review_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
