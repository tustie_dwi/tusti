$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
});
var edit_peserta = false;
var elementPeserta = null;
var jenis_peserta = "alumni";
var keterangan_peserta = "Alumni";
function editPesertaKegiatan(peserta_id,nama,gelar_awal,gelar_akhir,email,keterangan,tgl_registrasi,jenis_pendaftaran,metode_pembayaran,total_tagihan,total_bayar,telp,hp,alamat,instansi,alamat_instansi,el){
	$('#peserta_id').val(peserta_id);
	$('#nama').val(nama);
	$('#gelar_awal').val(gelar_awal);
	$('#gelar_akhir').val(gelar_akhir);
	$('#email').val(email);
	$('#keterangan').val(keterangan);
	var t = tgl_registrasi.split(/[- :]/);
	$('#tgl_registrasi').val(t[2]+"-"+t[1]+"-"+t[0]);
	$('#jenis_pendaftaran').val(jenis_pendaftaran);
	$('#metode_pembayaran').val(metode_pembayaran);
	$('#total_tagihan').val(total_tagihan);
	$('#total_bayar').val(total_bayar);
	$('#telp').val(telp);
	$('#hp').val(hp);
	$('#alamat').val(alamat);
	$('#instansi').val(instansi);
	$('#alamat_instansi').val(alamat_instansi);
	
	$('#peserta_id').removeAttr('disabled');
	elementPeserta = el;
	edit_peserta = true;
}
function deletePesertaaKegiatan(peserta_id, el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_kegiatan_peserta",
	  	data: $.param({peserta_id:peserta_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function simpanPesertaKegiatan(){
	var cekvalid = true;
	$('#form_kegiatan_peserta :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_kegiatan_peserta",
		  	data: $("#form_kegiatan_peserta").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
		  		$('#table_kegiatan_peserta').prepend(
		  			"<tr><td>"+hasil.gelar_awal+" "+hasil.nama+" "+hasil.gelar_akhir+"</td>"+
		  			"<td>"+hasil.email+"</td>"+
		  			"<td>"+hasil.hp+"</td>"+
		  			"<td>"+hasil.alamat+"</td>"+
		  			"<td style='min-width:200px'>"+
		  				"<a class='label label-primary' href='"+base_url+"module/event/kegiatan/peserta/"+$('#kegiatanid').val()+"/"+hasil.pesertaid+"'><i class='fa fa-gear'></i> Kelola Tagihan</a><br/>"+
		  				"<a class='label label-success' href='"+base_url+"module/event/kegiatan/berkas/"+$('#kegiatanid').val()+"/"+hasil.pesertaid+"'><i class='fa fa-gear'></i> Kelola Berkas</a><br/>"+
		  				"<a class='label label-warning' onclick='editPesertaKegiatan(\""+hasil.peserta_id+"\",\""+hasil.nama+"\",\""+hasil.gelar_awal+"\",\""+hasil.gelar_akhir+"\",\""+hasil.email+"\",\""+hasil.keterangan+"\",\""+hasil.tgl_registrasi+"\",\""+hasil.jenis_pendaftaran+"\",\""+hasil.metode_pembayaran+"\",\""+hasil.total_tagihan+"\",\""+hasil.total_bayar+"\",\""+hasil.telp+"\",\""+hasil.hp+"\",\""+hasil.alamat+"\",\""+hasil.instansi+"\",\""+hasil.alamat_instansi+"\",this)'><i class='fa fa-pencil'></i> Rubah</a><br>"+
		  				"<a class='label label-danger' onclick='deletePesertaaKegiatan(\""+hasil.peserta_id+"\",this)'><i class='fa fa-remove'></i> Hapus</a>"+
		  			"</td></tr>");	  
				$("#form_kegiatan_peserta")[0].reset();
				if(edit_peserta) $(elementPeserta).parent().parent().remove();
				$('#peserta_id').val("");
				$('#peserta_id').attr('disabled','disabled');
				edit_peserta = false;
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function cancelPesertaKegiatan(){
	$("#form_kegiatan_peserta")[0].reset();
	$('#peserta_id').val("");
	$('#peserta_id').attr('disabled','disabled');
	edit_peserta = false;
}
function yeah(a,b){
	jenis_peserta = b;
	keterangan_peserta = a;
}
function submit_jenis_peserta(){
	console.log($('#jenis_peserta :selected').val());
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/submit_jenis_peserta_kegiatan",
	  	data: $("#form_jenis_peserta").serialize(),
	  	success:function(data, textStatus, jqXHR) {
			$("#form_jenis_peserta")[0].reset();
			$('#jenis_peserta').append("<span class='label label-default' style='margin:0px 3px'>"+keterangan_peserta+" <a href='' style='opacity:0.6;color:white;' onclick='deleteJenisPeserta(\""+jenis_peserta+"\",this)'>X</a></span>");
			$('#jenis_peserta_peserta').append("<option id='"+jenis_peserta+"' value='"+jenis_peserta+"'>"+keterangan_peserta+"</option>");
	  	},
	 	error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function deleteJenisPeserta(jenis_peserta,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_kegiatan_jenis_peserta",
	  	data: $.param({kegiatan_id:$('#kegiatan_id').val(), jenis_peserta:jenis_peserta}),
	  	success:function(data, textStatus, jqXHR) {
			$('#'+jenis_peserta).remove();
			$(el).parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
