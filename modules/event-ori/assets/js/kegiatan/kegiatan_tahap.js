$(document).ready(function(){
	$(".tabel-hasil").DataTable();
	$(".date").datetimepicker({format: 'DD-MM-YYYY'});
});
var edit_tahap = false;
var cur_tahap = null;
function cancelTahap(){
	$('#form_tahap').slideUp( "fast",function (){
		$('#list-tahap').removeClass("col-md-7");		
	});
	$('#keterangan').val("");
	edit_tahap = false;
}
function tambahSubTahap(parent_id){
	$('#list-tahap').addClass("col-md-7");
	$('#form_tahap').slideDown( "fast" );
	
	$('#parent_id').val(parent_id);
	
	$('#tahap_id').attr("disabled","disabled");
	$('#urut').attr("disabled","disabled");
}
function editSubTahap(urut, tahap_id, keterangan, parent_id, el){
	$('#list-tahap').addClass("col-md-7");
	$('#form_tahap').slideDown( "fast" );
	
	$('#parent_id').val(parent_id);
	$('#keterangan').val(keterangan);
	$('#tahap_id').val(tahap_id);
	$('#tahap_id').removeAttr("disabled");
	$('#urut').val(urut);
	$('#urut').removeAttr("disabled");
	cur_tahap = el;
	edit_tahap = true;
}
function deleteSubTahap (tahap_id,el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/event/kegiatan/delete_tahap_kegiatan",
	  	data: $.param({tahap_id:tahap_id}),
	  	success:function(data, textStatus, jqXHR) {
			$(el).parent().remove();
		},
		error: function(jqXHR, textStatus, errorThrown) {
		    alert ('Proses Simpan Gagal!');      
		}
	});
}
function submitSubTahap(){
	var cekvalid = true;
	$('#form_tahap_kegiatan :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/event/kegiatan/submit_tahap_kegiatan",
		  	data: $("#form_tahap_kegiatan").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var hasil = JSON.parse(data);
				if(edit_tahap) $(cur_tahap).parent().find('h4').first().text(hasil.keterangan);
				else 
					$('#parentid_'+hasil.parent_id.replace(/\./g,'_')).append("<div class='list-group-item' style='padding:15px'>"+
						"<a data-toggle='collapse' href='#parentid_"+hasil.urut.replace(/\./g,'_')+"'><h4 style=''>"+hasil.keterangan+"</h4></a>"+
						"<a class='btn-default btn-xs' onclick='tambahSubTahap(\""+hasil.urut+"\")'><i class='fa fa-plus'></i></a>"+
						"<a class='btn-default btn-xs' onclick='editSubTahap(\""+hasil.urut+"\",\""+hasil.tahap_id+"\",\""+hasil.keterangan+"\",\""+hasil.parent_id+"\", this)'><i class='fa fa-pencil'></i></a>"+
						"<a class='btn-default btn-xs' onclick='deleteSubTahap(\""+hasil.tahap_id+"\", this)'><i class='fa fa-remove'></i></a>"+
						"<div class='list-group collapse in' style='margin:0px' id='parentid_"+hasil.urut.replace(/\./g,'_')+"'></div></div>"
					);
		  		cancelTahap();
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
