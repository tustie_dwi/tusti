<?php	
if(isset($posts)){
	$i= 1;
	foreach ($posts as $dt): ?>
		<tr>
			<td width="15%">
				<small><i class="fa fa-clock-o">&nbsp;</i><?php echo $dt->last_update ?></small>
			</td>
			<td>
				<strong><?php echo $dt->content_title; ?></strong>
					<code><?php echo $dt->content_link; ?></code>
				<?php if($dt->content_status == 'publish') 
					echo '<span class="label label-success">Published</span>'; 
					else echo '<span class="label label-warning">Draft</span>';
					
					echo " <span class='badge'>".$dt->content_category."</span>";
					if($dt->content_comment == '1') 
						echo ' <span class="label label-danger">*</span>';
					?>
				<br>
					<small><?php echo strip_tags($dt->content_content). "..."; ?></small>
			</td>
			<td width="10%">
				<ul class='nav nav-pills' style='margin:0;'>
					<li class='dropdown'>
					  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
					  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
						<li>
						<a class='btn-edit-post' href="<?php echo $this->location('module/event/content/edit/page/'.$dt->content_id); ?>"><i class='fa fa-edit'></i> Edit</a>	
						</li>
						<li>
						<a class='btn-edit-post' onclick="delete_(this, 'post_content', '<?php echo $dt->content_id ?>')" href="javascript::"><i class='fa fa-trash-o'></i> Delete</a>	
						</li>
						<li>
						<a class='btn-edit-post' href="<?php echo $this->location('module/event/content/detail/page/'.$dt->content_id); ?>"><i class='fa fa-eye'></i> View</a>	
						</li>
					  </ul>
					</li>
				</ul>
			</td>
		</tr>
		
		<?php $this->get_content_child($dt->content_id, 'content-menu'); ?>
		
	<?php endforeach; 
 }
?>