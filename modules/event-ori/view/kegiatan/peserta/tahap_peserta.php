<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li><a href="<?php echo $this->location('module/event/kegiatan/peserta/'.$kegiatan->kegiatanid); ?>">Kelola Peserta</a></li>
	  	<li class="active"><a>Peserta Tahap</a></li>
	</ol>
	<div class="col-md-7">
		<div class="panel">
			<div class="panel-heading">
				<h4 class="panel-title">
					Tahapan Peserta
				</h4>
			</div>
			<div class="panel-body">
				<table class="table tabel-hasil">
					<thead>
						<th></th>
					</thead>
					<tbody id="table_tahap_peserta">
						<?php
							if(!$peserta_tahap){}
							else
							foreach ($peserta_tahap as $key => $value) {
								echo "<tr>
										<td>
											<a class='label label-danger pull-right' onclick='hapusPesertaTahap(\"".$value->peserta_tahap_id."\",this)'><i class='fa fa-remove'></i> Hapus</a>
											<a class='label label-warning pull-right' onclick='editPesertaTahap(\"".$value->peserta_tahap_id."\", \"".$value->tahap_id."\",\"".$value->catatan."\",\"".$value->is_aktif."\",this)'><i class='fa fa-pencil'></i> Rubah</a>
											<h5>".$value->keterangan."</h5>
											<p>".$value->catatan."</p>
										</td>
									  </tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="panel">
			<form id="form_tahap_peserta">
			<div class="panel-heading">
				<h4 class="panel-title">
					Form Tahap Peserta
				</h4>
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label>Tahapan Peserta</label>
					<select class="form-control" name="tahap_id" id="tahap_id">
						<?php
							foreach ($tahap_kegiatan as $key => $value) {
								echo "<option value='".$value->tahap_id."'>".$value->keterangan."</option>";
							}
						?>
					</select>
					<input type="hidden" name="peserta_id" id="peserta_id" value="<?php echo $peserta->peserta_id; ?>">
					<input type="hidden" name="peserta_tahap_id" id="peserta_tahap_id" disabled="">
				</div>
				<div class="form-group">
					<label>Catatan</label>
					<textarea class="form-control" name="catatan" id="catatan"></textarea>
				</div>
				<div class="form-group">
					<label for="is_aktif">Aktif ?</label><br/>
					<input type="checkbox" name="is_aktif" id="is_aktif" />
				</div>
			</div>
			<div class="panel-footer">
				<button class="btn btn-primary" type="button" onclick="submitTahapPeserta()">Simpan</button>
				<button class="btn btn-default" type="button" onclick="cancelTahapPeserta()">Cancel</button>
			</div>
			</form>
		</div>
	</div>
<?php $this->foot(); ?>