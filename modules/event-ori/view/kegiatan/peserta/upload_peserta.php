<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li><a href="<?php echo $this->location('module/event/kegiatan/peserta/'.$kegiatan->kegiatanid); ?>">Kelola Peserta</a></li>
	  	<li class="active"><a>Kelola Berkas</a></li>
	</ol>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Daftar Tagihan : <?php echo $peserta->gelar_awal.' '.$peserta->nama.' '.$peserta->gelar_akhir." untuk kegiatan ".$kegiatan->nama.'@'.$kegiatan->lokasi ?></h3>
				</div>
				<div class="panel-body">
					<form class="navbar-form navbar-left" role="search">
						<div class="input-group" style='width:200px;'>
					      	<input type="text" class="form-control" placeholder="Search for..." id="search">
					      	<span class="input-group-btn">
					        	<button class="btn btn-default" type="button" disabled ><i class='fa fa-search'></i></button>
					      	</span>
					    </div>
					</form>
					<br/>
					
					<ul class="media-list">
					<?php
					 	if(!$peserta_upload){}
						else
							foreach ($peserta_upload as $key => $value) {
								$a="<li class='media' style='border : 1px solid #ddd;padding: 5px; border-radius: 5px; box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.1);'>
										<div class='media-body' style='display: block;'>
											<a class='btn btn-primary btn-xs pull-right' style='margin-left: 5px' href='". $this->config->file_url_view ."/".$value->file_loc."'><i class='fa fa-download'> </i></a>
											<a class='btn btn-primary btn-xs pull-right' style='margin-left: 5px'onclick='tambahReview(\"".$value->upload_id."\",this)'><i class='fa fa-comment'></i></a> 
											<a class='btn btn-warning btn-xs pull-right' onclick='editPesertaUpload(\"".$value->upload_id."\",\"".$value->kategori_dokumen."\",\"".$value->is_valid."\")'><i class='fa fa-pencil'></i></a>
										    <a class='btn btn-danger btn-xs pull-right' onclick='deletePesertaUpload(\"".$value->upload_id."\",this)'><i class='fa fa-remove'></i></a>
									      	<h4 class='media-heading'>".$value->keterangan."</h4>
									      	<p>".$value->file_name."</p>
									    ";
								if(!$value->reviews){}
								else
									foreach ($value->reviews as $k => $val)
										 $a.="<div class='media-body' style='padding:5px 20px; display: block; border-top: 1px solid #ddd;'>
										      	<small class='pull-right'>".$val->tgl_review."</small>
										      	<h4 class='media-heading'>
										      	".$val->review_by."
										      	</h4>
										      	<p>".$val->hasil_review."</p>
										      	<a class='btn btn-danger btn-xs' onclick='deleteReview(\"".$val->review_id."\",this)'><i class='fa fa-times-circle-o'></i> </a>
										      	<a class='btn btn-warning btn-xs' onclick='editReview(\"".$val->review_id."\", \"".$val->review_by."\",\"".$val->tgl_review."\",\"".$val->hasil_review."\",\"".$val->st_review."\",\"".$val->catatan."\",\"".$value->upload_id."\",this)'><i class='fa fa-edit'></i> </a>
										      </div>";
									$a.="</div></li>";
								echo $a;	    
							}
					?>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<form id="form_upload" action="<?php echo $this->location('module/event/kegiatan/submit_peserta_upload/'.$kegiatan->kegiatanid.'/'.$peserta->pesertaid); ?>" method="post" enctype="multipart/form-data">
			<div class="panel">
				<div class="panel-heading">
					<button class="btn btn-primary btn-xs pull-right" type="button" data-toggle="modal" data-target="#modal__kategori_dokumen" onclick="cekDokumenKategori()"><i class="fa fa-gear"></i> Dokumen Kategori</button>
					<h3 class="panel-title">Upload Berkas</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>Kategori File</label>
						<select id="kategori_dokumen" name="kategori_dokumen" class="form-control">
							<?php
								foreach ($dokumen_kategori as $key => $value) {
									echo "<option value='".$value->kategori_dokumen."'>".$value->keterangan."</option>";
								}
							?>
						</select>
						<input type="hidden" name="peserta_id" id="peserta_id" value="<?php echo $peserta->peserta_id; ?>"/>
						<input type="hidden" name="upload_id" id="upload_id" disabled=""/>
					</div>
					<div class="form-group">
						<label>Pilih File</label>
						<input type="file" class="form-control" id="file" name="file" />
					</div>
					<div class="form-group">
						<label for="is_valid">is valid ?</label><br/>
						<input type="checkbox" name="is_valid" id="is_valid"/>
					</div>
				</div>
				<div class="panel-footer">
					<button type="submit" class="btn btn-default"><i class="fa fa-cloud-upload"></i> upload</button>
					<button type="button" class="btn btn-default" onclick='cancelPesertaUpload()'> Cancel</button>
				</div>
			</div>
			</form>
		</div>
	</div>
	<div class="modal fade" id="modal_review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<form id="form_review">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title" id="title_modal_review"></h4>
		      		</div>
		      		<div class="modal-body">
		      			<div class="form-group">
		      				<label>Review By</label>
		      				<input type="text" class="form-control" name="review_by" id="review_by" required="">
		      				<input type="hidden" class="form-control" name="upload_id" id="upload_id_review">
		      				<input type="hidden" class="form-control" name="review_id" id="review_id" disabled="">
		      			</div>
		      			<div class="form-group">
		      				<label>Tanggal Review</label>
		      				<div class='input-group' >
								<input type='text' class="form-control date" name="tgl_review" id='tgl_review' required=""/>
								<span class="input-group-addon"><span class="fa fa-calendar"></span>
								</span>
							</div>
		      			</div>
		      			<div class="form-group">
		      				<label>Status Review</label>
		      				<input type="text" class="form-control" name="st_review" id="st_review" required="">
		      			</div>
		      			<div class="form-group">
		      				<label>Hasil Review</label>
		      				<textarea class="form-control" name="hasil_review" id="hasil_review" required=""></textarea>
		      			</div>
		      			<div class="form-group">
		      				<label>Catatan</label>
		      				<textarea class="form-control" name="catatan" id="catatan"></textarea>
		      			</div>
		      		</div>
		      		<div class="modal-footer">
		      			<button class="btn btn-primary btn-md" type="button" onclick="simpanReview()">Simpan</button>
		      			<button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancelDokumenKategori()">Close</button>
		    		</div>
	    		</form>
	      	</div>
	    </div>
	</div>
	<div class="modal fade" id="modal__kategori_dokumen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        		<h4 class="modal-title" >Dokumen Kategori</h4>
	      		</div>
	      		<div class="modal-body">
	      			<div style='width:100%; max-height: 300px;overflow: auto;'>
	      				<ul class="list-group" id="list_kategori_dokumen">
						</ul>
	      			</div><br/>
	      			<form id="form_jenis_peserta">
	      				<label>Tambah Kategori Dokumen</label>
	      				<div class="row">
	      					<div class="col-md-4">
	      						<input type="text" class="form-control" id="kategori_dokumen_" name="kategori_dokumen" placeholder="Kategori Id" required="">
	      						<input type="hidden" class="form-control" id="kategori_dokumen_hidden" name="kategori_dokumen" required="" disabled="">
	      					</div>
	      					<div class="col-md-8">
								<input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" required="">	      						
	      					</div>
	      					<div class="col-md-12" style="margin-top:5px;">
	      						<button type="button" class="btn btn-primary btn-xs" onclick="tambahDokumenKategori()">Simpan</button>
	      					</div>
		      			</div>
		      		</form>
	      		</div>
	      		<div class="modal-footer">
	      			<button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancelDokumenKategori()">Close</button>
	    		</div>
	      	</div>
	    </div>
	</div>
<?php $this->foot(); ?>