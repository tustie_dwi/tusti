<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li class="active"><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li class="active"><a>Kelola Biaya</a></li>
	</ol>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-heading">
					<a class="btn btn-primary btn-xs pull-right" href="<?php echo $this->location('module/event/master/biaya/'); ?>"><i class="fa fa-gear"></i> Jenis Biaya dan Peserta</a>
					<h4 class="panel-title">Rincian Biaya Kegiatan : <?php echo $kegiatan->nama."@".$kegiatan->lokasi ?></h4>
				</div>
				<div class="panel-body">
					<table class="table tabel-hasil" id="example">
						<thead>
							<th>Nama</th>
							<th>Jenis Biaya</th>
							<th>Harga</th>
							<th>Tanggal Mulai</th>
							<th>Tanggal Selesai</th>
							<th>#</th>
						</thead>
						<tbody id="table_kegiatan_biaya">
							<?php
								if(!$biaya_kegiatan){}
								else
								foreach ($biaya_kegiatan as $biaya){
									echo "
									<tr>
										<td>".$biaya->nama_biaya."</td>
										<td>".$biaya->keteranganBiaya."</td>
										<td>".$biaya->harga."</td>
										<td>".$biaya->tgl_mulai."</td>
										<td>".$biaya->tgl_selesai."</td>
										<td style='min-width:120px'>
											<a class='label label-warning' onclick='editBiayaKegiatan(\"".$biaya->komponen_id."\",\"".$biaya->jenis_biaya."\",\"".$biaya->nama_biaya."\",\"".$biaya->harga."\",\"".$biaya->tgl_mulai."\",\"".$biaya->tgl_selesai."\",\"".$biaya->is_aktif."\",this)'><i class='fa fa-pencil'></i> Rubah</a>
											<a class='label label-danger' onclick='hapusBiayaKegiatan(\"".$biaya->komponen_id."\",this)'><i class='fa fa-remove'></i> Hapus</a>
										</td>
									</tr>";
								} 
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<form id="form_kegiatan_biaya" name="form_kegiatan_biaya">
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">Form Biaya Kegiatan</h4>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label>Nama Biaya</label>
							<input type="text" class="form-control" id="nama_biaya" name="nama_biaya" required="" />
							<input type="hidden" class="form-control" id="kegiatan_id" name="kegiatan_id" value="<?php echo $kegiatan->kegiatan_id; ?>"/>
							<input type="hidden" class="form-control" id="komponen_id" name="komponen_id" disabled=""/>
						</div>
						<div class="form-group">
							<label>Jenis Biaya</label>
							<select class="form-control" id="jenis_biaya" name="jenis_biaya" required="">
								<?php
									foreach ($jenis_biaya as $jenis){
										echo "<option value='".$jenis->jenis_biaya."'>".$jenis->keterangan_biaya."</option>";
									} 
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Harga</label>
							<input type="number" class="form-control" id="harga" name="harga" required=""/>
						</div>
						<div class="form-group">
							<label for="is_aktif">publish ?</label><br/>
							<input type="checkbox" name="is_aktif" id="is_aktif" value="pub"/>
						</div>
						<div class="form-group" style="margin-left:-15px; margin-right:-15px;">
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal Mulai</label>
									<div class='input-group' >
										<input type='text' class="form-control date" name="tgl_mulai" id='tgl_mulai' required="" />
										<span class="input-group-addon"><span class="fa fa-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal Selesai</label>
									<div class='input-group' >
										<input type='text' class="form-control date" name="tgl_selesai" id='tgl_selesai' required=""/>
										<span class="input-group-addon"><span class="fa fa-calendar"></span>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button type="button" class="btn btn-primary" onclick="simpanBiayaKegiatan()">Simpan</button>
						<button type="button" class="btn btn-warning" onclick="cancelBiayaKegiatan()">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php $this->foot(); ?>