<?php $this->head(); ?>
	<ol class="breadcrumb" style="margin:-15px -30px 10px -30px;">
	  	<li class="active"><a href="<?php echo $this->location('module/event/kegiatan'); ?>">Agenda Kegiatan</a></li>
	  	<li class="active"><a>Jenis Biaya dan Peserta</a></li>
	</ol>
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-header" style="  padding: 10px; border-bottom: 1px solid #eee;">
					<button type="button" class="btn btn-default pull-right"  data-toggle="modal" data-target="#tambah_jenis_biaya" onclick="tambahJenisBiaya()"><i class="fa fa-plus"></i> jenis biaya</button>
					<h4>Jenis Biaya </h4>
				</div>
				<div class="panel-body">
					<table class="table tabel-hasil" id="example">
						<thead>
							<th>Jenis Peserta</th>
							<th>Biaya</th>
							<th>#</th>
						</thead>
						<tbody id="table_jenis_biaya">
							<?php
								if (!$jenis_biaya){}
								else
								foreach ($jenis_biaya as $value) {
									echo 
										"<tr>
											<td>".$value->keterangan_peserta."</td>
											<td>".$value->keterangan_biaya."</td>
											<td>
												<a class='label label-danger pull-right' onclick='deleteJenisBiaya(\"".$value->jenis_biaya."\",this)'><i class='fa fa-remove'></i> Hapus</a>
												<a class='label label-warning pull-right' onclick='editJenisBiaya(\"".$value->jenis_biaya."\",\"".$value->jenis_peserta."\",\"".$value->keterangan_biaya."\",this)'><i class='fa fa-pencil'></i> Edit</a>
											</td>
										</tr>";
										// <a class='label label-primary pull-right' href='".$this->location('module/event/master/biaya/'.$value->jenisBiaya)."'><i class='fa fa-gear'></i> Kelola</a>
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel">
				<div class="panel-header" style="  padding: 10px; border-bottom: 1px solid #eee;">
					<h4>Jenis Peserta</h4>
				</div>
				<div class="panel-body">
					<ul class="list-group" id="list_jenis_peserta">
						<?php
							if ($jenis_peserta)
							foreach ($jenis_peserta as $value) {
								echo  
								"<li class='list-group-item'>
									<a class='badge' onclick='deleteJenisPeserta(\"".$value->jenis_peserta."\",this)'><i class='fa fa-remove'></i></a>
									<a class='badge' onclick='editJenisPeserta(\"".$value->jenis_peserta."\",\"".$value->keterangan."\",this)'><i class='fa fa-pencil'></i></a>
									".$value->keterangan."
								</li>";
							}
						?>
						<li class="list-group-item btn btn-default">
							<center>
								<a href="" class="btn-block" data-toggle="modal" data-target="#tambah_jenis_peserta" onclick="tambahJenisPeserta()"><i class="fa fa-plus"></i> Tambah</a>
							</center>
						</li>
					</ul>					
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="tambah_jenis_peserta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	  		<form class="form-horizontal" id="form_jenis_peserta">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        			<h4 class="modal-title" id="myModalLabel">Tambah Jenis Peserta</h4>
	      			</div>
	      			<div class="modal-body">
        				<div class="form-group">
	        				<div class="col-xs-4">
	        					<label>Kode Jenis Peserta</label>
	        				</div>
	        				<div class="col-xs-8">
	        					<input type="text" id='jenis_peserta' class="form-control" placeholder="Kode Jenis Peserta" name="jenis_peserta" required="">
	        					<input type="hidden" id='jenis_peserta_hidden' class="form-control" placeholder="kode jenis peserta" name="jenis_peserta">
	        				</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-xs-4">
	        					<label>Keterangan Peserta</label>
	        				</div>
	        				<div class="col-xs-8">
	        					<input type="text" id='keterangan' class="form-control" placeholder="keterangan" name="keterangan" required="">
	        				</div>
	        			</div>
	      			</div>
	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        			<button type="button" class="btn btn-primary" id="submit_jenis_peserta" data-dismiss="modal">Save changes</button>
	      			</div>
	    		</div>
    		</form>
  		</div>
	</div>
	<div class="modal fade" id="tambah_jenis_biaya" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	  		<form class="form-horizontal" id="form_jenis_biaya">
	    		<div class="modal-content">
	      			<div class="modal-header">
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        			<h4 class="modal-title" >Tambah Jenis Peserta</h4>
	      			</div>
	      			<div class="modal-body">
        				<div class="form-group">
	        				<div class="col-xs-4">
	        					<label>Kode Jenis Biaya</label>
	        				</div>
	        				<div class="col-xs-8">
	        					<input type="text" class="form-control" placeholder="Kode Jenis Biaya" name="jenis_biaya" id="jenis_biaya" required="">
	        					<input type="hidden" class="form-control" name="jenis_biaya" id="jenis_biaya_hidden">
	        				</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-xs-4">
	        					<label>Keterangan Biaya</label>
	        				</div>
	        				<div class="col-xs-8">
	        					<input type="text" id='keterangan_biaya' class="form-control" placeholder="Keterangan " name="keterangan" required="">
	        				</div>
	        			</div>
	        			<div class="form-group">
	        				<div class="col-xs-4">
	        					<label>Jenis Peserta</label>
	        				</div>
	        				<div class="col-xs-8">
	        					<select class="form-control" id="select_peserta" name="jenis_peserta"></select>
	        				</div>
	        			</div>
	      			</div>
	      			<div class="modal-footer">
	        			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        			<button type="button" class="btn btn-primary" id="submit_jenis_biaya" data-dismiss="modal">Save changes</button>
	      			</div>
	    		</div>
    		</form>
  		</div>
	</div>
<?php $this->foot(); ?>