<?php
class model_master extends model {

	public function __construct() {
		parent::__construct();	
	}
	//jenis biaya
	public function get_jenis_kegitan(){
		$sql = "select * from db_ptiik_apps.tbl_jeniskegiatan";
		return $this->db->query($sql);
	}
	public function get_unit_id(){
		$sql = "select fakultas_id, unit_id, keterangan, english_version from db_ptiik_apps.tbl_unit_kerja where is_aktif=1";
		return $this->db->query($sql);
	}
	public function get_jenis_biaya($jenis_biaya=null){
		$sql = "select 
				tbl_jenis_peserta.keterangan as keterangan_peserta,
				tbl_jenis_peserta.jenis_peserta,  
				tbl_master_biaya_jenis.jenis_biaya,
				mid(md5(tbl_master_biaya_jenis.jenis_biaya),6,6) as jenisBiaya, 
				tbl_master_biaya_jenis.keterangan as keterangan_biaya
				from db_ptiik_event.tbl_master_biaya_jenis,db_ptiik_event.tbl_jenis_peserta 
				where tbl_jenis_peserta.jenis_peserta=tbl_master_biaya_jenis.jenis_peserta";
		if(!$jenis_biaya){
			return $this->db->query( $sql );
		}
		else{
			$sql .= " and tbl_master_biaya_jenis.jenis_biaya='".$jenis_biaya."'";
			return $this->db->getRow($sql);
		}
	}
	public function simpan_jenis_biaya($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_master_biaya_jenis',$data);
	}
	public function hapus_jenis_biaya($jenis_biaya){
		$result = $this->db->query("delete from db_ptiik_event.tbl_master_biaya_jenis where jenis_biaya='$jenis_biaya'");
	}
	//jenis peserta
	public function get_jenis_peserta(){
		$sql = "select keterangan, 
				jenis_peserta, 
				mid(md5(jenis_peserta),6,6) as jenisPeserta 
				from db_ptiik_event.tbl_jenis_peserta";
		$result = $this->db->query( $sql );
		return $result;
	}
	public function simpan_jenis_peserta($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_jenis_peserta',$data);
	}
	public function hapus_jenis_peserta($jenis_peserta){
		$result = $this->db->query("delete from db_ptiik_event.tbl_jenis_peserta where jenis_peserta='$jenis_peserta'");
	}
	//kegiatan
	function get_nextid_kegiatan(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kegiatan_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_kegiatan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_kegiatan($kegiatan_id=null){
		$sql = "select kegiatan_id, mid(md5(kegiatan_id),6,6) as kegiatanid,
				nama, lokasi, tgl_mulai, tgl_selesai, is_aktif, last_update,
				tbl_kegiatan.jenis_kegiatan,tbl_kegiatan.unit_id , tbl_kegiatan.kode_kegiatan
				from db_ptiik_event.tbl_kegiatan";
		if(!$kegiatan_id){
			return $this->db->query($sql);
		}else{
			$sql .= " where kegiatan_id='".$kegiatan_id."'";
			return $this->db->getRow($sql);
		}
	}
	public function get_kegiatanDet($kegiatanid){
		$sql = "select kegiatan_id, mid(md5(kegiatan_id),6,6) as kegiatanid,
				nama, lokasi, tgl_mulai, tgl_selesai, is_aktif, last_update,
				tbl_kegiatan.jenis_kegiatan,tbl_kegiatan.unit_id , tbl_kegiatan.kode_kegiatan 
				from db_ptiik_event.tbl_kegiatan
				where mid(md5(kegiatan_id),6,6)='".$kegiatanid."'";
		return $this->db->getRow($sql);
	}
	public function simpan_kegiatan($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_kegiatan',$data);
	}
	public function hapus_kegiatan($kegiatan_id){
		$this->db->query("delete from db_ptiik_event.tbl_kegiatan where kegiatan_id='$kegiatan_id'");
		$this->db->query("delete from db_ptiik_event.tbl_master_biaya_komponen where kegiatan_id='$kegiatan_id'");
		$this->db->query("delete from db_ptiik_event.tbl_jenis_peserta_kegiatan where kegiatan_id='$kegiatan_id'");
		$result = $this->db->query("select * from db_ptiik_event.tbl_peserta_registrasi where kegiatan_id='$kegiatan_id'");
		if(!$result){}
		else
			foreach ($result as $key => $value) {
				$this->db->query("delete from db_ptiik_event.tbl_peserta_tagihan where peserta_id='".$value->peserta_id."'");
			}
		$this->db->query("delete from db_ptiik_event.tbl_peserta_registrasi where kegiatan_id='$kegiatan_id'");
	}
	//kegiatan biaya
	public function get_nextid_biaya_komponen (){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(komponen_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_master_biaya_komponen"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_biaya_komponen($kegiatanid=null,$komponen_id=null){
		$sql = "select tbl_master_biaya_komponen.komponen_id,  
				tbl_master_biaya_komponen.nama_biaya, tbl_master_biaya_komponen.harga, 
				tbl_master_biaya_komponen.tgl_mulai, tbl_master_biaya_komponen.tgl_selesai, 
				tbl_master_biaya_komponen.is_aktif, tbl_master_biaya_jenis.keterangan as keteranganBiaya, 
				tbl_master_biaya_jenis.jenis_biaya
				from db_ptiik_event.tbl_master_biaya_komponen, db_ptiik_event.tbl_master_biaya_jenis
				where tbl_master_biaya_komponen.jenis_biaya = tbl_master_biaya_jenis.jenis_biaya";
		if(!$kegiatanid){}
		else $sql .= " and mid(md5(tbl_master_biaya_komponen.kegiatan_id),6,6)='".$kegiatanid."'";
	
		if(!$komponen_id) return  $this->db->query($sql);
		else{
			$sql .= " and tbl_master_biaya_komponen.komponen_id='".$komponen_id."'";
			return $this->db->getRow($sql);
		}
	}
	public function hapus_kegiatan_biaya ($komponen_id){
		$result = $this->db->query("delete from db_ptiik_event.tbl_master_biaya_komponen where komponen_id='$komponen_id'");
	}
	public function simpan_kegiatan_biaya($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_master_biaya_komponen',$data);
	}
	//kegiatan peserta
	public function get_nextid_peserta_kegiatan(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(peserta_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_peserta_registrasi"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_peserta($kegiatanid=null, $peserta_id=null){
		$sql = "select tbl_peserta_registrasi.peserta_id, mid(md5(peserta_id),6,6) as pesertaid,
				tbl_peserta_registrasi.nama, tbl_peserta_registrasi.gelar_awal, tbl_peserta_registrasi.gelar_akhir, tbl_peserta_registrasi.email,
				tbl_peserta_registrasi.keterangan, tbl_peserta_registrasi.tgl_registrasi, tbl_peserta_registrasi.jenis_pendaftaran,
				tbl_peserta_registrasi.metode_pembayaran, tbl_peserta_registrasi.total_tagihan, tbl_peserta_registrasi.total_bayar,
				tbl_peserta_registrasi.telp, tbl_peserta_registrasi.hp, tbl_peserta_registrasi.alamat, tbl_peserta_registrasi.instansi,
				tbl_peserta_registrasi.alamat_instansi from db_ptiik_event.tbl_peserta_registrasi where 1 ";
		if(!$kegiatanid){}
		else $sql .= " and mid(md5(tbl_peserta_registrasi.kegiatan_id),6,6)='$kegiatanid'";
		
		if(!$peserta_id) return $this->db->query($sql);
		else{
			$sql .= " and tbl_peserta_registrasi.peserta_id='$peserta_id'";
			return $this->db->getRow($sql);
		}
	}
	public function get_pesertaDet($pesertaid){
		$sql = "select tbl_peserta_registrasi.peserta_id, mid(md5(peserta_id),6,6) as pesertaid,
				tbl_peserta_registrasi.nama, tbl_peserta_registrasi.gelar_awal, tbl_peserta_registrasi.gelar_akhir, tbl_peserta_registrasi.email,
				tbl_peserta_registrasi.keterangan, tbl_peserta_registrasi.tgl_registrasi, tbl_peserta_registrasi.jenis_pendaftaran,
				tbl_peserta_registrasi.metode_pembayaran, tbl_peserta_registrasi.total_tagihan, tbl_peserta_registrasi.total_bayar,
				tbl_peserta_registrasi.telp, tbl_peserta_registrasi.hp, tbl_peserta_registrasi.alamat, tbl_peserta_registrasi.instansi,
				tbl_peserta_registrasi.alamat_instansi from db_ptiik_event.tbl_peserta_registrasi where 1
				and mid(md5(tbl_peserta_registrasi.peserta_id),6,6)='$pesertaid'";
			return $this->db->getRow($sql);
	}
	public function simpan_kegiatan_peserta ($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_registrasi',$data);
	}
	public function hapus_kegiatan_peserta ($peserta_id){
		$this->db->query("delete from db_ptiik_event.tbl_peserta_tagihan where peserta_id='$peserta_id'");
		$this->db->query("delete from db_ptiik_event.tbl_peserta_registrasi where peserta_id='$peserta_id'");
	}
	public function get_jenis_peserta_kegiatan($kegiatanid){
		$sql = "select tbl_jenis_peserta_kegiatan.kegiatan_id, tbl_jenis_peserta_kegiatan.jenis_peserta, tbl_jenis_peserta.keterangan 
		from db_ptiik_event.tbl_jenis_peserta_kegiatan, db_ptiik_event.tbl_jenis_peserta
		where tbl_jenis_peserta_kegiatan.jenis_peserta = tbl_jenis_peserta.jenis_peserta
		and mid(md5(tbl_jenis_peserta_kegiatan.kegiatan_id),6,6)='$kegiatanid'";
		return $this->db->query($sql);
	}
	public function simpan_kegiatan_jenis_peserta($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_jenis_peserta_kegiatan',$data);
	}
	public function hapus_kegiatan_jenis_peserta ($kegiatan_id,$jenis_peserta){
		$result = $this->db->query("delete from db_ptiik_event.tbl_jenis_peserta_kegiatan where kegiatan_id='$kegiatan_id' and jenis_peserta='$jenis_peserta'");
	}
	//tagihan peserta
	public function get_nextid_peserta_tagihan(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tagihan_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_peserta_tagihan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_tagihan_peserta($pesertaid=null, $tagihan_id=null){
		$sql = "select tbl_peserta_tagihan.tagihan_id, tbl_peserta_tagihan.komponen_id, peserta_id,
				tbl_peserta_tagihan.inf_jumlah, tbl_peserta_tagihan.is_bayar, tbl_peserta_tagihan.tgl_bayar, 
				tbl_master_biaya_komponen.nama_biaya, tbl_master_biaya_komponen.harga, tbl_master_biaya_komponen.harga,
				(tbl_master_biaya_komponen.harga*tbl_peserta_tagihan.inf_jumlah) as total 
				from db_ptiik_event.tbl_peserta_tagihan, db_ptiik_event.tbl_master_biaya_komponen
				where tbl_peserta_tagihan.komponen_id=tbl_master_biaya_komponen.komponen_id";
		if(!$pesertaid){}
		else $sql .= " and mid(md5(tbl_peserta_tagihan.peserta_id),6,6)='$pesertaid'";
		
		if(!$tagihan_id) return $this->db->query($sql);
		else{
			$sql .= " and tbl_peserta_tagihan.tagihan_id='$tagihan_id'";
			return $this->db->getRow($sql);
		}
	}
	public function simpan_tagihan_peserta($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_tagihan',$data);
		$totalBaru  = $data['inf_jumlah']*$this->get_biaya_komponen(null,$data["komponen_id"])->harga;
		$totalLama = $this->get_peserta(null,$data["peserta_id"])->total_tagihan;
		$total = $totalBaru + $totalLama;
		$query = "update db_ptiik_event.tbl_peserta_registrasi set total_tagihan='$total' where peserta_id='".$data["peserta_id"]."'";
		$this->db->query($query);
		// print_r("update db_ptiik_event.tbl_peserta_registrasi set total_tagihan='$total' where peserta_id='".$data["peserta_id"]."'");
	}
	public function hapus_tagihan_peserta($tagihan_id){
		$tagihan = $this->get_tagihan_peserta(null,$tagihan_id);
		
		$totalBaru  = $tagihan->inf_jumlah*$this->get_biaya_komponen(null,$tagihan->komponen_id)->harga;
		$totalLama = $this->get_peserta(null,$tagihan->peserta_id)->total_tagihan;
		$total = $totalLama - $totalBaru;
		$query = "update db_ptiik_event.tbl_peserta_registrasi set total_tagihan='$total' where peserta_id='".$tagihan->peserta_id."'";
		$this->db->query($query);

		$result = $this->db->query("delete from db_ptiik_event.tbl_peserta_tagihan where tagihan_id='$tagihan_id'");
	}
	//peserta upload
	public function get_nextid_peserta_upload(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(upload_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_peserta_upload"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_nextid_dokumen_review(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(review_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_dokumen_review"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_upload_pesertaDet($pesertaid){
		$sql = "select tbl_peserta_upload.upload_id,
				tbl_peserta_upload.kategori_dokumen,
				tbl_peserta_upload.file_name,
				tbl_peserta_upload.file_size,  
				tbl_peserta_upload.file_loc, 
				tbl_peserta_upload.is_valid, 
				tbl_peserta_upload.last_update,
				tbl_peserta_upload.tgl_upload,
				tbl_master_kategori_dokumen.keterangan
				from db_ptiik_event.tbl_peserta_upload, db_ptiik_event.tbl_master_kategori_dokumen 
				where tbl_peserta_upload.kategori_dokumen=tbl_master_kategori_dokumen.kategori_dokumen
				and mid(md5(tbl_peserta_upload.peserta_id),6,6)='$pesertaid'";
		return $this->db->query($sql);
	}
	public function get_dokumen_review ($upload_id,$review_id=null){
		$sql = "select tbl_dokumen_review.review_id, tbl_dokumen_review.review_by, tbl_dokumen_review.tgl_review, 
				tbl_dokumen_review.hasil_review, tbl_dokumen_review.st_review, tbl_dokumen_review.catatan,
				tbl_dokumen_review.review_id 
				from db_ptiik_event.tbl_dokumen_review
				where tbl_dokumen_review.upload_id='$upload_id'";
		if(!$review_id)
			return $this->db->query($sql);
		else{
			$sql .= " and tbl_dokumen_review.review_id='$review_id'";
			return $this->db->getRow($sql);
		}
	} 
	public function get_dokumen_kategori(){
		$sql ="select kategori_dokumen, keterangan from db_ptiik_event.tbl_master_kategori_dokumen";
		return $this->db->query($sql);
	}
	public function simpan_kategori_dokumen($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_master_kategori_dokumen',$data);
	}
	public function hapus_kategori_dokumen($kategori_dokumen){
		$result = $this->db->query ("delete from db_ptiik_event.tbl_master_kategori_dokumen where kategori_dokumen='$kategori_dokumen'");
	}
	public function simpan_peserta_upload($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_upload',$data);
	}
	public function update_peserta_upload($data){
		//$result = $this->db->update('db_ptiik_event`.`tbl_master_kategori_dokumen',$data,$where);
		$sql = "update db_ptiik_event.tbl_peserta_upload 
				set kategori_dokumen='".$data['kategori_dokumen']."', 
				is_valid='".$data['is_valid']."', 
				last_update='".$data['last_update']."' 
				where upload_id='".$data['upload_id']."'"; 
		if (array_key_exists('file_name', $data)){
			$sql = "update db_ptiik_event.tbl_peserta_upload 
				set file_name='".$data['file_name']."', 
				file_size='".$data['file_size']."', 
				file_loc='".$data['file_loc']."',
				file_type='".$data['file_type']."'  
				where upload_id='".$data['upload_id']."'";
			$this->db->query($sql);
		}
	}
	public function hapus_peserta_upload($upload_id){
		$result = $this->db->query ("delete from db_ptiik_event.tbl_peserta_upload where upload_id='$upload_id'");
		$result = $this->db->query ("delete from db_ptiik_event.tbl_dokumen_review where upload_id='$upload_id'");
	}
	public function simpan_dokumen_review($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_dokumen_review',$data);
	}
	public function hapus_dokumen_review($review_id){
		$result = $this->db->query ("delete from db_ptiik_event.tbl_dokumen_review where review_id='$review_id'");
	}
	//tahap kegiatan
	public function get_nextid_kegiatan_tahap(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tahap_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_kegiatan_tahap"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_nexturut_tahap($parent_id,$kegiatan_id){
		if($parent_id==0)
			$sql = "SELECT IFNULL(MAX(urut),0)+1 as next from db_ptiik_event.tbl_kegiatan_tahap where parent_id='$parent_id' and kegiatan_id='$kegiatan_id'";
		else
			$sql = "SELECT IF(urut LIKE '%.%',max(substring_index(urut, '.',-1))+1, 1) as next from db_ptiik_event.tbl_kegiatan_tahap where parent_id='$parent_id' and kegiatan_id='$kegiatan_id'";
		$getRow = $this->db->getRow($sql);
		return ($parent_id==0)?$getRow->next:$parent_id.".".$getRow->next;
	}
	public function get_kegiatan_tahapDet($kegiatanid, $tahapid=null){
		if(!$tahapid){
			$sql ="select tahap_id, mid(md5(tahap_id),6,6) as tahapid, keterangan, parent_id, urut from db_ptiik_event.tbl_kegiatan_tahap where mid(md5(kegiatan_id),6,6)='$kegiatanid' order by urut asc";
			return $this->db->query($sql);	
		}
		else{
			$sql ="select tahap_id, mid(md5(tahap_id),6,6) as tahapid, keterangan, parent_id, urut from db_ptiik_event.tbl_kegiatan_tahap where mid(md5(kegiatan_id),6,6)='$kegiatanid' and mid(md5(tahap_id),6,6)='$tahapid' order by urut asc";
			return $this->db->getRow($sql);
		}
	}
	public function get_tahap_kegiatan($tahap_id){
		$sql ="select tahap_id, mid(md5(tahap_id),6,6) as tahapid, kegiatan_id, keterangan, parent_id, urut from db_ptiik_event.tbl_kegiatan_tahap where tahap_id='$tahap_id'";
		return $this->db->getRow($sql);
	}
	public function simpan_tahap_kegiatan($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_kegiatan_tahap',$data);
	}
	public function hapus_tahap_kegiatan($tahap_id){
		$tahap = $this->get_tahap_kegiatan($tahap_id);
		$this->hapus_tahap_kegiatan_tree($tahap->kegiatan_id, $tahap->urut);
		$this->db->query("delete from db_ptiik_event.tbl_kegiatan_tahap where tahap_id='$tahap_id'");
	}
	public function hapus_tahap_kegiatan_tree($kegiatan_id, $urut){
		$data = $this->db->query ("select * from db_ptiik_event.tbl_kegiatan_tahap where kegiatan_id='$kegiatan_id' and parent_id='$urut'");
		if(!$data){}
		else
			foreach ($data as $key => $select) {
				if(!$select) continue ;
				else{
					$this->hapus_tahap_kegiatan_tree($kegiatan_id, $select->urut);
					$this->db->query ("delete from db_ptiik_event.tbl_kegiatan_tahap where tahap_id='".$select->tahap_id."'");
				}
			}
	}
	//tahap nilai
	public function get_nextid_kegiatan_nilai(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(komponen_nilai_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_kegiatan_komponen_nilai"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_nexturut_nilai ($parent_id,$kegiatan_id){
		if($parent_id==0)
			$sql = "SELECT IFNULL(MAX(urut),0)+1 as next from db_ptiik_event.tbl_kegiatan_komponen_nilai where parent_id='$parent_id' and kegiatan_id='$kegiatan_id'";
		else
			$sql = "SELECT IF(urut LIKE '%.%',max(substring_index(urut, '.',-1))+1, 1) as next from db_ptiik_event.tbl_kegiatan_komponen_nilai where parent_id='$parent_id' and kegiatan_id='$kegiatan_id'";
		$getRow = $this->db->getRow($sql);
		return ($parent_id==0)?$getRow->next:$parent_id.".".$getRow->next;
	}
	public function get_kegiatan_nilai($komponen_nilai_id){
		$sql = "select komponen_nilai_id, mid(md5(komponen_nilai_id),6,6) as komponen_nilaiid, kegiatan_id, judul, keterangan, is_aktif, urut, parent_id from db_ptiik_event.tbl_kegiatan_komponen_nilai where komponen_nilai_id='$komponen_nilai_id'";
		return $this->db->getRow($sql);
	}
	public function get_kegiatan_nilaiDet($kegiatanid){
		$sql = "select komponen_nilai_id, mid(md5(komponen_nilai_id),6,6) as komponen_nilaiid, kegiatan_id, judul, keterangan, is_aktif, urut, parent_id from db_ptiik_event.tbl_kegiatan_komponen_nilai where mid(md5(kegiatan_id),6,6)='$kegiatanid'";
		return $this->db->query($sql);
	}
	public function simpan_komponen_nilai_kegiatan($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_kegiatan_komponen_nilai',$data);
	}
	public function hapus_komponen_nilai_kegiatan($komponen_nilai_id){
		$komponen_nilai = $this->get_kegiatan_nilai($komponen_nilai_id);
		$this->hapus_komponen_nilai_kegiatan_tree($komponen_nilai->kegiatan_id, $komponen_nilai->urut);
		$this->db->query("delete from db_ptiik_event.tbl_kegiatan_komponen_nilai where komponen_nilai_id='$komponen_nilai_id'");
	}
	public function hapus_komponen_nilai_kegiatan_tree ($kegiatan_id, $urut){
		$data = $this->db->query ("select * from db_ptiik_event.tbl_kegiatan_komponen_nilai where kegiatan_id='$kegiatan_id' and parent_id='$urut'");
		if(!$data){}
		else
			foreach ($data as $key => $select) {
				if(!$select) continue ;
				else{
					$this->hapus_komponen_nilai_kegiatan_tree($kegiatan_id, $select->urut);
					$this->db->query ("delete from db_ptiik_event.tbl_kegiatan_komponen_nilai where komponen_nilai_id='".$select->komponen_nilai_id."'");
				}
			}
	}
	//peserta tahap
	public function get_nextid_peserta_tahap(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(peserta_tahap_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_peserta_tahap"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_kegiatan_tahap_peserta($peserta_tahap_id){
		$sql = "select tbl_kegiatan_tahap.keterangan, tbl_kegiatan_tahap.tahap_id,
			tbl_peserta_tahap.peserta_tahap_id, tbl_peserta_tahap.catatan, tbl_peserta_tahap.is_aktif
			from db_ptiik_event.tbl_kegiatan_tahap, db_ptiik_event.tbl_peserta_tahap 
			where tbl_peserta_tahap.tahap_id=tbl_kegiatan_tahap.tahap_id
			and tbl_peserta_tahap.peserta_tahap_id='$peserta_tahap_id'";
		return $this->db->getRow($sql);
	}
	public function get_kegiatan_tahap_pesertaDet($pesertaid){
		$sql = "select tbl_kegiatan_tahap.keterangan, tbl_kegiatan_tahap.tahap_id,
			tbl_peserta_tahap.peserta_tahap_id, tbl_peserta_tahap.catatan, tbl_peserta_tahap.is_aktif
			from db_ptiik_event.tbl_kegiatan_tahap, db_ptiik_event.tbl_peserta_tahap 
			where tbl_peserta_tahap.tahap_id=tbl_kegiatan_tahap.tahap_id
			and mid(md5(tbl_peserta_tahap.peserta_id),6,6)='$pesertaid'";
		return $this->db->query($sql);
	}
	public function simpan_tahap_peserta($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_tahap',$data);
	}
	public function hapus_tahap_peserta($peserta_tahap_id){
		$this->db->query ("delete from db_ptiik_event.tbl_peserta_tahap where peserta_tahap_id='$peserta_tahap_id'");
	}
	//peserta nilai
	public function get_nextid_peserta_nilai(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(nilai_peserta_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_peserta_nilai"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	public function get_peserta_nilai($nilai_peserta_id){
		$sql = "select tbl_peserta_nilai.nilai, tbl_peserta_nilai.uraian, tbl_peserta_nilai.nilai_peserta_id,
				tbl_peserta_nilai.dinilai_oleh, tbl_peserta_nilai.karyawan_id, tbl_peserta_nilai.peserta_tahap_id, 
				tbl_peserta_nilai.komponen_nilai_id,  tbl_kegiatan_tahap.keterangan as keteranganTahap, 
				tbl_peserta_tahap.catatan, tbl_kegiatan_komponen_nilai.judul, 
				tbl_kegiatan_komponen_nilai.keterangan as keteranganKomponenNilai
				from db_ptiik_event.tbl_kegiatan_tahap, db_ptiik_event.tbl_peserta_tahap, db_ptiik_event.tbl_peserta_nilai, db_ptiik_event.tbl_kegiatan_komponen_nilai
				where tbl_kegiatan_tahap.tahap_id=tbl_peserta_tahap.tahap_id
				and tbl_peserta_tahap.peserta_tahap_id=tbl_peserta_nilai.peserta_tahap_id 
				and tbl_peserta_nilai.komponen_nilai_id=tbl_kegiatan_komponen_nilai.komponen_nilai_id
				and tbl_peserta_nilai.nilai_peserta_id='$nilai_peserta_id'";
		return $this->db->getRow($sql);
	}
	public function get_peserta_nilaiDet($pesertaid){
		$sql = "select tbl_peserta_nilai.nilai, tbl_peserta_nilai.uraian, tbl_peserta_nilai.nilai_peserta_id,
				tbl_peserta_nilai.dinilai_oleh, tbl_peserta_nilai.karyawan_id, tbl_peserta_nilai.peserta_tahap_id,
				tbl_peserta_nilai.komponen_nilai_id,  tbl_kegiatan_tahap.keterangan as keteranganTahap, 
				tbl_peserta_tahap.catatan, tbl_kegiatan_komponen_nilai.judul, 
				tbl_kegiatan_komponen_nilai.keterangan as keteranganKomponenNilai
				from db_ptiik_event.tbl_kegiatan_tahap, db_ptiik_event.tbl_peserta_tahap, db_ptiik_event.tbl_peserta_nilai, db_ptiik_event.tbl_kegiatan_komponen_nilai
				where tbl_kegiatan_tahap.tahap_id=tbl_peserta_tahap.tahap_id
				and tbl_peserta_tahap.peserta_tahap_id=tbl_peserta_nilai.peserta_tahap_id 
				and tbl_peserta_nilai.komponen_nilai_id=tbl_kegiatan_komponen_nilai.komponen_nilai_id
				and mid(md5(tbl_peserta_tahap.peserta_id),6,6)='$pesertaid'";
		return $this->db->query($sql);
	}
	public function simpan_peserta_nilai($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_nilai',$data);
	}
	public function hapus_peserta_nilai($nilai_peserta_id){
		$this->db->query ("delete from db_ptiik_event.tbl_peserta_nilai where nilai_peserta_id='$nilai_peserta_id'");
	}
}
?>
	