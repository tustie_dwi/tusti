$(document).ready(function() {
	$('.e9').select2();
	
	var content_fakultas_id = $('#select_fakultas').val();
	var content_unit_id		= $('#unit-select').val();
	get_unit(content_fakultas_id,content_unit_id);
	//alert(content_unit_id);
	
	if($('[name="content-upload"]').length){
		$('[name="content-upload"]').datetimepicker({
		  pickTime : true,
		  format : 'YYYY-MM-DD'
		});
	}
	
	
	if($('#identifier').val()=='content'){
		var filledtag = $('.tag-result').text().split(',');
	
		if(filledtag){
			jQuery("#tag").tagsManager({
		      prefilled: filledtag
		    });
		}else{
			jQuery("#tag").tagsManager();
		}
		
		var category = $('#select_category option:selected').data('category');
		
		if(category=='content'){
			$('.language-box').hide();
			$('#parent-form-group').show();
		}
		else if(category=='menu'){
			$('.language-box').hide();
			$('#parent-form-group').show();
		}
		else if(category=='upload'){
			$('.language-box').show();
			$('#parent-form-group').hide();
		}
		
	}

	// $('.e5').select2({ minimumInputLength: 1});
	
	if($('#lang-select').val()){
		var lang_select	= $('#lang-select').val();
	}else var lang_select = 0;
	
	$('#select_lang').select2().select2('val',lang_select);
	
	
	if(($('#unit-select').val()).length>0){
		var unit__id = $('#unit-select').val();
	}else var unit__id = 0;
	
	setTimeout(function(){
		$('#select_unit').select2().select2('val',unit__id);
	}, 500);
	
	//alert($('#unit-select').val());
	// $('#select_unit').val(unit__id);
	
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/content/read_unit",
      data : $.param({
		fakultasid : $('#select_fakultas').val(),
		unit_id : $('#unit-select').val()
	}),
       success: function(data){
	       $('#select_unit').html(data);
       }
    });
	
	/*
	 * START COMS_CONTENT========================== 
	 * */
	
	var content_fakultas_id = $('#select_fakultas').val();
	var content_unit_id		= $('#unit-select').val();
	get_unit(content_fakultas_id,content_unit_id);
	//alert(content_unit_id);
	
	var content_category = $('#select_category').val();
	if(content_category!='0'){
		var content_parent_id	= $('#content-parent-select').val();
		var content_content_id	= $('#content-content-id').val();
		
		var lang = Array();
		if(category!='upload'){
			$('input[name="lng[]"]:checked').each(function() {
			   lang.push(this.value);
			});
		}else lang			 = null;
		
		if(content_unit_id!=0){
			var unit_id_ = content_unit_id;
		}else{
		var unit_id_ = $('#select_unit').val();
		}
		
		get_content_parent(content_category,content_parent_id,content_content_id,lang,unit_id_);
	}
	
	var grup_slide	= $('#file-group-selected').val();
	$('#select_file_group').val(grup_slide);
	$('#select_file_group').select2().select2('val',grup_slide);
	
	/*
	 * END COMS_CONTENT========================== START COMS_FILE
	 * */
});

function delete_(e, param, id){
	var x = confirm('Yakin Hapus Data ini?');
	if(x){
		if(param=='post_content'){
			delete_content(e, id);
		}
		else if(param=='file'){
			delete_file(e, id);
		}
	}
}

function delete_tag(e, contentid, tag){
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/content/delete_tag",
      data : $.param({
		contentid : contentid,
		tag	: tag
	}),
       success: function(data){
	      $(e).parent().remove();
       }
    });
}

/*
 * SETTING===================================================
 * */

function edit_category(content_category, category, note){
	$('#category').val(category);
	$('#category').select2().select2('val',category);
	$('#content').val(content_category);
	$('#note').val(note);
	
	$('input[name="hidcategory"]').val(category);
	$('input[name="hidcontent"]').val(content_category);
	
	$('.category-content-cancel').show();
}

$("#b_content_category").click(function(e){
	var category = $('#category').val();
	var content = $('#content').val().length;
	var note = $('#note').val().length;

	if(category != 0 && content > 0 && note > 0){
	  var postData = new FormData($('#form-setting')[0]);
		$.ajax({
			url : base_url + "module/master/content/save_Setting",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				if(data=='sukses'){
					alert('Proses Simpan Berhasil');
				}else alert ('Proses Simpan Gagal!');
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Proses Simpan Gagal!');      
			    },
			    cache: false,
				contentType: false,
				processData: false
		  });
		e.preventDefault(); //STOP default action
		return false;
	}
	else{
		alert("Lengkapi Data Anda");
	}
});

/*
 * END SETTING========================== START COMS_CONTENT
 * */

$("#select_unit").change(function() {
	var category = $('#select_unit option:selected').data('category');
	$('#index-category').val(category);
	$("#param").submit(); //SUBMIT FORM
});

$("#select_lang").change(function() {
	var category = $('#select_category option:selected').data('category');
	$('#index-category').val(category);
	$("#param").submit(); //SUBMIT FORM
});

$("#select_category").change(function() {
	var category = $('#select_category option:selected').data('category');
	$('#index-category').val(category);
	$("#param").submit(); //SUBMIT FORM
});


$('#select_fakultas').change(function(){
	var content_fakultas_id = $(this).val();
	var content_unit_id		= $('#unit-select').val();
	get_unit(content_fakultas_id,content_unit_id);
});

function view_attach(title, type, note, file_loc){
	var output = '';
	$('#attach-modal-title').html('<b>'+title+'</b>');
	
	if(type=='document'||type=='presentation'||type=='spreadsheet'){
		output = '<iframe src="https://docs.google.com/gview?url='+file_loc+'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
		$('#attach-modal-content').html(output);
	}
	else if(type=='image'){
		output = '<img src="'+file_loc+'" width="480" height="360" class="img-thumbnail" />';
		$('#attach-modal-content').html(output);
	}
	
	$('#attach-modal-note').text(note);
	$('#attach-modal').modal('show');
}

$('input[name="lng[]"]').change(function() {
	var content_category = $('#select_category').val();
	var category = $('#select_category option:selected').data('category');
	
	var lang = Array();
	//if(category!='upload'){
		$('input[name="lng[]"]:checked').each(function() {
		   lang.push(this.value);
		});
	//}else lang			 = null;
	
	var unit_id_ = $('#select_unit').val();
	get_content_parent(content_category,'','',lang,unit_id_);
});


$('#select_unit').change(function(){
	var content_category = $('#select_category').val();
	var category = $('#select_category option:selected').data('category');
	
	var lang = Array();
	if(category!='upload'){
		$('input[name="lng[]"]:checked').each(function() {
		   lang.push(this.value);
		});
	}else lang			 = null;
	
	var unit_id_ = $(this).val();
	get_content_parent(content_category,'','',lang,unit_id_);
});

$('#select_category').change(function(){
	var content_category = $(this).val();
	var category = $('#select_category option:selected').data('category');
	
	var lang = Array();
	if(category!='upload'){
		$('input[name="lng[]"]:checked').each(function() {
		   lang.push(this.value);
		});
	}else lang			 = null;

	var unit_id_ = $('#select_unit').val();
	// alert(unit_id_);
	get_content_parent(content_category,'','',lang,unit_id_);
	
	
	// $('#index-category').val(category);
	if(category=='content'){
		$('.language-box').hide();
		$('.content-form').show();
		$('#contentexcerpt-form-group').show();
		$('#comment-sticky-form-group').show();
		$('.file-form').hide();
		$('#urut-form-group').show();
		if(content_category=='news'||content_category=='pengumuman'){
			$('#parent-form-group').hide();
		}else $('#parent-form-group').show();
		
	}
	else if(category=='menu'){
		$('.language-box').hide();
		$('.content-form').show();
		$('#urut-form-group').show();
		$('#parent-form-group').show();
		$('#comment-sticky-form-group').hide();
		$('.file-form').hide();
		$('#contentexcerpt-form-group').hide();
	}
	else if(category=='upload'){
		$('.language-box').show();
		$('.file-form').show();
		$('.content-form').hide();
		$('#parent-form-group').show();
	}
	
});

function get_content_parent(content_category,content_parent_id,content_content_id,lang,unitid){
	//alert(content_parent_id);
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/content/read_content",
      data : $.param({
		content_category : content_category,
		content_parent_id : content_parent_id,
		content_content_id : content_content_id,
		lang : lang,
		unitid : unitid
	}),
       success: function(data){
       	//alert(data);
	       $('#select_parent').html(data);
	      /* if(content_parent_id=='' || content_parent_id=='08495d'){
	       	content_parent_id = 0;
	       }*/
	       $('#select_parent').select2().select2('val', content_parent_id);
       }
    });
}

function get_unit(content_fakultas_id,content_unit_id){
	
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/content/read_unit",
      data : $.param({
		fakultasid : content_fakultas_id,
		unit_id : content_unit_id
	}),
       success: function(data){
		//alert(data);
	       $('#select_unit').html(data);
	       $('#select_unit').select2().select2('val', unit_id);
       }
    });
}

function delete_content(e, contentid){
	var parent = $(e).parent().parent().parent().parent().parent().parent();
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/content/delete_content",
      data : $.param({
		contentid : contentid
	}),
       success: function(data){
	      if(data=='sukses'){
	      	alert('Data berhasil dihapus!');
	      	parent.remove();
	      }else if(data=='gagal'){
	      	alert('Data gagal dihapus!');
	      }
	      // else{
	      	// alert(data);
	      // }
       }
    });
}

$('#btn-add-file').click(function(){
	var parent = $(this).parents('.form-group');
	var attach_group = parent.find('.attach-form-group');
	var output = '';
	
	output += '<div class="form-group">';
	output += ' <blockquote>';
	output += '		<input name="attach-name[]" class="form-control" type="text" placeholder="Judul">';
	output += '		<input name="attach-file[]" class="form-control attach-file" type="file" accept="application/msexcel,application/pdf,application/msword">';
	output += '		<br><textarea name="attach-note[]" class="form-control"></textarea>';
	output += '		<a class="btn btn-danger pull-right" onclick="delete_file_attach(this,\'\')">Delete</a>';
	output += ' </blockquote>';
	output += '</div>';
	
	attach_group.append(output);
});

function delete_file_attach(e,val){
	if(val!=''){
		var x = confirm('Yakin Hapus Data?');
		if(x){
			$.ajax({
			  type : "POST",
			  dataType: "HTML",
		      url: base_url + "/module/master/content/delete_content_upload",
		      data : $.param({
				uploadid : val
			}),
		       success: function(data){
			   	  if(data=='sukses'){
			    		alert('Data Berhasil Dihapus!');
			       		$(e).parent().remove();
			       }else{
			       		alert('Data Gagal Dihapus!');
			       }
			   }
		    });
		}
	}else{
		var parent = $(e).parent().parent();
		parent.remove();
	}
}

$("#btn-publish").click(function(e){
	var fakultas_id = $('#select_fakultas').val();
	var cabang_id	= $('#select_cabang').val();
	var category	= $('#select_category').val();
	var judul		= $('#title').val();
	
	if(fakultas_id!=0 && cabang_id!=0 && category!=0 && judul.length!=0){
		submit_coms_content('publish',e);
	}else{
		alert('Lengkapi data anda!');
	}
});

$("#btn-draft").click(function(e){
	var fakultas_id = $('#select_fakultas').val();
	var cabang_id	= $('#select_cabang').val();
	var category	= $('#select_category').val();
	var judul		= $('#title').val();
	
	if(fakultas_id!=0 && cabang_id!=0 && category!=0 && judul.length!=0){
		submit_coms_content('draft',e);
	}else{
		alert('Lengkapi data anda!');
	}
});

function submit_coms_content(status,e){
	var content = CKEDITOR.instances['content'].getData();
	$("#content").val(content);
	
	var content_en = CKEDITOR.instances['content_en'].getData();
	$("#content_en").val(content_en);
	
	var postData = new FormData($('#form-content')[0]);
	postData.append("status", status);
	$.ajax({
		url : base_url + "module/master/content/save_coms_content",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			// alert(data);
			if(data=='sukses'){
				alert('Proses Simpan Berhasil');
				location.reload();
			}else if(data=='wrong type'){
				alert ('Jenis File Thumbnail Salah!');
			}else if(data=='wrong attach type'){
				alert ('Jenis File Attach Salah!');
			}else{
				if(data=='gagal'){
					alert ('Proses Simpan Gagal!');
				}else if(data.length>104){ //rename eror but succes!
					alert('Proses Simpan Berhasil');
					location.reload();
				}else{
					alert ('Proses Simpan Gagal!');
				}
			}
			// location.reload();
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
			// alert(errorThrown);
		    alert ('Proses Simpan Gagal!');      
		    },
		    cache: false,
			contentType: false,
			processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
}

/*
 * END COMS_CONTENT========================== START COMS_FILE
 * */

function delete_file(e, fileid){
	var parent = $(e).parent().parent().parent().parent().parent().parent();
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/content/delete_file",
      data : $.param({
		fileid : fileid
	}),
       success: function(data){
	      if(data=='sukses'){
	      	alert('Data berhasil dihapus!');
	      	parent.remove();
	      }else if(data=='gagal'){
	      	alert('Data gagal dihapus!');
	      }
	      // else{
	      	// alert(data);
	      // }
       }
    });
}

$("#btn-file-publish").click(function(e){
	var filetitle 		= $('#filetitle').val();
	var file			= $('#file-file').val();
	var hid_file_img	= $('#hid-file-img').val();
	
	if(filetitle.length!=0 && file.length!=0 ){
		submit_coms_upload('publish',e);
	}else{
		if(hid_file_img){
			submit_coms_upload('publish',e);
		}else{
			alert('Lengkapi data anda!');
		}
	}
});

$("#btn-file-draft").click(function(e){
	var filetitle 	= $('#filetitle').val();
	var file		= $('#file-file').val();
	var hid_file_img = $('#hid-file-img').val();
	
	if(filetitle.length!=0 && file.length!=0 ){
		submit_coms_upload('draft',e);
	}else{
		if(hid_file_img){
			submit_coms_upload('draft',e);
		}else{
			alert('Lengkapi data anda!');
		}
	}
});

function submit_coms_upload(status,e){
	var content = CKEDITOR.instances['filenote'].getData();
	$("#filenote").val(content);
	
	var postData = new FormData($('#form-content')[0]);
	postData.append("status", status);
	$.ajax({
		url : base_url + "module/master/content/save_coms_upload",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			 alert(data);
			if(data=='sukses'){
				alert('Proses Simpan Berhasil');
				location.reload();
			}else if(data=='wrong type'){
				alert ('Jenis File Thumbnail Salah!');
			}else{
				alert ('Proses Simpan Gagal!');
			}
			// location.reload();
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Proses Simpan Gagal!');      
	    },
	    cache: false,
		contentType: false,
		processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
}