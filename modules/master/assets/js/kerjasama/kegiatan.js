$(document).ready(function(){
	// alert();
})

$(".e9").select2();

function tambah(){
	$(".right-pane").hide();
	$("#form-manage").show();
}

function batal(){
	$(".right-pane").hide();
	$("#form-data").show();
}

$(document).on("change", "[name='fakultas']", function(){
	var fak_ = $(this).val();
	
	var uri = base_url + 'module/master/kerjasama/get_instansi_by_fakultas';
	$("#loading").show();
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({fak : fak_}),
        success:function(msg) 
        {
        	$("[name=instansi]").html(msg);
            $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	$("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
});

$(document).on("change", "[name='kategori']", function(){
	var kat = $(this).val();
	
	if(kat == '') $("[kategori]").show();
	else{
		$("[kategori]").hide();
		$("[kategori='"+kat+"']").show();
		$("[name='instansi']").val('');
	}
});

function lihat_detail(){
	var ins_ = $("[name='instansi']").val();
	$(".right-pane").hide();
	$("#form-data").show();
	if(ins_ != ''){
		var uri = base_url + 'module/master/kerjasama/get_detail_instansi';
		$("#loading").show();
		$.ajax({
	        url : uri,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({ins : ins_}),
	        success:function(msg) 
	        {
	        	$("#form-data").html(msg);
	            $("#loading").fadeOut();
	        },
	        error: function(msg) 
	        {
	        	$("#loading").fadeOut();
				alert("Proses gagal, mohon ulangi lagi !!"); 
	        }
	    });
   }
}

$(document).on("click",".btn-edit-kegiatan", function(){
	$(".right-pane").hide();
	$("#form-manage").show();
	
	$("[name=id]").val($(this).data("id"));
	$("[name=judul]").val($(this).data("judul"));
	$("[name=tgl_mulai]").val($(this).data("tgl_mulai"));
	$("[name=tgl_selesai]").val($(this).data("tgl_selesai"));
	$("[name=lokasi]").html($(this).data("lokasi"));
	$("[name=keterangan]").html($(this).data("keterangan"));
	
	var tes = [];
	
	$.each($(this).data("ruang").split(",") ,function(key, value){
		tes.push(value);
	});
	
	$("#ruang").select2("val", tes);
	$("#instansi").select2("val", $(this).data("instansi"));
});

function check_tgl(){
	if($("[name='tgl_selesai']").val() != '') {
		var start = new Date($("[name='tgl_mulai']").val());
		var end = new Date($("[name='tgl_selesai']").val());

		if(end < start) $("[name='tgl_selesai']").val('');
	}
}

$(function () {
    $('.pick-date').datetimepicker({
        pickDate: true,
        pickTime: false
    });
});


$(function () {
    $('.pick-time').datetimepicker({
        pickDate: false
    });
});














