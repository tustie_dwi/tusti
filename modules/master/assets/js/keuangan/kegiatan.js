var templateForm = null;
var isEditBayar = false;
var elEditBayar = null;
$(document).ready(function() {
	templateForm = $('#form-detail-keu').clone();
	$('#form-detail-keu').remove();
	$(".date").datepicker({
		dateFormat : 'dd-mm-yy',
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true
	});
	$(".namaKegiatan").autocomplete({
		source : base_url + "/module/master/keuangan/agenda_search",
		minLength : 0,
		select : function(event, ui) {
			$.ajax({
				type : "POST",
				url : base_url + "module/master/keuangan/get_agenda_peserta",
				data : $.param({
					agendaid : ui.item.agendaid
				}),
				success : function(data, textStatus, jqXHR) {
					$('#peserta-keu').empty();
					$.each(JSON.parse(data), function(ind, value) {
						var template = templateForm.clone();
						template.find('#nama').val(value.nama);
						template.find('#karyawan_id').val(value.karyawan_id);
						template.find('#satuan').val("0");
						template.find('#qty').val("0");
						template.find('#total').val("0");
						$('#peserta-keu').append(template);
					});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert('Load Peserta Agenda Gagal');
				}
			});
		}
	});
	$(".namaKaryawan").autocomplete({
		source : base_url + "/module/master/keuangan/karyawan_search",
		minLength : 0,
		select : function(event, ui) {
			$(this).parent().find('#karyawan_id').val(ui.item.karyawan_id);
		}
	});
	
	if($("#agenda_kegiatan").children().length==0){
		$('#panel_keu').hide();
	}
	else{
		$('#form-keu').hide();
	}
	$(".table").DataTable();
})
$(document).on('keyup', '.satuan', function() {
	var qty = $(this).parent().parent().find(".qty").val();
	if (qty == '') {
		$(this).parent().parent().find(".qty").val(0);
		qty = 0;
	}
	var satuan = $(this).val();
	$(this).parent().parent().find(".total").val((qty * satuan));
});
$(document).on('keyup', '.qty', function() {
	var qty = $(this).val();
	var satuan = $(this).parent().parent().find(".satuan").val();
	if (satuan == '') {
		$(this).parent().parent().find(".satuan").val(0);
		satuan = 0;
	}
	$(this).parent().parent().find(".total").val((qty * satuan));
});
$(document).on('keyup', '.total', function() {
	var qty = $(this).parent().parent().find(".qty").val();
	if (qty == '') {
		$(this).parent().parent().find(".qty").val(0);
		qty = 0;
	}

	var satuan = $(this).parent().parent().find(".satuan").val();
	if (satuan == '') {
		$(this).parent().parent().find(".satuan").val(0);
		satuan = 0;
	}
	$(this).val((qty * satuan));
});
function deleteKeu(bayarid, el){
	$.ajax({
	  	type: "POST",
	  	url: base_url+"module/master/keuangan/delete_keu_bayar",
	  	data: $.param({bayarid:bayarid}),
	  	success:function(data, textStatus, jqXHR) {
	  		$(el).closest('tr').remove();
	  		
	  	}
	});
}
function editPesertaKeu(bayar_id, el){
	$('#panel_keu').slideUp('fast');
	$('#panel-peserta-keu').slideDown('fast');
	$("#bayar_id").val(bayar_id);
	$.ajax({
		type: "POST",
		url: base_url+"module/master/keuangan/get_keu_peserta",
		data: $.param({bayar_id:bayar_id}),
		success:function(data, textStatus, jqXHR) {
			if (data != null && data != ""){ 
				$.each(JSON.parse(data),function (index, value){
					var template = templateForm.clone();
					template.find("#nama").val(value.nama);
					template.find("#karyawan_id").val(value.karyawan_id);
					template.find("#hr_id").val(value.hr_id);
					template.find("#keterangan_detail").val(value.keterangan);
					template.find("#satuan").val(value.satuan);
					template.find("#qty").val(value.qty);
					template.find("#total").val(value.qty*value.satuan);
					template.find(".bayar_id_detail").val(value.bayar_id);
					$('#peserta-keu').prepend(template);
				});
			}
		}
	});
	$(".namaKaryawan").autocomplete({
		source : base_url + "/module/master/keuangan/karyawan_search",
		minLength : 0,
		select : function(event, ui) {
			$(this).parent().find('#karyawan_id').val(ui.item.karyawan_id);
		}
	});
}
function editKeu(el){
	var value = JSON.parse(Base64.decode($(el).data('param')));
	$('#kategori_bayar').val(value.kategori_bayar);
	$('#keterangan').val(value.keterangan);
	var t = value.tgl_mulai.split(/[- :]/);
	$('#tgl_mulai').val(t[2]+"-"+t[1]+"-"+t[0]);
	var t = value.tgl_selesai.split(/[- :]/);
	$('#tgl_selesai').val(t[2]+"-"+t[1]+"-"+t[0]);
	var t = value.tgl_bayar.split(/[- :]/);
	$('#tgl_bayar').val(t[2]+"-"+t[1]+"-"+t[0]);
	$('#bayar_id').val(value.bayar_id);
	$('#bayar_id').removeAttr("disabled");
	
	$('#panel_keu').slideUp('fast');
	$('#form-keu').slideDown('fast');
	isEditBayar = true;
	elEditBayar = el;
}
function tambahKegiatanKeu(){
	$('#panel_keu').slideUp('fast');
	$('#form-keu').slideDown('fast');
	
	$('#bayar_id').attr('disabled',"");
	$("#form-keu")[0].reset();
	
}
function submitKeuBayar (){
	var cekvalid = true;
	$('#form-keu :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
		  	type: "POST",
		  	url: base_url+"module/master/keuangan/submit_keu_bayar",
		  	data: $("#form-keu").serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var value = JSON.parse(data);
		  		$('.bayar_id_detail').val(value.bayar_id);
		  		$('#bayar_id').val(value.bayar_id);
		  		$('#agenda_kegiatan').prepend("<tr>"+
					"<td>"+
						"<a class='btn btn-danger btn-xs pull-right' onclick='deleteKeu(\""+value.bayarid+"\",this)'><i class='fa fa-trash-o'></i> delete</a>"+					
						"<a class='btn btn-warning btn-xs pull-right' data-param='"+Base64.encode(data)+"' onclick='editKeu(this)'><i class='fa fa-trash-o'></i> edit</a>"+
						"<a class='btn btn-primary btn-xs pull-right' onclick='editPesertaKeu(\""+value.bayar_id+"\",this)' style='margin:0px 3px'><i class='fa fa-users'></i> peserta</a>"+
						"<a class='btn btn-default btn-xs pull-right' style='margin:0px 3px'><i class='fa fa-print'></i> Cetak</a>"+
						"<p>"+value.keterangan+"</p>"+
						"Periode : "+value.periode+" : <i class='fa fa-calendar'></i> "+value.tgl_mulai+" - <i class='fa fa-calendar'></i> "+value.tgl_selesai+
					"</td></tr>");
				$("#form-keu")[0].reset();
				if(isEditBayar){
					$(elEditBayar).closest("tr").remove();
					$('#form-keu').slideUp('fast');
					$('#panel_keu').slideDown('fast');
				}else{
					$('#form-keu').slideUp('fast');
					$('#panel-peserta-keu').slideDown('fast');
				}
				$('#bayar_id').attr('disabled',"");
				isEditBayar = false;
		  	},
		 	error: function(jqXHR, textStatus, errorThrown) {
			    alert ('Proses Simpan Gagal!');      
			}
		});
	}
}
function cancelkeuBayar(){
	$('#form-keu').slideUp('fast');
	$('#panel_keu').slideDown('fast');
	
	$('bayar_id').attr("disabled","");
	isEditBayar = false;
	$("#form-keu")[0].reset();
}

function tambahPesertaKeu (){
	var form = null;
	$('#peserta-keu form').each(function (){form = $(this);});
	if (form == null || form.find('input').val() != ""){
		var template = templateForm.clone();
		$('#peserta-keu').append(template);
		$('.bayar_id_detail').val( $('#bayar_id').val() );
		$(".namaKaryawan").autocomplete({
			source : base_url + "/module/master/keuangan/karyawan_search",
			minLength : 0,
			select : function(event, ui) {
				$(this).parent().find('#karyawan_id').val(ui.item.karyawan_id);
			}
		});
		var form = null;
		$('#peserta-keu form').each(function (){form = $(this);});
		form.find('#nama').focus();
	}
}
function tambahOnEnter(event){
	console.log(event);
	if (event.keyCode == 13){
       tambahPesertaKeu();
    }
}
function hapusKeuPeserta(el){
	var form = $(el).closest('form');
	console.log(form.find('#hr_id'));
	console.log(form);
	$.ajax({
		type: "POST",
	 	url: base_url+"module/master/keuangan/delete_keu_peserta",
	 	data:$.param({ hr_id:form.find('#hr_id').val()}),
	  	success:function(data, textStatus, jqXHR) {
	  	}
	});
	form.remove();
}
function finishKeuPeserta(){
	$.ajax({
		type: "POST",
	 	url: base_url+"module/master/keuangan/delete_all_keu_peserta",
	 	data:$.param({ bayar_id:$('#bayar_id').val() }),
	  	success:function(data, textStatus, jqXHR) {
	  	}
	});
	$('#peserta-keu form').each(function (){
	  	var form = $(this);
	  	var cekvalid = true;
		form.find('input').each(function(){
			$(this).parent().parent().addClass('has-success');
			$(this).parent().parent().removeClass('has-error');
			if(!this.validity.valid){
			    $(this).focus();
			    $(this).parent().addClass('has-error');
			 	cekvalid = false;
			}else{
			  	$(this).parent().parent().addClass('has-success');
			}
		});
		if(cekvalid){
			$.ajax({
				type: "POST",
			  	url: base_url+"module/master/keuangan/submit_keu_peserta",
			  	data: form.serialize(),
			  	success:function(data, textStatus, jqXHR) {}
			});
		}else{
			form.remove();
		}
	});
}
function cancelKeuPeserta(){
	$('#panel-peserta-keu').slideUp('fast');
	$('#panel_keu').slideDown('fast');
	
	$('#peserta-keu').empty();
}

function addKategoriBayar(){
	var cekvalid = true;
	$('#form_kategori_bayar :input[required="required"]').each(function(){
		$(this).parent().addClass('has-success');
		$(this).parent().removeClass('has-error');
		if(!this.validity.valid)
		{
		   $(this).focus();
		   $(this).parent().addClass('has-error');
		   cekvalid = false;
		}else{
		  	$(this).parent().addClass('has-success');
		}
	});
	if(cekvalid){
		$.ajax({
			type: "POST",
		  	url: base_url+"module/master/keuangan/submit_kategori_bayar",
		  	data: $('#form_kategori_bayar').serialize(),
		  	success:function(data, textStatus, jqXHR) {
		  		var value = JSON.parse(data);
		  		if (value.kelompok=='non') $('#kategori_bayar').prepend("<option value='"+value.kategori_bayar+"'>"+value.keterangan+"</option>");
		  		$('#list-kategori-bayar').prepend("<li class='list-group-item'>"+
					"<a class='btn btn-sm btn-danger pull-right' onclick='deleteKategoriBayar(\""+value.kategori_bayar+"\",this)'><i class='fa fa-trash-o'></i></a>"+
        			"<h4 class='list-group-item-heading'>"+value.keterangan+"</h4>"+
        			"<p class='list-group-item-text'>"+value.kategori_bayar+"</p>"+
        		"</li>");
        		
		  	}
		});
	}
}
function deleteKategoriBayar(kategori_bayar, el){
	$.ajax({
		type: "POST",
		url: base_url+"module/master/keuangan/delete_kategori_bayar",
		data: $.param({kategori_bayar:kategori_bayar}),
		success:function(data, textStatus, jqXHR) {
			$('#kategori_bayar option[value="'+kategori_bayar+'"]').remove();
			$(el).parent().remove();
		}
	});
}
