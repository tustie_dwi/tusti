$(document).ready(function(){
	$("select[name='category_index']").change(function(){
		$("#form_category_comment").submit();
	});
});

function doDelete(i){
	var x = confirm("Are you sure you want to delete?");
 	if (x){
  		var del_id = i;
  		var url = base_url + 'module/content/page/delete_comments';
	  	$.ajax({
			type : "POST",
			dataType : "html",
			url : url,
			data : $.param({
				id : del_id
			}),
			success : function(msg) {
				if (msg) {
					alert(msg);
					$("#comment"+i).fadeOut();
				} else {
					alert(msg);
				}
			}
		});
	}
  else {
  	location.reload();
  }
};

function updateStatus(i,status,category){
	var url = base_url + 'module/content/page/update_status_comments';
  	$.ajax({
			type : "POST",
			dataType : "html",
			url : url,
			data : $.param({
				id : i,
				status_id : status
			}),
			success : function(msg) {
				if (msg) {
					alert(msg);
					var form = document.createElement("form");
					var input = document.createElement("input");
					
					form.action = base_url + 'module/content/page/comments';
					form.method = "post"
					
					input.name = "category_index";
					input.value = category;
					form.appendChild(input);
					
					document.body.appendChild(form);
					form.submit();
				} else {
					alert(msg);
					var form = document.createElement("form");
					var input = document.createElement("input");
					
					form.action = base_url + 'module/content/page/comments';
					form.method = "post"
					
					input.name = "category_index";
					input.value = category;
					form.appendChild(input);
					
					document.body.appendChild(form);
					form.submit();
				}
			}
	});
};
