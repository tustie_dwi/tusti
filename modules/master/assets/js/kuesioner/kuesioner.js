$(document).ready(function() {
	$('.e9').select2();
	
	$('.date').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
	
	$("#form-rancangan-mk").submit(function(e){
		var judul 	= $("input[name='judul']").val();
		var ket		= $("texarea[name='keterangan']").val();
		
		if(judul!=='0' && ket!=='-'){
			var postData = new FormData($(this)[0]);
			$.ajax({
					url : base_url + "module/master/kuesioner/save",
					type: "POST",
					data : postData,
					async: false,
					success:function(msg) 
					{
						alert(msg);
					    window.location.href = base_url + "module/master/kuesioner";
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
					    alert ('Proses Gagal!');      
					    // location.reload(); 
					},
				    cache: false,
					contentType: false,
					processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
});

function edit(id){
	var form = document.createElement("form");
	var input = document.createElement("input");
	
	form.action = window.location.href;
	form.method = "post"
	
	input.name = "id";
	input.value = id;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
}
