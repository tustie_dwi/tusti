$(document).ready(function(){
	var mhs = '';
});

$(document).on('change','[name=fakultas]',function(){
	var fakultas_ = $("[name=fakultas]").val();
	var id = fakultas_.split("|");
	
	var uri = base_url + 'module/master/krs/get_prodi';
	$("#loading").show();
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({fakultas : id[1]}),
        success:function(msg) 
        {
        	$("[name=prodi]").html(msg);
            $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	$("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
});

function get_mhs(nilai){
	var isi = '';
	var count = 0;
	$(".table-data tbody").html('');
	$.each(mhs, function(i, f){
		if(count < 13){
			if(nilai == '') {
				$(".table-data tbody").append(create_row(f.mhs_id, f.nama,f.nim,f.angkatan,f.prodi,f.gender));
				count++;
			}
			else if(f.nama.toLowerCase().indexOf(nilai.toLowerCase()) >= 0 || f.nim.toLowerCase().indexOf(nilai.toLowerCase()) >= 0) {
				$(".table-data tbody").append(create_row(f.mhs_id, f.nama,f.nim,f.angkatan,f.prodi,f.gender));
				count++;
			}
		}
	});
	
	if(count == 0) $(".table-data tbody").html('<tr><td class="text-center" colspan="4">Daftar Kosong</td></tr>');
}

function tampil_krs(){
	$("#krs-box").show();
	$("#mhs-box").hide();
}

function tampil_mhs(){
	$("#mhs-temp").val('');
	$("#mhs-box").show();
	$("#krs-box").hide();
}

$(document).on('change', '[name=tahun]', function(){
	var thn = $("[name=tahun]").val().split("|");
	var id = $("#mhs-temp").val();
	
	if(id != ''){
		var uri = base_url + 'module/master/krs/get_krs';
		$("#loading").show();
		$.ajax({
	        url : uri,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({mhsid : id, tahun : thn[1]}),
	        success:function(msg) 
	        {
	        	$("#krs-box").html(msg);
	        	$("#tahun-info").html(": " + thn[0]);
	            $("#loading").fadeOut();
	        },
	        error: function(msg) 
	        {
	        	$("#loading").fadeOut();
				alert("Proses gagal, mohon ulangi lagi !!"); 
	        }
	    });
	}
});

$(document).on('click','.mhs-row', function(){
	var id = $(this).data('mhs');
	var thn = $("[name=tahun]").val().split("|");
 	$("#mhs-temp").val(id);
 	
	var uri = base_url + 'module/master/krs/get_krs';
	$("#loading").show();
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({mhsid : id, tahun : thn[1]}),
        success:function(msg) 
        {
        	tampil_krs();
        	$("#krs-box").html(msg);
        	$("#tahun-info").html(": " + thn[0]);
            $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	$("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
});

function create_row(mhsid, nama, nim, angkatan, prodi, gender){
	var str = '<tr style="cursor: pointer" class="mhs-row" data-mhs="'+mhsid+'">';
		str += '<td>'+nama+' <span class="label label-info">'+gender+'</span></td>';
		str += '<td>'+nim+'</td>';
		str += '<td>'+angkatan+'</td>';
		str += '<td>'+prodi+'</td>';
		str += '</tr>';
	return str;
}

$(document).on('change','[name]', function(e){
	$("#loading").show();
	var formData = new FormData($('#form-krs')[0]);
	var URL = base_url + 'module/master/krs/get_mhs';
	
    $.ajax({
	    url : URL,
	    type: "POST",
	    dataType : "json",
	    data : formData,
	    async: false,
	    success:function(msg) 
	    {
	    	// console.log(msg);
	    	if(msg.tipe == 'false') {
	    		$(".table-data tbody").html('<tr><td class="text-center" colspan="4">Daftar Kosong</td></tr>');
	    		mhs = '';
	    	}
	    	else if(msg.tipe == 'kosong') {
	    		$(".table-data tbody").html('<tr><td class="text-center" colspan="4">Daftar Kosong</td></tr>');
	    		mhs = '';
	    	}
	    	else{
	    		mhs = msg;
		    	get_mhs('');
		    }
	       	$("#loading").fadeOut();
	    },
	    error: function(msg) 
	    {
	    	$("#loading").fadeOut();
	        alert("Proses gagal, mohon ulangi lagi !!"); 
	    },
	    cache: false,
	    contentType: false,
	    processData: false
    });
    e.preventDefault(); //STOP default action
});
