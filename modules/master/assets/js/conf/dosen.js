var fakultasid;
var cabangid;
	
$(document).ready(function() {
	$(".tagmanager").tagsManager();
	
	//=====EDIT=====//
	var cek_tugas = $("#tugas_id").length;
	if(cek_tugas !== 0){
		var ket = $("input[name='tugas_']").val();
		var data_push = ket.split(',');
		i=0;
		$.each(data_push , function(){
			jQuery("#tugas_karyawan").tagsManager('pushTag', data_push[i]);
			i++;
		})
	}
	
	$(".e9").select2();
	$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	
	var cek = $("#pend_trakhir").length;
	if(cek !== 0){
		var select = document.getElementById("pend_trakhir");
		var c = $("input[name='pend_trakhir_edit']").val();
	    for(var i = 0;i < select.options.length;i++){
	        if(select.options[i].value == c ){
	           select.options[i].selected = true;
	            $("#pend_trakhir").select2("val", c);
	        }
	    }
    }
	
	$('#check-nip').click(function() {
		if( this.checked ){
			$('#label-nip').html("&nbsp;NIP");
		}else{
			$('#label-nip').html("&nbsp;NIK");
		}
	});
	
	$.each($('input[name="saktif"]'), function(i,f){
		if($(this).is(':checked')){
			var value_ = $(this).val();
	
			if(value_=='keluar'){
				$('.form-group-pensiun').show();
			}else if(value_=='pensiun'){
				$('.form-group-pensiun').show();
			}else{
				$('.form-group-pensiun').hide();
			}
		}
		
	});
	
	$('#email').blur(function() {
		var sEmail = $(this).val();
        if ($.trim(sEmail).length == 0) {
           // alert('Please enter valid email address');
            e.preventDefault();
        }
        if (validateEmail(sEmail)) {
            //alert('Email is valid');
        }
        else {
            alert('Alamat email salah');
            $(this).val('');
            e.preventDefault();
        }
	});
	
	$("select[name='cabang']").change(function(){
		var fakultas	= $("select[name='fakultas']").val();
		var cabang		= $(this).val();
		if(fakultas!="-" && cabang!="-"){
			$.ajax({
				type : 'POST',
				dataType : 'html',
				url : base_url + "module/master/staff/get_ruang_select",
				data : $.param({
							fakultas : fakultas,
							cabang : cabang
						}),
				success : function(msg){
					$("select[name='ruang[]']").html(msg);
				}
			});
		}
		
	});
	
	$("#pilih_cabang").change(function() {
		$("#param").submit(); //SUBMIT FORM
	});
	
	$('#dsn-ttp').click(function() {
		if( this.checked ){
			$('#label-dsn').html("&nbsp;<b>Tetap</b>");
		}else{
			$('#label-dsn').html("&nbsp;<b>Kontrak</b>");
		}
	});
	
	$("input[name='files']").change(function(){
		$('#list').html('<img width="80" id="ava" src="#" alt="your image" />');
        readURL(this);
    });
	
	//---Karyawan Unit----
	fakultasid = $('#select_fakultas_kunit').val();
	cabangid   = $('#select_cabang_kunit').val();
	get_unit(fakultasid,cabangid,'unit','');
	
	$('#select_fakultas_kunit').change(function() {
		fakultasid = $(this).val();
		cabangid   = $('#select_cabang_kunit').val();
		get_unit(fakultasid,cabangid,'unit','');
	});
	
	$('#select_cabang_kunit').change(function() {
		fakultasid = $('#select_fakultas_kunit').val();
		cabangid   = $(this).val();
		get_unit(fakultasid,cabangid,'unit','');
	});
	//-----------------------
	
	//---Karyawan kenaikan----
	fakultasid = $('#select_fakultas_kenaikan_unit').val();
	cabangid   = $('#select_cabang_kenaikan_unit').val();
	get_unit(fakultasid,cabangid,'kenaikan','');
	
	$('#select_fakultas_kenaikan_unit').change(function() {
		fakultasid = $(this).val();
		cabangid   = $('#select_cabang_kenaikan_unit').val();
		get_unit(fakultasid,cabangid,'kenaikan','');
	});
	
	$('#select_cabang_kenaikan_unit').change(function() {
		fakultasid = $('#select_fakultas_kenaikan_unit').val();
		cabangid   = $(this).val();
		get_unit(fakultasid,cabangid,'kenaikan','');
	});
	//-----------------------
	
	$("#b_karyawan").click(function(e){			
		var postData = new FormData($('#form')[0]);
		$.ajax({
			url : base_url + "module/master/staff/saveToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				// alert(msg);
			    alert ('Simpan data sukses');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Simpan data gagal');      
			},
		    cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault(); //STOP default action
		return false;
	});//-------END SUBMIT karyawan-------------------------------------
	
	$("#form_bio").submit(function(e){
		var biografi = CKEDITOR.instances['biografi'].getData();
		$("#biografi").val(biografi);
		
		var biografi_eng = CKEDITOR.instances['biografi_eng'].getData();
		$("#biografi_eng").val(biografi_eng);
		
		var about = CKEDITOR.instances['about'].getData();
		$("#about").val(about);
		
		var about_eng = CKEDITOR.instances['about_eng'].getData();
		$("#about_eng").val(about_eng);
					
		var postData = new FormData($(this)[0]);
		$.ajax({
			url : base_url + "module/master/staff/save_bioToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(msg) 
			{
				alert(msg);
			    //alert ('Simpan data sukses');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Simpan data gagal');      
			},
		    cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault(); //STOP default action
		return false;
	});//-------END SUBMIT bio-karyawan-------------------------------------
	
	$("#b_karyawanunit").click(function(e){
	  var unit = $('#select_unit_kunit').val();
	  var periodemulai = $('input[name="periodemulai"]').val();
	  var periodeselesai = $('input[name="periodeselesai"]').val();
	  
	  if(unit!='-'&&periodemulai.length!=0&&periodeselesai.length!=0){				
		var postData = new FormData($('#form-unit')[0]);
		$.ajax({
			url : base_url + "module/master/staff/save_karyawan_unitToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
			    // alert ('Upload Success!');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Simpan data gagal');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
	  }else{
		alert("Please Complete the Form");
	  }
	  e.preventDefault(); //STOP default action
	  return false;
	});//-------END SUBMIT karyawan unit-------------------------------------
	
	
	$("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
    	
        var id = $(e.target).attr("href").substr(1);
       //  alert(id);
        window.location.hash = id;
    });

    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
	    
 
	$("#b_karyawanprestasi").click(function(e){
	
	  var periodemulai = $('input[name="mulai"]').val();
	  var periodeselesai = $('input[name="selesai"]').val();
	  
	  if(periodemulai.length!=0&&periodeselesai.length!=0){				
		var postData = new FormData($('#form-prestasi')[0]);
		
		$.ajax({
			url : base_url + "module/master/staff/save_karyawan_prestasiToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
				//location.reload(); 
				$('#pekerjaanTab a[href="' + hash + '"]').tab('load');
				$('#pekerjaanTab li:eq(3) a').tab('show');
				
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Simpan data gagal');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
	  }else{
		alert("Lengkapi data anda");
	  }
	  e.preventDefault(); //STOP default action
	  return false;
	});
	
	
	$(".edit_prestasi").click(function(){
		var id = $(this).data("prestasi");
		//alert(id);
		var form = document.createElement("form");
	    var input = document.createElement("input");
		
		form.action = document.URL;
		form.method = "post"
		
		input.name = "prestasi_id";
		input.value = id;
		form.appendChild(input);
				
		document.body.appendChild(form);
		form.submit();
	});
	
	//-------END SUBMIT karyawan prestasi-------------------------------------
	
	
	
	
	
	$("#b_karyawankenaikan").click(function(e){
	  var seljenis = $('#select_jenis').val();
	  var tanggalsk = $('input[name="tanggalsk"]').val();
	  var nosk = $('input[name="nosk"]').val();
	  var periodemulai = $('input[name="periodemulaikenaikan"]').val();
	  var periodeselesai = $('input[name="periodeselesaikenaikan"]').val();
	  var tmt = $('input[name="tmt"]').val();
	  var keterangan = $('textarea[name="keterangan"]').val();
	  var docid = $('input[name="docid"]').val();
	  
	  //if(seljenis!='-'&&tanggalsk.length!=0&&nosk.length!=0&&periodemulai.length!=0&&periodeselesai.length!=0&&tmt.length!=0&&keterangan.length!=0&&docid&&docid.length!=0){	

		if(periodemulai.length!=0){
		var postData = new FormData($('#form-kenaikan')[0]);
		$.ajax({
			url : base_url + "module/master/staff/save_karyawan_kenaikanToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
			    // alert ('Upload Success!');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Simpan data gagal');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
	  }else{
		alert("lengkapi data anda");
	  }
	  e.preventDefault(); //STOP default action
	  return false;
	});//-------END SUBMIT karyawan unit-------------------------------------
	
});

// $('input[name="periodemulaikenaikan"]').bind("propertychange input paste", function(){
	// var val_ = $(this).val();
	// get_thn_bln(val_);
// });

$('input[name="periodemulaikenaikan"]').on('changeDate', function () {
    var val_ = $(this).val();
	get_thn_bln(val_);
});

function get_thn_bln(input){
	var mulai = $('input[name="tglmskstaff"]').val();
	var start = convert_(mulai);
	start = new Date(start);
	input = new Date(input);
	
	var months = input.getMonth() - start.getMonth() + (12 * (input.getFullYear() - start.getFullYear()));
	var year   = parseInt(months/12)
	var month  = (parseInt(months%12)+1);
	// alert(year);alert(month);
	$('input[name="tahun-kerja"]').val(year);
	$('input[name="bulan-kerja"]').val(month);
	
}

function convert_(mulai){
	var date = mulai.split('-');
	date = date[2]+'/'+date[1]+'/'+date[0];
	return date;
}

$(".select_library").click(function(){
		setTimeout(function(){
			$(".library_attach").show();
		},200);
	});

function get_unit(fakultasid,cabangid,param,unit_id){
	$.ajax({
		type : 'POST',
		dataType : 'html',
		url : base_url + "module/master/staff/get_unit",
		data : $.param({
					fakultasid : fakultasid,
					cabangid : cabangid,
					unitid : unit_id
				}),
		success : function(msg){
			if(msg!=''){
				if(param=='unit'){
					$('#select_unit_kunit').html(msg);
				}
				else if(param=='kenaikan'){
					$('#select_unit_kenaikan_unit').html(msg);
					if(unit_id!=''){
						$('#select_unit_kenaikan_unit option').each(function() {
							if($(this).val()==unit_id){
								$('#select_unit_kenaikan_unit').select2().select2('val',unitid);
								// $(this).prop('selected', true);
							}
						});
					}else $('#select_unit_kenaikan_unit').select2().select2('val','-');
				}
			}
		}
	});
}

function edit_unit(unitid,karyawanid,periodemulai,periodeselesai,isaktif,unit_id){
	$('#select_unit_kunit option').each(function() {
		if($(this).val()==unitid){
			$('#select_unit_kunit').select2().select2('val',unitid);
			// $(this).prop('selected', true);
		}
	});
	
	$('input[name="periodemulai"]').val(periodemulai);
	
	if(periodeselesai=='0000-00-00'){
		$('input[name="periodeselesai"]').val('Sekarang');
		$('input[name="periodeselesai"]').prop('disabled', true);
		
		$('input[name="current"]').prop('checked', true);
		$('input[name="isaktif"]').prop('checked', true);
	}else{
		$('input[name="periodeselesai"]').val(periodeselesai);
		$('input[name="periodeselesai"]').prop('disabled', false);
		$('input[name="current"]').prop('checked', false);
		if(isaktif=='1'){
			$('input[name="isaktif"]').prop('checked', true);
		}else{
			$('input[name="isaktif"]').prop('checked', false);
		}
		
	}
	
	$('input[name="hidIdval"]').val(unit_id+','+periodemulai+','+periodeselesai+','+isaktif);
	$('#cancel-unit').show();
}

function delete_unit(unitid,karyawanid,periodemulai,periodeselesai,isaktif,e){
	var x = confirm("Delete this unit ?");
	if(x){
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : base_url + "module/master/staff/delete_unit",
			data : $.param({
						unitid : unitid,
						karyawanid : karyawanid,
						periodemulai : periodemulai,
						periodeselesai : periodeselesai,
						isaktif : isaktif
					}),
			success : function(msg){
				alert(msg);
				$(e).parent().parent().parent().parent().parent().parent().parent().remove();
			}
		});
	}
	
}

//----kenaikan------------------
$('input[name="jenis"]').click(function(){
	var jenis = $(this).val();
	if(jenis=='jabatan'){
		$('.jenis-kenaikan').find('label').text('Jabatan');
	}else{
		$('.jenis-kenaikan').find('label').text('Kenaikan');
	}
	
	$.ajax({
		type : 'POST',
		dataType : 'html',
		url : base_url + "module/master/staff/get_jenis",
		data : $.param({
					jenis : jenis
				}),
		success : function(msg){
			// alert(msg);
			if(msg!=''){
				$('#select_jenis').html(msg);
			};
		}
	});
	
});

function select_img(id,filename){
	var info = filename;
	info += '&nbsp;<a href="#" onclick="remove_selected_img()"><i class="fa fa-trash-o"></i></a>';
	info += '<input type="hidden" name="docid" value="'+id+'" />';
	$('#document-place').html(info);
	$('#selectfile').modal('hide');
}

function remove_selected_img(){
	$('#document-place').empty();
}

$('#cancel-unit').click(function(){
	$('#select_unit_kunit').select2().select2('val','-');
	$('input[name="periodemulai"]').val('');
	$('input[name="periodeselesai"]').val('');
	$('input[name="periodeselesai"]').prop('disabled', false);
	$('input[name="current"]').prop('checked', false);
	$('input[name="isaktif"]').prop('checked', false);
	$('input[name="hidIdval"]').val('');
	$('#cancel-unit').hide();
});

$('input[name="current"]').click(function() {
		if( this.checked ){
			$('input[name="periodeselesai"]').prop('disabled', true);
			$('input[name="periodeselesai"]').val('Sekarang');
			
			$('.periode-akhir').html('Sampai');
			
			$('input[name="isaktif"]').prop('checked', true);
			$('input[name="isaktif"]').parent().find('b').html("&nbsp;Aktif");
			
		}else{
			$('input[name="periodeselesai"]').prop('disabled', false);
			$('input[name="periodeselesai"]').val('');
			
			$('.periode-akhir').html('Periode Selesai');
			
			$('input[name="isaktif"]').prop('checked', false);
			$('input[name="isaktif"]').parent().find('b').html("&nbsp;Tidak Aktif");
		}
});

$('input[name="isaktif"]').click(function() {
		if( this.checked ){
			$('input[name="isaktif"]').parent().find('b').html("&nbsp;Aktif");
		}else{
			$('input[name="isaktif"]').parent().find('b').html("&nbsp;Tidak Aktif");
		}
});

$(document).on('click', 'input[name="saktif"]', function(){
	var value_ = $(this).val();
	if(value_=='keluar'){
		$('.form-group-pensiun').show();
	}else if(value_=='pensiun'){
		$('.form-group-pensiun').show();
	}else{
		$('.form-group-pensiun').hide();
	}
})

function edit_kenaikan(id){
	$('#cancel-kenaikan').show();	
	$.ajax({
			type : 'POST',
			dataType : 'JSON',
			url : base_url + "module/master/staff/edit_kenaikan",
			data : $.param({
						kenaikanid : id
					}),
			success : function(data){
				// alert(data);
				// location.reload();
				$.each(data, function(i,f){
					$('input[name="hidNaikId"]').val(f.kenaikanid);
					
					$('input[name="jenis"]').each(function(){
						if($(this).val()==f.jenis){
							$(this).prop("checked", true);
						}else $(this).prop("checked", false);
					});
					
					//----------------------------------------------------------
					var selectedval;
					if(f.jenis=='jabatan'){
						$('.jenis-kenaikan').find('label').text('Jabatan');
						selectedval = f.jabatan_id;
						
					}else{
						$('.jenis-kenaikan').find('label').text('Kenaikan');
						selectedval = f.jenis_kenaikan_id;
					}
					
					$.ajax({
						type : 'POST',
						dataType : 'html',
						url : base_url + "module/master/staff/get_jenis",
						data : $.param({
									jenis : f.jenis,
									selectedval : selectedval
								}),
						success : function(msg){
							if(msg!=''){
								$('#select_jenis').html(msg);
								$('#select_jenis').select2().select2('val', selectedval);
							};
						}
					});
					//----------------------------------------------------------
					
					$('#select_golongan option').each(function(){
						if($(this).val()==f.golongan){
							$('#select_golongan').select2().select2('val', $(this).val());
						}else{
							$(this).removeAttr('selected');
						}
					});
					
					$('#select_fakultas_kenaikan_unit option').each(function(){
						if($(this).val()==f.fakultas_id){
							$('#select_fakultas_kenaikan_unit').select2().select2('val', $(this).val());
						}else{
							$(this).removeAttr('selected');
						}
					});
								
					cabangid   = $('#select_cabang_kenaikan_unit').val();
					get_unit(f.fakultas_id,cabangid,'kenaikan',f.unit_id);
										
					$('input[name="tanggalsk"]').val(f.tgl_sk);
					$('input[name="nosk"]').val(f.no_sk);
					$('input[name="periodemulaikenaikan"]').val(f.periode_mulai);
					$('input[name="periodeselesaikenaikan"]').val(f.periode_selesai);
					$('input[name="tmt"]').val(f.tmt);
					
					$('input[name="nonota"]').val(f.no_nota);
					$('input[name="tanggalnota"]').val(f.tgl_nota);
					$('input[name="nostlud"]').val(f.no_stlud);
					$('input[name="tanggalstlud"]').val(f.tgl_stlud);
					$('input[name="pejabat"]').val(f.pejabat_penetap);
					$('input[name="kredit"]').val(f.kredit);
					
					$('textarea[name="keterangan"]').val(f.keterangan);
					
					var masakerja = f.masa_kerja;
					var numkerja = masakerja/30;
					var year = numkerja/12;
					var month = numkerja%12;
					// alert(parseInt(year));alert(parseInt(month));
					$('input[name="tahun-kerja"]').val(parseInt(year));
					$('input[name="bulan-kerja"]').val(parseInt(month));
					
					if(f.is_aktif=='1'){
						$('input[name="isaktif"]').prop("checked", true);
						$('input[name="isaktif"]').parent().find('b').text('Aktif');
					}else{
						$('input[name="isaktif"]').prop("checked", false);
						$('input[name="isaktif"]').parent().find('b').text('Tidak Aktif');
					}
					
					if(f.documentid!='-'){
						var info = f.file_name;
						info += '&nbsp;<a href="#" onclick="remove_selected_img()"><i class="fa fa-trash-o"></i></a>';
						info += '<input type="hidden" name="docid" value="'+f.documentid+'" />';
						$('#document-place').html(info);
					}
					
				})
			}
		});
}

$('#cancel-kenaikan').click(function(){
	fakultasid = $('#select_fakultas_kenaikan_unit').val();
	cabangid   = $('#select_cabang_kenaikan_unit').val();
	get_unit(fakultasid,cabangid,'kenaikan','');
	
	$('input[name="hidNaikId"]').val('');
					
	$('input[name="jenis"]').each(function(){
		if($(this).val()=='jabatan'){
			$(this).prop("checked", true);
		}else $(this).prop("checked", false);
	});
	
	$.ajax({
		type : 'POST',
		dataType : 'html',
		url : base_url + "module/master/staff/get_jenis",
		data : $.param({
					jenis : 'jabatan'
				}),
		success : function(msg){
			if(msg!=''){
				$('#select_jenis').html(msg);
				$('#select_jenis').select2().select2('val', '-');
			};
		}
	});
	
	$('#select_golongan').val('-');
	$('#select_golongan').select2().select2('val', '-');
	
	$('input[name="tanggalsk"]').val('');
	$('input[name="nosk"]').val('');
	$('input[name="periodemulaikenaikan"]').val('');
	$('input[name="periodeselesaikenaikan"]').val('');
	$('input[name="tmt"]').val('');
	$('textarea[name="keterangan"]').val('');
	
	$('input[name="nonota"]').val('');
	$('input[name="tanggalnota"]').val('');
	$('input[name="nostlud"]').val('');
	$('input[name="tanggalstlud"]').val('');
	$('input[name="pejabat"]').val('');
	$('input[name="kredit"]').val('');
	$('input[name="tahun-kerja"]').val('');
	$('input[name="bulan-kerja"]').val('');
	
	$('input[name="isaktif"]').prop("checked", false);
	$('input[name="isaktif"]').parent().find('b').text('Tidak Aktif');
	$('#document-place').empty();
	$(this).hide();
});

function delete_kenaikan(kenaikanid){
	var x = confirm("Delete this unit ?");
	if(x){
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : base_url + "module/master/staff/delete_kenaikan",
			data : $.param({
						kenaikanid : kenaikanid
					}),
			success : function(msg){
				alert(msg);
				location.reload();
			}
		});
	}
}

function show_info(id){
	var stat = $('#'+id+'-info').attr('class');
	
	if(stat=='hidden'){
		$('#'+id+'-info').fadeIn();
		$('#'+id+'-info').attr('class','visible');
	}else{
		$('#'+id+'-info').fadeOut();
		$('#'+id+'-info').attr('class','hidden');
	}
	
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#ava').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
};

function validateEmail(sEmail) {
	var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if( !emailReg.test( sEmail ) ) {
		return false;
	} else {
		return true;
	}
};