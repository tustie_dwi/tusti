$(document).ready(function(){
	$(".delete").click(function(){
		var uri_ = $(this).data("uri");
		var id = $(this).data("id");
		var kode_ = $(this).data("kode");
		var ket_ = $(this).data("ket");
		
		
		if(confirm("Apakah Anda yakin menghapus data "+ ket_ +" ["+ kode_ +"]"+ "?")){
			$.ajax({
				type	: 'POST',
				dataType : 'html',
				url : uri_,
				success : function(msg){					
					$("#notif").html("<div class='alert alert-warning'><button type='button' class='close' data-dismiss='alert'>&times;</button>OK, data "+ ket_ +" ["+ kode_ +"]"+ " telah berhasil dihapus.</div>");
					$("#post-" + id).fadeOut(500);
				}
			});
		}
	});
	
	$("#submit-ruang").click(function(){
		var kode_ = $("#form-ruang [name='kode']").val();
		var kapasitas_ = $("#form-ruang [name='kapasitas']").val();
		var keterangan_ = $("#form-ruang [name='keterangan']").val();
		var hidId_ = $("#form-ruang [name='hidId']").val();
		var uri_ = $("#uri").val();
		
		if(hidId_ != '') {
			var init = 'diubah';
		}
		else {
			var init = 'disimpan';
		}
		
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : uri_,
			data : $.param({
						kode : kode_,
						kapasitas : kapasitas_,
						keterangan : keterangan_,
						hidId : hidId_
					}),
			success : function(msg){
				$("#notif").html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>OK, data "+ keterangan_ +" ["+ kode_ +"] berhasil "+ init +".</div>");
				if(init == 'disimpan'){
					$("#form-ruang [name='kode']").val('');
					$("#form-ruang [name='kapasitas']").val('');
					$("#form-ruang [name='keterangan']").val('');
				}
			}
		});
	});	
	
	
	//=============karyawan ruang==============//
	$(".e9").select2();
	
	var edit = $('#karyawan').val();
	// alert(edit);
	$('#karyawan').attr("disabled","disabled");
	$('#cancel-btn').hide();
	
	var width_mk	= $("#ruang").outerWidth(true);
	$("#karyawan").css('width',width_mk+'px');
	
	if(edit !== null){
		if(edit.trim()!=="0"){
			$('#cancel-btn').show();
			$('#karyawan').removeAttr("disabled");
		}else{
			$('#karyawan').attr("disabled","disabled");
			$('#cancel-btn').hide();
		}
	}
	$('#ruang,#karyawan').change(function(){
		var unit	= $('#ruang').val();
		var mk		= $('#karyawan').val();
		
		if(unit.trim()!=="0" || mk !== '0'){
			$('#cancel-btn').show();
		}
		else{
			$('#cancel-btn').hide();
		}
	})
	
	$('#ruang').change(function(){
		var unit 	= $('#ruang').val();
		if(unit!=='0'){
			$('#karyawan').removeAttr("disabled");
		}
		else{
			$('#karyawan').val('0');
			$('#karyawan').attr("disabled","disabled");
			$('#cancel-btn').hide();
		}
	});
	
	$("#ruang_karyawan_form").submit(function(e){
		var ruang		= $('#ruang').val();
		var karyawan	= $('#karyawan').val();
		if(ruang.trim()!=="0" && karyawan !== null){
			// alert(ruang+'\n'+karyawan);
			var formData = new FormData($(this)[0]);
			var URL = base_url + 'module/master/general/ruang/save_karyawan_ruang';
			$.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert(msg);
		            location.href = base_url + 'module/master/general/ruang/karyawan';
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});
		}
		else{
			alert("Form yang anda submit belum lengkap!");
		}
		e.preventDefault(); //STOP default action
		return false;
	});
	
	//index
	$('#ruang_index').change(function(){
		$('#display').empty();
		var ruang	= $('#ruang_index').val();
		var URL		= base_url + 'module/master/general/ruang/get_karyawan_ruang';
		//alert(ruang);
		if(ruang!=='0'){
			$.ajax({
				url : URL,
				type : "POST",
				dataType : "HTML",
		        data : $.param({
					ruang : ruang
				}),
		        async: false,
		        success:function(msg) 
		        {
		            if(msg !== ""){
		            	$('#display').html(msg);
		            }
		        }
			})
		}
		else{
			var output = '<div class="col-sm-12" align="center" style="margin-top:20px;">';
			output += '<div class="well">Silahkan pilih ruang terlebih dahulu</div>';
			output += '</div>';
			$('#display').html(output);
		}
	});
});

function doDelete(ruang,karyawan){
	var x = confirm("Are you sure you want to delete?");
	var url = base_url + 'module/master/general/ruang/delete_karyawan_ruang';
 	if (x){
	  	$.ajax({
			type : "POST",
			dataType : "html",
			url : url,
			data : $.param({
				ruang : ruang,
				karyawan : karyawan
			}),
			success : function(msg) {
				if(msg!==""){
					$('.sett-'+ruang+"-"+karyawan).fadeOut();
				}
			}
		});
	}
}
