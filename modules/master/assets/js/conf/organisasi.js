$(document).ready(function(){
	// var org = $("input[name='periode_mulai']").length;
	// if(org){
		//var current = $('#checkbox').prop('checked');
		// // if(current==1){
			// alert(current);
		// // }
	// }
	// $("input[name='current']").change(function(){
		// var cek = $(this).is(':checked');
		// if(cek==1){
// 			
		// }
	// });
	
	$("#form-organisasi").submit(function(e){
		var organisasi 		= $("input[name='nama_organisasi']").val();
		var sebagai 		= $("input[name='jabatan_sebagai']").val();
		var periode_mulai 	= $("input[name='periode_mulai']").val();
		var kar_id			= $("input[name='kar_id']").val();
		//alert(organisasi+"\n"+sebagai+"\n"+periode_mulai);
		if(organisasi.trim()!=="" && sebagai.trim()!=="" && periode_mulai.trim()!==""){
			 var formData = new FormData($(this)[0]);
			 $.ajax({
	            url : base_url + "module/master/staff/save_organisasi",
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.href = base_url + 'module/master/staff/edit/'+kar_id;
		        },
		        error: function(jqXHR, textStatus, errorThrown) 
		        {
		            alert ('Upload Failed!');      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		      });
	     }
	     else{
	     	alert("Silahkan lengkapi form terlebih dahulu!");
	     }
	     e.preventDefault();
	     return false;
	});
	
	$(".edit_organisasi").click(function(){
		var org_id	= $(this).data("id");
		//alert(org_id);
		var form = document.createElement("form");
	    var input = document.createElement("input");
		
		form.action = document.URL;
		form.method = "post"
		
		input.name = "edit_organisasi";
		input.value = org_id;
		form.appendChild(input);
		
		document.body.appendChild(form);
		form.submit();		
	});
	
	$(".delete_organisasi").click(function(){
		var x = confirm("Are you sure?");
		if(x){
			var org_id	= $(this).data("id");
			$.ajax({
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/staff/delete_organisasi',
				data : $.param({
					id : org_id
				}),
				success : function(msg) {
					alert(msg);
					$("#"+org_id).fadeOut();
				}
			});
		}	
	});
});