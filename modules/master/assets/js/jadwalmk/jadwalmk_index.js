//--------FUNCTION ON DOCUMENT READY--------------------------
$(document).ready(function() { 
	$(".e9").select2();
	
	var cabang			= document.getElementById("select_cabang");
	var cabangid		= $(cabang).val();
	
	var type			= document.getElementById("select_type");
	var typeid			= $(type).val();
	
	var fakultas		= document.getElementById("select_fakultas");
	var fakultasid		= $(fakultas).val();

	var thnakademik		= document.getElementById("select_thnakademik");
	var thnakademikid	= $(thnakademik).val();
	
	// var prodi 			= document.getElementById("select_prodi");
	// var prodiid			= $(prodi).val();
	var prodiid			= $('#prodiid').val();
	
	var mk 				= document.getElementById("select_mk");
	var mkid			= $(mk).val();

	var pengampu		= document.getElementById("select_pengampu");
	var pengampuid		= $(pengampu).val();
	
	var kelas			= document.getElementById("select_kelas");
	var kelasid			= $(kelas).val();	
	
	//--- SELEKSI DARI FAKULTAS ---//
	if (fakultasid == '0') {
		$("#select_cabang").attr("disabled", true);
		$("#select_cabang").select2('enable', false);
		$("#select_type").attr("disabled", true);
		$("#select_type").select2('enable', false);
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} else {
		//--- POST SELEKSI PRODI DARI FAKULTAS ---//
		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/jadwalmk/get_prodi_by_fakultas',
			data : $.param({
				fakultas_id : fakultasid,
				prodi_id	: prodiid
			}),
			success : function(msg) {
				if (msg == '') {

				} else {
					$("#select_cabang").removeAttr("disabled");
					$("#select_prodi").html(msg);
					if(prodiid.length==0){
						$("#select_prodi").select2('val', 0);
					}else $("#select_prodi").select2('val', prodiid);
					$("#select_cabang").select2('enable', true);
				}
			}
		});
		//--- END POST SELEKSI PRODI DARI FAKULTAS ---//
	}
	//--- END SELEKSI DARI FAKULTAS ---//
	
	//--- SELEKSI DARI CABANG ---//
	if (cabangid == '0') {
		$("#select_type").attr("disabled", true);
		$("#select_type").select2('enable', false);
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} 
	else {
		$("#select_type").removeAttr("disabled");
		$("#select_type").select2('enable', true);
	}
	//--- END SELEKSI DARI CABANG ---//
	
	//--- SELEKSI DARI TYPE ---//
	if (typeid == 'x') {
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} 
	else {
		$("#select_thnakademik").removeAttr("disabled");
		$("#select_thnakademik").select2('enable', true);
	}
	//--- END SELEKSI DARI TYPE ---//	
	
	//--- SELEKSI DARI TAHUN AKADEMIK ---//
	if (thnakademikid == '0') {
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} else {
		$("#select_prodi").removeAttr("disabled");
		$("#select_prodi").select2('enable', true);
	}
	//--- END SELEKSI DARI TAHUN AKADEMIK ---//
	
	//--- SELEKSI DARI PRODI ---//
	if (prodiid == '0') {
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} else {
		//--- POST SELEKSI MK DARI THNAKADEMIK DAN PRODI ---//
		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/jadwalmk/get_mk_by_thnakademik',
			data : $.param({
				thnakademik_id : thnakademikid,
				prodi_id	   : prodiid,
				mk_id		   : mkid
			}),
			success : function(msg) {
				if (msg == '') {

				} else {
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
					$("#select_mk").select2('enable', true);
				}
			}
		});
		//--- END POST SELEKSI MK DARI THNAKADEMIK DAN PRODI ---//
	}
	//--- END SELEKSI DARI PRODI ---//
	
	//--- SELEKSI DARI MK ---//
	if (mkid == '0') {
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} else {
		
		//--- POST SELEKSI PENGAMPU DARI MK ---//
		$.ajax({
				type : "POST",
				dataType : "html",
				url : base_url + 'module/master/jadwalmk/get_pengampu_by_mk',
				data : $.param({
					mk_id : mkid,
					pengampu_id : pengampuid
				}),
				success : function(msg) {
					// alert(msg);
					if (msg == '') {
						$("#select_pengampu").attr("disabled", true);
					} else {
						$("#select_pengampu").removeAttr("disabled");
						$("#select_kelas").removeAttr("disabled");	
						$("#select_pengampu").html(msg);
						$("#select_kelas").select2('enable', true);
						$("#select_pengampu").select2('enable', true);
					}
				}
		});
		//--- END POST SELEKSI PENGAMPU DARI MK ---//

	}
	//--- END SELEKSI DARI MK ---//
	
	//--- POST SELEKSI DARI PARAMETER ---//
	$('#parameter').change(function() {
		$("#param").submit();
	});
	//--- END POST SELEKSI DARI PARAMETER ---//
	
	$('#import-file-excel').click(function() {
		var importForm = $('.import-form').data('visibility');
		if(importForm=='hide'){
			$('.import-form').slideDown();
			$('.import-form').data('visibility', 'show');
		}else{
			$('.import-form').slideUp();
			$('.import-form').data('visibility', 'hide');
		}
	});
	
	$('input[name="excel-import"]').change(function(e) {
		// $("#import").submit();
		var postData = new FormData($('#import')[0]);
		$.ajax({
			url : base_url + "module/master/jadwalmk/import",
			type: "POST",
			data : postData,
			beforeSend: function() {
		       $('#loading-gif').show();
		    },
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
				$('#loading-gif').hide();
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Proses Simpan Gagal!');      
		    },
		    cache: false,
			contentType: false,
			processData: false
		  });
		e.preventDefault(); //STOP default action
		return false;
	});
});


//--------FUNCTION ON CHANGE PARAMETER--------------------------
//--- SELEKSI DARI FAKULTAS ---//
$('#select_fakultas').change(function() {
	var fakultas		= document.getElementById("select_fakultas");
	var fakultasid		= $(fakultas).val();
	
	var prodi 			= document.getElementById("select_prodi");
	var prodiid			= $(prodi).val();
	
	// alert(prodiid);
	if (fakultasid == '0') {
		$("#select_cabang").attr("disabled", true);
		$("#select_cabang").select2('enable', false);
		$("#select_type").attr("disabled", true);
		$("#select_type").select2('enable', false);
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} 
	else {
		$("#select_cabang").select2().select2("val", '0');
		$("#select_type").select2().select2("val", 'x');
		$("#select_thnakademik").select2().select2("val", '0');
		$("#select_prodi").select2().select2("val", '0');
		$("#select_mk").select2().select2("val", '0');
		$("#select_pengampu").select2().select2("val", '0');
		$("#select_kelas").select2().select2("val", '0');
		//--- POST SELEKSI PRODI DARI FAKULTAS ---//
		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/jadwalmk/get_prodi_by_fakultas',
			data : $.param({
				fakultas_id : fakultasid,
				prodi_id	: prodiid
			}),
			success : function(msg) {
				if (msg == '') {

				} else {
					$("#select_cabang").removeAttr("disabled");
					$("#select_prodi").html(msg);
					$("#select_cabang").select2('enable', true);
				}
			}
		});
		//--- END POST SELEKSI PRODI DARI FAKULTAS ---//
	}
});
//--- END SELEKSI DARI FAKULTAS ---//

//--- SELEKSI DARI CABANG ---//
$('#select_cabang').change(function() {
	var cabang			= document.getElementById("select_cabang");
	var cabangid		= $(cabang).val();
	
	if (cabangid == '0') {
		$("#select_type").attr("disabled", true);
		$("#select_type").select2('enable', false);
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
		$("#select_type").select2().select2("val", 'x');
		$("#select_thnakademik").select2().select2("val", '0');
		$("#select_prodi").select2().select2("val", '0');
		$("#select_mk").select2().select2("val", '0');
		$("#select_pengampu").select2().select2("val", '0');
		$("#select_kelas").select2().select2("val", '0');
	} 
	else {
		$("#select_type").removeAttr("disabled");
		$("#select_type").select2('enable', true);
		$("#select_type").select2().select2("val", 'x');
		$("#select_thnakademik").select2().select2("val", '0');
		$("#select_prodi").select2().select2("val", '0');
		$("#select_mk").select2().select2("val", '0');
		$("#select_pengampu").select2().select2("val", '0');
		$("#select_kelas").select2().select2("val", '0');
	}
});
//--- END SELEKSI DARI CABANG ---//

//--- SELEKSI DARI TYPE ---//
$('#select_type').change(function() {
	var type			= document.getElementById("select_type");
	var typeid			= $(type).val();
	
	// alert(typeid);
	if (typeid == 'x') {
		$("#select_fakultas").attr("disabled", true);
		$("#select_fakultas").select2('enable', false);
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} 
	else {
		$("#select_thnakademik").removeAttr("disabled");
		$("#select_thnakademik").select2('enable', true);
	}
});
//--- END SELEKSI DARI TYPE ---//

//--- SELEKSI DARI TAHUN AKADEMIK ---//
$('#select_thnakademik').change(function() {
	var thnakademik		= document.getElementById("select_thnakademik");
	var thnakademikid	= $(thnakademik).val();
	
	if (thnakademikid == '0') {
		$("#select_prodi").attr("disabled", true);
		$("#select_prodi").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} else {
		$("#select_prodi").removeAttr("disabled");
		$("#select_prodi").select2('enable', true);
	}
});
//--- END SELEKSI DARI TAHUN AKADEMIK ---//
	
//--- SELEKSI DARI PRODI ---//
$('#select_prodi').change(function() {
	var thnakademik		= document.getElementById("select_thnakademik");
	var thnakademikid	= $(thnakademik).val();
	
	var prodi 			= document.getElementById("select_prodi");
	var prodiid			= $(prodi).val();
	
	var mk 				= document.getElementById("select_mk");
	var mkid			= $(mk).val();
	
	if (prodiid == '0') {
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
		$("#select_pengampu").attr("disabled", true);
		$("#select_pengampu").select2('enable', false);
		$("#select_kelas").attr("disabled", true);
		$("#select_kelas").select2('enable', false);
	} else {
		//--- POST SELEKSI MK DARI THNAKADEMIK DAN PRODI ---//
		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/master/jadwalmk/get_mk_by_thnakademik',
			data : $.param({
				thnakademik_id : thnakademikid,
				prodi_id	   : prodiid,
				mk_id		   : mkid
			}),
			success : function(msg) {
				if (msg == '') {
					
				} else {
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
					$("#select_mk").select2('enable', true);
				}
			}
		});
		//--- END POST SELEKSI MK DARI THNAKADEMIK DAN PRODI ---//
	}
});
//--- END SELEKSI DARI PRODI ---//