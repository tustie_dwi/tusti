$(document).ready(function() {
	$(".e9").select2();
	
	var fakultas_id_ = $("#select_fakultas").val();
		
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + "/module/master/jadwalmk/get_prodi_by_fakultas",
		data : $.param({
			fakultas_id : fakultas_id_
		}),
		success : function(msg) {
			if (msg == '') {
				//$("#select_thn").html(msg);
				$("#select_prodi").attr("disabled", true);
				$("#addnamamk").attr("disabled", true);
			} else {
				$("#select_prodi").removeAttr("disabled");
				$("#select_prodi").html('<option value="0">Select Prodi</option>');
				$("#select_prodi").html(msg);
				$("#select_prodi").select2('enable', true);
			}
		}
	});
	
	$("#select_cabang").change(function() {
		var cabangid_ = $(this).val();
		
		var fakultas_id_ = $('#select_fakultas').val();
		var uri = $('#select_fakultas').attr("uri_");
		
		$.ajax({
			  type : "POST",
			  dataType: "json",
               url: base_url + "/module/master/conf/namamkfrommkditawarkan",
               data : $.param({
				fakultasid : fakultas_id_,
				cabangid  : cabangid_
			}),
               success: function(data){
                 $('#addnamamk').autocomplete(
                 {
                       source: data,
                       minLength: 0,
                       select: function(event, ui) { 
						$('#addnamamk').val(ui.item.value);
						$('#namamk').val(ui.item.id);
						}    
                 });
               }
          });
	});
	
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var cabangid_ = $('#select_cabang').val();
		var thnakademikid = $('#thn_akademik').val();
		
		// alert(thnakademikid);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + "/module/master/jadwalmk/get_prodi_by_fakultas",
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					//$("#select_thn").html(msg);
					$("#select_prodi").attr("disabled", true);
					$("#addnamamk").attr("disabled", true);
				} else {
					$("#select_prodi").removeAttr("disabled");
					$("#select_prodi").html('<option value="0">Select Prodi</option>');
					$("#select_prodi").html(msg);
					$("#select_prodi").select2('enable', true);
				}
			}
		});
		
		$.ajax({
			type : "POST",
			dataType : "json",
			url : base_url + "/module/master/conf/dosen",
			data : $.param({
				fakultas_id : $('#select_fakultas').val()
			}),
			success : function(msg) {
				// alert(msg);
				$("#dosen").autocomplete({ 
					source: msg,
					minLength: 0, 
					select: function(event, ui) { 
						$('#dosen-id').val(ui.item.id); 
						$('#dosen').val(ui.item.value);
					} 
				});
			}
		});
		
	});
	
	$("#thn_akademik").change(function() {
		var fakultas_id_ 	= $("#select_fakultas").val();
		var cabangid_ 		= $('#select_cabang').val();
		var thnakademikid_ 	= $(this).val();
		
		$.ajax({
			  type : "POST",
			  dataType: "json",
               url: base_url + "/module/master/conf/namamkfrommkditawarkan",
               data : $.param({
				fakultasid : fakultas_id_,
				cabangid  : cabangid_,
				thnakademikid : thnakademikid_
			}),
               success: function(data){
               		 $("#addnamamk").removeAttr("disabled");
                     $('#addnamamk').autocomplete(
                     {
                           source: data,
                           minLength: 0,
                           select: function(event, ui) { 
							$('#addnamamk').val(ui.item.value);
							$('#namamk').val(ui.item.id);
							}    
                     });
               }
          });
	});
	
	$('#select_cabang').change(function() {
		var cabangid_ = $('#select_cabang').val();
		$.ajax({
			type : "POST",
			dataType : "json",
			url : base_url + "/module/master/conf/ruang",
			data : $.param({
				cabang_id : cabangid_
			}),
			success : function(msg) {
				if (msg == '') {
					
				} else {
					$("#ruang").autocomplete({ 
						source: msg,
						minLength: 0, 
						select: function(event, ui) { 
							$('#ruang').val(ui.item.value);
						} 
					});
				}
			}
		});
	});
	
	$(function () {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : base_url + "/module/master/conf/dosen",
			data : $.param({
				fakultas_id : $('#select_fakultas').val()
			}),
			success : function(msg) {
				// alert(msg);
				$("#dosen").autocomplete({ 
					source: msg,
					minLength: 0, 
					select: function(event, ui) { 
						$('#dosen-id').val(ui.item.id); 
						$('#dosen').val(ui.item.value);
					} 
				});
			}
		});
				
		// $("#dosen").autocomplete({ 
			// source: base_url + "/module/master/conf/dosen",
			// minLength: 0, 
			// select: function(event, ui) { 
				// $('#dosen-id').val(ui.item.id); 
				// $('#dosen').val(ui.item.value);
			// } 
		// });
							
	});
	
	$("#submit-jadwal").click(function(e){
		var select_fakultas = $('#select_fakultas').val();
		var thn_akademik = $('#thn_akademik').val();
		var select_prodi = $('#select_prodi').val();
		var dosen = $('#dosen').val().length;
		var namamk = $('#addnamamk').val().length;
		var kelas = $('#kelas').val().length;

		if(select_fakultas!=0 && thn_akademik!=0 && select_prodi!=0 && dosen > 0 && namamk > 0 && kelas > 0){
	      var postData = $('#form-jadwal-addnew').serializeArray();
          $.ajax({
            url : base_url + "module/master/jadwalmk/saveToDB",
	        type: "POST",
	        data : postData,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Success!');
	            location.reload();
	            // location.href=base_url + "module/akademik/jadwalmk/index";
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        }
	      });
	    e.preventDefault(); //STOP default action
		}
		else{
			alert("Please fill the form");
		}
	});
	
	
});

function validate() {
	if (document.getElementById('isonline').checked) {
    	$("#form_no_is_online").fadeIn();
    	//$('#form').removeAttr('style');  
    } 
    else {
       $("#form_no_is_online").fadeOut();
       //$("#form").attr("style", "display: none;");
    }
}

function cekisonline() {
	var cabang		= document.getElementById("select_cabang");
	var cabangid	= $(cabang).val();
	
	var fakultas	= document.getElementById("select_fakultas");
	var fakultasid	= $(fakultas).val();
	
	var thnakademik	= document.getElementById("thn_akademik");
	var thnakademikid = $(thnakademik).val();
	
	var addnamamk	= document.getElementById("namamk");
	var namamk		= $(addnamamk).val();
	
	var ceknamamk	= document.getElementById("addnamamk");
	var ceknamamkval = $(ceknamamk).val();
	
	// alert(ceknamamkval);
	$.ajax({
	  type : "POST",
	  dataType: "html",
       url: base_url + "/module/master/jadwalmk/cekonline",
       data : $.param({
		namamk : namamk
	}),
       success: function(data){
       	// alert(data);
       	// $.setTimeout(function(){
           	if (data != '0') {
				document.getElementById("msgrmv").remove();
				$('#isonline').removeAttr("disabled");
				$('#isonline').prop("checked", false);
				$('#form_no_is_online').fadeOut();
				var del = document.getElementById( 'iscourse_mk' );
				if(del){
					del.parentNode.removeChild( del );
				}
			} else {
				$('#msgmk').html("<div id='msgrmv'>*Mata kuliah ini hanya bisa di tampilkan saat jam perkuliahan</div>");
				$('#isonline').prop("checked", true);
				$('#isonline').attr("disabled", true);
				$('#check').append("<input type=\"hidden\" name=\"isonline\" values=\"1\" id=\"iscourse_mk\" />");
				$('#form_no_is_online').fadeIn();
				//window.location.href = base_url + "/module/akademik/jadwalmk/writenew?data=" + data;
				//document.getElementById('isonline').value = data;
			}
       	// }, 100);    
       }
  });
  
  // $.ajax({
	  // type : "POST",
	  // dataType: "html",
       // url: base_url + "/module/master/jadwalmk/cek_mkditawarkan",
       // data : $.param({
		// ceknamamkval : ceknamamkval,
		// cabangid : cabangid,
		// fakultasid : fakultasid,
		// thnakademikid : thnakademikid
	// }),
       // success: function(data){
//        	
       	// $.setTimeout(function(){
       		// if (data == '') {
       			// $('#namamk').val('');
				// $('#msgrmv').remove();
				// $('#isonline').removeAttr("disabled");
				// $('#isonline').prop("checked", false);
				// $('#form_no_is_online').fadeOut();
				// var del = $( '#iscourse_mk' );
				// del.parentNode.removeChild( del );
			// } else {
				// $('#msgmk').html("<div id='msgrmv'>*Mata kuliah ini hanya bisa di tampilkan saat jam perkuliahan</div>");
				// $('#isonline').prop("checked", true);
				// $('#isonline').attr("disabled", true);
				// $('#check').append("<input type=\"hidden\" name=\"isonline\" values=\"1\" id=\"iscourse_mk\" />");
				// $('#form_no_is_online').fadeIn();
				// //window.location.href = base_url + "/module/akademik/jadwalmk/writenew?data=" + data;
				// //document.getElementById('isonline').value = data;
			// }
		// }, 100);	
       // }
//        
  // });
  
}
