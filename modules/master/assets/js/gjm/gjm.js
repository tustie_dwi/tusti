var fakultasid;
var cabangid;
var fakultas;
	
$(document).ready(function() {
	$(".e9").select2();
	$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	
	
	
	$("#pilih_cabang").change(function() {
		$("#param").submit(); //SUBMIT FORM
	});
	
	
	
	$("input[name='files']").change(function(){
		$('#list').html('<img width="80" id="ava" src="#" alt="your image" />');
        readURL(this);
    });
	
	//---Karyawan Unit----
	fakultasid = $('#select_fakultas').val();
	cabangid   = $('#select_cabang').val();	
	var content_unit_id		= $('#unit-select').val();
	get_unit(fakultasid,content_unit_id);
	
	$('#select_fakultas').change(function() {
			$("#param").submit();
		fakultasid = $(this).val();
		cabangid   = $('#select_cabang').val();
		var content_unit_id		= $('#unit-select').val();
		get_unit(fakultasid,content_unit_id);
		//get_unit(fakultasid,cabangid,'unit','');
	});
	
	var fakultas = $('#select_fak').val();
	var content_unit	= $('#unit-select-id').val();
	get_unit_id(fakultas,content_unit);
	
	$('#select_fak').change(function() {	
		fakultas = $(this).val();
		cabangid   = $('#select_cab').val();	
		var content_unit_id		= $('#unit-select-id').val();	
		get_unit_id(fakultas,content_unit);
	});
	
	$('#select_cab').change(function() {
		fakultas = $('#select_fak').val();
		cabangid   = $(this).val();
		var content_unit	= $('#unit-select-id').val();
		get_unit_id(fakultas,content_unit);
	});
	
	$('#select_cabang').change(function() {
	
		fakultasid = $('#select_fakultas').val();
		cabangid   = $(this).val();
		$("#param").submit();
		var content_unit_id		= $('#unit-select').val();
		get_unit(fakultasid,content_unit_id);
	});
	//-----------------------
	
	
	$("#b_gjm").click(function(e){		
	
	var postData = new FormData($('#form-gjm')[0]);
	$.ajax({
		url : base_url + "module/master/gjm/saveToDB",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			
		    alert ('Simpan data sukses');
		    location.reload();  
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Simpan data gagal');      
		},
	    cache: false,
		contentType: false,
		processData: false
	});
	e.preventDefault(); //STOP default action
	return false;
	});//-------END SUBMIT karyawan-------------------------------------
	
	
		
});

// $('input[name="periodemulaikenaikan"]').bind("propertychange input paste", function(){
	// var val_ = $(this).val();
	// get_thn_bln(val_);
// });

$('input[name="periodemulaikenaikan"]').on('changeDate', function () {
    var val_ = $(this).val();
	get_thn_bln(val_);
});

function get_thn_bln(input){
	var mulai = $('input[name="tglmskstaff"]').val();
	var start = convert_(mulai);
	start = new Date(start);
	input = new Date(input);
	
	var months = input.getMonth() - start.getMonth() + (12 * (input.getFullYear() - start.getFullYear()));
	var year   = parseInt(months/12)
	var month  = (parseInt(months%12)+1);
	// alert(year);alert(month);
	$('input[name="tahun-kerja"]').val(year);
	$('input[name="bulan-kerja"]').val(month);
	
}


$(".select_library").click(function(){
		setTimeout(function(){
			$(".library_attach").show();
		},200);
	});


function get_unit(content_fakultas_id,content_unit_id){
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/gjm/read_unit",
      data : $.param({
		fakultasid : content_fakultas_id,
		unit_id : content_unit_id
	}),
       success: function(data){
	       $('#select_unit').html(data);
       }
    });
}

function get_unit_id(content_fakultas_id,content_unit_id){
	$.ajax({
	  type : "POST",
	  dataType: "HTML",
      url: base_url + "/module/master/gjm/read_unit",
      data : $.param({
		fakultasid : content_fakultas_id,
		unit_id : content_unit_id
	}),
       success: function(data){
	       $('#select_unit_id').html(data);
       }
    });
}



function select_img(id,filename){
	var info = filename;
	info += '&nbsp;<a href="#" onclick="remove_selected_img()"><i class="fa fa-trash-o"></i></a>';
	info += '<input type="hidden" name="docid" value="'+id+'" />';
	$('#document-place').html(info);
	$('#selectfile').modal('hide');
}

function remove_selected_img(){
	$('#document-place').empty();
}

$('#cancel-unit').click(function(){
	$('#select_unit_kunit').select2().select2('val','-');
	$('input[name="periodemulai"]').val('');
	$('input[name="periodeselesai"]').val('');
	$('input[name="periodeselesai"]').prop('disabled', false);
	$('input[name="current"]').prop('checked', false);
	$('input[name="isaktif"]').prop('checked', false);
	$('input[name="hidIdval"]').val('');
	$('#cancel-unit').hide();
});



$('input[name="isaktif"]').click(function() {
		if( this.checked ){
			$('input[name="isaktif"]').parent().find('b').html("&nbsp;Aktif");
		}else{
			$('input[name="isaktif"]').parent().find('b').html("&nbsp;Tidak Aktif");
		}
});


function show_info(id){
	var stat = $('#'+id+'-info').attr('class');
	
	if(stat=='hidden'){
		$('#'+id+'-info').fadeIn();
		$('#'+id+'-info').attr('class','visible');
	}else{
		$('#'+id+'-info').fadeOut();
		$('#'+id+'-info').attr('class','hidden');
	}
	
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#ava').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
};

