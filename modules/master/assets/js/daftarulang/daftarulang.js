$(document).ready(function() {
	$("#select_indexfakultas").change(function() {
		$("#export-data").hide();
		var fakultas_id = $("#select_indexfakultas").val();
		//alert(fakultas_id);
		$("#select_indexcabang").select2("val","0");
		$("#display").empty();
		$("select[name='tahun_akademik']").select2("val","0");
		$("#select_indexangkatan").select2("val","0");
		$("#select_indexangkatan").attr("disabled");
		$("#select_indexangkatan").select2("enable", false);
		
		if(fakultas_id == 0){
			$("#select_indexcabang").attr("disabled");
			$("#select_indexcabang").select2("enable", false);
		}
		else{
			$("#select_indexcabang").removeAttr("disabled");
			$("#select_indexcabang").select2("enable", true);
		}
	});
	
	$("#select_indexcabang").change(function() {
		$("#export-data").hide();
		$("#display").empty();
		var cabang_ = $("#select_indexcabang").val();
		var fakultas_id_ = $("#select_indexfakultas").val();
		var uri = $(this).data("uri");
		
		$("select[name='tahun_akademik']").select2("val","0");
		$("#select_indexangkatan").select2("val","0");
		
		if(cabang_ == 0){
			$("#select_indexangkatan").attr("disabled");
			$("#select_indexangkatan").select2("enable", false);
		}
		else{
			//alert("TES");
			$.ajax({
				type : "POST",
				dataType : "html",
				url : uri,
				data : $.param({
					fakultas_id : fakultas_id_,
					cabang : cabang_
				}),
				success : function(msg) {
					// alert(msg);
					if (msg == '') {
						$("#select_indexangkatan").html('<option value="0">Select Angkatan</option>');
						$("#select_indexangkatan").attr("disabled","disabled");
					} else {
						// alert(uri);
						$("#select_indexangkatan").removeAttr("disabled");
						$("#select_indexangkatan").html(msg);
						$("#select_indexangkatan").select2("enable", true);
						
					}
				}
			});
		}
		
	});
	
	$("select[name='select_data']").change(function() {
		/*$("#export-data").hide();
		$("#display").empty();
		$("select[name='tahun_akademik']").select2("val","0");	
		$("#select_indexangkatan").select2("val","0");	*/
		$("#display").empty();
			show_data();
	});
	
	$("select[name='tahun_akademik']").change(function() {
		/*$("#export-data").hide();
		$("#display").empty();
		$("#select_indexangkatan").select2("val","0");		*/
		$("#display").empty();
		show_data();
	});
	
	$("#select_indexangkatan").change(function() {
		$("#export-data").hide();
		$("#display").empty();
		show_data();
		/*var fakultas_id_ = $("#select_indexfakultas").val();
		var cabang_ = $("#select_indexcabang").val();
		var data_ = $("select[name='select_data']").val();
		var tahun_akademik_ = $("select[name='tahun_akademik']").val();
		var angkatan_ = $("#select_indexangkatan").val();
		var uri = $(this).data("uri");
	//	alert(angkatan);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id  : fakultas_id_,
				cabang 		 : cabang_,
				data 		 : data_,
				tahun_akademik : tahun_akademik_,
				angkatan	 : angkatan_
			}),
			success : function(msg) {
				if (msg == "<div class='well'>Sorry, no content to show</div>") {
					$("#export-data").hide();
					$("#display").html('<div class="well"><center>Sorry no content to show</center></div>');
					
				} else {
					$("#display").html(msg);
					$("#example").dataTable({ 
						"sDom": "<'row'<'col-sm-12'<'pull-right'f><'pull-left'>r<'clearfix'>>>t<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>",
						"sPaginationType": "bootstrap",
						"aaSorting": [],
						"bSort":false,
						"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"sSearch": ""
						},
						bRetrieve : true}).fnDraw();
					$("#export-data").show();
					$("#export-data").click(function() {
						//ExcellentExport.excel(this, 'my-view-Table', 'mahasiswa');
						var form = document.createElement("form");
						var input = document.createElement("input");
						var input2 = document.createElement("input");
						var input3 = document.createElement("input");
						var input4 = document.createElement("input");
						var input5 = document.createElement("input");
						
						form.action = base_url + 'module/master/daftar/export';
						form.method = "post"
						
						input.name = "fakultas_id";
						input.value = fakultas_id_;
						form.appendChild(input);
						
						input2.name = "cabang";
						input2.value = cabang_;
						form.appendChild(input2);
						
						input3.name = "data";
						input3.value = data_;
						form.appendChild(input3);
						
						input4.name = "angkatan";
						input4.value = angkatan_;
						form.appendChild(input4);
						
						input5.name = "tahun_akademik";
						input5.value = tahun_akademik_;
						form.appendChild(input5);
						
						document.body.appendChild(form);
						form.submit();
					});
				}
			}
		});*/
	});
	
	$(".e9").select2();
	
	
});

function validate(id,mhs){
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url+'module/master/daftar/validate',
		data : $.param({
			id  : id,
			mhs	 : mhs
		}),
		success : function(msg) {
			alert(msg);
			location.reload();
		}
	});
};

function show_data(){
	$("#export-data").hide();
		var fakultas_id_ = $("#select_indexfakultas").val();
		var cabang_ = $("#select_indexcabang").val();
		var data_ = $("select[name='select_data']").val();
		var tahun_akademik_ = $("select[name='tahun_akademik']").val();
		var angkatan_ = $("#select_indexangkatan").val();
		//var uri = $(this).data("uri");
		var uri=base_url+'module/master/daftar/tampilkan_index';
	//	alert(angkatan);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id  : fakultas_id_,
				cabang 		 : cabang_,
				data 		 : data_,
				tahun_akademik : tahun_akademik_,
				angkatan	 : angkatan_
			}),
			success : function(msg) {
				if (msg == "<div class='well'>Sorry, no content to show</div>") {
					$("#export-data").hide();
					$("#display").html('<div class="well"><center>Sorry no content to show</center></div>');
					
				} else {
					$("#display").html(msg);
					$("#example").dataTable({ 
						"sDom": "<'row'<'col-sm-12'<'pull-right'f><'pull-left'>r<'clearfix'>>>t<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>",
						"sPaginationType": "bootstrap",
						"aaSorting": [],
						"bSort":false,
						"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"sSearch": ""
						},
						bRetrieve : true}).fnDraw();
					$("#export-data").show();
					$("#export-data").click(function() {
						//ExcellentExport.excel(this, 'my-view-Table', 'mahasiswa');
						var form = document.createElement("form");
						var input = document.createElement("input");
						var input2 = document.createElement("input");
						var input3 = document.createElement("input");
						var input4 = document.createElement("input");
						var input5 = document.createElement("input");
						
						form.action = base_url + 'module/master/daftar/export';
						form.method = "post"
						
						input.name = "fakultas_id";
						input.value = fakultas_id_;
						form.appendChild(input);
						
						input2.name = "cabang";
						input2.value = cabang_;
						form.appendChild(input2);
						
						input3.name = "data";
						input3.value = data_;
						form.appendChild(input3);
						
						input4.name = "angkatan";
						input4.value = angkatan_;
						form.appendChild(input4);
						
						input5.name = "tahun_akademik";
						input5.value = tahun_akademik_;
						form.appendChild(input5);
						
						document.body.appendChild(form);
						form.submit();
					});
				}
			}
		});
}