<?php 
 $this->head();	  
 
 $header="Comment and Feedback";
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<div class="row">
		<div class="col-md-12">	
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="#"><?php echo $header;?></a></li>
			 <li class="active"><a href="#">Data</a></li>
			</ol>
			<div class="row">
				<div class="col-md-4">
					<div class="block-box">
						<div class="content">	
						<form method="post" id="form_category_comment" action="<?php echo $this->location("module/content/page/comments/"); ?>">
							<div class="form-group">
								<label class="control-label">Category</label>
								<select class="form-control" name="category_index">
									<option value="0">Silahkan Pilih</option>
									<option value="comment" <?php if(isset($category_index) && $category_index=="comment") echo "selected" ?>>Comment</option>
									<option value="feedback" <?php if(isset($category_index) && $category_index=="feedback") echo "selected" ?>>Feedback</option>
								</select>
							</div>
						</form>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="block-box">
						<div class="content">
						 <?php
						 if( isset($comment) && $comment!="") :	?>
							
							<table class='table table-hover' id='example'>
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
							<?php
								$i = 1;
								if($comment > 0){
									foreach ($comment as $dt): 
									?>
										<tr valign="top" id="comment<?php echo $dt->id ?>">
											<td style="max-width: 50px">
												<small><i class="fa fa-clock-o"></i> <?php echo $dt->comment_post; ?>
												<br>By <span class="text text-danger"><?php echo $dt->user_name . "</span> <i class='fa fa-leaf'></i> ". $dt->ip_address ?>
												</small>
											</td>
											<td style="max-width: 100px">
												<b><?php echo $dt->content_title; ?></b>
												<?php
													switch($dt->is_approve){
														case 0 : $status = "<span class='label label-warning'>Need To Process</span>";
																 break;
														case 1 : $status = "<span class='label label-success'>Approved</span>";
																 break;
														case 2 : $status = "<span class='label label-danger'>Rejected</span>";
																 break;
													}
													echo $status;
														?>
												<br><small><?php echo ucfirst($dt->comment) ?></small>
											</td>
											<td style="max-width: 30px">
												<ul class='nav nav-pills'>
													<li class='dropdown pull-right'>
													  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
													  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
														<li>
														<a class='btn-edit-post' href="#" onclick="updateStatus('<?php echo $dt->comment_id ?>','1','<?php echo $dt->category ?>')"><i class='fa fa-check'></i> Approve</a>
														<a class='btn-edit-post' href="#" onclick="updateStatus('<?php echo $dt->comment_id ?>','2','<?php echo $dt->category ?>')"><i class='fa fa-ban'></i> Reject</a>
														</li>
														<li>
														<a class='btn-edit-post' href="#" onclick="doDelete('<?php echo $dt->comment_id ?>','<?php echo $dt->category ?>')"><i class='fa fa-trash-o'></i> Delete</a>
														</li>	
													  </ul>
													</li>
												</ul>
											</td>
										</tr>
										<?php
										$i++;
									 endforeach; 
								 } ?>
									</tbody>
							</table>
						<?php
						 else: 
						 ?>
						<div class="span3" align="center" style="margin-top:20px;">
							<div class="well">Sorry, no content to show</div>
						</div>
						<?php endif; ?>
						</div>
					</div>
				</div>
		    </div>
	    </div>
    </div> <!-- row -->
<?php
$this->foot();
?>