<?php if(isset($absen)): ?>
	
<?php 
$num = 1;
$kryn='';
$karyawannum = Array();
foreach ($absen as $dt) {
	if(isset($dt->karyawan_id)){
		if($kryn == $dt->karyawan_id){
			$karyawannum[$dt->karyawan_id] = $num++;
		}else{
			$num = 1;
			$karyawannum[$dt->karyawan_id] = 0;
		}
		$kryn = $dt->karyawan_id;
		$type = "dosen";
	}
	elseif(isset($dt->mahasiswa_id)){
		if($kryn == $dt->mahasiswa_id){
			$karyawannum[$dt->mahasiswa_id] = $num++;
		}else{
			$num = 1;
			$karyawannum[$dt->mahasiswa_id] = 0;
		}
		$kryn = $dt->mahasiswa_id;
		$type = "mahasiswa";
	}
}
// print_r($karyawannum);
?>

	
<?php //echo $prodi; ?>
<table class="table table-bordered table-condensed">
<thead>
	<tr>
		<th><center>No</center></th>
		<th><center>Nama</center></th>
		<?php if($type=="dosen"){ ?>
		<th><center>Gol</center></th>
		<?php } ?>
		<th><center>Prodi</center></th>
		<th><center>Mata Kuliah</center></th>
		<th><center>Kelas</center></th>
		<?php if($type=="dosen"){ ?>
		<th><center>SKS</center></th>
		<?php } ?>
		<th><center>Jumlah Absensi</center></th>
	</tr>
</thead>
<tbody>
	
	<?php $num = 1; $kryn='';?>
	<?php foreach ($absen as $dt) { 
			$praktikum = $mconf->get_mk_praktikum($dt->mkditawarkan_id);
				if($praktikum):
					$ispraktikum = 1;
					$sks_valid = $dt->sks - 1;
					$strclass= "panel-danger";
				else:
					$ispraktikum = "";
					$sks_valid = $dt->sks;
					$strclass= "panel-default";
				endif;
			?>
	<?php if(isset($dt->karyawan_id)){ ?>
	
	<tr>
		<?php if($kryn != $dt->karyawan_id){ ?>
			<td align="center" valign="center" <?php if($kryn != $dt->karyawan_id)echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?> >
			<?php 
			if($kryn != $dt->karyawan_id){
				echo $no++;
			}
			?>
			</td>
			<td <?php if($kryn != $dt->karyawan_id)echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?>>
				<?php 
				if($kryn != $dt->karyawan_id){
					echo $dt->nama;
					if($dt->nik=='') echo "<br><small><strong>NIK. -</strong></small>";
					else echo "<br><small><strong>NIK. ". $dt->nik ."</strong></small>";
				}
				?>
				
			</td>
			<td <?php if($kryn != $dt->karyawan_id) echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?>>
				<?php if($kryn != $dt->karyawan_id){ echo $dt->golongan; } ?>
				<?php $kryn = $dt->karyawan_id; ?>
			</td>
		<?php } ?>
			<td>
			<?php 				
					
					echo $dt->prodi_mk;
			?>
			</td>
		
		<td>
			<?php 				
					echo "[".$dt->kode_mk."] ";
					echo $dt->namamk;
					if($ispraktikum) echo "*";			
			?>
		</td>
		<td>
			<?php 				
					
					echo $dt->kelas;
			?>
			</td>
		<td><center>
			<?php 
				if($sks_valid > 3){
					$sks= ($sks_valid/2);
					//echo $dt->sks;
				}else{
					$sks= $sks_valid;					
				}				
			?><?php echo $sks ?> sks</center>
		</td>
		<td><center>
			<h4><?php echo $dt->jml; ?></h4></center>
		</td>
	</tr>
	<?php } ?>
	
	
	<!--ASISTEN-->
	<?php if(isset($dt->mahasiswa_id)){ ?>
		<tr>
			<?php if($kryn != $dt->mahasiswa_id){ ?>
				<td align="center" valign="center" <?php if($kryn != $dt->mahasiswa_id)echo 'rowspan="'.($karyawannum[$dt->mahasiswa_id]+1).'"' ?> >
				<?php 
				if($kryn != $dt->mahasiswa_id){
					$index = $no;
					echo $no++;
				}
				?>
				</td>
				<td <?php if($kryn != $dt->mahasiswa_id)echo 'rowspan="'.($karyawannum[$dt->mahasiswa_id]+1).'"' ?>>
					<?php 
					if($kryn != $dt->mahasiswa_id){
						echo $dt->nama;
						if($dt->nim=='') echo "<br><small><strong>NIK. -</strong></small>";
						else echo "<br><small><strong>NIK. ". $dt->nim ."</strong></small>";
					}
					?>
					<?php $kryn = $dt->mahasiswa_id; ?>
				</td>
			<?php } ?>
			<td>
				<?php 				
						
						echo $dt->prodi_mk;	
				?>
				
			</td>
			<td>
				<?php 				
						echo "[".$dt->kode_mk."] ";
						echo $dt->namamk;
						if($ispraktikum) echo "*";			
				?>
				
			</td>
			<td>
				<?php 				
						
						echo $dt->kelas;	
				?>
				
			</td>
			<td><center>
				<h4><span class="text text-danger"><?php echo $dt->jml; ?></span></h4></center>
			</td>
		</tr>
		
	<?php } ?>
	<?php } ?>
	
</tbody>
</table>
<?php else: ?>
	<div class="well" align="center">
		No Data Show
	</div>
<?php endif; ?>
<script src="<?php echo $this->asset("js/jsread.js"); ?>"></script>
