<?php 
 $this->head();
 $header="Rancangan MK Pilihan";
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<div class="row">
	<div class="col-md-12">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#"><?php echo $header;?></a></li>
		  <li class="active"><a href="#">Data</a></li>
		</ol>
		<div class="row">
			<div class="col-md-6">
				<div class="block-box">
					<form id="form-rancangan-mk">
						<div class="form-group">
							 <label class="control-label">Fakultas</label>
							 <select class="form-control" name="fakultas">
							 	<option value="0">Silihkan Pilih</option>
							 	<?php 
						 		  foreach($fakultas as $f){
									echo "<option value='".$f->unit_id."'";
									echo ">".$f->fakultas."</option>";
								  } 
								?>
							 </select>
						</div>
						<legend>Semester</legend>
						<div class="form-group">
							 <label class="control-label">Tahun</label>
							 <input type="text" required="required" class="form-control" name="tahun" id="tahun" placeholder="Tahun" value="<?php if(isset($edit)) echo $edit->tahun; ?>"/>
						</div>
						<div class="form-group">
							 <label class="control-label">Semester</label>
							 <select class="form-control" name="semester">
							 	<option value="0">Silihkan Pilih</option>
							 	<option value="ganjil" <?php if(isset($edit)&&$edit->is_ganjil=="ganjil") echo "selected"; ?>>Ganjil</option>
							 	<option value="genap" <?php if(isset($edit)&&$edit->is_ganjil=="genap") echo "selected"; ?>>Genap</option>
							 </select>
						</div>
						<div class="form-group">
							 <label class="checkbox"><input type="checkbox" name="is_pendek" id="is_pendek" value="1" <?php if(isset($edit)&&$edit->is_pendek=="Pendek") echo "checked"; ?>> Semester Pendek?</label>
						</div><br><br>
						<legend>General Info</legend>
						<div class="form-group">
							 <label class="control-label">Judul</label>
							 <input type="text" required="required" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php if(isset($edit)) echo $edit->judul; ?>"/>
						</div>
						<div class="form-group">
							 <label class="control-label">Keterangan</label>
							 <textarea class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan"><?php if(isset($edit)) echo $edit->keterangan; else echo "-" ?></textarea>
						</div>
						<div class="form-group">
							 <label class="control-label">Pelaksanaan</label>
							 <div class="row">
								 <div class="col-md-5">
								 	<input type="text" name="tgl_mulai" class="form-control date" placeholder="Tanggal Mulai" value="<?php if(isset($edit)) echo $edit->tgl_mulai; ?>"/>
								 </div>
								 <div class="col-md-2">
								 	sampai
								 </div>
								 <div class="col-md-5">
								 	<input type="text" name="tgl_selesai" class="form-control date" placeholder="Tanggal Selesai" value="<?php if(isset($edit)) echo $edit->tgl_selesai; ?>"/>
								 </div>
							 </div>
						</div>
						<div class="form-group">
							 <label class="checkbox"><input type="checkbox" name="is_aktif" id="is_aktif" value="1" <?php if(isset($edit)&&$edit->is_aktif==1) echo "checked"; ?>> Aktif?</label>
						</div><br><br>
						<legend>Setting MK</legend>
						<?php 
							  $i=0;
							  foreach($prodi as $p) {?>
							<div class="form-group">
								 <label class="control-label"><?php echo $p->keterangan; ?></label>
								 <select class="form-control e9" multiple="multiple" name="<?php echo $p->unit_id."[]"; ?>">
								 	<?php 
							 		  foreach($mk as $dt){
										echo "<option value='".$dt->id."'";
										if(isset($detail)){
											foreach($detail as $det){
												if($dt->id == $det->matakuliah_id && $p->unit_id == $det->prodi_id){
													echo "selected";
												}
											}
										}
										echo ">".$dt->value."</option>";
									  } 
									?>
								 </select>
							</div>
						<?php $i++;
							  } ?>
						<br>
						<div class="form-group">
							 <input type="text" name="kuesionerid" class="form-control" value="<?php if(isset($edit)) echo $edit->kuesioner_id; ?>">
							 <button type="submit" class="btn btn-primary">Save Konfigurasi</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6">
				<div class="block-box">
					<table class="table example">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php if(isset($posts)){
									foreach($posts as $p){?>
										<tr>
											<td>
												<?php 
													$tgl_mulai 		= date("M d, Y", strtotime($p->tgl_mulai));
													$tgl_selesai 	= date("M d, Y", strtotime($p->tgl_selesai));
													$thn_akademik	= $p->tahun." ".$p->is_ganjil." ".$p->is_pendek;
													if($p->is_aktif){
														$aktif = "<label class='label label-success'>Aktif</label>";
													}
													else $aktif = "";
													
													echo "<b>".$p->judul."</b><br><code>".ucwords($thn_akademik)."</code>&nbsp;&nbsp;". $aktif ."<br>";
													echo "<i class='fa fa-calendar-o'></i> ".$tgl_mulai;
													echo " - ".$tgl_selesai;
												?>												
											</td>
											<td>
												<ul class='nav nav-pills'>
													<li class='dropdown pull-right'>
													  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
													  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
														<li>	
															<a class='btn-edit-post' href="javascript:" onclick="edit('<?php echo $p->kuesionerid ?>')"><i class='fa fa-edit'></i> Edit</a>
														</li>
													  </ul>
													</li>
												</ul>
											</td>
										</tr>
							<?php 	}
								  } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
 $this->foot();
?>