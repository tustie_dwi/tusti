<?php $this->head(); ?>

	<legend><a href="<?php echo $this->location('module/master/prestasi/write'); ?>" class="btn btn-primary pull-right">
    <i class="icon-pencil icon-white"></i> New Prestasi</a> Prestasi List
	</legend>
	<div class="block-box">
	 <?php 
	if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	if( isset($posts) ) :	
	?>
	
	<table class='table table-hover' id='example'>
		<thead>
			<tr>
				<th style="display:none">&nbsp;</th>
				<th>Pelaksanaan</th>
					
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php				
			$i = 0;		
			if($posts){			
				foreach ($posts as $dt): 
					$i++;
					?>
					<tr>
						<td style="display:none"></td>
						<td>
						<div class="col-md-6">
							<span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
								echo date("M d, Y", strtotime($dt->tgl_mulai)); 
								if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
									echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
								}
							?>
							</span><br>
							<?php echo "<b>".$dt->judul."</b> <span class='label label-success'>".UcWords(strToLower($dt->jenis_id))."</span>"; ?><br>
							 
							<small><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara ?>
							&nbsp; <i class="fa fa-map-marker"></i> <?php echo $dt->lokasi ?></small>
							<?php if($dt->link_berita) echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-search'></i> <b><a href=".$dt->link_berita." target='_blank' class='text text-danger'>Baca Berita</a></b></span></small>" ?>
						
						</div>
						<div class="col-md-6">
						
						<?php 
						
						$mhs = explode(",", $dt->mhs);
						
						for($j=0;$j<count($mhs);$j++){ 
							$str = explode("-",$mhs[$j]);
							echo "<code>".$str[1]."</code> ";
						}
					
						?><br><small><?php echo $dt->inf_prestasi; ?></small>&nbsp;<span class="label label-warning"><?php echo ucwords($dt->tingkat); ?></span>
						&nbsp;<span class="label label-danger"><?php echo $dt->jenis_prestasi; ?></span></div></td>
						<td>        
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-default' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/master/prestasi/write/'.$dt->id); ?>"><i class='fa fa-pencil'></i> Edit</a>	
									</li>	
										<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/master/prestasi/delete/'.$dt->prestasi_id); ?>"><i class='fa fa-trash-o'></i> Delete</a>	
									</li>										
								  </ul>
								</li>
							</ul></td>
					</tr>
					<?php
				endforeach; 
			}
		?>
		</tbody></table>
	<?php
	 else: 
	 ?>
    <div class="col-md-12" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</div>
<?php $this->foot(); ?>