<?php
$this->head();

if(isset($posts)){
	$header		= "Edit Prestasi";		
	foreach($posts as $dt):
		$data['id']			= $dt->prestasi_id;
		$data['judul']		= $dt->judul;
		$data['semester']	= $dt->tahun_akademik;
		$data['ljenis']		= $dt->jenis_id;
		$data['penyelenggara']	= $dt->penyelenggara;
		$data['lokasi']		= $dt->lokasi;
		$data['mulai']		= $dt->tgl_mulai;
		$data['selesai']	= $dt->tgl_selesai;
		$data['mulai']		= $dt->tgl_mulai;
		$data['prestasi']	= $dt->inf_prestasi;
		$data['tingkat']	= $dt->tingkat;
		$data['jprestasi']	= $dt->jenis_prestasi;
		$data['infpeserta']	= $dt->inf_peserta;
		$data['catatan']	= $dt->keterangan;
		$data['mhs']		= $dt->mhs;
		$data['linkberita']		= $dt->link_berita;
	endforeach;
}else{
	$header		= "Write New Prestasi";
	$data['id']			= "";
	$data['judul']		= "";
	$data['semester']	= "";
	$data['ljenis']		= "";
	$data['penyelenggara']	= "";
	$data['lokasi']		= "";
	$data['mulai']		= "";
	$data['selesai']	= "";
	$data['mulai']		= "";
	$data['prestasi']	= "";
	$data['tingkat']	= "";
	$data['jprestasi']	= "non";
	$data['catatan']	= "";
	$data['mhs']		= "";
	$data['infpeserta']	= "";
	$data['linkberita']	= "";
}
?>


	
	<legend>
		<a href="<?php echo $this->location('module/master/prestasi'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> Prestasi List</a> <?php echo $header; ?>
    </legend> 
	<div class="block-box">  
         
		<form method=post  action="<?php echo $this->location('module/master/prestasi/save'); ?>" class="form-horizontal">
			<div class="form-group">
				<label class="control-label">Tahun Akademik</label>
				
					<select name="cmbsemester" class="form-control">
					<?php
					if(isset($semester)){
						foreach($semester as $dt):
							echo "<option value=".$dt->tahun_akademik." ";
							if($data['semester']==$dt->tahun_akademik){ echo "selected"; } 
							echo ">".$dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek."</option>";
						endforeach;
					}
					?>
					</select>
			</div>
			
			<div class="form-group">
				<label class="control-label">Jenis Lomba</label>
					
					<select name="cmbjenislomba" class="form-control">
					<?php
					if(isset($jenislomba)){
						foreach($jenislomba as $dt):
							echo "<option value=".$dt->id." ";
							if($data['ljenis']==$dt->id){ echo "selected"; } 
							echo ">".$dt->value." </option>";
						endforeach;
					}
					?>
					</select>
			</div>
			
				
			
				<ul class="nav nav-tabs" id="writeTab">
					<li class="active"><a href="#info">General Info</a></li>
					<li><a href="#peserta">Peserta</a></li>
					<li><a href="#prestasi">Prestasi</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="info">
						<?php $this->view('prestasi/general.php', $data); ?>
					</div>
					<div class="tab-pane" id="peserta">
						<?php $this->view('prestasi/peserta.php', $data); ?>
					</div>
					<div class="tab-pane" id="prestasi">
						<?php $this->view('prestasi/prestasi.php', $data); ?>
					</div>
				</div>
				<div class="form-actions">
					<input type="hidden" name="hidId" value="<?php echo $data['id']; ?>">
					<input type="submit" name="b_prestasi" id="b_prestasi" value="  Save & Publish  " class="btn btn-primary">
				</div>
			
		</form>
	</div>
</div>
<?php	
$this->foot();

?>