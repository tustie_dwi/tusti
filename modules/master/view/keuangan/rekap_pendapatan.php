<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Keuangan</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Keuangan</a></li>
		  <li class="active"><a onclick="tes()" href="#">Rekap Keuangan per Periode</a></li>
		</ol>
		
	</div>
</div>

	
	<div class="col-md-12 block-box">
		 <div class="pull-right hide-from-print">			    	
			<a class="btn btn-danger btn-cetak-excel"><i class="fa fa-file"></i> Export To Excel</a>
		</div>
		<!--<div class="block-box hide-from-print">
			<div class="row">
								
				<div class="col-md-6">
					<input id="thn" onchange="get_transaksi_per_tahun(this.value)" class="form-control" type="number" max="<?php echo date('Y') ?>" value="<?php echo $tahun ?>" />
				</div>
			</div>
		</div>-->
		
		
			<?php
				if(isset($posts)) :
			?>
			<table class="table table-hover example">
					<thead>
						<tr valign="top">
							<!--<th rowspan="2"><!--<input type="checkbox" id="checkAll"></th>-->
							<th style="display:none"></th>
							<th>Dosen</th>						
							<!--<th>Periode Mulai</th>
							<th>Periode Selesai</th>-->
							<th>Total</th>
							<th>bagi 11</th>
							<th>x 14</th>
							<th>x 28</th>
						</tr>					
						
					</thead>
					<tbody>
			<?php
			foreach($posts as $key){
			
				if(($key->gelar_awal)&&($key->gelar_awal!="-")){
					$gawal= ", ".$key->gelar_awal;
				}else{
					$gawal="";
				}
				
				if(($key->gelar_akhir)&&($key->gelar_akhir!="-")){
					$gakhir= ", ".$key->gelar_akhir;
				}else{
					$gakhir="";
				}
								
								
				?>
					<tr>
							<td style="display:none"></td>
						<td><?php echo $key->nama.$gawal.$gakhir ?></td>
						<!--<td><?php //echo date("M d, Y", strtoTime($key->tgl_mulai)) ?> </td><td><?php //echo date("M d, Y", strtoTime($key->tgl_selesai))  ?></td>-->
						<td align="right"><?php echo number_format($key->total,2) ?></td>
						<td align="right"><?php echo number_format($key->total/11,2) ?></td>
						<td align="right"><?php echo number_format(($key->total/11) * 14,2) ?></td>
						<td align="right"><?php echo number_format(($key->total/11) * 28,2) ?></td>
					</tr>
				<?php
			
			}
			?>
			</tbody>
			</table>
			<?php
		endif;
			?>
	</div>
	
	

<?php $this->foot(); ?>

<script type="text/javascript">
	
	$(document).ready(function(){
		$(".e9").select2();
		
		$("#sel_bulan").val();
		$(".btn-cetak-excel").click(function(){
			
			window.open(base_url + 'module/master/keuangan/rekap_pendapatan',"_blank");	
			//get_transaksi_per_tahun();
		});
		
		//get_transaksi_per_tahun();
		//get_transaksi_per_periode();
	});
	
	function convert_bulan(init){
		switch(init){
			case "1" : return "Januari";
			case "2" : return "Februari";
			case "3" : return "Maret";
			case "4" : return "April";
			case "5" : return "Mei";
			case "6" : return "Juni";
			case "7" : return "Juli";
			case "8" : return "Agustus";
			case "9" : return "September";
			case "10" : return "Oktober";
			case "11" : return "November";
			case "12" : return "Desember";
			
			default : return "---";
		}
	}
	
	function get_transaksi_per_tahun(){
		var uri = base_url + 'module/master/keuangan/get_rekap_pendapatan';
		//var thn = $("#thn").val();
		
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : uri,
			//data : $.param({tahun : thn}),
			success:function(msg){
				$(".view-data").html(msg);
			}
		});
	}
	
	
	
</script>

<script src="<?php echo $this->asset("js/jsread.js"); ?>"></script>




