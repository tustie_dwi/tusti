<?php $this->head(); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel" id="panel_keu">
				<div class="panel-heading">
					<button class="btn btn-sm btn-primary pull-right" onclick="tambahKegiatanKeu();">Tambah Kegiatan Bayar</button>
					<h4 class="panel-title">
						Pembayaran - Keuangan
					</h4>
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<th>
								Kegiatan
							</th>
						</thead>
						<tbody id="agenda_kegiatan">
							<?php
								 if($kegiatan)
								 foreach ($kegiatan as $key => $value) {
									 echo "<tr>
									 		<td>
									 			<a class='btn btn-danger btn-xs pull-right' onclick='deleteKeu(\"".$value->bayarid."\",this)'><i class='fa fa-trash-o'></i> Delete</a>
									 			<a class='btn btn-warning btn-xs pull-right' data-param='".base64_encode(json_encode($value))."' onclick='editKeu(this)'><i class='fa fa-trash-o'></i> Edit</a>
									 			<a class='btn btn-primary btn-xs pull-right' style='margin:0px 3px' onclick='editPesertaKeu(\"".$value->bayar_id."\",this)'><i class='fa fa-users'></i> Peserta</a>
									 			<a class='btn btn-default btn-xs pull-right' style='margin:0px 3px' href='".$this->location('module/master/keuangan/cetak/kegiatan/'.$value->bayarid)."' target='_blank' ><i class='fa fa-print'></i> Cetak</a>
									 			<p>$value->keterangan</p>
										 		Periode : $value->periode : <i class='fa fa-calendar'></i> $value->tgl_mulai - <i class='fa fa-calendar'></i> $value->tgl_selesai
									 		</td></tr>";
								 }
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<form class="form-horizontal" id="form-keu">
			<div class="panel">
				<div class="panel-heading">
					<button type="button" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#modalSettingKategoriBayar">
					  <i class='fa fa-cog'></i> Kategori Bayar
					</button>
					<h4 class="panel-title">Pembayaran Kegiatan</h4>
				</div>
				<div class="panel-body" style="padding:30px">
					<div class="form-group">
						<label>Jenis Pembayaran</label>
						<select class="form-control" name="kategori_bayar" id="kategori_bayar">
							<?php
								foreach ($kategori_bayar as $key => $value) {
									echo "<option value='$value->kategori_bayar'>$value->keterangan</option>";
								}
							?>
						</select>
						<input type="hidden" name="bayar_id" id="bayar_id" disabled="" />
					</div>
					<div class="form-group">
						<label>Tgl Kegiatan</label>
						<div class="row">
							<div class="col-md-6">
								<input type="text" required class="form-control date" name="tgl_mulai" id="tgl_mulai" placeholder="Mulai..">
							</div>
							<div class="col-md-6">
								<input type="text" required class="form-control date" name="tgl_selesai" id="tgl_selesai" placeholder="Selesai..">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Tgl Bayar</label>
						<input type="text" required class="form-control date" name="tgl_bayar" id="tgl_bayar" placeholder="Tgl Pembayaran..">
					</div>
					<div class="form-group">
						<label>Nama Agenda</label>
						<input type="text" required class="form-control namaKegiatan" name="keterangan" id="keterangan" placeholder="Nama Kegiatan..">
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-success btn-sm" type="button" onclick="submitKeuBayar()">Simpan</button>
					<button class="btn btn-default btn-sm" type="button" onclick="cancelkeuBayar()">Cancel</button>
				</div>
			</div>
			</form>
		</div>
		<div class="col-md-12">
			<div class="panel" id="panel-peserta-keu" style="display: none">
				<div class="panel-heading">
					<button class="btn btn-primary btn-xs pull-right" type="button" id="tambahPeserta" onclick="tambahPesertaKeu()"><i class='fa fa-plus'></i> Tambah Peserta</button>
					<h4 class="panel-title">Honor Peserta</h4>
				</div>
				<div class="panel-body" id="">
					<div class="row">
						<div class="col-md-2 form-group">
							<label>Nama</label>
						</div>
						<div class="col-md-2 form-group">
					   		<label>Keterangan Peserta</label>
						</div>
						<div class="col-md-2 form-group">
							<label>Satuan</label>
						</div>
						<div class="col-md-2 form-group">
							<label>Jumlah</label>
						</div>
						<div class="col-md-2 form-group">
							<label>Total</label>
						</div>
						<div class="col-md-2 form-group" id="tombol">
							#
						</div>
					</div>
					<div id="peserta-keu">
						<form id="form-detail-keu">
							<div class="row">
								<div class="col-md-2 form-group">
						    		<input type="text" class="form-control namaKaryawan" id="nama" name="nama" placeholder="Nama" required="required">
						    		<input type="hidden" class="bayar_id_detail" name="bayar_id">
						    		<input type="hidden" id="karyawan_id" name="karyawan_id">
						    		<input type="hidden" id="hr_id" name="hr_id">
						  		</div>
						  		<div class="col-md-2 form-group">
						    		<input type="text" class="form-control" id="keterangan_detail" name="keterangan" placeholder="Keterangan Peserta" required="required">
						  		</div>
						  		<div class="col-md-2 form-group">
						    		<input type="number" class="form-control satuan" id="satuan" name="satuan" placeholder="Satuan" required="required" value="0">
						  		</div>
						  		<div class="col-md-2 form-group">
						    		<input type="number" class="form-control qty" id="qty" name="qty" placeholder="Quantity" required="required" value="1" onkeyup="tambahOnEnter(event)">
						  		</div>
						  		<div class="col-md-2 form-group">
						    		<input type="number" class="form-control total" id="total" name="total" placeholder="Total" required="required" value="0" onkeyup="tambahOnEnter(event)">
						  		</div>
						  		<div class="col-md-2 form-group" id="tombol">
						  			<a class="btn btn-xs btn-danger pull-right" style="margin:0px 3px" onclick="hapusKeuPeserta(this)"><i class="fa fa-trash-o"></i> Delete</a>
						  		</div>
					  		</div>
						</form>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-sm btn-success pull-right" onclick="cancelKeuPeserta()"> Finish</button>
					<button class="btn btn-sm btn-primary" onclick="finishKeuPeserta()"> Simpan</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalSettingKategoriBayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  		<div class="modal-dialog">
    		<div class="modal-content">
      			<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        			<h4 class="modal-title">Kategori Bayar</h4>
      			</div>
      			<div class="modal-body">
        			<ul class="list-group" id="list-kategori-bayar" style="height: 300px; overflow-y: auto">
        				<?php
        					foreach ($kategori as $key => $value) {
								echo "<li class='list-group-item'>
										<a class='btn btn-sm btn-danger pull-right' onclick='deleteKategoriBayar(\"".$value->kategori_bayar."\",this)'><i class='fa fa-trash-o'></i></a>
        								<h4 class='list-group-item-heading'>".$value->keterangan."</h4>
        								<p class='list-group-item-text'>".$value->kategori_bayar."</p>
        							  </li>"; 
							}
        				?>
					</ul>
					<hr style="margin:2px 0px"/>
					<form id="form_kategori_bayar">
						<label>Kelompok</label>
						<div class="row form-group">
							<div class="col-md-4">
								<select class="form-control" name="kelompok">
									<option value='non'>Non</option>
									<option value='mengajar'>Mengajar</option>
								</select>
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" name="kategori_bayar" placeholder="kategori bayar"/>
							</div>
						</div>
						<label>Keterangan</label>
						<div class="row form-group">
							<div class="col-md-12">
								<input type="text" class="form-control" name="keterangan" placeholder="keterangan"/>
							</div>
						</div>
						<button type="button" class="btn btn-primary btn-sm" type="button" onclick="addKategoriBayar()">Simpan</button>
					</form>
      			</div>
      			<div class="modal-footer">
        			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      			</div>
      			
    		</div><!-- /.modal-content -->
  		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php $this->foot();?>