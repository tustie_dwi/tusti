<html>
	<head>
		<style>
			td, th{
				padding :5px;
			}
			td.center{
				text-align: center;
			}
		</style>
	</head>
	<body>
		<table style="width: 100%">
			<tr>
				<td>Nama Kegiatan : </td>
				<td><?php echo $kegiatan->keterangan; ?></td>
			</tr>
			<tr>
				<td>Kategori : </td>
				<td><?php echo $kegiatan->keteranganKategoriBayar; ?></td>
			</tr>
			<tr>
				<td>Tanggal : </td>
				<td><?php echo $kegiatan->tgl_mulai; ?> Hingga <?php echo $kegiatan->tgl_selesai; ?></h4></td>
			</tr>
			<tr>
				<td>Total Biaya : </td>
				<td><?php echo (!$kegiatan->total)?0:$kegiatan->total; ?></td>
			</tr>
			<tr>
				<td>Detail Peserta : </td>
			</tr>
		</table>
		<br/>
		<table style="width: 100%">
			<thead>
				<tr>
					<th style="border-bottom: 1px solid #ddd;">Nama Peserta</th>
					<th style="border-bottom: 1px solid #ddd;">NIK/NIP</th>
					<th style="border-bottom: 1px solid #ddd;">Keterangan</th>
					<th style="border-bottom: 1px solid #ddd;">Satuan @</th>
					<th style="border-bottom: 1px solid #ddd;">Jumlah</th>
					<th style="border-bottom: 1px solid #ddd;">Total</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$total = 0;
					if($peserta)
					foreach ($peserta as $key => $value) {
						echo "<tr>
								<td class='center'>$value->nama</td>
								<td class='center'>$value->karyawan_id</td>
								<td class='center'>$value->keterangan</td>
								<td style='text-align: right;'>Rp ".number_format($value->satuan, 2, ',', '.')."</td>
								<td class='center'>$value->qty</td>
								<td style='text-align: right;'>Rp ".number_format($value->total, 2, ',', '.')."</td>
							 </tr>";
						$total += $value->total;
					} 
					echo "<tr>
							<td colspan='5' style='border-top: 1px solid #ddd;'>Total Keseluruhan</td>
							<td style='text-align: right; border-top: 1px solid #ddd;'>Rp ".number_format($total, 2, ',', '.')."</td>
						 </tr>";
				?>
			</tbody>
		</table>
		
	</body>
	<script>
		window.print();
	</script>
</html>