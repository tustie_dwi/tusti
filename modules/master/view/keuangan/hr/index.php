<?php $this->head(); 
ini_set('memory_limit','20000M');
?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Keuangan</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Keuangan</a></li>
		  <li class="active"><a href="#">Honorarium</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<?php $this->view('keuangan/hr/form.php', $data); ?>
	</div>
	
	<div class="col-md-9">
	<div class="block-box">
	<?php 	
		 if( isset($posts) ) :	
			?>
		<form method="post" action="<?php echo $this->location('module/master/keuangan/proses/bayar') ?>">	
			<table class='table table-hover'>
					<thead>
						<tr>
							<!--<th><!--<input type="checkbox" id="checkAll"></th>-->
							<th>Dosen</th>
							<th>MK</th>
							<th>Kelas</th>
							<th>SKS</th>
							<th>Hadir</th>
							<th>Satuan</th>
							<th>Total</th>
							<th>Wajib</th>
							<th>PPH</th>
							<th>Jumlah</th>							
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
					<input type="hidden" name="mulai" value="<?php echo $mulai; ?>">
					<input type="hidden" name="selesai" value="<?php echo $selesai; ?>">
					<input type="hidden" name="prodi" value="<?php echo $prodiid; ?>">
			<?php
				$i = 1;
				$list_dosen = '';
				$mconf = new model_honor();
				$dosen = array();
				if($posts) :
					foreach($posts as $key) {
						$dosen[$key->karyawan_id]['nama'] = $key->nama;
						$dosen[$key->karyawan_id]['karyawan_id'] = $key->karyawan_id;
						$dosen[$key->karyawan_id]['golongan'] = $key->golongan;
						$dosen[$key->karyawan_id]['nik'] = $key->nik;
						$dosen[$key->karyawan_id]['prodi'] = $key->prodi_id;
						$dosen[$key->karyawan_id]['pangkat'] = $key->pangkat;
						$dosen[$key->karyawan_id]['jabatan'] = $key->jabatan;
						$dosen[$key->karyawan_id]['pendidikan'] = $key->pendidikan_terakhir;
						$dosen[$key->karyawan_id]['mku'] = $key->is_mku;
						$dosen[$key->karyawan_id]['hbase'] = $key->home_base;
						if($key->is_nik=='nik') $dosen[$key->karyawan_id]['pns'] = 0;
						else $dosen[$key->karyawan_id]['pns'] = 1;
		
						$detail = $mconf->get_rekap_detail($prodiid, $mulai, $selesai, $key->karyawan_id);
						$rowspan = count($detail);						
						$dosen[$key->karyawan_id]['kolom'] = $rowspan;																
		
						$dosen_tmp = $key->karyawan_id;											
					}
				endif;
				print_nilai($dosen, $mconf,$prodiid, $mulai, $selesai, $list_dosen);						
				
				 ?>
			</tbody></table>
			<div class="well">
				<input type="submit" name='b_proses' value=" Proses Pembayaran " class='btn btn-danger'>
			</div>
		</form>
		<?php
		 else: 
		 ?>			
		<div class="well">Sorry, no content to show</div>
	
		<?php endif; ?>
	</div>
	</div>
</div>

<?php	

function print_nilai($dosen, $mconf, $prodiid, $mulai, $selesai, &$list_dosen){
		//$mconf = new model_conf();
		//rumus = (jumlah ttap muka ) * (jumlah sks - kewajiban mengajar) * tarif - pajak
		$j=0;
		$total_honor=0;
		$total_honor_pph=0;
		$total_honor_all=0;
		
		$comp[] = array(3,6,9,12,18,24,30,36);
		
		foreach($dosen as $key => $value){
			$rows = $value['kolom'];
			$detail = $mconf->get_rekap_detail($prodiid, $mulai, $selesai, $value['karyawan_id']);
			
			$dayDif    = date('z',strtotime($selesai)) - date('z',strtotime($mulai));
			$numWeeks  = number_format($dayDif / 7,0);
			
			if($value['pns']==1) $strpns = "NIP";
			else $strpns = "NIK";
			
			$tarif = $mconf->get_tarif($value['golongan'], $value['pns'], $value['pendidikan'], 's1');
			$tarif_s2 = $mconf->get_tarif($value['golongan'], $value['pns'], $value['pendidikan'], 's2');
			
			if($tarif):
				$ttarif = $tarif->tarif;
				$pph	= $tarif->potongan;
			else:
				if($value['pendidikan']=='S3'):
					
					$ttarif = 85000;				
					$pph	= 5;
				else:
					$ttarif = 80000;				
					$pph	= 5;
				endif;
			endif;
			
			if($tarif_s2):
				$ttarif_s2 = $tarif_s2->tarif;
				$pph_s2	= $tarif_s2->potongan;
			endif;
			//$kewajiban = ($numWeeks * 6) * $satuan;
			
			$kewajiban = ($numWeeks * 6);
			if($detail):
				$j=0;
				$total_all=0;
				$total_bersih = 0;
				$total_sks = 0;
				$total_hadir = 0;
				
				$kewajiban_sisa = 0;
				$kewajiban_total_sisa = $kewajiban;
				$kewajiban_total = 0;
				$z = 0;
				
				$arr_detail = array();
				
				foreach($detail as $dt) :
					$j++;
					if($j==1){
						echo "<tr>";
						//echo "<td rowspan='".$rows."' >";
						//echo "</td>";
						echo "<td rowspan='".$rows."'>".$value['nama'].' ' .$value['karyawan_id']."<small>";
						echo "<input type='hidden' id='checkItem' name='chkdosentmp[]' value='".$value['karyawan_id']."'>";
						echo "<input type='hidden' name='chkdosen[]' value='".$value['karyawan_id']."' >";
						
						$list_dosen .= $value['karyawan_id'] . "|";
						if($value['nik'] && ($value['nik']!='-')) echo "<br><b>".$strpns.". ".$value['nik']."</b>";
						if($value['golongan'] && ($value['golongan']!='-')) echo " <br>".$value['golongan'];
						if($value['pangkat'] && ($value['pangkat']!='-')) echo " / ".$value['pangkat'];
						if($value['jabatan'] && ($value['jabatan']!='-')) echo " / ".$value['jabatan'];
						
						echo "</small></td>";
					}
					$praktikum = $mconf->get_mk_praktikum($dt->mkditawarkan_id);
					
					if(strToLOwer($dt->strata)=='s2'):
						$satuan = $ttarif_s2;
						$pph_ok = $pph_s2;
					else:
						if($dt->is_mku==1):
							if(strToLower(trim($dt->nama_mk))=='kewirausahaan'):
								$satuan = 80000;
							else:
								if($value['pendidikan']=='S3') $satuan = 50000;									
								else $satuan = 50000;
							endif;
						else:
							if(strToLower(trim($dt->nama_mk))=='bahasa jepang dasar') $satuan = 50000;									
							else $satuan = $ttarif;
						endif;
						$pph_ok = $pph;
					endif;
					
					if($praktikum):
						$sksx = $dt->sks-1;
						$isprak = 1;
					else:
						$sksx = $dt->sks;
						$isprak = 0;
					endif;
					
					if($sksx  > 3):
						$sks_valid=($sksx/2);
					else:
						$sks_valid=$sksx;
					endif;
				
					
					$cek_wajib = $mconf->get_rekap_detail_bayar_wajib($mulai, $selesai, $value['karyawan_id']);
					$total_bersih_sum = $total_all;
					if($praktikum) $is_praktikum = 1;
					else $is_praktikum = 0;
					
					$arr_temp = 
						array(
							'is_wajib_potong' => $cek_wajib,
							'numweeks' => $numWeeks,
							'prodi' => $dt->prodi_id,
							'mk' => $dt->nama_mk,
							'kode_mk' => $dt->kode_mk,
							'kelas' => $dt->kelas_id,
							'sks' => $sks_valid,
							'jml' => $dt->jml,
							'hbase' => $value['hbase'],
							'kewajiban' => $kewajiban,
							'satuan' => $satuan,
							'PPH' => $pph_ok,
							'rows'=>$rows,
							'is_praktikum' => $is_praktikum,
							'staff'=>$value['karyawan_id'],
							'gol'=>$value['golongan'],
							'mkid'=>$dt->mkditawarkan_id,
							'hadir_baru'=> $dt->jml,
							'wajib' => 0
						);
					
					array_push($arr_detail, $arr_temp);
				
				endforeach;
				// echo "<pre>".print_r($arr_detail)."<pre>";
				
				$cek_potong = $mconf->cek_dosen_titip($value['karyawan_id']);
				
				/*if($value['hbase']=='PTIIK' && $cek_potong == 'tidak') cek_potongan_keu($arr_detail, $kewajiban, $j);
				else tulis_data($arr_detail, $kewajiban, $j);*/
				/*if($value['hbase']=='PTIIK' && $value['karyawan_id']!= '201301101100') cek_potongan_keu($arr_detail, $kewajiban, $j);
				else tulis_data($arr_detail, $kewajiban, $j);*/
				
				if($value['hbase']=='PTIIK' && $cek_potong == 'tidak') cek_potongan_keu($arr_detail, $kewajiban, $j);
				else tulis_data($arr_detail, $kewajiban, $j);
			endif;				
			
		}
	}
	
	function factor_x($n, &$factor, $rec){
		$factors_array = array();
	 	for ($x = 1; $x <= sqrt(abs($n)); $x++)
	 	{
		    if ($n % $x == 0)
		    {
		        $z = $n/$x; 

		        if (array_key_exists($x,$rec)) $factor[$x] = $z;
		       }
		   }
	}
	
	function loop(&$rec, &$loop, $start, $stop, &$res, $target){
		if($start == -1) $start = $stop;
		else{
			if(! isset($loop[$start])){
				$start = 0;
			}
			if($start == $stop) return;
		}

		$index = $loop[$start];

		for ($i=0; $i < $rec[$index]; $i++) { 
			array_push($res, $index);

			// echo $index . ' ';
			$init = cek_nilai($res, $target);

			if($init == 'end') {
				return 'selesai';

			}
			$data = loop($rec, $loop, ($start+1), $stop, $res, $target);

			if($data == 'selesai') {
				return 'selesai';
			}
		}

		if($start != $stop){	
			for ($i=0; $i < $rec[$index]; $i++) { 
				array_pop($res);
			}
		}		
	}

	function cek_nilai(&$res, $target){
		$val = array_sum($res);

		if($val == $target) return 'end';
		else return 'continue';
	}
	
	function cek_potongan_keu($arr, $kewajiban, $count){
		$kewajiban_temp = $kewajiban;
		
		//proses pengecekan total sks dosen
		$rec = array();
		$jml_total_tmp = 0;
		for($i=0; $i<$count; $i++){
			$jml_total_tmp += $arr[$i]['sks'] * $arr[$i]['jml'];
			$arr[$i]['hadir_baru'] = $arr[$i]['jml'];
			$arr[$i]['wajib'] = 0;
			
			if(! isset($rec[$arr[$i]['sks']])) $rec[$arr[$i]['sks']] = $arr[$i]['jml'];
			else $rec[$arr[$i]['sks']] += $arr[$i]['jml'];
		}
		
		if($jml_total_tmp < $kewajiban_temp){ //jika jumlah sks kurang dari jml kewajiban
			for($i=0; $i<$count; $i++){
				$jml_total = $arr[$i]['sks'] * $arr[$i]['jml'];
				
				$arr[$i]['hadir_baru'] = 0;
				$arr[$i]['wajib'] = $jml_total * $arr[$i]['satuan'];
			}
			$kewajiban_temp = 0;
		}
		else{ //jika jml sks lbeih dari kewajiban
			$factor = array();
			factor_x($kewajiban, $factor, $rec);
			
			$data = '';
			foreach($factor as $key => $value){
				if($rec[$key] >= $value) $data = array($key => $value);
			}
			
			if($data == ''){
				$loop = array();
				foreach($rec as $rec_key => $rec_val){
					array_push($loop, $rec_key);
				}
				
				$res = array();
				
				for($loop_i=0; $loop_i < count($rec); $loop_i++){
					loop($rec, $loop, -1, 0, $res, $kewajiban);
					if(array_sum($res) == $kewajiban) {
						break;
					}
				}
				
				foreach($res as $k => $v){
					if(! isset($data[$v])) $data[$v] = 1;
					else $data[$v]++;
				}
			}
			
			for($i=0; $i<$count; $i++){ 
				if(isset($data[$arr[$i]['sks']])){
					if($data[$arr[$i]['sks']] > 0){
						if( ($arr[$i]['jml'] - $data[$arr[$i]['sks']]) < 0){
							$data[$arr[$i]['sks']] = $data[$arr[$i]['sks']] - $arr[$i]['jml'];
							$arr[$i]['wajib'] = $arr[$i]['sks'] * $arr[$i]['jml'] * $arr[$i]['satuan'];
							$arr[$i]['hadir_baru'] = 0;
						}
						else{
							$arr[$i]['hadir_baru'] = $arr[$i]['jml'] - $data[$arr[$i]['sks']];
							
							$arr[$i]['wajib'] = $arr[$i]['sks'] * $data[$arr[$i]['sks']] * $arr[$i]['satuan'];
							$data[$arr[$i]['sks']] = 0;
						}
					}
				}
			}
			
		}
		//proses menampilkan data
		$total_all=0;
		for ($i=0; $i <$count ; $i++) {
			$total = ($arr[$i]['sks'] * $arr[$i]['jml']) * $arr[$i]['satuan'];
			
			$wajib = $pot = 0;
			$pot = $arr[$i]['hadir_baru'];
			$wajib = $arr[$i]['wajib'];
			
			$total_pph = ($total-$wajib) * ($arr[$i]['PPH']/100);
			$jumlah = ($total - $wajib) - $total_pph;
			$total_all = $total_all + $jumlah;
			
			echo "<td><small><strong>".$arr[$i]['kode_mk']."</strong> <span class='badge'>".$arr[$i]['prodi']."</span></small><br>";
			echo $arr[$i]['mk'];
			if($arr[$i]['is_praktikum'] == 1) echo " <span class='label label-danger'>* praktikum</span>";
			echo "</td>";
			echo "<td>".$arr[$i]['kelas']."</td>";
			echo "<td>".$arr[$i]['sks']."</td>";
			echo "<td>".$arr[$i]['jml']."</td>";
			echo "<td>".number_format($arr[$i]['satuan'])."</td>";
			echo "<td>".number_format($total)."</td>";
			echo "<td>".number_format($wajib)."</td>";
			echo "<td>".number_format($total_pph)."</td>";
			echo "<td>".number_format($jumlah)."</td>";
				if($i==($arr[$i]['rows']-1)):							
							echo "<td align='right'><b>".number_format($total_all)."</b></td>";
						else:
							echo "<td colspan=2></td>";
						endif;
			echo "</tr>";
			
			$json = array(
				'hidprak' => $arr[$i]['is_praktikum'],
				'hidprodi' => $arr[$i]['prodi'],
				'hidmk' => $arr[$i]['mkid'],
				'hiddosen' => $arr[$i]['staff'],
				'hidkelas' => $arr[$i]['kelas'],
				'hidgol' => $arr[$i]['gol'],
				'hidnama' => $arr[$i]['mk'],
				'hidkode' => $arr[$i]['kode_mk'],
				'hidsks' => $arr[$i]['sks'],
				'hidhadir' => $arr[$i]['jml'],
				'hidsatuan' => $arr[$i]['satuan'],
				'hidtotal' => $total,
				'hidpph' => $total_pph,
				'hidjml' => $jumlah,
				'hidwajib' => $wajib,
				'hidpotongan' => $pot,
				'hidbayar' => $jumlah,
				'hidmgg' => $arr[$i]['numweeks']
			);
			
			echo "<input type='hidden' name='bayar_detail".$arr[$i]['staff']."[]' value='".json_encode($json)."'>";
			
			if($i==($count-1)):							
				// echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";
			else:
				// echo "<td colspan=2></td>";
			endif;
		}
	}
	
	
	
	
	function cek_potongan_keu_last($arr, $kewajiban, $count){
		$kewajiban_temp = $kewajiban;
		
		//proses penghitugan
		$jml_total_tmp = 0;
		for($i=0; $i<$count; $i++){
			$jml_total_tmp += $arr[$i]['sks'] * $arr[$i]['jml'];
		}
		
		if($jml_total_tmp < $kewajiban_temp){ //jika jumlah sks kurang dari jml kewajiban
			for($i=0; $i<$count; $i++){
				$jml_total = $arr[$i]['sks'] * $arr[$i]['jml'];
				
				$arr[$i]['hadir_baru'] = 0;
				$arr[$i]['wajib'] = $jml_total * $arr[$i]['satuan'];
			}
			$kewajiban_temp = 0;
		}
		else{ //jika jml sks lbeih dari kewajiban
			for($i=0; $i<$count; $i++){
				$jml_total = $arr[$i]['sks'] * $arr[$i]['jml'];
				
				$divider = $arr[$i]['sks'];
				$hasil_bulat = floor($jml_total / $divider);
				$sisa = ($jml_total % $divider) / $arr[$i]['sks'];
				
				if($kewajiban_temp >= ($hasil_bulat * $divider)) {
					$kewajiban_temp -= ($hasil_bulat * $divider);
					$arr[$i]['hadir_baru'] = $sisa;
					$arr[$i]['wajib'] = ($arr[$i]['jml'] - $arr[$i]['hadir_baru']) * $arr[$i]['satuan'] * $arr[$i]['sks'];
				}
				elseif($kewajiban_temp == 0) {}
				else {
					$sks_sisa_max = $kewajiban_temp - ($kewajiban_temp%$divider);
					$arr[$i]['hadir_baru'] = $sks_sisa_max/$divider;
					$arr[$i]['wajib'] = $sks_sisa_max * $arr[$i]['satuan'];
					
					$kewajiban_temp = 0;
					$i = $count;
				}
			}
		}
			
		//proses menampilkan data
		$total_all=0;
		for ($i=0; $i <$count ; $i++) {
			$total = ($arr[$i]['sks'] * $arr[$i]['jml']) * $arr[$i]['satuan'];
			
			$wajib = $pot = 0;
			$pot = $arr[$i]['hadir_baru'];
			$wajib = $arr[$i]['wajib'];
			
			$total_pph = ($total-$wajib) * ($arr[$i]['PPH']/100);
			$jumlah = ($total - $wajib) - $total_pph;
			$total_all = $total_all + $jumlah;
			
			echo "<td><small><strong>".$arr[$i]['kode_mk']."</strong> <span class='badge'>".$arr[$i]['prodi']."</span></small><br>";
			echo $arr[$i]['mk'];
			if($arr[$i]['is_praktikum'] == 1) echo " <span class='label label-danger'>* praktikum</span>";
			echo "</td>";
			echo "<td>".$arr[$i]['kelas']."</td>";
			echo "<td>".$arr[$i]['sks']."</td>";
			echo "<td>".$arr[$i]['jml']."</td>";
			echo "<td>".number_format($arr[$i]['satuan'])."</td>";
			echo "<td>".number_format($total)."</td>";
			echo "<td>".number_format($wajib)."</td>";
			echo "<td>".number_format($total_pph)."</td>";
			echo "<td>".number_format($jumlah)."</td>";
				if($i==($arr[$i]['rows']-1)):							
							echo "<td align='right'><b>".number_format($total_all)."</b></td>";
						else:
							echo "<td colspan=2></td>";
						endif;
			echo "</tr>";
			
			$json = array(
				'hidprak' => $arr[$i]['is_praktikum'],
				'hidprodi' => $arr[$i]['prodi'],
				'hidmk' => $arr[$i]['mkid'],
				'hiddosen' => $arr[$i]['staff'],
				'hidkelas' => $arr[$i]['kelas'],
				'hidgol' => $arr[$i]['gol'],
				'hidnama' => $arr[$i]['mk'],
				'hidkode' => $arr[$i]['kode_mk'],
				'hidsks' => $arr[$i]['sks'],
				'hidhadir' => $arr[$i]['jml'],
				'hidsatuan' => $arr[$i]['satuan'],
				'hidtotal' => $total,
				'hidpph' => $total_pph,
				'hidjml' => $jumlah,
				'hidwajib' => $wajib,
				'hidpotongan' => $pot,
				'hidbayar' => $jumlah,
				'hidmgg' => $arr[$i]['numweeks']
			);
			
			echo "<input type='hidden' name='bayar_detail".$arr[$i]['staff']."[]' value='".json_encode($json)."'>";
			
			if($i==($count-1)):							
				// echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";
			else:
				// echo "<td colspan=2></td>";
			endif;
		}
	}


	function cek_potongan_keu_ori($arr, $kewajiban, $count){
		$kewajiban_temp = $kewajiban;
		
		for($i=0; $i<$count; $i++){
			if($arr[$i]['sks']==2) $divider=2;
			else $divider=3;
			//$divider=6;
			
			$jml_total = $arr[$i]['sks'] * $arr[$i]['jml'];
			
			$hasil_bulat = floor($jml_total / $divider);
			$sisa = ($jml_total % $divider) / $arr[$i]['sks'];
			
			if($kewajiban_temp >= ($hasil_bulat * $divider)) {
				$kewajiban_temp -= ($hasil_bulat * $divider);
				$arr[$i]['hadir_baru'] = $sisa;
				$arr[$i]['wajib'] = ($arr[$i]['jml'] - $arr[$i]['hadir_baru']) * $arr[$i]['satuan'] * $arr[$i]['sks'];
			}
			elseif($kewajiban_temp == 0) {}
			else {
				$arr[$i]['hadir_baru'] = $sisa + (($jml_total - $kewajiban_temp) / $arr[$i]['sks']);
				$arr[$i]['wajib'] = ($arr[$i]['jml'] - $arr[$i]['hadir_baru']) * $arr[$i]['satuan'] * $arr[$i]['sks'];
				$kewajiban_temp = 0;
				$i = $count;
				
				
				
			}
		}
		
		$total_all=0;
		for ($i=0; $i <$count ; $i++) {
			$total = ($arr[$i]['sks'] * $arr[$i]['jml']) * $arr[$i]['satuan'];
			
			$wajib = $pot = 0;
			$pot = $arr[$i]['hadir_baru'];
			$wajib = $arr[$i]['wajib'];
			
			$total_pph = ($total-$wajib) * ($arr[$i]['PPH']/100);
			$jumlah = ($total - $wajib) - $total_pph;
			$total_all = $total_all + $jumlah;
			
			echo "<td><small><strong>".$arr[$i]['kode_mk']."</strong> <span class='badge'>".$arr[$i]['prodi']."</span></small><br>";
			echo $arr[$i]['mk'];
			if($arr[$i]['is_praktikum'] == 1) echo " <span class='label label-danger'>* praktikum</span>";
			echo "</td>";
			echo "<td>".$arr[$i]['kelas']."</td>";
			echo "<td>".$arr[$i]['sks']."</td>";
			echo "<td>".$arr[$i]['jml']."</td>";
			echo "<td>".number_format($arr[$i]['satuan'])."</td>";
			echo "<td>".number_format($total)."</td>";
			echo "<td>".number_format($wajib)."</td>";
			echo "<td>".number_format($total_pph)."</td>";
			echo "<td>".number_format($jumlah)."</td>";
				if($i==($arr[$i]['rows']-1)):							
							echo "<td align='right'><b>".number_format($total_all)."</b></td>";
						else:
							echo "<td colspan=2></td>";
						endif;
			echo "</tr>";
			
			$json = array(
				'hidprak' => $arr[$i]['is_praktikum'],
				'hidprodi' => $arr[$i]['prodi'],
				'hidmk' => $arr[$i]['mkid'],
				'hiddosen' => $arr[$i]['staff'],
				'hidkelas' => $arr[$i]['kelas'],
				'hidgol' => $arr[$i]['gol'],
				'hidnama' => $arr[$i]['mk'],
				'hidkode' => $arr[$i]['kode_mk'],
				'hidsks' => $arr[$i]['sks'],
				'hidhadir' => $arr[$i]['jml'],
				'hidsatuan' => $arr[$i]['satuan'],
				'hidtotal' => $total,
				'hidpph' => $total_pph,
				'hidjml' => $jumlah,
				'hidwajib' => $wajib,
				'hidpotongan' => $pot,
				'hidbayar' => $jumlah,
				'hidmgg' => $arr[$i]['numweeks']
			);
			
			echo "<input type='hidden' name='bayar_detail".$arr[$i]['staff']."[]' value='".json_encode($json)."'>";
			
			if($i==($count-1)):							
				// echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";
			else:
				// echo "<td colspan=2></td>";
			endif;
		}
	}

	function cek_potongan_new($arr, $kewajiban, $count){
		$kewajiban_temp = $kewajiban;
		for($i=0; $i<$count; $i++){
			$jml_total = $arr[$i]['sks'] * $arr[$i]['jml'];
			
			$kewajiban_temp -= $jml_total;
			if($kewajiban_temp <= 0){
				$arr[$i]['hadir_baru'] = $jml_total + $kewajiban_temp;
				$i = $count;
			}
			else{
				$arr[$i]['hadir_baru'] = $jml_total;
			}
		}
		
		$total_all=0;
		for ($i=0; $i <$count ; $i++) {
			$total = ($arr[$i]['sks'] * $arr[$i]['jml']) * $arr[$i]['satuan'];
			
			$wajib = $pot = 0;
			$pot = $arr[$i]['hadir_baru'];
			$wajib = $arr[$i]['hadir_baru'] * $arr[$i]['satuan'];
			
			$total_pph = ($total-$wajib) * ($arr[$i]['PPH']/100);
			$jumlah = ($total - $wajib) - $total_pph;
			$total_all = $total_all + $jumlah;
			
			//echo "<tr>"; 
			//echo "<td colspan='2'>";
			echo "<td><small><strong>".$arr[$i]['kode_mk']."</strong> <span class='badge'>".$arr[$i]['prodi']."</span></small><br>";
			echo $arr[$i]['mk'];
			if($arr[$i]['is_praktikum'] == 1) echo " <span class='label label-danger'>* praktikum</span>";
			echo "</td>";
			echo "<td>".$arr[$i]['kelas']."</td>";
			echo "<td>".$arr[$i]['sks']."</td>";
			echo "<td>".$arr[$i]['jml']."</td>";
			echo "<td>".number_format($arr[$i]['satuan'])."</td>";
			echo "<td>".number_format($total)."</td>";
			echo "<td>".number_format($wajib)."</td>";
			echo "<td>".number_format($total_pph)."</td>";
			echo "<td>".number_format($jumlah)."</td>";
				if($i==($arr[$i]['rows']-1)):							
							echo "<td align='right'><b>".number_format($total_all)."</b></td>";
						else:
							echo "<td colspan=2></td>";
						endif;
			echo "</tr>";
			
			$json = array(
				'hidprak' => $arr[$i]['is_praktikum'],
				'hidprodi' => $arr[$i]['prodi'],
				'hidmk' => $arr[$i]['mkid'],
				'hiddosen' => $arr[$i]['staff'],
				'hidkelas' => $arr[$i]['kelas'],
				'hidgol' => $arr[$i]['gol'],
				'hidnama' => $arr[$i]['mk'],
				'hidkode' => $arr[$i]['kode_mk'],
				'hidsks' => $arr[$i]['sks'],
				'hidhadir' => $arr[$i]['jml'],
				'hidsatuan' => $arr[$i]['satuan'],
				'hidtotal' => $total,
				'hidpph' => $total_pph,
				'hidjml' => $jumlah,
				'hidwajib' => $wajib,
				'hidpotongan' => $pot,
				'hidbayar' => $jumlah,
				'hidmgg' => $arr[$i]['numweeks']
			);
			
			echo "<input type='hidden' name='bayar_detail".$arr[$i]['staff']."[]' value='".json_encode($json)."'>";
			
			// echo '<input type="hidden" name="hidprak'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['is_praktikum'].'">';	
			// echo '<input type="hidden" name="hidprodi'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['prodi'].'">';	
			// echo '<input type="hidden" name="hidmk'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['mkid'].'">';	
			// echo '<input type="hidden" name="hiddosen'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['staff'].'">';
			// echo '<input type="hidden" name="hidkelas'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['kelas'].'">';	
			// echo '<input type="hidden" name="hidgol'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['gol'].'">';
			// echo '<input type="hidden" name="hidnama'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['mk'].'">';
			// echo '<input type="hidden" name="hidkode'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['kode_mk'].'">';
			// echo '<input type="hidden" name="hidsks'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['sks'].'">';	
			// echo '<input type="hidden" name="hidhadir'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['jml'].'">';
			// echo '<input type="hidden" name="hidsatuan'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['satuan'].'">';	
			// echo '<input type="hidden" name="hidtotal'.$arr[$i]['staff'].'[]" value="'.$total.'">';
			// echo '<input type="hidden" name="hidpph'.$arr[$i]['staff'].'[]" value="'.$total_pph.'">';	
			// echo '<input type="hidden" name="hidjml'.$arr[$i]['staff'].'[]" value="'.$jumlah.'">';
			// echo '<input type="hidden" name="hidwajib'.$arr[$i]['staff'].'[]" value="'.$wajib.'">';
			// echo '<input type="hidden" name="hidpotongan'.$arr[$i]['staff'].'[]" value="'.$pot.'">';
			// echo '<input type="hidden" name="hidbayar'.$arr[$i]['staff'].'[]" value="'.$jumlah.'">';
			// echo '<input type="hidden" name="hidmgg'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['numweeks'].'">';					
			
			if($i==($count-1)):							
				// echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";
			else:
				// echo "<td colspan=2></td>";
			endif;
		}
	}

	function cek_potongan($arr, $kewajiban, $count){
		if($arr[0]['hbase'] != 'PTIIK') return;
		$kewajiban_main = $kewajiban;
		
		$start_main = 0;
		$stop_main = 0;
		$is_potong = array();
		// $potong_awal = $arr[$start_main]['mk']. ' '. $arr[$start_main]['prodi']. ' ' .$arr[$start_main]['kelas'];
		
		while($stop_main == 0) :
			$is_potong = array();
			
			$kewajiban = $kewajiban_main;
			
			$total = $arr[$start_main]['sks'] * $arr[$start_main]['jml'];
			$kewajiban_awal = $kewajiban;
			$kewajiban = $kewajiban - $total;
			$kewajiban_tmp = $kewajiban;
			
			$start = ($start_main + 1);
			$stop = 0;
			
			// echo $kewajiban . ' = ' .$kewajiban_awal . ' - ' . $total . '<br>';
			$potong_awal= $arr[$start_main]['mk']. ' '. $arr[$start_main]['prodi']. ' ' .$arr[$start_main]['kelas'];
			while($stop==0){
				$is_potong = array($potong_awal);
				
				// echo $potong_awal . '--<br>';
				for($j=$start; $j<$count; $j++){
					$hasil_kali = ($arr[$j]['sks'] * $arr[$j]['jml']);
					$kewajiban_awal = $kewajiban;
					$kewajiban = $kewajiban - $hasil_kali;
					
					$potong_desc = $arr[$j]['mk']. ' '. $arr[$j]['prodi']. ' ' .$arr[$j]['kelas'];
					array_push($is_potong, $potong_desc);
					// echo $kewajiban . ' = ' .$kewajiban_awal . ' - ' . $hasil_kali . '<br>';
					
					if($kewajiban == 0) {
						$stop = 1;
						$stop_main = 1;
						break;
					}
				}
				// echo "<hr>";
				
				$kewajiban = $kewajiban_tmp;
				$start++;
				if($start > $count) $stop = 1;
			}
			
			$start_main++;
			if($start_main >= $count) $stop_main = 1;
		endwhile;
		
		$total_all=0;
		for ($i=0; $i <$count ; $i++) {
			$total = ($arr[$i]['sks'] * $arr[$i]['jml']) * $arr[$i]['satuan'];
			
			if( $arr[$i]['is_wajib_potong']):
				$wajib = 0;
					$pot = 0;
			else:
				$dipotong= cek_potong($is_potong, $arr[$i]['prodi'], $arr[$i]['mk'], $arr[$i]['kelas']);
				if($dipotong):
					$wajib = $total;
					$pot = $arr[$i]['sks'] * $arr[$i]['jml'];
				else:
					$wajib = 0;
					$pot = 0;
				endif;
			endif;
			
			$total_pph = ($total-$wajib) * ($arr[$i]['PPH']/100);
			$jumlah = ($total - $wajib) - $total_pph;
			
			$total_all = $total_all + $jumlah;
			
			//echo "<tr>"; 
			//echo "<td colspan='2'>";
			echo "<td><small><strong>".$arr[$i]['kode_mk']."</strong> <span class='badge'>".$arr[$i]['prodi']."</span></small><br>";
			echo $arr[$i]['mk'];
			if($arr[$i]['is_praktikum'] == 1) echo " <span class='label label-danger'>* praktikum</span>";
			echo "</td>";
			echo "<td>".$arr[$i]['kelas']."</td>";
			echo "<td>".$arr[$i]['sks']."</td>";
			echo "<td>".$arr[$i]['jml']."</td>";
			echo "<td>".number_format($arr[$i]['satuan'])."</td>";
			echo "<td>".number_format($total)."</td>";
			echo "<td>".number_format($wajib)."</td>";
			echo "<td>".number_format($total_pph)."</td>";
			echo "<td>".number_format($jumlah)."</td>";
				if($i==($arr[$i]['rows']-1)):							
							echo "<td align='right'><b>".number_format($total_all)."</b></td>";
						else:
							echo "<td colspan=2></td>";
						endif;
			echo "</tr>";
			
			echo '<input type="hidden" name="hidprak'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['is_praktikum'].'">';	
			echo '<input type="hidden" name="hidprodi'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['prodi'].'">';	
			echo '<input type="hidden" name="hidmk'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['mkid'].'">';	
			echo '<input type="hidden" name="hiddosen'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['staff'].'">';
			echo '<input type="hidden" name="hidkelas'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['kelas'].'">';	
			echo '<input type="hidden" name="hidgol'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['gol'].'">';
			echo '<input type="hidden" name="hidnama'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['mk'].'">';
			echo '<input type="hidden" name="hidkode'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['kode_mk'].'">';
			echo '<input type="hidden" name="hidsks'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['sks'].'">';	
			echo '<input type="hidden" name="hidhadir'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['jml'].'">';
			echo '<input type="hidden" name="hidsatuan'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['satuan'].'">';	
			echo '<input type="hidden" name="hidtotal'.$arr[$i]['staff'].'[]" value="'.$total.'">';
			echo '<input type="hidden" name="hidpph'.$arr[$i]['staff'].'[]" value="'.$total_pph.'">';	
			echo '<input type="hidden" name="hidjml'.$arr[$i]['staff'].'[]" value="'.$jumlah.'">';
			echo '<input type="hidden" name="hidwajib'.$arr[$i]['staff'].'[]" value="'.$wajib.'">';
			echo '<input type="hidden" name="hidpotongan'.$arr[$i]['staff'].'[]" value="'.$pot.'">';
			echo '<input type="hidden" name="hidbayar'.$arr[$i]['staff'].'[]" value="'.$jumlah.'">';
			echo '<input type="hidden" name="hidmgg'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['numweeks'].'">';					
			
			if($i==($count-1)):							
				// echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";
			else:
				// echo "<td colspan=2></td>";
			endif;
		}
	}
	
	function cek_potong($arr, $prodi, $mk, $kelas){
		foreach ($arr as $key) {
			if($key == $mk . ' ' . $prodi . ' '. $kelas) {
				return true;
			}
		}
	}
	
	function tulis_data($arr, $kewajiban, $count){
		$total_all=0;
		for ($i=0; $i <$count ; $i++) {
		
			$total = ($arr[$i]['sks'] * $arr[$i]['jml']) * $arr[$i]['satuan'];
			
		
				$wajib = 0;
				$pot = 0;
		
			
			$total_pph = ($total-$wajib) * ($arr[$i]['PPH']/100);
			$jumlah = ($total - $wajib) - $total_pph;
			
			$total_all = $total_all + $jumlah;
			
			//echo "<tr>"; 
			//echo "<td colspan='2'>";
			echo "<td><small><strong>".$arr[$i]['kode_mk']."</strong> <span class='badge'>".$arr[$i]['prodi']."</span></small><br>";
			echo $arr[$i]['mk'];
			if($arr[$i]['is_praktikum'] == 1) echo " <span class='label label-danger'>* praktikum</span>";
			echo "</td>";
			echo "<td>".$arr[$i]['kelas']."</td>";
			echo "<td>".$arr[$i]['sks']."</td>";
			echo "<td>".$arr[$i]['jml']."</td>";
			echo "<td>".number_format($arr[$i]['satuan'])."</td>";
			echo "<td>".number_format($total)."</td>";
			echo "<td>".number_format($wajib)."</td>";
			echo "<td>".number_format($total_pph)."</td>";
			echo "<td>".number_format($jumlah)."</td>";
				if($i==($arr[$i]['rows']-1)):							
							echo "<td align='right'><b>".number_format($total_all)."</b></td>";
						else:
							echo "<td colspan=2></td>";
						endif;
			echo "</tr>";
			
			$json = array(
				'hidprak' => $arr[$i]['is_praktikum'],
				'hidprodi' => $arr[$i]['prodi'],
				'hidmk' => $arr[$i]['mkid'],
				'hiddosen' => $arr[$i]['staff'],
				'hidkelas' => $arr[$i]['kelas'],
				'hidgol' => $arr[$i]['gol'],
				'hidnama' => $arr[$i]['mk'],
				'hidkode' => $arr[$i]['kode_mk'],
				'hidsks' => $arr[$i]['sks'],
				'hidhadir' => $arr[$i]['jml'],
				'hidsatuan' => $arr[$i]['satuan'],
				'hidtotal' => $total,
				'hidpph' => $total_pph,
				'hidjml' => $jumlah,
				'hidwajib' => $wajib,
				'hidpotongan' => $pot,
				'hidbayar' => $jumlah,
				'hidmgg' => $arr[$i]['numweeks']
			);
			
			echo "<input type='hidden' name='bayar_detail".$arr[$i]['staff']."[]' value='".json_encode($json)."'>";
			
			// echo '<input type="hidden" name="hidprak'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['is_praktikum'].'">';	
				// echo '<input type="hidden" name="hidprodi'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['prodi'].'">';	
				// echo '<input type="hidden" name="hidmk'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['mkid'].'">';	
				// echo '<input type="hidden" name="hiddosen'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['staff'].'">';
				// echo '<input type="hidden" name="hidkelas'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['kelas'].'">';	
				// echo '<input type="hidden" name="hidgol'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['gol'].'">';
				// echo '<input type="hidden" name="hidnama'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['mk'].'">';
				// echo '<input type="hidden" name="hidkode'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['kode_mk'].'">';
				// echo '<input type="hidden" name="hidsks'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['sks'].'">';	
				// echo '<input type="hidden" name="hidhadir'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['jml'].'">';
				// echo '<input type="hidden" name="hidsatuan'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['satuan'].'">';	
				// echo '<input type="hidden" name="hidtotal'.$arr[$i]['staff'].'[]" value="'.$total.'">';
				// echo '<input type="hidden" name="hidpph'.$arr[$i]['staff'].'[]" value="'.$total_pph.'">';	
				// echo '<input type="hidden" name="hidjml'.$arr[$i]['staff'].'[]" value="'.$jumlah.'">';
				// echo '<input type="hidden" name="hidwajib'.$arr[$i]['staff'].'[]" value="'.$wajib.'">';
				// echo '<input type="hidden" name="hidpotongan'.$arr[$i]['staff'].'[]" value="'.$pot.'">';
				// echo '<input type="hidden" name="hidbayar'.$arr[$i]['staff'].'[]" value="'.$jumlah.'">';
				// echo '<input type="hidden" name="hidmgg'.$arr[$i]['staff'].'[]" value="'.$arr[$i]['numweeks'].'">';		
			
			// $total_bersih_sum += 
			
			if($i==($count-1)):							
				// echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";
			else:
				// echo "<td colspan=2></td>";
			endif;
		}
	}
	

$this->foot();

?>
