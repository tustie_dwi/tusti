<?php 	
		 if( isset($posts) ) :	
			?>
	
			<table class="table table-hover">
					<thead>
						<tr valign="top">
							<!--<th rowspan="2"><!--<input type="checkbox" id="checkAll"></th>-->
							<th rowspan="2">Dosen</th>						
							<th colspan="4">Matakuliah Diampu</th>
							<th rowspan="2">Satuan</th>
							<th rowspan="2">Total</th>
							<th rowspan="2">Wajib</th>
							<th rowspan="2">PPH</th>
							<th rowspan="2">Jumlah</th>							
							<th rowspan="2">Total</th>
						</tr>						
						<tr>
							<th>Nama MK</th>
							<th>Kelas</th>
							<th>SKS</th>						
							<th>Hadir</th>
							
							
						</tr>
					</thead>
					<tbody>
					
					<input type="hidden" id="tmp" value="<?php echo $kode; ?>">
			<?php
				$i = 1;
				
					$mconf = new model_honor();
					foreach($posts as $key) {
								
					//if($mhs_tmp != ''){
						$dosen[$key->karyawan_id]['nama'] = $key->nama;
						$dosen[$key->karyawan_id]['karyawan_id'] = $key->karyawan_id;
						$dosen[$key->karyawan_id]['golongan'] = $key->golongan;
						$dosen[$key->karyawan_id]['nik'] = $key->nik;
						$dosen[$key->karyawan_id]['prodi'] = $key->prodi_id;
						$dosen[$key->karyawan_id]['pangkat'] = $key->pangkat;
						$dosen[$key->karyawan_id]['jabatan'] = $key->jabatan;
						$dosen[$key->karyawan_id]['pendidikan'] = $key->pendidikan_terakhir;
						$dosen[$key->karyawan_id]['mku'] = $key->is_mku;
						$dosen[$key->karyawan_id]['kode'] = $key->bayar_id;
						if($key->is_nik=='nik') $dosen[$key->karyawan_id]['pns'] = 0;
						else $dosen[$key->karyawan_id]['pns'] = 1;
					//}
						$detail = $mconf->get_rekap_detail_bayar("", "", "", $key->karyawan_id,$kode);
						$rowspan = count($detail);						
						$dosen[$key->karyawan_id]['kolom'] = $rowspan;																

						$dosen_tmp = $key->karyawan_id;											
					}
					print_nilai($dosen, $mconf,"", "", "", $kode);						
				
				 ?>
			</tbody>
			</table>
			
			
		<?php
		 else: 
		 ?>			
		<div class="well hide-from-print">Sorry, no content to show</div>
	
		<?php endif; ?>
<?php	

function print_nilai($dosen, $mconf, $prodiid, $mulai, $selesai, $kode){
		//$mconf = new model_conf();
		//rumus = (jumlah ttap muka ) * (jumlah sks - kewajiban mengajar) * tarif - pajak
		$k=0;
		$total_honor=0;
		$total_honor_pph=0;
		$total_honor_all=0;
		foreach($dosen as $key => $value){
			$k++;
			$rows = $value['kolom'];
			$detail = $mconf->get_rekap_detail_bayar($prodiid, $mulai, $selesai, $value['karyawan_id'], $kode);
			
				
			if($value['pns']==1) $strpns = "NIP";
			else $strpns = "NIK";
			if($detail):
				$j=0;
				$total_all=0;
				$total_bersih = 0;
				$total_sks = 0;
				$total_hadir = 0;
				$total_wajib = 0;
				foreach($detail as $dt){
					$j++;
					
					$tarif = $dt->satuan;
					$pph = $dt->pph;
					$satuan = $dt->satuan;
					
					$isproses = $dt->is_proses;
					
					
					$kewajiban = ($dt->jml_mgg * 6);
					
					if($j==1){
						echo "<tr>";
						//echo "<td rowspan='".$rows."' >";
						//echo "<input type='checkbox' id='checkItem' name='chkdosen[]' value='".$value['karyawan_id']."'></td>";
						echo "<td rowspan='".$rows."'>".$value['nama']."<small>";
						if($value['nik'] && ($value['nik']!='-')) echo "<br><b>".$strpns.". ".$value['nik']."</b>";
						if($value['golongan'] && ($value['golongan']!='-')) echo " <br>".$value['golongan'];
						if($value['pangkat'] && ($value['pangkat']!='-')) echo " / ".$value['pangkat'];
						if($value['jabatan'] && ($value['jabatan']!='-')) echo " / ".$value['jabatan'];
						
						echo "</small></td>";
					}
					
					$sks_valid = $dt->sks;
					
					$total_sks = $total_sks + $sks_valid;
					$total_hadir = $total_hadir + $dt->hadir;
					
					//$total = (($sks_valid * $dt->jml) - $kewajiban) * $satuan;
					$total = ($sks_valid * $dt->hadir) * $satuan;
				
				
									
					
					echo "<td><b>".$dt->kode_mk. "</b> - ".$dt->nama_mk . " (".$dt->prodi_id.")";
					if($dt->praktikum==1) echo "<small><em> * praktikum</em></small>";
					if($dt->is_proses=='selisih') echo " <span class='label label-danger'>* absen baru</span>";
					echo "</td>";
					echo "<td>".$dt->kelas_id."</td>";
					echo "<td>".$dt->sks."</td>";
					echo "<td>".$dt->hadir."</td>";
					//echo "<td><b>".$kewajiban."</b><br><small>*) ".$numWeeks." mgg</small></td>";					
					echo "<td align='right'>".number_format($satuan)."</td>";
					echo "<td align='right'>".number_format($total)."</td>";
										
					$wajib =  $dt->kewajiban;
					
					
					$total_pph = $dt->pph;
					
					$jumlah = ($total - $wajib) - $total_pph;
					$total_all = $total_all + $jumlah;					
				
					$total_bersih_sum = $total_all;
					
					/*--- total akhir --*/
					//$total_honor = $total_honor + ($total-$wajib);
					$total_wajib = $total_wajib + $wajib;
					$total_honor = $total_honor + ($total);
					$total_honor_pph = $total_honor_pph + $total_pph;
					//$total_honor_all = $total_honor - $total_honor_pph;
					$total_honor_all = $total_honor - $total_honor_pph-$total_wajib;
					
					
					echo "<td align='right'>".number_format($wajib)."</td>";
					echo "<td align='right'>".number_format($total_pph)."</td>";	
					echo "<td align='right'>".number_format($jumlah)."</td>";
						if($j==$rows):							
							echo "<td align='right'><b>".number_format($total_bersih_sum)."</b></td>";
						else:
							echo "<td colspan=2></td>";
						endif;
						
					echo "</tr>";
				}
			endif;				
			
		}
		
		echo "<tr valign='top'>";
			echo "<td colspan=3>Jumlah Honor Tidak Kena Pajak</td><td colspan=9 align='right'><h4>".number_format($total_honor)."</h4></td>";
		echo "</tr>";
		
		echo "<tr valign='top'>";
			echo "<td colspan=3>Jumlah Potongan</td><td colspan=9 align='right'><h4>".number_format($total_wajib)."</h4></td>";
		echo "</tr>";
		
		
		echo "<tr valign='top'>";
			echo "<td colspan=3>Jumlah Honor Kena Pajak</td><td colspan=9 align='right'><h4>".number_format($total_honor_pph)."</h4></td>";
		echo "</tr>";
		echo "<tr valign='top'>";
			echo "<td colspan=3>Jumlah</td><td colspan=9 align='right'><h4>".number_format($total_honor_all)."</h4></td>";
		echo "</tr>";
		echo '<input type="hidden" name="hidtotalnon" value='.$total_honor.'>';	
		echo '<input type="hidden" name="hidtotalpph" value='.$total_honor_pph.'>';	
		echo '<input type="hidden" name="hidtotalall" value='.$total_honor_all.'>';	
	}
?>