<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">HR Mengajar</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		   <li><a href="#>">Report</a></li>
		  <li class="active"><a href="<?php echo $this->location('module/master/report/keuangan/hr') ?>">HR Mengajar</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-3 hide-from-print">
		<div class="block-box">
				<form method="post" action="<?php echo $this->location('module/master/report/keuangan/hr') ?>">		
					<div class="control-group">
						<label class="form-label"></label>
						<div class="form-group">
							<select name="cmbrekap" id="cmbrekap" class="cmb-rekap form-control">
								<option value="-">Please Select</option>
								<?php
								if(isset($periode)):
									foreach($periode as $dt):
										?>
										<option value="<?php echo $dt->bayar_id;?>"><?php echo date("M d, Y", strtotime($dt->tgl_mulai));?> - <?php echo date("M d, Y", strtotime($dt->tgl_selesai));?></option>
										<?php
									endforeach;
								endif;
								?>
							</select>
						</div>
					</div>
					
					<button class="btn btn-primary"> <i class='fa fa-search'></i> Cari</button>
				</form>
			</div>

	</div>	
	
	<div class="col-md-9 block-box" id="content-print">
		
	</div>
	
</div>

<?php
$this->foot();

?>
