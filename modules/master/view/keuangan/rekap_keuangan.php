<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Keuangan</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Keuangan</a></li>
		  <li class="active"><a onclick="tes()" href="#">Rekap Keuangan per Periode</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="block-box">
			<div class="row">
				<div class="col-md-6">
					<select class="form-control e9" id="sel_bulan" onchange="get_transaksi_per_periode()">
						<option value="1">Januari</option>
						<option value="2">Februari</option>
						<option value="3">Maret</option>
						<option value="4">April</option>
						<option value="5">Mei</option>
						<option value="6">Juni</option>
						<option value="7">Juli</option>
						<option value="8">Agustus</option>
						<option value="9">September</option>
						<option selected="" value="10">Oktober</option>
						<option value="11">November</option>
						<option value="12">Desember</option>
					</select>
				</div>
				
				<div class="col-md-6">
					<input id="thn" onchange="get_transaksi_per_tahun(this.value)" class="form-control" type="number" max="<?php echo date('Y') ?>" value="<?php echo $tahun ?>" />
				</div>
			</div>
		</div>
		<div class='block-box'>
			<?php
				if(0) {
					echo "<div class='well text-center'>Data pembayaran <code>kosong</code></div>";
				}
				else{
					echo '<div id="container" style="height: 400px;"></div>';
				}
			?>
		</div>
		
		<div id="perBulan" class="block-box">
		  
		</div>
	</div>
	
</div>
<?php $this->foot(); ?>

<?php
	if(1) :
?>

<script type="text/javascript">
	
	$(document).ready(function(){
		$(".e9").select2();
		
		$("#sel_bulan").val();
		
		get_transaksi_per_tahun();
		get_transaksi_per_periode();
	});
	
	function convert_bulan(init){
		switch(init){
			case "1" : return "Januari";
			case "2" : return "Februari";
			case "3" : return "Maret";
			case "4" : return "April";
			case "5" : return "Mei";
			case "6" : return "Juni";
			case "7" : return "Juli";
			case "8" : return "Agustus";
			case "9" : return "September";
			case "10" : return "Oktober";
			case "11" : return "November";
			case "12" : return "Desember";
			
			default : return "---";
		}
	}
	
	function get_transaksi_per_tahun(){
		var uri = base_url + 'module/master/keuangan/get_rekap_tahun';
		var thn = $("#thn").val();
		
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : uri,
			data : $.param({tahun : thn}),
			success:function(msg){
				/*if(msg == '-') alert('Data tidak ditemukan');
				else update_chart(msg, thn);*/
				update_chart(msg, thn);
			}
		});
	}
	
	function get_transaksi_per_periode(){
		var uri = base_url + 'module/master/keuangan/get_ajax_rekap_tahun';
		var bln = $("#sel_bulan").val();
		var thn = $("#thn").val();
		
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : uri,
			data : $.param({bulan : bln, tahun : thn}),
			success:function(msg){
				/*if(msg == '-') alert('Data tidak ditemukan');
				else update_chart_periode(msg, bln);*/
				update_chart_periode(msg, bln);
			}
		});
	}
	
	function update_chart(data, thn){
		data = JSON.parse(data);
		
		$('#container').highcharts({
	        chart: {
	            type: 'bar'
	        },
	        title: {
	            text: 'Rekap Keuangan per Tahun'
	        },
	        subtitle: {
	            text: 'Periode ' + thn
	        },
	        xAxis: {
	            categories: ['Tidak Kena Pajak', 'PPH', 'Jumlah Kena Pajak'],
	            title: {
	                text: null
	            }
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Rupiah',
	                align: 'high'
	            },
	            labels: {
	                overflow: 'justify'
	            }
	        },
	        plotOptions: {
	            bar: {
	                dataLabels: {
	                    enabled: true
	                }
	            }
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'top',
	            x: -40,
	            y: 0,
	            floating: true,
	            borderWidth: 1,
	            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
	            shadow: true
	        },
	        credits: {
	            enabled: false
	        },
	        series: data
	    });
	}
	
	
	function update_chart_periode(data, bulan){
		data = JSON.parse(data);
		
		console.log(data[0]);
		
		$('#perBulan').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Rekap Keuangan per Periode'
	        },
	        subtitle: {
	            text: 'Bulan ' + convert_bulan(bulan)
	        },
	        xAxis: {
	            categories: data[1]
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Rupiah'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>Rp {point.y}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: data[0]
	    });
	}
	
</script>

<?php endif; ?>




