<?php $this->head(); 
if($posts!=''){
	if($edit_view!='file'){
		foreach ($posts as $dt){
			$content_id			= $dt->content_id;
			$hidId				= $dt->contentid;
			$contentcategory	= $dt->content_category;
			$category			= $dt->category;
			$menuorder			= $dt->menu_order;
			$unitid				= $dt->unit_id;
			$fakultasid 		= $dt->fakultas_id;
			$cabangid 			= $dt->cabang_id;
			$title				= $dt->content_title;
			$content			= $dt->content;
			$listdata			= $dt->content_data;
			$lang				= $dt->content_lang;
			$contentupload		= $dt->content_upload;
			$comment			= $dt->content_comment;
			$img				= $dt->content_thumb_img;
			$contentid			= $dt->parent_id;
			$contentexcerpt		= $dt->content_excerpt;
			$issticky			= $dt->is_sticky;
		}
	}
	else{
		foreach ($posts as $dt){
			$file_id			= $dt->file_id;
			$hidFileId			= $dt->fileid;
			$contentcategory	= $dt->content_category;
			$category			= $dt->category;
			$unitid				= $dt->unit_id;
			$fakultasid 		= $dt->fakultas_id;
			$cabangid 			= $dt->cabang_id;
			$filetitle 			= $dt->file_title;
			$filenote			= $dt->file_note;
			$filedata			= $dt->file_data;
			$filegroup 			= $dt->file_group;
			$file_file_loc		= $dt->file_loc;
			$lang				= $dt->file_lang;
			$file_file_type		= $dt->file_type;
			$file_file_name		= $dt->file_name;
			$file_file_size		= $dt->file_size;
		}	
		
		
		$content			= $filenote;
		// $lang 				= 'in';
		$comment			= '';
		$img				= '';
		$issticky			= '';
	}
	//---------CHANGE CONTENT-------------------------------
	$src 	= Array();
	$result = Array();
	$string = explode('<', $content);
	foreach ($string as $s) {
		if (strpos($s,'img') !== false) {
			$imgsrc = explode('src', $s);
			preg_match_all('/".*?"|\'.*?\'/', $imgsrc[1], $source);
			// echo substr(str_replace('%20', ' ', $source[0][0]), 1, -1)."<br><br>";
			$src[] = substr(str_replace('%20', ' ', $source[0][0]), 1, -1);
		}
	};
	foreach ($src as $s) {
		$file_tmp_name = $s;
		$cekfile = explode('/', $file_tmp_name);
		if($cekfile[0]!='http:'){
			$file_loc = $this->config->file_url_view."/".$file_tmp_name;
		}else{
			$file_loc = $file_tmp_name;
		}
		$result[] = $file_loc;
	}
	
	$content = str_replace($src, $result, $content);
	if($edit_view=='file'){$filenote=$content;};
	//---------CHANGE CONTENT-------------------------------
	$sys = 'edit';
}else{
	$lang 				= 'in';
	$unitid 			= '';
	$comment			= '';
	$img				= '';
	$issticky			= '';
	$content_id 		= '';
	$contentcategory	= '';
	$category			= 'content';
	$sys 				= 'new';
}

?>
<style>
  .thumb {
    height: 150px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }
</style>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/master/content/page'); ?>">Content</a></li>
  <?php 
  if($sys!='new'){
  	echo '<li class="active"><a href="#">Edit</a></li>';
  }else{
  	echo '<li class="active"><a href="#">New</a></li>';
  }
  ?>
</ol>
<div class="breadcrumb-more-action">
	<?php 
	//if($user!="mahasiswa"&&$user!="dosen"){ 
	if($user!="mahasiswa"){ 
		if($sys!='new'){
	?>
		<a href="<?php echo $this->location('module/master/content/write/page'); ?>" class="btn btn-primary">
		<i class="fa fa-pencil icon-white"></i> New Content</a> 
	<?php 
		}
	} 
	?>
</div>

<div class="col-md-12 block-box">
	<!-- action="<?php echo $this->location('module/master/content/save_coms_content'); ?>"  -->
	<form method="post" name="form-content" id="form-content" class="form-horizontal" enctype="multipart/form-data">
	<input type="hidden" id="identifier" value="content" />
		
		<div class="form-group">	
			<label class="control-label">Fakultas</label>		
				<select id="select_fakultas" class="form-control e9" name="fakultas" >
				<option value="0">Pilih Fakultas</option>
				<?php if(count($get_fakultas)> 0) {
					foreach($get_fakultas as $dt) :
						echo "<option value='".$dt->hid_id."' ";
						if(isset($fakultasid)){
							if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
								echo "selected";
							}
						}
						echo " >".ucWords($dt->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
		</div>
		
		<div class="form-group">	
			<label class="control-label">Cabang</label>					
				<select id="select_cabang" class="form-control e9" name="cabang" >
				<option value="0">Pilih Cabang</option>
				<?php if(count($cabang)> 0) {
					foreach($cabang as $c) :
						echo "<option value='".$c->cabang_id."' ";
						if(isset($cabangid)){
							if($cabangid==$c->cabang_id){
								echo "selected";
							}
						}
						echo " >".ucWords($c->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
		</div>
		
		<div class="form-group">	
			<label class="control-label">Unit</label>					
				<select id="select_unit" class="form-control e9" name="unit" >
				<option value="0">Pilih Unit</option>
				</select>
		</div>
		
		<div class="form-group">	
			<label class="control-label">Kategori</label>					
				<select id="select_category" class="form-control e9" name="category" >
				<option value="0">Pilih Kategori</option>
				<?php if(count($content_category)> 0) {
					foreach($content_category as $cc) :
						echo '<optgroup label="'.$cc->category.'">';
						$this->read_category_child($cc->category,$contentcategory);
						echo '</optgroup>';
					endforeach;
				} ?>
				</select>
		</div>
		
		<div class="language-box">
			<label class="control-label">Bahasa</label><br>
			<label class="radio-inline">
			  <input name="lng[]" value="in" type="checkbox" <?php if($lang=='in') echo 'checked="checked"'; if($sys!='new') echo 'disabled="disabled"'; ?> > <b>Indonesia</b>
			</label>
			<label class="radio-inline">
			  <input name="lng[]" value="en" type="checkbox" <?php if($lang=='en') echo 'checked="checked"'; if($sys!='new') echo 'disabled="disabled"'; ?> > <b>English</b>
			</label>
		</div>
		
		<div class="form-group" id="parent-form-group" <?php if($category!='upload')echo 'style="display: block;"';else echo 'style="display: none;"'; ?> >	
			<label class="control-label">Parent Page</label>					
				<select id="select_parent" class="form-control e9" name="parent-page" >
				<option value="0">Pilih Parent</option>
				</select>
		</div>
		
		<div class="content-form" <?php if($category!='upload')echo 'style="display: block;"';else echo 'style="display: none;"'; ?>>
			
			<div class="form-group">
				<label class="control-label">Tanggal Upload</label>
				<?php if($sys=='new'){ ?>
		        <input type="text" class="form-control" name="content-upload" value="<?php echo date("Y-m-d") ?>" />
		        <?php }else{ ?>
		        <input type="text" class="form-control" name="content-upload" value="<?php echo $contentupload ?>" />
		        <input type="hidden" name="lng" value="<?php if(isset($lang))echo $lang ?>" />
		        <?php } ?>
			</div>	
			
			<div class="form-group" id="urut-form-group">
				<label class="control-label">No Urut</label>
				<input name="urut" id="urut" class="form-control" value="<?php if(isset($menuorder))echo $menuorder ?>" type="text">
			</div>
			
			<div class="form-group">
				<label class="control-label">Judul</label>
				<input name="title" id="title" class="form-control" value="<?php if(isset($title))echo $title ?>" type="text">
			</div>
			
			<div class="form-group">
				<textarea name="content" id="content" class="form-control ckeditor"><?php if(isset($content))echo $content ?></textarea>
				<?php if($sys!='new'&&$edit_view!='file'){echo '<br>';$this->get_tag_content($content_id,'edit');} ?>
			</div>
			
			<div class="form-group">
				<label class="control-label">Tag</label>
				<input name="tag" id="tag" class="form-control" type="text">
			</div>
			
			<div class="form-group" id="contentexcerpt-form-group">
				<label class="control-label">Ringkasan Berita</label>
				<textarea name="contentexcerpt" id="contentexcerpt" class="form-control"><?php if(isset($contentexcerpt))echo $contentexcerpt ?></textarea>
			</div>
			
			<div class="form-group">
				<label class="control-label">List Data</label>
				<input name="list-data" id="list-data" class="form-control" value="<?php if(isset($listdata))echo $listdata ?>" type="text">
			</div>
			
			<div id="comment-sticky-form-group">
				<label class="checkbox-inline">
				  <input name="comment" value="1" type="checkbox" <?php if($comment=='1') echo 'checked="checked"'; ?> > <b>Comments ON</b>
				</label>
				<label class="checkbox-inline">
				  <input name="issticky" value="1" type="checkbox" <?php if($issticky=='1') echo 'checked="checked"'; ?> > <b>Sticky</b>
				</label>
			</div>
			
			<div class="form-group">
				<label class="control-label"><h3>Attach File</h3></label>
				<fieldset class="form-horizontal">
					<div class="attach-form-group">
						<?php if($sys!='new'&&$edit_view!='file'){
							$this->get_attach_file($content_id,'edit'); 
						} ?>
						<!-- <div class="form-group">
						  <blockquote>
							<input name="attach-name[]" class="form-control" type="text" placeholder="Judul">
							<input name="attach-file[]" class="form-control attach-file" type="file" accept="application/msexcel,application/pdf,application/msword">
							<br><textarea name="attach-note[]" class="form-control"></textarea>
							<a class="btn btn-danger pull-right" onclick="delete_file_attach(this)">Delete</a>
						  </blockquote>
						</div> -->
					</div>
				</fieldset>
				<?php //if($sys=='new'){ ?>
					<!-- <input name="attach-sys" class="form-control" type="hidden" value="new"> -->
				<?php //} ?>
				<a class="btn btn-primary" id="btn-add-file">Tambah File</a>
			</div>
			
			<div class="form-group">
				<h3>Thumbnail</h3>
				<div class="fileinput fileinput-new" data-provides="fileinput">
				  <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
					<img src="<?php if($img) echo $this->config->file_url_view."/".$img; else  echo $this->config->default_thumb ?>">
				  	<?php
					if($img){
					?>
					<input type="hidden" id="hidimg" name="hidimg" value="<?php echo $img; ?>" />
					<?php } ?>
				  </div>
				  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
				  <div>
					<span class="btn btn-primary btn-file"><span class="fileinput-new"><i class="fa fa-camera"></i> Select Image</span><span class="fileinput-exists"><i class="fa fa-edit"></i> Change</span>
					<input type="file" name="files" accept="image/*"></span>
					<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><i class="fa fa-trash-o"></i> Remove</a>
				  </div>
				</div>
			</div>
	        
	        <input type="hidden" id="content-content-id" value="<?php if(isset($content_id))echo $content_id ?>" />
	        <input type="hidden" id="content-parent-select" value="<?php if(isset($contentid))echo $contentid ?>" />
	        <input type="hidden" id="unit-select" value="<?php if(isset($unitid))echo $unitid ?>" />
	        <input type="hidden" name="hidId" value="<?php if(isset($hidId))echo $hidId ?>" />
	        <!-- <input type="submit" value="submit"> -->
	        <a class="btn btn-primary" data-loading-text="Publishing..." id="btn-publish" name="b_pub">Save and Publish</a>
	        <a class="btn btn-warning" data-loading-text="Saving..." id="btn-draft" name="b_draft">Save as Draft</a>
	        
        </div>
        <div class="file-form" <?php if($category!='upload')echo 'style="display: none;"';else echo 'style="display: block;"'; ?> >
        	<input type="hidden" id="file-group-selected" value="<?php if(isset($filegroup))echo $filegroup;else echo '0'; ?>" />
        	<div class="form-group">	
				<label class="control-label">Grup</label>					
				<select id="select_file_group" class="form-control e9" name="file-group" >
					<option value="0">Pilih Grup</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
			</div>
					
        	<div class="form-group">
				<label class="control-label">Judul File</label>
				<input name="filetitle" id="filetitle" class="form-control" value="<?php if(isset($filetitle))echo $filetitle ?>" type="text">
			</div>
        	
        	<output id="list">
        		<?php 
        		if(isset($file_file_loc)){
        			if($contentcategory=='slider'){
        				echo '<img src="'.$this->config->file_url_view."/".$file_file_loc.'" width="400" />';
        			}elseif($contentcategory=='video'){
        				// echo '<img src="'.$this->asset($file_file_loc).'" width="400" />';
        				echo '<video width="400" height="320" controls>
							   <source src="'.$this->config->file_url_view."/".$file_file_loc.'" type="'.$file_file_type.'">
							  </video>';
						
						// echo '<div class="embed-responsive embed-responsive-16by9">
								  // <iframe class="embed-responsive-item" src="'.$this->asset($file_file_loc).'"></iframe>
								// </div>';
						
        			}
					echo '<input type="hidden" id="hid-file-img" name="hid-file-img" value="'.$file_file_loc.'" />';
					
					echo '<input type="hidden" id="hid-file-type" name="hid-file-type" value="'.$file_file_type.'" />';
					echo '<input type="hidden" id="hid-file-name" name="hid-file-name" value="'.$file_file_name.'" />';
					echo '<input type="hidden" id="hid-file-size" name="hid-file-size" value="'.$file_file_size.'" />';
        		}
        		?>
        	</output>
        	<div class="form-group">
				<label class="control-label">File</label>
				<input name="file-file" id="file-file" class="form-control" type="file">
			</div>
			
			<div class="form-group">
				<label class="control-label">Note</label>
				<textarea name="filenote" id="filenote" class="form-control ckeditor" ><?php if(isset($filenote))echo $filenote ?></textarea>
			</div>
        	
        	<div class="form-group">
				<label class="control-label">File data</label>
				<input name="filedata" id="filedata" class="form-control" value="<?php if(isset($filedata))echo $filedata ?>" type="text">
			</div>
        	
        	<?php if($sys!='new'){ ?>
	         <input type="hidden" name="lng" value="<?php if(isset($lang))echo $lang ?>" />
	        <?php }?>
        	<input type="hidden" name="hidFileId" value="<?php if(isset($hidFileId))echo $hidFileId ?>" />
        	<a class="btn btn-primary" data-loading-text="Publishing..." id="btn-file-publish" name="b_pub">Save and Publish</a>
	        <a class="btn btn-warning" data-loading-text="Saving..." id="btn-file-draft" name="b_draft">Save as Draft</a>
        </div>             
	</form>
</div>
<script>
  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
	var f = files[0];
        var reader = new FileReader();
         
          reader.onload = (function(theFile) {
                return function(e) {
                	var extension = theFile.name.substring(theFile.name.lastIndexOf('.'));
                	var validFileType = ".jpg , .png , .bmp, .jpeg";
				    if (validFileType.toLowerCase().indexOf(extension) < 0) {
				    	document.getElementById('list').innerHTML = ['<video width="400" height="320" controls><source src="', e.target.result,'"></video>'].join('');
				    }else{
				    	document.getElementById('list').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="400" />'].join('');
				    }
                };
          })(f);
           
          reader.readAsDataURL(f);
    }
  document.getElementById('file-file').addEventListener('change', handleFileSelect, false);
</script>
<?php $this->foot(); ?>