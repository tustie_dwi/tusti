<?php $this->head(); ?>
<h2 class="title-page">Setting Status Penelitian</h2>
<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/master/penelitian'); ?>">Penelitian</a></li>
		   <li><a href="<?php echo $this->location('module/master/penelitian/status'); ?>">Setting Status</a></li>
		</ol>
		<div class="row">
			<div class="col-md-6"><!--LIST-->
				<div class="block-box">
					<div class="content">
						<?php if(isset($posts)&&$posts!==""): ?>
						<table class='table table-hover example'>
							<thead>
								<tr>
									<th>Detail</th>	
									<th>&nbsp;</th>	
								</tr>
							</thead>
							<tbody>
							<?php foreach ($posts as $p) { ?>
							<tr>
								<td>
									<?php
										echo "<h5>".$p->keterangan."</h5>";
									?>
								</td>
								<td>
									<ul class='nav nav-pills'>
										<li class='dropdown pull-right'>
										  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
										  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
											<li>
												<a class='btn-edit-post' href="<?php echo $this->location('module/master/penelitian/status/'.$p->statusid); ?>" ><i class='fa fa-edit'></i> Edit</a>
											</li>
										  </ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
							</tbody>
						</table>
					<?php else: ?>
						<div class="well" align="center">
							No Data Available
						</div>
					<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-md-6"><!--FORM-->
				<form method="post" name="form-status" id="form-status">
					<div class="panel panel-default">
						<div class="panel-heading" id="form-box"><?php echo $param ?></div>
						<div class="panel-body ">
							<div class="form-group">
								<label class="control-label">Kategori</label>								
								<select class="form-control" name="kategori">
									<option value="0">Silahkan Pilih</option>
									<option value="penelitian" <?php if(isset($edit)&&$edit->kategori=="penelitian") echo "selected"; ?>>Penelitian</option>
									<option value="pengabdian" <?php if(isset($edit)&&$edit->kategori=="pengabdian") echo "selected"; ?>>Pengabdian</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">Keterangan</label>								
								<textarea class="form-control" name="keterangan"><?php if(isset($edit)) echo $edit->keterangan ?></textarea>
							</div>
							<div class="form-group">
								<input type="hidden" name="hidId" value="<?php if(isset($edit->status_id)) echo $edit->status_id; ?>">
								<button type="submit" class="btn btn-primary">Data Valid &amp; Save</button>
								<button type="button" class="btn btn-danger" style="display: none">Cancel</button>			
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<?php $this->foot(); ?>