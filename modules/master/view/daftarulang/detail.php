<?php $this->head();
if (isset($posts)) {
	if($posts){
		$nim			= $posts->nim;
		$mhsid			= $posts->mhs_id;
		$nama			= $posts->nama;
		$jenis_kelamin	= $posts->jenis_kelamin;
		$tmpt_lahir		= $posts->tmp_lahir;
		$tgl_lahir		= $posts->tgl_lahir;
		$seleksi		= $posts->jalur_masuk;
		$angkatan		= $posts->angkatan;
		$cabangid		= $posts->cabang_id;
		$prodiid		= $posts->prodi;
		$email			= $posts->email;
		$alamat			= $posts->alamat_malang;
		$telp			= $posts->telp;
		$hp				= $posts->hp;
		$isaktif		= $posts->is_aktif;
		$ipk			= $posts->ipk_lulus;
		$tgl_lulus		= $posts->tgl_lulus;
		$masa_kuliah	= $posts->inf_masa_studi;
		$tgl_mulai_kuliah	= $posts->tgl_mulai_studi;
		$tgl_selesai_kuliah	= $posts->tgl_selesai_studi;
		$image			= $posts->foto;
		$nama_ortu		= $posts->orang_tua;
		$email_ortu		= $posts->email_ortu;
		$alamat_ortu	= $posts->alamat_ortu;
		$telp_ortu		= $posts->telp_ortu;
		$hp_ortu		= $posts->hp_ortu;
		$alamat_surat	= $posts->alamat_surat;
		$catatan		= $posts->catatan;
		if($posts->is_valid==1){
			$valid 		= "Valid";
		}
		else{
			$valid 		= "Not Valid";
		}
		$fakultas		=$posts->fakultas;
	}
}
	$header="Detail Mahasiswa";
?>
<div class="row">
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/daftar/daftarulang'); ?>">Daftar Ulang</a></li>
	  <li class="active"><a href="#"><?php echo $header ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<!-- <a href="<?php echo $this->location('module/master/mhs/write'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Mahasiswa</a>  -->
    </div>
	<?php
		if($jenis_kelamin == "L"){
			$Kelamin = "Laki-laki";
		}
		else $Kelamin = "Perempuan";
		
		if($image){ $foto=$image; }else{ $foto='upload/foto/no_foto.png';}
	?>
	<div class="row">
		<div class="col-md-12">
					<div class="media">
								<a class="pull-left" style="text-decoration:none" href="<?php echo $this->location('module/master/daftar/edit/'.$mhsid) ?>">
									<img class="media-object img-thumbnail" src="<?php echo $this->asset($foto); ?>"  width="200" ><br>
									<button type="button" class="btn btn-default" style="width:200px"> <i class="fa fa-edit"></i> Edit Data Mahasiswa </button>
								</a>
								<div class="media-body">
									<h2>
									<?php 
									echo ucWords(strToLower($nama));
									?>
									<span class='label label-success'><?php echo $angkatan ?></span>
									<span class='label label-danger'><?php echo ucWords($isaktif) ?></span>
									<span class='label label-default'><?php echo $cabangid; ?></span>									
									</h2>
									<small>
									<code><?php echo $posts->nim;?></code>&nbsp;<span class="text text-default"><?php echo $fakultas;?></span>&nbsp;
									<span class="text text-danger"><?php echo $prodiid;?></span> <span class="label label-normal"><?php echo ucWords($seleksi); ?></span><br>
									<?php
									echo "<i class='fa fa-calendar'></i> ".date("M d, Y", strtotime($tgl_lahir))."&nbsp; ";
									
									if($alamat){
										echo "<i class='fa fa-map-marker'></i> ".$alamat."&nbsp; ";
									}
									
									if($telp){
										echo "<i class='fa fa-phone'></i> ".$telp."&nbsp; ";
									}
									
									if($hp){
										echo "<i class='fa fa-mobile'></i> ".$hp."&nbsp; ";
									}
									
									if($email){
										echo "<i class='fa fa-envelope'></i> ".$email."&nbsp; ";
									}
									?>
									</small><br>
									<?php if($nama_ortu){ ?>
									<h4><span class="text text-info"><?php echo ucWords($nama_ortu) ?></span> <small>orang tua</small></h4>
									<small>
									<?php if($alamat_ortu){
										echo "<i class='fa fa-map-marker'></i> ".$alamat_ortu."&nbsp; ";
									}
									
									if($telp_ortu){
										echo "<i class='fa fa-phone'></i> ".$telp_ortu."&nbsp; ";
									}
									
									if($hp_ortu){
										echo "<i class='fa fa-mobile'></i> ".$hp_ortu."&nbsp; ";
									}
									
									if($email_ortu){
										echo "<i class='fa fa-envelope'></i> ".$email_ortu."&nbsp; ";
									} ?>
									</small>
									<?php } ?>
								</div>
							</div>
	
			
	    </div>
    </div>
</div>
<?php $this->foot(); ?>