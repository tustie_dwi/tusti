<?php 
 $this->head();
 $header="Daftar Ulang";
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<div class="row">
	<div class="col-md-12">	
		<ol class="breadcrumb">
		 <ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/daftar/daftarulang'); ?>">Daftar Ulang</a></li>
	  <li class="active"><a href="#">Data</a></li>
	</ol>
		</ol>
		<div class="row">
			<div class="col-md-4">	
				<div id="parameter" class="block-box">
				   <div class="content">		
						<div class="form-group">	
							<label>Fakultas</label>			
							<?php if($fakultas_id != '-'){
									   echo '<select class="form-control e9" name="fakultas" id="select_indexfakultas" disabled>';
								  }
								  else echo '<select class="form-control e9" name="fakultas" id="select_indexfakultas">';
							?>
								<option class="sub_01" value="0">Pilih Fakultas</option>			
								<?php						
									foreach($fakultas as $dt):
										echo "<option class='sub_".$dt->fakultasid."' value='".$dt->hid_id."' ";
										if($fakultas_id==$dt->hid_id){
											echo "selected";
										}
										echo ">".$dt->keterangan."</option>";
									endforeach;
								?>
							</select>				
						</div>
						<div class="form-group">	
							<label>Cabang</label>
							<?php $uri_parent = $this->location('module/master/daftar/tampilkan_indexangkatan'); ?>
							<?php echo '<select class="form-control e9" name="cabang" id="select_indexcabang" data-uri="'.$uri_parent.'">' ?>
								<option class="sub_01" value="0">Pilih Cabang</option>			
								<?php								
									foreach($cabang as $dt):
										echo "<option class='sub_".$dt->cabang_id."' value='".$dt->cabang_id."' ";
										echo ">".$dt->keterangan."</option>";
									endforeach;
								?>
							</select>				
						</div>
						<div class="form-group">	
							<label>Data</label>
							<select class="form-control e9" name="select_data">
								<option value="0">Tidak Valid</option>
								<option value="1">Valid</option>
							</select>					
						</div>
						<div class="form-group">	
							<label>Tahun Akademik</label>
							<select class="form-control e9" name="tahun_akademik">
								<option class="sub_01" value="0">Pilih Tahun Akademik</option>			
								<?php								
									foreach($thn_akademik as $dt):
										echo "<option value='".$dt->tahun_akademik."' ";
										echo ">".$dt->semester."</option>";
									endforeach;
								?>
							</select>					
						</div>
						<div class="form-group">	
							<label>Angkatan</label>				
							<?php $uri_parent = $this->location('module/master/daftar/tampilkan_index'); ?>
								<?php echo '<select class="form-control e9" name="select_thn" id="select_indexangkatan" disabled data-uri="'.$uri_parent.'">' ?>
								<option value="0">Pilih Angkatan</option>
							</select>					
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="block-box">
				<a download="mahasiswa.xls" class="btn btn-primary" id="export-data" href="javascript:" style="display: none">Export to Excel</a>
				<div id="display">
				<?php if(isset($mhs)) 	$this->view('daftarulang/viewselection.php',$data);
				else '<div class="well"><center>Sorry no content to show</center></div>';
				?>
					
				</div>
				</div>
			</div>
		 </div>
	</div>
</div>
<?php
 $this->foot();
?>