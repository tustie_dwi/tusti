<?php $this->head(); $header="Daftar Ulang";?>
<h2 class="title-page"><?php echo $header; ?></h2>
<div class="row">
	<div class="col-md-12">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/master/daftar/daftarulang') ?>"><?php echo $header;?></a></li>
		  <li class="active"><a href="#">Data</a></li>
		</ol>
		<div class="row">
			<div class="col-md-12">
				<!--EXPORT-->
				<table class="table table-bordered" id="my-view-Table">
					<thead>
				        <tr>
				           <?php		   
								foreach ($cols as $key => $value) {
									 $arr[] = $value;	
									 echo "<th>".strToUpper($key)."</th>";
								}
						   ?>
				        </tr>
				    </thead>
					<tbody>       
				           <?php
						   foreach($mhs as $row)
							{
								echo "<tr>";
								foreach ($row as $key => $value) {
									 $arr[$key] = $value;	
									 echo "<td>$value</td>";
								}
								echo "</tr>";
							}
						   ?>
				    </tbody>
				</table>
		  </div>
	</div>
</div>
<script src="<?php echo $this->asset("js/jsread.js"); ?>"></script>
<?php $this->foot(); ?>