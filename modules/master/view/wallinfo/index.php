<?php $this->head(); ?>
<div class="row-fluid">
	<legend><a href="<?php echo $this->location('module/master/wallinfo/write'); ?>" class="btn btn-primary pull-right">
    <i class="icon-pencil icon-white"></i> New Wall Information</a> Wall Information List
	</legend>
	<div class="block-box">
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( $posts ) :	
	?>
	
	<table class="table table-hover" id="example" data-id='module/master/wallinfo'>
		<thead>
			<tr>
				<th width='1%'></th>
				<th>Informasi</th>	
				<th>&nbsp;</th>	
				<th>Last Update</th>					
				<th>Act</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
				?>
				<tr id='post-<?php echo $dt->wall_id;?>' data-id='<?php echo $dt->wall_id; ?>' valign=top>
					<td></td>
					<td><b><?php echo $dt->judul; ?></b>
						<blockquote>
							<small>										
															
								Tgl  Ditayangkan: <i class="icon-time"></i>&nbsp;<b><?php echo date("M d, Y", strtotime($dt->tgl_publish));?></b>
															
							</small>
						</blockquote>						
					</td>
					<td><?php 
					
					if($dt->is_publish==1){
						echo "<span class='label label-success'>Published</span>";
					}else{
						echo "<span class='label label-warning'>Draft</span>";
					}
					?></td>
					
					<td><small><span class="text text-warning"><em><?php echo $dt->name; ?> </em></span><br>
						<i class="icon-time"></i>&nbsp;<b><?php echo date("M d, Y H:i", strtotime($dt->last_update));?></b></small>					
					</td>
					
					<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/master/wallinfo/write/'.$dt->id); ?>"><i class='icon-pencil'></i> Edit</a>	
									</li>
									<li>
										<a id='<?php echo $dt->wall_id; ?>' class='btn-delete-post' ><i class='icon-remove'></i> Delete</a>
									</li>	
								  </ul>
								</li>
							</ul>
						</td>
				
					</tr>
						<?php
				 endforeach; 
			 }
			 ?>
		</tbody></table>
	<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
	</div>
</div>
<?php $this->foot(); ?>