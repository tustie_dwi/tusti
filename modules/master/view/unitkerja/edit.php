<?php
if(isset($edit)){
	$header		= "Edit Unit Kerja";
	$id				= $edit->hidId;
	$fakultasid		= $edit->fakultas_id;
	$keterangan		= trim($edit->keterangan);
	$kode			= $edit->kode;
	$parentid		= $edit->parent_id;
	$kategori 		= $edit->kategori;
	$english_version= $edit->english;
	$icon			= $edit->icon;
	$aktif			= $edit->is_aktif;	
	$logo			= $edit->logo;
	$strata			= $edit->strata;
	$desc 			= $edit->tentang;
	$desc_			= $edit->about;
}
else{
	$header			= "Write Unit Kerja";
	$id				= '';
	$fakultasid		= '';
	$keterangan		= '';
	$kode			= '';
	$parentid		= '';
	$kategori		= "-";
	$english_version = "";
	$aktif			= 0;
}

?> 
	
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil"></i> <?php echo $header ?></div>
	<div class="panel-body">		
			<form method="post" enctype="multipart/form-data" id="form-unitkerja">
				<div class="form-group">
					<label class="radio-inline">
				     	<input type="radio" name="data" value="pusat" <?php if($kategori=='-') echo "checked";?>/> Pusat
				    </label>
				    <label class="radio-inline">
				     	<input type="radio" name="data" value="fakultas" <?php if($kategori=="fakultas") echo "disabled"; ?>/> Fakultas
				    </label>
				</div>
				<?php $uri = $this->location('module/master/general/get_unit') ?>
				<div class="form-group" id="fakultas_data">	
					<label class="control-label">Fakultas</label>
					<select class="e9 form-control" name="fakultas_id" id="btn-fakultas" data-uri="<?php echo $uri ?>">
						<option value="0" data-uri='1'>Pilih Fakultas</option>
						<?php if(count($fakultas) > 0) {
							foreach($fakultas as $f) :
								echo "<option class='fakultas' value='".$f->fakultas_id."' ";
								if($fakultasid==$f->fakultas_id){
									echo "selected";
								}
								echo " >".$f->keterangan."</option>";
							endforeach;
						} ?>
					</select>					
				</div>
				
				<div class="form-group">
					<label class="control-label">Kategori</label>					
					<select name="cmbkategori" class="e9 form-control">
						<option value="-">Silahkan Pilih</option>
						<?php
							foreach($kategori_unit as $k) :
								echo "<option value='".$k->kategori."' ";
								if($kategori==$k->kategori){
									echo "selected";
								}
								echo " >".$k->keterangan."</option>";
							endforeach;
						?>
					</select>
				</div>
				
				<div class="form-group">
					<label class="control-label">Kode Unit Kerja</label>					
					<input type="text" name="kode" class="form-control" autocomplete="off" value="<?php echo $kode ?>"/>
				</div>
				
				<div class="form-group">	
					<label class="control-label">Sub Unit dari</label>					
					<select class="e9 form-control" name="parent_id" <?php if($parentid == '') echo "disabled"; ?>  id="btn-unit">
						<option value="0" data-uri='1'>Silahkan Pilih</option>
						<?php foreach($unit_kerja as $key) { ?>
							<?php if($key->unit_id != $id && $id != $key->parent_id) { ?>
								<option <?php if($key->unit_id == $parentid) echo "selected"; ?> value="<?php echo $key->unit_id ?>"><?php echo $key->keterangan ?></option>
							<?php } ?>
						<?php } ?>
					</select>					
				</div>
				
				<div class="form-group">
					<label class="control-label">Keterangan</label>					
					<input type="text" name="keterangan" class="form-control" autocomplete="off" value="<?php echo $keterangan ?>"/>					
				</div>
				
				<div class="form-group">
					<label class="control-label">English Version</label>					
					<input type="text" name="coba" class="form-control" autocomplete="off" value="<?php echo $english_version ?>"/>					
				</div>
				
				<div class="form-group">
					<label class="control-label">Deskripsi</label>					
					<input type="text" name="desc" class="ckeditor form-control"  id="keterangan" autocomplete="off" value="<?php if(isset($desc)) echo $desc; ?>"/>					
				</div>
				
				<div class="form-group">
					<label class="control-label">Deskripsi(en)</label>					
					<input type="text" name="desc_en" class="form-control" autocomplete="off" value="<?php if(isset($desc_)) echo $desc_; ?>"/>					
				</div>
				
				<div class="form-group" id="strata">
					<label class="control-label">Strata</label>					
					<input type="text" name="strata" class="e9 form-control" autocomplete="off" value="<?php if(isset($strata)) echo $strata; ?>"/>					
				</div>
				
				<div class="form-group">
					<label class="control-label">Icon</label>
					<?php if(isset($icon)&& ($icon)){ ?>
						<div class='well'>
							<img style="width: 100px; height: auto;" src="<?php echo $this->config->file_url_view.'/'.$icon; ?>"/>
							<input type="hidden" name="hidimg" id="hidimg" value="<?php if(isset($icon)) echo $icon; ?>">
						</div>
					<?php } else { ?>
						<div class='well'>
							<p>Icon Belum Tersedia</p>
						</div>
					<?php }	?>				
					<input type="file" name="uploads" class="form-control"/>					
				</div>		
				
				<div class="form-group">
					<label class="control-label">Logo</label>
					<?php if(isset($logo) && ($logo)){ ?>
						<div class='well'>
							<img style="width: 100px; height: auto;" src="<?php echo $this->config->file_url_view.'/'.$logo; ?>"/>
							<input type="hidden" name="hidlogo" id="hidlogo" value="<?php if(isset($logo)) echo $logo; ?>">
						</div>
					<?php } else { ?>
						<div class='well'>
							<p>Logo Belum Tersedia</p>
						</div>
					<?php }	?>				
					<input type="file" name="logo_files" class="form-control"/>					
				</div>		
				
				<div class="form-group">
					<label for="aktif" class="checkbox"><input type="checkbox" name="aktif" id="aktif" value="1" <?php if($aktif==1) echo "checked"; ?>> Aktif</label>		
				</div>			
								
				<div class="form-group">
					<label class="control-label"></label>					
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input id="uri" value="<?php echo $this->location('module/master/general/unit/save'); ?>" type="hidden" >
						<button id="btn-unitkerja" class="btn btn-primary" type="submit"> Simpan</button>
						<a href="<?php echo $this->location("module/master/general/unit"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>						
				</div>
			</form>
		</div>
	</div>
