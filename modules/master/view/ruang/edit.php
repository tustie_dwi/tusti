<?php

if(isset($edit)){
	$form		= "Edit Ruang";
	
	$kode 		= $edit->kode_ruang;
	$kapasitas 	= $edit->kapasitas;
	$keterangan = $edit->keterangan;
	$fakultasid	= $edit->fakultas_id;
	$cabangid	= $edit->cabang_id;
	$id 		= $edit->ruang_id;
	$kategoriid	= $edit->kategori_ruang;
	
}else{
	$form		= "Tambah Ruang";
	
	$kode 		= '';
	$kapasitas 	= '';
	$keterangan = '';
	$id 		= '';
	$fakultasid	= "";
	$cabangid	= "";
	$kategoriid	= "lain";
}

$mconf = new model_confinfo();

$fakultas = $mconf->get_fakultas();
$cabang	  = $mconf->get_cabangub();

$kategori = array('administrasi','akademik','konseling','kuliah','lab','dosen','seminar','rapat','pimpinan','tik','lain');

?>
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil"></i> <?php echo $form ?></div>
	<div class="panel-body">	
		<form method="post" id="form-ruang" action = "<?php echo $this->location("module/master/general/ruang/save"); ?>">
			<div class="form-group">	
				<label for="fakultas" class="control-label">Fakultas</label>					
				<select name="cmbfakultas" class="form-control">
					<?php
					foreach($fakultas as $dt):
						?>
						<option value="<?php echo $dt->fakultas_id;?>" <?php if($fakultasid==$dt->fakultas_id){ echo "selected"; } ?> ><?php echo $dt->keterangan;?></option>
						<?php
					endforeach;
					?>
				</select>				
			</div>
			
			<div class="form-group">	
				<label for="fakultas" class="control-label">Cabang</label>					
				<select name="cmbcabang" class="form-control">
					<?php
					foreach($cabang as $dt):
						?>
						<option value="<?php echo $dt->cabang_id;?>" <?php if($cabangid==$dt->cabang_id){echo "selected"; } ?> ><?php echo $dt->keterangan;?></option>
						<?php
					endforeach;
					?>
				</select>				
			</div>
			
			<div class="form-group">	
				<label for="fakultas" class="control-label">Kode Ruang</label>			
				<input type="text" class="form-control" name="kode" value="<?php echo $kode; ?>">
				<input type="hidden" name="hidId" value="<?php echo $id;?>">
			</div>
			
			<div class="form-group">	
				<label for="fakultas" class="control-label">Kapasitas</label>					
				<input type="text" class="form-control" name="kapasitas" value="<?php echo $kapasitas; ?>">					
			</div>
			
			<div class="form-group">	
				<label for="fakultas" class="control-label">Kategori Ruang</label>					
				<select name="cmbkategori" class="form-control">
					<?php
					for($i=0;$i<count($kategori);$i++){
						?>
						<option value="<?php echo $kategori[$i];?>" <?php if($kategoriid==$kategori[$i]){echo "selected"; } ?> ><?php echo ucWords($kategori[$i]);?></option>
						<?php
					}
					?>
				</select>					
			</div>
				
			<div class="form-group">	
				<label for="fakultas" class="control-label">Keterangan</label>				
				<textarea class="form-control" name="keterangan" rows="3" cols="40"><?php echo $keterangan; ?></textarea>							
			</div>
			<div class="form-group">
				<input type="submit" name="b_submit" value="  Submit " class="btn btn-primary">
				<a href="<?php echo $this->location("module/master/general/ruang"); ?>" class="btn"> <i class="fa fa-ban"></i > Cancel</a>
			</div>
		</form>
	</div>
</div>
