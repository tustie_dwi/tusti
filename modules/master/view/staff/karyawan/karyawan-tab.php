<?php 
if($posts !=""){
	// var_dump($posts);
	// foreach ($posts as $dt):
		$hidId			= $posts->hid_id;
		$nik			= $posts->nik;
		$pin			= $posts->pin;
		$nama			= $posts->nama;
		$gelar_awal	 	= $posts->gelar_awal;
		$gelar_akhir	= $posts->gelar_akhir;
		$pend_trakhir	= $posts->pendidikan_terakhir;
		$tgl_lahir		= $posts->tgl_lahir;
		$jenis_kelamin 	= $posts->jenis_kelamin;
		$telp			= $posts->telp;
		$hp				= $posts->hp;
		$is_status		= $posts->is_status;
		$alamat			= $posts->alamat;
		$email			= $posts->email;
		$fakultas_id	= $posts->fakultas_id;
		$cabang_id		= $posts->cabang_id;
		$biografi		= $posts->biografi;
		$is_nik			= $posts->is_nik;
		$is_aktif		= $posts->is_aktif;
		$home_base		= $posts->home_base;
		$is_tetap		= $posts->is_tetap;
		$foto			= $posts->foto;
		$cabangid		= $posts->cabang_id;
		$tgl_msk		= $posts->tgl_masuk;
		$tgl_pensiun	= $posts->tgl_pensiun;
		$interest		= $posts->interest;
		$tugas			= $posts->tugas;
		$tugas_id		= $posts->tugas_id;
		$gol			= $posts->golongan;
		$biografi_en	= $posts->biografi_en;
		$nama_bank	= $posts->nama_bank;
		$rekening	= $posts->rekening;
	// endforeach;
		$ruang_id_ = "";
		if($ruangan){
			foreach($ruangan as $ru){
				$ruang_id_ .= $ru->ruang_id.",";
			}
			$ruang_id = substr($ruang_id_, 0, -1);
		}
	
}else{
	$hidId			= "";
	$cabangid		= "";
	$is_nik			= "";
	$jenis_kelamin  = "";
	$pend_trakhir	= "0";
	$is_aktif	    = "";
	$homebase	    = "";
	$interest	    = "";
	$is_tetap 		= "";
	$is_status 		= "";
	$home_base		= "";
	$tugas			= "";
	$tugas_id		= "";
	$gol 			= "";
	$biografi_en	= "";
}
?>

<div class="row">
	<form method=post name="form" id="form" enctype="multipart/form-data">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Biodata Pribadi</div>
				<div class="panel-body ">
					<div class="form-group">
						<label class="control-label">
						<input id="check-nip" name="chknip" <?php if($is_nik=="nip")echo " checked "; ?> value="nip" type="checkbox"><span id="label-nip">&nbsp;<?php if($is_nik=="nip")echo "NIP";else echo "NIK" ?></span></label>
						
							<input name="nik" class="form-control" value="<?php if(isset($nik))echo $nik?>" placeholder="NIP/NIK" required="" type="text">
							 <span class="help-block">centang jika NIP</span>
				  	</div>
					<div class="form-group">
						<label class="control-label">Tgl Masuk</label>
						<input name="tglmasuk" class="form_datetime form-control" value="<?php if(isset($tgl_msk))echo $tgl_msk?>" placeholder="YYYY-MM-DD" required="" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">PIN Fingerprint</label>
						<input name="pin" class="form-control" value="<?php if(isset($pin))echo $pin?>" type="text">
					</div>
				  	
				  	<div class="form-group">
						<label class="control-label">Nama</label>								
							<input name="nama" class="form-control" id="karyawan" value="<?php if(isset($nama))echo $nama?>" placeholder="Nama lengkap" required="" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">Gelar Awal</label>								
						<input name="gelarawal" class="form-control" value="<?php if(isset($gelar_awal))echo $gelar_awal?>" placeholder="Ir." type="text"><span class="help-block">contoh penulisan gelar depan: Ir.</span>
					</div>
					
					<div class="form-group">
						<label class="control-label">Gelar Belakang</label>
						<input name="gelarakhir" class="form-control" value="<?php if(isset($gelar_akhir))echo $gelar_akhir?>" placeholder="ST., MT" type="text"><span class="help-block">contoh penulisan gelar belakang: ST., MT</span>
					</div>
					
					<div class="form-group">
						<label class="control-label">Pendidikan Terakhir</label>
						<select class="form-control e9" name="pend_trakhir" id="pend_trakhir">
							<option value="0">Silahkan Pilih</option>
							<option value="SMA">SMA/Sederajat</option>
							<option value="Diploma">Diploma [D3]</option>
							<option value="S1">Sarjana [S1]</option>
							<option value="S2">Magister [S2]</option>
							<option value="S3">Doktor [S3]</option>
						</select>
						<input type="hidden" name="pend_trakhir_edit" value="<?php if(isset($pend_trakhir)) echo $pend_trakhir ?>" />
					</div>
					
					<div class="form-group">
						<label class="control-label">Tgl Lahir</label>
						<input name="tgl" class="form_datetime form-control" value="<?php if(isset($tgl_lahir))echo $tgl_lahir?>" placeholder="YYYY-MM-DD" required="" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">Jenis Kelamin</label>								
						<label class="radio-inline"><input name="rjenis" <?php if($jenis_kelamin=="L")echo " checked "; ?> value="L" type="radio"> Laki -laki</label>									
						<label class="radio-inline"><input name="rjenis" <?php if($jenis_kelamin=="P")echo " checked "; ?> value="P" type="radio"> Perempuan</label>
					</div>
					
					<div class="form-group">
						<label class="control-label">Telepon</label>								
						<input name="telepon" class="form-control" value="<?php if(isset($telp))echo $telp?>" placeholder="Telepon" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">No Hp</label>
						
							<input name="nohp" class="form-control" value="<?php if(isset($hp))echo $hp?>" placeholder="No hp" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">Email</label>
						<input id="email" name="email" class="form-control" value="<?php if(isset($email))echo $email?>" placeholder="Email" type="text">
					</div>				
					
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Data Pendukung</div>
				<div class="panel-body ">					
					
					<div class="form-group">
						<label class="control-label">Alamat</label>								
						<textarea name="alamat" class="form-control" placeholder="Alamat rumah"><?php if(isset($alamat))echo $alamat?></textarea>
					</div>

					<div class="form-group">
						<label class="control-label">Nama Bank</label>								
						<input name="namabank" class="form-control" value="<?php if(isset($nama_bank))echo $nama_bank?>" placeholder="Nama bank" type="text">
					</div>
					<div class="form-group">
						<label class="control-label">No Rekening</label>								
						<input name="rekening" class="form-control" value="<?php if(isset($rekening))echo $rekening?>" placeholder="No rekening" type="text">
					</div>					
					
					<div class="form-group">
						<label class="control-label">Fakultas</label>								
						<select id="select_fakultas" class="e9 form-control" name="fakultas">
							<option value="-">Select Fakultas</option>
							<?php if(count($fakultas)> 0) {
								foreach($fakultas as $f) :
									echo "<option value='".$f->hid_id."' ";
									if($fakultas_id==$f->hid_id){
										echo "selected";
									}
									echo " >".$f->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
					
					<div class="form-group">
						<label class="control-label">Cabang</label>								
						<select id="select_cabang" class="e9 form-control" name="cabang">
							<option value="-">Select Cabang</option>
							<?php if(count($cabang)> 0) {
								foreach($cabang as $c) :
									echo "<option value='".$c->cabang_id."' ";
									if($cabangid==$c->cabang_id){
										echo "selected";
									}
									echo " >".$c->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
					
					<div class="form-group">
						<label class="control-label">Ruang</label>								
						<select id="select_ruang" class="e9 form-control" name="ruang[]" multiple="multiple">
							<?php 
							if(isset($ruang)){
								foreach($ruang as $r){
									echo "<option value='".$r->ruang_id."'";
									if(isset($ruangan)){
										foreach($ruangan as $dr){
											if($dr->ruang_id == $r->ruang_id)  echo "selected";
										}
									}
									echo ">".$r->keterangan." - ".$r->kode_ruang."</option>";
								}
							} 
							?>
						</select>
						<input type="hidden" class="form-control" name="ruang_id" value="<?php if(isset($posts)) echo $ruang_id ?>" />
					</div>
					
					<div class="form-group">
						<label class="control-label">Status Aktif</label>	<br>																
						<label class="radio-inline"><input name="saktif" <?php if($is_aktif=="aktif") echo " checked "; ?> value="aktif" type="radio"> Aktif</label>									
						<label class="radio-inline"><input name="saktif" <?php if($is_aktif=="tubel") echo " checked "; ?> value="tubel" type="radio"> Tugas Belajar</label>
						<label class="radio-inline"><input name="saktif" <?php if($is_aktif=="keluar") echo " checked "; ?> value="keluar" type="radio"> Keluar</label>
						<label class="radio-inline"><input name="saktif" <?php if($is_aktif=="meninggal") echo " checked "; ?> value="meninggal" type="radio"> Meninggal</label>
						<label class="radio-inline"><input name="saktif" <?php if($is_aktif=="pensiun") echo " checked "; ?> value="pensiun" type="radio"> Pensiun</label>
					</div>
					<div class="form-group">
						<label class="control-label">Golongan Terakhir</label>
						<input name="gol_terakhir" class="form-control" value="<?php if(isset($gol))echo $gol?>" placeholder="Golongan Ex. Ia, IIb, IIIc, IVd" type="text">
					</div>
					
					<div class="form-group form-group-pensiun" style="display :none;">
						<label class="control-label">Tgl Pensiun</label>
						<input name="tglpensiun" class="form_datetime form-control" value="<?php if(isset($tgl_pensiun))echo $tgl_pensiun?>" placeholder="YYYY-MM-DD" required="" type="text">
					</div>
					
					<div class="form-group checkbox">
						<label>
						<input id="dsn-ttp" name="chkdsnstatus" value="" <?php if($is_tetap=="tetap")echo " checked "; ?> type="checkbox"><span id="label-dsn"><b><?php if($is_tetap=="tetap")echo "Tetap";else echo "Kontrak"; ?></b></span></label>								
						<span class="help-block">centang jika tetap</span>
				  	</div>
					
					<div class="form-group checkbox">
						<label><input name="chkdosen" <?php if($is_status=="dosen")echo " checked "; ?> value="1" type="checkbox"><b>Dosen</b></label>
						<span class="help-block">centang jika dosen</span>
					</div>
					
					<div class="form-group">
						<label class="control-label">Home Base</label>	<br>																
						<label class="radio-inline"><input name="homebase" <?php if($home_base=="UB")echo " checked "; ?> value="UB" type="radio"> UB</label>									
						<label class="radio-inline"><input name="homebase" <?php if($home_base=="PTIIK")echo " checked "; ?> value="PTIIK" type="radio"> PTIIK</label>
						<label class="radio-inline"><input name="homebase" <?php if($home_base=="luar")echo " checked "; ?> value="luar" type="radio"> Luar</label>
					</div>
					
					<div class="form-group">
						<label class="control-label">Tugas</label>
						<input type="text" class="form-control tagmanager" name="tugas" id="tugas_karyawan" placeholder="Tugas"/>
						<input type="hidden" class="form-control" name="tugas_id" id="tugas_id" value="<?php echo $tugas_id ?>"/>
						<input type="hidden" class="form-control" name="tugas_" id="tugas_" value="<?php echo $tugas ?>"/>
					</div>
					
					<div class="form-group">
						<label class="control-label" for="content_title">Foto</label>								                
						<input id="files" name="files" type="file">
						<output id="list"><?php if(isset($foto)&&$foto!=""){ ?><img width="80" src='<?php echo $this->config->file_url_view."/".$foto ?>' /><?php } ?></output>
						<input name="hidimg" value="<?php if(isset($foto))echo $foto; ?>" type="hidden">
					</div>
					
					<div class="form-actions">
						<label class="control-label">&nbsp;</label>
						<input name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>" type="hidden">
						<input name="b_karyawan" id="b_karyawan" value=" Data Valid &amp; Save " class="btn btn-primary" type="submit">&nbsp;
						<a href="<?php echo $this->location('module/masterdata/dosen'); ?>" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>