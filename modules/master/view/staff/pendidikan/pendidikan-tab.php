<?php
	//var_dump($edit_pend);
?>
<div class="row">
	<div class="col-sm-8">
		<div class="panel panel-default">
			<div class="panel-heading">Daftar Riwayat Pendidikan</div>
			<div class="panel-body ">
				<div id="display">
					<?php if(!isset($formal) && !isset($nonformal)){ ?>
							<div class="col-sm-12" align="center" style="margin-top:20px;">
							    <div class="well">Data tidak dapat ditemukan</div>
							</div>
					<?php }
						  if(isset($formal)){ ?>
							<h4>FORMAL</h4>
							<table class='table table-condensed example'>
								<thead>
									<tr>
										<th>Jenjang</th>
										<th>Keterangan</th>
										<th>Latar Belakang Ilmu</th>
										<?php if($hid_id==$this->coms->authenticatedUser->staffid || $role == 'administrator' || $role == 'student employee'){ ?>
										<th>&nbsp;</th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach($formal as $f){ ?>
									<tr id="<?php echo "formal".$f->formalid ?>">
										<td><?php echo "<label class='label label-success'>".$f->jenjang."</label>" ?></td>
										<td>
											<?php echo $f->program_studi."<br>" ?>
											<?php echo "<code>".$f->nama_sekolah."</code><br>" ?>
											<?php echo $f->alamat_sekolah."<br>" ?>
										</td>
										<td><?php echo $f->latar_belakang_ilmu ?></td>
										<?php if($hid_id==$this->coms->authenticatedUser->staffid || $role == 'administrator' || $role == 'student employee'){ ?>
										<td>
											<ul class='nav nav-pills'>
												<li class='dropdown pull-right'>
												  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
												  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
													<li>
													<a href="javascript::" class="edit_pend" data-jenis="formal" data-pend="<?php echo ucwords($f->formalid) ?>"><i class="fa fa-edit"></i> Edit</a>
													<a href="javascript::" class="delete_pend" data-jenis="formal" data-pend="<?php echo ucwords($f->formalid) ?>"><i class="fa fa-trash-o"></i> Delete</a>
													</li>
												  </ul>
												</li>
											</ul>
										</td>
										<?php } ?>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<br>
					<?php }
						  if(isset($nonformal)){ ?>
							<h4>NON FORMAL</h4>
							<table class='table table-condensed example'>
								<thead>
									<tr>
										<th>Peran</th>
										<th>Nama Kegiatan</th>
										<th>Penyelenggara</th>
										<th>Tempat Kegiatan</th>
										<?php if($hid_id==$this->coms->authenticatedUser->staffid || $role == 'administrator' || $role == 'student employee'){ ?>
										<th>&nbsp;</th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach($nonformal as $nf){ ?>
									<tr id="<?php echo "nonformal".$nf->nonformalid ?>">
										<td><?php echo ucwords($nf->sebagai) ?></td>
										<td>
											<?php echo ucwords($nf->nama_kegiatan)."<br>" ?>
											<?php echo "<code>".$nf->jeniskegiatan."</code><br>" ?>
											<?php echo "<label class='label label-success'>".ucfirst($nf->tingkat)."</label><br>" ?>
										</td>
										<td><?php echo ucwords($nf->penyelenggara) ?></td>
										<td>
											<?php echo ucwords($nf->tempat_kegiatan)."<br>" ?>
											<?php echo "<code>".ucwords($nf->kota_kegiatan)."</code>" ?>
										</td>
										<?php if($hid_id==$this->coms->authenticatedUser->staffid || $role == 'administrator' || $role == 'student employee'){ ?>
										<td>
											<ul class='nav nav-pills'>
												<li class='dropdown pull-right'>
												  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
												  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
													<li>
													<a href="javascript::" class="edit_pend" data-jenis="nonformal" data-pend="<?php echo ucwords($nf->nonformalid) ?>"><i class="fa fa-edit"></i> Edit</a>
													<a href="javascript::" class="delete_pend" data-jenis="nonformal" data-pend="<?php echo ucwords($nf->nonformalid) ?>"><i class="fa fa-trash-o"></i> Delete</a>
													</li>
												  </ul>
												</li>
											</ul>
										</td>
										<?php } ?>
									</tr>
									<?php } ?>
								</tbody>
							</table>
					<?php } ?>
				</div>
			</div>
		</div>
    </div>
	<div class="col-sm-4">
		<div class="form-group">
			<input type="hidden" id="hid" value="<?php echo $id ?>" />
			<input type="hidden" id="type_form" value="<?php if(isset($jenis_pend)) echo $jenis_pend ?>" />
			<label class="control-label">Jenis Pendidikan</label>
			<div class="controls">
			    <label class="radio-inline">
			     	<input name="pendidikan" value="formal" type="radio" checked>Formal
			    </label>
			    <label class="radio-inline">
			     	<input name="pendidikan" value="nonformal" type="radio">Non-Formal
			    </label>
		    </div>
		    <input type="hidden" name="tab" />
		</div>
	    <div id="form-formal">
	    	<?php echo $this->view('staff/pendidikan/formal.php', $data); ?>
	    </div>
	    <div id="form-nonformal">
	    	<?php echo $this->view('staff/pendidikan/non-formal.php', $data); ?>
	    </div>
	    <!-- Modal Folder-->
		<div class="modal fade" id="media" tabindex="-1" role="dialog" aria-labelledby="mediaLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close close_btn" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="mediaLabel">Media Library</h4>
		      </div>
		      <div class="modal-body">
		    	<div class="content-table">
					<div class="view_content">
					
					</div>
				</div>
				<div class="content-file"></div>	
		      </div>
		      <div class="modal-footer">
				<input type="hidden" name="parent" class="form-control" id="par" value="0"/>
		        <button type="button" class="btn btn-default" id="home_button" onclick="view_content('0','<?php echo $id ?>')"><i class="fa fa-home"></i> Home</button>
		        <button type="button" class="btn btn-primary close_btn" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
    </div>
</div>