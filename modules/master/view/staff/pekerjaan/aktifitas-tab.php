<?php 
	$hidId			= $posts->hid_id;
	$nama			= $posts->nama;
	$fakultas_id	= $posts->fakultas_id;
	$cabangid		= $posts->cabang_id;
	$golonganid		= $posts->golongan;
	$tgl_masuk		= $posts->tgl_masuk;
?>

<div class="row">	
	<div class="col-md-12">	
		<div class="panel panel-default">
			<div class="panel-heading">Log Aktifitas</div>
			<div class="panel-body ">
				<table class="table example">
					<thead>
						<tr>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						
						$tugas = $mdosen->get_rekap_aktifitas($posts->hid_id);
						
						if($tugas):
							foreach($tugas as $dt):
								?>
								<tr>
									<td>
										<div class="col-md-8">
											<?php echo $dt->judul  ?>&nbsp;<span class="label label-danger"><?php echo $dt->jenis_kegiatan ?></span><br>
											<small>
												<?php
												
											echo "<span class='text text-danger'><i class='fa fa-calendar-o'></i>&nbsp;";
												if(date("M d, Y",strtotime($dt->tgl)) == date("M d, Y",strtotime($dt->tgl_selesai))){
													echo date("M d, Y",strtotime($dt->tgl));		
												
												}else{
													if(date("M, Y",strtotime($dt->tgl)) == date("M, Y",strtotime($dt->tgl_selesai))){
														echo date("M d",strtotime($dt->tgl))." - ".date("d, Y",strtotime($dt->tgl_selesai));
													}else{
														echo date("M d",strtotime($dt->tgl))." - ".date("M d, Y",strtotime($dt->tgl_selesai));
													}
												}
										
										echo "&nbsp; <i class='fa fa-clock-o'></i> ".date("H:i",strtotime($dt->jam_mulai))." - ".date("H:i",strtotime($dt->jam_selesai))."</span>&nbsp; ";
										echo "<i class='fa fa-map-marker'></i> ".$dt->lokasi."&nbsp;";
														if($dt->inf_ruang){
															echo "R.". $dt->inf_ruang;
														}else{
															echo "&nbsp;";
														} 
											?>
											</small>
										</div>
										
									</td>
								</tr>
								<?php
							endforeach;
						else:
							?>
							<tr><td>Sorry, no content to show</td></tr>
							<?php
						endif;						
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
