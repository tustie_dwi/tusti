<i id="loading" class="fa fa-refresh fa-spin" style="position: absolute; display: none"></i>
<h3 style="margin-top: 0; text-align: center"><?php echo $reservasi->kegiatan ?></h3>
<p class="text-center" id="detail-keterangan">
	<i style="color: #2980b9" class="fa fa-bookmark"></i> <?php echo $reservasi->jenis_kegiatan ?>
	<i style="color: #e74c3c" class="fa fa-calendar"></i> <?php echo $reservasi->tgl_mulai ?> s/d <?php echo $reservasi->tgl_selesai ?><br>
	<?php
		if($reservasi->is_valid == 0){
			echo "<i class='fa fa-tasks'></i> Belum diprosess";
		}
		elseif($reservasi->is_valid == 1){
			echo "<i class='fa fa-check'></i> Disetujui";
		}
		elseif($reservasi->is_valid == 2){
			echo "<i class='fa fa-ban'></i> Ditolak";
		}
	?>
</p>

<span id="approval-wrap">
	<?php if($reservasi->is_valid == 0) : ?>
		<button onclick="approve('<?php echo $jadwal ?>','1')" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Setujui</button>
		<button onclick="approve('<?php echo $jadwal ?>','2')"  class="btn btn-danger btn-xs"><i class="fa fa-ban"></i> Tolak</button>
	<?php elseif($reservasi->is_valid == 1) : ?>
		<button onclick="approve('<?php echo $jadwal ?>','2')"  class="btn btn-danger btn-xs"><i class="fa fa-ban"></i> Tolak</button>
	<?php elseif($reservasi->is_valid == 2) : ?>
		<button onclick="approve('<?php echo $jadwal ?>','1')" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Setujui</button>
	<?php endif; ?>
</span>
<p id="detail-catatan" style="margin-top: 10px">
<?php echo $reservasi->catatan ?>
</p>

<br>
<p id="ruang-field">
	<?php
		if($ruang){
			foreach($ruang as $key){
				echo '<button class="btn btn-primary btn-xs">'.$key->keterangan.' <i class="fa fa-clock-o"></i> '. $key->jam_mulai . ' - ' . $key->jam_selesai .'</button>';
			}
		}
	?>
</p>

<style type="text/css" media="screen">
	#ruang-field .btn-xs{
		margin-bottom: 3px;
		margin-left: 3px;
	}
</style>
