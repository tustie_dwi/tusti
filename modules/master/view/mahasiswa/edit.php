<?php $this->head();

if (isset($posts)) {
	$header = "Edit Mahasiswa";

	if($posts){
		$mhs_id			= $posts->mhs_id;
		$hidId			= $posts->mahasiswa_id;
		$nim			= $posts->nim;
		$nama			= $posts->nama;
		$jenis_kelamin	= $posts->jenis_kelamin;
		$tmpt_lahir		= $posts->tmp_lahir;
		$tgl_lahir		= $posts->tgl_lahir;
		$seleksi		= $posts->jalur_seleksi;
		$angkatan		= $posts->angkatan;
		$cabangid		= $posts->cabang_id;
		$prodiid		= $posts->prodi_id;
		$email			= $posts->email;
		$alamat			= $posts->alamat;
		$telp			= $posts->telp;
		$hp				= $posts->hp;
		$isaktif		= $posts->is_aktif;
		$ipk			= $posts->ipk_lulus;
		$tgl_lulus		= $posts->tgl_lulus;
		$masa_kuliah	= $posts->inf_masa_studi;
		$tgl_mulai_kuliah	= $posts->tgl_mulai_studi;
		$tgl_selesai_kuliah	= $posts->tgl_selesai_studi;
		$image			= $posts->foto;
		$nama_ortu		= $posts->nama_ortu;
		$email_ortu		= $posts->email_ortu;
		$alamat_ortu	= $posts->alamat_ortu;
		$telp_ortu		= $posts->telp_ortu;
		$hp_ortu		= $posts->hp_ortu;
		$alamat_surat	= $posts->alamat_surat;
		$catatan		=$posts->catatan;
		$ceknew = 0;
	}

} else {
	$header = "Write New Mahasiswa";
	$nim			= "";
	$nama			= "";
	$jenis_kelamin	= "";
	$tmpt_lahir		= "";
	$tgl_lahir		= "";
	$seleksi		="";
	$angkatan		= "";
	$cabangid		= "";
	$prodiid		= "";
	$email			= "";
	$alamat			= "";
	$telp			= "";
	$hp				= "";
	$isaktif		= "";
	$ipk			= "";
	$tgl_lulus		= "";
	$masa_kuliah	= "";
	$tgl_mulai_kuliah	= "";
	$tgl_selesai_kuliah	= "";
	$image			= "";
	$nama_ortu		= "";
	$email_ortu		="";
	$alamat_ortu	= "";
	$telp_ortu		="";
	$hp_ortu		= "";
	$alamat_surat	="";
	$catatan		="";
	$ceknew = 1;
}
?>



<div class="row">
	
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/mhs'); ?>">Kemahasiswaan</a></li>
	  <li class="active"><a href="#"><?php echo $header;?></a></li>
	</ol>
	<div class="breadcrumb-more-action">
		<?php if($nim !=""){	?>
		<a href="<?php echo $this->location('module/master/mhs/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Mahasiswa</a>
		<?php } ?>
	</div>
	<div class="row">
	<form method="post" id="form-mhs">
		 <div class="col-md-6">
		 	<div class="block-box">
			  <div class="header"><h4>Biodata Pribadi</h4></div>
				<div class="content">
					<div class="form-group">
						 <label class="control-label">NIM</label>
							<input type="number" required="required" class="form-control" name="nim_mhs" id="nim_mhs" placeholder="NIM Mahasiswa" value="<?php if(isset($nim)) echo $nim;?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Nama</label>
							<input type="text" required="required" class="form-control" name="nama_mhs" id="nama_mhs" placeholder="Nama Mahasiswa" value="<?php if(isset($nama)) echo $nama;?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Jenis Kelamin</label><br>								
							<input name="rjenis" <?php if(isset($jenis_kelamin)&&$jenis_kelamin=="L")echo " checked "; ?> value="L" type="radio"> Laki -laki								
							<input name="rjenis" <?php if(isset($jenis_kelamin)&&$jenis_kelamin=="P")echo " checked "; ?> value="P" type="radio"> Perempuan
					</div>
					<div class="form-group">
						 <label class="control-label">TTL</label>
							<input type="text" required="required" class="form-control" name="tmpt_lahir_mhs" id="tmpt_lahir_mhs" placeholder="Tempat Lahir Mahasiswa" value="<?php if(isset($tmpt_lahir)) echo $tmpt_lahir;?>"/>
						 <label class="control-label">&nbsp;</label>
							<input type="text" required="required" class="form-control form_datetime" name="tgl_lahir_mhs" id="tgl_lahir_mhs" placeholder="Tanggal Lahir Mahasiswa" value="<?php if(isset($tgl_lahir)) echo $tgl_lahir;?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Jalur Seleksi</label>
						 <select class="form-control e9" name="jalur_seleksi_mhs" id="jalur_seleksi_mhs">
						 	<option value="reguler" <?php if(isset($seleksi)&&$seleksi=="reguler") echo "selected"; ?> >Reguler</option>
						 	<option value="sap" <?php if(isset($seleksi)&&$seleksi=="sap") echo "selected"; ?>>SAP</option>
						 </select>
					</div>
					<div class="form-group">
						 <label class="control-label">Angkatan</label>
							<input type="number" required="required" class="form-control" name="angkatan_mhs" id="angkatan_mhs" placeholder="Angkatan" value="<?php if(isset($angkatan)) echo $angkatan;?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Cabang</label>
						 <select class="for-control e9" name="cabang_mhs" >
						 	<option value="0">Pilih Cabang</option>
						 	<?php
							if (count($cabang) > 0) {
								foreach ($cabang as $dt) :
									echo "<option value='" . $dt -> cabang_id . "' ";
									if($cabangid == $dt -> cabang_id ){
										echo "selected";
									}
									echo ">" . $dt -> keterangan . "</option>";
								endforeach;
							}
							?>
						 </select>
					</div>
					<div class="form-group">
						 <label class="control-label">Program Studi</label>
						 <select class="for-control e9" name="prodi_mhs" id="prodi_mhs" >
						 	<option value="0">Pilih Prodi</option>
						 	<?php
							if (count($prodi) > 0) {
								foreach ($prodi as $dt) :
									echo "<option value='" . $dt -> prodi_id . "' ";
									if($prodiid==$dt -> prodi_id){
										echo "selected";
									}
									echo ">" . $dt -> keterangan . "</option>";
								endforeach;
							}
							?>
						 </select>
						 <?php if($prodiid!=""){ ?>
						 	<input type="hidden" name="prodi_mhs" value="<?php if(isset($prodiid))echo $prodiid; ?>" />
					 	 <?php } ?>
					</div>
					<div class="form-group">
						 <label class="control-label">Email</label>
							<input type="email" required="required" class="form-control" name="email_mhs" id="email_mhs" placeholder="Email Mahasiswa" value="<?php if(isset($email))echo $email; ?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Alamat Malang</label>
							<textarea required="required" class="form-control" name="alamat_mhs" id="alamat_mhs" placeholder="Alamat Mahasiswa di Malang"><?php if(isset($alamat))echo $alamat; ?></textarea>
					</div>
					<div class="form-group">
						 <label class="control-label">Telp</label>
							<input type="text" required="required" class="form-control" name="tlp_mhs" id="tlp_mhs" placeholder="No Telp Mahasiswa" value="<?php if(isset($telp))echo $telp; ?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">HP</label>
							<input type="text" required="required" class="form-control" name="hp_mhs" id="hp_mhs" placeholder="No HP Mahasiswa" value="<?php if(isset($hp))echo $hp; ?>"/>
					</div>
					<div class="checkbox">
						<label>
						 <input type="checkbox" name="is_aktif" id="is_aktif" value="1" <?php if ($isaktif=="aktif") { echo "checked"; } ?>>Aktif
						</label>
					  </div>
					
					<div class="form-group tidak-aktif">
						 <label class="control-label">IPK Lulus</label>
							<input type="text" class="form-control" name="ipk_lulus_mhs" id="ipk_lulus_mhs" placeholder="IPK Lulus" value="<?php if(isset($ipk))echo $ipk; ?>"/>
					</div>
					<div class="form-group tidak-aktif">
						 <label class="control-label">Tanggal Lulus</label>
							<input type="text" class="form-control form_datetime" name="tgl_lulus_mhs" id="tgl_lulus_mhs" placeholder="Tanggal Lulus" value="<?php if(isset($tgl_lulus))echo $tgl_lulus; ?>"/>
					</div>
					<div class="form-group tidak-aktif">
						 <label class="control-label">Masa Studi</label>
							<input type="number" class="form-control" name="masa_studi_mhs" id="masa_studi_mhs" placeholder="Masa Studi" value="<?php if(isset($masa_kuliah))echo $masa_kuliah; ?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Tanggal Mulai Studi</label>
							<input type="text" class="form-control form_datetime" name="tgl_mulai_studi_mhs" id="tgl_mulai_studi_mhs" placeholder="Tanggal Mulai Studi" value="<?php if(isset($tgl_mulai_kuliah)) echo $tgl_mulai_kuliah; ?>" />
					</div>
					<div class="form-group tidak-aktif">
						 <label class="control-label">Tanggal Selesai Studi</label>
							<input type="text" class="form-control form_datetime" name="tgl_selesai_studi_mhs" id="tgl_selesai_studi_mhs" placeholder="Tanggal Selesai Studi" value="<?php if(isset($tgl_selesai_kuliah)) echo $tgl_selesai_kuliah; ?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Foto</label>
						 <div class="controls">                    
							<input id="files" name="files" type="file">
							<output id="list"></output>
							<?php 
								if($ceknew == 0){
									if(isset($image)){ ?>
										<div class='well'>
											<img style="width: 100px; height: auto;" src="<?php echo $this->config->file_url_view."/".$image; ?>"/>
											<input name="hidimg" value="<?php echo $image ?>" type="hidden">
										</div>
									<?php } else { ?>
										<div class='well'>
											<p>Foto Belum Tersedia</p>
										</div>
									<?php } 
								}
							?>
						</div>
					</div>
				</div>
			</div>
		
	 	 </div>
	 	 <div class="col-md-6">
	 	 	<div class="block-box">
			  <div class="header"><h4>Data Orang Tua</h4></div>
				<div class="content">
					<div class="form-group">
						 <label class="control-label">Nama</label>
							<input type="text" required="required" class="form-control" name="nama_ortu" id="nama_ortu" placeholder="Nama Orang Tua" value="<?php if(isset($nama_ortu)) echo $nama_ortu; ?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Email</label>
							<input type="email" class="form-control" name="email_ortu" id="email_ortu" placeholder="Email Orang Tua" value="<?php if(isset($email_ortu)) echo $email_ortu; ?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">Alamat</label>
							<textarea required="required" class="form-control" name="alamat_ortu" id="alamat_ortu" onchange="alamat()" placeholder="Alamat Orang Tua"><?php if(isset($alamat_ortu)) echo $alamat_ortu; ?></textarea>
					</div>
					<div class="form-group">
						 <label class="control-label">Telp</label>
							<input type="text" required="required" class="form-control" name="tlp_ortu" id="tlp_ortu" placeholder="No Telp Orang Tua" value="<?php if(isset($telp_ortu)) echo $telp_ortu; ?>"/>
					</div>
					<div class="form-group">
						 <label class="control-label">HP</label>
							<input type="text" required="required" class="form-control" name="hp_ortu" id="hp_ortu" placeholder="No HP Orang Tua" value="<?php if(isset($hp_ortu)) echo $hp_ortu; ?>"/>
					</div>
				</div>
			</div>
			<div class="block-box">
			  <div class="header"><h4>Surat Menyurat</h4></div>
				<div class="content">
					<div class="form-group">
						 <label class="control-label" style="color: red"><small>Tuliskan alamat surat menyurat dengan benar. Apabila alamat surat menyurat berbeda dengan alamat orang tua, beri keterangan pada kolom <b>Catatan</b></small></label>
					</div>
					<div class="form-group">
						 <label class="control-label">Alamat</label>
							<textarea required="required" class="form-control" name="alamat_surat" id="alamat_surat" placeholder="Alamat Surat Menyurat"><?php if(isset($alamat_surat)) echo $alamat_surat; ?></textarea>
					</div>
					<div class="form-group">
						 <label class="control-label">Catatan *)</label>
							<textarea class="form-control" name="catatan_surat" id="catatan_surat" placeholder="Catatan"><?php if(isset($catatan)) echo $catatan; ?></textarea>
					</div>
				</div>
			</div>
		 <input type="hidden" name="hidId" value="<?php if(isset($hidId)) echo $hidId;?>" />
	 	 <input type="hidden" name="ceknew" value="<?php if(isset($ceknew)) echo $ceknew;?>" />
	 	 <input type="submit" class="btn btn-primary" value="Simpan" />
	 	 <a href="<?php echo $this->location("module/master/mhs"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>
	 	 </div>
	 	 
 	</form>
	</div>
</div>

<?php $this->foot(); ?>