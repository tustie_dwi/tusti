<div class="panel-heading">
	<i class="fa fa-bookmark"></i> KRS Mahasiswa
	
	<button onclick="tampil_mhs()" class="btn btn-danger btn-xs pull-right"><i class="fa fa-times"></i></button>
</div>
<div class="panel-body">	
	<dl class="dl-horizontal">
    	<dt>Nama / NIM</dt>
    	<dd>: <?php echo $mhs->nama . ' / '. $mhs->nim ?></dd>
    	
    	<dt>Tahun Akademik</dt>
    	<dd id="tahun-info"></dd>
    	
    	<dt>Fakultas / Prodi</dt>
    	<dd>: <?php echo $mhs->kode_fakultas . ' / '. $mhs->prodi_id ?></dd>
    	
    	<dt>Angkatan</dt>
    	<dd>: <?php echo $mhs->angkatan ?></dd>
    </dl>
    
    <table class="table table-borderedx table-condensed">
    	<thead>
    		<tr>
	    		<th style="font-weight: bold" width="5%">No</th>
	    		<th style="font-weight: bold" width="15%">Kode</th>
	    		<th style="font-weight: bold">Mata Kuliah</th>
	    		<th style="font-weight: bold" width="10%" class="text-center">Kelas</th>
	    		<th style="font-weight: bold" width="10%" class="text-center">SKS</th>
	    		<th style="font-weight: bold" width="10%" class="text-center">Nilai</th>
    		</tr>
    	</thead>
    	
    	<tbody>
    		<?php if($krs) : ?>
    			<?php $no=1; $total_sks = 0; $inf_bobot=0; foreach($krs as $key ) { ?>
		    		<tr>
			    		<td><?php echo $no . '.'; $no++ ?></td>
			    		<td><?php echo $key->kode_mk ?></td>
			    		<td><?php echo $key->keterangan ?> </td>
			    		<td class="text-center"><?php echo $key->kelas ?></td>
			    		<td class="text-center"><?php echo $key->sks; $total_sks += $key->sks; ?></td>
			    		<td class="text-center"><?php echo $key->inf_huruf; $inf_bobot += ($key->inf_bobot * $key->sks); ?></td>
		    		</tr>
    			<?php } ?>
    			
    			<tr>
    				<td style="font-weight: bold" colspan="3" class="text-center">Total SKS</td>
    				<td></td>
    				<td style="font-weight: bold" class="text-center"><?php echo $total_sks ?></td>
    				<td></td>
    			</tr>
    			<tr>
    				<td style="font-weight: bold" colspan="3" class="text-center">IP Semester</td>
    				<td></td>
    				<td style="font-weight: bold" class="text-center"><?php echo round(($inf_bobot/$total_sks),2) ?></td>
    				<td></td>
    			</tr>
    		<?php endif; ?>
    	</tbody>
    </table>
</div>