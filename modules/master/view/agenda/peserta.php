<div class='form-group'> 
	<label><abbr title='Informasi peserta yang mengikuti kegiatan'>Informasi Peserta</abbr></label>	
	<textarea name='infpeserta' class='form-control' rows='5'><?php echo $infpeserta;?></textarea>
</div>
<div class='form-group'> 
	<label class="control-label">&nbsp;</label>
	
		<h5>Dosen/Staff</h5>
		<table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>By</th>
						<th>Value</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Group</td>
						<td>
						<em>
							<label class='radio inline'><input type="radio" name='srgroup' value='' <?php if($gruppeserta==''){ echo "checked"; } ?>> None</label>
							<label class='radio inline'><input type="radio" name='srgroup' value='staff' <?php if($gruppeserta=='staff'){ echo "checked"; } ?> > Semua Staff <?php echo $fakultas; ?></label>
							<label class='radio inline'><input type="radio" name='srgroup' value='dosen' <?php if($gruppeserta=='dosen'){ echo "checked"; } ?>> Semua Dosen <?php echo $fakultas; ?></label>
							<label class='radio inline'><input type="radio" name='srgroup' value='all' <?php if($gruppeserta=='all'){ echo "checked"; } ?>> Semua Dosen & Staff <?php echo $fakultas; ?></label></em>
						</td>
					</tr>
					<tr>
						<td>Nama</td>
						<td>
							<?php
		
								$nama="";

								if(isset($staff)){
									if($staff){								
										$i=0;
										
										foreach($staff as $dt):
											$i++;
											
											if($dt->instansi){
												$strinstansi = "@".$dt->instansi;
											}else{
												$strinstansi = "";
											}
											
											$nama = $nama . $dt->nama .$strinstansi."& ";											 													
										endforeach;
									}
								}
								?>
							<div class="well">
								<input type="hidden" value="<?php echo $nama; ?>" class="tmpstaff" />	
								<input type='text' name='tpeserta' placeholder='Nama Peserta..' class='tagStaff form-control'/>
							</div>
						</div>
						</td>
					</tr>
				</tbody>
			</table>
</div>
<div class='form-group'> 
	<label class="control-label">&nbsp;</label>
	
		<h5>Mahasiswa</h5>
          <table class="table table-bordered table-condensed">
				<thead>
					<tr>
						<th>By</th>
						<th>Value</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Angkatan</td>						
						<td>
						<?php
								$nangkatan="";
								if(isset($mangkatan)){
									if($mangkatan){										
										$i=0;										
										foreach($mangkatan as $dt):
											$i++;											
											
											$nangkatan = $nangkatan .$dt->group_by."& ";																										
										endforeach;										
									}
								}
								?>
						<input type="hidden" value="<?php echo $nangkatan; ?>" class="tmpangkatan" />	
						<input type='text' name='tangkatan' placeholder='Angkatan..' class='tagAngkatan form-control'/></td>
					</tr>
					<tr>
						<td>Nama</td>
						<td>
							<?php
								$namamhs="";
								if(isset($mhs)){
									if($mhs){
										//echo "<p>";
											$i=0;
											
											foreach($mhs as $dt):
												$i++;
										
												//echo "<span class='label label-info'>".$dt->nama;
												if($dt->mahasiswa_id){
													//echo "@".$dt->instansi;
													$strinstansi = " - ".$dt->nama;
												}else{
													$strinstansi = "";
												}
												
												$namamhs = $namamhs . $dt->mahasiswa_id .$strinstansi."& ";
												//echo "</span>&nbsp;"; 															
											endforeach;
										//echo "</p>";
									}
								}
								?>
						<div class="well">
						<input type="hidden" value="<?php echo $namamhs; ?>" class="tmpmhs" />	
						<input type='text' name='tmhs' placeholder='Nama Mahasiswa..' class='tagMhs form-control'/>
						</div>
						</td>
					</tr>
				</tbody>
			</table>
</div>
	
<div class='form-group'> 
	<label><abbr title='Peserta dari luar'>*) Peserta luar</abbr></label>
	
	<?php
		$namapeserta="";
		if(isset($peserta)){
			if($peserta){
			//echo "<p>";
					$i=0;
					
					foreach($peserta as $dt):
						$i++;
						if($dt->instansi){
							//echo "@".$dt->instansi;
							$strinstansi = "@".$dt->instansi;
						}else{
							$strinstansi = "";
						}
						
						$namapeserta = $namapeserta . $dt->nama .$strinstansi."& ";							
					endforeach;
				//echo "</p>";
			}
		}
		?>
		<input type="hidden" value="<?php echo $namapeserta; ?>" class="tmppeserta" />	
		<input type='text' name='tluar' placeholder='Nama Peserta..' class='tagPeserta form-control'/>
</div>
						
						