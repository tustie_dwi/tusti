<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Kerjasama</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Kerjasama</a></li>
		  <li class="active"><a href="#">Kegiatan</a></li>
		</ol>
		<div class="breadcrumb-more-action">
			<a href="#" class="btn btn-primary" onclick="tambah()">
    		<i class="fa fa-pencil icon-white"></i> Tambah Kegiatan</a> 
        </div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="panel panel-default" >
			  	<div class="panel-heading">
			  		<i class="fa fa-search"></i> Pencarian Kegiatan
			  		
			  		<i class="fa fa-refresh fa-spin pull-right" id="loading" style="display: none"></i>
			  	</div>
				<div class="panel-body">	
					<form role="form" method="post" aaction="<?php echo $this->location("module/master/kerjasama/instansi") ?>">
					  	<div class="form-group">
					  		<select class="form-control" name="fakultas">
					    		<option value="">Semua Fakultas</option>
					    		<?php
									if($fakultas) :
										foreach($fakultas as $key) :
											if($sel_fakultas == $key->fakultas_id) $sel = "selected";
											else $sel = '';
											
											echo "<option ".$sel." value='".$key->fakultas_id."'>".$key->keterangan."</option>";
										endforeach;
									endif;						    		
					    		?>
					    	</select>
						</div>
						<div class="form-group">
							<select class="form-control" name="kategori">
					    		<option value="">Semua Kategori</option>
					    		<?php
									if($kategori) :
										foreach($kategori as $key) :
											if($sel_kategori == $key->kategori_id) $sel = "selected";
											else $sel = '';
											
											echo "<option ".$sel." value='".$key->kategori_id."'>".$key->keterangan."</option>";
										endforeach;
									endif;						    		
					    		?>
					    	</select>
						</div>
						<div class="form-group">
							<select class="form-control" name="instansi">
					    		<option value="">Pilih Instansi</option>
					    	</select>
						</div>
						<button onclick="lihat_detail()" type="button" class="btn btn-primary btn-sm btn-block">Lihat Detail <i class="fa fa-arrow-right"></i></button>
					</form>
				</div>
		</div>
	</div>
	<div class="col-md-9 right-pane" id="form-data" style="display: nonex">
		
	</div>
	
	<?php $this->view('kerjasama/kegiatan-manage.php', $data); ?>
	
</div>
<?php $this->foot(); ?>