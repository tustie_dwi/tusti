<div class='block-box'>
	<?php if($kegiatan) : ?>
		<table class='table table-hover' id='example' style="font-size: 100%">
			<thead>
				<tr>
					<th width="9%">No.</th>
					<th>Kegiatan</th>
					<th>Ruang</th>
					<th>Lokasi</th>
					<th>Keterangan</th>
					<th width="10%">Menu</th>
				</tr>
			</thead>
			<tbody>
				<?php $no=1; foreach($kegiatan as $key) : ?>
					<tr>
						<td><?php echo $no; $no++ ?></td>
						<td>
							<?php 
								echo $key->judul;
								echo '<br><small><i class="fa fa-briefcase"></i> ' . $key->nama_instansi . '</small>'; 
								echo '<br><small><i style="color: #2980b9" class="fa fa-calendar"></i> '.$key->tgl_mulai . ' s/d ' . $key->tgl_selesai.'</small>';
							?>
						</td>
						<td><?php echo "<i class='fa fa-square' style='color: #16a085'></i> " . str_replace(",", "<br><i style='color: #16a085' class='fa fa-square'></i> ", $key->ruang_ket)  ?></td>
						<td><?php echo $key->lokasi ?></td>
						<td><?php echo $key->keterangan ?></td>
						<td>
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
									<li>
										<a target="dokumen" class='btn-edit-post' href="<?php echo $this->location('module/master/kerjasama/upload_file/'.$key->id) ?>">
											<i class='fa fa-archive'></i> Dokumen
										</a>
										<a class='btn-edit-kegiatan' href="#"
											data-id="<?php echo $key->id ?>"
											data-judul="<?php echo $key->judul ?>"
											data-keterangan="<?php echo $key->keterangan ?>"
											data-lokasi="<?php echo $key->lokasi ?>"
											data-tgl_mulai="<?php echo $key->tgl_mulai ?>"
											data-tgl_selesai="<?php echo $key->tgl_selesai ?>"
											data-instansi="<?php echo $key->instansi_id ?>"
											data-ruang="<?php echo $key->ruang ?>"
										>
											<i class='fa fa-edit'></i> Edit
										</a>
										<a class='btn-edit-post' onclick="return confirm('Apakah Anda ingin menghapus data ini?')" href="<?php echo $this->location('module/master/kerjasama/del_kegiatan/'.$key->id) ?>">
											<i class='fa fa-trash-o'></i> Hapus
										</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php else : ?>
			<div class="well">Kegiatan tidak ditemukan</div>
		<?php endif; ?>
</div>
<script src="<?php echo $this->location() ?>assets/js/datatables/DT_bootstrap.js"></script>
