<?php $this->head(); ?>

<div class="row">
	<div class="col-md-12">	
	<h2 class="title-page">Kerjasama</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Kerjasama</a></li>
		  <li class="active"><a href="#">Dokumen</a></li>
		</ol>
		
		<div class="breadcrumb-more-action">
			<a href="#" class="btn btn-primary" onclick="tambah()">
    		<i onclick="tambah()" class="fa fa-pencil icon-white"></i> Tambah Dokumen</a> 
        </div>
	</div>
</div>

<div class="row">
	<div class="col-md-4">
		<div class='block-box'>
			<h3 class="text-center" style="margin-top: 5px">Fakultas</h3>
			
			<div class="list-group" id="fakultas-wrap">
		    </div>
		</div>
	</div>
	
	<div class="col-md-8" style="display: nonex"  id="data-wrap">
		<div class='block-box'>
			<h3 class="text-center" style="margin-top: 5px">Daftar Dokumen</h3>
		</div>
	</div>
	
	<div class="col-md-8" style="display: none" id="add-wrap">
		<div class="panel panel-default">
			  	<div class="panel-heading">
			  		<i class="fa fa-plus"></i> Tambah Dokumen
			  		
			  		<i class="fa fa-refresh loading pull-right fa-spin"style="display:none"></i>
			  	</div>
				<div class="panel-body">	
					<form enctype="multipart/form-data" role="form" method="post" action="<?php echo $this->location('module/master/kerjasama/upload_dokumen/1') ?>">
						<div class="form-group">
						    <label >Fakultas</label>
						    <select type="text" class="form-control" id="select-fakultas" onchange="get_instansi_by_fakulas(this.value)">
						    	<option>Pilih Fakultas</option>
						    </select>
						</div>
						<div class="form-group">
						    <label >Instansi</label>
						    <select name="instansi_id" type="text" class="form-control" id="select-instansi" onchange="get_kegiatan_by_instansi(this.value)">
						    	<option>Pilih Instansi</option>
						    </select>
						</div>
						<div class="form-group">
						    <label >Kegiatan</label>
						    <select name="kegiatan_id" type="text" class="form-control" id="select-kegiatan" >
						    	<option>Pilih Kegiatan</option>
						    </select>
						</div>
						<div class="form-group">
						    <label >Judul</label>
						    <input type="text" name="judul" required="" class="form-control" >
						</div>
						<div class="form-group">
						    <label >Keterangan</label>
						    <textarea name="keterangan" class="form-control" ></textarea>
						</div>
						<div class="form-group">
						    <input type="file" multiple="" name="dokumen[]" required="" class="form-control" >
						</div>
						
						<div class="form-group">
						    <button class="btn btn-primary">Simpan</button>
						    <button onclick="batal()" class="btn btn-default">Batal</button>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>
<?php $this->foot(); ?>

<script type="text/javascript" charset="utf-8">
	<?php if($fakultas): ?>
		var fakultas = [<?php foreach ($fakultas as $key) echo "'".$key->fakultas_id . "|". $key->keterangan."',"; ?>];
	<?php else: ?>
		var fakultas = [];
	<?php endif; ?>	
</script>








