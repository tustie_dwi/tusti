<div id="parameter" class="block-box">
	<form action="<?php echo $this->location('module/master/content/page'); ?>" role="form" id="param" method="POST">
		
		<div class="form-group">	
			<label class="control-label">Fakultas</label>		
				<select id="select_fakultas" class="form-control e9" name="fakultas" >
				<option value="0">Pilih Fakultas</option>
				<?php if(count($get_fakultas)> 0) {
					foreach($get_fakultas as $dt) :
						echo "<option value='".$dt->hid_id."' ";
						if(isset($fakultasid)){
							if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
								echo "selected";
							}
						}
						echo " >".ucWords($dt->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
		</div>

		<div class="form-group">	
			<label class="control-label">Cabang</label>					
				<select id="select_cabang" class="form-control e9" name="cabang" >
				<option value="0">Pilih Cabang</option>
				<?php if(count($cabang)> 0) {
					foreach($cabang as $c) :
						echo "<option value='".$c->cabang_id."' ";
						if(isset($cabangid)){
							if($cabangid==$c->cabang_id){
								echo "selected";
							}
						}
						echo " >".ucWords($c->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
		</div>
		
		<input type="hidden" id="unit-select" value="<?php if(isset($unitid))echo $unitid ?>" />
		<div class="form-group">	
			<label class="control-label">Unit</label>					
				<select id="select_unit" class="form-control e9" name="unit" >
				<option value="0">Pilih Unit</option>
				</select>
		</div>
		
		<input type="hidden" id="lang-select" value="<?php if(isset($lang))echo $lang ?>" />
		<div class="form-group">	
			<label class="control-label">Bahasa</label>					
				<select id="select_lang" class="form-control e9" name="lang" >
					<option value="0">Pilih Bahasa</option>
					<option value="in">Indonesia</option>
					<option value="en">English</option>
				</select>
		</div>
		
		<div class="form-group">	
			<label class="control-label">Kategori</label>					
				<select id="select_category" class="form-control e9" name="category" >
				<option value="0">Pilih Kategori</option>
				<?php if(count($content_category)> 0) {
					foreach($content_category as $cc) :
						echo '<optgroup label="'.$cc->category.'">';
						$this->read_category_child($cc->category,$contentcategory);
						echo '</optgroup>';
					endforeach;
				} ?>
				</select>
		</div>
		
		<input type="hidden" id="index-category" name="index-category" value="<?php if(isset($index_category))echo $index_category; ?>" />
		
	</form>
</div>