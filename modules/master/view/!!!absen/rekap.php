<?php if(isset($absen)): ?>
	
<?php 
$num = 1;
$kryn='';
$karyawannum = Array();
foreach ($absen as $dt) {
	// if($dt->karyawan_id){
		if($kryn == $dt->karyawan_id){
			$karyawannum[$dt->karyawan_id] = $num++;
		}else{
			$num = 1;
			$karyawannum[$dt->karyawan_id] = 0;
		}
		$kryn = $dt->karyawan_id;
	// }
}
// print_r($karyawannum);
?>

	
<?php //echo $prodi; ?>
<table class="table table-bordered table-condensed">
<thead>
	<tr>
		<th><center>No</center></th>
		<th><center>Nama</center></th>
		<th><center>Mata Kuliah</center></th>
		<th><center>SKS</center></th>
		<th><center>Jumlah Absensi</center></th>
	</tr>
</thead>
<tbody>
	
	<?php $num = 1; $kryn='';?>
	<?php foreach ($absen as $dt) { ?>
	<?php if($dt->karyawan_id){ ?>
	
	<tr>
		<?php if($kryn != $dt->karyawan_id){ ?>
			<td width="3%" align="center" valign="center" <?php if($kryn != $dt->karyawan_id)echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?> >
			<?php 
			if($kryn != $dt->karyawan_id){
				echo $no++;
			}
			?>
			</td>
			<td width="20%" <?php if($kryn != $dt->karyawan_id)echo 'rowspan="'.($karyawannum[$dt->karyawan_id]+1).'"' ?>>
				<?php 
				if($kryn != $dt->karyawan_id){
					echo $dt->nama;
					if($dt->nik=='') echo "<br><small><strong>NIK. -</strong></small>";
					else echo "<br><small><strong>NIK. ". $dt->nik ."</strong></small>";
				}
				?>
				<?php $kryn = $dt->karyawan_id; ?>
			</td>
		<?php } ?>
		<td width="50%">
			<div class="panel-group" id="accordion">
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" href="#<?php $link = str_replace(' ', '', $dt->namamk." - ".$dt->kelas." - ".$dt->prodi_mk); echo str_replace(str_split('&/'), '', $link); ?>" style="font-size: 13px;">
						<?php 
							echo "<strong>[".$dt->kode_mk."]</strong> ";
							echo $dt->namamk." - ".$dt->kelas;
							echo " [".$dt->prodi_mk."]"
						?>
					 </a>
      			  </h4>
   			 	</div>
   			 	<?php if($view_!="cetak"){ ?>
   			 	<div id="<?php $link = str_replace(' ', '', $dt->namamk." - ".$dt->kelas." - ".$dt->prodi_mk); echo str_replace(str_split('.&/'), '', $link); ?>" class="panel-collapse collapse">
      				<div class="panel-body">
      					<?php //echo $dt->jadwal_id;
      						$mabsen = new model_absen();
							$absen = $mabsen->rekap_absen($dt->tahun_akademik, $dt->prodi_id, $dt->mkditawarkan_id, $dt->kelas, "", $datemulai, $dateselesai, "byjadwal", $dt->jadwal_id);
      					?>
      					<?php if($dt->tahun_akademik == $thn_aktif){ ?>
      					<a href="javscript:" class="btn btn-primary tambah_absen" data-toggle="modal" data-target="#myModal" onclick="add_absen('<?php echo $dt->karyawan_id ?>','<?php echo $dt->pengampu_id ?>','<?php echo $dt->jadwal_id ?>')"><i class="fa fa-plus"></i> Tambah Absen</a>
      					<?php } ?>
      					<table class="table noborder example" style="font-size: 12px;">
      						<thead>
      							<tr>
	      							<th>Hari / Tanggal</th>
	      							<th>Ruang</th>
	      							<th>Materi</th>
	      							<th>Kehadiran</th>
	      							<th class="action_btn">&nbsp;</th>
      							</tr>
      						</thead>
      						<tbody>
      							<?php foreach($absen as $a){
      									$tgl = explode("-",$a->tgl_pertemuan);
										$tgl = $tgl[2]."-".$tgl[1]."-".$tgl[0];
	      								if($a->is_hadir==1){
	      									$hadir = "Hadir";
	      								}else $hadir = "Tidak Hadir";
	      						?>
	      							<tr>
	      								<td>
	      									<?php
	      										$day = date("l", strtotime($tgl));
												switch($day){
													case "Monday" : $hari = "Senin";
																	break;
													case "Tuesday" : $hari = "Selasa";
																	break;
													case "Wednesday" : $hari = "Rabu";
																	break;
													case "Thursday" : $hari = "Kamis";
																	break;
													case "Friday" : $hari = "Jumat";
																	break;
													case "Saturday" : $hari = "Sabtu";
																	break;
													case "Sunday" : $hari = "Minggu";
																	break;
												}; 
	      										echo ucfirst($hari) .", ".$tgl."<br>"; 
												echo "[".substr($a->jam_masuk,0,5)." - ";
												echo substr($a->jam_selesai,0,5)."]"; 
											?>
										</td>
	      								<td><?php echo $a->ruang_id ?></td>
	      								<td><?php echo ucfirst($a->materi); ?></td>
	      								<td><?php echo $hadir; ?></td>
	      								<td class="action_btn" width="10%">
	      									<?php if($dt->tahun_akademik == $thn_aktif){ ?>
	      									<a href="#" title="Edit" data-toggle="modal" data-target="#myModal" onclick="edit_absen('<?php echo $a->hidId; ?>', '<?php echo $a->karyawan_id; ?>')"><i class="fa fa-edit" style="color: blue;"></i></a>&nbsp;&nbsp;&nbsp;
	      									<a href="#" title="Delete" onclick="delete_absen('<?php echo $a->hidId; ?>')"><i class="fa fa-trash-o" style="color: red;"></i></a>
	      									<?php } ?>
	      								</td>
	      							</tr>
      							<?php } ?>
      						</tbody>
      					</table>
      				</div>
  				</div>
  				<?php } ?>
			</div>
		   </div>
		</td>
		<td width="3%"><center>
			<?php 
				if($dt->sks<=3){
					echo $dt->sks;
				}elseif($dt->sks>3){
					$sks = $dt->sks-1;
					if($sks>3){
						echo "2";
					}else{
						echo $sks;
					}
				}
				
			?></center>
		</td>
		<td width="10%"><center>
			<?php echo $dt->jml; ?></center>
		</td>
	</tr>
	<?php } ?>
	<?php } ?>
	
</tbody>
</table>
<?php else: ?>
	<div class="well" align="center">
		No Data Show
	</div>
<?php endif; ?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        	<form id="form-absen-dosen">
					<div class="form-group">
				        <label class="control-label">Jadwal Matakuliah</label>
				        <div class="controls">
				        	<select name="jadwal-mk" class="e9 form-control">
				        		<option value="0">Silahkan Pilih</option>
				        	</select>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Tanggal</label>
				        <div class="controls">
				        	<input type="text" name="tgl_pertemuan" class="date form-control" placeholder="Masukkan Tanggal Pertemuan"/>
				        </div>
				    </div>
			    
				    <div class="form-group">
				        <label class="control-label">Jam Mulai</label>
				        <div class="controls">
				        	<input type="text" class="time form-control" name="jam_mulai" placeholder="Masukkan Jam Mulai"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Jam Selesai</label>
				        <div class="controls">
				        	<input type="text" class="time form-control" name="jam_selesai" placeholder="Masukkan Jam Selesai"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Materi</label>
				        <div class="controls">
				        	<input type="text" class="form-control" name="materi" placeholder="Masukkan Nama Materi"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Total Pertemuan</label>
				        <div class="controls">
				        	<input type="text" class="form-control" name="total" placeholder="Masukkan Total Pertemuan" value="14"/>
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Sesi</label>
				        <div class="controls">
				        	<select name="sesi" class="e9 form-control">
				        		<?php for($i=1;$i<21;$i++){ ?>
				        			<option value="<?php echo $i ?>">Sesi <?php echo $i ?></option>
				        		<?php } ?>
				        	</select>
				        </div>
				    </div>	 
				    <div class="form-group">
				        <label class="control-label"></label>
				        <div class="controls">
				        	<input type="hidden" name="hidId_absen"/>
				        	<input type="hidden" name="hidId_absendosen"/>
				        	<input type="hidden" name="dosen"/>
				        	<button type="submit" class="btn btn-primary">Data Valid & Save</button>
				        	<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				        </div>
				    </div>  
		    </form>
      </div>
    </div>
  </div>
</div>