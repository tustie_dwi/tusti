<?php
class master_keuangan extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	//rekap keuangan
	function rekap_pendapatan(){
		$mrekap = new model_keuangan();

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
				
		$data['tahun'] = date('Y');
		$data['bulan'] = date('m');
		
		$mrekap = new model_keuangan();
		//$tahun = $_POST['tahun'];
		$data['posts'] = $mrekap->get_rekap_pendapatan();
			
		$this->view('keuangan/rekap_pendapatan.php', $data);
	}
	
	function get_rekap_pendapatan(){
		if(! isset($_POST['tahun'])) $this->redirect('module/master/keuangan/rekap_pendapatan');
		$mrekap = new model_keuangan();
		$tahun = $_POST['tahun'];
		$rekap_per_tahun = $mrekap->get_rekap_pendapatan($tahun);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		if($rekap_per_tahun) :
			?>
			<table class="table table-hover example">
					<thead>
						<tr valign="top">
							<!--<th rowspan="2"><!--<input type="checkbox" id="checkAll"></th>-->
							<th>Dosen</th>						
							<th>Periode</th>
							<th>Total</th>
						</tr>					
						
					</thead>
					<tbody>
			<?php
			foreach($rekap_per_tahun as $key){
				?>
					<tr>
						<td><?php echo $key->nama ?></td>
						<td><?php echo $key->tgl_mulai ?> - <?php echo $key->tgl_selesai ?></td>
						<td align="right"><?php echo number_format($key->total) ?></td>
					</tr>
				<?php
			
			}
			?>
			</tbody>
			</table>
			<?php
		endif;
	}
	
	//rekap keuangan
	function rekap_keuangan(){
		$mrekap = new model_keuangan();

		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->coms->add_script('highchart/highcharts.js');
		$this->coms->add_script('highchart/highcharts-3d.js');
		$this->coms->add_script('highchart/modules/exporting.js');
		
		$data['tahun'] = date('Y');
		$data['bulan'] = date('m');
			
		$this->view('keuangan/rekap_keuangan.php', $data);
	}
	
	function get_rekap_tahun(){
		if(! isset($_POST['tahun'])) $this->redirect('module/master/keuangan/rekap_keuangan');
		$mrekap = new model_keuangan();
		$tahun = $_POST['tahun'];
		$rekap_per_tahun = $mrekap->get_rekap_tahun($tahun);
		
		if($rekap_per_tahun) :
			$uang = array();
			foreach($rekap_per_tahun as $key){
				//$temp['name'] = $this->get_bulan($key->bulan);
				$temp['name'] = $key->tahun;
				$temp['data'] = array(
					0 => intval($key->total_non_pph), 
					1 => intval($key->total_pph), 
					2 => intval($key->total)
				);
				array_push($uang, $temp);
			}
			
			echo json_encode($uang);			
		
		else : echo "-";			
		endif;
	}
	
	function get_ajax_rekap_tahun(){
		if(! isset($_POST['bulan'])) $this->redirect('module/master/keuangan/rekap_keuangan');
		$mrekap = new model_keuangan();
		$tahun = $_POST['tahun'];
		$bulan = $_POST['bulan'];
		
		$rekap_per_periode = $mrekap->get_rekap_periode($bulan, $tahun);
		
		$main = array();
		$data = array();
		
		$temp['tdk_kena_pajak']['name'] = 'Tidak kena pajak';
		$temp['pph']['name'] = 'PPH';
		$temp['kena_pajak']['name'] = 'Jumlah Kena Pajak';
		
		$temp['tdk_kena_pajak']['data'] = array();
		$temp['pph']['data'] = array();
		$temp['kena_pajak']['data'] = array();
		
		$kategori = array();
		
		// $p_kategori = $p_tidak_kena_pajak = $p_pph = $p_kena_pajak = '';
		if($rekap_per_periode){
			foreach ($rekap_per_periode as $key) {
				array_push($temp['tdk_kena_pajak']['data'], intval($key->total_non_pph));
				array_push($temp['pph']['data'], intval($key->total_pph));
				array_push($temp['kena_pajak']['data'], intval($key->total));
				array_push($kategori, $key->tgl_mulai . ' s/d ' . $key->tgl_selesai);
			}
			
			array_push($data, $temp['tdk_kena_pajak']);
			array_push($data, $temp['pph']);
			array_push($data, $temp['kena_pajak']);
			
			array_push($main, $data);
			array_push($main, $kategori);
			echo json_encode($main);		
		}
		else{
			echo "-";	
		}
		
	}
	
	function get_bulan($init){
		switch ($init) {
			case '01' : return 'Jan';break;
			case '02' : return 'Feb';break;
			case '03' : return 'Mar';break;
			case '04' : return 'Apr';break;
			case '05' : return 'Mei';break;
			case '06' : return 'Jun';break;
			case '07' : return 'Jul';break;
			case '08' : return 'Agt';break;
			case '09' : return 'Sep';break;
			case '10' : return 'Okt';break;
			case '11' : return 'Nov';break;
			case '12' : return 'Des';break;
			default: return '"Jan"'; break;
		}
	}
	// 
	function titip($tahun=NULL){
		$mkeuangan = new model_keuangan();
		$mconf = new model_general();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		
		$this->add_script('js/keuangan/titip.js');	
		$data['tahun'] = $mkeuangan->get_list_tahunakademik();
		
		if($tahun == NULL || $tahun == '-') $tahun = $mkeuangan->get_tahunakademik_aktif();
		$data['tahun_detail'] = $mkeuangan->get_detail_tahun($tahun);
		
		$data['tahun_tmp'] = $tahun;
		
		$data['titip'] = $mkeuangan->get_data_dosen_titip($tahun);
		$data['dosen'] = $mkeuangan->get_dosen();
		$data['mk'] = $mconf->get_mkditawarkan("",$tahun);
		$data['prodi'] = $mconf->get_unit('PTIIK','','prodi');
		
		$this->view('keuangan/titip.php', $data);
	}
	
	function save_titip(){
		$mkeuangan = new model_keuangan();
		
		if($_POST['dosen_sk'] == '-' || $_POST['dosen_titip'] == '-') $this->redirect('module/master/keuangan/titip');
		
		if($_POST['titip'] == '' ) :
			$data = array(
				'titip_id' => $mkeuangan->get_titip_id($_POST['dosen_sk'], $_POST['dosen_titip'], $_POST['cmbtahun'], $_POST['cmbprodi'], $_POST['cmbmk'],strToUpper($_POST['kelas']) ),
				'tahun_akademik' => $_POST['cmbtahun'],
				'karyawan_id' => $_POST['dosen_sk'],
				'mkditawarkan_id' => $_POST['cmbmk'],
				'kelas' => strToUpper($_POST['kelas']),
				'prodi'=>$_POST['cmbprodi'],
				'titip_ke' => $_POST['dosen_titip']	
			);
			$mkeuangan->save_titip($data);
		else :
			$data = array(
				'tahun_akademik' => $_POST['cmbtahun'],
				'karyawan_id' => $_POST['dosen_sk'],
				'prodi'=>$_POST['cmbprodi'],
				'mkditawarkan_id' => $_POST['cmbmk'],
				'kelas' => strToUpper($_POST['kelas']),
				'titip_ke' => $_POST['dosen_titip']	
			);
			
			$where = array(
				'titip_id' => $_POST['titip']
			);
			$mkeuangan->update_titip($data, $where);
		endif;
		
		$this->redirect('module/master/keuangan/titip/');
	}
	
	function del_titip($id=NULL){
		$mkeuangan = new model_keuangan();
		$mkeuangan->del_titip($id);
		$this->redirect('module/master/keuangan/titip');
	}
	
	//konfigurasi
	function conf(){
		$mkeuangan = new model_keuangan();
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/keuangan/conf.js');	
		$data['golongan'] = $mkeuangan->get_golongan();
		$data['conf'] = $mkeuangan->get_conf();
		$this->view('keuangan/conf.php', $data);
	}
	
	function save_conf(){
		$mkeuangan = new model_keuangan();
		
		$golongan	= $_POST['golongan'];
		$jenjang	= $_POST['jenjang_pendidikan'];
		$tarif		= $_POST['tarif'];
		
		if(isset($_POST['is_pns'])) $pns='1';
		else $pns='0';
		
		$potongan	= $_POST['potongan'];
		$lastupdate	= date("Y-m-d H:i:s");
		
		$datanya	= array(
			'golongan'=>$golongan, 
			'jenjang_pendidikan'=>$jenjang, 
			'tarif'=>$tarif, 
			'is_pns'=>$pns, 
			'strata'=>$_POST['strata'],
			'potongan'=>$potongan, 
			'kategori'=>'mengajar', 
			'user'=>$this->coms->authenticatedUser->id, 
			'last_update'=>$lastupdate
		);
		
		if(isset($_POST['conf_id'])){
			$where = array(
				'MID(MD5(conf_id),8,6)' => $_POST['conf_id']
			);
			$mkeuangan->update_conf($datanya, $where);
		}
		else {
			$datanya['conf_id'] = $mkeuangan->get_conf_id();
			$mkeuangan->replace_conf($datanya);
		}
		
		// foreach($datanya as $key => $value) echo $key . ' => ' . $value . ' <br>';
		// $mkeuangan->replace_conf($datanya);
		$this->redirect('module/master/keuangan/conf');
	}
	
	//honorarium
	function  index($prodi=NULL,$mulai=NULL, $selesai=NULL){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		$mconf = new model_honor();		
		
		$data['prodi'] 	  = $mconf->get_prodi();
		
		if(isset($_POST['tmulai'])) $tmulai = $_POST['tmulai'];
		else $tmulai = $mulai;
		
		if(isset($_POST['tselesai'])) $tselesai = $_POST['tselesai'];
		else $tselesai = $selesai;
		
		if(isset($_POST['cmbprodi'])) $prodi = $_POST['cmbprodi'];
		else $prodi = $prodi;
		
		$data['mulai'] = $tmulai;
		$data['selesai'] = $tselesai;
		$data['prodiid'] = $prodi;
		
		if($tmulai && $tselesai) $data['posts'] = $mconf->get_rekap_absen($prodi, $tmulai, $tselesai);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->add_script('js/keuangan/jsall.js');	
		$this->add_style('css/calendar/calendar.css');		
			
		$this->view( 'keuangan/hr/index.php', $data );
	}

	function proses($str=NULL){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		if(isset($_POST['chkdosen']) && ($str=='bayar')):
			$mconf = new model_honor();	
			
			$dosen 	= $_POST['chkdosen'];
			$mulai 	= $_POST['mulai'];
			$selesai= $_POST['selesai'];
			$prodi	= $_POST['prodi'];
			
			$isproses = $mconf->get_proses_bayar($mulai);
			if($isproses) $strproses = "selisih";
			else $strproses = "main";
			
		//	$mconf = new model_conf();
		
			// foreach($_POST['chkdosen'] as $key){
				// echo $key . '<br>';
			// }
			// echo $_POST['mulaix'];
			// echo "<hr>";
		
			if($dosen):
				$total_honor=0;
				$total_honor_pph=0;
				$total_honor_all=0;
				$kode = $mconf->get_reg_bayar();
				
				$lastupdate	= date("Y-m-d H:i:s");
				$user	 	= $this->coms->authenticatedUser->username;
				
								
				for($i=0;$i< count($dosen) ;$i++){
					$prodiid = count($_POST['bayar_detail'.$dosen[$i]]);				
					
					for($j=0;$j<$prodiid;$j++){
						$hrid = $kode.$i.$j;
						
						$data = json_decode($_POST['bayar_detail'.$dosen[$i]][$j]);
						
						if(! empty($data)){
							?>
								<!--<pre><?php print_r($data) ?></pre>-->
							<?php
							// echo "<hr>";
						}
									
						$mkid	= $data->hidmk;
						$kelas	= $data->hidkelas;
						$gol	= $data->hidgol;
						$namamk	= $data->hidnama;
						$kodemk	= $data->hidkode;
						$sks	= $data->hidsks;
						$hadir	= $data->hidhadir;
						$satuan	= $data->hidsatuan;
						$total	= $data->hidtotal;
						$pph	= $data->hidpph;
						$jumlah	= $data->hidjml;
						$wajib	= $data->hidwajib;
						$potongan	= $data->hidpotongan;
						$bayar	= $data->hidbayar;
						$mgg	= $data->hidmgg;
						$prak	= $data->hidprak;
						$hidprod	= $data->hidprodi;
						
						$datanya = array(
								'hr_id'=>$hrid, 
								'karyawan_id'=>$dosen[$i], 
								'bayar_id'=>$kode, 
								'mkditawarkan_id'=>$mkid, 
								'nama_mk'=>$namamk, 
								'kode_mk'=>$kodemk, 
								'prodi'=>$hidprod, 
								'kelas'=>$kelas,
								'sks'=>$sks, 
								'hadir'=>$hadir, 
								'satuan'=>$satuan, 
								'pph'=>$pph, 
								'kewajiban'=>$wajib, 
								'golongan'=>$gol, 
								'total'=>$total, 
								'total_bayar'=>$jumlah, 
								'jml_mgg'=>$mgg, 
								'praktikum'=>$prak, 
								'potongan'=>$potongan, 
								'is_proses'=>$strproses, 
								'titip_ke'=>$dosen[$i]
						);
						$mconf->replace_data_bayar_detail($datanya);
						$mconf->update_titip($dosen[$i], $mkid, $hidprod, $kelas);
						
						$total_honor = $total_honor + ($total-$wajib);
						$total_honor_pph = $total_honor_pph + $pph;
						$total_honor_all = $total_honor - $total_honor_pph;
						
					}
					
					$absen= $mconf->get_absen_id($prodi, $mulai, $selesai, $dosen[$i]);
					if($absen):
						foreach($absen as $key):
							$datanya  = array('absendosen_id'=>$key->absendosen_id, 'bayar_id'=>$kode);
							$mconf->replace_data_bayar_absen($datanya);
						endforeach;
					endif;
				}
				
				/*$datanyax = array('bayar_id'=>$kode, 'periode'=>date('Ym'), 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai,'total_pph'=>$_POST['hidtotalpph'], 
								'total_non_pph'=>$_POST['hidtotalnon'], 'total'=>$_POST['hidtotalall'], 
								'tgl_bayar'=>date("Y-m-d"), 'kategori_bayar'=>'hr mengajar', 'user_id'=>$user, 'last_update'=>$lastupdate);*/
								
				$datanya = array('bayar_id'=>$kode, 'periode'=>date('Ym'), 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai,'total_pph'=>$total_honor_pph, 
								'total_non_pph'=>$total_honor, 'total'=>$total_honor_all, 
								'tgl_bayar'=>date("Y-m-d"), 'kategori_bayar'=>'hr mengajar', 'user_id'=>$user, 'last_update'=>$lastupdate,'is_proses'=>$strproses);
				$mconf->replace_data_bayar($datanya);
				
				//$mconf->update_titip($datanya);
			endif;
			unset ($_POST['b_proses']);
			
			$this->rekap($prodi, $mulai, $selesai, $kode);
			exit();
		endif;		
	}

	function rekap($prodi=NULL,$mulai=NULL, $selesai=NULL, $kode=NULL, $cetak=NULL, $custom=NULL){
		$mconf = new model_honor();		
		
		$data['prodi'] 	  = $mconf->get_prodi();
		$data['mconf']    = $mconf;
		$data['karyawan_list'] = $mconf->get_karyawan();
		if(isset($_POST['tmulai'])) $tmulai = $_POST['tmulai'];
		else $tmulai = $mulai;
		
		if(isset($_POST['tselesai'])) $tselesai = $_POST['tselesai'];
		else $tselesai = $selesai;
		
		if(isset($_POST['cmbprodi'])) $prodi = $_POST['cmbprodi'];
		else $prodi = $prodi;
		
		
		if($prodi==1) $prodi="";
		if($kode==1) $kode="";
		
		$data['mulai'] = $tmulai;
		$data['selesai'] = $tselesai;
		$data['prodiid'] = $prodi;
		$data['kode']	= $kode;
		
		if($tmulai && $tselesai) {
			$data['posts'] = $mconf->get_rekap_absen_bayar($prodi, $tmulai, $tselesai, $kode);
		}
		
		if(isset($_POST['cetak'])||($cetak)):
			$data['custom'] = $custom;
			switch($cetak){
				case 1 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr.php', $data);
				break;
				case 2 :
					$data['selisih'] = "";
					$data['posts_data'] = $mconf->get_rekap_absen_bayar_cetak_amplop($prodi, $tmulai, $tselesai, $kode);
					$this->view('keuangan/hr/report-hr-amplop.php', $data);
				break;
				case 3 :
					$data['selisih'] = "main";
					$this->view('keuangan/hr/report-hr-spj.php', $data);
				break;
				case 4 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr-excel.php', $data);
				break;
				case 5 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr-mku.php', $data);
				break;
				case 6 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr-titip.php', $data);
				break;
				case 7 :
					$data['selisih'] = "selisih";
					$this->view('keuangan/hr/report-hr-spj.php', $data);
				break;
				case 8 :
					$data['selisih'] = "selisih";
					$this->view('keuangan/hr/report-hr.php', $data);
				break;
				case 9 :
					$data['selisih'] = "selisih";
					$this->view('keuangan/hr/report-hr-mku.php', $data);
				break;
				case 10 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr-bank.php', $data);
				break;
				case 11 :
					$data['selisih'] = "selisih";
					$this->view('keuangan/hr/report-hr-bank.php', $data);
				break;
			}
			
		else:
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			$this->add_script('js/keuangan/jsall.js');	
			$this->add_style('css/calendar/calendar.css');	
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			
			$this->view( 'keuangan/hr/rekap.php', $data );
		endif;
	}
	
	//agenda kegiatan
	function kegiatan (){
		$mconf = new model_honor();
		$data['kategori_bayar'] = $mconf->get_keu_kategori_bayar();
		$data['kategori']= $mconf->get_list_kategori();
		$data['kegiatan'] = $mconf->get_keu_bayar();
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('js/base64.js');		
		$this->add_style('css/calendar/calendar.css');		
		
		$this->add_script('js/keuangan/kegiatan.js');
		$this->view( 'keuangan/kegiatan/index.php', $data );
	}
	function get_agenda_peserta(){
		if(isset($_POST)){
			$mmaster = new model_honor();
			$agendaid = $_POST['agendaid'];
			echo json_encode($mmaster->get_agenda_peserta($agendaid));
		}
	}
	function agenda_search(){
		$mmaster = new model_honor();
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$data = $mmaster->get_agenda_term($str);
		$json_response = json_encode($data);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
	}
	function karyawan_search(){
		$mmaster = new model_honor();
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$data = $mmaster->get_karyawan_term($str);
		$json_response = json_encode($data);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
	}
	//keu kegiatan
	function submit_keu_bayar(){
		$mmaster = new model_honor();
		$bayar_id = (isset($_POST['bayar_id']))?$_POST['bayar_id']:$mmaster->get_reg_bayar();
		$data = array (
			'bayar_id'=>$bayar_id,
			'periode'=>date('Ym'),
			"tgl_mulai"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_mulai']),'Y-m-d H:i:s'),
			"tgl_selesai"=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_selesai']),'Y-m-d H:i:s'),
			"kategori_bayar"=>$_POST['kategori_bayar'],
			"keterangan"=>$_POST['keterangan'],
			"kategori"=>'main',
			"is_proses"=>'main',
			'tgl_bayar'=>date_format(date_create_from_format('d-m-Y', $_POST['tgl_bayar']),'Y-m-d H:i:s'),
			'user_id'=>$this->coms->authenticatedUser->username, 
			'last_update'=>date('Y-m-d H:i:s')
		);
		$mmaster->replace_data_bayar($data);
		echo json_encode($mmaster->get_keu_bayar(substr(md5($bayar_id), 5, 6)));
	}
	function delete_keu_bayar(){
		$mmaster = new model_honor();
		$mmaster->hapus_data_bayar($_POST['bayarid']);
	}
	
	function get_keu_peserta(){
		$mmaster = new model_honor();
		$hasil = $mmaster->get_keu_peserta($_POST['bayar_id']);
		if($hasil)
			echo json_encode($hasil);
	}
	function submit_keu_peserta (){
		$mmaster = new model_honor();
		$hr_id = (isset($_POST['hr_id']) && $_POST['hr_id']!= "")?$_POST['hr_id']:$mmaster->get_reg_bayar_detail();
		$data = array(
			'hr_id'=>$hr_id,
			'bayar_id'=>$_POST['bayar_id'],
			'karyawan_id'=>($_POST['karyawan_id']=='')?'-':$_POST['karyawan_id'],
			'nama'=>$_POST['nama'],
			'keterangan'=>$_POST['keterangan'],
			'qty'=>$_POST['qty'],
			'satuan'=>$_POST['satuan'],
			'total'=>$_POST['total'],
			'praktikum'=>'0',
		);
		$mmaster->replace_data_bayar_detail($data);
	}
	function delete_all_keu_peserta(){
		$mmaster = new model_honor();
		$mmaster->hapus_data_bayar_detailByBayarId($_POST['bayar_id']);
	}
	function delete_keu_peserta (){
		$mmaster = new model_honor();
		$mmaster->hapus_data_bayar_detail($_POST['hr_id']);
	}
	function submit_kategori_bayar(){
		$data = array(
			'kategori_bayar'=>$_POST['kategori_bayar'],
			'keterangan'=>$_POST['keterangan'],
			'kelompok'=>$_POST['kelompok']
		);
		$mmaster = new model_honor();
		$mmaster->simpan_kategori_bayar($data);
		echo json_encode($data);
	}
	function delete_kategori_bayar(){
		$mmaster = new model_honor();
		$mmaster->delete_kategori_bayar($_POST['kategori_bayar']);
	}
	function cetak($kriteria, $id){
		$mconf = new model_honor();
		if($kriteria == 'kegiatan'){
			$data['kegiatan'] =  $data['kegiatan'] = $mconf->get_keu_bayar($id);
			$data['peserta'] = $mconf->get_keu_pesertaDet($id);
			
			
			$this->view( 'keuangan/kegiatan/cetak.php', $data );
		}
	}
}
?>
