<?php
class master_layanan extends comsmodule {
	
	//---kenaikan belum edit--------------------------
	
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($str=NULL, $id=NULL){
		$mconf 	= new model_general();	
		$mmutu	= new model_layanan();
		
		$level = $this->coms->authenticatedUser->level;
		
		if($level=='1'){
			$data['fakultas']	= $mconf->get_fakultas();
		}else{
			$data['fakultas']	= $mconf->get_fakultas($this->coms->authenticatedUser->fakultas);
		}
		
		$data['cabang']		= $mconf->get_cabang();
		$data['fakultas_id']= $this->coms->authenticatedUser->fakultas; 
		$data['kategori']	= $mmutu->read_category();
		$data['mconf']		= $mconf;
		
		if(isset($_POST['fakultas'])){
			$fakultas = $_POST['fakultas'];
			$data['fakultas_id']=$fakultas;
		}else $fakultas=NULL;
		
		if(isset($_POST['cabang'])&&$_POST['cabang']!='0'){
			$cabang = $_POST['cabang'];
			$data['cabangid']=$cabang;
		}else $cabang=NULL;
		
		$data['posts'] = $mmutu->read($id, $fakultas, $cabang);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		$this->add_script('js/layanan/layanan.js');
		
		if($str=='write'){
			$data['form'] = 'write';
		}
			
		$this->view('layanan/index.php', $data );
	}
	
	function detail($id){
		if(  !$id ) {
			$this->redirect('module/master/staff');
			exit;
		}
		$mconf 	= new model_dosen();			
		
		$data['posts'] 		= $mconf->read($id, '', '');
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->view('staff/detail.php', $data);
		
	}
	
	function write(){
		$this->index('write');
		exit;		
	}	
	
	function edit($id){
		$this->index('write', $id);
		exit;	
	}
	
	function saveToDB(){
		ob_start();
		$mconf 	= new model_dosen();
		
		//---------------DATA----------------------------------------
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		if(isset($_POST['chknip'])!=""){
			$isnik	= 'nip';
		}else{
			$isnik	= 'nik';
		}

		$nik	= $_POST['nik'];
		$nama	= $_POST['nama'];
		
		if(isset($_POST['gelarawal'])!=""){
			$gelarawal	= $_POST['gelarawal'];
		}else{
			$gelarawal	= null;
		}
		
		if(isset($_POST['gelarakhir'])!=""){
			$gelarakhir	= $_POST['gelarakhir'];
		}else{
			$gelarakhir	= null;
		}
		
		$tgl		= $_POST['tgl'];
		$rjenis 	= $_POST['rjenis'];
		$biografi	= $_POST['biografi'];
		$telepon	= $_POST['telepon'];
		$nohp		= $_POST['nohp'];
		$alamat		= $_POST['alamat'];
		$fakultas	= $_POST['fakultas'];
		$cabang		= $_POST['cabang'];
		$isaktif	= $_POST['saktif'];
		$email		= $_POST['email'];
		$tglmasuk	= $_POST['tglmasuk'];
		$tglpensiun	= $_POST['tglpensiun'];
		$pin		= $_POST['pin'];
		
		if(isset($_POST['chkdsnstatus'])!=""){
			$is_tetap	= 'tetap';
		}else{
			$is_tetap	= 'kontrak';
		}
		
		if(isset($_POST['chkdosen'])!=""){
			$is_status	= 'dosen';
		}else{
			$is_status	= 'staff';
		}
		
		if($_POST['hidId']){
			$karyawan_id = $_POST['hidId'];
			$action 	 = "update";
		}else{
			$t			 = explode('-', $tgl);
			$tglid		 = $t[0].$t[1];

			$karyawan_id = $mconf->get_reg_number($tglid);//--------------------
			$action 	 = "insert";
		}
		
		
		foreach ($_FILES['files'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}else $cekicon = 'sukses';
			}
		}

		//------------------UPLOAD FILE START---------------------------------------------
		if($cekicon!='error'){
			foreach ($_FILES['files'] as $id => $icon) {
				$brokeext			= $this->get_extension($icon);
				if($id == 'name'){
					$ext 	= $brokeext;
					$file	= $icon;
				}
				if($id == 'type'){
					$file_type = $_FILES['files']['type'];
				}
				if($id == 'size'){
					$file_size = $_FILES['files']['size'];
				}
				if($id == 'tmp_name'){
					$file_tmp_name = $_FILES['files']['tmp_name'];
				}
			}
			$seleksi_ext 	= $mconf->get_ext($ext);
			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
		
				switch(strToLower($jenis)){
					case 'image':
					$extpath = "image";
					break;
				}
			}
			else{
				echo "gagal";
				//$this->redirect('module/akademik/mkditawarkan/index/failed');
			}
			$upload_dir = 'assets/upload/foto/'.$fakultas. DIRECTORY_SEPARATOR .$is_status. DIRECTORY_SEPARATOR ;		
			$upload_dir_db = 'upload/foto/'.$fakultas. DIRECTORY_SEPARATOR .$is_status. DIRECTORY_SEPARATOR ;	
			
			if (!file_exists($upload_dir)) {
				mkdir($upload_dir, 0777, true);
				chmod($upload_dir, 0777);
			}
			$namafile = $karyawan_id . '.' . $ext;
			$file_loc = $upload_dir_db . $namafile;
			
			$allowed_ext = array('jpg','jpeg','png','gif');
			if(!in_array($ext,$allowed_ext)){
				echo "Silahkan upload gambar";
				//$this->redirect('module/akademik/mkditawarkan/index/wrongfiletype');
			}
			if($file_size > $maxfilesize){
				echo "Ukuran file anda terlalu besar.";
				// $this->redirect('module/akademik/mkditawarkan/index/toobig');
			}
		}
		else {
			if(!isset($_POST['hidimg'])&&$_POST['hidimg']=""){
				$file_loc	= "notinc";
			}else {
				$file_loc	= $_POST['hidimg'];
			}
		}
		
		if($_SERVER['REQUEST_METHOD'] == 'POST') {
				rename($file_tmp_name, $upload_dir . $namafile);
		}			
		//------------------UPLOAD FILE END-----------------------------------------------------
		
		//---------------END DATA----------------------------------------
		
		
		$datanya 	= Array(
						'foto'=>$file_loc,
						'karyawan_id'=>$karyawan_id, 
						'nik'=>$nik, 
						'pin'=>$pin, 
						'nama'=>$nama,
						'gelar_awal'=>$gelarawal,
						'gelar_akhir'=>$gelarakhir,
						'tgl_lahir'=>$tgl,
						'jenis_kelamin'=>$rjenis,
						'biografi'=>$biografi,
						'tgl_masuk'=>$tglmasuk,
						'tgl_pensiun'=>$tglpensiun,
						'telp'=>$telepon,
						'hp'=>$nohp,
						'email'=>$email,
						'alamat'=>$alamat,
						'fakultas_id'=>$fakultas,
						'cabang_id'=>$cabang,
						'is_nik'=>$isnik,
						'is_aktif'=>$isaktif,
						'is_tetap'=>$is_tetap,
						'is_status'=>$is_status,
						'home_base'=>$fakultas,
						'user_id'=>$userid,
						'last_update'=>$lastupdate
						);
		if($file_loc=="notinc"){unset($datanya[0]);}			
		$mconf->replace_dosen($datanya);
		$mconf->update_foto($file_loc, $karyawan_id);
	}

	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function save_karyawan_prestasiToDB(){
		$mconf 	= new model_dosen();
		
		$karyawanid = $_POST['hidId'];
	
		
		$mulai = $_POST['mulai'];
		$selesai = $_POST['selesai'];
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$prestasiid = $mconf->get_reg_prestasi();
		
		
		if($_POST['hidIdval']==''){ //--insert new
			$datanya 	= Array(
						'prestasi_id'=>$prestasiid,
						'jenis_id'=>'lain', 
						'tgl_mulai'=>$mulai, 
						'tgl_selesai'=>$selesai,
						'judul'=>$_POST['judul'],
						'penyelenggara'=>$_POST['penyelenggara'],
						'lokasi'=>$_POST['lokasi'],
						'inf_prestasi'=>$_POST['prestasi'],
						'tingkat'=>$_POST['tingkat'],
						'jenis_prestasi'=>$_POST['rjenis'],
						'keterangan'=>$_POST['catatan'],
						'penghargaan'=>$_POST['penghargaan'],
						'kategori'=>'staf',
						'link_berita'=>$_POST['linkberita'],
						'user_id'=>$userid,
						'last_update'=>$lastupdate
						);
						
			if($mconf->insert_prestasi($datanya)==TRUE){
				$pesertaid = $mconf->get_reg_prestasi_peserta($prestasiid, $karyawanid);
				$peserta = Array('peserta_id'=>$pesertaid, 'karyawan_id'=>$karyawanid, 'prestasi_id'=>$prestasiid, 'inf_nama'=>$_POST['hidNama']);
				
				if($mconf->insert_prestasi_peserta($peserta)==TRUE) echo "Insert data success"; 
				echo "Data berhasil disimpan";
			}else{
				echo "Data gagal disimpan";
			}
		
		}else{ //----edit
			$paramdata = explode(',', $_POST['hidIdval']);
			$paramunitid = $paramdata[0];
			$paramperiodemulai = $paramdata[1];
			$paramperiodeselesai = $paramdata[2];
			$paramisaktif = $paramdata[3];
			
			if($mconf->update_unit($unitid,$karyawanid,$periodemulai,$periodeselesai,$isaktif,$paramunitid,$paramperiodemulai,$paramperiodeselesai,$paramisaktif)==TRUE){
				echo "Data berhasil disimpan";
			}else{
				echo "Data gagal disimpan";
			}
			
		}
		
	}
	
	
	function save_karyawan_unitToDB(){
		$mconf 	= new model_dosen();
		
		$karyawanid = $_POST['hidId'];
		$fakultasid = $_POST['fakultas'];
		$cabangid   = $_POST['cabang'];
		$unitid		= $mconf->get_unit($_POST['unit'])->hidId;
		
		$periodemulai = $_POST['periodemulai'];
		
		if(isset($_POST['current'])!=""){
			$periodeselesai = '0000-00-00';
		}else{
			$periodeselesai = $_POST['periodeselesai'];
		}
		
		if(isset($_POST['isaktif'])!=""){
			$isaktif	= '1';
		}else{
			$isaktif	= '0';
		}
		
		if($_POST['hidIdval']==''){ //--insert new
			$datanya 	= Array(
						'unit_id'=>$unitid,
						'karyawan_id'=>$karyawanid, 
						'periode_mulai'=>$periodemulai, 
						'periode_selesai'=>$periodeselesai,
						'is_aktif'=>$isaktif
						);
						
			if($mconf->insert_unit($datanya)==TRUE){
				echo "Data berhasil disimpan";
			}else{
				echo "Data gagal disimpan";
			}
		
		}else{ //----edit
			$paramdata = explode(',', $_POST['hidIdval']);
			$paramunitid = $paramdata[0];
			$paramperiodemulai = $paramdata[1];
			$paramperiodeselesai = $paramdata[2];
			$paramisaktif = $paramdata[3];
			
			if($mconf->update_unit($unitid,$karyawanid,$periodemulai,$periodeselesai,$isaktif,$paramunitid,$paramperiodemulai,$paramperiodeselesai,$paramisaktif)==TRUE){
				echo "Data berhasil disimpan";
			}else{
				echo "Data gagal disimpan";
			}
			
		}
		
	}
	
	function get_unit(){
		$mconf 	= new model_dosen();
		
		$fakultasid = $_POST['fakultasid'];
		$cabangid = $_POST['cabangid'];
		
		if(isset($_POST['unitid'])&&$_POST['unitid']!=''){
			 $unitid	= $_POST['unitid'];
		}else $unitid	= '';
		
		$unit = $mconf->get_unit('',$fakultasid,$cabangid);
		echo "<option value='-'>Select Unit</option>";
		if($unit){
			foreach($unit as $u) :
				echo "<option value='".$u->unit_id."' ";
				if($unitid==$u->unit_id){
					echo "selected";
				}
				echo " >".$u->keterangan."</option>";
			endforeach;
		}else echo '';
	}

	function delete_unit(){
		$mconf 		= new model_dosen();
		
		$unitid			= $_POST['unitid'];
		$karyawanid 	= $_POST['karyawanid'];
		$periodemulai 	= $_POST['periodemulai'];
		$periodeselesai = $_POST['periodeselesai'];
		$isaktif 		= $_POST['isaktif'];
		
		if($mconf->delete_unit($unitid,$karyawanid,$periodemulai,$periodeselesai,$isaktif)==TRUE){
			echo "Success";
		}else echo "Failed";
		// echo $unitid." ".$karyawanid;
	}
	
	function delete_kenaikan(){
		$mconf 		= new model_dosen();
		
		$kenaikanid			= $_POST['kenaikanid'];
		
		if($mconf->delete_kenaikan($kenaikanid)==TRUE){
			echo "Success";
		}else echo "Failed";
	}
	
	function get_jenis(){
		$mconf 		= new model_dosen();
		
		$jenis			= $_POST['jenis'];
		
		if(isset($_POST['selectedval'])&&$_POST['selectedval']!=''){
			$selval = $_POST['selectedval'];
		}else $selval='';
		
		if($jenis=='jabatan'){
			$data = $mconf->get_jabatan();
			$jabatanid = '';
			
			echo "<option value='-'>Select Jabatan</option>";
			if($data){
				foreach($data as $d) :
					echo "<option value='".$d->jabatan_id."' ";
					if($selval==$d->jabatan_id){
						echo "selected";
					}
					echo " >".$d->keterangan."</option>";
				endforeach;
			}else echo '';
			
		}else{
			$mjenis = new model_jenis();
			$data = $mjenis->read();
			$jenisid = '';
			
			echo "<option value='-'>Select Kenaikan</option>";
			if($data){
				foreach($data as $d) :
					echo "<option value='".$d->jenis_kenaikan_id."' ";
					if($jenisid==$d->jenis_kenaikan_id){
						echo "selected";
					}
					echo " >".$d->keterangan."</option>";
				endforeach;
			}else echo '';
			
		}
		
	}
	
	//======Document======//
	function detailchild($id=NULL, $str=NULL){
		$mdocument  = new model_document();
		
		$child = $mdocument->read($id,'');
		
		$level = $this->coms->authenticatedUser->level; 
		
		//echo $str." ".$id;
		echo "<ul>";
			if(isset($child)){ 
				//$n = 1;
				$folder = $str;
				foreach ($child as $dt) {
					$folder = $str."/".$dt->judul;
					if($dt->is_available == '1'){
						$aktif = "";
					}
					else{
						$aktif = "<code><small>*)tidak aktif</small></code>";
					}
					if($level!="3"){
				?>
						<li>
 							<span class="span text text-default" onclick="view_content('<?php echo $dt->id ?>','<?php echo $dt->kar_id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name."<br>".$aktif ?></span>
 							<a title="setting" class='btn-add' href="#" data-toggle="modal" data-target="#newfolder" onclick="rename('<?php echo $dt->id ?>','<?php echo $dt->folder_name ?>','<?php echo $dt->is_available ?>')"><span class='text'><i class="fa fa-gear"></i></span></a>
					 		<a title="create new folder" class='btn-add' data-toggle="modal" data-target="#newfolder" onclick="newfolder_parent('<?php echo $dt->id ?>')" href="#"><span class='text'><i class="fa fa-plus"></i></span></a>
					 		<a title="upload file" class='btn-add' data-toggle="modal" data-target="#newfile" href="#" onclick="newfile_parent('<?php echo $dt->id ?>','<?php echo $folder; ?>')"><span class='text'><i class="fa fa-cloud-upload"></i></span></a>
					 	<?php if($level=="1"){ ?>
					 		<a title="delete folder" class='btn-add' href="#" onclick="doDelete('<?php echo $dt->folderid ?>','folder')"><span class='text'><i class="fa fa-trash-o"></i></span></a>
						<?php } ?>
							<?php echo $this->detailchild( $dt->id, $folder) ?>
						</li>
					<?php 
					}else{
						?>
						<li><span class="span text text-default" onclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name." ".$aktif ?></span>
						<?php echo $this->detailchild( $dt->id, $folder) ?>
						</li>
					<?php
					}
				} 
			}
		echo "</ul>";
	}
	
	//folder
	function save_folder(){
		$mdocument  = new model_document();
		$mconf  = new model_general();
		
		$level = $this->coms->authenticatedUser->level;
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		
		if($level=='18'){
			$kar_id 	= $_POST['kar_id'];
			$karyawanid = $mconf->get_karyawan_id_by_md5($kar_id)->id;
		}
		else{
			$karyawanid 	= $this->coms->authenticatedUser->staffid;
		}
		
		$id 		= $_POST['id'];
		
		if(isset($_POST['aktif'])){
			$aktif = 1;
		}
		else{
			$aktif = 0;
		}
		
		if($id != ""){
			$folder_name = $_POST['folder_name'];
			$mdocument->rename_folder($id, $folder_name, $aktif);
			echo "Rename folder process success!";
			exit();
		}
		else{
			$parent = $_POST['parent'];
			$folder_name = $_POST['folder_name'];
			$folder_id = $mdocument->folder_id();
			
			$datanya 	= Array(
								'id' => $folder_id,
								'karyawan_id' => $karyawanid,
								'folder_name' => $folder_name,
								'parent_id' => $parent,
								'is_available' => $aktif,
								'user_id' => $user,
								'last_update' => $lastupdate
							   );
			$mdocument->replace_folder($datanya);
			echo "Create folder process success!";
			exit();
		}
	}
	
	//file
	function save_file(){
		$mdocument  = new model_document();
		$user		= $this->coms->authenticatedUser->id;
		$folder_id  = $_POST['folder'];
		$folder		= $_POST['folder_loc'];
		$title		= $_POST['title'];
		$keterangan	= $_POST['keterangan'];
		$uploads	= $_FILES['uploads'];
		
		$i=0;
		foreach ($_FILES['uploads']['name'] as $id => $file){
			$lastupdate	= date("Y-m-d H:i:s");
			$name  = $_FILES['uploads']['name'][$id];
			$ext   = $this->get_extension($name);
			switch(strToLower($ext)){
				case 'jpg':
				case 'jpeg':
				case 'png':
				case 'gif':
					$jenisfile = "image";
					break;
				case 'doc':
				case 'ppt':
				case 'pdf':
				case 'xls':
				case 'docx':
				case 'pptx':
				case 'xlsx':
					$jenisfile = "document";
					break;
			}
			$file_type=$_FILES["uploads"]["type"][$id];
			$file_size=$_FILES["uploads"]["size"][$id];
			$upload_dir = 'assets/upload/file/'.$user. '/library/kepegawaian/'.$folder. '/';
			if($folder!==""){
				$upload_dir_db = 'upload/file/'.$user. '/library/kepegawaian/'.$folder. '/';
			}
			else{
				$upload_dir_db = 'upload/file/'.$user. '/library/kepegawaian/';
			}
			
			$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx');
			if(!in_array($ext,$allowed_ext)){
				echo "Maaf, tipe data file yang anda upload tidak diperbolehkan!";
				exit();
			}
			else{
				if (!file_exists($upload_dir)) {
					mkdir($upload_dir, 0777, true);
					chmod($upload_dir, 0777);
				}
				$file_loc = $upload_dir_db . $name;
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					rename($_FILES['uploads']['tmp_name'][$id], $upload_dir . $name);
				}
				
				$file_id = $mdocument->file_id();
				$datanya 	= Array('file_id' => $file_id,
									'folder_id' => $folder_id,
									'jenis_file' => $jenisfile,
									'judul' => $title[$i],
									'keterangan' => $keterangan[$i],
									'file_name' => $name,
									'file_type' => $file_type,
									'file_loc' => $file_loc,
									'file_size' => $file_size,
									'user_id' => $user,
									'last_update' => $lastupdate
									);
				$mdocument->replace_file($datanya);
				
				//echo $title[$i]."\n".$name."\n".$file_type."\n".$file_size."\n".$jenisfile."\n";
				$i++;
			}
		}	
		
		echo "Upload file Success!";
		exit();
	}
	
	function view_content(){
		$mdocument  = new model_document();
		$mconf		= new model_general();
		
		$folder_id 	= $_POST['id'];
		$level = $this->coms->authenticatedUser->level;
		
		if($level=='18'){
			$kar_id 		= $_POST['karyawan'];
			$karyawanid 	= $mconf->get_karyawan_id_by_md5($kar_id)->id;
		}
		else{
			$karyawanid 	= $this->coms->authenticatedUser->staffid;
		}
		
		
		
		$child 		= $mdocument->read($folder_id, $karyawanid);
		$childfile 	= $mdocument->read_file($folder_id,'',$karyawanid);
		
		//print_r($child) ; 
		$level 		= $this->coms->authenticatedUser->level; 
		?>
		<table class="table table-hover example">
			<thead>
				<tr>
					<th><small>Name</small></th>
					<th><small>Size</small></th>
					<th><small>Last Modified</small></th>
					<th class="library_attach" style="display:none">&nbsp;</th>
					<?php if($level=="1"){ ?>
					<th>&nbsp;</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
		<?php
		if(isset($child)){?>
			
			<?php
			foreach ($child as $dt) {?>
				<tr class="folder<?php echo $dt->folderid ?>">
					<td><span class='span' style="cursor: pointer" onclick="view_content('<?php echo $dt->id ?>','<?php echo $dt->kar_id ?>')"><i class="fa fa-folder-o"></i> <?php echo $dt->folder_name ?></span></td>
					<td></td>
					<td></td>
					<td class="library_attach" style="display:none"></td>
					<?php if($level=="1"){ ?>
					<td>
						<span onclick="doDelete('<?php echo $dt->id ?>','folder')"><i class="fa fa-trash-o" style="cursor: pointer"></i></span>
					</td>
					<?php }?>	
				</tr>
			<?php 
			} 
		}
		if(isset($childfile)){?>
			<?php
			foreach ($childfile as $d) {?>
				<tr class="file<?php echo $d->id ?>">
					<td>
						<span style="cursor: pointer">
							<?php if($d->jenis_file == 'document'){ ?>
								<i class="fa fa-file-text-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'image'){ ?>
								<i class="fa fa-picture-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'video'){ ?>
								<i class="fa fa-video-camera"></i>&nbsp;
							<?php } ?>
							<a class="" href="#" onClick="doView('<?php echo $d->file_id;?>','<?php echo $d->folder_id;?>')";><span class="text text-default"><?php echo $d->file_name ?></span></a>
						</span>
					</td>
					<td><small><?php echo $this->formatSizeUnits($d->file_size) ?></small></td>
					<td><small><i class="fa fa-clock-o"></i>&nbsp;<?php echo $d->last_update ?></small></td>
					<td class="library_attach" style="display:none">
						<span onclick="select('<?php echo $d->file_id;?>','<?php echo $d->file_name;?>')"><i class="fa fa-check-square-o" style="cursor: pointer"></i></span>
					</td>
					<?php if($level=="1"){ ?>
					<td>					
						<span onclick="doDelete('<?php echo $d->id ?>','file')"><i class="fa fa-trash-o" style="cursor: pointer"></i></span>						
					</td>
					<?php }?>	
				</tr>
			<?php 
			} 
		}
		?>				
			</tbody>
		</table>
		<script>
			$(document).ready( function() {
			  $('.example').dataTable( {
			  	"bDestroy": true,
				"fnInitComplete": function(oSettings, json) {
				  //alert( 'DataTables has finished its initialisation.' );
				}
			  } );
			} )

		</script>
		<?php
	}

	function formatSizeUnits($bytes){
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}
	
	function files($id=NULL, $folder=NULL){
		$mdocument  = new model_document();
		
		$karyawanid	= $this->coms->authenticatedUser->staffid; 
		$data['detail']	= $mdocument->read_file("", $id, $karyawanid);
		$this->view('staff/document/view_content.php', $data );
	}
	
	function download($id=NULL){
		$mdocument  = new model_document();
		$karyawanid	= $this->coms->authenticatedUser->staffid; 
		$dt		= $mdocument->read_file("",$id,$karyawanid);
		
		$path 	= "assets/".$dt->file_loc;	
			
		if (file_exists($path)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename=' . urlencode(basename($path)));
			// header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path));
			ob_clean();
			flush();
			readfile($path);
			exit;
		}else{
			die('File Not Found');
		}
	}
	
	function delete($folder_id=NULL,$jenis_del=NULL){ //folder
		$mdocument  = new model_document();
		
		$jenis_del = $_POST['jenis_del'];
		
		if($jenis_del == 'file'){
			$id = $_POST['delete_id'];
			$del_file = $mdocument->delete_file($id);
			if($del_file == TRUE){
				echo "Delete file success!";
			}
		}
		else{
			
			if (!$folder_id){
				$id = $_POST['delete_id'];
			}
			else $id = $folder_id;
			
			//echo $id."\n";
			
			$get_child_id = $mdocument->get_id_from_parent($id);
			$cek_file_in_folder = $mdocument->read_file($id);
			//echo count($cek_file_in_folder)."\n";
			//echo count($get_child_id)."\n";
			if(isset($cek_file_in_folder)){
				$del_file = $mdocument->delete_file_by_folder($id);
			}
			$del_parent = $mdocument->delete_folder($id);
			
			if(isset($get_child_id)){
				foreach ($get_child_id as $c) {
					$folderid=$c->id;
					//echo $folderid."\n";
					$this->delete($folderid);
				}
			}
			if($del_parent == TRUE) echo "Delete file success!";
		}
		
	}
	
	function edit_kenaikan(){
		$mconf 	= new model_dosen();
		
		$kenaikan_id = $_POST['kenaikanid'];
		$datakenaikan = $mconf->get_kenaikan('',$kenaikan_id);
		
		$return_arr = array();
		
		foreach($datakenaikan as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);
		echo $json_response;
	}
	
	function save_karyawan_kenaikanToDB(){
		$mconf 	= new model_dosen();
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		if($_POST['hidNaikId']){
			$kenaikan_id = $_POST['hidNaikId'];
			$action 	  = "update";
		}else{
			$kenaikan_id = $mconf->get_kenaikanId();
			$action 	  = "insert";
		}
				
		$jenis = $_POST['jenis'];
		if($jenis=='jabatan'){
			$jabatanid = $mconf->get_jabatanIdby($_POST['select_jenis'])->jabatan_id;
			$jenis_kenaikanid  = '-';
		}else{
			$jabatanid = '-';
			$jenis_kenaikanid  = $mconf->get_jeniskenaikanIdby($_POST['select_jenis'])->jenis_kenaikan_id;
		}
		
		$fakultasid = $_POST['fakultas'];
		$unitid		= $mconf->get_unit($_POST['unit'])->hidId;
		
		$golongan = $_POST['select_golongan'];
		$karyawanid = $_POST['hidId'];
		$tglsk = $_POST['tanggalsk'];
		$nosk = $_POST['nosk'];
		$periodemulai = $_POST['periodemulaikenaikan'];
		$periodeselesai = $_POST['periodeselesaikenaikan'];
		$tmt = $_POST['tmt'];
		$pejabat = $_POST['pejabat'];
		$kredit = $_POST['kredit'];
		$nonota = $_POST['nonota'];
		$tanggalnota = $_POST['tanggalnota'];
		$nostlud = $_POST['nostlud'];
		$tanggalstlud = $_POST['tanggalstlud'];
		
		$tahunkerja = $_POST['tahun-kerja'];
		$bulankerja = $_POST['bulan-kerja'];
		
		$tahun_in_days = $tahunkerja*365;
		$bulan_in_days = $bulankerja*30;
		
		$masakerja = $tahun_in_days + $bulan_in_days;
		
		if(isset($_POST['isaktif'])!=""){
			$isaktif	= '1';
		}else{
			$isaktif	= '0';
		}
		
		if(isset($_POST['docid'])!=""){
			$docid	= $_POST['docid'];
		}else{
			$docid	= '-';
		}
		
		$keterangan = $_POST['keterangan'];
		
		$datanya = Array(
				   'kenaikan_id'=>$kenaikan_id,
				   'jenis_kenaikan_id'=>$jenis_kenaikanid,
				   'document_id'=>$docid,
				   'jabatan_id'=>$jabatanid,
				   'golongan'=>$golongan,
				   'karyawan_id'=>$karyawanid,
				   'tgl_sk'=>$tglsk,
				   'no_sk'=>$nosk,
				   'periode_mulai'=>$periodemulai,
				   'periode_selesai'=>$periodeselesai,
				   'tmt'=>$tmt,
				   'pejabat_penetap'=>$pejabat,
				   'kredit'=>$kredit,
				   'no_nota'=>$nonota,
				   'tgl_nota'=>$tanggalnota,
				   'no_stlud'=>$nostlud,
				   'tgl_stlud'=>$tanggalstlud,
				   'masa_kerja'=>$masakerja,
				   'is_aktif'=>$isaktif,
				   'jenis'=>$jenis,
				   'fakultas_id'=>$fakultasid,
				   'unit_id'=>$unitid,
				   'keterangan'=>$keterangan,
				   'user_id'=>$userid,
				   'last_update'=>$lastupdate
				   );
				   
		if($mconf->replace_kenaikan($datanya)){
			// $mconf->update_golongan($karyawanid,$golongan);
			echo "Success";
		}else{
			echo "Failed";
		};
		
	}
	
	//=======Pendidikan=======//
	function get_jenis_kegiatan(){
		$mconf 	= new model_dosen();
		$mconf 		= new model_general();
		
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$data = $mconf->get_jenis_kegiatan("non",$str);
		
		if($data){
			$return_arr = array();
	
			foreach($data as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			
			$json_response = json_encode($return_arr);
			
			//$data['tes'] = $json_response;
			# Return the response
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}
	
	function save_pendidikan(){
		$mconf 	= new model_dosen();
		$mconf 		= new model_general();
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$level = $this->coms->authenticatedUser->level;
		
		if($level=='18'){
			$kar_id 	= $_POST['kar_id'];
			$karyawanid = $mconf->get_karyawan_id_by_md5($kar_id)->id;
		}
		else{
			$karyawanid 	= $this->coms->authenticatedUser->staffid;
		}
		
		
		if(isset($_POST['document_formal'])){
			//echo "formal";
			$jenjang 		= $_POST['jenjang'];
			$thn_lulus 		= $_POST['thn_lulus'];
			$prodi 			= $_POST['prodi'];
			$background		= $_POST['background'];
			$nama_sekolah 	= $_POST['nama_sekolah'];
			$alamat_sekolah = $_POST['alamat_sekolah'];
			$document_formal = $_POST['document_formal'];
			
			if(isset($_POST['hidid']) && $_POST['hidid'] != ""){
				$id = $_POST['hidid'];
			}else{
				$id = $mconf->formal_id();
			}
			
			$datanya = Array(
				   'formal_id'=>$id,
				   'document_id'=>$document_formal,
				   'karyawan_id'=>$karyawanid,
				   'jenjang'=>$jenjang,
				   'tahun_lulus'=>$thn_lulus,
				   'program_studi'=>$prodi,
				   'latar_belakang_ilmu'=>$background,
				   'nama_sekolah'=>$nama_sekolah,
				   'alamat_sekolah'=>$alamat_sekolah,
				   'user_id'=>$userid,
				   'last_update'=>$lastupdate
			);
			//var_dump($datanya);
			if($mconf->replace_pendidikan_formal($datanya)){
				echo "Success";
			}else{
				echo "Failed";
			}
			
		}
		else if(isset($_POST['document_non_formal'])){
			//echo "non formal";
			$nama_kegiatan 			= $_POST['nama_kegiatan'];
			$jenis_kegiatan 		= $_POST['jenis_kegiatan_id'];
			$penyelenggara 			= $_POST['penyelenggara'];
			$tgl_mulai				= $_POST['tgl_mulai'];
			$tgl_selesai 			= $_POST['tgl_selesai'];
			$tmpt_kegiatan 			= $_POST['tmpt_kegiatan'];
			$kota_kegiatan 			= $_POST['kota_kegiatan'];
			$tingkat 				= $_POST['tingkat'];
			$sebagai 				= $_POST['sebagai'];
			$document_non_formal	= $_POST['document_non_formal'];
			
			if(isset($_POST['hidid']) && $_POST['hidid'] != ""){
				$id = $_POST['hidid'];
			}else{
				$id = $mconf->non_formal_id();
			}
			
			$datanya = Array(
					'non_formal_id'=>$id,
					'document_id'=>$document_non_formal,
					'karyawan_id'=>$karyawanid,
				    'nama_kegiatan'=>$nama_kegiatan,
					'jenis_kegiatan'=>$jenis_kegiatan,
					'penyelenggara'=>$penyelenggara,
					'tgl_mulai'=>$tgl_mulai,
					'tgl_selesai'=>$tgl_selesai,
					'tempat_kegiatan'=>$tmpt_kegiatan,
					'kota_kegiatan'=>$kota_kegiatan,
					'tingkat'=>$tingkat,
					'sebagai'=>$sebagai,
					'user_id'=>$userid,
				    'last_update'=>$lastupdate
			);
			//var_dump($datanya);
			if($mconf->replace_pendidikan_non_formal($datanya)){
				echo "Success";
			}else{
				echo "Failed";
			}
		}
	}
}
?>