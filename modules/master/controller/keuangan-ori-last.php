<?php
class master_keuangan extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	//dosen_titip
	function titip($tahun=NULL){
		$mkeuangan = new model_keuangan();
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		
		$this->add_script('js/keuangan/titip.js');	
		$data['tahun'] = $mkeuangan->get_list_tahunakademik();
		
		if($tahun == NULL || $tahun == '-') $tahun = substr(md5($mkeuangan->get_tahunakademik_aktif()),8,7);
		$data['tahun_detail'] = $mkeuangan->get_detail_tahun($tahun);
		
		$data['titip'] = $mkeuangan->get_data_dosen_titip($tahun);
		$data['dosen'] = $mkeuangan->get_dosen();
		$this->view('keuangan/titip.php', $data);
	}
	
	function save_titip(){
		$mkeuangan = new model_keuangan();
		
		if($_POST['dosen_sk'] == '-' || $_POST['dosen_titip'] == '-') $this->redirect('module/master/keuangan/titip');
		
		if($_POST['titip'] == '' ) :
			$data = array(
				'titip_id' => $mkeuangan->get_titip_id($_POST['dosen_sk'], $_POST['dosen_titip']),
				'tahun_akademik' => $mkeuangan->get_tahunakademik_aktif(),
				'karyawan_id' => $_POST['dosen_sk'],
				'titip_ke' => $_POST['dosen_titip']	
			);
			$mkeuangan->save_titip($data);
		else :
			$data = array(
				'tahun_akademik' => $mkeuangan->get_tahunakademik_aktif(),
				'karyawan_id' => $_POST['dosen_sk'],
				'titip_ke' => $_POST['dosen_titip']	
			);
			
			$where = array(
				'titip_id' => $_POST['titip']
			);
			$mkeuangan->update_titip($data, $where);
		endif;
		
		$this->redirect('module/master/keuangan/titip');
	}
	
	function del_titip($id=NULL){
		$mkeuangan = new model_keuangan();
		$mkeuangan->del_titip($id);
		$this->redirect('module/master/keuangan/titip');
	}
	
	//konfigurasi
	function conf(){
		$mkeuangan = new model_keuangan();
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/keuangan/conf.js');	
		$data['golongan'] = $mkeuangan->get_golongan();
		$data['conf'] = $mkeuangan->get_conf();
		$this->view('keuangan/conf.php', $data);
	}
	
	function save_conf(){
		$mkeuangan = new model_keuangan();
		
		$golongan	= $_POST['golongan'];
		$jenjang	= $_POST['jenjang_pendidikan'];
		$tarif		= $_POST['tarif'];
		
		if(isset($_POST['is_pns'])) $pns='1';
		else $pns='0';
		
		$potongan	= $_POST['potongan'];
		$lastupdate	= date("Y-m-d H:i:s");
		
		$datanya	= array(
			'golongan'=>$golongan, 
			'jenjang_pendidikan'=>$jenjang, 
			'tarif'=>$tarif, 
			'is_pns'=>$pns, 
			'potongan'=>$potongan, 
			'kategori'=>'mengajar', 
			'user'=>$this->coms->authenticatedUser->id, 
			'last_update'=>$lastupdate
		);
		
		if(isset($_POST['conf_id'])){
			$where = array(
				'MID(MD5(conf_id),8,6)' => $_POST['conf_id']
			);
			$mkeuangan->update_conf($datanya, $where);
		}
		else {
			$datanya['conf_id'] = $mkeuangan->get_conf_id();
			$mkeuangan->replace_conf($datanya);
		}
		
		// foreach($datanya as $key => $value) echo $key . ' => ' . $value . ' <br>';
		// $mkeuangan->replace_conf($datanya);
		$this->redirect('module/master/keuangan/conf');
	}
	
	//honorarium
	function  index($prodi=NULL,$mulai=NULL, $selesai=NULL){	
		
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
			
		$mconf = new model_honor();		
		
		$data['prodi'] 	  = $mconf->get_prodi();
		
		if(isset($_POST['tmulai'])) $tmulai = $_POST['tmulai'];
		else $tmulai = $mulai;
		
		if(isset($_POST['tselesai'])) $tselesai = $_POST['tselesai'];
		else $tselesai = $selesai;
		
		if(isset($_POST['cmbprodi'])) $prodi = $_POST['cmbprodi'];
		else $prodi = $prodi;
		
		$data['mulai'] = $tmulai;
		$data['selesai'] = $tselesai;
		$data['prodiid'] = $prodi;
		
		if($tmulai && $tselesai) $data['posts'] = $mconf->get_rekap_absen($prodi, $tmulai, $tselesai);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->add_script('js/keuangan/jsall.js');	
		$this->add_style('css/calendar/calendar.css');		
			
		$this->view( 'keuangan/hr/index.php', $data );
	}

	function proses($str=NULL){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		if(isset($_POST['chkdosen']) && ($str=='bayar')):
			$mconf = new model_honor();	
			
			$dosen 	= $_POST['chkdosen'];
			$mulai 	= $_POST['mulai'];
			$selesai= $_POST['selesai'];
			$prodi	= $_POST['prodi'];
			
			$isproses = $mconf->get_proses_bayar($mulai);
			if($isproses) $strproses = "selisih";
			else $strproses = "main";
			
		//	$mconf = new model_conf();
		
			// foreach($_POST['chkdosen'] as $key){
				// echo $key . '<br>';
			// }
			// echo $_POST['mulaix'];
			// echo "<hr>";
		
			if($dosen):
				$total_honor=0;
				$total_honor_pph=0;
				$total_honor_all=0;
				$kode = $mconf->get_reg_bayar();
				
				$lastupdate	= date("Y-m-d H:i:s");
				$user	 	= $this->coms->authenticatedUser->username;
				
								
				for($i=0;$i< count($dosen) ;$i++){
					$prodiid = count($_POST['bayar_detail'.$dosen[$i]]);				
					
					for($j=0;$j<$prodiid;$j++){
						$hrid = $kode.$i.$j;
						
						$data = json_decode($_POST['bayar_detail'.$dosen[$i]][$j]);
						
						if(! empty($data)){
							?>
								<!-- <pre><?php print_r($data) ?></pre> -->
							<?php
							// echo "<hr>";
						}
									
						$mkid	= $data->hidmk;
						$kelas	= $data->hidkelas;
						$gol	= $data->hidgol;
						$namamk	= $data->hidnama;
						$kodemk	= $data->hidkode;
						$sks	= $data->hidsks;
						$hadir	= $data->hidhadir;
						$satuan	= $data->hidsatuan;
						$total	= $data->hidtotal;
						$pph	= $data->hidpph;
						$jumlah	= $data->hidjml;
						$wajib	= $data->hidwajib;
						$potongan	= $data->hidpotongan;
						$bayar	= $data->hidbayar;
						$mgg	= $data->hidmgg;
						$prak	= $data->hidprak;
						
						$datanya = array(
								'hr_id'=>$hrid, 
								'karyawan_id'=>$dosen[$i], 
								'bayar_id'=>$kode, 
								'mkditawarkan_id'=>$mkid, 
								'nama_mk'=>$namamk, 
								'kode_mk'=>$kodemk, 
								'prodi'=>$prodiid[$j], 
								'kelas'=>$kelas,
								'sks'=>$sks, 
								'hadir'=>$hadir, 
								'satuan'=>$satuan, 
								'pph'=>$pph, 
								'kewajiban'=>$wajib, 
								'golongan'=>$gol, 
								'total'=>$total, 
								'total_bayar'=>$jumlah, 
								'jml_mgg'=>$mgg, 
								'praktikum'=>$prak, 
								'potongan'=>$potongan, 
								'is_proses'=>$strproses, 
								'titip_ke'=>$dosen[$i]
						);
						$mconf->replace_data_bayar_detail($datanya);
						
						$total_honor = $total_honor + ($total-$wajib);
						$total_honor_pph = $total_honor_pph + $pph;
						$total_honor_all = $total_honor - $total_honor_pph;
						
					}
					
					$absen= $mconf->get_absen_id($prodi, $mulai, $selesai, $dosen[$i]);
					if($absen):
						foreach($absen as $key):
							$datanya  = array('absendosen_id'=>$key->absendosen_id, 'bayar_id'=>$kode);
							$mconf->replace_data_bayar_absen($datanya);
						endforeach;
					endif;
				}
				
				/*$datanyax = array('bayar_id'=>$kode, 'periode'=>date('Ym'), 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai,'total_pph'=>$_POST['hidtotalpph'], 
								'total_non_pph'=>$_POST['hidtotalnon'], 'total'=>$_POST['hidtotalall'], 
								'tgl_bayar'=>date("Y-m-d"), 'kategori_bayar'=>'hr mengajar', 'user_id'=>$user, 'last_update'=>$lastupdate);*/
								
				$datanya = array('bayar_id'=>$kode, 'periode'=>date('Ym'), 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai,'total_pph'=>$total_honor_pph, 
								'total_non_pph'=>$total_honor, 'total'=>$total_honor_all, 
								'tgl_bayar'=>date("Y-m-d"), 'kategori_bayar'=>'hr mengajar', 'user_id'=>$user, 'last_update'=>$lastupdate,'is_proses'=>$strproses);
				$mconf->replace_data_bayar($datanya);
			endif;
			unset ($_POST['b_proses']);
			
			$this->rekap($prodi, $mulai, $selesai, $kode);
			exit();
		endif;		
	}

	function rekap($prodi=NULL,$mulai=NULL, $selesai=NULL, $kode=NULL, $cetak=NULL, $custom=NULL){
		$mconf = new model_honor();		
		
		$data['prodi'] 	  = $mconf->get_prodi();
		$data['mconf']    = $mconf;
		$data['karyawan_list'] = $mconf->get_karyawan();
		if(isset($_POST['tmulai'])) $tmulai = $_POST['tmulai'];
		else $tmulai = $mulai;
		
		if(isset($_POST['tselesai'])) $tselesai = $_POST['tselesai'];
		else $tselesai = $selesai;
		
		if(isset($_POST['cmbprodi'])) $prodi = $_POST['cmbprodi'];
		else $prodi = $prodi;
		
		
		if($prodi==1) $prodi="";
		if($kode==1) $kode="";
		
		$data['mulai'] = $tmulai;
		$data['selesai'] = $tselesai;
		$data['prodiid'] = $prodi;
		$data['kode']	= $kode;
		
		if($tmulai && $tselesai) {
			$data['posts'] = $mconf->get_rekap_absen_bayar($prodi, $tmulai, $tselesai, $kode);
		}
		
		if(isset($_POST['cetak'])||($cetak)):
			$data['custom'] = $custom;
			switch($cetak){
				case 1 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr.php', $data);
				break;
				case 2 :
					$data['selisih'] = "";
					$data['posts_data'] = $mconf->get_rekap_absen_bayar_cetak_amplop($prodi, $tmulai, $tselesai, $kode);
					$this->view('keuangan/hr/report-hr-amplop.php', $data);
				break;
				case 3 :
					$data['selisih'] = "main";
					$this->view('keuangan/hr/report-hr-spj.php', $data);
				break;
				case 4 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr-excel.php', $data);
				break;
				case 5 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr-mku.php', $data);
				break;
				case 6 :
					$data['selisih'] = "";
					$this->view('keuangan/hr/report-hr-titip.php', $data);
				break;
				case 7 :
					$data['selisih'] = "selisih";
					$this->view('keuangan/hr/report-hr-spj.php', $data);
				break;
				case 8 :
					$data['selisih'] = "selisih";
					$this->view('keuangan/hr/report-hr.php', $data);
				break;
				case 9 :
					$data['selisih'] = "selisih";
					$this->view('keuangan/hr/report-hr-mku.php', $data);
				break;
			}
			
		else:
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			$this->add_script('js/keuangan/jsall.js');	
			$this->add_style('css/calendar/calendar.css');	
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			
			$this->view( 'keuangan/hr/rekap.php', $data );
		endif;
	}

	
}
?>