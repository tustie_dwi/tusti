<?php
class master_general extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
	}
	
	/*--------------------------------------------------------------------- RUANG ----------------------------------------------------------*/
	function ruang($str=NULL, $id=NULL){
		$mruang = new model_ruang();

		$data['posts'] = $mruang->read();
		if($str=='edit') $data['edit']  = $mruang->read($id);
		
		if($str=='delete') $this->delete_ruang($id);
		if($str=='save') $this->save_ruang();

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/conf/ruang.js');

		$this->view('ruang/index.php', $data);
	}
	
	function delete_ruang($id=NULL){		
		$mruang = new model_ruang();
		$mruang->delete($id);
		$this->redirect('module/master/general/ruang');
		exit();
	}
	
	function save_ruang(){
		$mruang= new model_ruang();

		$kode = $_POST['kode'];
		$kapasitas = $_POST['kapasitas'];
		$keterangan = $_POST['keterangan'];
		
		if($_POST['hidId'] != '') $id = $_POST['hidId'];
		else $id = $kode;
		
		$data = Array(
			'ruang_id' => $id,
			'keterangan' => $keterangan,
			'kode_ruang' => $kode,
			'fakultas_id'=> $_POST['cmbfakultas'],
			'cabang_id'=> $_POST['cmbcabang'],
			'kategori_ruang'=> $_POST['cmbkategori'],
			'kapasitas' => $kapasitas
		);
		$mruang->replace_ruang($data);
		$this->redirect('module/master/general/ruang');
		exit();
	}
	/* ---------------------------------------------------------------- END RUANG -----------------------------------------------------*/
	
	/* ---------------------------------------------------------------- FAKULTAS ------------------------------------------------------*/
	function fakultas($str=NULL, $id=NULL){
		$mfak = new model_fakultas();	
		
		$data['posts'] = $mfak->read();
		
		if($str=='edit') $data['edit']  = $mfak->read($id);		
		if($str=='delete') $this->delete_fakultas($id);
		if($str=='save') $this->save_fakultas();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->add_script('js/conf/fakultas.js');
		
		$this->view( 'fakultas/index.php', $data );
	}
	
	function save_fakultas(){
		$mfak	 	= new model_fakultas();
		
		$ceknew 	= $_POST['ceknew'];
		$ketcek		= $mfak->cekketfakultas($_POST['keterangan']);
		$kodecek	= $mfak->cekkodefakultas($_POST['kode_fakultas']);
		$keterangan	= $_POST['keterangan'];	
		
		if($ceknew==1){
			if(isset($ketcek)||isset($kodecek)){
				echo "Duplicate!";
				exit();
			}
		}
		
		if($_POST['hidId']){
			$fakultas_id 	= $_POST['kode_fakultas'];
			$fakultas_id_param = $_POST['hidId'];
			$action 	= "update";
		}else{
			$fakultas_id= $_POST['kode_fakultas'];
			$action 	= "insert";	
		}
		
		//upload image//
		foreach ($_FILES['files'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		$month = date('m');
		$year = date('Y');
		if($cekicon!='error'){
				$name = $_FILES['files']['name'];
				$ext	= $this->get_extension($name);
				$seleksi_ext = $mfak->get_ext($ext);
				if($seleksi_ext!=NULL){
					$jenis_file_id 	= $seleksi_ext->jenis_file_id;
					$jenis			= $seleksi_ext->keterangan;
					$maxfilesize	= $seleksi_ext->max_size;
					
					switch(strToLower($jenis)){
						case 'image':
							$extpath = "image";
						break;
					}
				}
				else{
					echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
					exit();
				}
				
				$allowed_ext = array('jpg','jpeg','png', 'gif');
				
				$icon_size=$_FILES["files"]["size"] ; 
				$ceknamaicon = $mfak->cek_nama_icon($name);
				$upload_dir = 'assets/upload/icon/fakultas/'.$fakultas_id. DIRECTORY_SEPARATOR;
				$upload_dir_db = 'upload/icon/fakultas/'.$fakultas_id. DIRECTORY_SEPARATOR;
				
				// if (!file_exists($upload_dir)) {
						// mkdir($upload_dir, 0777, true);
						// chmod($upload_dir, 0777);
				// }
				$nama_file = $fakultas_id.".".$ext;
				$icon_loc = $upload_dir_db . $nama_file;
				if(!in_array($ext,$allowed_ext)){
					echo "Maaf, tipe file yang di upload salah";
					exit();
				}
				elseif (($ceknamaicon==NULL) && ($icon_size <= $maxfilesize)){
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
						//------UPLOAD USING CURL----------------
						//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
						$url	     = $this->config->file_url;
						$filedata    = $_FILES["files"]['tmp_name'];
						$filename    = $nama_file;
						$filenamenew = $nama_file;
						$filesize    = $file_size;
					
						$headers = array("Content-Type:multipart/form-data");
						$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
						$ch = curl_init();
						$options = array(
							CURLOPT_URL => $url,
							CURLOPT_HEADER => true,
							CURLOPT_POST => 1,
							CURLOPT_HTTPHEADER => $headers,
							CURLOPT_POSTFIELDS => $postfields,
							CURLOPT_INFILESIZE => $filesize,
							CURLOPT_RETURNTRANSFER => true,
							 CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => 2
						); // cURL options 
						curl_setopt_array($ch, $options);
						curl_exec($ch);
						if(!curl_errno($ch))
						{
							$info = curl_getinfo($ch);
							if ($info['http_code'] == 200)
							   $errmsg = "File uploaded successfully";
						}
						else
						{
							$errmsg = curl_error($ch);
						}
						curl_close($ch);
						//------UPLOAD USING CURL----------------
						//rename($_FILES['files']['tmp_name'], $upload_dir . $nama_file);
					}
				}
		}
		else{
			if(isset($_POST['hidimg'])){
				$icon_loc = $_POST['hidimg'];
			}
		}
		
		if(isset($icon_loc)){
			$datanya 	= Array(
							'fakultas_id'=>$fakultas_id, 
							'keterangan'=>$keterangan, 
							'kode_fakultas'=>$fakultas_id,
							'image'=>$icon_loc
							);
			$mfak->replace_fakultas($datanya);
			exit();
		}else{
			$datanya 	= Array(
							'fakultas_id'=>$fakultas_id, 
							'keterangan'=>$keterangan, 
							'kode_fakultas'=>$fakultas_id
							);
			$mfak->replace_fakultas($datanya);
			exit();
		}
	}
	
	/*----------------------------------------------------------------- END FAKULTAS --------------------------------------------------*/
	
	/*----------------------------------------------------------------- UNIT KERJA/LAB/PRODI ------------------------------------------*/
	function unit($str=NULL, $id=NULL){
		$munitkerja = new model_unitkerja();
		
		$data['parent'] 	= $munitkerja->read_parent();
		$data['child'] 		= $munitkerja->read_child();
		$data['fakultas'] 	= $munitkerja->get_fakultas();
		$data['kategori_unit'] 	= $munitkerja->get_kategori_unit();
		
		if($str=='edit'):
			$data['edit']  = $munitkerja->get_unit_kerja($id);	
			$data['unit_kerja'] = $munitkerja->get_unitByUnitId($id);
		endif;
				
		if($str=='delete') $this->delete_unit($id);
		if($str=='save') $this->save_unit();
		
	
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');
		$this->coms->add_script('ckeditor/ckeditor.js');		
		$this->add_script('js/conf/unitkerja.js');	
		
		
		$this->view("unitkerja/index.php", $data);			
	}
	
		
	
	function get_unit(){
		$munit = new model_unitkerja();
		$data  = $munit->get_unitByFak($_POST['fakultasid']);
		if(isset($data)){
			echo "<option value='0'>Pilih Unit Kerja Parent</option>";
			foreach ($data as $key) {
				echo "<option value='" . $key->unit_id . "'>" . $key->keterangan . ' - ' . $key->kode . "</option>";
			}
		}
	}
	
	function delete_unit(){
		$munitkerja = new model_unitkerja();
		$id 		= $_POST['id'];
		$kategori 	= $_POST['kategori'];
		$munitkerja->del_unitkerja($id,"tbl_unit_kerja","unit_id");
		if($kategori=="prodi"){
			$munitkerja->del_unitkerja($id,"tbl_prodi","prodi_id");
		}
		else if($kategori=="fakultas"){
			$munitkerja->del_unitkerja($id,"tbl_fakultas","fakultas_id");
		}
		
		$this->redirect('module/master/general/unit');
	}
	
	function save_unit(){
		$munitkerja = new model_unitkerja();
		$user_id = $this->coms->authenticatedUser->id;
		$last_update = date('Y-m-d H:i:s');
		$this->add_script('js/unitkerja.js');
		
		if($_POST['hidId'] != '') {
			$is_baru = 0;
			$kode = $_POST['hidId'];
			$datanya = array('description'=>$_POST['keterangan']);
			$idnya = array('unit_id'=>$kode);
			$munitkerja->update_unitkerja_coms($datanya, $idnya);
		}
		else {
			$is_baru = 1;
			$kode = $munitkerja->get_unitid();
			$datanya = array('description'=>$_POST['keterangan'], 'unit_id'=>$kode);
			$munitkerja->replace_unitkerja_coms($datanya);
		}
		
		if(isset($_POST['aktif'])&&$_POST['aktif']==1){
			$is_aktif	= 1;
		}
		else{
			$is_aktif	= 0;
		}
		
		if(isset($_POST['hidimg'])&&$_POST['hidimg']!=""){
			$icon_loc = $_POST['hidimg'];
		}else{
			$icon_loc = $this->upload_icon_unit($_FILES['uploads'],$kode, 'icon');
		}
		
		if($_FILES['logo_files']['tmp_name']){
			$logo_loc = $this->upload_icon_unit($_FILES['logo_files'],$kode,'logo');
		}else{
			$logo_loc = $_POST['hidlogo'];
		
		}
		/*if(isset($_POST['hidlogo'])&&$_POST['hidlogo']!=""){
			$logo_loc = $_POST['hidlogo'];
		}else{
			$logo_loc = $this->upload_icon_unit($_FILES['logo_files'],$kode,'logo');
		}
		 echo $_FILES['logo_files']['tmp_name'];
		 exit();*/
		 
		$kategori = $_POST['cmbkategori'];
		$english  = $_POST['coba'];
		
		if($kategori=='prodi'){
			$fakultasid = $_POST['fakultas_id'];
			$datanya = array('prodi_id'=> $kode, 'fakultas_id'=>$_POST['fakultas_id'], 'keterangan'=>$_POST['keterangan'], 'english_version'=>$english, 'kode_prodi'=>$_POST['kode'], 'parent_id'=>$_POST['parent_id']);
			$munitkerja->replace_prodi($datanya);
		}
		else if($kategori=='fakultas'){
			$fakultasid = $kode;
			$datanya = array('fakultas_id'=> $kode, 'keterangan'=>$_POST['keterangan'], 'english_version'=>$english, 'kode_fakultas'=>$_POST['kode'], 'image'=>$icon_loc, 'is_aktif'=>$is_aktif);
			$munitkerja->replace_fakultas($datanya);
		}else{
			$fakultasid = $_POST['fakultas_id'];
		}
		
		if(isset($_POST['parent_id'])){
			$parent = $_POST['parent_id'];
		}
		else{
			$parent = 0;
		}
		
		$data = array(
			'unit_id' => $kode,
			'fakultas_id' => $fakultasid,
			'keterangan' => $_POST['keterangan'],
			'english_version'=>$english,
			'kode' => $_POST['kode'],
			'parent_id' => $parent,
			'cabang_id' => "UBM",
			'kategori' => $kategori,
			'is_aktif' => $is_aktif,
			'strata' => $_POST['strata'],
			'tentang'=>$_POST['desc'],
			'about'=>$_POST['desc_en'],
			'icon'=>$icon_loc,
			'logo'=>$logo_loc,
			'user_id' => $user_id,
			'last_update' => $last_update
		);
		
		
		if($is_baru == 0) {
			if(! $munitkerja->replace_unitkerja($data)) echo "err";
			else echo "ok";
		}
		else {
			if(! $munitkerja->insert_unitkerja($data)) echo "err";
			else echo "ok";
		}
		// $this->redirect('module/master/general/unit/index/ok');
	}

	function upload_icon_unit($form=NULL, $unit=NULL, $cat=NULL){
		$mfak = new model_fakultas();
		
		//upload image//
		foreach ($form as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		$month = date('m');
		$year = date('Y');
		if($cekicon!='error'){
				$name 		 = $form['name'];
				$ext		 = $this->get_extension($name);
				$seleksi_ext = $mfak->get_ext($ext);
				if($seleksi_ext!=NULL){
					$jenis_file_id 	= $seleksi_ext->jenis_file_id;
					$jenis			= $seleksi_ext->keterangan;
					$maxfilesize	= $seleksi_ext->max_size;
					
					switch(strToLower($jenis)){
						case 'image':
							$extpath = "image";
						break;
					}
				}
				else{
					echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
					exit();
				}
				
				$allowed_ext = array('jpg','jpeg','png', 'gif');
				
				$icon_size=$form["size"] ;
				$upload_dir = 'assets/upload/icons/unit-kerja/'.$cat.'/';
				$upload_dir_db = 'upload/icons/unit-kerja/'.$cat.'/';
				
				$nama_file = $unit.".".$ext;
				$icon_loc = $upload_dir_db . $nama_file;
				if(!in_array($ext,$allowed_ext)){
					echo "Maaf, tipe file yang di upload salah";
					exit();
				}
				else{
					// if (!file_exists($upload_dir)) {
						// mkdir($upload_dir, 0777, true);
						// chmod($upload_dir, 0777);
					// }
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
						//------UPLOAD USING CURL----------------
						//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
						$url	     = $this->config->file_url;
						$filedata    = $form['tmp_name'];
						$filename    = $nama_file;
						$filenamenew = $nama_file;
						$filesize    = $icon_size;
						
						
						$headers = array("Content-Type:multipart/form-data");
						$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
						$ch = curl_init();
						$options = array(
							CURLOPT_URL => $url,
							CURLOPT_HEADER => true,
							CURLOPT_POST => 1,
							//CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
							CURLOPT_HTTPHEADER => $headers,
							CURLOPT_POSTFIELDS => $postfields,
							CURLOPT_INFILESIZE => $filesize,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_SSL_VERIFYPEER => false,
							CURLOPT_SSL_VERIFYHOST => 2
						); // cURL options 
						curl_setopt_array($ch, $options);
						curl_exec($ch);
						if(!curl_errno($ch))
						{
							$info = curl_getinfo($ch);
							if ($info['http_code'] == 200)
							   $errmsg = "File uploaded successfully";
						}
						else
						{
							$errmsg = curl_error($ch);
						}
						curl_close($ch);
						// //------UPLOAD USING CURL----------------
						// return $url." == ".$filedata." == ".$filename." == ".$filenamenew." == ".$filesize." == ".$errmsg;
						// rename($form['tmp_name'], $upload_dir . $nama_file);
					}
				}
		}
		else{
			return $icon_loc="";
		}
		
		if($errmsg == "File uploaded successfully"){
			return $icon_loc;
		}
		else{
			return $icon_loc="";
		}
	} 
	
	/*----------------------------------------------------------------- END UNIT KERJA/LAB/PRODI --------------------------------------*/
	
	/* JABATAN */
	function jabatan($str=NULL, $id=NULL){
		$mjabatan = new model_jabatan();	
		
		$data['posts'] = $mjabatan->read('','','parent');
		$data['cmbparent'] = $mjabatan->read();
		$data['panel'] = 'Write Jabatan';
		
		if($str=='edit'){
			$data['edit'] 		= $mjabatan->read($id);
			$data['panel'] 		= 'Edit Jabatan';
		}
	//	if($str=='delete') $this->delete_unit($id);
		if($str=='save') $this->save_jabatan();

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');	
		$this->add_script('js/conf/jabatan.js');
		
		$this->view( 'jabatan/index.php', $data );
	}
	
	
	function child($jabatan_id){
		$mjabatan = new model_jabatan();	
		$data = $mjabatan->read('','','child',$jabatan_id);
		if(isset($data)){
			foreach ($data as $d) {
				echo "<ul>";
				echo "<li>";
				echo "<span><i class='fa fa-minus-square'></i>&nbsp;".$d->keterangan."</span>";
				
				echo '	<div class="btn-group">
						    <button type="button" class="btn btn-default dropdown-toggle btn-edit" data-toggle="dropdown">
						      Action
						      <span class="fa fa-caret-down"></span>
						    </button>
						    <ul class="dropdown-menu">
						      <li><a class="" href="'.$this->location('module/master/general/jabatan/edit/'.$d->jabatan_id).'"><i class="fa fa-edit"></i> Edit</a></li>
						    </ul>
						</div>';
				
				$this->child($d->jabatan_id);
				echo "</li>";
				echo "</ul>";
			}
		}
		
	}
	
	function save_jabatan(){
		$mjabatan = new model_jabatan();	
		
		$keterangan = $_POST['keterangan'];
		
		if(isset($_POST['parent'])&&$_POST['parent']!=''&&$_POST['parent']!='0'){
			$parent = $mjabatan->read($_POST['parent'], 'md5');
		}else $parent = '0';
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$jabatanid = $_POST['hidId'];
		}else $jabatanid = $mjabatan->get_reg_number();
		
		$datanya = array(
						 'jabatan_id'=>$jabatanid,
						 'keterangan'=>$keterangan,
						 'parent_id'=>$parent
				        );
		
		if($mjabatan->replace_jabatan($datanya)){
			echo "Data berhasil disimpan";
		}else{
			echo "Data gagal disimpan";
		}
		
	}
	/* end jabatan */
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
}
?>
	