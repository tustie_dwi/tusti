<?php
class master_reservasi extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mreservasi = new model_reservasi();
		
		$role = $this->coms->authenticatedUser->role;
		
		if($role != 'administrator') :
			$fak = $this->coms->authenticatedUser->fakultas;
			$cabang = $this->coms->authenticatedUser->cabang;
		else :
			$fak = $this->config->fakultas;
			$cabang = $this->config->cabang;
		endif;
		
		$ruang = '';
		$tgl_mulai = $tgl_selesai = date("Y-m-d");
		
		if( isset($_POST['tgl_mulai'])) $tgl_mulai = $_POST['tgl_mulai'];
		if( isset($_POST['tgl_selesai'])) $tgl_selesai = $_POST['tgl_selesai'];
		// if( isset($_POST['ruang'])) $ruang = $_POST['ruang'];
		
		$data['tgl_mulai_select'] = $tgl_mulai;
		$data['tgl_selesai_select'] = $tgl_selesai;
		// $data['jam'] = $mreservasi->get_jam($mreservasi->get_is_pendek());
		// $data['ruang_select'] = $ruang;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->coms->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		
		$this->add_script('js/reservasi/reservasi.js');	
		
		$data['kegiatan'] = $mreservasi->get_kegiatan($tgl_mulai, $tgl_selesai, $ruang);
		$data['ruang'] = $mreservasi->get_ruang($cabang, $fak, $ruang);		
		$data['ruang_all'] = $mreservasi->get_ruang($cabang, $fak, "");	
		$data['jenis_kegiatan'] = $mreservasi->get_jeniskegiatan();
		$this->view('reservasi/order.php', $data);
	}

	function approve(){
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		
		if(isset($_POST['start'])) $tgl_mulai = $_POST['start'];
		else $tgl_mulai = date('Y-m-d');
		
		if(isset($_POST['end'])) $tgl_selesai = $_POST['end'];
		else $tgl_selesai = date('Y-m-d');
		
		$data['tgl_mulai'] = $tgl_mulai;
		$data['tgl_selesai'] = $tgl_selesai;
		
		$mreservasi = new model_reservasi();
		
		$role = $this->coms->authenticatedUser->role;
		if($role =='dosen' || $role == 'mahasiswa') $this->redirect('module/master/reservasi');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/reservasi/reservasi.js');	
		$data['reservasi'] = $mreservasi->get_reservasi($tgl_mulai, $tgl_selesai);
		
		$this->view('reservasi/approve.php', $data);
	}

	function detail_reservasi(){
		$mreservasi = new model_reservasi();
		$data['reservasi'] = $mreservasi->get_reservasi_detail($_POST['id']);
		$data['jadwal'] = $_POST['id'];
		$data['ruang'] = $mreservasi->get_ruangan_detail($_POST['id']);
		$this->view('reservasi/detail.php', $data);
	}
	
	function approval(){
		$mreservasi = new model_reservasi();
		$mreservasi->approve($_POST['jadwal'], $_POST['init']);
	}

	function booking(){
		$mreservasi = new model_reservasi();
		$jadwal_id = $mreservasi->get_jadwal_id();
		$data = array(
			'jadwal_id' => $jadwal_id,
			'kegiatan' => $_POST['kegiatan'],
			'jenis_kegiatan_id' =>$_POST['jenis_kegiatan'],
			'tgl_mulai' => $_POST['tgl_mulai'],
			'tgl_selesai' => $_POST['tgl_selesai'],
			// 'jam_mulai' => $_POST['jam_mulai'],
			// 'jam_selesai' => $_POST['jam_selesai'],
			'catatan' => $_POST['catatan'],
			'is_status' => 'reserved',
			'repeat_on' => 'daily',
			'tahun_akademik' => $mreservasi->get_tahun_akademik(),
			'user_id' => $this->coms->authenticatedUser->id,
			'last_update' => date("Y-m-d H:i:s")
		);
		$mreservasi->save_jadwal($data);
		
		if(isset($_POST['ruang'])) :
			foreach($_POST['ruang'] as $key) {
				$dat = explode("|", $key);
				$data = array(
					'detail_id' => $mreservasi->get_detail_id(),
					'jadwal_id' => $jadwal_id,
					'ruang' => $dat[0],
					'hari' => $this->convert_hari(date("w", strtotime($_POST['tgl_mulai']))),
					'inf_hari' => date("w", strtotime($_POST['tgl_mulai'])),
					'tgl' =>  $_POST['tgl_mulai'],
					'jam_mulai' => $dat[1],
					'jam_selesai' => $dat[2],
					'inf_jenis_kegiatan' =>$_POST['jenis_kegiatan']
				);
				$mreservasi->save_detail($data);
			}
		endif;
		
		$this->redirect('module/master/reservasi');
	}

	function convert_hari($init_hari){
		switch ($init_hari) {
			case '1': return "senin"; break;
			case '2': return "selasa"; break;
			case '3': return "rabu"; break;
			case '4': return "kamis"; break;
			case '5': return "jumat"; break;
			case '6': return "sabtu"; break;
			
			default: return "minggu"; break;
		}
	}
	
	
	
}
	
?>