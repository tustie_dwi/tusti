<?php
class master_penelitian extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$role			= $this->coms->authenticatedUser->role;
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		if($role=="dosen" || $role=="administrator" || $role=="student employee"){
			$mpenelitian 	= new model_penelitian();
			$mgeneral 		= new model_general();
			
			$data['role']	= $role;
			$data['thn']	= $mgeneral->get_semester();
			if($role!="administrator"){
				$data['fakultas']	= $mgeneral->get_fakultas($fakultas);
			}else $data['fakultas']	= $mgeneral->get_fakultas();
			$data['status']		= $mpenelitian->read_status();
			
			if(isset($_POST['fakultas_index'])&&isset($_POST['tahunakademik_index'])&&isset($_POST['kategori_index'])){
				$fakultas_ind			= $_POST['fakultas_index'];
				$tahun_ind				= $_POST['tahunakademik_index'];
				$kategori_ind			= $_POST['kategori_index'];
				$data['fakultas_ind']	= $fakultas_ind;
				$data['thn_ind']		= $tahun_ind;
				$data['kat_ind']		= $kategori_ind;
				$data['posts'] 			= $mpenelitian->read_selection("",$fakultas_ind,$tahun_ind,$kategori_ind);
			}
			else{
				$data['posts'] 			= "";
			}
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			
			$this->add_script('js/penelitian/penelitian.js');
			
			$this->view('penelitian/index.php', $data);
		}
	}
	
	function write(){
		$role			= $this->coms->authenticatedUser->role;
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		if($role=="dosen" || $role=="administrator" || $role=="student employee"){
			$mpenelitian 	= new model_penelitian();
			$mgeneral 		= new model_general();
			$data['param']		= "Tambah Penelitian";
			$data['thn']		= $mgeneral->get_semester();
			$data['posts']		= "";
			$data['cabang']		= $mgeneral->get_cabang();
			if($role!="administrator"){
				$data['fakultas']	= $mgeneral->get_fakultas($fakultas);
			}else $data['fakultas']	= $mgeneral->get_fakultas();
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datetimepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->add_script('js/penelitian/penelitian.js');
			
			$this->view('penelitian/edit.php', $data);
		}
	}

	function edit($id=NULL){
		if(!$id){
			$this->redirect('module/master/penelitian/');
			exit();			
		}
		$role			= $this->coms->authenticatedUser->role;
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		if($role=="dosen" || $role=="administrator" || $role=="student employee"){
			$mpenelitian 	= new model_penelitian();
			$mgeneral 		= new model_general();
			$data['param']		= "Edit Penelitian";
			$data['id']			= $id;
			$data['thn']		= $mgeneral->get_semester();
			$data['posts']		= "";
			$data['cabang']		= $mgeneral->get_cabang();
			if($role!="administrator"){
				$data['fakultas']	= $mgeneral->get_fakultas($fakultas);
			}else $data['fakultas']	= $mgeneral->get_fakultas();
			$data['status']			= $mpenelitian->read_status("",$mpenelitian->read($id,"","","","singlerow")->kategori);
			$data['peserta'] 			= $mpenelitian->read($id,"","","","","","peserta");
			$data['posts_single'] 	= $mpenelitian->read($id,"","","","singlerow");
			$data['posts_edit'] 	= $mpenelitian->read_selection($id);
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datetimepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->add_script('js/penelitian/penelitian.js');
			
			$this->view('penelitian/edit.php', $data);
		}
	}

	function lihat($id=NULL){
		if(!$id){
			$this->redirect('module/master/penelitian/');
			exit();			
		}
		$role			= $this->coms->authenticatedUser->role;
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		if($role=="dosen" || $role=="administrator" || $role=="student employee"){
			$mpenelitian 	= new model_penelitian();
			$mgeneral 		= new model_general();
			$data['id']				= $id;
			$data['status']			= $mpenelitian->read_status("",$mpenelitian->read($id,"","","","singlerow")->kategori);
			$data['peserta'] 		= $mpenelitian->read($id,"","","","","","peserta");
			$data['posts_single'] 	= $mpenelitian->read($id,"","","","singlerow");
			$data['progress'] 		= $mpenelitian->read($id,"","","","","","","","progress");
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datetimepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->add_script('js/penelitian/penelitian.js');
			
			$this->view('penelitian/detail.php', $data);
		}
	}
	
	function hapus(){
		$mpenelitian 	= new model_penelitian();
		$penelitian_id	= $_POST['id'];
		$delete_progres = $mpenelitian->delete_progress($penelitian_id);
		if($delete_progres){
		   $delete_peserta	= $mpenelitian->hapus_peserta("", $penelitian_id);
			if($delete_peserta){
				$delete_publish	= $mpenelitian->delete_publish($penelitian_id);
				if($delete_publish){
					$delete_penelitian = $mpenelitian->delete_penelitian($penelitian_id);
					if($delete_penelitian) echo "Berhasil";
					else echo "Gagal";
				}
			}
		} 
	}
	
	function save_penelitian(){
		$mpenelitian 	= new model_penelitian();
		$mgeneral 		= new model_general();
		$month 					= date('m');
		$year 					= date('Y');
		$fakultasid				= $this->coms->authenticatedUser->fakultas;
		//PENELITIAN
		if(isset($_POST['hidId'])&&$_POST['hidId']!=""){
			$penelitian_id		= $_POST['hidId'];
		}else $penelitian_id	= $mpenelitian->penelitian_id();
		$periode		= $_POST['periode'];
		$kategori		= $_POST['kategori'];
		$thn_akademik	= $_POST['thn_akademik'];
		$judul			= $_POST['judul'];
		$keterangan		= $_POST['keterangan'];
		$sumber_dana	= $_POST['sumber_dana'];
		$status			= $_POST['status'];
		$fakultas		= $_POST['fakultas'];
		$cabang			= $_POST['cabang'];
		$user 			= $this->coms->authenticatedUser->id;
		$last_update	= date("Y-m-d H:i:s");
		
		$isketua		= $_POST['is_ketua'];
		$mhs			= $_POST['mhs'];
		$mhsid			= $_POST['mhs_id'];
		$dosen			= $_POST['dosen'];
		$dosenid		= $_POST['dosen_id'];
		$peserta_id_dosen	= $_POST['peserta_id_dosen'];
		$peserta_id_mhs		= $_POST['peserta_id_mhs'];
		$jml_mhs		= count($mhs);
		$jml_dosen		= count($dosen);
		
		if($_FILES['dokumen_penelitian']['name'] != ""){
			$form_penelitian		= $_FILES['dokumen_penelitian'];
			$dir_penelitian			= 'assets/upload/file/'. $fakultasid . "/penelitian/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
			$dir_penelitian_db		= 'upload/file/'. $fakultasid . "/penelitian/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
			$loc_penelitian			= $this->upload_file($form_penelitian, $dir_penelitian, $dir_penelitian_db);
			if($loc_penelitian!="" || $loc_penelitian!="Maaf, tipe data file yang anda upload tidak diperbolehkan!"){
				$file_loc_penelitian = $loc_penelitian;
			}else $file_loc_penelitian = "";
		}elseif(isset($_POST['file_loc_penelitian'])&&$_POST['file_loc_penelitian']!==""){
			$file_loc_penelitian	= $_POST['file_loc_penelitian'];
		}else $file_loc_penelitian = "";
		
		$data_penelitian	= array(
							  		'penelitian_id'=>$penelitian_id,
							  		'periode'=>$periode,
							  		'kategori'=>$kategori,
							  		'tahun_akademik'=>$thn_akademik,
							  		'file_pendukung'=>$file_loc_penelitian,
							  		'judul'=>$judul,
							  		'keterangan'=>$keterangan,
							  		'inf_sumber_dana'=>$sumber_dana,
							  		'st_publikasi'=>$status,
							  		'fakultas_id'=>$fakultas,
							  		'cabang_id'=>$cabang,
							  		'user_id'=>$user,
							  		'last_update'=>$last_update
							  	   );
		$save_penelitian	= $mpenelitian->replace_penelitian($data_penelitian);
		
		//PENELITIAN-PROGRESS
		if(isset($_POST['progress_id'])&&$_POST['progress_id']!=""){
			$progress_id		= $_POST['progress_id'];
		}else $progress_id		= $mpenelitian->progress_id();
		$tgl_pelaksanaan		= $_POST['tgl_pelaksanaan'];
		$lokasi					= $_POST['lokasi'];
		$sumber_dana_progress	= $_POST['sumber_dana_progress'];
		$jumlah_dana_progress	= $_POST['jumlah_dana_progress'];
		$keterangan_progress	= $_POST['keterangan_progress'];
		$no_dokumen				= $_POST['no_dokumen_progress'];
		
		if($_FILES['dokumen_progress']['name']!=""){
			$form_progress			= $_FILES['dokumen_progress'];
			$dir_progress			= 'assets/upload/file/'. $fakultasid . "/penelitian/" .$year. '_' .$month.  "/progress" .DIRECTORY_SEPARATOR ;
			$dir_progress_db		= 'upload/file/'. $fakultasid . "/penelitian/" .$year. '_' .$month.  "/progress" . DIRECTORY_SEPARATOR ;
			$loc_progress			= $this->upload_file($form_progress, $dir_progress, $dir_progress_db);
			if($loc_progress!="" || $loc_progress!="Maaf, tipe data file yang anda upload tidak diperbolehkan!"){
				$file_loc_progress 	= $loc_progress;
			}else $file_loc_progress = "";
		}elseif(isset($_POST['file_loc_progress'])&&$_POST['file_loc_progress']!==""){
			$file_loc_progress	= $_POST['file_loc_progress'];
		}else $file_loc_progress = "";
		
		$data_progress		= array(
							  		'progress_id'=>$progress_id,
							  		'penelitian_id'=>$penelitian_id,
							  		'status_id'=>$status,
							  		'tgl_pelaksanaan'=>$tgl_pelaksanaan,
							  		'lokasi'=>$lokasi,
							  		'sumber_dana'=>$sumber_dana_progress,
							  		'jumlah_dana'=>floatval($jumlah_dana_progress),
							  		'no_dokumen'=>$no_dokumen,
							  		'dokumen_pendukung'=>$file_loc_progress,
							  		'keterangan'=>$keterangan_progress,
							  		'user_id'=>$user,
							  		'last_update'=>$last_update
							  	   );
		
		//PROGRESS PUBLIKASI
		if(isset($_POST['kategori_publikasi'])){
			if(isset($_POST['publikasi_id'])&&$_POST['publikasi_id']!=""){
				$publikasi_id		= $_POST['publikasi_id'];
			}else $publikasi_id		= $mpenelitian->publikasi_id();
			
			$kategori_publikasi		= $_POST['kategori_publikasi'];
			$tgl_pelaksanaan_seminar= $_POST['tgl_pelaksanaan_seminar'];
			$lokasi_seminar			= $_POST['lokasi_seminar'];
			$jenis_jurnal			= $_POST['jenis_jurnal'];
			$edisi_jurnal			= $_POST['edisi_jurnal'];
			$nama_jurnal			= $_POST['nama_jurnal'];
			$link_jurnal			= $_POST['link_jurnal'];
			$penerbit_buku			= $_POST['penerbit_buku'];
			$isbn_buku				= $_POST['isbn_buku'];
			$judul_buku				= $_POST['judul_buku'];
			
			if($_FILES['file_buku']['name']!=""){
				$form_buku				= $_FILES['file_buku'];
				$dir_buku				= 'assets/upload/file/'. $fakultasid . "/penelitian/" .$year. '_' .$month.  "/publish" .DIRECTORY_SEPARATOR ;
				$dir_buku_db			= 'upload/file/'. $fakultasid . "/penelitian/" .$year. '_' .$month.  "/publish" . DIRECTORY_SEPARATOR ;
				$loc_buku				= $this->upload_file($form_buku, $dir_buku, $dir_buku_db);
				if($loc_buku!="" || $loc_buku!="Maaf, tipe data file yang anda upload tidak diperbolehkan!"){
					$file_loc_buku 	= $loc_buku;
				}else $file_loc_buku = "";
			}elseif(isset($_POST['file_loc_buku'])&&$_POST['file_loc_buku']!==""){
				$file_loc_buku	= $_POST['file_loc_buku'];
			}else $file_loc_buku = "";
		
			$data_publikasi		= array(
							  		'publish_id'=>$publikasi_id,
							  		'penelitian_id'=>$penelitian_id,
							  		'tgl_pelaksanaan'=>$tgl_pelaksanaan_seminar,
							  		'lokasi'=>$lokasi_seminar,
							  		'kategori'=>$kategori_publikasi,
							  		'jurnal_jenis'=>$jenis_jurnal,
							  		'jurnal_edisi'=>$edisi_jurnal,
							  		'jurnal_nama'=>$nama_jurnal,
							  		'jurnal_link'=>$link_jurnal,
							  		'buku_penerbit'=>$penerbit_buku,
							  		'buku_isbn'=>$isbn_buku,
							  		'buku_judul'=>$judul_buku,
							  		'buku_file'=>$file_loc_buku,
							  		'user_id'=>$user,
							  		'last_update'=>$last_update
							  	   );
			if($save_penelitian) $mpenelitian->replace_penelitian_publish($data_publikasi);
		}
		
		if($save_penelitian){
			$mpenelitian->replace_penelitian_progress($data_progress);
			
			$urut=1;
			if($jml_dosen>0){				
				for($i=0;$i<$jml_dosen;$i++){
					$karyawan_id	= $dosenid[$i];
					$nama			= $dosen[$i];
					if(isset($peserta_id_dosen[$i])&&$peserta_id_dosen[$i]!=""){
						$peserta_id		= $peserta_id_dosen[$i];
					}else $peserta_id		= $mpenelitian->peserta_id();
					if(isset($isketua[$i])&&$isketua[$i]==1){
						$ketua		= $isketua[$i];
					}
					else $ketua		= "";
					
					$data_peserta	= array(
											'peserta_id'=>$peserta_id,
											'penelitian_id'=>$penelitian_id,
											'nama'=>$nama,
											'karyawan_id'=>$karyawan_id,
											'mahasiswa_id'=>"",
											'is_ketua'=>$ketua,
											'urut'=>$urut
										   );
					$mpenelitian->replace_penelitian_peserta($data_peserta);
					$urut++;
				}
			}
			if($jml_mhs>0){
				for($j=0;$j<$jml_mhs;$j++){
					$mahasiswa_id	= $mhsid[$j];
					
					if(isset($peserta_id_mhs[$j])&&$peserta_id_mhs[$j]!=""){
						$peserta_id		= $peserta_id_mhs[$j];
						$nama			= $mhs[$j];
					}else {
						$peserta_id	= $mpenelitian->peserta_id();
						$nama			= explode(' - ', $mhs[$j]);
						$nama			= $nama[1];
					}
					$ketua			= "";
					
					$data_peserta	= array(
											'peserta_id'=>$peserta_id,
											'penelitian_id'=>$penelitian_id,
											'nama'=>$nama,
											'karyawan_id'=>"",
											'mahasiswa_id'=>$mahasiswa_id,
											'is_ketua'=>$ketua,
											'urut'=>$urut
										   );
					$mpenelitian->replace_penelitian_peserta($data_peserta);
					$urut++;
				}
			}
		}
		echo "Berhasil!";
	}
	
	//STATUS
	function status($id=NULL){
		$role			= $this->coms->authenticatedUser->role;
		if($role=="student employee" || $role=="administrator"){
			$mpenelitian = new model_penelitian();
			
			$data['param']	= "Tambah Setting Status Penelitian";
			if($id){
				$data['edit']	= $mpenelitian->read_status($id);
			}
			$data['posts']	= $mpenelitian->read_status();
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			
			$this->add_script('js/penelitian/penelitian.js');
			
			$this->view('penelitian/status/index.php', $data);
		}
	}
	
	function save_status(){
		$role			= $this->coms->authenticatedUser->role;
		if($role=="student employee" || $role=="administrator"){
			$mpenelitian = new model_penelitian();
			
			$kategori 	= $_POST['kategori'];
			$keterangan	= $_POST['keterangan'];
			
			if(isset($_POST['hidId'])&&$_POST['hidId']){
				$status_id	= $_POST['hidId'];
			}
			else{
				$status_id	= $mpenelitian->status_id();
			}
			
			$datanya	= array(
							     'status_id'=>$status_id,
							     'keterangan'=>$keterangan,
							     'kategori'=>$kategori
							   );
			$save	= $mpenelitian->replace_status($datanya);
			if($save){
				echo "Berhasil";
			} else echo "Gagal";
		}
	}

	//FUNGSI TAMBAHAN
	function get_status(){
		$mpenelitian 	= new model_penelitian();
		$kategori 		= $_POST['kategori'];
		$data 			= $mpenelitian->read_status("",$kategori);
		echo "<option value='0'>Silahkan Pilih</option>";
		if($data){
			foreach($data as $d){
				echo "<option value='".$d->status_id."' name='".$d->keterangan."'";
				echo ">".$d->keterangan."</option>";
			}
		}
	}
	
	function view_progress_form(){
		$mpenelitian 	= new model_penelitian();
		$data['keterangan']	= $_POST['keterangan'];

		if($_POST['penelitian']!==""){
			$data['progress']	= $mpenelitian->read($_POST['penelitian'],"","","","",$_POST['status_id']);
		}
		
		$this->view('penelitian/progress.php', $data);
	}
	
	function get_publish_data(){
		$mpenelitian 	= new model_penelitian();

		if($_POST['penelitian']!==""){
			$data['publish']	= $mpenelitian->read($_POST['penelitian'],"","","","",$_POST['status_id'],"",$_POST['kategori_publish']);
		}
		else $data['kosong']	= "";
		
		$this->view('penelitian/publikasi.php', $data);
	}
	
	function del_peserta(){
		$mpenelitian 	= new model_penelitian();
		$id		= $_POST['pesertaid'];
		$delete	= $mpenelitian->hapus_peserta($id);
		if($delete){
			echo "Berhasil";
		}
		else echo "Gagal";
	}
	
	function upload_file($form=NULL, $dir=NULL, $dir_db=NULL){
		foreach ($form as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		if($cekicon!='error'){	
			$name 		= $form['name'];
			$ext		= $this->get_extension($name);
			$file_type	= $form["type"]; 
			$file_size	= $form["size"];
			
			$upload_dir = $dir;
			$upload_dir_db 	= $dir_db;
			$allowed_ext 	= array('jpg','jpeg','png', 'gif', 'pdf', 'ppt', 'pptx', 'doc', 'docx');
			if(!in_array($ext,$allowed_ext)){
				echo "Maaf, tipe data file yang anda upload tidak diperbolehkan!";
				exit();
			}
			else{
				if(!file_exists($upload_dir)) {
					mkdir($upload_dir, 0777, true);
					chmod($upload_dir, 0777);
				}
				$file_loc = $upload_dir_db . $name;
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					rename($form['tmp_name'], $upload_dir . $name);
				}
				
				return $file_loc;
			}
		}
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
}
?>