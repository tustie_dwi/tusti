<?php
class master_kerjasama extends comsmodule {
	
	//---kenaikan belum edit--------------------------
	
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mkerjasama = new model_kerjasama();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/kerjasama/kategori.js');	
		
		$data['kategori'] = $mkerjasama->get_kategori();
		
		$this->view('kerjasama/kategori.php', $data);
	}
	
	function save_kategori(){
		$mkerjasama = new model_kerjasama();
		if(empty($_POST['id_kategori'])) :		
			$data = array(
				'kategori_id' => $mkerjasama->get_kategori_id(),
				'keterangan' => $_POST['keterangan']
			);
			$mkerjasama->save_kategori($data);
		else :
			$data = array(
				'keterangan' => $_POST['keterangan']
			);
			$where = array(
				'MID(MD5(tbl_master_kerjasama.kategori_id),8,6)' => $_POST['id_kategori']
			);
			
			$mkerjasama->edit_kategori($data, $where);
		endif;
		$this->redirect('module/master/kerjasama/');
	}
	
	function del_kategori($id){
		$mkerjasama = new model_kerjasama();
		$mkerjasama->del_kategori($id);
		$this->redirect('module/master/kerjasama/');
	}

	//instansi
	function instansi(){
		$mkerjasama = new model_kerjasama();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/kerjasama/instansi.js');	
		
		if(isset($_POST['fakultas'])) $fakultas = $_POST['fakultas'];
		else $fakultas = '';
		
		if(isset($_POST['kategori'])) $kategori = $_POST['kategori'];
		else $kategori = '';
		
		$data['sel_fakultas'] = $fakultas;
		$data['sel_kategori'] = $kategori;
		
		$data['instansi'] = $mkerjasama->get_instansi($fakultas, $kategori);
		$data['kategori'] = $mkerjasama->get_kategori();
		$data['fakultas'] = $mkerjasama->get_fakultas();
		
		$this->view('kerjasama/instansi.php', $data);
	}
	
	function del_instansi($id){
		$mkerjasama = new model_kerjasama();
		$mkerjasama->del_instansi($id);
		$this->redirect('module/master/kerjasama/instansi');
	}
	
	function save_instansi(){
		$mkerjasama = new model_kerjasama();
		
		$instansi_id = $mkerjasama->get_instansi_id();
		if(empty($_POST['id'])) :
			if(! empty($_FILES["logo"]["name"])){
				$logo = $this->upload_instansi(substr(md5($instansi_id),7,6));
			}
			else $logo = '';
			
			$data = array(
				'instansi_id' => $instansi_id,
				'kategori_id' => $_POST['kategori_id'],
				'fakultas_id' => $_POST['fakultas_id'],
				'nama_instansi' => $_POST['nama_instansi'],
				'contact_person' => $_POST['contact_person'],
				'telp' => $_POST['telp'],
				'alamat' => $_POST['alamat'],
				'keterangan' => $_POST['keterangan'],
				'logo' => $logo,
				'user_id' => $this->coms->authenticatedUser->id,
				'last_update' => date('Y-m-d H:I:s')
			);
			$mkerjasama->save_instansi($data);
			// foreach($data as $key => $value) echo $key . " => " . $value . "<br>";
		else :
			$data = array(
				'kategori_id' => $_POST['kategori_id'],
				'fakultas_id' => $_POST['fakultas_id'],
				'nama_instansi' => $_POST['nama_instansi'],
				'contact_person' => $_POST['contact_person'],
				'telp' => $_POST['telp'],
				'alamat' => $_POST['alamat'],
				'keterangan' => $_POST['keterangan'],
				'user_id' => $this->coms->authenticatedUser->id,
				'last_update' => date('Y-m-d H:I:s')
			);
			
			if(! empty($_FILES["logo"]["name"])){
				$logo = $this->upload_instansi($_POST['id']);
				$temp = array('logo' => $logo);
				if(!empty($logo)) $data = $data + $temp;
			}
			$where = array(
				'MID(MD5(tbl_kerjasama_instansi.instansi_id),8,6)' => $_POST['id']
			);
			
			$mkerjasama->edit_instansi($data, $where);
			
		endif;
		$this->redirect('module/master/kerjasama/instansi');
		
	}

	function upload_instansi($instansi_id){
		$maxfilesize = 0;
		$file_ext = $this->get_file_ext($_FILES["logo"]["name"]);
		
		if (strpos("jpg, png, jpeg, gif", strtolower($file_ext)) !== false) $is_file = 1;
		else $is_file = 0;
		
		$file_type=$_FILES["logo"]["type"];
		$file_size=$_FILES["logo"]["size"]; 
		
		$upload_dir = 'assets/upload/file/'. $_POST['fakultas_id'] . DIRECTORY_SEPARATOR . 'kerjasama/instansi'. DIRECTORY_SEPARATOR ;		
		$upload_dir_db = 'upload/file/'. $_POST['fakultas_id'] . DIRECTORY_SEPARATOR . 'kerjasama/instansi'. DIRECTORY_SEPARATOR ;

		if (!file_exists($upload_dir)) {
			mkdir($upload_dir, 0777, true);
			chmod($upload_dir, 0777);
		}
		$file_loc = $upload_dir_db . $instansi_id;
		
		if ($file_size <= 1000000 AND $is_file == 1){
			if(file_exists($upload_dir . $instansi_id)) unlink($upload_dir . $instansi_id);			
			if($_SERVER['REQUEST_METHOD'] == 'POST') {
				//------UPLOAD USING CURL----------------
							//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
							$url	     = $this->config->file_url;
							$filedata    = $_FILES['logo']['tmp_name'];
							$filename    = $instansi_id.".".strtolower($file_ext);
							$filenamenew = $instansi_id.".".strtolower($file_ext);
							$filesize    = $file_size;
						
							$headers = array("Content-Type:multipart/form-data");
							$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
				            $ch = curl_init();
				            $options = array(
				                CURLOPT_URL => $url,
				                CURLOPT_HEADER => true,
				                CURLOPT_POST => 1,
								CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
				                CURLOPT_HTTPHEADER => $headers,
				                CURLOPT_POSTFIELDS => $postfields,
				                CURLOPT_INFILESIZE => $filesize,
				                CURLOPT_RETURNTRANSFER => true,
								CURLOPT_SSL_VERIFYPEER => false,
				  				CURLOPT_SSL_VERIFYHOST => 2
				            ); // cURL options 
							curl_setopt_array($ch, $options);
					        curl_exec($ch);
					        if(!curl_errno($ch))
					        {
					            $info = curl_getinfo($ch);
					            if ($info['http_code'] == 200)
					               $errmsg = "File uploaded successfully";
					        }
					        else
					        {
					            $errmsg = curl_error($ch);
					        }
					        curl_close($ch);
							//------UPLOAD USING CURL----------------
							
				rename($_FILES['logo']['tmp_name'], $upload_dir . $instansi_id);
			}
			return $upload_dir_db.$filename;
		}
		else return '';
	}

	function get_file_ext($name){
		$ext = explode(".", $name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	//dokumen all
	function dokumen(){
		$mkerjasama = new model_kerjasama();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->coms->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		
		$this->add_script('js/kerjasama/dokumen_all.js');	
		
		$data['fakultas'] = $mkerjasama->get_fakultas();
		$this->view('kerjasama/dokumen_all.php', $data);
	}
	
	function get_all_kegiatan(){
		$mkerjasama = new model_kerjasama();
		$data['kegiatan'] = $mkerjasama->get_all_kegiatan($_POST['instansi']);
		$data['instansi'] = $_POST['instansi'];
		
		$this->view('kerjasama/dokumen_all_select.php', $data);
	}
	
	function get_all_data(){
		$mkerjasama = new model_kerjasama();
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->coms->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		
		$temp = explode("|", $_POST['kegiatan']);
		
		if(! isset($temp[1])) :
			$data['dokumen'] = $mkerjasama->get_dokumen($_POST['kegiatan']);
			$data['kegiatan'] = $_POST['kegiatan'];
			$data['detail'] = $mkerjasama->get_detail_kegiatan($_POST['kegiatan']);
		else :
			$data['dokumen'] = $mkerjasama->get_dokumen($temp[0], 1);
			$data['fakultas'] = $mkerjasama->get_fakultas_detail($temp[0]);
			$data['kegiatan'] = $temp[0];
		endif;
		
		$this->view('kerjasama/dokumen_all_data.php', $data);
	}
	
	function get_fakultas_instansi(){
		$mkerjasama = new model_kerjasama();
		$data = $mkerjasama->get_fakultas_instansi();
		
		$instansi;
		if($data){
			foreach($data as $key){
				$temp = array(
					'instansi_id' => $key->instansi_id,
					'instansi' => $key->nama_instansi
				);
				if(isset($instansi[$key->fakultas_id])){
					array_push($instansi[$key->fakultas_id], $temp);
				}
				else{
					$instansi[$key->fakultas_id] = array($temp);
				}
			}
		}
		
		echo json_encode($instansi);
	}

	function get_kegiatan_by_instansi(){
		$mkerjasama = new model_kerjasama();
		$data = $mkerjasama->get_kegiatan_title($_POST['ins']);
		
		if($data){
			echo "<option value='-'>Kosong</option>";
			foreach($data as $key) echo "<option value='".$key->kegiatan_id."'>".$key->judul."</option>";
		}
		else echo "<option value='-'>Kegiatan tidak ditemukan</option>";
	}
	
	//kegiatan
	function kegiatan(){
		$mkerjasama = new model_kerjasama();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->coms->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		
		$this->add_script('js/kerjasama/kegiatan.js');	
		
		$data['ruang'] = $mkerjasama->get_ruangan($this->coms->authenticatedUser->fakultas, $this->coms->authenticatedUser->cabang);
		
		if(isset($_POST['fakultas'])) $fakultas = $_POST['fakultas'];
		else $fakultas = '';
		
		if(isset($_POST['kategori'])) $kategori = $_POST['kategori'];
		else $kategori = '';
		
		$data['sel_fakultas'] = $fakultas;
		$data['sel_kategori'] = $kategori;
		
		$data['instansi'] = $mkerjasama->get_instansi_list();
		$data['kategori'] = $mkerjasama->get_kategori();
		$data['fakultas'] = $mkerjasama->get_fakultas();
		
		$this->view('kerjasama/kegiatan.php', $data);
	}

	function save_kegiatan(){
		$mkerjasama = new model_kerjasama();
		$ruang = '';
		if(isset($_POST['ruang'])){
			foreach($_POST['ruang'] as $dt) $ruang = $ruang . ', '. $dt;
		}
		else $ruang = '';
		$data = array(
			'kegiatan_id' => $mkerjasama->get_kegiatan_id(),
			'instansi_id' => $_POST['instansi_id'],
			'tgl_mulai' => $_POST['tgl_mulai'],
			'tgl_selesai' => $_POST['tgl_selesai'],
			'judul' => $_POST['judul'],
			'keterangan' => $_POST['keterangan'],
			'lokasi' => $_POST['lokasi'],
			'ruang' => substr($ruang,2),
			'user_id' => $this->coms->authenticatedUser->id,
			'last_update' => date('Y-m-d H:I:s')
		);
		
		if(! empty($_POST['id'])){
			$data = array(
				'instansi_id' => $_POST['instansi_id'],
				'tgl_mulai' => $_POST['tgl_mulai'],
				'tgl_selesai' => $_POST['tgl_selesai'],
				'judul' => $_POST['judul'],
				'keterangan' => $_POST['keterangan'],
				'lokasi' => $_POST['lokasi'],
				'ruang' => substr($ruang,2),
				'user_id' => $this->coms->authenticatedUser->id,
				'last_update' => date('Y-m-d H:I:s')
			);
			
			$where = array(
				'MID(MD5(kegiatan_id),8,6)' => $_POST['id']
			);
			// foreach($data as $key => $value) echo $key . ' => ' . $value . '<br>';
			$mkerjasama->edit_kegiatan($data, $where);
		}
		else{
			// foreach($data as $key => $value) echo $key . ' => ' . $value . '<br>';
			$mkerjasama->save_kegiatan($data);
		}
		$this->redirect('module/master/kerjasama/kegiatan');
	}
	
	function get_instansi_by_fakultas(){
		$mkerjasama = new model_kerjasama();
		$data = $mkerjasama->get_instansi_by_fakultas($_POST['fak']);
		
		if($data) :
			foreach($data as $key){
				echo "<option kategori='".$key->kategori_id."' value='".$key->instansi_id."'>".$key->nama_instansi."</option>";
			}
		else :
			echo "<option value=''>-- Data kosong --</option>";
		endif;
	}

	function get_detail_instansi(){
		$mkerjasama = new model_kerjasama();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->add_script('js/kerjasama/kategori.js');	
		
		$data['kegiatan'] = $mkerjasama->get_kegiatan($_POST['ins']);
		
		$this->view('kerjasama/kegiatan-detail.php', $data);
	}

	function del_kegiatan($id){
		$mkerjasama = new model_kerjasama();
		$mkerjasama->del_kegiatan($id);
		$this->redirect('module/master/kerjasama/kegiatan');
	}
	
	//dokumen
	function upload_file($kegiatan){
		$mkerjasama = new model_kerjasama();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->add_script('js/select/select2.js');
		$this->add_style('js/select/select2.css');
		
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.min.css');
		$this->coms->add_script('js/bootstrap-timepicker.min.js'); //timepicker
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		
		//$this->add_script('js/kerjasama/kegiatan.js');	
		
		$data['dokumen'] = $mkerjasama->get_dokumen($kegiatan);
		$data['kegiatan'] = $kegiatan;
		$data['detail'] = $mkerjasama->get_detail_kegiatan($kegiatan);
		
		$this->view('kerjasama/dokumen.php', $data);
	}
	
	function del_file($kategori, $file){
		$mkerjasama = new model_kerjasama();
		$mkerjasama->del_file($file);
		$this->redirect('module/master/kerjasama/upload_file/'.$kategori);
	}

	function upload_dokumen($is_global=0){
		if(isset($_FILES["dokumen"]['name'])){
			$mkerjasama = new model_kerjasama();
			foreach($_FILES["dokumen"]['name'] as $id => $name){
				if(! empty($name)){
					$maxfilesize = 0;
					$file_ext = $this->get_file_ext($name);
					$init_file = $mkerjasama->cek_file_type($file_ext);
					
					if(isset($init_file->max_size)){
						$maxfilesize = $init_file->max_size;
						$jenis_file_id = $init_file->jenis_file_id;
					}
					
					$file = "";
					$file_type=$_FILES["dokumen"]["type"][$id] ; 
					$file_size=$_FILES["dokumen"]["size"][$id] ; 
					
					$file_tmp_name = $_FILES['dokumen']['tmp_name'][$id];
					$year = date('Y');
					$month = date('m');
					$upload_dir = 'assets/upload/file/'. $_POST['fakultas'] . DIRECTORY_SEPARATOR . 'kerjasama'. DIRECTORY_SEPARATOR .$year. '-' .$month . DIRECTORY_SEPARATOR ;		
					$upload_dir_db = 'upload/file/'. $_POST['fakultas'] . DIRECTORY_SEPARATOR . 'kerjasama'. DIRECTORY_SEPARATOR .$year. '-' .$month . DIRECTORY_SEPARATOR ;
					
					
					if (!file_exists($upload_dir)) {
						mkdir($upload_dir, 0777, true);
						chmod($upload_dir, 0777);
					}
					$file_loc = $upload_dir_db . $name;
					
					echo $init_file->keterangan;
					if ($file_size <= $maxfilesize AND isset($init_file->max_size)){
						if(file_exists($upload_dir . $name)) unlink($upload_dir . $name);			
						// if($_SERVER['REQUEST_METHOD'] == 'POST') {
							// rename($_FILES['dokumen']['tmp_name'][$id], $upload_dir . $name);
						// }
						
						if($_SERVER['REQUEST_METHOD'] == 'POST') {
							//------UPLOAD USING CURL----------------
							//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
							$url	     = $this->config->file_url;
							$filedata    = $file_tmp_name;
							$filename    = $name;
							$filenamenew = $name;
							$filesize    = $file_size;
						
							$headers = array("Content-Type:multipart/form-data");
							$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
				            $ch = curl_init();
				            $options = array(
				                 CURLOPT_URL => $url,
								CURLOPT_HEADER => true,
								CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
								CURLOPT_POST => 1,
								CURLOPT_HTTPHEADER => $headers,
								CURLOPT_POSTFIELDS => $postfields,
								CURLOPT_INFILESIZE => $filesize,
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_SSL_VERIFYPEER => false,
								CURLOPT_SSL_VERIFYHOST => 2
				            ); // cURL options 
							curl_setopt_array($ch, $options);
					        curl_exec($ch);
					        if(!curl_errno($ch))
					        {
					            $info = curl_getinfo($ch);
					            if ($info['http_code'] == 200)
					               $errmsg = "File uploaded successfully";
					        }
					        else
					        {
					            $errmsg = curl_error($ch);
					        }
					        curl_close($ch);
							//------UPLOAD USING CURL----------------
							
							rename($file_tmp_name, $upload_dir . $name);
						}
						
						$data = Array(
							'dokumen_id' => $mkerjasama->get_dokumen_id(),
						    'kegiatan_id'=> str_replace("-", "", $_POST['kegiatan_id']),
						    'instansi_id'=>$_POST['instansi_id'],
						    'judul'=>$_POST['judul'],
						    'keterangan'=>$_POST['keterangan'],
						    'file_name'=>$name,
						    'file_loc'=>$file_loc,
						    'file_size'=>$file_size,
						    'file_type'=>$file_type,
						    'jenis_file'=>$init_file->keterangan,
						    'user_id'=>$this->coms->authenticatedUser->id,
						    'last_update'=>date('Y-m-d H:i:s')
						);
						
						$mkerjasama->save_dokumen($data);
						
						// foreach ($data as $key => $value) {
							// echo $key . ' => ' . $value . '<br>';
						// }
					} //end id isempty
				}
			} //end of foreach
		}
		
		if($is_global == 0) $this->redirect('module/master/kerjasama/upload_file/'.substr(md5($_POST['kegiatan_id']),7,6));
		else $this->redirect('module/master/kerjasama/dokumen');	
}








	
}
?>