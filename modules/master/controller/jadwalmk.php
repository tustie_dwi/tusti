<?php
class master_jadwalmk extends comsmodule {
	private $coms;
	
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mjadwal = new model_jadwalmk();
		$mconf = new model_conf();
		$user = $this->coms->authenticatedUser->role;
		
		$data['user']			= $user;
		$data['prodi']			= $mjadwal->get_prodi();
		$data['matakuliah']		= $mjadwal->get_namamk();
		$data['pengampu']		= $mjadwal->get_pengampuindex("");
		$data['kelas']			= $mjadwal->get_allkelas();
		$data['thnakademik']	= $mjadwal->get_all_thn_akademik();
		$data['get_fakultas']	= $mconf->get_fakultas();
		$data['cabang']			= $mconf->get_cabangub();
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		/*-----------------------type---------------------------------*/
		if (isset($_POST['type'])&&$_POST['type']!="0") {
			if($_POST['type']=='course'){
				$typeid 		= '0';
			}
			elseif($_POST['type']=='online'){
				$typeid 		= '1';
			}
			else {
				$typeid 		= '';
			}
			$data['type']	= $_POST['type'];
		} else {
			$typeid = null;
		}
		/*-----------------------fakultas---------------------------*/
		if (isset($_POST['fakultas'])&&$_POST['fakultas']!="0") {
			$fakultasid 	= $_POST['fakultas'];
			$data['fakultasid']=$fakultasid;
		} else {
			$fakultasid = null;
		}
		/*-----------------------thnakademik---------------------------*/
		if (isset($_POST['thnakademik'])&&$_POST['thnakademik']!="0") {
			$thnakademikid 	= $_POST['thnakademik'];
			$data['thnakademikid']=$thnakademikid;
		} else {
			$thnakademikid = null;
		}
		/*-----------------------prodi-------------------------------*/
		if (isset($_POST['prodi'])&&$_POST['prodi']!="0") {
			$prodiid = $_POST['prodi'];
			$data['prodiid']=$prodiid;
		} else {
			$prodiid = null;
		}
		/*-----------------------mk----------------------------------*/
		if (isset($_POST['mtk'])&&$_POST['mtk']!="0") {
			$mkid = $_POST['mtk'];
			$data['matakuliahid']=$mkid;
		} else {
			$mkid = null;
		}
		/*-----------------------pengampu----------------------------*/
		if (isset($_POST['pengampu'])&&$_POST['pengampu']!="0") {
			$pengampuid = $_POST['pengampu'];
			$data['pengampuid']=$pengampuid;
		} else {
			$pengampuid = null;
		}
		/*-----------------------kelas--------------------------------*/
		if (isset($_POST['kelas'])&&$_POST['kelas']!="0") {
			$kelasid 			= $_POST['kelas'];
			$data['kelasid']	= $kelasid;
		} else {
			$kelasid = null;
		}
		/*-----------------------cabang--------------------------------*/
		if (isset($_POST['cabang'])&&$_POST['cabang']!="0") {
			$cabangid 			= $_POST['cabang'];
			$data['cabangid']	= $cabangid;
		} else {
			$cabangid = null;
		}
		
		$read 	= $mjadwal->read_by_parameter($prodiid, $mkid, $pengampuid, $kelasid, $thnakademikid, $typeid, $fakultasid, $cabangid);
		/*--------------------------------------------------------------*/	
		if($read!=null){
			$data['posts'] = $read;
		}
		else {
			$data['posts'] = null;
		}
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/jadwalmk/jadwalmk_index.js');	
		// $this->add_script('js/jadwalmk/delete.js');
		
		$this->view('jadwalmk/index.php', $data);		
		
	}
	
	function excel($thn = null, $prodi=null, $mk=null, $pengampu=null, $kelas=null){
		$mjadwal = new model_jadwalmk();
		$fakultas = $this->coms->authenticatedUser->fakultas;
		$thnakademik = $mjadwal->get_tahun_akademik();
		$thnakademik = $thnakademik[0]->tahun_akademik;
		/** Error reporting */
		error_reporting(E_ALL);
		
		/** PHPExcel */
		require ('library/phpexcel/PHPExcel.php');
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$styleHeaderArray = array(
	    'font'  => array(
	        'bold'  => true
	        // 'color' => array('rgb' => 'FF0000'),
	        // 'size'  => 15,
	        // 'name'  => 'Verdana'
	    ));
		
		/*
		 * 
		 * SHEET 1
		 * 
		 * */
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'English version')
		            ->setCellValue('B1', 'Nama Mata Kuliah')
		            ->setCellValue('C1', 'Kode')
		            ->setCellValue('D1', 'SKS')
					->setCellValue('E1', 'Kurikulum')
					->setCellValue('F1', 'Kelas')
					->setCellValue('G1', 'Prodi')
					->setCellValue('H1', 'Ruang')
					->setCellValue('I1', 'Jam Mulai')
					->setCellValue('J1', 'Jam Selesai')
					->setCellValue('K1', 'Hari')
					->setCellValue('L1', 'Praktikum')
					->setCellValue('M1', 'Dosen');
		
		$i=2;
		$data = $mjadwal->get_excel_data($fakultas,$thnakademik);
		if($thn) $data = $mjadwal->get_excel_data($fakultas,$thn);
		if($thn && $prodi) $data = $mjadwal->get_excel_data($fakultas,$thn,$prodi);
		if($thn && $prodi && $mk) $data = $mjadwal->get_excel_data($fakultas,$thn,$prodi,$mk);
		if($thn && $prodi && $mk && $pengampu) $data = $mjadwal->get_excel_data($fakultas,$thn,$prodi,$mk,$pengampu);
		if($thn && $prodi && $mk && $pengampu && $kelas) $data = $mjadwal->get_excel_data($fakultas,$thn,$prodi,$mk,$pengampu,$kelas);
		
		$index_= rand(0,count($data));
		if($data)
		foreach ($data as $key => $value) {
			$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A'.$i, $value->english_version)
		            ->setCellValue('B'.$i, $value->namamk)
		            ->setCellValue('C'.$i, $value->kode_mk)
		            ->setCellValue('D'.$i, $value->sks)
					->setCellValue('E'.$i, $value->kurikulum)
					->setCellValue('F'.$i, $value->kelas)
					->setCellValue('G'.$i, $value->prodi_id)
					->setCellValue('H'.$i, $value->ruang_id)
					->setCellValue('I'.$i, $value->jam_mulai)
					->setCellValue('J'.$i, $value->jam_selesai)
					->setCellValue('K'.$i, $value->hari)
					->setCellValue('L'.$i, $value->is_praktikum)
					->setCellValue('M'.$i, $value->dosen);
			$i++;
		}
		
		// $objPHPExcel->setActiveSheetIndex(0)
		            // ->setCellValue('A'.$i, $data[$index_]->english_version)
		            // ->setCellValue('B'.$i, $data[$index_]->namamk)
		            // ->setCellValue('C'.$i, $data[$index_]->kode_mk)
		            // ->setCellValue('D'.$i, $data[$index_]->sks)
					// ->setCellValue('E'.$i, $data[$index_]->kurikulum)
					// ->setCellValue('F'.$i, $data[$index_]->kelas)
					// ->setCellValue('G'.$i, $data[$index_]->prodi_id)
					// ->setCellValue('H'.$i, $data[$index_]->ruang_id)
					// ->setCellValue('I'.$i, $data[$index_]->jam_mulai)
					// ->setCellValue('J'.$i, $data[$index_]->jam_selesai)
					// ->setCellValue('K'.$i, $data[$index_]->hari)
					// ->setCellValue('L'.$i, $data[$index_]->is_praktikum)
					// ->setCellValue('M'.$i, $data[$index_]->dosen);
		// foreach ($data as $d) {
			// $objPHPExcel->setActiveSheetIndex(0)
		            // ->setCellValue('A'.$i, $d->english_version)
		            // ->setCellValue('B'.$i, $d->namamk)
		            // ->setCellValue('C'.$i, $d->kode_mk)
		            // ->setCellValue('D'.$i, $d->sks)
					// ->setCellValue('E'.$i, $d->kurikulum)
					// ->setCellValue('F'.$i, $d->kelas)
					// ->setCellValue('G'.$i, $d->prodi_id)
					// ->setCellValue('H'.$i, $d->ruang_id)
					// ->setCellValue('I'.$i, $d->jam_mulai)
					// ->setCellValue('J'.$i, $d->jam_selesai)
					// ->setCellValue('K'.$i, $d->hari)
					// ->setCellValue('L'.$i, $d->is_praktikum)
					// ->setCellValue('M'.$i, $d->dosen);
			// $i++;
		// }
		
		for($col = 'A'; $col !== 'N'; $col++) {
		    $objPHPExcel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($col.'1')->applyFromArray($styleHeaderArray);
		}

		// Rename existing worksheet with index 0 (created when creating a new workbook)
		$objPHPExcel->getSheet(0)->setTitle('Jadwal');
		/*
		 * 
		 * END SHEET 1
		 * 
		 * */
		
		/*
		 * 
		 * SHEET 2
		 * 
		 * */
		// Create a new worksheet
		$DosenSheet = new PHPExcel_Worksheet();
		// Rename sheet
		$DosenSheet->setTitle('Dosen');
		// Add new sheet to workbook at index 1
		$objPHPExcel->addSheet($DosenSheet,1); 
		$objPHPExcel->setActiveSheetIndex(1)
		            ->setCellValue('A1', 'Karyawan ID')
		            ->setCellValue('B1', 'Nama');
		
		$i=2;
		$dosen = $mjadwal->get_excel_dosen($fakultas);
		
		foreach ($dosen as $d) {
			$objPHPExcel->setActiveSheetIndex(1)
		            ->setCellValue('A'.$i, $d->karyawan_id)
		            ->setCellValue('B'.$i, $d->nama);
					
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()
				->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
					
			$i++;
		}
		
		for($col = 'A'; $col !== 'C'; $col++) {
		    $objPHPExcel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($col.'1')->applyFromArray($styleHeaderArray);
		}
		/*
		 * 
		 * END SHEET 2
		 * 
		 * */
		
		/*
		 * 
		 * SHEET 3
		 * 
		 * */
		// Create a new worksheet
		$MkSheet = new PHPExcel_Worksheet();
		// Rename sheet
		$MkSheet->setTitle('Mata Kuliah');
		// Add new sheet to workbook at index 1
		$objPHPExcel->addSheet($MkSheet,2); 
		$objPHPExcel->setActiveSheetIndex(2)
		            ->setCellValue('A1', 'Mata Kuliah')
		            ->setCellValue('B1', 'English Version');
		
		$i=2;
		$mk = $mjadwal->get_excel_mk($fakultas);
		
		foreach ($mk as $d) {
			$objPHPExcel->setActiveSheetIndex(2)
		            ->setCellValue('A'.$i, $d->namamk)
		            ->setCellValue('B'.$i, $d->english_version);
					
			$i++;
		}
		
		for($col = 'A'; $col !== 'C'; $col++) {
		    $objPHPExcel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($col.'1')->applyFromArray($styleHeaderArray);
		}
		/*
		 * 
		 * END SHEET 3
		 * 
		 * */
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		/*
		 * SAVE TO EXCEL 2005 IF NEEDED
		 * */
		// Redirect output to a client’s web browser (Excel5)
		// header('Content-Type: application/vnd.ms-excel');
		// header('Content-Disposition: attachment;filename="Jadwal-'.$fakultas.'.xls"');
		// header('Cache-Control: max-age=0');
// 		
		// // Redirect output to a client’s web browser (Excel5)
		// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
// 		
		// $objWriter->save('php://output');
		/*
		 * SAVE TO EXCEL 2005 IF NEEDED
		 * */
		
		// Save Excel 2007 file
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Jadwal-'.$fakultas.'.xlsx"');
		header('Cache-Control: max-age=0');
		
		// Redirect output to a client’s web browser (Excel5)
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		$objWriter->save('php://output');
		
		exit;
	}
	
	function import(){
		$mjadwal = new model_jadwalmk();
		$fakultas = $this->coms->authenticatedUser->fakultas;
		$thnakademik = $mjadwal->get_tahun_akademik();
		$thnakademik = $thnakademik[0]->tahun_akademik;
		
		$file 		= $_FILES['excel-import'];
		
		/** Error reporting */
		error_reporting(E_ALL);
		
		/** PHPExcel */
		require ('library/phpexcel/PHPExcel.php');
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		$path = $file['tmp_name'];
        $objPHPExcel = PHPExcel_IOFactory::load($path);
		
		 //Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        // Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; $row++){ 
                //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);
				
				$eng_ver = $rowData[0][0];
				$namamk = $rowData[0][1];
				$kode = $rowData[0][2];
				$sks = $rowData[0][3];
				$kurikulum = $rowData[0][4];
				$kelas = $rowData[0][5];
				$prodi = $rowData[0][6];
				$ruang = $rowData[0][7];
				$jam_mulai = $rowData[0][8];
				$jam_selesai = $rowData[0][9];
				$hari = $rowData[0][10];
				
				if($rowData[0][11]=='Praktikum'){
					$praktikum = '1';
				}else $praktikum = '0';
				
				$dosen = $rowData[0][12];
				
                //Insert into database                
				$check_namamk = $mjadwal->ceknamamk_namamk($namamk);
				if(isset($check_namamk)){
					$namamkid = $check_namamk;
				}else $namamkid = $this->insert_into_tbl_namamk($namamk);
				
				$cek_matakuliah = $mjadwal->ceknamamk_matakuliah($namamk);
				if(isset($cek_matakuliah)){
					$matakuliahid = $cek_matakuliah;
				}else $matakuliahid = $this->insert_into_tbl_matakuliah($namamkid,$kode,$sks,$kurikulum);
				
				$cek_mkditawarkan = $mjadwal->cek_mkditawarkan_by_namamk($namamk, $thnakademik, $praktikum);
				if(isset($cek_mkditawarkan)){
					$mkditawarkanid = $cek_mkditawarkan;
				}else $mkditawarkanid = $this->insert_into_tbl_mkditawarkan($matakuliahid,$thnakademik,$praktikum);
				
				$karyawan_id = $mjadwal->get_karyawanid($dosen);
				
				$cek_pengampu_id = $mjadwal->get_pengampu_excel($karyawan_id,$mkditawarkanid);
				if(isset($cek_pengampu_id)){
					$pengampu_id = $cek_pengampu_id;
				}else $pengampu_id = $this->insert_pengampu($karyawan_id,$mkditawarkanid);
				
				$cek_jadwal = $mjadwal->cek_jadwalmk($kelas,$pengampu_id,$mkditawarkanid,$prodi,$ruang,$jam_mulai,$jam_selesai,$hari);
				if(isset($cek_jadwal)){
					$jadwalid = $cek_jadwal;
				}else{
					$jadwalid = $mjadwal->get_reg_number($mkditawarkanid, $pengampu_id);
				}
				$this->insert_into_tbl_jadwalmk($jadwalid,$kelas,$pengampu_id,$mkditawarkanid,$prodi,$ruang,$jam_mulai,$jam_selesai,$hari);
				
      	} //--end for
		// echo $row;
		echo "Import Sukses!";
	}
	
	function insert_into_tbl_namamk($namamk){
		$mmk	= new model_mk();
		//------------------insert into namamk------------------------------
		$namamk_id	= $mmk->get_namamk_reg_number();						//---add jika data pada tabel namamk tidak ada
		$fakultas = $this->coms->authenticatedUser->fakultas;
		$keterangan = $namamk;
		$eng_ver	= "";
		
		$datanya 	= Array(
					'namamk_id'=>$namamk_id, 
					'fakultas_id'=>$fakultas, 
					'keterangan'=>$keterangan, 
					'english_version'=>$eng_ver
					);
		$mmk->replace_namamk($datanya);
		return $namamk_id;
	}
	
	function insert_into_tbl_matakuliah($namamkid,$kode,$sks,$kurikulum){
		$mmk	= new model_mk();
		//------------------insert into matakuliah------------------------------
		$matakuliah_id	= $mmk->get_reg_numbermk();	
		$namamkid		= $namamkid;
		$kodemk			= $kode;
		$kurikulum		= $kurikulum;
		$sks			= $sks;
		
		$datanya 	= Array(
							'matakuliah_id'=>$matakuliah_id, 
							'namamk_id'=>$namamkid, 
							'kode_mk'=>$kodemk, 
							'kurikulum'=>$kurikulum,
							'sks'=>$sks
							);
		$mmk->replace_matakuliah($datanya);	
		return $matakuliah_id;
	}
	
	function insert_into_tbl_mkditawarkan($matakuliahid,$thnakademik,$praktikum){
		$mmk	= new model_mk();
		$mkditawarkan_id	= $mmk->get_mkditawarkan_reg_number();
		$thnakademik		= $thnakademik;
		$matakuliahid		= $matakuliahid;
		$ispraktikum		= $praktikum;
		$parent_id			= "";
		$user			= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		
		$datanya 	= Array(
							'mkditawarkan_id'=>$mkditawarkan_id, 
							'tahun_akademik'=>$thnakademik, 
							'matakuliah_id'=>$matakuliahid, 
							'is_praktikum'=>$ispraktikum,
							'user_id'=>$user,
							'last_update'=>$lastupdate
							);
		$mmk->replace_mkditawarkan($datanya);
		return $mkditawarkan_id;
	}
	
	function insert_pengampu($karyawan_id,$mkditawarkanid){
		$mpngm	= new model_pengampu();
		$karyawanid		= $karyawan_id;
		$pengampu_id	= $mpngm->get_reg_number($mkditawarkanid, $karyawan_id);
		$iskoor			= "";
		
		$datapengampu 	= Array(
							'pengampu_id'=>$pengampu_id, 
							'mkditawarkan_id'=>$mkditawarkanid, 
							'karyawan_id'=>$karyawanid, 
							'is_koordinator'=>$iskoor
							);
		$mpngm->replace_pengampu($datapengampu);
		return $pengampu_id;
	}
	
	function insert_into_tbl_jadwalmk($jadwalid,$kelas,$dosenpengampu,$mkditawarkan,$prodi,$ruang,$jam_mulai,$jam_selesai,$hari){
		$mjadwal = new model_jadwalmk();
		$user			= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		$datanya 	= Array(
							'jadwal_id'=>$jadwalid, 
							'prodi_id'=>$prodi, 
							'pengampu_id'=>$dosenpengampu,
							'mkditawarkan_id'=>$mkditawarkan,
							'kelas'=>$kelas,
							'ruang_id'=>$ruang,
							'hari'=>$hari,
							'jam_mulai'=>$jam_mulai,
							'jam_selesai'=>$jam_selesai,
							'user_id'=>$user,
							'last_update'=>$lastupdate,
							);
		$mjadwal->replace_jadwalmk($datanya);
	}
	
	function write($id){
		$mjadwal 	= new model_jadwalmk();	
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 			= "";
		$data['read']			= $mjadwal->get_data($id);
		$data['datapengampu']	= $mjadwal->get_pengampu($id);
		$data['prodi']			= $mjadwal->get_prodi();	
		$data['fakultas']		= $mconf->get_fakultas();
		$data['thnakademik']	= $mjadwal->get_tahun_akademik();
		$data['write']			= '1';
		$data['cekonline']		= $mjadwal->get_isonline_from_mkditawarkan($id);
		$data['cabang']			= $mconf->get_cabangub();
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jadwalmk/jadwalmk.js');
		$this->add_script('js/jquery/jquery-ui-timepicker-addon.js');
		$this->add_script('js/jquery/timepicker.js');

		$this->view( 'jadwalmk/edit.php', $data );
		}

	}

	function writenew(){
		$mjadwal = new model_jadwalmk();	
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 			= "";
		$data['prodi']			= $mjadwal->get_prodi();	
		$data['fakultas']		= $mconf->get_fakultas();
		$data['thnakademik']	= $mjadwal->get_tahun_akademik();
		$data['write']			= '1';
		$data['cabang']			= $mconf->get_cabangub();
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jquery/jquery-ui-timepicker-addon.js');
		$this->add_script('js/jquery/timepicker.js');
		$this->add_script('js/jadwalmk/jadwalmk_addnew.js');
		
		$this->view( 'jadwalmk/writenew.php', $data );
		}

	}
	
	//----------------------------------------------------------------------------------------
	
	public function edit($id){
		$mjadwal = new model_jadwalmk();
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 			= $mjadwal->read($id);	
		$data['prodi']			= $mjadwal->get_prodi();
		$data['fakultas']		= $mconf->get_fakultas();
		$data['thnakademik']	= $mjadwal->get_tahun_akademik();
		$data['datapengampu']	= $mjadwal->get_pengampubymk($id);
		$data['edit']			= '1';
		$mkditawarkan			= $mjadwal->getmkditawarkan_byjadwalid($id);
		$data['cekonline']		= $mjadwal->get_isonline_from_mkditawarkan($mkditawarkan);
		$data['cabang']			= $mconf->get_cabangub();
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jquery/jquery-ui-timepicker-addon.js');
		$this->add_script('js/jquery/timepicker.js');
		$this->add_script('js/jadwalmk/jadwalmk.js');
			
		$this->view( 'jadwalmk/edit.php', $data );
		}
	}

	//----------------------------------------------------------------------------------------
	
	public function detail($id)
	{
		$mjadwal = new model_jadwalmk();	
		$user = $this->coms->authenticatedUser->role;
		
		$data['posts']=$mjadwal->read($id);
		$data['user']=$user;
		$mkditawarkan			= $mjadwal->getmkditawarkan_byjadwalid($id);
		$data['cekonline']		= $mjadwal->get_isonline_from_mkditawarkan($mkditawarkan);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
						
		$this->view( 'jadwalmk/detail.php', $data );
	}	
	
	//----------------------------------------------------------------------------------------
	
	public function delete()
	{
		$mjadwal = new model_jadwalmk();	
		// $user = $this->coms->authenticatedUser->role;
		
		 if(isset($_POST['delete_id']) && !empty($_POST['delete_id']) ) {
		 	$delete_id = $_POST['delete_id'];
			$result = $mjadwal->delete($delete_id);
			if($result==TRUE)
			{
				echo "true";
			}
		 }
	}	
	
	//----------------------------------------------------------------------------------------
	function save(){
			if(isset($_POST['b_jadwal'], $_POST['prodi'])){
				$this->saveToDB();
				exit();
			}else{
				$this->index();
				exit;
			}
	}

	function saveToDB(){
		ob_start();
		$mjadwal = new model_jadwalmk();
		$mmk	= new model_mk();
		$mpngm	= new model_pengampu();
		
		$hid_id 		= $_POST['hidId'];
		$user			= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		$cabang 		= $_POST['cabang'];
		
		//-------------------cek mkditawarkanid and insert if there isnt any-----------------------
		$namamk			= $mjadwal->ceknamamk_mkditawarkan($_POST['namamk']); 	//---get mkditawarkan_id
		if(!isset($namamk)){													//---cek mkditawarkan_id
			
			$cek_matakuliah = $mjadwal->ceknamamk_matakuliah($_POST['addnamamk']); //---get matakuliah_id
			if(!isset($cek_matakuliah)){										//---cek matakuliah_id
			
				$cek_namamk		= $mjadwal->ceknamamk_namamk($_POST['addnamamk']);	//---get namamk_id
				if(!isset($cek_namamk)){										//---cek namamk_id
				
				//------------------insert into namamk------------------------------
					$namamk_id	= $mmk->get_namamk_reg_number();						//---add jika data pada tabel namamk tidak ada
					$fakultas	= $mmk->get_fakultas_id($_POST['fakultas']);
					$keterangan = $_POST['addnamamk'];
					$eng_ver	= "";
					
					$datanya 	= Array(
								'namamk_id'=>$namamk_id, 
								'fakultas_id'=>$fakultas, 
								'keterangan'=>$keterangan, 
								'english_version'=>$eng_ver
								);
					$mmk->replace_namamk($datanya);
					// echo "insert to namamk";
				}
				
				//------------------insert into matakuliah------------------------------
				$matakuliah_id	= $mmk->get_reg_numbermk();	
				$namamkid		= $mjadwal->ceknamamk_namamk($_POST['addnamamk']);
				$kodemk			= "";
				$kurikulum		= "";
				$sks			= "";
				
				$datanya 	= Array(
									'matakuliah_id'=>$matakuliah_id, 
									'namamk_id'=>$namamkid, 
									'kode_mk'=>$kodemk, 
									'kurikulum'=>$kurikulum,
									'sks'=>$sks
									);
				$mmk->replace_matakuliah($datanya);	
				// echo "insert to matakuliah";
			}
			
			
			//------------------insert into mk ditawarkan------------------------------
			$mkditawarkan_id	= $mmk->get_mkditawarkan_reg_number();
			$thnakademik		= $_POST['thn_akademik'];
			$matakuliahid		= $mjadwal->ceknamamk_matakuliah($_POST['addnamamk']);
			$isblok				= "0";
			$ispraktikum		= "0";
			$parent_id			= "";
			$isonlinemk			= "1";
			$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid, 
								'cabang_id'=>$cabang,
								'is_blok'=>$isblok,
								'is_online'=>$isonlinemk,
								'is_praktikum'=>$ispraktikum,
								'parent_id'=>$parent_id,
								'user_id'=>$user,
								'last_update'=>$lastupdate
								);
			$mmk->replace_mkditawarkan($datanya);
			// echo "insert to mkditawarkan";
			$namamk = $mkditawarkan_id;
		}
		
		//-------------------END cek mkditawarkanid and insert if there isnt any-----------------------
		
				
		if(isset($_POST['isaktif'])!=""){
			$isaktif	= $_POST['isaktif'];
		}else{
			$isaktif	= 0;
		}
		
		/* jika is course 1 maka is online 0, jika is course 0 maka is online 1 */
		if(isset($_POST['isonline'])!=""){ //$_POST['isonline'] = is course
			$isonline	= 0;
		}else{
			$isonline	= 1;
		}
		
		if(isset($_POST['prodi'])&&$_POST['prodi']!=""){
			$prodiid			= $mjadwal->get_prodiid($_POST['prodi']);
		}else{
			$this->redirect('module/akademik/jadwalmk/index/nok');
			exit();
		}
		
		$kelas				= $_POST['kelas'];
		
		
		if($isonline==0){
			//----------------------RUANG----------------------------------------------
			$ruangx				= explode(' - ', $_POST['ruang']);
			foreach ($ruangx as $a){
				$ruangp[] = $a;
			}
			for($i=0;$i<1;$i++){
				$namaruang = $ruangp[0];
				$koderuang = $ruangp[1];
			}
			$ruang				= $mjadwal->get_kelas($namaruang, $koderuang);
			//----------------------END RUANG----------------------------------------------
			
			$hari				= $_POST['hari'];
			$jammulai			= $_POST['jammulai'];
			$jamselesai			= $_POST['jamselesai'];
			//$isonline			= $_POST['isonline'];
			//------------------------------------------------------------------------------
		}
		else{
			$cek = $mjadwal->get_is_online_by_mkditawarkan($namamk);
			if($cek=='0'){
				$this->redirect('module/akademik/jadwalmk/index/wrongonline');
				exit();
			}
			
			$ruang				= "";
			$hari				= "";
			$jammulai			= "";
			$jamselesai			= "";
		}
		
		if(isset($_POST['writenew'])){
			//------insert into pengampu----------------

			$karyawanid		= $_POST['dosen'];
			$pengampu_id	= $mpngm->get_reg_number($namamk, $karyawanid);
			$iskoor			= "";
			
			$datapengampu 	= Array(
								'pengampu_id'=>$pengampu_id, 
								'mkditawarkan_id'=>$namamk, 
								'karyawan_id'=>$karyawanid, 
								'is_koordinator'=>$iskoor
								);
			$mpngm->replace_pengampu($datapengampu);
			
			$dosenpengampu	= $mjadwal->get_pengampubydos_mk($karyawanid, $namamk);
		}
		else {
			$dosenpengampu		= $_POST['dosen'];
			$namamk				= $mjadwal->ceknamamk_mkditawarkan($_POST['namamk']);
		}
		
		if($hid_id!=""){
			$jadwal_id 	= $hid_id;
		}else{
			$jadwal_id	= $mjadwal->get_reg_number($namamk, $dosenpengampu);	
		}
		
		if(isset($jadwal_id, $prodiid, $dosenpengampu, $namamk, $kelas, $isaktif)){
			
			$datanya 	= Array(
								'jadwal_id'=>$jadwal_id, 
								'prodi_id'=>$prodiid, 
								'pengampu_id'=>$dosenpengampu,
								'mkditawarkan_id'=>$namamk,
								'cabang_id'=>$cabang,
								'kelas'=>$kelas,
								'is_aktif'=>$isaktif,
								'is_online'=>$isonline,
								'ruang_id'=>$ruang,
								'hari'=>$hari,
								'jam_mulai'=>$jammulai,
								'jam_selesai'=>$jamselesai,
								'user_id'=>$user,
								'last_update'=>$lastupdate,
								);
			$mjadwal->replace_jadwalmk($datanya);
			// echo "insert to jadwalmk";
			// $this->redirect('module/akademik/jadwalmk/index/ok');
			// exit();
		}else{
			// $this->redirect('module/akademik/jadwalmk/index/nok');
			// exit();
		}
		
	}
	//----------------------------------------------------------------------------------------
	
	function get_dosenbyfakultas()
	{
		$mjadwal = new model_jadwalmk();
		$id = $_POST['fakultas_id'];
		$parent = $mjadwal->get_pengampubyfak($id);
		echo "<option value='0'>Select Dosen Pengampu</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->karyawan_id."'>".$p->nama."</option>" ;	
		}	
	}
	
	//-----------------------------------------------------------------------------------------
	
	function cekonline()
	{
		$mjadwal 		= new model_jadwalmk();
		$namamk 		= $_POST['namamk'];
		$cekonline		= $mjadwal->get_isonline_from_mk($namamk);
		
		if($cekonline!='0'){
			echo '';
		}
		else {
			echo $cekonline;
		}
	}
	
	function get_prodi_by_fakultas()
	{
		$mjadwal = new model_jadwalmk();
		if ($_POST['fakultas_id']!="0") {
			$fakultasid 	= $_POST['fakultas_id'];
			if (isset($_POST['prodi_id'])) {
			$prodiid = $_POST['prodi_id'];
			}
			else {
				$prodiid = "";
			}
			$parent 		= $mjadwal->get_prodi_by_fakultas($fakultasid);
			echo "<option></option>" ;
			echo "<option value='0'>Select Prodi</option>" ;
			if(count($parent)> 0) {
				foreach($parent as $p )
				{
					echo "<option value='".$p->prodi_id."' ";
					if(isset($prodiid)){
								if($prodiid==$p->prodi_id){
									echo "selected";
								}
							}
					echo ">".$p->keterangan."</option>" ;	
				}
			}	
		} else {
			echo '';
		}
	}
	
	function get_mk_by_thnakademik()
	{
		$mjadwal = new model_jadwalmk();
		if ($_POST['thnakademik_id']!="0") {
			$thnakademikid 	= $_POST['thnakademik_id'];
			$prodiid 		= $_POST['prodi_id'];
			$mkid 			= $_POST['mk_id'];
			$parent 		= $mjadwal->get_mkbythnakademik($thnakademikid, $prodiid);
			echo "<option></option>" ;
			echo "<option value='0'>Select Mata Kuliah</option>" ;
			if(count($parent)> 0) {
				foreach($parent as $p )
				{
					echo "<option value='".$p->mkditawarkan_id."' ";
					if(isset($mkid)){
								if($mkid==$p->mkditawarkan_id){
									echo "selected";
								}
							}
					echo ">".$p->namamk."</option>" ;	
				}
			}	
		} else {
			echo '';
		}
	}
	
	function get_pengampu_by_mk()
	{
		$mjadwal = new model_jadwalmk();
		if ($_POST['mk_id']!="0") {
			$id					= $_POST['mk_id'];
			$pengampuid			= $_POST['pengampu_id'];
			
			$parent = $mjadwal->get_pengampuindex($id);
			echo "<option></option>" ;
			echo "<option value='0'>Select Dosen Pengampu</option>" ;
			if(count($parent)> 0) {
				foreach($parent as $p )
				{
					echo "<option value='".$p->pengampu_id."' ";
					if(isset($pengampuid)){
						if($pengampuid==$p->pengampu_id){
							echo "selected";
							}
						}
					echo " >".$p->nama."</option>" ;	
				}
			}	
		} else {
			echo '';
		}
		
		
	}
	
	function cek_mkditawarkan()
	{
		$mjadwal = new model_jadwalmk();
		
		/*-----------------------cabang---------------------------*/
		if (isset($_POST['cabangid'])&&$_POST['cabangid']!="0") {
			$cabangid 	= $_POST['cabangid'];
		} else $cabangid = null;
		/*-----------------------fakultas---------------------------*/
		if (isset($_POST['fakultasid'])&&$_POST['fakultasid']!="0") {
			$fakultasid = $_POST['fakultasid'];
		} else $fakultasid = null;
		/*-----------------------thnakademik---------------------------*/
		if (isset($_POST['thnakademikid'])&&$_POST['thnakademikid']!="0") {
			$thnakademikid = $_POST['thnakademikid'];
		} else $thnakademikid = null;
		/*-----------------------ceknamamkval---------------------------*/
		if (isset($_POST['ceknamamkval'])&&$_POST['ceknamamkval']!="0") {
			$ceknamamkval = $_POST['ceknamamkval'];
		} else $ceknamamkval = null;
		
		$cekmkditawarkan_id = $mjadwal->get_mkditawarkan($cabangid, $fakultasid, $thnakademikid, $ceknamamkval);
		
		if($cekmkditawarkan_id){
			echo $cekmkditawarkan_id;
		}
		else echo '';
	}
	
}
?>