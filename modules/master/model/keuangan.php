<?php
class model_keuangan extends model {
	private $coms;
	public function __construct() {
		parent::__construct();	
	}
	
	
	function get_rekap_pendapatan($thn=NULL){
		$sql="SELECT DISTINCT
				db_ptiik_apps.tbl_karyawan.nama,
				db_ptiik_apps.tbl_karyawan.gelar_awal,
				db_ptiik_apps.tbl_karyawan.gelar_akhir,
				db_ptiik_apps.tbl_karyawan.nik,
				db_ptiik_apps.tbl_karyawan.pendidikan_terakhir,
				db_ptiik_apps.tbl_karyawan.is_nik,
				db_ptiik_apps.tbl_karyawan.is_mku,
				db_ptiik_apps.tbl_karyawan.karyawan_id,
				db_ptiik_apps.tbl_master_golongan.golongan,
				db_ptiik_apps.tbl_master_golongan.pangkat,
				db_ptiik_apps.tbl_master_golongan.inf_golongan,
				db_ptiik_apps.tbl_master_golongan.jabatan,
				Sum(db_ptiik_apps.tbl_keu_bayar_detail.total_bayar) as total
				FROM
				db_ptiik_apps.tbl_karyawan
				INNER JOIN db_ptiik_apps.tbl_keu_bayar_detail ON db_ptiik_apps.tbl_karyawan.karyawan_id = db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_master_golongan ON db_ptiik_apps.tbl_karyawan.golongan = db_ptiik_apps.tbl_master_golongan.golongan
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON db_ptiik_apps.tbl_keu_bayar.bayar_id = db_ptiik_apps.tbl_keu_bayar_detail.bayar_id
				WHERE year(tgl_mulai)='2014'  AND home_base='PTIIK'
				GROUP BY
				db_ptiik_apps.tbl_karyawan.karyawan_id
				ORDER By nama";
		return $this->db->query($sql);
	}
	
	//rekap keuangan
	function get_rekap_tahun($tahun){
		$sql = "SELECT 
					MID(tbl_keu_bayar.periode,5,2) bulan,
					LEFT(tbl_keu_bayar.periode,4) tahun,
					SUM(tbl_keu_bayar.total_non_pph) total_non_pph,
					SUM(tbl_keu_bayar.total_pph) total_pph,
					SUM(tbl_keu_bayar.total) total
				FROM db_ptiik_apps.tbl_keu_bayar
				WHERE left(tbl_keu_bayar.periode,4) = '$tahun'
				GROUP BY left(tbl_keu_bayar.periode,4)
				ORDER BY tbl_keu_bayar.periode";
		return $this->db->query($sql);
	}
	
	function get_rekap_periode($bulan=NULL, $tahun=NULL){
		$sql = "SELECT 
					tbl_keu_bayar.tgl_mulai,
					tbl_keu_bayar.tgl_selesai,
					SUM(tbl_keu_bayar.total_non_pph) total_non_pph,
					SUM(tbl_keu_bayar.total_pph) total_pph,
					SUM(tbl_keu_bayar.total) total
				FROM db_ptiik_apps.tbl_keu_bayar
				WHERE ( MONTH(tbl_keu_bayar.tgl_mulai) = '$bulan' OR MONTH(tbl_keu_bayar.tgl_selesai) = '$bulan') 
					AND YEAR(tbl_keu_bayar.last_update) = '$tahun'
				GROUP BY tbl_keu_bayar.tgl_selesai, tbl_keu_bayar.tgl_mulai
				ORDER BY tbl_keu_bayar.tgl_mulai";
				
		return $this->db->query($sql);
	}
	
	//dosen_titip
	function get_dosen(){
		$sql = "SELECT tbl_karyawan.nama, tbl_karyawan.karyawan_id
				FROM db_ptiik_apps.tbl_karyawan
				WHERE tbl_karyawan.is_status = 'dosen'
				ORDER BY tbl_karyawan.nama";
		return $this->db->query($sql);
	}
	
	function get_titip_id($dosen_sk, $dosen_titip, $tahun=NULL, $prodi=NULL, $mk=NULL, $kelas=NULL){
		$sql = "SELECT titip_id FROM db_ptiik_apps.tbl_keu_dosen_titip WHERE karyawan_id = '$dosen_sk' AND titip_ke='$dosen_titip' AND tahun_akademik='$tahun' AND prodi='$prodi' AND mkditawarkan_id='$mk'  AND kelas='$kelas' ";
		$row = $this->db->getRow($sql);
		if($row):
			return $row->titip_id;
		else:
			$sqli="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(titip_id,4) AS 
				 unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				 FROM db_ptiik_apps.tbl_keu_dosen_titip WHERE left(titip_id,6)='".date("Ym")."' "; 
			$rowi= $this->db->getRow( $sqli );
			
			return $rowi->data;
		endif;
		
		/*$titip = $tahun.substr(md5($dosen_sk),0,5);
		$titip .= substr(md5($dosen_titip),0,5);*/
		
		//return $titip;
	}
	
	function get_tahunakademik_aktif(){
		$sql = "SELECT tbl_tahunakademik.tahun_akademik AS data
				FROM db_ptiik_apps.tbl_tahunakademik
				WHERE tbl_tahunakademik.is_aktif = '1'";
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_titip($data){
			return $this->db->replace('db_ptiik_apps`.`tbl_keu_dosen_titip',$data);	
	
	}
	
	function del_titip($id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_keu_dosen_titip WHERE tbl_keu_dosen_titip.titip_id = '$id'";
		$this->db->query($sql);
	}
	
	function get_data_dosen_titip($tahun=NULL){
		$sql = "SELECT
				kar_a.karyawan_id AS dosen_sk_karyawan_id,
				kar_a.nama AS dosen_sk_nama,
				kar_a.nik AS dosen_sk_nik,
				kar_b.karyawan_id AS dosen_titip_karyawan_id,
				kar_b.nama AS dosen_titip_nama,
				kar_b.nik AS dosen_titip_nik,
				db_ptiik_apps.tbl_keu_dosen_titip.titip_id,
				db_ptiik_apps.tbl_keu_dosen_titip.tahun_akademik,
				db_ptiik_apps.tbl_keu_dosen_titip.mkditawarkan_id,
				db_ptiik_apps.tbl_keu_dosen_titip.kelas,
				db_ptiik_apps.tbl_keu_dosen_titip.prodi,
				db_ptiik_apps.tbl_namamk.keterangan as nama_mk
				FROM
				db_ptiik_apps.tbl_keu_dosen_titip
				LEFT JOIN db_ptiik_apps.tbl_karyawan AS kar_a ON kar_a.karyawan_id = db_ptiik_apps.tbl_keu_dosen_titip.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan AS kar_b ON kar_b.karyawan_id = db_ptiik_apps.tbl_keu_dosen_titip.titip_ke
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_keu_dosen_titip.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
				WHERE (MID(MD5(tbl_keu_dosen_titip.tahun_akademik),9,7) = '$tahun' OR tbl_keu_dosen_titip.tahun_akademik='$tahun')
				ORDER BY kar_a.nama";
		
		return $this->db->query($sql);
	}
	
	function update_titip($data, $where){
		$this->db->update('db_ptiik_apps`.`tbl_keu_dosen_titip',$data, $where);
	}

	function get_list_tahunakademik(){
		$sql = "SELECT 
					tbl_tahunakademik.tahun_akademik as id, 
					tbl_tahunakademik.tahun,
					tbl_tahunakademik.is_ganjil,
					tbl_tahunakademik.is_pendek,
					MID(MD5(tbl_tahunakademik.tahun_akademik),9,7) tahun_akademik
				FROM db_ptiik_apps.tbl_tahunakademik
				ORDER BY tbl_tahunakademik.tahun_akademik DESC";
		return $this->db->query($sql);
	}
	
	function get_detail_tahun($tahun){
		$sql = "SELECT 
					tbl_tahunakademik.tahun,
					tbl_tahunakademik.is_ganjil,
					tbl_tahunakademik.is_pendek
				FROM db_ptiik_apps.tbl_tahunakademik
				WHERE MID(MD5(tbl_tahunakademik.tahun_akademik),9,7) = '$tahun'";
		return $this->db->getRow($sql);
	}
	//conf
	function get_conf(){
		$sql = "SELECT 
					MID(MD5(tbl_keu_conf.conf_id),8,6) conf_id,
					tbl_keu_conf.golongan,
					tbl_keu_conf.jenjang_pendidikan,
					tbl_keu_conf.tarif,
					tbl_keu_conf.strata,
					tbl_keu_conf.is_pns,
					tbl_keu_conf.potongan,
					tbl_master_golongan.jabatan,
					tbl_master_golongan.pangkat
				FROM db_ptiik_apps.tbl_keu_conf
				INNER JOIN db_ptiik_apps.tbl_master_golongan ON tbl_master_golongan.golongan = tbl_keu_conf.golongan";
		return $this->db->query($sql);
	}
	
	function get_golongan(){
		$sql= "SELECT 
					tbl_master_golongan.golongan,
					tbl_master_golongan.pangkat
				FROM db_ptiik_apps.tbl_master_golongan";
		return $this->db->query($sql);
	}
	
	function update_conf($data, $where){
		$this->db->replace('db_ptiik_apps`.`tbl_keu_conf',$data, $where);		
	}
	
	function get_conf_id(){
		$kode = date("Ym");
		$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(conf_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_keu_conf WHERE left(conf_id, 6)='".$kode."'";		
		$dt = $this->db->getRow( $sql );			
		return $dt->data;
	}
	
	function replace_conf($data){
		$this->db->replace('db_ptiik_apps`.`tbl_keu_conf',$data);		
	}

}
?>