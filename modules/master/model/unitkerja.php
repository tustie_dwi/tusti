<?php
class model_unitkerja extends model {
	
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function read_parent(){
		$sql = "SELECT mid(md5(u.unit_id),6,6) unit_id, u.unit_id hidId, f1.fakultas_id, u.keterangan, u.kode, u.kategori, u.is_aktif
				FROM `db_ptiik_apps`.`tbl_unit_kerja` u
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` f1 ON u.fakultas_id = f1.fakultas_id
				WHERE u.parent_id = '0'";
		return $this->db->query($sql);
	}
	
	function read_child(){
		$sql = "SELECT mid(md5(u.unit_id),6,6) unit_id, u.unit_id hidId, f1.keterangan fakultas, u.keterangan, u.kode, u.parent_id,u.kategori, u.is_aktif
				FROM `db_ptiik_apps`.`tbl_unit_kerja` u
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` f1 ON u.fakultas_id = f1.fakultas_id
				WHERE u.parent_id <> '0'";
		return $this->db->query($sql);
	}
	
	function get_kategori_unit(){
		$sql = "SELECT *
				FROM `db_ptiik_apps`.`tbl_unit_kerja_kategori` u
				WHERE 1";
		return $this->db->query($sql);
	}
	
	function get_unitByFak($id=NULL){
		$sql = "SELECT u.keterangan, u.unit_id, u.kode
				FROM `db_ptiik_apps`.`tbl_unit_kerja` u
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` f1 ON u.fakultas_id = f1.fakultas_id
				WHERE 1";
		if($id){
			$sql .= " AND u.fakultas_id = '" . $id . "'";
		}
		return $this->db->query($sql);
	}
	
	function get_unit_kerja($id=NULL){
		$sql = "SELECT mid(md5(u.unit_id),6,6) unit_id, u.unit_id hidId, f1.keterangan fakultas, u.keterangan, u.kode, u.parent_id, f1.fakultas_id, u.kategori,
					   u.english_version as english, u.is_aktif, u.icon, u.logo, u.strata, u.tentang, u.about
				FROM `db_ptiik_apps`.`tbl_unit_kerja` u
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` f1 ON u.fakultas_id = f1.fakultas_id
				WHERE 1 ";
		if($id) $sql .= " AND (mid(md5(u.unit_id),6,6) = '" . $id ."' OR u.unit_id='$id')";	
		
		$sql .= "ORDER BY u.unit_id DESC";
		
		if($id) return $this->db->getRow( $sql );
		else return $this->db->query( $sql );	
	}
	
	function get_unitByUnitId($id=NULL){
		$sql = "SELECT concat(u2.keterangan, ' - ', u2.kode) keterangan, u2.unit_id, u2.parent_id
				FROM `db_ptiik_apps`.`tbl_unit_kerja` u1
				LEFT JOIN `db_ptiik_apps`.`tbl_unit_kerja` u2 ON u2.fakultas_id = u1.fakultas_id
				WHERE mid(md5(u1.unit_id),6,6) = '" . $id . "'";
		return $this->db->query($sql);
	}
	
	function get_fakultas(){
		$sql = "SELECT fakultas_id, concat(keterangan, ' - ', kode_fakultas) as keterangan FROM `db_ptiik_apps`.`tbl_fakultas` ORDER BY keterangan";
		return $this->db->query($sql);
	}
	
	function get_unitid(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(unit_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_unit_kerja
			where left(unit_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_unitkerja($data){
		if($this->db->replace('db_ptiik_apps`.`tbl_unit_kerja',$data)) return TRUE;
		else return FALSE;
	}
	
	function replace_unitkerja_coms($data){
		return $this->db->replace('db_coms`.`coms_level',$data);
	}
	
	function update_unitkerja_coms($data, $id){
		return $this->db->update('db_coms`.`coms_level',$data, $id);
	}
	
	function replace_prodi($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_prodi',$data);
	}
	
	function replace_fakultas($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_fakultas',$data);
	}
	
	function insert_unitkerja($data){
		if($this->db->insert('db_ptiik_apps`.`tbl_unit_kerja',$data)) return TRUE;
		else return FALSE;
	}
	
	function del_unitkerja($id,$table,$field){
		$sql = "UPDATE `db_ptiik_apps`.`".$table."` SET `is_aktif` = 0 WHERE mid(md5(`".$field."`),6,6) = '" . $id . "'";
		return $this->db->query($sql);
	}
	
}
?>