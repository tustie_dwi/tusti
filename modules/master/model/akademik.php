<?php
class model_akademik extends model {
	private $coms;
	
	public function __construct() {
		parent::__construct();	
	}
	
	function get_all_dosen_pengampu(){
		$sql="SELECT 
			  (SELECT 
			  `db_ptiik_apps`.`tbl_karyawan`.nama 
			  FROM 
			  `db_ptiik_apps`.`tbl_karyawan` 
			  WHERE `db_ptiik_apps`.`tbl_karyawan`.karyawan_id =  `db_ptiik_apps`.`tbl_pengampu`.karyawan_id) as nama,
			  
			  `db_ptiik_apps`.`tbl_pengampu`.is_koordinator,
              `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id
              
			  FROM 
			  `db_ptiik_apps`.`tbl_pengampu`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan`
			  
			  WHERE 
			  `db_ptiik_apps`.`tbl_pengampu`.mkditawarkan_id = `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id
			"; 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_all_thn_akademik(){
		$sql = "SELECT tahun_akademik, CONCAT(tahun , ' ' , is_ganjil , ' ' , is_pendek) as thnakademik FROM `db_ptiik_apps`.`tbl_tahunakademik` ORDER BY tahun_akademik DESC
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_fakultas(){
		$sql = "SELECT mid(md5(fakultas_id),6,6) as fakultasid, keterangan, fakultas_id as hid_id
				FROM `db_ptiik_apps`.`tbl_fakultas`";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_cabangub(){
		$sql = "SELECT `cabang_id`, `keterangan`
				FROM `db_ptiik_apps`.`tbl_cabang` 
				WHERE 1
				";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
}
?>