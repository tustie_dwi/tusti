<?php
class model_dosen extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_rekap_prestasi($id=NULL, $str=NULL){
		$sql = "SELECT DISTINCT
				tbl_prestasi.jenis_id,
				tbl_prestasi.kategori,
				tbl_prestasi.tahun_akademik,
				tbl_prestasi.judul,
				tbl_prestasi.penyelenggara,
				tbl_prestasi.tgl_mulai,
				tbl_prestasi.tgl_selesai,
				tbl_prestasi.lokasi,
				tbl_prestasi.inf_prestasi,
				tbl_prestasi.tingkat,
				tbl_prestasi.jenis_prestasi,
				tbl_prestasi.keterangan,
				tbl_prestasi.inf_peserta,
				tbl_prestasi.link_berita,
				tbl_prestasi.penghargaan,
				tbl_prestasi.user_id,
				tbl_prestasi.last_update,
				tbl_prestasi.prestasi_id,
				tbl_prestasi_peserta.karyawan_id,
				tbl_prestasi_peserta.inf_nama,
				tbl_prestasi_peserta.mahasiswa_id,
					(
						SELECT group_concat(inf_nama)
						FROM
						`db_ptiik_apps`.`tbl_prestasi_peserta`
						 WHERE `db_ptiik_apps`.`tbl_prestasi`.`prestasi_id`=`db_ptiik_apps`.`tbl_prestasi_peserta`.`prestasi_id`
					) as `peserta`
				FROM
				db_ptiik_apps.tbl_prestasi
				INNER JOIN db_ptiik_apps.tbl_prestasi_peserta ON tbl_prestasi.prestasi_id = tbl_prestasi_peserta.prestasi_id 
				WHERE (mid(md5(`tbl_prestasi_peserta`.`karyawan_id`),9,7) = '$id' or tbl_prestasi_peserta.karyawan_id='$id' or tbl_prestasi_peserta.mahasiswa_id='$id') 
				";
		if($str) $sql.= " AND tbl_prestasi.prestasi_id = '$str' ";				
				
		$sql.= "  ORDER BY tbl_prestasi.tgl_mulai DESC ";
		
		if($str) return $this->db->getRow( $sql );
		else return $this->db->query( $sql );	
	}
	
	function get_rekap_aktifitas($id=NULL){
		$sql = "SELECT
					tbl_aktifitas.karyawan_id,
					tbl_aktifitas.jenis_kegiatan_id,
					tbl_aktifitas.hari,
					tbl_aktifitas.tgl,
					tbl_aktifitas.tgl_selesai,
					tbl_aktifitas.jam_mulai,
					tbl_aktifitas.jam_selesai,
					tbl_aktifitas.judul,
					tbl_aktifitas.inf_ruang,
					tbl_aktifitas.catatan,
					tbl_aktifitas.lokasi,
					tbl_jeniskegiatan.keterangan AS jenis_kegiatan
					FROM
					db_ptiik_apps.tbl_aktifitas
					INNER JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_aktifitas.jenis_kegiatan_id = tbl_jeniskegiatan.jenis_kegiatan_id 
					WHERE (mid(md5(`tbl_aktifitas`.`karyawan_id`),9,7) = '$id' or tbl_aktifitas.karyawan_id='$id') ORDER BY tbl_aktifitas.tgl DESC, tbl_aktifitas.jam_mulai DESC
					";
		return $this->db->query( $sql );	
	}
	
	function get_rekap_absen($id=NULL, $periode=NULL){
		$sql = "SELECT
				tbl_rekap_absen.rekap_id,
				tbl_rekap_absen.karyawan_id,
				tbl_rekap_absen.hadir,
				tbl_rekap_absen.ijin,
				tbl_rekap_absen.sakit,
				tbl_rekap_absen.alpha,
				tbl_rekap_absen.periode
				FROM
				db_ptiik_apps.tbl_rekap_absen WHERE 1 
				";
				
		if($id) $sql.= " AND  (mid(md5(`tbl_rekap_absen`.`karyawan_id`),9,7) = '$id' or karyawan_id='$id') ";
		if($periode) $sql.= " AND  (periode='$periode') ";
		
		$sql.= " ORDER BY periode DESC ";
		
		return $this->db->query( $sql );	
	}
	
	
	function get_rekap_layanan($id=NULL){
		$sql = "SELECT
					tbl_layanan.judul,
					tbl_layanan.no_permintaan,
					tbl_layanan.tgl_harapan_selesai,
					tbl_layanan.tgl_permintaan,
					tbl_layanan_pelaksana.is_finish,
					tbl_layanan.status_proses,
					tbl_layanan_status.keterangan AS `status`,
					tbl_layanan_pelaksana.pelaksana_id,
					tbl_layanan_pelaksana.karyawan_id,
					tbl_karyawan.nama as `req_dari`
					FROM
					db_ptiik_apps.tbl_layanan
					INNER JOIN db_ptiik_apps.tbl_layanan_pelaksana ON tbl_layanan.permintaan_id = tbl_layanan_pelaksana.permintaan_id
					INNER JOIN db_ptiik_apps.tbl_layanan_status ON tbl_layanan.status_proses = tbl_layanan_status.status_proses
					INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_layanan.request_dari = tbl_karyawan.karyawan_id
					";
		if($id) $sql.= " AND  (mid(md5(`tbl_layanan_pelaksana`.`karyawan_id`),9,7) = '$id' or tbl_layanan_pelaksana.karyawan_id='$id') ";
		
		
		$sql.= " ORDER BY tbl_layanan.no_permintaan DESC ";
		
		return $this->db->query( $sql );	
	}
	
	function get_rekap_layanan_progress($id=NULL){
		$sql = "SELECT
					tbl_layanan_progress.progress_id,
					tbl_layanan_progress.pelaksana_id,
					tbl_layanan_progress.tgl_mulai,
					tbl_layanan_progress.keterangan,
					tbl_layanan_progress.tgl_target_selesai,
					tbl_layanan_progress.tgl_selesai,
					tbl_layanan_progress.judul,
					tbl_layanan_progress.catatan,
					tbl_layanan_progress.inf_progress
					FROM
					db_ptiik_apps.tbl_layanan_progress WHERE pelaksana_id='$id' 
					";
		return $this->db->query( $sql );
	}
	
	function read($id=NULL, $fakultas=NULL, $cabang=NULL){
		$sql = "SELECT mid(md5(`tbl_karyawan`.`karyawan_id`),9,7) as `karyawan_id`,
				`tbl_karyawan`.`karyawan_id` as hid_id,
				`tbl_karyawan`.`pin`,
				`tbl_karyawan`.`nik`,
				`tbl_karyawan`.`nama`,
				CONCAT(IFNULL(`tbl_karyawan`.`gelar_awal`,''), ' ', `tbl_karyawan`.`nama`, ' ', IFNULL(`tbl_karyawan`.`gelar_akhir`,'')) as fullname,
				`tbl_karyawan`.`gelar_awal`,
				`tbl_karyawan`.`gelar_akhir`,
				`tbl_karyawan`.`tgl_lahir`,
				`tbl_karyawan`.`jenis_kelamin`,
				`tbl_karyawan`.`telp`,
				`tbl_karyawan`.`hp`,
				`tbl_karyawan`.`is_status`,
				`tbl_karyawan`.`alamat`,
				`tbl_karyawan`.`email`,
				`tbl_karyawan`.`fakultas_id`,
				`tbl_karyawan`.`cabang_id`,
				`tbl_karyawan`.`golongan`,
				`tbl_karyawan`.`nama_bank`,
				`tbl_karyawan`.`no_rekening` as rekening,
				`tbl_karyawan`.`tgl_masuk`,
				`tbl_karyawan`.`tgl_pensiun`,
				`tbl_karyawan`.`pendidikan_terakhir`,
				`tbl_karyawan`.`biografi`,
				`tbl_karyawan`.`biografi_en`,
				`tbl_karyawan`.`is_nik`,
				`tbl_karyawan`.`home_base`,
				`tbl_karyawan`.`interest`,
				`tbl_karyawan`.`interest_en`,
				`tbl_karyawan`.`about`,
				`tbl_karyawan`.`about_en`,
				`tbl_karyawan`.`is_tetap`,
				`tbl_karyawan`.`is_aktif`,
				`tbl_karyawan`.`cabang_id`,
				`tbl_karyawan`.`foto`,
				`tbl_karyawan`.`user_id`,
				`tbl_karyawan`.`last_update`,
                `tbl_fakultas`.`keterangan` as fakultas,
                `tbl_cabang`.`keterangan` as cabang
						";
			  $sql.= ",(SELECT GROUP_CONCAT(tbl_karyawan_tugas.tugas)  FROM `db_ptiik_apps`.`tbl_karyawan_tugas` WHERE tbl_karyawan_tugas.karyawan_id = tbl_karyawan.karyawan_id) as tugas,
                (SELECT GROUP_CONCAT(tbl_karyawan_tugas.tugas_id) FROM `db_ptiik_apps`.`tbl_karyawan_tugas` WHERE tbl_karyawan_tugas.karyawan_id = tbl_karyawan.karyawan_id) as tugas_id";
		$sql.= " FROM `db_ptiik_apps`.`tbl_karyawan`
                LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` ON `tbl_fakultas`.`fakultas_id` = `tbl_karyawan`.`fakultas_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_cabang` ON `tbl_cabang`.`cabang_id` = `tbl_karyawan`.`cabang_id`
				WHERE 1 ";
		
		if($fakultas!=""){
			$sql=$sql . " AND tbl_karyawan.fakultas_id = '".$fakultas."' ";
		}
		
		if($cabang!=""){
			$sql=$sql . " AND tbl_karyawan.cabang_id ='".$cabang."' ";
			// echo $sql;
		}
		
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_karyawan`.`karyawan_id`),9,7) = '".$id."' ";
			$result = $this->db->getRow( $sql );	
		}else{	
		// echo $sql;
		$result = $this->db->query( $sql );	
		}
		// echo $sql;
		return $result;	
	}
	
	function get_ruang_karyawan($karyawan=NULL){
		$sql = "SELECT *
				FROM `db_ptiik_apps`.`tbl_karyawan_ruang`
				WHERE 1";
		if($karyawan){
			$sql .= " AND karyawan_id = '".$karyawan."'";
		}
		$result = $this->db->query( $sql );	
		// echo $sql;
		return $result;
	}
	
	function get_fakultas($id=NULL){
		$sql = "SELECT mid(md5(`tbl_fakultas`.`fakultas_id`),9,7) as `fakultas_id`,
				`tbl_fakultas`.`fakultas_id` as `hid_id`,
				`tbl_fakultas`.`keterangan`,
				`tbl_fakultas`.`image`,
				`tbl_fakultas`.`kode_fakultas`
				FROM `db_ptiik_apps`.`tbl_fakultas`";
		if($id!=""){
			$sql=$sql . " WHERE `tbl_fakultas`.`fakultas_id`='".$id."' ";
		}
		$sql = $sql . "ORDER BY `tbl_fakultas`.`fakultas_id` DESC";
		$result = $this->db->query( $sql );	
		return $result;
	}
	
	function update_bio_karyawan($karyawan_id,$biografi,$biografi_en,$about,$about_en,$interest,$interest_en){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_karyawan`
				SET tbl_karyawan.biografi = '".$biografi."',
					tbl_karyawan.biografi_en = '".$biografi_en."',
					tbl_karyawan.about = '".$about."',
					tbl_karyawan.about_en = '".$about_en."',
					tbl_karyawan.interest = '".$interest."',
					tbl_karyawan.interest_en = '".$interest_en."'
				WHERE tbl_karyawan.karyawan_id = '".$karyawan_id."'";
		$result = $this->db->query( $sql );	
		return $result;
	}
	
	function get_reg_number($tglid){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(karyawan_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_karyawan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function tugas_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tugas_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_karyawan_tugas`
				where left(tugas_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_dosen($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_karyawan',$datanya);
		//var_dump($datanya);
	}
	
	function replace_tugas_dosen($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_karyawan_tugas',$datanya);
		//var_dump($datanya);
	}
	
	function replace_ruang_dosen($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_karyawan_ruang',$datanya);
		//var_dump($datanya);
	}
	
	function delete_ruang_id($id=NULL, $dosen=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_karyawan_ruang`
				WHERE 1";
		if($id){
			$sql .= " AND tbl_karyawan_ruang.ruang_id = '".$id."'";
		}
		
		if($dosen) $sql.=" AND tbl_karyawan_ruang.karyawan_id = '".$dosen."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_tugas($tugas=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_karyawan_tugas`
				WHERE tbl_karyawan_tugas.tugas_id = '".$tugas."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function update_foto($file_loc, $karyawan_id) {
		$sql = "update db_coms.coms_user set foto ='".$file_loc."' where karyawan_id = '".$karyawan_id."'
				";
		if($result = $this->db->query( $sql )){
		return TRUE;
		}
	}
	
	function get_cabang(){
		$sql = "SELECT `cabang_id`, `keterangan`
				FROM `db_ptiik_apps`.`tbl_cabang` 
				WHERE 1
				";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, keterangan, extention, max_size FROM `db_ptiik_apps`.`tbl_jenisfile` WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		if(isset($result)){
		return $result;
		}
	}
	
	function get_unit($unitid=NULL,$fakultasid=NULL,$cabangid=NULL){
		$sql = "SELECT mid(md5(u.unit_id),9,7) unit_id, u.unit_id hidId, f1.fakultas_id, u.keterangan, u.kode, u.kategori
				FROM `db_ptiik_apps`.`tbl_unit_kerja` u
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` f1 ON u.fakultas_id = f1.fakultas_id
				WHERE 1";
		
		if($fakultasid){
			$sql .= " AND f1.fakultas_id = '".$fakultasid."' ";
		}
		
		if($cabangid){
			$sql .= " AND u.cabang_id = '".$cabangid."' ";
		}
		
		if($unitid){
			$sql .= " AND mid(md5(u.unit_id),9,7) = '".$unitid."' ";
			return $this->db->getRow( $sql );
		}else{
			return $this->db->query($sql);
		}
		
	}
	
	function delete_unit($unitid=NULL,$karyawanid=NULL,$periodemulai=NULL,$periodeselesai=NULL,$isaktif=NULL){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_karyawan_unit` 
				WHERE mid(md5(tbl_karyawan_unit.karyawan_id),9,7) = '".$karyawanid."' 
				AND tbl_karyawan_unit.unit_id = '".$unitid."' 
				AND tbl_karyawan_unit.periode_mulai = '".$periodemulai."'
				AND tbl_karyawan_unit.periode_selesai = '".$periodeselesai."'
				AND tbl_karyawan_unit.is_aktif = '".$isaktif."'
				";
		// echo $sql;
		if($this->db->query($sql))return TRUE;else return FALSE;
	}
	
	function delete_kenaikan($kenaikanid=NULL){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_karyawan_kenaikan` 
				WHERE mid(md5(tbl_karyawan_kenaikan.kenaikan_id),9,7) = '".$kenaikanid."' 
				";
		// echo $sql;
		if($this->db->query($sql))return TRUE;else return FALSE;
	}
	
	function get_unitkaryawan($id){
		$sql = "SELECT 
				`tbl_karyawan_unit`.*,
				 mid(md5(tbl_karyawan_unit.karyawan_id),9,7) as karyawanid,
				 mid(md5(tbl_karyawan_unit.unit_id),9,7) as unitid,
				`tbl_unit_kerja`.`kategori` as `kategori_unit`,
				`tbl_unit_kerja`.`keterangan` as `keteranganunit`
				FROM `db_ptiik_apps`.`tbl_karyawan_unit` 
				INNER JOIN `db_ptiik_apps`.`tbl_unit_kerja` ON `tbl_unit_kerja`.`unit_id` = `tbl_karyawan_unit`.`unit_id`
				WHERE 1
			   ";
		
		if($id){
			$sql .= " AND mid(md5(tbl_karyawan_unit.karyawan_id),9,7) = '".$id."' ";
		}
		
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_reg_prestasi($semester=NULL){
				
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(prestasi_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_prestasi ";		
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
				
		return $strresult;
	}
	
	function get_reg_prestasi_peserta($kode=NULL, $peserta=NULL){
				
		$sql = "SELECT peserta_id FROM db_ptiik_apps.tbl_prestasi_peserta WHERE prestasi_id ='$kode' AND (karyawan_id = '$peserta' OR mahasiswa_id ='$peserta') ";
		$result = $this->db->getRow( $sql );	
		
		if($result){
			$strresult = $result->peserta_id;
		}else{
			$str = substr($kode,0,10);
			
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(peserta_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_prestasi_peserta ";		
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
			
		}		
		
		return $strresult;
	}
	
	function insert_prestasi($datanya){
		if($this->db->insert('db_ptiik_apps`.`tbl_prestasi',$datanya)) return TRUE;
		else return FALSE;
	}
	
	function update_prestasi($datanya, $idnya){
		if($this->db->update('db_ptiik_apps`.`tbl_prestasi',$datanya, $idnya)) return TRUE;
		else return FALSE;
	}
	
	function insert_prestasi_peserta($datanya){
		if($this->db->insert('db_ptiik_apps`.`tbl_prestasi_peserta',$datanya)) return TRUE;
		else return FALSE;
	}
	
	function insert_unit($datanya){
		if($this->db->insert('db_ptiik_apps`.`tbl_karyawan_unit',$datanya)) return TRUE;
		else return FALSE;
	}
	
	function update_unit($unitid,$karyawanid,$periodemulai,$periodeselesai,$isaktif,$paramunitid,$paramperiodemulai,$paramperiodeselesai,$paramisaktif){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_karyawan_unit` 
				SET `unit_id`='".$unitid."',
				`periode_mulai`='".$periodemulai."',
				`periode_selesai`='".$periodeselesai."',
				`is_aktif`='".$isaktif."'
				WHERE `karyawan_id` = '".$karyawanid."'
				AND `unit_id` = '".$paramunitid."'
				AND `periode_mulai` = '".$paramperiodemulai."'
				AND `periode_selesai` = '".$paramperiodeselesai."'
				AND `is_aktif` = '".$paramisaktif."'
			   ";	
		
		if($this->db->query($sql)) return TRUE;
		else return FALSE;
	}
	
	//---kenaikan------
	function get_jabatan(){
		$sql = "SELECT 
				mid(md5(`tbl_master_jabatan`.`jabatan_id`),9,7) as `jabatan_id`,
				mid(md5(`tbl_master_jabatan`.`parent_id`),9,7) as `parent_id`,
				`tbl_master_jabatan`.`jabatan_id` as `jabatanid`,
				`tbl_master_jabatan`.`keterangan`,
				`tbl_master_jabatan`.`parent_id` as `parentid`
				FROM `db_ptiik_apps`.`tbl_master_jabatan` 
				WHERE 1
			   ";
		
		$result = $this->db->query($sql);
		return $result;
	}
	
	function get_golongan(){
		$sql = "SELECT * FROM `db_ptiik_apps`.`tbl_master_golongan` WHERE 1
			   ";
		
		$result = $this->db->query($sql);
		return $result;
	}
	
	function update_golongan($karyawanid=NULL,$golongan=NULL){
		$sql = "UPDATE db_ptiik_apps.`tbl_karyawan` SET tbl_karyawan.`golongan`='".$golongan."' WHERE tbl_karyawan.`karyawan_id`='".$karyawanid."' ";
		$this->db->query($sql);
	}
	
	function update_jabatan($karyawanid=NULL,$jabatan=NULL){
		$sql = "UPDATE db_ptiik_apps.`tbl_karyawan` SET tbl_karyawan.`jabatan_id`='".$jabatan."' WHERE tbl_karyawan.`karyawan_id`='".$karyawanid."' ";
		$this->db->query($sql);
	}
	
	function get_jeniskenaikanIdby($id=NULL){
		$sql = "SELECT 
				tbl_master_jenis_kenaikan.jenis_kenaikan_id
				FROM `db_ptiik_apps`.`tbl_master_jenis_kenaikan` 
				WHERE 1";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_master_jenis_kenaikan`.`jenis_kenaikan_id`),9,7)='".$id."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		return $dt;
		
	}
	
	function get_file($karyawanid=NULL){
		$sql = "SELECT 
					mid(md5(`tbl_media_library`.`file_id`),9,7) as `documentid`,
					`tbl_media_library`.*,
					`tbl_media_library_folder`.*
				FROM `db_ptiik_apps`.`tbl_media_library` 
				INNER JOIN `db_ptiik_apps`.`tbl_media_library_folder` ON `tbl_media_library_folder`.`id` = `tbl_media_library`.`folder_id`
				WHERE 1
				AND `tbl_media_library_folder`.`is_available = '1'
			   ";
		
		if($karyawanid){
			$sql .= " AND mid(md5(`tbl_media_library_folder`.`karyawan_id`),9,7) = '".$karyawanid."' ";
		}
		// echo $sql;
		$result = $this->db->query($sql);
		return $result;
	}
	
	function get_kenaikan($karyawanid=NULL,$kenaikanid=NULL){
		$sql = "SELECT 
				mid(md5(`tbl_karyawan_kenaikan`.`kenaikan_id`),9,7) as `kenaikan_id`,
				`tbl_karyawan_kenaikan`.`kenaikan_id` as `kenaikanid`,
				
				mid(md5(`tbl_karyawan_kenaikan`.`jenis_kenaikan_id`),9,7) as `jenis_kenaikan_id`,
				`tbl_karyawan_kenaikan`.`jenis_kenaikan_id` as `jeniskenaikanid`,
				
				mid(md5(`tbl_karyawan_kenaikan`.`document_id`),9,7) as `document_id`,
				`tbl_karyawan_kenaikan`.`document_id` as `documentid`,
				
				mid(md5(`tbl_karyawan_kenaikan`.`jabatan_id`),9,7) as `jabatan_id`,
				`tbl_karyawan_kenaikan`.`jabatan_id` as `jabatanid`,
				
				`tbl_karyawan_kenaikan`.`golongan` as `golongan`,
				
				mid(md5(`tbl_karyawan_kenaikan`.`karyawan_id`),9,7) as `karyawan_id`,
				`tbl_karyawan_kenaikan`.`karyawan_id` as `karyawanid`,
				
				mid(md5(`tbl_karyawan_kenaikan`.`unit_id`),9,7) as `unit_id`,
				`tbl_karyawan_kenaikan`.`unit_id` as `unitid`,
				
				`tbl_karyawan_kenaikan`.`fakultas_id`,
				`tbl_karyawan_kenaikan`.`tgl_sk`,
				`tbl_karyawan_kenaikan`.`no_sk`,
				`tbl_karyawan_kenaikan`.`periode_mulai`,
				`tbl_karyawan_kenaikan`.`periode_selesai`,
				`tbl_karyawan_kenaikan`.`no_nota`,
				`tbl_karyawan_kenaikan`.`tgl_nota`,
				`tbl_karyawan_kenaikan`.`tmt`,
				`tbl_karyawan_kenaikan`.`no_stlud`,
				`tbl_karyawan_kenaikan`.`tgl_stlud`,
				`tbl_karyawan_kenaikan`.`pejabat_penetap`,
				`tbl_karyawan_kenaikan`.`kredit`,
				`tbl_karyawan_kenaikan`.`masa_kerja`,
				`tbl_karyawan_kenaikan`.`is_aktif`,
				`tbl_karyawan_kenaikan`.`jenis`,
				`tbl_karyawan_kenaikan`.`keterangan`,
				`tbl_karyawan_kenaikan`.`user_id`,
				`tbl_karyawan_kenaikan`.`last_update`,
				
				`tbl_master_jabatan`.`keterangan` as keterangan_jabatan,
				`tbl_master_golongan`.`pangkat` as pangkat_golongan,
                                
                `tbl_media_library`.`file_name`
				
				FROM `db_ptiik_apps`.`tbl_karyawan_kenaikan` 
				LEFT JOIN `db_ptiik_apps`.`tbl_master_jabatan` ON `tbl_master_jabatan`.`jabatan_id` = `tbl_karyawan_kenaikan`.`jabatan_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_master_golongan` ON `tbl_master_golongan`.`golongan` = `tbl_karyawan_kenaikan`.`golongan`
				LEFT JOIN `db_ptiik_apps`.`tbl_media_library` ON `tbl_media_library`.`file_id` = `tbl_karyawan_kenaikan`.`document_id`
				WHERE 1
			   ";
		if($karyawanid){
			$sql .= " AND mid(md5(`tbl_karyawan_kenaikan`.`karyawan_id`),9,7) = '".$karyawanid."' ";
		}
		
		if($kenaikanid){
			$sql .= " AND mid(md5(`tbl_karyawan_kenaikan`.`kenaikan_id`),9,7) = '".$kenaikanid."' ";
		}
		
		// echo $sql;
		$result = $this->db->query($sql);
		return $result;
	}
	
	function get_kenaikanId(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kenaikan_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_karyawan_kenaikan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_jabatanIdby($jabatanid=NULL){
		$sql = "SELECT tbl_master_jabatan.jabatan_id
				FROM db_ptiik_apps.tbl_master_jabatan
				WHERE 1
			   "; 
		if($jabatanid){
			$sql .= " AND mid(md5(tbl_master_jabatan.jabatan_id),9,7) = '".$jabatanid."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		// $strresult = $dt->data;
		return $dt;
	}
	
	function replace_kenaikan($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_karyawan_kenaikan',$datanya);
		//var_dump($datanya);
	}
	
	//====pendidikan====//
	function read_pendidikan($jns_pendidikan=NULL, $karyawan=NULL, $pend_id=NULL,$nama_kegiatan=NULL){		
		$sql = "SELECT *";
		if($jns_pendidikan=="formal"){
			$sql .= " , mid(md5(`tbl_karyawan_formal`.`formal_id`),9,7) as `formalid`
					  FROM db_ptiik_apps.tbl_karyawan_formal
					  LEFT JOIN db_ptiik_apps.tbl_media_library 
					  	ON tbl_karyawan_formal.document_id = tbl_media_library.file_id";
		}
		else{
			$sql .= " , mid(md5(`tbl_karyawan_non_formal`.`non_formal_id`),9,7) as `nonformalid`,
					  tbl_jeniskegiatan.keterangan as jeniskegiatan
					  FROM db_ptiik_apps.tbl_karyawan_non_formal
					  LEFT JOIN db_ptiik_apps.tbl_media_library 
					  	ON tbl_karyawan_non_formal.document_id = tbl_media_library.file_id
					  LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan
					    ON tbl_karyawan_non_formal.jenis_kegiatan = tbl_jeniskegiatan.jenis_kegiatan_id";
		}
		$sql .= " WHERE 1"; 
		if($karyawan){
			$sql .= " AND (karyawan_id = '".$karyawan."' OR mid(md5(karyawan_id),9,7) = '".$karyawan."')";
		}
		if($nama_kegiatan){
			$sql .= " AND nama_kegiatan = '".$nama_kegiatan."'";
			
		}
		if($pend_id!=""){
			if($jns_pendidikan=="formal"){
				$sql .= " AND mid(md5(tbl_karyawan_formal.formal_id),9,7) = '".$pend_id."'";
			}
			else{
				$sql .= " AND mid(md5(tbl_karyawan_non_formal.non_formal_id),9,7) = '".$pend_id."'";
			}
			$result = $this->db->getRow($sql);
			// echo $sql;
			return $result;
		}
		
		
		$result = $this->db->query($sql);
		// echo $sql;
		return $result;
	}
	
	function formal_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(formal_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_karyawan_formal"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_pendidikan_formal($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_karyawan_formal',$datanya);
	}
	
	function non_formal_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(non_formal_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_karyawan_non_formal"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_pendidikan_non_formal($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_karyawan_non_formal',$datanya);
	}
	
	function delete_pendidikan_formal($pend_id){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_karyawan_formal`
				WHERE mid(md5(tbl_karyawan_formal.formal_id),9,7) = '".$pend_id."'";
		
		$result = $this->db->query($sql);
		return $result;
	}
	
	function delete_pendidikan_non_formal($pend_id){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_karyawan_non_formal`
				WHERE mid(md5(tbl_karyawan_non_formal.non_formal_id),9,7) = '".$pend_id."'";

		$result = $this->db->query($sql);
		return $result;
	}

	//organisasi
	function organisasi_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(organisasi_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_karyawan_organisasi`
				where left(organisasi_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_organisasi($id=NULL,$org_id=NULL){
		$sql = "SELECT tbl_karyawan_organisasi.*,
					   mid(md5(tbl_karyawan_organisasi.organisasi_id),9,7) as org_id
				FROM `db_ptiik_apps`.`tbl_karyawan_organisasi`
				WHERE 1";
		if($id){
			$sql .= " AND mid(md5(tbl_karyawan_organisasi.karyawan_id),9,7) = '".$id."'";
		}
		if($org_id){
			$sql .= " AND mid(md5(tbl_karyawan_organisasi.organisasi_id),9,7) = '".$org_id."'";
			$result = $this->db->getRow( $sql );
			return $result;
		}  
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function replace_organisasi($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_karyawan_organisasi',$datanya);
	}

	function delete_organisasi($id){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_karyawan_organisasi`
				WHERE mid(md5(tbl_karyawan_organisasi.organisasi_id),9,7) = '".$id."'";

		$result = $this->db->query($sql);
		return $result;
	}
	
}
?>