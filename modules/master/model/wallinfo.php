<?php
class model_wallinfo extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
				
	function read($id=NULL, $tgl=NULL,$page = NULL, $perpage = NULL) {
	
		if(! $page){
			$page = 1;
		}
		
		if(! $perpage){
			$perpage = 10;
		} 
		
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
					mid(md5(`tbl_wallinfo`.`wall_id`),5,5) as `id`,
					`tbl_wallinfo`.`wall_id`,
					`tbl_wallinfo`.`judul`,
					`tbl_wallinfo`.`keterangan`,
					`tbl_wallinfo`.`tgl_publish`,
					`tbl_wallinfo`.`is_publish`,
					`db_ptiik_apps`.`tbl_wallinfo`.`last_update`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					db_coms.`coms_user`.`name`
				FROM
				`db_ptiik_apps`.`tbl_wallinfo`
				Inner Join db_coms.`coms_user` ON `db_ptiik_apps`.`tbl_wallinfo`.`user` = db_coms.`coms_user`.`username`
				Left Join `db_ptiik_apps`.`tbl_karyawan` ON db_coms.`coms_user`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE 1=1
				";
		if($id){
			$sql=$sql . " AND mid(md5(`tbl_wallinfo`.`wall_id`),5,5)='".$id."'";
		}
		
		if($tgl){
			$sql=$sql . " AND `tbl_wallinfo`.`tgl_publish` <= '".$tgl." '  AND is_publish='1' ";
		}
		
		$sql= $sql . "ORDER BY `tbl_wallinfo`.`last_update` DESC LIMIT $offset, $perpage";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	function get_reg_wallinfo(){
		$sql="SELECT concat('".date('Ym')."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(wall_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_wallinfo WHERE left(wall_id, 6) ='".date('Ym')."'";		
		$result = $this->db->getRow( $sql );
		
		return $result->data;
	}
	
			
	function replace_info($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_wallinfo',$datanya);
	}
	
	function update_info($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_wallinfo',$datanya,$idnya);
	}
	
	function delete_info($datanya){
		if($this->db->delete('db_ptiik_apps`.`tbl_wallinfo', $datanya))		
			return true;
		else $this->error = $this->db->getLastError();
			return false;
	}
	
	function delete($id) {
		$result = $this->db->delete("db_ptiik_apps`.`tbl_wallinfo", array('wall_id'=>$id));
		if( !$result )
			$this->error = $this->db->getLastError();
		return $result;
	}
	
}