<?php
class model_document extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	/* folder */
	function read($parent=NULL, $karyawan=NULL) {		
		$sql = "SELECT MID( MD5( tbl_media_library_folder.id), 9, 7) as folderid,
						MID( MD5( tbl_media_library_folder.karyawan_id), 9, 7) as kar_id,
					   tbl_media_library_folder.*
				FROM db_ptiik_apps.tbl_media_library_folder
				WHERE 1
			   ";
		if($parent!=""){
			$sql .= " AND tbl_media_library_folder.parent_id = '".$parent."'";
		}
		else{
			$sql .= " AND tbl_media_library_folder.parent_id = '0'";
		}
		
		if($karyawan!=""){
			$sql .= " AND tbl_media_library_folder.karyawan_id = '".$karyawan."'";
		}
		$sql .= " ORDER BY tbl_media_library_folder.folder_name";
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function folder_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_media_library_folder";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function rename_folder($id=NULL, $folder_name=NULL, $aktif=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_media_library_folder
				SET folder_name = '".$folder_name."', is_available = '".$aktif."'
				WHERE id = '".$id."'";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function replace_folder($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_media_library_folder', $data);
	}
	
	//file
	function read_file($id=NULL, $file=NULL, $karyawanid=NULL) {		
		$sql = "SELECT MID( MD5( tbl_media_library.file_id), 9, 7) as id, tbl_media_library.*
				FROM db_ptiik_apps.tbl_media_library
				LEFT JOIN db_ptiik_apps.tbl_media_library_folder ON tbl_media_library_folder.id = tbl_media_library.folder_id
				WHERE 1
			   ";
	   
		if($id){
			if($karyawanid!=""){
		   		$sql .= " AND tbl_media_library_folder.karyawan_id = '".$karyawanid."'";
		    }
			$sql .= " AND tbl_media_library.folder_id = '".$id."' OR MID( MD5(tbl_media_library.folder_id), 9, 7) = '".$id."'";
		}
		else{
			if($file!=""){
				$sql .= " AND tbl_media_library.file_id = '".$file."'";
			}else{	
				$sql .= " AND tbl_media_library.folder_id = '0'";
			}
		}
		$sql .= " ORDER BY tbl_media_library.file_name";
		
		if($file!=""){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		// echo $sql;
		return $result;
	}
	
	function file_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_media_library";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_file($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_media_library', $data);
	}
	
	//delete
	function delete_file($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_media_library
				WHERE MID( MD5(tbl_media_library.file_id), 9, 7) = '".$id."'";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_id_from_parent($folder_id){
		$sql = "SELECT MID( MD5(tbl_media_library_folder.id), 9, 7) as id
				FROM db_ptiik_apps.tbl_media_library_folder
				WHERE 1";
		if($folder_id!=""){
			$sql .= " AND MID( MD5(tbl_media_library_folder.parent_id), 9, 7) = '".$folder_id."'";
		}
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function delete_file_by_folder($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_media_library
				WHERE MID( MD5(tbl_media_library.folder_id), 9, 7) = '".$id."'";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_folder($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_media_library_folder
				WHERE MID( MD5(tbl_media_library_folder.id), 9, 7) = '".$id."'";
		$result = $this->db->query( $sql );
		return $result;
	}
}
?>