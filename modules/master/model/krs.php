<?php
class model_krs extends model {
	private $coms;
	
	public function __construct() {
		parent::__construct();	
	}
	
	function get_tahun(){
		$sql = "SELECT 
					tbl_tahunakademik.tahun, 
					tbl_tahunakademik.is_ganjil, 
					tbl_tahunakademik.is_pendek, 
					tbl_tahunakademik.tahun_akademik 
				FROM db_ptiik_apps.tbl_tahunakademik
				ORDER BY tbl_tahunakademik.tahun_akademik DESC";
		return $this->db->query($sql);
	}
	
	function get_fakultas(){
		$sql = "SELECT tbl_fakultas.fakultas_id, tbl_fakultas.keterangan, tbl_fakultas.kode_fakultas kode
				FROM db_ptiik_apps.tbl_fakultas
				ORDER BY tbl_fakultas.keterangan";
		return $this->db->query($sql);
	}
	
	function get_prodi($fakultas=NULL){
		$sql = "SELECT tbl_prodi.prodi_id, tbl_prodi.keterangan
				FROM db_ptiik_apps.tbl_prodi
				WHERE tbl_prodi.fakultas_id = '$fakultas'";
		return $this->db->query($sql);
	}
	
	function get_mhs($angkatan=NULL, $prodi=NULL){
		$sql = "SELECT tbl_mahasiswa.nim, 
						tbl_mahasiswa.nama, 
						tbl_mahasiswa.prodi_id, 
						tbl_mahasiswa.angkatan, 
						tbl_mahasiswa.jenis_kelamin,
						MID(MD5(tbl_mahasiswa.mahasiswa_id),8,6) mahasiswa_id
				FROM db_ptiik_apps.tbl_mahasiswa
				WHERE tbl_mahasiswa.prodi_id = '$prodi'
					AND tbl_mahasiswa.angkatan = '$angkatan'
				ORDER BY tbl_mahasiswa.nama";
		return $this->db->query($sql);
	}
	
	function get_krs($mhs=NULL, $tahun=NULL){
		$sql = "SELECT tbl_namamk.keterangan, tbl_krs.kelas, tbl_matakuliah.kode_mk, tbl_matakuliah.sks, tbl_krs.inf_huruf, tbl_krs.inf_bobot
				FROM db_ptiik_apps.tbl_krs
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_krs.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				WHERE MID(MD5(tbl_krs.mahasiswa_id),8,6) = '$mhs'
					AND tbl_krs.inf_semester = '$tahun'";
					
		return $this->db->query($sql);
	}
	
	function get_detail_mhs($mhs=NULL){
		$sql = "SELECT 
					tbl_mahasiswa.nim, 
					tbl_mahasiswa.nama, 
					tbl_mahasiswa.prodi_id, 
					tbl_mahasiswa.angkatan, 
					tbl_fakultas.kode_fakultas
				FROM db_ptiik_apps.tbl_mahasiswa
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				INNER JOIN db_ptiik_apps.tbl_fakultas ON tbl_fakultas.fakultas_id = tbl_prodi.fakultas_id
				WHERE MID(MD5(tbl_mahasiswa.mahasiswa_id),8,6) = '$mhs'";
		return $this->db->getRow($sql);
	}
	
}
?>