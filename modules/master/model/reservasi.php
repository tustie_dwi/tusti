<?php
class model_reservasi extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function save_jadwal($data){
		if(! $this->db->replace('db_ptiik_apps`.`tbl_pemakaian_ruang',$data)) echo mysql_error();
	}
	
	function save_detail($data){
		if(! $this->db->replace('db_ptiik_apps`.`tbl_pemakaian_ruang_detail',$data)) echo mysql_error();
	}
	
	function get_jadwal_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jadwal_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_pemakaian_ruang WHERE left(jadwal_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_detail_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(detail_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_pemakaian_ruang_detail WHERE left(detail_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_tahun_akademik(){
		$sql = "SELECT tbl_tahunakademik.tahun_akademik data
				FROM db_ptiik_apps.tbl_tahunakademik
				WHERE is_aktif = '1'";
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_ruang($cabang, $fak, $ruang){
		$sql = "SELECT tbl_ruang.ruang_id, tbl_ruang.kode_ruang, tbl_ruang.keterangan
				FROM db_ptiik_apps.tbl_ruang
				WHERE tbl_ruang.cabang_id = '$cabang' AND tbl_ruang.fakultas_id = '$fak'
				";
		
		if($ruang != ''){
			$sql .= " AND tbl_ruang.kode_ruang = '$ruang'";
		}
		
		$sql .= " ORDER BY tbl_ruang.kode_ruang";
				
		return $this->db->query($sql);
	}
	
	function get_jeniskegiatan(){
		$sql = "SELECT tbl_jeniskegiatan.jenis_kegiatan_id, tbl_jeniskegiatan.keterangan
				FROM db_ptiik_apps.tbl_jeniskegiatan";
		return $this->db->query($sql);
	}
	
	function get_kegiatan($tgl_mulai, $tgl_selesai, $ruang){
		/*$sql ="SELECT DISTINCT
					tbl_pemakaian_ruang.kegiatan,
					tbl_pemakaian_ruang_detail.ruang,
					tbl_pemakaian_ruang_detail.jam_mulai,
					tbl_pemakaian_ruang_detail.jam_selesai,
					tbl_pemakaian_ruang.tgl_mulai,
					tbl_pemakaian_ruang.tgl_selesai
				FROM db_ptiik_apps.tbl_pemakaian_ruang
				INNER JOIN db_ptiik_apps.tbl_pemakaian_ruang_detail ON tbl_pemakaian_ruang.jadwal_id = tbl_pemakaian_ruang_detail.jadwal_id
				WHERE  (DATE_FORMAT(tbl_pemakaian_ruang.tgl_mulai, '%Y-%m-%d') <= '$tgl_selesai'
					AND DATE_FORMAT(tbl_pemakaian_ruang.tgl_selesai, '%Y-%m-%d') >= '$tgl_mulai' )
				";
				
		// $sql .= " AND '$tanggal' BETWEEN DATE(tbl_pemakaian_ruang.tgl_mulai) AND DATE(tbl_pemakaian_ruang.tgl_selesai)";
		if($ruang){
			$sql .= " AND tbl_pemakaian_ruang_detail.ruang = '$ruang'";
		}*/
		$sql="SELECT DISTINCT
					tbl_pemakaian_ruang.kegiatan,
					tbl_pemakaian_ruang_detail.ruang,
					tbl_pemakaian_ruang_detail.jam_mulai,
					tbl_pemakaian_ruang_detail.jam_selesai,
					tbl_pemakaian_ruang_detail.tgl as tgl_mulai,
					tbl_pemakaian_ruang_detail.tgl as tgl_selesai
				FROM db_ptiik_apps.tbl_pemakaian_ruang
				LEFT JOIN db_ptiik_apps.tbl_pemakaian_ruang_detail ON tbl_pemakaian_ruang.jadwal_id = tbl_pemakaian_ruang_detail.jadwal_id 	
		WHERE  (DATE(tbl_pemakaian_ruang_detail.tgl) <= '$tgl_selesai'
					AND DATE(tbl_pemakaian_ruang_detail.tgl) >= '$tgl_mulai') ";
		if($ruang){
			$sql .= " AND tbl_pemakaian_ruang_detail.ruang = '$ruang'";
		}
		//var_dump( $sql);
		return $this->db->query($sql);
	}
	
	//approval
	
	function get_reservasi(){
		$sql = "SELECT 
					MID(MD5(tbl_pemakaian_ruang.jadwal_id),8,6) jadwal_id,
					tbl_pemakaian_ruang.kegiatan
				FROM db_ptiik_apps.tbl_pemakaian_ruang
				ORDER BY tbl_pemakaian_ruang.last_update DESC";
		return $this->db->query($sql);
	}
	
	function get_reservasi_detail($jadwal){
		$sql = "SELECT 
					tbl_pemakaian_ruang.kegiatan,
					DATE(tbl_pemakaian_ruang.tgl_mulai) tgl_mulai,
					DATE(tbl_pemakaian_ruang.tgl_selesai) tgl_selesai,
					tbl_pemakaian_ruang.jam_mulai,
					tbl_pemakaian_ruang.jam_selesai,
					tbl_pemakaian_ruang.catatan,
					tbl_jeniskegiatan.keterangan jenis_kegiatan,
					tbl_pemakaian_ruang.is_valid
				FROM db_ptiik_apps.tbl_pemakaian_ruang
				LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_pemakaian_ruang.jenis_kegiatan_id
				WHERE MID(MD5(tbl_pemakaian_ruang.jadwal_id),8,6) = '$jadwal'";
		return $this->db->getRow( $sql );
	}
	
	function approve($jadwal, $init){
		$sql = "UPDATE db_ptiik_apps.tbl_pemakaian_ruang SET is_valid = '$init' WHERE MID(MD5(tbl_pemakaian_ruang.jadwal_id),8,6) = '$jadwal'";
		$this->db->query($sql);
	}
	
	function get_ruangan_detail($jadwal){
		$sql = "SELECT 
					SUBSTR(tbl_pemakaian_ruang_detail.jam_mulai,1,5) jam_mulai,
					SUBSTR(tbl_pemakaian_ruang_detail.jam_selesai,1,5) jam_selesai,
					tbl_ruang.keterangan
				FROM db_ptiik_apps.tbl_pemakaian_ruang_detail
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_ruang.ruang_id = tbl_pemakaian_ruang_detail.ruang
				WHERE MID(MD5(tbl_pemakaian_ruang_detail.jadwal_id),8,6) = '$jadwal'
				ORDER BY tbl_ruang.keterangan, tbl_pemakaian_ruang_detail.jam_mulai";
		return $this->db->query($sql);
	}
	
	
	
	
	
	
	
	
	
	
	
}
?>