<?php
class model_jabatan extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL,$param=NULL,$parent=NULL,$parentid=NULL){
		$sql = "SELECT 
				mid(md5(`tbl_master_jabatan`.`jabatan_id`),6,6) as `jabatan_id`,
				`tbl_master_jabatan`.`jabatan_id` as `jabatanid`,
				`tbl_master_jabatan`.`keterangan`,
				mid(md5(`tbl_master_jabatan`.`parent_id`),6,6) as `parent_id`,
				`tbl_master_jabatan`.`parent_id` as `parentid`,
				(SELECT a.`keterangan` FROM `db_ptiik_apps`.`tbl_master_jabatan`as a WHERE a.`jabatan_id` = `tbl_master_jabatan`.`parent_id` ) as `parent_keterangan`
				FROM `db_ptiik_apps`.`tbl_master_jabatan` 
				WHERE 1";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_master_jabatan`.`jabatan_id`),6,6)='".$id."' ";
		}
		
		if($parentid){
			$sql=$sql . " AND mid(md5(`tbl_master_jabatan`.`parent_id`),6,6)='".$parentid."' ";
		}
		
		if($parent=='parent'){
			$sql=$sql . " AND `tbl_master_jabatan`.`parent_id` = '0' ";
		}
		if($parent=='child'){
			$sql=$sql . " AND `tbl_master_jabatan`.`parent_id` != '0' ";
		}
		
		if($param=='md5'){
			$result = $this->db->getRow( $sql );	
			return $result->jabatanid;	
		}else{
			$result = $this->db->query( $sql );	
			return $result;	
		}
		
	}

	function get_reg_number(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jabatan_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_master_jabatan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_jabatan($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_master_jabatan',$datanya);
	}
	
	function clean(){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_master_jabatan` WHERE 1";
		$this->db->query( $sql );	
	}
	
}
?>