<?php
class model_honor extends model {
	private $coms;
	public function __construct() {
		parent::__construct();	
	}
	
	function cek_dosen_titip($karyawana_id=NULL){
		$sql = "SELECT tbl_keu_dosen_titip.titip_ke
				FROM db_ptiik_apps.tbl_keu_dosen_titip
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_keu_dosen_titip.tahun_akademik 
					AND tbl_tahunakademik.is_aktif = '1' AND  tbl_keu_dosen_titip.potong='0' 
				WHERE tbl_keu_dosen_titip.titip_ke = '$karyawana_id'";
		
		$data = $this->db->query($sql);
		if($data) return "potong";
		else return "tidak";
	}
	
	function get_periode_bayar(){
		$sql = "SELECT DISTINCT
				tbl_keu_bayar.bayar_id,
				tbl_keu_bayar.tgl_mulai,
				tbl_keu_bayar.tgl_selesai
				FROM
				db_ptiik_apps.tbl_keu_bayar
				WHERE is_proses='main' ";
		return $this->db->query($sql);

	}
	
	function get_karyawan_term($term){
		$sql ="select CONCAT(gelar_awal,' ', nama,' ', gelar_akhir) as value,
				karyawan_id, CONCAT(gelar_awal,' ', nama,' ', gelar_akhir) as label, 
				foto as icon  from db_ptiik_apps.tbl_karyawan
				WHERE CONCAT(gelar_awal, nama, gelar_akhir) like '%".$term."%'
				ORDER BY nama ASC limit 0,5 ";
		return $this->db->query($sql);
	}
	function get_agenda_term ($term){
		$sql ="select judul as value,
				agenda_id, mid(md5(agenda_id),6,6) as agendaid, judul as label
				from db_ptiik_apps.tbl_agenda
				WHERE judul like '%".$term."%'
				ORDER BY judul ASC limit 0,5 ";
		return $this->db->query($sql);
	}
	function get_agenda_peserta($agendaid){
		$sql ="select nama, karyawan_id, mahasiswa_id from db_ptiik_apps.tbl_agenda_peserta
				where mid(md5(agenda_id),6,6)='$agendaid'";
		return $this->db->query($sql);
	}
	
	function get_karyawan(){
		$sql = "SELECT tbl_karyawan.nik as id, tbl_karyawan.nama as value
				FROM db_ptiik_apps.tbl_karyawan
				ORDER BY tbl_karyawan.nama";
		return $this->db->query($sql);
	}
	
	function get_mk_praktikum($mkid=NULL){
		$sql = "SELECT DISTINCT
				db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
				db_ptiik_apps.tbl_jadwalmk.is_praktikum
				FROM
				db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id
				WHERE db_ptiik_apps.tbl_jadwalmk.is_praktikum='1' AND db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id='$mkid'";
		
		return $this->db->getRow($sql);
	}
	
	function get_tarif($golongan=NULL, $pns=NULL, $pendidikan=NULL, $strata=NULL){
		$sql = "SELECT
				tbl_keu_conf.potongan,
				tbl_keu_conf.tarif
				FROM
				db_ptiik_apps.tbl_keu_conf WHERE 1 ";
		 $sql.= " AND golongan='$golongan' ";
		 $sql.= " AND is_pns='$pns' ";
		 $sql.= " AND jenjang_pendidikan='$pendidikan' ";
		if($strata) $sql.= " AND lcase(tbl_keu_conf.strata)='$strata' ";
		return $this->db->getRow($sql);
	
	}
	
	function get_proses_bayar($mulai=NULL){
		$sql = "SELECT bayar_id FROM db_ptiik_apps.tbl_keu_bayar WHERE '$mulai' BETWEEN tgl_mulai AND tgl_selesai";
		
		return $this->db->getRow($sql);
	}
	
	function get_rekap_absen_bayar_cetak($prodi=NULL, $mulai=NULL, $selesai=NULL, $kode=NULL, $mku=NULL, $isproses=NULL){
		$sql = "SELECT DISTINCT
				tbl_karyawan.nama,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir,
				tbl_karyawan.nik,
				tbl_karyawan.pendidikan_terakhir,
				tbl_karyawan.no_rekening as rekening,
				tbl_karyawan.is_nik,
				tbl_karyawan.is_mku,
				tbl_karyawan.karyawan_id,
				tbl_master_golongan.golongan,
				tbl_master_golongan.pangkat,
				tbl_master_golongan.inf_golongan,
				tbl_master_golongan.jabatan,
				tbl_keu_bayar.bayar_id,
				tbl_keu_bayar.is_proses,
				tbl_keu_bayar_detail.prodi AS prodi_id
				FROM
				db_ptiik_apps.tbl_karyawan
				INNER JOIN db_ptiik_apps.tbl_keu_bayar_detail ON tbl_karyawan.karyawan_id = tbl_keu_bayar_detail.titip_ke
				LEFT JOIN db_ptiik_apps.tbl_master_golongan ON tbl_karyawan.golongan =tbl_master_golongan.golongan 
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON tbl_keu_bayar.bayar_id = tbl_keu_bayar_detail.bayar_id 
				WHERE 1 ";
		if($mku):
			$sql.= " AND  (tbl_keu_bayar_detail.nama_mk  IN ('Bahasa Indonesia') OR (tbl_keu_bayar_detail.nama_mk LIKE '%Agama%'))";
		else:
			$sql.= "  AND  tbl_keu_bayar_detail.nama_mk  NOT IN ('Bahasa Indonesia') AND (tbl_keu_bayar_detail.nama_mk NOT LIKE '%Agama%') ";
		endif;
		if($prodi) $sql.= "  AND db_ptiik_apps.tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		
	
		$sql.= " ORDER BY db_ptiik_apps.tbl_karyawan.nama ASC";
		
		return $this->db->query($sql);
	}
	
	function get_rekap_absen_bayar_cetak_amplop($prodi=NULL, $mulai=NULL, $selesai=NULL, $kode=NULL, $mku=NULL,$isproses=NULL){
		
		$sql = "SELECT DISTINCT
				tbl_karyawan.nama,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir,
				tbl_karyawan.nik,
				tbl_karyawan.no_rekening as rekening,
				tbl_karyawan.pendidikan_terakhir,
				tbl_karyawan.is_nik,
				tbl_karyawan.is_mku,
				tbl_karyawan.karyawan_id,
				tbl_master_golongan.golongan,
				tbl_master_golongan.pangkat,
				tbl_master_golongan.inf_golongan,
				tbl_master_golongan.jabatan,
				tbl_keu_bayar.bayar_id,
				tbl_keu_bayar.is_proses,
				tbl_keu_bayar_detail.prodi AS prodi_id
				FROM
				db_ptiik_apps.tbl_karyawan
				INNER JOIN db_ptiik_apps.tbl_keu_bayar_detail ON tbl_karyawan.karyawan_id = tbl_keu_bayar_detail.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_master_golongan ON tbl_karyawan.golongan =tbl_master_golongan.golongan 
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON tbl_keu_bayar.bayar_id = tbl_keu_bayar_detail.bayar_id 
				WHERE 1 ";
		if($mku):
			$sql.= " AND  (tbl_keu_bayar_detail.nama_mk  IN ('Bahasa Indonesia') OR (tbl_keu_bayar_detail.nama_mk LIKE '%Agama%'))";
		else:
			$sql.= "  AND  tbl_keu_bayar_detail.nama_mk  NOT IN ('Bahasa Indonesia') AND (tbl_keu_bayar_detail.nama_mk NOT LIKE '%Agama%') ";
		endif;
		if($prodi) $sql.= "  AND db_ptiik_apps.tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		$sql.= " ORDER BY db_ptiik_apps.tbl_karyawan.nama ASC";
		
		return $this->db->query($sql);
	}
	
	
	function get_rekap_detail_bayar_cetak_amplop($prodi=NULL, $mulai=NULL, $selesai=NULL, $dosen=NULL, $kode=NULL, $mku=NULL,$isproses=NULL){
	
		$sql = "SELECT DISTINCT 
				db_ptiik_apps.tbl_keu_bayar_detail.nama_mk,
				db_ptiik_apps.tbl_keu_bayar_detail.kode_mk,
				db_ptiik_apps.tbl_keu_bayar_detail.prodi as `prodi_id`,
				db_ptiik_apps.tbl_keu_bayar_detail.kelas as `kelas_id`,
				db_ptiik_apps.tbl_keu_bayar_detail.sks,
				db_ptiik_apps.tbl_keu_bayar_detail.hr_id,
				db_ptiik_apps.tbl_keu_bayar_detail.titip_ke as karyawan_id,
				db_ptiik_apps.tbl_keu_bayar_detail.praktikum,
				db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id,
				db_ptiik_apps.tbl_keu_bayar_detail.hadir,
				db_ptiik_apps.tbl_keu_bayar_detail.satuan,
				db_ptiik_apps.tbl_keu_bayar_detail.pph,
				db_ptiik_apps.tbl_keu_bayar_detail.kewajiban,
				db_ptiik_apps.tbl_keu_bayar_detail.golongan,
				db_ptiik_apps.tbl_keu_bayar_detail.potongan,
				db_ptiik_apps.tbl_keu_bayar_detail.jenjang_pendidikan,
				db_ptiik_apps.tbl_keu_bayar_detail.total,
				db_ptiik_apps.tbl_keu_bayar_detail.total_bayar,
				db_ptiik_apps.tbl_keu_bayar_detail.jml_mgg,
				tbl_keu_bayar.is_proses,
				db_ptiik_apps.tbl_matakuliah.is_mku
				FROM
				db_ptiik_apps.tbl_keu_bayar_detail
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON db_ptiik_apps.tbl_keu_bayar_detail.bayar_id = db_ptiik_apps.tbl_keu_bayar.bayar_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				WHERE db_ptiik_apps.tbl_keu_bayar_detail.total_bayar > 0 
				 ";
		if($prodi) $sql.= "  AND tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($dosen) $sql.= "  AND tbl_karyawan.karyawan_id='$dosen' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		
		return $this->db->query($sql);
	}
	
	
	function get_rekap_detail_bayar_cetak($prodi=NULL, $mulai=NULL, $selesai=NULL, $dosen=NULL, $kode=NULL, $mku=NULL, $isproses=NULL){
		
	
		$sql = "SELECT DISTINCT 
				db_ptiik_apps.tbl_keu_bayar_detail.nama_mk,
				db_ptiik_apps.tbl_keu_bayar_detail.kode_mk,
				db_ptiik_apps.tbl_keu_bayar_detail.prodi as `prodi_id`,
				db_ptiik_apps.tbl_keu_bayar_detail.kelas as `kelas_id`,
				db_ptiik_apps.tbl_keu_bayar_detail.sks,
				db_ptiik_apps.tbl_keu_bayar_detail.hr_id,
				db_ptiik_apps.tbl_keu_bayar_detail.titip_ke as karyawan_id,
				db_ptiik_apps.tbl_keu_bayar_detail.praktikum,
				db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id,
				db_ptiik_apps.tbl_keu_bayar_detail.hadir,
				db_ptiik_apps.tbl_keu_bayar_detail.satuan,
				db_ptiik_apps.tbl_keu_bayar_detail.pph,
				db_ptiik_apps.tbl_keu_bayar_detail.kewajiban,
				db_ptiik_apps.tbl_keu_bayar_detail.golongan,
				db_ptiik_apps.tbl_keu_bayar_detail.potongan,
				db_ptiik_apps.tbl_keu_bayar_detail.jenjang_pendidikan,
				db_ptiik_apps.tbl_keu_bayar_detail.total,
				db_ptiik_apps.tbl_keu_bayar_detail.total_bayar,
				db_ptiik_apps.tbl_keu_bayar_detail.jml_mgg,
				tbl_keu_bayar.is_proses,
				db_ptiik_apps.tbl_matakuliah.is_mku
				FROM
				db_ptiik_apps.tbl_keu_bayar_detail
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON db_ptiik_apps.tbl_keu_bayar_detail.bayar_id = db_ptiik_apps.tbl_keu_bayar.bayar_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_keu_bayar_detail.titip_ke = db_ptiik_apps.tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				WHERE db_ptiik_apps.tbl_keu_bayar_detail.total_bayar > 0 
				 ";
		if($prodi) $sql.= "  AND tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($dosen) $sql.= "  AND tbl_karyawan.karyawan_id='$dosen' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		
		return $this->db->query($sql);
	}
	
	function get_rekap_absen_titip($prodi=NULL, $mulai=NULL, $selesai=NULL, $kode=NULL, $mku=NULL, $isproses=NULL){
		$sql = "SELECT DISTINCT
					tbl_karyawan.nama,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir,
					tbl_karyawan.nik,
					tbl_karyawan.pendidikan_terakhir,
					tbl_karyawan.is_nik,
					tbl_karyawan.is_mku,
					tbl_karyawan.karyawan_id,
					tbl_master_golongan.golongan,
					tbl_master_golongan.pangkat,
					tbl_master_golongan.inf_golongan,
					tbl_master_golongan.jabatan,
					tbl_keu_bayar.bayar_id,
					tbl_keu_bayar_detail.prodi AS prodi_id
				FROM
					db_ptiik_apps.tbl_karyawan
					INNER JOIN db_ptiik_apps.tbl_keu_bayar_detail ON tbl_karyawan.karyawan_id = tbl_keu_bayar_detail.titip_ke
					LEFT JOIN db_ptiik_apps.tbl_master_golongan ON tbl_karyawan.golongan =tbl_master_golongan.golongan 
					INNER JOIN db_ptiik_apps.tbl_keu_bayar ON tbl_keu_bayar.bayar_id = tbl_keu_bayar_detail.bayar_id
				WHERE
				db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id <> db_ptiik_apps.tbl_keu_bayar_detail.titip_ke
				 ";
		if($mku):
			$sql.= " AND  (tbl_keu_bayar_detail.nama_mk  IN ('Bahasa Indonesia') OR (tbl_keu_bayar_detail.nama_mk LIKE '%Agama%'))";
		else:
			$sql.= "  AND  tbl_keu_bayar_detail.nama_mk  NOT IN ('Bahasa Indonesia') AND (tbl_keu_bayar_detail.nama_mk NOT LIKE '%Agama%') ";
		endif;
		if($prodi) $sql.= "  AND db_ptiik_apps.tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		$sql.= " ORDER BY db_ptiik_apps.tbl_karyawan.nama ASC";
	
		return $this->db->query($sql);
	}
	
	function get_rekap_detail_absen_titip($prodi=NULL, $mulai=NULL, $selesai=NULL, $dosen=NULL, $kode=NULL, $mku=NULL,$isproses=NULL){
		
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_keu_bayar_detail.nama_mk,
					db_ptiik_apps.tbl_keu_bayar_detail.kode_mk,
					db_ptiik_apps.tbl_keu_bayar_detail.prodi AS prodi_id,
					db_ptiik_apps.tbl_keu_bayar_detail.kelas AS kelas_id,
					db_ptiik_apps.tbl_keu_bayar_detail.sks,
					db_ptiik_apps.tbl_keu_bayar_detail.hr_id,
					db_ptiik_apps.tbl_keu_bayar_detail.praktikum,
					db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id,
					db_ptiik_apps.tbl_keu_bayar_detail.hadir,
					db_ptiik_apps.tbl_keu_bayar_detail.satuan,
					db_ptiik_apps.tbl_keu_bayar_detail.pph,
					db_ptiik_apps.tbl_keu_bayar_detail.kewajiban,
					db_ptiik_apps.tbl_keu_bayar_detail.golongan,
					db_ptiik_apps.tbl_keu_bayar_detail.potongan,
					db_ptiik_apps.tbl_keu_bayar_detail.jenjang_pendidikan,
					db_ptiik_apps.tbl_keu_bayar_detail.total,
					db_ptiik_apps.tbl_keu_bayar_detail.total_bayar,
					db_ptiik_apps.tbl_keu_bayar_detail.jml_mgg,
					db_ptiik_apps.tbl_matakuliah.is_mku,
					tbl_keu_bayar.is_proses,
					a.nama as dosen_titip,
					tbl_keu_bayar_detail.karyawan_id,
					tbl_keu_bayar_detail.titip_ke,
					tbl_karyawan.nama as titip_ke_nama,
					a.gelar_awal,
					a.gelar_akhir,
					a.nik,a.is_nik
					FROM
					db_ptiik_apps.tbl_keu_bayar_detail
					INNER JOIN db_ptiik_apps.tbl_keu_bayar ON db_ptiik_apps.tbl_keu_bayar_detail.bayar_id = db_ptiik_apps.tbl_keu_bayar.bayar_id
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_keu_bayar_detail.titip_ke = db_ptiik_apps.tbl_karyawan.karyawan_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_karyawan AS a ON db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id = a.karyawan_id
					INNER JOIN db_ptiik_apps.tbl_keu_dosen_titip ON tbl_keu_bayar_detail.mkditawarkan_id = tbl_keu_dosen_titip.mkditawarkan_id AND tbl_keu_bayar_detail.kelas = tbl_keu_dosen_titip.kelas AND tbl_keu_bayar_detail.prodi = tbl_keu_dosen_titip.prodi
					WHERE
					db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id <> db_ptiik_apps.tbl_keu_bayar_detail.titip_ke
				 ";
		if($prodi) $sql.= "  AND tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($dosen) $sql.= "  AND tbl_karyawan.karyawan_id='$dosen' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		
		return $this->db->query($sql);
	}
	
	
	
	function get_rekap_absen_bayar($prodi=NULL, $mulai=NULL, $selesai=NULL, $kode=NULL,$isproses=NULL, $staff=NULL){
		$sql = "SELECT DISTINCT
				tbl_karyawan.nama,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir,
				tbl_karyawan.nik,
				tbl_karyawan.pendidikan_terakhir,
				tbl_karyawan.no_rekening as rekening,
				tbl_karyawan.is_nik,
				tbl_karyawan.is_mku,
				tbl_karyawan.karyawan_id,
				tbl_master_golongan.golongan,
				tbl_master_golongan.pangkat,
				tbl_master_golongan.inf_golongan,
				tbl_master_golongan.jabatan,
				tbl_keu_bayar.tgl_mulai,
				tbl_keu_bayar.tgl_selesai,
				tbl_keu_bayar.bayar_id,
				tbl_keu_bayar_detail.prodi AS prodi_id
				FROM
				db_ptiik_apps.tbl_karyawan
				INNER JOIN db_ptiik_apps.tbl_keu_bayar_detail ON tbl_karyawan.karyawan_id = tbl_keu_bayar_detail.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_master_golongan ON tbl_karyawan.golongan =tbl_master_golongan.golongan 
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON tbl_keu_bayar.bayar_id = tbl_keu_bayar_detail.bayar_id 
				WHERE 1 ";
		if($prodi) $sql.= "  AND db_ptiik_apps.tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		if($staff) $sql.= " AND tbl_karyawan.karyawan_id='$staff' ";
		$sql.= " ORDER BY db_ptiik_apps.tbl_karyawan.nama ASC";
		//echo $sql;
		return $this->db->query($sql);
	}
	
	function get_rekap_detail_bayar($prodi=NULL, $mulai=NULL, $selesai=NULL, $dosen=NULL, $kode=NULL,$isproses=NULL){
		
		$sql = "SELECT DISTINCT 
				db_ptiik_apps.tbl_keu_bayar_detail.nama_mk,
				db_ptiik_apps.tbl_keu_bayar_detail.kode_mk,
				db_ptiik_apps.tbl_keu_bayar_detail.prodi as `prodi_id`,
				db_ptiik_apps.tbl_keu_bayar_detail.kelas as `kelas_id`,
				db_ptiik_apps.tbl_keu_bayar_detail.sks,
				db_ptiik_apps.tbl_keu_bayar_detail.hr_id,
				db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id,
				db_ptiik_apps.tbl_keu_bayar_detail.praktikum,
				db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id,
				db_ptiik_apps.tbl_keu_bayar_detail.hadir,
				db_ptiik_apps.tbl_keu_bayar_detail.satuan,
				db_ptiik_apps.tbl_keu_bayar_detail.pph,
				db_ptiik_apps.tbl_keu_bayar_detail.kewajiban,
				db_ptiik_apps.tbl_keu_bayar_detail.golongan,
				db_ptiik_apps.tbl_keu_bayar_detail.jenjang_pendidikan,
				db_ptiik_apps.tbl_keu_bayar_detail.total,
				db_ptiik_apps.tbl_keu_bayar_detail.total_bayar,
				db_ptiik_apps.tbl_keu_bayar_detail.jml_mgg,
				tbl_keu_bayar.is_proses,
				db_ptiik_apps.tbl_matakuliah.is_mku
				FROM
				db_ptiik_apps.tbl_keu_bayar_detail
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON db_ptiik_apps.tbl_keu_bayar_detail.bayar_id = db_ptiik_apps.tbl_keu_bayar.bayar_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				WHERE
				1 ";
		if($prodi) $sql.= "  AND tbl_keu_bayar_detail.prodi='$prodi' ";
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($dosen) $sql.= "  AND tbl_keu_bayar_detail.karyawan_id='$dosen' ";
		if($kode) $sql.= "  AND tbl_keu_bayar.bayar_id ='$kode' ";
		if($isproses) $sql.= "  AND tbl_keu_bayar.is_proses ='$isproses' ";
		//echo $sql;
		return $this->db->query($sql);
	}
	function get_rekap_detail_bayar_wajib($mulai=NULL, $selesai=NULL, $dosen=NULL){
		
		$sql = "SELECT DISTINCT 				
				db_ptiik_apps.tbl_keu_bayar_detail.kewajiban
				FROM
				db_ptiik_apps.tbl_keu_bayar_detail
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON db_ptiik_apps.tbl_keu_bayar_detail.bayar_id = db_ptiik_apps.tbl_keu_bayar.bayar_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_keu_bayar_detail.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_keu_bayar_detail.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				WHERE tbl_keu_bayar_detail.kewajiban > 0 
				";
		
		if($mulai) $sql.= "  AND tbl_keu_bayar.tgl_mulai ='$mulai' ";
		if($selesai) $sql.= "  AND tbl_keu_bayar.tgl_selesai ='$selesai' ";
		if($dosen) $sql.= "  AND tbl_keu_bayar_detail.karyawan_id='$dosen' ";
		
		
		return $this->db->getRow($sql);
	}
	
	
	function get_rekap_absen($prodi=NULL, $mulai=NULL, $selesai=NULL, $kategori=NULL){
		$sql = "SELECT DISTINCT
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.nik,
					db_ptiik_apps.tbl_master_golongan.golongan,
					db_ptiik_apps.tbl_master_golongan.pangkat,
					db_ptiik_apps.tbl_master_golongan.jabatan,
					db_ptiik_apps.tbl_karyawan.pendidikan_terakhir,
					db_ptiik_apps.tbl_karyawan.is_nik,
					db_ptiik_apps.tbl_karyawan.is_mku,
					db_ptiik_apps.tbl_karyawan.home_base,
					db_ptiik_apps.tbl_karyawan.karyawan_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id
				FROM db_ptiik_apps.tbl_absen_dosen
				INNER JOIN db_ptiik_apps.tbl_pengampu ON tbl_absen_dosen.pengampu_id = tbl_pengampu.pengampu_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_master_golongan ON tbl_karyawan.golongan = db_ptiik_apps.tbl_master_golongan.golongan
				INNER JOIN db_ptiik_apps.tbl_absen ON db_ptiik_apps.tbl_absen_dosen.absen_id = db_ptiik_apps.tbl_absen.absen_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id 
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id
			INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id				
				WHERE tbl_jadwalmk.is_praktikum = '0' AND (db_ptiik_apps.tbl_absen_dosen.absendosen_id  NOT IN (SELECT absendosen_id FROM db_ptiik_apps.tbl_keu_bayar_mengajar))  
				AND  (tbl_namamk.keterangan NOT LIKE ('%Pancasila%') AND tbl_namamk.keterangan NOT IN ('Bahasa Indonesia') AND tbl_namamk.keterangan NOT LIKE ('%Agama%') )  ";
						
		if($prodi) $sql.= "  AND db_ptiik_apps.tbl_jadwalmk.prodi_id='$prodi' ";
		if($mulai) $sql.= "  AND db_ptiik_apps.tbl_absen.tgl >='$mulai' ";
		if($selesai) $sql.= "  AND db_ptiik_apps.tbl_absen.tgl <='$selesai' ";
		$sql.= " ORDER BY db_ptiik_apps.tbl_karyawan.nama ASC";
		
		return $this->db->query($sql);
	}
	
	function get_rekap_detail($prodi=NULL, $mulai=NULL, $selesai=NULL, $dosen=NULL){
		$sql = "SELECT
					COUNT(tbl_absen_dosen.absendosen_id) AS jml, 
					tbl_pengampu.mkditawarkan_id,
					tbl_jadwalmk.kelas kelas_id, 
					tbl_jadwalmk.prodi_id,
					tbl_namamk.keterangan AS nama_mk,
					tbl_matakuliah.kode_mk,
					tbl_matakuliah.is_mku, 
					tbl_matakuliah.sks,
					tbl_mkditawarkan.is_blok,
					tbl_matakuliah.strata,
					tbl_pengampu.karyawan_id
				FROM db_ptiik_apps.tbl_absen_dosen
				INNER JOIN db_ptiik_apps.tbl_absen ON tbl_absen_dosen.absendosen_id = db_ptiik_apps.tbl_absen.absen_id
				INNER JOIN db_ptiik_apps.tbl_pengampu ON tbl_absen_dosen.pengampu_id = tbl_pengampu.pengampu_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_pengampu.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
				WHERE tbl_jadwalmk.is_praktikum = '0' AND (db_ptiik_apps.tbl_absen_dosen.absendosen_id  NOT IN (SELECT absendosen_id FROM db_ptiik_apps.tbl_keu_bayar_mengajar)) ";
								
		if($prodi) $sql.= "  AND db_ptiik_apps.tbl_jadwalmk.prodi_id='$prodi' ";
		if($mulai) $sql.= "  AND db_ptiik_apps.tbl_absen.tgl >='$mulai' ";
		if($selesai) $sql.= "  AND db_ptiik_apps.tbl_absen.tgl <='$selesai' ";
		if($dosen) $sql.= "  AND db_ptiik_apps.tbl_pengampu.karyawan_id='$dosen' ";
		
		$sql.= "GROUP BY db_ptiik_apps.tbl_pengampu.karyawan_id,
				db_ptiik_apps.tbl_jadwalmk.kelas,
				db_ptiik_apps.tbl_jadwalmk.prodi_id,
				db_ptiik_apps.tbl_pengampu.mkditawarkan_id ";
		
				
		$sql.= " ORDER BY tbl_matakuliah.strata ASC, tbl_mkditawarkan.is_praktikum ASC, tbl_jadwalmk.kelas DESC, COUNT(tbl_absen_dosen.absendosen_id) ASC, tbl_matakuliah.sks DESC, db_ptiik_apps.tbl_namamk.keterangan ASC";
		// echo $sql . ' <br>';
		return $this->db->query($sql);
	}
	
	function get_absen_id($prodi=NULL, $mulai=NULL, $selesai=NULL, $dosen=NULL){
		$sql = "SELECT db_ptiik_apps.tbl_absen_dosen.absendosen_id
				FROM db_ptiik_apps.tbl_absen_dosen
				INNER JOIN db_ptiik_apps.tbl_absen ON db_ptiik_apps.tbl_absen_dosen.absen_id = db_ptiik_apps.tbl_absen.absen_id
				INNER JOIN db_ptiik_apps.tbl_pengampu ON tbl_absen_dosen.pengampu_id = tbl_pengampu.pengampu_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_pengampu.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					WHERE (db_ptiik_apps.tbl_absen_dosen.absendosen_id  NOT IN (SELECT absendosen_id FROM db_ptiik_apps.tbl_keu_bayar_mengajar)) ";
		if($prodi) $sql.= "  AND db_ptiik_apps.tbl_jadwalmk.prodi_id='$prodi' ";
		if($mulai) $sql.= "  AND db_ptiik_apps.tbl_absen.tgl >='$mulai' ";
		if($selesai) $sql.= "  AND db_ptiik_apps.tbl_absen.tgl <='$selesai' ";
		if($dosen) $sql.= "  AND db_ptiik_apps.tbl_pengampu.karyawan_id='$dosen' ";
		
		return $this->db->query($sql);
	}
	
	function get_keu_conf($id=NULL){
		$sql = "SELECT
					mid(md5(db_ptiik_apps.tbl_keu_conf.conf_id),9,7) as `id`,
					db_ptiik_apps.tbl_keu_conf.conf_id,
					db_ptiik_apps.tbl_keu_conf.golongan,
					db_ptiik_apps.tbl_keu_conf.jenjang_pendidikan,
					db_ptiik_apps.tbl_keu_conf.tarif,
					db_ptiik_apps.tbl_keu_conf.potongan,
					db_ptiik_apps.tbl_keu_conf.is_pns,
					db_ptiik_apps.tbl_keu_conf.kategori,
					db_ptiik_apps.tbl_master_golongan.pangkat,
					db_ptiik_apps.tbl_master_golongan.inf_golongan,
					db_ptiik_apps.tbl_master_golongan.jabatan
					FROM
					db_ptiik_apps.tbl_keu_conf
					INNER JOIN db_ptiik_apps.tbl_master_golongan ON db_ptiik_apps.tbl_keu_conf.golongan = db_ptiik_apps.tbl_master_golongan.golongan WHERE 1 		
				";
		if($id):
			$sql.= " AND (db_ptiik_apps.tbl_keu_conf.conf_id = '$id'  OR mid(md5(db_ptiik_apps.tbl_keu_conf.conf_id),9,7)='$id')";
			return $this->db->getRow($sql);
		else:
			$sql.= " ORDER BY tbl_keu_conf.conf_id DESC";
			return $this->db->query($sql);
		endif;
	}
	
	
	function get_golongan(){
		$sql = "SELECT
				db_ptiik_apps.tbl_master_golongan.golongan,
				db_ptiik_apps.tbl_master_golongan.pangkat,
				db_ptiik_apps.tbl_master_golongan.inf_golongan,
				db_ptiik_apps.tbl_master_golongan.jabatan
				FROM
				db_ptiik_apps.tbl_master_golongan 
				";
		return $this->db->query($sql);
	}
	
	function get_reg_bayar(){
		
		$kode = date("Ym");
				
		$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(bayar_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_keu_bayar WHERE left(bayar_id, 6)='".$kode."'";		
		$dt = $this->db->getRow( $sql );			
		$strresult = $dt->data;
		
		return $strresult;
	}
	function get_reg_bayar_detail(){
		
		$kode = date("Ym");
				
		$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(hr_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_keu_bayar_detail WHERE left(bayar_id, 6)='".$kode."'";		
		$dt = $this->db->getRow( $sql );			
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_keu_kategori_bayar(){
		$sql = "select * from db_ptiik_apps.tbl_keu_kategori_bayar where kelompok='non'";
		return $this->db->query($sql);
	}
	
	function replace_data_bayar($datanya){
		$this->db->replace('db_ptiik_apps`.`tbl_keu_bayar',$datanya);		
	}
	
	function hapus_data_bayar($bayarid){
		$this->db->query("delete from db_ptiik_apps.tbl_keu_bayar where mid(md5(bayar_id),6,6)='$bayarid'");
		$this->db->query("delete from db_ptiik_apps.tbl_keu_bayar_detail where mid(md5(bayar_id),6,6)='$bayarid'");
	}
	function hapus_data_bayar_detailByBayarId($bayar_id){
		$this->db->query("delete from db_ptiik_apps.tbl_keu_bayar_detail where bayar_id='$bayar_id'");
	}
	function hapus_data_bayar_detail($hr_id){
		$this->db->query("delete from db_ptiik_apps.tbl_keu_bayar_detail where hr_id='$hr_id'");
	}
	
	function replace_data_bayar_detail($datanya){
		$this->db->replace('db_ptiik_apps`.`tbl_keu_bayar_detail',$datanya);
	}
	
	function replace_data_bayar_absen($datanya){
		$this->db->replace('db_ptiik_apps`.`tbl_keu_bayar_mengajar',$datanya);		
	}
	
	
	function get_reg_conf($golongan=NULL, $jenjang=NULL, $kategori=NULL, $pns=NULL){
		$sql = "SELECT conf_id FROM db_ptiik_apps.tbl_keu_conf WHERE golongan ='$golongan' AND jenjang_pendidikan='$jenjang' AND kategori='$kategori' AND is_pns='$pns'";
		$result = $this->db->getRow( $sql );	
		
		if($result){			
			$strresult = $result->conf_id;
			
		}else{
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(conf_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_keu_conf WHERE left(conf_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );			
			$strresult = $dt->data;
		}
		
		
		return $strresult;
	}
	
	
	function  get_jenis_kegiatan($str=NULL){
		$sql = "SELECT jenis_kegiatan_id as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_jeniskegiatan  WHERE 1 = 1 ";
		
		if($str){
			$sql = $sql . " AND kategori = '".$str."' ";
		}
		
		$sql = $sql . " ORDER BY keterangan ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	/*master get nama kegiatan */
	function  get_nama_kegiatan($str=NULL){
		$sql = "SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id = '".$str."' ";
		$result = $this->db->query( $sql );
		
		foreach($result as $dt):
			$strvalue	= $dt->keterangan;
		endforeach;
		
		return $strvalue;
	}
	
	function get_kalender_akademik($isaktif=NULL){
		$sql = "SELECT
					tbl_kalenderakademik.kalender_id,
					tbl_kalenderakademik.jenis_kegiatan_id,
					tbl_kalenderakademik.tahun_akademik,
					tbl_kalenderakademik.tgl_mulai,
					tbl_kalenderakademik.tgl_selesai,
					tbl_kalenderakademik.is_aktif
			    FROM
					db_ptiik_apps.tbl_kalenderakademik
					INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_kalenderakademik.tahun_akademik = tbl_tahunakademik.tahun_akademik 
				WHERE 1 = 1 ";
		if($isaktif){
			$sql = $sql . "  AND tbl_tahunakademik.is_aktif = '1' ";
		}
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master konfigurasi */
	function read() {		
		$sql = "SELECT
					*
				FROM
					`db_ptiik_apps`.`tbl_config`					
				WHERE is_aktif='1'
				";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jenjang pendidikan */
	function  get_jenjang_pendidikan(){
		$sql = "SELECT `db_ptiik_apps`.`tbl_jenjangpendidikan`.`jenjang_pendidikan` FROM `db_ptiik_apps`.`tbl_jenjangpendidikan`";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jabatan */
	function  get_jabatan(){
		$sql = "SELECT `db_ptiik_apps`.`tbl_jabatan`.`jabatan_id`, `db_ptiik_apps`.`tbl_jabatan`.`keterangan` FROM `db_ptiik_apps`.`tbl_jabatan`";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester  */
	function get_semester(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					ORDER BY 
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik` DESC
					";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester aktif */
	function get_semester_aktif(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					WHERE
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` =  '1'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	function get_angkatan(){
		$sql = "SELECT distinct angkatan FROM db_ptiik_apps.tbl_mahasiswa ORDER BY angkatan DESC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master program studi atau jurusan*/
	function get_prodi($term=NULL){
		$sql= "SELECT prodi_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_prodi` WHERE keterangan like '%".$term."%' ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mahasiswa */
	function get_mahasiswa($term=NULL){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `value` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master dosen */
	function get_dosen($term=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif <> 'keluar' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master pengampu */
	function get_pengampu($term=NULL){
		$sql= "SELECT nama as `value`, karyawan_id as `id` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif <> 'keluar' 
				ORDER BY nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master nama mk */
	function get_namamk($term=NULL){
		$sql= "SELECT namamk_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_namamk` 
					WHERE keterangan like '%".$term."%' 
				ORDER BY keterangan ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master mk */
	function get_mk($term=NULL){
		$sql= "SELECT `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` as `id`, 
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ', `db_ptiik_apps`.`tbl_namamk`.`keterangan`,'(',`db_ptiik_apps`.`tbl_matakuliah`.`sks`,' sks)') as `value` FROM `db_ptiik_apps`.`tbl_matakuliah`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id` 
					WHERE concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
				ORDER BY `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` ASC limit 0,500 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/* master jam mulai*/
	function  get_jam_mulai($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_mulai as `value` FROM db_ptiik_apps.tbl_jam 
				WHERE jam_mulai like '%".$term."%'
				ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jam selesai*/
	function  get_jam_selesai($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_selesai as `value` FROM db_ptiik_apps.tbl_jam  WHERE jam_selesai like '%".$term."%' ORDER BY jam_selesai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master hari*/
	function  get_hari($term=NULL){
	$sql = "SELECT hari as `id`, hari as `value` FROM db_ptiik_apps.tbl_hari WHERE hari like '%".$term."%' ORDER BY db_ptiik_apps.tbl_hari.id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master ruang*/
	function  get_ruang($term=NULL){
		$sql = "SELECT ruang as `id`, concat(ruang,' - ',keterangan) as `value` FROM db_ptiik_apps.tbl_ruang WHERE keterangan like '%".$term."%' ORDER BY ruang ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kelas*/
	function  get_kelas($term=NULL){
		$sql = "SELECT kelas_id as `id`, kelas_id as `value` FROM db_ptiik_apps.tbl_kelas WHERE keterangan like '%".$term."%' ORDER BY kelas_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master nama dosen */
	function get_nama_dosen($id=NULL){
		$sql= "SELECT nama , karyawan_id ,nik FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE karyawan_id = '".$id."' ";
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
	/*master jadwal dosen */
	function get_jadwal_dosen(){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`
				FROM
				`db_ptiik_apps`.`vw_jadwaldosen`
				";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk ditawarkan */
	function get_mkditawarkan($ispraktikum=NULL,$semester=NULL,$term=NULL){
		$sql="SELECT
				`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` as `id`,
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ' ,`db_ptiik_apps`.`tbl_namamk`.`keterangan`) as `value`			
				FROM
				`db_ptiik_apps`.`tbl_mkditawarkan`
				Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
				Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`		
			  WHERE concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
			";
			
		if($ispraktikum!=""){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`is_praktikum`= '".$ispraktikum."' ";
		}
		if($semester!=""){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` ='".$semester."' ";
		}
				
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_nama_bulan($bln=NULL){
		switch($bln){
			   case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;    
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }
		}
		
		return $bln;
	}
	
	function replace_conf($datanya, $username) {
		$result	= $this->db->replace('db_ptiik_apps`.`tbl_keu_conf',$datanya);		
		return $result;
	}
	
	function update_titip($karyawan=NULL, $mk=NULL, $prodi=NULL, $kelas=NULL){
		$sqlx="UPDATE db_ptiik_apps.tbl_keu_bayar_detail
		set titip_ke = 
		CASE 
		  WHEN karyawan_id = '201301081116' AND prodi='ILKOM' THEN '130207072327'
			WHEN karyawan_id = '201301091130' AND prodi='SISKOM' THEN '140808084908'
		WHEN karyawan_id = '201301101100' AND prodi='ILKOM' THEN '130110053030'
		WHEN karyawan_id = '201301091130' AND prodi='ILKOM' THEN '201301081109'
		WHEN karyawan_id = '201301091150' AND prodi='ILKOM' THEN '130110042409'
		else karyawan_id
		END";
		
		$sql = "SELECT karyawan_id FROM db_ptiik_apps.tbl_keu_dosen_titip WHERE titip_ke='$karyawan' AND mkditawarkan_id='$mk' AND prodi='$prodi' AND kelas='$kelas' ";
		$row = $this->db->getRow($sql);
		if($row) $tid = $row->karyawan_id;
		else $tid=$karyawan;
		
		if($tid):		
			$sqlu= "UPDATE db_ptiik_apps.tbl_keu_bayar_detail
						set titip_ke = '$tid' WHERE karyawan_id='$karyawan'  AND mkditawarkan_id='$mk' AND prodi='$prodi' AND kelas='$kelas' "; 
			return $this->db->query( $sqlu );
		endif;
	}
	function get_list_kategori(){
		$sql = "select * from db_ptiik_apps.tbl_keu_kategori_bayar";
		return $this->db->query($sql);
	} 
	function get_keu_bayar ($bayarid=null){
		$sql = "select bayar_id, mid(md5(bayar_id),6,6) as bayarid, periode, tgl_mulai, tgl_selesai, 
				tbl_keu_bayar.kategori_bayar, tbl_keu_bayar.keterangan, tgl_bayar, total_non_pph, total_pph, total, kategori, is_proses,
				tbl_keu_kategori_bayar.keterangan as keteranganKategoriBayar, tbl_keu_kategori_bayar.kelompok
				from db_ptiik_apps.tbl_keu_bayar, db_ptiik_apps.tbl_keu_kategori_bayar where tbl_keu_bayar.kategori_bayar=tbl_keu_kategori_bayar.kategori_bayar
				and tbl_keu_kategori_bayar.kelompok='non'";
		if(!$bayarid)return $this->db->query($sql);
		else {
			$sql.= " and mid(md5(bayar_id),6,6)='$bayarid'";
			return $this->db->getRow($sql); 
		}
	}
	function get_keu_peserta ($bayar_id){
		$sql = "select nama, hr_id, bayar_id, karyawan_id, qty, satuan, keterangan from  db_ptiik_apps.tbl_keu_bayar_detail where bayar_id='$bayar_id'";
		return $this->db->query($sql);
	}
	function get_keu_pesertaDet ($bayarid){
		$sql = "select nama, hr_id, bayar_id, karyawan_id, qty, satuan, keterangan, total from  db_ptiik_apps.tbl_keu_bayar_detail where mid(md5(bayar_id),6,6)='$bayarid'";
		return $this->db->query($sql);
	}
	function simpan_kategori_bayar($data){
		$this->db->replace('db_ptiik_apps`.`tbl_keu_kategori_bayar',$data);		
	}
	function delete_kategori_bayar($kategori_bayar){
		$this->db->query("delete from db_ptiik_apps.tbl_keu_kategori_bayar where kategori_bayar='$kategori_bayar'");
	}
}
?>	