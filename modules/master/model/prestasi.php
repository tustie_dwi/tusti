<?php
class model_prestasi extends model {
	
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT
					mid(md5(tbl_prestasi.prestasi_id), 5,5) as `id`, 
					tbl_prestasi.prestasi_id,
					tbl_prestasi.jenis_id,
					tbl_prestasi.kategori,
					tbl_prestasi.tahun_akademik,
					tbl_prestasi.judul,
					tbl_prestasi.penyelenggara,
					tbl_prestasi.tgl_mulai,
					tbl_prestasi.tgl_selesai,
					tbl_prestasi.lokasi,
					tbl_prestasi.inf_prestasi,
					tbl_prestasi.tingkat,
					tbl_prestasi.jenis_prestasi,
					tbl_prestasi.keterangan,
					tbl_prestasi.inf_peserta,
					tbl_prestasi.link_berita,
					tbl_prestasi.`user_id` as user,
					tbl_prestasi.last_update,
					(
						SELECT group_concat(concat(mahasiswa_id,' - ',inf_nama))
						FROM
						`db_ptiik_apps`.`tbl_prestasi_peserta`
						 WHERE `db_ptiik_apps`.`tbl_prestasi`.`prestasi_id`=`db_ptiik_apps`.`tbl_prestasi_peserta`.`prestasi_id`
					) as `mhs`
				FROM
					db_ptiik_apps.tbl_prestasi 
				WHERE tbl_prestasi.kategori = 'mahasiswa'  
					";
		if($id){
			$sql = $sql . " AND (mid(md5(tbl_prestasi.prestasi_id), 5,5) = '".$id."' OR tbl_prestasi.prestasi_id = '".$id."') ";
		}
		
		$sql = $sql . " ORDER BY tbl_prestasi.last_update DESC";
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	
	function get_jenis_lomba(){
		$sql = "SELECT jenis_id as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_prestasi_jenis ORDER BY keterangan ASC";
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function addnol($string){
		if($string < 10){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	function get_reg_number($semester=NULL){
				
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(prestasi_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_prestasi ";		
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
				
		return $strresult;
	}
	
	function get_reg_mhs($kode=NULL, $mhs=NULL){
				
		$sql = "SELECT peserta_id FROM db_ptiik_apps.tbl_prestasi_peserta WHERE prestasi_id ='".$kode."' AND inf_nama = '".$mhs."' ";
		$result = $this->db->getRow( $sql );	
		
		if($result){
			$strresult = $result->peserta_id;
		}else{
			$str = substr($kode,0,10);
			
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(peserta_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_prestasi_peserta ";		
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
			
		}		
		
		return $strresult;
	}
	
	function get_mhs_id($mhs=NULL){
		$sql = "SELECT mahasiswa_id FROM db_ptiik_apps.tbl_mahasiswa WHERE nama= '".$mhs."' ";
		$result = $this->db->getRow( $sql );
		
		if($result){
			$strresult = $result->mahasiswa_id;
		}else{
			$strresult = "-";
		}
		
		return $strresult;		
	}
	
	function replace_peserta($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_prestasi_peserta',$datanya);
	}
	
	function replace_prestasi($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_prestasi',$datanya);
	}
	
	function delete_peserta($datanya) {
		return $this->db->delete("db_ptiik_apps`.`tbl_prestasi_peserta",$datanya);
		
		//return $result;
	}
	function delete_prestasi($datanya) {
		return $this->db->delete("db_ptiik_apps`.`tbl_prestasi",$datanya);
		
		//return $result;
	}
}