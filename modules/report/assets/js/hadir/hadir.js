$(document).ready(function(){
	// alert();
	
	$(".e9").select2();
});

$("#dosen").change(function(){
	var nilai = $("#dosen").select2("val");
	
	$("[name='tipe']").val("dosen");
	$("[name='nilai']").val(nilai);
	
	document.getElementById("form-hadir").submit();
});

$("#mkd").change(function(){
	var nilai = $("#mkd").select2("val");
	
	$("[name='tipe']").val("mkd");
	$("[name='nilai']").val(nilai);
	
	document.getElementById("form-hadir").submit();
});

$("#max").change(function(){
	$("[name='max']").val($(this).val());
	document.getElementById("form-hadir").submit();
});

$("#max").keyup(function(){
	$("[name='max']").val($(this).val());
});
