$(document).ready(function(){
	$(".e9").select2();
});

function kirim(){
	var karyawan = $("#karyawan").select2("val");
	var start = $("#start").select2("val");
	var end = $("#end").select2("val");
	var tahun = $("#tahun").val();
	
	if(karyawan == '-' || start == '-' || end == '-' || tahun == '') {
		alert("Isi data dengan lengkap");
	}
	else{
		if(parseInt(start) > parseInt(end)) {
			alert("Data periode bulan tidak valid");
		}
		else document.getElementById("form-hadir").submit();
	}
	
}
