$(document).ready(function(){
	$(".e9").select2();
	
	var view_by = $("input[name='view_by'][checked='checked']").val();	
	if(view_by=="tahun"){
		$("#tahun").show();
		$("#periode").hide();
	}
	else{
		$("#periode").show();
		$("#tahun").hide();
	}
	
	$("input[name='view_by']").change(function(){
		var view = $(this).val();
		if(view=="tahun"){
			$("#tahun").show();
			$("#periode").hide();
		}
		else{
			$("#periode").show();
			$("#tahun").hide();
		}
	});
	
	$("select[name='tahun_index']").change(function(){
		var form = document.createElement("form");
		var input = document.createElement("input");
		form.action = base_url + 'module/report/keu/pendapatan';
		form.method = "post"
		
		input.name = "tahun";
		input.value = $(this).val();
		form.appendChild(input);
		
		document.body.appendChild(form);
		form.submit();
	});
	
	$("select[name='bulan_mulai']").change(function(){
		var mulai = $(this).val();
		if(mulai=="0"){
			location.href = base_url + 'module/report/keu/pendapatan';
		}
		else{
			$("select[name='bulan_selesai']").select2('val', 0);
		}
		
	});
	
	$("select[name='bulan_selesai']").change(function(){
		var mulai = $("select[name='bulan_mulai']").val();
		var selesai = $(this).val();
		if(mulai!=="0" && selesai!=="0"){
			var form = document.createElement("form");
			var input = document.createElement("input");
			var input2 = document.createElement("input");
			form.action = base_url + 'module/report/keu/pendapatan';
			form.method = "post"
			
			input.name = "bulan_mulai";
			input.value = mulai;
			form.appendChild(input);
			
			input2.name = "bulan_selesai";
			input2.value = $(this).val();
			form.appendChild(input2);
			
			document.body.appendChild(form);
			form.submit();
		}
		else{
			alert("Silahkan pilih bulan terlebih dahulu!");
			location.href = base_url + 'module/report/keu/pendapatan';
		}
	});
	
	$("#export-data").click(function() {
		var viewby = $("input[name='view_by'][checked='checked']").val();
		if(viewby == "tahun"){
			var form = document.createElement("form");
			var input = document.createElement("input");
			var input2 = document.createElement("input");
			
			form.action = base_url + 'module/report/keu/pendapatan';
			form.method = "post"
			
			input.name = "tahun";
			input.value = $("select[name='tahun_index']").val();
			form.appendChild(input);
			
			input2.name = "js";
			input2.value = "js";
			form.appendChild(input2);
			
			document.body.appendChild(form);
			form.submit();
		}
		else if(view_by == "periode"){
			var mulai 	= $("select[name='bulan_mulai']").val();
			var selesai = $("select[name='bulan_selesai']").val();
			if(mulai!=="0" && selesai!=="0"){
				var form = document.createElement("form");
				var input = document.createElement("input");
				var input2 = document.createElement("input");
				var input3 = document.createElement("input");
				form.action = base_url + 'module/report/keu/pendapatan';
				form.method = "post"
				
				input.name = "bulan_mulai";
				input.value = mulai;
				form.appendChild(input);
				
				input2.name = "bulan_selesai";
				input2.value = selesai;
				form.appendChild(input2);
				
				input3.name = "js";
				input3.value = "js";
				form.appendChild(input3);
				
				document.body.appendChild(form);
				form.submit();
			}
			else{
				alert("Silahkan pilih bulan terlebih dahulu!");
				location.href = base_url + 'module/report/keu/pendapatan';
			}
		}
	});
});
