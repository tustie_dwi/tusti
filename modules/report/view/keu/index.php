<?php $this->head(); ?>
<div class="row">
	<div class="col-md-12">	
	<h2 class="title-page">Pendapatan</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#">Report</a></li>
		  <li><a href="<?php echo $this->location('module/report/keu') ?>">Keuangan</a></li>
		  <li class="active"><a href="<?php echo $this->location('module/report/keu/pendapatan') ?>">Pendapatan</a></li>
		</ol>
		<!-- <div class="breadcrumb-more-action">
			<a href="<?php echo $this->location('module/report/absen/unit') ?>" class="btn btn-primary"><i class="fa fa-briefcase"></i> Rekap Absen per Unit</a>
	    </div> -->
	
		<div class="row">
			<div class="col-md-5">
				<div class="panel panel-default">
					<div class="panel-heading">Pendapatan</div>
					<div class="panel-body">
						<div class="form-group">
						    <label class="radio-inline">
						     	<input type="radio" name="view_by" value="tahun" <?php if(!isset($bulan_mulai_lengkap)) echo 'checked="checked"'; else echo ''; ?> /> Tahun
						    </label>
						    <label class="radio-inline">
						     	<input type="radio" name="view_by" value="periode" <?php if(isset($bulan_mulai_lengkap)) echo 'checked="checked"'; else echo ''; ?> /> Periode
						    </label>
					    </div>
						<div class="form-group" id="tahun">
							<label class="control-label">Tahun</label>
						    <select class="e9 form-control" name="tahun_index">
						    	<option value='0'>Pilih Tahun</option>
						    	<?php
						    		if($thn) :
										foreach($thn as $key) {
											echo "<option value='".$key->tahun."'";
											if(isset($thn_select) && $thn_select==$key->tahun){
												echo "selected";
											}
											echo ">".$key->tahun ."</option>";
										}
									endif;
						    	?>
						    </select>
						</div>
						<div class="form-group" id="periode" style="display: none">
							<label >Periode</label>
							<div class="row">
								<div class="col-sm-6">
									<select class="e9 form-control" name="bulan_mulai">
								    	<option value='0'>Pilih Bulan Mulai</option>
								    	<?php
								    		if($bln) :
												foreach($bln as $key) {
													echo "<option value='".$key->bulan_lengkap."'";
													if(isset($bulan_mulai_lengkap) && $bulan_mulai_lengkap==$key->bulan_lengkap){
														echo "selected";
													}
													echo ">".$this->bulan($key->bulan)." ".$key->tahun."</option>";
												}
											endif;
								    	?>
								    </select>
								</div>
								<div class="col-sm-6">
									<select class="e9 form-control" name="bulan_selesai">
								    	<option value='0'>Pilih Bulan Selesai</option>
								    	<?php
								    		if($bln) :
												foreach($bln as $key) {
													echo "<option value='".$key->bulan_lengkap."'";
													if(isset($bulan_mulai_lengkap) && $bulan_selesai_lengkap==$key->bulan_lengkap){
														echo "selected";
													}
													echo ">".$this->bulan($key->bulan)." ".$key->tahun."</option>";
												}
											endif;
								    	?>
								    </select>
						    	</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-7">
				<div class="block-box">
					<?php 
						if(isset($thn_select)){
							$judul = "Pendapatan Tahun 2014";
						}
						elseif(isset($bulan_mulai_lengkap)){
							$judul = "Pendapatan Periode ".$this->bulan(substr($bulan_mulai_lengkap, 5,2))." ".substr($bulan_mulai_lengkap, 0,4)." s/d ".$this->bulan(substr($bulan_selesai_lengkap, 5,2))." ".substr($bulan_selesai_lengkap, 0,4);
						}
					?>
					<h4><?php if(isset($judul)) echo $judul ?></h4>
					<a download="mahasiswa.xls" class="btn btn-primary pull-right" id="export-data" href="javascript:" <?php if(!isset($karyawan)) echo 'style="display: none"'; ?>>Export to Excel</a>
					<div class="content">
						<?php 
						if(isset($karyawan)){
						?>
							<table class="table table-bordered" id="example">
								<thead>
									<tr>
										<th>Dosen</th>
										<?php if(isset($bulan_mulai_lengkap)){?>
											<?php foreach($kolom as $k){?>
												<th><?php echo $this->bulan($k->bulan)." ".$k->tahun ?></th>
											<?php } ?>
										<?php }else{ ?>
											<th>Pendapatan <?php echo $thn_select ?></th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php 
									foreach($karyawan as $p){
										if($p->nik == "") $nik = "-";
										else  $nik = $p->nik;
									?>
										<tr>
											<td>
												<?php echo $p->fullname."<br>" ?>
												<?php echo "<code>NIK. ".$nik."<code>" ?>
											</td>
											<?php if(isset($bulan_mulai_lengkap)){?>
													<?php foreach($pendapatan as $pen){?>
														<?php if($pen->fullname == $p->fullname){?>
															<td style="text-align: right">
																<?php echo $this->conversi_rupiah($pen->pendapatan) ?>
															</td>
														<?php } ?>
												<?php } ?>
											<?php }else{ ?>
												<td style="text-align: right">
													<?php echo $this->conversi_rupiah($p->pendapatan) ?>
												</td>
											<?php } ?>
											
										</tr>
									<?php
									} 
									?>
								</tbody>
							</table>
						<?php
						}else{
						?>
						<div class="well" align="center">
							No Data Available
						</div>
						<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->foot(); ?>