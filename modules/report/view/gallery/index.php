	<?php $this->head(); ?>
	<div class="col-md-12">	
			
			<div class="content-filter">
				<div class="row">
					<div class="mixit-content">
						<?php
						if(isset($post) && ($post)){
							foreach($post as $key):
								if($key->file_loc) $imgthumb = $this->config->file_url_view."/".$key->file_loc; 
								else $imgthumb = $this->config->default_thumb_web;
							?>
								<div class="col-sm-3 col-xs-6 mix <?php echo $key->content_category; ?>">							
									<a href="<?php echo $key->file_data; ?>" data-fancybox-type="iframe" class="fancybox content-gallery" title="<?php echo $key->file_title ;?>" target="_blank"> 
										<div class="gallery-item"> 
											<div class="img-holder"> 
												<img alt="<?php echo $key->file_title ;?>" src="<?php echo $imgthumb ;?>" class="img img-responsive img-gallery"> 
											</div> 
											<div class="name-holder"><?php echo $key->file_title ;?>										
											</div> 
										</div> 
									</a>
								</div>
							<?php
							endforeach;
						}
						?>						
					</div>
				</div>
			</div>

					
	</div>
	<?php $this->foot(); ?>
	