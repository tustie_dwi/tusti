<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Report</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Report</a></li>
		  <li class="active"><a href="#>">Daftar Hadir</a></li>
		</ol>
	</div>
</div>

<div id="name">
	<form id="form-hadir" action="<?php echo $this->location("module/report/hadir/dosen"); ?>" method="POST">
		<input name="max" type="hidden" value="<?php echo $max ?>" />
	</form>
	<div class="col-span-12">
		<div class="block-box">
			<div class="form-horizontal">
			  	<div class="form-group">
			  		<label class="col-sm-2 control-label">Pertemuan Maximal</label>
				    <div class="col-sm-4">
				    	<input id="max" class="form-control input-sm" type="number" value="<?php echo $max ?>">
				    </div>
				</div>
		  	</div>
			
			<?php
				if(! $hadir) {
					echo "<br><div class='well text-center'><h4>Daftar hadir ".$keterangan." <code>kosong</code></h4></div>";
				}
				else{
					echo '<div id="container" style="height: 400px;"></div>';
				}
			?>
		</div>
	</div>
</div>

<?php
	$absen='';
	
	if($hadir){
		foreach ($hadir as $key) {
			if($tipe == 'dosen') {
				if(! isset($absen[$key->keterangan.$key->kelas.$key->prodi_id]) ){
					$absen[$key->keterangan.$key->kelas.$key->prodi_id]['keterangan'] = $key->keterangan . ' ('.$key->kelas.') '. $key->prodi_id;
					$absen[$key->keterangan.$key->kelas.$key->prodi_id]['hadir'] = $key->jml;
				}
			}
		}
		
	}
?>

<?php
	$kategori = '';
	$masuk = '';
	$masuk_kumulatif = 0;
	$kumulatif = '';
	$max_tmp = '';
	
	if($hadir){
		foreach($absen as $key){
			$kategori .= ", '" . $key['keterangan'] . "'";
			$masuk .= ", ". $key['hadir'];
			
			$masuk_kumulatif += $key['hadir'];
			$kumulatif .= ", ". $masuk_kumulatif;
			$max_tmp .= ", null";
		}
		$kategori = substr($kategori, 2);
		$masuk = substr($masuk, 2);
		$max_tmp = substr($max_tmp, 2);
	}
	
?>


<?php $this->foot(); ?>
<style type="text/css">
	#container {
		height: 400px; 
		min-width: 310px; 
		max-width: 800px;
		margin: 0 auto;
	}
</style>

<?php if($hadir) : ?>
<script type="text/javascript">
	
	$(function () {
	    $('#container').highcharts({
	
	        chart: {
	            type: 'column',
	            options3d: {
	                enabled: true,
	                alpha: 10,
	                beta: 20,
	                depth: 70
	            },
	            marginTop: 90,
            	marginRight: 40
	        },
			
	        title: {
	            text: 'Daftar Hadir <?php echo $keterangan ?>'
	        },
			subtitle: {
	            text: '<?php echo $fakultas ?>'
	        },
	        xAxis: {
	        	categories: [<?php echo $kategori ?>, 'Pertemuan Maximal'],
	        	labels:
                        {
                            enabled: false
                        }
	        },
	
	        yAxis: {
	            allowDecimals: false,
	            min: 0,
	            title: {
	                text: 'Jumlah Kehadiran'
	            }
	        },
	
	        tooltip: {
	            headerFormat: '<b>{point.key}</b><br>',
	            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
	        },
	
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                depth: 40
	            }
	        },
	
	        series: [{
	            name: 'Pertemuan Kuliah',
	            data: [<?php echo $masuk ?>, null],
	            stack: 'hadir',
	            color: '#2980b9'
	        },
	        {
	            name: 'Pertemuan Maximal',
	            data: [<?php echo $max_tmp . ', ' . $max ?>],
	            stack: 'hadir',
	            color: '#16a085'
	        }]
	    });
	});
</script>
<?php endif; ?>