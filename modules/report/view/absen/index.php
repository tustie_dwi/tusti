<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Report</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Report</a></li>
		  <li><a href="#>">Daftar Absen</a></li>
		  <li class="active"><a href="#>">Karyawan</a></li>
		</ol>
		
		<div class="breadcrumb-more-action">
			<a href="<?php echo $this->location('module/report/absen/unit') ?>" class="btn btn-primary"><i class="fa fa-briefcase"></i> Rekap Absen per Unit</a>
	    </div>
	</div>
</div>

<div id="name">
	<div class="col-span-12">
		<div class="col-md-4  block-box">
			<form class="form-horizontal" role="form" id="form-hadir" action="<?php echo $this->location("module/report/absen"); ?>" method="POST">
			  	<div class="form-group">
				  
				      <select class="form-control e9" id="karyawan" name="karyawan">
							<option value='-'>Pilih Karyawan</option>
							<?php
								if($dosen) :
									foreach($dosen as $key){
										echo "<option value='".$key->karyawan_id."|".$key->gelar_awal." ". $key->nama." ".$key->gelar_akhir."'>".$key->nama."</option>";
									}
								endif;				
							?>
						</select>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-6">
							<select class="form-control e9" id="start" name="start">
								<option value='-'>Pilih Bulan</option>
								<option value="01">Januari</option>
								<option value="02">Februari</option>
								<option value="03">Maret</option>
								<option value="04">April</option>
								<option value="05">Mei</option>
								<option value="06">Juni</option>
								<option value="07">Juli</option>
								<option value="08">Agustus</option>
								<option value="09">September</option>
								<option value="10">Oktober</option>
								<option value="11">Novemeber</option>
								<option value="12">Desemeber</option>
							</select>
						</div>
						
						<div class="col-md-6">
							<select class="form-control e9" id="end" name="end">
								<option value='-'>Pilih Bulan</option>
								<option value="01">Januari</option>
								<option value="02">Februari</option>
								<option value="03">Maret</option>
								<option value="04">April</option>
								<option value="05">Mei</option>
								<option value="06">Juni</option>
								<option value="07">Juli</option>
								<option value="08">Agustus</option>
								<option value="09">September</option>
								<option value="10">Oktober</option>
								<option value="11">Novemeber</option>
								<option value="12">Desemeber</option>
							</select>
						</div>
						</div>
				    </div>
				<div class="form-group">
			    	<input id="tahun" name="tahun" class="form-control" type="number" autocomplete="off" value="<?php echo $tahun ?>" max="<?php echo date('Y') ?>">
				</div>
				
				<div class="form-group">
					<a onclick="kirim()" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Cari</a>
				</div>
			  	
		  	</form>
			
		</div>
		<div class="col-md-8">
			
			<?php
				if(! $hadir) {
					echo "<div class='well text-center'>Daftar hadir ".$keterangan." <code>kosong</code></div>";
				}
				else{
					echo '<div id="container" style="height: 400px;"></div>';
				}
			?>
		</div>
	</div>
</div>

<?php
	$masuk = $ijin = $sakit = $alpha = $periode =  '';
	$max = 0;
	if($hadir){
		foreach ($hadir as $key) {
			$masuk .= ', '.$key->hadir;
			$ijin .= ', '.$key->ijin;
			$sakit .= ', '.$key->sakit;
			$alpha .= ', '.$key->alpha;
			
			$periode .= ', '. get_bulan($key->bulan);
			
			// $max += countDays($tahun, intval($key->bulan), array(0,6)); // 23
		}
		
		$masuk = substr($masuk, 2);
		$ijin = substr($ijin, 2);
		$sakit = substr($sakit, 2);
		$alpha = substr($alpha, 2);
		$periode = substr($periode, 2);
	}
	
	function get_bulan($init){
		switch ($init) {
			case '01' : return '"Jan"';break;
			case '02' : return '"Feb"';break;
			case '03' : return '"Mar"';break;
			case '04' : return '"Apr"';break;
			case '05' : return '"Mei"';break;
			case '06' : return '"Jun"';break;
			case '07' : return '"Jul"';break;
			case '08' : return '"Agt"';break;
			case '09' : return '"Sep"';break;
			case '10' : return '"Okt"';break;
			case '11' : return '"Nov"';break;
			case '12' : return '"Des"';break;
			default: return '"Jan"'; break;
		}
	}
	
	function countDays($year, $month, $ignore) {
	    $count = 0;
	    $counter = mktime(0, 0, 0, $month, 1, $year);
	    while (date("n", $counter) == $month) {
	        if (in_array(date("w", $counter), $ignore) == false) {
	            $count++;
	        }
	        $counter = strtotime("+1 day", $counter);
	    }
	    return $count;
	}
?>

<?php $this->foot(); ?>
<style type="text/css">
	#container {
		height: 400px; 
		min-width: 310px; 
		max-width: 800px;
		margin: 0 auto;
	}
</style>

<?php if($hadir) : ?>
<script type="text/javascript">


	$(document).ready(function(){
		<?php
			if($karyawan != '') {
				echo '$("#karyawan").select2("val","'.$karyawan.'");';
				echo '$("#start").select2("val","'.$start.'");';
				echo '$("#end").select2("val","'.$end.'");';
			}
		?>
	});
	
	
	
	var max = {
		<?php
			foreach ($hadir as $key) {
				echo get_bulan($key->bulan).' : '. countDays($tahun, intval($key->bulan), array(0,6)). ', ';
			}
		?>
		
		"-" : 0
	};
	
	$(function () {
		
			Highcharts.theme = {
			   colors: ["#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
				  "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
			   chart: {
				  backgroundColor: {
					 linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
					 stops: [
						[0, '#2a2a2b'],
						[1, '#3e3e40']
					 ]
				  },
				  style: {
					 fontFamily: "'Unica One', sans-serif"
				  },
				  plotBorderColor: '#606063'
			   },
			   title: {
				  style: {
					 color: '#E0E0E3',
					 textTransform: 'uppercase',
					 fontSize: '12px'
				  }
			   },
			   subtitle: {
				  style: {
					 color: '#E0E0E3',
					 textTransform: 'uppercase'
				  }
			   },
			   xAxis: {
				  gridLineColor: '#707073',
				  labels: {
					 style: {
						color: '#E0E0E3'
					 }
				  },
				  lineColor: '#707073',
				  minorGridLineColor: '#505053',
				  tickColor: '#707073',
				  title: {
					 style: {
						color: '#A0A0A3'

					 }
				  }
			   },
			   yAxis: {
				  gridLineColor: '#707073',
				  labels: {
					 style: {
						color: '#E0E0E3'
					 }
				  },
				  lineColor: '#707073',
				  minorGridLineColor: '#505053',
				  tickColor: '#707073',
				  tickWidth: 1,
				  title: {
					 style: {
						color: '#A0A0A3'
					 }
				  }
			   },
			   tooltip: {
				  backgroundColor: 'rgba(0, 0, 0, 0.85)',
				  style: {
					 color: '#F0F0F0'
				  }
			   },
			   plotOptions: {
				  series: {
					 dataLabels: {
						color: '#B0B0B3'
					 },
					 marker: {
						lineColor: '#333'
					 }
				  },
				  boxplot: {
					 fillColor: '#505053'
				  },
				  candlestick: {
					 lineColor: 'white'
				  },
				  errorbar: {
					 color: 'white'
				  }
			   },
			   legend: {
				  itemStyle: {
					 color: '#E0E0E3'
				  },
				  itemHoverStyle: {
					 color: '#FFF'
				  },
				  itemHiddenStyle: {
					 color: '#606063'
				  }
			   },
			   credits: {
				  style: {
					 color: '#666'
				  }
			   },
			   labels: {
				  style: {
					 color: '#707073'
				  }
			   },

			   drilldown: {
				  activeAxisLabelStyle: {
					 color: '#F0F0F3'
				  },
				  activeDataLabelStyle: {
					 color: '#F0F0F3'
				  }
			   },

			   navigation: {
				  buttonOptions: {
					 symbolStroke: '#DDDDDD',
					 theme: {
						fill: '#505053'
					 }
				  }
			   },

			   // scroll charts
			   rangeSelector: {
				  buttonTheme: {
					 fill: '#505053',
					 stroke: '#000000',
					 style: {
						color: '#CCC'
					 },
					 states: {
						hover: {
						   fill: '#707073',
						   stroke: '#000000',
						   style: {
							  color: 'white'
						   }
						},
						select: {
						   fill: '#000003',
						   stroke: '#000000',
						   style: {
							  color: 'white'
						   }
						}
					 }
				  },
				  inputBoxBorderColor: '#505053',
				  inputStyle: {
					 backgroundColor: '#333',
					 color: 'silver'
				  },
				  labelStyle: {
					 color: 'silver'
				  }
			   },

			   navigator: {
				  handles: {
					 backgroundColor: '#666',
					 borderColor: '#AAA'
				  },
				  outlineColor: '#CCC',
				  maskFill: 'rgba(255,255,255,0.1)',
				  series: {
					 color: '#7798BF',
					 lineColor: '#A6C7ED'
				  },
				  xAxis: {
					 gridLineColor: '#505053'
				  }
			   },

			   scrollbar: {
				  barBackgroundColor: '#808083',
				  barBorderColor: '#808083',
				  buttonArrowColor: '#CCC',
				  buttonBackgroundColor: '#606063',
				  buttonBorderColor: '#606063',
				  rifleColor: '#FFF',
				  trackBackgroundColor: '#404043',
				  trackBorderColor: '#404043'
			   },

			   // special colors for some of the
			   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
			   background2: '#505053',
			   dataLabelsColor: '#B0B0B3',
			   textColor: '#C0C0C0',
			   contrastTextColor: '#F0F0F3',
			   maskColor: 'rgba(255,255,255,0.3)'
			};

			// Apply the theme
			Highcharts.setOptions(Highcharts.theme);

	    $('#container').highcharts({
	
	        chart: {
	            type: 'column',
	            options3d: {
	                enabled: true,
	                alpha: 10,
	                beta: 20,
	                depth: 60
	            },
	            marginTop: 90,
            	marginRight: 40
	        },
			
	        title: {
	            text: 'Daftar Hadir : <?php echo $keterangan ?>'
	        },
			subtitle: {
	            text: 'Rekap Absensi Karyawan'
	        },
	        xAxis: {
	        	categories: [<?php echo $periode ?>],
	        	labels:
				{
					enabled: true
				}
	        },
	
	        yAxis: {
	            allowDecimals: false,
	            min: 0,
	            title: {
	                text: 'Jumlah Kehadiran'
	            }
	        },
	
	        tooltip: {
	            headerFormat: '<b>{point.key}</b><br>',
	            pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y}'
	        },
	
	        plotOptions: {
			 column: {
				stacking: 'normal',
                depth: 40,
				dataLabels:{
					enabled:true,
					formatter:function() {
						var pcnt = (this.y / max[this.x] ) * 100;
						
 						if(pcnt > 0) return '<span style="color:#fff">'+Highcharts.numberFormat(pcnt) + '%</span>';
					}
				}
            }
	           
				
	        },
	
	        series: [
		        {
		            name: 'Masuk',
					type: 'column',
		            data: [<?php echo $masuk ?>],
		            stack: 'masuk',
		            color: '#2980b9'
		        },
		        {
		            name: 'Ijin',
					type: 'column',
		            data: [<?php echo $ijin ?>],
		            stack: 'ijin',
		            color: '#f39c12'
		        },
		        {
		            name: 'Sakit',
					type: 'column',
		            data: [<?php echo $sakit ?>],
		            stack: 'sakit',
		            color: '#27ae60'
		        },
		        {
		            name: 'Alpha',
					type: 'column',
		            data: [<?php echo $alpha ?>],
		            stack: 'alpha',
		            color: '#e74c3c'
		        }
	        ]
	    });
	});
</script>
<?php endif; ?>