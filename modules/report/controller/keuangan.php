<?php
class report_keuangan extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mrekap = new model_keuangan();

		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->coms->add_script('highchart/highcharts.js');
		$this->coms->add_script('highchart/highcharts-3d.js');
		$this->coms->add_script('highchart/modules/exporting.js');
		
		$data['tahun'] = date('Y');
		$data['bulan'] = date('m');
			
		$this->view('keuangan/rekap_keuangan.php', $data);
	}
	
	function get_rekap_tahun(){
		if(! isset($_POST['tahun'])) $this->redirect('module/report/keuangan');
		$mrekap = new model_keuangan();
		
		$tahun = $_POST['tahun'] - 10;
		
		$data = $mrekap->rekap_tahun($tahun);
		$temp['tahun'] = array();
		
		$x['pph'] = array();
		$x['total'] = array();
		$x['non_pph'] = array();
		
		$walk = 0;
		for($i = $tahun; $i <= $_POST['tahun']; $i++){
			array_push($temp['tahun'], $i);
			
			$x['pph'][$walk] = 0;
			$x['total'][$walk] = 0;
			$x['non_pph'][$walk] = 0;
			
			$walk++;
		}
		
		if($data) :
			foreach($data as $key){
				$x['pph'][($key->tahun - $tahun)] = intval($key->total_pph);
				$x['total'][($key->tahun - $tahun)] = intval($key->total);
				$x['non_pph'][($key->tahun - $tahun)] = intval($key->total_non_pph);
			}
		endif;
		
		$temp['uang'] = array(
			array(
				'name' => 'PPH',
				'data' => $x['pph']
			),
			array(
				'name' => 'Non PPH',
				'data' => $x['non_pph']
			),
			array(
				'name' => 'Total',
				'data' => $x['total']
			)
		);
		
		echo json_encode($temp);		
	}
	
	function get_rekap_bulan(){
		if(! isset($_POST['tahun'])) $this->redirect('module/report/keuangan');
		$mrekap = new model_keuangan();
		
		$tahun = $_POST['tahun'];
		
		$data = $mrekap->get_rekap_bulan($tahun);
		
		$x['pph'] = array();
		$x['total'] = array();
		$x['non_pph'] = array();
		
		for($i=0; $i<12; $i++){
			$x['pph'][$i] = 0;
			$x['total'][$i] = 0;
			$x['non_pph'][$i] = 0;
		}
		
		if($data) :
			foreach($data as $key){
				$x['pph'][($key->bulan - 1)] = intval($key->total_pph);
				$x['total'][($key->bulan - 1)] = intval($key->total);
				$x['non_pph'][($key->bulan - 1)] = intval($key->total_non_pph);
			}
		endif;
		
		$temp = array(
			array(
				'name' => 'PPH',
				'data' => $x['pph']
			),
			array(
				'name' => 'Non PPH',
				'data' => $x['non_pph']
			),
			array(
				'name' => 'Total',
				'data' => $x['total']
			)
		);
		echo json_encode($temp);	
	}
}
?>