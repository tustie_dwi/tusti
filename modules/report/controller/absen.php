<?php
class report_absen extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		if($this->coms->authenticatedUser->role == 'mahasiswa') $this->redirect();
		if($this->coms->authenticatedUser->role == 'dosen') $this->redirect();
		
		if($this->coms->authenticatedUser->role == 'administrator'):
			$fakultas = $this->config->fakultas;
			$cabang	= $this->config->cabang;
		else:
			$fakultas = $this->coms->authenticatedUser->fakultas;
			$cabang	= $this->coms->authenticatedUser->cabang;
		endif;
		
		$mabsen = new model_absen();
		$this->add_script("js/absen/absen.js");
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->coms->add_script('highchart/highcharts.js');
		$this->coms->add_script('highchart/highcharts-3d.js');
		$this->coms->add_script('highchart/modules/exporting.js');
		
		$data['dosen'] = $mabsen->get_dosen($fakultas, $cabang);
		
		if(isset($_POST['karyawan'])){
			$bln_start = $_POST['start'];
			$bln_end = $_POST['end'];
			$tahun = $_POST['tahun'];
			
			$karyawan = explode("|", $_POST['karyawan']);
			
			$data['hadir'] = $mabsen->get_rekap_absen($karyawan[0], $tahun, $bln_start, $bln_end);
			$data['keterangan'] = $karyawan[1];
			$data['karyawan'] = $_POST['karyawan'];
			$data['tahun'] = $_POST['tahun'];
			$data['start'] = $_POST['start'];
			$data['end'] = $_POST['end'];
		}
		else{
			$data['hadir'] = '';
			$data['keterangan'] = '';
			$data['karyawan'] = '';
			$data['tahun'] = date('Y');
			$data['start'] = '';
			$data['end'] = '';
		}
		
		$data['nilai'] = '-'; 
		
		$this->view( 'absen/index.php', $data );
	}

	function unit(){
		if($this->coms->authenticatedUser->role == 'mahasiswa') $this->redirect();
		if($this->coms->authenticatedUser->role == 'dosen') $this->redirect();
		
		if($this->coms->authenticatedUser->role == 'administrator'):
			$fakultas = $this->config->fakultas;
			$cabang	= $this->config->cabang;
		else:
			$fakultas = $this->coms->authenticatedUser->fakultas;
			$cabang	= $this->coms->authenticatedUser->cabang;
		endif;
		
		$mabsen = new model_absen();
		$this->add_script("js/absen/absen.js");
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->coms->add_script('highchart/highcharts.js');
		$this->coms->add_script('highchart/highcharts-3d.js');
		$this->coms->add_script('highchart/modules/exporting.js');
		
		$data['unit'] = $mabsen->get_unit($fakultas, $cabang);
		
		if(isset($_POST['karyawan'])){
			$bln_start = $_POST['start'];
			$bln_end = $_POST['end'];
			$tahun = $_POST['tahun'];
			
			$karyawan = explode("|", $_POST['karyawan']);
			
			$data['hadir'] = $mabsen->get_absen_unit($karyawan[0], $tahun, $bln_start, $bln_end);
			$data['keterangan'] = $karyawan[1];
			$data['karyawan'] = $_POST['karyawan'];
			$data['tahun'] = $_POST['tahun'];
			$data['start'] = $_POST['start'];
			$data['end'] = $_POST['end'];
		}
		else{
			$data['hadir'] = '';
			$data['keterangan'] = '';
			$data['karyawan'] = '';
			$data['tahun'] = date('Y');
			$data['start'] = '';
			$data['end'] = '';
		}
		
		$data['nilai'] = '-'; 
		
		$this->view( 'absen/unit.php', $data );
	}
}
?>