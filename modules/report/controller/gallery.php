<?php
class report_gallery extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mpage = new model_page();
		
		$data['post'] = $mpage->read_slide("0","en",12,'video');
		/*$this->coms->add_script("ptiik/js/jquery-1.11.1.min.js");
		$this->coms->add_script("ptiik/js/bootstrap.min.js");
		$this->coms->add_script("ptiik/js/ptiik/js/owl.carousel.min.js");
		$this->coms->add_script("ptiik/js/jquery.fancybox.pack.js");
		$this->coms->add_script("ptiik/js/helpers/jquery.fancybox-media.js?v=1.0.6");
		$this->coms->add_script("ptiik/js/jquery.mixitup.min.js");
		$this->coms->add_style("ptiik/css/insideptiik.slider.min.css");
		$this->coms->add_style("ptiik/css/menuapps.slider.min.css");
		$this->coms->add_style("ptiik/css/jquery.fancybox.css");*/
		
		$this->view( 'gallery/index.php', $data );
	}

	
}
?>