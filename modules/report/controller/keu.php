<?php
class report_keu extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$this->redirect('module/report/keu/pendapatan');
	}
	
	function pendapatan(){
		$mkeu			= new model_keu();
		$role 			= $this->coms->authenticatedUser->role;
		
		if(isset($_POST['tahun']) && $_POST['tahun']!="0"){
			$tahun				= $_POST['tahun'];
			$data['karyawan']		= $mkeu->read('1',$tahun);
			$data['thn_select']	= $tahun;
		}
		
		if(isset($_POST['bulan_mulai']) && isset($_POST['bulan_selesai']) && $_POST['bulan_mulai']!="0" && $_POST['bulan_selesai']!="0"){
			$mulai							= $_POST['bulan_mulai'];
			$selesai						= $_POST['bulan_selesai'];
			
			$data['karyawan']				= $mkeu->read('', '', $mulai, $selesai);
			$data['pendapatan']				= $mkeu->read('pendapatan', '', $mulai, $selesai);
			$data['kolom']					= $mkeu->read('get_bulan', '', $mulai, $selesai);
			
			$data['bulan_mulai_lengkap']	= $mulai;
			$data['bulan_selesai_lengkap']	= $selesai;
		}
		
		$data['thn']	= $mkeu->read('get_tahun');
		$data['bln']	= $mkeu->read('get_bulan');
		
		$this->add_script('js/keu/keu.js');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('datepicker/js/moment-2.4.0.js');
		$this->coms->add_script('datepicker/js/bootstrap-datetimepicker.min.js');
		$this->coms->add_style('datepicker/css/bootstrap-datetimepicker.css');
		$this->coms->add_script('js/select2/select2.js');
		$this->coms->add_style('js/select2/select2.css');
		if(isset($_POST['js'])){
			$this->add_script('js/keu/jsread.js');
		}
		$this->view('keu/index.php', $data);
	}

	//===============fungsi tambahan===============//
	
	function conversi_rupiah($nominal){
		$angka = $nominal;
		$jumlah_desimal ="2";
		$pemisah_desimal =",";
		$pemisah_ribuan =".";
		
		return "Rp ".number_format($angka, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);
		//hasil : Rp 9.500.000,00
	}
	
	function bulan($isi){
		$bln = Array(12);
		$bln['01']="January";
		$bln['02']="February";
		$bln['03']="March";
		$bln['04']="April";
		$bln['05']="May";
		$bln['06']="June";
		$bln['07']="July";
		$bln['08']="August";
		$bln['09']="September";
		$bln['10']="October";
		$bln['11']="November";
		$bln['12']="December";
		
		return $bln[$isi];
	}
}
?>