<?php
class report_hadir extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		if($this->coms->authenticatedUser->role == 'mahasiswa') $this->redirect();
		if($this->coms->authenticatedUser->role == 'dosen') $this->redirect('module/report/hadir/dosen');
		
		if($this->coms->authenticatedUser->role=='administrator'):
			$fakultas = $this->config->fakultas;
			$cabang	= $this->config->cabang;
		else:
			$fakultas = $this->coms->authenticatedUser->fakultas;
			$cabang	= $this->coms->authenticatedUser->cabang;
		endif;
		
		$mhadir = new model_reporthadir();
		$this->add_script("js/hadir/hadir.js");
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->coms->add_script('highchart/highcharts.js');
		$this->coms->add_script('highchart/highcharts-3d.js');
		$this->coms->add_script('highchart/modules/exporting.js');
		
		$data['fakultas'] = $mhadir->get_detail_fakultas($fakultas);
		$data['mkd'] = $mhadir->get_mkd();
		$data['dosen'] = $mhadir->get_dosen($fakultas, $cabang);
		
		if(isset($_POST['tipe'])){
			$tmp = explode("|", $_POST['nilai']);
			
			if($_POST['tipe'] == 'dosen'){
				$data['keterangan'] = "Dosen : " .$tmp[1];
			}
			else{
				$data['keterangan'] = "Mata Kuliah  : " .$tmp[1];
			}
			
			$data['hadir'] = $mhadir->get_absen_dosen($tmp[0], $this->config->fakultas, $this->config->cabang, $_POST['tipe']);
			$data['tipe'] = $_POST['tipe'];
			$data['max'] = $_POST['max'];
			$data['nilai'] = $_POST['nilai']; 
		}
		else{
			$data['keterangan'] = '';
			$data['max'] = '14';
			$data['nilai'] = '-'; 
			$data['tipe'] = '';
			$data['hadir'] = '';
		}
		
		$this->view( 'hadir/index.php', $data );
	}
	
	function dosen(){
		$mhadir = new model_reporthadir();
		$this->add_script("js/hadir/hadir.js");
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->coms->add_script('highchart/highcharts.js');
		$this->coms->add_script('highchart/highcharts-3d.js');
		$this->coms->add_script('highchart/modules/exporting.js');
		
		$data['fakultas'] = $mhadir->get_detail_fakultas($this->coms->authenticatedUser->fakultas);
		$data['mkd'] = $mhadir->get_mkd();
		$data['dosen'] = $mhadir->get_dosen($this->coms->authenticatedUser->fakultas, $this->coms->authenticatedUser->cabang);
		
		$dosen = $mhadir->get_dosen_detail($this->coms->authenticatedUser->staffid);
		$data['keterangan'] = "Dosen : " . $dosen->nama;
		$data['hadir'] = $mhadir->get_absen_dosen($dosen->karyawan_id, $this->coms->authenticatedUser->fakultas, $this->coms->authenticatedUser->cabang, 'dosen');
		$data['tipe'] = 'dosen';
		
		if(isset($_POST['max'])) $data['max'] = $_POST['max'];
		else $data['max'] = '14';
		
		
		$this->view( 'hadir/dosen.php', $data );
	}
	
}
?>