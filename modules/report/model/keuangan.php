<?php
class model_keuangan extends model {
	private $coms;
	public function __construct() {
		parent::__construct();	
	}
	
	//rekap keuangan
	function rekap_tahun($thn){
		$end = $thn + 10;
		$sql = "SELECT 
					YEAR(tbl_keu_bayar.tgl_mulai) tahun,
					SUM(tbl_keu_bayar.total_non_pph) total_non_pph,
					SUM(tbl_keu_bayar.total) total,
					SUM(tbl_keu_bayar.total_pph) total_pph
				FROM db_ptiik_apps.tbl_keu_bayar
				WHERE YEAR(tbl_keu_bayar.tgl_mulai) >= $thn AND YEAR(tbl_keu_bayar.tgl_mulai) <= $end
				GROUP BY YEAR(tbl_keu_bayar.tgl_mulai)
				ORDER BY tbl_keu_bayar.tgl_mulai";
		return $this->db->query($sql);
	}
	
	function get_rekap_bulan($thn){
		$sql = "SELECT 
					MONTH(tbl_keu_bayar.tgl_mulai) bulan,
					SUM(tbl_keu_bayar.total_non_pph) total_non_pph,
					SUM(tbl_keu_bayar.total) total,
					SUM(tbl_keu_bayar.total_pph) total_pph
				FROM db_ptiik_apps.tbl_keu_bayar
				WHERE YEAR(tbl_keu_bayar.tgl_mulai) = '$thn'
				GROUP BY MONTH(tbl_keu_bayar.tgl_mulai)
				ORDER BY tbl_keu_bayar.tgl_mulai";
		return $this->db->query($sql);
	}
	
}
?>