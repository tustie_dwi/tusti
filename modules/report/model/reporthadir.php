<?php
class model_reporthadir extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_absen_dosen($dosen_id=NULL, $fak=NULL, $cabang=NULL, $tipe='dosen'){
		$sql = "SELECT 
					tbl_karyawan.nama,
					tbl_jadwalmk.kelas,
					tbl_jadwalmk.prodi_id,
					tbl_namamk.keterangan,
					COUNT(tbl_absen_dosen.absen_id) jml
				FROM db_ptiik_apps.tbl_absen_dosen
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_absen_dosen.pengampu_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_absen ON tbl_absen.absen_id = tbl_absen_dosen.absen_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_mkditawarkan.tahun_akademik = tbl_tahunakademik.tahun_akademik 
						AND tbl_tahunakademik.is_aktif = '1'
				WHERE tbl_karyawan.fakultas_id = '$fak' AND tbl_karyawan.cabang_id = '$cabang' ";
				
				if($tipe == 'dosen') $sql .= " AND MID(MD5(tbl_karyawan.karyawan_id),9,7) = '$dosen_id' ";
				else $sql .= " AND MID(MD5(tbl_mkditawarkan.mkditawarkan_id),9,7) = '$dosen_id' ";
				
				
		$sql .= " GROUP BY tbl_jadwalmk.jadwal_id
				ORDER BY tbl_namamk.keterangan, tbl_jadwalmk.prodi_id, tbl_jadwalmk.kelas";
				
		return $this->db->query($sql);
	}
	
	function get_dosen($fak=NULL, $cabang=NULL){
		$sql = "SELECT 
					tbl_karyawan.nik,
					tbl_karyawan.nama,
					MID(MD5(tbl_karyawan.karyawan_id),9,7) karyawan_id,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir
				FROM db_ptiik_apps.tbl_karyawan
				WHERE tbl_karyawan.fakultas_id = '$fak' AND tbl_karyawan.cabang_id = '$cabang' AND tbl_karyawan.is_status = 'dosen'
				ORDER BY tbl_karyawan.nama";
		return $this->db->query($sql);
	}
	
	function get_mkd(){
		$sql = "SELECT 
					tbl_namamk.keterangan,
					tbl_matakuliah.kode_mk,
					MID(MD5(tbl_mkditawarkan.mkditawarkan_id),9,7) mkd_id
				FROM db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_mkditawarkan.tahun_akademik
					AND tbl_tahunakademik.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				ORDER BY tbl_namamk.keterangan";
		return $this->db->query($sql);
	}
	
	function get_detail_fakultas($fak=NULL){
		$sql = "SELECT tbl_fakultas.keterangan
				FROM db_ptiik_apps.tbl_fakultas
				WHERE tbl_fakultas.fakultas_id = '$fak'";
		$data = $this->db->getRow($sql);
		return $data->keterangan;
	}

	//dosen
	function get_dosen_detail($dosen=NULL){
		$sql = "SELECT 
					MID(MD5(tbl_karyawan.karyawan_id),9,7) karyawan_id,
					tbl_karyawan.nama,
					tbl_karyawan.gelar_akhir,
					tbl_karyawan.gelar_awal
				FROM db_ptiik_apps.tbl_karyawan
				WHERE tbl_karyawan.karyawan_id = '$dosen'";
		return $this->db->getRow($sql);
	}
	
	
	
	
	
	
	
}

?>