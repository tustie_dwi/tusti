<?php
class model_absen extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	//absen per unit
	
	function get_unit($fakultas, $cabang){
		$sql = "SELECT 
					MID(MD5(tbl_unit_kerja.unit_id),9,7) unit_id, 
					tbl_unit_kerja.keterangan
				FROM db_ptiik_apps.tbl_unit_kerja
				WHERE keterangan <> '' AND tbl_unit_kerja.fakultas_id = '$fakultas' AND tbl_unit_kerja.cabang_id = '$cabang'
				ORDER BY tbl_unit_kerja.keterangan";
		return $this->db->query($sql);
	}
	
	function get_absen_unit($unit=NULL, $tahun=NULL, $start=NULL, $end=NULL){
		$start = $tahun . $start;
		$end = $tahun . $end;
		
		$sql = "SELECT 
					tbl_rekap_absen.periode,
					SUM(tbl_rekap_absen.hadir) hadir,
					SUM(tbl_rekap_absen.ijin) ijin,
					SUM(tbl_rekap_absen.sakit) sakit,
					SUM(tbl_rekap_absen.alpha) alpha,
					tbl_karyawan.nama,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir,
					MID(tbl_rekap_absen.periode,5) bulan
				FROM db_ptiik_apps.tbl_rekap_absen
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_rekap_absen.karyawan_id = tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_karyawan_unit ON tbl_karyawan_unit.karyawan_id = tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_karyawan_unit.unit_id AND MID(MD5(tbl_unit_kerja.unit_id),9,7) = '$unit'
				WHERE tbl_rekap_absen.periode BETWEEN $start AND $end
				GROUP BY tbl_karyawan.nama
				ORDER BY tbl_karyawan.nama";
		return $this->db->query($sql);
	}
		
	//absen per karyawan
	function get_dosen($fak=NULL, $cabang=NULL){
		$sql = "SELECT 
					tbl_karyawan.nik,
					tbl_karyawan.nama,
					MID(MD5(tbl_karyawan.karyawan_id),9,7) karyawan_id,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir
				FROM db_ptiik_apps.tbl_karyawan
				WHERE tbl_karyawan.fakultas_id = '$fak' AND tbl_karyawan.cabang_id = '$cabang'
				ORDER BY tbl_karyawan.nama";
		return $this->db->query($sql);
	}
	
	function get_rekap_absen($karyawan=NULL, $tahun=NULL, $start=NULL, $end=NULL){
		$start = $tahun . $start;
		$end = $tahun . $end;
		
		$sql = "SELECT 
					tbl_rekap_absen.periode,
					tbl_rekap_absen.hadir,
					tbl_rekap_absen.ijin,
					tbl_rekap_absen.sakit,
					tbl_rekap_absen.alpha,
					MID(tbl_rekap_absen.periode,5) bulan,
					tbl_karyawan.nama,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir
				FROM db_ptiik_apps.tbl_rekap_absen
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_rekap_absen.karyawan_id = tbl_karyawan.karyawan_id
				WHERE MID(MD5(tbl_karyawan.karyawan_id),9,7) = '$karyawan'
					AND tbl_rekap_absen.periode BETWEEN $start AND $end
				ORDER BY tbl_rekap_absen.periode";
					
		return $this->db->query($sql);
	}
}