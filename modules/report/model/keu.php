<?php
class model_keu extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($str=NULL,$tahun=NULL,$bulan_mulai=NULL,$bulan_selesai=NULL){
		$sql = "SELECT IFNULL(tbl_karyawan.nik,'-') as nik,
					 CONCAT(IFNULL(tbl_karyawan.gelar_awal,''), ' ', tbl_karyawan.nama, ' ',IFNULL(tbl_karyawan.gelar_akhir,'')) as fullname,
					 SUM(tbl_keu_bayar_detail.total_bayar) as pendapatan,
					 SUBSTR(tbl_keu_bayar.tgl_mulai,1,4) as tahun,
					 SUBSTR(tbl_keu_bayar.tgl_mulai,6,2) as bulan,
					 SUBSTR(tbl_keu_bayar.tgl_mulai,1,7) as bulan_lengkap
				FROM db_ptiik_apps.tbl_keu_bayar
				LEFT JOIN db_ptiik_apps.tbl_keu_bayar_detail ON tbl_keu_bayar_detail.bayar_id = tbl_keu_bayar.bayar_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_keu_bayar_detail.karyawan_id = tbl_karyawan.karyawan_id
				WHERE tbl_karyawan.home_base = 'PTIIK'";
		if($bulan_mulai!="" && $bulan_selesai!=""){
			$sql .= " AND (SUBSTR(tbl_keu_bayar.tgl_mulai,1,7) BETWEEN '".$bulan_mulai."' AND '".$bulan_selesai."')";
			if($str=="" || ($str!="get_tahun" && $str!="get_bulan")){
				$sql .= " GROUP BY tbl_karyawan.karyawan_id";
				if($str=="pendapatan"){
					$sql .= ",bulan_lengkap";
				}
				$sql .= " ORDER BY fullname,bulan_lengkap";
			}
		}
		if($tahun!=""){
			$sql .= " AND SUBSTR(tbl_keu_bayar.tgl_mulai,1,4) = '".$tahun."'
					  GROUP BY tbl_karyawan.karyawan_id
					  ORDER BY fullname";
		}
		if($str=="get_tahun"){
			$sql .= " GROUP BY tahun";
		}
		elseif($str=="get_bulan"){
			$sql .= " GROUP BY bulan_lengkap
					  ORDER BY bulan_lengkap";
		}
		
		$result = $this->db->query($sql);
		return $result;
	}
	
}
?>