<?php
class scheduling_absen extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 		
	}
		
	function  index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$this->write();
		
	}
	
	function ok(){
		$this->index('ok');
	}
	function nok(){
		$this->index('nok');
	}
	
	function rekap(){
		if(isset($_POST['cmbjenis']) && ($_POST['cmbjenis']!='-')){
			$this->detail_rekap($_POST['cmbjenis']);
			exit();
		}
		
		$data['absen'] = "";
		
		$this->add_script('js/absen.js');	
		$this->view( 'absen/rekap.php',$data );
	}
	
	
	function detail_rekap(){
		$mabsen = new model_absen();	
		$mconf	= new  model_conf();
		
		if(isset($_POST['cmbjenis'])){
			$type 	= $_POST['cmbjenis'];
		}else{
			$type	= "";
		}
		
		$data['type']	= $type;								
		$data['semester']= $mconf->get_semester();
		
		$staffid = $this->coms->authenticatedUser->staffid;
		$role = $this->coms->authenticatedUser->role;
		
		if(isset($_POST['cmbsemester'])){
			$sid = $_POST['cmbsemester'];
		}else{
			$sid = "";
		}		
						
		if(isset($_POST['cmbmk'])){
			$mk 			 = $_POST['cmbmk'];			
			$data['pertemuan']= $mabsen->get_jumlah_pertemuan($sid, $mk);
		}else{
			$mk				 = "";
			$data['pengampu']= "";
			$data['pertemuan']= '18';
		}
				
		$data['sid']		= $sid;
		
		if($type=='mhs'){
			$this->view( 'absen/rekap_mhs.php', $data );
		}else{
			$this->view( 'absen/rekap_dosen.php', $data );
		}
	}
	
	function data_rekap(){
		$sid = $_POST['cmbsemester'];
		$periode = $_POST['cmbperiode'];
		$dosen 	 = $_POST['cmbdosen'];
						
		$mabsen = new model_absen();	
		
		$data['jadwal']		= $mabsen->get_rekap_absen_by_mk($sid, $dosen, $periode);	
		$data['periode']	= $periode;
		$data['pertemuan']	= '14';
					
		$this->view( 'absen/data_rekap_dosen.php', $data );
	}
	
	
	function get_absen_by_dosen(){
		$staffid = $this->coms->authenticatedUser->staffid;
		$role 	= $this->coms->authenticatedUser->role;
		
		$sid = $_POST['cmbsemester'];
		
		$mabsen = new model_absen();

		if(strtolower($role)=='dosen'){
			$pengampu= $mabsen->get_dosen_mk($sid, "","", $staffid);
		}else{
			$pengampu= $mabsen->get_dosen_mk($sid);
		}			
	
		echo "<option value='0'>Please Select...</option>";
		if($pengampu){
			foreach ($pengampu as $dt):
				echo "<option  value='".$dt->karyawan_id."'  ";
				
				echo ">".$dt->nama."</option>";
			endforeach;	
		}		
	}
	
	function get_periode_absen(){
		$sid = $_POST['cmbsemester'];
		$dosen = $_POST['cmbdosen'];
		
		$mabsen = new model_absen();
		
		$periode = $mabsen->get_periode_absen($sid, $dosen);
				
		echo "<option value='0'>Please Select...</option>";
		if($periode){
			foreach ($periode as $dt):
				echo "<option  value='".$dt->periode."'  ";
				
				echo ">".$dt->periode."</option>";
			endforeach;	
		}		
	}
	
	
	function save_absen($kegiatan=NULL){
	
		$jadwal= $_POST['hidval'];
		if($jadwal):
			$mabsen = new model_absen();
			$mconf	= new  model_conf();
			
			$userid		= $this->coms->authenticatedUser->username;;
			$lastupdate = date("Y-m-d H:i");
		
			
			$jmulai		= $_POST['jammulai'];
			$jselesai	= $_POST['jamselesai'];
			$mkid		= $_POST['cmbmk'];
			$tgl 		= $_POST['tmulai'];	
			$total		= $_POST['totalpertemuan'];
			$sesi		= $_POST['cmbsesi'];
			$materi		= $_POST['materi'];
		
			$dosenid	= $mconf->get_dosen_id($_POST['pengampu'],$mkid);
			$staffid	= $_POST['pengampu'];						
			
			if(isset($_POST['chkpraktikum']) && ($_POST['chkpraktikum'] == 1)):
				$kode= $mabsen->get_reg_praktikum($jadwal, $tgl);
				$praktikum=1;
			else:
				$kode		= $mabsen->get_reg_number($jadwal, $tgl);
				$praktikum=0;
			endif;
						
			if(isset($_POST['chkmhs'])):
				$mhs 		= $_POST['chkmhs'];
				$hadir		= $_POST['cmbhadir'];
				
				for($i=0;$i<count($mhs);$i++){
				$id 	= $mabsen->add_nol($i);
				
				$mhsid	= $kode.$id;
				
				$datanya= array('absenmhs_id'=>$mhsid, 'mahasiswa_id'=>$mhs[$i], 'absen_id'=>$kode, 'is_hadir'=>$hadir[$i], 'user'=>$userid,  'last_update'=>$lastupdate);
				
				$mabsen->replace_absen_mhs($datanya);

				if($praktikum==1):
					$mhsid	= $mabsen->get_reg_praktikum_mhs($kode,$mhs[$i]);
					
					$datanya1= array('absen_id'=>$mhsid, 'mahasiswa_id'=>$mhs[$i], 'praktikum_id'=>$kode, 'is_hadir'=>$hadir[$i],  'user_id'=>$userid, 'last_update'=>$lastupdate);
					
					$mabsen->replace_absen_praktikum_mhs($datanya1);	
				endif;
			}
		
			endif;
			
			if($praktikum==1){
			
				$row 		= $mabsen->get_nama_mk_by_praktikum($_POST['cmbmk']);
				$namamk		= $row->nama_mk;
				$kodemk		= $row->kode_mk;
				
				$shari 	= $this->getHari(date("N",  strtotime($tgl)));
				
				if(isset($_POST['asisten'])){
					$datadel = array('praktikum_id'=>$kode);
					$mabsen->delete_absen_asisten($datadel);
					
					$i=0;
					foreach($_POST['asisten'] as $asisten):
						$i++;				
						$asistenid 		= $mabsen->get_reg_number_asisten($kode,$asisten);
						$dataasisten 	= array('jadwal_asisten_id'=>$asistenid, 'absen_id'=>$kode, 'asisten_id'=>$asisten, 'is_hadir'=>1, 'user_id'=>$userid, 'last_update'=>$lastupdate);
				
						$mabsen->replace_absen_asisten($dataasisten);
					endforeach;
				}
			
			
				$datanya 	= array('praktikum_id'=>$kode, 'hari'=>$shari,'tahun_akademik'=>$_POST['cmbsemester'], 'jadwal_id'=>$jadwal, 'mkditawarkan_id'=>$_POST['cmbmk'], 'prodi_id'=>$_POST['cmbprodi'], 
								'kelas_id'=>$_POST['cmbkelas'], 'tgl'=>$tgl, 
								'jam_mulai'=>$jmulai, 'jam_selesai'=>$jselesai, 'judul'=>'-', 'sesi_ke'=>$sesi,'nama_mk'=>$namamk, 'kode_mk'=>$kodemk, 'ruang'=>$_POST['ruangid'],
								'last_update'=>$lastupdate, 'user_id'=>$userid, 'kelompok'=>$_POST['cmbkelompok']);
										
				$mabsen->replace_absen_praktikum($datanya);
					
				
				$datanya 	= array('absen_id'=>$kode,  'jadwal_id'=>$jadwal, 'tgl'=>$tgl, 'jam_masuk'=>$jmulai,'jam_selesai'=>$jselesai, 'materi'=>$materi, 'sesi_ke'=>$sesi,
									'total_pertemuan'=>$total, 'last_update'=>$lastupdate, 'user'=>$userid, 'praktikum'=>1, 'kelompok'=>$_POST['cmbkelompok']);
				$save = $mabsen->replace_absen($datanya);
				
				
			}else{
			
				$datanya 	= array('absen_id'=>$kode,  'jadwal_id'=>$jadwal, 'tgl'=>$tgl, 'jam_masuk'=>$jmulai,'jam_selesai'=>$jselesai, 'materi'=>$materi, 'sesi_ke'=>$sesi,
									'total_pertemuan'=>$total, 'last_update'=>$lastupdate, 'user'=>$userid);
									
				$datadosen 	= array('absendosen_id'=>$kode, 'absen_id'=>$kode, 'pengampu_id'=>$dosenid, 'is_hadir'=>1, 'user'=>$userid, 'last_update'=>$lastupdate);
				
				$save = $mabsen->replace_absen($datanya, $datadosen);
			}
					
			if( $save ) {
				$result['status'] = "OK";
				$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
			} else {
				$result['status'] = "NOK";
				$result['error'] = $mabsen->error;
			}
			
			echo json_encode($result);
			
			unset($_POST['b_add']);
		endif;
	}
	
		
	function write($note=NULL, $style = "calendar"){
		if(isset($_POST['b_add'])){
			switch($_POST['cmbjenis']){
				case 'kuliah':
					$this->save_absen();
				break;
				
				case 'praktikum':
					$this->save_absen(1);
				break;
				
				
			}
		}else{
			if(isset($_POST['cmbjenis'])){
				switch($_POST['cmbjenis']){
					case 'kuliah':
						$this->jadwal();
					break;
					
					case 'praktikum':
						$this->jadwal();
					break;
					
					case 'uts':
						$this->ujian('uts');
					break;
					
					case 'uas':
						$this->ujian('uas');
					break;
					
					default :
						$this->jadwal();
					break;
				}
			}else{
				$this->jadwal();
			}
		}		
	}
	
	
	
	function form_report(){
		$mjadwal = new model_jadwal();
		$mconf	 	= new model_conf();
	
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$semesterid = "";
		}
		
		if(isset($_POST['cmblokasi']) ){
			$data['cmblokasi'] = htmlspecialchars($_POST['cmblokasi']);
		}else{
			$data['cmblokasi'] = "-";
		}
		
				
		if($data['jeniskegiatan']=='uts'||$data['jeniskegiatan']=='uas'){
			$data['lokasi']	= $mjadwal->get_lokasi("-");
			$data['tgl']	= $mjadwal->get_tgl_ujian($data['jeniskegiatan'], $semesterid, $data['cmbhari'],"-");
			$data['jam']	= $mjadwal->get_jam_ujian($data['jeniskegiatan'], $semesterid, $data['cmbhari'],"-","-");
			$data['prodi']	= $mconf->get_prodi();
			
			$this->view('absen/report_form.php', $data);
		}
	}
	
	
	
	function jadwal_by_form(){
		
		$mconf	 	= new model_conf();
		$mjadwal	= new model_jadwal();
		$minfo	 	= new model_info();
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$semesterid = "";
		}
		
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
		if(isset($_POST['cmbjadwal']) ){
			$jadwalid = $_POST['cmbjadwal'];
		}else{
			$jadwalid = "";
		}
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbruang']) ){
			//$data['cmbruang'] = htmlspecialchars($_POST['cmbruang']);
			$dataruang=explode("|", $_POST['cmbruang']);
			$data['cmbruang'] = $dataruang[0];
			
		}else{
			$data['cmbruang'] = "-";
		}
		
		
		if(isset($_POST['cmbmulai']) ){
			$data['cmbmulai'] = htmlspecialchars($_POST['cmbmulai']);
		}else{
			$data['cmbmulai'] = "-";
		}
		
		if(isset($_POST['cmbselesai']) ){
			$data['cmbselesai'] = htmlspecialchars($_POST['cmbselesai']);
		}else{
			$data['cmbselesai'] = "-";
		}
		
		if(isset($_POST['cmbtgl']) ){
			$data['cmbtgl'] = htmlspecialchars($_POST['cmbtgl']);			
		}else{
			$data['cmbtgl'] = "";
		}
		
		if(isset($_POST['cmbprodi']) ){
			$data['cmbprodi'] = htmlspecialchars($_POST['cmbprodi']);
		}else{
			$data['cmbprodi'] = "-";
		}
		
		
		$data['prodi']		= $mconf->get_prodi();
		$data['mk']			= $mconf->get_mkditawarkan(0,$semesterid);	
		//$data['ruang']		= $mconf->get_ruang($this->config->fakultas,'kuliah');
		
				
		if($jadwalid){
			$jadwal	= $minfo->get_jadwal_by_hari($this->config->fakultas,$data['cmbhari'], $semesterid, "", $data['cmbmulai'], $data['cmbselesai'], $data['cmbruang'],$jadwalid);
		}else{		
			$jadwal="";
		}
		
				
		if($jadwalid){
			$data['tmpid'] 	 = $jadwal->jadwal_id;
			$data['prodiid'] = $jadwal->prodi_id;
			$data['mkid'] 	 = $jadwal->mkditawarkan_id;
			$data['kelasid'] = $jadwal->kelas_id;
			$data['mulai']	= $jadwal->jam_mulai;
			$data['selesai']= $jadwal->jam_selesai;
			$data['ruangid']= $jadwal->ruang;
			
			$data['jadwalid']		= $jadwal->jadwal_id;
			$data['is_praktikum']	= $jadwal->is_praktikum;
			$data['mhs']			= $mconf->get_mhs_mk($jadwal->jadwal_id);
			
			$data['kelompok'] 		= $minfo->get_kelompok_praktikum();
			
			if($data['cmbtgl']):
				$data['absen']	= $minfo->get_absen_by_jadwal("", "", "","", $data['cmbtgl']);
				if($data['absen']) $data['dosenid'] = $data['absen']->dosen_id;			
				else $data['dosenid'] = $jadwal->dosen_id;
				
				$data['dasisten']= $minfo->get_absen_asisten($jadwal->jadwal_id,$data['cmbtgl'] );
				$data['kelompokid'] = $minfo->get_kelompok_praktikum($jadwal->jadwal_id,$data['cmbtgl'] );
			else:
				$data['dasisten']= $minfo->get_absen_asisten($jadwal->jadwal_id);
				$data['dosenid'] = $jadwal->dosen_id;				
			endif;
		}else{
			$data['tmpid'] 	 = "";
			$data['prodiid'] = "";
			$data['dosenid'] = "";
			$data['mkid'] 	 = "";
			$data['kelasid'] = "";
			$data['mulai']	= $data['cmbmulai'];
			$data['selesai']= $data['cmbselesai'];
			$data['ruangid']= $data['cmbruang'];
			$data['jadwalid']	= "";
			$data['is_praktikum']	= "";
			$data['mhs'] = "";
		}
		
		$data['minfo']	= $minfo;
		$data['staff']	= $mconf->get_karyawan();
		
		$data['pengampu'] = $mconf->get_dosen_diampu("", $data['mkid'], $semesterid);	
				
		$this->view('absen/jadwal_form.php', $data);
	}
	
	function  cek_asisten(){
		$mk 	  = $_POST['mkid'];
		
		$mabsen = new model_absen();	
		
		$asisten	= $mabsen->get_asisten($mk);	
		if($asisten){
			foreach($asisten as $dt):
				echo "<option value='$dt->asisten_id|$dt->nim - $dt->nama'>".$dt->nim. ' - '. $dt->nama."</option>";
			endforeach;
		}
		
	}
	
	function cek_mahasiswa(){
		$mconf = new model_conf();
		
		$jadwal 		= $_POST['jadwal'];
		$data['absenid']= $_POST['absen'];
		if(isset($_POST['kelompok']) && ($_POST['kelompok'])):
			$data['mhs']	= $mconf->get_mhs_praktikum($jadwal, $_POST['kelompok']);	
		else:
			$data['mhs']	= $mconf->get_mhs_mk($jadwal);	
		endif;
		
			
		$this->view( 'absen/daftar_hadir_mhs.php', $data );
	}
	
	
	function get_pengampu(){
		$mjadwal	 = new model_jadwal();
				
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
		if(isset($_POST['cmbprodi']) ){
			$data['prodiid'] = htmlspecialchars($_POST['cmbprodi']);
		}else{
			$data['prodiid'] = "-";
		}
							
		$pengampu 	= $mjadwal->get_data_ujian($mkid, 'dosen', $data['prodiid']);
				
		if($pengampu){
			$data = array();
			foreach($pengampu as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}
		
	}
	
		
	
	
	function get_pengampu_kuliah(){
		$mconf	 = new model_conf();
				
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
						
		$pengampu 	= $mconf->get_dosen_diampu("",$mkid);
				
		if($pengampu){
			$data = array();
			foreach($pengampu as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}
		
	}
	
	function  get_staff(){		
		$mconf 	= new model_conf();
				
		$staff 	= $mconf->get_karyawan();
				
		if($staff){
			$data = array();
			foreach($staff as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}
	}
	
	function get_mk(){
		$mjadwal	 = new model_jadwal();
		$mconf		 = new model_conf();
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$semesterid = "";
		}
				
		$data['mk']	= $mconf->get_mkditawarkan(0,$semesterid);		
				
	}
	
	
	function jadwal_view(){
		$minfo	 = new model_info();
		if(isset($_POST['cmbsemester'])){
			$data['semesterid'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['semesterid'] = "";
		}
		
		$pendek = $minfo->get_semester_id($data['semesterid']);
		
		if($pendek):
			$data['pendek'] = $pendek->is_pendek;
		else:
			$data['pendek'] = "";
		endif;
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbruang']) ){
			//$data['cmbruang'] = htmlspecialchars($_POST['cmbruang']);
			$dataruang=explode("|", $_POST['cmbruang']);
			$data['cmbruang'] = $dataruang[0];
			
		}else{
			$data['cmbruang'] = "-";
		}
		
		
		if(isset($_POST['cmbmulai']) ){
			$data['cmbmulai'] = htmlspecialchars($_POST['cmbmulai']);
		}else{
			$data['cmbmulai'] = "-";
		}
		
		if(isset($_POST['cmbselesai']) ){
			$data['cmbselesai'] = htmlspecialchars($_POST['cmbselesai']);
		}else{
			$data['cmbselesai'] = "-";
		}
		
		if(isset($_POST['cmbtgl']) ){
			$data['cmbtgl'] = htmlspecialchars($_POST['cmbtgl']);
		}else{
			$data['cmbtgl'] = "-";
		}
		
		$data['jenis']	= $data['jeniskegiatan'];
		
		//var_dump($data['cmbruang']);
		
		if(isset($_POST['cmbjenis'])){
			switch($_POST['cmbjenis']){
								
				case 'kuliah':
					if($data['cmbruang']!="-")	$data['ruang'] 	= $minfo->get_ruang($this->config->fakultas,"kuliah", "", "", $data['cmbruang'] );
					else $data['ruang'] 	= $minfo->get_ruang($this->config->fakultas,"kuliah" );
					
					$data['praktikum'] = "";		
					$this->draw_grid_kuliah($data);
				break;
				
				case 'praktikum':
					if($data['cmbruang']!="-")	$data['ruang'] 	= $minfo->get_ruang($this->config->fakultas,"kuliah", "", "", $data['cmbruang'] );
					else $data['ruang'] 	= $minfo->get_ruang($this->config->fakultas,"kuliah" );
					
					$data['praktikum'] = 1;		
					$this->draw_grid_kuliah($data);
				break;			
								
				default :
					if($data['cmbruang']!="-")	$data['ruang'] 	= $minfo->get_ruang($this->config->fakultas,"kuliah", "", "", $data['cmbruang'] );
					else $data['ruang'] 	= $minfo->get_ruang($this->config->fakultas,"kuliah" );
					
					$data['praktikum'] = "";	
					$this->draw_grid_kuliah($data);
				break;
			}
		}else{
			if($data['cmbruang']!="-")	$data['ruang'] 	= $mconf->get_ruang($this->config->fakultas,"kuliah", "", "", $data['cmbruang'] );
			else $data['ruang'] 	= $mconf->get_ruang($this->config->fakultas,"kuliah" );
			$data['praktikum'] = "";	
			$this->draw_grid_kuliah($data);
		}
		
	}
	
	
	function draw_grid_kuliah($data=NULL){
			
		$mconf		= new model_info();
		$mjadwal 	= new model_jadwal();

		if($data['jeniskegiatan']=='praktikum'){			
			$drepeat	= $mjadwal->get_kalender_akademik("", 'kuliah',$data['semesterid']);
		}else{			
			$drepeat	= $mjadwal->get_kalender_akademik("", $data['jeniskegiatan'],$data['semesterid']);
		}
		
		if($drepeat){
			$data['tmulai']		= $drepeat->tgl_mulai;
			$data['tselesai']	= $drepeat->tgl_selesai;	
		}else{
			$data['tmulai']		= date("Y-m-d");
			$data['tselesai']	= date("Y-m-d");
		}				
		
		$data['dosen'] 	= $mconf->get_dosen_pengampu();
		$data['hari'] 	= $mconf->get_hari();	

		if($data['pendek']!=""):
			$data['jam'] 	= $mconf->get_jam_mulai_pendek();
		else:
			$data['jam'] 	= $mconf->get_jam_mulai();	
		endif;
		
		if($data['cmbruang']!="-")	$data['ruang'] 	= $mconf->get_ruang($this->config->fakultas,"kuliah", "", "", $data['cmbruang'] );
		else $data['ruang'] 	= $mconf->get_ruang($this->config->fakultas,"kuliah" );
		
		$data['mk'] 	= $mconf->get_mkditawarkan(0);
		$data['prak'] 	= $mconf->get_mkditawarkan(1);
		$data['jadwal'] = $mconf->get_jadwal_by_hari($this->config->fakultas,$data['cmbhari'], $data['semesterid'], $data['praktikum'],"","",$data['cmbruang']);
		$data['rowmk']  = $mconf->get_row_mk($data['cmbhari'], $data['semesterid']);
		$data['mkruang']= $mconf->get_mk_ruang($data['cmbhari'], $data['semesterid']);
		$data['by']		= "hari";
		$data['iddosen']= "";
		$data['idruang']= "";
		$data['idhari'] = $data['cmbhari'];
		$data['idmk']   = "";
		$data['idprak']	= "";
		$data['prodi']	= $mconf->get_prodi();	
		$data['idprodi']= "";

								
		$this->add_style('css/calendar/calendar.css');

		$this->view('absen/jadwalbyhari.php', $data);		
	}
	
	function jadwal(){
	
		$mjadwal = new model_jadwal();	
		$mconf	 = new model_conf();
		
		if(isset($_POST['cmbsemester'])){
			$data['semesterid'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['semesterid'] = "";
		}
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}		

	
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}	

		if(isset($_POST['cmbruang']) ){
			//$data['cmbruang'] = htmlspecialchars($_POST['cmbruang']);
			$dataruang=explode("|", $_POST['cmbruang']);
			$data['cmbruang'] = $dataruang[0];
			
		}else{
			$data['cmbruang'] = "-";
		}		
					
		$data['kegiatan'] 	= $mjadwal->get_jenis_kegiatan();
		$data['semester']	= $mconf->get_semester();
		$data['hari']   	= $mconf->get_hari();
		$data['ruang'] 		= $mconf->get_ruang("", "kuliah");
		$data['repeatin']	= "monthly";
		
		$data['posts']		= "";
		//$data['panitia'] 	= "";
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('js/bootstrap/tag.css');
		$this->coms->add_script('js/bootstrap/tag.js');
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
				
		$this->add_script('js/absen.js');
		$this->add_script('js/jsall.js');
							
		$this->view('absen/edit.php', $data);
	}
	
	
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		return $ext; 
	}  
	
	function delete_ruang($id){
		$mconf   = new model_conf();	
		
		$datanya  = array('jadwal_id'=>$id);	
		$mconf->delete_jadwal_ruang($datanya);
		$mconf->delete_jadwal_ruang_detail($datanya);
	}
	
	function delete_jadwal($id){
		$mjadwal = new model_jadwal();
		
		$datanya  = array('detail_id'=>$id);
		$mjadwal->delete_jadwal_kegiatan($datanya);
	}
	
	function delete_ujian($id){
		$mjadwal = new model_jadwal();
		
		$datanya  = array('ujian_id'=>$id);
		$mjadwal->delete_jadwalujian($datanya);
		$mjadwal->delete_pengawas($datanya);		
	}

	function delete($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('detail_id'=>$id);	
		
		if($mjadwal->delete_jadwal_kegiatan($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function deletemk($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('jadwal_id'=>$id);	
		
		if($mjadwal->delete_jadwalmk($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_ujian($id);
			$this->delete_jadwal($id);
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function delete_absen(){
		$mabsen= new model_absen();
		
		$id=$_POST['id'];
		
		$datanya  = array('absen_id'=>$id);
		
		if($mabsen->delete_absen($datanya)){ 		
			$result['status'] = "OK";
			$mabsen->delete_absen_dosen($datanya);
			$mabsen->delete_absen_mhs($datanya);
		}else {
			$result['status'] = "NOK";
		}
		
		echo json_encode($result);	
	}
	
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 200) {

			// truncate string
			$stringCut = substr($string, 0, 200);

			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}
	
	function dateDiff($in,$out){		
		$interval =round(($out - $in)/(3600*24));
		return  $interval ;
	} 
	
	
	
	function getHari($str){
		switch($str){
			case '1':
				$strout	= 'senin';
			break;
			case '2':
				$strout	= 'selasa';
			break;
			case '3':
				$strout	= 'rabu';
			break;
			case '4':
				$strout	= 'kamis';
			break;
			case '5':
				$strout	= 'jumat';
			break;
			case '6':
				$strout	= 'sabtu';
			break;
			case '7':
				$strout	= 'minggu';
			break;
		}
		
		return $strout;
	}

}
?>