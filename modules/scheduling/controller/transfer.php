<?php
class scheduling_transfer extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 		
	}
		
	
	function ok(){
		$this->index('ok');
	}
	function nok(){
		$this->index('nok');
	}
	
	function absensi($str=NULL, $kelas=NULL, $angkatan=NULL){
		$mtransfer 	= new model_transfer();
				
		ob_start();
		
		$dosen 	= $mtransfer->read_absen($str, $kelas);
		
		
		if($dosen){
			foreach($dosen as $dt):
				if($dt->dosen_id!='-'){
					$id = $mtransfer->get_reg_absen($dt->mkditawarkan_id, $dt->kelas, $dt->prodi_id, $dt->tgl_absensi);
					$mhs	= $mtransfer->read_absen_mhs($dt->mkditawarkan_id, $dt->kelas, $dt->prodi_id, $angkatan);
					
					$datanya =  array('absen_id'=>$id, 'mkditawarkan_id'=>$dt->mkditawarkan_id,'prodi_id'=>$dt->prodi_id, 'kelas_id'=>$dt->kelas, 'tgl'=>$dt->tgl_absensi, 'materi'=>$dt->materi, 
							'total_pertemuan'=>'18', 'sesi_ke'=>$dt->pertemuan_ke, 'user'=>'tdp', 'last_update'=>date("Y-m-d"));
								
					$datadosen =  array('absendosen_id'=>$id, 'dosen_id'=>$dt->dosen_id, 'absen_id'=>$id, 'is_hadir'=>1, 'user'=>'tdp', 'last_update'=>date("Y-m-d"));
					$mtransfer->replace_absen($datanya, $datadosen);
					
					echo $datanya."<br>";
					
					if($mhs){
						$i=0;
						foreach ($mhs as $row):
							$i++;
							$kode 	= $mtransfer->add_nol($i);
					
							$mhsid	= $id.$kode;
							switch($row->kode_absensi){
								case 'H' :
									$ishadir = '1';
								break;
								case 'S' :
									$ishadir = '2';
								break;
								case 'I' :
									$ishadir = '3';
								break;
								case 'A' :
									$ishadir = '4';
								break;
							}
							
							$datanya1= array('absenmhs_id'=>$mhsid, 'mahasiswa_id'=>$row->nim, 'absen_id'=>$id, 'is_hadir'=>$ishadir, 'last_update'=>date("Y-m-d"));
							
							$mtransfer->replace_absen_mhs($datanya1);		

						endforeach;		
					}
				}				
			endforeach;
		}
	}
	
	function detailjadwal($prak=NULL){
		ob_start();
		$mjadwal = new model_jadwal();
		$mtransfer = new model_transfer();
		$mconf	 = new model_conf();
		$tmulai="2013-12-18";
		$tselesai="2013-12-27";
		
		if($prak){
			$datamk= $mtransfer->read_prak('20130100');
		}else{		
			$datamk= $mtransfer->read_jadwalmk('20130100');
		}
		
		
		foreach ($datamk as $data):
			$mkid = $data->mkditawarkan_id;
			$ruang	= $data->ruang;
			$prodi	= $data->prodi_id;
			$kelas	= strToUpper($data->kelas_id);
			$repeaton	= 'monthly';
			$semester	= $data->tahun_akademik;		
			$jammulai	= $data->jam_mulai;
			$jamselesai	= $data->jam_selesai;
			
			$action 	= "insert";
			
			if($data->is_praktikum=='1'){
					$jeniskegiatan	= 'praktikum';
					$dosen		= 'Asisten';	
					$dosenid	= '-';
					$staffid	= '-';
					$isprak		=1;
				}else{				
					$jeniskegiatan	= 'kuliah';	
					$dosenid	= $data->dosen_id;
					$staffid	= $data->karyawan_id;
					$dosen		= $mconf->get_nama_dosen($staffid);						
					$isprak		=0;
				}
					
				
				$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);
				$lastupdate	= $data->last_update;
				$user		= $data->user;		
				$kode 		= $data->jadwal_id;
				$jadwalid	= $data->detail_id;
				
				$hari		= strToLower($data->hari);
				$nhari		= $mconf->getNHari($hari);
			
				$keterangan	= $dosen."(".Ucwords($hari).")";
				
				if($kelas){	
					echo $kode."<br>";
						$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester,$repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $staffid, $kegiatan, 
						$keterangan, 'tdp', $lastupdate, $action,$kelas,$mkid, $tmulai, $tselesai);		
				}
				
				
		endforeach;
	}
	
	function jadwalmk($type=null){
		ob_start();
		
	
		$mjadwal = new model_jadwal();
		$mtransfer = new model_transfer();
		$mconf	 = new model_conf();
		
					
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		
		
		$action 	= "insert";
		//echo "1";
		
		$datamk = $mtransfer->read_jadwal_ok();
		
		foreach($datamk as $data):
			$mkid = $data->mkditawarkan_id;
			$ruang	= $data->ruang;
			$prodi	= $data->prodi_id;
			$kelas	= strToUpper($data->kelas);
			$repeaton	= 'monthly';
			$semester	= $data->tahun_akademik;		
			$jammulai	= $data->jam_mulai;
			$jamselesai	= $data->jam_selesai;
						
			echo $jamselesai."<br>";			
				
				//$nhari		= $mconf->getNHari($hari);
				
				
				if($data->is_praktikum=='1'){
					$jeniskegiatan	= 'praktikum';
					$dosen		= 'Asisten';	
					$dosenid	= '-';
					$staffid	= '-';
					$isprak		=1;
				}else{				
					$jeniskegiatan	= 'kuliah';	
					$dosen		= $data->nama;	
					$dosenid	= $data->dosen_id;
					$staffid	= $data->karyawan_id;					
					$isprak		=0;
				}
				
				$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);
				$lastupdate	= date("Y-m-d H:i:s");
				$user		= 'tdp';		
				$kode = $mjadwal->get_reg_number($jeniskegiatan, $staffid, $semester);
				
				
						
				if($dosenid){
					
					$datanya = Array('jadwal_id'=>$kode, 'tahun_akademik'=>$semester, 'karyawan_id'=>$staffid, 'jenis_kegiatan_id'=>$jeniskegiatan, 
							'repeat_on'=>$repeaton, 'user'=>$user, 'last_update'=>$lastupdate);
						$mjadwal->replace_penjadwalan($datanya);
					
						//for($j=0;$j<count($_POST['chkhari']);$j++){
							//$hari	 	= strToLower($_POST['hari']);
							$hari		= strToLower($data->hari);
							$nhari		= $mconf->getNHari($hari);
						
							$keterangan	= $dosen."(".Ucwords($hari).")";
							
							$jadwalid = $mjadwal->get_reg_detail($kode, $jeniskegiatan, $staffid, $semester, $hari, $jammulai, $ruang, $nhari);
							
							/*if($type){					
								$jadwalid 	= '2'.$nhari.$mkid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);		
							}else{					
								$jadwalid 	= '1'.$nhari.$staffid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);	
							}*/
							
							//$mjadwal->update_dpenjadwalan($kode,$jammulai, $hari, $ruang);
							
							$datanya = Array('detail_id'=>$jadwalid, 'jadwal_id'=>$kode, 'ruang'=>$ruang, 'hari'=>$hari, 'jam_mulai'=>$jammulai,'jam_selesai'=>$jamselesai, 
							'is_aktif'=>1, 'user'=>$user, 'last_update'=>$lastupdate);
							$mjadwal->replace_detail_penjadwalan($datanya);					
											
							if($kelas){	
								
								$mjadwal->update_jadwal_exist($dosenid, $mkid, $jammulai, $kelas, $ruang, $hari, $isprak, $jadwalid);
													
								$datanya	= array('jadwal_id'=>$jadwalid, 'dosen_id'=>$dosenid, 'mkditawarkan_id'=>$mkid, 'kelas_id'=>$kelas, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 
													'hari'=>$hari, 'ruang'=>$ruang, 'is_praktikum'=>$isprak, 'prodi_id'=>$prodi, 'user'=>$user, 'last_update'=>$lastupdate,'is_aktif'=>1);	
								
								$mjadwal->replace_jadwalmk($datanya);
								//$mconf->log("db_ptiik_apps.tbl_jadwalmk", $datanya, $user, $action);
								
								$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester,$repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $staffid, $kegiatan, 
								$keterangan, $user, $lastupdate, $action,$kelas,$mkid);					
								
							}
						//}
				}
		
		endforeach;
				
		exit();
	}
		
	function index($type=null){
		ob_start();
		
	
		$mjadwal = new model_jadwal();
		$mtransfer = new model_transfer();
		$mconf	 = new model_conf();
		
					
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		
		
		
		$action 	= "insert";
		//echo "1";
		
		$datamk = $mtransfer->read_jadwal();
		
		foreach($datamk as $data):
			$mkid = $data->mkditawarkan_id;
			$ruang	= $data->ruang;
			$prodi	= $data->prodi_id;
			$kelas	= strToUpper($data->kelas);
			$repeaton	= 'monthly';
			$semester	= $data->tahun_akademik;		
			$jammulai	= $data->jam_mulai;
			$jamselesai	= $data->jam_selesai;
						
			echo $jamselesai."<br>";			
				
				//$nhari		= $mconf->getNHari($hari);
				
				
				if($data->pengampu=='Prak (Asisten)'){
					$jeniskegiatan	= 'praktikum';
					$dosen		= 'Asisten';	
					$dosenid	= '-';
					$staffid	= '-';
					$isprak		=1;
				}else{				
					$jeniskegiatan	= 'kuliah';	
					$dosen		= $mconf->get_dosen_by_nama($data->pengampu);	
					$dosenid	= $mconf->get_dosen_id($dosen,$mkid);
					$staffid	= $dosenid;					
					$isprak		=0;
				}
				
				$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);
				$lastupdate	= date("Y-m-d H:i:s");
				$user		= $this->coms->authenticatedUser->username;		
				$kode = $mjadwal->get_reg_number($jeniskegiatan, $staffid, $semester);
				
				
						
				if($dosenid){
					
					$datanya = Array('jadwal_id'=>$kode, 'tahun_akademik'=>$semester, 'karyawan_id'=>$staffid, 'jenis_kegiatan_id'=>$jeniskegiatan, 
							'repeat_on'=>$repeaton, 'user'=>$user, 'last_update'=>$lastupdate);
						$mjadwal->replace_penjadwalan($datanya);
					
						//for($j=0;$j<count($_POST['chkhari']);$j++){
							//$hari	 	= strToLower($_POST['hari']);
							$hari		= strToLower($data->hari);
							$nhari		= $mconf->getNHari($hari);
						
							$keterangan	= $dosen."(".Ucwords($hari).")";
							
							$jadwalid = $mjadwal->get_reg_detail($kode, $jeniskegiatan, $staffid, $semester, $hari, $jammulai, $ruang, $nhari);
							
							/*if($type){					
								$jadwalid 	= '2'.$nhari.$mkid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);		
							}else{					
								$jadwalid 	= '1'.$nhari.$staffid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);	
							}*/
							
							//$mjadwal->update_dpenjadwalan($kode,$jammulai, $hari, $ruang);
							
							$datanya = Array('detail_id'=>$jadwalid, 'jadwal_id'=>$kode, 'ruang'=>$ruang, 'hari'=>$hari, 'jam_mulai'=>$jammulai,'jam_selesai'=>$jamselesai, 
							'is_aktif'=>1, 'user'=>$user, 'last_update'=>$lastupdate);
							$mjadwal->replace_detail_penjadwalan($datanya);					
											
							if($kelas){	
								
								$mjadwal->update_jadwal_exist($dosenid, $mkid, $jammulai, $kelas, $ruang, $hari, $isprak, $jadwalid);
													
								$datanya	= array('jadwal_id'=>$jadwalid, 'dosen_id'=>$dosenid, 'mkditawarkan_id'=>$mkid, 'kelas_id'=>$kelas, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 
													'hari'=>$hari, 'ruang'=>$ruang, 'is_praktikum'=>$isprak, 'prodi_id'=>$prodi, 'user'=>$user, 'last_update'=>$lastupdate,'is_aktif'=>1);	
								
								$mjadwal->replace_jadwalmk($datanya);
								$mconf->log("db_ptiik_apps.tbl_jadwalmk", $datanya, $user, $action);
								
								$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester,$repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $staffid, $kegiatan, 
								$keterangan, $user, $lastupdate, $action,$kelas,$mkid, $tmulai, $tselesai);					
								
							}
						//}
				}
		
		endforeach;
				
		exit();
	}
	
	
	
	function insert_detail_jadwal_ruang($mjadwal=NULL, $mconf=NULL,$semester=NULL,$repeaton=NULL, $kode=NULL, $jadwalid=NULL, 
										$ruang=NULL, $jammulai=NULL, $jamselesai=NULL, $hari=NULL, $jeniskegiatan=NULL, $dosenid=NULL, $kegiatan=NULL, $keterangan=NULL, 
										$user=NULL, $lastupdate=NULL, $action=NULL, $kelas=NULL, $mk=NULL, $tgl=NULL, $tselesai=NULL, $staff=NULL){
		/*$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();*/
		
		switch($repeaton){
			case 'daily':
				if($tgl){
					$in = strtotime($tgl);
					$out = strtotime($tgl);
					
					$y=0;
				}else{
					$in	  = strtotime(date("Y-m-d"));
					$out  = strtotime('+7 days', $in);
					$y=1;
				}				
			break;
			
			case 'weekly':
				$in	  = strtotime(date("Y-m-d"));
				$out  = strtotime('+7 days', $in);
				$y=7;
			break;
			
			case  'monthly':				
				if($tgl){
					if($tgl!='0000-00-00'){
						$in = strtotime($tgl);
						$out = strtotime($tselesai);
					}else{
						$in	  = strtotime(date("Y-m-d"));
						$out  = strtotime('+1 month', $in);
					}
				}else{
					$in	  = strtotime(date("Y-m-d"));
					$out  = strtotime('+1 month', $in);
				}		
				
				$y=$this->dateDiff($in,$out);
			break;
			
			case 'yearly':
				$in	  = strtotime(date("Y-m-d"));
				$out = strtotime('+1 year', $in);
				$y	= $this->dateDiff($in,$out);
			break;
		}
		
		$n = $this->dateDiff($in,$out);
		
		$datanya = array('jadwal_id'=>$jadwalid, 'ruang_id'=>$ruang, 'kegiatan'=>$kegiatan, 'catatan'=>$keterangan, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai , 
						 'repeat_on'=>$repeaton, 'user_id'=>$user, 'last_update'=>$lastupdate, 'jenis_kegiatan_id'=>$jeniskegiatan, 'tahun_akademik'=>$semester);
		$mconf->replace_jadwal_ruang($datanya);
		
		//$mconf->log("db_ptiik_apps.tbl_jadwalruang", $datanya, $user, $action);				
		
		$detailid	= $n.$jadwalid;
								
		for($i=0;$i<=$n;$i++){		
			$shari 	= $mconf->getHari(date("N",  strtotime('+'.$i.' days', $in)));
			$nhari	= date("N",  strtotime('+'.$i.' days', $in));
			$tgl	= date("Y-m-d", strtotime('+'.$i.' days', $in));			
			
			if(($shari==$hari)){														
				$this->insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i,$ruang, $kode, $jeniskegiatan,$dosenid,$kelas,$mk);
				
				
				$datanya = array('karyawan_id'=>$dosenid, 'tgl'=>$tgl, 'tgl_selesai'=>$tgl,'ruang'=>$ruang, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan'=>$jeniskegiatan,  'kegiatan'=>$kegiatan, 'keterangan'=>$keterangan, 'id'=>$jadwalid);
				$mjadwal->replace_agenda_staff($datanya);
			}	
		
		}			
		
	}
	
	
		
	
	
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		return $ext; 
	}  
	
	function delete_ruang($id){
		$mconf   = new model_conf();	
		
		$datanya  = array('jadwal_id'=>$id);	
		$mconf->delete_jadwal_ruang($datanya);
		$mconf->delete_jadwal_ruang_detail($datanya);
	}
	
	function delete_jadwal($id){
		$mjadwal = new model_jadwal();
		
		$datanya  = array('detail_id'=>$id);
		$mjadwal->delete_jadwal_kegiatan($datanya);
	}

	function delete($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('detail_id'=>$id);	
		
		if($mjadwal->delete_jadwal_kegiatan($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function deletemk($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('jadwal_id'=>$id);	
		
		if($mjadwal->delete_jadwalmk($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_jadwal($id);
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 200) {

			// truncate string
			$stringCut = substr($string, 0, 200);

			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}
	
	function dateDiff($in,$out){		
		$interval =round(($out - $in)/(3600*24));
		return  $interval ;
	} 
	
	function insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i, $ruang, $infjadwal, $infjeniskegiatan, $dosen, $kelas, $mk){
		$mconf 	 = new model_conf();			
		
		$detailid = $nhari.$jadwalid.$i;
	
		//echo $detailid;
		$datanya = array('detail_id'=>$detailid, 'jadwal_id'=>$jadwalid, 'tgl'=>$tgl, 'hari'=>$shari, 'inf_hari'=>$nhari, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'ruang'=>$ruang, 'inf_jadwal_id'=>$infjadwal, 'inf_jenis_kegiatan'=>$infjeniskegiatan, 'dosen_id'=>$dosen, 'kelas'=>$kelas, 'mk_id'=>$mk);
		$mconf->replace_detail_jadwal_ruang($datanya);
		
		$mconf->update_jadwal_valid($jadwalid, $tgl, $shari, $jammulai, $jamselesai, $ruang, $infjeniskegiatan, $dosen, $kelas, $mk);		
		//$mconf->log("db_ptiik_apps.tbl_detailjadwalruang", $datanya, $user, $action);		
	}

}
?>