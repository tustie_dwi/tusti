<?php
class model_infojadwal extends model {

	public function __construct() {
		parent::__construct();	
	}
	function get_absen_asisten($jadwal=NULL, $absen=NULL ){
		$sql = "SELECT
					db_ptiik_apps.tbl_praktikum.jadwal_id,
					db_ptiik_apps.tbl_praktikum.tgl,
					db_ptiik_apps.tbl_praktikum.jam_mulai,
					db_ptiik_apps.tbl_praktikum.jam_selesai,
					db_ptiik_apps.tbl_praktikum.praktikum_id,
					db_ptiik_apps.tbl_praktikum_asisten_jadwal.asisten_id,
					db_ptiik_apps.tbl_praktikum_asisten_jadwal.jadwal_asisten_id,
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.nama
					FROM
					db_ptiik_apps.tbl_praktikum
					INNER JOIN db_ptiik_apps.tbl_praktikum_asisten_jadwal ON db_ptiik_apps.tbl_praktikum.praktikum_id = db_ptiik_apps.tbl_praktikum_asisten_jadwal.praktikum_id
					INNER JOIN db_ptiik_apps.tbl_praktikum_asisten ON db_ptiik_apps.tbl_praktikum_asisten_jadwal.asisten_id = db_ptiik_apps.tbl_praktikum_asisten.asisten_id
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_praktikum_asisten.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id WHERE tbl_praktikum.jadwal_id='$jadwal' AND tbl_praktikum.praktikum_id='$absen' ";
	
		return $this->db->query($sql);
	}
	
	function  get_kelompok_praktikum($jadwal=NULL, $absen=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_praktikum_kelompok.kelompok_id,
					db_ptiik_apps.tbl_praktikum_kelompok.nama
					FROM
					db_ptiik_apps.tbl_praktikum_kelompok
					LEFT JOIN db_ptiik_apps.tbl_praktikum ON db_ptiik_apps.tbl_praktikum_kelompok.kelompok_id = db_ptiik_apps.tbl_praktikum.kelompok WHERE 1 
					";
		if($jadwal) $sql.= " AND tbl_praktikum.jadwal_id='$jadwal' ";
		if($absen) $sql.= " AND tbl_praktikum.praktikum_id='$absen' ";
		
		if($absen) return $this->db->getRow($sql);
		else return $this->db->query($sql);
	}
	
	function get_absen_by_jadwal($jadwal=NULL, $absen=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_absen.absen_id,
					db_ptiik_apps.tbl_absen.jadwal_id,
					db_ptiik_apps.tbl_absen.tgl,
					db_ptiik_apps.tbl_absen.jam_masuk,
					db_ptiik_apps.tbl_absen.jam_selesai,
					db_ptiik_apps.tbl_absen.materi,
					db_ptiik_apps.tbl_absen.sesi_ke,
					db_ptiik_apps.tbl_absen.total_pertemuan,
					db_ptiik_apps.tbl_jadwalmk.kelas,
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id
					FROM
					db_ptiik_apps.tbl_absen
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id WHERE 1 ";
		if($jadwal) $sql.= " AND tbl_absen.jadwal_id='$jadwal' ";
		if($absen) $sql.= " AND tbl_absen.absen_id='$absen' ";
		
		if($absen) return $this->db->getRow($sql);
		else return $this->db->query($sql);
	}
	
	function get_semester_id($str=NULL){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					WHERE
					1 = 1
					";
		if($str){
			$sql.= " AND `db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik` =  '".$str."'";
		}else{
			$sql.= " AND `db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` =  '1'";
		}
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	function get_wisudawan(){
		$sql = "SELECT
				db_ptiik_apps.tbl_mahasiswa.nim,
				db_ptiik_apps.tbl_mahasiswa.nama,
				db_ptiik_apps.tbl_mahasiswa.angkatan,
				db_ptiik_apps.tbl_wisudawan.tgl_lahir,
				db_ptiik_apps.tbl_wisudawan.tmp_lahir,
				db_ptiik_apps.tbl_wisudawan.periode,
				db_ptiik_apps.tbl_wisudawan.tahun,
				db_ptiik_apps.tbl_wisudawan.foto,
				db_ptiik_apps.tbl_wisudawan.ip_lulus,
				db_ptiik_apps.tbl_wisudawan.tgl_lulus,
				db_ptiik_apps.tbl_wisudawan.sk_ijasah,
				db_ptiik_apps.tbl_mahasiswa.email,
				db_ptiik_apps.tbl_prodi.keterangan as `prodi`
				FROM
				db_ptiik_apps.tbl_mahasiswa
				INNER JOIN db_ptiik_apps.tbl_wisudawan ON db_ptiik_apps.tbl_mahasiswa.mahasiswa_id = db_ptiik_apps.tbl_wisudawan.mahasiswa_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.jurusan = db_ptiik_apps.tbl_prodi.prodi_id
				ORDER BY db_ptiik_apps.tbl_wisudawan.tahun DESC, db_ptiik_apps.tbl_wisudawan.periode DESC, 
								db_ptiik_apps.tbl_mahasiswa.nim ASC
				";
		$result = $this->db->query($sql);
		
		return $result;

	}
	
	function get_data_ujian($mk=NULL, $type=NULL,$prodi=NULL, $kelas=NULL){
		switch($type){
			case 'dosen':
				$sql = "SELECT DISTINCT
							`db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id`,
							`db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`,
							`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`,
							`db_ptiik_apps`.`tbl_karyawan`.`nama`
							FROM
							`db_ptiik_apps`.`tbl_pengampu`
							Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
							Inner Join `db_ptiik_apps`.`tbl_jadwalmk` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` AND `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`
						WHERE `db_ptiik_apps`.`tbl_pengampu`.`is_aktif` = '1' ";
							
				$sqlzz = "SELECT DISTINCT
							`db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id`,
							`db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`,
							`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`,
							`db_ptiik_apps`.`tbl_karyawan`.`nama`
							FROM
							`db_ptiik_apps`.`tbl_pengampu`
							Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
							Inner Join `db_ptiik_apps`.`tbl_jadwalmk` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` AND `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`
						WHERE `db_ptiik_apps`.`tbl_pengampu`.`is_aktif` = '1'   ";
			break;
			
			case 'prodi':
				$sql = "SELECT DISTINCT
							db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
							db_ptiik_apps.tbl_jadwalmk.prodi_id as `id`,
							db_ptiik_apps.tbl_prodi.keterangan AS `value`
							FROM
							db_ptiik_apps.tbl_pengampu
							INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_pengampu.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id AND db_ptiik_apps.tbl_pengampu.dosen_id = db_ptiik_apps.tbl_jadwalmk.dosen_id
							INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
						WHERE 1 = 1 
					";

			break;
			
			case 'kelas':
				$sql = "SELECT DISTINCT
							db_ptiik_apps.tbl_jadwalmk.kelas,
							db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id
							FROM `db_ptiik_apps`.`tbl_pengampu` Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` Inner Join `db_ptiik_apps`.`tbl_jadwalmk` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` AND `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`
						WHERE 1 = 1 
					";
			break;
			
		}
		
		if($kelas){
			$sql = $sql . " AND tbl_jadwalmk.kelas = '".$kelas."' ";
		}	
		
		if($mk){
			$sql = $sql . " AND tbl_jadwalmk.mkditawarkan_id='".$mk."'  ";
		}	
		
		if($prodi){
			$sql = $sql . " AND db_ptiik_apps.tbl_jadwalmk.prodi_id='".$prodi."'  ";
		}	
		
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function read_ujian_by_tgl($tgl=NULL, $mulai=NULL, $selesai=NULL, $ruang=NULL){
		$sql = "SELECT
					tbl_jadwalujian.ujian_id,
					tbl_jadwalujian.kelas_id,
					tbl_jadwalujian.dosen_id,
					tbl_jadwalujian.mkditawarkan_id,
					tbl_jadwalujian.prodi_id,
					tbl_jadwalujian.jam_mulai,
					tbl_jadwalujian.jam_selesai,
					tbl_jadwalujian.tgl,
					tbl_jadwalujian.hari,
					tbl_jadwalujian.jenis_ujian,
					tbl_jadwalujian.ruang,
					tbl_jadwalujian.is_project,
					tbl_jadwalujian.`user`,
					tbl_jadwalujian.is_aktif,
					tbl_jadwalujian.last_update
				FROM
					db_ptiik_apps.tbl_jadwalujian WHERE 1 = 1 ";
		if($tgl){
			$sql.= " AND tbl_jadwalujian.tgl = '".$tgl."' ";
		}
		
		if($mulai){
			$sql.= " AND tbl_jadwalujian.jam_mulai = '".$mulai."' ";
		}
		
		if($selesai){
			$sql.= " AND tbl_jadwalujian.jam_selesai = '".$selesai."' ";
		}
		
		if($ruang){
			$sql.= " AND tbl_jadwalujian.ruang = '".$ruang."' ";
		}
		
		$result = $this->db->getRow($sql);
		
		return $result;
	}
	
	function get_mhs($str=NULL, $angkatan=NULL){
		$sql = "SELECT
				tbl_mahasiswa.mahasiswa_id,
				tbl_mahasiswa.nim,
				tbl_mahasiswa.nama,
				tbl_mahasiswa.angkatan,
				tbl_mahasiswa.telp,
				tbl_mahasiswa.hp,
				tbl_mahasiswa.email,
				tbl_mahasiswa.alamat,
				tbl_mahasiswa.tgl_lahir,
				tbl_mahasiswa.tmp_lahir,
				tbl_mahasiswa.jenis_kelamin,
				tbl_prodi.keterangan AS prodi,
				tbl_mahasiswa.jurusan,
				tbl_prodi.prodi_id,
				tbl_mahasiswa.jalur_seleksi,
				tbl_mahasiswa.nama_ortu,
				tbl_mahasiswa.alamat_ortu,
				tbl_mahasiswa.telp_ortu,
				tbl_mahasiswa.hp_ortu,
				tbl_mahasiswa.email_ortu,
				tbl_mahasiswa.alamat_surat,
				tbl_mahasiswa.catatan,
				tbl_mahasiswa.dosen_pembimbing,
				tbl_karyawan.nama AS pembimbing,
				tbl_karyawan.nik,
				tbl_karyawan.is_nik,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir
				FROM
				db_ptiik_apps.tbl_mahasiswa
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_mahasiswa.jurusan = tbl_prodi.prodi_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_mahasiswa.dosen_pembimbing = tbl_karyawan.karyawan_id
				WHERE tbl_mahasiswa.mahasiswa_id = '".$str."' AND tbl_mahasiswa.is_aktif='aktif'
				";
		if($angkatan) $sql.= " AND tbl_mahasiswa.angkatan < '$angkatan' ";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_mhs_mk_pilihan_all($kuesioner=NULL, $mhs=NULL, $angkatan=NULL){
			$sql = "SELECT DISTINCT
						db_ptiik_apps.tbl_prodi.keterangan AS prodi,
						db_ptiik_apps.tbl_prodi.prodi_id,
						db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
						db_ptiik_apps.tbl_mahasiswa.nim,
						db_ptiik_apps.tbl_mahasiswa.nama,
						db_ptiik_apps.tbl_mahasiswa.angkatan,
						db_ptiik_apps.tbl_mahasiswa.telp,
						db_ptiik_apps.tbl_mahasiswa.hp,
						db_ptiik_apps.tbl_mahasiswa.email,
						db_ptiik_apps.tbl_mahasiswa.alamat,
						db_ptiik_apps.tbl_mahasiswa.tgl_lahir,
						db_ptiik_apps.tbl_mahasiswa.tmp_lahir,
						db_ptiik_apps.tbl_mahasiswa.jalur_seleksi,
						db_ptiik_apps.tbl_mahasiswa.alamat,
						db_ptiik_apps.tbl_mahasiswa.jenis_kelamin,
						db_ptiik_apps.tbl_mahasiswa.dosen_pembimbing,
						db_ptiik_apps.tbl_karyawan.nama AS pembimbing,
						db_ptiik_apps.tbl_kuesioner_mhs.semester, 
						db_ptiik_apps.tbl_karyawan.nik,
						db_ptiik_apps.tbl_karyawan.is_nik,
						db_ptiik_apps.tbl_karyawan.gelar_awal,
						db_ptiik_apps.tbl_karyawan.gelar_akhir
						FROM
						db_ptiik_apps.tbl_mahasiswa
						INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.jurusan = db_ptiik_apps.tbl_prodi.prodi_id
						LEFT JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_mahasiswa.dosen_pembimbing = db_ptiik_apps.tbl_karyawan.karyawan_id
						LEFT JOIN db_ptiik_apps.tbl_kuesioner_mhs ON db_ptiik_apps.tbl_mahasiswa.mahasiswa_id = db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id
					WHERE db_ptiik_apps.tbl_mahasiswa.mahasiswa_id='".$mhs."' AND tbl_mahasiswa.is_aktif='aktif' 
					";
		if($angkatan) $sql.= " AND tbl_mahasiswa.angkatan < '$angkatan' ";
		
		if($kuesioner) $sql.= " AND db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id = '$kuesioner' ";
		
		
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_mhs_mk_pilihan($kuesioner=NULL, $mhs=NULL){
			$sql = "SELECT DISTINCT
						db_ptiik_apps.tbl_prodi.keterangan AS prodi,
						db_ptiik_apps.tbl_prodi.prodi_id,
						db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
						db_ptiik_apps.tbl_mahasiswa.nim,
						db_ptiik_apps.tbl_mahasiswa.nama,
						db_ptiik_apps.tbl_mahasiswa.angkatan,
						db_ptiik_apps.tbl_mahasiswa.telp,
						db_ptiik_apps.tbl_mahasiswa.hp,
						db_ptiik_apps.tbl_mahasiswa.email,
						db_ptiik_apps.tbl_mahasiswa.alamat,
						db_ptiik_apps.tbl_mahasiswa.tgl_lahir,
						db_ptiik_apps.tbl_mahasiswa.tmp_lahir,
						db_ptiik_apps.tbl_mahasiswa.jalur_seleksi,
						db_ptiik_apps.tbl_mahasiswa.alamat,
						db_ptiik_apps.tbl_mahasiswa.jenis_kelamin,
						db_ptiik_apps.tbl_mahasiswa.dosen_pembimbing,
						db_ptiik_apps.tbl_karyawan.nama AS pembimbing,
						db_ptiik_apps.tbl_kuesioner_mhs.semester, 
						db_ptiik_apps.tbl_karyawan.nik,
						db_ptiik_apps.tbl_karyawan.is_nik,
						db_ptiik_apps.tbl_karyawan.gelar_awal,
						db_ptiik_apps.tbl_karyawan.gelar_akhir
						FROM
						db_ptiik_apps.tbl_mahasiswa
						INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.jurusan = db_ptiik_apps.tbl_prodi.prodi_id
						LEFT JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_mahasiswa.dosen_pembimbing = db_ptiik_apps.tbl_karyawan.karyawan_id
						INNER JOIN db_ptiik_apps.tbl_kuesioner_mhs ON db_ptiik_apps.tbl_mahasiswa.mahasiswa_id = db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id
					WHERE db_ptiik_apps.tbl_mahasiswa.mahasiswa_id='".$mhs."' AND db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id='".$kuesioner."'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	function get_mhs_daftar_krs($semester=NULL, $mhs=NULL){
		
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_prodi.keterangan AS prodi,
					db_ptiik_apps.tbl_prodi.prodi_id,
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					tbl_mahasiswa.nim,
					tbl_mahasiswa.nama,
					tbl_mahasiswa.angkatan,
					tbl_mahasiswa.telp,
					tbl_mahasiswa.hp,
					tbl_mahasiswa.email,
					tbl_mahasiswa.alamat,
					tbl_mahasiswa.tgl_lahir,
					tbl_mahasiswa.tmp_lahir,
					tbl_mahasiswa.jalur_seleksi,
					db_ptiik_apps.tbl_mahasiswa.alamat,
					db_ptiik_apps.tbl_mahasiswa.jenis_kelamin,
					db_ptiik_apps.tbl_krs_tmp.tgl_daftar,
					db_ptiik_apps.tbl_krs_tmp.is_valid,
					tbl_mahasiswa.dosen_pembimbing,
					tbl_karyawan.nama AS pembimbing,
					tbl_karyawan.nik,
					tbl_karyawan.is_nik,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir
					FROM
					db_ptiik_apps.tbl_mahasiswa
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.jurusan = db_ptiik_apps.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_krs_tmp ON db_ptiik_apps.tbl_mahasiswa.mahasiswa_id = db_ptiik_apps.tbl_krs_tmp.mahasiswa_id
					LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_mahasiswa.dosen_pembimbing = tbl_karyawan.karyawan_id
					WHERE db_ptiik_apps.tbl_mahasiswa.mahasiswa_id='".$mhs."' AND db_ptiik_apps.tbl_krs_tmp.tahun_akademik='".$semester."'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_mhs_daftar($semester=NULL, $mhs=NULL){
		
		$sql = "SELECT
					db_ptiik_apps.tbl_prodi.keterangan AS prodi,
					db_ptiik_apps.tbl_prodi.prodi_id,
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.angkatan,
					db_ptiik_apps.tbl_mahasiswa.jurusan,
					db_ptiik_apps.tbl_daftarulang.daftar_id,
					db_ptiik_apps.tbl_daftarulang.tahun_akademik,
					db_ptiik_apps.tbl_daftarulang.nama,
					db_ptiik_apps.tbl_daftarulang.tgl_lahir,
					db_ptiik_apps.tbl_daftarulang.tmp_lahir,
					db_ptiik_apps.tbl_daftarulang.tgl_daftar,
					db_ptiik_apps.tbl_daftarulang.jalur_masuk as `jalur_seleksi`,
					db_ptiik_apps.tbl_daftarulang.email,
					db_ptiik_apps.tbl_daftarulang.alamat_malang as `alamat`,
					db_ptiik_apps.tbl_daftarulang.telp,
					db_ptiik_apps.tbl_daftarulang.hp,
					db_ptiik_apps.tbl_daftarulang.orang_tua as `nama_ortu`,
					db_ptiik_apps.tbl_daftarulang.alamat_ortu,
					db_ptiik_apps.tbl_daftarulang.email_ortu,
					db_ptiik_apps.tbl_daftarulang.telp_ortu,
					db_ptiik_apps.tbl_daftarulang.hp_ortu,
					db_ptiik_apps.tbl_daftarulang.is_valid,
					db_ptiik_apps.tbl_daftarulang.is_finish,
					db_ptiik_apps.tbl_daftarulang.alamat_surat,
					db_ptiik_apps.tbl_daftarulang.catatan
					FROM
					db_ptiik_apps.tbl_mahasiswa
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.jurusan = db_ptiik_apps.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_daftarulang ON db_ptiik_apps.tbl_mahasiswa.mahasiswa_id = db_ptiik_apps.tbl_daftarulang.mahasiswa_id
					WHERE db_ptiik_apps.tbl_mahasiswa.mahasiswa_id='".$mhs."' AND db_ptiik_apps.tbl_daftarulang.tahun_akademik='".$semester."'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function datanotfound(){
		echo "<h1>Maaf, data tidak ditemukan</h1>";
		return false;
	}
	
	function get_quote(){
		$sql = "SELECT
					db_ptiik_apps.tbl_quote.keterangan,
					db_ptiik_apps.tbl_quote.by,
					db_ptiik_apps.tbl_quote.is_publish
					FROM
					db_ptiik_apps.tbl_quote WHERE is_publish='1' ORDER BY RAND() LIMIT 0,1
					";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function  get_lokasi(){
		$sql = "SELECT DISTINCT
				left(db_ptiik_apps.tbl_ruang.ruang,2) as `id`,
				db_ptiik_apps.tbl_ruang.lokasi
				FROM
				db_ptiik_apps.tbl_ruang
				WHERE
				db_ptiik_apps.tbl_ruang.lokasi <> 'perpus' ";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_ruang_staf($str=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_ruang_karyawan.ruang,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_karyawan.is_dosen,
					`db_ptiik_apps`.`tbl_file`.`file_name` as `foto`,
					db_ptiik_apps.tbl_jabatan.keterangan AS jabatan
					FROM
					db_ptiik_apps.tbl_ruang_karyawan
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_ruang_karyawan.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					Left Join `db_ptiik_apps`.`tbl_file` ON `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` = `db_ptiik_apps`.`tbl_file`.`karyawan_id`
									Left Join `db_ptiik_apps`.`tbl_jabatan` ON `db_ptiik_apps`.`tbl_karyawan`.`jabatan_id` = `db_ptiik_apps`.`tbl_jabatan`.`jabatan_id`
									left Join `db_ptiik_apps`.`tbl_unitkerja` ON `db_ptiik_apps`.`tbl_karyawan`.`unit_id` = `db_ptiik_apps`.`tbl_unitkerja`.`unit_id`
					WHERE
									`db_ptiik_apps`.`tbl_karyawan`.`is_aktif` =  'aktif' AND
									`db_ptiik_apps`.`tbl_karyawan`.`home_base` =  'PTIIK'
					";
		if($str){
			$sql = $sql . " AND left(db_ptiik_apps.tbl_ruang_karyawan.ruang,2) = '".$str."' ";
		}
		
	
		
		$sql = $sql . "ORDER BY
					CAST(IFNULL(tbl_jabatan.urut,'99') as UNSIGNED ), `db_ptiik_apps`.`tbl_karyawan`.`nama`";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_tasks($id=NULL){
	$sql = "SELECT
					db_ptiik_apps.tbl_task_karyawan.task_id,
					db_ptiik_apps.tbl_task_karyawan.karyawan_id,
					db_ptiik_apps.tbl_task_karyawan.judul,
					db_ptiik_apps.tbl_task_karyawan.keterangan,
					db_ptiik_apps.tbl_task_karyawan.tgl_mulai,
					db_ptiik_apps.tbl_task_karyawan.tgl_selesai,
					db_ptiik_apps.tbl_task_karyawan.progress,
					db_ptiik_apps.tbl_task_karyawan.`user`,
					db_ptiik_apps.tbl_task_karyawan.last_update
				FROM
					db_ptiik_apps.tbl_task_karyawan
				WHERE 1 = 1 
				";
		if($id){
			$sql = $sql . " AND db_ptiik_apps.tbl_task_karyawan.karyawan_id = '".$id."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function read_skripsi($semester=NULL, $id=NULL){
		$sql = "SELECT
				mid(md5(`dbptiik_tesis`.`tbl_skripsi`.`skripsi_id`),5,5) as `id`,
				`dbptiik_tesis`.`tbl_skripsi`.`skripsi_id`,
				`dbptiik_tesis`.`tbl_skripsi`.`tahun_akademik`,
				`db_ptiik_apps`.`tbl_mahasiswa`.`nim`,
				`db_ptiik_apps`.`tbl_mahasiswa`.`nama`,
				`db_ptiik_apps`.`tbl_prodi`.`prodi_id`,
				`db_ptiik_apps`.`tbl_prodi`.`keterangan` AS `prodi`,
				`dbptiik_tesis`.`tbl_skripsi`.`mahasiswa_id`,
				`dbptiik_tesis`.`tbl_skripsi`.`tgl_pengajuan`,
				`dbptiik_tesis`.`tbl_skripsi`.`judul_proposal`,
				`dbptiik_tesis`.`tbl_skripsi`.`judul`,
				`dbptiik_tesis`.`tbl_skripsi`.`is_lulus`,
				`dbptiik_tesis`.`tbl_skripsi`.`is_disetujui`,
				concat(db_ptiik_apps.tbl_tahunakademik.tahun, ' ', db_ptiik_apps.tbl_tahunakademik.is_ganjil, ' ', db_ptiik_apps.tbl_tahunakademik.is_pendek) as `semester`,
				(SELECT group_concat(`db_ptiik_apps`.`tbl_karyawan`.`nama`  ORDER BY `tbl_dosenskripsi`.`dosen_ke` ASC ) 
					FROM
						`dbptiik_tesis`.`tbl_dosenskripsi`
						Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `dbptiik_tesis`.`tbl_dosenskripsi`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
						 where `dbptiik_tesis`.`tbl_skripsi`.`skripsi_id`=`dbptiik_tesis`.`tbl_dosenskripsi`.`skripsi_id` AND `tbl_dosenskripsi`.`is_pembimbing`='pembimbing'						
						 ) as `pembimbing`
				FROM
					`dbptiik_tesis`.`tbl_skripsi`
					Inner Join `db_ptiik_apps`.`tbl_mahasiswa` ON `tbl_skripsi`.`mahasiswa_id` = `tbl_mahasiswa`.`mahasiswa_id`
					Inner Join `db_ptiik_apps`.`tbl_prodi` ON `tbl_mahasiswa`.`jurusan` = `tbl_prodi`.`prodi_id`
					left Join db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_tahunakademik.tahun_akademik = `dbptiik_tesis`.`tbl_skripsi`.tahun_akademik
				WHERE 1=1
				";
		if($id){
			$sql=$sql . " AND (mid(md5(`dbptiik_tesis`.`tbl_skripsi`.`skripsi_id`),5,5)='".$id."' OR  `tbl_skripsi`.`mahasiswa_id`='".$id."')";
		}
		
		if($semester){
			$sql=$sql . " AND `tbl_skripsi`.`tahun_akademik`='".$semester."' ";
		}	
		
		$sql = $sql . "ORDER BY `dbptiik_tesis`.`tbl_skripsi`.`last_update` DESC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function read_kknp($semester=NULL, $id=NULL){
		$sql = "SELECT
					mid(md5(`tbl_kknp`.`kknp_id`),5,5) as `id`,
					`tbl_kknp`.`kknp_id`,
					`tbl_kknp`.`tahun_akademik`,
					`tbl_kknp`.`pembimbing_id`,
					`tbl_kknp`.`perusahaan_id`,
					`tbl_kknp`.`tgl_mulai`,
					`tbl_kknp`.`tgl_selesai`,
					`tbl_kknp`.`tgl_laporan`,
					`tbl_kknp`.`last_update`,
					`pt11k_user`.`name` as `username`,
					`tbl_kknp`.`catatan`,
					`tbl_kknp`.`is_status`,
					`tbl_kknp`.`is_surat`,
					(SELECT
						GROUP_CONCAT(concat(`tbl_mahasiswa`.`nama`,'-',db_ptiik_apps.tbl_mahasiswa.jurusan,'-',db_ptiik_apps.tbl_prodi.keterangan,'-',`tbl_kknp_mhs`.`is_aktif`) ORDER BY `tbl_mahasiswa`.`nama` ASC SEPARATOR '@' )
						FROM
						dbptiik_kknp.tbl_kknp_mhs
						INNER JOIN db_ptiik_apps.tbl_mahasiswa ON dbptiik_kknp.tbl_kknp_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
						INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.jurusan = db_ptiik_apps.tbl_prodi.prodi_id
						WHERE `tbl_kknp_mhs`.kknp_id = `tbl_kknp`.`kknp_id`
					) as `mhs`, 
					
					(SELECT GROUP_CONCAT(keterangan) FROM
						`dbptiik_kknp`.`tbl_kknp_objek`
							WHERE `tbl_kknp_objek`.kknp_id = `tbl_kknp`.`kknp_id`
					) as `objek`, 
					
					`db_ptiik_apps`.`tbl_karyawan`.`nik`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					`tbl_kknp_perusahaan`.`nama` AS `perusahaan`,
					`tbl_kknp_perusahaan`.`alamat`,
					`db_ptiik_apps`.`tbl_pangkatgolongan`.`golongan`,
					`db_ptiik_apps`.`tbl_pangkatgolongan`.`pangkat`,
					concat(db_ptiik_apps.tbl_tahunakademik.tahun, ' ', db_ptiik_apps.tbl_tahunakademik.is_ganjil, ' ', db_ptiik_apps.tbl_tahunakademik.is_pendek) as `semester`					
				FROM
					`dbptiik_kknp`.`tbl_kknp`
					left Join `db_ptiik_apps`.`tbl_karyawan` ON `dbptiik_kknp`.`tbl_kknp`.`pembimbing_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
					Inner Join `dbptiik_kknp`.`tbl_kknp_perusahaan` ON `dbptiik_kknp`.`tbl_kknp`.`perusahaan_id` = `dbptiik_kknp`.`tbl_kknp_perusahaan`.`perusahaan_id`
					Left Join `db_ptiik_apps`.`tbl_pangkatgolongan` ON `db_ptiik_apps`.`tbl_karyawan`.`golongan` = `db_ptiik_apps`.`tbl_pangkatgolongan`.`golongan`
					left Join `coms`.`pt11k_user` ON `dbptiik_kknp`.`tbl_kknp`.`user` = `coms`.`pt11k_user`.`username`
					left Join db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_tahunakademik.tahun_akademik = `dbptiik_kknp`.`tbl_kknp`.tahun_akademik
			    WHERE 1=1
				";
		if($id){
			$sql=$sql . " AND mid(md5(`dbptiik_kknp`.`tbl_kknp`.`kknp_id`),5,5)='".$id."' ";
		}	

		if($semester){
			$sql=$sql . " AND `tbl_kknp`.`tahun_akademik`='".$semester."' ";
		}			
		
		$sql = $sql . "ORDER BY `dbptiik_kknp`.`tbl_kknp`.`last_update` DESC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	function read_prestasi($semester=NULL, $jenis=NULL, $id=NULL){
		$sql = "SELECT
					mid(md5(tbl_prestasi.prestasi_id), 5,5) as `id`, 
					tbl_prestasi.prestasi_id,
					tbl_prestasi.jenis_id,
					tbl_prestasi.tahun_akademik,
					tbl_prestasi.judul,
					tbl_prestasi.penyelenggara,
					tbl_prestasi.tgl_mulai,
					tbl_prestasi.tgl_selesai,
					tbl_prestasi.lokasi,
					tbl_prestasi.inf_prestasi,
					tbl_prestasi.tingkat,
					tbl_prestasi.jenis_prestasi,
					tbl_prestasi.keterangan,
					tbl_prestasi.link_berita,
					tbl_prestasi.inf_peserta,
					tbl_prestasi.`user`,
					tbl_prestasi.last_update,
					(
						SELECT group_concat(inf_nama)
						FROM
						`db_ptiik_apps`.`tbl_prestasi_mhs`
						 WHERE `db_ptiik_apps`.`tbl_prestasi`.`prestasi_id`=`db_ptiik_apps`.`tbl_prestasi_mhs`.`prestasi_id`
					) as `mhs`,
					concat(db_ptiik_apps.tbl_tahunakademik.tahun, ' ', db_ptiik_apps.tbl_tahunakademik.is_ganjil, ' ', db_ptiik_apps.tbl_tahunakademik.is_pendek) as `semester`
				FROM
					db_ptiik_apps.tbl_prestasi 
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_prestasi.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
				WHERE 1 = 1 
					";
		if($id){
			$sql = $sql . " AND (mid(md5(tbl_prestasi.prestasi_id), 5,5) = '".$id."' OR tbl_prestasi.prestasi_id = '".$id."') ";
		}
		
		if($semester){
			$sql = $sql . " AND tbl_prestasi.tahun_akademik = '".$semester."' ";
		}
		
		if($jenis){
			$sql = $sql . " AND tbl_prestasi.jenis_prestasi = '".$jenis."' ";
		}
		
		$sql = $sql . " ORDER BY tbl_prestasi.tahun_akademik DESC, tbl_prestasi.tgl_mulai DESC";
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function read_soal($id=NULL, $tgl=NULL,$page = NULL, $perpage = NULL) {
	
		if(! $page){
			$page = 1;
		}
		
		if(! $perpage){
			$perpage = 10;
		} 
		
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
					mid(md5(`tbl_soalrekrutmen`.`soal_id`),5,5) as `id`,
					`tbl_soalrekrutmen`.`soal_id`,
					`tbl_soalrekrutmen`.`jenis_soal`,
					`tbl_soalrekrutmen`.`judul`,
					`tbl_soalrekrutmen`.`keterangan`,
					`tbl_soalrekrutmen`.`tgl_publish`,
					`tbl_soalrekrutmen`.`is_publish`
				FROM
					`db_ptiik_apps`.`tbl_soalrekrutmen`
				WHERE 1=1
				";
		if($id){
			//$sql=$sql . " AND mid(md5(`tbl_soalrekrutmen`.`soal_id`),5,5)='".$id."'";
			$sql = $sql . " AND lcase(replace(`tbl_soalrekrutmen`.`jenis_soal`,' ','')) = '".strToLower($id)."' ";
		}
		
		if($tgl){
			$sql=$sql . " AND `tbl_soalrekrutmen`.`tgl_publish` = '".$tgl." '  AND is_publish='1' ";
		}
		
		$sql= $sql . "ORDER BY `tbl_soalrekrutmen`.`tgl_publish` DESC LIMIT $offset, $perpage";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function read_wall($id=NULL, $tgl=NULL,$page = NULL, $perpage = NULL) {
	
		if(! $page){
			$page = 1;
		}
		
		if(! $perpage){
			$perpage = 10;
		} 
		
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
					mid(md5(`tbl_wallinfo`.`wall_id`),5,5) as `id`,
					`tbl_wallinfo`.`wall_id`,
					`tbl_wallinfo`.`judul`,
					`tbl_wallinfo`.`keterangan`,
					`tbl_wallinfo`.`tgl_publish`,
					`tbl_wallinfo`.`is_publish`
				FROM
					`db_ptiik_apps`.`tbl_wallinfo`
				WHERE 1=1
				";
		if($id){
			$sql=$sql . " AND mid(md5(`tbl_wallinfo`.`wall_id`),5,5)='".$id."'";
		}
		
		if($tgl){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_wallinfo`.`tgl_publish` <= '".$tgl." '  AND is_publish='1' ";
		}
		
		$sql= $sql . "ORDER BY `tbl_wallinfo`.`tgl_publish` DESC LIMIT $offset, $perpage";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jabatan */
	function  get_absen($id=NULL){
		$sql = "SELECT
				`vw_absen`.`jabatan`,
				`vw_absen`.`nama`,
				`vw_absen`.`gelar_awal`,
				`vw_absen`.`gelar_akhir`,
				date_format(`vw_absen`.`tgl_absen`,'%Y-%m-%d') as 'tgl_absen',
				`vw_absen`.`jam_absen`,
				`vw_absen`.`foto`,
				`vw_absen`.`is_absen`
				FROM
				`db_ptiik_apps`.`vw_absen`
				
				";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function  get_absen_pimpinan($id=NULL){
		$sql = "SELECT
				`vw_absenpimpinan`.`jabatan`,
				`vw_absenpimpinan`.`nama`,
				`vw_absenpimpinan`.`gelar_awal`,
				`vw_absenpimpinan`.`gelar_akhir`,
				date_format(`vw_absenpimpinan`.`tgl_absen`,'%Y-%m-%d') as 'tgl_absen',
				`vw_absenpimpinan`.`jam_absen`,
				`vw_absenpimpinan`.`foto`,
				`vw_absenpimpinan`.`is_absen`
				FROM
				`db_ptiik_apps`.`vw_absenpimpinan`
				";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_finger_print($tgl=NULL, $pin=NULL){
		$sql = "SELECT
				`tbl_fingerprint`.`pin`,
				`tbl_fingerprint`.`tgl`,
				`tbl_fingerprint`.`status`,
				`tbl_fingerprint`.`is_absen`
				FROM
				 `db_ptiik_apps`.`tbl_fingerprint`
				 WHERE `tbl_fingerprint`.`status` = 0 
				";
		if($tgl){
			$sql = $sql . "AND date_format(`tbl_fingerprint`.`tgl`, '%Y-%m-%d') = '".$tgl."' ";
		}
		if($pin){
			$sql = $sql . "AND `tbl_fingerprint`.`pin` = '".$pin."' ";
		}
		
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;	
	}
		
		
	function get_civitas($id=NULL,$isdosen=NULL, $ruang=NULL){
		$sql = "SELECT
				mid(md5(`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`),5,5) as `id`,
				`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`,
				`db_ptiik_apps`.`tbl_karyawan`.`nama`,
				`db_ptiik_apps`.`tbl_karyawan`.`nik`,
				`db_ptiik_apps`.`tbl_karyawan`.`is_nik`,
				`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
				`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`,
				`db_ptiik_apps`.`tbl_karyawan`.`pin`,
				`db_ptiik_apps`.`tbl_file`.`file_name` as `foto`,
				`db_ptiik_apps`.`tbl_file`.`url`,
				`db_ptiik_apps`.`tbl_file`.`is_jenis`,
				`db_ptiik_apps`.`tbl_karyawan`.`home_base`,
				`db_ptiik_apps`.`tbl_jabatan`.`keterangan` AS `jabatan`,
				(SELECT GROUP_CONCAT(concat(`tbl_unitkerja`.`nama`,'-',`tbl_unitkerja`.`unit_id`) SEPARATOR '@' ) FROM
						`db_ptiik_apps`.`tbl_unit_karyawan`
						Inner Join `db_ptiik_apps`.`tbl_unitkerja` ON `db_ptiik_apps`.`tbl_unitkerja`.`unit_id` = `db_ptiik_apps`.`tbl_unit_karyawan`.`unit_id`
						 where `db_ptiik_apps`.`tbl_unit_karyawan`.`karyawan_id`=`tbl_karyawan`.`karyawan_id`						
						 ) as `unitkerja`,
					(SELECT GROUP_CONCAT(ruang) FROM
						`db_ptiik_apps`.`tbl_ruang_karyawan`
						WHERE `db_ptiik_apps`.`tbl_ruang_karyawan`.`karyawan_id`=`tbl_karyawan`.`karyawan_id`						
						 ) as `ruangkerja`,	
				`db_ptiik_apps`.`tbl_karyawan`.`is_dosen`,
				`db_ptiik_apps`.`tbl_karyawan`.`is_tetap`,
				`db_ptiik_apps`.`tbl_jabatan`.`urut`
				FROM
				`db_ptiik_apps`.`tbl_karyawan`
				Left Join `db_ptiik_apps`.`tbl_file` ON `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` = `db_ptiik_apps`.`tbl_file`.`karyawan_id`
				Left Join `db_ptiik_apps`.`tbl_jabatan` ON `db_ptiik_apps`.`tbl_karyawan`.`jabatan_id` = `db_ptiik_apps`.`tbl_jabatan`.`jabatan_id`
				left Join `db_ptiik_apps`.`tbl_unitkerja` ON `db_ptiik_apps`.`tbl_karyawan`.`unit_id` = `db_ptiik_apps`.`tbl_unitkerja`.`unit_id`
				WHERE
				`db_ptiik_apps`.`tbl_karyawan`.`is_aktif` =  'aktif' AND
				`db_ptiik_apps`.`tbl_karyawan`.`home_base` =  'PTIIK'
				
				";
		if($id){
			$sql = $sql . " AND (`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` = '".$id."' OR (mid(md5(`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`),5,5) = '".$id."') )";
		}
		
		if($isdosen){
			switch ($isdosen){
				case 'dosen':
					$sql 	= $sql . " AND `db_ptiik_apps`.`tbl_karyawan`.`is_dosen` = '1' ";
					//$order 	= " case when tbl_jabatan.urut is null then `db_ptiik_apps`.`tbl_karyawan`.`pin` else tbl_jabatan.urut end";
					$order	= " CAST(IFNULL(tbl_jabatan.urut,'99') as UNSIGNED ), `db_ptiik_apps`.`tbl_karyawan`.`nama`";
				break;
				case 'staff':
					$sql 	= $sql . " AND `db_ptiik_apps`.`tbl_karyawan`.`is_dosen` = '0' ";
					//$order 	= " case when tbl_jabatan.urut is null then `db_ptiik_apps`.`tbl_karyawan`.`nama` else tbl_jabatan.urut end";
					$order	= " CAST(IFNULL(tbl_jabatan.urut,'99') as UNSIGNED ), `db_ptiik_apps`.`tbl_karyawan`.`nama`";
				break;
			}
			
		}else{
			$order = " `db_ptiik_apps`.`tbl_karyawan`.`nama` ASC";
		}
		
		if($ruang){
			$sql = $sql . " AND db_ptiik_apps.tbl_karyawan.ruang LIKE '%".$ruang."%' ";
		}
				
		$sql = $sql. " ORDER BY ".$order;
		$result = $this->db->query( $sql );

		return $result;	
	}
	
	/*master jadwal dosen reservasi*/
	function get_jadwal_dosen_resv($isprak=NULL, $id=NULL, $hari=NULL, $semester=NULL){
		$sql="SELECT DISTINCT
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mk`,
				db_ptiik_apps.`vw_jadwaldosen`.`namamk`,
				db_ptiik_apps.`vw_jadwaldosen`.`sks`,
				db_ptiik_apps.`vw_jadwaldosen`.`kurikulum`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_pendek`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_koordinator`,
				db_ptiik_apps.`vw_jadwaldosen`.`nik`,
				db_ptiik_apps.`vw_jadwaldosen`.`nama`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwaldosen`.`kelas_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`karyawan_id`,
				db_ptiik_apps.vw_jadwaldosen.prodi_id,
				db_ptiik_apps.vw_jadwaldosen.prodi,
				db_ptiik_apps.vw_jadwaldosen.mkditawarkan_id
				FROM
					db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					1= 1 
				";
		if($id){
			$sql = $sql . " AND db_ptiik_apps.`vw_jadwaldosen`.`karyawan_id` = '".$id."' ";
		}
		if($isprak=="praktikum"){
			$sql = $sql . " AND db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum` = '1' ";
		}else{
			$sql = $sql . " AND db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum` = '0' ";
		}
		
		if($hari){
			$sql = $sql . " AND db_ptiik_apps.`vw_jadwaldosen`.`hari` = '".$hari."' ";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . " ORDER BY db_ptiik_apps.`vw_jadwaldosen`.`namamk` ASC, db_ptiik_apps.`vw_jadwaldosen`.`kelas_id` ASC ";
		
		
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
	}
	
	/*master jadwal dosen */
	function get_jadwal_dosen($id=NULL, $hari=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mk`,
				db_ptiik_apps.`vw_jadwaldosen`.`namamk`,
				db_ptiik_apps.`vw_jadwaldosen`.`sks`,
				db_ptiik_apps.`vw_jadwaldosen`.`kurikulum`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_pendek`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_koordinator`,
				db_ptiik_apps.`vw_jadwaldosen`.`nik`,
				db_ptiik_apps.`vw_jadwaldosen`.`nama`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwaldosen`.`kelas_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`repeat_on`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum`,
				db_ptiik_apps.`vw_jadwaldosen`.`hari`,
				db_ptiik_apps.`vw_jadwaldosen`.`ruang`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					1= 1 
				";
		if($id){
			$sql = $sql . " AND db_ptiik_apps.`vw_jadwaldosen`.`karyawan_id` = '".$id."' ";
		}
		
		if($hari){
			$sql = $sql . " AND db_ptiik_apps.`vw_jadwaldosen`.`hari` = '".$hari."' ";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` ASC";
		
		
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
	}
	
	function get_ujian_dosen($jenis=NULL, $id=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwalujian`.`kode_mk`,
				db_ptiik_apps.`vw_jadwalujian`.`namamk`,
				db_ptiik_apps.`vw_jadwalujian`.`sks`,
				db_ptiik_apps.`vw_jadwalujian`.`kurikulum`,
				db_ptiik_apps.`vw_jadwalujian`.`tahun`,
				db_ptiik_apps.`vw_jadwalujian`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwalujian`.`is_pendek`,
				db_ptiik_apps.`vw_jadwalujian`.`is_project`,
				db_ptiik_apps.`vw_jadwalujian`.`nik`,
				db_ptiik_apps.`vw_jadwalujian`.`nama`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwalujian`.`kelas_id`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwalujian`.`tgl`,
				db_ptiik_apps.`vw_jadwalujian`.`hari`,
				db_ptiik_apps.`vw_jadwalujian`.`ruang`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi_id`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					1 = 1 
				";
		if($jenis){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian` = '".$jenis."'";
		}
		
		if($id){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`karyawan_id` = '".$id."'";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwalujian`.`jam_mulai` ASC";
		
		
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
	}
	
	function get_jadwal_by_ruang($id=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mk`,
				db_ptiik_apps.`vw_jadwaldosen`.`namamk`,
				db_ptiik_apps.`vw_jadwaldosen`.`sks`,
				db_ptiik_apps.`vw_jadwaldosen`.`kurikulum`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_pendek`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_koordinator`,
				db_ptiik_apps.`vw_jadwaldosen`.`nik`,
				db_ptiik_apps.`vw_jadwaldosen`.`nama`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwaldosen`.`kelas_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`repeat_on`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_selesai`,	
				db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum`,				
				db_ptiik_apps.`vw_jadwaldosen`.`hari`,
				db_ptiik_apps.`vw_jadwaldosen`.`ruang`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					db_ptiik_apps.`vw_jadwaldosen`.`ruang` = '".$id."' 
				";
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` ASC";
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
	}
	
	function get_ujian_by_ruang($jenis=NULL, $id=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwalujian`.`kode_mk`,
				db_ptiik_apps.`vw_jadwalujian`.`namamk`,
				db_ptiik_apps.`vw_jadwalujian`.`sks`,
				db_ptiik_apps.`vw_jadwalujian`.`kurikulum`,
				db_ptiik_apps.`vw_jadwalujian`.`tahun`,
				db_ptiik_apps.`vw_jadwalujian`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwalujian`.`is_pendek`,
				db_ptiik_apps.`vw_jadwalujian`.`is_project`,
				db_ptiik_apps.`vw_jadwalujian`.`nik`,
				db_ptiik_apps.`vw_jadwalujian`.`nama`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwalujian`.`kelas_id`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_selesai`,	
				db_ptiik_apps.`vw_jadwalujian`.`tgl`,				
				db_ptiik_apps.`vw_jadwalujian`.`hari`,
				db_ptiik_apps.`vw_jadwalujian`.`ruang`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi_id`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					1=1 
				";
		if($jenis){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian` = '".$jenis."'";
		}
		
		if($id){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`ruang` = '".$id."' ";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwalujian`.`jam_mulai` ASC";
		
		
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
	}
	
	function get_jadwal_by_hari($fak=NULL,$id=NULL, $semester=NULL, $praktikum=NULL, $mulai=NULL, $selesai=NULL, $ruang=NULL, $jadwal=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwaldosen`.`jadwal_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`mkditawarkan_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mk`,
				db_ptiik_apps.`vw_jadwaldosen`.`namamk`,
				db_ptiik_apps.`vw_jadwaldosen`.`sks`,
				db_ptiik_apps.`vw_jadwaldosen`.`kurikulum`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_pendek`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_koordinator`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik`,
				db_ptiik_apps.`vw_jadwaldosen`.`karyawan_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`dosen_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`nik`,
				db_ptiik_apps.`vw_jadwaldosen`.`nama`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwaldosen`.`kelas_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`repeat_on`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum`,
				db_ptiik_apps.`vw_jadwaldosen`.`hari`,
				db_ptiik_apps.`vw_jadwaldosen`.`ruang`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					db_ptiik_apps.`vw_jadwaldosen`.`hari` = '".$id."'  
				";
				
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		if($praktikum){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum` = '".$praktikum."' ";
		}
		
		if($mulai){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` = '".$mulai."' ";
		}
		
		if($selesai){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`jam_selesai` = '".$selesai."' ";
		}
		
		if($ruang){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`ruang` = '".$ruang."' ";
		}
		if($jadwal){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`jadwal_id` = '".$jadwal."' ";
		}
		
		if($fak) $sql.= " AND db_ptiik_apps.`vw_jadwaldosen`.`prodi_id` IN (SELECT prodi_id FROM db_ptiik_apps.tbl_prodi WHERE fakultas_id='$fak') ";
		
		$sql = $sql . " ORDER BY db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` ASC,db_ptiik_apps.`vw_jadwaldosen`.tgl_selesai ASC, db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` ASC";
		
		if($jadwal){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
				
		return $result;	
	}
	
	function get_ujian_by_hari($jenis=NULL, $id=NULL, $tgl=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwalujian`.`kode_mk`,
				db_ptiik_apps.`vw_jadwalujian`.`namamk`,
				db_ptiik_apps.`vw_jadwalujian`.`sks`,
				db_ptiik_apps.`vw_jadwalujian`.`kurikulum`,
				db_ptiik_apps.`vw_jadwalujian`.`tahun`,
				db_ptiik_apps.`vw_jadwalujian`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwalujian`.`is_pendek`,
				db_ptiik_apps.`vw_jadwalujian`.`nik`,
				db_ptiik_apps.`vw_jadwalujian`.`nama`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwalujian`.`kelas_id`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwalujian`.`is_project`,
				db_ptiik_apps.`vw_jadwalujian`.`hari`,
				db_ptiik_apps.`vw_jadwalujian`.`ruang`,
				db_ptiik_apps.`vw_jadwalujian`.`tgl`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi_id`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi`
				FROM
					db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					1 = 1
				";
		if($jenis){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian` = '".$jenis."'";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}
		
		if($id){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`hari` = '".$id."'  ";
		}
		
		if($tgl){
			$sql = $sql . " AND db_ptiik_apps.`vw_jadwalujian`.`tgl` = '".$tgl."' ";
		}
		
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwalujian`.`jam_mulai` ASC";
		$result = $this->db->query( $sql );
				
		return $result;	
	}
	
	function get_tgl_ujian_by_hari($jenis=NULL, $id=NULL, $semester=NULL){
		$sql="SELECT DISTINCT 
				db_ptiik_apps.`vw_jadwalujian`.`tgl`
				FROM
					db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					db_ptiik_apps.`vw_jadwalujian`.`hari` = '".$id."' AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian`='".$jenis."'
				";
				
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}
			
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_tgl_ujian_by_mk($jenis=NULL,$id=NULL, $semester=NULL){
		$sql="SELECT DISTINCT 
				db_ptiik_apps.`vw_jadwalujian`.`tgl`
				FROM
					db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					db_ptiik_apps.`vw_jadwalujian`.`mkditawarkan_id` = '".$id."' AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian`='".$jenis."' 
				";
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_tgl_ujian_by_dosen($jenis=NULL,$id=NULL,$semester=NULL){
		$sql="SELECT DISTINCT 
				db_ptiik_apps.`vw_jadwalujian`.`tgl`
				FROM
					db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					db_ptiik_apps.`vw_jadwalujian`.`karyawan_id` = '".$id."' AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian`='".$jenis."' 
				";
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_tgl_ujian_by_ruang($jenis=NULL,$id=NULL, $semester=NULL){
		$sql="SELECT DISTINCT 
				db_ptiik_apps.`vw_jadwalujian`.`tgl`
				FROM
					db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					db_ptiik_apps.`vw_jadwalujian`.`ruang` = '".$id."' AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian`='".$jenis."'
				";
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}
			
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_jadwal_by_mk($id=NULL, $hari=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mk`,
				db_ptiik_apps.`vw_jadwaldosen`.`namamk`,
				db_ptiik_apps.`vw_jadwaldosen`.`sks`,
				db_ptiik_apps.`vw_jadwaldosen`.`kurikulum`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_pendek`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_koordinator`,
				db_ptiik_apps.`vw_jadwaldosen`.`nik`,
				db_ptiik_apps.`vw_jadwaldosen`.`nama`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwaldosen`.`kelas_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`repeat_on`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum`,
				db_ptiik_apps.`vw_jadwaldosen`.`hari`,
				db_ptiik_apps.`vw_jadwaldosen`.`ruang`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi`
				FROM
				db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					1=1 
				";
		if($id){
			$sql = $sql . "AND db_ptiik_apps.`vw_jadwaldosen`.`mkditawarkan_id` = '".$id."'";
		}
		
		if($hari){
			$sql = $sql . "AND lcase(db_ptiik_apps.`vw_jadwaldosen`.`hari`) = '".$hari."'";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` ASC";
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_ujian_by_mk($jenis=NULL, $id=NULL, $hari=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwalujian`.`kode_mk`,
				db_ptiik_apps.`vw_jadwalujian`.`namamk`,
				db_ptiik_apps.`vw_jadwalujian`.`sks`,
				db_ptiik_apps.`vw_jadwalujian`.`kurikulum`,
				db_ptiik_apps.`vw_jadwalujian`.`tahun`,
				db_ptiik_apps.`vw_jadwalujian`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwalujian`.`is_pendek`,
				db_ptiik_apps.`vw_jadwalujian`.`nik`,
				db_ptiik_apps.`vw_jadwalujian`.`nama`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwalujian`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwalujian`.`kelas_id`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwalujian`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwalujian`.`is_project`,
				db_ptiik_apps.`vw_jadwalujian`.`hari`,
				db_ptiik_apps.`vw_jadwalujian`.`ruang`,
				db_ptiik_apps.`vw_jadwalujian`.`tgl`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi_id`,
				db_ptiik_apps.`vw_jadwalujian`.`prodi`
				FROM
				db_ptiik_apps.`vw_jadwalujian`
				WHERE 
					1=1 
				";
		if($jenis){
			$sql = $sql . "AND db_ptiik_apps.`vw_jadwalujian`.`jenis_ujian` = '".$jenis."'";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}
		
		if($id){
			$sql = $sql . "AND db_ptiik_apps.`vw_jadwalujian`.`mkditawarkan_id` = '".$id."'";
		}
		
		if($hari){
			$sql = $sql . "AND lcase(db_ptiik_apps.`vw_jadwalujian`.`hari`) = '".$hari."'";
		}
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwalujian`.`jam_mulai` ASC";
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	
	function get_ruang_by_mk($id=NULL, $hari=NULL, $by=NULL, $semester=NULL){
		$sql = "SELECT DISTINCT
				`db_ptiik_apps`.`vw_jadwaldosen`.`ruang`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`hari`
				FROM
					db_ptiik_apps.`vw_jadwaldosen` 
				WHERE 
					
					1=1 
				";
		switch($by){
			case 'prak':
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwaldosen`.`mkditawarkan_id` = '".$id."'  AND db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum` = '1'";
			break;
			case 'mk';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwaldosen`.`mkditawarkan_id` = '".$id."'";
			break;
			case 'dosen';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwaldosen`.`karyawan_id` = '".$id."'";
			break;
			case 'ruang';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwaldosen`.`ruang` = '".$id."' ";
			break;
		
		}
		
		if($hari){
			$sql = $sql . "AND lcase(db_ptiik_apps.`vw_jadwaldosen`.`hari`) = '".$hari."'";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`vw_jadwaldosen`.`hari` ASC, db_ptiik_apps.`vw_jadwaldosen`.`ruang` ASC";
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
				
	}
	
	function get_ruang_ujian_by_mk($id=NULL, $hari=NULL, $by=NULL, $semester=NULL){
		$sql = "SELECT DISTINCT
				`db_ptiik_apps`.`vw_jadwalujian`.`ruang`,
				`db_ptiik_apps`.`vw_jadwalujian`.`hari`
				FROM
					db_ptiik_apps.`vw_jadwalujian` 
				WHERE 
					
					1=1 
				";
		switch($by){
			case 'prak':
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalujian`.`mkditawarkan_id` = '".$id."'  AND db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum` = '1'";
			break;
			case 'mk';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalujian`.`mkditawarkan_id` = '".$id."'";
			break;
			case 'dosen';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalujian`.`karyawan_id` = '".$id."'";
			break;
			case 'ruang';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalujian`.`ruang` = '".$id."' ";
			break;
		
		}
		
		if($hari){
			$sql = $sql . "AND lcase(db_ptiik_apps.`vw_jadwalujian`.`hari`) = '".$hari."'";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`vw_jadwalujian`.`hari` ASC, db_ptiik_apps.`vw_jadwalujian`.`ruang` ASC";
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
				
	}
	
	function get_ruang_by_bimbingan($id=NULL, $hari=NULL, $by=NULL){
		$sql = "SELECT DISTINCT
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`ruang`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`ruang` as `id`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`ruang` as `value`
				FROM
					db_ptiik_apps.`vw_jadwalbimbingan` 
				WHERE 
					
					1=1 
				";
				
		switch($by){			
			case 'dosen';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`karyawan_id` = '".$id."'";
			break;
			case 'ruang';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`ruang` = '".$id."' ";
			break;
		
		}
		
		if($hari){
			$sql = $sql . "AND lcase(db_ptiik_apps.`vw_jadwalbimbingan`.`hari`) = '".$hari."'";
		}
		
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwalbimbingan`.`ruang` ASC";
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
				
	}
	
	function get_dosen_bimbingan($id=NULL, $ruang=NULL, $by=NULL){
		$sql = "SELECT DISTINCT
				mid(md5(db_ptiik_apps.vw_jadwalbimbingan.karyawan_id),5,5) as `id`,
				db_ptiik_apps.vw_jadwalbimbingan.karyawan_id,
				db_ptiik_apps.vw_jadwalbimbingan.ruang,
				db_ptiik_apps.vw_jadwalbimbingan.nama,
				db_ptiik_apps.vw_jadwalbimbingan.nik,
				db_ptiik_apps.vw_jadwalbimbingan.gelar_awal,
				db_ptiik_apps.vw_jadwalbimbingan.gelar_akhir,
				db_ptiik_apps.tbl_file.file_name as `foto`,
				db_ptiik_apps.tbl_file.is_jenis,
				db_ptiik_apps.tbl_file.url,
				db_ptiik_apps.tbl_jabatan.keterangan AS jabatan
				FROM
				db_ptiik_apps.vw_jadwalbimbingan
				LEFT JOIN db_ptiik_apps.tbl_file ON db_ptiik_apps.vw_jadwalbimbingan.karyawan_id = db_ptiik_apps.tbl_file.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_jabatan ON db_ptiik_apps.vw_jadwalbimbingan.jabatan_id = db_ptiik_apps.tbl_jabatan.jabatan_id
				WHERE
				db_ptiik_apps.tbl_file.is_jenis = 'foto'  
				";
				
		switch($by){			
			case 'dosen';
				$sql = $sql . "AND (db_ptiik_apps.`vw_jadwalbimbingan`.`karyawan_id` = '".$id."' OR mid(md5(db_ptiik_apps.`vw_jadwalbimbingan`.`karyawan_id`),5,5)='".$id."')";
			break;
			case 'ruang';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`ruang` = '".$id."' ";
			break;
			case 'hari';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`hari` = '".$id."' ";
			break;
		
		}
		
		if($ruang){
			$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`ruang` = '".$ruang."'";
		}
		
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`vw_jadwalbimbingan`.`nama` ASC, db_ptiik_apps.`vw_jadwalbimbingan`.`hari` ASC";
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
				
	}
	
	function get_jadwal_by_bimbingan($id=NULL){
		$sql = "SELECT
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`jenis_kegiatan_id`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`tahun_akademik`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`ruang`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`hari`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`jam_mulai`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`jam_selesai`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`tahun`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`is_ganjil`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`is_pendek`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`karyawan_id`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`nama`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`gelar_awal`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`gelar_akhir`
				FROM
				`db_ptiik_apps`.`vw_jadwalbimbingan`
				";
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`vw_jadwalbimbingan`.`nama` ASC,`db_ptiik_apps`.`vw_jadwalbimbingan`.`jam_mulai` ASC, `db_ptiik_apps`.`vw_jadwalbimbingan`.`ruang` ASC";
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function draw_jadwal_by_type( $by=NULL, $type=NULL, $id=NULL, $url=NULL){
		$jadwal	=$this->get_jadwal_by_bimbingan($id);
		
		$ruang	= $this->get_ruang_by_bimbingan();
		
		switch($type){
			case 'hari';
				$strheader = "Hari : ".ucFirst($id);
				$hari 	= $this->get_hari($id,"'sabtu','minggu'");
				
			break;

			case 'dosen':
				$dosen = $this->get_nama_dosen($id);
				
				if(($dosen->gelar_awal)&&($dosen->gelar_awal!="-")){
					$gawal= ", ".$dosen->gelar_awal;
				}else{
					$gawal="";
				}
				
				if(($dosen->gelar_akhir)&&($dosen->gelar_akhir!="-")){
					$gakhir= ", ".$dosen->gelar_akhir;
				}else{
					$gakhir="";
				}
					
				
				$strheader = "Dosen Pembimbing : ".$dosen->name.$dosen->gelar_awal.$gawal.$gakhir;
				$hari 	= $this->get_hari('',"'sabtu','minggu'");
				
				
			break;

			case 'ruang':
				$strheader = "Ruang Bimbingan : ".ucFirst($id);
				$hari 	= $this->get_hari('',"'sabtu','minggu'");
			break;
			
			default:
				$strheader = "Jadwal Bimbingan & Konsultasi";
				$hari 	= $this->get_hari('',"'sabtu','minggu'");
			break;

		}
		
		//echo $hari;
		?>
		
		<div class="table-responsive">
			<table class="table table-bordered table-jadwal">
			<thead>
				<tr class="calendar-row">
					<th class="calendar-day-head" colspan=2>R/H</th>
					<?php
						foreach($hari as $dt):
							echo "<th class='calendar-day-head'>".ucWords($dt->value)."</th>";
						endforeach;
					?>
				</tr>
			</thead>
		<tbody>
		<?php
		foreach($ruang as $dtruang):
			$jmlrows = $this->get_dosen_bimbingan($id, strtolower($dtruang->ruang), $type);
			$dosen	 = $this->get_dosen_bimbingan($id, strtolower($dtruang->ruang), $type);
			
			$x=0;
			
			if($jmlrows){
				foreach($jmlrows as $dt):
					$x++;
					
					$rows = $x;
				endforeach;		
			}else{
				$rows = 0;
			}
			
			if($dosen){
				$j=1;
				foreach($dosen as $ddosen):	
					if($type=='dosen'){
						$surl 	= $url."info/details/".$ddosen->id."/month";
					}else{
						$surl 	= $url."info/jadwal/bimbingan/dosen/".$ddosen->id;
					}
					
					if(($ddosen->gelar_awal!="")&&($ddosen->gelar_awal!="-")){
						$gelarawal= ", ".$ddosen->gelar_awal;
					}else{
						$gelarawal="";
					}
					
					if(($ddosen->gelar_akhir!="")&&($ddosen->gelar_akhir!="-")){
						$gelarakhir= ", ".$ddosen->gelar_akhir;
					}else{
						$gelarakhir="";
					}
					
					
					if(! $ddosen->foto){
						$foto= "http://ptiik.ub.ac.id/apps/assets/uploads/foto/no_foto.png";
					} else {
						$foto = $ddosen->url;
					}	
					
					if(strtolower($dtruang->ruang)==strtolower($ddosen->ruang)){
						if($j==1){
							echo "<tr><td rowspan='".$rows."' class='ruang'><b>".ucFirst($dtruang->ruang)."</b></td>";
							echo "<td class='no-shadow'>";
							echo "<div align='left' class='text text-default'><div class='media'>
								  <a class='pull-left' href=".$surl." style='	margin-right: 5px;margin-bottom: 10px;'><img src=".$foto." class='img-thumbnail' width='50' height='50'></a></div>
							<div class='media-body'>
								<b class='media-heading'>".ucWords($ddosen->nama).$gelarawal.$gelarakhir."</b>";								
							echo "<br>$ddosen->nik &nbsp;<span class='text text-info' style='font-weight:normal'>$ddosen->jabatan</span>";								
							echo "<br><code>R. $ddosen->ruang </code>
								</div></div>";
							echo "</td>";
						}else{
							echo "<tr>";
							echo "<td class='no-shadow'>";
							echo "<div align='left' class='text text-default'><div class='media'>
								  <a class='pull-left' href=".$surl." style='
									margin-right: 5px;margin-bottom: 10px;
								'><img src=".$foto." class='img-thumbnail' width='50' height='50'></a></div>
							<div class='media-body'>
								<b class='media-heading'>".ucWords($ddosen->nama).$gelarawal.$gelarakhir."</b>";								
							echo "<br>$ddosen->nik &nbsp;<span class='text text-info' style='font-weight:normal'>$ddosen->jabatan</span>";								
							echo "<br><code>R. $ddosen->ruang </code>
								</div></div>";
						}
						
						$i=0;
						$skip = false;
						foreach($hari as $dt):
							$detail = $this->get_detail_bimbingan($ddosen->karyawan_id, $ddosen->ruang, $dt->id);
							
							if($detail){
								if($detail == 1){
								echo "<td class='tif'>";
									foreach($detail as $detail):
										echo $detail->mulai." - ".$detail->selesai;
									endforeach;
								echo "</td>";
								}else{
									echo "<td align='left'><ul class='list-inline'>";
										$x=0;
										foreach($detail as $row):
											$x++;											
											echo "<li class='label label-info' style='font-weight:normal;font-size:11px;text-shadow:none'><i class='fa fa-clock-o'></i> ".$row->mulai." - ".$row->selesai."</li> ";
										endforeach;
									echo "</ul></td>";
								}
							}else{
								echo "<td>&nbsp;</td>";
							}
						endforeach;
						echo "</tr>";
					}
					$j++;
				endforeach;
			}
		endforeach;		
	?>
	</tbody>
	</table>
	</div>
				</div>
			</div>
		</div>
	</section>
</section>
	<?php			
	}
	
	function get_detail_bimbingan($dosen=NULL, $ruang=NULL, $hari=NULL){
		$sql = "SELECT time_format(jam_mulai, '%H:%i') as `mulai`, time_format(jam_selesai, '%H:%i') as `selesai` FROM `db_ptiik_apps`.`vw_jadwalbimbingan` WHERE 
					karyawan_id='".$dosen."'  AND ruang='".$ruang."' AND hari='".strtolower($hari)."' ORDER BY jam_mulai ASC  ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function draw_ujian_by_mk($jenis=NULL, $id=NULL, $by=NULL){
		switch($by){
			
			case 'mk';
				$jadwal	= $this->get_ujian_by_mk($jenis, $id);
				$tgl    = $this->get_tgl_ujian_by_mk($jenis, $id);
			break;
			case 'dosen';
				$jadwal	=$this->get_ujian_dosen($jenis, $id);
				$tgl    = $this->get_tgl_ujian_by_dosen($jenis, $id);
			break;
			case 'ruang';
				$jadwal	=$this->get_ujian_by_ruang($jenis, $id);
				$tgl    = $this->get_tgl_ujian_by_ruang($jenis, $id);
			break;
		
		}
		$hari 	= $this->get_hari();
		
		$jam	=$this->get_jam_mulai(1);
		
		if($tgl){				
			?>
			<ul class="nav nav-tabs" id="writeTab">
				<?php
				$k=0;
					foreach($tgl as $dt):
						$k++;
					
						if($k==1){ $str="class=active";  }else{ $str="";}
						?><li <?php echo $str; ?>><a href="#<?php echo date("Ymd", strtotime($dt->tgl)); ?>" data-toggle="tab"><?php echo date("M d, Y", strtotime($dt->tgl)); ?></a></li><?php
					endforeach;
				?>
			</ul>
			<div class="tab-content">	
			<?php
					$k=0;
				foreach($tgl as $row):
					$k++;
					
					if($k==1){ $str="active";  }else{ $str="";}
					?>
					<div class="tab-pane <?php echo $str; ?>" id="<?php echo date("Ymd", strtotime($row->tgl)); ?>">
						<?php
					if($jadwal){
						foreach($jadwal as $dt) {
							$namamk	= $dt->namamk;
							$kodemk	= $dt->kode_mk;
							$dosen	= $dt->nama;
							
							if(($dt->gelar_awal!="")&&($dt->gelar_awal!="-")){
								$gelarawal= ", ".$dt->gelar_awal;
							}else{
								$gelarawal="";
							}
								
							if(($dt->gelar_akhir!="")&&($dt->gelar_akhir!="-")){
								$gelarakhir= ", ".$dt->gelar_akhir;
							}else{
								$gelarakhir="";
							}
							
							
						}
						
					}else{
						$namamk	= "";
						$kodemk	= "";
						$dosen	= "";
						$gelarawal="";
						$gelarakhir="";
						
						echo "<div class='well'><span class='text text-error'>Data tidak ditemukan</span></div>";
						$this->error;
						return false;
					}

					switch($by){
						case 'hari';
							$strheader = "<i class='fa fa-clock-o'></i> ".ucFirst($idhari).", ". date("M d, Y", strtotime($row->tgl));
						break;

						case 'dosen':
							$strheader = "<i class='fa fa-clock-o'></i> ".date("M d, Y", strtotime($row->tgl)). "&nbsp;&nbsp;<span class='text-warning'>".$dosen.$gelarawal.$gelarakhir."</span>";
						break;

						case 'mk':
							$strheader = "<i class='fa fa-clock-o'></i> ".date("M d, Y", strtotime($row->tgl)). "&nbsp;&nbsp;<span class='text-warning'> ".$kodemk. " - ". $namamk."</span>";
						break;

						
						case 'ruang':
							$strheader = "<i class='fa fa-clock-o'></i> ".date("M d, Y", strtotime($row->tgl)). "&nbsp;&nbsp;<span class='text-warning'> R. ".ucFirst($id)."</span>";
						break;

					}
					?>
					<h4><?php echo $strheader; ?></h4>
					<div class="legenda">
						<div class="alert alert-danger">
							  <button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Perhatian!</strong> Untuk jadwal ujian hari <span class="label label-danger">Jumat</span>, ujian sesi ke <b>2</b> dimulai pukul <span class="label label-danger">09.30</span>.
							</div>
						<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Prodi Informatika</small>
						<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Prodi Sistem Komputer</small>
						<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Prodi Sistem Infomasi</small>
						<span class="label label-inverse">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Project</small>
						<code class="pull-right">Informasi jadwal ujian pada semester aktif.</code>
					</div>
					<div class="table-responsive">
					<table class="table table-bordered table-jadwal">
						<thead>
							<tr class="calendar-row">
								<th class="calendar-day-head" colspan=2>H/J</th>
								<?php
								
								$runday = date("N", strtotime($row->tgl));		
			
								$nhari = $this->get_namahari($runday);
									/*foreach($jam as $dtjam):
										echo "<th class='calendar-day-head'><small>".$dtjam->mulai." - ".$dtjam->selesai."</small></th>";
									endforeach;*/
									
									$i=0;
									foreach($jam as $dtjam):
										$i++;
										
										if($nhari=='jumat'):
											if($i==2):
												$mulai = date("H:i", strtotime('-30 minutes', strtotime($dtjam->mulai)));
												$selesai = date("H:i", strtotime('-30 minutes', strtotime($dtjam->selesai)));
											else:
												$mulai = date("H:i", strtotime($dtjam->mulai));
												$selesai = date("H:i", strtotime($dtjam->selesai));
											endif;
											
											echo "<th class='calendar-day-head'><small>".$mulai." - ".$selesai."</small></th>";
										else:
											echo "<th class='calendar-day-head'><small>".$dtjam->mulai." - ".$dtjam->selesai."</small></th>";
										endif;
									endforeach;
									
								?>
							</tr>
						</thead>
					<tbody>
					<?php
					foreach($hari as $dthari):
					$jmlrows = $this->get_ruang_ujian_by_mk($id, strtolower($dthari->value), $by);
					$ruang	 = $this->get_ruang_ujian_by_mk($id, strtolower($dthari->value), $by);
					
					$x=0;
					
					if($jmlrows){
						foreach($jmlrows as $dt):
							$x++;
							
							$rows = $x;
						endforeach;		
					}else{
						$rows = 0;
					}
					
					if($ruang){
						$j=1;
						foreach($ruang as $druang):									
							if(strtolower($dthari->value)==strtolower($druang->hari)){
								if($j==1){
									echo "<tr><td rowspan='".$rows."' class='ruang'>".ucFirst($dthari->value)."</td>";
									echo "<td class='ruang'>".ucFirst($druang->ruang)."</td>";
								}else{
									echo "<tr><td class='ruang'>".ucFirst($druang->ruang)."</td>";
								}
								
								$i=0;
								$skip = false;
								for($i = 0; $i< count($jam); $i++){									
									if($jam[$i]->is_istirahat==1){
										$class = "istirahat";
									}else{
										$class = "";
									}
									
									$skip = false;
									
									if(count($jadwal)>0){
										foreach($jadwal as $mk) {
											if(($mk->jam_mulai == $jam[$i]->mulai) && (strtolower($mk->hari) == strtolower($dthari->value)) && (strtolower($mk->ruang) == strtolower($druang->ruang)) && ($mk->tgl==$row->tgl)) {
												$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;
																							
												$colspan = $colspan;
												
																		
												switch($mk->prodi_id){
													case 'SISKOM':
														$class = "siskom";
													break;
													case 'SI':
														$class = "si";
													break;
													case 'ILKOM':
														$class = "tif";
													break;
												}
												if($mk->is_project=='0'){
													echo '<td colspan="'.$colspan.'" class="'.$class.'"><strong>'.$mk->namamk.'</strong><br>'.$mk->nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';
												}else{
													echo '<td colspan="'.$colspan.'" class="'.$class.'"><strong>'.$mk->namamk.'</strong><br><span class="label label-inverse">'.$mk->nama.'</span><br>( Kelas - '.$mk->kelas_id.' )</td>';
												}
												
												$i+=$colspan-1;
												$skip = true;
												break;
											}
										}
										if(!$skip) echo "<td class=$class>&nbsp;</td>";
									}else{
										 echo "<td class=$class>&nbsp;</td>";
									}
								}
								echo "</tr>";
							}
							$j++;
						endforeach;
					}
					endforeach;		
					?>
				</tbody>
				</table>
				<div class="legenda">
					<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<small class="margin">Prodi Informatika</small>
					<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<small class="margin">Prodi Sistem Komputer</small>
					<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<small>Prodi Sistem Infomasi</small>
				</div>
				</div>
			</div>
			<?php
			endforeach;
			?>
			</div>
		<?php
		}else{
			echo "<div class='well'><span class='text text-error'>Data tidak ditemukan</span></div>";
		}
		?>
				</div>
			</div>
		</div>
	</section>
</section>
		<?php
	}
	
	
	function draw_jadwal_by_mk($id=NULL, $by=NULL, $x=NULL){
		$hari 	= $this->get_hari();
		
		$jam	=$this->get_jam_mulai();
		
		$tgl 	= date("Y-m-d");
		$tglval = strtotime(date("Y-m-d"));
		
		switch($by){
			case 'prak':
				$jadwal	=$this->get_jadwal_by_prak($id);
			break;
			case 'mk';
				$jadwal	=$this->get_jadwal_by_mk($id);
			break;
			case 'dosen';
				$jadwal	=$this->get_jadwal_dosen($id);
			break;
			case 'ruang';
				$jadwal	=$this->get_jadwal_by_ruang($id);
			break;
		
		}
		
		
	?>
			<div class="table-responsive">
				<table class="table table-bordered table-jadwal">
					<thead>
						<tr class="calendar-row">
							<th class="calendar-day-head" colspan=2>H/J</th>
							<?php
								foreach($jam as $dt):
									echo "<th class='calendar-day-head'><small>".$dt->mulai." - ".$dt->selesai."</small></th>";
								endforeach;
							?>
						</tr>
					</thead>
				<tbody>
				<?php
				foreach($hari as $dthari):
					$jmlrows = $this->get_ruang_by_mk($id, strtolower($dthari->value), $by);
					$ruang	 = $this->get_ruang_by_mk($id, strtolower($dthari->value), $by);
					
					$x=0;
					
					if($jmlrows){
						foreach($jmlrows as $dt):
							$x++;
							
							$rows = $x;
						endforeach;		
					}else{
						$rows = 0;
					}
					
					if($ruang){
						$j=1;
						foreach($ruang as $druang):									
							if(strtolower($dthari->value)==strtolower($druang->hari)){
								if($j==1){
									echo "<tr><td rowspan='".$rows."'class='ruang' >".ucFirst($dthari->value)."</td>";
									echo "<td class='ruang'>".ucFirst($druang->ruang)."</td>";
								}else{
									echo "<tr><td class='ruang'>".ucFirst($druang->ruang)."</td>";
								}
								
								$i=0;
								$skip = false;
								for($i = 0; $i< count($jam); $i++){									
									if($jam[$i]->is_istirahat==1){
										$class = "istirahat";
									}else{
										$class = "";
									}
									
									$skip = false;
									
									if(count($jadwal)>0){
										foreach($jadwal as $mk) {
											if($mk->jam_mulai == $jam[$i]->mulai && strtolower($mk->hari) == strtolower($dthari->value) && strtolower($mk->ruang) == strtolower($druang->ruang)) {
												$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;
												
												if($colspan < 2){
													$colspan = $colspan +1;
												}else{
													$colspan = $colspan;
												}
												
												if($mk->repeat_on==""){
														$drepeat = "monthly";
														$dtmulai 	= $mk->tgl_mulai;
														$dtselesai 	= $mk->tgl_selesai;
														$label		= "<b>".$mk->namamk."</b>";
												}else{
													$drepeat = $mk->repeat_on;
													$dtmulai 	= $mk->tgl_mulai;
													$dtselesai 	=  $mk->tgl_selesai;
													$in = strtotime($dtselesai);
													$tglselesai	= strtotime(date("Y-m-d", strtotime('+1 days', $in)));
													
													
													if($drepeat=='daily'){
														if($tglval>$in){
															$label  = "";
														}else{
															$label	= "<b>".$mk->namamk."</b><br><span class='label label-danger'>".$mk->tgl_mulai." *</span>";
														}
													}else{
														$label		= "<b>".$mk->namamk."</b>";
													}
												}
													
																		
												switch($mk->prodi_id){
													case 'SISKOM':
														$class = "siskom";
													break;
													case 'SI':
														$class = "si";
													break;
													case 'ILKOM':
														$class = "tif";
													break;
												}
												
												if($mk->nama){
													$nama = $mk->nama;
												}else{
													$nama = 'Dosen Pengampu';
												}
													
												if($label){
													if($mk->is_praktikum=='0'){
														echo '<td colspan="'.$colspan.'" class="'.$class.'">'.$label.'<br>'.$nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';
													}else{
														echo '<td colspan="'.$colspan.'" class="'.$class.'">'.$label.'<br><span class="label label-inverse">'.$nama.'</span><br>( Kelas - '.$mk->kelas_id.' )</td>';
													}
												}else{
													echo '<td colspan="'.$colspan.'">&nbsp;</td>';
												}
												
												$i+=$colspan-1;
												$skip = true;
												break;
											}
										}
										if(!$skip) echo "<td class=$class>&nbsp;</td>";
									}else{
										 echo "<td class=$class>&nbsp;</td>";
									}
								}
								echo "</tr>";
							}
							$j++;
						endforeach;
					}
				endforeach;		
		?>
			</tbody>
			</table>
			<div class="legenda">
				<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<small class="margin">Prodi Informatika</small>
				<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<small class="margin">Prodi Sistem Komputer</small>
				<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<small>Prodi Sistem Infomasi</small>
			</div>
		
			</div>
				</div>
			</div>
		</div>
	</section>
</section>
<?php			
	}
	
	function get_jumlah_ruang($id=NULL, $dosen=NULL, $semester=NULL){
		$sql = "SELECT DISTINCT
				count(`db_ptiik_apps`.`vw_jadwaldosen`.`ruang`) as `data`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`hari`
				FROM
					db_ptiik_apps.`vw_jadwaldosen` 
				WHERE 1 = 1 ";
				
		if($id){			
			$sql = $sql ." AND db_ptiik_apps.`vw_jadwaldosen`.`mkditawarkan_id` = '".$id."' ";
		}
		if($dosen){			
			$sql = $sql ." AND db_ptiik_apps.`vw_jadwaldosen`.`karyawan_id` = '".$id."' ";
		}
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
			
		$sql = $sql ."	GROUP BY `db_ptiik_apps`.`vw_jadwaldosen`.`hari`, db_ptiik_apps.`vw_jadwaldosen`.`mkditawarkan_id`";		
	
		$result = $this->db->query( $sql );
		
		return $result;	
				
	}
	
	
	function get_jadwal_by_prak($id=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mk`,
				db_ptiik_apps.`vw_jadwaldosen`.`namamk`,
				db_ptiik_apps.`vw_jadwaldosen`.`sks`,
				db_ptiik_apps.`vw_jadwaldosen`.`kurikulum`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_pendek`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_koordinator`,
				db_ptiik_apps.`vw_jadwaldosen`.`nik`,
				db_ptiik_apps.`vw_jadwaldosen`.`nama`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwaldosen`.`kelas_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`repeat_on`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`tgl_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum`,
				db_ptiik_apps.`vw_jadwaldosen`.`hari`,
				db_ptiik_apps.`vw_jadwaldosen`.`ruang`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi`
				FROM
				db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					db_ptiik_apps.`vw_jadwaldosen`.`mkditawarkan_id` = '".$id."' 
					AND db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum`='1'
				";
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` ASC";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_jadwal_by_prodi($id=NULL, $semester=NULL){
		$sql="SELECT
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mk`,
				db_ptiik_apps.`vw_jadwaldosen`.`namamk`,
				db_ptiik_apps.`vw_jadwaldosen`.`sks`,
				db_ptiik_apps.`vw_jadwaldosen`.`kurikulum`,
				db_ptiik_apps.`vw_jadwaldosen`.`tahun`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_ganjil`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_pendek`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_koordinator`,
				db_ptiik_apps.`vw_jadwaldosen`.`nik`,
				db_ptiik_apps.`vw_jadwaldosen`.`nama`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_awal`,
				db_ptiik_apps.`vw_jadwaldosen`.`gelar_akhir`,
				db_ptiik_apps.`vw_jadwaldosen`.`kelas_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`jam_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_mulai`,
				db_ptiik_apps.`vw_jadwaldosen`.`kode_selesai`,
				db_ptiik_apps.`vw_jadwaldosen`.`is_praktikum`,
				db_ptiik_apps.`vw_jadwaldosen`.`hari`,
				db_ptiik_apps.`vw_jadwaldosen`.`ruang`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi_id`,
				db_ptiik_apps.`vw_jadwaldosen`.`prodi`
				FROM
				db_ptiik_apps.`vw_jadwaldosen`
				WHERE 
					db_ptiik_apps.`vw_jadwaldosen`.`prodi` = '".$id."' 
				";
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		
		$sql = $sql . "ORDER BY db_ptiik_apps.`vw_jadwaldosen`.`jam_mulai` ASC";
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	
	function get_agenda_civitas($tgl=NULL, $type=NULL, $id=NULL){
		$sql = "SELECT DISTINCT 
					db_ptiik_apps.tbl_kegiatan_karyawan.karyawan_id,
					db_ptiik_apps.tbl_kegiatan_karyawan.tgl,
					time_format(db_ptiik_apps.tbl_kegiatan_karyawan.jam_mulai, '%H:%i') as `jam_mulai`,
					time_format(db_ptiik_apps.tbl_kegiatan_karyawan.jam_selesai, '%H:%i') as `jam_selesai`,
					db_ptiik_apps.tbl_kegiatan_karyawan.ruang,
					db_ptiik_apps.tbl_kegiatan_karyawan.jenis_kegiatan,
					db_ptiik_apps.tbl_kegiatan_karyawan.kegiatan,
					db_ptiik_apps.tbl_kegiatan_karyawan.namamk,
					db_ptiik_apps.tbl_kegiatan_karyawan.keterangan,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_karyawan.is_dosen
					FROM
					db_ptiik_apps.tbl_kegiatan_karyawan
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_kegiatan_karyawan.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				WHERE 1 = 1
					";
		if($tgl){
			//$sql = $sql . " AND db_ptiik_apps.tbl_kegiatan_karyawan.tgl = '".$tgl."' ";
			$sql = $sql . " AND ('".$tgl."' BETWEEN db_ptiik_apps.tbl_kegiatan_karyawan.tgl AND db_ptiik_apps.tbl_kegiatan_karyawan.tgl_selesai) ";
		}
		
		if($type){
			switch ($type){
				case 'dosen':
					$sql 	= $sql . " AND `db_ptiik_apps`.`tbl_karyawan`.`is_dosen` = '1' ";
				break;
				case 'staff':
					$sql 	= $sql . " AND `db_ptiik_apps`.`tbl_karyawan`.`is_dosen` = '0' ";
				break;
			}
			
		}
		
		if($id){
			$sql = $sql . " AND (db_ptiik_apps.tbl_kegiatan_karyawan.karyawan_id = '".$id."' OR (mid(md5(db_ptiik_apps.tbl_kegiatan_karyawan.karyawan_id),5,5) = '".$id."')) ";
		}
		
		$sql = $sql . " ORDER BY time_format(db_ptiik_apps.tbl_kegiatan_karyawan.jam_mulai, '%H:%i') ";
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
	
	function get_civitas_kuliah($tgl=NULL, $id=NULL){
	
	}
	
	
	function get_civitas_agenda($tgl=NULL, $id=NULL){
		$sql = "SELECT DISTINCT 
					db_ptiik_apps.tbl_peserta.agenda_id,
					`db_ptiik_apps`.`tbl_agenda`.`judul`,
					`db_ptiik_apps`.`tbl_agenda`.`lokasi`,
					`db_ptiik_apps`.`tbl_agenda`.penyelenggara,
					`db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`,
					`tbl_jeniskegiatan`.`keterangan` as `jenis`,
					`db_ptiik_apps`.`tbl_detailagenda`.`tgl`,
					time_format(`db_ptiik_apps`.`tbl_detailagenda`.`jam_mulai`, '%H:%i') as `jam_mulai`,
					time_format(`db_ptiik_apps`.`tbl_detailagenda`.`jam_selesai`, '%H:%i') as `jam_selesai`,
					`db_ptiik_apps`.`tbl_detailagenda`.`ruang`,
					`db_ptiik_apps`.`tbl_unitkerja`.`nama` as `unit_penyelenggara`,
					`db_ptiik_apps`.`tbl_peserta`.`karyawan_id`
					FROM
					`db_ptiik_apps`.`tbl_peserta`
					Inner Join `db_ptiik_apps`.`tbl_agenda` ON `db_ptiik_apps`.`tbl_peserta`.`agenda_id` = `db_ptiik_apps`.`tbl_agenda`.`agenda_id`
					Inner Join `db_ptiik_apps`.`tbl_detailagenda` ON `db_ptiik_apps`.`tbl_agenda`.`agenda_id` = `db_ptiik_apps`.`tbl_detailagenda`.`agenda_id`
					Left Join `db_ptiik_apps`.`tbl_jeniskegiatan` ON `db_ptiik_apps`.`tbl_agenda`.`jenis_kegiatan_id` = `db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`
					left Join `db_ptiik_apps`.`tbl_unitkerja` ON `db_ptiik_apps`.`tbl_agenda`.`unit_id` = `db_ptiik_apps`.`tbl_unitkerja`.`unit_id`
				WHERE `tbl_agenda`.`is_valid` = '1'  
				";
		if($tgl){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_detailagenda`.`tgl` = '".$tgl."'  ";
		}
		
		if($id){
			$sql = $sql . " AND (`db_ptiik_apps`.`tbl_peserta`.`karyawan_id` = '".$id."'  OR mid(md5(`db_ptiik_apps`.`tbl_peserta`.`karyawan_id`)5,5) = '".$id."') ";
		}
		
		$sql = $sql . " ORDER BY time_format(`db_ptiik_apps`.`tbl_detailagenda`.`jam_mulai`, '%H:%i') ASC";
		
		$result = $this->db->query( $sql );
		//echo $sql."<br>";
		return $result;	
	}
	
	function get_civitas_detail($tgl=NULL,$id=NULL, $staff=NULL){
		$sql ="SELECT
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') as `jam_mulai`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') as `jam_selesai`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`dosen_id`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`ruang`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`jenis_kegiatan_id`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`kegiatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`catatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`kode_mk`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`namamk`
				FROM
				`db_ptiik_apps`.`tbl_detailjadwalruang`
				Inner Join `db_ptiik_apps`.`tbl_jadwalruang` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`
				Left Join `db_ptiik_apps`.`vw_jadwaldosen` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`vw_jadwaldosen`.`jadwal_id`
				WHERE  `db_ptiik_apps`.`tbl_detailjadwalruang`.`is_aktif` = '1'  
				";
		if($tgl){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl` = '".$tgl."' ";
		}
		
		if($id){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_detailjadwalruang`.`dosen_id` = '".$id."' ";
		}
		
		if($staff){
			$sql = $sql . " AND  (`db_ptiik_apps`.`vw_jadwaldosen`.`karyawan_id` = '".$staff."'  OR mid(md5(`db_ptiik_apps`.`vw_jadwaldosen`.`karyawan_id`)5,5) = '".$staff."') ";
		}
		
		$sql = $sql . " ORDER BY time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') ASC";
		
		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_row_mk($hari=NULL, $semester=NULL){
		$sql = "SELECT DISTINCT
					Count(`vw_jadwaldosen`.`ruang`) as `jml`,
					`vw_jadwaldosen`.`ruang`,
					`vw_jadwaldosen`.`hari`
					FROM
					db_ptiik_apps.`vw_jadwaldosen`
					GROUP BY
					`vw_jadwaldosen`.`ruang`,
					`vw_jadwaldosen`.`hari`
					HAVING 1 =1 					
					";
		if($hari){
			$sql = $sql . " AND `vw_jadwaldosen`.`hari` = '".$hari."' ";
		}
		
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_mk_ruang($hari=NULL, $semester=NULL){
		$sql = "SELECT 
					`vw_jadwaldosen`.`ruang`,
					`vw_jadwaldosen`.`hari`,
					`vw_jadwaldosen`.`kode_mk`,
					`vw_jadwaldosen`.`namamk`
					FROM
					db_ptiik_apps.`vw_jadwaldosen`
					WHERE 1 =1 					
					";
		if($hari){
			$sql = $sql . " AND `vw_jadwaldosen`.`hari` = '".$hari."' ";
		}
		if($semester){
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`tahun_akademik` = '".$semester."'";
		}else{
			$sql = $sql. "  AND db_ptiik_apps.`vw_jadwaldosen`.`semester_aktif` = '1' ";
		}
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	
	/*master dosen */
	function get_dosen(){
		$sql= "SELECT DISTINCT karyawan_id as `id`, nama as `name`, nik as `nik`, mid(md5(karyawan_id),5,5) as `tmpid` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE  is_dosen='1' AND is_aktif NOT IN ('keluar', 'meninggal')
				ORDER BY nama ASC";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_nama_dosen($id=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `name`, nik as `nik`, gelar_awal, gelar_akhir FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE  (karyawan_id='".$id."' OR mid(md5(karyawan_id),5,5)='".$id."' ) ";
	
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}	
	
	function get_dosen_pengampu(){
		$sql = "SELECT DISTINCT
					`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` AS `id`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama` AS `name`,
					`db_ptiik_apps`.`tbl_karyawan`.`nik` AS `nik`
					FROM
					`db_ptiik_apps`.`tbl_karyawan`
					Inner Join `db_ptiik_apps`.`tbl_pengampu` ON `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` = `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`
					Inner Join `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					Inner Join `db_ptiik_apps`.`tbl_tahunakademik` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` = `db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`
					WHERE
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` =  '1' AND `db_ptiik_apps`.`tbl_pengampu`.`is_aktif` = '1'  
					ORDER BY `db_ptiik_apps`.`tbl_karyawan`.`nama` ASC";
		$result = $this->db->query( $sql );
		
		return $result;			
	}
	
	/* master jam mulai*/
	function  get_jam_mulai($isujian=NULL){
		$sql = "SELECT TIME_FORMAT(jam_mulai,'%H:%i') as `mulai`, TIME_FORMAT(jam_selesai,'%H:%i') as `selesai`, urut, is_istirahat FROM db_ptiik_apps.tbl_jam  WHERE 1=1 ";
		
		if($isujian){
			$sql = $sql . " AND is_ujian='1' ";
		}else{
			$sql = $sql . " AND is_aktif='1' ";
		}
		
		$sql = $sql . " ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function  get_jam_mulai_pendek($isujian=NULL){
		$sql = "SELECT TIME_FORMAT(jam_mulai,'%H:%i') as `mulai`, TIME_FORMAT(jam_selesai,'%H:%i') as `selesai`, urut, is_istirahat FROM db_ptiik_apps.tbl_jam  WHERE is_pendek='1' ";
		
		$sql = $sql . " ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jam selesai*/
	function  get_jam_selesai(){
		$sql = "SELECT jam_mulai as `mulai`, jam_selesai as `selesai`, urut FROM db_ptiik_apps.tbl_jam  WHERE is_aktif=1 ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	/* master hari*/
	function  get_hari($id=NULL, $str=NULL){
		$sql = "SELECT hari as `id`, hari as `value` FROM db_ptiik_apps.tbl_hari WHERE 1=1 ";
		if($id){
			$sql = $sql . " AND lcase(hari)= '".$id."' ";
		}
		
		if($str){
			$sql = $sql . " AND lcase(hari) NOT IN ($str) ";
		}
		$sql = $sql . "ORDER BY db_ptiik_apps.tbl_hari.id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master ruang*/
	function  get_ruang($fak=NULL, $kat=NULL,$isujian=NULL, $semester=NULL){
		//$sql = "SELECT ruang as `id`, ruang as `value` FROM db_ptiik_apps.tbl_ruang  WHERE is_aktif=1 AND (is_kuliah=1 OR is_lab=1) ORDER BY ruang ASC";
		if($kat=="kuliah"){
			$sql = "SELECT ruang_id as `id`, kode_ruang as `value` FROM db_ptiik_apps.tbl_ruang  WHERE is_aktif=1 AND kategori_ruang IN ('kuliah','lab') AND fakultas_id='$fak' ORDER BY ruang_id ASC";
		}
		else{
			if($isujian){
				$sql = "SELECT DISTINCT ruang as `id`, ruang as `value`, vw_jadwalujian.semester_aktif FROM db_ptiik_apps.`vw_jadwalujian` WHERE prodi_id IN (SELECT prodi_id FROm db_ptiik_apps.tbl_prodi WHERE fakultas_id='$fak')";
				if($semester){
					$sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`tahun_akademik` = '".$semester."'";
				}else{
					// $sql = $sql. "  AND db_ptiik_apps.`vw_jadwalujian`.`semester_aktif` = '1' ";
				}
				$sql = $sql. " ORDER BY ruang ASC";
			}else{
				$sql = "SELECT DISTINCT ruang as `id`, ruang as `value` FROM 	db_ptiik_apps.`vw_jadwaldosen` WHERE prodi_id IN (SELECT prodi_id FROm db_ptiik_apps.tbl_prodi WHERE fakultas_id='$fak') ORDER BY ruang ASC";
			}
		}
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	/* master kelas*/
	function  get_kelas(){
		$sql = "SELECT kelas_id as `id`, kelas_id as `value` FROM db_ptiik_apps.tbl_kelas ORDER BY kelas_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master prodi*/
	function  get_prodi(){
		$sql = "SELECT prodi_id, singkat  as `id`, keterangan as `value` FROM `db_ptiik_apps`.tbl_prodi ORDER BY singkat ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk ditawarkan */
	function get_mkditawarkan($ispraktikum=NULL){
		$sql="SELECT DISTINCT 
				db_ptiik_apps.`tbl_mkditawarkan`.`mkditawarkan_id` as `id`,
				concat(`tbl_namamk`.`keterangan`,' (',`tbl_matakuliah`.`kode_mk`,')') as `value`			
				FROM
				db_ptiik_apps.`tbl_mkditawarkan`
				Inner Join db_ptiik_apps.`tbl_matakuliah` ON db_ptiik_apps.`tbl_mkditawarkan`.`matakuliah_id` = `tbl_matakuliah`.`matakuliah_id`
				Inner Join db_ptiik_apps.`tbl_namamk` ON `tbl_matakuliah`.`namamk_id` = `tbl_namamk`.`namamk_id`		
				Inner Join `db_ptiik_apps`.`tbl_tahunakademik` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` = `db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`
			  WHERE `db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` = '1' 
			";
			
		if($ispraktikum!=0){
			$sql = $sql . " AND db_ptiik_apps.`tbl_mkditawarkan`.`is_praktikum`= '".$ispraktikum."' ";
		}
		
		$sql = $sql . " ORDER BY db_ptiik_apps.`tbl_namamk`.`keterangan` ASC";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function read_layanan(){
		$sql = "SELECT
				mid(md5(dbptiik_layanan.tbl_permintaanlayanan.permintaan_id),5,5) as `id`,
				dbptiik_layanan.tbl_permintaanlayanan.permintaan_id,
				dbptiik_layanan.tbl_permintaanlayanan.judul,
				dbptiik_layanan.tbl_permintaanlayanan.status_proses,
				dbptiik_layanan.tbl_permintaanlayanan.keterangan,
				dbptiik_layanan.tbl_permintaanlayanan.no_permintaan,
				dbptiik_layanan.tbl_permintaanlayanan.periode,
				dbptiik_layanan.tbl_permintaanlayanan.tgl_permintaan,
				dbptiik_layanan.tbl_permintaanlayanan.approve_by,
				dbptiik_layanan.tbl_permintaanlayanan.catatan,
				dbptiik_layanan.tbl_statuslayanan.keterangan AS `status`,
				dbptiik_layanan.tbl_permintaanlayanan.request_dari,
				db_ptiik_apps.tbl_karyawan.nama,
				dbptiik_layanan.tbl_permintaanlayanan.unit_kerja_dituju,
				db_ptiik_apps.tbl_unitkerja.nama AS unit
				FROM
				dbptiik_layanan.tbl_permintaanlayanan
				INNER JOIN dbptiik_layanan.tbl_statuslayanan ON dbptiik_layanan.tbl_permintaanlayanan.status_proses = dbptiik_layanan.tbl_statuslayanan.status_proses
				INNER JOIN db_ptiik_apps.tbl_karyawan ON dbptiik_layanan.tbl_permintaanlayanan.request_dari = db_ptiik_apps.tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_unitkerja ON dbptiik_layanan.tbl_permintaanlayanan.unit_kerja_dituju = db_ptiik_apps.tbl_unitkerja.unit_id
				ORDER BY `dbptiik_layanan`.`tbl_permintaanlayanan`.`tgl_permintaan` DESC,
				dbptiik_layanan.tbl_permintaanlayanan.no_permintaan DESC
				LIMIT 0, 10";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_namahari($runday){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
			case '7': $hari='minggu'; break;
		}
		
		return $hari;
	}
	
	
	function read_slide(){
		$sql = "SELECT
					dbptiik_web.slide.slide_id,
					dbptiik_web.slide.slide_file,
					dbptiik_web.slide.slide_location,
					dbptiik_web.slide.slide_link,
					dbptiik_web.slide.slide_target,
					dbptiik_web.slide.slide_title,
					dbptiik_web.slide.is_active
					FROM
					dbptiik_web.slide
					WHERE
					dbptiik_web.slide.is_active = '1'
					LIMIT 0, 10";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
}