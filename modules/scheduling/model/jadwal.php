<?php
class model_jadwal extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
	
	function get_generate_praktikum(){
		$sql= "SELECT
				tbl_jadwalmk.jadwal_id,
				tbl_jadwalmk.kelas,
				tbl_jadwalmk.pengampu_id,
				tbl_jadwalmk.mkditawarkan_id,
				tbl_jadwalmk.cabang_id,
				tbl_jadwalmk.prodi_id,
				tbl_jadwalmk.jam_mulai,
				tbl_jadwalmk.jam_selesai,
				tbl_jadwalmk.hari,
				tbl_jadwalmk.ruang_id,
				tbl_jadwalmk.repeat_on,
				tbl_jadwalmk.tgl_mulai,
				tbl_jadwalmk.tgl_selesai,
				tbl_jadwalmk.is_online,
				tbl_jadwalmk.is_praktikum,
				tbl_jadwalmk.user_id,
				tbl_jadwalmk.is_aktif,
				tbl_jadwalmk.last_update,
				tbl_tahunakademik.tahun_akademik
				FROM
				db_ptiik_apps.tbl_jadwalmk
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_mkditawarkan.tahun_akademik = tbl_tahunakademik.tahun_akademik
				WHERE
				tbl_jadwalmk.is_praktikum = 1 AND
				tbl_tahunakademik.is_aktif = 1";
		$result = $this->db->query( $sql );
	
		return $result;

	}
	
	function get_hari_nama($id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_hari.hari,
					db_ptiik_apps.tbl_hari.hari_en,
					db_ptiik_apps.tbl_hari.id
					FROM
					db_ptiik_apps.tbl_hari WHERE 1=1 
					";
		if($id!="-"){
			$sql.= " AND db_ptiik_apps.tbl_hari.hari ='".$id."' ";			
		}
		
		$result = $this->db->query( $sql );
	
		return $result;
	}
	
	function get_jenis_kegiatan($id=NULL){
		$sql = "SELECT jenis_kegiatan_id, keterangan FROM db_ptiik_apps.`tbl_jeniskegiatan`  WHERE 1 = 1 ";
		
		if($id){
			$sql = $sql. " AND jenis_kegiatan_id IN ('kuliah', 'praktikum', 'bimbingan', 'uts', 'uas') ";
		}else{
			$sql = $sql. " AND jenis_kegiatan_id IN ('kuliah', 'praktikum') ";
		}
		
		$sql = $sql . " ORDER BY `tbl_jeniskegiatan`.keterangan ASC ";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_prodi_nama($id=NULL, $fakultas=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_prodi.prodi_id,
				db_ptiik_apps.tbl_prodi.keterangan
				FROM
				db_ptiik_apps.tbl_prodi WHERE 1 = 1 
				";
		if($id!="-"){
			$sql.= " AND db_ptiik_apps.tbl_prodi.prodi_id ='".$id."' ";			
		}
		
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_panitia_ujian($semester=NULL, $jenis=NULL, $type=NULL){
		$sql = "SELECT DISTINCT
				db_ptiik_apps.tbl_karyawan.nama,
				db_ptiik_apps.tbl_karyawan.gelar_awal,
				db_ptiik_apps.tbl_karyawan.gelar_akhir,
				db_ptiik_apps.tbl_karyawan.nik,
				db_ptiik_apps.tbl_ujian_panitia.karyawan_id,
				db_ptiik_apps.tbl_ujian_panitia.panitia_id,
				db_ptiik_apps.tbl_ujian_panitia.jenis_kegiatan_id,
				db_ptiik_apps.tbl_ujian_panitia.tahun_akademik
				FROM
				db_ptiik_apps.tbl_karyawan
				INNER JOIN db_ptiik_apps.tbl_ujian_panitia ON db_ptiik_apps.tbl_ujian_panitia.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id				
				WHERE db_ptiik_apps.tbl_ujian_panitia.tahun_akademik ='".$semester."' 
					AND LCASE(db_ptiik_apps.tbl_ujian_panitia.jenis_kegiatan_id) ='".strtolower($jenis)."'  ORDER BY db_ptiik_apps.tbl_karyawan.nama ASC
				";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_koordinator_ujian($semester=NULL, $jenis=NULL, $type=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_penjadwalan.koordinator_id,
					db_ptiik_apps.tbl_penjadwalan.tahun_akademik,
					db_ptiik_apps.tbl_penjadwalan.jenis_kegiatan_id,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir
					FROM
					db_ptiik_apps.tbl_penjadwalan
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_penjadwalan.koordinator_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					WHERE koordinator_id <> '' 
					AND db_ptiik_apps.tbl_penjadwalan.tahun_akademik ='".$semester."' 
					AND LCASE(db_ptiik_apps.tbl_penjadwalan.jenis_kegiatan_id) ='".strtolower($jenis)."' ";
		
		if($type){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		if($result){
			$str = $result;
		}else{
			$str	= "";
		}
		return $str;	
	}
	
	function get_tahun_akademik_name($str=NULL){
		$sql= "SELECT
				db_ptiik_apps.tbl_tahunakademik.tahun,
				db_ptiik_apps.tbl_tahunakademik.is_pendek,
				db_ptiik_apps.tbl_tahunakademik.is_ganjil
				FROM
				db_ptiik_apps.tbl_tahunakademik WHERE tahun_akademik='".$str."' ";
		
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	function get_lokasi( $id=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_ruang.lokasi
					FROM
					db_ptiik_apps.tbl_ruang WHERE 1 = 1 ";
		if($id!="-"){
			$sql.= " AND db_ptiik_apps.tbl_ruang.lokasi ='".$id."' ";			
		}
		
		$sql.= " ORDER BY
					db_ptiik_apps.tbl_ruang.lokasi ASC";
		$result = $this->db->query( $sql );
		
		return $result;	

	}
	
	function get_lokasi_ruang($jenis=NULL, $semester=NULL, $hari=NULL, $prodi=NULL, $lokasi=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_ruang.lokasi
					FROM
					db_ptiik_apps.tbl_jadwalujian
					INNER JOIN db_ptiik_apps.tbl_ruang ON db_ptiik_apps.tbl_jadwalujian.ruang_id = db_ptiik_apps.tbl_ruang.ruang_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					WHERE 1 = 1 
					";
		if($semester!="-"){
			$sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik ='".$semester."' ";
		}
		
		if($hari!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.hari ='".$hari."' ";
		}
		
		if($jenis!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) ='".strtolower($jenis)."' ";
		}
		
		if($prodi!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.prodi_id ='".$prodi."' ";
		}
		
		if($lokasi!="-"){
			$sql.= " AND db_ptiik_apps.tbl_ruang.lokasi ='".$lokasi."' ";
		}
		
		$sql.= " ORDER BY
					db_ptiik_apps.tbl_jadwalujian.ruang_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
	function get_jam_ujian($jenis=NULL, $semester=NULL, $hari=NULL, $tgl=NULL, $jam=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_jadwalujian.jam_mulai
					FROM
					db_ptiik_apps.tbl_jadwalujian
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					WHERE 1 = 1 ";
		if($semester!="-"){
			$sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik ='".$semester."' ";
		}
		
		if($hari!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.hari ='".$hari."' ";
		}
		
		if($jenis!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) ='".strtolower($jenis)."' ";
		}
		
		if($tgl!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.tgl) ='".$tgl."' ";
		}
		
		if($jam!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jam_mulai) ='".$jam."' ";
		}
		
		$sql.= " ORDER BY db_ptiik_apps.tbl_jadwalujian.jam_mulai ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	

	}
	
	function get_tgl_ujian($jenis=NULL, $semester=NULL, $hari=NULL, $tgl=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_jadwalujian.tgl,
					db_ptiik_apps.tbl_jadwalujian.hari
					FROM
					db_ptiik_apps.tbl_jadwalujian
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					WHERE 1 = 1
				";
		if($semester!="-"){
			$sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik ='".$semester."' ";
		}
		
		if($hari!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.hari ='".$hari."' ";
		}
		
		if($jenis!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) ='".strtolower($jenis)."' ";
		}
		
		if($tgl!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.tgl) ='".$tgl."' ";
		}
		
		
		$sql.= " ORDER BY db_ptiik_apps.tbl_jadwalujian.tgl ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_absen_pengawas($jenis=NULL, $semester=NULL, $lokasi=NULL, $hari=NULL, $tgl=NULL, $jam=NULL, $prodi=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_jadwalujian.kelas as kelas_id,
					db_ptiik_apps.tbl_mkditawarkan.tahun_akademik,
					db_ptiik_apps.tbl_namamk.keterangan AS namamk,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_pengawas.is_hadir,
					db_ptiik_apps.tbl_jadwalujian.jam_mulai,
					db_ptiik_apps.tbl_jadwalujian.jam_selesai,
					db_ptiik_apps.tbl_jadwalujian.tgl,
					db_ptiik_apps.tbl_jadwalujian.hari,
					db_ptiik_apps.tbl_prodi.keterangan AS prodi,
					db_ptiik_apps.tbl_prodi.prodi_id,
					db_ptiik_apps.tbl_jadwalujian.ruang_id as ruang,
					db_ptiik_apps.tbl_jadwalujian.is_project,
					db_ptiik_apps.tbl_ruang.lokasi
					FROM
					db_ptiik_apps.tbl_jadwalujian
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengawas.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_jadwalujian.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_ruang ON db_ptiik_apps.tbl_jadwalujian.ruang_id = db_ptiik_apps.tbl_ruang.ruang_id
					WHERE 1 = 1 

				";
		if($jenis!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) ='".strtolower($jenis)."' ";
		}
		
		if($semester!="-"){
			$sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik ='".$semester."' ";
		}
		
		if($hari!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.hari ='".$hari."' ";
		}
		
		if($lokasi!="-"){
			$sql.= " AND db_ptiik_apps.tbl_ruang.lokasi ='".$lokasi."' ";
		}
		
		if($tgl!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.tgl ='".$tgl."' ";
		}
		if($jam!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.jam_mulai ='".$jam."' ";
		}
		if($prodi!="-"){
			$sql.= " AND db_ptiik_apps.tbl_prodi.prodi_id='".$prodi."' ";
		}
		
		$sql.= " ORDER BY	namamk ASC, db_ptiik_apps.tbl_jadwalujian.ruang_id ASC, db_ptiik_apps.tbl_jadwalujian.jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	function get_data_pengawas($jenis=NULL, $semester=NULL, $hari=NULL, $fakultas=NULL){
		$sql = "SELECT DISTINCT 
					Lcase(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) as `jenis_ujian`,
					db_ptiik_apps.tbl_pengawas.karyawan_id,
					db_ptiik_apps.tbl_mkditawarkan.tahun_akademik,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir
					FROM
					db_ptiik_apps.tbl_jadwalujian
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengawas.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				WHERE 1 = 1 
					";
		if($jenis!="-"){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) ='".strtolower($jenis)."' ";
		}
		
		if($semester!="-"){
			$sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik ='".$semester."' ";
		}
		
		if($hari!="-"){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.hari ='".$hari."' ";
		}
		$sql.= " ORDER BY
					db_ptiik_apps.tbl_karyawan.nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	

	}
	
	function get_total_pengawas($jenis=NULL, $semester=NULL, $hari=NULL, $pengawas=NULL, $pengganti=NULL){
		$sql = "SELECT
				Count(db_ptiik_apps.tbl_pengawas.karyawan_id) as `total`
				FROM
				db_ptiik_apps.tbl_jadwalujian
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id WHERE 1 = 1 ";
		if($jenis){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) ='".strtolower($jenis)."' ";
		}
		
		if($semester){
			$sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik ='".$semester."' ";
		}
		
		if($hari){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.hari ='".$hari."' ";
		}
		
		if($pengawas){
			$sql.= " AND db_ptiik_apps.tbl_pengawas.karyawan_id ='".$pengawas."' ";
		}
		if($pengganti){
			$sql.= " AND db_ptiik_apps.tbl_pengawas.pengganti ='".$pengganti."' ";
		}		
		$sql.= "GROUP BY
				db_ptiik_apps.tbl_pengawas.karyawan_id,
				db_ptiik_apps.tbl_mkditawarkan.tahun_akademik,
				db_ptiik_apps.tbl_pengawas.pengganti, 
				Lcase(db_ptiik_apps.tbl_jadwalujian.jenis_ujian)";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	function get_rekap_pengawas($jenis=NULL, $semester=NULL, $hari=NULL, $pengawas=NULL){
		$sql = "SELECT
				Count(db_ptiik_apps.tbl_pengawas.is_hadir) as `total`
				FROM
				db_ptiik_apps.tbl_jadwalujian
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id WHERE tbl_pengawas.is_hadir = '1' ";
		if($jenis){
			$sql.= " AND LCASE(db_ptiik_apps.tbl_jadwalujian.jenis_ujian) ='".strtolower($jenis)."' ";
		}
		
		if($semester){
			$sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik ='".$semester."' ";
		}
		
		if($hari){
			$sql.= " AND db_ptiik_apps.tbl_jadwalujian.hari ='".$hari."' ";
		}
		
		if($pengawas){
			$sql.= " AND db_ptiik_apps.tbl_pengawas.karyawan_id ='".$pengawas."' ";
		}
		
		$sql.= "GROUP BY
				db_ptiik_apps.tbl_pengawas.is_hadir,
				db_ptiik_apps.tbl_pengawas.karyawan_id,
				db_ptiik_apps.tbl_mkditawarkan.tahun_akademik,
				Lcase(db_ptiik_apps.tbl_jadwalujian.jenis_ujian)";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}


	function get_kalender_akademik($isaktif=NULL, $kegiatan=NULL, $semester=NULL){
		$sql = "SELECT
					tbl_kalenderakademik.kalender_id,
					tbl_kalenderakademik.jenis_kegiatan_id,
					tbl_kalenderakademik.tahun_akademik,
					tbl_kalenderakademik.tgl_mulai,
					tbl_kalenderakademik.tgl_selesai,
					tbl_kalenderakademik.is_aktif
			    FROM
					db_ptiik_apps.tbl_kalenderakademik
					INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_kalenderakademik.tahun_akademik = tbl_tahunakademik.tahun_akademik 
				WHERE 1 = 1 ";
		if($isaktif){
			$sql = $sql . "  AND tbl_tahunakademik.is_aktif = '1' ";
		}
		
		if($semester){
			$sql = $sql . "  AND tbl_tahunakademik.tahun_akademik = '".$semester."' ";
		}
		
		if($kegiatan){
			$sql = $sql . "  AND tbl_kalenderakademik.jenis_kegiatan_id = '".$kegiatan."' ";
		}
		
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	
	function get_pengawas($mk=NULL, $dosen=NULL, $prodi=NULL, $kelas=NULL,$tgl=NULL){
		
		if(($mk) && ($dosen) && ($prodi) && ($kelas) && ($tgl)){
			$sql = "SELECT
						db_ptiik_apps.tbl_pengawas.pengawas_id,
						db_ptiik_apps.tbl_pengawas.karyawan_id,
						db_ptiik_apps.tbl_pengawas.ujian_id,
						db_ptiik_apps.tbl_pengawas.is_hadir,
						db_ptiik_apps.tbl_pengawas.pengganti,
						db_ptiik_apps.tbl_karyawan.nama
					FROM
						db_ptiik_apps.tbl_karyawan
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_karyawan.karyawan_id = db_ptiik_apps.tbl_pengawas.karyawan_id
					INNER JOIN db_ptiik_apps.tbl_jadwalujian ON db_ptiik_apps.tbl_pengawas.ujian_id = db_ptiik_apps.tbl_jadwalujian.ujian_id
					WHERE 1 = 1
					";
				
			$sql = $sql . " AND db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = '".$mk."'  AND 
							db_ptiik_apps.tbl_jadwalujian.pengampu_id='".$dosen."' AND 
							db_ptiik_apps.tbl_jadwalujian.kelas='".$kelas."' AND 
							db_ptiik_apps.tbl_jadwalujian.prodi_id='".$prodi."' AND 
							db_ptiik_apps.tbl_jadwalujian.tgl='".$tgl."' ORDER by db_ptiik_apps.tbl_pengawas.urut ASC
							";
							
			$result = $this->db->query($sql);
		}else{
			$result="";
		}
		
		//echo $sql;
		return $result;		
	}
	
	
	function read_ujian($mk=NULL, $dosen=NULL, $prodi=NULL, $kelas=NULL,$tgl=NULL){
		$sql = "SELECT
					tbl_jadwalujian.ujian_id,
					tbl_jadwalujian.kelas,
					tbl_jadwalujian.pengampu_id,
					tbl_jadwalujian.mkditawarkan_id,
					tbl_jadwalujian.prodi_id,
					tbl_jadwalujian.jam_mulai,
					tbl_jadwalujian.jam_selesai,
					tbl_jadwalujian.tgl,
					tbl_jadwalujian.hari,
					tbl_jadwalujian.jenis_ujian,
					tbl_jadwalujian.ruang,
					tbl_jadwalujian.is_project,
					tbl_jadwalujian.`user`,
					tbl_jadwalujian.is_aktif,
					tbl_jadwalujian.last_update
				FROM
					db_ptiik_apps.tbl_jadwalujian WHERE 1=1 ";
		if(($mk) && ($dosen) && ($prodi) && ($kelas)){
			$sql = $sql . " AND db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = '".$mk."'  AND 
							db_ptiik_apps.tbl_jadwalujian.pengampu_id='".$dosen."' AND 
							db_ptiik_apps.tbl_jadwalujian.kelas='".$kelas."' AND 
							db_ptiik_apps.tbl_jadwalujian.prodi_id='".$prodi."' ";
		}
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function read_ujian_by_tgl($tgl=NULL, $mulai=NULL, $selesai=NULL, $ruang=NULL){
		$sql = "SELECT
					tbl_jadwalujian.ujian_id,
					tbl_jadwalujian.ujian_id as jadwal_id,
					tbl_jadwalujian.kelas as kelas_id,
					tbl_jadwalujian.pengampu_id as dosen_id,
					tbl_jadwalujian.mkditawarkan_id,
					tbl_jadwalujian.prodi_id,
					tbl_jadwalujian.jam_mulai,
					tbl_jadwalujian.jam_selesai,
					tbl_jadwalujian.tgl,
					tbl_jadwalujian.hari,
					tbl_jadwalujian.jenis_ujian,
					tbl_jadwalujian.ruang_id as ruang,
					tbl_jadwalujian.is_project,
					tbl_jadwalujian.`user_id`,
					tbl_jadwalujian.is_aktif,
					tbl_jadwalujian.last_update
				FROM
					db_ptiik_apps.tbl_jadwalujian WHERE 1 = 1 ";
		if($tgl){
			$sql.= " AND tbl_jadwalujian.tgl = '".$tgl."' ";
		}
		
		if($mulai){
			$sql.= " AND tbl_jadwalujian.jam_mulai = '".$mulai."' ";
		}
		
		if($selesai){
			$sql.= " AND tbl_jadwalujian.jam_selesai = '".$selesai."' ";
		}
		
		if($ruang){
			$sql.= " AND tbl_jadwalujian.ruang_id = '".$ruang."' ";
		}
		
		$result = $this->db->getRow($sql);		
		
		return $result;
	}
	
	function get_data_ujian($mk=NULL, $type=NULL,$prodi=NULL, $kelas=NULL){
		switch($type){
			case 'dosen':
				$sql = "SELECT DISTINCT
							`db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id`,
							`db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`,
							`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`,
							`db_ptiik_apps`.`tbl_karyawan`.`nama`
							FROM
							`db_ptiik_apps`.`tbl_pengampu`
							Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
							Inner Join `db_ptiik_apps`.`tbl_jadwalmk` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` AND `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`
						WHERE `db_ptiik_apps`.`tbl_pengampu`.`is_aktif` = '1' ";
							
			
			break;
			
			case 'prodi':
				$sql = "SELECT DISTINCT
							db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
							db_ptiik_apps.tbl_jadwalmk.prodi_id as `id`,
							db_ptiik_apps.tbl_prodi.keterangan AS `value`
							FROM
							db_ptiik_apps.tbl_pengampu
							INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_pengampu.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id AND db_ptiik_apps.tbl_pengampu.pengampu_id = db_ptiik_apps.tbl_jadwalmk.pengampu_id
							INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
						WHERE 1 = 1 
					";

			break;
			
			case 'kelas':
				$sql = "SELECT DISTINCT
							db_ptiik_apps.tbl_jadwalmk.kelas,
							db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id
							FROM `db_ptiik_apps`.`tbl_pengampu` Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id` Inner Join `db_ptiik_apps`.`tbl_jadwalmk` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` AND `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`
						WHERE 1 = 1 
					";
			break;
			
		}
		
		if($kelas){
			$sql = $sql . " AND tbl_jadwalmk.kelas = '".$kelas."' ";
		}	
		
		if($mk){
			$sql = $sql . " AND tbl_jadwalmk.mkditawarkan_id='".$mk."'  ";
		}	
		
		if($prodi){
			$sql = $sql . " AND db_ptiik_apps.tbl_jadwalmk.prodi_id='".$prodi."'  ";
		}	
	
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function draw_grid_jadwal($semesterid=NULL, $mkid=NULL, $dosenid=NULL, $ruangid=NULL, $style, $jeniskegiatan=NULL, $prodi=NULL,$kelas=NULL ){
	
	 $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
			 
		 $str = '<table  class="table table-bordered table-jadwal"><thead>';
        
         /* table headings */
		 if($jeniskegiatan=='kuliah'){
			$headings = array('Hari/MK','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
		 }else{
			$headings = array('Hari/Dosen','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
		 }
         $str.= '<tr class="'. $style .'-row"><th class="calendar-day-head">'
             .implode('</th><th class="'. $style .'-day-head">',$headings).'</th></tr></thead>';
	
		if($jeniskegiatan){
			
			switch($jeniskegiatan){
				case 'kuliah':
					$str.= $this->grid_row_mk($semesterid, $mkid, $dosenid, $ruangid, $style, 0, 'kuliah');	
				break;
				
				case 'praktikum':
					$str.= $this->grid_row_mk($semesterid,$mkid, $dosenid, $ruangid, $style, 1, 'praktikum');	
				break;
				
				case 'uts':
					$str.= $this->grid_row_mk("",$mkid, $dosenid, $ruangid, $style, 0, 'uts', $prodi, $kelas);	
				break;
				
				case 'uas':
					$str.= $this->grid_row_mk("",$mkid, $dosenid, $ruangid, $style, 0, 'uas', $prodi, $kelas);	
				break;
				
				default:			
					$str.= $this->grid_row_kegiatan($semesterid,$jeniskegiatan, $dosenid, $ruangid, $style);	
				break;
			}
		}
		
         $str.= '</table>';
		 
		 return $str;
	}
	
	
	
	function grid_row_kegiatan($semester=NULL, $jeniskegiatan=NULL, $dosenid=NULL, $ruangid=NULL, $style){
	
	$col="";
	
	$sql = "SELECT
			db_ptiik_apps.tbl_penjadwalan.jadwal_id,
			db_ptiik_apps.tbl_penjadwalan.karyawan_id,
			db_ptiik_apps.tbl_penjadwalan.jenis_kegiatan_id,
			db_ptiik_apps.tbl_penjadwalan.tahun_akademik,
			db_ptiik_apps.tbl_penjadwalan.repeat_on,
			db_ptiik_apps.tbl_karyawan.nama
			FROM
			db_ptiik_apps.tbl_penjadwalan
			INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_penjadwalan.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
			WHERE
			1 = 1
				";
	if($jeniskegiatan){
		$sql = $sql . " AND `tbl_penjadwalan`.`jenis_kegiatan_id` = '".$jeniskegiatan."' ";
	}
	
	if($dosenid){
		$sql = $sql . " AND `tbl_penjadwalan`.`karyawan_id` = '".$dosenid."' ";
	}
	
	if($semester){
		$sql = $sql . " AND `tbl_penjadwalan`.`tahun_akademik` = '".$semester."' ";
	}
	
	$sql = $sql. " ORDER BY db_ptiik_apps.tbl_karyawan.nama ASC";
	$result = $this->db->query($sql);
	if($result){
		foreach ($result as $row):
			$namadosen 		= $this->get_nama_dosen($row->karyawan_id);
			$namakegiatan 	= $this->get_nama_kegiatan($jeniskegiatan);
			
			$col.= '<tr>';
				$col.= '<td class="ruang" style="text-align:left">'.$namadosen.'</td>';
				for($i=1;$i<=6;$i++){
					$data = $this->detail_kegiatan($row->jadwal_id, $row->karyawan_id, $ruangid,  $i);
										
					$col.='<td class="">';
					if($data){
						foreach($data as $dt):
							$col.= '<ul class="nav nav-pills" style="margin:0;"><li class="dropdown">
									<small>
										<a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" data-placement="right"
											title="R.'.$dt->ruang.' ('.$dt->jam_mulai.'-'.$dt->jam_selesai.')&#013;'.$namakegiatan.'&#013;'.$namadosen. '"><i class="fa fa-clock-o"></i> '.$ruangid." ". $dt->jam_mulai.'</a> 
											<ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
												<li id="post-'.$dt->detail_id.'" data-id="'.$dt->detail_id.'">
													<a data-id="'.$dt->detail_id.'" id='.$dt->detail_id.' class="btn-delete-post-kegiatan" ><i class="icon-remove"></i> Delete</a>
												</li>								
											  </ul>
									</small></li></ul>';
						endforeach;
					}
					$col.='</td>';
				}
		endforeach;
	}
	$col.= '</tr>';
			
		return $col;		
	
	}
	
	function grid_row_mk($semester=NULL,$mkid=NULL, $dosenid=NULL, $ruangid=NULL, $style, $isprak=NULL, $type=NULL, $prodi=NULL, $kelas=NULL){
		
		if($isprak==1){
			$sql = "SELECT DISTINCT 
						`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,
						`db_ptiik_apps`.`tbl_namamk`.`keterangan` AS `namamk`,
						`db_ptiik_apps`.`tbl_matakuliah`.`sks`,						
						`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					FROM
						`db_ptiik_apps`.`tbl_mkditawarkan`
						Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
						Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`
						left Join `db_ptiik_apps`.`tbl_pengampu` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					WHERE 1 = 1 
					";
		}else{
			$sql = "SELECT
						`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,
						`db_ptiik_apps`.`tbl_namamk`.`keterangan` AS `namamk`,
						`db_ptiik_apps`.`tbl_matakuliah`.`sks`,
						`db_ptiik_apps`.`tbl_pengampu`.`is_koordinator`,
						`db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`,
						`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`,
						`db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`,
						`db_ptiik_apps`.`tbl_karyawan`.`nama`,
						`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
						`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`
					FROM
						`db_ptiik_apps`.`tbl_mkditawarkan`
						Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
						Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`
						left Join `db_ptiik_apps`.`tbl_pengampu` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
						Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
					WHERE 1 = 1 
					";
		}
		if($dosenid){
			$sql = $sql . " AND (mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`),5,5)='".$dosenid."' OR `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`='".$dosenid."' 
							OR `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`='".$dosenid."') ";
		}	
		
		if($mkid){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = '".$mkid."' ";
		}
		
		if($isprak){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`is_praktikum` = '".$isprak."' ";
		}
		
		if($semester){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		
		$result = $this->db->query($sql);
	
		$col="";
		if($result){
			foreach ($result as $row):
				$col.= '<tr class="'. $style .'-row">';
					if($isprak!=1){
						$col.= '<td class="'. $style .'-weekend-day"><small>'.$row->namamk.'</small><br><code><small>'.$row->nama.'</small></code></td>';
						$namadosen 	= $row->nama;
					}else{
						$col.= '<td class="'. $style .'-weekend-day"><small>'.$row->namamk.'</small><br><code><small>Praktikum (Asisten) </small></code></td>';
						$namadosen = 'Praktikum (Asisten) ';
					}
					for($i=1;$i<=6;$i++){
						switch ($type){
							case 'kuliah':
								$data = $this->detail_jadwal($row->mkditawarkan_id, $row->dosen_id, $ruangid,  $i, $isprak);
								$sclass= "btn-delete-post";
							break;
							case 'praktikum':
								$data = $this->detail_jadwal($row->mkditawarkan_id, '-', $ruangid,  $i, $isprak);
								$sclass= "btn-delete-post";
							break;
							case 'uts':
								$data = $this->detail_ujian($row->mkditawarkan_id, $row->dosen_id, $i, $prodi, $kelas, $type);
								$sclass= "btn-delete-ujian";
							break;
							case 'uas':
								$data = $this->detail_ujian($row->mkditawarkan_id, $row->dosen_id, $i, $prodi, $kelas, $type);
								$sclass= "btn-delete-ujian";
							break;
							
						}
												
						$col.='<td class="'. $style .'-day" >';
						if($data){
							foreach($data as $dt):
								$col.= '<ul class="nav nav-pills" style="margin:0;"><li class="dropdown">
										<small><a href="#" class="tooltip-toggle" id="drop4" role="button" data-toggle="tooltip" data-placement="right" data-html="true" 
											title="R.'.$dt->ruang.' ('.$dt->jam_mulai.'-'.$dt->jam_selesai.')&#013;'.$row->namamk. ' ('.$dt->prodi_id.' - '.$dt->kelas_id.')&#013;'.$namadosen.'"><i class="icon-time"></i></a>
											<a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" data-placement="right" 
												title="R.'.$dt->ruang.' ('.$dt->jam_mulai.'-'.$dt->jam_selesai.')&#013;'.$row->namamk. ' ('.$dt->prodi_id.' - '.$dt->kelas_id.')&#013;
												'.$namadosen.'"><small>'.$dt->jam_mulai.'-'.$dt->ruang.'</small></a> 
												<ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
													<li id="post-'.$dt->jadwal_id.'" data-id="'.$dt->jadwal_id.'">
														<a data-id="'.$dt->jadwal_id.'" id='.$dt->jadwal_id.' class="'.$sclass.'" ><i class="icon-remove"></i> Delete</a>
													</li>								
												  </ul>
										</small></li></ul>';
							endforeach;
						}
						$col.='</td>';
					}
				$col.= '</tr>';
			endforeach;
		}
		
		return $col;

	}
	
	function detail_kegiatan($jadwalid, $dosenid, $ruangid, $runday){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
		}
		
		$sql = "SELECT
				`tbl_penjadwalan_detail`.`detail_id`,
				`tbl_penjadwalan_detail`.`jadwal_id`,
				`tbl_penjadwalan_detail`.`ruang_id` as ruang,
				`tbl_penjadwalan_detail`.`hari`,
				TIME_FORMAT(`tbl_penjadwalan_detail`.`jam_mulai`, '%H:%i') as `jam_mulai`,
				TIME_FORMAT(`tbl_penjadwalan_detail`.`jam_selesai`, '%H:%i') as `jam_selesai`
				FROM
				`db_ptiik_apps`.`tbl_penjadwalan_detail` WHERE `hari`='".$hari."' ";
		if($jadwalid){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_penjadwalan_detail`.`jadwal_id` = '".$jadwalid."' ";
		}
		
		
		if($ruangid){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_penjadwalan_detail`.`ruang_id` = '".$ruangid."' ";
		}
		
		$result = $this->db->query($sql);
		
		return $result;

	}
	
	function detail_ujian($mkid=NULL, $dosenid=NULL, $runday=NULL, $prodi=NULL, $kelas=NULL, $jenis=NULL){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
		}
		
		$sql = "SELECT
				`db_ptiik_apps`.`tbl_jadwalujian`.`ujian_id` as `jadwal_id`,
				`db_ptiik_apps`.`tbl_jadwalujian`.`kelas`,
				`db_ptiik_apps`.`tbl_jadwalujian`.`pengampu_id`,
				`db_ptiik_apps`.`tbl_jadwalujian`.`mkditawarkan_id`,
				`db_ptiik_apps`.`tbl_jadwalujian`.`prodi_id`,
				`db_ptiik_apps`.`tbl_jadwalujian`.`tgl`,
				TIME_FORMAT(`db_ptiik_apps`.`tbl_jadwalujian`.`jam_mulai`, '%H:%i') as `jam_mulai`,
				TIME_FORMAT(`db_ptiik_apps`.`tbl_jadwalujian`.`jam_selesai`, '%H:%i') as `jam_selesai`, 
				`db_ptiik_apps`.`tbl_jadwalujian`.`hari`,
				`db_ptiik_apps`.`tbl_jadwalujian`.`ruang_id`,
				`db_ptiik_apps`.`tbl_jadwalujian`.`is_project`
				FROM
				`db_ptiik_apps`.`tbl_jadwalujian` WHERE `hari`='".$hari."' AND is_aktif='1' ";
		if($mkid){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalujian`.`mkditawarkan_id` = '".$mkid."' ";
		}
		
		if($dosenid){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalujian`.`pengampu_id` = '".$dosenid."' ";
		}
		
		if($prodi){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalujian`.`prodi_id` = '".$prodi."' ";
		}
		
		if($kelas){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalujian`.`kelas` = '".$kelas."' ";
		}
		
		if($jenis){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalujian`.`jenis_ujian` = '".$jenis."' ";
		}
		
		
		$result = $this->db->query($sql);
		//secho $sql;
		return $result;

	}
	
	
	function detail_jadwal($mkid=NULL, $dosenid=NULL, $ruangid=NULL, $runday=NULL, $isprak=NULL){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
		}
		
		$sql = "SELECT
				`db_ptiik_apps`.`tbl_jadwalmk`.`jadwal_id`,
				`db_ptiik_apps`.tbl_jadwalmk.kelas,
				`db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id` as dosen_id,
				`db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id`,
				`db_ptiik_apps`.`tbl_jadwalmk`.`prodi_id`,
				TIME_FORMAT(`db_ptiik_apps`.`tbl_jadwalmk`.`jam_mulai`, '%H:%i') as `jam_mulai`,
				TIME_FORMAT(`db_ptiik_apps`.`tbl_jadwalmk`.`jam_selesai`, '%H:%i') as `jam_selesai`, 
				`db_ptiik_apps`.`tbl_jadwalmk`.`hari`,
				`db_ptiik_apps`.tbl_jadwalmk.ruang_id_id,
				`db_ptiik_apps`.`tbl_jadwalmk`.`is_praktikum`
				FROM
				`db_ptiik_apps`.`tbl_jadwalmk` WHERE `hari`='".$hari."' AND is_aktif='1' ";
		if($mkid){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` = '".$mkid."' ";
		}
		
		if($dosenid){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id` = '".$dosenid."' ";
		}
		
		if($ruangid){
			$sql = $sql . " AND `db_ptiik_apps`.tbl_jadwalmk.ruang_id_id = '".$ruangid."' ";
		}
		
		if($isprak){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_jadwalmk`.`is_praktikum` = '".$isprak."' ";
		}
		
		$result = $this->db->query($sql);
		
		return $result;

	}
	
	
	
				
	function read($keyword, $page = 1, $perpage = 10, $id) {
	
		$keyword 	= $this->db->escape($keyword);
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
				mid(md5(`tbl_penjadwalan`.`jadwal_id`),5,5) as `id`, 
				`tbl_penjadwalan`.`jadwal_id`,
				`tbl_penjadwalan`.`karyawan_id`,
				`tbl_penjadwalan`.`jenis_kegiatan_id`,
				`tbl_penjadwalan`.`tahun_akademik`,
				`tbl_penjadwalan`.`repeat_on`,
				`tbl_penjadwalan`.`user`,
				`tbl_penjadwalan`.`last_update`,
				`db_ptiik_apps`.`tbl_karyawan`.`nama`,
				`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
				`db_ptiik_apps`.`tbl_jeniskegiatan`.`keterangan` AS `jeniskegiatan`,
				`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
				`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
				`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
				FROM
				`db_ptiik_apps`.`tbl_penjadwalan`
				Left Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_penjadwalan`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				Inner Join `db_ptiik_apps`.`tbl_jeniskegiatan` ON `db_ptiik_apps`.`tbl_penjadwalan`.`jenis_kegiatan_id` = `db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`
				Inner Join `db_ptiik_apps`.`tbl_tahunakademik` ON `db_ptiik_apps`.`tbl_penjadwalan`.`tahun_akademik` = `db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`
				WHERE 1 = 1 
				";
		if($id){
			$sql=$sql . " AND mid(md5(`db_ptiik_apps`.`tbl_penjadwalan`.`jadwal_id`),5,5)='".$id."' ";
		}
		
		$sql= $sql . "ORDER BY `tbl_penjadwalan`.`last_update` DESC LIMIT $offset, $perpage";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_detail_jadwal($id=NULL) {
		$sql = "SELECT
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`detail_id`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`jadwal_id`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`hari`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`jam_mulai`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`jam_selesai`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`is_aktif`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`user`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`last_update`,
				(select count(a.`hari`) FROM `db_ptiik_apps`.`tbl_penjadwalan_detail` as `a` where `a`.jadwal_id =  `db_ptiik_apps`.`tbl_penjadwalan_detail`.`jadwal_id` 
				AND a.`hari`=`db_ptiik_apps`.`tbl_penjadwalan_detail`.`hari` 
				GROUP BY
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`hari`,
				`db_ptiik_apps`.`tbl_penjadwalan_detail`.`jadwal_id`) as `jml`
				FROM
				`db_ptiik_apps`.`tbl_penjadwalan_detail`
				WHERE 1 = 1 
				";	
		if($id!=""){
			$sql=$sql . " AND mid(md5(`db_ptiik_apps`.`tbl_penjadwalan_detail`.`jadwal_id`),5,5)='".$id."' ";
		}
		
		$result = $this->db->query( $sql );
		
		return $result;	
								
	}
	

	
	function get_nama_dosen($id=NULL){
		$sql = "SELECT nama FROM db_ptiik_apps.`tbl_karyawan` WHERE karyawan_id = '".$id."' ";
		$result = $this->db->query( $sql );		
		
		if($result){
			foreach($result as $dt):
				$str = $dt->nama;
			endforeach;
		}else{
			$str = "";
		}
		
		return $str;		
		
	}
	
	function get_nama_kegiatan($id=NULL){
		$sql = "SELECT keterangan as `value` FROM db_ptiik_apps.`tbl_jeniskegiatan` WHERE jenis_kegiatan_id = '".$id."' ";
		$result = $this->db->getRow( $sql );		
		
		if($result){
			$str = ucWords($result->value);
		}else{
			$str = "";
		}
		
		return $str;		
		
	}
	
			
	function replace_penjadwalan($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_penjadwalan',$datanya);		
	}	
	
	function replace_detail_penjadwalan($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_penjadwalan_detail',$datanya);
	}
	
	function delete_penjadwalan($datanya,$idnya){
		return $this->db->update('db_ptiik_apps`.`tbl_penjadwalan',$datanya,$idnya);
	}
	
	function replace_jadwalujian($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_jadwalujian',$datanya);
	}
	
	function replace_pengawas($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_pengawas',$datanya);
	}
	
	function delete_pengawas($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_pengawas',$datanya);
	}
	
	function delete_pengawas_ruang($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_detailjadwalruang',$datanya);
	}
	
	function replace_jadwalmk($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
	}
	
	function update_jadwalmk($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_jadwalmk',$datanya,$idnya);
	}
	
	function disable_jadwalmk($datanya, $idnya){
		return $this->db->update('db_ptiik_apps`.`tbl_jadwalmk',$datanya, $idnya);
	}
	
	function delete_jadwalmk($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
	}
	function delete_jadwalujian($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_jadwalujian',$datanya);
	}
	
	function delete_jadwal_kegiatan($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_penjadwalan_detail',$datanya);
	}
	
	function update_jadwal_exist($dosenid, $mkid, $jammulai, $kelas, $ruang, $hari, $isprak, $jadwalid){
		$sql = "SELECT jadwal_id FROM db_ptiik_apps.tbl_jadwalmk WHERE mkditawarkan_id='".$mkid."'  AND ruang='".$ruang."' AND dosen_id ='".$dosenid."' 
					AND hari='".$hari."' 
					AND (time_format(jam_selesai,'%H:%i')>time_format('".$jammulai."','%H:%i') AND time_format(jam_mulai,'%H:%i')<>time_format('".$jammulai."','%H:%i') ) ";
		$result = $this->db->query( $sql );		
		
		if($result){			
			foreach($result as $dt):
				$this->update_jadwal($dt->jadwal_id);
				$this->update_djadwal($dt->jadwal_id);
			endforeach;
		}
		
		return $result;
	}
	
	
	function update_jadwal($id=NULL){
		if($id){
			$where['jadwal_id'] = $id;
			$data['is_aktif'] = 0;
			
			return $this->db->update('db_ptiik_apps`.`tbl_jadwalmk', $data, $where);
		}
	}
	
	function update_dpenjadwalan($jadwalid,$jammulai, $hari, $ruang){
		$sql = "SELECT detail_id FROM db_ptiik_apps.tbl_penjadwalan_detail WHERE jadwal_id='".$jadwalid."' AND ruang='".$ruang."' AND hari='".$hari."' 
					AND (time_format(jam_selesai,'%H:%i')>time_format('".$jammulai."','%H:%i') AND time_format(jam_mulai,'%H:%i')<>time_format('".$jammulai."','%H:%i') ) ";
		$result = $this->db->query( $sql );		
		//echo $sql;
		if($result){
			foreach($result as $dt):
				$this->update_djadwal($dt->detail_id);
			endforeach;
		}
		
		return $result;
	}
	
	function update_djadwal($id=NULL){
		if($id){
			$where['detail_id'] = $id;
			$data['is_aktif'] = 0;
			
			return $this->db->update('db_ptiik_apps`.`tbl_penjadwalan_detail', $data, $where);
		}
	}
	
	function get_reg_number($jeniskegiatan=NULL, $staff=NULL, $semester=NULL, $tgl=NULL){
		$sql = "SELECT jadwal_id FROM db_ptiik_apps.tbl_penjadwalan WHERE jenis_kegiatan_id ='".$jeniskegiatan."' AND tahun_akademik = '".$semester."' AND karyawan_id='".$staff."'  ";
		$result = $this->db->getRow( $sql );	
		
		if($result){
			$strresult = $result->jadwal_id;
		}else{
			$kode = strtoUpper(substr($jeniskegiatan,0,2)).$semester;
			
			$sql="SELECT concat('".$kode."',RIGHT(concat( '00000' , CAST(IFNULL(MAX(CAST(right(jadwal_id,5) AS unsigned)), 0) + 1 AS unsigned)),5)) as `data` 
				FROM db_ptiik_apps.tbl_penjadwalan WHERE (jenis_kegiatan_id ='".$jeniskegiatan."' AND tahun_akademik = '".$semester."')";		
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		}
		
		
		return $strresult;
	}
	
	function get_reg_detail($jadwal=NULL, $jeniskegiatan=NULL, $staff=NULL, $semester=NULL, $hari=NULL, $jam=NULL, $ruang=NULL, $nhari=NULL){
		$sql = "SELECT detail_id FROM db_ptiik_apps.tbl_penjadwalan_detail WHERE jadwal_id ='".$jadwal."' AND hari='".$hari."' AND  
				time_format(jam_mulai,'%H:%i')=time_format('".$jam."','%H:%i') AND ruang_id='".$ruang."'";
		$result = $this->db->getRow( $sql );

		if($result){
			$strresult = $result->detail_id;
		}else{
			$kode = strtoUpper(substr($jeniskegiatan,0,2)).$semester.$nhari;
			$kjadwal = substr($jadwal, 0, 10);
			
			$sql="SELECT concat('".$kode."',RIGHT(concat( '00000' , CAST(IFNULL(MAX(CAST(right(detail_id,5) AS unsigned)), 0) + 1 AS unsigned)),5)) as `data` 
					FROM db_ptiik_apps.tbl_penjadwalan_detail WHERE (left(jadwal_id, 10)='".$kjadwal."') AND hari='".$hari."' ";		
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		}
				
		return $strresult;
	}
	
	function get_reg_ujian($jadwal=NULL, $jeniskegiatan=NULL, $mk=NULL, $dosen=NULL, $prodi=NULL, $kelas=NULL, $semester=NULL, $hari=NULL, $jam=NULL, $ruang=NULL, $nhari=NULL){
		//$sql = "SELECT detail_id FROM db_ptiik_apps.tbl_penjadwalan_detail WHERE jadwal_id ='".$jadwal."' ";
		$sql = "SELECT ujian_id FROM db_ptiik_apps.tbl_jadwalujian WHERE mkditawarkan_id ='".$mk."' AND prodi_id = '".$prodi."' AND dosen_id = '".$dosen."' AND kelas_id = '".$kelas."' AND jenis_ujian='".$jeniskegiatan."' ";
		$result = $this->db->getRow( $sql );	
		
		if($result){
			$strresult = $result->ujian_id;
		}else{
			$kode = strtoUpper(substr($jeniskegiatan,0,2)).$semester.$nhari;
			$kjadwal = substr($jadwal, 0, 10);
			
			$sql="SELECT concat('".$kode."',RIGHT(concat( '00000' , CAST(IFNULL(MAX(CAST(right(detail_id,5) AS unsigned)), 0) + 1 AS unsigned)),5)) as `data` 
					FROM db_ptiik_apps.tbl_penjadwalan_detail WHERE left(jadwal_id, 10)='".$kjadwal."'";		
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		}
		
		
		return $strresult;
	}
	
	function get_nama_mk($id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_namamk.keterangan
					FROM
					db_ptiik_apps.tbl_mkditawarkan
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					WHERE  db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = '".$id."' 
					";
		$data = $this->db->getRow( $sql );
		
		$result = $data->keterangan;
		
		return $result;
	}
	
	function delete_peserta_kegiatan($id) {
		return $this->db->delete("db_ptiik_apps`.`tbl_aktifitas", $id);
		
	}
	
	
	function replace_agenda_staff($datanya){
		$result= $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$datanya);
		return $result;
	}
	
	function update_koordinator_ujian($datanya, $idnya){
		return $this->db->update('db_ptiik_apps`.`tbl_penjadwalan',$datanya, $idnya);
	}
	
	function delete_panitia($id){
		return $this->db->delete("db_ptiik_apps`.`tbl_ujian_panitia", $id);
	}
	
	function replace_panitia($datanya){
		$result= $this->db->replace('db_ptiik_apps`.`tbl_ujian_panitia',$datanya);
		return $result;
	}
	
	function  get_nama_bulan($bln=NULL){
		switch($bln){
			   case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;    
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }
		}
		
		return $bln;
	}
}