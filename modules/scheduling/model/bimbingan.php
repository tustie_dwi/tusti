<?php
class model_bimbingan extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
	
	function get_dosen_bimbingan($id=NULL, $ruang=NULL, $by=NULL){
		$sql = "SELECT DISTINCT
				mid(md5(db_ptiik_apps.vw_jadwalbimbingan.karyawan_id),5,5) as `id`,
				db_ptiik_apps.vw_jadwalbimbingan.karyawan_id,
				db_ptiik_apps.vw_jadwalbimbingan.ruang,
				db_ptiik_apps.vw_jadwalbimbingan.nama,
				db_ptiik_apps.vw_jadwalbimbingan.nik,
				db_ptiik_apps.vw_jadwalbimbingan.gelar_awal,
				db_ptiik_apps.vw_jadwalbimbingan.gelar_akhir,
				db_ptiik_apps.tbl_file.file_name as `foto`,
				db_ptiik_apps.tbl_file.is_jenis,
				db_ptiik_apps.tbl_file.url,
				db_ptiik_apps.tbl_jabatan.keterangan AS jabatan
				FROM
				db_ptiik_apps.vw_jadwalbimbingan
				LEFT JOIN db_ptiik_apps.tbl_file ON db_ptiik_apps.vw_jadwalbimbingan.karyawan_id = db_ptiik_apps.tbl_file.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_jabatan ON db_ptiik_apps.vw_jadwalbimbingan.jabatan_id = db_ptiik_apps.tbl_jabatan.jabatan_id
				WHERE
				db_ptiik_apps.tbl_file.is_jenis = 'foto'  
				";
				
		switch($by){			
			case 'dosen';
				$sql = $sql . "AND (db_ptiik_apps.`vw_jadwalbimbingan`.`karyawan_id` = '".$id."' OR mid(md5(db_ptiik_apps.`vw_jadwalbimbingan`.`karyawan_id`),5,5)='".$id."')";
			break;
			case 'ruang';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`ruang` = '".$id."' ";
			break;
			case 'hari';
				$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`hari` = '".$id."' ";
			break;
		
		}
		
		if($ruang){
			$sql = $sql . "AND db_ptiik_apps.`vw_jadwalbimbingan`.`ruang` = '".$ruang."'";
		}
		
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`vw_jadwalbimbingan`.`nama` ASC, db_ptiik_apps.`vw_jadwalbimbingan`.`hari` ASC";
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
				
	}
	
	function get_jadwal_by_bimbingan($id=NULL){
		$sql = "SELECT
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`jenis_kegiatan_id`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`tahun_akademik`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`ruang`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`hari`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`jam_mulai`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`jam_selesai`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`tahun`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`is_ganjil`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`is_pendek`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`karyawan_id`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`nama`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`gelar_awal`,
				`db_ptiik_apps`.`vw_jadwalbimbingan`.`gelar_akhir`
				FROM
				`db_ptiik_apps`.`vw_jadwalbimbingan`
				";
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`vw_jadwalbimbingan`.`nama` ASC,`db_ptiik_apps`.`vw_jadwalbimbingan`.`jam_mulai` ASC, `db_ptiik_apps`.`vw_jadwalbimbingan`.`ruang` ASC";
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	
	function draw_jadwal_by_type( $by=NULL, $type=NULL, $id=NULL, $url=NULL){
			$jadwal	=$this->get_jadwal_by_bimbingan($id);
			
			$ruang	= $this->get_ruang_by_bimbingan();
			
			switch($type){
				case 'hari';
					$strheader = "Hari : ".ucFirst($id);
					$hari 	= $this->get_hari($id,"'sabtu','minggu'");
					
				break;

				case 'dosen':
					$dosen = $this->get_nama_dosen($id);
					
					if(($dosen->gelar_awal)&&($dosen->gelar_awal!="-")){
						$gawal= ", ".$dosen->gelar_awal;
					}else{
						$gawal="";
					}
					
					if(($dosen->gelar_akhir)&&($dosen->gelar_akhir!="-")){
						$gakhir= ", ".$dosen->gelar_akhir;
					}else{
						$gakhir="";
					}
						
					
					$strheader = "Dosen Pembimbing : ".$dosen->name.$dosen->gelar_awal.$gawal.$gakhir;
					$hari 	= $this->get_hari('',"'sabtu','minggu'");
					
					
				break;

				case 'ruang':
					$strheader = "Ruang Bimbingan : ".ucFirst($id);
					$hari 	= $this->get_hari('',"'sabtu','minggu'");
				break;
				
				default:
					$strheader = "Jadwal Bimbingan & Konsultasi";
					$hari 	= $this->get_hari('',"'sabtu','minggu'");
				break;

			}
			
			//echo $hari;
			?>
			
			<div class="table-responsive">
				<table class="table table-bordered table-jadwal">
				<thead>
					<tr class="calendar-row">
						<th class="calendar-day-head" colspan=2>R/H</th>
						<?php
							foreach($hari as $dt):
								echo "<th class='calendar-day-head'>".ucWords($dt->value)."</th>";
							endforeach;
						?>
					</tr>
				</thead>
			<tbody>
			<?php
			foreach($ruang as $dtruang):
				$jmlrows = $this->get_dosen_bimbingan($id, strtolower($dtruang->ruang), $type);
				$dosen	 = $this->get_dosen_bimbingan($id, strtolower($dtruang->ruang), $type);
				
				$x=0;
				
				if($jmlrows){
					foreach($jmlrows as $dt):
						$x++;
						
						$rows = $x;
					endforeach;		
				}else{
					$rows = 0;
				}
				
				if($dosen){
					$j=1;
					foreach($dosen as $ddosen):	
						if($type=='dosen'){
							$surl 	= $url."info/details/".$ddosen->id."/month";
						}else{
							$surl 	= $url."info/jadwal/bimbingan/dosen/".$ddosen->id;
						}
						
						if(($ddosen->gelar_awal!="")&&($ddosen->gelar_awal!="-")){
							$gelarawal= ", ".$ddosen->gelar_awal;
						}else{
							$gelarawal="";
						}
						
						if(($ddosen->gelar_akhir!="")&&($ddosen->gelar_akhir!="-")){
							$gelarakhir= ", ".$ddosen->gelar_akhir;
						}else{
							$gelarakhir="";
						}
						
						
						if(! $ddosen->foto){
							$foto= "http://ptiik.ub.ac.id/apps/assets/uploads/foto/no_foto.png";
						} else {
							$foto = $ddosen->url;
						}	
						
						if(strtolower($dtruang->ruang)==strtolower($ddosen->ruang)){
							if($j==1){
								echo "<tr><td rowspan='".$rows."' class='ruang'><b>".ucFirst($dtruang->ruang)."</b></td>";
								echo "<td class='no-shadow'>";
								echo "<div align='left' class='text text-default'><div class='media'>
									  <a class='pull-left' href=".$surl." style='	margin-right: 5px;margin-bottom: 10px;'><img src=".$foto." class='img-thumbnail' width='50' height='50'></a></div>
								<div class='media-body'>
									<b class='media-heading'>".ucWords($ddosen->nama).$gelarawal.$gelarakhir."</b>";								
								echo "<br>$ddosen->nik &nbsp;<span class='text text-info' style='font-weight:normal'>$ddosen->jabatan</span>";								
								echo "<br><code>R. $ddosen->ruang </code>
									</div></div>";
								echo "</td>";
							}else{
								echo "<tr>";
								echo "<td class='no-shadow'>";
								echo "<div align='left' class='text text-default'><div class='media'>
									  <a class='pull-left' href=".$surl." style='
										margin-right: 5px;margin-bottom: 10px;
									'><img src=".$foto." class='img-thumbnail' width='50' height='50'></a></div>
								<div class='media-body'>
									<b class='media-heading'>".ucWords($ddosen->nama).$gelarawal.$gelarakhir."</b>";								
								echo "<br>$ddosen->nik &nbsp;<span class='text text-info' style='font-weight:normal'>$ddosen->jabatan</span>";								
								echo "<br><code>R. $ddosen->ruang </code>
									</div></div>";
							}
							
							$i=0;
							$skip = false;
							foreach($hari as $dt):
								$detail = $this->get_detail_bimbingan($ddosen->karyawan_id, $ddosen->ruang, $dt->id);
								
								if($detail){
									if($detail == 1){
									echo "<td class='tif'>";
										foreach($detail as $detail):
											echo $detail->mulai." - ".$detail->selesai;
										endforeach;
									echo "</td>";
									}else{
										echo "<td align='left'><ul class='list-inline'>";
											$x=0;
											foreach($detail as $row):
												$x++;											
												echo "<li class='label label-info' style='font-weight:normal;font-size:11px;text-shadow:none'><i class='fa fa-clock-o'></i> ".$row->mulai." - ".$row->selesai."</li> ";
											endforeach;
										echo "</ul></td>";
									}
								}else{
									echo "<td>&nbsp;</td>";
								}
							endforeach;
							echo "</tr>";
						}
						$j++;
					endforeach;
				}
			endforeach;		
		?>
		</tbody>
		</table>
		</div>
					</div>
				</div>
			</div>
		</section>
	</section>
	<?php			
	}
	
	function get_detail_bimbingan($dosen=NULL, $ruang=NULL, $hari=NULL){
		$sql = "SELECT time_format(jam_mulai, '%H:%i') as `mulai`, time_format(jam_selesai, '%H:%i') as `selesai` FROM `db_ptiik_apps`.`vw_jadwalbimbingan` WHERE 
					karyawan_id='".$dosen."'  AND ruang='".$ruang."' AND hari='".strtolower($hari)."' ORDER BY jam_mulai ASC  ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master hari*/
	function  get_hari($id=NULL, $str=NULL){
		$sql = "SELECT hari as `id`, hari as `value` FROM db_ptiik_apps.tbl_hari WHERE 1=1 ";
		if($id){
			$sql = $sql . " AND lcase(hari)= '".$id."' ";
		}
		
		if($str){
			$sql = $sql . " AND lcase(hari) NOT IN ($str) ";
		}
		$sql = $sql . "ORDER BY db_ptiik_apps.tbl_hari.id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_nama_dosen($id=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `name`, nik as `nik`, gelar_awal, gelar_akhir FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE  (karyawan_id='".$id."' OR mid(md5(karyawan_id),5,5)='".$id."' ) ";
	
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}	
	
}