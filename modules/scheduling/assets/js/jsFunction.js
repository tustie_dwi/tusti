$(function() {
		
	$('#writeTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
  });
  
  
   $('#myModal').bind('hide', function(){
	var hari = $('.hari').attr('value');	
   });
   
   $("[data-toggle=tooltip]").tooltip();
   /*$('.dropdown-toggle').tooltip({
		selector: "a[rel=tooltip]"
	});*/
	
	$(".pop").each(function() {
		var $pElem= $(this);
		$pElem.popover(
			{
			  title: getPopTitle($pElem.attr("id")),
			  content: getPopContent($pElem.attr("id")),
			  trigger:'hover'
			}
		);
	});
					
	function getPopTitle(target) {
		return $("#" + target + "_content > div.popTitle").html();
	};
			
	function getPopContent(target) {
		return $("#" + target + "_content > div.popContent").html();
	};
	
	$(".btn-delete-post").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/deletemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	
	$(".btn-delete-post-kegiatan").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
		
	$( "#date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$( ".date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	
	$(".dosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
		} 
	});  

	$(".ndosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ndosenid').val(ui.item.id); 
			$('#ndosen').val(ui.item.value);
		} 
	});  		

	$(".pengampu").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#pengampuid').val(ui.item.id); 
			$('#pengampu').val(ui.item.value);
		} 
	});  			
	
	$("#dosen").autocomplete({ 
		source: base_url + "/module/akademik/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
			$('#frmDosen').submit(); 
		} 
	});  			
	
		
	$(".hari").autocomplete({ 
		source: base_url + "/module/masterdata/conf/hari",
		minLength: 0, 
		select: function(event, ui) { 
			$('#hariid').val(ui.item.id); 
			$('#hari').val(ui.item.value);
		} 
	});  
		
	
	
	$("#ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".blokwaktu").autocomplete({ 
		source: base_url + "/module/penjadwalan/conf/blokwaktu",
		minLength: 0, 
		select: function(event, ui) { 
			$('#blokid').val(ui.item.id); 
			$('#blok').val(ui.item.value);
		} 
	});  
	
	$(".jammulai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jammulai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jammulaiid').val(ui.item.id); 
			$('#jammulai').val(ui.item.value);
		} 
	});  
	
	$(".jamselesai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jamselesai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jamselesaiid').val(ui.item.id); 
			$('#jamselesai').val(ui.item.value);
		} 
	});  
	
	$(".prodi").autocomplete({ 
		source: base_url + "/module/masterdata/conf/prodi",
		minLength: 0, 
		select: function(event, ui) { 
			$('#prodiid').val(ui.item.id); 
			$('#prodi').val(ui.item.value);
		} 
	});  
	
	$("#mkditawarkan").autocomplete({ 
		source: base_url + "/module/akademik/conf/mkditawarkan/1",
		minLength: 0, 
		select: function(event, ui) { 
			$('#mkditawarkanid').val(ui.item.id); 
			$('#mkditawarkan').val(ui.item.value);
		} 
	});  
	
	
	$(".cmbmulti").select2();	
	$("#cmbmulti").select2();
	$("#cmbunit").select2();
	$("#cmbruang").select2();
	

   
   $(".btn-delete-post").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/deletemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	$(".btn-delete-ujian").click(function(){
		alert("a");
		var pid = $(this).data("id");
		//var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		//var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/deleteujian/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					//row.fadeOut();
					load_jadwal();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	
	$(".btn-delete-post-kegiatan").click(function(){
		alert("a");
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	
	$( "#date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$( ".date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	
	$(".dosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
		} 
	});  

	$(".ndosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ndosenid').val(ui.item.id); 
			$('#ndosen').val(ui.item.value);
		} 
	});  		

	$(".pengampu").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#pengampuid').val(ui.item.id); 
			$('#pengampu').val(ui.item.value);
		} 
	});  			
	
	$("#dosen").autocomplete({ 
		source: base_url + "/module/akademik/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
			$('#frmDosen').submit(); 
		} 
	});  			
	
		
	$(".hari").autocomplete({ 
		source: base_url + "/module/masterdata/conf/hari",
		minLength: 0, 
		select: function(event, ui) { 
			$('#hariid').val(ui.item.id); 
			$('#hari').val(ui.item.value);
		} 
	});  
		
	
	
	$("#ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	$(".ruangform").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#input-ruang').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".blokwaktu").autocomplete({ 
		source: base_url + "/module/penjadwalan/conf/blokwaktu",
		minLength: 0, 
		select: function(event, ui) { 
			$('#blokid').val(ui.item.id); 
			$('#blok').val(ui.item.value);
		} 
	});  
	
	$(".jammulai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jammulai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jammulaiid').val(ui.item.id); 
			$('#jammulai').val(ui.item.value);
		} 
	});  
	
	$(".jamselesai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jamselesai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jamselesaiid').val(ui.item.id); 
			$('#jamselesai').val(ui.item.value);
		} 
	});  
	
	$(".prodi").autocomplete({ 
		source: base_url + "/module/masterdata/conf/prodi",
		minLength: 0, 
		select: function(event, ui) { 
			$('#prodiid').val(ui.item.id); 
			$('#prodi').val(ui.item.value);
		} 
	});  
	
	$("#mkditawarkan").autocomplete({ 
		source: base_url + "/module/akademik/conf/mkditawarkan/1",
		minLength: 0, 
		select: function(event, ui) { 
			$('#mkditawarkanid').val(ui.item.id); 
			$('#mkditawarkan').val(ui.item.value);
		} 
	});  
	
	$(".cmbmulti").select2();
	$(".e9").select2();	
	$('#jadwalform').hide();	
		  
	$(".tagStaffUjian").autocomplete({ 
		source: base_url + "/module/penjadwalan/conf/staff_tmp",
		minLength: 0, 
		select: function(event, ui) { 
			$('.tmpstaffujian').val(ui.item.id); 
			$('.tagStaffUjian').val(ui.item.value);
		} 
	});

	$(".tagStaff").tagsManager({
			prefilled: $('.tmpstaff').val(),	
			deleteTagsOnBackspace: true,	
			preventSubmitOnEnter: true,
			typeahead: true,
			typeaheadAjaxSource: base_url + "/module/masterdata/conf/staff",
			AjaxPush: base_url + "/module/masterdata/conf/staff/push",
			blinkBGColor_1: '#FFFF9C',
			blinkBGColor_2: '#CDE69C',
			hiddenTagListName: 'hidStaff'
		  });	  	
	});



 $('.cmbjadwal').change(function(e) {
	
	e.preventDefault();	
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	$('#jadwalform').hide();	
	
	
	if((kegiatanid=='uts') || (kegiatanid=='uas')){
		if(semesterid!="-"){
			$("#form-report-koordinator").show();
			$("#form-report").show();
			$("#form-hari").show();
			$("#form-kegiatan").hide();
			get_koordinator(semesterid, kegiatanid);
			get_panitia(semesterid, kegiatanid);
			create_report_form();	
		}
	}else{
		if(kegiatanid=='bimbingan'){
			$("#form-kegiatan").show();
			$("#form-report-koordinator").hide();
			$("#form-report").hide();
			$("#form-hari").hide();
			get_kegiatan(semesterid, kegiatanid);
		}else{
			$("#form-hari").show();
			$("#form-kegiatan").hide();
			$("#form-report-koordinator").hide();
			$("#form-report").hide();
		}
	}
	load_jadwal();
	
});


function get_kegiatan(semesterid, kegiatanid){
	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/kegiatan_form',
			data : $.param({
				cmbsemester : semesterid,
				cmbjenis 	: kegiatanid
				
			}),
			success : function(msg) {
				if (msg == '') {
					
				} else {
					$("#form-kegiatan").html(msg);
				
				}
			}
		});
}

function get_koordinator(semesterid, kegiatanid){
	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/get_koordinator',
			data : $.param({
				cmbsemester : semesterid,
				cmbjenis 	: kegiatanid
				
			}),
			success : function(msg) {
				if (msg == '') {
					$('#ndosenid').val(""); 
					$('#ndosen').val("");
				} else {
					var msg = JSON.parse(msg);
					
					for (var i = 0; i < msg.length; i++) {
						var data = msg[i];						
						$('#ndosenid').val(data.koordinator_id); 
						$('#ndosen').val(data.nama);
					}	
				
				}
			}
		});
}

function get_panitia(semesterid, kegiatanid){
	var nama="";
	var id = "";
		
	var tagApi = jQuery(".tagStaffUjian").tagsManager({
		prefilled: $('.tmpstaffujian').val(),	
		deleteTagsOnBackspace: true,	
		preventSubmitOnEnter: true,
		typeahead: true,
		replace:true,
		hiddenTagListName: 'hidStaff'
      });
		
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/get_panitia',
			data : $.param({
				cmbsemester : semesterid,
				cmbjenis 	: kegiatanid				
			}),
			success : function(msg) {
			
				tagApi.tagsManager('empty');
				if (msg == '') {
					$('#panitiaid').val(""); 
					$('#panitia').val("");
				} else {
					var msg = JSON.parse(msg);
					
					for (var i = 0; i < msg.length; i++) {
						var data = msg[i];	
						nama += data.nama +",";
						id += data.nama +"&";
						
						tagApi.tagsManager("pushTag", data.nama);
						
					}
					
					$(".tagStaffUjian").autocomplete({ 
						source: base_url + "/module/penjadwalan/conf/staff_tmp",
						minLength: 0, 
						select: function(event, ui) { 
							$('.tmpstaffujian').val(ui.item.id); 
							$('.tagStaffUjian').val(ui.item.value);
						} 
					});  
					
				}
			}
		});
}

function create_report_form(){
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	
		
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/form_report',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid
				
			}),
			success : function(msg) {
				
				if (msg == '') {
					//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
				} else {
					$("#form-report").html(msg);
					$("#form-report-view").hide();
					 //location.reload();
				}
			}
		});
}
	
function load_jadwal(){			
	$('#jadwalform').hide();
		
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	
	var ruang		= document.getElementById("ruangid");
	var ruangid		= $(ruang).val();
	
	var mulai		= document.getElementById("input-mulai");
	var jammulai	= $(mulai).val();
	
	var selesai		= document.getElementById("input-selesai");
	var jamselesai	= $(selesai).val();
	
	var tgl		= document.getElementById("input-tgl");
	var tglval	= $(tgl).val();
	
	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/jadwal_view',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid,
				cmbruang 	: ruangid,
				cmbmulai 	: jammulai,	
				cmbselesai 	: jamselesai,
				cmbtgl		: tglval
				
			}),
			success : function(msg) {
				
				if (msg == '') {
					//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
				} else {
					$("#content").html(msg);
					 //location.reload();
				}
			}
		});
	}
	
	function save_proses(){
	
		$('#form-add-ujian').submit(function (e) {
		
			 e.preventDefault(); //STOP default action
			var today = new Date();	
			
			var postData = $(this).serializeArray();
			//var postData = new FormData($('#form-add-ujian')[0]);
			
			var kegiatan	= document.getElementById("cmbjenis");
			var kegiatanid	= $(kegiatan).val();
			
			var semester 	= document.getElementById("cmbsemester");
			var semesterid	= $(semester).val();
			
			var tmp	= $(document.getElementById("hidtmp")).val();
			
			if(kegiatanid=='uts' || kegiatanid=='uas'){
				var formURL = base_url + 'module/penjadwalan/jadwal/save_ujian/'+kegiatanid;
			}else{
				if(kegiatanid=='bimbingan'){
					var formURL = base_url + 'module/penjadwalan/jadwal/save_kegiatan';
				}else{
					var formURL = base_url + 'module/penjadwalan/jadwal/save_kuliah/'+kegiatanid;
				}
			}
			
			  $.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				success:function(msg,data, textStatus, jqXHR) 
				{	
					
					$('.status-submit').html("<em>OK! Last saved on "+today+"</em>");
					if(kegiatanid=="bimbingan"){
						load_jadwal();	
						$("#form-report-koordinator").hide();
						$("#form-report").hide();
						$("#form-hari").hide();
						get_kegiatan(semesterid, kegiatanid);
							
					}else{
						if(tmp==""){
							load_jadwal();
						}
					}
										
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					alert ('Failed!');      
				}
			  });
		   
			return false;
		});
	
	}
	

	
	

  

	
	
