$('#jadwalform').hide();
$("#mainform #form-report-koordinator").hide();
$("#form-report-view").hide();
$("#content").hide();  

$(".cmbmulti").select2();	
$("#cmbmulti").select2();
$("#cmbunit").select2();
$("#cmbruang").select2();

$('#jadwalform').hide();	
$("[data-toggle=tooltip]").tooltip();

$(".pop").each(function() {
	var $pElem= $(this);
	$pElem.popover(
		{
		  title: getPopTitle($pElem.attr("id")),
		  content: getPopContent($pElem.attr("id")),
		  trigger:'hover'
		}
	);
});
					
function getPopTitle(target) {
	return $("#" + target + "_content > div.popTitle").html();
};
			
function getPopContent(target) {
	return $("#" + target + "_content > div.popContent").html();
};

$("#date").datepicker({
	dateFormat: 'yy-mm-dd',
	changeMonth: true,
	changeYear: true,
	showButtonPanel: true
});
	
$(".dosen").autocomplete({ 
	source: base_url + "/module/masterdata/conf/dosen",
	minLength: 0, 
	select: function(event, ui) { 
		$('#dosenid').val(ui.item.id); 
		$('#dosen').val(ui.item.value);
	}
});  

$(".ndosen").autocomplete({ 
	source: base_url + "/module/masterdata/conf/dosen",
	minLength: 0, 
	select: function(event, ui) { 
		$('#ndosenid').val(ui.item.id); 
		$('#ndosen').val(ui.item.value);
	} 
}); 

$(".tagStaffUjian").autocomplete({ 
	source: base_url + "/module/scheduling/conf/staff_tmp",
	minLength: 0, 
	select: function(event, ui) { 
		$('#panitiaid').val(ui.item.id); 
		$('#panitia').val(ui.item.value);
	} 
});
	
$(".pengampu").autocomplete({ 
	source: base_url + "/module/masterdata/conf/dosen",
	minLength: 0, 
	select: function(event, ui) { 
		$('#pengampuid').val(ui.item.id); 
		$('#pengampu').val(ui.item.value);
	} 
});
	
$("#dosen").autocomplete({ 
	source: base_url + "/module/akademik/conf/dosen",
	minLength: 0, 
	select: function(event, ui) { 
		$('#dosenid').val(ui.item.id); 
		$('#dosen').val(ui.item.value);
		$('#frmDosen').submit(); 
	} 
}); 
	
$(".hari").autocomplete({ 
	source: base_url + "/module/masterdata/conf/hari",
	minLength: 0, 
	select: function(event, ui) { 
		$('#hariid').val(ui.item.id); 
		$('#hari').val(ui.item.value);
	} 
});  
	
$(".ruang").autocomplete({ 
	source: base_url + "/module/masterdata/conf/ruang",
	minLength: 0, 
	select: function(event, ui) { 
		$('#ruangid').val(ui.item.id); 
		$('#ruang').val(ui.item.value);
	} 
});  
	
$(".blokwaktu").autocomplete({ 
	source: base_url + "/module/scheduling/conf/blokwaktu",
	minLength: 0, 
	select: function(event, ui) { 
		$('#blokid').val(ui.item.id); 
		$('#blok').val(ui.item.value);
	} 
});  
	
$(".prodi").autocomplete({ 
	source: base_url + "/module/masterdata/conf/prodi",
	minLength: 0, 
	select: function(event, ui) { 
		$('#prodiid').val(ui.item.id); 
		$('#prodi').val(ui.item.value);
	} 
});  
	
$("#mkditawarkan").autocomplete({ 
	source: base_url + "/module/akademik/conf/mkditawarkan/1",
	minLength: 0, 
	select: function(event, ui) { 
		$('#mkditawarkanid').val(ui.item.id); 
		$('#mkditawarkan').val(ui.item.value);
	} 
});
   
$(".ruangform").autocomplete({ 
	source: base_url + "/module/masterdata/conf/ruang",
	minLength: 0, 
	select: function(event, ui) { 
		$('#input-ruang').val(ui.item.id); 
		$('#ruang').val(ui.item.value);
	} 
});  
	
$(".blokwaktu").autocomplete({ 
	source: base_url + "/module/scheduling/conf/blokwaktu",
	minLength: 0, 
	select: function(event, ui) { 
		$('#blokid').val(ui.item.id); 
		$('#blok').val(ui.item.value);
	} 
}); 
	
$(".tagStaff").tagsManager({
	prefilled: $('.tmpstaff').val(),	
	deleteTagsOnBackspace: true,	
	preventSubmitOnEnter: true,
	typeahead: true,
	typeaheadAjaxSource: base_url + "/module/masterdata/conf/staff",
	AjaxPush: base_url + "/module/masterdata/conf/staff/push",
	blinkBGColor_1: '#FFFF9C',
	blinkBGColor_2: '#CDE69C',
	hiddenTagListName: 'hidStaff'
});	  	

$('.cmbjadwal').change(function() {
	// e.preventDefault();	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	
	$('#jadwalform').hide();
	if((kegiatanid=='uts') || (kegiatanid=='uas')){
		if(semesterid!="-"){
			$("#form-report-koordinator").show();
			$("#form-report").show();
			$("#form-hari").show();
			$("#form-kegiatan").hide();
			get_koordinator(semesterid, kegiatanid);
			get_panitia(semesterid, kegiatanid);
			create_report_form();	
		}
	}else if(kegiatanid=='bimbingan'){
		$("#form-kegiatan").show();
		$("#form-report-koordinator").hide();
		$("#form-report").hide();
		$("#form-hari").hide();
		get_kegiatan(semesterid, kegiatanid, fakultasid);
	}else{
		$("#form-hari").show();
		$("#form-kegiatan").hide();
		$("#form-report-koordinator").hide();
		$("#form-report").hide();
	}
	load_jadwal();
});

function refreshDateTimePicker (){
	$(".date").datepicker({
		dateFormat: 'yy-mm-dd'
	});
	$(".jammulai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jammulai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jammulaiid').val(ui.item.id); 
			$('#jammulai').val(ui.item.value);
		}
	});  
	$(".jamselesai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jamselesai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jamselesaiid').val(ui.item.id); 
			$('#jamselesai').val(ui.item.value);
		} 
	});
}

function get_kegiatan(semesterid, kegiatanid, fakultas){
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/kegiatan_form',
		data : $.param({
			cmbsemester : semesterid,
			cmbjenis 	: kegiatanid,
			cmbfak		: fakultas
		}),
		success : function(msg) {
			if (msg == '') {
			} else {
				$("#form-kegiatan").html(msg);
				refreshDateTimePicker();
			}
		}
	});
}

function get_koordinator(semesterid, kegiatanid){
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/get_koordinator',
		data : $.param({
			cmbsemester : semesterid,
			cmbjenis 	: kegiatanid
		}),
		success : function(msg) {
			if (msg == '') {
				$('#ndosenid').val(""); 
				$('#ndosen').val("");
			} else {
				var msg = JSON.parse(msg);
				
				for (var i = 0; i < msg.length; i++) {
					var data = msg[i];						
					$('#ndosenid').val(data.koordinator_id); 
					$('#ndosen').val(data.nama);
				}	
			
			}
		}
	});
}

function get_panitia(semesterid, kegiatanid){
	var nama="";
	var id = "";
		
	var tagApi = jQuery(".tagStaffUjian").tagsManager({
		prefilled: $('.tmpstaffujian').val(),	
		deleteTagsOnBackspace: true,	
		preventSubmitOnEnter: true,
		typeahead: true,
		replace:true,
		hiddenTagListName: 'hidStaff'
      });
		
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/get_panitia',
		data : $.param({
			cmbsemester : semesterid,
			cmbjenis 	: kegiatanid				
		}),
		success : function(msg) {
			tagApi.tagsManager('empty');
			if (msg == '') {
				$('#panitiaid').val(""); 
				$('#panitia').val("");
			} else {
				var msg = JSON.parse(msg);
				for (var i = 0; i < msg.length; i++) {
					var data = msg[i];	
					nama += data.nama +",";
					id += data.nama +"&";
					tagApi.tagsManager("pushTag", data.nama);
				}
			}
		}
	});
}

function create_report_form(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/form_report',
		data : $.param({
			cmbsemester : semesterid,
			cmbhari		: hariid,
			cmbfak		: fakultasid,
			cmbjenis 	: kegiatanid
			
		}),
		success : function(msg) {
			if (msg == '') {
			} else {
				$("#form-report").html(msg);
				$("#form-report-view").hide();
			}
		}
	});
}
	
function load_jadwal(){
	$('#jadwalform').hide();
	
	$("#content").show();
		
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	
	var ruang		= document.getElementById("ruangid");
	var ruangid		= $(ruang).val();
	
	var mulai		= document.getElementById("input-mulai");
	var jammulai	= $(mulai).val();
	
	var selesai		= document.getElementById("input-selesai");
	var jamselesai	= $(selesai).val();
	
	var tgl		= document.getElementById("input-tgl");
	var tglval	= $(tgl).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/jadwal_view',
		data : $.param({
			cmbsemester : semesterid,
			cmbhari		: hariid,
			cmbjenis 	: kegiatanid,
			cmbfak	 	: fakultasid,
			cmbruang 	: ruangid,
			cmbmulai 	: jammulai,	
			cmbselesai 	: jamselesai,
			cmbtgl		: tglval
		}),
		beforeSend : function (){
			$('#loadingModal').modal({
				backdrop : 'static',
				show:true
			});
			// $("#content").html("<center><img src='"+base_url + 'modules/scheduling/assets/images/loading.gif'+"'></center>");
		},
		success : function(msg) {
			if (msg == '') {
			} else {
				$('#loadingModal').modal('hide');
				$("#content").html(msg);
				$('.status-submit').html("");
			}
		}
	});
}
	
function save_proses(){
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	
	var mk	= document.getElementById("cmbmk");
	var mkid	= $(mk).val();
	if(mkid=="-"){
		$(mk).focus();
		return ;
	}
	
	var prodi	= document.getElementById("cmbprodi");
	var prodiid	= $(prodi).val();
	if(prodiid=="-"){
		$(prodi).focus();
		return ;
	}
	
	var dosen	= document.getElementById("cmbdosen");
	var dosenid	= $(dosen).val();
	if(dosenid=="130904012736"){
		$(dosen).focus();
		return ;
	}

	// $('#form-add-ujian').submit(function () {
		// e.preventDefault(); //STOP default action
	var today = new Date();	
	var postData = $('#form-add-ujian').serializeArray();
	//var postData = new FormData($('#form-add-ujian')[0]);
	
	var tmp	= $(document.getElementById("hidtmp")).val();
	
	if(kegiatanid=='uts' || kegiatanid=='uas'){
		
		var formURL = base_url + 'module/scheduling/jadwal/save_ujian/'+kegiatanid;
		
	}else{
		if(kegiatanid=='bimbingan'){
			var formURL = base_url + 'module/scheduling/jadwal/save_kegiatan';
		}else{
			var formURL = base_url + 'module/scheduling/jadwal/save_kuliah/'+kegiatanid;
		}
	}
	
 	$.ajax({
		url : formURL,
		type: "POST",
		data : postData,
		success:function(msg,data, textStatus, jqXHR) 
		{
			if(kegiatanid=="bimbingan"){
				load_jadwal();	
				$("#form-report-koordinator").hide();
				$("#form-report").hide();
				$("#form-hari").hide();
				get_kegiatan(semesterid, kegiatanid);
			}else{
				load_jadwal();
			}
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
			alert ('Failed!');      
		}
   });
		// return false;
	// });

}
//event Jadwal
$(document).on('click', '.popup-form', function(e){
	e.preventDefault();
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	if(semesterid == "-") {
		alert("Jangan Lupa Pilih Semester!"); 
		return;
	}
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	if(kegiatanid == "-") {
		alert("Jangan Lupa Pilih Jenis Kegiatan!"); 
		return;
	}
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	if(hariid == "-") {
		alert("Jangan Lupa Pilih Hari!"); 
		return;
	}
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	if(fakultasid == "-") {
		alert("Jangan Lupa Pilih Fakultas!"); 
		return;
	}
	
	var ruang=$(this).data('ruang');
	var mulai=$(this).data('jammulai');
	var selesai=$(this).data('jamselesai');
	var tglmulai=$(this).data('tmulai');
	var tglselesai=$(this).data('tselesai');
	var repeat=$(this).data('repeat');
	var id=$(this).data('id');
	
	var data = ruang.split('|');
	
	var idruang_ = data[0];
	var nruang_ = data[1];
	
	$('#jadwalform').hide('fast');
	$('#jadwalform').show('fast');
	$('#jadwalform #form-tgl').hide();
	$('#jadwalform #form-repeat').hide();

	$.ajax({	
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/jadwal_by_form',
		data : $.param({
			cmbsemester : semesterid,
			cmbhari		: hariid,
			cmbfak		: fakultasid,
			cmbjenis 	: kegiatanid,
			cmbruang 	: ruang,
			cmbmulai 	: mulai,	
			cmbselesai 	: selesai,
			cmbjadwal	: id
			
		}),beforeSend : function (){
			$('#loadingModal').modal({
				backdrop : 'static',
				show:true
			});
			// $("#content").html("<center><img src='"+base_url + 'modules/scheduling/assets/images/loading.gif'+"'></center>");
		},success : function(msg) {
			if (msg == '') {
			} else {
				$("#jadwalformdetail").html(msg);
			}
			$('#loadingModal').modal('hide');
		},error: function (request, status, error) {
			$('#loadingModal').modal('hide');
		}
	});
	$('#jadwalform #input-ruang').val(idruang_);
	$('#jadwalform #input-ruang-nama').val(nruang_);
	$('#jadwalform #input-mulai').val(mulai);
	$('#jadwalform #input-selesai').val(selesai);
	$('#jadwalform #form-repeat').show();
	$("#cmbrepeat option[value="+repeat+"]").attr('selected', 'selected');	
	$('#jadwalform #input-tgl-mulai').val(tglmulai.substring(0, 10));
	$('#jadwalform #input-tgl-selesai').val(tglselesai.substring(0, 10));
	refreshDateTimePicker();
});
$(document).on("click", ".btn-delete-jadwal",function(){
	var pid = $(this).data("id");
	if(confirm("Delete this post? Once done, this action can not be undone.")) {
		$.post(
			base_url + 'module/scheduling/jadwal/deletemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					//row.fadeOut();
					load_jadwal();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
			alert(xhr.responseText);
		});
	}
});
$(document).on("click", ".btn-delete-post",function(){
	var pid = $(this).data("id");
	var mod	= $(this).parents('li').data("id");
	
	if(confirm("Delete this post? Once done, this action can not be undone.")) {
	
	var row = $(this).parents('li');
			
	$.post(
		base_url + 'module/scheduling/jadwal/deletemk/'+pid,
		function(data){
			if(data.status.trim() == "OK")
				row.fadeOut();
			else alert(data.error);
		},
		"json"
		).error(function(xhr) {
			alert(xhr.responseText);
		});
	}
});
$(document).on("click", ".btn-disable-jadwal",function() {
	var pid = $(this).data("id");
	if(confirm("Disable this schedule? Once done, this action can not be undone.")) {
	$.post(
		base_url + 'module/scheduling/jadwal/disablemk/'+pid,
		function(data){
			if(data.status.trim() == "OK")				
				load_jadwal();
			else alert(data.error);
		},
		"json"
		).error(function(xhr) {
			alert(xhr.responseText);
		});
	}
});
//event Ujian
$(document).on('click', '.popup-form-ujian', function(e){
	e.preventDefault();
	var ruang=$(this).data('ruang');
	var mulai=$(this).data('jammulai');
	var selesai=$(this).data('jamselesai');
	var tgl=$(this).data('tgl');
	
	$('#jadwalform').show('slow');
	$('#jadwalform #form-tgl').hide();
	$('#jadwalform #form-repeat').hide();
	
	var data = ruang.split('|');
	
	var idruang_ = data[0];
	var nruang_ = data[1];
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();

	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/scheduling/jadwal/ujian_by_form',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid,
				cmbruang 	: ruang,
				cmbmulai 	: mulai,	
				cmbselesai 	: selesai,
				cmbtgl		: tgl
				
			}),beforeSend : function (){
				$('#loadingModal').modal({
					backdrop : 'static',
					show:true
				});
				// $("#content").html("<center><img src='"+base_url + 'modules/scheduling/assets/images/loading.gif'+"'></center>");
			},success : function(msg) {
				
				if (msg == '') {
					//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					//$("#jadwalformdetail").html(msg);
				} else {
					$("#jadwalformdetail").html(msg);
					 //location.reload();
				}
				$('#loadingModal').modal('hide');
			},error: function (request, status, error) {
				$('#loadingModal').modal('hide');
			}
		});
	$('#jadwalform #input-ruang').val(idruang_);
	$('#jadwalform #input-ruang-nama').val(nruang_);
	$('#jadwalform #input-mulai').val(mulai);
	$('#jadwalform #input-selesai').val(selesai);
	$('#jadwalform #form-tgl').show();
	$('#jadwalform #input-tgl').val(tgl);				
	
});
$(document).on('click', '#jadwalform .add-pengawas', function(e) {
	var x 			= $(document.getElementById("hidtmp")).val();
			
	var prodi		= document.getElementById("cmbprodi");
	var prodiid		= $(prodi).val();
	
	var mk			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var kelas		= document.getElementById("cmbkelas");
	var kelasid		= $(kelas).val();
	var output		= "";		
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/get_staff',
		data : $.param({
			
			cmbprodi	: prodiid,
			cmbmk		: mkid,
			cmbkelas	: kelasid
			
		}),
		success : function(msg) {
			
			if (msg == '') {
				//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
			} else {
				output += "<div class='col-md-6'><select class='form-control e9 pengawas-"+ x +"' name='pengawas[]' ><option value='-'>None</option>";
				var msg = JSON.parse(msg);
				for (var i = 0; i < msg.length; i++) {
					var data = msg[i];
					output += "<option value="+data.id+" data-uri='1'>"+data.value+"</option>";
				}
				output += "</select></div>";
				output += "<div class='col-md-6'><input type='checkbox' name='chk"+x+"' value='1' >Hadir";
				output += "&nbsp;<input type='checkbox' name='radio"+x+"' value='1'>Pengganti</div>";
				$('#jadwalformdetail #pengawas-form').append(output);
				x = x + 1;
				
			}
		}
	});
});
$(document).on("click", ".btn-new-jadwal",function(){
	$(document.getElementById("hidtmp")).val("");
	$(document.getElementById("cmbmk")).val("");
	$(document.getElementById("cmbkelasx")).val("");
	$(document.getElementById("cmbprodi")).val("");
	$(document.getElementById("new-jadwal")).hide();		
});
$(document).on("click", ".btn-delete-ujian",function(){
	var pid = $(this).data("id");
	if(confirm("Delete this post? Once done, this action can not be undone.")) {	
		$.post(
			base_url + 'module/scheduling/jadwal/deleteujian/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					load_jadwal();
				else alert(data.error);
			},
			"json"
		).error(function(xhr) {
			alert(xhr.responseText);
		});
	}
});
//pengampu
$(document).on('change', '#jadwalform .cmbdetail', function(e) {
	var prodi		= document.getElementById("cmbprodi");
	var prodiid		= $(prodi).val();
	
	var mk			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var kelas		= document.getElementById("cmbkelas");
	var kelasid		= $(kelas).val();
	var output		= "";			
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/scheduling/jadwal/get_pengampu_kuliah',
			data : $.param({					
				cmbprodi	: prodiid,
				cmbmk		: mkid,
				cmbkelas	: kelasid					
			}),
			success : function(msg) {					
				if (msg == '') {
					//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
				} else {
					output += "<label>Pengampu</label><select id='cmbdosen' class='e9 form-control' name='pengampu' ><option value='130904012736'>Dosen Pengampu</option>";
					var msg = JSON.parse(msg);
					for (var i = 0; i < msg.length; i++) {
						var data = msg[i];
						output += "<option value="+data.karyawan_id+" data-uri='1'>"+data.nama+"</option>";
					}
					output += "</select></div>";
					$('#jadwalformdetail #dosen-form').html(output);
				}
			}
		});
	
});	
//lain lain
$(document).on("click", '#btn-modal-close',function(e){
	e.preventDefault();	
	$('#jadwalform').hide();
});
$(document).on("click",'#writeTab a',function (e) {
  e.preventDefault();
  $(this).tab('show');
});
//laporan
$(document).on('change',".cmbreport", function(e){
	e.preventDefault();
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	
	var lokasi		= document.getElementById("cmblokasi");
	var lokasiid	= $(lokasi).val();
	
	var prodi		= document.getElementById("cmbprodix");
	var prodiid		= $(prodi).val();
	
	var tgl			= document.getElementById("cmbtgl");
	var tglid		= $(tgl).val();
	
	var jam			= document.getElementById("cmbjam");
	var jamid		= $(jam).val();
	
	// var id			= document.getElementById("tmpid");
	var id			= document.getElementById("hidtmp");
	var hidid		= $(id).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	if(hidid=='panitia'){
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/scheduling/jadwal/cetak_panitia',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid,
				cmbtgl		: tglid,
				cmbfak		: fakultasid						
			}),beforeSend : function (){
				$('#loadingModal').modal({
					backdrop : 'static',
					show:true
				});
			},success : function(msg) {
				$('#loadingModal').modal('hide');
				if (msg == '') {
				} else {
					$("#content").html(msg);						
				}
			}
		});
	}
	else{
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/scheduling/jadwal/cetak_hadir',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid,
				cmblokasi	: lokasiid,
				cmbprodi	: prodiid,
				cmbtgl		: tglid,
				cmbjam		: jamid,
				cmbfak		: fakultasid
			}),beforeSend : function (){
				$('#loadingModal').modal({
					backdrop : 'static',
					show:true
				});
			},success : function(msg) {
				$('#loadingModal').modal('hide');
				if (msg == '') {
				} else {
					$("#content").html(msg);						
				}
			}
		});
	}
});
$(document).on("click", ".btn-cetak-absen", function(){
	$('#jadwalform').hide();
	$("#form-report-view").show();
	$("#form-report-view .frm-absen").hide().show('fast');
	$("#form-report-view .frm-panitia").hide();
	$("#form-report-view #hidtmp").val("");
});
$(document).on("click", ".btn-cetak-panitia",function(){
	$('#jadwalform').hide();
 	$("#form-report-view").show();
 	$("#form-report-view .frm-absen").hide();
	$("#form-report-view .frm-panitia").hide().show('fast');
	$("#form-report-view #hidtmp").val("panitia");
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	
	var fakultas	= document.getElementById("cmbfak");
		var fakultasid	= $(fakultas).val();
	
	var tglid = "-";
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/cetak_panitia',
		data : $.param({
			cmbsemester : semesterid,
			cmbhari		: hariid,
			cmbjenis 	: kegiatanid,
			cmbtgl		: tglid,
			cmbfak		: fakultasid		
		}),
		success : function(msg) {
			//alert(msg);
			if (msg == '') {
			} else {
				$("#content").html(msg);						
			}
		}
	});
});
$(document).on("click", ".btn-cetak-absen-ok", function(){
	$('#jadwalform').hide();
	$('.title-page').hide();
	$('.breadcrumb').hide();
	window.print();
	setTimeout(function() {
		$('.title-page').show();
		$('.breadcrumb').show();
	}, 100);
});
$(document).on("click", ".btn-cetak-panitia-ok", function(){
	$('#jadwalform').hide();
	$('.title-page').hide();
	$('.breadcrumb').hide();
	window.print();
	setTimeout(function() {
		$('.title-page').show();
		$('.breadcrumb').show();
	}, 100);
});
$(document).on("click", ".btn-cetak-rekap", function(){
	$('#jadwalform').hide();
	$("#form-report-view").hide();
	$("#form-report-view #hidtmp").val("");
	  
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/scheduling/jadwal/cetak_rekap',
		data : $.param({
			cmbsemester : semesterid,
			cmbhari		: hariid,
			cmbjenis 	: kegiatanid,
			cmbfak		: fakultasid	
			
		}),beforeSend : function (){
			$('#loadingModal').modal({
				backdrop : 'static',
				show:true
			});
			// $("#content").html("<center><img src='"+base_url + 'modules/scheduling/assets/images/loading.gif'+"'></center>");
		},success : function(msg) {
			$('#loadingModal').modal('hide');
			if (msg == '') {
				//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
			} else {
				$("#content").html(msg);
				$('.title-page').hide();
				$('.breadcrumb').hide();
				window.print();
				setTimeout(function() {
					$('.title-page').show();
					$('.breadcrumb').show();
				}, 100);
				//window.open(base_url + 'module/scheduling/jadwal/cetak_rekap/'+kegiatanid+'/'+semesterid+'/'+hariid,"_blank");
			}
		},error: function (request, status, error) {
			$('#loadingModal').modal('hide');
		}
	});
});//kegiatan
//kegiatan
$(document).on("click",".btn-delete-post-kegiatan",function(){
	var pid = $(this).data("id");
	var mod	= $(this).parents('li').data("id");
	
	if(confirm("Delete this post? Once done, this action can not be undone.")) {
		var row = $(this).parents('li');
		$.post(
			base_url + 'module/scheduling/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
		).error(function(xhr) {
			alert(xhr.responseText);
		});
	}
});