<?php
class masterdata_jenis extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($id=NULL){
		$mjenis = new model_jenis();	
		
		$data['posts'] = $mjenis->read();
		$data['panel'] = 'New Jenis Kenaikan';
		
		if($id!=""){
			$data['edit'] 		= $mjenis->read($id);
			$data['panel'] 		= 'Edit Jenis Kenaikan';
		}

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');	
		$this->add_script('js/jenis.js');
		
		$this->view( 'jenis/index.php', $data );
	}
	
	function saveToDB(){
		$mjenis = new model_jenis();	
		
		$keterangan = $_POST['keterangan'];
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$jenisid = $_POST['hidId'];
		}else $jenisid = $mjenis->get_reg_number();
		
		$datanya = array(
						 'jenis_kenaikan_id'=>$jenisid,
						 'keterangan'=>$keterangan
				        );
		
		if($mjenis->replace_jenis($datanya)){
			echo "Success";
		}else{
			echo "Failed";
		}
		
	}
	
}
?>