<?php
class masterdata_ruang extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;

		$coms->require_auth('auth');
	}
	
	function index(){
		$mruang = new model_ruang();

		$data['posts'] = $mruang->read();

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/ruang.js');

		$this->view('ruang/index.php', $data);
	}
	
	function edit($id){
		$mruang = new model_ruang();

		$data['posts'] = $mruang->read();
		$data['edit']  = $mruang->read($id);

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/ruang.js');

		$this->view('ruang/index.php', $data);
	}
	
	function delete($id){
		$mruang = new model_ruang();
		$mruang->delete($id);
		$this->redirect('module/masterdata/ruang');
	}
	
	function save(){
		$mruang= new model_ruang();

		$kode = $_POST['kode'];
		$kapasitas = $_POST['kapasitas'];
		$keterangan = $_POST['keterangan'];
		
		if($_POST['hidId'] != '') $id = $_POST['hidId'];
		else $id = $kode;
		
		$data = Array(
			'ruang_id' => $id,
			'keterangan' => $keterangan,
			'kode_ruang' => $kode,
			'fakultas_id'=> $_POST['cmbfakultas'],
			'cabang_id'=> $_POST['cmbcabang'],
			'kategori_ruang'=> $_POST['cmbkategori'],
			'kapasitas' => $kapasitas
		);
		$mruang->replace_ruang($data);
		$this->redirect('module/masterdata/ruang');
		exit();
	}
	
	function write(){
		$this->add_script('js/ruang.js');
		
		$this->view('ruang/edit.php');
	}

	//=========Karyawan Ruang=======//
	function karyawan($ruang=NULL,$karyawan=NULL){
		$mruang 		= new model_ruang();
		$mdosen 		= new model_dosen();
		
		
		$level	 				= $this->coms->authenticatedUser->level;
		
		if($level == '1'){
			$fakultas_id 			= "";
			$cabang_id 				= "";
		}
		else{
			$fakultas_id 			= $this->coms->authenticatedUser->fakultas;
			$cabang_id 				= $this->coms->authenticatedUser->cabang;
		}
		$data['ruang']			= $mruang->read('', $fakultas_id, $cabang_id);
		$data['karyawan']		= $mdosen->read('', $fakultas_id, $cabang_id);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');		
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		
		$this->coms->add_script('js/select2/select2.js');
		$this->coms->add_style('js/select2/select2.css');
		$this->add_script('js/ruang.js');
		
		$this->view('ruang/karyawan/index.php',$data);
	}
	
	function get_karyawan_ruang(){
		$mruang 		= new model_ruang();
		
		$fakultas_id 	= $this->coms->authenticatedUser->fakultas;
		$cabang_id 		= $this->coms->authenticatedUser->cabang;
		
		$ruang 			= $_POST['ruang'];
		$ruang_kar 		= $mruang->read_karyawan_ruang($ruang);
		if(isset($ruang_kar)){
			$data['posts'] = $ruang_kar;
			$this->view('ruang/karyawan/view_selection.php',$data);
		}
		else{ ?>
			<div class="col-sm-12" align="center" style="margin-top:20px;">
			    <div class="well">Maaf, data tidak ditemukan</div>
			</div> <?php
		}
	}
	
	function save_karyawan_ruang(){
		$mruang 		= new model_ruang();
		$mdosen 		= new model_dosen();
		
		$fakultas_id 	= $this->coms->authenticatedUser->fakultas;
		$cabang_id 		= $this->coms->authenticatedUser->cabang;
		
		$ruang			= $_POST['ruang'];
		$karyawan		= $_POST['karyawan'];
		
		$ruang_id		= $mruang->read($ruang,$fakultas_id,$cabang_id)->ruang_id;
		$count			= count($karyawan);
		// echo $count;
		for($i=0;$i<$count;$i++){
			$kar_id		= $mdosen->read($karyawan[$i],$fakultas_id,$cabang_id)->hid_id;
			$datanya 		= Array ('ruang_id'=> $ruang_id,
									 'karyawan_id'=> $kar_id
									);
			// var_dump($datanya);
			$cek = $mruang->cek_data($kar_id,$ruang_id);
			// echo $cek;
			if(!$cek){
				// echo "TES";
				$mruang->replace_karyawan_ruang($datanya);
			}
		}
		echo "OK, data telah diupdate.";
	    exit();
	}
	
	function delete_karyawan_ruang(){
		$mruang 	= new model_ruang();
		$ruang 		= $_POST['ruang'];
		$karyawan	= $_POST['karyawan'];
		$del  		= $mruang->delete_karyawan_ruang($ruang,$karyawan);
		if($del==TRUE){
			echo "berhasil";
		}
	}
	
}
?>