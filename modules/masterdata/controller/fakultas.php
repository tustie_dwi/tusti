<?php
class masterdata_fakultas extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($id=NULL){
		$mfak = new model_fakultas();	
		
		//$data['posts'] = $mfak->clean();	
		$data['posts'] = $mfak->read();
		
		if($id!=""){
			$data['edit'] 		= $mfak->read($id);
		}

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->add_script('js/fakultas.js');
		
		$this->view( 'fakultas/index.php', $data );
	}
	
	function write(){
		$mfak= new model_fakultas();		
		
		$data['posts'] 		= "";	
				
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/fakultas.js');
		
		$this->view('fakultas/edit.php', $data);
		
	}	
	
	function edit($id){
		if(  !$id ) {
			$this->redirect('module/akademik/fakultas');
			exit;
		}
		
		$mfak = new model_fakultas();	
			
			$data['posts'] 		= $mfak->read();	
			$data['edit'] 		= $mfak->read($id);	
			
							
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
			$this->add_script('js/fakultas.js');			
			
			$this->view('fakultas/index.php', $data);
	}
	
	
	function save(){
			if(isset($_POST['b_fakultas'])){
				$this->saveToDB();
				exit();
			}else{
				$this->index();
				exit;
			}
	}
	
	function saveToDB(){
		$mfak	 	= new model_fakultas();
		
		$ceknew 	= $_POST['ceknew'];
		$ketcek		= $mfak->cekketfakultas($_POST['keterangan']);
		$kodecek	= $mfak->cekkodefakultas($_POST['kode_fakultas']);
		$keterangan	= $_POST['keterangan'];	
		
		if($ceknew==1){
			if(isset($ketcek)||isset($kodecek)){
				echo "Duplicate!";
				exit();
			}
		}
		
		if($_POST['hidId']!=""){
			$fakultas_id 	= $_POST['kode_fakultas'];
			$fakultas_id_param = $_POST['hidId'];
			$action 	= "update";
		}else{
			$fakultas_id= $_POST['kode_fakultas'];
			$action 	= "insert";	
		}
		
		//upload image//
		foreach ($_FILES['files'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		$month = date('m');
		$year = date('Y');
		if($cekicon!='error'){
				$name = $_FILES['files']['name'];
				$ext	= $this->get_extension($name);
				$seleksi_ext = $mfak->get_ext($ext);
				if($seleksi_ext!=NULL){
						$jenis_file_id 	= $seleksi_ext->jenis_file_id;
						$jenis			= $seleksi_ext->keterangan;
						$maxfilesize	= $seleksi_ext->max_size;
						
						switch(strToLower($jenis)){
							case 'image':
								$extpath = "image";
							break;
						}
				}
				else{
						echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
						exit();
				}
				
				$allowed_ext = array('jpg','jpeg','png', 'gif');
				
				$icon_size=$_FILES["files"]["size"] ; 
				$ceknamaicon = $mfak->cek_nama_icon($name);
				$upload_dir = 'assets/upload/icon/fakultas/'.$fakultas_id. DIRECTORY_SEPARATOR;
				$upload_dir_db = 'upload/icon/fakultas/'.$fakultas_id. DIRECTORY_SEPARATOR;
				
				if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
							chmod($upload_dir, 0777);
				}
				$nama_file = $fakultas_id.".".$ext;
				$icon_loc = $upload_dir_db . $nama_file;
				if(!in_array($ext,$allowed_ext)){
						echo "Maaf, tipe file yang di upload salah";
						exit();
				}
				elseif (($ceknamaicon==NULL) && ($icon_size <= $maxfilesize)){
						// //echo $file_size;				
						if($_SERVER['REQUEST_METHOD'] == 'POST') {
									rename($_FILES['files']['tmp_name'], $upload_dir . $nama_file);
						}
				}
		}
		else{
			if(isset($_POST['hidimg'])){
				$icon_loc = $_POST['hidimg'];
			}
		}
		
		if(isset($icon_loc)){
			$datanya 	= Array(
							'fakultas_id'=>$fakultas_id, 
							'keterangan'=>$keterangan, 
							'kode_fakultas'=>$fakultas_id,
							'image'=>$icon_loc
							);
			$mfak->replace_fakultas($datanya);
			echo "Berhasil mengupdate!";
			exit();
		}
		else{
			$datanya 	= Array(
							'fakultas_id'=>$fakultas_id, 
							'keterangan'=>$keterangan, 
							'kode_fakultas'=>$fakultas_id
							);
			$mfak->replace_fakultas($datanya);
			echo "Berhasil mengupdate!";
			exit();
			
		}
	}

	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
}
?>