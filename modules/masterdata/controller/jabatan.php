<?php
class masterdata_jabatan extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($id=NULL){
		$mjabatan = new model_jabatan();	
		
		$data['posts'] = $mjabatan->read('','','parent');
		$data['cmbparent'] = $mjabatan->read();
		$data['panel'] = 'New Jabatan';
		
		if($id!=""){
			$data['edit'] 		= $mjabatan->read($id);
			$data['panel'] 		= 'Edit Jabatan';
		}

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');	
		$this->add_script('js/jabatan.js');
		
		$this->view( 'jabatan/index.php', $data );
	}
	
	function cleanss(){
		$mjabatan = new model_jabatan();	
		$mjabatan->clean();
	}
	
	function child($jabatan_id){
		$mjabatan = new model_jabatan();	
		$data = $mjabatan->read('','','child',$jabatan_id);
		if(isset($data)){
			foreach ($data as $d) {
				echo "<ul>";
				echo "<li>";
				echo "<span><i class='fa fa-minus-square'></i>&nbsp;".$d->keterangan."</span>";
				
				echo '	<div class="btn-group">
						    <button type="button" class="btn btn-default dropdown-toggle btn-edit" data-toggle="dropdown">
						      Action
						      <span class="fa fa-caret-down"></span>
						    </button>
						    <ul class="dropdown-menu">
						      <li><a class="" href="'.$this->location('module/masterdata/jabatan/index/'.$d->jabatan_id).'"><i class="fa fa-edit"></i> Edit</a></li>
						    </ul>
						</div>';
				
				$this->child($d->jabatan_id);
				echo "</li>";
				echo "</ul>";
			}
		}
		
	}
	
	function saveToDB(){
		$mjabatan = new model_jabatan();	
		
		$keterangan = $_POST['keterangan'];
		
		if(isset($_POST['parent'])&&$_POST['parent']!=''&&$_POST['parent']!='0'){
			$parent = $mjabatan->read($_POST['parent'], 'md5');
		}else $parent = '0';
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$jabatanid = $_POST['hidId'];
		}else $jabatanid = $mjabatan->get_reg_number();
		
		$datanya = array(
						 'jabatan_id'=>$jabatanid,
						 'keterangan'=>$keterangan,
						 'parent_id'=>$parent
				        );
		
		if($mjabatan->replace_jabatan($datanya)){
			echo "Success";
		}else{
			echo "Failed";
		}
		
	}
	
}
?>