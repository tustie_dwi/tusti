<?php
class masterdata_prodi extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($id=NULL){
		$mfakultas = new model_fakultas();
		$mprodi = new model_prodi();	
		
		if(isset($id)){		
			$data['posts'] 		= $mprodi->read($id);
		}
		$data['tes']="HALO";
		$data['fak'] 		= $mfakultas->read();
		
		$data['prodi'] 		= $mprodi->get_prodi();
		$data['fakultas'] 	= $mprodi->getFakultas();

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		 $this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');
		
		$this->add_script('js/prodi.js');	
		$this->view( 'prodi/tree.php', $data );
	}
	
	// function tree($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		// $mprodi = new model_prodi();
// 			
		// $data['posts'] = $mprodi->read('');
// 		
		// $data['menu'] = $this->show_menu('IS NULL');
// 
		// $this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		// $this->coms->add_script('js/datatables/jquery.dataTables.js');	
		// $this->coms->add_script('js/datatables/DT_bootstrap.js');	
		// $this->coms->add_style('bootstrap-combined.min.css');
		// $this->coms->add_script('bootstrap-tree.js');
// 	
		// switch($by){
			// case 'ok';
				// $data['status'] 	= 'OK';
				// $data['statusmsg']  = 'OK, data telah diupdate.';
			// break;
			// case 'nok';
				// $data['status'] 	= 'Not OK';
				// $data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			// break;
			// case 'asign';
				// $data['status'] 	= 'Not OK';
				// $data['statusmsg']  = 'Maaf, data telah di Assign.';
			// break;
			// case 'duplicate';
				// $data['status'] 	= 'Not OK';
				// $data['statusmsg']  = 'Maaf, data telah ada.';
			// break;
			// case 'duplicate_kode';
				// $data['status'] 	= 'Not OK';
				// $data['statusmsg']  = 'Maaf, Kode Program Studi tidak boleh sama.';
			// break;
		// }
// 		
// 		
		// $this->view( 'prodi/tree.php', $data );
	// }

	function show_menu ($url = '') {
		$menu = new model_prodi();
		
		if(count($menu->find($url))>0) {
			$row = "";
			$data = "<ul>";
			if($url == 'IS NULL') {
				$row .= "<li class='nav-home'><a href=".$this->location().">Beranda</a></li>";
			}
			foreach($menu->find($url) as $tmp) {
				$class = "";
				$temp = "";
				
				if(count($menu->find(' = '.$tmp->prodi_id))>0) {
					$temp = $this->show_menu(' = '.$tmp->prodi_id);
					$class = "drop";
				}
				
				$row .= "<li><a href='cktif'];
		}else{
			$is_aktif	= 0;
		}		

		//upload image//
		foreach ($_FILES['files'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		$month = date('m');
		$year = date('Y');
		if($cekicon!='error'){
				$name = $_FILES['files']['name'];
				$ext	= $this->get_extension($name);
				$seleksi_ext = $mprodi->get_ext($ext);
				if($seleksi_ext!=NULL){
						$jenis_file_id 	= $seleksi_ext->jenis_file_id;
						$jenis			= $seleksi_ext->keterangan;
						$maxfilesize	= $seleksi_ext->max_size;
						
						switch(strToLower($jenis)){
							case 'image':
								$extpath = "image";
							break;
						}
				}
				else{
						echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
						exit();
				}
				
				$allowed_ext = array('jpg','jpeg','png', 'gif');
				
				$icon_size=$_FILES["files"]["size"] ; 
				$ceknamaicon = $mprodi->cek_nama_icon($name);
				$upload_dir = 'assets/upload/icon/prodi/'.$kode_prodi. DIRECTORY_SEPARATOR;
				$upload_dir_db = 'upload/icon/prodi/'.$kode_prodi. DIRECTORY_SEPARATOR;
				
				if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
							chmod($upload_dir, 0777);
				}
				$nama_file = $kode_prodi.".".$ext;
				$icon_loc = $upload_dir_db . $nama_file;
				if(!in_array($ext,$allowed_ext)){
						echo "Maaf, tipe file yontent/read/".$tmp->keterangan."' id='menu-".$tmp->prodi_id."' class='".$class."'>".$tmp->keterangan."</a>";
				$row .= $temp . "</li>";
			}
			$data .= $row;
			$data .= "</ul>";
			return $data;
		}
	}
	
	function save(){
			if(isset($_POST['b_prodi'])!=""){
				$this->saveToDB();
				exit();
			}else{
				$this->index();
				exit;
			}
	}
	
	function saveToDB(){
		ob_start();
		
		$mprodi	 	= new model_prodi();
		
		$ceknew 			= $_POST['ceknew'];
		$ketcek				= $mprodi->cekketprodi($_POST['keterangan'], $_POST['strata']);
		$ketcekfakultas		= $mprodi->cekketprodibyketfakultas($_POST['keterangan']);
		$kodecek			= $mprodi->cekkodeprodi($_POST['kode_prodi']);
		$kodecekfakultas	= $mprodi->cekkodeprodibykodefakultas($_POST['kode_prodi']);
		$stratacek			= $mprodi->cekstrataprodi($_POST['strata']);
		
		
		if($ceknew==1){
			if(isset($kodecek)||isset($ketcek)||isset($ketcekfakultas)||isset($kodecekfakultas)){
				echo "Duplicate!";
				exit();
			}
		}
						
		if($_POST['hidId']!=""){
			$prodi_id 	= $_POST['hidId'];
			$action 	= "update";
		}else{
			$prodi_id	= $mprodi->get_reg_number();
			$action 	= "insert";	
		}
		
		$fakultas_id	= $_POST['select_fakultas'];
		$keterangan		= $_POST['keterangan'];
		$strata			= $_POST['strata'];
		$kode_prodi		= $_POST['kode_prodi'];
		$parent_id		= $_POST['select_parent'];
				
		if(isset($_POST['is_aktif'])!=""){
			$is_aktif	= $_POST['is_aang di upload salah";
						exit();
				}
				elseif (($ceknamaicon==NULL) && ($icon_size <= $maxfilesize)){
						// //echo $file_size;				
						if($_SERVER['REQUEST_METHOD'] == 'POST') {
									rename($_FILES['files']['tmp_name'], $upload_dir . $nama_file);
						}
				}
		}
		else{
			if(isset($_POST['hidimg'])){
				$icon_loc = $_POST['hidimg'];
			}
		}
		
		if(isset($icon_loc)){
			$datanya 	= Array(
									'prodi_id'=>$prodi_id,
									'fakultas_id'=>$fakultas_id, 
									'keterangan'=>$keterangan, 
									'strata'=>$strata, 
									'kode_prodi'=>$kode_prodi,
									'parent_id'=>$parent_id,
									'is_aktif'=>$is_aktif,
									'image'=>$icon_loc
									);
			$mprodi->replace_prodi($datanya);
			echo "Berhasil mengupdate!";
			exit();
		}
		else{
			$datanya 	= Array(
									'prodi_id'=>$prodi_id,
									'fakultas_id'=>$fakultas_id, 
									'keterangan'=>$keterangan, 
									'strata'=>$strata, 
									'kode_prodi'=>$kode_prodi,
									'parent_id'=>$parent_id,
									'is_aktif'=>$is_aktif
									);
			$mprodi->replace_prodi($datanya);
			echo "Berhasil mengupdate!";
			exit();
		}
		
		
		// $this->index('ok');
		// exit();
	}
	
	function tampilkan_parent()
	{
		$mprodi = new model_prodi();
		$id = $_POST['fakultas_id'];
		$parent = $mprodi->tampilkan_parent($id);
		echo "<option value='0'>Select Program Studi</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->id_parent."'>".$p->keterangan_parent."</option>" ;	
		}	
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	
}
?>