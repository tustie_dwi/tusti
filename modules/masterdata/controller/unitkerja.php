<?php
class masterdata_unitkerja extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}

	function index(){
		$munitkerja = new model_unitkerja();
		$data['parent'] = $munitkerja->read_parent();
		$data['child'] = $munitkerja->read_child();
		$data['fakultas'] = $munitkerja->get_fakultas();
	
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');	
		$this->add_script('js/unitkerja.js');	
		
		
		$this->view("unitkerja/index.php", $data);			
	}
	
	function write(){
		$munitkerja = new model_unitkerja();
		$data['posts'] = '';
		$data['fakultas'] = $munitkerja->get_fakultas();
		$data['parent'] = $munitkerja->read_parent();
		
		$this->add_script('js/unitkerja.js');	
		$this->view("unitkerja/edit.php", $data);
	}
	
	function edit($id){
		$munitkerja = new model_unitkerja();
		$data['posts'] = $munitkerja->get_unit_kerja();
		$data['unit'] = $munitkerja->get_unit_kerja($id);
		$data['parent'] = $munitkerja->read_parent();
		$data['child'] = $munitkerja->read_child();
		$data['fakultas'] = $munitkerja->get_fakultas();
		$data['unit_kerja'] = $munitkerja->get_unitByUnitId($id);
		$this->add_script('js/unitkerja.js');	
		
		$this->view("unitkerja/index.php", $data);
	}
	
	function get_unit(){
		$munit = new model_unitkerja();
		$data  = $munit->get_unitByFak($_POST['fakultasid']);
		if(isset($data)){
			echo "<option value='0'>Pilih Unit Kerja Parent</option>";
			foreach ($data as $key) {
				echo "<option value='" . $key->unit_id . "'>" . $key->keterangan . ' - ' . $key->kode . "</option>";
			}
		}
	}
	
	function del($id){
		$munitkerja = new model_unitkerja();
		$munitkerja->del_unitkerja($id);
		$this->redirect('module/masterdata/unitkerja');
	}
	
	function save(){
		$munitkerja = new model_unitkerja();
		$user_id = $this->coms->authenticatedUser->id;
		$last_update = date('Y-m-d H:i:s');
		$this->add_script('js/unitkerja.js');
		
		if($_POST['hidId'] != '') {
			$is_baru = 0;
		}
		else {
			$is_baru = 1;
		}
		
		if(isset($_POST['chklab'])):
			$is_lab = $_POST['chklab'];		
		else:
			$is_lab = '-';
		endif;
		
		
		$data = array(
			'unit_id' => $_POST['kode'],
			'fakultas_id' => $_POST['fakultas_id'],
			'keterangan' => $_POST['keterangan'],
			'kode' => $_POST['kode'],
			'kategori' => $is_lab,
			'parent_id' => $_POST['parent_id'],
			'user_id' => $user_id,
			'last_update' => $last_update
		);
		if($is_baru == 0) {
			if(! $munitkerja->replace_unitkerja($data)) echo "err";
			else echo "ok";
		}
		else {
			if(! $munitkerja->insert_unitkerja($data)) echo "err";
			else echo "ok";
		}
		// $this->redirect('module/masterdata/unitkerja/index/ok');
	}
	
}
?>