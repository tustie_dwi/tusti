<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-pencil-square"></i> Tambah Pengaturan Ruang Karyawan</div>
	<div class="panel-body">
		<form method="post" id="ruang_karyawan_form">
			<div class="form-group">
				<label class="control-label">Ruang</label>
				<div class="controls">
					<select class="form-control e9" name="ruang" id="ruang">
						<option value="0">Silahkan Pilih</option>
						<?php
							foreach($ruang as $row):
								echo "<option value='".$row->id."' ";
								echo ">".$row->keterangan."</option>";
							endforeach;
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">Karyawan</label>
				<div class="controls">
					<select class="form-control e9" name="karyawan[]" id="karyawan" multiple="multiple">
						<?php
							foreach($karyawan as $row):
								echo "<option value='".$row->karyawan_id."' ";
								echo ">".$row->gelar_awal." ".$row->nama." ".$row->gelar_akhir."</option>";
							endforeach;
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="controls">
					<input type="submit" value="Tambahkan" class="btn btn-primary">
					<input type="button" id="cancel-btn" onclick="location.href='<?php echo $this->location('module/masterdata/ruang/karyawan')?>'" value="Cancel" class="btn btn-danger">
				</div>
			</div>	
		</form>
	</div>	
</div>