<table class="table table-hover example">
<thead>
	<tr>							
		<th>&nbsp;</th>
		<th>&nbsp;</th>							
	</tr>
</thead>
<tbody>
<?php 	
if($posts){
foreach ($posts as $dt):  ?>
	<tr class="sett-<?php echo $dt->ruangid ?>-<?php echo $dt->karyawanid ?>">
		<td style="vertical-align: middle">
		<?php
			if($dt->foto){ $foto=$dt->foto; }else{ $foto='upload/foto/no_foto.png';} 
			?>
			
			<div class="media">
				<a class="pull-left" style="text-decoration:none" href="#" onClick = "#">
					<img class="media-object img-thumbnail" src="<?php echo $this->asset($foto); ?>"  width="50" height="50">
				</a>
			  <div class="media-body">
				<?php
				echo "<a href='#' class='text text-default'><b>".$dt->gelar_awal." ".ucWords(strToLower($dt->nama))." ".$dt->gelar_akhir."</b></a>
					<small><span class='text text-danger'>".$dt->email."</span></small>
					&nbsp;<span class='label label-success'>".$dt->home_base."</span>
						  <span class='label label-warning'>".$dt->cabang."</span><br>";
				echo "<span>".$dt->fakultas."</span><br>";
				echo $dt->nik. "<span class='label label-normal'>".ucWords($dt->alamat)."</span>";
				?>
			  </div>
			</div>
		</td>
		<td style="vertical-align: middle">            
			<ul class='nav nav-pills' style='margin:0;'>
				<li class='dropdown pull-right'>
				  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
				  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>									
					<li>
						<a onclick="doDelete('<?php echo $dt->ruangid ?>','<?php echo $dt->karyawanid ?>')" class='btn-edit-post' href="#"><i class='fa fa-trash-o'></i> Delete</a>	
					</li>
				  </ul>
				</li>
			</ul>
		</td>
	</tr>
	<?php endforeach; 
	}?>
</tbody>
</table>