<?php $this->head(); ?>

<h2 class="title-page">Ruang</h2>
 
<div class="row">
		<div class="col-md-8">	
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="<?php echo $this->location('module/masterdata/ruang'); ?>">Ruang</a></li>
			  <li class="active"><a href="#">Data</a></li>
			</ol>
				
			<?php
				
			 if( isset($posts) ) :	?>
             
                <div class="block-box">
				<table class='table table-hover' id='example' data-id='module/masterdata/ruang'>
						<thead>
							<tr>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
					
					<?php 
						$i = 1;
						if($posts > 0){
							foreach ($posts as $dt): 
					?>
						<tr id='post-<?php echo $dt->ruang_id ?>' valign=top>
							<td>
							<div class="col-md-6">
							<?php echo $dt->keterangan ?> <code><?php echo $dt->kode_ruang ?></code>&nbsp;<span class="label label-success"><?php echo $dt->fakultas_id ?></span>
							<span class="label label-danger"><?php echo $dt->cabang_id ?></span>&nbsp;<span class="label label-default"><?php echo ucWords($dt->kategori_ruang) ?></span>
							</div>
							<div class="col-md-3">
							<?php echo $dt->kapasitas ?>
							</div>
							<div class="col-md-3">
								<ul class='nav nav-pills pull-right'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
										<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/masterdata/ruang/edit/'.$dt->id) ?>"><i class='fa fa-edit'></i> Edit</a>	
										</li>
										<li>
											<a href="#" class="delete" data-kode="<?php echo $dt->kode_ruang ?>" data-ket="<?php echo $dt->keterangan ?>" data-id="<?php echo $dt->ruang_id ?>" data-uri="<?php echo $this->location('module/masterdata/ruang/delete/'.$dt->ruang_id) ?>">
												<i class='fa fa-times'></i> Delete
											</a>
										</li>
									  </ul>
									</li>
								</ul>
							</div>
							</td>
						</tr>
				<?php endforeach; } ?>
				</tbody></table>
                </div>
				
				
				
			 <?php else: ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="col-md-4">
			
				<?php echo $this->view("ruang/edit.php", $data); ?>
				
		</div>	
	</div>
<?php $this->foot(); ?>