<?php 
	if(isset($jenis_pend)&&$jenis_pend=="formal"){
		//echo "FORMAL";
		$hidid				= $edit_pend->formal_id;
		$jenjang			= $edit_pend->jenjang;
		$thn_lulus			= $edit_pend->tahun_lulus;
		$program_studi		= $edit_pend->program_studi;
		$latar_belakang_ilmu= $edit_pend->latar_belakang_ilmu;
		$nama_sekolah		= $edit_pend->nama_sekolah;
		$alamat_sekolah		= $edit_pend->alamat_sekolah;
		$document_id		= $edit_pend->document_id;
		$file_name			= $edit_pend->file_name;
	}
?>
<form method="post" name="form" id="form_pendidikan_formal" enctype="multipart/form-data">
		<div class="panel panel-default">
			<div class="panel-heading">Pendidikan Formal</div>
			<div class="panel-body ">
				<div class="form-group">
					<label class="control-label">Jenjang</label>
					<div class="controls">
						<select class="form-control e9" name="jenjang" id="jenjang">
							<option value="0">Silahkan Pilih</option>
							<option value="SD">SD/Sederajat</option>
							<option value="SMP">SMP/Sederajat</option>
							<option value="SMA">SMA/Sederajat</option>
							<option value="D3">Diploma [D3]</option>
							<option value="S1">Sarjana [S1]</option>
							<option value="S2">Magister [S2]</option>
							<option value="S3">Doktor [S3]</option>
						</select>
						<input type="hidden" name="jenjang_edit" value="<?php if(isset($jenjang)) echo $jenjang ?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label">Tahun Lulus</label>
					<input type="text" name="thn_lulus" id="thn_lulus" class="form-control" value="<?php if(isset($thn_lulus)) echo $thn_lulus ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Program Studi</label>
					<input type="text" name="prodi" id="prodi" class="form-control" value="<?php if(isset($program_studi)) echo $program_studi ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Latar Belakang Ilmu</label>
					<input type="text" name="background" id="background" class="form-control"value="<?php if(isset($latar_belakang_ilmu)) echo $latar_belakang_ilmu ?>" />
				</div>
				<div class="form-group">
					<label class="control-label">Nama Sekolah</label>
					<input type="text" name="nama_sekolah" id="nama_sekolah" class="form-control" value="<?php if(isset($nama_sekolah)) echo $nama_sekolah ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Alamat Sekolah</label>
					<input type="text" name="alamat_sekolah" id="alamat_sekolah" class="form-control" value="<?php if(isset($alamat_sekolah)) echo $alamat_sekolah ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Dokumen Pendukung</label><br>
					<button class="btn btn-info btn-xs select_library" data-toggle="modal" data-target="#media" onclick="view_content('0','<?php echo $id ?>')"><i class="fa fa-cloud-upload"></i> Select File</button>
					<div class="document-place">
						<?php
						if(isset($hidid)){
							if(isset($file_name)) echo $file_name 
						?>
							<input type="hidden" name="docid" value="<?php if(isset($document_id)) echo $document_id ?>" />
							&nbsp;<a href="javascript::" onclick="remove_selected()"><i class="fa fa-trash-o"></i></a>
						<?php
						}
						?>
						
					</div>
					<input type="hidden" name="document_formal" id="document_formal" class="form-control" value="<?php if(isset($document_id)) echo $document_id ?>"/>
				</div>
				<div class="form-group">
					<input type="hidden" name="hidid" id="hidid" value="<?php if(isset($hidid)) echo $hidid ?>"/>
					<input type="hidden" name="kar_id" id="kar_id" value="<?php echo $id ?>"/>
					<!-- <input type="text" name="doc_id" id="doc_id" value="<?php if(isset($document_id)) echo $document_id ?>"/> -->
					<button type="submit" class="btn btn-primary">Simpan</button>
					<input type="button" id="cancel-btn" onclick="location.href='<?php echo $this->location('module/masterdata/dosen/edit/'.$id )?>'" value="Batal" class="btn btn-danger">
				</div>
			</div>
		</div>
</form>