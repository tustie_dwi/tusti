<?php 
	if(isset($jenis_pend)&&$jenis_pend=="nonformal"){
		//echo "NONFORMAL";
		$hidid				= $edit_pend->non_formal_id;
		$nama_kegiatan		= $edit_pend->nama_kegiatan;
		$jenis_kegiatan		= $edit_pend->jeniskegiatan;
		$jenis_kegiatan_id	= $edit_pend->jenis_kegiatan;
		$penyelenggara		= $edit_pend->penyelenggara;
		$tgl_mulai			= $edit_pend->tgl_mulai;
		$tgl_selesai		= $edit_pend->tgl_selesai;
		$tempat_kegiatan	= $edit_pend->tempat_kegiatan;
		$kota_kegiatan		= $edit_pend->kota_kegiatan;
		$tingkat			= $edit_pend->tingkat;
		$sebagai			= $edit_pend->sebagai;
		$document_id		= $edit_pend->document_id;
		$file_name			= $edit_pend->file_name;
	}
?>
<form method="post" name="form" id="form_pendidikan_nonformal" enctype="multipart/form-data">
		<div class="panel panel-default">
			<div class="panel-heading">Pendidikan Non Formal</div>
			<div class="panel-body ">
				<div class="form-group">
					<label class="control-label">Nama Kegiatan</label>
					<input type="text" name="nama_kegiatan" id="nama_kegiatan" class="form-control" value="<?php if(isset($nama_kegiatan)) echo $nama_kegiatan ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Jenis Kegiatan</label>
					<input type="text" name="jenis_kegiatan" id="jenis_kegiatan" class="form-control typeahead" value="<?php if(isset($jenis_kegiatan)) echo $jenis_kegiatan ?>"/>
					<input type="hidden" name="jenis_kegiatan_id" class="form-control" value="<?php if(isset($jenis_kegiatan_id)) echo $jenis_kegiatan_id ?>">
				</div>
				<div class="form-group">
					<label class="control-label">Penyelenggara</label>
					<input type="text" name="penyelenggara" id="penyelenggara" class="form-control" value="<?php if(isset($penyelenggara)) echo $penyelenggara ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Tanggal Mulai</label>
					<input type="text" name="tgl_mulai" id="tgl_mulai" class="form_datetime form-control" value="<?php if(isset($tgl_mulai)) echo $tgl_mulai ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Tanggal Selesai</label>
					<input type="text" name="tgl_selesai" id="tgl_selesai" class="form_datetime form-control" value="<?php if(isset($tgl_selesai)) echo $tgl_selesai ?>"/>
				</div><br /><div class="form-group">
					<label class="control-label">Tempat Kegiatan</label>
					<input type="text" name="tmpt_kegiatan" id="tmpt_kegiatan" class="form-control" value="<?php if(isset($tempat_kegiatan)) echo $tempat_kegiatan ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Kota Kegiatan</label>
					<input type="text" name="kota_kegiatan" id="kota_kegiatan" class="form-control" value="<?php if(isset($kota_kegiatan)) echo $kota_kegiatan ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Tingkat</label>
					<select class="form-control e9" name="tingkat" id="tingkat">
						<option value="0">Silahkan Pilih</option>
						<option value="universitas">Universitas</option>
						<option value="kota">Kota</option>
						<option value="provinsi">Provinsi</option>
						<option value="nasional">Nasional</option>
						<option value="internasional">Internasional</option>
					</select>
					<input type="hidden" name="tingkat_edit" value="<?php if(isset($tingkat)) echo $tingkat ?>" />
				</div>
				<div class="form-group">
					<label class="control-label">Sebagai</label>
					<input type="text" name="sebagai" id="sebagai" class="form-control" value="<?php if(isset($sebagai)) echo $sebagai ?>"/>
				</div>
				<div class="form-group">
					<label class="control-label">Dokumen Pendukung</label><br>
					<button class="btn btn-info btn-xs" data-toggle="modal" data-target="#media" onclick="view_content('0','<?php echo $id ?>')"><i class="fa fa-cloud-upload"></i> Select File</button>
					<div class="document-place">
					<?php
					if(isset($hidid)){
						if(isset($file_name)) echo $file_name 
					?>
						<input type="hidden" name="docid" value="<?php if(isset($document_id)) echo $document_id ?>" />
						&nbsp;<a href="javascript::" onclick="remove_selected()"><i class="fa fa-trash-o"></i></a>
					<?php
					}
					?>
					</div>
					<input type="hidden" name="document_non_formal" id="document_non_formal" class="form-control" value="<?php if(isset($document_id)) echo $document_id ?>"/>
				</div>
				<div class="form-group">
					<input type="hidden" name="hidid" id="hidid" value="<?php if(isset($hidid)) echo $hidid ?>"/>
					<input type="hidden" name="kar_id" id="kar_id" value="<?php echo $id ?>"/>
					<!-- <input type="hidden" name="doc_id" class="doc_id"/> -->
					<button type="submit" class="btn btn-primary">Simpan</button>
					<input type="button" id="cancel-btn" onclick="location.href='<?php echo $this->location('module/masterdata/dosen/edit/'.$id )?>'" value="Batal" class="btn btn-danger">
				</div>
			</div>
		</div>
</form>