<?php $this->head();
	// foreach ($posts as $dt):
		$karyawan_id	= $posts->karyawan_id;
		$nik			= $posts->nik;
		$nama			= $posts->nama;
		$gelar_awal	 	= $posts->gelar_awal;
		$gelar_akhir	= $posts->gelar_akhir;
		$tgl_lahir		= $posts->tgl_lahir;
		$jenis_kelamin 	= $posts->jenis_kelamin;
		$telp			= $posts->telp;
		$hp				= $posts->hp;
		$is_status		= $posts->is_status;
		$alamat			= $posts->alamat;
		$email			= $posts->email;
		$fakultas_id	= $posts->fakultas_id;
		$cabang_id		= $posts->cabang_id;
		$biografi		= $posts->biografi;
		$is_nik			= $posts->is_nik;
		$is_aktif		= $posts->is_aktif;
		$home_base		= $posts->home_base;
		$is_tetap		= $posts->is_tetap;
		$foto			= $posts->foto;
		$cabangid		= $posts->cabang_id;
		
		$fakultas		= $posts->fakultas;
		$cabang			= $posts->cabang;
	// endforeach;
	
	$header="Detail ".ucwords($is_status);
?>
<div class="row">
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/masterdata/dosen'); ?>">Staff/Dosen</a></li>
	  <li class="active"><a href="#">Data</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/masterdata/dosen/write'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Staff/Dosen</a> 
    </div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="media">
				<img class="media-object  pull-left img-thumbnail" width="171" height="180" src='<?php echo $this->asset($foto) ?>' />				
				<div class="media-body">
					<h2><?php echo $nama; if($gelar_awal){echo ", ". $gelar_awal;} if($gelar_akhir){echo ", ".$gelar_akhir." ";}?>
						<small><span class="label label-default"><?php echo $is_status; ?></span></small>
					</h2><hr>
					<h4 style="padding-left: 15px;"><?php echo strtoupper($is_nik).". ".$nik; ?>
						<span class="label label-info"><?php echo $fakultas_id; ?></span>
						<span class="label label-success"><?php echo $cabang; ?></span>
					</h4>
					<div class="col-md-12">
						<i class="fa fa-home"></i> Alamat : <?php echo $alamat; ?><br>
						<i class="fa fa-calendar-o"></i> Tanggal Lahir : <?php echo $tgl_lahir; ?><br>
						<i class="fa fa-envelope-o"></i> Email : <?php echo $email; ?><br>
						<i class="fa fa-bullseye"></i> Status : <?php echo $is_aktif; ?><br>
						<i class="fa fa-mobile-phone"></i> Telp : <?php echo $telp."/".$hp; ?>
					</div>
					
				</div>
			</div>
			
			<div class="control-group">
				<h3>BIOGRAFI</h3>
				<?php echo $biografi; ?>
			</div>
			<br><br>
			<div class="well">
				<a class="btn-edit-post" href="<?php echo $this->location('module/masterdata/dosen/edit/'.$karyawan_id) ?>">
				<button type="button" class="btn btn-info"><i class="fa fa-edit"></i> Edit Data Dosen</button></a>
			</div>
	    </div>
    </div>
</div>
<?php $this->foot(); ?>