<?php 
	$hidId			= $posts->hid_id;
	$nama			= $posts->nama;
	$fakultas_id	= $posts->fakultas_id;
	$cabangid		= $posts->cabang_id;
	$golonganid		= $posts->golongan;
	$tgl_masuk		= $posts->tgl_masuk;
?>

<div class="row">
	
	<div class="col-md-12">
		<form method=post name="form-kenaikan" id="form-kenaikan">
		<div class="panel panel-default">
			<div class="panel-heading">Riwayat Kenaikan</div>
			<div class="panel-body ">
			  <div class="col-md-6">	
			   <div class="row">
			  		
				<!--<div class="form-group">
					<label class="control-label">&nbsp;</label>								
					<input name="nama" disabled class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">					
				</div>-->
				
				<!--<div class="form-group">
					<label class="control-label">Fakultas</label>								
					<select id="select_fakultas_kunit" class="e9 form-control" name="fakultas">
						<option value="-">Select Fakultas</option>
						<?php /*if(count($fakultas)> 0) {
							foreach($fakultas as $f) :
								echo "<option value='".$f->hid_id."' ";
								if($fakultas_id==$f->hid_id){
									echo "selected";
								}
								echo " >".$f->keterangan."</option>";
							endforeach;
						} ?>
					</select>
				</div>
				
				<div class="form-group">
					<label class="control-label">Cabang</label>								
					<select id="select_cabang_kunit" class="e9 form-control" name="cabang">
						<option value="-">Select Cabang</option>
						<?php if(count($cabang)> 0) {
							foreach($cabang as $c) :
								echo "<option value='".$c->cabang_id."' ";
								if($cabangid==$c->cabang_id){
									echo "selected";
								}
								echo " >".$c->keterangan."</option>";
							endforeach;
						} */?>
					</select>
				</div>-->
				
				<div class="form-group">
					<input name="tglmskstaff" disabled class="form-control" value="<?php if(isset($tgl_masuk))echo $tgl_masuk?>" type="hidden">
					
					<div class="controls">
					    <label class="radio-inline">
					     	<input name="jenis" value="jabatan" checked="" type="radio">Jabatan
					    </label>
					    <label class="radio-inline">
					     	<input name="jenis" value="kenaikan" type="radio">Lainnya
					    </label>
				    </div>
			    </div>
				
				<div class="form-group jenis-kenaikan">
					<label class="control-label">Jabatan</label>								
					<select id="select_jenis" class="e9 form-control" name="select_jenis">
						<option value="-">Select Jabatan</option>
						<?php foreach($datajabatan as $d) :
							echo "<option value='".$d->jabatan_id."' ";
							if(isset($jabatanid)&&$jabatanid==$d->jabatan_id){
								echo "selected";
							}
							echo " >".$d->keterangan."</option>";
						endforeach; ?>
					</select>
				</div>
				
				<div class="form-group">
					<label class="control-label">Nama Pangkat</label>								
					<select id="select_golongan" class="e9 form-control" name="select_golongan">
						<option value="-">Select Pangkat</option>
						<?php foreach($golongan as $d) :
							echo "<option value='".$d->golongan."' ";
							if(isset($golonganid)&&$golonganid==$d->golongan){
								echo "selected";
							}
							echo " >".$d->pangkat;	if($d->jabatan) echo " / ".$d->jabatan;
							echo " (".$d->golongan.")</option>";
						endforeach; ?>
					</select>
				</div>
				
				<div class="form-group">
					<label class="control-label">Tgl Efektif / TMT</label>								
					<input name="tmt" required="required" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
				</div>
				
				<div class="row">
					<div class="form-group col-md-6">
						<label class="control-label">Periode Mulai</label>								
						<input name="periodemulaikenaikan" required="required" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group col-md-6">
						<label class="control-label">Periode Selesai</label>								
						<input name="periodeselesaikenaikan" required="required" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
				</div>
				
				<div class="row">					
					<div class="form-group col-md-6">
						<label class="control-label periode-akhir">No SK</label>								
						<input name="nosk" required="required" class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group col-md-6">
						<label class="control-label">Tanggal SK</label>								
						<input name="tanggalsk" required="required" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label class="control-label periode-akhir">No Nota</label>								
						<input name="nonota" required="required" class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group col-md-6">
						<label class="control-label">Tanggal Nota</label>								
						<input name="tanggalnota" required="required" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-md-6">
						<label class="control-label periode-akhir">No STLUD</label>								
						<input name="nostlud" required="required" class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group col-md-6">
						<label class="control-label">Tanggal STLUD</label>								
						<input name="tanggalstlud" required="required" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
				</div>
			   </div>	
			  </div>
			  
			  <div class="col-md-6">
			   <!-- <div class="row"> -->
				
				<div class="form-group">
					<label class="control-label">&nbsp;</label>
				</div>
								
				<div class="form-group">
					<label class="control-label">Pejabat Penetap</label>								
					<input name="pejabat" required="required" class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">
				</div>
				
				<div class="form-group">
					<label class="control-label">Kredit</label>								
					<input name="kredit" required="required" class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">
				</div>
				
				<div class="form-group">
					<label class="control-label">Masa Kerja</label>		
					<fieldset class="form-inline">
						<div class="form-group col-md-6">
							<div class="row">
							    <div class="input-group">
							      <div class="input-group-addon">Tahun</div>
							     <input name="tahun-kerja" required="required" class="form-control" type="text">
							    </div>
						    </div>
					    </div>
					    <div class="form-group col-md-6">
					    	<div class="row">
							    <div class="input-group">
							      <div class="input-group-addon">Bulan</div>
							     <input name="bulan-kerja" required="required" class="form-control" type="text">
							    </div>
						    </div>
					    </div>
					</fieldset>
				</div>
				
				<div class="form-group">
					<label class="control-label">Keterangan</label>								
					<textarea name="keterangan" required="required" class="form-control"></textarea>
				</div>
				
				<div class="form-group">
					<label class="control-label">Dokumen Pendukung</label><br>
					<button class="btn btn-info btn-xs select_library" data-toggle="modal" data-target="#mediafile" onclick="view_content('0','<?php echo $id ?>')"><i class="fa fa-cloud-upload"></i> Select File</button>
					
					<div id="document-place">
						
					</div>
				</div>
				
				<div class="form-group checkbox">
					<label><input name="isaktif" value="1" type="checkbox"><b>Tidak Aktif</b></label>
					<span class="help-block help-isaktif">centang jika aktif</span>
				</div>
			
			   <!-- </div> -->
			  </div>
			  
				<div class="form-actions">
					<label class="control-label">&nbsp;</label>
					<input type="hidden" name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>">
					<input type="hidden" name="hidNaikId" value="<?php if(isset($hidNaikId))echo $hidNaikId; ?>">
					<a href="javascript::" id="cancel-kenaikan" class="btn btn-danger pull-right" style="display: none;margin-left: 10px;">Cancel</a>
					<input name="b_karyawankenaikan" id="b_karyawankenaikan" value=" Data Valid &amp; Save " class="btn btn-primary pull-right" type="submit">&nbsp;
				</div>
			</div>
		</div>
	</form>
	</div>
	
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">Kenaikan Karyawan</div>
			<div class="panel-body ">
				<?php if(isset($kenaikan)): ?>
				<table id="example" class='table table-hover'>
				<thead>
					<tr>
						<th>&nbsp;</th>	
					</tr>
				</thead>
				<tbody>
				<?php foreach ($kenaikan as $k) { ?>
				<tr>
					<td>
					  <div class="col-md-9">
					  	<?php check_TMT($k->tmt,'info'); ?>
					  	<br>
						Jenis Kenaikan : 
						<?php 
							echo ucwords($k->jenis); 
							if($k->jenis=='jabatan'){
								echo "&nbsp;".$k->keterangan_jabatan;
							}else{
								echo "&nbsp;".$k->golongan;
							} 
						?>
						<?php if($k->is_aktif!='1'){ ?>
						<span class="label label-info">*)</span>
						<?php } ?>
						<br>
						<?php echo $k->keterangan ?>
						<br>
						<button class="btn btn-info btn-xs" onclick="show_info('<?php echo $k->kenaikan_id ?>')"><i class="fa fa-plus"></i> Info</button>
						<span id="<?php echo $k->kenaikan_id ?>-info" style="display: none;" class="hidden">
						<blockquote style="margin-top: 10px;">
						tanggal SK : <?php echo str_replace('-', '/', $k->tgl_sk); ?><br>
						Periode Mulai : <?php echo str_replace('-', '/', $k->periode_mulai); ?><br>
						Periode Selesai : <?php echo str_replace('-', '/', $k->periode_selesai); ?><br>
						tmt : <?php check_TMT($k->tmt,'date'); ?><br>
						No SK : <?php echo $k->no_sk; ?><br>
						</blockquote>
						</span>
					  </div>
					  <div class="col-md-3">
						<ul class='nav nav-pills'>
							<li class='dropdown pull-right'>
							  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
							  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
								<li>
								<a class='btn-edit-post' onclick="edit_kenaikan('<?php echo $k->kenaikan_id ?>')" href="javascript::" ><i class='fa fa-edit'></i> Edit</a>	
								</li>
								<!-- <li>
								<a class='btn-edit-post' href="javascript::" onclick="delete_kenaikan('<?php echo $k->kenaikan_id ?>')"><i class='fa fa-trash-o'></i> Delete</a>	
								</li> -->
							  </ul>
							</li>
						</ul>
					  </div>
					</td>
				</tr>
				<?php } ?>
				</tbody>
				</table>
				<?php else:	?>
				<div class="well" align="center">No Data</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

</div>

<!-- Modal Folder-->
<div class="modal fade" id="mediafile" tabindex="-1" role="dialog" aria-labelledby="mediafile" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_btn" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="mediafile">Media Library</h4>
      </div>
      <div class="modal-body">
    	<div class="content-table">
			<div class="view_content">

			</div>
		</div>
		<div class="content-file"></div>	
      </div>
      <div class="modal-footer">
		<input type="hidden" name="parent" class="form-control" id="par" value="0"/>
        <button type="button" class="btn btn-default" id="home_button" onclick="view_content('0','<?php echo $id ?>');back('0')"><i class="fa fa-home"></i> Home</button>
        <button type="button" class="btn btn-primary close_btn" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php
function check_TMT($tmt,$param){
	$current = date("Y-m-d");
	
	$ts1 = strtotime($current);
	$ts2 = strtotime($tmt);
	
	$year1 = date('Y', $ts1);
	$year2 = date('Y', $ts2);
	
	$month1 = date('m', $ts1);
	$month2 = date('m', $ts2);
	
	$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
	
	if($diff=='1'){
		if($param=='date'){
			echo "<span class='label label-warning'>".str_replace('-', '/', $tmt)."</span>";
			echo "&nbsp;*) will be expired within 1 month";
		}else{
			echo "<span class='label label-warning'>tmt will be expired within 1 month</span>";
		}
		
	}
	elseif($diff=='0'||$diff<'0'){
		if($param=='date'){
			echo "<span class='label label-danger'>".str_replace('-', '/', $tmt)."</span>";
			echo "&nbsp;*) expired";
		}else{
			echo "<span class='label label-danger'>tmt expired</span>";
		}
		
	}
	else{
		if($param=='date'){
			echo str_replace('-', '/', $tmt);
		}
	}		
}
 ?>