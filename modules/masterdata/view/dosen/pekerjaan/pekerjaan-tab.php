
	<ul class="nav nav-tabs" role="tablist" id="pekerjaanTab">	 
		<li><a href="#absen" role="tab" data-toggle="tab">Rekap Absen</a></li>
		<li><a href="#tugas" role="tab" data-toggle="tab">Tugas</a></li>
		<li><a href="#aktifitas" role="tab" data-toggle="tab">Aktifitas</a></li>
		<li><a href="#prestasi" role="tab" data-toggle="tab">Prestasi/penghargaan</a></li>
		<!--<li><a href="#hukuman" role="tab" data-toggle="tab">Hukuman</a></li>-->
	</ul>	
	<div class="tab-content">	 
	  <div class="tab-pane" id="absen"><?php echo $this->view( 'dosen/pekerjaan/absen-tab.php', $data ); ?></div>
	  <div class="tab-pane" id="tugas"><?php echo $this->view( 'dosen/pekerjaan/tugas-tab.php', $data ); ?></div>
	  <div class="tab-pane" id="aktifitas"><?php echo $this->view( 'dosen/pekerjaan/aktifitas-tab.php', $data ); ?></div>
	  <div class="tab-pane" id="prestasi"><?php echo $this->view( 'dosen/pekerjaan/prestasi-tab.php', $data ); ?></div>
	  <div class="tab-pane" id="hukuman"><?php echo $this->view( 'dosen/pekerjaan/hukuman-tab.php', $data ); ?></div>   
	</div>
