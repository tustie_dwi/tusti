<?php 
	$hidId			= $posts->hid_id;
	$nama			= $posts->nama;
	$fakultas_id	= $posts->fakultas_id;
	$cabangid		= $posts->cabang_id;
	$golonganid		= $posts->golongan;
	$tgl_masuk		= $posts->tgl_masuk;
?>

<div class="row">	
	<div class="col-md-12">	
		<div class="panel panel-default">
			<div class="panel-heading">Permintaan Layanan</div>
			<div class="panel-body ">
				<table class="table example">
					<thead>
						<tr>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						
						$tugas = $mdosen->get_rekap_layanan($posts->hid_id);
						
						if($tugas):
							foreach($tugas as $dt):
								?>
								<tr>
									<td>
										<div class="col-md-8">
											<?php echo $dt->judul  ?>&nbsp;<span class="badge"><?php echo $dt->status ?></span><br>
											<small>
												<span class="text text-info"><i class="fa fa-clock-o"></i>&nbsp;<?php echo date("M d, Y", strtotime($dt->tgl_permintaan));?></span> &nbsp;
												<em>by</em> <i class='fa fa-user'></i>&nbsp;<?php echo $dt->req_dari;?>
											</small><br>
											<small>
												<span class="text text-danger">Deadline <i class='fa fa-clock-o'></i>&nbsp;<b><?php echo date("M d, Y", strtotime($dt->tgl_harapan_selesai)); ?></b></span>	
											</small>
										</div>
										<div class="col-md-4">
											<?php
											$progress = $mdosen->get_rekap_layanan_progress($dt->pelaksana_id);
											
											if($progress):
											?>
											<ul>
											<?php
												foreach ($progress as $key):
												?>
													<small><li><?php echo $key->judul ?>&nbsp;<span class="label label-danger"><?php echo $key->inf_progress ?>%</span><br>
													Start <i class="fa fa-clock-o"></i>&nbsp;<?php echo date("M d, Y", strtotime($key->tgl_mulai)) ?>, 
													<span class="text text-danger">Target <i class="fa fa-clock-o"></i>&nbsp;<?php echo date("M d, Y", strtotime($key->tgl_target_selesai)) ?>, </span>
													<span class="text text-info">Finish <i class="fa fa-clock-o"></i>&nbsp;<?php if($key->tgl_selesai=='0000-00-00') echo 'Not available'; else echo date("M d, Y", strtotime($key->tgl_selesai)) ?></span>
													
													</li></small>
													<?php
												endforeach;
												?>
											</ul>
												<?php
											endif;
											?>
										</div>
									</td>
								</tr>
								<?php
							endforeach;
						
						endif;						
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
