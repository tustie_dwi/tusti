<?php

if(isset($unit)){
	$header		= "Edit Unit Kerja";
	
	foreach ($unit as $dt):
		$id				= $dt->hidId;
		$fakultasid		= $dt->fakultas_id;
		$keterangan		= trim($dt->keterangan);
		$kode			= $dt->kode;
		$parentid		= $dt->parent_id;
		$kategori 		= $dt->kategori;
	endforeach;
}
else{
	$header		= "Write Unit Kerja";
	$id				= '';
	$fakultasid		= '';
	$keterangan		= '';
	$kode			= '';
	$parentid		= '';
	$kategori		= "-";
}

?> 
	
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil"></i> <?php echo $header ?></div>
	<div class="panel-body">		
			<form method="post" enctype="multipart/form-data" id="form-unitkerja">
				<?php $uri = $this->location('module/masterdata/unitkerja/get_unit') ?>
				<div class="form-group">	
					<label class="control-label">Fakultas</label>
					<select class="e9 form-control" name="fakultas_id" id="btn-fakultas" data-uri="<?php echo $uri ?>">
						<option value="0" data-uri='1'>Pilih Fakultas</option>
						<?php if(count($fakultas) > 0) {
							foreach($fakultas as $f) :
								echo "<option class='fakultas' value='".$f->fakultas_id."' ";
								if($fakultasid==$f->fakultas_id){
									echo "selected";
								}
								echo " >".$f->keterangan."</option>";
							endforeach;
						} ?>
					</select>					
				</div>
				
				<div class="form-group">
					<label class="control-label">Kode Unit Kerja</label>					
						<input type="text" name="kode" class="form-control" autocomplete="off" value="<?php echo $kode ?>"/>
					
				</div>
				
				<div class="form-group">	
					<label class="control-label">Unit Kerja Parent</label>					
					<select class="e9 form-control" name="parent_id" <?php if($parentid == '') echo "disabled"; ?>  id="btn-unit">
						<option value="0" data-uri='1'>Pilih Unit Kerja Parent</option>
						<?php foreach($unit_kerja as $key) { ?>
							<?php if($key->unit_id != $id && $id != $key->parent_id) { ?>
								<option <?php if($key->unit_id == $parentid) echo "selected"; ?> value="<?php echo $key->unit_id ?>"><?php echo $key->keterangan ?></option>
							<?php } ?>
						<?php } ?>
					</select>					
				</div>
				
				<div class="form-group">
					<label class="control-label">Keterangan</label>					
					<input type="text" name="keterangan" class="e9 form-control" autocomplete="off" value="<?php echo $keterangan ?>"/>					
				</div>
				
				<div class="checkbox">
					<label>
					  <input type="checkbox" name="chklab" value="laboratorium" <?php if($kategori=='laboratorium') echo "checked";?>> Laboratorium
					</label>
				  </div>
				
				<div class="form-group">
					<label class="control-label"></label>					
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input id="uri" value="<?php echo $this->location('module/masterdata/unitkerja/save'); ?>" type="hidden" >
						<a id="btn-unitkerja" class="btn btn-primary">Submit</a>					
				</div>
			</form>
		</div>
	</div>
