<?php $this->head();

if(isset($edit)){
	foreach ($edit as $e) {
		$hidId = $e->jeniskenaikanid;
		$ket = $e->keterangan;
	}
}else{
	$hidId = '';
	$ket = '';
}

?>

<style>
	.btn-edit{
		border : none;
		padding: 3px 5px;
		padding-left: 10px;
	}
</style>

<h2 class="title-page">Jenis Kenaikan</h2>
 
<div class="row">
		<div class="col-md-7">	
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="<?php echo $this->location('module/masterdata/jenis'); ?>">Jenis Kenaikan</a></li>
			</ol>
		
		<?php if(isset($posts)){ ?>
		<div class="panel panel-default">
		  	<div class="panel-body">
		  	
				<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>&nbsp;</th>	
					</tr>
				</thead>
				<tbody>
				<?php if($posts > 0){
					foreach ($posts as $dt): ?>
					<tr>
						<td>
						<div class="col-md-9">
							<?php echo $dt->keterangan ?>
						</div>
						<div class="col-md-3">
							<ul class='nav nav-pills'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/masterdata/jenis/index/'.$dt->jenis_kenaikan_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</div>
						</td>
					</tr>
				<?php endforeach; 
					 } ?>
				</tbody>
				</table>
			
			</div>
		</div>
		<?php }else{ ?>
		<div class="panel panel-default">
		  	<div class="panel-body">
				<div class="well" align="center">No Data Show.</div>
			</div>
		</div>
		<?php } ?>
		</div>
		
	<div class="col-sm-5">
	<div class="panel panel-default">
	 <div class="panel-heading"><i class="fa fa-pencil-square"></i> <?php echo $panel ?></div>
	  <div class="panel-body">
		
		<form id="form">
			
			<div class="form-group">
		      <label>Keterangan</label>
		      <input type="text" value="<?php echo $ket ?>" class="form-control" id="keterangan" name="keterangan" required="required">
		    </div>
			
			<div class="form-group">
			  <input type="hidden" name="hidId" value="<?php echo $hidId ?>" />
		      <input type="submit" class="btn btn-primary" name="b_save" value="Submit">
		      <?php if($panel=='Edit Jenis Kenaikan'){ ?>
		      <a class="btn btn-danger" href="<?php echo $this->location('module/masterdata/jenis'); ?>">Cancel</a>
		      <?php } ?>
		    </div>
					
		</form>
		
	  </div>
	 </div>
	</div>
		
	</div>
<?php $this->foot(); ?>