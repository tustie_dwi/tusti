<?php $this -> head(); ?>
<fieldset>
	<legend>
		<a href="<?php echo $this -> location('module/akademik/prodi/write'); ?>" class="btn btn-primary pull-right"> <i class="icon-pencil icon-white"></i> New Prodi</a> Prodi List
	</legend>

	<?php

if(isset($status) and $status) :
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">
			&times;
		</button>
		<?php echo $statusmsg; ?>
	</div>
	<?php endif;

		if( isset($posts) ) :
		$str="<table class='table table-hover' id='example'>
		<thead>
		<tr>
		<th>No</th>
		<th>Strata</th>
		<th>Kode</th>
		<th>Program Studi</th>
		<th>Fakultas</th>
		<th>Act</th>
		</tr>
		</thead>
		<tbody>";

		$i = 1;
		if($posts > 0){
		foreach ($posts as $dt):
		if ($dt->is_aktif==1){
		$aktif = "Aktif";
		}
		else{
		$aktif = "Tidak Aktif";
		}

		$str.=	"<tr id='post-".$dt->prodi_id."' data-id='".$dt->prodi_id."' valign=top>
		<td>".$i++."</td>
		<td>".$dt->strata."</td>
		<td>".$dt->kode_prodi."</td>
		<td>".$dt->keterangan."&nbsp;<span class='label pull-right label-info' style='text-align:center;width:150px;font-weight:normal;'></span>
		<span class='label label-success'>".$aktif."</span>
		";
		if($dt->parent_id){
		$str.= "<br><code>Sub Prodi dari ".$dt->parent_keterangan."</code>";
		}
		$str.= "</td>
		<td>".$dt->fakultas_keterangan."</td>";

		$str.= "<td style='min-width: 80px;'>
		<ul class='nav nav-pills' style='margin:0;'>
		<li class='dropdown'>
		<a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
		<ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
		<li>
		<a class='btn-edit-post' href=".$this->location('module/akademik/prodi/edit/'.$dt->prodi_id)."><i class='icon-edit'></i> Edit</a>
		</li>
		</ul>
		</li>
		</ul>
		</td></tr>";
		endforeach;
		}
		$str.= "</tbody></table>";

		echo $str;

		else:
	?>
	<div class="span3" align="center" style="margin-top:20px;">
		<div class="well">
			Sorry, no content to show
		</div>
	</div>
	<?php endif; ?>
</fieldset>

	<?php if( isset($posts) ) : 
		 
	$str = "<div class='tree'>";
	
	$i = 1;
		if($posts > 0){
		foreach ($posts as $dt):
		$str .= "<ul>
					<li><span><i class='icon-minus-sign'></i>".$dt->fakultas_keterangan."</span><a href=''></a>
						<ul>
							<li>
								<span>".$dt->keterangan."</span><a href=''></a>
							</li>
						</ul>
					</li>
				</ul>";
		endforeach;
		}
		$str .= "</div>";
	echo $str;
	else:
	?>
	<div class="span3" align="center" style="margin-top:20px;">
		<div class="well">
			Sorry, no content to show
		</div>
	</div>
	<?php endif; ?>
	
	<?php echo $menu; ?>

<?php $this -> foot(); ?>