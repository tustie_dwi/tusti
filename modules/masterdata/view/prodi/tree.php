<?php $this->head(); ?>

<h2 class="title-page">Program Studi</h2>
<div class="row">
	<div class="col-md-8">
			<ol class="breadcrumb">
				  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
				  <li><a href="<?php echo $this->location('module/masterdata/prodi'); ?>">Program Studi</a></li>
				  <li class="active"><a href="#">Data</a></li>
			</ol>
			 <?php
			 
			 if(isset($status) and $status) : ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<?php echo $statusmsg; ?>
				</div>
			<?php 
			endif; 
			?>
			
			<?php
				function get_prodi($fak_id, $prodi, $uri){
					echo "<ul>";
					foreach ($prodi as $key) {
						if($fak_id == $key->fakultas_id && $key->parent_id == '0'){
							echo "
							<li>
								<span><i class='fa fa-minus-square'></i> " . $key->keterangan . "</span>
								<a href=''></a>
								<div class='btn-group'>
									    <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
									      Action
									      <span class='fa fa-caret-down'></span>
									    </button>
									    <ul class='dropdown-menu'>
											<li>
												<a class='' href='".$uri."/".$key->id."'><i class='fa fa-edit'></i> Edit</a>
											</li>
										</ul>
								</div>
							";
							get_child($key->prodi_id, $prodi,$uri);
						}	
					}
					echo "</ul>";
		
				}
				
				function get_child($prodi_id, $prodi, $uri){
					echo "<ul>";
					foreach ($prodi as $key) {
						if($prodi_id == $key->parent_id && $key->parent_id != '0')
						echo "<li>
								<span>".$key->keterangan."</span>
								<a href=''></a>
								<div class='btn-group'>
									    <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
									      Action
									      <span class='fa fa-caret-down'></span>
									    </button>
									    <ul class='dropdown-menu'>
											<li>
												<a class='' href='".$uri."/".$key->id."'><i class='fa fa-edit'></i> Edit</a>
											</li>
										</ul>
								</div>
							  ";
					}
					echo "</ul>";
				}
			?>
			
			<div class="tree">
				<?php
					$uri = $this->location('module/masterdata/prodi/index');
					echo "<ul>";
					foreach ($fak as $key) {
						echo "<li><span><i class='fa fa-minus-square'></i> " . $key->keterangan . "</span><a href=''></a>";
						get_prodi($key->hid_id, $prodi, $uri);
					}
					echo "</ul>";
				?>
			</div>
	</div>
	<div class="col-md-4">
			
			<?php 
			echo $this->view("prodi/edit.php", $data); ?>
				
	</div>	
</div>
	 
<?php $this->foot(); ?>