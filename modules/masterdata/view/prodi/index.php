<?php $this->head(); ?>
<div class="row">
	<div class="col-md-8"> <!--INDEX-->
		<fieldset>
			<legend><a href="<?php echo $this->location('module/masterdata/prodi/write'); ?>" class="btn btn-primary pull-right">
		    <i class="icon-pencil icon-white"></i> New Prodi</a> Prodi List
			</legend>
			
			 <?php
			 
			 if(isset($status) and $status) : ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<?php echo $statusmsg; ?>
				</div>
			<?php 
			endif; 
			
			 if( isset($posts) ) :	 ?>
				<table class='table table-hover' id='example'>
						<thead>
							<tr>
								<th>No</th>
								<th>Strata</th>
								<th>Kode</th>
								<th>Program Studi</th>
								<th>Fakultas</th>
								<th>Act</th>
							</tr>
						</thead>
						<tbody>
				
				<?php 
					$i = 1;
					if($posts > 0){
						foreach ($posts as $dt): 
							if ($dt->is_aktif==1){
								$aktif = "Aktif";
							}
							else{
								$aktif = "Tidak Aktif";
							} ?>
							
							<tr id='post-".$dt->prodi_id."' data-id='".$dt->prodi_id."' valign=top>
										<td><?php echo $i++ ?></td>
										<td><?php echo $dt->strata ?></td>
										<td><?php echo $dt->kode_prodi ?></td>
										<td><?php echo $dt->keterangan ?>&nbsp;<span class='label pull-right label-info' style='text-align:center;width:150px;font-weight:normal;'></span>
										<span class='label label-success'><?php echo $aktif ?></span>
									<?php
											if($dt->parent_id){ ?>
												<br><code>Sub Prodi dari <?php echo $dt->parent_keterangan ?></code>
									<?php
											} ?>
										</td>
										<td><?php echo $dt->fakultas_keterangan ?></td>
												
										<td>            
											<ul class='nav nav-pills' style='margin:0;'>
												<li class='dropdown'>
												  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
												  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
													<li>
													<a class='btn-edit-post' href="<?php echo $this->location('module/masterdata/prodi/edit/'.$dt->prodi_id)?>"><i class='fa fa-edit'></i> Edit</a>	
													</li>
												  </ul>
												</li>
											</ul>
										</td>
							</tr>
						 <?php 
						 endforeach;
					 } ?>
					</tbody>
				</table>
			<?php
			 else: 
			 ?>
		    <div class="span3" align="center" style="margin-top:20px;">
			    <div class="well">Sorry, no content to show</div>
		    </div>
		    <?php endif; ?>
		</fieldset>
	</div>
	
	<div class="col-md-4"> <!--FORM-->
			
		<?php echo $this->view("prodi/edit.php", $data); ?>
				
	</div>
</div>
<?php $this->foot(); ?>