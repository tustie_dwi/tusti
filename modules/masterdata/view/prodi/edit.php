<?php 

if (isset($posts)) {
	$header = "Edit Prodi";

	foreach ($posts as $dt) :
		$fakultas_id = $dt -> fakultas_id;
		$keterangan = $dt -> keterangan;
		$strata = $dt -> strata;
		$kode_prodi = $dt -> kode_prodi;
		$parent_id = $dt -> parent_id;
		$is_aktif = $dt -> is_aktif;
		$image = $dt -> image;
		$id = $dt -> hid_id;
		$ceknew = 0;
	endforeach;
	$frmact = $this -> location('module/akademik/prodi/save');

} else {
	$header = "Write New Prodi";
	$id = "";
	$fakultas_id = "";
	$keterangan = "";
	$strata = "";
	$kode_prodi = "";
	$parent_id = "";
	$is_aktif = 0;
	$image = "";
	$ceknew = 1;
	$frmact = $this -> location('module/akademik/prodi/save');
}
?>

<div>

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil"></i> <?php echo $header ?></div>
	<div class="panel-body">
			<form method="post" id="form-prodi">
				<div class="form-group">
					<label for="kodeProdi" class="control-label">Fakultas</label>
	
						<?php $uri_parent = $this -> location('module/masterdata/prodi/tampilkan_parent'); ?>
						<?php echo '<select class="form-control" id="select_fakultas" name="select_fakultas" data-uri="' . $uri_parent . '">'; ?>
						<option value="0">Select Fakultas</option>
						<?php
						if (count($fakultas) > 0) {
							foreach ($fakultas as $dt) :
								echo "<option value='" . $dt -> fakultas_id . "' ";
								if ($fakultas_id == $dt -> fakultas_id) {
									echo "selected";
								}
								echo ">" . $dt -> keterangan . "</option>";
							endforeach;
						}
						?>
						</select>
				</div>
				
				<div class="form-group">
					 <label for="kodeProdi" class="control-label">Strata</label>
						<select id="strata" name="strata" class="form-control">
							<option value="0" <?php if($strata=="0") echo "selected"; ?> >Pilih Strata</option>
							<option value="S1" <?php if($strata=="S1") echo "selected"; ?> >Sarjana</option>
							<option value="S2" <?php if($strata=="S2") echo "selected"; ?> >Magister</option>
							<option value="S3" <?php if($strata=="S3") echo "selected"; ?> >Doktor</option>
							<option value="Vokasi" <?php if($strata=="Vokasi") echo "selected"; ?> >Diploma</option>
							<option value="SPKD" <?php if($strata=="SPKD") echo "selected"; ?> >Seleksi Program Khusus Penyandang Disabilitas</option>
						</select>
				</div>
				
				<div class="form-group">
					<label for="kodeProdi" class="control-label">Program Studi</label>
						<select id="select_parent" class="form-control" name="select_parent" disabled="disabled">
							<option value="0">Select Program Studi</option>
							<?php
							if (count($prodi) > 0) {
								foreach ($prodi as $dt) :
									echo "<option value='" . $dt -> parent_id . "' ";
									if ($parent_id == $dt -> parent_id) {
										echo "selected";
									}
									echo ">" . $dt -> keterangan . "</option>";
								endforeach;
							}
							?>
						</select>
						<?php if(isset($parent_id)&& $parent_id!=""){?>
							<input type="hidden" name="select_parent" value="<?php echo $parent_id ?>"/>
						<?php } ?>
				</div>
							
				<div class="form-group">
					 <label for="kodeProdi" class="control-label">Kode Prodi</label>
						<input required="required" type=text id="kodeProdi" class="form-control" id="kode_prodi" name="kode_prodi" value="<?php echo $kode_prodi; ?>">
				</div>
				
				<div class="form-group">
					 <label for="kodeProdi" class="control-label">Program Studi</label>
						<input required="required" type=text  class="form-control" name="keterangan" id="keterangan" value="<?php echo $keterangan; ?>">
				</div>
				
				<div class="form-group">
					 <label class="control-label" for="content_title">Foto</label>
					 <div class="controls">                    
						<input id="files" name="files" type="file">
						<output id="list"></output>
						<?php 
							if($ceknew == 0){
								if(isset($image)){ ?>
									<div class='well'>
										<img style="width: 100px; height: auto;" src="<?php echo $this->asset($image); ?>"/>
										<input name="hidimg" value="<?php echo $image ?>" type="hidden">
									</div>
								<?php } else { ?>
									<div class='well'>
										<p>Icon Belum Tersedia</p>
									</div>
								<?php } 
							}
						?>
					</div>       
				</div>
			
				<div class="form-group">
					<label for="kodeProdi" class="control-label">Is Aktif ?</label>
						<label class="checkbox">
							<input type="checkbox" name="is_aktif" id="is_aktif" value="1" <?php
							if ($is_aktif == 1) { echo "checked";
							}
							?>>
							Ya</label>
						<br>
						<input type="hidden" name="hidId" value="<?php echo $id; ?>">
						<input type="hidden" name="ceknew" value="<?php echo $ceknew; ?>">
						<!-- <input type="submit" name="b_prodi" id="submit" value="Submit" class="btn btn-primary"> -->
						<input type="submit" class="btn btn-primary" name="b_prodi" id="submit" data-value="1" value="Publish">
						<a href="<?php echo $this->location("module/masterdata/prodi"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>
				</div>

			</form>
		</div>
</div>