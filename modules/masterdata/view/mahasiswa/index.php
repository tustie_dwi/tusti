<?php $this->head(); 
$header="Daftar Mahasiswa";?>
<div class="row">
	 <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="#">Kemahasiswaan</a></li>
	  <li class="active"><a href="#"><?php echo $header;?></a></li>
	  
	</ol>
	<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location("module/masterdata/mhs/write"); ?>" class="btn btn-primary pull-right">
		<i class="fa fa-pencil"></i > New Mahasiswa</a>
    </div>
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :
		 ?>
		 <div class="col-md-5">
		 	<div class="form-group">	
				<label>Fakultas</label>			
				<?php if($fakultas_id != '-'){
						   echo '<select class="form-control e9" name="fakultas" id="select_indexfakultas" disabled>';
					  }
	 				  else echo '<select class="form-control e9" name="fakultas" id="select_indexfakultas">';
				?>
					<option class="sub_01" value="0">Select Fakultas</option>			
					<?php						
						foreach($fakultas as $dt):
							echo "<option class='sub_".$dt->fakultasid."' value='".$dt->hid_id."' ";
							if($fakultas_id==$dt->hid_id){
								echo "selected";
							}
							echo ">".$dt->keterangan."</option>";
						endforeach;
					?>
				</select>				
			</div>
		 	<div class="form-group">	
				<label>Cabang</label>
				<?php $uri_parent = $this->location('module/masterdata/mhs/tampilkan_indexangkatan'); ?>
				<?php echo '<select class="form-control e9" name="cabang" id="select_indexcabang" data-uri="'.$uri_parent.'">' ?>
					<option class="sub_01" value="0">Select Cabang</option>			
					<?php
														
						foreach($cabang as $dt):
							echo "<option class='sub_".$dt->cabang_id."' value='".$dt->cabang_id."' ";
							echo ">".$dt->keterangan."</option>";
						endforeach;
					?>
				</select>				
			</div>
			<div class="form-group">	
				<label>Angkatan</label>				
				<?php $uri_parent = $this->location('module/masterdata/mhs/tampilkan_index'); ?>
					<?php echo '<select class="form-control e9" name="select_thn" id="select_indexangkatan" disabled data-uri="'.$uri_parent.'">' ?>
					<option value="0">Select Angkatan</option>
				</select>					
			</div>
		</div>
		<div class="col-md-7" id="display"></div>
	 <?php
	 else: 
	 ?>
		<div class="well">Sorry, no content to show</div>
    <?php endif; ?>
</div>
<?php $this->foot(); ?>