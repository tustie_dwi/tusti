<?php
class model_conf extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	/* master konfigurasi */
	
	
	
	
	/* master semester aktif */
	function get_semester_aktif(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					WHERE
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` =  '1'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	/* master semester aktif */
	function get_semester(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					ORDER BY `db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik` DESC
					";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	/* master program studi atau jurusan*/
	function get_prodi($term=NULL){
		$sql= "SELECT prodi_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_prodi` WHERE keterangan like '%".$term."%' ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master unit kerja atau struktur*/
	function get_unit($term=NULL){
		$sql= "SELECT unit_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_unitkerja` WHERE keterangan like '%".$term."%'  ORDER BY jenis ASC, nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	/*master mahasiswa */
	function get_mahasiswa($term=NULL){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `value` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_json_mhs($term=NULL){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `text` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master dosen */
	function get_dosen($term=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_status='dosen' AND is_aktif <> 'keluar' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master dosen */
	function get_staff($term=NULL){
		$sql= "SELECT nama as `tag` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_aktif <> 'keluar' 
				ORDER BY nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master dosen */
	function get_staff_tag($term=NULL){
		$sql= "SELECT karyawan_id as `value`, nama as `text` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_aktif <> 'keluar' 
				ORDER BY nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_mhs($term=NULL){
		$sql= "SELECT nama as `tag` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master pengampu */
	function get_pengampu($term=NULL){
		$sql= "SELECT nama as `value`, karyawan_id as `id` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_status='dosen' AND is_aktif <> 'keluar' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master nama mk */
	function get_namamk($term=NULL){
		$sql= "SELECT namamk_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_namamk` 
					WHERE keterangan like '%".$term."%' 
				ORDER BY keterangan ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master mk */
	function get_mk($term=NULL){
		$sql= "SELECT `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` as `id`, 
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ', `db_ptiik_apps`.`tbl_namamk`.`keterangan`,'(',`db_ptiik_apps`.`tbl_matakuliah`.`sks`,' sks)') as `value` FROM `db_ptiik_apps`.`tbl_matakuliah`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id` 
					WHERE concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
				ORDER BY `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/* master jam mulai*/
	function  get_jam_mulai($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_mulai as `value` FROM db_ptiik_apps.tbl_jam 
				WHERE jam_mulai like '%".$term."%'
				ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jam selesai*/
	function  get_jam_selesai($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_selesai as `value` FROM db_ptiik_apps.tbl_jam  WHERE jam_selesai like '%".$term."%' ORDER BY jam_selesai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master hari*/
	function  get_hari($term=NULL){
		$sql = "SELECT hari as `id`, hari as `value` FROM db_ptiik_apps.tbl_hari WHERE hari like '%".$term."%' ORDER BY db_ptiik_apps.tbl_hari.id ASC";
		$result = $this->db->query( $sql );
		
		//echo $sql;
		
		return $result;	
	}
	
	/* master ruang*/
	function  get_ruang($term=NULL){
		$sql = "SELECT ruang as `id`, concat(ruang, ' - ', keterangan) as `value` FROM db_ptiik_apps.tbl_ruang WHERE db_ptiik_apps.tbl_ruang.keterangan like '%".$term."%' 
				ORDER BY db_ptiik_apps.tbl_ruang.ruang ASC";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	/* master kategori ruang*/
	function  get_kategori_ruang($term=NULL){
		$sql = "SELECT kategori_ruang as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_kategoriruang WHERE keterangan like '%".$term."%' ORDER BY kategori_ruang ASC";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	
	/* master kelas*/
	function  get_kelas($term=NULL){
		$sql = "SELECT kelas_id as `id`, kelas_id as `value` FROM db_ptiik_apps.tbl_kelas WHERE keterangan like '%".$term."%' ORDER BY kelas_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master nama dosen */
	function get_nama_dosen($id){
		$sql= "SELECT nama , karyawan_id ,nik FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE karyawan_id = '".$id."' ";
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
	/*master id dosen */
	function get_id_dosen($str=NULL){
		$sql= "SELECT karyawan_id FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE nama like '".$str."%' ";
		$result = $this->db->query( $sql );
		
		
		
		return $result;	
		
	}
	
	/*master jadwal dosen */
	function get_jadwal_dosen(){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`
				FROM
				`db_ptiik_apps`.`vw_jadwaldosen`
				";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk ditawarkan */
	function get_mkditawarkan($ispraktikum,$semester,$term){
		$sql="SELECT
				`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` as `id`,
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ' ,`db_ptiik_apps`.`tbl_namamk`.`keterangan`) as `value`			
				FROM
				`db_ptiik_apps`.`tbl_mkditawarkan`
				Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
				Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`		
			  WHERE concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
			";
			
		if($ispraktikum!=""){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`is_praktikum`= '".$ispraktikum."' ";
		}
		if($semester!=""){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` ='".$semester."' ";
		}
				
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jenis kegiatan*/
	function  get_jenis_kegiatan($kategori=NULL, $str=NULL){
		$sql = "SELECT jenis_kegiatan_id as `id`, 
					   keterangan as `value` 
				FROM db_ptiik_apps.tbl_jeniskegiatan
				WHERE 1";
		
		if($kategori){
			$sql .= " AND kategori = '".$kategori."'";
		}
		
		if($str){
			$sql .= " AND keterangan LIKE '%".$str."%'";
		}
		
		$sql .= "ORDER BY keterangan ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function replace_conf($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_config',$datanya);
	}
	
	function replace_semester($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tahunakademik',$datanya);
	}
	
	function update_conf($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_config',$datanya,$idnya);
	}
		
	function update_semester($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_tahunakademik',$datanya,$idnya);
	}
	
	public function log($tablename, $datanya, $username, $action){
		$return_arr = array();
		array_push($return_arr,$datanya);		
			
		$data['user']		= $username;
		$data['session']	= session_id();
		$data['tgl']		= date("Y-m-d H:i:s");
		$data['reference']	= $_SERVER['HTTP_REFERER'];
		$data['table_name']	= $tablename;
		$data['field']		= json_encode($return_arr);	
		$data['action']		= $action;			
		
		$result = $this->db->insert("coms`.`coms_log", $data);
		
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) return false;
	}
	
	function get_fakultas(){
		$sql = "SELECT mid(md5(fakultas_id),6,6) as fakultasid, keterangan, fakultas_id as hid_id
				FROM `db_ptiik_apps`.`tbl_fakultas`";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_cabangub(){
		$sql = "SELECT `cabang_id`, `keterangan`
				FROM `db_ptiik_apps`.`tbl_cabang` 
				WHERE 1
				";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_prodiub($fakultas_id=NULL,$cabang_id=NULL){
		$sql = "SELECT tbl_prodi.`prodi_id`, tbl_prodi.`keterangan`
				FROM `db_ptiik_apps`.`tbl_prodi`
				LEFT JOIN  `db_ptiik_apps`.`tbl_mahasiswa`
					ON tbl_mahasiswa.prodi_id = tbl_prodi.prodi_id
				WHERE 1
				";
		if($fakultas_id){
			$sql .= " AND tbl_prodi.fakultas_id = '".$fakultas_id."'";
		}
		if($cabang_id){
			$sql .= " AND tbl_mahasiswa.cabang_id = '".$cabang_id."'";
		}
		$sql .= " GROUP BY tbl_prodi.`prodi_id`";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_karyawan_id_by_md5($kar_id=NULL){
		$sql = "SELECT tbl_karyawan.karyawan_id as id
				FROM `db_ptiik_apps`.`tbl_karyawan`
				WHERE mid(md5(tbl_karyawan.karyawan_id),6,6) = '".$kar_id."'
				";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
}