<?php
class model_ruang extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
		
	public function read($id=NULL,$fakultas=NULL,$cabang=NULL){
		$sql = "SELECT 	mid(md5(ruang_id),6,6) as `id`,
						ruang_id,
						keterangan,
						kode_ruang,
						fakultas_id, cabang_id, kategori_ruang, 
						kapasitas
				FROM `db_ptiik_apps`.`tbl_ruang` WHERE 1 ";
		if($fakultas!=""){
			$sql .= " AND fakultas_id = '" . $fakultas ."'";
		}
		if($cabang!=""){
			$sql .= " AND cabang_id = '" . $cabang ."'";
		}
		if($id!="") {
			$sql .= " AND mid(md5(ruang_id),6,6) = '" . $id ."'";
			$result = $this->db->getRow( $sql );
			return $result;
		}
		$sql .= " ORDER by keterangan";
		$result = $this->db->query( $sql );	
		// echo $sql;
		return $result;	
	}
	
	public function get_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(ruang_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_ruang`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	public function replace_ruang($data){
		$this->db->replace('db_ptiik_apps`.`tbl_ruang',$data);
	}
	
	public function delete($id){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_ruang` WHERE mid(md5(`ruang_id`),6,6) = '" . $id ."'";
		$this->db->query($sql);
	}
	
	
	//=====Karyawan Ruang=====//
	function read_karyawan_ruang($ruang=NULL,$karyawan=NULL){
		$sql = "SELECT mid(md5(`tbl_karyawan_ruang`.`karyawan_id`),6,6) as karyawanid,
					   mid(md5(`tbl_karyawan_ruang`.`ruang_id`),6,6) as ruangid,
					   `tbl_karyawan`.`nik`,
					   `tbl_karyawan`.`nama`,
					   `tbl_karyawan`.`gelar_awal`,
					   `tbl_karyawan`.`gelar_akhir`,
					   CONCAT(`tbl_karyawan`.`gelar_awal`, ' ',  `tbl_karyawan`.`nama`, ' ', `tbl_karyawan`.`gelar_akhir`) as gelar_lengkap,
					   `tbl_karyawan`.`is_status`,
					   `tbl_karyawan`.`alamat`,
					   `tbl_karyawan`.`email`,
					   `tbl_karyawan`.`golongan`,
					   `tbl_karyawan`.`pendidikan_terakhir`,
					   `tbl_karyawan`.`is_nik`,
					   `tbl_karyawan`.`home_base`,
					   `tbl_karyawan`.`is_tetap`,
					   `tbl_karyawan`.`is_aktif`,
					   `tbl_karyawan`.`foto`,
		               `tbl_fakultas`.`keterangan` as fakultas,
		               `tbl_cabang`.`keterangan` as cabang
				FROM `db_ptiik_apps`.`tbl_karyawan_ruang`
				LEFT JOIN `db_ptiik_apps`.`tbl_karyawan` ON tbl_karyawan_ruang.karyawan_id = tbl_karyawan.karyawan_id
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` ON `tbl_fakultas`.`fakultas_id` = `tbl_karyawan`.`fakultas_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_cabang` ON `tbl_cabang`.`cabang_id` = `tbl_karyawan`.`cabang_id`
				WHERE 1 ";
		if($ruang!=""){
			$sql .= " AND mid(md5(tbl_karyawan_ruang.ruang_id),6,6) = '".$ruang."'";
		}
		if($karyawan!=""){
			$sql .= " AND mid(md5(tbl_karyawan_ruang.karyawan_id),6,6) = '".$karyawan."'";
			$result = $this->db->getRow( $sql );
			//echo $sql;
			return $result;
		}
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function cek_data($kar_id=NULL,$ruang_id=NULL){
		$sql = "SELECT karyawan_id 
				FROM `db_ptiik_apps`.`tbl_karyawan_ruang`
				WHERE karyawan_id = '".$kar_id."' AND ruang_id = '".$ruang_id."'"; 
		$result = $this->db->getRow( $sql );
		//echo $sql;
		return $result;
	}
	
	function replace_karyawan_ruang($data){
		$this->db->replace('db_ptiik_apps`.`tbl_karyawan_ruang',$data);
	}
	
	function delete_karyawan_ruang($ruang=NULL,$karyawan=NULL){
		$sql	= "DELETE FROM db_ptiik_apps.tbl_karyawan_ruang
				   WHERE mid(md5(tbl_karyawan_ruang.ruang_id),6,6) = '".$ruang."' AND mid(md5(tbl_karyawan_ruang.karyawan_id),6,6) = '".$karyawan."'";
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;
	}
}
?>