<?php
class model_jenis extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT 
				mid(md5(`tbl_master_jenis_kenaikan`.`jenis_kenaikan_id`),6,6) as jenis_kenaikan_id,
				tbl_master_jenis_kenaikan.jenis_kenaikan_id as jeniskenaikanid,
				tbl_master_jenis_kenaikan.keterangan
				FROM `db_ptiik_apps`.`tbl_master_jenis_kenaikan` 
				WHERE 1";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_master_jenis_kenaikan`.`jenis_kenaikan_id`),6,6)='".$id."' ";
		}
		
		$result = $this->db->query( $sql );	
		return $result;	
		
		
	}

	function get_reg_number(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jenis_kenaikan_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_master_jenis_kenaikan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_jenis($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_master_jenis_kenaikan',$datanya);
	}
	
}
?>