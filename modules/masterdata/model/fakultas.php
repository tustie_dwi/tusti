<?php
class model_fakultas extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT mid(md5(`tbl_fakultas`.`fakultas_id`),6,6) as `fakultas_id`,
				`tbl_fakultas`.`fakultas_id` as `hid_id`,
				`tbl_fakultas`.`keterangan`,
				`tbl_fakultas`.`image`,
				`tbl_fakultas`.`kode_fakultas`
				FROM `db_ptiik_apps`.`tbl_fakultas`";
		if($id!=""){
			$sql=$sql . " WHERE mid(md5(`tbl_fakultas`.`fakultas_id`),6,6)='".$id."' ";
		}
		$sql = $sql . "ORDER BY `tbl_fakultas`.`fakultas_id` DESC";
		$result = $this->db->query( $sql );	
		return $result;	
	}

	function get_reg_number(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(fakultas_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_fakultas"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_fakultas($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_fakultas',$datanya);
	}
	
	function update_fakultas($fakultas_id, $fakultas_id_param, $keterangan) {
		$sql = "UPDATE `db_ptiik_apps`.`tbl_fakultas`
				SET `tbl_fakultas`.`fakultas_id`='".$fakultas_id."', 
					`tbl_fakultas`.`keterangan`='".$keterangan."',
					`tbl_fakultas`.`kode_fakultas`='".$fakultas_id."'
				WHERE `tbl_fakultas`.`fakultas_id`='".$fakultas_id_param."'
				";
		if($result = $this->db->query( $sql )){
		return TRUE;
		}
	}
	
	function clean(){
		 $sql = "DELETE FROM `db_ptiik_apps`.`tbl_fakultas`";
		 $this->db->query( $sql );
	}
	
	function cekketfakultas($ket){
		$sql = "SELECT `tbl_fakultas`.`fakultas_id`
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE `tbl_fakultas`.`keterangan` = '".$ket."'";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->fakultas_id;}
	}
	
	function cekkodefakultas($kode){
		$sql = "SELECT `tbl_fakultas`.`fakultas_id`
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE `tbl_fakultas`.`kode_fakultas` = '".$kode."'";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
			return $result = $id->fakultas_id;
		}
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, 
					   keterangan, extention, 
					   max_size 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		return $result;

	}
	
	function cek_nama_icon($term){
		$sql = "SELECT icon
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE icon = '".$term."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->icon;
			return $result;
		}
		
	}
}
?>