<?php
class model_prodi extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT MID(MD5(`tbl_prodi`.`prodi_id`),6,6) AS `prodi_id`,
					   `tbl_prodi`.`prodi_id` AS `hid_id`,
					   `tbl_prodi`.`fakultas_id` AS `fakultas_id`,
					   `tbl_prodi`.`keterangan`,
					   `tbl_prodi`.`strata`,
					   `tbl_prodi`.`kode_prodi`,
					   `tbl_prodi`.`parent_id`,
					   `tbl_prodi`.`is_aktif`,
					   `tbl_prodi`.`image`,
					   `b`.`keterangan` AS `parent_keterangan`,
					   `tbl_fakultas`.`keterangan` AS `fakultas_keterangan`
				FROM `db_ptiik_apps`.`tbl_prodi`
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` `b` ON `b`.`prodi_id` = `tbl_prodi`.`prodi_id`
				INNER JOIN `db_ptiik_apps`.`tbl_fakultas` ON `tbl_prodi`.`fakultas_id` =  `tbl_fakultas`.`fakultas_id`";
		if($id!=""){
			$sql=$sql . " WHERE mid(md5(`tbl_prodi`.`prodi_id`),6,6)='".$id."' ";
		}
		$sql = $sql . "ORDER BY `tbl_prodi`.`prodi_id` DESC";
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	function get_prodi(){
		$sql = "SELECT tbl_prodi.fakultas_id,
		 			   tbl_prodi.kode_prodi,
		 			   tbl_prodi.parent_id,
		 			   tbl_prodi.keterangan,
		 			   tbl_prodi.prodi_id,
		 			   MID(MD5(`tbl_prodi`.`prodi_id`),6,6) AS `id`
				FROM `db_ptiik_apps`.`tbl_prodi`";
		return $result = $this->db->query($sql);
	}
	
	function get_prodi_child($prodi_id){
		$sql = "SELECT `tbl_prodi`.`keterangan`, `tbl_prodi`.`prodi_id`
				FROM `db_ptiik_apps`.`tbl_prodi`
				WHERE `tbl_prodi`.`parent_id` = '". $prodi_id ."' ";
		return $result = $this->db->query($sql);
	}
	
	function get_reg_number(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(prodi_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_prodi"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_prodi($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_prodi',$datanya);
	}
	
	public function tampilkan_parent($id){
		$sql = "SELECT 	`tbl_prodi`.prodi_id as id_parent,
						`tbl_prodi`.keterangan as keterangan_parent
				FROM `db_ptiik_apps`.`tbl_prodi`, `db_ptiik_apps`.`tbl_fakultas`
				WHERE `tbl_fakultas`.fakultas_id = '$id' and
					  `tbl_fakultas`.fakultas_id = `tbl_prodi`.fakultas_id";		
		$result = $this->db->getResults( $sql );
		// echo $sql;
		return $result;
	}
	
	public function getFakultas(){
		$sql = "SELECT fakultas_id, keterangan FROM `db_ptiik_apps`.`tbl_fakultas`";		
		$result = $this->db->getResults( $sql );
		return $result;
	}
	
	function clean(){
		 $sql = "DELETE FROM `db_ptiik_apps`.`tbl_prodi`";
		 $this->db->query( $sql );
	}
	
	function rekursif($criteria = '0') {
		$sql = "select `tbl_prodi`.* from `db_ptiik_apps`.`tbl_prodi` where `tbl_prodi`.`parent_id` = '".$criteria."'";
		$result = $this->db->query( $snd($where) {
		$sql = "SELECT DISTINCT prodi_id , keterangan
				FROM tb_prodi
				WHERE parent_id ". $where ."";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, 
					   keterangan, extention, 
					   max_size 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		return $result;

	}
	
	function cek_nama_icon($term){
		$sql = "SELECT icon
				FROM `db_ptiik_apps`.`tbl_prodi`
				WHERE icon = '".$term."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->icon;
			return $result;
		}
		
	}
}

?>ql );
		return $result;	
	}
	
	function cekketprodi($ket, $strata){
		$sql = "SELECT `tbl_prodi`.`prodi_id`
				FROM `db_ptiik_apps`.`tbl_prodi`
				WHERE `tbl_prodi`.`keterangan` = '".$ket."' AND `tbl_prodi`.`strata` = '".$strata."'
				";
		$id = $this->db->getrow($sql);
		if(isset($id)){
		return $result = $id->prodi_id;}
	}
	
	function cekketprodibyketfakultas($ket){
		$sql = "SELECT `tbl_fakultas`.`fakultas_id`
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE `tbl_fakultas`.`keterangan` = '".$ket."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->fakultas_id;}
	}
	
	function cekkodeprodi($kode){
		$sql = "SELECT `tbl_prodi`.`prodi_id`
				FROM `db_ptiik_apps`.`tbl_prodi`
				WHERE `tbl_prodi`.`kode_prodi` = '".$kode."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->prodi_id;}
	}
	
	function cekkodeprodibykodefakultas($kode){
		$sql = "SELECT `tbl_fakultas`.`fakultas_id`
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE `tbl_fakultas`.`kode_fakultas` = '".$kode."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->fakultas_id;
		}
	}
	
	function cekstrataprodi($strata){
		$sql = "SELECT `tbl_prodi`.`prodi_id`
				FROM `db_ptiik_apps`.`tbl_prodi`
				WHERE `tbl_prodi`.`strata` = '".$strata."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->prodi_id;}
	}

	public function fi