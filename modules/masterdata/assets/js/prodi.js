$(document).ready(function() {
	
	// pathArray = window.location.href.split( '/' );
	// protocol = pathArray[0];
	// host = pathArray[2];
	// url = protocol + '//' + host;
// 	
	// uri_ = url + '/beta/pjj/module/akademik/prodi/tree_fakultas';
// 	
	// $.ajax({
		// type		: "POST",
		// dataType	: "html",
		// url			: uri_,
		// success		: function(msg){
			// $(".tree").html(msg);
		// }
	// });
	$("input[name='files']").change(function(){
		$('#list').html('<img style="width: 100px; height: auto;" id="ava" src="#" alt="your image" />');
        readURL(this);
        $('.well').hide();
    });
	
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var uri = $(this).data("uri");
		//alert(uri);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("select#select_parent").html('<option value="0">Select Prodi</option>');
					$("select#select_parent").attr("disabled","disabled");
				} else {
					$("select#select_parent").removeAttr("disabled");
					$("select#select_parent").html(msg);
				}
			}
		});
	});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#ava').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
}; 

$("#form-prodi").submit(function(e){
		var select_fakultas = $('#select_fakultas').val();
		var strata = $('#strata').val();
		var keterangan = $('#keterangan').val().length;

		if(select_fakultas!==0 && strata!==0 && keterangan > 0){
		  //var postData = $('#form').serializeArray();
		  var formData = new FormData($(this)[0]);
		  //alert(strata);
          $.ajax({
            url : base_url + "module/masterdata/prodi/saveToDB",
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert (msg);
	            location.href = base_url + "module/masterdata/prodi/"; 
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	      });
	    e.preventDefault(); //STOP default action
	    
		}
		else{
			alert("Please fill the form");
		}
});