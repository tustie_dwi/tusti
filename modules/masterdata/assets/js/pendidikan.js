$(document).ready(function(){
	//$.fn.dataTableExt.sErrMode = 'throw';
	$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	$("input[name='jenis_kegiatan']").autocomplete({ 
		source: base_url + "module/masterdata/dosen/get_jenis_kegiatan",
		minLength: 0, 
		select: function(event, ui) { 
			$("input[name='jenis_kegiatan']").val(ui.item.value);
			$("input[name='jenis_kegiatan_id']").val(ui.item.id);
		}
	});
	//=====================================FORM=====================================//
	var type_form = $("#type_form").val();
	//alert(type_form);
	if(type_form.trim()!==""){
		var radio = type_form;
		$("#form-"+radio).show();
		if(radio=="formal"){
			$("#form-nonformal").hide();
			$("input[name='pendidikan'][value='nonformal']").prop("disabled", true);
			var select = document.getElementById("jenjang");
			var c = $("input[name='jenjang_edit']").val();
		    for(var i = 0;i < select.options.length;i++){
		        if(select.options[i].value == c ){
		           select.options[i].selected = true;
		            $("#jenjang").select2("val", c);
		        }
		    }
		}
		else{
			$("#form-formal").hide();
			$("input[name='pendidikan'][value='formal']").prop("disabled", true);
			var select = document.getElementById("tingkat");
			var c = $("input[name='tingkat_edit']").val();
		    for(var i = 0;i < select.options.length;i++){
		        if(select.options[i].value == c ){
		           select.options[i].selected = true;
		            $("#tingkat").select2("val", c);
		        }
		    }
		}
		$("#cancel-btn").show();
	}
	else{
		var radio = "formal";
		$("#form-formal").show();
		$("#form-nonformal").hide();
		$("#cancel-btn").hide();
	}
	
	
	
	$("input[name='pendidikan']").prop("checked", false);
	$("input[name='pendidikan'][value='"+radio+"']").prop("checked", true);
	$("input[name='tab']").val(radio);
	
	$("input[name='pendidikan']").change(function(e){
		$("#document_non_formal").val("");
		$("#document_formal").val("");
		$(".document-place").empty(); //tempat nama file
		var pendidikan = $(this).val();
		if(pendidikan == "nonformal"){
			radio = "nonformal";
			$("input[name='tab']").val(radio);
			$("#form-nonformal").show();
			$("#form-formal").hide();
		}
		else{
			radio = "formal";
			$("input[name='tab']").val(radio);
			$("#form-formal").show();
			$("#form-nonformal").hide();
		}
	});
	//=====================================FORM=====================================//
	
	
	$('#form_pendidikan_formal').submit(function(e){
		var docid		= $('#document_formal').val();
		var jenjang		= $('#jenjang').val();
		var thn_lulus	= $('#thn_lulus').val();
		var prodi		= $('#prodi').val();
		var nama_sekolah= $('#nama_sekolah').val();
		var kar_id		= $("#kar_id").val();
		if(docid.trim()!=="" && jenjang !== 0 && thn_lulus.trim()!=="" && prodi.trim()!=="" && nama_sekolah.trim()!==""){
			var formData = new FormData($(this)[0]);
			var URL = base_url + 'module/masterdata/dosen/save_pendidikan';
			$.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.href = base_url + 'module/masterdata/dosen/edit/'+kar_id;
		            //location.reload();
		        },
		        error: function(msg) 
		        {
		            alert ("ERROR!");
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});
		}
		else{
			alert("Please check your input data!");
		}
		e.preventDefault(); //STOP default action
		return false;
	});
	
	$('#form_pendidikan_nonformal').submit(function(e){
		var docid			= $('#document_non_formal').val();
		var nama_kegiatan	= $('#nama_kegiatan').val();
		var jenis_kegiatan	= $('#jenis_kegiatan').val();
		var jenis_kegiatan_id	= $('#jenis_kegiatan_id').val();
		var penyelenggara	= $('#penyelenggara').val();
		var sebagai			= $('#sebagai').val();
		var tingkat			= $('#tingkat').val();
		var kar_id		= $("#kar_id").val();
		if(docid.trim()!=="" && nama_kegiatan.trim()!=="" && jenis_kegiatan.trim()!=="" && penyelenggara.trim()!=="" && tingkat !== 0 && sebagai.trim()!==""){
			if(jenis_kegiatan_id.trim()!==""){
				var formData = new FormData($(this)[0]);
				var URL = base_url + 'module/masterdata/dosen/save_pendidikan';
				$.ajax({
		            url : URL,
			        type: "POST",
			        dataType : "HTML",
			        data : formData,
			        async: false,
			        success:function(msg) 
			        {
			            alert (msg);
			            location.href = base_url + 'module/masterdata/dosen/edit/'+kar_id;
			        },
			        error: function(msg) 
			        {
			            alert ("ERROR!");
			        },
			        cache: false,
			        contentType: false,
			        processData: false
				});
			}
			else{
				alert("Silahkan pilih jenis kegiatan sesuai dengan yang disediakan!");
			}
		}
		else{
			alert("Please check your input data!");
		}
		e.preventDefault(); //STOP default action
			return false;
	});
	
	$(".select_library").click(function(){
		setTimeout(function(){
			$(".library_attach").show();
		},200);
	});
	
	$(".edit_pend").click(function(){
		var id = $(this).data("pend");
		var jenis = $(this).data("jenis");
		//alert(id);
		var form = document.createElement("form");
	    var input = document.createElement("input");
	    var input2 = document.createElement("input");
		
		form.action = document.URL;
		form.method = "post"
		
		input.name = "pend_id";
		input.value = id;
		form.appendChild(input);
		
		input2.name = "jenis";
		input2.value = jenis;
		form.appendChild(input2);
		
		document.body.appendChild(form);
		form.submit();
	});
});

function select(id,filename){
    var type = $('#myTab .active a').attr('href').substr(1);
    // alert(type2);
    
	if(type == "pendidikan"){
		//alert(type);
		var form = $("input[name='tab']").val();
		if(form == "formal"){
			$("#document_formal").val(id);
		}
		else{
			$("#document_non_formal").val(id);
		}
		var info = filename;
		info += '&nbsp;<a href="javascript::" onclick="remove_selected()"><i class="fa fa-trash-o"></i></a>';
		info += '<input type="hidden" name="docid" value="'+id+'" />';
		$('.document-place').html(info);
		$('#media').modal('hide');
	}
	else if(type == "kenaikan"){
		var info = filename;
		info += '&nbsp;<a href="javascript::" onclick="remove_selected_img()"><i class="fa fa-trash-o"></i></a>';
		info += '<input type="hidden" name="docid" value="'+id+'" />';
		$('#document-place').html(info);
		$('#mediafile').modal('hide');
	}
}
