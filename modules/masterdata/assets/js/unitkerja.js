$(document).ready(function(){
	$("#btn-fakultas").change(function(){
		var nilai = $('#btn-fakultas').val();
		var uri = $(this).data("uri");
		
		if(nilai != '0') {
			$.ajax({
				type	: 'POST',
				dataType: 'html',
				url		: uri,
				data	: $.param({fakultasid : nilai}),
				success	: function(msg){
					if(msg != ''){
						$("#btn-unit").removeAttr("disabled");
						$("#btn-unit").html(msg);
					}
					else{
						$("#btn-unit").attr("disabled","disabled");
					}
				}
			});
		}
		else {
			$("#btn-unit").attr("disabled","disabled");
			$("#btn-unit").html("<option value='0'>Pilih Unit Kerja Parent</option>");
		}
	});
	
	$(".btn-del").click(function(){
		var uri_ = $(this).data("uri");
		var kode_ = $(this).data("kode");
		var id_ = $(this).data("id");
		
		if(confirm("Apakah Anda yakin ingin menghapus data unit kerja "+kode_+" ?")){
			$.ajax({
				type : 'POST',
				dataType : 'html',
				url : uri_,
				success : function(msg){
					$("#notif").html("<div class='alert alert-warning'><button type='button' class='close' data-dismiss='alert'>&times;</button>OK, data unit kerja "+ kode_ +" telah berhasil dihapus.</div>");
					$(".unit-"+id_).fadeOut(500);
				}
			});
		}
	});
	
	$("#btn-unitkerja").click(function(){
		var fakultas_id_ = $('#btn-fakultas').val();
		var kode_ = $("#form-unitkerja [name='kode']").val();
		var parent_id_ = $("#form-unitkerja [name='parent_id']").val();
		var keterangan_ = $("#form-unitkerja [name='keterangan']").val();
		var hidId_ = $("#form-unitkerja [name='hidId']").val();
		var chklab_ = $("#form-unitkerja [name='chklab']").val();
		var uri_ = $("#uri").val();
		
		if(hidId_== ''){
			var init = 'disimpan';
		}
		else{
			var init = 'diubah';
		}
		
		$.ajax({
			type : 'POST',
			dataType : 'html',
			url : uri_,
			data : $.param({
						fakultas_id: fakultas_id_,
						kode : kode_,
						parent_id : parent_id_,
						keterangan : keterangan_,
						chklab : chklab_,
						hidId : hidId_
					}),
			success : function(msg){
				if(msg == 'err'){
					$("#notif").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Oops, data tidak tersimpan. Kode Unit Kerja sudah ada!!</div>");
				}
				else{
					
					alert("Ok, data berhasil "+ init +".");
					location.href = base_url + "module/masterdata/unitkerja/"; 
					//$("#notif").html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>OK, data unit kerja "+ kode_ +" telah berhasil "+init+"</div>");
				}
			}
		});
	});
});
