$(document).ready(function() {
	$("input[name='files']").change(function(){
		$('#list').html('<img style="width: 100px; height: auto;" id="ava" src="#" alt="your image" />');
	    readURL(this);
	    $('.well').hide();
	});
	
	$("#form-fakultas").submit(function(e){
		var kode_fakultas = $('#kode_fakultas').val().length;
		var keterangan = $('#keterangan').val().length;
		
		if(kode_fakultas > 0 && keterangan > 0){
		  //var postData = $('#form').serializeArray();
		  var formData = new FormData($(this)[0]);
	      $.ajax({
	        url : base_url + "module/masterdata/fakultas/saveToDB",
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert (msg);
	            window.location.href = base_url + "module/masterdata/fakultas/index";
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload data failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	      });
	     e.preventDefault(); //STOP default action
	    
		}
		else{
			alert("Please fill the form");
		}
	});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#ava').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
};