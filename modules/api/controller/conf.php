<?php
class api_conf extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		//$coms->require_auth('auth'); 
	}	
	
	function conto(){
		$a = array('a' => 'Apple' ,'b' => 'banana' , 'c' => 'Coconut');
 
		//serialize the array
		$s = gzcompress(base64_encode(json_encode($a)));
		echo "aaa".$s;
		//{"a":"Apple","b":"banana","c":"Coconut"}
		 
		echo '<br /><br />';
		 
		//unserialize
		$o = json_decode(base64_decode(gzuncompress($s)));
		if($o){	
			$return_arr = array();
			
			foreach($o as $row){	
				
				foreach ($row as $key => $value) {
					 $arr[$key] = $value;				
				}
				array_push($return_arr,$arr);
			}
			
			$json_response = json_encode($return_arr);

			echo $json_response;
		}
	
		//echo $o;
	}
	
	function jadwal_json(){
		$mjadwal = new model_jadwalinfo();
		
		$data['lang'] = $this->config->lang;
		
		
		$lang = $data['lang'];
		
		$is_pendek = $mjadwal->get_is_pendek();
		$data  = $mjadwal->get_jadwal($this->config->cabang, $this->config->fakultas, $is_pendek, $data['lang']);
		$jadwal;
		
		foreach($data as $key){
			
			if($key->dosen == '') $dosen = 'Asisten';
			else $dosen = $key->dosen;
			
			if($key->mk) $strmk = $key->mk;
			else $strmk = $key->mk_ori;
			
			$tmp = array(
				'str_kelas' => $key->strkelas,
				'matakuliah' => $strmk,
				'mk_id' => $key->mk_id,
				'kelas' => $key->kelas,
				'jam_mulai' => substr($key->jam_mulai,0,5),
				'jam_selesai' => substr($key->jam_selesai,0,5),
				'karyawan_id' => $key->karyawan_id,
				'dosen' => $dosen,
				'prodi' => $key->prodi,
				'urut' => $key->urut,
				'urut_selesai' => $key->urut_selesai,
				'praktikum' => $key->is_praktikum,
				'repeat_on' => $key->repeat_on,
				'tgl_mulai' => $key->tgl_mulai,
				'tgl_selesai' => $key->tgl_selesai
			);
			
			if(! isset($jadwal[$key->hari][$key->ruang_id])) $jadwal[$key->hari][$key->ruang_id] = array();
			array_push($jadwal[$key->hari][$key->ruang_id], $tmp);
		}
		$result = json_encode($jadwal);
		
		echo base64_encode($result);
		//$k = gzcompress(base64_encode(trim($result)),-1);
		//echo $result;
		//echo gzcompress(base64_encode(trim($result)),-1);
		//echo base64_decode(gzuncompress(trim($k));
	}
	
	function get_tweet(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		
		$result = $mconf->get_tweet(10);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($result as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
	
		# Return the response
		echo base64_encode($json_response);
	}
	
	function get_pengumuman(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		
		$result = $mconf->read_content('pengumuman', '', 10);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($result as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
	
		# Return the response
		echo base64_encode($json_response);
	}
	
	function get_berita($lang=NULL){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if($lang) $str_=$lang;
		else $str_='en';
		
		$mconf= new model_conf();
		
		$result = $mconf->read_content('news', $str_, 10);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($result as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
	
		# Return the response
		//echo $json_response;
		echo base64_encode($json_response);
	}
	
	
	function get_cuaca(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		
		$result = $mconf->get_cuaca();
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($result as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
	
		# Return the response
		echo base64_encode($json_response);
	}
	
	
	function absen_pimpinan(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		
		$result = $mconf->get_absen_pimpinan();
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
	
		# Return the response
		echo base64_encode($json_response);
	}
	
	
	function namamk(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf	= new model_conf();
		
		$fakid = $this->config->fakultas;
		
		$result = $mconf->get_namamk($str, $fakid);
		
		$return_arr = array();
		
		if($result){
			foreach($result as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			
			//$json_response = json_encode($return_arr);
			$json_response = gzcompress(base64_encode(trim(json_encode($return_arr))),-1);
			
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
			echo $json_response;
		}else{
			echo json_encode('failed');
		}
		
	}
	
	function namamk_from_matakuliah(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		$fakid = $this->config->fakultas;
		$result = $mconf->get_namamk_from_matakuliah($str, $fakid);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		//$json_response = json_encode($return_arr);
		
		$json_response = gzcompress(base64_encode(trim(json_encode($return_arr))),-1);
		
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
		
	}
	
	function namamkfrommkditawarkan(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if($_POST['fakultasid']){
			$fakid = $_POST['fakultasid'];
		}else $fakid = '';
		
		if($_POST['cabangid']){
			$cabangid = $_POST['cabangid'];
		}else $cabangid = '';
		
		if($_POST['thnakademikid']){
			$thnakademikid = $_POST['thnakademikid'];
		}else $thnakademikid = '';
		
		$mconf= new model_conf();
		
		$result = $mconf->get_namamkfrommkditawarkan($str, $fakid, $cabangid, $thnakademikid);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key]= $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		echo $json_response;
		
	}
	
	function namamk_from_mkditawarkan_by_pengampu(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$mconf= new model_conf();
		
		$result = $mconf->get_all_namamk_from_mkditawarkan($str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key]= $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		//$this->view( 'mk/tes.php', $data );
	}
	
	function dosen(){
		$mconf= new model_conf();
		
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if(isset($_POST['fakultas_id'])){
			$fakultas_id = $mconf->get_fakultas_id($_POST['fakultas_id']);
		}else $fakultas_id = NULL;
		
		$result = $mconf->get_nama_dosen($str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		//$json_response = json_encode($return_arr);
		$json_response = gzcompress(base64_encode(trim(json_encode($return_arr))),-1);
		
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		//$this->view( 'mk/tes.php', $data );

	}
	
	
	function staff(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		
		$result = $mconf->get_nama_staff($str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		//$this->view( 'mk/tes.php', $data );

	}
	
	function ruang(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if(isset($_POST['cabang_id'])){
			$cabangid = $_POST['cabang_id'];
		}else $cabangid = "";
		
		$mconf= new model_conf();
		
		$result = $mconf->get_ruang($str, $cabangid);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		//$json_response = json_encode($return_arr);
		
		$json_response = gzcompress(base64_encode(trim(json_encode($return_arr))),-1);
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$this->view( 'mk/tes.php', $data );
	}
	
	function mhs(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if(isset($_POST['fakultas_id'])){
			$fakultas_id = $_POST['fakultas_id'];
		}else $fakultas_id = "";
		
		$mconf= new model_conf();
		
		$result = $mconf->get_allmhs($str,$fakultas_id);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;
			}
			array_push($return_arr,$arr);
		}
		
		//$json_response = json_encode($return_arr);
		
		$json_response = gzcompress(base64_encode(trim(json_encode($return_arr))),-1);
		
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$this->view( 'mk/tes.php', $data );
	}
	
		
}
?>