<?php
class model_conf extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read_content($cat=NULL, $lang=NULL,$limit=NULL){
		$sql= "SELECT
					mid(md5(db_ptiik_coms.coms_content.content_id),9,7) AS id,
					db_ptiik_coms.coms_content.content_id,
					db_ptiik_coms.coms_content.content_category,
					coms_content.content_lang,
					db_ptiik_coms.coms_content.content_title as post_title,		
					db_ptiik_coms.coms_content.content AS post_content,					
					db_ptiik_coms.coms_content.content_excerpt as post_excerpt,
					db_ptiik_coms.coms_content.content_upload as post_date,
					db_ptiik_coms.coms_content.content_comment,
					db_ptiik_coms.coms_content.content_thumb_img AS thumb_img,
					db_ptiik_coms.coms_content.content_thumb_img AS icon,
					db_ptiik_coms.coms_content.is_sticky,
					db_ptiik_coms.coms_content.user_id,
					db_ptiik_coms.coms_content.last_update AS content_modified
				FROM  db_ptiik_coms.coms_content WHERE coms_content.content_category ='$cat' ";
		if($lang) $sql.= " AND coms_content.content_lang='$lang' ";
	
		$sql.=" ORDER BY db_ptiik_coms.coms_content.last_update DESC LIMIT 0,$limit ";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	
	function get_tweet($limit=NULL){
		$sql = "SELECT
					db_ptiik_coms.tbl_twitter.created_at,
					db_ptiik_coms.tbl_twitter.keterangan
					FROM
					db_ptiik_coms.tbl_twitter  ORDER BY created_at DESC LIMIT 0,".$limit."
					";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	
	function get_cuaca($limit=NULL){
		$sql = "SELECT
					db_ptiik_coms.tbl_cuaca.main,
					db_ptiik_coms.tbl_cuaca.icon,
					concat('http://ptiik.ub.ac.id/assets/images/',db_ptiik_coms.tbl_cuaca.icon,'.png') as img
					FROM
					db_ptiik_coms.tbl_cuaca LIMIT 0,1
					";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	
	function get_absen_pimpinan(){
	$sql="SELECT DISTINCT 
				mid(md5(tbl_karyawan.karyawan_id),9,7) as id,
				tbl_karyawan.nama,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir,
				tbl_master_jabatan.keterangan as jabatan,
				tbl_karyawan.foto,
				(SELECT is_absen FROM  db_ptiik_apps.tbl_absen_finger WHERE tbl_absen_finger.karyawan_id=tbl_karyawan.karyawan_id AND date_format(tgl_absen,'%Y-%m-%d') ='".date("Y-m-d")."' AND tbl_absen_finger.is_aktif=1) as `is_absen`
				FROM
				db_ptiik_apps.tbl_karyawan_kenaikan
				INNER JOIN db_ptiik_apps.tbl_master_jabatan ON tbl_karyawan_kenaikan.jabatan_id = tbl_master_jabatan.jabatan_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan_kenaikan.karyawan_id = tbl_karyawan.karyawan_id ORDER BY tbl_master_jabatan.jabatan_id ASC, pin DESC
				";
						
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_namamk($str=NULL, $fakid=NULL){
		$sql = "SELECT namamk_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_namamk`
				WHERE keterangan like '%".$str."%'
				AND fakultas_id = '".$fakid."'
				";
		$result = $this->db->query( $sql );		
		return $result;	
	}
	
	function get_namamk_from_matakuliah($str=NULL, $fakid=NULL){
		$sql = "SELECT tbl_namamk.keterangan as `value` FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah`
				WHERE tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
                AND tbl_namamk.keterangan LIKE '%".$str."%'
                AND tbl_namamk.fakultas_id = '".$fakid."'
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_all_namamk_from_mkditawarkan($str=NULL){
		$sql = "SELECT a.keterangan as `value` FROM `db_ptiik_apps`.`tbl_namamk` as a, `db_ptiik_apps`.`tbl_matakuliah` as b, `db_ptiik_apps`.`tbl_mkditawarkan` as c
				WHERE a.namamk_id = b.namamk_id
				AND b.matakuliah_id = c.matakuliah_id
                AND a.keterangan LIKE '%".$str."%'
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_namamkfrommkditawarkan($str=NULL, $fakid=NULL, $cabangid=NULL, $thnakademikid=NULL){
		$sql = "SELECT tbl_namamk.keterangan as `value`, c.mkditawarkan_id as `id`
				FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah` as b, `db_ptiik_apps`.`tbl_mkditawarkan` as c
				WHERE tbl_namamk.namamk_id = b.namamk_id
                AND b.matakuliah_id = c.matakuliah_id
                AND keterangan like '%".$str."%'
                 ";
                 
         if($fakid){
			$sql = $sql . " AND mid(md5(tbl_namamk.fakultas_id),6,6) = '".$fakid."' ";
		}
              	
		if($cabangid){
			$sql = $sql . " AND c.cabang_id = '".$cabangid."' ";
		}
		
		if($thnakademikid){
			$sql = $sql . " AND c.tahun_akademik = '".$thnakademikid."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_nama_dosen($str=NULL,$fakultas=NULL){
		$sql = "SELECT karyawan_id as `id`, nama as `value`
				FROM `db_ptiik_apps`.`tbl_karyawan` WHERE nama LIKE '%".$str."%' AND is_status='dosen' AND is_aktif NOT IN ('keluar','meninggal') "; 
		if($fakultas){
			$sql .= " AND fakultas_id = '".$fakultas."'";
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_nama_staff($str=NULL){
		$sql = "SELECT karyawan_id as `id`, nama as `value`
				FROM `db_ptiik_apps`.`tbl_karyawan` WHERE nama LIKE '%".$str."%' AND  is_aktif NOT IN ('keluar','meninggal') "; 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_ruang($str=NULL, $cabangid=NULL){
		$sql = "SELECT concat(keterangan, ' - ',ruang_id, ' - Kapasitas : ',kapasitas) as `value` FROM `db_ptiik_apps`.`tbl_ruang`
		 		WHERE concat(keterangan, ' - ',ruang_id, ' - Kapasitas : ',kapasitas) LIKE '%".$str."%'";
		
		if($cabangid){
			$sql = $sql . " AND cabang_id = '".$cabangid."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_allmhs($term=NULL, $fakultas_id=NULL){
		$sql = "SELECT concat(nim, ' - ', nama) as value,
					   nama,
					   nim,
					   mahasiswa_id as hid_id,
					   MID( MD5(mahasiswa_id), 6, 6) as mhs_id 
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
		 		WHERE tbl_mahasiswa.is_aktif = 'aktif' AND tbl_mahasiswa.nama LIKE '%".$term."%'";
		
		if($fakultas_id){
			$sql = $sql . " AND MID( MD5(tbl_prodi.fakultas_id), 6, 6) = '".$fakultas_id."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;	
	}	

	public function readtahun(){
		$que = "SELECT mid(md5(tahun.tahun_akademik),6,6) tahun_akademik, tahun.tahun_akademik hidId, tahun.tahun, tahun.is_ganjil, tahun.is_pendek, tahun.is_aktif
				FROM `db_ptiik_apps`.`tbl_tahunakademik` `tahun` WHERE tahun.is_aktif = '1'";
		return $this->db->query($que);
	}
	
	public function get_idtahun(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tahun_akademik,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_tahunakademik`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	public function replace_tahun($data){
		$sql = "UPDATE db_ptiik_apps.tbl_tahunakademik SET is_aktif = '0'";
		$this->db->query($sql);
		$this->db->replace('db_ptiik_apps`.`tbl_tahunakademik',$data);
	}
	
	public function deletetahun($id){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_tahunakademik` WHERE mid(md5(`tahun_akademik`),6,6) = '" . $id ."'";
		$this->db->query($sql);
	}
	
	public function read(){
		$que = "SELECT jenis.keterangan jenis_kegiatan, 
				tahun.tahun, 
				kalender.tgl_mulai, 
				kalender.tgl_selesai tgl_selesai, 
				kalender.is_aktif, 
				mid(md5(kalender.kalender_id),6,6) kalender_id,
				kalender.kalender_id hidId,
				kalender.jenis_kegiatan_id,
				kalender.tahun_akademik
				FROM `db_ptiik_apps`.`tbl_kalenderakademik` `kalender`, `db_ptiik_apps`.`tbl_jeniskegiatan` `jenis`, `db_ptiik_apps`.`tbl_tahunakademik` `tahun`
				WHERE kalender.jenis_kegiatan_id = jenis.jenis_kegiatan_id 
				AND kalender.tahun_akademik = tahun.tahun_akademik
				AND kalender.is_aktif = '1'";
		return $this->db->query($que);
	}
	
	public function get_kalender($tahunid){
		$que = "SELECT *
				FROM `db_ptiik_apps`.`tbl_kalenderakademik` `kalender`
				WHERE kalender.tahun_akademik = '".$tahunid."'
				AND kalender.is_aktif = '1'";
		return $this->db->query($que);
	}
	
	public function get_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kalender_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_kalenderakademik`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	public function get_kegiatan(){
		$sql ="SELECT * FROM `db_ptiik_apps`.`tbl_jeniskegiatan` `k` WHERE k.kategori = 'akademik'";
		return $this->db->query($sql);
	}
	
	public function get_tahun(){
		$sql = "SELECT tahun.tahun, tahun.tahun_akademik FROM `db_ptiik_apps`.`tbl_tahunakademik` `tahun` WHERE tahun.is_aktif = '1'";
		return $this->db->query($sql);
	}
	
	public function get_jeniskegiatan(){
		$sql = "SELECT * FROM `db_ptiik_apps`.`tbl_jeniskegiatan` `jenis`";
		return $this->db->query($sql);
	}
	
	public function update_kalender(){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_kalenderakademik` SET `is_aktif` = '0'";
		$this->db->query($sql);
	}
	
	public function replace_kalender($data){
		$this->db->replace('db_ptiik_apps`.`tbl_kalenderakademik',$data);
	}
	
	function get_fakultas($id=NULL){
		$sql = "SELECT mid(md5(fakultas_id),6,6) as fakultasid, keterangan, fakultas_id as hid_id
				FROM `db_ptiik_apps`.`tbl_fakultas`";		
		
		if($id){
			$sql .= "WHERE fakultas_id = '".$id."' ";
		}		
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_cabangub(){
		$sql = "SELECT `cabang_id`, `keterangan`
				FROM `db_ptiik_apps`.`tbl_cabang` 
				WHERE 1
				";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_fakultas_id($id){
		$sql = "SELECT fakultas_id
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE mid(md5(fakultas_id),6,6) = '".$id."'
				";		
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->fakultas_id;}
	}
	
	function get_unit($fakultasid=NULL,$param=NULL){
		$sql = "SELECT 
					tbl_unit_kerja.unit_id,
					tbl_unit_kerja.keterangan,
					tbl_unit_kerja.fakultas_id,
        			tbl_fakultas.keterangan as fakultas
				FROM db_ptiik_apps.tbl_unit_kerja
				LEFT JOIN db_ptiik_apps.tbl_fakultas ON tbl_fakultas.fakultas_id = tbl_unit_kerja.fakultas_id
				WHERE 1
			   ";
		
		if($fakultasid){
			$sql .= " AND tbl_unit_kerja.fakultas_id = '".$fakultasid."' ";
		}
		
		if($param=='getfakultas'){
			$sql .= " GROUP BY tbl_unit_kerja.fakultas_id ";
		}
			   
		return $this->db->query($sql);
	}
	
	function get_ruang_select($fakultas=NULL, $cabang=NULL){
		$sql = "SELECT * 
				FROM `db_ptiik_apps`.`tbl_ruang`
		 		WHERE kategori_ruang NOT IN ('kuliah')";
		
		if($fakultas){
			$sql = $sql . " AND fakultas_id = '".$fakultas."' ";
		}
		if($cabang){
			$sql = $sql . " AND cabang_id = '".$cabang."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function read_cabang($id=NULL){
		$sql = "SELECT * 
				FROM `db_ptiik_apps`.`tbl_cabang`
		 		WHERE 1 ";
		
		if($id){
			$sql .= " AND tbl_cabang.cabang_id = '".$id."' ";
		}
		
		return $this->db->query($sql);
	}
	
	function replace_cabang($datanya){
		$this->db->replace('db_ptiik_apps`.`tbl_cabang',$datanya);
	}
	
	function update_cabang($cabangid=NULL,$keterangan=NULL,$cabangid_ori=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_cabang SET tbl_cabang.cabang_id = '".$cabangid."', tbl_cabang.keterangan = '".$keterangan."'
				WHERE tbl_cabang.cabang_id = '".$cabangid_ori."' ";
				
		return $this->db->query($sql);
		
	}
	
}
?>