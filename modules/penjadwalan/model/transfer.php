<?php
class model_transfer extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
	
	function read_absen($prodi=NULL, $kelas=NULL){
		$sqlq = "SELECT
				dbptiik_tmp.tbl_absen_dosen.id_kehadiran,
				dbptiik_tmp.tbl_absen_dosen.tahun,
				dbptiik_tmp.tbl_absen_dosen.semester,
				dbptiik_tmp.tbl_absen_dosen.semester_pendek,
				dbptiik_tmp.tbl_absen_dosen.nama,
				dbptiik_tmp.tbl_absen_dosen.nama_mk,
				dbptiik_tmp.tbl_absen_dosen.k_mk,
				dbptiik_tmp.tbl_absen_dosen.prodi,
				dbptiik_tmp.tbl_absen_dosen.jurusan,
				dbptiik_tmp.tbl_absen_dosen.kelas,
				dbptiik_tmp.tbl_absen_dosen.nip,
				dbptiik_tmp.tbl_absen_dosen.tgl_absensi,
				dbptiik_tmp.tbl_absen_dosen.materi,
				dbptiik_tmp.tbl_absen_dosen.status_kehadiran,
				dbptiik_tmp.tbl_absen_dosen.id_jadwal,
				dbptiik_tmp.tbl_absen_dosen.pertemuan_ke,
				dbptiik_tmp.tbl_absen_dosen.prodi_id,
				(SELECT karyawan_id FROM db_ptiik_apps.tbl_karyawan WHERE (tmp_id IN (dbptiik_tmp.tbl_absen_dosen.nip)  OR (db_ptiik_apps.tbl_karyawan.nama LIKE (left(dbptiik_tmp.tbl_absen_dosen.nama,10))))) AS karyawan_id,
				db_ptiik_apps.vw_jadwaldosen.jadwal_id
				FROM
				dbptiik_tmp.tbl_absen_dosen
				INNER JOIN db_ptiik_apps.vw_jadwaldosen ON db_ptiik_apps.vw_jadwaldosen.namamk = dbptiik_tmp.tbl_absen_dosen.nama_mk AND dbptiik_tmp.tbl_absen_dosen.kelas = db_ptiik_apps.vw_jadwaldosen.kelas_id AND dbptiik_tmp.tbl_absen_dosen.prodi_id = db_ptiik_apps.vw_jadwaldosen.prodi_id AND dbptiik_tmp.tbl_absen_dosen.tahun = db_ptiik_apps.vw_jadwaldosen.tahun
				ORDER BY dbptiik_tmp.tbl_absen_dosen.nama ASC
				";
		$sql = "SELECT DISTINCT 
				dbptiik_tmp.tbl_absen_dosen.id_kehadiran,
				dbptiik_tmp.tbl_absen_dosen.tahun,
				dbptiik_tmp.tbl_absen_dosen.semester,
				dbptiik_tmp.tbl_absen_dosen.semester_pendek,
				dbptiik_tmp.tbl_absen_dosen.nama,
				dbptiik_tmp.tbl_absen_dosen.nama_mk,
				dbptiik_tmp.tbl_absen_dosen.k_mk,
				dbptiik_tmp.tbl_absen_dosen.prodi,
				dbptiik_tmp.tbl_absen_dosen.jurusan,
				dbptiik_tmp.tbl_absen_dosen.kelas,
				dbptiik_tmp.tbl_absen_dosen.nip,
				dbptiik_tmp.tbl_absen_dosen.tgl_absensi,
				dbptiik_tmp.tbl_absen_dosen.materi,
				dbptiik_tmp.tbl_absen_dosen.status_kehadiran,
				dbptiik_tmp.tbl_absen_dosen.id_jadwal,
				dbptiik_tmp.tbl_absen_dosen.pertemuan_ke,
				dbptiik_tmp.tbl_absen_dosen.prodi_id,
				db_ptiik_apps.vw_jadwaldosen.mkditawarkan_id,
				db_ptiik_apps.vw_jadwaldosen.dosen_id
				FROM
				db_ptiik_apps.vw_jadwaldosen
				INNER JOIN dbptiik_tmp.tbl_absen_dosen ON db_ptiik_apps.vw_jadwaldosen.tahun = dbptiik_tmp.tbl_absen_dosen.tahun AND db_ptiik_apps.vw_jadwaldosen.is_ganjil = dbptiik_tmp.tbl_absen_dosen.semester AND db_ptiik_apps.vw_jadwaldosen.namamk = dbptiik_tmp.tbl_absen_dosen.nama_mk AND db_ptiik_apps.vw_jadwaldosen.kelas_id = dbptiik_tmp.tbl_absen_dosen.kelas AND db_ptiik_apps.vw_jadwaldosen.prodi_id = dbptiik_tmp.tbl_absen_dosen.prodi_id WHERE dbptiik_tmp.tbl_absen_dosen.prodi_id='".$prodi."' AND  dbptiik_tmp.tbl_absen_dosen.kelas ='".$kelas."' 
				ORDER BY dbptiik_tmp.tbl_absen_dosen.tgl_absensi 
				";
		 $result = $this->db->query($sql);
		 
		 // echo $sql;
		 return $result;
	}
	
	function read_absen_mhs($mk=NULL, $kelas=NULL, $prodi=NULL, $angkatan=NULL){
		$sql = "SELECT DISTINCT 
					dbptiik_tmp.tbl_absen_mhs.tahun,
					dbptiik_tmp.tbl_absen_mhs.semester,
					dbptiik_tmp.tbl_absen_mhs.semester_pendek,
					dbptiik_tmp.tbl_absen_mhs.nim,
					dbptiik_tmp.tbl_absen_mhs.nama,
					dbptiik_tmp.tbl_absen_mhs.nip,
					dbptiik_tmp.tbl_absen_mhs.nama_dosen,
					dbptiik_tmp.tbl_absen_mhs.nama_mk,
					dbptiik_tmp.tbl_absen_mhs.k_mk,
					dbptiik_tmp.tbl_absen_mhs.prodi,
					dbptiik_tmp.tbl_absen_mhs.jurusan,
					dbptiik_tmp.tbl_absen_mhs.kelas,
					dbptiik_tmp.tbl_absen_mhs.tgl_absensi,
					dbptiik_tmp.tbl_absen_mhs.materi,
					dbptiik_tmp.tbl_absen_mhs.kode_absensi,
					dbptiik_tmp.tbl_absen_mhs.status_kehadiran,
					dbptiik_tmp.tbl_absen_mhs.id_jadwal,
					dbptiik_tmp.tbl_absen_mhs.pertemuan_ke,
					dbptiik_tmp.tbl_absen_mhs.prodi_id,
					db_ptiik_apps.vw_jadwaldosen.mkditawarkan_id
					FROM
					db_ptiik_apps.vw_jadwaldosen
					INNER JOIN dbptiik_tmp.tbl_absen_mhs ON dbptiik_tmp.tbl_absen_mhs.nama_mk = db_ptiik_apps.vw_jadwaldosen.namamk AND db_ptiik_apps.vw_jadwaldosen.tahun = dbptiik_tmp.tbl_absen_mhs.tahun AND db_ptiik_apps.vw_jadwaldosen.kelas_id = dbptiik_tmp.tbl_absen_mhs.kelas AND db_ptiik_apps.vw_jadwaldosen.prodi_id = dbptiik_tmp.tbl_absen_mhs.prodi_id AND db_ptiik_apps.vw_jadwaldosen.is_ganjil = dbptiik_tmp.tbl_absen_mhs.semester 
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON dbptiik_tmp.tbl_absen_mhs.nim = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					WHERE db_ptiik_apps.vw_jadwaldosen.mkditawarkan_id = '".$mk."' 
					AND dbptiik_tmp.tbl_absen_mhs.prodi_id='".$prodi."' AND dbptiik_tmp.tbl_absen_mhs.kelas='".$kelas."' AND db_ptiik_apps.tbl_mahasiswa.angkatan = '".$angkatan."' 
					ORDER BY dbptiik_tmp.tbl_absen_mhs.nim ASC 
					";
		 $result = $this->db->query($sql);
		 echo $sql."<br>";
		 return $result;
	}
	
	function read_jadwal(){
	
		$sql = "SELECT
					`transferdata`.`ganjil2013`.`no`,
					`transferdata`.`ganjil2013`.`prodi`,
					`transferdata`.`ganjil2013`.`hari`,
					`transferdata`.`ganjil2013`.`kode_mulai`,
					`transferdata`.`ganjil2013`.`kode_selesai`,
					`transferdata`.`ganjil2013`.`kelas`,
					`transferdata`.`ganjil2013`.`kode_mk`,
					`transferdata`.`ganjil2013`.`nama_mk`,
					`transferdata`.`ganjil2013`.`ruang`,
					`transferdata`.`ganjil2013`.`pengampu`,
					`transferdata`.`ganjil2013`.`jam_mulai`,
					`transferdata`.`ganjil2013`.`praktikum`,
					`db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`,
					`transferdata`.`tbl_prodi`.`prodi_id`,
					`transferdata`.`tbl_jam`.`jam_mulai` AS `jam_selesai`
					FROM
									`transferdata`.`ganjil2013`
									Inner Join `db_ptiik_apps`.`tbl_namamk` ON `transferdata`.`ganjil2013`.`nama_mk` = `db_ptiik_apps`.`tbl_namamk`.`keterangan`
									Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_namamk`.`namamk_id` = `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id`
									Inner Join `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id`
									Inner Join `transferdata`.`tbl_prodi` ON `transferdata`.`ganjil2013`.`prodi` = `transferdata`.`tbl_prodi`.`keterangan`
									Inner Join `transferdata`.`tbl_jam` ON `transferdata`.`ganjil2013`.`jam_selesai` = `transferdata`.`tbl_jam`.`jam_selesai`
					WHERE
				`db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` =  '20130100'
					";

		 $result = $this->db->query($sql);
		 return $result;
	}
	
	function read_jadwal_ok(){
		$sql = "SELECT
				`transferdata`.`jadwalmk2013`.`jadwal_id`,
				`transferdata`.`jadwalmk2013`.`kode_mk`,
				`transferdata`.`jadwalmk2013`.`namamk`,
				`transferdata`.`jadwalmk2013`.`sks`,
				`transferdata`.`jadwalmk2013`.`kurikulum`,
				`transferdata`.`jadwalmk2013`.`tahun`,
				`transferdata`.`jadwalmk2013`.`is_ganjil`,
				`transferdata`.`jadwalmk2013`.`is_pendek`,
				`transferdata`.`jadwalmk2013`.`kelas_id` as `kelas`,
				`transferdata`.`jadwalmk2013`.`hari`,
				`transferdata`.`jadwalmk2013`.`ruang`,
				`transferdata`.`jadwalmk2013`.`prodi_id`,
				`transferdata`.`jadwalmk2013`.`prodi`,
				`transferdata`.`jadwalmk2013`.`mkditawarkan_id`,
				`transferdata`.`jadwalmk2013`.`jam_mulai`,
				`transferdata`.`jadwalmk2013`.`jam_selesai`,
				`transferdata`.`jadwalmk2013`.`kode_mulai`,
				`transferdata`.`jadwalmk2013`.`kode_selesai`,
				`transferdata`.`jadwalmk2013`.`karyawan_id`,
				`transferdata`.`jadwalmk2013`.`is_praktikum`,
				`transferdata`.`jadwalmk2013`.`nama`,
				`transferdata`.`jadwalmk2013`.`gelar_awal`,
				`transferdata`.`jadwalmk2013`.`gelar_akhir`,
				`transferdata`.`jadwalmk2013`.`nik`,
				`transferdata`.`jadwalmk2013`.`is_koordinator`,
				`transferdata`.`jadwalmk2013`.`sesi_mulai`,
				`transferdata`.`jadwalmk2013`.`sesi_selesai`,
				`transferdata`.`jadwalmk2013`.`tahun_akademik`,
				`transferdata`.`jadwalmk2013`.`dosen_id`
				FROM
				`transferdata`.`jadwalmk2013`
				";
		$result = $this->db->query($sql);
		 return $result;
	}
	
	function read_jadwalmk($semester=NULL){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`detail_id`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`jadwal_id`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`is_aktif`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`user`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`last_update`,
					`db_ptiik_apps`.tbl_jadwalmk.kelas,
					`db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`prodi_id`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`jam_mulai`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`jam_selesai`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`hari`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`is_praktikum`,
					`db_ptiik_apps`.tbl_jadwalmk.ruang_id_id,
					`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`,
					`db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik`
					FROM
					`db_ptiik_apps`.`tbl_penjadwalan_detail`
					Inner Join `db_ptiik_apps`.`tbl_jadwalmk` ON `db_ptiik_apps`.`tbl_penjadwalan_detail`.`detail_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`jadwal_id`
					Inner Join `db_ptiik_apps`.`tbl_pengampu` ON `db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id` = `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`
					Inner Join `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
				WHERE 1 = 1 
				";
		if($semester){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		$result = $this->db->query($sql);
		 return $result;
	}
	
	
	function read_prak($semester=NULL){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`detail_id`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`jadwal_id`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`is_aktif`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`user`,
					`db_ptiik_apps`.`tbl_penjadwalan_detail`.`last_update`,
					`db_ptiik_apps`.tbl_jadwalmk.kelas,
					`db_ptiik_apps`.`tbl_jadwalmk`.`pengampu_id`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`prodi_id`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`jam_mulai`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`jam_selesai`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`hari`,
					`db_ptiik_apps`.`tbl_jadwalmk`.`is_praktikum`,
					`db_ptiik_apps`.tbl_jadwalmk.ruang_id_id,
					`db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik`
					FROM
					`db_ptiik_apps`.`tbl_penjadwalan_detail`
					Inner Join `db_ptiik_apps`.`tbl_jadwalmk` ON `db_ptiik_apps`.`tbl_penjadwalan_detail`.`detail_id` = `db_ptiik_apps`.`tbl_jadwalmk`.`jadwal_id`
					Inner Join `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_jadwalmk`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					WHERE `db_ptiik_apps`.`tbl_jadwalmk`.`is_praktikum` = '1'
				";
		if($semester){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		$result = $this->db->query($sql);
		 return $result;

	}
	
	function get_reg_absen($mk=NULL, $kelas=NULL, $prodi=NULL, $tgl=NULL){
		$sql = "SELECT absen_id FROM db_ptiik_apps.tbl_absen WHERE mkditawarkan_id ='".$mk."' AND kelas_id = '".$kelas."'  AND prodi_id = '".$prodi."' AND tgl = '".$tgl."'  ";
		$result = $this->db->getRow( $sql );	
		
		if($result){
			$strresult = $result->absen_id;
		}else{
			$kode = date("Ym", strtotime($tgl));
						
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(absen_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_absen WHERE left(absen_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$strresult = $dt->data;
		}
		
		
		return $strresult;
	}
	
	function replace_absen($datanya, $datadosen) {
		//return $this->db->replace('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_absen", $datanya);
			
		if( $result ) {		
			$this->replace_absen_dosen($datadosen);
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
			
	}
	
	function replace_absen_dosen($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_absendosen", $datanya);
			
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
			
	}
	
	function replace_absen_mhs($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_absenmhs", $datanya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
		
	
	function update_absen($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_jadwalmk',$datanya,$idnya);
	}
	
	function delete_absen($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
	}
	
	function add_nol($str){
		if($str < 10){
			$result = "0".$str;
		}else{
			$result = $str;
		}
		
		return $result;
	}
	
}