<?php
class model_absen extends model {

	public function __construct() {
		parent::__construct();	
	}
		
	public $error;
	public $id;
	public $modified;
	
	/*-------------------------------------------------------- absen praktikum ---------------------------------------------*/
	
	function get_nama_mk_by_praktikum($mk=NULL){
		$sql = "SELECT DISTINCT
				tbl_praktikum.nama_mk,
				tbl_praktikum.kode_mk
				FROM
				db_ptiik_apps.tbl_praktikum WHERE mkditawarkan_id = '$mk' ";
		return $this->db->getRow( $sql );
	}

	
	function get_asisten($mk=NULL, $praktikum=NULL){
		$sql = "SELECT DISTINCT
				db_ptiik_apps.tbl_praktikum_asisten.mahasiswa_id,
				db_ptiik_apps.tbl_mahasiswa.nama,
				db_ptiik_apps.tbl_mahasiswa.nim,
				db_ptiik_apps.tbl_praktikum_asisten.mkditawarkan_id,
				db_ptiik_apps.tbl_praktikum_asisten.tgl_mulai,
				db_ptiik_apps.tbl_praktikum_asisten.tgl_selesai,
				db_ptiik_apps.tbl_praktikum_asisten.is_aktif,
				db_ptiik_apps.tbl_praktikum_asisten.asisten_id
				FROM
				db_ptiik_apps.tbl_praktikum_asisten
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_praktikum_asisten.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id WHERE 1 ";
		if($mk) $sql.= " AND tbl_praktikum_asisten.mkditawarkan_id='$mk' ";
		
		return $this->db->query($sql);

	}
	
	function get_reg_praktikum($jadwal=NULL, $tgl=NULL){
		$sql = "SELECT praktikum_id FROM db_ptiik_apps.tbl_praktikum WHERE tbl_praktikum.jadwal_id='$jadwal' AND  tbl_praktikum.tgl='$tgl' ";
	
		$row = $this->db->getRow( $sql );	
	
		if($row){
			$result = $row->praktikum_id;
		}else{
		
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(absen_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_absen WHERE left(absen_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	function get_reg_praktikum_mhs($praktikum=NULL, $mhs=NULL){
		$sql = "SELECT absen_id FROM db_ptiik_apps.tbl_praktikum_absen_mhs WHERE praktikum_id ='".$praktikum."' AND mahasiswa_id='$mhs' ";
		$row = $this->db->getRow( $sql );	
	
		if($row){
			$result = $row->absen_id;
		}else{
		
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(absen_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_praktikum_absen_mhs WHERE left(absen_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	function get_reg_number_asisten($praktikum=NULL, $asisten=NULL){
		$sql = "SELECT jadwal_asisten_id FROM db_ptiik_apps.tbl_praktikum_asisten_absen WHERE praktikum_id ='".$praktikum."' AND asisten_id='$asisten' ";
		$row = $this->db->getRow( $sql );	
	
		if($row){
			$result = $row->jadwal_asisten_id;
		}else{
		
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jadwal_asisten_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_praktikum_asisten_absen WHERE left(jadwal_asisten_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	function replace_absen_praktikum_mhs($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_praktikum_absen_mhs", $datanya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
	
	function replace_absen_praktikum($datanya) {
		
		return $this->db->replace("db_ptiik_apps`.`tbl_praktikum", $datanya);
			
					
	}
	function replace_absen_asisten($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_praktikum_asisten_absen", $datanya);
			
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
			
	}
	
	function delete_absen_asisten($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_praktikum_asisten_absen', $datanya);
	}
	
	/* ------------------------------------------------------- end absen praktikum -----------------------------------------*/
	
	/* -------------------------------------------------------- kuliah -----------------------------------------------------*/
	function get_hadir_dosen($periode=NULL, $mk=NULL, $prodi=NULL, $kelas=NULL, $dosen=NULL, $ishadir=NULL){
		
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_pengampu.karyawan_id,					
					Count(db_ptiik_apps.tbl_absen_dosen.absen_id) as `jml`
					FROM
						db_ptiik_apps.tbl_absen_dosen
						INNER JOIN db_ptiik_apps.tbl_absen ON db_ptiik_apps.tbl_absen_dosen.absen_id = db_ptiik_apps.tbl_absen.absen_id
						INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
						INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
						INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_jadwalmk.pengampu_id = db_ptiik_apps.tbl_pengampu.pengampu_id
						INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
						INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					WHERE 1 = 1 ";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.kelas = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.kelas),5,5) = '".$kelas."') ) ";
		}
		
		if($dosen) $sql.= " AND db_ptiik_apps.tbl_pengampu.karyawan_id = '$dosen' ";
		
		if($ishadir){
			$sql = $sql . " AND db_ptiik_apps.tbl_absen_dosen.is_hadir = '".$ishadir."' ";
		}
		
		if($periode) $sql.= " AND date_format(db_ptiik_apps.tbl_absen.tgl, '%Y-%m') = '$periode' ";
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$str= $result->jml;		
		}else{
			$str= 0;
		}
		
		return $str;
	}
		
	
	function get_rekap_absen_by_mk($semester=NULL, $dosen=NULL, $periode=NULL){
		$sql = "SELECT DISTINCT
				date_format(db_ptiik_apps.tbl_absen.tgl, '%Y-%m') AS periode,
				db_ptiik_apps.tbl_absen_dosen.dosen_id,
				db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
				db_ptiik_apps.tbl_mkditawarkan.tahun_akademik,
				db_ptiik_apps.tbl_pengampu.karyawan_id,
				db_ptiik_apps.tbl_namamk.keterangan,
				db_ptiik_apps.tbl_matakuliah.kode_mk,
				db_ptiik_apps.tbl_jadwalmk.prodi_id,
				db_ptiik_apps.tbl_jadwalmk.kelas
				FROM
				db_ptiik_apps.tbl_absen_dosen
				INNER JOIN db_ptiik_apps.tbl_absen ON db_ptiik_apps.tbl_absen_dosen.absen_id = db_ptiik_apps.tbl_absen.absen_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_jadwalmk.pengampu_id = db_ptiik_apps.tbl_pengampu.pengampu_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id WHERE 1 ";
		if($semester) $sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = '$semester' ";
		if($dosen) $sql.= " AND db_ptiik_apps.tbl_pengampu.karyawan_id = '$dosen' ";
		if($periode) $sql.= " AND date_format(db_ptiik_apps.tbl_absen.tgl, '%Y-%m') = '$periode' ";
		
		return $this->db->query($sql);
	
	}
	
	function get_periode_absen($semester=NULL, $dosen=NULL){
		$sql = "SELECT DISTINCT
					date_format(db_ptiik_apps.tbl_absen.tgl, '%Y-%m') AS periode,
					db_ptiik_apps.tbl_absen_dosen.dosen_id,
					db_ptiik_apps.tbl_mkditawarkan.tahun_akademik,
					db_ptiik_apps.tbl_pengampu.karyawan_id
					FROM
					db_ptiik_apps.tbl_absen_dosen
					INNER JOIN db_ptiik_apps.tbl_absen ON db_ptiik_apps.tbl_absen_dosen.absen_id = db_ptiik_apps.tbl_absen.absen_id
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_jadwalmk.pengampu_id = db_ptiik_apps.tbl_pengampu.pengampu_id
					WHERE 1 ";
		if($semester) $sql.= " AND db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = '$semester' ";
		if($dosen) $sql.= " AND db_ptiik_apps.tbl_pengampu.karyawan_id = '$dosen' ";
		
		return $this->db->query($sql);
	}
	
	function get_absen($mk=NULL,$prodi=NULL, $kelas=NULL, $sesi=NULL){
		$sql ="SELECT DISTINCT
				db_ptiik_apps.tbl_absen.absen_id,
				db_ptiik_apps.tbl_absen.jadwal_id,
				db_ptiik_apps.tbl_absen.total_pertemuan,
				db_ptiik_apps.tbl_absen.tgl,
				db_ptiik_apps.tbl_absen.jam_masuk,
				db_ptiik_apps.tbl_absen.jam_selesai,
				db_ptiik_apps.tbl_absen.jumlah_jam,
				db_ptiik_apps.tbl_absen.materi,
				db_ptiik_apps.tbl_absen.sesi_ke,
				db_ptiik_apps.tbl_absen.last_update,
				db_ptiik_apps.tbl_absen.`user`,
				db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
				db_ptiik_apps.tbl_jadwalmk.prodi_id,
				db_ptiik_apps.tbl_jadwalmk.kelas
				FROM
				db_ptiik_apps.tbl_absen
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_absen.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
				WHERE 1=1 ";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.kelas = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.kelas),5,5) = '".$kelas."') ) ";
		}
		
		if($sesi){
			$sql = $sql . " AND (tbl_absen.sesi_ke = '".$sesi."') ";
		}
			
		$result = $this->db->getRow($sql);
		
		return $result;

	}
	/* --------------------------------------- end absen kuliah ------------------------------*/
		
	function get_mk($semester=NULL, $staff=NULL){
		if($staff){
			$sql = "SELECT DISTINCT
					`tbl_namamk`.`keterangan` as `mk`,
					`tbl_matakuliah`.`kode_mk`,
					`tbl_mkditawarkan`.`tahun_akademik`,
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					`tbl_jadwalmk`.`mkditawarkan_id`,
					mid(md5(`tbl_jadwalmk`.`mkditawarkan_id`),5,5) as `id`
					FROM
						db_ptiik_apps.`tbl_jadwalmk`
						Inner Join db_ptiik_apps.`tbl_mkditawarkan` ON `tbl_jadwalmk`.`mkditawarkan_id` = `tbl_mkditawarkan`.`mkditawarkan_id`
						Inner Join db_ptiik_apps.`tbl_matakuliah` ON `tbl_mkditawarkan`.`matakuliah_id` = `tbl_matakuliah`.`matakuliah_id`
						Inner Join db_ptiik_apps.`tbl_namamk` ON `tbl_matakuliah`.`namamk_id` = `tbl_namamk`.`namamk_id`
						INNER JOIN db_ptiik_apps.tbl_pengampu ON tbl_jadwalmk.pengampu_id = tbl_pengampu.pengampu_id
					WHERE 1 = 1";
		}else{
			$sql = "SELECT DISTINCT
					`tbl_namamk`.`keterangan` as `mk`,
					`tbl_matakuliah`.`kode_mk`,
					`tbl_mkditawarkan`.`tahun_akademik`,
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					`tbl_jadwalmk`.`mkditawarkan_id`,
					mid(md5(`tbl_jadwalmk`.`mkditawarkan_id`),5,5) as `id`
					FROM
						db_ptiik_apps.`tbl_jadwalmk`
						Inner Join db_ptiik_apps.`tbl_mkditawarkan` ON `tbl_jadwalmk`.`mkditawarkan_id` = `tbl_mkditawarkan`.`mkditawarkan_id`
						Inner Join db_ptiik_apps.`tbl_matakuliah` ON `tbl_mkditawarkan`.`matakuliah_id` = `tbl_matakuliah`.`matakuliah_id`
						Inner Join db_ptiik_apps.`tbl_namamk` ON `tbl_matakuliah`.`namamk_id` = `tbl_namamk`.`namamk_id`
					WHERE 1 = 1 ";
		}
					
		if($semester){
			$sql = $sql . " AND `tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_pengampu.karyawan_id='".$staff."') ";
		}	
		
		$sql = $sql. "	ORDER BY `tbl_namamk`.`keterangan` ASC";
		
		$result = $this->db->query($sql);
		
				
		return $result;
	}
	
	function get_tgl($id=NULL){
		$sql = "SELECT DISTINCT 
					`tbl_absen`.`tgl`
					FROM
					db_ptiik_apps.`tbl_absen`
				WHERE 1 = 1
					";
		if($id){
			
		}
	}
	
	function  get_mhs_mk($mk=NULL,$prodi=NULL, $kelas=NULL, $dosen=NULL, $semester=NULL, $absen=NULL){
		$sql = "SELECT DISTINCT 
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_mahasiswa.angkatan,
					db_ptiik_apps.tbl_jadwalmk.kelas,
					db_ptiik_apps.tbl_jadwalmk.pengampu_id,
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id,
					db_ptiik_apps.tbl_jadwalmk.is_praktikum
					FROM
					db_ptiik_apps.tbl_krs
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_krs.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id 
						  AND db_ptiik_apps.tbl_krs.kelas = db_ptiik_apps.tbl_jadwalmk.kelas
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_jadwalmk.prodi_id 
						AND db_ptiik_apps.tbl_krs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.kelas = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.kelas),5,5) = '".$kelas."') ) ";
		}
		
		if($dosen){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.pengampu_id),5,5)='".$dosen."' OR tbl_jadwalmk.pengampu_id='".$dosen."') ";
		}	
		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_krs.inf_semester),5,5)='".$semester."' OR tbl_krs.inf_semester='".$semester."') ";
		}		

		$sql = $sql . " ORDER BY db_ptiik_apps.tbl_mahasiswa.mahasiswa_id ";
		
		$result = $this->db->query($sql);
		
		return $result;		
	}
	
	function get_is_hadir($mhs=NULL, $absen=NULL){
		$sql = "SELECT db_ptiik_apps.tbl_absen_mhs.is_hadir
					FROM
					db_ptiik_apps.tbl_absen_mhs
					INNER JOIN db_ptiik_apps.tbl_absen ON tbl_absen_mhs.absen_id = tbl_absen.absen_id WHERE 1 ";
		if($mhs) $sql.=" AND db_ptiik_apps.tbl_absen_mhs.mahasiswa_id ='$mhs' ";
		if($absen) $sql.=" AND db_ptiik_apps.tbl_absen_mhs.absen_id ='$absen' ";	
		
		$result = $this->db->getRow($sql);
		
		return $result;		
	}
	
	function get_jadwal_mk($mk=NULL, $dosen=NULL, $semester=NULL, $staff=NULL){
		$sql = "SELECT
					mid(md5(db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id),5,5) as `id`,
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					db_ptiik_apps.tbl_jadwalmk.jadwal_id,
					db_ptiik_apps.tbl_jadwalmk.kelas,
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.pengampu_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id,
					db_ptiik_apps.tbl_jadwalmk.jam_mulai,
					db_ptiik_apps.tbl_jadwalmk.jam_selesai,
					db_ptiik_apps.tbl_jadwalmk.hari,
					db_ptiik_apps.tbl_jadwalmk.ruang_id,
					db_ptiik_apps.tbl_prodi.singkat as `prodi`,
					db_ptiik_apps.tbl_karyawan.nama
				FROM
					db_ptiik_apps.tbl_jadwalmk
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_jadwalmk.pengampu_id = db_ptiik_apps.tbl_pengampu.pengampu_id
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					WHERE 1= 1 AND db_ptiik_apps.tbl_jadwalmk.is_aktif='1' ";
		if($mk){
			$sql = $sql . " AND (tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR mid(md5(tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ";
		}
		
		if($dosen){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.pengampu_id),5,5)='".$dosen."' OR tbl_jadwalmk.pengampu_id='".$dosen."') ";
		}
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_pengampu.karyawan_id='".$staff."') ";
		}		

		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_mkditawarkan.tahun_akademik),5,5)='".$semester."' OR tbl_mkditawarkan.tahun_akademik='".$semester."') ";
		}

		$sql = $sql . " ORDER BY db_ptiik_apps.tbl_prodi.singkat ASC, db_ptiik_apps.tbl_jadwalmk.kelas ASC";
		$result = $this->db->query($sql);
		
		
		return $result;

	}
	
	function get_prodi_mk($mk=NULL, $id=NULL, $semester=NULL, $staff=NULL){
		$sql = "SELECT DISTINCT
					mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) as `id`,	
					db_ptiik_apps.tbl_jadwalmk.prodi_id,					
					db_ptiik_apps.tbl_prodi.singkat as `prodi`
				FROM
					db_ptiik_apps.tbl_jadwalmk
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_jadwalmk.pengampu_id = db_ptiik_apps.tbl_pengampu.pengampu_id
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					WHERE 1= 1 AND db_ptiik_apps.tbl_jadwalmk.is_aktif='1' ";
		if($mk){
			$sql = $sql . " AND (tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR mid(md5(tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ";
		}
		
		if($id){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.pengampu_id),5,5)='".$id."' OR tbl_jadwalmk.pengampu_id='".$id."') ";
		}
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_pengampu.karyawan_id='".$staff."') ";
		}		

		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_mkditawarkan.tahun_akademik),5,5)='".$semester."' OR tbl_mkditawarkan.tahun_akademik='".$semester."') ";
		}

		$sql = $sql . " ORDER BY db_ptiik_apps.tbl_prodi.singkat ASC, db_ptiik_apps.tbl_jadwalmk.kelas ASC";
		$result = $this->db->query($sql);
		
		return $result;

	}
	
	function get_kelas_mk($mk=NULL, $id=NULL, $semester=NULL, $prodi=NULL){
		$sql = "SELECT DISTINCT
					mid(md5(db_ptiik_apps.tbl_jadwalmk.kelas),5,5) as `id`,
					db_ptiik_apps.tbl_jadwalmk.kelas
				FROM
					db_ptiik_apps.tbl_jadwalmk
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_pengampu ON db_ptiik_apps.tbl_jadwalmk.pengampu_id = db_ptiik_apps.tbl_pengampu.pengampu_id
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					WHERE 1= 1 AND db_ptiik_apps.tbl_jadwalmk.is_aktif='1' ";
		if($mk){
			$sql = $sql . " AND (tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR mid(md5(tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ";
		}
		
		if($id){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.pengampu_id),5,5)='".$id."' OR tbl_jadwalmk.pengampu_id='".$id."') ";
		}
		
		if($prodi){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.prodi_id),5,5)='".$prodi."' OR tbl_jadwalmk.prodi_id='".$prodi."') ";
		}		

		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_mkditawarkan.tahun_akademik),5,5)='".$semester."' OR tbl_mkditawarkan.tahun_akademik='".$semester."') ";
		}

		$sql = $sql . " ORDER BY db_ptiik_apps.tbl_jadwalmk.kelas ASC";
		$result = $this->db->query($sql);
	
		return $result;

	}
	
	function get_dosen_id($id,$mkid){
		$sql = "SELECT dosen_id FROM db_ptiik_apps.tbl_pengampu where `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`='".$id."' AND 
				`db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`='".$mkid."' ";
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function get_dosen_mk($semester=NULL, $mk=NULL, $id=NULL, $staff=NULL){
		$sqlz = "SELECT DISTINCT
					`tbl_pengampu`.`is_koordinator`,
					`tbl_pengampu`.`pengampu_id`,
					mid(md5(`tbl_pengampu`.`pengampu_id`),5,5) as `id`,
					`tbl_pengampu`.`karyawan_id`,
					`tbl_pengampu`.`mkditawarkan_id`,
					mid(md5(`tbl_pengampu`.`mkditawarkan_id`),5,5) as `mkid`, 
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					`tbl_karyawan`.`nik`,
					`tbl_karyawan`.`nama`,
					`tbl_karyawan`.`gelar_awal`,
					`tbl_karyawan`.`gelar_akhir`
				FROM
					`db_ptiik_apps`.`tbl_pengampu`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`					
					Inner Join `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
				WHERE 1 = 1 
				";
		$sql = "SELECT DISTINCT
					`tbl_pengampu`.`karyawan_id`,					
					`tbl_karyawan`.`nik`,
					`tbl_karyawan`.`nama`,
					`tbl_karyawan`.`gelar_awal`,
					`tbl_karyawan`.`gelar_akhir`
				FROM
					`db_ptiik_apps`.`tbl_pengampu`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`					
					Inner Join `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
				WHERE 1 = 1 
				";
		if($semester){
			$sql = $sql . " AND `tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		
		if($mk){
			$sql = $sql . " AND (`tbl_mkditawarkan`.`mkditawarkan_id` = '".$mk."' OR mid(md5(`tbl_pengampu`.`mkditawarkan_id`),5,5) = '".$mk."') ";
		}
		
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_pengampu.karyawan_id='".$staff."') ";
		}	
		
		if($id){
			$sql=$sql . " AND (mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`),5,5)='".$id."' OR `db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`='".$id."') ";
		}	
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function get_jumlah_pertemuan($semester=NULL, $mk=NULL, $id=NULL){
		$sql = "SELECT jumlah_pertemuan FROM db_ptiik_apps.tbl_mkditawarkan WHERE 1 = 1 ";
		
		if($semester){
			$sql = $sql . " AND `tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		
		if($mk){
			$sql = $sql . " AND (`tbl_mkditawarkan`.`mkditawarkan_id` = '".$mk."' OR mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),5,5) = '".$mk."') ";
		}
		
		$result = $this->db->getRow($sql);
		
		return $result->jumlah_pertemuan;
		
	}
	
	
	function get_hadir_mhs($mk=NULL, $prodi=NULL, $kelas=NULL, $mhs=NULL, $ishadir=NULL){
		$sql = "SELECT
					Count(db_ptiik_apps.tbl_absen_mhs.is_hadir) as `jml`,
					db_ptiik_apps.tbl_absen_mhs.is_hadir
				FROM
					db_ptiik_apps.tbl_absen
				INNER JOIN db_ptiik_apps.tbl_absen_mhs ON db_ptiik_apps.tbl_absen.absen_id = db_ptiik_apps.tbl_absen_mhs.absen_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_absen.jadwal_id = tbl_jadwalmk.jadwal_id
				GROUP BY
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id,
					db_ptiik_apps.tbl_jadwalmk.kelas,
					db_ptiik_apps.tbl_absen_mhs.mahasiswa_id,
					db_ptiik_apps.tbl_absen_mhs.is_hadir HAVING 1 = 1 ";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.kelas = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.kelas),5,5) = '".$kelas."') ) ";
		}
		
		if($mhs){
			$sql = $sql . " AND db_ptiik_apps.tbl_absen_mhs.mahasiswa_id = '".$mhs."' ";
		}
		
		if($ishadir){
			$sql = $sql . " AND db_ptiik_apps.tbl_absen_mhs.is_hadir = '".$ishadir."' ";
		}
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$str= $result->jml;		
		}else{
			$str= 0;
		}
		
		return $str;
	}	
	
	
				
	function replace_absen($datanya, $datadosen=NULL) {
		//return $this->db->replace('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_absen", $datanya);
			
		if( $result ) {	
			if($datadosen):
				$this->replace_absen_dosen($datadosen);
				return $result;
			else:
				return $result;
			endif;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
			
	}
	
	function replace_absen_dosen($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_absen_dosen", $datanya);
			
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
			
	}
	
	function replace_absen_mhs($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_absen_mhs", $datanya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
		
	
	function update_absen($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_jadwalmk',$datanya,$idnya);
	}
	
	function delete_absen($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
	}
	
	function get_reg_number($jadwal=NULL, $tgl=NULL){
		$sql = "SELECT absen_id FROM db_ptiik_apps.tbl_absen WHERE jadwal_id = '".$jadwal."' AND tgl='".$tgl."' ";
		$row = $this->db->getRow( $sql );	
		
		if($row){
			$result = $row->absen_id;
		}else{
		
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(absen_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_absen WHERE left(absen_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	function add_nol($str){
		if($str < 10){
			$result = "0".$str;
		}else{
			$result = $str;
		}
		
		return $result;
	}
	
	
}