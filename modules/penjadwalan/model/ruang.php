<?php
class model_ruang extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
				
	function read($keyword, $page = 1, $perpage = 10, $id=NULL, $jeniskegiatan=NULL, $month=NULL, $year=NULL, $ruang=NULL) {
	
		$keyword 	= $this->db->escape($keyword);
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
				mid(md5(`tbl_detailjadwalruang`.`detail_id`),5,5) as `id`, 
				`tbl_detailjadwalruang`.`detail_id`,
				`tbl_detailjadwalruang`.`jadwal_id`,
				`tbl_detailjadwalruang`.`ruang`,
				`tbl_detailjadwalruang`.`hari`,
				`tbl_detailjadwalruang`.`inf_hari`,
				`tbl_detailjadwalruang`.`tgl`,
				`tbl_detailjadwalruang`.`jam_mulai`,
				`tbl_detailjadwalruang`.`jam_selesai`,
				`tbl_detailjadwalruang`.`inf_jenis_kegiatan`,
				`tbl_detailjadwalruang`.`inf_jadwal_id`,
				`tbl_detailjadwalruang`.`is_aktif`,
				`tbl_detailjadwalruang`.`keterangan`,
				`tbl_jadwalruang`.`kegiatan`,
				`tbl_jadwalruang`.`jenis_kegiatan_id`,
				`tbl_jadwalruang`.`is_kuliah`,
				`tbl_jadwalruang`.`repeat_on`,
				`tbl_jadwalruang`.`tahun_akademik`,
				`tbl_jadwalruang`.`catatan`,
				`tbl_jadwalruang`.`ruang_id`
				FROM
				db_ptiik_apps.`tbl_detailjadwalruang`
				Inner Join db_ptiik_apps.`tbl_jadwalruang` ON db_ptiik_apps.`tbl_detailjadwalruang`.`jadwal_id` = db_ptiik_apps.`tbl_jadwalruang`.`jadwal_id`
				WHERE 1 = 1 
				";
		if($id){
			$sql=$sql . " AND mid(md5(`tbl_detailjadwalruang`.`detail_id`),5,5)='".$id."' ";
		}
		
		if($jeniskegiatan){
			$sql=$sql . " AND `tbl_detailjadwalruang`.`inf_jenis_kegiatan`='".$jeniskegiatan."' ";
		}
		
		if($month){
			$sql=$sql . " AND month(`tbl_detailjadwalruang`.`tgl`)='".$month."' ";
		}
		
		if($year){
			$sql=$sql . " AND year(`tbl_detailjadwalruang`.`tgl`)='".$year."' ";
		}
		
		if($ruang){
			$sql=$sql . " AND `tbl_detailjadwalruang`.`ruang`='".$ruang."' ";
		}
		
		if($keyword){
			//$sql=$sql . " AND (date_format(`tbl_detailjadwalruang`.`tgl`,'%Y-%m-%d') ='".$keyword."' OR ) ";
			//$sql=$sql . " AND DATE_SUB(CURDATE(),INTERVAL 30 DAY) ";
		}
		
		$sql= $sql . "ORDER BY db_ptiik_apps.`tbl_detailjadwalruang`.`tgl` DESC LIMIT $offset, $perpage";
	
		$result = $this->db->query( $sql );		
		
		return $result;	
	}
	
	
	
	/* draw calendar */
	function drawRuang($month, $year, $style, $ruang,$jeniskegiatan) {		
		 if(($month == NULL) || ($year == NULL))
         {
             $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
         }else{
			 $title = date('F Y',mktime(0,0,0,$month,1,$year));
		 }

         if((substr($month, 0, 1)) == 0)
         {
            $tempMonth = substr($month, 1);                                                                                              
            $month = $tempMonth;
         }
     
         $str = '<table cellpadding="0" cellspacing="0" class="table table-bordered">';
        
         $headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
         $str.= '<tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr>';

         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         /* row for week one */
         $str.= '<tr class="'. $style .'-row">';
    
         /* print "blank" days until the first of the current week */
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
            
                 /* add in the day number */
                 $str.= '<div class="'. $style .'-day-number">'.$list_day.'</div>';
				 
				 $data = $this->getEvent($list_day, $month, $year, $running_day, $ruang,$jeniskegiatan);
				 
				 if(count($data)>0){
					 foreach($data as $dt):							
							$str.= '<div class="'. $txtstyle .'-text"><small><a href=ruang/viewagenda/'.$dt->ruang.' class="tooltip-toggle" id="tooltip" data-toggle="tooltip" data-placement="top" 
									title="'.$dt->kegiatan.'&#013;';
							if($dt->catatan){
							$str.= $dt->catatan.'&#013;R.';
							}
							$str.= $dt->ruang.' ('.$dt->jam_mulai.' - '.$dt->jam_selesai.')">'.ucWords($dt->jeniskegiatan).' ('.$dt->jam_mulai.'- R.'.$dt->ruang.')</a></small></div>';						
					 endforeach;
				 }
                
             $str.= '</td>';
			 
		
			 
			 
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
    
         /* finish the rest of the days in the week */
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
    
         /* final row */
         $str.= '</tr>';
    
         /* end the table */
         $str.= '</table>';
		 
		 return $str;
     }

	
	function getEvent($day,$month,$year, $hari, $ruang,$jeniskegiatan){
		$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		$tglend	= $year."-".$month."-".($day+1);
		$runday	= $this->get_hari_by_running_day($hari);
		
		
		$sqlx="SELECT DISTINCT
				`db_ptiik_apps`.`tbl_jadwalruang`.`ruang_id`,
				if(`db_ptiik_apps`.`tbl_jadwalruang`.`is_kuliah`=1,'Kuliah',`db_ptiik_apps`.`tbl_jeniskegiatan`.`keterangan`) as `kegiatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`tahun_akademik`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`hari`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`,'%H:%i') as `jam_mulai`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`
				FROM
				`db_ptiik_apps`.`tbl_detailjadwalruang`
				Inner Join `db_ptiik_apps`.`tbl_jadwalruang` ON `db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id` = `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id`
				Left Join `db_ptiik_apps`.`tbl_jeniskegiatan` ON `db_ptiik_apps`.`tbl_jadwalruang`.`jenis_kegiatan_id` = `db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`
			 WHERE `db_ptiik_apps`.`tbl_detailjadwalruang`.`is_aktif`='1' AND 
					if(`db_ptiik_apps`.`tbl_jadwalruang`.`is_kuliah`=1, `db_ptiik_apps`.`tbl_detailjadwalruang`.`hari`='".$runday."', date_format(`tbl_detailjadwalruang`.`tgl`,'%Y-%m-%d')='".$tgl."') 
			";
		$sql = "SELECT
				`tbl_detailjadwalruang`.`detail_id`,
				`tbl_detailjadwalruang`.`jadwal_id`,
				`tbl_detailjadwalruang`.`ruang`,
				`tbl_detailjadwalruang`.`hari`,
				`tbl_detailjadwalruang`.`tgl`,
				`tbl_detailjadwalruang`.`inf_hari`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`,'%H:%i') as `jam_mulai`,
				time_format(`tbl_detailjadwalruang`.`jam_selesai`,'%H:%s') as `jam_selesai`,
				`tbl_detailjadwalruang`.`inf_jenis_kegiatan` as `jeniskegiatan`,
				`tbl_jadwalruang`.`kegiatan`,
				`tbl_jadwalruang`.`catatan`
				FROM
				`db_ptiik_apps`.`tbl_detailjadwalruang` 
				Inner Join `db_ptiik_apps`.`tbl_jadwalruang` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`
				WHERE date_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl`,'%Y-%m-%d')='".$tgl."'  
				"; 
		if($ruang){
			$sql = $sql." AND `db_ptiik_apps`.`tbl_detailjadwalruang`.`ruang` = '".$ruang."' ";
		}	

		if($jeniskegiatan){
			$sql=$sql . " AND `tbl_detailjadwalruang`.`inf_jenis_kegiatan`='".$jeniskegiatan."' ";
		}	
		
		$sql = $sql. "ORDER BY `db_ptiik_apps`.`tbl_detailjadwalruang`.`ruang` ASC, `db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai` ASC";		
	
		return $this->db->query( $sql );
	
	}
	
	function draw_grid_ruang($tgl=NULL, $kategori=NULL,$ruangid=NULL, $style){
	
		 
		$str = '<table cellpadding="0" cellspacing="0" class="table table-bordered">';

		/* table headings */	
			/*$headings = array('Ruang/Jam','08','08:00','Rabu','Kamis','Jumat','Sabtu');

			$str.= '<tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
			 .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr>';*/

		$blokwaktu	= $this->get_blok_waktu();

		$str.= '<tr class="'. $style .'-row"><td class="'. $style .'-day-head"><small>J/R</small></td>';
			foreach($blokwaktu as $dt):
				$str.= '<td class="'. $style .'-day-head"><small>'.$dt->jam_mulai.'</small></td>';
			endforeach;
		$str.= '</tr>';
	
		if($tgl){
			$ruang = $this->get_ruang($kategori, $ruangid);
			date_default_timezone_set('Asia/Jakarta');
			$jam=date("H:i", strtotime("now"));
			$tsekarang=date('Y-m-d');
			//echo $jam;
			$waktu = str_replace(":", "",$jam);
			
		
						
			foreach($ruang as $dt):
				$str.= '<tr>';
					$str.= '<td><a href="#" class="tooltip-toggle" id="drop4" role="button" data-toggle="tooltip" data-placement="right" 
					data-html="true" title="'.$dt->id.' - '.$dt->value.'">'.$dt->id.'</a></td>';
					foreach($blokwaktu as $data):
						$cek 	= $this->cek_ketersediaan($tgl, $dt->id, $data->jam_mulai);
						$popid 	= str_replace(".","",$dt->id).substr($data->jam_mulai,0,2);
						
						$str.= '<td>';
							
						if($cek){	
							$tglo=str_replace("-", "",$tgl);
							
							switch($tglo){
								case ($tglo==str_replace("-", "",$tsekarang) && ($waktu<str_replace(":","",$data->jam_mulai))):
									$strclass= "label-important";
									$strtitle= "Reserved";
									$stricon = "x";
								break;
								case ($tglo==str_replace("-", "",$tsekarang) && ($waktu>str_replace(":","",$data->jam_mulai))):
									$strclass= "label-success";
									$strtitle= "Finished";
									$stricon = "v";
								break;
								case ($tglo<str_replace("-", "",$tsekarang)):
									$strclass= "label-success";
									$strtitle= "Finished";
									$stricon = "v";
								break;
								case ($tglo>str_replace("-", "",$tsekarang)):
									$strclass= "label-important";
									$strtitle= "Reserved";
									$stricon = "x";
								break;
							}
							
							
							/*$str.= '<a href="#" class="tooltip-toggle pop" id="pop'.$popid.'" role="button" data-toggle="popover" data-placement="right" data-html="true" 
									title="'.$strtitle.'"><span class="label '.$strclass.' pull-center">&nbsp;&nbsp;'.$stricon.'&nbsp;&nbsp;</span></a>';
							$str.='<div id="pop'.$popid.'_content" class="popSourceBlock">
										<div class="popTitle">
											Reserved
										</div>
										<div class="popContent">';											
											$i=0;
											foreach($cek as $cek):
												$i++;
												//$str.= '<p><small><b>'.$cek->kegiatan.'</b>';
												if($cek->catatan){
													//$str.= '<br><em>'. $cek->catatan.'</em>';
												}											
												//$str.= '<br><b class="text-info">R. '.$dt->id. ' ('.$cek->jam_mulai.' - '.$cek->jam_selesai.')</b></small></p>';
											endforeach;
							$str.='		</div>
									</div>';
							*/
							
						}else{
							$str.= '<div align=center>&nbsp;</div>';
						}
						
						$str.= '</td>';
					endforeach;
				$str.= '</tr>';
			endforeach;
			
		}
		
         /* end the table */
         $str.= '</table>';
		 
		 return $str;
	}
	
	function cek_ketersediaan($tgl, $ruang, $jam){
		$sql = "SELECT 
					`tbl_detailjadwalruang`.`detail_id`,
					`tbl_jadwalruang`.`kegiatan`,
					`tbl_jadwalruang`.`catatan`,
					time_format(`tbl_detailjadwalruang`.`jam_mulai`,'%H:%i') as `jam_mulai`,
					time_format(`tbl_detailjadwalruang`.`jam_selesai`,'%H:%i') as `jam_selesai`
				FROM 
					db_ptiik_apps.tbl_detailjadwalruang 
				Inner Join db_ptiik_apps.`tbl_jadwalruang` ON `tbl_detailjadwalruang`.`jadwal_id` = `tbl_jadwalruang`.`jadwal_id`
				WHERE 
					`tbl_detailjadwalruang`.tgl='".$tgl."' AND `tbl_detailjadwalruang`.ruang='".$ruang."' 
					AND ((`tbl_detailjadwalruang`.jam_mulai <= '".$jam."'  AND `tbl_detailjadwalruang`.jam_selesai > '".$jam."') 
					OR (`tbl_detailjadwalruang`.jam_mulai >= '".$jam."'  AND `tbl_detailjadwalruang`.jam_selesai < '".$jam."') 
					OR (`tbl_detailjadwalruang`.jam_selesai = '".$jam."')) AND db_ptiik_apps.tbl_detailjadwalruang.is_aktif=1";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_blok_waktu(){
		$sql = "SELECT distinct time_format(jam_mulai,'%H:%i') as `jam_mulai`, time_format(jam_selesai,'%H:%i') as `jam_selesai` FROM db_ptiik_apps.tbl_blokwaktu ORDER BY db_ptiik_apps.tbl_blokwaktu.jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_ruang($kategori=NULL, $ruang=NULL){
		$sql = "SELECT ruang as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_ruang WHERE  is_aktif = 1 ";
		
		if($kategori){
			$sql = $sql . " AND kategori_ruang = '".$kategori."' ";
		}
		
		if($ruang){
			$sql = $sql . " AND ruang = '".$ruang."' ";
		}
		
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_hari_by_running_day($str){
		switch($str){
			case '0':
				$result	= "minggu";
			break;
			case '1':
				$result = "senin";
			break;
			case '2':
				$result = "selasa";
			break;
			case '3':
				$result = "rabu";
			break;
			case '4':
				$result = "kamis";
			break;
			case '5':
				$result = "jumat";
			break;
			case '6':
				$result = "sabtu";
			break;
		}
		
		return $result;
	}
	
	function addnol($string){
		if($string < 10){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	
}