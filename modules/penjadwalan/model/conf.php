<?php
class model_conf extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_fakultas(){
		$sql = "SELECT tbl_fakultas.fakultas_id, tbl_fakultas.keterangan, tbl_fakultas.kode_fakultas kode
				FROM db_ptiik_apps.tbl_fakultas
				ORDER BY tbl_fakultas.keterangan";
		return $this->db->query($sql);
	}
	
	
	
	function  get_mhs_mk($jadwal=NULL){
		$sql = "SELECT DISTINCT 
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_mahasiswa.angkatan,
					db_ptiik_apps.tbl_jadwalmk.kelas,
					db_ptiik_apps.tbl_jadwalmk.pengampu_id,
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id,
					db_ptiik_apps.tbl_jadwalmk.is_praktikum
					FROM
					db_ptiik_apps.tbl_krs
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_krs.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id 
						  AND db_ptiik_apps.tbl_krs.kelas = db_ptiik_apps.tbl_jadwalmk.kelas
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_jadwalmk.prodi_id 
						AND db_ptiik_apps.tbl_krs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					";
		if($jadwal){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.jadwal_id = '".$jadwal."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.jadwal_id),5,5) = '".$jadwal."') ) ";
		}
		
		$sql = $sql . " ORDER BY db_ptiik_apps.tbl_mahasiswa.mahasiswa_id ";
		
		$result = $this->db->query($sql);
		
		return $result;		
	}
	
	function  get_mhs_praktikum($jadwal=NULL, $kelompok=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_mahasiswa.angkatan,
					db_ptiik_apps.tbl_jadwalmk.kelas,
					db_ptiik_apps.tbl_jadwalmk.pengampu_id,
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id,
					db_ptiik_apps.tbl_jadwalmk.is_praktikum,
					db_ptiik_apps.tbl_praktikum_mhs.kelompok_id,
					db_ptiik_apps.tbl_praktikum_mhs.praktikan_id,
					db_ptiik_apps.tbl_krs.krs_id
					FROM
					db_ptiik_apps.tbl_krs
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_krs.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id AND db_ptiik_apps.tbl_krs.kelas = db_ptiik_apps.tbl_jadwalmk.kelas
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_jadwalmk.prodi_id AND db_ptiik_apps.tbl_krs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					INNER JOIN db_ptiik_apps.tbl_praktikum_mhs ON db_ptiik_apps.tbl_krs.krs_id = db_ptiik_apps.tbl_praktikum_mhs.inf_krs_id  WHERE db_ptiik_apps.tbl_jadwalmk.is_praktikum='1'  
					";
		if($jadwal) $sql.= " AND tbl_jadwalmk.jadwal_id='$jadwal' ";
		if($kelompok) $sql.= " AND tbl_praktikum_mhs.kelompok_id='$kelompok' ";
		
		return $this->db->query($sql);
	}
	
	function get_id_dosen_tmp($str=NULL){
		$sql= "SELECT karyawan_id FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE nama like '".$str."%' ";
		
		$result = $this->db->query( $sql );
				
		return $result;	
		
	}
	
	/* master konfigurasi */
	function read() {		
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_config`.`tahun`,
					`db_ptiik_apps`.`tbl_config`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_config`.`is_pendek`,
					`db_ptiik_apps`.`tbl_config`.`kode_skripsi`,
					`db_ptiik_apps`.`tbl_config`.`is_aktif`
				FROM
					`db_ptiik_apps`.`tbl_config`					
				WHERE is_aktif='1'
				";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jenjang pendidikan */
	function  get_jenjang_pendidikan(){
		$sql = "SELECT `db_ptiik_apps`.`tbl_jenjangpendidikan`.`jenjang_pendidikan` FROM `db_ptiik_apps`.`tbl_jenjangpendidikan`";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jabatan */
	function  get_jabatan(){
		$sql = "SELECT `db_ptiik_apps`.`tbl_jabatan`.`jabatan_id`, `db_ptiik_apps`.`tbl_jabatan`.`keterangan` FROM `db_ptiik_apps`.`tbl_jabatan` ORDER BY urut ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester  */
	function get_semester(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					ORDER BY `db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` DESC, 
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik` DESC
					";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester aktif */
	function get_semester_aktif(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					WHERE
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` =  '1'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	function get_semester_id($str=NULL){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					WHERE
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik` =  '".$str."'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	/* master program studi atau jurusan*/
	function get_prodi($term=NULL){
		$sql= "SELECT prodi_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_prodi` WHERE keterangan like '%".$term."%' ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master unit kerja atau struktur*/
	function get_unit($term=NULL){
		$sql= "SELECT unit_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_unit_kerja` WHERE keterangan like '%".$term."%'  ORDER BY jenis ASC, nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	/*master mahasiswa */
	function get_mahasiswa($term=NULL){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `value` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mahasiswa */
	function get_nama_mahasiswa($term=NULL){
		$sql= "SELECT nama FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE mahasiswa_id = '".$term."'  ";
		$result = $this->db->query( $sql );
	
		if($result){
			foreach($result as $dt){
				$strid = $dt->nama;
			}
		}else{
			$strid="-";
		}
		
		return $strid;	
	}

/*master id dosen */
	function get_id_dosen($str=NULL){
		$sql= "SELECT karyawan_id FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE nama = '".$str."' ";
		$result = $this->db->query( $sql );
		
		if($result){
			foreach($result as $dt){
				$strid = $dt->karyawan_id;
			}
		}else{
			$strid="-";
		}
		return $strid;			
	}
		
	
	/*master dosen */
	function get_dosen($term=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif NOT IN ('keluar', 'meninggal')
				ORDER BY nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_karyawan($term=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%'  AND is_aktif ='aktif'
				ORDER BY nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master dosen */
	function get_staff($term=NULL){
		$sql= "SELECT nama as `tag` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_aktif='aktif' 
				ORDER BY nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_staff_tmp($term=NULL){
		$sql = "SELECT karyawan_id as `id`, nama as `value`
				FROM `db_ptiik_apps`.`tbl_karyawan` WHERE nama LIKE '%".$term."%' AND  is_aktif NOT IN ('keluar','meninggal') "; 
		$result = $this->db->query( $sql );
		return $result;
	}	
	
	function get_mhs($term=NULL){
		$sql= "SELECT nama as `tag` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master pengampu */
	function get_pengampu($term=NULL){
		$sql= "SELECT nama as `value`, karyawan_id as `id` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif='aktif' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master nama mk */
	function get_namamk($term=NULL){
		$sql= "SELECT namamk_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_namamk` 
					WHERE keterangan like '%".$term."%' 
				ORDER BY keterangan ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master mk */
	function get_mk($term=NULL){
		$sql= "SELECT `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` as `id`, 
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ', `db_ptiik_apps`.`tbl_namamk`.`keterangan`,'(',`db_ptiik_apps`.`tbl_matakuliah`.`sks`,' sks)') as `value` FROM `db_ptiik_apps`.`tbl_matakuliah`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id` 
					WHERE concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
				ORDER BY `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/* master jam mulai*/
	function  get_jam_mulai($term=NULL, $ujian=NULL){
		$sql = "SELECT jam_mulai as `id`, time_format(jam_mulai,'%H:%i') as `value` FROM db_ptiik_apps.tbl_jam 
				WHERE jam_mulai like '%".$term."%'  ";
		if($ujian){
			$sql = $sql. " AND is_ujian='1' ";
		}else{
			$sql = $sql. " AND is_aktif='1' ";
		}	
		
		$sql = $sql. " ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jam selesai*/
	function  get_jam_selesai( $term=NULL, $ujian=NULL){
		$sql = "SELECT jam_mulai as `id`, time_format(jam_selesai,'%H:%i') as `value` FROM db_ptiik_apps.tbl_jam  WHERE jam_selesai like '%".$term."%' ";
		if($ujian){
			$sql = $sql. " AND is_ujian='1' ";
		}else{
			$sql = $sql. " AND is_aktif='1' ";
		}
		
		$sql = $sql. " ORDER BY jam_selesai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master hari*/
	function  get_hari($term=NULL){
		$sql = "SELECT id as `id`, hari as `value` FROM db_ptiik_apps.tbl_hari WHERE hari like '%".$term."%' ORDER BY tbl_hari.id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master get nama kegiatan */
	function  get_nama_kegiatan($str=NULL){
		$sql = "SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id = '".$str."' ";
		$result = $this->db->query( $sql );
		
		foreach($result as $dt):
			$strvalue	= $dt->keterangan;
		endforeach;
		
		return $strvalue;
	}
	
	
	/* master jam general*/
	function  get_blok_waktu($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_mulai as `value` FROM db_ptiik_apps.tbl_blokwaktu
				WHERE jam_mulai like '%".$term."%'
				ORDER BY tbl_blokwaktu.jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master ruang*/
	function  get_ruang($term=NULL, $kategori=NULL, $fak=NULL){
		$sql = "SELECT ruang_id as `id`, concat(kode_ruang, ' - ', keterangan) as `value` FROM db_ptiik_apps.tbl_ruang WHERE 1=1 ";
		
		if($kategori){
			$sql = $sql . "AND kategori_ruang = '".$kategori."' ";
		}
		
		if($term){
			$sql = $sql . "AND keterangan like '%".$term."%' ";
		}
		
		if($fak) $sql.= " AND fakultas_id='$fak' ";
		
		$sql = $sql . "ORDER BY kode_ruang ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kategori ruang*/
	function  get_kategori_ruang($term=NULL){
		$sql = "SELECT kategori_ruang as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_kategoriruang WHERE keterangan like '%".$term."%' ORDER BY kategori_ruang ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kelas*/
	function  get_kelas($term=NULL){
		$sql = "SELECT kelas_id as `id`, kelas_id as `value` FROM db_ptiik_apps.tbl_kelas WHERE keterangan like '%".$term."%' ORDER BY kelas_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master nama dosen */
	function get_nama_dosen($id=NULL){
		$sql= "SELECT nama , karyawan_id ,nik FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE karyawan_id = '".$id."' ";
		$result = $this->db->getRow( $sql );
		
		if($result):
			$str	= $result->nama;
		else:
			$str="";
		endif;
		
		return $str;	
		
	}
	
	/*master nama dosen */
	function get_dosen_by_nama($id){
		$sql= "SELECT nama , karyawan_id ,nik FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE nama LIKE '%".$id."%' ";
		$result = $this->db->query( $sql );
		
		echo $sql."<br>";
		foreach($result as $dt):
			$str	= $dt->karyawan_id;
		endforeach;
		
		return $str;	
		
	}
	
		
	/*master jadwal dosen */
	function get_jadwal_dosen(){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`
				FROM
				`db_ptiik_apps`.`vw_jadwaldosen`
				";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk ditawarkan */
	function get_mkditawarkan($ispraktikum=NULL,$semester=NULL,$term=NULL, $mkid=NULL){
		$sql="SELECT
				`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` as `id`,
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ' ,`db_ptiik_apps`.`tbl_namamk`.`keterangan`) as `value`,
					`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,
					`db_ptiik_apps`.`tbl_namamk`.`keterangan` AS `namamk`,
					`db_ptiik_apps`.`tbl_matakuliah`.`sks`,
					`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
				FROM
				`db_ptiik_apps`.`tbl_mkditawarkan`
				Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
				Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`		
			  WHERE concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
			";
			
		if($ispraktikum){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`is_praktikum`= '".$ispraktikum."' ";
		}
		if($semester){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` ='".$semester."' ";
		}
		
		if($mkid):
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`= '".$mkid."' ";
			
			$sql = $sql . " ORDER BY `db_ptiik_apps`.`tbl_namamk`.`keterangan` ASC";
				
			$result = $this->db->getRow( $sql );
		else:
			$sql = $sql . " ORDER BY `db_ptiik_apps`.`tbl_namamk`.`keterangan` ASC";
				
			$result = $this->db->query( $sql );
		endif;		
	//echo $sql;
		
		return $result;	
	}
	
	/* master jenis kegiatan*/
		function get_jenis_kegiatan($id=NULL){
		$sql = "SELECT jenis_kegiatan_id, keterangan FROM db_ptiik_apps.`tbl_jeniskegiatan`  WHERE 1 = 1 ";
		
		if($id){
			$sql = $sql. " AND jenis_kegiatan_id IN ('kuliah', 'praktikum', 'bimbingan', 'uts', 'uas') ";
		}else{
			$sql = $sql. " AND jenis_kegiatan_id IN ('kuliah', 'praktikum') ";
		}
		
		$sql = $sql . " ORDER BY `tbl_jeniskegiatan`.keterangan ASC ";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	/*master mk diampu */
	
	
	function get_mk_diampu($id=NULL,$mk=NULL, $semester=NULL){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,
					`db_ptiik_apps`.`tbl_namamk`.`keterangan` AS `namamk`,
					`db_ptiik_apps`.`tbl_matakuliah`.`sks`,
					`db_ptiik_apps`.`tbl_pengampu`.`is_koordinator`,
					`db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`,
					`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`,
					`db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`
				FROM
					`db_ptiik_apps`.`tbl_mkditawarkan`
					Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`
					Inner Join `db_ptiik_apps`.`tbl_pengampu` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE `db_ptiik_apps`.`tbl_pengampu`.`is_aktif` = '1'  
				";
				
		if($id){
			$sql=$sql . " AND (mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`),5,5)='".$id."' OR `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`='".$id."') ";
		}

		if($mk){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`='".$mk."' ";
		}	
		if($semester){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik`='".$semester."' ";
		}	
	
		$sql = $sql . " ORDER BY `db_ptiik_apps`.`tbl_karyawan`.`nama` ASC, `db_ptiik_apps`.`tbl_namamk`.`keterangan` ASC";

		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function get_dosen_diampu($id=NULL,$mk=NULL, $semester=NULL){
		$sql = "SELECT DISTINCT 				
					`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`,
					`db_ptiik_apps`.`tbl_pengampu`.`pengampu_id`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`
				FROM
					`db_ptiik_apps`.`tbl_mkditawarkan`
					Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`
					Inner Join `db_ptiik_apps`.`tbl_pengampu` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE `db_ptiik_apps`.`tbl_pengampu`.`is_aktif` = '1'  
				";
				
		if($id){
			$sql=$sql . " AND (mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`),5,5)='".$id."' OR `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`='".$id."') ";
		}

		if($mk){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`='".$mk."' ";
		}	
		if($semester){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik`='".$semester."' ";
		}	

		$sql = $sql . " ORDER BY `db_ptiik_apps`.`tbl_karyawan`.`nama` ASC, `db_ptiik_apps`.`tbl_namamk`.`keterangan` ASC";

		$result = $this->db->query($sql);
		
		return $result;
	}
	
	
	function get_pengampu_kegiatan($id=NULL,$mk=NULL, $semester=NULL){
		$sql = "SELECT DISTINCT 				
					`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`
				FROM
					`db_ptiik_apps`.`tbl_mkditawarkan`
					Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`
					Inner Join `db_ptiik_apps`.`tbl_pengampu` ON `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE `db_ptiik_apps`.`tbl_pengampu`.`is_aktif` = '1'  
				";
				
		if($id){
			$sql=$sql . " AND (mid(md5(`db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`),5,5)='".$id."' OR `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`='".$id."') ";
		}

		if($mk){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`='".$mk."' ";
		}	
		if($semester){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik`='".$semester."' ";
		}	

		$sql = $sql . " ORDER BY `db_ptiik_apps`.`tbl_karyawan`.`nama` ASC, `db_ptiik_apps`.`tbl_namamk`.`keterangan` ASC";

		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function get_dosen_id($id=NULL,$mkid=NULL){
		$sql = "SELECT pengampu_id FROM db_ptiik_apps.tbl_pengampu where `db_ptiik_apps`.`tbl_pengampu`.`karyawan_id`='".$id."' AND 
				`db_ptiik_apps`.`tbl_pengampu`.`mkditawarkan_id`='".$mkid."' ";
		$result = $this->db->getRow($sql);
		
		if($result){			
			$dosen	= $result->pengampu_id;
			
		}else{
			$dosen = "";
		}
		
		return $dosen;
	}
	
	function replace_conf($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_config',$datanya);
	}
	
	function replace_semester($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tahunakademik',$datanya);
	}
	
	function update_conf($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_config',$datanya,$idnya);
	}
		
	function update_semester($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_tahunakademik',$datanya,$idnya);
	}
	
	public function log($tablename, $datanya, $username, $action){
		$return_arr = array();
		array_push($return_arr,$datanya);		
			
		$data['user']		= $username;
		$data['session']	= session_id();
		$data['tgl']		= date("Y-m-d H:i:s");
		$data['reference']	= $_SERVER['HTTP_REFERER'];
		$data['table_name']	= $tablename;
		$data['field']		= json_encode($return_arr);	
		$data['action']		= $action;			
		
		$result = $this->db->insert("coms`.`coms_log", $data);
		
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) return false;
	}
	
	public function getHari($str=NULL){
		switch($str){
			case '1':
				$strout	= 'senin';
			break;
			case '2':
				$strout	= 'selasa';
			break;
			case '3':
				$strout	= 'rabu';
			break;
			case '4':
				$strout	= 'kamis';
			break;
			case '5':
				$strout	= 'jumat';
			break;
			case '6':
				$strout	= 'sabtu';
			break;
			case '7':
				$strout	= 'minggu';
			break;
		}
		
		return $strout;
	}
	
	public function getNHari($str=NULL){
		switch(strToLower($str)){
			case 'senin':
				$strout	= '1';
			break;
			case 'selasa':
				$strout	= '2';
			break;
			case 'rabu':
				$strout	= '3';
			break;
			case 'kamis':
				$strout	= '4';
			break;
			case 'jumat':
				$strout	= '5';
			break;
			case 'sabtu':
				$strout	= '6';
			break;
			case 'minggu':
				$strout	= '7';
			break;
		}
		
		return $strout;
	}
	
	public function replace_jadwal_ruang($datanya) {
		//var_dump($datanya);
		return $this->db->replace('db_ptiik_apps`.`tbl_pemakaian_ruang',$datanya);
	}
	
	public function replace_jadwal_praktikum($datanya) {
		
	//	var_dump($this->db->replace('db_ptiik_apps`.`tbl_praktikum',$datanya));
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum',$datanya);
	}
	
	public function replace_detail_jadwal_ruang($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_pemakaian_ruang_detail',$datanya);
	}
	
	function delete_jadwal_ruang($datanya){
		//var_dump($datanya);
		return $this->db->delete('db_ptiik_apps`.`tbl_pemakaian_ruang',$datanya);
	}
	
	function delete_jadwal_ruang_detail($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_pemakaian_ruang_detail',$datanya);
	}
	
	function update_jadwal_valid($jadwalid, $tgl, $hari, $jammulai, $jamselesai, $ruang, $infjeniskegiatan, $dosen, $kelas, $mk){
		$sql = "SELECT detail_id, jadwal_id FROM db_ptiik_apps.tbl_pemakaian_ruang_detail WHERE inf_jenis_kegiatan='".$infjeniskegiatan."' AND hari='".$hari."' AND ruang='".$ruang."' AND dosen_id='".$dosen."' 
					AND (time_format(jam_selesai,'%H:%i')>time_format('".$jammulai."','%H:%i') AND time_format(jam_mulai,'%H:%i')<>time_format('".$jammulai."','%H:%i') ) ";
		$result = $this->db->query( $sql );		
		//echo $sql;
		if($result){
			foreach($result as $dt):
				$this->update_detail_jadwal_ruang($dt->detail_id, $dt->jadwal_id);
			endforeach;
		}
		
		return $result;
	}
	
	function update_detail_jadwal_ruang($id=NULL, $jadwal=NULL){
		if($id){
			$where['detail_id'] = $id;
			$data['is_aktif'] = 0;
			$wherejadwal['jadwal_id'] = $jadwal;
			$datavalid['is_valid'] = 0;
			
			$this->db->update('db_ptiik_apps`.`tbl_pemakaian_ruang', $datavalid, $wherejadwal);
			return $this->db->update('db_ptiik_apps`.`tbl_pemakaian_ruang_detail', $data, $where);
		}
	}
	
	function delete_peserta_kegiatan($id) {
		return $this->db->delete("db_ptiik_apps`.`tbl_aktifitas", $id);
		
	}
	
	function replace_event_staff($datanya){		
		$result= $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$datanya);
		
		return $result;
	}
	
	function get_aktifitas_id($kegiatan=NULL, $karyawan=NULL, $tgl=NULL, $selesai=NULL, $jam=NULL, $jselesai=NULL, $judul=NULL, $mk=NULL, $catatan=NULL, $ruang=NULL){
		$sql = " SELECT aktifitas_id FROM db_ptiik_apps.tbl_aktifitas WHERE jenis_kegiatan_id='".$kegiatan."' AND karyawan_id = '".$karyawan."' AND tgl='".$tgl."' AND jam_mulai='".$jam."' AND judul='".$judul."'";
		if($mk){
			$sql.= " AND nama_mk='".$mk."' ";
		}
		if($catatan){
			$sql.= " AND catatan='".$catatan."' ";
		}
		if($ruang){
			$sql.= " AND inf_ruang='".$ruang."' ";
		}
		$rs = $this->db->getRow( $sql );
		if($rs){
			$result = $rs->aktifitas_id;
		}else{
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '000000' , CAST(IFNULL(MAX(CAST(right(aktifitas_id,6) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_aktifitas WHERE left(aktifitas_id,6)='".date("Ym")."' "; 
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
}