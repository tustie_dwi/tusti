<?php
class penjadwalan_jadwal extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 		
	}
		
	function  index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$this->write();		
	}
	
	function write($note=NULL, $style = "calendar"){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa') || ($this->coms->authenticatedUser->role =='asisten')) $this->coms->no_privileges();
		
		if(isset($_POST['b_add'])){
			switch($_POST['cmbjenis']){
				case 'kuliah':
					$this->save_kuliah();
				break;
				
				case 'praktikum':
					$this->save_kuliah(1);
				break;
				
				case 'uts':
					$this->save_ujian('uts');
				break;
				
				case 'uas':
					$this->save_ujian('uas');
				break;
				
				default :
					$this->save_kegiatan();
				break;
			}
		}else{
			if(isset($_POST['cmbjenis'])){
				switch($_POST['cmbjenis']){
					case 'kuliah':
						$this->jadwal();
					break;
					
					case 'praktikum':
						$this->jadwal();
					break;
					
					case 'uts':
						$this->ujian('uts');
					break;
					
					case 'uas':
						$this->ujian('uas');
					break;
					
					default :
						$this->jadwal();
					break;
				}
			}else{
				$this->jadwal();
			}
		}		
	}
	
	
	function jadwal(){
	
		$mconf	 = new model_conf();
		
		if(isset($_POST['cmbsemester'])){
			$data['semesterid'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['semesterid'] = "";
		}
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}	

		if(isset($_POST['cmbfak'])){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "";
		}				

	
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}		
						
					
		$data['kegiatan'] 	= $mconf->get_jenis_kegiatan('akademik');
		$data['semester']	= $mconf->get_semester();
		$data['hari']   	= $mconf->get_hari();
		$data['fakultas']	= $mconf->get_fakultas();
		$data['repeatin']	= "monthly";
		
		$data['posts']		= "";
			
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		/*$this->coms->add_style('js/bootstrap/tag.css');
		$this->coms->add_script('js/bootstrap/tag.js');*/
		$this->coms->add_script('tagmanager/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
				
		$this->add_script('js/jadwal.js');
		
		$this->view('jadwal/index.php', $data);
	}
	
	function ujian($note=NULL, $id=NULL, $jadwal=NULL, $style = "calendar"){
		$mjadwal = new model_jadwal();	
		$mconf	 = new model_conf();
				
		$data['kegiatan'] 	= $mjadwal->get_jenis_kegiatan('akademik');
		$data['semester']	= $mconf->get_semester();
				
		if($id){
			foreach($jadwal as $dt):
				$semesterid = $dt->tahun_akademik;	
				$staffid	= $dt->karyawan_id;	
				$staff		= $dt->nama;	
				$data['jeniskegiatan'] 	= $dt->jenis_kegiatan_id;
			endforeach;			
		}else{
			if(isset($_POST['cmbsemester'])){
				$semesterid = htmlspecialchars($_POST['cmbsemester']);
			}else{
				$semesterid = "";
			}
			
			if(isset($_POST['cmbjenis'])){
				$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
			}else{
				$data['jeniskegiatan'] = "";
			}			
			
		}
				
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbfak']) ){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "-";
		}

	
		$data['semester']	= $mconf->get_semester();
		$data['semesterid']	= $semesterid;	
						
		$data['repeaton']	= Array('daily', 'weekly', 'monthly', 'yearly');
		$data['hari']   	= $mconf->get_hari();
		$data['infhari']	= '';
		$data['mkid']		= "";
		
		$data['posts']	= "";
			
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');		
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->add_script('js/jadwal.js');	
							
		$this->view('jadwal/ujian.php', $data);
	}
	
	function ok(){
		$this->index('ok');
	}
	function nok(){
		$this->index('nok');
	}
	
	function save_ujian($type=null){
		ob_start();		
	
		$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();
		
		$semester	= $_POST['cmbsemester'];
				
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		
		if(isset($_POST['hidId'])){
			$action 	= "update";
		}else{
			$action 	= "insert";
		}
		
		$mkid	 	= $_POST['cmbmk'];		
		$ruang	 	= $_POST['ruangid'];		
		$prodi	 	= $_POST['cmbprodi'];
		$kelas	 	=strToUpper($_POST['cmbkelas']);
		$repeaton	= 'daily';
		$tgl		= $_POST['tgl'];
		$semester	= $_POST['cmbsemester'];		
		$jammulai	= $_POST['jammulai'];
		$jamselesai	= $_POST['jamselesai'];
		$jeniskegiatan	= $_POST['cmbjenis'];
		
		$koordinator	= $_POST['koordinatorid'];
		
		$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);				
		$dosenid	= $mconf->get_dosen_id($_POST['pengampu'],$mkid);
		$staffid	= $_POST['pengampu'];			
		$dosen		= $mconf->get_nama_dosen($staffid);	

		if(isset($_POST['chkproject'])){
			$isprak		= 1;
		}else{		
			$isprak		= 0;
		}
			
				
		$kode = $mjadwal->get_reg_number($jeniskegiatan, $staffid, $semester);
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;			
		
		if($dosenid){
			echo $dosenid;
			$datanya = Array('jadwal_id'=>$kode, 'tahun_akademik'=>$semester, 'karyawan_id'=>$staffid, 'koordinator_id'=>$koordinator, 'jenis_kegiatan_id'=>$jeniskegiatan, 
							'repeat_on'=>$repeaton, 'user_id'=>$user, 'last_update'=>$lastupdate);
			$mjadwal->replace_penjadwalan($datanya);
			
			if($koordinator){
				$dataupdate  = array('koordinator_id'=>$koordinator);
				$idnya		=  array('tahun_akademik'=>$semester, 'jenis_kegiatan_id'=>$jeniskegiatan);
				
				$mjadwal->update_koordinator_ujian($dataupdate, $idnya);
			}
			
		
			if($tgl){
				
				//$hari		= strToLower($_POST['chkhari'][$j]);
				$hari 		= $mconf->getHari(date("N",  strtotime($tgl)));
				$nhari		= $mconf->getNHari($hari);
				
				if($isprak=='0'){
					$keterangan	= $dosen."(".Ucwords($hari).")";
				}else{			
					$keterangan	= $dosen."(".Ucwords($hari).") - Project";
				}
				
				$jadwalid = $mjadwal->get_reg_ujian($kode, $jeniskegiatan, $mkid, $dosenid, $prodi, $kelas, $semester, $hari, $jammulai, $ruang, $nhari);			
				
				$mjadwal->delete_peserta_kegiatan($jadwalid);				
			//	$mjadwal->update_dpenjadwalan($kode,$jammulai, $hari, $ruang);
				
				$datanya = Array('detail_id'=>$jadwalid, 'jadwal_id'=>$kode, 'ruang_id'=>$ruang, 'hari'=>$hari, 'jam_mulai'=>$jammulai,'jam_selesai'=>$jamselesai, 
								 'is_aktif'=>1, 'user_id'=>$user, 'last_update'=>$lastupdate);
				$mjadwal->replace_detail_penjadwalan($datanya);		

				$namamk = $mjadwal->get_nama_mk($mkid);
				
												
				if($hari){	
					if(isset($_POST['hidStaff'])&&(count($_POST['hidStaff'])>0)){
						
						$mjadwal->delete_panitia(array('jenis_kegiatan_id'=>$jeniskegiatan,'tahun_akademik'=>$semester));
											
						//$mjadwal->delete_pengawas_ruang(array('jadwal_id'=>$jadwalid));
						
						$panitia = explode(",", $_POST['hidStaff']);
						
											
						for($i=0;$i<count($panitia);$i++){
							
							$panitiaid	= strtoUpper(substr($jeniskegiatan,0,2)).$semester.$i;
							if($panitia[$i]!=""){		
								$temp	= $mconf->get_id_dosen_tmp($panitia[$i]);
								
								$staffid="";
								if($temp):
									foreach($temp as $dt){
										$staffid = $dt->karyawan_id;
										
										if($staffid){
											$idpanitia	= $staffid;
										}else{
											$idpanitia	= "-";
										}
									}				
									
									$urut = $i+1;
									if(($idpanitia) && $idpanitia!="-"){
										$datanya=array('panitia_id'=>$panitiaid, 'jenis_kegiatan_id'=>$jeniskegiatan,'tahun_akademik'=>$semester, 'karyawan_id'=>$idpanitia);
										$mjadwal->replace_panitia($datanya);

										$aktifitasid = $mconf->get_aktifitas_id($jeniskegiatan, $idpanitia, $tgl, $tgl, $jammulai, $jamselesai, $jeniskegiatan, "", 'Panitia', $ruang);
				

										$datanya = array('karyawan_id'=>$idpanitia, 'tgl'=>$tgl, 'tgl_selesai'=>$tgl, 'inf_ruang'=>$ruang, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jeniskegiatan, 'judul'=>'Panitia', 'jadwal_id'=>$jadwalid,'aktifitas_id'=>$aktifitasid, 'catatan'=>$keterangan);
										
										$mjadwal->replace_agenda_staff($datanya);											
									}
								endif;

							}
							
						}
					}
					
					if(isset($_POST['pengawas'])&&(count($_POST['pengawas'])>0)){
						
						$mjadwal->delete_pengawas(array('ujian_id'=>$jadwalid));
											
						
						$staff = $_POST['pengawas'];
					
						for($i=0;$i<count($staff);$i++){
							
							$pesertaid	= $jadwalid.$i;
							if($staff[$i]!="-"){		
								
								$idkaryawan=$staff[$i];
								
								
								$urut = $i+1;
								
								if(isset($_POST['chk'.$urut])&&($_POST['chk'.$urut]=='1')){
									$hadir	= $_POST['chk'.$urut];
								}else{
									$hadir	= '0';
								}
								
								if(isset($_POST['radio'.$urut])&&($_POST['radio'.$urut]=='1')){
									$pengganti	= $_POST['radio'.$urut];
								}else{
									$pengganti	= '0';
								}
								
								$datanya=array('pengawas_id'=>$pesertaid, 'ujian_id'=>$jadwalid, 'karyawan_id'=>$idkaryawan,'urut'=>$urut, 'is_hadir'=>$hadir,'pengganti'=>$pengganti);
								$mjadwal->replace_pengawas($datanya);	

								
								$aktifitasid = $mconf->get_aktifitas_id($jeniskegiatan, $idkaryawan, $tgl, $tgl, $jammulai, $jamselesai, $jeniskegiatan, "", 'Pengawas', $ruang);
			

								$datanya = array('karyawan_id'=>$idkaryawan, 'tgl'=>$tgl, 'tgl_selesai'=>$tgl, 'inf_ruang'=>$ruang, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jeniskegiatan, 'judul'=>'Pengawas', 'jadwal_id'=>$jadwalid,'aktifitas_id'=>$aktifitasid, 'catatan'=>$keterangan);
								
								$mjadwal->replace_agenda_staff($datanya);
							}
							
						}
					}else{
						$staff="";
					}
					
					$mjadwal->update_jadwal_exist($dosenid, $mkid, $jammulai, $kelas, $ruang, $hari, $isprak, $jadwalid);
										
					$datanya	= array('ujian_id'=>$jadwalid, 'jenis_ujian'=>$jeniskegiatan, 'pengampu_id'=>$dosenid, 'tgl'=>$tgl,
					'mkditawarkan_id'=>$mkid, 'kelas'=>$kelas, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'hari'=>$hari, 'ruang_id'=>$ruang, 'is_project'=>$isprak, 'prodi_id'=>$prodi, 'user_id'=>$user, 'last_update'=>$lastupdate,'is_aktif'=>1, 'cabang_id'=>'UBM');	
					
					$mjadwal->replace_jadwalujian($datanya);
										
					$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester,$repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $staffid, $kegiatan, 
					$keterangan, $user, $lastupdate, $action,$kelas,$mkid, $tgl, $tgl, $staff,'-','-',$prodi);					
					
				}
			}
		}
		
		unset($_POST['b_add']);
		$this->write($jeniskegiatan);
		
		exit();
	}
	
		
	function save_kuliah($type=null){
		ob_start();		
	
		$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();
		
		$semester	= $_POST['cmbsemester'];
				
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		
		if(isset($_POST['hidtmp'])){
			$action 	= "update";
		}else{
			$action 	= "insert";
		}
		
		
		if($_POST['cmbmk']){
			$mkid	 = $_POST['cmbmk'];
		}else{
			$mkid	 = "";
		}		
		
		
		if($_POST['ruangid']){
			$ruang	 = $_POST['ruangid'];
		}else{
			$ruang	 = "";
		}
		
		$prodi	 	= $_POST['cmbprodi'];
		$kelas	 	= $_POST['cmbkelas'];
		$repeaton	= 'monthly';
		$semester	= $_POST['cmbsemester'];		
		$jammulai	= $_POST['jammulai'];
		$jamselesai	= $_POST['jamselesai'];
		$jeniskegiatan	= $_POST['cmbjenis'];
		$crepeat		= $_POST['cmbrepeat'];
		$tmulai		= $_POST['tmulai'];
		$tselesai	= $_POST['tselesai'];
		
		
				
		$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);
				
		if((isset($_POST['chkpraktikum']) && $_POST['chkpraktikum']=='1') || ($type=='praktikum')){
			$dosen		= 'Asisten';	
			$dosenid	= '-';
			$staffid	= '-';
			$isprak		=1;
		}else{
			if($_POST['pengampu']=='130904012736'){
				$dosenid = '130904012736';
			}else{
				$dosenid	= $mconf->get_dosen_id($_POST['pengampu'],$mkid);
			}
			$staffid	= $_POST['pengampu'];
			
			$dosen		= $mconf->get_nama_dosen($staffid);					
			$isprak		=0;
		}
		
		//$mk = $mconf->get_mkditawarkan($isprak, $semester, "", $mkid);
		$mk = $mconf->get_mkditawarkan("", $semester, "", $mkid);
		
		if($mkid):
			$namamk = $mk->namamk;
			$kodemk = $mk->kode_mk;
		else:
			$namamk = "";
			$kodemk = "";
		endif;
		
		$kode = $mjadwal->get_reg_number($jeniskegiatan, $staffid, $semester);
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;			
		
		if($dosenid){	
			
			$datanya = Array('jadwal_id'=>$kode, 'tahun_akademik'=>$semester, 'karyawan_id'=>$staffid, 'jenis_kegiatan_id'=>$jeniskegiatan, 
							'repeat_on'=>$repeaton, 'user_id'=>$user, 'last_update'=>$lastupdate, 'tgl_mulai'=>$tmulai, 'tgl_selesai'=>$tselesai);
			$mjadwal->replace_penjadwalan($datanya);
			
			$mjadwal->delete_peserta_kegiatan(array('jadwal_id'=>$kode));
		
			//for($j=0;$j<count($_POST['cmbhari']);$j++){
				
				//$hari		= strToLower($_POST['cmbhari'][$j]);
				$hari		= strToLower($_POST['cmbhari']);
				$nhari		= $mconf->getNHari($hari);
			
				$keterangan	= $dosen."(".Ucwords($hari).")";
				
				$jadwalid = $mjadwal->get_reg_detail($kode, $jeniskegiatan, $staffid, $semester, $hari, $jammulai, $ruang, $nhari);				
				
			//	$mjadwal->update_dpenjadwalan($kode,$jammulai, $hari, $ruang);
				
											
				if($kelas){	
					$datanya = Array('detail_id'=>$jadwalid, 'jadwal_id'=>$kode, 'ruang_id'=>$ruang, 'hari'=>$hari, 'jam_mulai'=>$jammulai,'jam_selesai'=>$jamselesai, 
								'is_aktif'=>1, 'user_id'=>$user, 'last_update'=>$lastupdate);
								$mjadwal->replace_detail_penjadwalan($datanya);		
					
					$mjadwal->update_jadwal_exist($dosenid, $mkid, $jammulai, $kelas, $ruang, $hari, $isprak, $jadwalid);
										
					$datanya	= array('jadwal_id'=>$jadwalid, 'pengampu_id'=>$dosenid, 'mkditawarkan_id'=>$mkid, 'kelas'=>$kelas, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 
										'hari'=>$hari, 'ruang_id'=>$ruang, 'is_praktikum'=>$isprak, 'prodi_id'=>$prodi, 'user_id'=>$user, 'last_update'=>$lastupdate,'is_aktif'=>1, 'tgl_mulai'=>$tmulai, 'tgl_selesai'=>$tselesai, 'repeat_on'=>$crepeat);	
					
					$mjadwal->replace_jadwalmk($datanya);
										
					$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester,$repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $staffid, $kegiatan, 
					$keterangan, $user, $lastupdate, $action,$kelas,$mkid, $tmulai, $tselesai, '-', $namamk, $kodemk, $prodi);					
					
				}
			//}
		}
		unset($_POST['b_add']);
		//$this->write($jeniskegiatan);
		
		exit();
	}
	
	function save_kegiatan(){
		ob_start();
			
		$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();
		
		$semester	= $_POST['cmbsemester'];
				
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		
		
		$repeaton	= $_POST['cmbrepeat'];
		$semester	= $_POST['cmbsemester'];			
		$jammulai	= $_POST['bjammulai'];
		$jamselesai	= $_POST['bjamselesai'];
		$jeniskegiatan	= $_POST['cmbjenis'];
		
		
		$dosenid	= $_POST['pengampu'];			
		
		$dosen		= $mconf->get_nama_dosen($dosenid);	
				
		
		if(isset($_POST['hidId'])){
			$kode 	= $_POST['hidId'];
			$action = "update";
		}else{
			//$kode	= strtoUpper(substr($jeniskegiatan,0,2).$dosenid.substr($semester,4,2));
			$kode = $mjadwal->get_reg_number($jeniskegiatan, $dosenid, $semester);
			$action = "insert";
		}
				
		
		$ruang	 = $_POST['bruangid'];						
		$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);
			
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->username;
			
		
		if($dosenid){		
			$datanya = Array('jadwal_id'=>$kode, 'tahun_akademik'=>$semester, 'karyawan_id'=>$dosenid, 'jenis_kegiatan_id'=>$jeniskegiatan, 
								'repeat_on'=>$repeaton, 'user_id'=>$user, 'last_update'=>$lastupdate);
			$mjadwal->replace_penjadwalan($datanya);
			
			for($j=0;$j<count($_POST['chkhari']);$j++){
				//$hari	 	= strToLower($_POST['hari']);
				$hari		= strToLower($_POST['chkhari'][$j]);
				$nhari		= $mconf->getNHari($hari);
				
				//$jadwalid = $nhari.$dosenid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);	
				$jadwalid = $mjadwal->get_reg_detail($kode, $jeniskegiatan, $dosenid, $semester, $hari, $jammulai, $ruang, $nhari);
				
				$keterangan	= $dosen."(".Ucwords($hari).")";
			
				$datanya = Array('detail_id'=>$jadwalid, 'jadwal_id'=>$kode, 'ruang_id'=>$ruang, 'hari'=>$hari, 'jam_mulai'=>$jammulai,'jam_selesai'=>$jamselesai, 'is_aktif'=>1, 'user_id'=>$user, 'last_update'=>$lastupdate);
				$mjadwal->replace_detail_penjadwalan($datanya);
				
				$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester, $repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $dosenid, 
				$kegiatan, $keterangan, $user, $lastupdate, $action,'-','-');
			}
		}
			
		unset($_POST['b_add']);
				
		exit();
	}
	
	function generate_praktikum(){
		$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();
		
		$posts = $mjadwal->get_generate_praktikum();
		if($posts):
			foreach($posts as $dt):
				$nhari		= $mconf->getNHari($dt->hari);
				
				$jadwalid = $mjadwal->get_reg_detail($dt->jadwal_id, 'praktikum', '-', $dt->tahun_akademik, $dt->hari, $dt->jam_mulai, $dt->ruang_id, $nhari);
				
				$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $dt->tahun_akademik, $dt->repeat_on, $dt->jadwal_id, $jadwalid, $dt->ruang_id, $dt->jam_mulai, $dt->jam_selesai, $dt->hari, 'praktikum', '-', 
				 'Praktikum', 
					'-', '1', date("y-m-d H:i"), 'insert',$dt->kelas,$dt->mkditawarkan_id, $dt->tgl_mulai, $dt->tgl_selesai, '-','', '', $dt->prodi_id);
			endforeach;
		endif;
		
	}
	
	function insert_detail_jadwal_ruang($mjadwal=NULL, $mconf=NULL,$semester=NULL,$repeaton=NULL, $kode=NULL, $jadwalid=NULL, 
										$ruang=NULL, $jammulai=NULL, $jamselesai=NULL, $hari=NULL, $jeniskegiatan=NULL, $dosenid=NULL, $kegiatan=NULL, $keterangan=NULL, 
										$user=NULL, $lastupdate=NULL, $action=NULL, $kelas=NULL, $mk=NULL, $tgl=NULL, $tselesai=NULL, $staff=NULL, $namamk=NULL, $kodemk=NULL, $prodi=NULL){
		/*$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();*/
		
		switch($repeaton){
			case 'daily':
				if($tgl){
					$in = strtotime($tgl);
					$out = strtotime($tgl);
					
					$y=0;
				}else{
					$in	  = strtotime(date("Y-m-d"));
					$out  = strtotime('+7 days', $in);
					$y=1;
				}				
			break;
			
			case 'weekly':
				$in	  = strtotime(date("Y-m-d"));
				$out  = strtotime('+7 days', $in);
				$y=7;
			break;
			
			case  'monthly':
				if($jeniskegiatan=='bimbingan'){
					$drepeat	= $mjadwal->get_kalender_akademik('1', 'kuliah');
					if($drepeat){
						$in		= strtotime($drepeat->tgl_mulai);
						$out	= strtotime($drepeat->tgl_selesai);	
						$tgl	= $drepeat->tgl_mulai;
						$tselesai	= $drepeat->tgl_selesai;
					}else{
						$in	  = strtotime(date("Y-m-d"));
						$out  = strtotime('+1 month', $in);
					}
				}else{
					if($tgl){
						if($tgl!='0000-00-00'){
							$in = strtotime($tgl);
							$out = strtotime($tselesai);
						}else{
							$in	  = strtotime(date("Y-m-d"));
							$out  = strtotime('+1 month', $in);
						}
					}else{
						$in	  = strtotime(date("Y-m-d"));
						$out  = strtotime('+1 month', $in);
					}	
				}				
				
				$y=$this->dateDiff($in,$out);
			break;
			
			case 'yearly':
				$in	  = strtotime(date("Y-m-d"));
				$out = strtotime('+1 year', $in);
				$y	= $this->dateDiff($in,$out);
			break;
		}
		
		$n = $this->dateDiff($in,$out);
		
		$datanya = array('jadwal_id'=>$jadwalid, 'ruang_id'=>$ruang, 'tgl_mulai'=>$tgl, 'tgl_selesai'=>$tselesai,'kegiatan'=>$kegiatan, 'catatan'=>$keterangan, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai , 
						 'repeat_on'=>$repeaton, 'user_id'=>$user, 'last_update'=>$lastupdate, 'jenis_kegiatan_id'=>$jeniskegiatan, 'tahun_akademik'=>$semester);
		$mconf->replace_jadwal_ruang($datanya);
		
			
		
		//$mconf->log("db_ptiik_apps.tbl_jadwalruang", $datanya, $user, $action);				
		
		$detailid	= $n.$jadwalid;
		
		$mconf->delete_peserta_kegiatan(array('jadwal_id'=>$jadwalid));
								
		for($i=0;$i<=$n;$i++){		
			$shari 	= $mconf->getHari(date("N",  strtotime('+'.$i.' days', $in)));
			$nhari	= date("N",  strtotime('+'.$i.' days', $in));
			$tglx	= date("Y-m-d", strtotime('+'.$i.' days', $in));			
			
			if(($shari==$hari)){
								
				$this->insert_detail($jadwalid, $tglx, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i,$ruang, $kode, $jeniskegiatan,$dosenid,$kelas,$mk, $namamk, $kodemk, $semester, $prodi);				
								
				$aktifitasid = $mconf->get_aktifitas_id($jeniskegiatan, $dosenid, $tgl, $tselesai, $jammulai, $jamselesai, $kegiatan, "", $keterangan, $ruang);
			

				$datanya = array('karyawan_id'=>$dosenid, 'tgl'=>$tglx, 'tgl_selesai'=>$tglx, 'inf_ruang'=>$ruang, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'jenis_kegiatan_id'=>$jeniskegiatan, 'judul'=>$kegiatan, 'jadwal_id'=>$jadwalid,'aktifitas_id'=>$aktifitasid, 'catatan'=>$keterangan);
				$mconf->replace_event_staff($datanya);
			}	
		
		}	
		

	}
	
	function form_report(){
		$mjadwal = new model_jadwal();
		$mconf	 	= new model_conf();
	
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbfak']) ){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "-";
		}
		
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$semesterid = "";
		}
		
		if(isset($_POST['cmblokasi']) ){
			$data['cmblokasi'] = htmlspecialchars($_POST['cmblokasi']);
		}else{
			$data['cmblokasi'] = "-";
		}
		
				
		if($data['jeniskegiatan']=='uts'||$data['jeniskegiatan']=='uas'){
			$data['lokasi']	= $mjadwal->get_lokasi("-");
			$data['tgl']	= $mjadwal->get_tgl_ujian($data['jeniskegiatan'], $semesterid, $data['cmbhari'],"-");
			$data['jam']	= $mjadwal->get_jam_ujian($data['jeniskegiatan'], $semesterid, $data['cmbhari'],"-","-");
			$data['prodi']	= $mconf->get_prodi();
			
			$this->view('jadwal/report_form.php', $data);
		}
	}
	
	function cetak_panitia(){
		$mjadwal = new model_jadwal();
		
		$data['type']	= "panitia";
	
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbfak']) ){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "-";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbsemester'])){
			$data['semesterid'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['semesterid'] = "";
		}
		
		if(isset($_POST['cmbtgl']) ){
			$data['cmbtgl'] = htmlspecialchars($_POST['cmbtgl']);
		}else{
			$data['cmbtgl'] = "";
		}
		
		$data['tgl']	= $mjadwal->get_tgl_ujian($data['jeniskegiatan'], $data['semesterid'], "-", $data['cmbtgl']);
		$data['posts']	= $mjadwal->get_panitia_ujian($data['semesterid'], $data['jeniskegiatan']);		
		$data['hari']	= $mjadwal->get_hari_nama($data['cmbhari']);
				
		$this->view('jadwal/report_view.php', $data);
	}
	
	function cetak_hadir(){
		$mjadwal = new model_jadwal();
		
		$data['type']	= "hadir";
		
		if(isset($_POST['cmbfak']) ){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "-";
		}
	
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbsemester'])){
			$data['semesterid'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['semesterid'] = "";
		}
		
		if(isset($_POST['cmblokasi']) ){
			$data['cmblokasi'] = htmlspecialchars($_POST['cmblokasi']);
		}else{
			$data['cmblokasi'] = "";
		}
				
		if(isset($_POST['cmbprodi']) ){
			$data['cmbprodi'] = htmlspecialchars($_POST['cmbprodi']);
		}else{
			$data['cmbprodi'] = "";
		}
		
		if(isset($_POST['cmbtgl']) ){
			$data['cmbtgl'] = htmlspecialchars($_POST['cmbtgl']);
		}else{
			$data['cmbtgl'] = "";
		}
		
		if(isset($_POST['cmbjam']) ){
			$data['cmbjam'] = htmlspecialchars($_POST['cmbjam']);
		}else{
			$data['cmbjam'] = "";
		}
		
		$data['prodi']			= $mjadwal->get_prodi_nama($data['cmbprodi'], $data['fakid']);
		$data['hari']			= $mjadwal->get_hari_nama($data['cmbhari']);
				
		$this->view('jadwal/report_view.php', $data);
	}
	
	
	function cetak_rekap($jenis=NULL, $semester=NULL, $hari=NULL){
		$mjadwal = new model_jadwal();
		
		$data['type']	= "rekap";
		
		if(isset($_POST['cmbfak']) ){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "-";
		}
	
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = $jenis;
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = $hari;
		}
		
		if(isset($_POST['cmbsemester'])){
			$data['semesterid'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['semesterid'] = $semester;
		}
		
		$data['posts']	= $mjadwal->get_data_pengawas($data['jeniskegiatan'], $data['semesterid'], $data['cmbhari']);
		
		$this->view('jadwal/report_view.php', $data);
		
	}
	
	function jadwal_by_form(){
		
		$mconf	 	= new model_conf();
		$mjadwal	 = new model_jadwal();
		$minfo	 	= new model_info();
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$semesterid = "";
		}
		
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
		if(isset($_POST['cmbjadwal']) ){
			$jadwalid = $_POST['cmbjadwal'];
		}else{
			$jadwalid = "";
		}
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbruang']) ){
			//$data['cmbruang'] = htmlspecialchars($_POST['cmbruang']);
			$dataruang=explode("|", $_POST['cmbruang']);
			$data['cmbruang'] = $dataruang[0];
		}else{
			$data['cmbruang'] = "-";
		}
		
		
		if(isset($_POST['cmbmulai']) ){
			$data['cmbmulai'] = htmlspecialchars($_POST['cmbmulai']);
		}else{
			$data['cmbmulai'] = "-";
		}
		
		if(isset($_POST['cmbfak']) ){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "-";
		}
		
		if(isset($_POST['cmbselesai']) ){
			$data['cmbselesai'] = htmlspecialchars($_POST['cmbselesai']);
		}else{
			$data['cmbselesai'] = "-";
		}
		
		if(isset($_POST['cmbtgl']) ){
			$data['cmbtgl'] = htmlspecialchars($_POST['cmbtgl']);
		}else{
			$data['cmbtgl'] = "";
		}
		
		if(isset($_POST['cmbprodi']) ){
			$data['cmbprodi'] = htmlspecialchars($_POST['cmbprodi']);
		}else{
			$data['cmbprodi'] = "-";
		}
		
		
		$data['prodi']		= $mconf->get_prodi();
		$data['mk']			= $mconf->get_mkditawarkan(0,$semesterid);	
		
		
		
				
		if($jadwalid){
			$jadwal	= $minfo->get_jadwal_by_hari($data['fakid'],$data['cmbhari'], $semesterid, "", $data['cmbmulai'], $data['cmbselesai'], $data['cmbruang'],$jadwalid);
		}else{		
			$jadwal="";
		}
		
				
		if($jadwalid){
			$data['tmpid'] 	 = $jadwal->jadwal_id;
			$data['prodiid'] = $jadwal->prodi_id;
			$data['dosenid'] = $jadwal->dosen_id;
			$data['mkid'] 	 = $jadwal->mkditawarkan_id;
			$data['kelasid'] = $jadwal->kelas_id;
			$data['mulai']	= $jadwal->jam_mulai;
			$data['selesai']= $jadwal->jam_selesai;
			$data['ruangid']= $jadwal->ruang;
			$data['jadwalid']	= $jadwal->jadwal_id;
			$data['is_praktikum']	= $jadwal->is_praktikum;
		}else{
			$data['tmpid'] 	 = "";
			$data['prodiid'] = "";
			$data['dosenid'] = "";
			$data['mkid'] 	 = "";
			$data['kelasid'] = "";
			$data['mulai']	= $data['cmbmulai'];
			$data['selesai']= $data['cmbselesai'];
			$data['ruangid']= $data['cmbruang'];
			$data['jadwalid']	= "";
			$data['is_praktikum']	= "";			
		}
		$data['staff']		= $mconf->get_karyawan();
		
		$data['pengampu'] = $mconf->get_dosen_diampu("", $data['mkid'], $semesterid);	
				
		$this->view('jadwal/jadwal_form.php', $data);
	}
	
	
	
	function ujian_by_form(){
		
		$mconf	 	= new model_conf();
		$mjadwal	 = new model_jadwal();
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$semesterid = "";
		}
		
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbruang']) ){
			$dataruang=explode("|", $_POST['cmbruang']);
			$data['cmbruang'] = $dataruang[0];
		}else{
			$data['cmbruang'] = "-";
		}
		
		
		if(isset($_POST['cmbmulai']) ){
			$data['cmbmulai'] = htmlspecialchars($_POST['cmbmulai']);
		}else{
			$data['cmbmulai'] = "-";
		}
		
		if(isset($_POST['cmbselesai']) ){
			$data['cmbselesai'] = htmlspecialchars($_POST['cmbselesai']);
		}else{
			$data['cmbselesai'] = "-";
		}
		
		if(isset($_POST['cmbtgl']) ){
			$data['cmbtgl'] = htmlspecialchars($_POST['cmbtgl']);
		}else{
			$data['cmbtgl'] = "";
		}
		
		if(isset($_POST['cmbprodi']) ){
			$data['cmbprodi'] = htmlspecialchars($_POST['cmbprodi']);
		}else{
			$data['cmbprodi'] = "-";
		}
		
		
		$data['prodi']		= $mconf->get_prodi();
		$data['mk']			= $mconf->get_mkditawarkan(0,$semesterid);	
		
		if($data['cmbtgl']){
			$jadwal	= $mjadwal->read_ujian_by_tgl($data['cmbtgl'], $data['cmbmulai'], $data['cmbselesai'], $data['cmbruang']);
		}else{		
			$jadwal="";
		}
		
				
		if($jadwal){
			$data['tmpid'] 	 = $jadwal->ujian_id;
			$data['prodiid'] = $jadwal->prodi_id;
			$data['dosenid'] = $jadwal->dosen_id;
			$data['mkid'] 	 = $jadwal->mkditawarkan_id;
			$data['kelasid'] = $jadwal->kelas_id;
			$data['project'] = $jadwal->is_project;
			$data['tgltmp'] = $jadwal->tgl;
			$data['mulai']	= $jadwal->jam_mulai;
			$data['selesai']= $jadwal->jam_selesai;
			$data['ruangid']= $jadwal->ruang;
			$data['pengawas']	= $mjadwal->get_pengawas($jadwal->mkditawarkan_id, $jadwal->dosen_id, $jadwal->prodi_id,$jadwal->kelas_id,$jadwal->tgl);
			$data['jadwalid']	= $jadwal->ujian_id;
			$data['isproject']	= $jadwal->is_project;
		}else{
			$data['tmpid'] 	 = "";
			$data['prodiid'] = "";
			$data['dosenid'] = "";
			$data['mkid'] 	 = "";
			$data['kelasid'] = "";
			$data['project'] = "";
			$data['tgltmp'] = $data['cmbtgl'];
			$data['mulai']	= $data['cmbmulai'];
			$data['selesai']= $data['cmbselesai'];
			$data['ruangid']= $data['cmbruang'];
			$data['pengawas']	= "";
			$data['jadwalid']	= "";
			$data['is_project']	= "";			
		}
		$data['staff']		= $mconf->get_karyawan();
		$data['pengampu'] 	= $mjadwal->get_data_ujian($data['mkid'] , 'dosen', $data['prodiid']);
				
		$this->view('jadwal/ujian_form.php', $data);
	}
	
	function get_pengampu(){
		$mjadwal	 = new model_jadwal();
				
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
		if(isset($_POST['cmbprodi']) ){
			$data['prodiid'] = htmlspecialchars($_POST['cmbprodi']);
		}else{
			$data['prodiid'] = "-";
		}
							
		$pengampu 	= $mjadwal->get_data_ujian($mkid, 'dosen', $data['prodiid']);
				
		if($pengampu){
			$data = array();
			foreach($pengampu as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}
		
	}
	
	function get_koordinator(){
		$mjadwal	 = new model_jadwal();
				
		
		if(isset($_POST['cmbsemester']) ){
			$data['cmbsemester'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['cmbsemester'] = "-";
		}
			
		if(isset($_POST['cmbjenis']) ){
			$data['cmbjenis'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['cmbjenis'] = "-";
		}
		
		$koordinator 	= $mjadwal->get_koordinator_ujian($data['cmbsemester'], $data['cmbjenis']);
				
		if($koordinator){
			$data = array();
			foreach($koordinator as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}else{
			echo "";
		}
		
	}
	
	function get_panitia(){
		$mjadwal	 = new model_jadwal();
				
		
		if(isset($_POST['cmbsemester']) ){
			$data['cmbsemester'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['cmbsemester'] = "-";
		}
			
		if(isset($_POST['cmbjenis']) ){
			$data['cmbjenis'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['cmbjenis'] = "-";
		}
		
		$koordinator 	= $mjadwal->get_panitia_ujian($data['cmbsemester'], $data['cmbjenis']);
				
		if($koordinator){
			$data = array();
			foreach($koordinator as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}else{
			echo "";
		}
		
	}
	
	
	function get_pengampu_kuliah(){
		$mconf	 = new model_conf();
				
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
						
		$pengampu 	= $mconf->get_dosen_diampu("",$mkid);
				
		if($pengampu){
			$data = array();
			foreach($pengampu as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}
		
	}
	
	function  get_staff(){		
		$mconf 	= new model_conf();
				
		$staff 	= $mconf->get_karyawan();
				
		if($staff){
			$data = array();
			foreach($staff as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			
			echo json_encode($data);
		}
	}
	
	function get_mk(){
		$mjadwal	 = new model_jadwal();
		$mconf		 = new model_conf();
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$semesterid = "";
		}
				
		$data['mk']			= $mconf->get_mkditawarkan(0,$semesterid);		
				
	}
	
	
	function jadwal_view(){
		$minfo	 = new model_info();
		if(isset($_POST['cmbsemester'])){
			$data['semesterid'] = htmlspecialchars($_POST['cmbsemester']);
		}else{
			$data['semesterid'] = "";
		}
		
		$pendek = $minfo->get_semester_id($data['semesterid']);
		
		if($pendek):
			$data['pendek'] = $pendek->is_pendek;
		else:
			$data['pendek'] = "";
		endif;
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = htmlspecialchars($_POST['cmbjenis']);
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbfak'])){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "";
		}	
		
		if(isset($_POST['cmbhari']) ){
			$data['cmbhari'] = htmlspecialchars($_POST['cmbhari']);
		}else{
			$data['cmbhari'] = "-";
		}
		
		if(isset($_POST['cmbruang']) ){
			//$data['cmbruang'] = htmlspecialchars($_POST['cmbruang']);
			$dataruang=explode("|", $_POST['cmbruang']);
			$data['cmbruang'] = $dataruang[0];
			
		}else{
			$data['cmbruang'] = "-";
		}
		
		
		if(isset($_POST['cmbmulai']) ){
			$data['cmbmulai'] = htmlspecialchars($_POST['cmbmulai']);
		}else{
			$data['cmbmulai'] = "-";
		}
		
		if(isset($_POST['cmbselesai']) ){
			$data['cmbselesai'] = htmlspecialchars($_POST['cmbselesai']);
		}else{
			$data['cmbselesai'] = "-";
		}
		
		if(isset($_POST['cmbtgl']) ){
			$data['cmbtgl'] = htmlspecialchars($_POST['cmbtgl']);
		}else{
			$data['cmbtgl'] = "-";
		}
		
					
		$data['jenis']	= $data['jeniskegiatan'];
		
		if(isset($_POST['cmbjenis'])){
			switch($_POST['cmbjenis']){
				case 'bimbingan':
					$data['ruang'] 	= $minfo->get_ruang($data['fakid'],"",1);
					$this->draw_grid_kegiatan($data);
				break;
				
				case 'kuliah':
					$data['ruang'] 	= $minfo->get_ruang($data['fakid'],'kuliah');
					$data['praktikum'] = "";		
					$this->draw_grid_kuliah($data);
				break;
				
				case 'praktikum':
					$data['ruang'] 	= $minfo->get_ruang($data['fakid'],'kuliah');
					$data['praktikum'] = 1;		
					$this->draw_grid_kuliah($data);
				break;
				
				case 'uts':
					//$this->ujian('uts');
					$data['ruang'] 	= $minfo->get_ruang($data['fakid'],"",1);
					$data['jam'] 	= $minfo->get_jam_mulai(1);	
					$data['tgl']	= $minfo->get_tgl_ujian_by_hari($data['jeniskegiatan'],$data['cmbhari'],$data['semesterid']);
					$data['posts']  = $minfo->get_ujian_by_hari($data['jeniskegiatan'],$data['cmbhari'],"",$data['semesterid']);
					$data['rowmk']  = $minfo->get_row_mk($data['cmbhari']);
					$data['mkruang']= $minfo->get_mk_ruang($data['cmbhari']);
					$data['by']		= "hari";
					$data['idhari'] = $data['cmbhari'];
					
					$this->draw_grid_ujian($data);
				break;
				
				case 'uas':
					$data['ruang'] 	= $minfo->get_ruang($data['fakid'],"",1);
					$data['jam'] 	= $minfo->get_jam_mulai(1);	
					$data['tgl']	= $minfo->get_tgl_ujian_by_hari($data['jeniskegiatan'],$data['cmbhari'],$data['semesterid']);
					$data['posts']  = $minfo->get_ujian_by_hari($data['jeniskegiatan'],$data['cmbhari'],"",$data['semesterid']);
					$data['rowmk']  = $minfo->get_row_mk($data['cmbhari']);
					$data['mkruang']= $minfo->get_mk_ruang($data['cmbhari']);
					$data['by']		= "hari";
					$data['idhari'] = $data['cmbhari'];
					
					$this->draw_grid_ujian($data);
				break;
				
				default :
					$data['ruang'] 	= $minfo->get_ruang('kuliah');
					$data['praktikum'] = "";	
					$this->draw_grid_kuliah($data);
				break;
			}
		}else{
			$data['ruang'] 	= $minfo->get_ruang($data['fakid'], 'kuliah');
			$data['praktikum'] = "";	
			$this->draw_grid_kuliah($data);
		}
		
	}
	
	function draw_grid_kegiatan($data=NULL){
		$mjadwal 	= new model_jadwal();
	
		$data['posts']		= $mjadwal->draw_grid_jadwal($data['semesterid'], "", "", $data['cmbruang'], "calendar", $data['jeniskegiatan'] );
		
		$this->view('jadwal/kegiatan_view.php', $data);	
	}
	
	
	function draw_grid_ujian($data=NULL){
								
		$this->add_style('css/calendar/calendar.css');

		$this->view('jadwal/ujian_view.php', $data);		
	}
	
	function draw_grid_kuliah($data=NULL){
			
		$mconf		= new model_info();
		$mjadwal 	= new model_jadwal();

		if($data['jeniskegiatan']=='praktikum'){			
			$drepeat	= $mjadwal->get_kalender_akademik("", 'kuliah',$data['semesterid']);
		}else{			
			$drepeat	= $mjadwal->get_kalender_akademik("", $data['jeniskegiatan'],$data['semesterid']);
		}
		
		if($drepeat){
			$data['tmulai']		= $drepeat->tgl_mulai;
			$data['tselesai']	= $drepeat->tgl_selesai;	
		}else{
			$data['tmulai']		= date("Y-m-d");
			$data['tselesai']	= date("Y-m-d");
		}				
		
		$data['dosen'] 	= $mconf->get_dosen_pengampu();
		$data['hari'] 	= $mconf->get_hari();	

		if($data['pendek']!=""):
			$data['jam'] 	= $mconf->get_jam_mulai_pendek();
		else:
			$data['jam'] 	= $mconf->get_jam_mulai();	
		endif;
		
		$data['ruang'] 	= $mconf->get_ruang($data['fakid'], "kuliah");
		$data['mk'] 	= $mconf->get_mkditawarkan(0);
		$data['prak'] 	= $mconf->get_mkditawarkan(1);
		$data['jadwal'] = $mconf->get_jadwal_by_hari($data['fakid'], $data['cmbhari'], $data['semesterid'], $data['praktikum']);
		$data['rowmk']  = $mconf->get_row_mk($data['cmbhari'], $data['semesterid']);
		$data['mkruang']= $mconf->get_mk_ruang($data['cmbhari'], $data['semesterid']);
		$data['by']		= "hari";
		$data['iddosen']= "";
		$data['idruang']= "";
		$data['idhari'] = $data['cmbhari'];
		$data['idmk']   = "";
		$data['idprak']	= "";
		$data['prodi']	= $mconf->get_prodi();	
		$data['idprodi']= "";

								
		$this->add_style('css/calendar/calendar.css');

		$this->view('jadwal/jadwal_view.php', $data);		
	}
	
	
	
	function kegiatan_form(){
		
		$mjadwal = new model_jadwal();	
		$mconf	 = new model_conf();
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = $_POST['cmbsemester'];
		}else{
			$semesterid = "";
		}
		
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = $_POST['cmbjenis'];
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if(isset($_POST['cmbfak'])){
			$data['fakid'] = htmlspecialchars($_POST['cmbfak']);
		}else{
			$data['fakid'] = "";
		}	
		
		
		$dosenid	= "";
		$drepeat	= $mjadwal->get_kalender_akademik('1', $data['jeniskegiatan']);
		
		
		$data['kegiatan'] 	= $mconf->get_jenis_kegiatan('akademik');
		$data['semester']	= $mconf->get_semester();
		$data['ruangan']	= $mconf->get_ruang("", "", $data['fakid']);
		$data['mdosen']		= $mconf->get_dosen();
		$data['prodi']		= $mconf->get_prodi();
		$data['jammulai']	= $mconf->get_jam_mulai();
		$data['jamselesai']	= $mconf->get_jam_selesai();
		$data['ruangid']	= "";
		$data['dosenid']	= "";
		$data['mkid']		= "";
		$data['semesterid']	= $semesterid;	
		
		if($drepeat){
			$data['tmulai']		= $drepeat->tgl_mulai;
			$data['tselesai']	= $drepeat->tgl_selesai;	
		}else{
			$data['tmulai']		= date("Y-m-d");
			$data['tselesai']	= date("Y-m-d");
		}		

		$data['pengampu'] = $mconf->get_pengampu_kegiatan("", "", $semesterid);		
					
		$data['repeaton']	= Array('daily', 'weekly', 'monthly', 'yearly');
		$data['hari']   	= $mconf->get_hari();
		$data['infhari']	= '';
		$data['repeatin']	= 'monthly';
		
		$this->view('jadwal/detailkegiatan.php', $data);
	
	}
	
	
	function edit($id, $style='calendar'){
		if(!$id) {
			$this->redirect('module/penjadwalan/jadwal');
			exit;
		}
		
		$mjadwal = new model_jadwal();
		$jadwal = $mjadwal->read("",1,1, $id);
		
		foreach($jadwal as $dt):
			$semesterid = $dt->tahun_akademik;	
			$staffid	= $dt->karyawan_id;	
			$staff		= $dt->nama;	
			$jeniskegiatan 	= $dt->jenis_kegiatan_id;
		endforeach;
				
		switch($jeniskegiatan){
			case 'kuliah':
				$this->edit_jadwal();
			break;
			
			case 'praktikum':
				$this->edit_jadwal();
			break;
			
			case 'uts':
				$this->ujian('uts', $id, $jadwal);
			break;
			
			case 'uas':
				$this->ujian('uas', $id, $jadwal);
			break;
			
			default :
				$this->edit_jadwal();
			break;
		}

	}	
	
	function edit_jadwal($id=NULL, $style='calendar', $jadwal=NULL){
		$mjadwal = new model_jadwal();	
		$mconf	 = new model_conf();
		
		
		//$jadwal = $mjadwal->read("",1,1, $id);
		
		foreach($jadwal as $dt):
			$semesterid = $dt->tahun_akademik;	
			$staffid	= $dt->karyawan_id;	
			$staff		= $dt->nama;	
			$data['jeniskegiatan'] 	= $dt->jenis_kegiatan_id;
		endforeach;
				
		$mkid		= "";
		$ruangid	= "";
		
		if($data['jeniskegiatan']=='praktikum'){
			$data['namadosen']="";
			$dosenid	= "";
		}else{
			$data['namadosen']= $staff;
			$dosenid	= $staffid;
		}
		
		$data['kegiatan'] 	= $mjadwal->get_jenis_kegiatan();
		$data['semester']	= $mconf->get_semester();
		$data['ruangan']	= $mconf->get_ruang();
		$data['ruangid']	= $ruangid;
		$data['dosenid']	= $dosenid;
		$data['mdosen']		= $mconf->get_dosen();
		$data['prodi']		= $mconf->get_prodi();
		$data['jammulai']	= $mconf->get_jam_mulai();
		$data['jamselesai']	= $mconf->get_jam_selesai();
		$data['mkid']		= $mkid;
		$data['semesterid']	= $semesterid;
		
		$data['pengampu'] = $mconf->get_dosen_diampu($dosenid, $mkid, $semesterid);	
		
		if($dosenid && ($data['jeniskegiatan']!='praktikum')){
			$data['mk']		= $mconf->get_mk_diampu($dosenid, "");
		}else{
			//$data['pengampu'] = $mconf->get_mk_diampu($dosenid, $mkid);
			if($data['jeniskegiatan']=='kuliah'){
				$data['mk']	= $mconf->get_mkditawarkan(0,$semesterid);
			}else{
				$data['mk']	= $mconf->get_mkditawarkan(1,$semesterid);
			}
		}
		
		$data['posts']		= $mjadwal->draw_grid_jadwal($semesterid, $mkid, $dosenid, $ruangid, $style, $data['jeniskegiatan'] );
		$data['repeaton']	= Array('daily', 'weekly', 'monthly', 'yearly');
		$data['repeatin']	= 'monthly';
		$data['hari']   	= $mconf->get_hari("");
		$data['infhari']	= '';
		
		$this->coms->add_style('css/calendar/calendar.css');		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('bootstrap/bootstrap-tooltip.js');
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->add_script('js/jadwal.js');
							
							
		$this->view('jadwal/edit.php', $data);
	}
	
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		return $ext; 
	}  
	
	function delete_ruang($id){
		$mconf   = new model_conf();	
		
		$datanya  = array('jadwal_id'=>$id);	
		$mconf->delete_jadwal_ruang($datanya);
		$mconf->delete_jadwal_ruang_detail($datanya);
	}
	
	function delete_jadwal($id){
		$mjadwal = new model_jadwal();
		
		$datanya  = array('detail_id'=>$id);
		$mjadwal->delete_jadwal_kegiatan($datanya);
	}
	
	function delete_ujian($id){
		$mjadwal = new model_jadwal();
		
		$datanya  = array('ujian_id'=>$id);
		$mjadwal->delete_jadwalujian($datanya);
		$mjadwal->delete_pengawas($datanya);		
	}

	function delete($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('detail_id'=>$id);	
		
		if($mjadwal->delete_jadwal_kegiatan($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function deletemk($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('jadwal_id'=>$id);	
		
		if($mjadwal->delete_jadwalmk($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_ujian($id);
			$this->delete_jadwal($id);
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function disablemk($id=NULL){
		$mjadwal = new model_jadwal();	
		$idnya  = array('jadwal_id'=>$id);	
		$datanya = array('is_aktif'=>0);
		if($mjadwal->disable_jadwalmk($datanya, $idnya )){ 		
			$result['status'] = "OK";
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);	
				
	}
	
	function deleteujian($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('ujian_id'=>$id);	
		
		if($mjadwal->delete_jadwalujian($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_jadwal($id);
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 200) {

			// truncate string
			$stringCut = substr($string, 0, 200);

			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}
	
	function dateDiff($in,$out){		
		$interval =round(($out - $in)/(3600*24));
		return  $interval ;
	} 
	
	function insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i, $ruang, $infjadwal, $infjeniskegiatan, $dosen, $kelas, $mk, $namamk=NULL, $kodemk=NULL, 
	$semester=NULL, $prodi=NULL){
		$mconf 	 = new model_conf();			
		
		$detailid = $nhari.$jadwalid.$i;
	
	
		$datanyax = array('detail_id'=>$detailid, 'jadwal_id'=>$jadwalid, 'tgl'=>$tgl, 'hari'=>$shari, 'inf_hari'=>$nhari, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'ruang_id'=>$ruang, 'inf_jadwal_id'=>$infjadwal, 'inf_jenis_kegiatan'=>$infjeniskegiatan, 'pengampu_id'=>$dosen, 'kelas'=>$kelas, 'mk_id'=>$mk);
		
		$datanya = array('detail_id'=>$detailid, 'jadwal_id'=>$jadwalid, 'tgl'=>$tgl, 'hari'=>$shari, 'inf_hari'=>$nhari, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'ruang'=>$ruang, 'inf_jadwal_id'=>$infjadwal, 'inf_jenis_kegiatan'=>$infjeniskegiatan, 'dosen_id'=>$dosen, 'kelas'=>$kelas, 'mk_id'=>$mk);
		$mconf->replace_detail_jadwal_ruang($datanya);
		
		
		if($infjeniskegiatan=='praktikum'):
					
			$datamk = $mconf->get_mkditawarkan("", "", "", $mk);
			$praktikum = array('is_aktif'=>0, 'prodi_id'=>$prodi, 'praktikum_id'=>$detailid, 'jadwal_id'=>$jadwalid, 'tgl'=>$tgl, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 
								'hari'=>$shari, 'ruang'=>$ruang, 'nama_mk'=>$datamk->namamk, 'kode_mk'=>$datamk->kode_mk, 'mkditawarkan_id'=>$mk, 'tahun_akademik'=>$semester, 
								'user_id'=>$user, 'last_update'=>date("Y-m-d H:i"), 'kelas_id'=>$kelas);			
			$mconf->replace_jadwal_praktikum($praktikum);			
		endif;
		
		$mconf->update_jadwal_valid($jadwalid, $tgl, $shari, $jammulai, $jamselesai, $ruang, $infjeniskegiatan, $dosen, $kelas, $mk);
		
		//$mconf->log("db_ptiik_apps.tbl_detailjadwalruang", $datanya, $user, $action);			
		
	}

}
?>