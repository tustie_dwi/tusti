<?php
class penjadwalan_ruang extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 		
	}
	
	function index(){
	
	}
	
	function terpakai( $tgl=NULL){		
		if($tgl){
			$tgl = date("Y-m-d",$tgl);	
		}else{
			if(isset($_POST['hidtgl'])){
				$tgl = date("Y-m-d",$_POST['hidtgl']);
			}else{
				$tgl = date("Y-m-d");
			}
		}
		$this->detail($tgl);
		exit();
				
	}
	
	function detail($tgl=NULL, $by = 'all', $keyword = NULL,$page = 1, $perpage = 1000){
		
		$mruang  = new model_ruanginfo();	
		$mconf   = new model_confinfo();
				
		
		$this->coms->add_style('css/calendar/calendar.css');
				
				
		if(isset($_POST['cmbkategori'])){
			$data['kategori']	 = htmlspecialchars($_POST['cmbkategori']);
		}else{
			$data['kategori']	 = 'kuliah';
		}
		
		if(isset($_POST['cmbruang'])){
			$data['ruangid'] = htmlspecialchars($_POST['cmbruang']);			
		}else{
			$data['ruangid'] = "";
		}	
		
		if($tgl){
			$data['tgl']	= $tgl;
		}else{
			$data['tgl']	= date("Y-m-d");
		}		
		
		$data['calendar'] 		= $this->calendar($data['tgl']);
		$data['utilitas']		= $mruang->get_utilitas($data['tgl']);
		$data['kategoriruang'] 	= $mconf->get_kategori_ruang_aktif();
		$data['ruang']	  		= $mconf->get_ruang("", $data['kategori']);
		$data['druang']	  		= $mconf->get_ruang("", $data['kategori'], $data['ruangid']);
		$data['blokwaktu']		= $mruang->get_blok_waktu();
		
		//$data['post']	  = $mruang->cek_ketersediaan($tgl);
		$this->view( 'ruang/terpakai.php', $data );
		
	}
	
	function ruang_json($tgl=NULL, $kategori=NULL){
		$tglc = date("Y-m-d",$tgl);
						
		$mruang = new model_ruanginfo();	
		$post	= $mruang->get_utilitas($tglc, $kategori);
		
		$return_arr = array();
		
		foreach($post as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr, JSON_NUMERIC_CHECK);
		
		echo $json_response;
		
	}
		
	
	
	function ok(){
		$this->index('ok');
	}
	
	
	function view_kegiatan(){
		$mruang = new model_ruanginfo();	
		
		$id = $_POST['id'];
		
		$data['posts'] = $mruang->read("", 1, 1, $id);
		$this->view('ruang/view_data.php', $data);			
			
		
		
	}
	
	
	function tersedia($jenis=NULL, $tglval=NULL, $style='calendar'){
		$mruang = new model_ruanginfo();	
		$mconf	= new model_confinfo();

		if(isset($_POST['cmbruang'])){
			$ruangid = htmlspecialchars($_POST['cmbruang']);			
		}else{
			$ruangid = "";
		}	

		if(isset($_POST['tgl'])){
			$tgl	 = htmlspecialchars($_POST['tgl']);
		}else{
			if($tglval){
				$tgl	 = date("Y-m-d",$tglval);
			}else{
				$tgl	 = date('Y-m-d');
			}
			
		}	

		if(isset($_POST['cmbkategori'])){
			$kategori	 = htmlspecialchars($_POST['cmbkategori']);
		}else{
			if($jenis){
				$kategori	 = $jenis;
			}else{
				$kategori	 = 'kuliah';
			}
		}				
		
		$data['kategoriruang'] =  $mconf->get_kategori_ruang_aktif();
		$data['ruangan']	= $mconf->get_ruang("",$kategori);
		$data['ruangid']	= $ruangid;
		$data['tgl']		= $tgl;
		$data['kategori']	= $kategori;					
		//$data['posts']		= $mruang->draw_grid_ruang($tgl, $kategori, $ruangid, $style);
		
		$this->coms->add_style('css/calendar/calendar.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');			
		$this->coms->add_script('js/jsruang.js');	
		//$this->coms->add_script('js/info/footer.js');	
			
		$this->view('ruang/tersedia.php', $data);
	}
	
	function approve(){
		if(isset($_POST['init'])) $data['msg'] = $_POST['init'];
		else $data['msg'] = NULL;
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$mbooking = new model_booking();
		$this->add_script('js/booking.js');
		$data['resv'] = $mbooking->get_resv();
		
		$this->view( 'booking/approve.php', $data );
	}
	
	function app($init, $resv_id){
		$mbooking = new model_booking();
		$user = $this->coms->authenticatedUser->id;
		
		if($mbooking->approve($init, $resv_id, $user)) $this->redirect_page('approve','ok');
		else $this->redirect_page('approve','err');
	}

	function redirect_page($url, $init){
		?>
			Redirecting ...
			<body onload="document.getElementById('form-redirect').submit();">
			<form id="form-redirect" method="post" action="<?php echo $this->location('module/masterdata/booking/' . $url) ?>">
			  <input name="init" value="<?php echo $init ?>" type="hidden">
			</form>
			</body>
		<?php
	}
	
	
	function calendar($tgl=NULL, $style='calendar'){
		
		$date = $tgl;
		$strtgl = strtotime($date);
		$nid = strtotime('+1 month',$strtgl);
		$pid = strtotime('-1 month',$strtgl);
		$month= date("m", $strtgl);
		$nmonth= date("M", $strtgl);
		$year= date("Y", $strtgl);
		$title= date("F Y", $strtgl);
		$today=strtotime(date("Y-m-d"));
		
		
         if((substr($month, 0, 1)) == 0)
         {
            $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         $str = '<table class="table calendar-date-mini">';            
         $headings = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
         $str.= '<thead><tr><td colspan=2 align="right"><a href='.$this->location('module/penjadwalan/ruang/terpakai/'.$pid).'><</a></td><td colspan="3" align="center"><b>'.$title.'</b></td>
				<td colspan=2 align="left"><a href='.$this->location('module/penjadwalan/ruang/terpakai/'.$nid).'>></a></td></tr>
				<tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
				 $stra = strtotime($year."-".$month."-".$list_day);
                 $str.= '<div class="'. $style .'-day-number"><a href='.$this->location('module/penjadwalan/ruang/terpakai/'.$stra).'><span class="text text-default">';
				if($stra==$strtgl){
					$str.= '<span class="label label-success">'.$list_day.'</span>';
				}else{
					$str.= $list_day;
				 }
				 $str.= '</span></a></div></td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
             
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
         $str.= '</tr>';
		 $str.= '<tr><td colspan=7 align=center  class="'. $style .'-day-head"><a href='.$this->location('module/penjadwalan/ruang/terpakai/'.$today).'><span class="text text-default">Today</span></a></td></tr>';
		 $str.= '</tbody>';    
         $str.= '</table>';
		 
		 return $str;
	}
}
?>