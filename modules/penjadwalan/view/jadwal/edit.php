	
<?php
$this->head();
$header			= "Penjadwalan";	
?>
<div class="row">
<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('home'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/penjadwalan/jadwal'); ?>">Penjadwalan</a></li>
	  <li class="active"><a href="#">Data</a></li>
	</ol>
  
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php endif;?>
	
	<div class="col-md-4">	
			<form name="frmDosen" id="frmDosen" method=post action="<?php echo $this->location('module/penjadwalan/jadwal')?>" >	
				<div class="well">
						<div class='form-group'>
							<label class="control-label">Jenis Kegiatan</label>
							
								<select name="cmbjenis" onChange="form.submit();" class='cmbmulti populate'>
									<option value="-">Jenis Kegiatan</option>
									<?php
										foreach($kegiatan as $dt):
											echo "<option value='".$dt->jenis_kegiatan_id."' ";
											if($jeniskegiatan==$dt->jenis_kegiatan_id){
												echo "selected ";
											}
											echo  ">".$dt->keterangan."</option>";
										endforeach;
									?>
								</select>
							
						</div>

						<div class='form-group'>
							<label class="control-label">Semester</label>
							
								<select name="cmbsemester" onChange='form.submit();' class='cmbmulti populate'>
									<option value="-">Semester</option>
									<?php
									foreach($semester as $dt):
										echo "<option value='".$dt->tahun_akademik."' ";
										if($semesterid==$dt->tahun_akademik){
											echo "selected";
										}
										echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
									endforeach;
									?>
								</select>
						</div>
						<?php
						if(($jeniskegiatan=='kuliah')||($jeniskegiatan=='praktikum')){ ?>
						
						<div class='form-group'>
							<label class="control-label">MK Diampu</label>
							
								<select name="cmbmk" onChange='form.submit();' class='cmbmulti populate'>
									<option value="0">Semua MK</option>
									<?php
									foreach($mk as $dt):
										echo "<option value='".$dt->mkditawarkan_id."' ";
										if($mkid==$dt->mkditawarkan_id){
											echo "selected";
										}
										echo ">".$dt->namamk."&nbsp;</option>";
									endforeach;	
									?>
								</select>
						</div>
						<?php } ?>
						
						<?php if($jeniskegiatan!='praktikum'){ ?>
						
						<div class='form-group'>
							<label class=control-label>Dosen</label>
							
								<select name="dosenid" onChange='form.submit();' class='cmbmulti  populate'>
									<option value="0">Semua Dosen</option>
									<?php
									foreach($mdosen as $dt):
										echo "<option value='".$dt->id."' ";
										if($dosenid==$dt->id){
											echo "selected";
										}
										echo ">".$dt->value."&nbsp;</option>";
									endforeach;	
									?>
								</select>
						</div>
						
						<?php } ?>
						
						<div class='form-group'>
							<label class=control-label>Ruang</label>
							
								<select name="cmbruang" onChange='form.submit();' class='cmbmulti populate'>
									<option value="0">Semua Ruang</option>
									<?php
									foreach($ruangan as $dt):
										echo "<option value='".$dt->id."' ";
											if($ruangid==$dt->id){ echo "selected"; }
										echo ">".$dt->value."</option>";
									endforeach;
									?>
								</select>
						</div>											
					</div>
				
			
			
			<?php if(($jeniskegiatan) && ($semesterid!='-')){ ?>
				<div class='form-group'>				
					<?php 
						switch ($jeniskegiatan){
							case 'kuliah':
								$this->view('jadwal/detailjadwal.php', $data); 
							break;
							
							case 'praktikum':
								$this->view('jadwal/detailjadwal.php', $data); 
							break;
							
							default:
								$this->view('jadwal/detailkegiatan.php', $data); 
							break;
						
						}?>				
				</div>	
				<?php  } ?>
			</form>			
		</div>
		
		<div class="col-md-8">
	 
			<?php
			if(isset($posts)) :	
				echo $posts;
			 else: 
			 ?>
			
				<div class="well">Sorry, no content to show</div>
			<?php 
			endif; 
			?>
		</div>
</div>
	<?php
$this->foot();
?>
