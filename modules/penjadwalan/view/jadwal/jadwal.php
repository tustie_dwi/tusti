<?php
class penjadwalan_jadwal extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 		
	}
		
	function  index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		
		$mjadwal = new model_jadwal();	
		
		$data['posts'] = $mjadwal->read($keyword, $page, $perpage,"");	
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		switch ($by){
			case 'ok':
				$data['status'] = 'OK';
				$data['statusmsg'] = 'OK, data telah diupdate.';
			break;
			case 'nok':
				$data['status'] = 'NOK';
				$data['statusmsg'] = 'Maaf, data tidak dapat diupdate.';
			break;
		}
		
			
		$this->view( 'jadwal/index.php', $data );
	}
	
	function ok(){
		$this->index('ok');
	}
	function nok(){
		$this->index('nok');
	}
	
		
	function save_kuliah($type=null){
		ob_start();
		
	
		$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();
		
		$semester	= $_POST['cmbsemester'];
				
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		
		if(isset($_POST['hidId'])){
		//	$jadwalid 	= $_POST['hidId'];
			$action 	= "update";
		}else{
			//$jadwalid	= $today.$jam;
			$action 	= "insert";
		}
		
		
		if($_POST['cmbmk']!=0){
			$mkid	 = $_POST['cmbmk'];
		}else{
			$mkid	 = $_POST['mkid'];
		}
		
		if($_POST['cmbruang']){
			$ruang	 = $_POST['cmbruang'];
		}else{
			$ruang	 = $_POST['ruangid'];
		}
		
		$prodi	 	= $_POST['prodiid'];
		//$hari	 	= strToLower($_POST['hari']);
		$kelas	 	= strToUpper($_POST['kelas']);
		$repeaton	= $_POST['cmbrepeat'];
		$semester	= $_POST['cmbsemester'];		
		$jammulai	= $_POST['jammulai'];
		$jamselesai	= $_POST['jamselesai'];
		$jeniskegiatan	= $_POST['cmbjenis'];

		
		$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);
		//$nhari		= $mconf->getNHari($hari);
		
		
		if($type){
			$dosen		= 'Asisten';	
			$dosenid	= '-';
			$staffid	= '-';
			//$jadwalid 	= '2'.$nhari.$mkid.substr($jammulai,0,2).substr($semester,4,2);		
			$kode		= strtoUpper(substr($jeniskegiatan,0,2).$mkid.substr($semester,4,2));
			$isprak		=1;
		}else{
			if(isset($_POST['pengampu'])){
				$dosen		= $mconf->get_nama_dosen($_POST['pengampu']);	
				$dosenid	= $mconf->get_dosen_id($_POST['pengampu'],$mkid);
				$staffid	= $_POST['pengampu'];
			}else{
				$dosen		= $_POST['dosen'];	
				$dosenid	= $mconf->get_dosen_id($_POST['dosenid'],$_POST['mkid']);
				$staffid	= $_POST['dosenid'];
			}
			
			//$jadwalid 	= '1'.$nhari.$staffid.substr($jammulai,0,2).substr($semester,4,2);
		
			$kode	= strtoUpper(substr($jeniskegiatan,0,2).$staffid.substr($semester,4,2));
			$isprak		=0;
		}
		
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->username;		
		
		
		
				
		if($dosenid!=0){	
			$datanya = Array('jadwal_id'=>$kode, 'tahun_akademik'=>$semester, 'karyawan_id'=>$staffid, 'jenis_kegiatan_id'=>$jeniskegiatan, 
							'repeat_on'=>$repeaton, 'user'=>$user, 'last_update'=>$lastupdate);
			$mjadwal->replace_penjadwalan($datanya);
		
			for($j=0;$j<count($_POST['chkhari']);$j++){
				//$hari	 	= strToLower($_POST['hari']);
				$hari		= strToLower($_POST['chkhari'][$j]);
				$nhari		= $mconf->getNHari($hari);
			
				$keterangan	= $dosen."(".Ucwords($hari).")";
				
				if($type){
					
					$jadwalid 	= '2'.$nhari.$mkid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);		
				}else{
					
					$jadwalid 	= '1'.$nhari.$staffid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);	
				}
				
				$datanya = Array('detail_id'=>$jadwalid, 'jadwal_id'=>$kode, 'ruang'=>$ruang, 'hari'=>$hari, 'jam_mulai'=>$jammulai,'jam_selesai'=>$jamselesai, 
				'is_aktif'=>1, 'user'=>$user, 'last_update'=>$lastupdate);
				$mjadwal->replace_detail_penjadwalan($datanya);
				
		
				
				$datanya	= array('jadwal_id'=>$jadwalid, 'dosen_id'=>$dosenid, 'mkditawarkan_id'=>$mkid, 'kelas_id'=>$kelas, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 
										'hari'=>$hari, 'ruang'=>$ruang, 'is_praktikum'=>$isprak, 'prodi_id'=>$prodi, 'user'=>$user, 'last_update'=>$lastupdate);
						
				if($kelas){
					
					$mjadwal->replace_jadwalmk($datanya);
					$mconf->log("dbptiik_akademik.tbl_jadwalmk", $datanya, $user, $action);
					
					$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester,$repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $staffid, $kegiatan, 
					$keterangan, $user, $lastupdate, $action,$kelas,$mkid);
					
					$mjadwal->update_jadwal_exist($dosenid, $mkid, $jammulai, $kelas, $ruang, $hari, $isprak, $jadwalid);
					$mjadwal->update_dpenjadwalan($jadwalid,$jammulai, $hari, $ruang);
					
				}
			}
		}
		unset($_POST['b_add']);
		$this->write('ok');
		
		exit();
	}
	
	function save_kegiatan(){
		ob_start();
			
		$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();
		
		$semester	= $_POST['cmbsemester'];
				
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		
		
		$repeaton	= $_POST['cmbrepeat'];
		$semester	= $_POST['cmbsemester'];			
		$jammulai	= $_POST['jammulai'];
		$jamselesai	= $_POST['jamselesai'];
		$jeniskegiatan	= $_POST['cmbjenis'];
		
		if($_POST['dosenid']!="0"){
			$dosenid	= $_POST['dosenid'];
			$dosen		= $_POST['dosen'];
		}else{
			$dosenid	= $_POST['ndosenid'];
			$dosen		= $_POST['ndosen'];
		}
		
				
		
		if(isset($_POST['hidId'])){
			$kode 	= $_POST['hidId'];
			$action = "update";
		}else{
			$kode	= strtoUpper(substr($jeniskegiatan,0,2).$dosenid.substr($semester,4,2));
			$action = "insert";
		}
				
		
		if($_POST['cmbruang']!=0){
			$ruang	 = $_POST['cmbruang'];
		}else{
			$ruang	 = $_POST['ruangid'];
		}		
						
		$kegiatan	= $mconf->get_nama_kegiatan($jeniskegiatan);
			
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->username;
			
		
		if($dosenid){		
			$datanya = Array('jadwal_id'=>$kode, 'tahun_akademik'=>$semester, 'karyawan_id'=>$dosenid, 'jenis_kegiatan_id'=>$jeniskegiatan, 
								'repeat_on'=>$repeaton, 'user'=>$user, 'last_update'=>$lastupdate);
			$mjadwal->replace_penjadwalan($datanya);
			
			for($j=0;$j<count($_POST['chkhari']);$j++){
				//$hari	 	= strToLower($_POST['hari']);
				$hari		= strToLower($_POST['chkhari'][$j]);
				$nhari		= $mconf->getNHari($hari);
				
				$jadwalid = $nhari.$dosenid.substr($jammulai,0,2).substr($semester,4,2).str_replace('.','',$ruang);	
				
				$keterangan	= $dosen."(".Ucwords($hari).")";
			
				$datanya = Array('detail_id'=>$jadwalid, 'jadwal_id'=>$kode, 'ruang'=>$ruang, 'hari'=>$hari, 'jam_mulai'=>$jammulai,'jam_selesai'=>$jamselesai, 'is_aktif'=>1, 'user'=>$user, 'last_update'=>$lastupdate);
				$mjadwal->replace_detail_penjadwalan($datanya);
				
				$this->insert_detail_jadwal_ruang($mjadwal, $mconf, $semester, $repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $dosenid, 
				$kegiatan, $keterangan, $user, $lastupdate, $action,'-','-');
			}
		}
			
		unset($_POST['b_add']);
		$this->write('ok');
		
		exit();
	}
	
	function insert_detail_jadwal_ruang($mjadwal, $mconf,$semester,$repeaton, $kode, $jadwalid, $ruang, $jammulai, $jamselesai, $hari, $jeniskegiatan, $dosenid, $kegiatan, $keterangan, 
	$user, $lastupdate, $action, $kelas, $mk){
		/*$mjadwal = new model_jadwal();
		$mconf	 = new model_conf();*/
		
		switch($repeaton){
			case 'daily':
				$in	  = strtotime(date("Y-m-d"));
				$out  = strtotime('+7 days', $in);
				$y=1;
			break;
			
			case 'weekly':
				$in	  = strtotime(date("Y-m-d"));
				$out  = strtotime('+7 days', $in);
				$y=7;
			break;
			
			case  'monthly':
				$in	  = strtotime(date("Y-m-d"));
				$out  = strtotime('+1 month', $in);
				$y=$this->dateDiff($in,$out);
			break;
			
			case 'yearly':
				$in	  = strtotime(date("Y-m-d"));
				$out = strtotime('+1 year', $in);
				$y	= $this->dateDiff($in,$out);
			break;
		}
		
		$n = $this->dateDiff($in,$out);
		
		$datanya = array('jadwal_id'=>$jadwalid, 'ruang_id'=>$ruang, 'kegiatan'=>$kegiatan, 'catatan'=>$keterangan, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai , 
						 'repeat_on'=>$repeaton, 'user_id'=>$user, 'last_update'=>$lastupdate, 'jenis_kegiatan_id'=>$jeniskegiatan, 'tahun_akademik'=>$semester);
		$mconf->replace_jadwal_ruang($datanya);
		
		$mconf->log("dbptiik_master.tbl_jadwalruang", $datanya, $user, $action);				
		
		$detailid	= $n.$jadwalid;
								
		for($i=0;$i<=$n;$i++){		
			$shari 	= $mconf->getHari(date("N",  strtotime('+'.$i.' days', $in)));
			$nhari	= date("N",  strtotime('+'.$i.' days', $in));
			$tgl	= date("Y-m-d", strtotime('+'.$i.' days', $in));			
			
			if(($shari==$hari)){														
				$this->insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i,$ruang, $kode, $jeniskegiatan,$dosenid,$kelas,$mk);
			}			
		}		
		
	}
	
		
	function write($note=NULL, $style = "calendar"){
		if(isset($_POST['b_add'])){
			switch($_POST['cmbjenis']){
				case 'kuliah':
					$this->save_kuliah();
				break;
				
				case 'praktikum':
					$this->save_kuliah(1);
				break;
				
				default :
					$this->save_kegiatan();
				break;
			}
		}
		
		$mjadwal = new model_jadwal();	
		$mconf	 = new model_conf();
		
		if(isset($_POST['cmbsemester'])){
			$semesterid = $_POST['cmbsemester'];
		}else{
			$semesterid = "";
		}
		
		if(isset($_POST['cmbmk']) ){
			$mkid = $_POST['cmbmk'];
		}else{
			$mkid = "";
		}
		
		if(isset($_POST['cmbruang'])){
			$ruangid = $_POST['cmbruang'];
		}else{
			$ruangid = "";
		}
		
		if(isset($_POST['dosen'])){
			$data['namadosen']	= $_POST['dosen'];
		}else{
			$data['namadosen']	= "";
		}
		
		if(isset($_POST['dosenid'])){
			$staffid	= $_POST['dosenid'];
		}else{
			$staffid	= "";
		}
						
		if(isset($_POST['cmbjenis'])){
			$data['jeniskegiatan'] = $_POST['cmbjenis'];
		}else{
			$data['jeniskegiatan'] = "";
		}
		
		if($data['jeniskegiatan']=='praktikum'){
			$dosenid	= "";
		}else{
			$dosenid	= $staffid;
		}
		
		$data['kegiatan'] 	= $mjadwal->get_jenis_kegiatan();
		$data['semester']	= $mconf->get_semester();
		$data['ruangan']	= $mconf->get_ruang();
		$data['mdosen']		= $mconf->get_dosen();
		$data['ruangid']	= $ruangid;
		$data['dosenid']	= $dosenid;
		$data['mkid']		= $mkid;
		$data['semesterid']	= $semesterid;		
		
		//echo $dosenid;
		if($dosenid!=0){
			$data['mk']		= $mconf->get_mk_diampu($dosenid, "");
		}else{
			$data['pengampu'] = $mconf->get_mk_diampu($dosenid, $mkid);
			if($data['jeniskegiatan']=='kuliah'){
				$data['mk']	= $mconf->get_mkditawarkan(0,$semesterid);
			}else{
				$data['mk']	= $mconf->get_mkditawarkan(1,$semesterid);
			}
		}
		
		$data['posts']		= $mjadwal->draw_grid_jadwal($semesterid, $mkid, $dosenid, $ruangid, $style, $data['jeniskegiatan'] );
		$data['repeaton']	= Array('daily', 'weekly', 'monthly', 'yearly');
		$data['hari']   	= $mconf->get_hari("");
		$data['infhari']	= '';
		$data['repeatin']	= 'monthly';
		
		$this->coms->add_style('css/calendar/calendar.css');		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('bootstrap/bootstrap-tooltip.js');
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->add_script('js/jsFunction.js');
							
		$this->view('jadwal/edit.php', $data);
	}
	
		
	function edit($id, $style='calendar'){
		if(!$id) {
			$this->redirect('module/penjadwalan/jadwal');
			exit;
		}
		$mjadwal = new model_jadwal();	
		$mconf	 = new model_conf();
		
		
		$jadwal = $mjadwal->read("",1,1, $id);
		
		foreach($jadwal as $dt):
			$semesterid = $dt->tahun_akademik;	
			$staffid	= $dt->karyawan_id;	
			$staff		= $dt->nama;	
			$data['jeniskegiatan'] 	= $dt->jenis_kegiatan_id;
		endforeach;
				
		$mkid		= "";
		$ruangid	= "";
		
		if($data['jeniskegiatan']=='praktikum'){
			$data['namadosen']="";
			$dosenid	= "";
		}else{
			$data['namadosen']= $staff;
			$dosenid	= $staffid;
		}
		
		$data['kegiatan'] 	= $mjadwal->get_jenis_kegiatan();
		$data['semester']	= $mconf->get_semester();
		$data['ruangan']	= $mconf->get_ruang();
		$data['ruangid']	= $ruangid;
		$data['dosenid']	= $dosenid;
		$data['mdosen']		= $mconf->get_dosen();
		$data['mkid']		= $mkid;
		$data['semesterid']	= $semesterid;
		if($dosenid && ($data['jeniskegiatan']!='praktikum')){
			$data['mk']		= $mconf->get_mk_diampu($dosenid, "");
		}else{
			$data['pengampu'] = $mconf->get_mk_diampu($dosenid, $mkid);
			if($data['jeniskegiatan']=='kuliah'){
				$data['mk']	= $mconf->get_mkditawarkan(0,$semesterid);
			}else{
				$data['mk']	= $mconf->get_mkditawarkan(1,$semesterid);
			}
		}
		
		$data['posts']		= $mjadwal->draw_grid_jadwal($semesterid, $mkid, $dosenid, $ruangid, $style, $data['jeniskegiatan'] );
		$data['repeaton']	= Array('daily', 'weekly', 'monthly', 'yearly');
		$data['repeatin']	= 'monthly';
		$data['hari']   	= $mconf->get_hari("");
		$data['infhari']	= '';
		
		$this->coms->add_style('css/calendar/calendar.css');		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('bootstrap/bootstrap-tooltip.js');
		$this->add_script('js/jsFunction.js');
							
		$this->view('jadwal/edit.php', $data);
	}	
	
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		return $ext; 
	}  
	
	function delete_ruang($id){
		$mconf   = new model_conf();	
		
		$datanya  = array('jadwal_id'=>$id);	
		$mconf->delete_jadwal_ruang($datanya);
		$mconf->delete_jadwal_ruang_detail($datanya);
	}
	
	function delete_jadwal($id){
		$mjadwal = new model_jadwal();
		
		$datanya  = array('detail_id'=>$id);
		$mjadwal->delete_jadwal_kegiatan($datanya);
	}

	function delete($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('detail_id'=>$id);	
		
		if($mjadwal->delete_jadwal_kegiatan($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function deletemk($id){
	
		$mjadwal = new model_jadwal();	
				
		$result   = array();
		
		$datanya  = array('jadwal_id'=>$id);	
		
		if($mjadwal->delete_jadwalmk($datanya)){ 		
			$result['status'] = "OK";
			$this->delete_jadwal($id);
			$this->delete_ruang($id);
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mjadwal->error;
		}
		
		echo json_encode($result);		
	}
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 200) {

			// truncate string
			$stringCut = substr($string, 0, 200);

			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}
	
	function dateDiff($in,$out){		
		$interval =round(($out - $in)/(3600*24));
		return  $interval ;
	} 
	
	function insert_detail($jadwalid, $tgl, $shari, $nhari, $jammulai, $jamselesai, $user, $action,$i, $ruang, $infjadwal, $infjeniskegiatan, $dosen, $kelas, $mk){
		$mconf 	 = new model_conf();			
		
		$detailid = $nhari.$jadwalid.$i;
	
		//echo $detailid;
		$datanya = array('detail_id'=>$detailid, 'jadwal_id'=>$jadwalid, 'tgl'=>$tgl, 'hari'=>$shari, 'inf_hari'=>$nhari, 'jam_mulai'=>$jammulai, 'jam_selesai'=>$jamselesai, 'ruang'=>$ruang, 'inf_jadwal_id'=>$infjadwal, 'inf_jenis_kegiatan'=>$infjeniskegiatan, 'dosen_id'=>$dosen, 'kelas'=>$kelas, 'mk_id'=>$mk);
		$mconf->replace_detail_jadwal_ruang($datanya);
		
		$mconf->update_jadwal_valid($jadwalid, $tgl, $shari, $jammulai, $jamselesai, $ruang, $infjeniskegiatan, $dosen, $kelas, $mk);
		
		$mconf->log("dbptiik_master.tbl_detailjadwalruang", $datanya, $user, $action);			
		
	}

}
?>