
<table class="table table-bordered table-condensed">
	<thead>
		<tr><th colspan="2"><h4>Detail Jadwal</h4></th></tr>
	</thead>
	<tbody>
		<?php if($mkid=='0'){ ?>
		<tr><td><small><em>Matakuliah</em></small></td><td>			
				<select name='mkid' class="cmbmulti span12 populate ">
				<?php
					foreach($mk as $row):
						echo "<option value='".$row->mkditawarkan_id."'>".$row->namamk."&nbsp;</option>";
					endforeach;	
				?>
				</select>
		</td></tr>
		<?php } 
		if(($dosenid=='0') && $jeniskegiatan!='praktikum'){ ?>
		<tr><td><small><em>Pengampu</em></small></td><td>	
			<select name='pengampu' class="cmbmulti span12 populate ">
				<?php
					foreach($pengampu as $row):
						echo "<option value='".$row->karyawan_id."'>".$row->nama."&nbsp;</option>";
					endforeach;	
				?>
				</select>
		</td></tr>
		<?php } ?>
		<tr><td><small><em>Prodi</em></small></td><td>	
			<select name="prodiid" class='cmbmulti span12 populate'>		
				<?php
				foreach($prodi as $dt):
					echo "<option value='".$dt->id."' ";
						//if($ruangid==$dt->id){ echo "selected"; }
					echo ">".$dt->value."</option>";
				endforeach;
				?>
			</select>
		</td></tr>		
		
		<?php
		if($ruangid=='0'){ ?>
			<tr><td><small><em>Ruang</em></small></td><td>
				<select name="ruangid" class='cmbmulti span12 populate'>				
				<?php
				foreach($ruangan as $dt):
					echo "<option value='".$dt->id."' ";
						//if($ruangid==$dt->id){ echo "selected"; }
					echo ">".$dt->value."</option>";
				endforeach;
				?>
			</select>
			</td></tr>	
		<?php } ?>
		<tr><td><small><em>Kelas</em></small></td><td>
				<?php
				$kelas = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
				?>
				<select name="kelas" class='cmbmulti span3 populate'>
					
					<?php
					for($i=0;$i<count($kelas);$i++){
						echo "<option value='".$kelas[$i]."' ";
							//if($ruangid==$dt->id){ echo "selected"; }
						echo ">".$kelas[$i]."</option>";
					}
					?>
				</select> 
			</td></tr>
		<tr><td><small><em>Waktu</em></small></td><td>
											
				<select name="jammulai" class='cmbmulti span4 populate'>
					
					<?php
					foreach($jammulai as $dt):
						echo "<option value='".$dt->id."' ";
							//if($ruangid==$dt->id){ echo "selected"; }
						echo ">".$dt->value."</option>";
					endforeach;
					?>
				</select> &nbsp;&nbsp;
				<select name="jamselesai" class='cmbmulti span4 populate'>
					
					<?php
					foreach($jamselesai as $dt):
						echo "<option value='".$dt->value."' ";
							//if($ruangid==$dt->id){ echo "selected"; }
						echo ">".$dt->value."</option>";
					endforeach;
					?>
				</select>
		</td></tr>
		
		<tr><td><small><em>Hari</em></small></td><td><?php
				$chari = explode(",", $infhari);				
				
				$i=0;
				$skip = false;
				
				for($i = 0; $i < count($hari); $i++){						
					$skip=false;
					if($infhari!=""){	
						
						for($j=0;$j<count($chari);$j++){	
							
							if($chari[$j] == $hari[$i]->id){
								echo "<label class='checkbox inline'><input type=checkbox name='chkhari[]' value='".$hari[$i]->value."' checked";
								
								$skip=true;
								break;										
							}
						}
						if(!$skip) echo "<label class='checkbox inline'><input type=checkbox name='chkhari[]' value='".$hari[$i]->value."' ";								
					}else{
						echo "<label class='checkbox inline'><input type=checkbox name='chkhari[]' value='".$hari[$i]->value."' ";
					}
					
					echo ">".ucFirst($hari[$i]->value)."</label>";
					if($i==3){
						echo "<br>";
					}
				}
			?></td></tr>
		<tr>
			<td><small><em>Repeat On</em></small></td>
			<td>		
				<input type="text" name="tmulai" class="span5 date" value="<?php echo $tmulai; ?>" > s/d <input type="text" name="tselesai" class="span5 date" value="<?php echo $tselesai; ?>"><br>
				<!--<select name='cmbrepeat' class='cmbmulti span9 populate'>
					<option value="daily">Daily</option>
				<?php
				for($i=0;$i<count($repeaton);$i++){
					echo "<option value='".$repeaton[$i]."' ";
					if($repeatin==$repeaton[$i]){
						echo "selected";
					}
					echo ">".Ucfirst($repeaton[$i])."</option>";
				}							
				?>
				</select>-->
			</div>
			</td>
		</tr>		
	</tbody>
 </table>
 


<div class="control-group well">
	<input type="submit" name="b_add" value=" Add Detail Jadwal" class="btn btn-info">
</div>	