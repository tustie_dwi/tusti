<?php
if($ujian){
	foreach($ujian as $dt){
		//$ntgl		= $dt->tgl;
		$mulai		= $dt->jam_mulai;
		$selesai	= $dt->jam_selesai;
		$ruang		= $dt->ruang;
		$id			= $dt->ujian_id;	
		$isproject	= $dt->is_project;
	}
}else{
	$ntgl	= $tgl;
	$mulai	= "";
	$selesai= "";
	$ruang	= "";
	$id		= "";
	$isproject	= 0;
}	



if($pengawas){
	$npengawas = "";	
	$i=0;
	foreach($pengawas as $dt):
		$i++;
		if($i>1){
			$str = ",";
		}else{
			$str = "";
		}
		$npengawas = $npengawas . $str. $dt->nama ;
	endforeach;
}else{
	$npengawas = "";
}

?>
 <table class="table table-bordered table-condensed">
	<thead>
		<tr><th colspan="2"><h4>Detail Jadwal</h4></th></tr>
	</thead>
	<tbody>
		<tr><td><small><em>Prodi</em></small></td><td>
		<select name='cmbprodi' onChange='form.submit();' class="cmbmulti populate span12">
			<option value="-">Please Select</option>
		<?php
			if($prodi){
				foreach($prodi as $row):
					echo "<option value='".$row->id."' ";
					if($prodiid==$row->id){ echo "selected"; }
					echo ">".$row->value."&nbsp;</option>";
				endforeach;	
			}
		?>
		</select>
		</td></tr>
		
		<tr><td><small><em>Kelas</em></small></td><td>
		<select name='cmbkelas' onChange='form.submit();' class="cmbmulti populate span12">
			<option value="-">Please Select</option>
		<?php
			if($kelas){
				foreach($kelas as $row):
					echo "<option value='".$row->id."' ";
					if($kelasid==$row->id){ echo "selected"; }
					echo ">".$row->value."&nbsp;</option>";
				endforeach;	
			}
		?>
		</select>
		</td></tr>
		<tr>
			<td><small><em>Tgl</em></small></td>
			<td>
				<input type="text" name="tgl" class="date" value="<?php echo $tgl; ?>">					
			</td>
		</tr>
		
		<tr><td><small><em>Pengampu</em></small></td><td>
		<select name='pengampu' onChange='form.submit();' class="cmbmulti populate span12">
			<option value="0">Please Select</option>
		<?php
			if($pengampu){
				foreach($pengampu as $row):
					echo "<option value='".$row->karyawan_id."' ";
					if($staffid==$row->karyawan_id) { echo "selected"; } 
					echo ">".$row->nama."&nbsp;</option>";
				endforeach;	
			}
		?>
		</select>
		</td></tr>
		
				
		<tr><td><small><em>Pengawas</em></small></td><td>
		<input type="hidden" value="<?php echo $npengawas; ?>" class="tmpstaff" />	
		<input type='text' name='tpeserta' placeholder='Nama Pengawas..' class='tagStaff span12'/>
		</td></tr>
				
		<tr><td><small><em>Ruang</em></small></td><td>
		<select name="ruangid" class='cmbmulti span12 populate'>		
			<?php
			foreach($ruangan as $dt):
				echo "<option value='".$dt->id."' ";
					if($ruang==$dt->id){ echo "selected"; }
				echo ">".$dt->value."</option>";
			endforeach;
			?>
		</select>
		</td></tr>
		
		
		
		<tr>
			<td><small><em>Jam</em></small></td>
			<td>			
				<input type=text name='jammulai' class='span4 jammulai' placeholder="Mulai.." value="<?php echo $mulai; ?>" required><span class="add-on"> s/d </span>
				<input type=text name='jamselesai' class='span4 jamselesai' placeholder="Selesai.." value="<?php echo $selesai; ?>" required>
				<input type=hidden name='jammulaiid' id='jammulaiid'>
				<input type=hidden name='jamselesaiid' id='jamselesaiid'>	
			</td>
		</tr>
		
		
			<tr>
			<td><small><em>Project?</em></small></td>
			<td>
				<input type="checkbox" name="chkproject" value="1" <?php if($isproject=='1') { echo "checked"; } ?> >&nbsp;Ya			
			</td>
		</tr>
	</tbody>
 </table>
 
<div class="control-group well">
	<input type="submit" name="b_add" value=" Add Detail Jadwal" class="btn btn-info">
</div>	