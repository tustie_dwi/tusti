<div class="legenda">
	<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small class="margin">Prodi Informatika</small>
	<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small class="margin">Prodi Sistem Komputer</small>
	<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small>Prodi Sistem Infomasi</small>
	<span class="label label-inverse">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small>Project</small>
	<code class="pull-right">Informasi jadwal ujian pada semester aktif.</code>
</div>
<div class="table-responsive">
<table class="table table-bordered table-jadwal">
	<thead>
		<tr class="calendar-row">
			<th class="calendar-day-head"><small>R/J</small></th>
			<?php
				foreach($jam as $dtjam):
					echo "<th class='calendar-day-head'><small>".$dtjam->mulai." - ".$dtjam->selesai."</small></th>";
				endforeach;
			?>
		</tr>
	</thead>
	<tbody>
	<?php
		if($ruang):
			foreach($ruang as $dtruang):
				echo "<tr><td class='ruang'>".ucFirst($dtruang->value)."</td>";
					$i=0;
					$skip = false;
					for($i = 0; $i< count($jam); $i++){

						if($jam[$i]->is_istirahat==1){
							$class = "istirahat";
						}else{
							$class = "";
						}
						
						echo "<td class='popup-form' data-ruang='".$dtruang->id."|".$dtruang->value."' data-jammulai='".$jam[$i]->mulai."' data-jamselesai='".$jam[$i]->selesai."' data-tgl=''>&nbsp;</td>";
					
					}

				echo "</tr>";
			endforeach;
		endif;
		?>
	</tbody>
</table>
<div class="legenda">
	<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small class="margin">Informatika / Ilmu Komputer</small>
	<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small class="margin">Sistem Komputer</small>
	<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small>Sistem Infomasi</small>
</div>
</div>