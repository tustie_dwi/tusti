	
<?php
$this->head();
$header			= "Penjadwalan";	
?>
<div class="col-md-12">  
	<!--<legend>
		<!--<a href="<?php echo $this->location('module/penjadwalan/jadwal'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Penjadwalan List</a> 
		<?php if($posts){	?>
		<a href="<?php echo $this->location('module/penjadwalan/jadwal/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="fa fa-edit"></i> Write New Penjadwalan</a>
		<?php } ?>-->
		<?php //echo $header; ?>
   <!-- </legend> -->
	<div class="row">
		<div class="span4">
			<form name="frmDosen" id="frmDosen" class="form-inline" method=post action="<?php echo $this->location('module/penjadwalan/jadwal/write')?>" >	
				<div class="well">
						<div class="form-group">
							<label>Jenis Kegiatan</label>
							
								<select name="cmbjenis" class="form-control cmbmulti cmbkegiatan" id="cmbjenis">
									<option value="-">Jenis Kegiatan</option>
									<?php
										foreach($kegiatan as $dt):
											echo "<option value='".$dt->jenis_kegiatan_id."' ";
											if($jeniskegiatan==$dt->jenis_kegiatan_id){
												echo "selected ";
											}
											echo  ">".$dt->keterangan."</option>";
										endforeach;
									?>
								</select>
							
						</div>

						<div class="form-group">
							<label>Semester</label>
							
								<select name="cmbsemester"  class="form-control cmbmulti cmbkegiatan" id="cmbsemester">
									<option value="-">Semester</option>
									<?php
									foreach($semester as $dt):
										echo "<option value='".$dt->tahun_akademik."' ";
										if($semesterid==$dt->tahun_akademik){
											echo "selected";
										}
										echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
									endforeach;
									?>
								</select>
						</div>
						<?php
						if(($jeniskegiatan=='kuliah')||($jeniskegiatan=='praktikum')){ ?>
						
						<div class="form-group">
							<label>MK Diampu</label>
							
								<select name="cmbmk" class="form-control cmbmulti cmbkegiatan" id="cmbmk">
									<option value="0">Semua MK</option>
									<?php
									foreach($mk as $dt):
										echo "<option value='".$dt->mkditawarkan_id."' ";
										if($mkid==$dt->mkditawarkan_id){
											echo "selected";
										}
										echo ">".$dt->namamk."&nbsp;</option>";
									endforeach;	
									?>
								</select>
						</div>
						<?php } ?>
						
						<?php if($jeniskegiatan!='praktikum'){ ?>
						
						<div class="form-group">
							<label>Dosen</label>
							
								<select name="dosenid" class='form-control cmbmulti cmbkegiatan' id="dosenid">
									<option value="0">Semua Dosen</option>
									<?php
									foreach($mdosen as $dt):
										echo "<option value='".$dt->id."' ";
										if($dosenid==$dt->id){
											echo "selected";
										}
										echo ">".$dt->value."&nbsp;</option>";
									endforeach;	
									?>
								</select>
						</div>
						
						<?php } ?>
						
						<div class="form-group">
							<label>Ruang</label>
								
								<select name="cmbruang" class="form-control cmbmulti cmbkegiatan" id="cmbruang">
									<option value="0">Semua Ruang</option>
									<?php
									foreach($ruangan as $dt):
										echo "<option value='".$dt->id."' ";
											if($ruangid==$dt->id){ echo "selected"; }
										echo ">".$dt->value."</option>";
									endforeach;
									?>
								</select>
						</div>											
					</div>
				
			<?php echo $jeniskegiatan; if(($jeniskegiatan) && ($semesterid!='-')){ ?>
				<div class='control-group '>				
					<?php 
						switch ($jeniskegiatan){
							case 'kuliah':
								$this->view('jadwal/detailjadwal.php', $data); 
							break;
							
							case 'praktikum':
								$this->view('jadwal/detailjadwal.php', $data); 
							break;
							
							default:
								$this->view('jadwal/detailkegiatan.php', $data); 
							break;
						
						}?>				
				</div>	
				<?php  } ?>
			</form>			
		</div>
		
		<div class="span8" id="content">
	 
			<?php
			if(isset($posts)) :	
				echo $posts;
			 else: 
			 ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php 
			endif; 
			?>
		</div>
		
	</div>
</div>
	<?php
$this->foot();
?>
