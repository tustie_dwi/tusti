<?php
	if(isset($posts)) :	
		echo $posts;
	 else: 
	 ?>
	<div class="span3" align="center" style="margin-top:20px;">
		<div class="well">Sorry, no content to show</div>
	</div>
	<?php 
	endif; 
	?>
	<script>
	$(".btn-delete-post-kegiatan").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/scheduling/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	</script>