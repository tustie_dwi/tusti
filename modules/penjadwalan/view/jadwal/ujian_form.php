<!--<link href="<?php// echo $this->asset("css/bootstrap/bootstrap-tagmanager.css"); ?>" rel="stylesheet">
<script type="text/javascript" src="<?php// echo $this->asset('js/bootstrap/bootstrap-tagmanager.js'); ?>"></script>-->

<div class="form-group">
	<label>MK</label>
	
		<select name="cmbmk" class="cmbmulti cmbdetail form-control" id="cmbmk">
			<option value="-">Please Select</option>
			<?php
			foreach($mk as $dt):
				echo "<option value='".$dt->mkditawarkan_id."' ";
				if($mkid==$dt->mkditawarkan_id){
					echo "selected";
				}
				echo ">".$dt->namamk."&nbsp;</option>";
			endforeach;	
			?>
		</select>
	
</div>	

<div class="form-group">
	<label>Prodi</label>
	
		<select name="cmbprodi" class="cmbmulti cmbdetail form-control" id="cmbprodi">
				<option value="-">Please Select</option>
				<?php
				foreach($prodi as $dt):
					echo "<option value='".$dt->id."' ";
						if($prodiid==$dt->id){ echo "selected"; }
					echo ">".$dt->value."</option>";
				endforeach;
			?>
			</select>
	
</div>

<div class="form-group">
	<label>Kelas</label>
	
		<?php
		$kelas = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		?>
		<select name="cmbkelas" class="cmbmulti cmbdetail form-control" id="cmbkelas">
			
			<?php
			for($i=0;$i<count($kelas);$i++){
				echo "<option value='".$kelas[$i]."' ";
					if($kelasid==$kelas[$i]){ echo "selected"; }
				echo ">".$kelas[$i]."</option>";
			}
			?>
		</select> 
	
</div>
<?php if($tmpid){ ?>
	<div class="form-group">
		<label>Pengampu</label>		
		<select name='pengampu' class="cmbmulti form-control populate ">
			<?php
				foreach($pengampu as $row):
					echo "<option value='".$row->karyawan_id."' ";
					if($dosenid==$row->pengampu_id){ echo "selected"; }
					echo " >".$row->nama."&nbsp;</option>";
				endforeach;	
			?>
			</select>		
	</div>
	
<?php }else{ ?>
	<div class="form-group" id="dosen-form"></div>
<?php } ?>
<input type="hidden" value="<?php echo $tmpid; ?>" id="hidtmp">
<div class="form-group">
	<input type="checkbox" name="chkproject" value="1" <?php if(isset($isproject)&&($isproject)=='1') { echo "checked"; } ?> >&nbsp;<b>Project</b>
</div>
<div id="pengawas-form-a">
	<div class="form-group">
		<a href="#" class="btn btn-success add-pengawas" ><i class="fa fa-plus"></i> Add Pengawas</a>
	</div>
	
	<div class="form-group" id="pengawas-form">
	<?php
	$x=1;
	if($pengawas){		
		foreach($pengawas as $dt):
			
			$pengawasid = $dt->karyawan_id;
			
			?>
			<div class='col-md-6'>				
				<select  class='e9 form-control pengawas-<?php echo $x; ?>' name='pengawas[]' >
					<option value='-'>None</option>
					<?php
					foreach($staff as $row):
						echo "<option value='".$row->id."' ";
						if($pengawasid==$row->id){ echo "selected"; } 
						echo ">". $row->value."</option>";
					endforeach;
					?>
				</select>
			</div>
			<div class="col-md-6">
				<input type="checkbox" name="chk<?php echo $x; ?>" value="1" <?php if($dt->is_hadir=='1'){  echo "checked";} ?> > Hadir			
				&nbsp;
				<input type="checkbox" name="radio<?php echo $x; ?>" value="1" <?php if($dt->pengganti=='1'){  echo "checked";} ?> > Pengganti			
			</div>
			<div class="clearfix"></div>
			<?php
			$x++;
		endforeach;
	}	
	?>

	</div>	
</div>

<div class="row">&nbsp;</div>
<div class="form-group">
	<input type="submit" id="btn-save-jadwal" name="b_add" value = " Save Penjadwalan " class="btn btn-default" onClick="save_proses();">
	
	<a href="#" class="btn btn-danger btn-delete-ujian" data-id="<?php echo $jadwalid; ?>" ><i class="fa fa-trash-o"></i> Delete Jadwal</a>
</div>
<div class="clearfix"></div>
<div>
	<span class="status-submit">&nbsp;</span>
</div>

	
<script>
	$(".btn-delete-ujian").click(function(){
		
		var pid = $(this).data("id");
			
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
					
		$.post(
			base_url + 'module/penjadwalan/jadwal/deleteujian/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					//row.fadeOut();
					load_jadwal();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
		
	

	$('#jadwalform .add-pengawas').click(function(e) {
		var x 			= $(document.getElementById("hidtmp")).val();
				
		var prodi		= document.getElementById("cmbprodi");
		var prodiid		= $(prodi).val();
		
		var mk			= document.getElementById("cmbmk");
		var mkid		= $(mk).val();
		
		var kelas		= document.getElementById("cmbkelas");
		var kelasid		= $(kelas).val();
		var output		= "";		
		
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/get_staff',
				data : $.param({
					
					cmbprodi	: prodiid,
					cmbmk		: mkid,
					cmbkelas	: kelasid
					
				}),
				success : function(msg) {
					
					if (msg == '') {
						//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					} else {
						output += "<div class='col-md-6'><select class='form-control e9 pengawas-"+ x +"' name='pengawas[]' ><option value='-'>None</option>";
						var msg = JSON.parse(msg);
						for (var i = 0; i < msg.length; i++) {
							var data = msg[i];
							output += "<option value="+data.id+" data-uri='1'>"+data.value+"</option>";
						}
						output += "</select></div>";
						output += "<div class='col-md-6'><input type='checkbox' name='chk"+x+"' value='1' >Hadir";
						output += "&nbsp;<input type='checkbox' name='radio"+x+"' value='1'>Pengganti</div>";
						$('#jadwalformdetail #pengawas-form').append(output);
						x = x + 1;
						
					}
				}
			});
	});
	
	$('#jadwalform .cmbdetail').change(function(e) {
	
	
		var prodi		= document.getElementById("cmbprodi");
		var prodiid		= $(prodi).val();
		
		var mk			= document.getElementById("cmbmk");
		var mkid		= $(mk).val();
		
		var kelas		= document.getElementById("cmbkelas");
		var kelasid		= $(kelas).val();
		var output		= "";			
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/get_pengampu',
				data : $.param({
					
					cmbprodi	: prodiid,
					cmbmk		: mkid,
					cmbkelas	: kelasid
					
				}),
				success : function(msg) {
					
					if (msg == '') {
						//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					} else {
						
						output += "<label>Pengampu</label><select id='cmbdosen' class='e9 form-control form-control' name='pengampu' >";
						var msg = JSON.parse(msg);
						for (var i = 0; i < msg.length; i++) {
							var data = msg[i];
							output += "<option value="+data.karyawan_id+" data-uri='1'>"+data.nama+"</option>";
						}
						output += "</select>";
					
						$('#jadwalformdetail #dosen-form').html(output);
						
					}
				}
			});
		
	});	
	
	$(function() {
			$(".e9").select2();			
	}); 	
	
	
</script>


