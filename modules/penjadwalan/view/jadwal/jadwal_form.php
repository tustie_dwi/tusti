<!--<link href="<?php// echo $this->asset("css/bootstrap/bootstrap-tagmanager.css"); ?>" rel="stylesheet">
<script type="text/javascript" src="<?php// echo $this->asset('js/bootstrap/bootstrap-tagmanager.js'); ?>"></script>-->
<?php if($jadwalid){ //$mkid="";$prodiid="";$dosenid="";$tmpid="";$kelasid="";
echo '<div class="form-group" id="new-jadwal"><a href="#" class="btn btn-warning btn-new-jadwal" ><i class="fa fa-check"></i> New Jadwal</a></div>'; } ?>
<div class="form-group">
	<label>MK </label>
	<select name="cmbmk" class="cmbmulti cmbdetail form-control" id="cmbmk">
			<option value="-">Please Select</option>
			<?php
			foreach($mk as $dt):
				echo "<option value='".$dt->mkditawarkan_id."' ";
				if($mkid==$dt->mkditawarkan_id){
					echo "selected";
				}
				echo ">".$dt->namamk."&nbsp;</option>";
			endforeach;	
			?>
		</select>
</div>	

<div class="form-group">
	<label>Prodi</label>
	
		<select name="cmbprodi" class="cmbmulti cmbdetail form-control" id="cmbprodi">
				<option value="-">Please Select</option>
				<?php
				foreach($prodi as $dt):
					echo "<option value='".$dt->id."' ";
						if($prodiid==$dt->id){ echo "selected"; }
					echo ">".$dt->value."</option>";
				endforeach;
			?>
			</select>
</div>

<div class="form-group">
	<label>Kelas</label>
	
		<?php
		$kelas = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		?>
		<select name="cmbkelas" class="cmbmulti cmbdetail form-control" id="cmbkelasx">
			
			<?php
			for($i=0;$i<count($kelas);$i++){
				echo "<option value='".$kelas[$i]."' ";
					if($kelasid==$kelas[$i]){ echo "selected"; }
				echo ">".$kelas[$i]."</option>";
			}
			?>
		</select> 
</div>
<?php 

if($tmpid){ ?>
	<div class="form-group">
		<label>Pengampu</label>		
		<select name='pengampu' class="cmbmulti form-control populate ">
			<option value='130904012736'>Dosen Pengampu</option>
			<?php
				foreach($pengampu as $row):
					echo "<option value='".$row->karyawan_id."' ";
					if($dosenid==$row->pengampu_id){ echo "selected"; }
					echo " >".$row->nama."&nbsp;</option>";
				endforeach;	
			?>
			</select>
	</div>
	
<?php }else{ ?>
	<div class="form-group" id="dosen-form"></div>
<?php } ?>
<input type="hidden" value="<?php echo $tmpid; ?>" id="hidtmp">
<div class="form-group">
	<input type="checkbox" name="chkpraktikum" value="1" <?php if(isset($is_praktikum)&&($is_praktikum)=='1') { echo "checked"; } ?> >&nbsp;<b>Praktikum</b>
</div>

<div class="form-group">
	<input type="submit" id="btn-save-jadwal" name="b_add" value = " Save Penjadwalan " class="btn btn-default" onClick="save_proses();">	
	<a href="#" class="btn btn-danger btn-disable-jadwal" data-id="<?php echo $jadwalid; ?>" ><i class="fa fa-ban"></i> Disabled Jadwal</a>
	<a href="#" class="btn btn-danger btn-delete-jadwal" data-id="<?php echo $jadwalid; ?>" ><i class="fa fa-trash-o"></i> Delete Jadwal</a>
	
</div>
<div class="clearfix"></div>
<div>
	<span class="status-submit">&nbsp;</span>
</div>
<script src="<?php echo $this->location('modules/penjadwalan/assets/js/jadwal.js')?>"></script>
<script>
$(".btn-disable-jadwal").click(function(){
		
		var pid = $(this).data("id");
		
		if(confirm("Disable this schedule? Once done, this action can not be undone.")) {
					
		$.post(
			base_url + 'module/penjadwalan/jadwal/disablemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					//row.fadeOut();
					load_jadwal();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
</script>




