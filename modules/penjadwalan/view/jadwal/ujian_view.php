	<div class="legenda">
	<div class="alert alert-warning">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong>Perhatian!</strong> Untuk memasukkan jadwal, silahkan klik pada masing-masing kolom yang tersedia.
		</div>
	
	</div>

	<?php
		if($tgl){
				
		?>
		<ul class="nav nav-tabs" id="writeTab">
			<?php
			$k=0;
			foreach($tgl as $dt):
				$k++;
			
				if($k==1){ $str="class=active";  }else{ $str="";}
				?><li <?php echo $str; ?>><a href="#<?php echo date("Ymd", strtotime($dt->tgl)); ?>" data-toggle="tab"><?php echo date("M d, Y", strtotime($dt->tgl)); ?></a></li><?php
			endforeach;
			?>
			<li><a href="#newjadwal" data-toggle="tab">New Jadwal</a></li>
		</ul>
		<div class="tab-content">	
			<?php			
			$k=0;
			foreach($tgl as $dt):
			$k++;
			
			if($k==1){ $str="active";  }else{ $str="";}
			?>
			<div class="tab-pane fade-in <?php echo $str; ?>" id="<?php echo date("Ymd", strtotime($dt->tgl)); ?>">
				<?php
				
					if($posts){					
						foreach($posts as $row) {
							$namamk	= $row->namamk;
							$kodemk	= $row->kode_mk;
							$dosen	= $row->nama;
							break;
						}
						switch($by){
							case 'hari';
								$strheader = ucFirst($idhari).", ". date("M d, Y", strtotime($dt->tgl));
							break;

							case 'dosen':
								$strheader = "<a class='btn btn-small disabled' href='#'><i class='icon-time'></i></a> ".date("M d, Y", strtotime($dt->tgl))." <span class='text-warning'> ".$dosen."</span>";
							break;

							case 'mk':
								$strheader = "<a class='btn btn-small disabled' href='#'><i class='icon-time'></i></a> ".date("M d, Y", strtotime($dt->tgl)). " <span class='text-warning'> ". $kodemk. " - ". $namamk."</span>";
							break;

							case 'ruang':
								$strheader = "<a class='btn btn-small disabled' href='#'><i class='icon-time'></i></a> ".date("M d, Y", strtotime($dt->tgl)). " <span class='text-warning'> R. ".ucFirst($idruang)."</span>";
							break;

							case 'prodi':
								$strheader = "Program Studi : ".ucWords($idprodi);
							break;
						}
					?>
					<h4><?php echo $strheader; ?></h4>
					<div class="legenda">
						<div class="alert alert-danger">
							  <button type="button" class="close" data-dismiss="alert">&times;</button>
							  <strong>Perhatian!</strong> Untuk jadwal ujian hari <span class="label label-danger">Jumat</span>, ujian sesi ke <b>2</b> dimajukan <span class="label label-danger">30 menit</span>. Ujian dimulai pukul <span class="label label-danger">09.30</span>.
							</div>
						<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Prodi Informatika</small>
						<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Prodi Sistem Komputer</small>
						<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Prodi Sistem Infomasi</small>
						<span class="label label-inverse">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Project</small>
						<code class="pull-right">Informasi jadwal ujian pada semester aktif.</code>
					</div>
					<div class="table-responsive">
					<table class="table table-bordered table-jadwal">
						<thead>
							<tr class="calendar-row">
								<th class="calendar-day-head"><small>R/J</small></th>
								<?php
									foreach($jam as $dtjam):
										echo "<th class='calendar-day-head'><small>".$dtjam->mulai." - ".$dtjam->selesai."</small></th>";
									endforeach;
								?>
							</tr>
						</thead>
						<tbody>
						<?php
								foreach($ruang as $dtruang):
									echo "<tr><td class='ruang'>".ucFirst($dtruang->value)."</td>";
										$i=0;
										$skip = false;
										for($i = 0; $i< count($jam); $i++){

											if($jam[$i]->is_istirahat==1){
												$class = "label-default";
											}else{
												$class = "";
											}

											if($idhari!='0'){
												$skip = false;
												foreach($posts as $mk) {
													
													if(($mk->jam_mulai == $jam[$i]->mulai) && (strtolower($mk->ruang) == strtolower($dtruang->id)) && ($mk->tgl==$dt->tgl)) {
														$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;

														
														$colspan = $colspan;
														
														
														switch($mk->prodi_id){
															case 'SISKOM':
																$class = "label-warning";
															break;
															case 'SI':
																$class = "label-success";
															break;
															case 'ILKOM':
																$class = "label-info";
															break;
														}
													
														if($mk->is_project=='0'){
															echo '<td colspan="'.$colspan.'" class="'.$class.' popup-form" data-ruang="'.$dtruang->id.'|'.$dtruang->value.'" data-jammulai="'.$jam[$i]->mulai.'" data-jamselesai="'.$jam[$i]->selesai.'" data-tgl="'.$dt->tgl.'"><strong>'.$mk->namamk.'</strong><br>'.$mk->nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';
														}else{
															echo '<td colspan="'.$colspan.'" class="'.$class.' popup-form" data-ruang="'.$dtruang->id.'|'.$dtruang->value.'" data-jammulai="'.$jam[$i]->mulai.'" data-jamselesai="'.$jam[$i]->selesai.'" data-tgl="'.$dt->tgl.'"><strong>'.$mk->namamk.'</strong><br><span class="label label-inverse">'.$mk->nama.'</span><br>( Kelas - '.$mk->kelas_id.' )</td>';
														}
														
														
														$i+=$colspan-1;
														$skip = true;
														break;
													}
												}
												if(!$skip) echo "<td class='".$class." popup-form' data-ruang='".$dtruang->id."|".$dtruang->value."' data-jammulai='".$jam[$i]->mulai."' data-jamselesai='".$jam[$i]->selesai."' data-tgl='".$dt->tgl."'>&nbsp;</td>";
											}else{
												 echo "<td class='".$class." popup-form' data-ruang='".$dtruang->id."|".$dtruang->value."' data-jammulai='".$jam[$i]->mulai."' data-jamselesai='".$jam[$i]->selesai."' data-tgl='".$dt->tgl."'>&nbsp;</td>";
											}
										}

									echo "</tr>";
								endforeach;
							?>
						</tbody>
					</table>
					<div class="legenda">
						<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Informatika / Ilmu Komputer</small>
						<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small class="margin">Sistem Komputer</small>
						<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<small>Sistem Infomasi</small>
					</div>
					</div>
					<?php 
					
					} else {
							echo "<div class='well'><span class='text text-error'>Data tidak ditemukan</span></div>";
						}
					?>
				</div>
			<?php
			endforeach;
			?>
			<div class="tab-pane" id="newjadwal"><?php echo $this->view('jadwal/new_jadwal.php', $data); ?></div>
		</div>
		
	<?php
	}else{
		echo $this->view('jadwal/new_jadwal.php', $data);
		//echo "<div class='well'><span class='text text-error'>Data tidak ditemukan</span></div>";
		?>
		
		<?php
	}
	?>
	<script>
		$(document).ready(function() {   
			$('#jadwalform').hide();
		 });
		 
		 $('.popup-form').click(function(e){
			e.preventDefault();
			var ruang=$(this).data('ruang');
			var mulai=$(this).data('jammulai');
			var selesai=$(this).data('jamselesai');
			var tgl=$(this).data('tgl');
			
			$('#jadwalform').show('slow');
			$('#jadwalform #form-tgl').hide();
			$('#jadwalform #form-repeat').hide();
			
			var data = ruang.split('|');
			
			var idruang_ = data[0];
			var nruang_ = data[1];
			
			var semester 	= document.getElementById("cmbsemester");
			var semesterid	= $(semester).val();
					
			var kegiatan	= document.getElementById("cmbjenis");
			var kegiatanid	= $(kegiatan).val();
			
			var hari		= document.getElementById("cmbhari");
			var hariid		= $(hari).val();
		
			$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/penjadwalan/jadwal/ujian_by_form',
					data : $.param({
						cmbsemester : semesterid,
						cmbhari		: hariid,
						cmbjenis 	: kegiatanid,
						cmbruang 	: ruang,
						cmbmulai 	: mulai,	
						cmbselesai 	: selesai,
						cmbtgl		: tgl
						
					}),
					success : function(msg) {
						
						if (msg == '') {
							//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
							//$("#jadwalformdetail").html(msg);
						} else {
							$("#jadwalformdetail").html(msg);
							 //location.reload();
						}
					}
				});
			

			$('#jadwalform #input-ruang').val(idruang_);
			$('#jadwalform #input-ruang-nama').val(nruang_);
			$('#jadwalform #input-mulai').val(mulai);
			$('#jadwalform #input-selesai').val(selesai);
			$('#jadwalform #form-tgl').show();
			$('#jadwalform #input-tgl').val(tgl);				
			
		});
			
		$('#btn-modal-close').click(function(e){
			e.preventDefault();	
			$('#jadwalform').hide();
		});
		
		
		
	</script>
	