
<div class="form-group">		
	<a href="#" class="btn btn-default btn-cetak-panitia"  ><i class="fa fa-print"></i> Panitia</a>		
	<a href="#" class="btn btn-default btn-cetak-absen"  ><i class="fa fa-print"></i> Pengawas</a>		
	<a href="#" class="btn btn-default btn-cetak-rekap"><i class="fa fa-print"></i> Rekap Pengawas</a>	
</div>
<div class="clearfix">&nbsp;</div>
<div id="form-report-view" class="well">
	
	<div class="form-group frm-absen">
		<label>Prodi</label>
		
			<select name="cmbprodi" class="cmbmulti cmbreport form-control" id="cmbprodix">
					<option value="-">Semua Prodi</option>
					<?php
					foreach($prodi as $dt):
						echo "<option value='".$dt->id."' ";
						echo ">".$dt->value."</option>";
					endforeach;
				?>
				</select>
	</div>
	
	<div class="form-group frm-absen">
		<label>Lokasi</label>
		
			<select name="cmblokasi" class="cmbmulti cmbreport form-control" id="cmblokasi">
				<option value="-">Semua Lokasi</option>
				<?php
				foreach($lokasi as $dt):
					echo "<option value='".$dt->lokasi."' ";
					if($cmblokasi==$dt->lokasi){
						echo "selected";
					}
					echo ">".$dt->lokasi."&nbsp;</option>";
				endforeach;	
				?>
			</select>
	</div>
	
	<div class="form-group">
		<label>Tgl</label>
		
			<select name="cmbtgl" class="cmbmulti cmbreport form-control" id="cmbtgl">
				<option value="-">Semua Tgl</option>
				<?php
				foreach($tgl as $dt):
					echo "<option value='".$dt->tgl."'>".$dt->tgl."&nbsp;</option>";
				endforeach;	
				?>
			</select>
	</div>
	
	<div class="form-group frm-absen">
		<label>Jam Mulai</label>
		
			<select name="cmbjam" class="cmbmulti cmbreport form-control" id="cmbjam">
				<option value="-">Semua Jam</option>
				<?php
				foreach($jam as $dt):
					echo "<option value='".$dt->jam_mulai."'>".$dt->jam_mulai."&nbsp;</option>";
				endforeach;	
				?>
			</select>
		<input type="hidden" id="hidtmp">
	</div>	
	

	<div class="form-group frm-absen">
		<a href="#" class="btn btn-danger btn-cetak-absen-ok"><i class="fa fa-check"></i> Cetak Report</a>
	</div>
	
	<div class="form-group frm-panitia">
		<a href="#" class="btn btn-danger btn-cetak-panitia-ok"><i class="fa fa-check"></i> Cetak Report</a>
	</div>

</div>

		
<script>
	$(".cmbreport").change(function(e){
		e.preventDefault();
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		
		var lokasi		= document.getElementById("cmblokasi");
		var lokasiid	= $(lokasi).val();
		
		var prodi		= document.getElementById("cmbprodix");
		var prodiid		= $(prodi).val();
		
		var tgl			= document.getElementById("cmbtgl");
		var tglid		= $(tgl).val();
		
		var jam			= document.getElementById("cmbjam");
		var jamid		= $(jam).val();
		
		var id			= document.getElementById("tmpid");
		var hidid		= $(id).val();
		
		var fakultas	= document.getElementById("cmbfak");
		var fakultasid	= $(fakultas).val();
		
		if(hidid=='panitia'){
			$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/penjadwalan/jadwal/cetak_panitia',
					data : $.param({
						cmbsemester : semesterid,
						cmbhari		: hariid,
						cmbjenis 	: kegiatanid,
						cmbtgl		: tglid,
						cmbfak		: fakultasid						
					}),
					success : function(msg) {
						//alert(msg);
						if (msg == '') {
						} else {
							$("#content").html(msg);						
						}
					}
				});
		}else{		
			$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/penjadwalan/jadwal/cetak_hadir',
					data : $.param({
						cmbsemester : semesterid,
						cmbhari		: hariid,
						cmbjenis 	: kegiatanid,
						cmblokasi	: lokasiid,
						cmbprodi	: prodiid,
						cmbtgl		: tglid,
						cmbjam		: jamid,
						cmbfak		: fakultasid
					}),
					success : function(msg) {
						
						if (msg == '') {
						} else {
							$("#content").html(msg);						
						}
					}
				});
		}
	});
	
	$(".btn-cetak-absen").click(function(){
		  $("#form-report-view").show('slow');
		  $("#form-report-view .frm-absen").show();
		  $("#form-report-view .frm-panitia").hide();
		  $("#form-report-view #hidtmp").val("");
	});
	
	$(".btn-cetak-panitia").click(function(){
		 $("#form-report-view").show('slow');
		 $("#form-report-view .frm-absen").hide();
		 $("#form-report-view .frm-panitia").show();
		 $("#form-report-view #hidtmp").val("panitia");
		 
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		
		var fakultas	= document.getElementById("cmbfak");
			var fakultasid	= $(fakultas).val();
		
		var tglid = "-";
		
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/cetak_panitia',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid,
				cmbtgl		: tglid,
				cmbfak		: fakultasid		
			}),
			success : function(msg) {
				//alert(msg);
				if (msg == '') {
				} else {
					$("#content").html(msg);						
				}
			}
		});
		
	});
	
	$(".btn-cetak-absen-ok").click(function(){
		$("#form-report-view .frm-absen").show();
		$("#form-report-view .frm-panitia").hide();
		 $("#form-report-view #hidtmp").val("");
		 
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		
		var lokasi		= document.getElementById("cmblokasi");
		var lokasiid	= $(lokasi).val();
		
		var prodi		= document.getElementById("cmbprodix");
		var prodiid		= $(prodi).val();
		
		var tgl			= document.getElementById("cmbtgl");
		var tglid		= $(tgl).val();
		
		var jam			= document.getElementById("cmbjam");
		var jamid		= $(jam).val();
		
		var fakultas	= document.getElementById("cmbfak");
		var fakultasid	= $(fakultas).val();
		
			
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/cetak_hadir',
				data : $.param({
					cmbsemester : semesterid,
					cmbhari		: hariid,
					cmbjenis 	: kegiatanid,
					cmblokasi	: lokasiid,
					cmbprodi	: prodiid,
					cmbtgl		: tglid,
					cmbjam		: jamid,
					cmbfak		: fakultasid						
				}),
				success : function(msg) {
				
					if (msg == '') {
					} else {
						$("#content").html(msg);
						window.print();						
					}
				}
			});
			
	});
	
	$(".btn-cetak-panitia-ok").click(function(){
		
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var tgl			= document.getElementById("cmbtgl");
		var tglid		= $(tgl).val();
		var fakultas	= document.getElementById("cmbfak");
		var fakultasid	= $(fakultas).val();
					
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/cetak_panitia',
				data : $.param({
					cmbsemester : semesterid,
					cmbhari		: hariid,
					cmbjenis 	: kegiatanid,
					cmbtgl		: tglid,
					cmbfak		: fakultasid	
				}),
				success : function(msg) {
				
					if (msg == '') {
					} else {
						$("#content").html(msg);
						window.print();
						
					}
				}
			});
			
	});
		
	$(".btn-cetak-rekap").click(function(){
		 $("#form-report-view").hide();
		 $("#form-report-view #hidtmp").val("");
		  
		 var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		var fakultas	= document.getElementById("cmbfak");
		var fakultasid	= $(fakultas).val();
		
			
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/cetak_rekap',
				data : $.param({
					cmbsemester : semesterid,
					cmbhari		: hariid,
					cmbjenis 	: kegiatanid,
					cmbfak		: fakultasid	
					
				}),
				success : function(msg) {
					
					if (msg == '') {
						//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					} else {
						$("#content").html(msg);
						window.print();
						//window.open(base_url + 'module/penjadwalan/jadwal/cetak_rekap/'+kegiatanid+'/'+semesterid+'/'+hariid,"_blank");
					}
				}
			});
	});
		
</script>


