
<?php 
$mjadwal = new model_jadwal();

$semester	= $mjadwal->get_tahun_akademik_name($semesterid);

$koordinator	= $mjadwal->get_koordinator_ujian($semesterid, $jeniskegiatan,$type);
		
if(($koordinator) && ($koordinator->gelar_awal)&&($koordinator->gelar_awal!="-")){
	$gkawal= ", ".$koordinator->gelar_awal;
}else{
	$gkgawal="";
}

if(($koordinator) &&($koordinator->gelar_akhir)&&($koordinator->gelar_akhir!="-")){
	$gkakhir= ", ".$koordinator->gelar_akhir;
}else{
	$gkakhir="";
}
		

if($jeniskegiatan=='uts'){
	$str 		= "UJIAN TENGAH SEMESTER (UTS)";
	$singkat 	= "UTS";
}else{
	$str 		= "UJIAN AKHIR SEMESTER (UAS)";
	$singkat	= "UAS";
}

if($type=='rekap'){
	$judul	= "REKAP HADIR  PENGAWAS";

?>
	<div id="header-report">
		<p>KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN							
			UNIVERSITAS BRAWIJAYA	<br>					
			PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER						
		</p>						
		<p>&nbsp;</p>		
		<div class="row">		
			<h3 align="center">REKAP HADIR PENGAWAS</h3>	
			<h4 align="center">					
			<?php 
			echo $str."&nbsp;".strToupper($semester->is_ganjil); 
			if($semester->is_pendek){ echo  " ".strToupper($semester->is_pendek);}

			echo " ". $semester->tahun."/".($semester->tahun + 1);
			?> 				<br>	
									
			PROGRAM TEKNOLOGI INFORMASI & ILMU KOMPUTER 	<br>						
			UNIVERSITAS BRAWIJAYA							
			</h4>
		</div>
	</div>
	<div class="content-report col-md-12">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Total Jadwal</th>
				<th>Total Hadir</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		if($posts){
		foreach($posts as $dt):
			$i++;		
						
			if(($dt->gelar_awal)&&($dt->gelar_awal!="-")){
				$gawal= ", ".$dt->gelar_awal;
			}else{
				$gawal="";
			}
			
			if(($dt->gelar_akhir)&&($dt->gelar_akhir!="-")){
				$gakhir= ", ".$dt->gelar_akhir;
			}else{
				$gakhir="";
			}
			?>
			<tr>
				<td><?php echo $i;?></td>
				<td><?php echo $dt->nama.$gawal.$gakhir;?></td>
				<td><?php 
				
				$jadwal = $mjadwal->get_total_pengawas($dt->jenis_ujian, $dt->tahun_akademik, "", $dt->karyawan_id, '0');
				
				echo $jadwal->total;
				
				?></td>
				
				<td><?php 
				
				$rekap = $mjadwal->get_rekap_pengawas($dt->jenis_ujian, $dt->tahun_akademik, "", $dt->karyawan_id);
				
				if($rekap){
					echo $rekap->total;
				}else{
					echo "0";
				}
				
				?></td>
			</tr>
			<?php
		endforeach;
	}else{
		echo "<tr><td colspan=4>Sorry, no content to show.</td></tr>";
	}
	?>
	</tbody>
	</table>
	<div class="pull-right">
			Malang, <?php echo date("d"). " ". $mjadwal->get_nama_bulan(date("m")) ." ". date("Y"); ?><br>
			Koordinator Pelaksana <?php
			echo $singkat."&nbsp;".ucwords($semester->is_ganjil); 
			if($semester->is_pendek){ echo  " ".ucwords($semester->is_pendek);}

			echo " ". $semester->tahun."/".($semester->tahun + 1);
			?><p>&nbsp;</p><p>&nbsp;</p>
			<?php if($koordinator) echo $koordinator->nama.$gkgawal.$gkakhir; ?>
		</div>
	</div>
	<div class="hide-from-print" style="border-top:solid 1px #ddd;padding:5px;"></div>
	<div class="page-break" id="footer"></div>
<?php 
}else { 
	if($type=="panitia"){
		if($tgl){
			foreach($tgl as $dttgl):
				?>
				<div class="header-report">
						<p>KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN							
							UNIVERSITAS BRAWIJAYA	<br>					
							PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER	
						</p>						
						<p>&nbsp;</p>	
						<div class="row">		
							<h3 align="center">DAFTAR HADIR PANITIA</h3>	
							<h4 align="center">					
							<?php 
							echo $str."&nbsp;".strToupper($semester->is_ganjil); 
							if($semester->is_pendek){ echo  " ".strToupper($semester->is_pendek);}

							echo " ". $semester->tahun."/".($semester->tahun + 1);
							?> 				<br>	
							PROGRAM TEKNOLOGI INFORMASI & ILMU KOMPUTER 	<br>						
							UNIVERSITAS BRAWIJAYA							
							</h4>
						</div>
						<p>&nbsp;</p>	
					</div>
					<div class="content-report col-md-12">
						<div class="row">
						<p>&nbsp;</p>	
							Hari/Tanggal : <b><?php echo ucWords($dttgl->hari).", ".date("d", strtotime($dttgl->tgl)). " ". $mjadwal->get_nama_bulan(date("m", strtotime($dttgl->tgl))) ." ". date("Y", strtotime($dttgl->tgl)); ?></b>
						</div>
						
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama </th>
										<th>NIP/NIK</th>
										<th>Tanda Tangan</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if($posts){
										$i=0;
										foreach($posts as $dt):
											$i++;
											
											if($i % 2){
												$style= "";
											}else{
												$style= "style='text-align:center'";
											}
											
												if(($dt->gelar_awal)&&($dt->gelar_awal!="-")){
													$gawal= ", ".$dt->gelar_awal;
												}else{
													$gawal="";
												}
												
												if(($dt->gelar_akhir)&&($dt->gelar_akhir!="-")){
													$gakhir= ", ".$dt->gelar_akhir;
												}else{
													$gakhir="";
												}

											?>
											<tr>
												<td><?php echo $i;?></td>
												<td><?php echo $dt->nama.$gawal.$gakhir;?></td>
												<td align="center"><?php echo $dt->nik;?></td>												
												<td <?php echo $style; ?>><?php echo $i;?></td>
											</tr>
											<?php
										endforeach;//end loop post
									}
									?>
								</tbody>
							</table>
						<p>&nbsp;</p>
						<div class="pull-right">
							
							Malang, <?php echo date("d", strtotime($dttgl->tgl)). " ". $mjadwal->get_nama_bulan(date("m", strtotime($dttgl->tgl))) ." ". date("Y", strtotime($dttgl->tgl)); ?><br>
							Koordinator Pelaksana <?php
							echo $singkat."&nbsp;".ucwords($semester->is_ganjil); 
							if($semester->is_pendek){ echo  " ".ucwords($semester->is_pendek);}

							echo " ". $semester->tahun."/".($semester->tahun + 1);
							?><p>&nbsp;</p><p>&nbsp;</p>
							<?php if($koordinator) echo $koordinator->nama.$gkgawal.$gkakhir; ?>
						</div>
					</div>
					<div class="page-break" id="footer"></div>
					<div class="row hide-from-print" style="border-bottom:solid 1px #ddd; padding:10px;"></div>
				<?php
			endforeach;
		}
	}else{
		if(isset($hari) && ($hari)){
			
			foreach ($hari as $dthari):
				
				$tgl	= $mjadwal->get_tgl_ujian($jeniskegiatan, $semesterid, $dthari->hari,$cmbtgl);
				
				if($tgl){
					foreach($tgl as $dttgl):
						$jam	= $mjadwal->get_jam_ujian($jeniskegiatan, $semesterid, $dthari->hari,$cmbtgl, $cmbjam);
						
						if($jam){
							
							foreach($prodi as $dtprodi):
								$lokasi	= $mjadwal->get_lokasi_ruang($jeniskegiatan, $semesterid, $dthari->hari,$dtprodi->prodi_id,$cmblokasi);
								
								if($lokasi){
									foreach($lokasi as $dtlokasi):
										foreach ($jam as $dtjam): 
											$posts = $mjadwal->get_absen_pengawas($jeniskegiatan, $semesterid, $dtlokasi->lokasi, $dthari->hari,$dttgl->tgl, $dtjam->jam_mulai,$dtprodi->prodi_id);
											if($posts){
											?>
													<div class="header-report">
														<p>KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN							
															UNIVERSITAS BRAWIJAYA	<br>					
															PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER	<br>
															PRODI <?php echo strToUpper($dtprodi->keterangan); ?>	
														</p>						
														<p>&nbsp;</p>	
														<div class="row">		
															<h3 align="center">DAFTAR HADIR PENGAWAS</h3>	
															<h4 align="center">					
															<?php 
															echo $str."&nbsp;".strToupper($semester->is_ganjil); 
															if($semester->is_pendek){ echo  " ".strToupper($semester->is_pendek);}

															echo " ". $semester->tahun."/".($semester->tahun + 1);
															?> 				<br>	
															PRODI <?php echo strToUpper($dtprodi->keterangan); ?>	<br>						
															PROGRAM TEKNOLOGI INFORMASI & ILMU KOMPUTER 	<br>						
															UNIVERSITAS BRAWIJAYA							
															</h4>
														</div>
														<p>&nbsp;</p>	
													</div>
													<div class="content-report col-md-12">
														<div class="row">
														<p>&nbsp;</p>	
															Hari/Tanggal : <b><?php echo ucWords($dthari->hari).", ".date("d", strtotime($dttgl->tgl)). " ". $mjadwal->get_nama_bulan(date("m", strtotime($dttgl->tgl))) ." ". date("Y", strtotime($dttgl->tgl)); ?></b>
														</div>
														<div class="row">
															<b><?php echo ucWords($dtlokasi->lokasi); ?></b>
														</div>
														
															<table class="table table-bordered" width="100%">
																<thead>
																	<tr>
																		<th width="4%">No</th>
																		<th>Matakuliah </th>
																		<th width="5%">Kelas</th>
																		<th width="5%">Jam</th>
																		<th width="5%">Ruang</th>
																		<th>Pengawas</th>
																		<th width="20%">Tanda Tangan</th>
																	</tr>
																</thead>
																<tbody>
																	<?php
																
																		$i=0;
																		foreach($posts as $dt):
																			$i++;
																			
																			if($i % 2){
																				$style= "";
																			}else{
																				$style= "style='text-align:center'";
																			}
																			
																				if(($dt->gelar_awal)&&($dt->gelar_awal!="-")){
																					$gawal= ", ".$dt->gelar_awal;
																				}else{
																					$gawal="";
																				}
																				
																				if(($dt->gelar_akhir)&&($dt->gelar_akhir!="-")){
																					$gakhir= ", ".$dt->gelar_akhir;
																				}else{
																					$gakhir="";
																				}
				
																			?>
																			<tr>
																				<td><?php echo $i;?></td>
																				<td><?php echo $dt->namamk;?></td>
																				<td align="center"><?php echo $dt->kelas_id;?></td>
																				<td align="center"><?php echo date("H:i",strtotime($dt->jam_mulai));?></td>
																				<td align="center"><?php echo $dt->ruang;?></td>
																				<td><?php echo $dt->nama.$gawal.$gakhir;?></td>
																				<td <?php echo $style; ?>><?php echo $i;?></td>
																			</tr>
																			<?php
																		endforeach;//end loop post
																
																	?>
																</tbody>
															</table>
														<p>&nbsp;</p>
														<div class="pull-right">
															
															Malang, <?php echo date("d", strtotime($dttgl->tgl)). " ". $mjadwal->get_nama_bulan(date("m", strtotime($dttgl->tgl))) ." ". date("Y", strtotime($dttgl->tgl)); ?><br>
															Koordinator Pelaksana <?php
															echo $singkat."&nbsp;".ucwords($semester->is_ganjil); 
															if($semester->is_pendek){ echo  " ".ucwords($semester->is_pendek);}

															echo " ". $semester->tahun."/".($semester->tahun + 1);
															?><p>&nbsp;</p><p>&nbsp;</p>
															<?php if ($koordinator) echo $koordinator->nama.$gkgawal.$gkakhir; ?>
														</div>
													</div>
													<div class="page-break" id="footer"></div>
													<div class="row hide-from-print" style="border-bottom:solid 1px #ddd; padding:10px;"></div>
										<?php
											} //end posts
											
										endforeach; //end loop jam
										
									endforeach; // end loop lokasi
								} //end lokasi
							endforeach;	//end loop prodi
						} //end jam
					endforeach; //end loop tgl
				} //end tgl			
			endforeach;
		}
	} //end if panitia
} 
?>


