	
<?php
$this->head();
$header			= "Write New Penjadwalan";	

?>
<div class="container-fluid">  
	<legend>
		<a href="<?php echo $this->location('module/penjadwalan/jadwal'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> Penjadwalan List</a> 
		<?php if($posts){	?>
		<a href="<?php echo $this->location('module/penjadwalan/jadwal/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Penjadwalan</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
	<div class="row-fluid">
		<div class="span4">
			<form name="frmDosen" id="form-add-ujian" method="post" role="form" action="<?php echo $this->location('module/penjadwalan/jadwal/write')?>" >	
				<div class="well" id="mainform">
						<div class='form-group'>
							<label>Jenis Kegiatan</label>
						
								<select id="cmbjenis" name="cmbjenis"  class="cmbjadwal cmbmulti">
									<option value="-">Jenis Kegiatan</option>
									<?php
										foreach($kegiatan as $dt):
											echo "<option value='".$dt->jenis_kegiatan_id."' ";
											if($jeniskegiatan==$dt->jenis_kegiatan_id){
												echo "selected ";
											}
											echo  ">".$dt->keterangan."</option>";
										endforeach;
									?>
								</select>
							
						</div>

						<div class='form-group'>
							<label>Semester</label>
						
								<select id="cmbsemester" name="cmbsemester"  class="cmbjadwal cmbmulti">
									<option value="-">Semester</option>
									<?php
									foreach($semester as $dt):
										echo "<option value='".$dt->tahun_akademik."' ";
										if($semesterid==$dt->tahun_akademik){
											echo "selected";
										}
										echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
									endforeach;
									?>
								</select>
							
						</div>
						<?php if($jeniskegiatan=='uts' || $jeniskegiatan=='uas') {  echo $jeniskegiatan; ?>
						<div class='form-group'>
							<label>Hari</label>
						
								<select id="cmbhari" name="cmbhari" class="cmbjadwal cmbmulti">
									<option value="-">Please Select</option>
									<?php
									foreach($hari as $dt):
										echo "<option value='".$dt->value."' ";
										if($cmbhari==$dt->value){
											echo "selected";
										}
										echo ">".ucwords($dt->value)."</option>";
									endforeach;
									?>
								</select>
							
						</div>
						<?php } ?>
				</div>
					
			<div id="jadwalform">
				<div class="form-group">
					<label>Ruang</label>					
					<input type="text" name="nama_ruang" class="ruangform form-control" id="input-ruang-nama" value="<?php if(isset($ruangid)){ echo $ruangid;} ?>" readonly>
					<input type="hidden" name="ruangid" class="ruangform form-control" id="input-ruang" value="<?php if(isset($ruangid)){ echo $ruangid;} ?>">					
				</div>

				<div class="form-group">
					<label>Waktu</label>
					<div class="row"><input type="text" name="jammulai" class="span4" id="input-mulai" value="<?php if (isset($mulai)){ echo $mulai;}  ?>" readonly> s/d <input type="text" class="span4" name="jamselesai" id="input-selesai" value="<?php if (isset($selesai)){ echo $selesai;}  ?>" readonly></div>
				</div>

				<div class="form-group">
					<label>Tgl</label>
				
						<input type="text" name="tgl" class="date" id="input-tgl" value="<?php if (isset($tgltmp)){ echo $tgltmp;}  ?>">
					
				</div>			
				
				<div id="jadwalformdetail"></div>
				
			</div>			
			</form>			
		</div>
		
		<div class="span8" id="content"></div>
		
	</div>
</div>

	<?php
$this->foot();
?>
