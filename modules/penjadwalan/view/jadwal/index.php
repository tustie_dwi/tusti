	
<?php
$this->head();

?>

<h2 class="title-page">Penjadwalan</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/penjadwalan/jadwal'); ?>">Penjadwalan</a></li>
	</ol>
	
		
	<div class="row">
		<div class="col-md-4 hide-from-print">
			<form name="frmDosen" id="form-add-ujian" method="post" role="form" action="<?php echo $this->location('module/penjadwalan/jadwal/write')?>" >	
				<div class="block-box" id="mainform">
					
					<div class="form-group">
						<label>Jenis Kegiatan</label>
						
							<select id="cmbjenis" name="cmbjenis"  class="cmbjadwal cmbmulti">
								<option value="-">Jenis Kegiatan</option>
								<?php
									foreach($kegiatan as $dt):
										echo "<option value='".$dt->jenis_kegiatan_id."' ";
										if($jeniskegiatan==$dt->jenis_kegiatan_id){
											echo "selected ";
										}
										echo  ">".$dt->keterangan."</option>";
									endforeach;
								?>
							</select>							
						</div>
						
						<div class="form-group">
						<label>Fakultas</label>
						
							<select id="cmbfak" name="cmbfak"  class="cmbjadwal cmbmulti">
								<option value="-">Fakultas</option>
								<?php
									foreach($fakultas as $dt):
										echo "<option value='".$dt->fakultas_id."' ";
										if($fakid==$dt->fakultas_id){
											echo "selected ";
										}
										echo  ">".$dt->keterangan."</option>";
									endforeach;
								?>
							</select>							
						</div>

						<div class="form-group">
							<label>Semester</label>
							
								<select id="cmbsemester" name="cmbsemester"  class="cmbjadwal cmbmulti">
									<option value="-">Semester</option>
									<?php
									foreach($semester as $dt):
										echo "<option value='".$dt->tahun_akademik."' ";
										if($semesterid==$dt->tahun_akademik){
											echo "selected";
										}
										echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
									endforeach;
									?>
								</select>
							
						</div>
						
						<div class="form-group" id="form-hari">
							<label>Hari</label>
							
								<select id="cmbhari" name="cmbhari" class="cmbjadwal cmbmulti">
									<option value="-">Please Select</option>
									<?php
									foreach($hari as $dt):
										echo "<option value='".$dt->value."' ";
										if($cmbhari==$dt->value){
											echo "selected";
										}
										echo ">".ucwords($dt->value)."</option>";
									endforeach;
									?>
								</select>
							
						</div>
						<div id="form-kegiatan"></div>
						
						<div id="form-report-koordinator" class="form-group">
							<div class="form-group span12">
								<label>Koordinator Pelaksana</label>
								<input type="text" name="koordinatoor" class="ndosen form-control" id="ndosen">
								<input type="hidden" name="koordinatorid" id="ndosenid">
							</div>
							<div class="form-group span12">
								<div class="control-label">Panitia</div>
								<?php
								$namapanitia="";

								if(isset($panitia)){
									if(count($panitia)!=0){
										$i=0;
										
										foreach($panitia as $dt):
											$i++;		
											
											$namapanitia = $namapanitia . $dt->nama .",";							
										endforeach;
									}
								}
								?>
								<input type="hidden" value="<?php echo $namapanitia; ?>" class="tmpstaffujian" id="panitiaid" />	
								<input type='text' name='tpanitia' placeholder='Nama Panitia..' class="tagStaffUjian form-control" id="panitia" />
							</div>
						</div>
					
				</div>
			
			<div id="form-report"></div>
					
			<div id="jadwalform" class="block-box">
				<div class="form-group">
					<label>Ruang</label>					
					<input type="text" name="nama_ruang" class="ruangform form-control" id="input-ruang-nama" value="<?php if(isset($ruangid)){ echo $ruangid;} ?>" readonly>
					<input type="hidden" name="ruangid" class="ruangform form-control" id="input-ruang" value="<?php if(isset($ruangid)){ echo $ruangid;} ?>">					
				</div>

				<div class="form-group">
					<label>Waktu</label>
					<div class="row">
						<div class="col-md-5">
						<input type="text" name="jammulai" class="jammulai form-control" id="input-mulai" value="<?php if (isset($mulai)){ echo $mulai;}  ?>"> 
						</div>
						<div class="col-md-2" align="center">&nbsp;s/d&nbsp;</div>
						<div class="col-md-5">
						<input type="text" class="jamselesai form-control" name="jamselesai" id="input-selesai" value="<?php if (isset($selesai)){ echo $selesai;}  ?>">
						</div>
					</div>
				</div>
				
				
					<div class="form-group" id="form-tgl">
						<label>Tgl</label>						
						<input type="text" name="tgl" class="date form-control" id="input-tgl" value="<?php if (isset($tgltmp)){ echo $tgltmp;}  ?>">						
					</div>
				

				
					<div class="form-group" id="form-repeat">
						<label>Repeat On</label>
						<div class="row">
							<div class="col-md-3">
								<select name='cmbrepeat' class='span12 form-control' id="cmbrepeat">
								<?php
								$repeaton = array('daily', 'monthly');
								
								for($i=0;$i<count($repeaton);$i++){
									echo "<option value='".$repeaton[$i]."'  ";
									if($repeatin==$repeaton[$i]){
										echo " selected";
									}
									echo ">".Ucfirst($repeaton[$i])."</option>";
								}							
								?>
								</select>								
							</div>
							
							<div class="col-md-9">
								<div class="col-md-6">
									<input type="text" name="tmulai" class="date form-control" id="input-tgl-mulai" value="<?php if (isset($tmulai)){ echo $tmulai;}  ?>">
								</div>
								<div class="col-md-6">
									<input type="text" name="tselesai" class="date form-control" id="input-tgl-selesai" value="<?php if (isset($tselesai)){ echo $tselesai;}  ?>">
								</div>
							</div>
						</div>
					</div>
									
				
				<div id="jadwalformdetail"></div>
				
			</div>			
			</form>			
		</div>		
		<div class="col-md-8 content-print block-box" id="content"></div>
		
	</div>


	<?php
$this->foot();
?>
