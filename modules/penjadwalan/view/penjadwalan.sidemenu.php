<li class="dropdown-submenu">
  <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-tasks"></i> Penjadwalan
  </a>
	<ul class="dropdown-menu">
		<li><a href="<?php echo $this->location($module_url.'jadwal');?>"><i class="icon-tasks"></i> Setting Penjadwalan</a></li>
		<li><a href="<?php echo $this->location($module_url.'ruang/terpakai');?>"><i class="icon-tasks"></i> Pemakaian Ruangan</a></li>
		<li><a href="<?php echo $this->location($module_url.'ruang/tersedia');?>"><i class="icon-tasks"></i> Ketersediaan Ruangan</a></li>
	</ul>
</li>