<?php 

$total = '14'; 
	if($jadwal){ ?>
		<h3>Periode <?php echo $periode; ?></h3>
		<table class="table table-hover">			
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Hadir</th>
					<th>% Hadir</th>
				</tr>
			</thead>
			<tbody>					
			<?php
				$mabsen = new model_absen();
									
				$i=0;
				$totalhadir =0;
				foreach($jadwal as $dt):
					$i++;
					
					$jml = $mabsen->get_hadir_dosen($periode, $dt->mkditawarkan_id, $dt->prodi_id, $dt->kelas_id, $dt->karyawan_id);
						
					$totalhadir = $totalhadir + $jml;
					?>
					<tr>
						<td>
						<input type="hidden" name="chkdosen[]" value="<?php echo $dt->karyawan_id; ?>">
						
						<?php  echo $dt->kode_mk. " - ". $dt->keterangan."&nbsp;<span class='label label-danger'>".$dt->kelas_id."</span><br><span class='badge'>".$dt->prodi_id;?></span></td>
						
						<td><?php echo $jml; ?></td>
						
						<td><?php $persentase = ($jml/$total) * 100; echo number_format($persentase,2);?> % <?php if ($persentase < 80){  echo "<span class='label label-danger'>*</span>";} ?></td>
					</tr>
					<?php
				endforeach;
				?>
			</tbody>
			
		</table>
		
		<?php }else{ echo "<div class=well><small>No content to show</small></div>"; }?>