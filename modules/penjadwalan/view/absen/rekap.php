<?php $this->head(); ?>

	<legend>
	<a href="<?php echo $this->location('module/penjadwalan/absen'); ?>" class="btn btn-default pull-right"><i class="fa fa-edit"></i> Write Kehadiran</a> 
	Rekapitulasi Kehadiran
    </legend>    
		<form method="post" action="<?php echo $this->location('module/penjadwalan/absen/rekap'); ?>" id="form-save-absen"  >
		 <div class="row">  
		 <span class="status-save" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>     
			 <div class="span6">
				<div class="control-group">
					<div class="control-label">Jenis Rekap</div>
					<div class="controls">
						<select name="cmbjenis" id="cmbjenis" class='cmbmulti' onChange="get_jenis_rekap();">
							<option value="-">Please Select..</option>
							<option value="dosen">Rekapitulasi Dosen</option>
							<option value="mhs">Rekapitulasi Mhs</option>
						</select>
					</div>
				</div>
				<div id="form-rekap-absen"></div>
			</div>
			<div class="span6" id="content-rekap"></div>
		</div>	
	</form>

<?php $this->foot(); ?>