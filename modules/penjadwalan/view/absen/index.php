<?php $this->head(); ?>
<div class="row-fluid">
	<legend><a href="<?php echo $this->location('module/penjadwalan/jadwal/write'); ?>" class="btn btn-default pull-right">
    <i class="fa fa-edit"></i> New Penjadwalan</a> Penjadwalan List
	</legend>
	<div id="demo">
	<?php 
	if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	if( isset($posts) ) :	
	 
	$str="<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>Penjadwalan</th>
						<th>Operation</th>
					</tr>
				</thead>
				<tbody>";		
				
			foreach ($posts as $dt): 
							
				$str.="	<tr id='post-".$dt->jadwal_id."' data-id='".$dt->jadwal_id."' valign=top valign=top>
							<td> ";	
				if(strToLower($dt->jenis_kegiatan_id)=='praktikum'){
					$str.= " Asisten ";
				}else{
					$str.= $dt->nama ." ";
				}
				
				$str.= "<span class='badge badge-info'>".$dt->jeniskegiatan."</span> ";
				
				$str.= "<code>".ucwords($dt->tahun." ". $dt->is_ganjil ." ". $dt->is_pendek)."</code>";
				$str.= "</td><td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href=".$this->location('module/penjadwalan/jadwal/edit/'.$dt->id)."><i class='icon-pencil'></i> Edit</a>	
									</li>														
								  </ul>
								</li>
							</ul>
						</td></tr>";
			endforeach; 
			
		$str.= "</tbody></table>";
		
		echo $str;
		
		echo "</div>";
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</div>
<?php 
$this->foot(); 


?>