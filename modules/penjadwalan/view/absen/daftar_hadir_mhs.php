<?php 


if($mhs){ ?>

	<table class="table table-hover">
		
		<thead>
			<tr>
				<th><!--<input type="checkbox" class="checkall">--></th>
				<th>Mahasiswa</th>
				<th>Hadir?</th>
			</tr>
		</thead>
		<tbody>					
		<?php
			$i=0;
			$mabsen = new model_absen();
			
			foreach($mhs as $dt):
				$i++;
				$row = $mabsen->get_is_hadir($dt->mahasiswa_id, $absenid);
				
				if($row){
					$ishadir = $row->is_hadir;
				}else{
					$ishadir = 'hadir';
				}
				?>
				<tr>
					<td><input type="hidden" name="chkmhs[]" value="<?php echo $dt->mahasiswa_id; ?>"><?php  echo $i;?></td>
					<td><?php echo "<b>".$dt->nim."</b> - ".$dt->nama; ?></td>
					<td>
					<select name="cmbhadir[]">
					<option value="hadir" <?php if($ishadir=='hadir'){ echo "selected"; } ?> >Hadir</option>
					<option value="sakit" <?php if($ishadir=='sakit'){ echo "selected"; } ?>>Sakit</option>
					<option value="ijin" <?php if($ishadir=='ijin'){ echo "selected"; } ?>>Ijin</option>
					<option value="alpha" <?php if($ishadir=='alpha'){ echo "selected"; } ?>>Alpha</option></select>
					</td>
				</tr>
				<?php
			endforeach;
			?>
		</tbody>
		
	</table>
	
<?php }else{ echo "<div class=well><small>No content to show</small></div>"; }?>