
<div class="control-group">		
	<a href="#" class="btn btn-default btn-cetak-panitia"  ><i class="fa fa-print"></i> Panitia</a>		
	<a href="#" class="btn btn-default btn-cetak-absen"  ><i class="fa fa-print"></i> Pengawas</a>		
	<a href="#" class="btn btn-default btn-cetak-rekap"><i class="fa fa-print"></i> Rekap Pengawas</a>	
</div>
<div class="clearfix">&nbsp;</div>
<div id="form-report-view" class="well">
	
	<div class="control-group frm-absen">
		<label class="control-label">Prodi</label>
		<div  class="controls">
			<select name="cmbprodi" class="cmbmulti cmbreport" id="cmbprodix">
					<option value="-">Semua Prodi</option>
					<?php
					foreach($prodi as $dt):
						echo "<option value='".$dt->id."' ";
						echo ">".$dt->value."</option>";
					endforeach;
				?>
				</select>
		</div>
	</div>
	
	<div class="control-group frm-absen">
		<label class="control-label">Lokasi</label>
		<div  class="controls">
			<select name="cmblokasi" class="cmbmulti cmbreport" id="cmblokasi">
				<option value="-">Semua Lokasi</option>
				<?php
				foreach($lokasi as $dt):
					echo "<option value='".$dt->lokasi."' ";
					if($cmblokasi==$dt->lokasi){
						echo "selected";
					}
					echo ">".$dt->lokasi."&nbsp;</option>";
				endforeach;	
				?>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label">Tgl</label>
		<div  class="controls">
			<select name="cmbtgl" class="cmbmulti cmbreport" id="cmbtgl">
				<option value="-">Semua Tgl</option>
				<?php
				foreach($tgl as $dt):
					echo "<option value='".$dt->tgl."'>".$dt->tgl."&nbsp;</option>";
				endforeach;	
				?>
			</select>
		</div>
	</div>
	
	<div class="control-group frm-absen">
		<label class="control-label">Jam Mulai</label>
		<div  class="controls">
			<select name="cmbjam" class="cmbmulti cmbreport" id="cmbjam">
				<option value="-">Semua Jam</option>
				<?php
				foreach($jam as $dt):
					echo "<option value='".$dt->jam_mulai."'>".$dt->jam_mulai."&nbsp;</option>";
				endforeach;	
				?>
			</select>
		</div>
		<input type="hidden" id="hidtmp">
	</div>	
	

	<div class="control-group frm-absen">
		<a href="#" class="btn btn-danger btn-cetak-absen-ok"><i class="fa fa-check"></i> Cetak Report</a>
	</div>
	
	<div class="control-group frm-panitia">
		<a href="#" class="btn btn-danger btn-cetak-panitia-ok"><i class="fa fa-check"></i> Cetak Report</a>
	</div>

</div>

		
<script>
	$(".cmbreport").change(function(e){
		e.preventDefault();
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		
		var lokasi		= document.getElementById("cmblokasi");
		var lokasiid	= $(lokasi).val();
		
		var prodi		= document.getElementById("cmbprodix");
		var prodiid		= $(prodi).val();
		
		var tgl			= document.getElementById("cmbtgl");
		var tglid		= $(tgl).val();
		
		var jam			= document.getElementById("cmbjam");
		var jamid		= $(jam).val();
		
		var id			= document.getElementById("tmpid");
		var hidid		= $(id).val();
		
		if(hidid=='panitia'){
			$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/penjadwalan/jadwal/cetak_panitia',
					data : $.param({
						cmbsemester : semesterid,
						cmbhari		: hariid,
						cmbjenis 	: kegiatanid,
						cmbtgl		: tglid				
					}),
					success : function(msg) {
						//alert(msg);
						if (msg == '') {
						} else {
							$("#content").html(msg);						
						}
					}
				});
		}else{		
			$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/penjadwalan/jadwal/cetak_hadir',
					data : $.param({
						cmbsemester : semesterid,
						cmbhari		: hariid,
						cmbjenis 	: kegiatanid,
						cmblokasi	: lokasiid,
						cmbprodi	: prodiid,
						cmbtgl		: tglid,
						cmbjam		: jamid					
					}),
					success : function(msg) {
						//alert(msg);
						if (msg == '') {
						} else {
							$("#content").html(msg);						
						}
					}
				});
		}
	});
	
	$(".btn-cetak-absen").click(function(){
		  $("#form-report-view").show('slow');
		  $("#form-report-view .frm-absen").show();
		  $("#form-report-view .frm-panitia").hide();
		  $("#form-report-view #hidtmp").val("");
	});
	
	$(".btn-cetak-panitia").click(function(){
		 $("#form-report-view").show('slow');
		 $("#form-report-view .frm-absen").hide();
		 $("#form-report-view .frm-panitia").show();
		 $("#form-report-view #hidtmp").val("panitia");
		 
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		
		var tglid = "-";
		
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/cetak_panitia',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid,
				cmbtgl		: tglid				
			}),
			success : function(msg) {
				//alert(msg);
				if (msg == '') {
				} else {
					$("#content").html(msg);						
				}
			}
		});
		
	});
	
	$(".btn-cetak-absen-ok").click(function(){
		$("#form-report-view .frm-absen").show();
		$("#form-report-view .frm-panitia").hide();
		 $("#form-report-view #hidtmp").val("");
		 
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		
		var lokasi		= document.getElementById("cmblokasi");
		var lokasiid	= $(lokasi).val();
		
		var prodi		= document.getElementById("cmbprodix");
		var prodiid		= $(prodi).val();
		
		var tgl			= document.getElementById("cmbtgl");
		var tglid		= $(tgl).val();
		
		var jam			= document.getElementById("cmbjam");
		var jamid		= $(jam).val();
		
			
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/cetak_hadir',
				data : $.param({
					cmbsemester : semesterid,
					cmbhari		: hariid,
					cmbjenis 	: kegiatanid,
					cmblokasi	: lokasiid,
					cmbprodi	: prodiid,
					cmbtgl		: tglid,
					cmbjam		: jamid					
				}),
				success : function(msg) {
				
					if (msg == '') {
					} else {
						$("#content").html(msg);
						window.print();
						
					}
				}
			});
			
	});
	
	$(".btn-cetak-panitia-ok").click(function(){
		
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var tgl			= document.getElementById("cmbtgl");
		var tglid		= $(tgl).val();
					
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/cetak_panitia',
				data : $.param({
					cmbsemester : semesterid,
					cmbhari		: hariid,
					cmbjenis 	: kegiatanid,
					cmbtgl		: tglid				
				}),
				success : function(msg) {
				
					if (msg == '') {
					} else {
						$("#content").html(msg);
						window.print();
						
					}
				}
			});
			
	});
		
	$(".btn-cetak-rekap").click(function(){
		 $("#form-report-view").hide();
		 $("#form-report-view #hidtmp").val("");
		  
		 var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var kegiatan	= document.getElementById("cmbjenis");
		var kegiatanid	= $(kegiatan).val();
		
		var hari		= document.getElementById("cmbhari");
		var hariid		= $(hari).val();
		
			
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/cetak_rekap',
				data : $.param({
					cmbsemester : semesterid,
					cmbhari		: hariid,
					cmbjenis 	: kegiatanid
					
				}),
				success : function(msg) {
					
					if (msg == '') {
						//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					} else {
						$("#content").html(msg);
						window.print();
						//window.open(base_url + 'module/penjadwalan/jadwal/cetak_rekap/'+kegiatanid+'/'+semesterid+'/'+hariid,"_blank");
					}
				}
			});
	});
		
</script>


