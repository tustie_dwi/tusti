	
<?php
$this->head();
$header			= "Write New Absen";	

?>
<div class="container">  
	<legend>
		<a href="<?php echo $this->location('module/master/akademik/absen'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> Rekap Absen</a> 
		<?php if($posts){	?>
		<a href="<?php echo $this->location('module/penjadwalan/absen/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Absen</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
	<div class="row">
		<div class="col-md-4">
			<form name="frmDosen" id="form-add-ujian" method="post" role="form" action="<?php echo $this->location('module/penjadwalan/absen/write')?>" >	
				<div class="block-box" id="mainform">
						<div class='form-group'>
							<label>Jenis Kegiatan</label>
							<div class="controls">
								<select id="cmbjenis" name="cmbjenis"  class="cmbjadwal cmbmulti">
									<option value="-">Jenis Kegiatan</option>
									<?php
										foreach($kegiatan as $dt):
											echo "<option value='".$dt->jenis_kegiatan_id."' ";
											if($jeniskegiatan==$dt->jenis_kegiatan_id){
												echo "selected ";
											}
											echo  ">".$dt->keterangan."</option>";
										endforeach;
									?>
								</select>
							</div>
						</div>

						<div class='form-group'>
							<label>Semester</label>
							<div class="controls">
								<select id="cmbsemester" name="cmbsemester"  class="cmbjadwal cmbmulti">
									<option value="-">Semester</option>
									<?php
									foreach($semester as $dt):
										echo "<option value='".$dt->tahun_akademik."' ";
										if($semesterid==$dt->tahun_akademik){
											echo "selected";
										}
										echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
									endforeach;
									?>
								</select>
							</div>
						</div>
						
						<div class='form-group' id="form-hari">
							<label>Hari</label>
							<div class="controls">
								<select id="cmbhari" name="cmbhari" class="cmbjadwal cmbmulti">
									<option value="-">Please Select</option>
									<?php
									foreach($hari as $dt):
										echo "<option value='".$dt->value."' ";
										if($cmbhari==$dt->value){
											echo "selected";
										}
										echo ">".ucwords($dt->value)."</option>";
									endforeach;
									?>
								</select>
							</div>
						</div>	
						<div class='form-group' id="form-hari">
							<label>Ruang</label>
							<div class="controls">
								<select id="cmbruang" name="cmbruang" class="cmbjadwal cmbmulti">
									<option value="-">Please Select</option>
									<?php
									if($ruang):
										foreach($ruang as $dt):
											echo "<option value='".$dt->id."' ";
											if(isset($ruangid)==$dt->id){
												echo "selected";
											}
											echo ">".ucwords($dt->value)."</option>";
										endforeach;
									endif;
									?>
								</select>
							</div>
						</div>										
					
				</div>
			
			<div id="form-report"></div>
					
			<div id="jadwalform">
				<div class="form-group">
					<label>Ruang</label>					
						<input type="text" name="nama_ruang" class="ruangform form-control" id="input-ruang-nama" value="<?php if(isset($ruangid)){ echo $ruangid;} ?>" readonly>
						<input type="hidden" name="ruangid" class="ruangform form-control" id="input-ruang" value="<?php if(isset($ruangid)){ echo $ruangid;} ?>">	
				</div>

				
				
				<div id="jadwalformdetail"></div>
				
			</div>			
			</form>			
		</div>
		
		<div class="col-md-8 content-print" id="content"></div>
		
	</div>
</div>

	<?php
$this->foot();
?>
