<div class="col-md-12">
	<div class="alert alert-warning">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <strong>Perhatian!</strong> Untuk memasukkan absen, silahkan klik pada jadwal yang tersedia.
	</div>
</div>
<div class="col-md-12">

	<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small class="margin">Prodi Informatika</small>
	<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small class="margin">Prodi Sistem Komputer</small>
	<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small>Prodi Sistem Infomasi</small>
	<span class="label label-inverse">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small>Praktikum</small>
	<span class="label label-danger">&nbsp;&nbsp;&nbsp;&nbsp;</span>
	<small>Kelas Tambahan</small>
</div>
<div class="table-responsive">
<table class="table table-bordered table-jadwal">
	<thead>
		<tr class="calendar-row">
			<th class="calendar-day-head"><small>R/J</small></th>
			<?php
				foreach($jam as $dt):
					echo "<th class='calendar-day-head'><small>".$dt->mulai." - ".$dt->selesai."</small></th>";
				endforeach;
			?>
		</tr>
	</thead>
	<tbody>
	<?php
	$tgl 	= date("Y-m-d");
	$tglval = strtotime(date("Y-m-d"));
	$minfo = new model_info();
	
	foreach($ruang as $dtruang):
		echo "<tr><td class='ruang'>".ucFirst($dtruang->value)."</td>";
			$i=0;
			$skip = false;
			for($i = 0; $i< count($jam); $i++){

				if($jam[$i]->is_istirahat==1){
					$class = "istirahat";
				}else{
					$class = "";
				}

				if($idhari!='0'){
					$skip = false;
					if($jadwal){
					
					
					foreach($jadwal as $mk) {
						if($mk->repeat_on==""){
							$drepeat = "monthly";
							$dtmulai 	= $tmulai;
							$dtselesai 	= $tselesai;
							$label		= "<b>".$mk->namamk."</b>";
						}else{
							$drepeat = $mk->repeat_on;
							$dtmulai 	= $mk->tgl_mulai;
							$dtselesai 	=  $mk->tgl_selesai;
							
							//if($tglval==strtotime($dtmulai) || (($tglval > strtotime($dtmulai))&&($tglval < strtotime($dtselesai))) || ($tglval==strtotime($dtselesai)) ){
							
							$in = strtotime($dtselesai);
							$tglselesai	= strtotime(date("Y-m-d", strtotime('+1 days', $in)));
							
							
							if($drepeat=='daily'){
								if($tglval>$in){
									$label  = "";
								}else{
									$label	= "<b>".$mk->namamk."</b><br><span class='label label-danger'>".$mk->tgl_mulai." *</span>";
								}
							}else{
								$label		= "<b>".$mk->namamk."</b>";
							}
							//}else{
								//$label		= $mk->namamk;
							//}
						}
						
						
						
						if($mk->jam_mulai == $jam[$i]->mulai && strtolower($mk->ruang) == strtolower($dtruang->id)) {
							$colspan = ($mk->kode_selesai-$mk->kode_mulai)+1;

							if($colspan < 2){
								$colspan = $colspan +1;
							}else{
								$colspan = $colspan;
							}
							
							switch($mk->prodi_id){
								case 'SISKOM':
									$class = "label-warning";
								break;
								case 'SI':
									$class = "label-success";
								break;
								case 'ILKOM':
									$class = "label-info";
								break;
							}
							
							if($mk->nama){
								$nama = $mk->nama;
							}else{
								$nama = 'Dosen Pengampu';
							}
							
							$detaildata = $minfo->get_jadwal_by_hari('PTIIK', $cmbhari, $mk->tahun_akademik, "", $mk->jam_mulai, $mk->jam_selesai, $dtruang->id);
							
							if($detaildata && (count($detaildata) > 1)){
								$strdetail = "";
								echo '<td colspan="'.$colspan.'" style="background:#ddd;">';
								foreach($detaildata as $key):
									switch($key->prodi_id){
										case 'SISKOM':
											$sclass = "label-warning";
										break;
										case 'SI':
											$sclass = "label-success";
										break;
										case 'ILKOM':
											$sclass = "label-info";
										break;
									}
									if($key->nama){
										$namad = $key->nama;
									}else{
										$namad = 'Dosen Pengampu';
									}
							
									$strdetail.= '<div style="border-bottom:solid 1px #000;"><a href="#" class="popup-form" data-repeat='.$drepeat.' data-tmulai='.$dtmulai.' data-tselesai='.$dtselesai.'  data-ruang='.$dtruang->id.'|'.$dtruang->value.' data-jammulai='.$key->jam_mulai.' data-jamselesai='.$key->jam_selesai.' data-id='.$key->jadwal_id.'><span class="text text-default"><b>'.$key->namamk.'</b><br><span class="label label-danger">'.$key->tgl_mulai.' *</span><br>'.$namad.'<br>( Kelas - '.$key->kelas_id.' )</span></a></div>';
								endforeach;
								echo $strdetail;
								echo "<div>
										<a href='#' class='popup-form' data-repeat='daily' data-tmulai='".$dtmulai."' data-tselesai='".$dtselesai."' data-ruang='".$dtruang->id.'|'.$dtruang->value."' data-jammulai='".$jam[$i]->mulai."' data-jamselesai='".$jam[$i]->selesai."' data-id='' colspan=".$colspan.">+ New Jadwal</a></div>";
								echo "</td>";
							}else{
								if($mk->is_praktikum=='0'){
									echo '<td colspan="'.$colspan.'" class="'.$class.' popup-form" data-repeat="'.$drepeat.'" data-tmulai="'.$dtmulai.'" data-tselesai="'.$dtselesai.'"  data-ruang="'.$dtruang->id.'|'.$dtruang->value.'" data-jammulai="'.$mk->jam_mulai.'" data-jamselesai="'.$mk->jam_selesai.'" data-id="'.$mk->jadwal_id.'">'.$label.'<br>'.$nama.'<br>( Kelas - '.$mk->kelas_id.' )</td>';
								}else{
									echo '<td colspan="'.$colspan.'" class="'.$class.' popup-form" data-repeat="'.$drepeat.'" data-tmulai="'.$dtmulai.'" data-tselesai="'.$dtselesai.'" data-ruang="'.$dtruang->id.'|'.$dtruang->value.'" data-jammulai="'.$mk->jam_mulai.'" data-jamselesai="'.$mk->jam_selesai.'" data-id="'.$mk->jadwal_id.'">'.$label.'<br><span class="label label-inverse">'.$nama.'</span><br>( Kelas - '.$mk->kelas_id.' )</td>';
								}
							}
							
														
							$i+=$colspan-1;
							$skip = true;
							break;
						}
					}
					}
					if(!$skip) echo "<td class='".$class."' data-repeat='monthly' data-tmulai='".$tmulai."' data-tselesai='".$tselesai."' data-ruang='".$dtruang->id.'|'.$dtruang->value."' data-jammulai='".$jam[$i]->mulai."' data-jamselesai='".$jam[$i]->selesai."' data-id=''>&nbsp;</td>";
				}else{
					 echo "<td class='".$class."' data-repeat='monthly' data-tmulai='".$tmulai."' data-tselesai='".$tselesai."' data-ruang='".$dtruang->id.'|'.$dtruang->value."' data-jammulai='".$jam[$i]->mulai."' data-jamselesai='".$jam[$i]->selesai."' data-id=''>&nbsp;</td>";
				}
			}

		echo "</tr>";
	endforeach;
?>
</tbody>
</table>
<div class="col-md-12">
<span class="label label-info">&nbsp;&nbsp;&nbsp;&nbsp;</span>
<small class="margin">Informatika / Ilmu Komputer</small>
<span class="label label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span>
<small class="margin">Sistem Komputer</small>
<span class="label label-warning">&nbsp;&nbsp;&nbsp;&nbsp;</span>
<small>Sistem Infomasi</small>
</div>

</div>

<script>
		$(document).ready(function() {   
			$('#jadwalform').hide();
		 });
		 
		
		 
		 $('.popup-form').click(function(e){
			
			e.preventDefault();
			var ruang=$(this).data('ruang');
			var mulai=$(this).data('jammulai');
			var selesai=$(this).data('jamselesai');
			var tglmulai=$(this).data('tmulai');
			var tglselesai=$(this).data('tselesai');
			var repeat=$(this).data('repeat');
			var id=$(this).data('id');
		
			$('#jadwalform').show('slow');
			$('#jadwalform #form-tgl').hide();
			$('#jadwalform #form-repeat').hide();
			
			var semester 	= document.getElementById("cmbsemester");
			var semesterid	= $(semester).val();
					
			var kegiatan	= document.getElementById("cmbjenis");
			var kegiatanid	= $(kegiatan).val();
			
			var hari		= document.getElementById("cmbhari");
			var hariid		= $(hari).val();
						
			var data = ruang.split('|');
			
			var idruang_ = data[0];
			var nruang_ = data[1];
		
			$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/penjadwalan/absen/jadwal_by_form',
					data : $.param({
						cmbsemester : semesterid,
						cmbhari		: hariid,
						cmbjenis 	: kegiatanid,
						cmbruang 	: ruang,
						cmbmulai 	: mulai,	
						cmbselesai 	: selesai,
						cmbjadwal	: id
						
					}),
					success : function(msg) {
						
						if (msg == '') {
							//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
							//$("#jadwalformdetail").html(msg);
						} else {
							$("#jadwalformdetail").html(msg);
							 //location.reload();
						}
					}
				});
			
			$('#jadwalform #input-ruang').val(idruang_);
			$('#jadwalform #input-ruang-nama').val(nruang_);
			$('#jadwalform #input-mulai').val(mulai);
			$('#jadwalform #input-selesai').val(selesai);
			$('#jadwalform #form-repeat').show();
			$("#cmbrepeat option[value="+repeat+"]").attr('selected', 'selected');	
			$('#jadwalform #input-tgl-mulai').val(tglmulai);
			$('#jadwalform #input-tgl-selesai').val(tglselesai);			
			
		});
			
		$('#btn-modal-close').click(function(e){
			e.preventDefault();	
			$('#jadwalform').hide();
		});
		
		
		
	</script>