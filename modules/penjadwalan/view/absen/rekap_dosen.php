<?php $total = $pertemuan; ?>	
<div class="control-group">
	<div class="control-label">Tahun Akademik</div>
	<div class="controls">
		<!--<input type="text" name="semester" value="<?php// echo $sval;?>">-->
		<select name="cmbsemester" id="cmbsemester" onChange="get_dosen()">
			<option value="-">Semester</option>
			<?php
			foreach($semester as $dt):
				echo "<option value='".$dt->tahun_akademik."' ";
				if($sid==$dt->tahun_akademik){
					echo "selected";
				}
				echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
			endforeach;
			?>
		</select>
	</div>
</div>
<div class="control-group">
	<div class="control-label">Pengampu</div>
	<div class="controls">
		<select name="cmbdosen" id="cmbdosen" onChange="get_periode_absen();">
			<option value='0'>Please Select...</option>						
		</select>
	</div>
</div>
<div class="control-group">
	<div class="control-label">Periode</div>
	<div class="controls">
		<select name="cmbperiode" id="cmbperiode" onChange="get_rekap_data();">
			<option value='0'>Please Select...</option>						
		</select>
	</div>
</div>
<!--<div class="control-group">
	<div class="control-label">MK Ditawarkan</div>
	<div class="controls">
		<select name="cmbmk" id="cmbmk" onChange="get_dosen();">
			<option value='0'>Please Select...</option>						
		</select>
	</div>
</div>
	
<div class="control-group">
	<div class="control-label">Pengampu</div>
	<div class="controls">
		<select name="cmbdosen" id="cmbdosen" onChange="get_rekap_dosen();">
			<option value='0'>Please Select...</option>						
		</select>
	</div>
</div>

<div class="control-group">
	<div class="control-label">Total Pertemuan</div>
	<div class="controls"><?php echo $total; ?></div>
</div>-->	
<div class="form-actions">
	<input type="button" value=" Cetak Kehadiran Dosen " class="btn btn-primary" data-loading-text="Cetak...">
	<span class="status-save" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>     
</div>
		
	
	