	
<?php
$this->head();

?>
<div class="container-fluid">  
	<div class="row">
	 <div class="col-md-12">
    	<div class="col-md-4">
			<div class="row">
		<?php
			echo $calendar;
		?>
			</div>
			<div class="row">			
				<?php echo $this->view('ruang/utilitas.php', $data)?>
			</div>
		
		</div>
		<div class="col-md-8">
			<?php		
			if($tgl){
				echo "<h3>".date('F d, Y', strtotime($tgl))."</h3>";
			}			
			?>	
			<form class="form-inline" method=post action="<?php echo $this->location('module/ruang/terpakai')?>" role="form">
				<div class="form-group">	
					<select class="form-control" name="cmbkategori" onChange='form.submit();' id="cmbkategori">
						<?php
						echo "<option value=''>Semua Kategori</option>";
						foreach($kategoriruang as $dt):
							echo "<option value='".$dt->id."' ";
								if($kategori==$dt->id){ echo "selected"; }
							echo ">".ucWords($dt->value)."</option>";
						endforeach;
						?>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" name="cmbruang" onChange='form.submit();'>
						<option value="">Semua Ruang</option>
						<?php
						foreach($ruang as $dt):
							echo "<option value='".$dt->id."' ";
								if($ruangid==$dt->id){ echo "selected"; }
							echo ">".$dt->value." </option>";
						endforeach;
						?>
					</select>	
				</div>
				<div class="form-group">
					<button type="submit" name="b_cek" class="btn btn-no-radius"> View </button>
					<input type="hidden" id="hidtgl" name="hidtgl" value="<?php echo strtotime($tgl); ?>">
				</div>
				</form>
			<br>
			
			<div class="table-responsive">			
				<table class='table table-bordered'>
					<thead>
						<tr>
							<th>Ruang</th>
							<th>Event</th>
							<?php
							/*foreach($blokwaktu as $dt):
								echo "<td>".$dt->jam_mulai."</td>";
							endforeach;*/
							?>
						</tr>
					</thead>
					<tbody>
						<?php
						$mruang = new model_ruanginfo();
						if($druang){
						foreach($druang as $dt):
							echo "<tr>";
							echo "<td><b>".$dt->id."</b></td>";
							//foreach($blokwaktu as $data):
								echo "<td>";
									//$cek 	= $mruang->cek_ketersediaan($tgl, $dt->id, $data->jam_mulai);
									$cek 	= $mruang->cek_ketersediaan($tgl, $dt->id);
									$content = "";
									$i=0;
									if($cek){
										foreach($cek as $cek):
											$i++;
											if($cek->jenis_kegiatan_id=='kuliah'){
												//$kegiatan = $cek->kegiatan . " ". $cek->catatan;
												$row = $mruang->get_detail_jadwal_mk($cek->jadwal_id);
												if($row){
													$kegiatan = $row->namamk.", Kelas ". $row->kelas_id.", ".$cek->catatan;
												}else{
													$kegiatan = $cek->kegiatan . " ". $cek->catatan;
												}
											}else{
												$kegiatan = $cek->kegiatan;
											}												
											$content.= '<small><i class="fa fa-clock-o"></i>&nbsp;'.$cek->jam_mulai.' - '.$cek->jam_selesai.', R. '.$dt->id.', <b>'.$kegiatan.'</b></small><br>';
																						
											//$content.= 'R. '.$dt->id. ' ('.$cek->jam_mulai.' - '.$cek->jam_selesai.')</small><br>';
										endforeach;
									}
									echo $content;
								echo "</td>";
							//endforeach;
							echo "</tr>";
						endforeach;
						}
						?>
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>
	<?php
//$this->foot();
//$this->view("footer-v4.php");
?>
