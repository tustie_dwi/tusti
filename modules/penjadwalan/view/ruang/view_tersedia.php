<table cellpadding="0" cellspacing="0" class="table table-bordered">
<?php

		$minfo = new model_ruanginfo();
		$blokwaktu	= $minfo->get_blok_waktu();

		?>
		<thead>
			<tr class="calendar-row">
				<td class="calendar-day-head"><small>J/R</small></td>
				<?php
				foreach($blokwaktu as $dt):
				?><td class="calendar-day-head"><small><?php echo $dt->jam_mulai ?></small></td>
				<?php				
				endforeach;
				?>
			</tr>
		</thead>
		<tbody>
		<?php
		if($tgl){
			$ruang = $minfo->get_ruang($kategori, $ruangid);
			
			date_default_timezone_set('Asia/Jakarta');
			$jam=date("H:i", strtotime("now"));
			$tsekarang=date('Y-m-d');
			
			$waktu = str_replace(":", "",$jam);
			
			$uri = $this->location('module/penjadwalan/ruang/app');
			$no = 0;		
			foreach($ruang as $dt):
				$no++;
				?>				
				<tr>
					<td><small><b>R. <?php echo $dt->id ?></b></small></td>
					<?php
					foreach($blokwaktu as $data):
						$cek 	= $minfo->cek_ketersediaan($tgl, $dt->id, $data->jam_mulai);
						$popid 	= str_replace(".","",$dt->id).substr($data->jam_mulai,0,2);
						
						$tglo=str_replace("-", "",$tgl);						
						
						switch($tglo){
							case ($tglo==str_replace("-", "",$tsekarang) && ($waktu<str_replace(":","",$data->jam_mulai))):
								$strclass= "label-important ";
								$tdclass = "popup-form-resv-disabled";
								$strtitle= "Reserved";
								$stricon = "x";								
							break;
							case ($tglo==str_replace("-", "",$tsekarang) && ($waktu>str_replace(":","",$data->jam_mulai))):
								$strclass= "label-success popup-form-resv";
								$tdclass = "popup-form-resv";
								$strtitle= "Finished";
								$stricon = "v";
							break;
							case ($tglo<str_replace("-", "",$tsekarang)):
								$strclass= "label-success popup-form-resv";
								$tdclass = "popup-form-resv";
								$strtitle= "Finished";
								$stricon = "v";
							break;
							case ($tglo>str_replace("-", "",$tsekarang)):
								$strclass= "label-important";
								$tdclass = "popup-form-resv-disabled";
								$strtitle= "Reserved";
								$stricon = "x";
							break;
							
						}
						
						if($cek){							
							?>
							<td data-id="<?php echo $dt->id ?>" data-mulai="<?php echo $data->jam_mulai ?>" >
							<?php												
							//$content = "";
							$i=0;
							$strapprove = 0;
							foreach($cek as $key):
								$i++;
								
								$content= $key->kegiatan. " - ".'R. '.$dt->id. ' ('.$key->jam_mulai.' - '.$key->jam_selesai.')';
								$strapprove = $key->is_approve;
								
								if ($strapprove	=='0') 
									if($stricon=='x')
									$strclassok = "label-danger";
									else
										$strclassok = "label-warning";
								else $strclassok = $strclass;
								?>
								<ul class="nav nav-pills" style="margin:0;"><li class="dropdown">
									<small>
										<a href="#" class="dropdown-toggle" id="drop4" role="button" data-toggle="dropdown" data-placement="right"
											title="<?php echo $content ?>"><span class="label <?php echo $strclassok; ?> pull-center"><?php echo $stricon ?></span></a> 
											<ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
												<!--<li id="post-<?php //echo $key->detail_id ?>" data-id="<?php //echo $key->detail_id ?>">
													<a data-id="<?php //echo $key->detail_id ?>" class="btn-view-ruang"><i class="fa fa-search"></i> View</a>
												</li>-->
												<?php if($strapprove==1): ?>
												<li id="post-<?php echo $key->detail_id ?>" data-id="<?php echo $key->detail_id ?>">
													<a data-id="<?php echo $key->detail_id ?>" class="btn-delete-ruang"><i class="fa fa-trash-o"></i> Delete</a>
												</li>	
												<li id="post-<?php echo $key->detail_id ?>" data-id="<?php echo $key->detail_id ?>">
													<a data-id="<?php echo $key->detail_id ?>" class="btn-approve-ruang"><i class="fa fa-check"></i> Approve</a>
												</li>	
												<?php endif; ?>												
											</ul>
									</small></li></ul>
								<?php
								
							endforeach;		
							?>
							</td>
							<?php
						}else{
							?>
							<td class="popup-form-resv" data-id="<?php echo $dt->id ?>" data-mulai="<?php echo $data->jam_mulai ?>" ></td>
						<?php } ?>						
						<?php
					endforeach;
					?>
				</tr>
				<?php
			endforeach;
			
		}
		?>
		</tbody></table>
		


