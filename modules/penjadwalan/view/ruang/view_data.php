<?php
if($posts):
	foreach ($posts as $dt):
		?>	
		<div class="row">
			<h3>Detail Kegiatan</h3>
			<div class="form-group">
				<label>Kegiatan</label>
				<div class="control-label"><?php echo $dt->kegiatan; ?></div>
			</div>
			<div class="form-group">
				<label>Ruang</label>
				<div class="control-label"><?php echo $dt->ruang; ?></div>
			</div>
			<div class="form-group">
				<label>Pelaksanaan</label>
				<div class="control-label"><?php echo date("M d, Y", strtotime($dt->tgl)); ?>, <?php echo date("H:i", strtotime($dt->jam_mulai)) ?> s/d  <?php echo date("H:i", strtotime($dt->jam_selesai)) ?></div>
			</div>
			<div class="form-group">
				<input type="hidden" id="input-tmp" name="hidtmp" value="<?php echo $dt->detail_id ?>">
				<a  href="#" class="btn btn-primary btn-approve-ruang"><i class="fa fa-check"></i> Approve</a>
				<a  href="#" class="btn btn-danger btn-delete-ruang"><i class="fa fa-ban"></i> Reject</a>
				<a  href="#" class="btn btn-default btn-close-ruang"><i class="fa fa-times"></i> Close</a>
			</div>
		</div>
		<?php
	endforeach;
else:
	echo "Sorry, no record found";
endif;
?>

<script>
	$('.btn-close-ruang').click(function(e){	
		$(".form-reservasi").removeClass("col-md-4");
		$(".view-tersedia").removeClass("col-md-8");
		$(".form-reservasi").hide();
			
	});
	
	$('.btn-delete-ruang').click(function(e){	
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
		$.post(
			base_url + 'module/penjadwalan/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
		
		$(".form-reservasi").removeClass("col-md-4");
		$(".view-tersedia").removeClass("col-md-8");
		$(".form-reservasi").hide();
			
	});
	
	$('.btn-approve-ruang').click(function(e){	
		$(".form-reservasi").removeClass("col-md-4");
		$(".view-tersedia").removeClass("col-md-8");
		$(".form-reservasi").hide();
			
	});
</script>