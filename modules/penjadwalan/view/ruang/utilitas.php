
		<script src="<?php echo $this->coms->asset("chart/chart/jquery-1.10.2.min.js"); ?>" ></script>
		<script src="<?php echo $this->coms->asset("chart/chart/knockout-3.0.0.js"); ?>" ></script>
		<script src="<?php echo $this->coms->asset("chart/chart/globalize.min.js"); ?>" ></script>
		<script src="<?php echo $this->coms->asset("chart/chart/dx.chartjs.js"); ?>" ></script>   
		
		<script>
			$(function ()  
				{
					//alert($('#hidtgl').val());
					
					var a = $('#hidtgl').val(); 
					var b = $('#cmbkategori').val(); 
					
				   $.getJSON( base_url +'module/penjadwalan/ruang/ruang_json/'+a+'/'+b, function(dataSource) {
					$('#chartContainer').dxChart({
						dataSource: dataSource,
						 commonSeriesSettings: {
							type: "spline",
							argumentField: "ruang",
							valueField: 'kegiatan'
						},
							
						commonAxisSettings: {
							grid: {
								visible: true
							}
						},
						series: [{ valueField: 'kegiatan', name: "Jml Kegiatan", 
						selectionStyle: {
							color: "red",
							hatching: "none"
						}  },],
						tooltip:{
							enabled: true,
							customizeText: function () {
							return this.argumentText + " - " + this.valueText;
							}
						},
						title: 'Utilisasi Ruang',
						
						valueAxis: {
							label: {
								customizeText: function() {
									return this.value;
								}
								//visible: true
							}
						},
						legend: {
							 verticalAlignment: "bottom",
							horizontalAlignment: "center"
						},
						commonPaneSettings: {
							border:{
								visible: true,
								right: false
							}       
						}
					})
				});
				}

			);
		</script>
	<div id="chartContainer" style="width: 100%; height: 440px;"></div>
	
