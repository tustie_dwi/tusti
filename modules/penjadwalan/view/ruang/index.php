<?php $this->head(); ?>
<SCRIPT language="javascript">
function OpenPage(page) {
	if (page != "-1") {
		window.open(page, '_self');
	}
}
</SCRIPT>
<div class="container-fluid">  
	<div class="row">
		<legend><a href="<?php echo $this->location('module/masterdata/agenda/write'); ?>" class="btn btn-primary pull-right">
		<i class="icon-pencil icon-white"></i> New Reservasi Ruang</a>
		List Pemakaian Ruang</legend>
		<div class="well">
			<?php
			 $month = $month;    
			 $year  = $year; 	 
			?>
				<form class="form-inline" method=post action="<?php echo $this->location('module/penjadwalan/ruang/terpakai')?>">
				<select name="month" onChange='form.submit();' class='span3'>
				<?php
					for($x = 1; $x <= 12; $x++)
					{
					
					echo "<option value='".$x."' ";
						if($month==$x){
							echo "selected";
						}
						
					echo ">".date('F',mktime(0,0,0,$x,1,$year))."</option>";
					 
					}

				?>
				</select>
				
				<?php
				 $year_range = 10;
				 $selectYear = '<select name="year" onChange="form.submit();" class="input-small">';
				 for($x = ($year-floor($year_range/2)); $x <= ($year+floor($year_range/2)); $x++)
				 {
					 $selectYear.= '<option value="'.$x.'"'.($x != $year ? '' : ' selected="selected"').'>'.$x.'</option>';
				 }
				 $selectYear.= '</select>';
				 
				 echo $selectYear;
				?>
				<select name="cmbruang" onChange='form.submit();' class='input-medium'>
					<option value="">All Ruang</option>
					<?php
					foreach($ruangan as $dt):
						echo "<option value='".$dt->id."' ";
						if($ruangid==$dt->id){ echo "selected"; }
						echo ">".$dt->value."</option>";
					endforeach;
					?>
				</select>
						<select name="kegiatan" onChange='form.submit();'>
					<option value="">All Kegiatan</option>
					<?php
						foreach($kegiatan as $dt):
							echo "<option value='".$dt->jenis_kegiatan_id."' ";
							if($jeniskegiatan==$dt->jenis_kegiatan_id){
								echo "selected ";
							}
							echo  ">".$dt->keterangan."</option>";
						endforeach;
					?>
				</select>
			</form>
		</div>
		<?php 
		if(isset($status) and $status) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $statusmsg; ?>
			</div>
		<?php 
		endif; 
		?>
		<div class="control-group">			
			<ul class="nav nav-tabs" id="writeTab">
				<li class="active"><a href="#list">List Pemakaian Ruang</a></li>
				<li><a href="#grid">Grid View</a></li>
			</ul>
			<div class="tab-content">			
				<div class="tab-pane active" id="list">	<?php $this->view('ruang/listview.php', $data); ?>	</div>
				<div class="tab-pane" id="grid"><?php  $this->view('ruang/gridview.php', $data); ?></div>
			</div>			
		</div>
	</div>
</div>

<?php $this->foot(); ?>