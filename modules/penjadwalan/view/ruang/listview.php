
	<?php
	
	//echo $jeniskegiatan;
	if( isset($posts) ) :		
	 
	$str="<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>Tgl/Jam</th>
						<th>Ruang</th>	
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>";		
			$i = 1;			
			foreach ($posts as $dt): 
				$str.="
						<tr id='post-".$dt->detail_id."' data-id='".$dt->detail_id."' valign=top valign=top>
							<td><span class='text text-warning'>".date("M d, Y", strtotime($dt->tgl))."</span>
							<br><i class='icon-time'></i>&nbsp;<small><b>".date('H:i',strtotime($dt->jam_mulai))." - ".date('H:i',strtotime($dt->jam_selesai))."</b></small></td>";		
							
					$str.= "<td><span class='text text-info'>".$dt->ruang_id."</span></td><td><em>".$dt->kegiatan."</em>";
					if(($dt->jenis_kegiatan_id=='kuliah')||($dt->jenis_kegiatan_id=='praktikum')){
						$str.= "<blockquote><small>".$dt->catatan."</small></blockquote>";
					}
					
					$str.= "</td></tr>";
			endforeach; 
			
		$str.= "</tbody></table>";
		
		echo $str;
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>