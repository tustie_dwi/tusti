$(function() {
  $('#jadwalform').hide();
  $("#mainform #form-report-koordinator").hide();
  $("#form-report-view").hide();
  $("#content").hide();
  
   $('#myModal').modal('hide', function(){
	//var hari = $('.hari').attr('value');
   });
   
   
		
	$('#writeTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
  });
  
   
  
  
   $("[data-toggle=tooltip]").tooltip();
   /*$('.dropdown-toggle').tooltip({
		selector: "a[rel=tooltip]"
	});*/
	
	$(".pop").each(function() {
		var $pElem= $(this);
		$pElem.popover(
			{
			  title: getPopTitle($pElem.attr("id")),
			  content: getPopContent($pElem.attr("id")),
			  trigger:'hover'
			}
		);
	});
					
	function getPopTitle(target) {
		return $("#" + target + "_content > div.popTitle").html();
	};
			
	function getPopContent(target) {
		return $("#" + target + "_content > div.popContent").html();
	};
	
	$(".btn-delete-post").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/deletemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	$(".btn-disable-jadwal").click(function(){
		
		var pid = $(this).data("id");
			
		if(confirm("Disable this schedule? Once done, this action can not be undone.")) {
					
		$.post(
			base_url + 'module/penjadwalan/jadwal/disablemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					//row.fadeOut();
					load_jadwal();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	
	$(".btn-delete-post-kegiatan").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
		
	$( "#date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$( ".date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
		
	$(".dosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
		} 
	});  

	$(".ndosen").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ndosenid').val(ui.item.id); 
			$('#ndosen').val(ui.item.value);
		} 
	}); 

$	(".tagStaffUjian").autocomplete({ 
		source: base_url + "/module/penjadwalan/conf/staff_tmp",
		minLength: 0, 
		select: function(event, ui) { 
			$('#panitiaid').val(ui.item.id); 
			$('#panitia').val(ui.item.value);
		} 
	});
	

	$(".pengampu").autocomplete({ 
		source: base_url + "/module/masterdata/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#pengampuid').val(ui.item.id); 
			$('#pengampu').val(ui.item.value);
		} 
	});  			
	
	$("#dosen").autocomplete({ 
		source: base_url + "/module/akademik/conf/dosen",
		minLength: 0, 
		select: function(event, ui) { 
			$('#dosenid').val(ui.item.id); 
			$('#dosen').val(ui.item.value);
			$('#frmDosen').submit(); 
		} 
	});  			
	
		
	$(".hari").autocomplete({ 
		source: base_url + "/module/masterdata/conf/hari",
		minLength: 0, 
		select: function(event, ui) { 
			$('#hariid').val(ui.item.id); 
			$('#hari').val(ui.item.value);
		} 
	});  
		
	
	
	$("#ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".ruang").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#ruangid').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".blokwaktu").autocomplete({ 
		source: base_url + "/module/penjadwalan/conf/blokwaktu",
		minLength: 0, 
		select: function(event, ui) { 
			$('#blokid').val(ui.item.id); 
			$('#blok').val(ui.item.value);
		} 
	});  
	
	$(".jammulai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jammulai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jammulaiid').val(ui.item.id); 
			$('#jammulai').val(ui.item.value);
		} 
	});  
	
	$(".jamselesai").autocomplete({ 
		source: base_url + "/module/masterdata/conf/jamselesai",
		minLength: 0, 
		select: function(event, ui) { 
			$('#jamselesaiid').val(ui.item.id); 
			$('#jamselesai').val(ui.item.value);
		} 
	});  
	
	$(".prodi").autocomplete({ 
		source: base_url + "/module/masterdata/conf/prodi",
		minLength: 0, 
		select: function(event, ui) { 
			$('#prodiid').val(ui.item.id); 
			$('#prodi').val(ui.item.value);
		} 
	});  
	
	$("#mkditawarkan").autocomplete({ 
		source: base_url + "/module/akademik/conf/mkditawarkan/1",
		minLength: 0, 
		select: function(event, ui) { 
			$('#mkditawarkanid').val(ui.item.id); 
			$('#mkditawarkan').val(ui.item.value);
		} 
	});  
	
	
	$(".cmbmulti").select2();	
	$("#cmbmulti").select2();
	$("#cmbunit").select2();
	$("#cmbruang").select2();
	

   
   $(".btn-delete-post").click(function(){
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/deletemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	$(".btn-delete-ujian").click(function(){
		
		var pid = $(this).data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/deleteujian/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					//row.fadeOut();
					load_jadwal();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	
	$(".btn-delete-post-kegiatan").click(function(){
		alert("a");
		
		var pid = $(this).data("id");
		var mod	= $(this).parents('li').data("id");
		
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
		
		var row = $(this).parents('li');
				
		$.post(
			base_url + 'module/penjadwalan/jadwal/delete/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					row.fadeOut();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
	
	
	
	$(".ruangform").autocomplete({ 
		source: base_url + "/module/masterdata/conf/ruang",
		minLength: 0, 
		select: function(event, ui) { 
			$('#input-ruang').val(ui.item.id); 
			$('#ruang').val(ui.item.value);
		} 
	});  
	
	$(".blokwaktu").autocomplete({ 
		source: base_url + "/module/penjadwalan/conf/blokwaktu",
		minLength: 0, 
		select: function(event, ui) { 
			$('#blokid').val(ui.item.id); 
			$('#blok').val(ui.item.value);
		} 
	});  

	
	$(".cmbmulti").select2();
	$(".e9").select2();	
	$('#jadwalform').hide();	
		  
	
	$(".tagStaff").tagsManager({
			prefilled: $('.tmpstaff').val(),	
			deleteTagsOnBackspace: true,	
			preventSubmitOnEnter: true,
			typeahead: true,
			typeaheadAjaxSource: base_url + "/module/masterdata/conf/staff",
			AjaxPush: base_url + "/module/masterdata/conf/staff/push",
			blinkBGColor_1: '#FFFF9C',
			blinkBGColor_2: '#CDE69C',
			hiddenTagListName: 'hidStaff'
		  });	  	
	});



 $('.cmbjadwal').change(function(e) {
	
	e.preventDefault();	
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	
	$('#jadwalform').hide();
	
		
	if((kegiatanid=='uts') || (kegiatanid=='uas')){
		if(semesterid!="-"){
			$("#form-report-koordinator").show();
			$("#form-report").show();
			$("#form-hari").show();
			$("#form-kegiatan").hide();
			get_koordinator(semesterid, kegiatanid);
			get_panitia(semesterid, kegiatanid);
			create_report_form();	
		}
	}else{
		if(kegiatanid=='bimbingan'){
			$("#form-kegiatan").show();
			$("#form-report-koordinator").hide();
			$("#form-report").hide();
			$("#form-hari").hide();
			get_kegiatan(semesterid, kegiatanid, fakultasid);
		}else{
			$("#form-hari").show();
			$("#form-kegiatan").hide();
			$("#form-report-koordinator").hide();
			$("#form-report").hide();
		}
	}
	load_jadwal();
	
});


function get_kegiatan(semesterid, kegiatanid, fakultas){
	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/kegiatan_form',
			data : $.param({
				cmbsemester : semesterid,
				cmbjenis 	: kegiatanid,
				cmbfak		: fakultas
				
			}),
			success : function(msg) {
				if (msg == '') {
					
				} else {
					$("#form-kegiatan").html(msg);
				
				}
			}
		});
}

function get_koordinator(semesterid, kegiatanid){
	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/get_koordinator',
			data : $.param({
				cmbsemester : semesterid,
				cmbjenis 	: kegiatanid
				
			}),
			success : function(msg) {
				if (msg == '') {
					$('#ndosenid').val(""); 
					$('#ndosen').val("");
				} else {
					var msg = JSON.parse(msg);
					
					for (var i = 0; i < msg.length; i++) {
						var data = msg[i];						
						$('#ndosenid').val(data.koordinator_id); 
						$('#ndosen').val(data.nama);
					}	
				
				}
			}
		});
}

function get_panitia(semesterid, kegiatanid){
	var nama="";
	var id = "";
		
	var tagApi = jQuery(".tagStaffUjian").tagsManager({
		prefilled: $('.tmpstaffujian').val(),	
		deleteTagsOnBackspace: true,	
		preventSubmitOnEnter: true,
		typeahead: true,
		replace:true,
		hiddenTagListName: 'hidStaff'
      });
		
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/get_panitia',
			data : $.param({
				cmbsemester : semesterid,
				cmbjenis 	: kegiatanid				
			}),
			success : function(msg) {
				
				tagApi.tagsManager('empty');
				if (msg == '') {
					$('#panitiaid').val(""); 
					$('#panitia').val("");
				} else {
					var msg = JSON.parse(msg);
					
					for (var i = 0; i < msg.length; i++) {
						var data = msg[i];	
						nama += data.nama +",";
						id += data.nama +"&";
						
						tagApi.tagsManager("pushTag", data.nama);
						
					}
					
					
					
					
				}
			}
		});
}

function create_report_form(){
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	
		
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/form_report',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbfak		: fakultasid,
				cmbjenis 	: kegiatanid
				
			}),
			success : function(msg) {
				
				if (msg == '') {
					//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
				} else {
					$("#form-report").html(msg);
					$("#form-report-view").hide();
					 //location.reload();
				}
			}
		});
}
	
function load_jadwal(){			
	$('#jadwalform').hide();
	
	$("#content").show();
		
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
			
	var kegiatan	= document.getElementById("cmbjenis");
	var kegiatanid	= $(kegiatan).val();
	
	var fakultas	= document.getElementById("cmbfak");
	var fakultasid	= $(fakultas).val();
	
	var hari		= document.getElementById("cmbhari");
	var hariid		= $(hari).val();
	
	var ruang		= document.getElementById("ruangid");
	var ruangid		= $(ruang).val();
	
	var mulai		= document.getElementById("input-mulai");
	var jammulai	= $(mulai).val();
	
	var selesai		= document.getElementById("input-selesai");
	var jamselesai	= $(selesai).val();
	
	var tgl		= document.getElementById("input-tgl");
	var tglval	= $(tgl).val();
	
	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/penjadwalan/jadwal/jadwal_view',
			data : $.param({
				cmbsemester : semesterid,
				cmbhari		: hariid,
				cmbjenis 	: kegiatanid,
				cmbfak	 	: fakultasid,
				cmbruang 	: ruangid,
				cmbmulai 	: jammulai,	
				cmbselesai 	: jamselesai,
				cmbtgl		: tglval
				
			}),
			success : function(msg) {
				
				if (msg == '') {
					//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
				} else {
					$("#content").html(msg);
					 //location.reload();
				}
			}
		});
	}
	
	function save_proses(){
	
		$('#form-add-ujian').submit(function (e) {
		
			 e.preventDefault(); //STOP default action
			var today = new Date();	
			
			var postData = $(this).serializeArray();
			//var postData = new FormData($('#form-add-ujian')[0]);
			
			var kegiatan	= document.getElementById("cmbjenis");
			var kegiatanid	= $(kegiatan).val();
			
			var semester 	= document.getElementById("cmbsemester");
			var semesterid	= $(semester).val();
			
			var fakultas	= document.getElementById("cmbfak");
			var fakultasid	= $(fakultas).val();
			
			var tmp	= $(document.getElementById("hidtmp")).val();
			
			if(kegiatanid=='uts' || kegiatanid=='uas'){
				
				var formURL = base_url + 'module/penjadwalan/jadwal/save_ujian/'+kegiatanid;
				
			}else{
				if(kegiatanid=='bimbingan'){
					var formURL = base_url + 'module/penjadwalan/jadwal/save_kegiatan';
				}else{
					var formURL = base_url + 'module/penjadwalan/jadwal/save_kuliah/'+kegiatanid;
				}
			}
			
			  $.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				success:function(msg,data, textStatus, jqXHR) 
				{	
				//alert(msg);
				
					$('.status-submit').html("<em>OK! Last saved on "+today+"</em>");
					if(kegiatanid=="bimbingan"){
						load_jadwal();	
						$("#form-report-koordinator").hide();
						$("#form-report").hide();
						$("#form-hari").hide();
						get_kegiatan(semesterid, kegiatanid);
							
					}else{
						if(tmp==""){
							load_jadwal();
						}
					}
										
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					alert ('Failed!');      
				}
			  });
		   
			return false;
		});
	
	}
	
	$(".btn-new-jadwal").click(function(){		
		$(document.getElementById("hidtmp")).val("");
		$(document.getElementById("cmbmk")).val("");
		$(document.getElementById("cmbkelasx")).val("");
		$(document.getElementById("cmbprodi")).val("");
		$(document.getElementById("new-jadwal")).hide();		
	});
	
	$(".btn-delete-jadwal").click(function(){
		
		var pid = $(this).data("id");
			
		if(confirm("Delete this post? Once done, this action can not be undone.")) {
					
		$.post(
			base_url + 'module/penjadwalan/jadwal/deletemk/'+pid,
			function(data){
				if(data.status.trim() == "OK")
					//row.fadeOut();
					load_jadwal();
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
	});
		

	
	$('#jadwalform .cmbdetail').change(function(e) {	
		
		var prodi		= document.getElementById("cmbprodi");
		var prodiid		= $(prodi).val();
		
		var mk			= document.getElementById("cmbmk");
		var mkid		= $(mk).val();
		
		var kelas		= document.getElementById("cmbkelas");
		var kelasid		= $(kelas).val();
		var output		= "";			
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/penjadwalan/jadwal/get_pengampu_kuliah',
				data : $.param({					
					cmbprodi	: prodiid,
					cmbmk		: mkid,
					cmbkelas	: kelasid					
				}),
				success : function(msg) {					
					if (msg == '') {
						//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					} else {
						
						output += "<label>Pengampu</label><select id='cmbdosen' class='e9 form-control' name='pengampu' ><option value='130904012736'>Dosen Pengampu</option>";
						var msg = JSON.parse(msg);
						for (var i = 0; i < msg.length; i++) {
							var data = msg[i];
							output += "<option value="+data.karyawan_id+" data-uri='1'>"+data.nama+"</option>";
						}
						output += "</select></div>";
						$('#jadwalformdetail #dosen-form').html(output);
						
					}
				}
			});
		
	});	
	

		 
		 $('.popup-form').click(function(e){
			
			e.preventDefault();
			var ruang=$(this).data('ruang');
			var mulai=$(this).data('jammulai');
			var selesai=$(this).data('jamselesai');
			var tglmulai=$(this).data('tmulai');
			var tglselesai=$(this).data('tselesai');
			var repeat=$(this).data('repeat');
			var id=$(this).data('id');
			
			var data = ruang.split('|');
			
			var idruang_ = data[0];
			var nruang_ = data[1];
		
			$('#jadwalform').show('slow');
			$('#jadwalform #form-tgl').hide();
			$('#jadwalform #form-repeat').hide();
			
			var semester 	= document.getElementById("cmbsemester");
			var semesterid	= $(semester).val();
					
			var kegiatan	= document.getElementById("cmbjenis");
			var kegiatanid	= $(kegiatan).val();
			
			var hari		= document.getElementById("cmbhari");
			var hariid		= $(hari).val();
			
			var fakultas	= document.getElementById("cmbfak");
			var fakultasid	= $(fakultas).val();
		
			$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/penjadwalan/jadwal/jadwal_by_form',
					data : $.param({
						cmbsemester : semesterid,
						cmbhari		: hariid,
						cmbfak		: fakultasid,
						cmbjenis 	: kegiatanid,
						cmbruang 	: ruang,
						cmbmulai 	: mulai,	
						cmbselesai 	: selesai,
						cmbjadwal	: id
						
					}),
					success : function(msg) {
						
						if (msg == '') {
							//$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
							//$("#jadwalformdetail").html(msg);
						} else {
							$("#jadwalformdetail").html(msg);
							 //location.reload();
						}
					}
				});
			

			$('#jadwalform #input-ruang').val(idruang_);
			$('#jadwalform #input-ruang-nama').val(nruang_);
			$('#jadwalform #input-mulai').val(mulai);
			$('#jadwalform #input-selesai').val(selesai);
			$('#jadwalform #form-repeat').show();
			$("#cmbrepeat option[value="+repeat+"]").attr('selected', 'selected');	
			$('#jadwalform #input-tgl-mulai').val(tglmulai);
			$('#jadwalform #input-tgl-selesai').val(tglselesai);			
			
		});
			
		$('#btn-modal-close').click(function(e){
			e.preventDefault();	
			$('#jadwalform').hide();
		});
	

	
	

  

	
	
