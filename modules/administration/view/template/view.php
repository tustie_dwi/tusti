<?php $this->head(); ?>
<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/administration/conf/template'); ?>">Template Surat</a></li>
	  <li><a href="<?php echo $this->location('module/administration/conf/display/'.$posts->temp_id); ?>">View</a></li>
	</ol>
    <div class="breadcrumb-more-action">
    	<div class="btn-group">
		    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
		      Conf
		      <span class="fa fa-caret-down"></span>
		    </button>
		    <ul class="dropdown-menu">
		      <li><a href="<?php echo $this->location('module/administration/conf/surat'); ?>"> Jenis Surat</a></li>
		      <li><a class="active" href="<?php echo $this->location('module/administration/conf/perihal'); ?>"> Perihal Surat</a></li>
		      <li><a href="<?php echo $this->location('module/administration/conf/instansi'); ?>"> Instansi</a></li>
		      <li><a href="<?php echo $this->location('module/administration/conf/template'); ?>"> Template</a></li>
		    </ul>
		</div>
    </div>
    <div class="row">
    	<div class="col-md-3">
		</div>
    	<div class="col-md-6">
			<div class="block-box">
				<div class="content">
					<?php
						echo "<div class='col-md-12'>";
						if($posts->kop_surat!=""){
							echo "<img width='100%' src='".$this->config->file_url_view.'/'.$posts->kop_surat."'>";
						}
						echo $posts->isi_surat;
						echo "</div>";
					?>
				</div>
			</div>
		</div>
		<div class="col-md-3">
		</div>
	</div>
<?php $this->foot(); ?>