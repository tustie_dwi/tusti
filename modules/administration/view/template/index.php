<?php $this->head(); ?>
<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<div class="row">
		<div class="col-md-5">
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="<?php echo $this->location('module/administration/conf/template'); ?>">Template Surat</a></li>
			</ol>
		    <div class="breadcrumb-more-action">
		    	<div class="btn-group">
				    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
				      Conf
				      <span class="fa fa-caret-down"></span>
				    </button>
				    <ul class="dropdown-menu">
				      <li><a href="<?php echo $this->location('module/administration/conf/surat'); ?>"> Jenis Surat</a></li>
				      <li><a class="active" href="<?php echo $this->location('module/administration/conf/perihal'); ?>"> Perihal Surat</a></li>
				      <li><a href="<?php echo $this->location('module/administration/conf/instansi'); ?>"> Instansi</a></li>
				      <li><a href="<?php echo $this->location('module/administration/conf/template'); ?>"> Template</a></li>
				    </ul>
				</div>
		    </div>
    
			<div class="block-box">
				<div class="content">
					<?php if(isset($posts)&&$posts!==""): ?>
						<table class='table table-hover surat_table'>
							<thead>
								<tr>
									<th>Detail</th>	
									<th>&nbsp;</th>	
								</tr>
							</thead>
							<tbody>
							<?php foreach ($posts as $p) { ?>
							<tr id="delete<?php echo $p->temp_id ?>">
								<td>
									<?php
										echo "<h5>".$p->jenis_surat."</h5>";
										echo "<label class='label label-success'>".ucfirst($p->kategori)."</label>";
									?>
								</td>
								<td>
									<ul class='nav nav-pills'>
										<li class='dropdown pull-right'>
										  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
										  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
											<li>
												<a class='btn-edit-post' href="<?php echo $this->location('module/administration/conf/template/'.$p->temp_id); ?>" ><i class='fa fa-edit'></i> Edit</a>		
												<a class='btn-edit-post' href="<?php echo $this->location('module/administration/conf/display/'.$p->temp_id); ?>" ><i class='fa fa-search'></i> Lihat</a>
												<a class='btn-edit-post delete' data-val="<?php echo $p->temp_id ?>" href="javascript::" ><i class='fa fa-trash-o'></i> Delete</a>
											</li>
										  </ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
							</tbody>
						</table>
					<?php else: ?>
						<div class="well" align="center">
							No Data Available
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<form method="post" name="form-template" id="form-template">
				<div class="panel panel-default">
					<div class="panel-heading" id="form-box"><?php echo $param ?></div>
					<div class="panel-body ">
						<div class="form-group">
							<label class="control-label">Kategori Template</label>								
							<select class="form-control" name="kategori">
								<option value="0">Silahkan Pilih</option>
								<option value="umum" <?php if(isset($edit->kategori)&&$edit->kategori=="umum") echo "selected" ?>>Umum</option>
								<option value="izin" <?php if(isset($edit->kategori)&&$edit->kategori=="izin") echo "selected" ?>>Izin</option>
							</select>
						</div>
						<div class="form-group" id="umum" <?php if(isset($edit->jenis_surat)&&$edit->jenis_surat=="Surat Izin") echo "style='display:none;'" ?>>
							<label class="control-label">Jenis Surat</label>								
							<select class="form-control" name="jenis">
								<option value="0">Silahkan Pilih</option>
								<?php
									foreach($jenis as $js){
										echo "<option value='".$js->jenis_surat."'";
										if(isset($edit->jenis_surat)&&$edit->jenis_surat==$js->jenis_surat){
											echo "selected";
										}
										echo ">".$js->keterangan."</option>";
									}
								?>
							</select>
						</div>
						<div class="form-group" <?php if(isset($edit->jenis_surat)&&$edit->jenis_surat=="Surat Izin") echo "style='display:none;'" ?>>
							<label class="control-label">Kop Surat</label>
							<input type="file" class="form-control" name="files"/><br>
							<?php if(isset($edit->kop_surat)&&$edit->kop_surat!==""){ ?>
								<img name="kop_display" class="well" width="400" src="<?php echo $this->config->file_url_view.'/'.$edit->kop_surat ?>"/>
								<input type="hidden" class="form-control" name="file_loc" value="<?php echo $edit->kop_surat ?>"/>
							<?php }	?>						
								
						</div>
						<div class="form-group">
							<label class="control-label">Isi Surat</label>								
							<textarea class="form-control ckeditor" name="isi_surat" id="isi_surat" placeholder="Masukkan Isi Surat"><?php if(isset($edit->isi_surat)) echo $edit->isi_surat  ?></textarea>
						</div>
						<div class="form-group">
							<input type="hidden" name="hidId" value="<?php if(isset($edit->template_id)) echo $edit->template_id; ?>">
							<button type="submit" class="btn btn-primary">Data Valid &amp; Save</button>
							<button type="button" class="btn btn-danger" style="display: none">Cancel</button>			
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $this->foot(); ?>