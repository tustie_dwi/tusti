<?php $this->head(); $i=1;?>


<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/administration/proker'); ?>">Program Kerja</a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/administration/proker/write/proker'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Program Kerja</a>
    </div>
    
    
   <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<!-- <div class="panel-heading">Program kerja</div> -->
				<div class="panel-body ">
					<?php if(isset($posts)): ?>
					
					<table id="example" class='table table-hover'>
					<thead>
						<tr>
							<th>No.</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($posts as $p) { ?>
					<tr>
						<td><?php echo $i++ ?></td>
						<td>
							<div class="col-md-8">
								<!-- isi -->
								<b>Rencana Kerja : </b><?php echo $p->judul ?> 
								<small>
									<span class="text text-info"><i class="fa fa-clock-o"></i>&nbsp;<?php echo convert_date($p->last_update) ?></span>
									<i class="fa fa-user">&nbsp;<?php echo ($p->namakaryawan) ?></i>
								</small>
								<br><b>Periode : </b><?php echo $p->periode ?>
								<br><span class="label label-success"><?php echo $p->unit ?></span>
								<br>
								
							</div>
							<div class="col-md-2">
								<?php if($p->is_valid=='1'){ ?>
									<span class="label label-info">Approved</span>
								<?php }elseif($p->is_valid=='2'){ ?>
									<span class="label label-danger">Rejected</span>
								<?php }else{ ?>
									<span class="label label-warning">Need Approval</span>
								<?php } ?>
							</div>
							<div class="col-md-2">
								<ul class='nav nav-pills'>
									<li class='dropdown pull-right'>
									  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
										<li>
										<a class='btn-detail-post' href="<?php echo $this->location('module/administration/proker/detail/proker/'.$p->rencana_id) ?>" ><i class='fa fa-eye'></i> Detail</a>	
										</li>
										<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/administration/proker/edit/proker/'.$p->rencana_id) ?>" ><i class='fa fa-edit'></i> Edit</a>	
										</li>
										<?php if($view_as!='dosen'): ?>
											<?php if($p->is_valid=='0'||$p->is_valid=='2'){ ?>
											<li>
											<a class='btn-edit-post' onclick="set_validasi('<?php echo $p->rencana_id ?>', 'approve')" href="javascript::" ><i class='fa fa-check'></i> Approve</a>	
											</li>
											<?php }elseif($p->is_valid=='1'){ ?>
											<li>
											<a class='btn-edit-post' onclick="set_validasi('<?php echo $p->rencana_id ?>', 'reject')" href="javascript::" ><i class='fa fa-ban'></i> Reject</a>	
											</li>	
											<?php } ?>
										<?php endif; ?>
									  </ul>
									</li>
								</ul>
							</div>
						</td>
					</tr>
					<?php } ?>
					</tbody>
					</table>
					
					<?php else: ?>
					<div class="well" align="center">
						No Data Available
					</div>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
function convert_date($last_update){
	$lastupdate = explode(' ', $last_update);
	$dates  = explode('-', $lastupdate[0]);
	
	$year  = $dates[0];
	$date  = $dates[2];
	$month = '';
	
	switch ($dates[1]) {
		case '01':
			$month = 'Jan';
		break;
		case '02':
			$month = 'Feb';
		break;
		case '03':
			$month = 'March';
		break;
		case '04':
			$month = 'Apr';
		break;
		case '05':
			$month = 'May';
		break;
		case '06':
			$month = 'Juny';
		break;
		case '07':
			$month = 'July';
		break;
		case '08':
			$month = 'Aug';
		break;
		case '09':
			$month = 'Sept';
		break;
		case '10':
			$month = 'Oct';
		break;
		case '11':
			$month = 'Nov';
		break;
		case '12':
			$month = 'Dec';
		break;
	}
	
	return $month." ".$year.", ".$date;;
	// return $lastupdate[0];
}
?>
<?php $this->foot(); ?>