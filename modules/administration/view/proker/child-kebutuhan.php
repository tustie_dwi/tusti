<input type="hidden" class="form-control" name="count-kebutuhan[]" value="<?php echo count($posts) ?>" />
<?php if(isset($posts)):?>
<?php foreach ($posts as $p) { ?>
<div class="kebutuhan-form">
	<div class="form-group">
	    <label class="col-sm-2 control-label">Kebutuhan</label>
	    <div class="col-sm-7">
	      <input name="kebutuhan[]" required="required" class="form-control" type="text" value="<?php echo $p->keterangan ?>">
	      <input name="hidId3[]" required="required" class="form-control" value="<?php echo $p->kebutuhanid ?>" type="hidden">
	    </div>
	    <a href="javascript::" onclick="delete_('kebutuhan',this)" class="btn btn-danger delete-kebutuhan"><i class="fa fa-minus-circle"></i> Delete Kebutuhan</a>
	</div>
	
	<div class="form-group">
	    <label class="col-sm-2 control-label">&nbsp;</label>
	    <div class="col-sm-9">
		    <fieldset class="form-inline">
			    <div class="form-group" style="margin-left: 1px;">
				    <div class="input-group">
				      <div class="input-group-addon">Quantity</div>
				     <input name="qty[]" value="<?php echo $p->qty ?>" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-qty" type="text">
				    </div>
			    </div>
			    <div class="form-group" style="margin-left: <?php if($param==''){echo '32px;';}else{echo '19px;';} ?>">
				    <div class="input-group">
				      <div class="input-group-addon">Harga</div>
				     <input name="harga[]" value="<?php echo $p->harga ?>" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-harga" type="text">
				    </div>
			    </div>
			    <div class="form-group" style="margin-left: <?php if($param==''){echo '32px;';}else{echo '19px;';} ?>">
				    <div class="input-group">
				      <div class="input-group-addon">Total</div>
				     <input name="total[]" value="<?php echo $p->total_biaya ?>" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-total" type="text">
				    </div>
			    </div>
			</fieldset>
	    </div>
	</div>
</div>
<?php } ?>
<?php endif;?>