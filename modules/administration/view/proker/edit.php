<?php 
$this->head(); 
if($posts!=''){
	$i=0;$x=0;
	foreach ($posts as $p) {
		$rencana_id = $p->rencana_id;
		$periode = $p->periode;
		$judul = $p->judul;
		$unitid = $p->unit_id;
		$st_proses = $p->st_proses;
		$hidId = $p->rencanaid;
	}
}
?>

<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/administration/proker'); ?>">Program Kerja</a></li>
	  <?php if($param!='Edit Program Kerja'): ?>
	  	<li><a href="<?php echo $this->location('module/administration/proker/write/proker'); ?>">Write</a></li>
	  <?php else: ?>
	  	<li><a href="<?php echo $this->location('module/administration/proker/edit/proker/'.$rencana_id); ?>">Edit</a></li>
	  <?php endif; ?>
	</ol>
    <div class="breadcrumb-more-action">
    	<?php if($param=='Edit Program Kerja'): ?>
		<a href="<?php echo $this->location('module/administration/proker/write/proker'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Program Kerja</a>
   		<?php endif; ?>
    </div>
    
    
    <div class="row">
		<div class="col-md-12">				
			<form method=post name="form-proker" id="form-proker" class="form-horizontal" role="form">
				<div class="panel panel-default">
					<div class="panel-heading" id="form-box"><?php echo $param ?></div>
					<div class="panel-body ">
						
						<div class="form-group">
						    <label class="col-sm-1 control-label">Judul</label>
						    <div class="col-sm-11">
						      <input name="judul" required="required" class="form-control" value="<?php if(isset($judul))echo $judul?>" type="text">
						    </div>
					  	</div>
						
						<div class="form-group">
						    <label class="col-sm-1 control-label">Periode</label>
						    <div class="col-sm-11">
						      <input name="periode" required="required" class="form-control" value="<?php if(isset($periode))echo $periode?>" type="text">
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-1 control-label">unit</label>
						    <div class="col-sm-11">
						      	<select id="select_unit" class="e9 form-control" name="select_unit">
									<option value="-">Select Unit</option>
									<?php if(isset($unitkaryawan)) {
										foreach($unitkaryawan as $u) :
											if(in_array($u->unit_id, $unit_sess)){
												echo "<option value='".$u->unit_id."' ";
												if(isset($unitid)&&$unitid==$u->unit_id){
													echo "selected";
												}
												echo " >".$u->keterangan."</option>";
											}
										endforeach;
									} ?>
								</select>
						    </div>
					  	</div>
						
						<div class="form-group">
						    <label class="col-sm-1 control-label">Status Proses</label>
						    <div class="col-sm-11">
						      <input name="stproses" required="required" class="form-control" value="<?php if(isset($st_proses))echo $st_proses?>" type="text">
						    </div>
					  	</div>
						
						<!-- Detail -->
						<legend></legend>
						<a href="javascript::" onclick="add_parent()" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Detail</a>
						
						<ul class="pager pull-right" style="margin: 0px 0px 0px 0px; padding: 0px; 0px; 0px; 0px;">
						  <li class="previous disabled" style="margin-right: 5px;"><a href="javascript::" class="prev-detail">&larr; Previous</a></li>
						  <li class="next disabled"><a href="javascript::" class="next-detail">Next &rarr;</a></li>
						</ul>
						
						<?php if($param=='Edit Program Kerja'): /*==EDIT==*/ ?>
						<input type="hidden" name="system-type" value="edit" />
						<ul class="page-list" style="display: none;">
						<?php foreach ($detail as $dt) { 
								if(!$dt->parentid){
									if(($x+1)==1){
										echo '<li class="active">'.($x+1).'</li>';
									}else{
										echo '<li>'.($x+1).'</li>';
									}  
								$x++; 
								} 
							} ?>
						</ul>
						
						<div class="parent-form-group">
						<?php foreach ($detail as $d) {
								if(!$d->parentid){ ?>
						
							<div class="detail-parent <?php echo ($i+1) ?>" data-parent="<?php echo ($i+1) ?>" <?php if(($i+1)!=1)echo "style='display: none;'"; ?>>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label"><span class="num-identity label label-success"><?php echo ($i+1) ?></span> Detail Kegiatan</label>
								    <div class="col-sm-9">
								      <input name="kegiatan[]" required="required" class="form-control" type="text" value="<?php echo $d->kegiatan ?>">
								      <input name="hidId2[]" required="required" class="form-control" value="<?php echo $d->detailid ?>" type="hidden">
								      <input name="type[]" required="required" class="form-control" value="parent" type="hidden">
								    </div>
								    <a href="javascript::" onclick="add_child(this)" class="btn btn-info"><i class="fa fa-plus"></i></a>
								    <a style="display: none;" href="javascript::" onclick="delete_('parent',this)" class="btn btn-danger delete-parent"><i class="fa fa-minus"></i></a>
								</div>
								
								<div class="parent-kebutuhan">
								 	<?php $this->get_kebutuhan($d->detail_id,''); ?>									
								</div>
								
								<!-- <div class="form-group">
								    <label class="col-sm-2 control-label"></label>
								    <div class="col-sm-9">
								      <a href="javascript::" onclick="add_kebutuhan(this)" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Kebutuhan</a>
								    </div>
								</div> -->
								
								<ul class="child" style="list-style-type: none;">
									<?php $this->get_detail_child($d->detail_id); ?>
								</ul>
								<legend></legend>
								
							</div>
						
									
						<?php			
									$i++;
								};
							};
						?>
						</div>
						
						
						<?php else: /*==NEW==*/?> 
						<ul class="page-list" style="display: none;">
							<li class="active">1</li>
						</ul>
						
						<div class="parent-form-group">
							
							<div class="detail-parent 1" data-parent="1">	
								<div class="form-group">
								    <label class="col-sm-2 control-label"><span class="num-identity label label-success">1</span> Detail Kegiatan</label>
								    <div class="col-sm-9">
								      <input name="kegiatan[]" required="required" class="form-control" type="text">
								      <input name="type[]" required="required" class="form-control" value="parent" type="hidden">
								    </div>
								    <a href="javascript::" onclick="add_child(this)" class="btn btn-info"><i class="fa fa-plus"></i></a>
								    <a style="display: none;" href="javascript::" onclick="delete_('parent',this)" class="btn btn-danger delete-parent"><i class="fa fa-minus"></i></a>
								</div>
								
								<div class="parent-kebutuhan">
								  <input type="hidden" class="form-control" name="count-kebutuhan[]" value="0" />
								  <div class="kebutuhan-form">	
									<!-- <div class="form-group">
									    <label class="col-sm-2 control-label">Kebutuhan</label>
									    <div class="col-sm-7">
									      <input name="kebutuhan[]" required="required" class="form-control" type="text">
									    </div>
									    <a href="javascript::" onclick="delete_('kebutuhan',this)" class="btn btn-danger delete-kebutuhan"><i class="fa fa-minus-circle"></i> Delete Kebutuhan</a>
									</div>
									
									<div class="form-group">
									    <label class="col-sm-2 control-label">&nbsp;</label>
									    <div class="col-sm-9">
										    <fieldset class="form-inline">
											    <div class="form-group" style="margin-left: 1px;">
												    <div class="input-group">
												      <div class="input-group-addon">Quantity</div>
												     <input name="qty[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-qty" type="text">
												    </div>
											    </div>
											    <div class="form-group" style="margin-left: 32px;">
												    <div class="input-group">
												      <div class="input-group-addon">Harga</div>
												     <input name="harga[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-harga" type="text">
												    </div>
											    </div>
											    <div class="form-group" style="margin-left: 32px;">
												    <div class="input-group">
												      <div class="input-group-addon">Total</div>
												     <input name="total[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-total" type="text">
												    </div>
											    </div>
											</fieldset>
									    </div>
									</div> -->
								  </div>
									
								</div>
								
								<!-- <div class="form-group">
								    <label class="col-sm-2 control-label"></label>
								    <div class="col-sm-9">
								      <a href="javascript::" onclick="add_kebutuhan(this)" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Kebutuhan</a>
								    </div>
								</div> -->
								
								<ul class="child" style="list-style-type: none;">
								</ul>
								<legend></legend>
						  	</div>
					  							  		
						</div>
						<?php endif; ?>
						<!-- Detail -->
						
						<div class="form-group">
						    <label class="col-sm-1 control-label"></label>
						    <div class="col-sm-11">
						      <input type="hidden" name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>">
							  <input name="b_proker" id="b_proker" value=" Data Valid &amp; Save " class="btn btn-primary pull-right" type="submit">&nbsp;
							  <!-- <a href="<?php echo $this->location('module/administration/conf/jenis'); ?>" id="cancel-jenissurat" class="btn btn-danger" style="display: none;">Cancel</a> -->
						    </div>
					  	</div>
						
					</div>
				</div>
			</form>
		</div>
		
	</div>
    
</div>

<?php $this->foot(); ?>