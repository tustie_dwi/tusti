<?php if(isset($posts)):?>
	<?php foreach ($posts as $p) { ?>
		<?php if($parent==$p->parent_id){ ?>
		<li class="controls" style="margin-top: 5px">
			<div class="form-group">
			<label class="col-sm-2 control-label">Sub Detail Kegiatan</label>
			<div class="col-sm-9">
			<input name="kegiatan[]" value="<?php echo $p->kegiatan ?>" required="required" class="form-control" type="text">
			<input name="hidId2[]" required="required" class="form-control" value="<?php echo $p->detailid ?>" type="hidden">
			<input name="type[]" required="required" class="form-control" value="child" type="hidden">
			</div>
			<a href="javascript::" onclick="delete_('child',this)" class="btn btn-danger delete-child"><i class="fa fa-minus"></i></a>
			</div>
			
			<div class="child-kebutuhan">
				<?php $this->get_kebutuhan($p->detail_id,'li'); ?>
			</div>
			<!-- <legend></legend> -->
			<div class="form-group">
			    <label class="col-sm-2 control-label"></label>
			    <div class="col-sm-9">
			      <a href="javascript::" onclick="add_kebutuhan(this,'child')" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Kebutuhan</a>
			    </div>
			</div>
		</li>
		<?php } ?>
	<?php } ?>
<?php endif; ?>
