<?php $this->head();?>
<?php 
	$i = 0;
	foreach ($posts as $p) {
		$rencana_id = $p->rencana_id;
		$periode = $p->periode;
		$judul = $p->judul;
		$unitid = $p->unit_id;
		$unit = $p->unit;
		$namakaryawan = $p->namakaryawan;
		$last_update = $p->last_update;
		$st_proses = $p->st_proses;
		$hidId = $p->rencanaid;
		$isvalid = $p->is_valid;
	}
	
	// foreach ($detail as $d) {
		// $rowspan[] = $this->count_tr($d->detail_id,$d->kebutuhan_count);
	// }
?>
<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/administration/proker'); ?>">Program Kerja</a></li>
	  <li><a href="<?php echo $this->location('module/administration/proker/detail/proker/'.$rencana_id); ?>">Data</a></li>
	</ol>
    <div class="breadcrumb-more-action">
    	<a href="<?php echo $this->location('module/administration/proker'); ?>" class="btn btn-default"><i class="fa fa-th-list"></i> Program Kerja List</a>
    </div>
    
    
   <div class="row">
   	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body ">				
				<dl class="dl-horizontal">
					<dt>Judul</dt>
					<dd><p><?php echo $judul ?></p></dd>
					<dt>Periode</dt>
					<dd><p><?php echo $periode ?></p></dd>
					<dt>Unit</dt>
					<dd><p><?php echo $unit ?></p></dd>
					<dt>Tanggal</dt>
					<dd><p><i class="fa fa-clock-o"></i> <?php echo convert_date($last_update) ?></p></dd>
					<dt>Status</dt>
					<dd><p>
						<?php if($isvalid){ ?>
							<span class="label label-info">Approved</span>
						<?php }else{ ?>
							<span class="label label-danger">Need Approval</span>
						<?php } ?>
					</p></dd>
				</dl>
				
				<a download="<?php echo $judul ?>.xls" class="btn btn-primary" id="export" href="#" > Export table to Excel</a>
				
				<legend></legend>
				<table class="table table-bordered" id="myTable">
				<thead>
					<tr>
						<th colspan="7" style="text-align: center;">Program Kerja Unit</th>
					</tr>
					<tr>
						<th>No</th>
						<th>Program Kerja</th>
						<th>Kegiatan/Sub Kegiatan</th>
						<th>&nbsp;</th>
						<th>Kebutuhan</th>
						<th>Harga Satuan</th>
						<th>Biaya</th>
					</tr>
				</thead>
				<tbody>
				<?php 
				foreach ($detail as $d) {
				$this->get_detail_($d->detail_id, $d->kegiatan, ($i+1)); 
					$i++;
				}
				?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
   </div>
</div>
<?php 
function convert_date($last_update){
	$lastupdate = explode(' ', $last_update);
	$dates  = explode('-', $lastupdate[0]);
	
	$year  = $dates[0];
	$date  = $dates[2];
	$month = '';
	
	switch ($dates[1]) {
		case '01':
			$month = 'Jan';
		break;
		case '02':
			$month = 'Feb';
		break;
		case '03':
			$month = 'March';
		break;
		case '04':
			$month = 'Apr';
		break;
		case '05':
			$month = 'May';
		break;
		case '06':
			$month = 'Juny';
		break;
		case '07':
			$month = 'July';
		break;
		case '08':
			$month = 'Aug';
		break;
		case '09':
			$month = 'Sept';
		break;
		case '10':
			$month = 'Oct';
		break;
		case '11':
			$month = 'Nov';
		break;
		case '12':
			$month = 'Dec';
		break;
	}
	
	return $month." ".$year.", ".$date;;
	// return $lastupdate[0];
}
?>
<?php $this->foot(); ?>