<?php 
$this->head(); 
if($posts!=''){
	
}
?>

<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/administration/proker/evaluasi'); ?>">Pelaksanaan Proker</a></li>
	  <?php if($param!='Edit Pelaksanaan Proker'): ?>
	  	<li><a href="<?php echo $this->location('module/administration/proker/write/evaluasi'); ?>">Write</a></li>
	  <?php else: ?>
	  	<li><a href="<?php echo $this->location('module/administration/proker/edit/evaluasi/'.$detailid); ?>">Edit</a></li>
	  <?php endif; ?>
	</ol>
    <div class="breadcrumb-more-action">
    	<?php if($param=='Edit Pelaksanaan Proker'): ?>
		<a href="<?php echo $this->location('module/administration/proker/write/evaluasi'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Pelaksanaan Proker</a>
   		<?php endif; ?>
    </div>
    
    
    <div class="row">
		<div class="col-md-12">				
			<form method=post name="form-evaluasi" id="form-evaluasi" class="form-horizontal" role="form">
				<div class="panel panel-default">
					<div class="panel-heading" id="form-box"><?php echo $param ?></div>
					<div class="panel-body ">
						
						<div class="form-group">
						    <label class="col-sm-2 control-label">Progam Kerja</label>
						    <div class="col-sm-10">
						      	<select id="select_proker" class="form-control" name="select_proker">
									<option value="-">Select Program Kerja</option>
									<?php if(isset($proker)) {
										foreach($proker as $pk) :
											echo '<optgroup label="'.$pk->judul.'" periode="'.$pk->periode.'">';
											$this->read_detail_child($pk->rencana_id,$detailid);
											echo '</optgroup>';
										endforeach;
									} ?>
								</select>
						    </div>
					  	</div>
						
						<div class="form-group">
						    <label class="col-sm-2 control-label">Periode Baseline</label>
						    <div class="col-sm-10">
						      <input name="periode-base" required="required" class="form-control periode-baseline" value="" type="text">
						    </div>
					  	</div>
						
						<legend></legend>
						<a href="javascript::" onclick="add_kegiatan(this)" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Kegiatan</a>
						<ul class="pager pull-right" style="margin: 0px 0px 0px 0px; padding: 0px; 0px; 0px; 0px;">
						  <li class="previous disabled" style="margin-right: 5px;"><a href="javascript::" class="prev-detail">&larr; Previous</a></li>
						  <li class="next disabled"><a href="javascript::" class="next-detail">Next &rarr;</a></li>
						</ul>
						
						<div class="page-kegiatan">
							
							
							<?php 
							if($param=='Edit Pelaksanaan Proker'): 
							 echo '<ul class="page-list" style="display: none;">';
							 for($x=0;$x<count($posts);$x++) {
							  echo '<li ';
							  if(($x+1)==count($posts)){ echo 'class="active"'; }
							  echo' >'.($x+1).'</li>';
							 }
							 echo '</ul>';
							else: 
							?>
							<ul class="page-list" style="display: none;">
								<li class="active">1</li>
							</ul>
							<?php endif; ?>
							
							
							<?php 
							if($param=='Edit Pelaksanaan Proker'): 
							 $num = 0;
							 foreach ($posts as $p) {
							?>
							<div class="page-kegiatan-parent <?php echo ($num+1) ?> <?php if(($num+1)==count($posts)){ echo 'active'; } ?>" data-parent="<?php echo ($num+1) ?>" <?php if(($num+1)!=count($posts)){ echo 'style="display:none;"'; } ?>>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">
								    	<span class="num-identity label label-success"><?php echo ($num+1) ?></span>
								    	Judul
								    </label>
								    <div class="col-sm-10">
								      <input name="judul[]" required="required" class="form-control" type="text" value="<?php echo $p->judul ?>">
								      <input name="type[]" required="required" class="form-control" value="kegiatan" type="hidden">
								      <input type="hidden" name="hidId[]" value="<?php echo $p->kegiatanid ?>" class="form-control">
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Keterangan</label>
								    <div class="col-sm-10">
								      <textarea name="keterangan-kegiatan[]" required="required" class="form-control"><?php echo $p->keterangan ?></textarea>
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Rencana Mulai</label>
								    <div class="col-sm-10">
								      <input name="tglrencanamulai[]" value="<?php echo $p->tgl_rencana_mulai ?>" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Target Selesai</label>
								    <div class="col-sm-10">
								      <input name="tgltargetselesai[]" value="<?php echo $p->tgl_target_selesai ?>" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Pelaksanaan</label>
								    <div class="col-sm-10">
								      <input name="tglpelaksanaan[]" value="<?php echo $p->tgl_pelaksanaan ?>" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
							  	
							  	<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Selesai</label>
								    <div class="col-sm-10">
								      <input name="tglselesai[]" value="<?php echo $p->tgl_selesai ?>" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">&nbsp;</label>
								    <div class="col-sm-10">
								      <a href="javascript::" onclick="add_baseline(this)" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Baseline</a>
								      <a href="javascript::" onclick="add_target(this)" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Target</a>
								      <a href="javascript::" onclick="delete_(this,'kegiatan')" class="delete-kegiatan btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Kegiatan</a>
								    </div>
							  	</div>
								
								<div class="page-baseline">
									<legend></legend>
									<?php $this->get_baseline($p->kegiatan_id) ?>
								</div>
								
								<div class="page-target">
									<legend></legend>
									<?php $this->get_target($p->kegiatan_id) ?>
								</div>
								
							</div>
							<?php $num++;
							 }
							else: 
							?>
							<div class="page-kegiatan-parent 1 active" data-parent="1">
								<div class="form-group">
								    <label class="col-sm-2 control-label">
								    	<span class="num-identity label label-success">1</span>
								    	Judul
								    </label>
								    <div class="col-sm-10">
								      <input name="judul[]" required="required" class="form-control" type="text">
								      <input name="type[]" required="required" class="form-control" value="kegiatan" type="hidden">
								      <input type="hidden" name="hidId[]" value="" class="form-control">
								    </div>
							  	</div>				
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Keterangan</label>
								    <div class="col-sm-10">
								      <textarea name="keterangan-kegiatan[]" required="required" class="form-control"></textarea>
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Rencana Mulai</label>
								    <div class="col-sm-10">
								      <input name="tglrencanamulai[]" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Target Selesai</label>
								    <div class="col-sm-10">
								      <input name="tgltargetselesai[]" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
								
								<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Pelaksanaan</label>
								    <div class="col-sm-10">
								      <input name="tglpelaksanaan[]" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
							  	
							  	<div class="form-group">
								    <label class="col-sm-2 control-label">Tanggal Selesai</label>
								    <div class="col-sm-10">
								      <input name="tglselesai[]" class="form_datetime form-control" required="" type="text">
								    </div>
							  	</div>
							
								<div class="form-group">
								    <label class="col-sm-2 control-label">&nbsp;</label>
								    <div class="col-sm-10">
								      <a href="javascript::" onclick="add_baseline(this)" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Baseline</a>
								      <a href="javascript::" onclick="add_target(this)" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Target</a>
								      <a href="javascript::" onclick="delete_(this,'kegiatan')" class="delete-kegiatan btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Kegiatan</a>
								    </div>
							  	</div>
								
								<div class="page-baseline">
									<legend></legend>
									<input name="baseline-num[]" class="form-control baseline-num" value="1" type="hidden">
									<div class="page-baseline-child page-baseline-1">						
										<div class="form-group">
										    <label class="col-sm-2 control-label">
										    	<span class="num-page-baseline-child-identity label label-danger">1</span>
										    	Baseline
										    </label>
										    <div class="col-sm-10">
										      <textarea name="baseline[]" required="required" class="form-control"></textarea>
										      <input name="type[]" required="required" class="form-control" value="baseline" type="hidden">
										      <input type="hidden" name="hidId2[]" value="" class="form-control">
										    </div>
									  	</div>
										
										<div class="form-group">
										    <label class="col-sm-2 control-label">&nbsp;</label>
										    <div class="col-sm-10">
										      <a href="javascript::" onclick="delete_(this,'baseline')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Baseline</a>
										    </div>
									  	</div>
									</div>
									
								</div>	<!-- page baseline -->
								
								<div class="page-target">
									<legend></legend>
									<input name="target-num[]" class="form-control target-num" value="1" type="hidden">
									<div class="page-target-child page-target-1">
										<div class="form-group">
										    <label class="col-sm-2 control-label">
										    	<span class="num-page-target-child-identity label label-warning">1</span>
										    	Target
										    </label>
										    <div class="col-sm-10">
										      <input name="judul-target[]" class="form-control" required="" type="text">
										      <input name="type[]" required="required" class="form-control" value="target" type="hidden">
										      <input type="hidden" name="hidId3[]" value="" class="form-control">
										    </div>
									  	</div>
										
										<div class="form-group">
										    <label class="col-sm-2 control-label">Keterangan</label>
										    <div class="col-sm-10">
										      <textarea name="target[]" required="required" class="form-control"></textarea>	
										    </div>
									  	</div>
										
										<div class="form-group">
										    <label class="col-sm-2 control-label">Tanggal Rencana Mulai</label>
										    <div class="col-sm-10">
										      <input name="tglrencanamulai[]" class="form_datetime form-control" required="" type="text">
										    </div>
									  	</div>
										
										<div class="form-group">
										    <label class="col-sm-2 control-label">Tanggal Target Selesai</label>
										    <div class="col-sm-10">
										      <input name="tgltargetselesai[]" class="form_datetime form-control" required="" type="text">
										    </div>
									  	</div>
										
										<div class="form-group">
										    <label class="col-sm-2 control-label">Tanggal Pelaksanaan</label>
										    <div class="col-sm-10">
										      <input name="tglpelaksanaan[]" class="form_datetime form-control" required="" type="text">
										    </div>
									  	</div>
									  	
									  	<div class="form-group">
										    <label class="col-sm-2 control-label">Tanggal Selesai</label>
										    <div class="col-sm-10">
										      <input name="tglselesai[]" class="form_datetime form-control" required="" type="text">
										    </div>
									  	</div>
										
										<div class="form-group">
										    <label class="col-sm-2 control-label">Capaian</label>
										    <div class="col-sm-10">
										      <div class="input-group">
				      							<input name="capaian[]" onkeypress="return isNumberKey(event)" required="required" class="form-control" type="text">
												<div class="input-group-addon">%</div>
											  </div>
										    </div>
									  	</div>
										
										<div class="bukti-pendukung-group">
										<div class="form-group">
										    <label class="col-sm-2 control-label">Bukti Pendukung</label>
										    <div class="col-sm-10">
										      <span class="result-attach"></span>
										      
										      <input name="buktipendukung[]" class="form-control" type="hidden">
										      
										      <a onclick="show_modal(this, 'link')" data-original-title="Attach Link" class="btn btn-success btn-xs pull-left btn-tooltip white" data-placement="bottom" title="Attach Link" >
										      	<i class="fa fa-link"></i>
										      </a>
										      <a onclick="show_modal(this, 'library')" data-original-title="Attach Library" style="margin: 0 2px" class="white btn btn-success btn-xs pull-left btn-tooltip" data-placement="bottom" title="Open Library">
										      	<i class="fa fa-archive"></i>
										      </a>
										    </div>
									  	</div>
										
										<!-- modal library -->
										<div class="modal fade modal-library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										  <div class="modal-dialog">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
										        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive"></i> Media Library</h4>
										      </div>
										      <div class="modal-body">
										      	
										      	<div class="library-content"></div>
										        
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="close-button btn btn-default" data-dismiss="modal">Close</button>
										        <a type="button" class="back-button btn btn-primary pull-left" onclick="back_btn_(this)"><i class="fa fa-arrow-circle-left"></i> Back</a>
										        <a type="button" class="home-button btn btn-primary pull-left" onclick="home_btn_(this)"><i class="fa fa-home"></i> Home</a>
										      </div>
										    </div>
										  </div>
										</div>
										<!-- modal library -->
										
										<!-- modal link -->
										<div class="modal fade modal-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										  <div class="modal-dialog">
										    <div class="modal-content">
										      <div class="modal-header">
										        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
										        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-link"></i> Attach Link</h4>
										      </div>
										      <div class="modal-body">
										        <input name="buktipendukung-link[]" class="form-control" type="text" onclick="sethttp(this)">
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										        <button type="button" class="btn btn-primary" onclick="attach_(this)"><i class="fa fa-check"></i> Attach</button>
										      </div>
										    </div>
										  </div>
										</div>
										<!-- modal link -->
										</div>
										
										<div class="form-group">
										    <label class="col-sm-2 control-label">Kendala</label>
										    <div class="col-sm-10">
										      <textarea name="kendala[]" required="required" class="form-control"></textarea>
										    </div>
									  	</div>	
									  	
									  	<div class="form-group">
										    <label class="col-sm-2 control-label">&nbsp;</label>
										    <div class="col-sm-10">
										      <a href="javascript::" onclick="delete_(this,'target')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Target</a>
										    </div>
									  	</div>
									  							
									</div>									
								</div>  <!-- page target -->
								
							</div> 
							
							<?php endif; ?>
							
						</div> <!-- page kegiatan -->
						
						<legend></legend>					
						<div class="form-group">
						    <label class="col-sm-2 control-label"></label>
						    <div class="col-sm-10">
						      <?php if($param=='Edit Pelaksanaan Proker'): ?>
						      <input type="hidden" name="page-type" value="edit">
						      <?php endif; ?>
							  <!-- <input name="b_evaluasi" id="b_evaluasi" value=" Data Valid &amp; Save " class="btn btn-primary pull-right" type="submit">&nbsp; -->
							  <a href="javascript::" name="b_evaluasi" id="b_evaluasi" class="btn btn-primary pull-right">Data Valid &amp; Save</a>
							  <!-- <a href="<?php echo $this->location('module/administration/conf/jenis'); ?>" id="cancel-jenissurat" class="btn btn-danger" style="display: none;">Cancel</a> -->
						    </div>
					  	</div>
						
					</div>
				</div>
			</form>
		</div>
		
	</div>
    
</div>

<?php $this->foot(); ?>