<?php $this->head();?>
<?php 
	$data = Array();
	$data[0] = 1; //--no
	$data[1] = 0; //--print
?>
<style>th{text-align: center;}</style>
<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/administration/proker/evaluasi'); ?>">Program Kerja</a></li>
	  <li><a href="<?php echo $this->location('module/administration/proker/detail/evaluasi/'.$detailid); ?>">Data</a></li>
	</ol>
    <div class="breadcrumb-more-action">
    	<a href="<?php echo $this->location('module/administration/proker/evaluasi'); ?>" class="btn btn-default"><i class="fa fa-th-list"></i> Pelaksanaan Program Kerja List</a>
    </div>
    
    
   <div class="row">
   	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body ">					
				<a download="<?php echo $judul ?>.xls" class="btn btn-primary" id="export" href="#" > Export table to Excel</a>
				
				<legend></legend>
				<h3><?php echo $judul ?></h3>
				<table class="table table-bordered" id="myTable">
				<thead>
					<tr>
						<th rowspan="2">Program Kerja</th>
						<th rowspan="2">No</th>
						<th rowspan="2">Kegiatan yang telah dilakukan</th>
						<th colspan="3">Evaluasi target</th>
						<th colspan="2">Keterangan</th>
					</tr>
					<tr>
						<th>Baseline</th>
						<th>Target</th>
						<th>capaian</th>
						<th>Bukti pendukung</th>
						<th>Kendala</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php 
						foreach ($posts as $p) {
							$data = $this->get_detail_evaluasi_($p->kegiatan_id, $p->judul, $judul, $data); 
						}
						?>
					</tr>
				</tbody>
				</table>
			</div>
		</div>
	</div>
   </div>
</div>
<?php 
function convert_date($last_update){
	$lastupdate = explode(' ', $last_update);
	$dates  = explode('-', $lastupdate[0]);
	
	$year  = $dates[0];
	$date  = $dates[2];
	$month = '';
	
	switch ($dates[1]) {
		case '01':
			$month = 'Jan';
		break;
		case '02':
			$month = 'Feb';
		break;
		case '03':
			$month = 'March';
		break;
		case '04':
			$month = 'Apr';
		break;
		case '05':
			$month = 'May';
		break;
		case '06':
			$month = 'Juny';
		break;
		case '07':
			$month = 'July';
		break;
		case '08':
			$month = 'Aug';
		break;
		case '09':
			$month = 'Sept';
		break;
		case '10':
			$month = 'Oct';
		break;
		case '11':
			$month = 'Nov';
		break;
		case '12':
			$month = 'Dec';
		break;
	}
	
	return $month." ".$year.", ".$date;;
	// return $lastupdate[0];
}
?>
<?php $this->foot(); ?>