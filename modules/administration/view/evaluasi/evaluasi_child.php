<?php if(isset($posts)): ?>
	<?php foreach ($posts as $p) { ?>
	<div class="col-md-8">	
		<b>Evaluasi : </b><?php echo $p->judul ?> <br>
			<blockquote><p>
				<?php echo $p->keterangan ?><br>
				<small>Tgl rencana mulai : <?php echo convert_date($p->tgl_rencana_mulai) ?><br></small>
				<small>Tgl target selesai : <?php echo convert_date($p->tgl_target_selesai) ?><br></small>
				<small>Tgl pelaksanaan : <?php echo convert_date($p->tgl_pelaksanaan) ?><br></small>
				<small>Tgl selesai : <?php echo convert_date($p->tgl_selesai) ?></small>
			</p></blockquote>
		</div>
		<div class="col-md-2">
			<?php if($p->is_valid=='1'){ ?>
				<span class="label label-info">Approved</span>
			<?php }elseif($p->is_valid=='2'){ ?>
				<span class="label label-danger">Rejected</span>
			<?php }else{ ?>
				<span class="label label-warning">Need Approval</span>
			<?php } ?>
		</div>
		<div class="col-md-2">
			<?php if($view_as!='dosen'): ?>
				<?php if($p->is_valid=='0'||$p->is_valid=='2'){ ?>
				<a class='btn-edit-post' onclick="set_validasi('<?php echo $p->kegiatan_id ?>', 'approve')" href="javascript::" ><i class='fa fa-check'></i> Approve</a>
				<?php }elseif($p->is_valid=='1'){ ?>	
				<a class='btn-edit-post' onclick="set_validasi('<?php echo $p->kegiatan_id ?>', 'reject')" href="javascript::" ><i class='fa fa-ban'></i> Reject</a>	
				<?php } ?>
			<?php endif; ?>
		</div>
	<?php } ?>
<?php endif;?>