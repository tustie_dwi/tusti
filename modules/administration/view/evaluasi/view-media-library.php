<input type="hidden" name="parent-folder-current" value="<?php echo $folder_parent ?>" />
<input type="hidden" name="count-folder" value="<?php echo $count_folder ?>" />
<?php if(isset($file)): ?>
	<?php foreach ($file as $f) { ?>
		
		<?php if($f->jenis_file=='image'){ ?>
		<div class="col-md-12">
			<h3><?php echo $f->file_name ?> 
				<a onclick="doDownload('<?php echo $f->file_id ?>')" href="<?php echo $this->location('module/administration/proker/download/'.$f->file_id);  ?>" class="btn btn-default">
				  <i class="fa fa-download"></i> Download This File
				</a> &nbsp;
				<a onclick="select_(this, '<?php echo $f->fileid ?>','<?php echo $f->file_name ?>')" class="btn btn-default">
				  <i class="fa fa-check-square-o"></i> Insert This File
				</a>	
			</h3>		
		</div>
		
		<div class="col-md-12">			
			<img src="<?php echo $this->config->file_url_view."/".$f->file_loc ?>" class="thumbnail">								
		</div>
		
		<?php  }elseif($f->jenis_file=='document'||$f->jenis_file=='presentation'||$f->jenis_file=='spreadsheet'){ ?>
		<div class="col-md-12">
			<h3><?php echo $f->file_name ?> 
				<a onclick="doDownload('<?php echo $f->file_id ?>')" href="<?php echo $this->location('module/administration/proker/download/'.$f->file_id);  ?>" class="btn btn-default">
				  <i class="fa fa-download"></i> Download This File</a> &nbsp;
				<a onclick="select_(this, '<?php echo $f->fileid ?>','<?php echo $f->file_name ?>')" class="btn btn-default">
				  <i class="fa fa-check-square-o"></i> Insert This File
				</a>	
			</h3>		
		</div>
		
		<div class="col-md-12">			
			<iframe src="//docs.google.com/viewer?url=<?php echo $this->asset($f->file_loc) ?>&embedded=true" width="550" height="780" style="border: none;"></iframe>								
		</div>
		<?php } ?>
	<?php } ?>
<?php endif; ?>