<input type="hidden" name="parent-folder-current" value="<?php echo $folder_parent ?>" />
<input type="hidden" name="count-folder" value="<?php echo $count_folder ?>" />
<?php if(isset($folder)||isset($file)): ?>
	<table class="table table-hover example">
		<thead>
			<tr>
				<th><small>Name</small></th>
				<th><small>Size</small></th>
				<th><small>Last Modified</small></th>
				<th class="library_attach"></th>
			</tr>
		</thead>
		<tbody>
		<?php if(isset($folder)): ?>
		<?php foreach ($folder as $f) { ?>
			<tr>
				<td>
					<span class='span' style="cursor: pointer" onclick="view_content(this,'<?php echo $f->folder_id ?>','<?php echo $count_folder ?>')">
						<i class="fa fa-folder-o"></i> <?php echo $f->folder_name ?>
					</span>
				</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		<?php } ?>
		<?php endif; ?>
		
		<?php if(isset($file)): ?>
		<?php foreach ($file as $fl) { ?>
			<tr>
				<td>
					<span class='span' style="cursor: pointer">
						<?php if($fl->jenis_file == 'document'){ ?>
							<i class="fa fa-file-text-o"></i>&nbsp;
						<?php }else if ($fl->jenis_file == 'image'){ ?>
							<i class="fa fa-picture-o"></i>&nbsp;
						<?php } ?>
						<a href="javascript::" onclick="get_content(this,'<?php echo $fl->file_id ?>','<?php echo $folder_parent ?>','<?php echo $count_folder ?>')">
							<span class="text text-default">
							  <?php echo $fl->file_name ?>
							</span>
						</a>
					</span>
				</td>
				<td><small><?php echo formatSizeUnits($fl->file_size) ?></small></td>
				<td><small><i class="fa fa-clock-o"></i>&nbsp;<?php echo $fl->last_update ?></small></td>
				<td class="library_attach">
					<span onclick="select_(this, '<?php echo $fl->fileid ?>','<?php echo $fl->file_name ?>')"><i class="fa fa-check-square-o" style="cursor: pointer"></i></span>
				</td>
			</tr>
		<?php } ?>
		<?php endif; ?>
		</tbody>
	</table>
<?php else: ?>
	No data show.
<?php endif; ?>

<?php 
function formatSizeUnits($bytes){
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}
?>