<?php $i=0; ?>
<input name="baseline-num[]" class="form-control baseline-num" value="<?php echo count($posts) ?>" type="hidden">
<?php if (isset($posts)): ?>
<?php foreach ($posts as $p) { ?>
	<div class="page-baseline-child page-baseline-<?php echo ($i+1) ?>">						
		<div class="form-group">
		    <label class="col-sm-2 control-label">
		    	<span class="num-page-baseline-child-identity label label-danger"><?php echo ($i+1) ?></span>
		    	Baseline
		    </label>
		    <div class="col-sm-10">
		      <textarea name="baseline[]" required="required" class="form-control"><?php echo $p->judul ?></textarea>
		      <input name="type[]" required="required" class="form-control" value="baseline" type="hidden">
		      <input type="hidden" name="hidId2[]" value="<?php echo $p->baselineid ?>" class="form-control">
		    </div>
	  	</div>
		
		<div class="form-group">
		    <label class="col-sm-2 control-label">&nbsp;</label>
		    <div class="col-sm-10">
		      <a href="javascript::" onclick="delete_(this,'baseline')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Baseline</a>
		    </div>
	  	</div>
	</div>
<?php $i++; } ?>
<?php endif; ?>