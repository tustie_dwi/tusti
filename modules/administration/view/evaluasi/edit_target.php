<?php $i=0; ?>
<input name="target-num[]" class="form-control target-num" value="<?php echo count($posts) ?>" type="hidden">
<?php if (isset($posts)): ?>
<?php foreach($posts as $p){ ?>
	<div class="page-target-child page-target-<?php echo ($i+1) ?>">
		<div class="form-group">
		    <label class="col-sm-2 control-label">
		    	<span class="num-page-target-child-identity label label-warning"><?php echo ($i+1) ?></span>
		    	Target
		    </label>
		    <div class="col-sm-10">
		      <input name="judul-target[]" class="form-control" required="" type="text" value="<?php echo $p->judul ?>">
		      <input name="type[]" required="required" class="form-control" value="target" type="hidden">
		      <input type="hidden" name="hidId3[]" value="<?php echo $p->kegiatanid ?>" class="form-control">
		    </div>
	  	</div>
		
		<div class="form-group">
		    <label class="col-sm-2 control-label">Keterangan</label>
		    <div class="col-sm-10">
		      <textarea name="target[]" required="required" class="form-control"><?php echo $p->keterangan ?></textarea>	
		    </div>
	  	</div>
		
		<div class="form-group">
		    <label class="col-sm-2 control-label">Tanggal Rencana Mulai</label>
		    <div class="col-sm-10">
		      <input name="tglrencanamulai[]" value="<?php echo $p->tgl_rencana_mulai ?>" class="form_datetime form-control" required="" type="text">
		    </div>
	  	</div>
		
		<div class="form-group">
		    <label class="col-sm-2 control-label">Tanggal Target Selesai</label>
		    <div class="col-sm-10">
		      <input name="tgltargetselesai[]" value="<?php echo $p->tgl_target_selesai ?>" class="form_datetime form-control" required="" type="text">
		    </div>
	  	</div>
		
		<div class="form-group">
		    <label class="col-sm-2 control-label">Tanggal Pelaksanaan</label>
		    <div class="col-sm-10">
		      <input name="tglpelaksanaan[]" value="<?php echo $p->tgl_pelaksanaan ?>" class="form_datetime form-control" required="" type="text">
		    </div>
	  	</div>
	  	
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Tanggal Selesai</label>
		    <div class="col-sm-10">
		      <input name="tglselesai[]" value="<?php echo $p->tgl_selesai ?>" class="form_datetime form-control" required="" type="text">
		    </div>
	  	</div>
		
		<div class="form-group">
		    <label class="col-sm-2 control-label">Capaian</label>
		    <div class="col-sm-10">
		      <div class="input-group">
				<input name="capaian[]" onkeypress="return isNumberKey(event)" value="<?php echo $p->capaian ?>" required="required" class="form-control" type="text">
				<div class="input-group-addon">%</div>
			  </div>
		    </div>
	  	</div>
		
		<div class="bukti-pendukung-group">
		<div class="form-group">
		    <label class="col-sm-2 control-label">Bukti Pendukung</label>
		    <div class="col-sm-10">
		      <span class="result-attach">
		      	<?php
		      		$http = explode('/', $p->doc_pendukung);
					if($http[0]=='http:'){
						echo '<a href="'.$p->doc_pendukung.'" class="btn btn-info btn-xs" style="margin-bottom: 2px;" target="_blank">'.$p->doc_pendukung.'</a><br>';
					}else{
						echo '<span class="btn btn-info btn-xs" style="margin-bottom: 2px;" >'.$p->file_name.'</span><br>';
					};
		      	?>
		      </span>
		      
		      <input name="buktipendukung[]" class="form-control" type="hidden" value="<?php echo $p->doc_pendukung ?>">
		      
		      <a onclick="show_modal(this, 'link')" data-original-title="Attach Link" class="btn btn-success btn-xs pull-left btn-tooltip white" data-placement="bottom" title="Attach Link" >
		      	<i class="fa fa-link"></i>
		      </a>
		      <a onclick="show_modal(this, 'library')" data-original-title="Attach Library" style="margin: 0 2px" class="white btn btn-success btn-xs pull-left btn-tooltip" data-placement="bottom" title="Open Library">
		      	<i class="fa fa-archive"></i>
		      </a>
		    </div>
	  	</div>
		
		<!-- modal library -->
		<div class="modal fade modal-library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive"></i> Media Library</h4>
		      </div>
		      <div class="modal-body">
		      	
		      	<div class="library-content"></div>
		        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <a type="button" class="btn btn-primary pull-left" onclick="folder_back(this)"><i class="fa fa-home"></i> Home</a>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- modal library -->
		
		<!-- modal link -->
		<div class="modal fade modal-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-link"></i> Attach Link</h4>
		      </div>
		      <div class="modal-body">
		        <input name="buktipendukung-link[]" class="form-control" type="text" onclick="sethttp(this)">
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" onclick="attach_(this)"><i class="fa fa-check"></i> Attach</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- modal link -->
		</div>
		
		<div class="form-group">
		    <label class="col-sm-2 control-label">Kendala</label>
		    <div class="col-sm-10">
		      <textarea name="kendala[]" required="required" class="form-control"><?php echo $p->kendala ?></textarea>
		    </div>
	  	</div>	
	  	
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">&nbsp;</label>
		    <div class="col-sm-10">
		      <a href="javascript::" onclick="delete_(this,'target')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Target</a>
		    </div>
	  	</div>					
	</div>
<?php $i++; } ?>
<?php endif; ?>