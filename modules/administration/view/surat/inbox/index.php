<?php $this->head(); ?>
<h2 class="title-page">Kotak Masuk</h2>
<div class="row">
	<div class="col-md-12"><!--LIST-->
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/administration/surat'); ?>">Surat</a></li>
		  <li><a href="<?php echo $this->location('module/administration/surat/inbox'); ?>">Kotak Masuk</a></li>
		</ol>
	
		<div class="block-box">
			<div class="content">
				<?php if(isset($posts)&&$posts!==""): ?>
				<table class='table table-hover surat_table example'>
					<thead>
						<tr>
							<th>Detail</th>	
							<th>&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
						<?php 
							   foreach ($posts as $p) { 
							   ?>
								<tr>
									<td>
										<?php
											echo "<b>Dari : ".ucwords($p->dari_nama)."</b><br>";
											echo ucwords($p->perihal)."&nbsp;";
											echo "<code><small>".$p->sifat_surat."</small></code>&nbsp;<label class='label label-success'>".$p->jenis_surat."</label><br>";
											echo "<i class='fa fa-calendar-o'></i>&nbsp;".date("d F Y",strtotime($p->tgl_surat))."<br>"; 
										?>
									</td>
									<td>
										<ul class='nav nav-pills'>
											<li class='dropdown pull-right'>
											  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
											  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
												<li>
													<a class='btn-edit-post' href="<?php echo $this->location('module/administration/surat/display/'.$p->surat_id); ?>"><i class='fa fa-search'></i> Lihat</a>
												</li>
											  </ul>
											</li>
										</ul>
									</td>
								</tr>
								<?php } ?>
					</tbody>
				</table>
				<?php else: ?>
					<div class="well" align="center">
						No Data Available
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>	
</div>
<?php $this->foot(); ?>