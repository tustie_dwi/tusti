<?php $this->head();
if(isset($posts_single)){
	$kepada_id="";
	$kirim_id="";
	foreach($posts as $ps){
		$kepada_id .= $ps->kepada_id.",";
		if($ps->kirim_id!=""){
			$kirim_id .= $ps->kirim_id.",";
		}
	}
	$kepada_id = substr($kepada_id, 0, -1);
	$kirim_id = substr($kirim_id, 0, -1);
}

if(isset($id)){
	$param = "Edit";
} else $param = "Write";

// echo $posts_single->fakultas_karyawan_id;
?>
<h2 class="title-page">Surat Menyurat</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/administration/surat'); ?>">Surat Menyurat</a></li>
  <li><a href="#"><?php echo $param ?></a></li>
</ol>
<div class="breadcrumb-more-action">
	<?php if(isset($posts_single) && $posts_single->suratid !=""){	?>
	<a href="<?php echo $this->location('module/administration/surat/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Tulis Surat Baru</a>
	<?php } ?>
</div>
<form method="post" id="form-surat"> 
	 <div class="row">
		 <div class="col-md-4">
		 	<div class="panel panel-default">
			  <div class="panel-heading">Kepada</div>
				<div class="panel-body">
					<input type="hidden" name="count_kepada" class="form-control" value="<?php if(isset($posts) && $posts != "") echo count($posts); ?>"/>
					<div class="form-group">
						<label class="control-label">Tujuan Surat</label>
						<select class="form-control" name="tujuan" id="tujuan_surat">
							<option value="0">Silahkan Pilih</option>
							<option value="kedalam" <?php if(isset($posts_single) && $posts_single->tujuan_surat=="kedalam") echo "selected"; ?>>Kedalam</option>
							<option value="keluar" <?php if(isset($posts_single) && $posts_single->tujuan_surat=="keluar") echo "selected"; ?>>Keluar</option>
						</select>
					</div>
					<div class="form-group" id="kepada">
						<label class="control-label">Kepada</label>
						<div class="controls" id="dalam">
							<label class="radio-inline">
						     	<input type="radio" name="kepada" value="karyawan" <?php if(isset($posts_single) && $posts_single->karyawan_id!="") echo 'checked="checked"'; else if(!isset($posts_single)) echo 'checked="checked"';?>/> Karyawan
						    </label>
						    <label class="radio-inline">
						     	<input type="radio" name="kepada" value="mahasiswa" <?php if(isset($posts_single) && $posts_single->mahasiswa_id!="") echo 'checked="checked"' ?>/> Mahasiswa
						    </label>
							<div class="form-group">
								<select class="form-control" name="fakultas">
									<option value="0">Silahkan Pilih Fakultas</option>
									<?php
									foreach($fakultas as $f){ 
										echo "<option value='".$f->hid_id."'";
										if(isset($posts_single) && $posts_single->fakultas_karyawan_id == $f->hid_id){
											echo "selected";
										}
										echo ">".$f->keterangan."</option>";
									} 
									?>
								</select>
								<select class="form-control" name="prodi" style="display: none" <?php if(isset($posts_single) && $posts_single->mahasiswa_id!="") echo ""; else {?>disabled="disabled"<?php } ?>>
									<option value="0">Silahkan Pilih Program Studi</option>
								</select>
							</div>
							<div class="form-group">
								<select class="form-control" name="cabang" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled" <?php } ?>>
									<option value="0">Silahkan Pilih Cabang</option>
									<?php
									foreach($cabang as $c){ 
										echo "<option value='".$c->cabang_id."'";
										if(isset($posts_single) && $posts_single->cabang_karyawan == $c->cabang_id){
											echo "selected";
										}
										echo ">".$c->keterangan."</option>";
									} 
									?>
								</select>
							</div>
						    <select multiple class="form-control e9" name="karyawan[]" id="karyawan" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled" <?php } ?>></select>
					    	<select multiple class="form-control e9" name="mhs[]" id="mhs" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled" <?php } ?>></select>
					    </div>
					    <div class="controls" id="luar">
					    	<?php if(isset($posts_single) && $posts_single->tujuan_surat=="keluar"){
					    			foreach($posts as $ps){?>
					    				<div class='form-group'>
					    					<input type='text' class='form-control' name='nama[]' placeholder='Masukkan Nama' value="<?php echo $ps->kepada ?>"/>
					    					<input type='text' class='form-control instansi typeahead' name='instansi[]' placeholder='Masukkan Nama Instansi' value="<?php echo $ps->instansi_surat ?>">
				    					</div>
					    	<?php	}
					    		  } ?>
					    </div>
					    <input type='hidden' class='form-control' name='kepada_id' value="<?php  if(isset($posts_single)) echo $kepada_id ?>">
					</div>
				</div>
			</div>
		 </div>
		 <div class="col-md-8">
		 	<div class="panel panel-default">
			  <div class="panel-heading">Data Surat</div>
				<div class="panel-body">
					<input type="hidden" name="periode" class="form-control" value="<?php echo date("Ym"); ?>"/>
					<input type="hidden" name="urut" class="form-control" value="<?php if(!isset($posts_single)) echo ($urut->urut); else{ echo $posts_single->no_urut; } ?>"/>
					<div class="form-group">
						<label class="control-label">Kategori Surat</label>
						<select class="form-control" name="kategori">
							<option value="0">Silahkan Pilih</option>
							<option value="masuk" <?php if(isset($posts_single) && $posts_single->kategori_surat=="masuk") echo "selected"; ?>>Surat Masuk</option>
							<option value="keluar" <?php if(isset($posts_single) && $posts_single->kategori_surat=="keluar") echo "selected"; ?>>Surat Keluar</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Dari</label>
						<div class="controls">
							<label class="radio-inline">
						     	<input type="radio" name="from" value="dalam"  <?php if(isset($posts_single) && $posts_single->dari_id=="") echo ""; else {?> checked="checked" <?php } ?>/> Dalam
						    </label>
						    <label class="radio-inline">
						     	<input type="radio" name="from" value="luar" <?php if(isset($posts_single) && $posts_single->dari_id=="") echo "checked='checked'"; ?>/> Luar
						    </label>
						    <div class="form-group" id="from_dalam">
								<select class="form-control" name="from_fakultas">
									<option value="0">Silahkan Pilih Fakultas</option>
									<?php
									foreach($fakultas as $f){ 
										echo "<option value='".$f->hid_id."'>".$f->keterangan."</option>";
									} 
									?>
								</select>
								<select class="form-control" name="from_cabang" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled" <?php } ?>>
									<option value="0">Silahkan Pilih Cabang</option>
									<?php
									foreach($cabang as $c){ 
										echo "<option value='".$c->cabang_id."'>".$c->keterangan."</option>";
									} 
									?>
								</select>
							</div>
					    </div>
						<input type="text" name="dari" class="form-control typeahead" id="dari" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled"<?php } ?> placeholder="Masukkan Nama Karyawan Lalu Tekan Enter"/>
						<input type="hidden" name="dari_id" class="form-control" id="dari_id" value="<?php if(isset($posts_single)) echo $posts_single->dari_id ?>"/>
						<!-- <input type="text" name="dari_id_all" class="form-control" id="dari_id_all"/> -->
						<input type="text" name="instansi_from" class="form-control" id="instansi_from" style="display: none" placeholder="Masukkan Nama Instansi"/>
						<input type="hidden" name="surat_id_tag" class="form-control" value="<?php if(isset($posts_single)) echo $posts_single->surat_id ?>"/>
					</div>
					<div class="form-group" id="pengolah_masuk">
						<label class="control-label">Pengolah Surat</label>
					    <div class="form-group">
							<select class="form-control" name="pengolah_fakultas">
								<option value="0">Silahkan Pilih Fakultas</option>
								<?php
								foreach($fakultas as $f){ 
									echo "<option value='".$f->hid_id."'>".$f->keterangan."</option>";
								} 
								?>
							</select>
							<select class="form-control" name="pengolah_cabang" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled" <?php } ?>>
								<option value="0">Silahkan Pilih Cabang</option>
								<?php
								foreach($cabang as $c){ 
									echo "<option value='".$c->cabang_id."'>".$c->keterangan."</option>";
								} 
								?>
							</select>
						</div>
						<div class="form-group">
							<input type="text" name="pengolah" class="form-control typeahead" id="pengolah" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled" <?php } ?> placeholder="Masukkan Nama Karyawan Lalu Tekan Enter"/>
							<input type="hidden" name="pengolah_id" class="form-control" id="pengolah_id" value="<?php if(isset($posts_single)) echo $posts_single->pengolah_id ?>"/>
							<!-- <input type="hidden" name="pengolah_id_all" class="form-control" id="pengolah_id_all"/> -->
						</div>
						<div class="form-group">
							<label class="control-label">Tanggal Surat Dikirim</label>
							<input type="text" name="tanggal_surat_kirim" class="form-control form_datetime" value="<?php if(isset($posts_single) && $posts_single->tgl_kirim!="") echo $posts_single->tgl_kirim ?>"/>
						</div>
						<div class="form-group">
							<label class="control-label">File Bukti Kirim/Terima</label>
							<label class="label label-info file_kirim"></label>
							<input type="file" name="file_kirim" class="form-control"/>
							<input type="hidden" name="file_kirim_loc" class="form-control" value="<?php if(isset($posts_single) && $posts_single->file_loc_kirim!="") echo $posts_single->file_loc_kirim?>"/>
						</div>
					</div>
					<div class="form-group" id="pengirim_masuk">
						<label class="control-label">Pengantar Surat</label>
					    <div class="form-group">
							<select class="form-control" name="pengantar_fakultas">
								<option value="0">Silahkan Pilih Fakultas</option>
								<?php
								foreach($fakultas as $f){ 
									echo "<option value='".$f->hid_id."'>".$f->keterangan."</option>";
								} 
								?>
							</select>
							<select class="form-control" name="pengantar_cabang">
								<option value="0">Silahkan Pilih Cabang</option>
								<?php
								foreach($cabang as $c){ 
									echo "<option value='".$c->cabang_id."'>".$c->keterangan."</option>";
								} 
								?>
							</select>
						</div>
						<input type="text" name="pengantar" class="form-control typeahead" id="pengantar" placeholder="Masukkan Nama Karyawan Lalu Tekan Enter"/>
						<input type="hidden" name="pengantar_id" class="form-control" id="pengantar_id" value="<?php if(isset($posts_single) && $posts_single->kuririd!="") echo $posts_single->kuririd ?>"/>
						<!-- <input type="hidden" name="pengantar_id_all" class="form-control" id="pengantar_id_all"/> -->
						<input type="hidden" name="pengantar-hide" value="<?php if(isset($posts_single) && $posts_single->kirim_oleh!="") echo $posts_single->kirim_oleh ?>"/>
						<input type="hidden" name="kirim_id" value="<?php if(isset($posts_single)) echo $kirim_id ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Tanggal Surat</label>
						<input type="text" name="tanggal_surat" class="form-control form_datetime" value="<?php if(isset($posts_single) && $posts_single->tgl_surat!="") echo $posts_single->tgl_surat ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Jenis Surat</label>
						<select class="form-control" name="jenis">
							<option value="0">Silahkan Pilih</option>
							<?php
								foreach($jenis as $js){
									echo "<option value='".$js->jenis_surat."'";
									if(isset($posts_single) && $posts_single->jenis_surat==$js->jenis_surat){
										echo "selected";
									}
									echo ">".$js->keterangan."</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Index Perihal Surat</label>
						<select class="form-control" name="index_perihal">
							<option value="0">Silahkan Pilih</option>
							<?php
								foreach($perihal as $pr){
									echo "<option value='".$pr->perihal_surat."'";
									if(isset($posts_single) && $posts_single->perihal_surat==$pr->perihal_surat){
										echo "selected";
									}
									echo ">".$pr->perihal_surat." - ".$pr->keterangan."</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Perihal</label>
						<input type="text" name="perihal" class="form-control" value="<?php if(isset($posts_single) && $posts_single->perihal!="") echo $posts_single->perihal ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Sifat Surat</label>
						<input type="text" name="sifat" class="form-control" value="<?php if(isset($posts_single) && $posts_single->sifat_surat!="") echo $posts_single->sifat_surat ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Derajat Surat</label>
						<input type="text" name="derajat" class="form-control" value="<?php if(isset($posts_single) && $posts_single->derajat_surat!="") echo $posts_single->derajat_surat ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Unit Kerja Surat</label>
						<select class="form-control" name="unit_kerja" <?php if((isset($posts_single) && $posts_single->suratid=="") || !isset($posts_single)){ ?>disabled="disabled"<?php } ?>>
					    	<option value="0">Silahkan Pilih</option>
							<?php
								foreach($unit_kerja as $u){
									echo "<option value='".$u->unit_kerja_surat."'";
									if(isset($posts_single) && $posts_single->unit_kerja_surat==$u->unit_kerja_surat){
										echo "selected";
									}
									echo ">".$u->keterangan."</option>";
								}
							?>
					    </select>
				    </div>
					<div class="form-group">
						<label class="control-label">No Surat</label>
						<input type="text" name="nosurat" class="form-control" value="<?php if(isset($posts_single) && $posts_single->no_surat!="") echo $posts_single->no_surat ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Template Surat</label>
						<select class="form-control" name="template">
					    	<option value="0">Silahkan Pilih</option>
							<?php
								foreach($template as $t){
									echo "<option value='".$t->temp_id."'";
									echo ">".$t->jenis_surat."</option>";
								}
							?>
					    </select>
					</div>
					<div class="form-group">
						<label class="control-label">Isi Surat</label>
						<textarea class="form-control ckeditor" name="isi_surat" id="isi_surat" style="height:300px;"><?php if(isset($posts_single) && $posts_single->isi_surat!="") echo $posts_single->isi_surat ?></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Lampiran</label>
						<input type="text" name="lampiran" class="form-control" value="<?php if(isset($posts_single) && $posts_single->lampiran!="") echo $posts_single->lampiran?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">File Lampiran</label>
						<label class="label label-info file_lampiran"></label>
						<input type="file" name="files" class="form-control"/>
						<input type="hidden" name="file_loc" class="form-control" value="<?php if(isset($posts_single) && $posts_single->file_loc!="") echo $posts_single->file_loc?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Status Proses</label>
						<input type="text" name="status_proses" class="form-control" value="<?php if(isset($posts_single) && $posts_single->status_proses!="") echo $posts_single->status_proses?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Keterangan</label>
						<textarea class="form-control" name="keterangan" id="keterangan"><?php if(isset($posts_single) && $posts_single->keterangan!="") echo $posts_single->keterangan ?></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Is Dinas ?</label>
						<label class="checkbox"><input type="checkbox" name="dinas" value="1" <?php if(isset($posts_single) && $posts_single->is_dinas==1) echo "checked"; ?>> Ya</label>
					</div>
					<div class="form-group">
						<input type="hidden" class="form-control" name="hidId" value="<?php if(isset($posts_single)) echo $posts_single->suratid ?>"/>
						<input type="submit" class="btn btn-primary" value="Submit"/>
						<a href="<?php if(isset($posts_single)) echo $this->location('module/administration/surat/edit/'.$posts_single->surat_id); else echo $this->location('module/administration/surat/write'); ?>" class="btn btn-danger"/>Reset</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<?php $this->foot(); ?>