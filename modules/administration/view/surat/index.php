<?php $this->head(); ?>
<h2 class="title-page">Surat Menyurat</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/administration/surat'); ?>">Surat Menyurat</a></li>
</ol>
<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/administration/surat/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Tulis Surat Baru</a>
</div>

<div class="row">
  <div class="col-md-12">
	<div class="block-box">
		<div class="content">
			<form method="post" id="form-kategori-index" action="<?php echo $this->location("module/administration/surat"); ?>">
				<div class="form-group">
					<label class="control-label">Kategori Surat</label>								
					<select name="kategori_index" class="form-control">
						<option value="0" <?php if(isset($kategori_index)&&$kategori_index=="0") echo "selected"?>>Silahkan Pilih Kategori Surat</option>
						<option value="masuk"<?php if(isset($kategori_index)&&$kategori_index=="masuk") echo "selected"?>>Surat Masuk</option>
						<option value="keluar"<?php if(isset($kategori_index)&&$kategori_index=="keluar") echo "selected"?>>Surat Keluar</option>
					</select>
				</div>
			</form>
			<?php if(isset($posts)&&$posts!==""): ?>
				<table class='table table-hover example'>
					<thead>
						<tr>
							<th>Detail</th>	
							<th>&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
					<?php foreach ($posts as $p) { ?>
					<tr>
						<td>
							<?php 
								if($p->kirim_id==NULL){
									$kirim="Belum Dikirim";
									$terima="";
								}else{
									$kirim="Telah Dikirim";
									if($p->diterima_oleh==NULL){
										$terima="Belum Diterima";
									}else $terima="Telah Diterima oleh ".$p->diterima_oleh;
								}
								echo "<code>".$p->jenis_surat."</code><br>";
								echo $p->perihal;  
								echo " <label class='label label-default'>".$kirim."</label>";
								echo " <label class='label label-success'>".$p->sifat_surat."</label>";
								echo "<br>".$p->tgl_surat;
								echo "<br><label class='label label-info'>".$terima."</label>";
							?>
						</td>
						<td>
							<ul class='nav nav-pills'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
									<li>
										<?php if($p->kirim_id==NULL){ ?>
											<a class='btn-edit-post' href="<?php echo $this->location('module/administration/surat/edit/'.$p->surat_id); ?>" ><i class='fa fa-edit'></i> Edit</a>
											<a class='btn-edit-post' href="<?php echo $this->location('module/administration/surat/kirim/'.$p->surat_id); ?>" ><i class='fa fa-refresh'></i> Kirim</a>
										<?php } ?>	
										<a class='btn-edit-post' href="<?php echo $this->location('module/administration/surat/display/'.$p->surat_id); ?>" ><i class='fa fa-search'></i> Lihat</a>
									</li>
								  </ul>
								</li>
							</ul>
						</td>
					</tr>
					<?php } ?>
					</tbody>
				</table>
			<?php else: ?>
				<div class="well" align="center">
					No Data Available
				</div>
			<?php endif; ?>
		</div>
	</div>
  </div>
</div>

<?php $this->foot(); ?>