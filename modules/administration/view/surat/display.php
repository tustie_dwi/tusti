<?php $this->head(); ?>
<h2 class="title-page">Surat Menyurat</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/administration/surat'); ?>">Surat Menyurat</a></li>
  <li><a href="#">Lihat</a></li>
</ol>
<!-- <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/administration/surat/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Tulis Surat Baru</a>
</div> -->

<div class="row">
	<div class="col-md-5">
		<div class="row">
			<div class="col-md-12">
				<div class="block-box">
					<div class="header">Detail</div>
					<div class="content">
						<table>
							<thead>
								<tr>
									<th style="min-width: 100px">&nbsp;</th>
									<th style="min-width: 10px">&nbsp;</th>	
									<th style="max-width: 300px">&nbsp;</th>	
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Sifat / Derajat</td>
									<td>: </td>
									<td><?php echo $posts_single->sifat_surat." / ".$posts_single->derajat_surat ?></td>
								</tr>
								<tr>
									<td>Perihal</td>
									<td>: </td>
									<td><?php echo $posts_single->perihal ?></td>
								</tr>
								<tr>
									<td>Lampiran</td>
									<td>: </td>
									<td><?php echo $posts_single->lampiran." Lembar" ?></td>
								</tr>
								<tr>
									<td>Dari</td>
									<td>: </td>
									<td><?php echo $posts_single->dari_nama ?></td>
								</tr>
								<tr>
									<td valign="top">Kepada</td>
									<td valign="top">: </td>
									<td valign="top">
										<?php 
											$i=1;
										    foreach($posts as $p){ ?>
												<ul class='nav nav-pills'>
													<li class='dropdown'>
													  <a class='dropdown-toggle' id='drop-unit' role='button' data-toggle='dropdown' href='#'><?php echo $i.". ".$p->kepada ?></a>
													  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop-unit'>
														<li>
															<?php if($p->kirim_id==NULL && $p->diterima_oleh==NULL){ ?>
																<a class='btn-edit-post terima' href="<?php echo $this->location('module/administration/surat/kirim/'.$p->surat_id); ?>"><i class='fa fa-refresh'></i> Kirim</a>
															<?php } elseif($p->kirim_id!=NULL && $p->diterima_oleh==NULL){ ?>
																<a class='btn-edit-post terima' href="#" data-toggle="modal" data-target=".modal_terima" data-uri="<?php echo $p->kirim_id ?>"><i class='fa fa-refresh'></i> Terima</a>
															<?php } ?>	
															<a class='btn-edit-post' href="<?php echo $this->location('module/administration/surat/lihat/umum/'.$p->kpd_id); ?>" ><i class='fa fa-print'></i> Cetak</a>
														</li>
													  </ul>
													</li>
												</ul>
									    <?php $i++;
											}
										?>
									</td>
								</tr>
								<tr>
									<td>Tanggal Surat</td>
									<td>: </td>
									<td><?php echo $posts_single->tgl_surat ?></td>
								</tr>
								<?php if($posts_single->kategori_surat == "masuk"){ ?>
								<tr>
									<td>Pengolah</td>
									<td>: </td>
									<td><?php echo $posts_single->pengolah_nama ?></td>
								</tr>
								<tr>
									<td>Tanggal Masuk</td>
									<td>: </td>
									<td><?php echo substr($posts_single->tgl_terima,0,10) ?></td>
								</tr>
								<tr>
									<td>No. Urut</td>
									<td>: </td>
									<td><?php echo $posts_single->no_urut ?></td>
								</tr>
								<?php } ?>
								<tr>
									<td>No. Surat</td>
									<td>: </td>
									<td><?php echo $posts_single->no_surat ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="block-box">
					<button type="button" id="tambah_disposisi_btn" class="btn btn-xs btn-primary pull-right daftar_disposisi">Tambah Disposisi</button>
					<button type="button" id="daftar_disposisi_btn" class="btn btn-xs btn-primary pull-right tambah_disposisi" style="display: none">Daftar Disposisi</button>
					<div class="header">Daftar Disposisi</div>
					<div class="content">
						<span class="daftar_disposisi">					
							<?php $this->view('surat/view_disposisi.php', $data); ?>
						</span>
						<span class="tambah_disposisi" style="display: none">					
							<form id="form-disposisi">
								<div class="form-group">
									<label class="control-label">Tanggal Disposisi</label>
									<input type="text" class="form-control form_datetime" name="tgl_disposisi" value="<?php if(isset($edit)) echo $edit->tgl_disposisi ?>" />
								</div>
								<div class="form-group">
									<label class="control-label">Kepada</label>
									<select multiple class="form-control e9" name="karyawan_disposisi[]" id="karyawan_disposisi"></select>
								</div>
								<div class="form-group">
									<label class="control-label">Isi Disposisi</label>
									<textarea class="form-control" name="isi_disposisi"><?php if(isset($edit)) echo $edit->catatan ?></textarea>
								</div>
								<div class="form-group">
									<input type="hidden" name="hidid_kpd" class="form-control" value="<?php if(isset($edit)) echo $edit->kepada_id ?>"/>
									<input type="hidden" name="hidid_disposisi" class="form-control" value="<?php if(isset($edit)) echo $edit->disposisi_id ?>"/>
									<input type="hidden" name="fakultas" class="form-control" value="<?php echo $posts_single->fakultas_karyawan_id ?>"/>
									<input type="hidden" name="cabang" class="form-control" value="<?php echo $posts_single->cabang_karyawan ?>"/>
									<input type="hidden" name="dari_nama" class="form-control" value="<?php echo $posts_single->dari_nama ?>"/>
									<input type="hidden" name="dari_id" class="form-control" value="<?php echo $posts_single->dari_id ?>"/>
									<input type="hidden" name="surat_id" class="form-control" value="<?php echo $posts_single->surat_id ?>"/>
									<input type="hidden" name="hidid_surat" class="form-control" value="<?php echo $posts_single->suratid ?>"/>
									<button type="submit" class="btn btn-primary">Data Valid&Save</button>
									<a type="button" class="btn btn-danger" href="<?php echo $this->location('module/administration/surat/display/'.$posts_single->surat_id) ?>">Batal</a>
								</div>
							</form>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7">
		<div class="block-box">
			<div class="content">
				<div class='col-md-12'>
				<?php echo $posts_single->isi_surat ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal_terima" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_terima" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-refresh"></i> Detail Terima Surat</h4>
      </div>
      <form method="post" id="form-terima-surat">
	      <div class="modal-body">
	      	<div class="form-group">
				<label class="control-label">Tanggal Surat Diterima</label>
				<input type="text" name="tanggal_surat_terima" class="form-control form_datetime"/>
			</div>
			<div class="form-group">
				<label class="control-label">File Bukti Kirim/Terima</label>
				<input type="file" name="file_kirim" class="form-control"/>
			</div>
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="kirim_id" class="form-control"/>
	      	<input type="hidden" name="kirimid" class="form-control" value="<?php echo $id ?>"/>
	        <button type="submit" class="close-button btn btn-primary">Terima</button>
	        <button type="button" class="close-button btn btn-default close_terima" data-dismiss="modal">Batal</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<?php $this->foot(); ?>