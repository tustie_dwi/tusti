<?php if(isset($disposisi)&&$disposisi!==""): ?>
	<table class='table table-hover example'>
		<thead>
			<tr>
				<th>Detail</th>	
				<th>Action</th>	
			</tr>
		</thead>
		<tbody>
			<?php foreach($disposisi as $dt){ ?>
					<tr>
						<td>
							<b>Kepada :</b>
								<br><?php echo $dt->kepada_nama ?><br>
							<b>Catatan :</b>
								<br><?php echo $dt->catatan ?><br>
							<b>Tanggal :</b>
								<br><?php echo $dt->tgl_disposisi ?><br>
						</td>
						<td>
							<a href="javascript:" title="edit" onclick="edit_disposisi('<?php echo $dt->disposisiid ?>')"><i class="fa fa-edit" style="color:blue"></i></a>&nbsp;
							<a href="javascript:" title="delete" onclick="hapus_disposisi('<?php echo $dt->disposisiid ?>')"><i class="fa fa-trash-o" style="color:red"></i></a>
						</td>
					</tr>
			<?php } ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="well" align="center">
		No Data Available
	</div>
<?php endif; ?>