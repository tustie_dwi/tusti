<?php $this->head(); ?>
<h2 class="title-page">Surat Menyurat</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/administration/surat'); ?>">Surat Menyurat</a></li>
  <li><a href="<?php echo $this->location('module/administration/surat/kirim/'.$id); ?>">Kirim</a></li>
</ol>
<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/administration/surat/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Tulis Surat Baru</a>
</div>

<div class="row">
  <div class="col-md-12">
	<div class="block-box">
		<div class="content">
			<div class="form-group">
				<table>
					<thead>
						<tr>
							<th style="min-width: 100px">&nbsp;</th>	
							<th style="max-width: 300px">&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Dari</td>
							<td>: <?php echo $posts_single->dari_nama ?></td>
						</tr>
						<tr>
							<td valign="top">Kepada</td>
							<td>: <?php 
										$kpd="";
										$i=1;
									    foreach($posts as $p){
											$kpd .= "<br>".$i.". ".$p->kepada;
											$i++;
										}
										echo $kpd;
								  ?>
							</td>
						</tr>
						<tr>
							<td>Perihal</td>
							<td>: <?php echo $posts_single->perihal ?></td>
						</tr>
						
					</tbody>
				</table>
			</div><br>
			<form method="post" id="form-kirim-surat">
				<?php if($posts_single->kirim_id==NULL && $posts_single->diterima_oleh==NULL){?>
				<div class="form-group">
					<label class="control-label">Pengantar Surat</label>
				    <div class="form-group">
						<select class="form-control" name="pengantar_fakultas">
							<option value="0">Silahkan Pilih Fakultas</option>
							<?php
							foreach($fakultas as $f){ 
								echo "<option value='".$f->hid_id."'>".$f->keterangan."</option>";
							} 
							?>
						</select>
						<select class="form-control" name="pengantar_cabang">
							<option value="0">Silahkan Pilih Cabang</option>
							<?php
							foreach($cabang as $c){ 
								echo "<option value='".$c->cabang_id."'>".$c->keterangan."</option>";
							} 
							?>
						</select>
					</div>
					<input type="text" name="pengantar" class="form-control typeahead" id="pengantar" placeholder="Masukkan Nama Karyawan Lalu Tekan Enter"/>
					<input type="hidden" name="pengantar_id" class="form-control" id="pengantar_id"/>
					<input type="hidden" name="pengantar_id_all" class="form-control" id="pengantar_id_all"/>
				</div>
				<div class="form-group">
					<label class="control-label">Tanggal Surat Dikirim</label>
					<input type="text" name="tanggal_surat_kirim" class="form-control form_datetime"/>
				</div>
				<div class="form-group">
					<label class="control-label">Keterangan</label>
					<textarea class="form-control" name="keterangan" id="keterangan"></textarea>
				</div>
				<?php }
					  else if($posts_single->kirim_id!=NULL && $posts_single->diterima_oleh==NULL){?>
					<div class="form-group">
						<label class="control-label">Tanggal Surat Diterima</label>
						<input type="text" name="tanggal_surat_terima" class="form-control form_datetime"/>
					</div>
					<div class="form-group">
						<label class="control-label">File Bukti Kirim/Terima</label>
						<input type="file" name="file_kirim" class="form-control"/>
					</div>
				<?php } ?>
				<div class="form-group">
					<?php 
					foreach($posts as $p){
						$kpd .= "<br>".$i.". ".$p->kepada;
						$i++;
						echo '<input type="hidden" name="kepada_id[]" class="form-control" value="'.$p->kepada_id.'"/>';
						echo '<input type="hidden" name="kirim_id[]" class="form-control" value="'.$p->kirim_id.'"/>';
					}
					?>
					<?php if($posts_single->kirim_id==NULL && $posts_single->diterima_oleh==NULL){?>
						<input type="submit" class="btn btn-primary" value="Kirim"/>
					<?php }
				  		  else if($posts_single->kirim_id!=NULL && $posts_single->diterima_oleh==NULL){?>
			  		  	<input type="submit" class="btn btn-primary" value="Terima"/>
		  		  	<?php } ?>
				</div>
			</form>
		</div>
	</div>
  </div>
 </div>
<?php $this->foot(); ?>