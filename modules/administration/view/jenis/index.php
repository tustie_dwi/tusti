<?php $this->head(); ?>

<style>
	.btn-edit{
		border : none;
		padding: 3px 5px;
		padding-left: 10px;
	}
</style>

<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/administration/conf/jenis'); ?>">Jenis Surat</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	
	<div class="btn-group">
	    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
	      Conf
	      <span class="fa fa-caret-down"></span>
	    </button>
	    <ul class="dropdown-menu">
	      <li><a href="<?php echo $this->location('module/administration/conf/jenis'); ?>"><!-- <i class='fa fa-edit'></i> --> Jenis Surat</a></li>
	      <li><a href="<?php echo $this->location('module/administration/conf/perihal'); ?>"><!-- <i class='fa fa-edit'></i> --> Perihal Surat</a></li>
	    </ul>
	</div>

    </div>
    
    
    <div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading">Jenis Surat</div>
				<div class="panel-body ">
					<?php if(isset($posts)): ?>
					<div class="tree">
						<ul>
							<?php foreach($posts as $row): ?>
							<li>
								<span><i class='fa fa-minus-square'></i> <?php echo $row->jenis_surat ?></span>
								
								<div class="btn-group">
								    <button type="button" class="btn btn-default dropdown-toggle btn-edit" data-toggle="dropdown">
								      Action
								      <span class="fa fa-caret-down"></span>
								    </button>
								    <ul class="dropdown-menu">
								      <li><a class="" href="javascript::" onclick="edit_jenis('<?php echo $row->jenis_surat ?>','<?php echo $row->keterangan ?>')"><i class='fa fa-edit'></i> Edit</a></li>
								    </ul>
								</div>	
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
					
					<?php else: ?>
					<div class="well" align="center">
						No Data Available
					</div>
					<?php endif;?>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<form method=post name="form-jenis" id="form-jenis">
			<div class="panel panel-default">
				<div class="panel-heading" id="form-box"><?php echo $param ?></div>
				<div class="panel-body ">
					<div class="form-group">
						<label class="control-label">Jenis Surat</label>								
						<input name="jenissurat" required="required" class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">Keterangan</label>								
						<input name="keterangan" required="required" class="form-control" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<textarea class="ckeditor"></textarea>
					
					<div class="form-group">
						<input type="hidden" name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>">
						<input name="b_jenissurat" id="b_jenissurat" value=" Data Valid &amp; Save " class="btn btn-primary" type="submit">&nbsp;
						
						<a href="<?php echo $this->location('module/administration/conf/jenis'); ?>" id="cancel-jenissurat" class="btn btn-danger" style="display: none;">Cancel</a>
					
					</div>
				</div>
			</div>
		</form>
		</div>
	</div>
    
</div>

<?php $this->foot(); ?>