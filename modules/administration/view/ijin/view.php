<?php $this->head(); ?>
<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <?php if($str=="umum"){ ?>
	  	<li><a href="<?php echo $this->location('module/administration/surat'); ?>">Surat Menyurat</a></li>
	  <?php }else{ ?>
	  	<li><a href="<?php echo $this->location('module/administration/surat/ijin'); ?>">Surat Izin</a></li>
	  <?php }?>
	</ol>
    <div class="row">
    	<div class="col-md-2">
		</div>
    	<div class="col-md-7">
			<div class="block-box">
				<div class="content">
					<?php
						echo "<div class='col-md-12' id='surat-body'>";
						if(isset($posts->kop_surat)&&$posts->kop_surat!=""){
							echo "<img width='100%' src='".$this->config->file_url_view.'/'.$posts->kop_surat."'>";
						}
						if($str=="umum"){
							$nama_pihak1	= ucwords($posts->dari_nama);
							if($posts->nik!="") $nik_pihak1 = $posts->nik; else $nik_pihak1 = "-";
							if($posts->golongan_dari!="") $golongan_dari_pihak1 = $posts->golongan_dari; else $golongan_dari_pihak1 = "-";
							if($posts->pangkat_dari!="") $pangkat_dari_pihak1 = $posts->pangkat_dari; else $pangkat_dari_pihak1 = "-";
							if($posts->jabatan_dari!="") $jabatan_dari_pihak1 = $posts->jabatan_dari; else $jabatan_dari_pihak1 = "-";
							
							if($posts->kepada!="") $nama_pihak2 = $posts->kepada; else $nama_pihak2 = "-";
							if($posts->tgl_lahir_karyawan!=""&&$posts->karyawan_id!="") $TTL_pihak2 = $posts->tgl_lahir_karyawan; 
								else if($posts->tgl_lahir_mahasiswa!=""&&$posts->mahasiswa_id!="") $TTL_pihak2 = $posts->tgl_lahir_mahasiswa;
								 else $TTL_pihak2 = "-";
							if($posts->alamat_karyawan!=""&&$posts->karyawan_id!="") $alamat_pihak2 = $posts->alamat_karyawan; 
								else if($posts->alamat_mahasiswa!=""&&$posts->mahasiswa_id!="") $alamat_pihak2 = $posts->alamat_mahasiswa;
								 else $alamat_pihak2 = "-";
							if($posts->nik_karyawan!=""&&$posts->karyawan_id!="") $NIM_pihak2 = $posts->nik_karyawan; 
								else if($posts->nim!=""&&$posts->mahasiswa_id!="") $NIM_pihak2 = $posts->nim;
								 else $NIM_pihak2 = "-";
							if($posts->fakultas_karyawan_ket!=""&&$posts->karyawan_id!="") $fakultas_pihak2 = $posts->fakultas_karyawan_ket; 
								else if($posts->fakultas_mahasiswa!=""&&$posts->mahasiswa_id!="") $fakultas_pihak2 = $posts->fakultas_mahasiswa;
								 else $fakultas_pihak2 = "-";
							if(isset($posts->semester_mahasiswa)&&$posts->semester_mahasiswa!=""&&$posts->mahasiswa_id!="") $semester_pihak2 = $posts->semester_mahasiswa;
								else $semester_pihak2 = "-";
							if(isset($posts->prodi_mahasiswa)&&$posts->prodi_mahasiswa!=""&&$posts->mahasiswa_id!="") $jurusan_pihak2 = $posts->prodi_mahasiswa;
								else $jurusan_pihak2 = "-";
							if(isset($posts->thn_akademik_mahasiswa)&&$posts->thn_akademik_mahasiswa!=""&&$posts->mahasiswa_id!="") $thn_akademik_pihak2 = $posts->thn_akademik_mahasiswa;
								else $thn_akademik_pihak2 = "-";
							if(isset($posts->nama_ortu)&&$posts->nama_ortu!=""&&$posts->mahasiswa_id!="") $nama_ortu_pihak2 = $posts->nama_ortu;
								else $nama_ortu_pihak2 = "-";
							
							//$tgl_surat = $posts->tgl_surat;
							$originalDate = $posts->tgl_surat;
							$newDate = date("d F Y", strtotime($originalDate));
							
							$surat = $posts->isi_surat;
							$surat = str_replace("{Nama Pihak 1}", $nama_pihak1, $surat);
							$surat = str_replace("{NIP Pihak 1}", $nik_pihak1, $surat);
							$surat = str_replace("{Pangkat Pihak 1}", $pangkat_dari_pihak1." / ".$golongan_dari_pihak1, $surat);
							$surat = str_replace("{Pekerjaan Pihak 1}", $jabatan_dari_pihak1, $surat);
							$surat = str_replace("{PT Pihak 1}", "Universitas Brawijaya", $surat);
							
							$surat = str_replace("{Nama Pihak 2}", $nama_pihak2, $surat);
							$surat = str_replace("{TTL Pihak 2}", $TTL_pihak2, $surat);
							$surat = str_replace("{Alamat Pihak 2}", $alamat_pihak2, $surat);
							
							$surat = str_replace("{Fakultas Pihak2}", $fakultas_pihak2, $surat);
							$surat = str_replace("{NIM Pihak 2}", $NIM_pihak2, $surat);
							$surat = str_replace("{SemesterPihak 2}", $semester_pihak2, $surat);
							$surat = str_replace("{Jurusan Pihak 2}", $jurusan_pihak2, $surat);
							$surat = str_replace("{Tahun Akademik Pihak 2}", ucwords($thn_akademik_pihak2), $surat);
							$surat = str_replace("{Nama Ortu Pihak 2}", ucwords($nama_ortu_pihak2), $surat);
							
							$surat = str_replace("{Tanggal Surat}", $newDate, $surat);
							
							echo $surat;
							
						}else{
							echo $posts->isi_surat;
						}
						echo "</div>";
					?>
				</div>
			</div>
			<button type="button" class="btn btn-primary" onclick="printSurat('surat-body');"><i class="fa fa-print"></i> Cetak</button>
		</div>
		<div class="col-md-3">
		</div>
	</div>
<?php $this->foot(); ?>