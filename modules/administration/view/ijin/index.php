<?php $this->head();
if(isset($user)){
	$nama 				= $user->value;
	$NIP 				= str_replace(" ", "", $user->nik);
	if($user->pangkat!=""){
		$pangkat 		= $user->pangkat;
	}else $pangkat 		= "-";
	if($user->gol!=""){
		$gol 			= $user->gol;
	}else $gol 			= "-";
	if($user->jabatan!=""){
		$jabatan 		= $user->jabatan;
	}else $jabatan 		= "-";
	$unit_	 			= explode('@',$user->unitkerja);
	$unit				= "";
	$unit_singkat		= "";
	foreach($unit_ as $u){
		$unit_data		= explode('-',$u);
		$unit			.= $unit_data[0].",";
		$unit_singkat	.= $unit_data[1].",";
	}
	$unit				= substr($unit, 0, -1);
	$unit_singkat		= substr($unit_singkat, 0, -1);
	
}
else{
	$nama			= "";
	$NIP			= "";
	$pangkat		= "";
	$gol			= "";
	$jabatan		= "";
	$unit			= "";
	$unit_singkat	= "";
	
}
?>
<h2 class="title-page">Surat Izin</h2>
<div class="row">
	<div class="col-md-5"><!--LIST-->
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/administration/surat/ijin'); ?>">Surat Izin</a></li>
		</ol>
		<?php if($jabatan=="PD 2" || $jabatan=="KTU"){ ?>
			<div class="block-box">
				<div class="header"><h4>Surat Ijin Yang Harus Divalidasi</h4></div>
				<div class="content">
					<?php if(isset($validasi)&&$validasi!==""): ?>
					<table class='table table-hover surat_table'>
						<thead>
							<tr>
								<th>Detail</th>	
								<th>&nbsp;</th>	
							</tr>
						</thead>
						<tbody>
							<?php 
								   foreach ($validasi as $v){ 
									   if($v->is_valid==1) $status = "<label class='label label-success'>Disetujui</label>";
									   elseif($v->is_valid==0) $status = "<label class='label label-danger'>Ditolak</label>";
									   else $status = "<label class='label label-warning'>Belum disetujui</label>";
								   ?>
									<tr>
										<td>
											<?php
												echo "<h5>".$v->keterangan."</h5>";
												echo $status;
											?>
										</td>
										<td>
											<ul class='nav nav-pills'>
												<li class='dropdown pull-right'>
												  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
												  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
													<li>
														<a class='btn-edit-post' href="#" onclick="validate('setuju','<?php echo $v->suratid ?>')"><i class='fa fa-check'></i> Setujui</a>
														<a class='btn-edit-post' href="#" onclick="validate('tolak','<?php echo $v->suratid ?>')"><i class='fa fa-ban'></i> Tolak</a>
													</li>
												  </ul>
												</li>
											</ul>
										</td>
									</tr>
									<?php } ?>
						</tbody>
					</table>
					<?php else: ?>
						<div class="well" align="center">
							No Data Available
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php } ?>
		
		<div class="block-box">
			<div class="content">
				<?php if(isset($posts)&&$posts!==""): ?>
				<table class='table table-hover surat_table'>
					<thead>
						<tr>
							<th>Detail</th>	
							<th>&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
						<?php 
							   foreach ($posts as $p) { 
							   if($p->is_valid==1) $status = "<label class='label label-success'>Disetujui</label>";
							   elseif($p->is_valid==0) $status = "<label class='label label-danger'>Ditolak</label>";
							   else $status = "<label class='label label-warning'>Belum disetujui</label>";
							   ?>
								<tr>
									<td>
										<?php
											if($role=="admin") echo "<code>A.N ".$nama."</code><br>";
											echo $p->keterangan;
											echo " ".$status;
											if($p->is_valid!=null){
												echo "<br><small>Oleh ".$p->validasi_by."</small>";
												echo "<br><small>Pada ".substr($p->tgl_validasi,0,10)."</small>";
											}
										?>
									</td>
									<td>
										<ul class='nav nav-pills'>
											<li class='dropdown pull-right'>
											  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
											  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
												<li>
													<a class='btn-edit-post' href="<?php echo $this->location('module/administration/surat/lihat/ijin/'.$p->suratid); ?>"><i class='fa fa-search'></i> Lihat</a>
												</li>
											  </ul>
											</li>
										</ul>
									</td>
								</tr>
								<?php } ?>
					</tbody>
				</table>
				<?php else: ?>
					<div class="well" align="center">
						No Data Available
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div> 
	<div class="col-md-7"><!--FORM-->
		<div class="panel panel-default">
			<div class="panel-heading">Tambah Surat Izin</div>
			<div class="panel-body ">
				<form method="post" id="form-surat-ijin">
					<input type="hidden" name="nama" class="form-control" value="<?php echo $nama; ?>"/>
					<input type="hidden" name="nik" class="form-control" value="<?php echo $NIP ?>"/>
					<input type="hidden" name="pangkat_gol" class="form-control" value="<?php echo $pangkat."/".$gol; ?>"/>
					<input type="hidden" name="jabatan" class="form-control" value="<?php echo $jabatan; ?>"/>
					<input type="hidden" name="unit" class="form-control" value="<?php echo $unit; ?>"/>
					<input type="hidden" name="unit_singkat" class="form-control" value="<?php echo $unit_singkat; ?>"/>
					<input type="hidden" name="periode" class="form-control" value="<?php echo date("Ym") ?>"/>
					<input type="hidden" name="urut" class="form-control" value="<?php echo $urut->urut ?>"/>
					<div class="form-group">
						<label class="control-label">Kategori Surat Izin</label>
						<select name="kategori_ijin" class="form-control">
							<option value="0">Silahkan Pilih</option>
							<?php
								foreach($ijin as $i){
									echo "<option value='".$i->kategori_id."'";
									echo " temp='".$i->temp_id."' ket='".$i->keterangan."'>".$i->keterangan."</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Tanggal Mulai</label>
						<input type="text" name="tgl_mulai" class="form-control form_datetime" />
					</div>
					<div class="form-group">
						<label class="control-label">Tanggal Selesai</label>
						<input type="text" name="tgl_selesai" class="form-control form_datetime" />
					</div>
					<div class="form-group">
						<label class="control-label">Keterangan</label>
						<textarea name="keterangan" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Surat</label>
						<textarea name="surat" class="form-control ckeditor" id="surat"></textarea>
					</div>
					<div class="form-group">
						<!-- <input type="hidden" name="hidId" class="form-control"> -->
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
</div>
<?php $this->foot(); ?>