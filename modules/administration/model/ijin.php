<?php
class model_ijin extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT *,
					   mid(md5(tbl_master_kategori_ijin.kategori_id),6,6) as kat_id,
					   mid(md5(tbl_master_kategori_ijin.template_id),6,6) as temp_id
				FROM db_ptiik_apps.tbl_master_kategori_ijin
				WHERE 1";
		if($id!=""){
	   		$sql .= " AND mid(md5(tbl_master_kategori_ijin.kategori_id),6,6) = '".$id."'";
			return $this->db->getRow($sql);
   		}
		return $this->db->query($sql);
	}
	
	function urut($thn=NULL){
		$sql = "SELECT concat( '0000' ,CAST( IFNULL( MAX( CAST( right( `no_urut` ,4 ) AS unsigned ) ) , 0 ) +1 AS unsigned )) AS urut
				FROM `db_ptiik_apps`.`tbl_surat_ijin`
				WHERE tbl_surat_ijin.periode LIKE '".$thn."%'    
			   ";
	    // echo $sql;
		return $this->db->getRow($sql);
	}
	
	function kategori_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kategori_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_master_kategori_ijin`
				where left(kategori_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_kategori_ijin($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_master_kategori_ijin', $data);
	}
	
	
	//surat ijin//
	function read_surat_ijin($id=NULL,$notid=NULL,$suratid=NULL){
		$sql = "SELECT *,
					   tbl_surat_ijin.cetak_surat as isi_surat,
					   mid(md5(tbl_surat_ijin.surat_id),6,6) as suratid
				FROM db_ptiik_apps.tbl_surat_ijin
				WHERE 1";
		if($notid!=""){
	   		$sql .= " AND tbl_surat_ijin.karyawan_id != '".$notid."' AND `is_valid` IS NULL";
	   		// echo $sql;
   		}
		if($id!=""){
	   		$sql .= " AND tbl_surat_ijin.karyawan_id = '".$id."'";
   		}
   		if($suratid!=""){
   			$sql .= " AND mid(md5(tbl_surat_ijin.surat_id),6,6) = '".$suratid."'";
			return $this->db->getRow($sql);
   		}
   		
   		// echo $sql;
		return $this->db->query($sql);
	}
	
	function surat_ijin_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(surat_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_surat_ijin`
				where left(surat_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_surat_ijin($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_ijin', $data);
	}
	
	function validate_ijin($surat_id=NULL, $is_valid=NULL, $validasi_by=NULL, $tgl_validasi=NULL, $user=NULL, $last_update=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_surat_ijin
				SET tbl_surat_ijin.is_valid = '".$is_valid."', 
				tbl_surat_ijin.validasi_by ='".$validasi_by."', 
				tbl_surat_ijin.tgl_validasi = '".$tgl_validasi."',
				tbl_surat_ijin.user_id = '".$user."',
				tbl_surat_ijin.last_update = '".$last_update."'
				WHERE mid(md5(tbl_surat_ijin.surat_id),6,6) = '".$surat_id."'";
		$result = $this->db->query($sql);
		return $result;
	}
	
}
?>