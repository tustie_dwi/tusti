<?php
class model_surat extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL,$kategori=NULL, $singlerow=NULL, $fakultas=NULL, $kepada=NULL, $karyawan=NULL, $mhs=NULL){
		$sql = "SELECT 
				    mid(md5(tbl_surat_menyurat.surat_id),6,6) as surat_id,
				    tbl_surat_menyurat.surat_id as suratid,
				    tbl_surat_menyurat.perihal_surat,
				    tbl_surat_menyurat.jenis_surat,
				    tbl_surat_menyurat.kategori_surat,
				    tbl_surat_menyurat.periode_surat,
				    tbl_surat_menyurat.sifat_surat,
				    tbl_surat_menyurat.derajat_surat,
				    tbl_surat_menyurat.no_urut,
				    tbl_surat_menyurat.no_surat,
				    tbl_surat_menyurat.unit_kerja_surat,
				    tbl_surat_menyurat.perihal,
				    tbl_surat_menyurat.tujuan_surat,
				    tbl_surat_menyurat.dari_id,
				    tbl_surat_menyurat.dari_nama,
				    tbl_surat_menyurat.isi_surat,
				    tbl_surat_menyurat.tgl_surat,
				    tbl_surat_menyurat.tgl_proses,
				    tbl_surat_menyurat.pengolah_id,
				    tbl_surat_menyurat.pengolah_nama,
				    tbl_surat_menyurat.lampiran,
				    tbl_surat_menyurat.file_loc,
				    tbl_surat_menyurat.status_proses,
				    tbl_surat_menyurat.is_dinas,
				    tbl_surat_menyurat.keterangan,
				    tbl_surat_menyurat.user_id,
				    tbl_surat_menyurat.last_update,
				    tbl_surat_kirim.kirim_id,
				    tbl_surat_kirim.karyawan_id as kuririd,
				    tbl_surat_kirim.kirim_oleh,
				    tbl_surat_kirim.diterima_oleh,
				    tbl_surat_kirim.tgl_kirim,
				    tbl_surat_kirim.tgl_terima,
				    tbl_surat_kirim.file_loc as file_loc_kirim,
				    tbl_surat_kepada.nama as kepada,
				    tbl_surat_kepada.instansi_surat,
				    tbl_surat_kepada.instansi_alamat,
				    mid(md5(tbl_surat_kepada.kepada_id),6,6) as kpd_id,
				    tbl_surat_kepada.kepada_id,
				    tbl_surat_kepada.mahasiswa_id,
				    tbl_surat_kepada.karyawan_id,
				    tbl_mahasiswa.cabang_id,
				    tbl_mahasiswa.nim,
				    tbl_mahasiswa.tgl_lahir as tgl_lahir_mahasiswa,
				    tbl_mahasiswa.alamat as alamat_mahasiswa,
			        tbl_mahasiswa.nama_ortu,
				    fakultas_mhs.fakultas_id,
				    fakultas_mhs.keterangan as fakultas_mahasiswa,
				    tbl_prodi.prodi_id,
				    tbl_prodi.keterangan as prodi_mahasiswa,";
		    if($kepada){
		    	$sql .= "(SELECT count(DISTINCT inf_semester) FROM db_ptiik_apps.tbl_krs WHERE tbl_krs.mahasiswa_id = tbl_mahasiswa.nim) as semester_mahasiswa,";
		    	$sql .= "(SELECT CONCAT(tahun,' ',is_ganjil,' ',is_pendek) as thn_akademik_mahasiswa FROM db_ptiik_apps.`tbl_tahunakademik` WHERE `is_aktif` = 1) as thn_akademik_mahasiswa,";
		    }
		    $sql .= "karyawan_kepada.fakultas_id as fakultas_karyawan_id,
				    karyawan_kepada.cabang_id as cabang_karyawan,
				    karyawan_kepada.tgl_lahir as tgl_lahir_karyawan,
				    karyawan_kepada.alamat as alamat_karyawan,
				    karyawan_kepada.nik as nik_karyawan,
				    karyawan_dari.nik,
				    karyawan_dari.golongan as golongan_dari,
				    tbl_master_golongan.pangkat as pangkat_dari,
				    tbl_master_jabatan.keterangan as jabatan_dari,
				    fakultas_karyawan.keterangan as fakultas_karyawan_ket
				FROM db_ptiik_apps.tbl_surat_menyurat
				LEFT JOIN db_ptiik_apps.tbl_surat_kepada ON tbl_surat_kepada.surat_id = tbl_surat_menyurat.surat_id
				LEFT JOIN db_ptiik_apps.tbl_surat_kirim ON tbl_surat_kirim.kepada_id = tbl_surat_kepada.kepada_id
				LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_surat_kepada.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
				LEFT JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				LEFT JOIN db_ptiik_apps.tbl_fakultas as fakultas_mhs ON fakultas_mhs.fakultas_id = tbl_prodi.fakultas_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan karyawan_kepada ON karyawan_kepada.karyawan_id = tbl_surat_kepada.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan karyawan_dari ON karyawan_dari.karyawan_id = tbl_surat_menyurat.dari_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan_kenaikan ON tbl_karyawan_kenaikan.karyawan_id = karyawan_dari.karyawan_id
				LEFT JOIN db_ptiik_apps.tbl_master_jabatan ON tbl_master_jabatan.jabatan_id = tbl_karyawan_kenaikan.jabatan_id
				LEFT JOIN db_ptiik_apps.tbl_master_golongan ON tbl_master_golongan.golongan = karyawan_dari.golongan
				LEFT JOIN db_ptiik_apps.tbl_fakultas as fakultas_karyawan ON fakultas_karyawan.fakultas_id = karyawan_kepada.fakultas_id
				WHERE 1
			   ";
		if($fakultas!="-"){
			$sql .= " AND tbl_surat_menyurat.fakultas_id = '".$fakultas."' ";
		}
		if($id!=""){
			$sql .= " AND mid(md5(tbl_surat_menyurat.surat_id),6,6) = '".$id."' ";
		}
		if($kategori!=""){
			$sql .= " AND tbl_surat_menyurat.kategori_surat = '".$kategori."' ";
			$sql .= " GROUP BY tbl_surat_menyurat.surat_id";
		}
		if($kepada!=""){
			$sql .= " AND mid(md5(tbl_surat_kepada.kepada_id),6,6) = '".$kepada."' ";
			// echo $sql;
			return $this->db->getRow($sql);
		}
		if($singlerow!=""){
			$sql .= " GROUP BY tbl_surat_menyurat.surat_id";
			// echo $sql;
			return $this->db->getRow($sql);
		}
		else{
			if($id!=""){
				$sql .= " GROUP BY tbl_surat_kepada.kepada_id";
			}
		}
		if($karyawan!=""){
			$sql .= " AND tbl_surat_kepada.karyawan_id = '".$karyawan."'
					  GROUP BY tbl_surat_menyurat.surat_id";
					  // echo $sql;
		}
		if($mhs!=""){
			$sql .= " AND tbl_surat_kepada.mahasiswa_id = '".$mhs."'
					  GROUP BY tbl_surat_menyurat.surat_id";
					  // echo $sql;
		}
		$sql .= " ORDER BY tbl_surat_menyurat.last_update";
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function disposisi($surat=NULL, $disposisi=NULL){
		$sql = "SELECT *,
					   mid(md5(tbl_surat_disposisi.disposisi_id),6,6) as disposisiid
				FROM `db_ptiik_apps`.`tbl_surat_disposisi`
				WHERE 1
			   ";
	    if($surat){
	    	$sql .= " AND mid(md5(tbl_surat_disposisi.surat_id),6,6) = '".$surat."'";
	    }
	    if($disposisi){
	    	$sql .= " AND mid(md5(tbl_surat_disposisi.disposisi_id),6,6) = '".$disposisi."'";
	    	return $this->db->getRow($sql);
	    }
	    // echo $sql;
		return $this->db->query($sql);
	}
	
	function disposisi_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(disposisi_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_surat_disposisi`
				where left(disposisi_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_disposisi($data=NULL){
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_disposisi',$data);
	}
	
	function delete_disposisi($id=NULL){
		$sql = "DELETE
			   	FROM `db_ptiik_apps`.`tbl_surat_disposisi`
			   	WHERE 1
			   ";
		if($id){
			$sql .= " AND mid(md5(tbl_surat_disposisi.disposisi_id),6,6) = '".$id."'";
		}
		return $this->db->query($sql);
	}
	
	function urut($thn){
		$sql = "SELECT CAST( IFNULL( MAX( CAST( right( `no_urut` ,4 ) AS unsigned ) ) , 0 ) +1 AS unsigned ) AS urut
				FROM `db_ptiik_apps`.`tbl_surat_menyurat`
				WHERE tbl_surat_menyurat.periode_surat LIKE '".$thn."%'
			   ";
	    // echo $sql;
		return $this->db->getRow($sql);
	}
	
	function get_unit_kerja_surat($id=NULL){
		$sql = "SELECT *
			   	FROM `db_ptiik_apps`.`tbl_surat_unit_kerja`
			   	WHERE 1
			   ";
		if($id){
			$sql .= " AND tbl_surat_unit_kerja.unit_kerja_surat = '".$id."'";
			return $this->db->getRow($sql);
		}
		return $this->db->query($sql);
	}
	
	function get_karyawan($str=NULL, $cabang=NULL,$fakultas=NULL,$id=NULL){
		$sql = "SELECT DISTINCT mid(md5(`db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`),9,7) AS id,
					tbl_karyawan.fakultas_id,
					tbl_karyawan.cabang_id,
					tbl_karyawan.karyawan_id,
					tbl_karyawan.nama,
					concat(IFNULL(tbl_karyawan.gelar_awal,''), ' ', tbl_karyawan.nama, ' ',IFNULL(tbl_karyawan.gelar_akhir,'')) as value,
					tbl_karyawan.nik,
					tbl_karyawan.is_nik,
					tbl_karyawan.tgl_lahir,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir,
					tbl_karyawan.pin,
					tbl_karyawan.foto,
					tbl_karyawan.home_base,
					(SELECT GROUP_CONCAT(concat(db_ptiik_apps.tbl_unit_kerja.keterangan,'-',db_ptiik_apps.tbl_unit_kerja.kode) SEPARATOR '@')
						FROM
							db_ptiik_apps.tbl_karyawan_unit
						INNER JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_apps.tbl_karyawan_unit.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
						WHERE
							`db_ptiik_apps`.`tbl_karyawan_unit`.`karyawan_id` = `tbl_karyawan`.`karyawan_id`
					) AS unitkerja,
					(SELECT GROUP_CONCAT(DISTINCT concat(db_ptiik_apps.tbl_master_jabatan.keterangan) SEPARATOR '@')
						FROM
							db_ptiik_apps.tbl_karyawan_kenaikan
						INNER JOIN db_ptiik_apps.tbl_master_jabatan ON db_ptiik_apps.tbl_karyawan_kenaikan.jabatan_id = db_ptiik_apps.tbl_master_jabatan.jabatan_id
						WHERE
							tbl_karyawan_kenaikan.is_aktif = 1
						AND `db_ptiik_apps`.`tbl_karyawan_kenaikan`.`karyawan_id` = `tbl_karyawan`.`karyawan_id`
					) AS jabatan,
					(SELECT GROUP_CONCAT(DISTINCT concat(db_ptiik_apps.tbl_master_jabatan.jabatan_id) SEPARATOR '@')
						FROM
							db_ptiik_apps.tbl_karyawan_kenaikan
						INNER JOIN db_ptiik_apps.tbl_master_jabatan ON db_ptiik_apps.tbl_karyawan_kenaikan.jabatan_id = db_ptiik_apps.tbl_master_jabatan.jabatan_id
						WHERE
							tbl_karyawan_kenaikan.is_aktif = 1
						AND `db_ptiik_apps`.`tbl_karyawan_kenaikan`.`karyawan_id` = `tbl_karyawan`.`karyawan_id`
					) AS jabatan_idx,
					(SELECT GROUP_CONCAT(concat(db_ptiik_apps.tbl_ruang.kode_ruang) SEPARATOR '@')
						FROM
							db_ptiik_apps.tbl_karyawan_ruang
						INNER JOIN db_ptiik_apps.tbl_ruang ON db_ptiik_apps.tbl_karyawan_ruang.ruang_id = db_ptiik_apps.tbl_ruang.ruang_id
						WHERE
							`db_ptiik_apps`.`tbl_karyawan_ruang`.`karyawan_id` = `tbl_karyawan`.`karyawan_id`
					) AS ruangkerja,
					tbl_karyawan.is_aktif,
					tbl_karyawan.is_tetap,
					tbl_karyawan.is_status,
					tbl_karyawan.interest,
					tbl_karyawan.biografi,
					tbl_karyawan.hp,
					tbl_karyawan.telp,
					tbl_karyawan.email,
					tbl_karyawan.alamat,
					tbl_master_golongan.golongan as gol,
 					tbl_master_golongan.pangkat as pangkat,
					tbl_master_golongan.inf_golongan,
					tbl_master_golongan.jabatan as jabatan_golongan
				FROM
					`db_ptiik_apps`.tbl_karyawan
				LEFT JOIN `db_ptiik_apps`.tbl_master_golongan ON tbl_karyawan.golongan = tbl_master_golongan.golongan
				WHERE 1
			   ";
		if($str!=""){
	   		$sql .= " AND tbl_karyawan.nama LIKE '%".$str."%'";
   		}
	    if($cabang!=""){
	   		$sql .= " AND tbl_karyawan.cabang_id = '".$cabang."'";
	    }
		if($fakultas!=""){
	   		$sql .= " AND tbl_karyawan.fakultas_id = '".$fakultas."'";
	    }
		if($id!=""){
	   		$sql .= " AND tbl_karyawan.karyawan_id = '".$id."'";
	   		// echo $sql;
	   		return $this->db->getRow($sql);
	    }
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_mhs($prodi=NULL,$cbg=NULL){
		$sql = "SELECT *,
					   concat(nim, ' - ', nama) as value
			   	FROM `db_ptiik_apps`.`tbl_mahasiswa`
			   	WHERE 1
			   ";
		if($cbg!=""){
	   		$sql .= " AND tbl_mahasiswa.cabang_id = '".$cbg."'";
	    }
		if($prodi!=""){
	   		$sql .= " AND tbl_mahasiswa.prodi_id = '".$prodi."'";
	    }
		
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_fakultas($id=NULL){
		$sql = "SELECT mid(md5(fakultas_id),6,6) as fakultasid, keterangan, fakultas_id as hid_id
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE 1";	
		if($id){
			$sql .= " AND tbl_fakultas.fakultas_id = '".$id."'";
		}	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_prodi($fakultas=NULL){
		$sql="SELECT mid(md5(`tbl_prodi`.`prodi_id`),6,6) prodi_id, 
					prodi_id as hid_id, 
					keterangan
			  FROM `db_ptiik_apps`.`tbl_prodi` 
			  WHERE is_aktif = 1
			 ";
		if($fakultas){
			$sql.=" AND tbl_prodi.fakultas_id = '".$fakultas."'";
		}
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_cabangub($id=NULL){
		$sql = "SELECT `cabang_id`, `keterangan`
				FROM `db_ptiik_apps`.`tbl_cabang` 
				WHERE 1
				";
		if($id){
			$sql .= " AND tbl_cabang.cabang_id = '".$id."'";
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function surat_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(surat_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_surat_menyurat`
				where left(surat_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function cek_instansi($instansi=NULL){
		$sql = "SELECT instansi_surat as data
				FROM `db_ptiik_apps`.`tbl_surat_instansi`
				WHERE tbl_surat_instansi.instansi_surat = '".$instansi."'"; 
		$dt = $this->db->getRow( $sql );
		if($dt){
		$strresult = $dt->data;
		return $strresult;
		}
	}
	
	function replace_surat($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_menyurat',$datanya);
		//var_dump($datanya);
	}
	
	function replace_folder($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_media_library_folder', $data);
	}
	
	//====KEPADA====//
	function kepada_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kepada_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_surat_kepada`
				where left(kepada_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function kirim_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kirim_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_surat_kirim`
				where left(kirim_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_nama_unitkerja($kpd=NULL){
		$sql = "SELECT tbl_karyawan.nama,
					  concat(IFNULL(tbl_karyawan.gelar_awal,''), ' ', tbl_karyawan.nama, ' ',IFNULL(tbl_karyawan.gelar_akhir,'')) as fullname,
					   tbl_karyawan_unit.unit_id as unit_kerja
				FROM `db_ptiik_apps`.`tbl_karyawan`
				LEFT JOIN `db_ptiik_apps`.`tbl_karyawan_unit` 
					ON tbl_karyawan.karyawan_id = tbl_karyawan_unit.karyawan_id AND tbl_karyawan_unit.is_aktif = 1
				WHERE 1";
		if($kpd){
			$sql .= " AND tbl_karyawan.karyawan_id = '".$kpd."'";
		}
		$result = $this->db->getRow( $sql );
		return $result;
	}
	
	function get_nama_mhs($kpd=NULL){
		$sql = "SELECT tbl_mahasiswa.nama
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				WHERE 1";
		if($kpd){
			$sql .= " AND tbl_mahasiswa.mahasiswa_id = '".$kpd."'";
		}
		$result = $this->db->getRow( $sql );
		return $result;
	}
	
	function replace_kepada($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_kepada',$datanya);
		// var_dump($datanya);
	}
	
	function replace_kirim($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_kirim',$datanya);
		// var_dump($datanya);
	}
	
	function delete_kepada_id($id=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_surat_kepada`
				WHERE 1";
		if($id){
			$sql .= " AND tbl_surat_kepada.kepada_id = '".$id."'";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_kirim_id($id=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_surat_kirim`
				WHERE 1";
		if($id){
			$sql .= " AND tbl_surat_kirim.kirim_id = '".$id."'";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function update_terima($terima_oleh=NULL, $tgl_terima=NULL, $file_loc_kirim=NULL, $user=NULL, $last_update=NULL, $id=NULL) {
		$sql = "UPDATE db_ptiik_apps.tbl_surat_kirim
			    SET diterima_oleh = '".$terima_oleh."', 
				     tgl_terima = '".$tgl_terima."', 
				     file_loc = '".$file_loc_kirim."',
				     user_id = '".$user."',
				     last_update = '".$last_update."'
			    WHERE tbl_surat_kirim.kirim_id = '".$id."'
			    ";
		$result = $this->db->query( $sql );
		return $result;
	}
}
?>