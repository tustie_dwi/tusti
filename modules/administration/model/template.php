<?php
class model_template extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL,$kategori=NULL){
		$sql = "SELECT *,
					   mid(md5(tbl_surat_template.template_id),6,6) as temp_id
				FROM db_ptiik_apps.tbl_surat_template
				WHERE 1";
		if($kategori!=""){
	   		$sql .= " AND tbl_surat_template.kategori = '".$kategori."'";
   		}
		if($id!=""){
	   		$sql .= " AND mid(md5(tbl_surat_template.template_id),6,6) = '".$id."'";
			return $this->db->getRow($sql);
   		}
		
		return $this->db->query($sql);
	}
	
	function template_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(template_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_surat_template`
				where left(template_id,6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_karyawan($str=NULL, $cabang=NULL,$fakultas=NULL,$id=NULL){
		$sql = "SELECT *,
					   concat(gelar_awal, ' ', nama, ' ', gelar_akhir) as value
			   	FROM `db_ptiik_apps`.`tbl_karyawan`
			   	WHERE 1
			   ";
		if($str!=""){
	   		$sql .= "AND tbl_karyawan.nama LIKE '%".$str."%'";
   		}
	    if($cabang!=""){
	   		$sql .= " AND tbl_karyawan.cabang_id = '".$cabang."'";
	    }
		if($fakultas!=""){
	   		$sql .= " AND tbl_karyawan.fakultas_id = '".$fakultas."'";
	    }
		if($id!=""){
	   		$sql .= " AND tbl_karyawan.karyawan_id = '".$id."'";
	   		return $this->db->getRow($sql);
	    }
		return $this->db->query($sql);
	}
	
	function replace_template($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_template', $data);
	}
	
	function delete($id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_surat_template
				WHERE mid(md5(tbl_surat_template.template_id),6,6) = '".$id."'";
		return $this->db->query($sql);
	}
}
?>