<?php
class model_instansi extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL,$term=NULL, $getrow=NULL){
		$sql = "SELECT tbl_surat_instansi.instansi_surat,
					   tbl_surat_instansi.instansi_surat as value, 
					   tbl_surat_instansi.alamat,
					   tbl_surat_instansi.telp
				FROM db_ptiik_apps.tbl_surat_instansi 
				WHERE 1
			   ";
		
		if($id){
			$sql .= " AND tbl_surat_instansi.instansi_surat = '".$id."' ";
			if($getrow){
				return $this->db->getRow($sql);
			}
		}
		
		if($term){
			$sql .= " AND tbl_surat_instansi.instansi_surat LIKE '%".$term."%' ";
		}
		
		$sql .= " ORDER BY tbl_surat_instansi.instansi_surat ASC ";
		
		return $this->db->query($sql);
	}
	
	function replace_instansi($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_instansi',$datanya);
		//var_dump($datanya);
	}
	
	function edit_instansi($instansi_id=NULL,$instansi=NULL,$alamat=NULL,$tlp=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_surat_instansi 
				SET tbl_surat_instansi.instansi_surat='".$instansi."', tbl_surat_instansi.alamat='".$alamat."', tbl_surat_instansi.telp='".$tlp."'
				WHERE tbl_surat_instansi.instansi_surat='".$instansi_id."'
				";
		
		return $this->db->query($sql);
	}
	
}
?>