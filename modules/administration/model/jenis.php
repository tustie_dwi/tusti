<?php
class model_jenis extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT tbl_surat_jenis.jenis_surat, 
					   tbl_surat_jenis.keterangan
				FROM db_ptiik_apps.tbl_surat_jenis 
				WHERE 1
			   ";
		
		if($id){
			$sql .= " AND tbl_surat_jenis.jenis_surat = '".$id."' ";
		}
		
		$sql .= " ORDER BY tbl_surat_jenis.jenis_surat ASC ";
		
		return $this->db->query($sql);
	}
	
	function replace_jenis($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_jenis',$datanya);
		//var_dump($datanya);
	}
	
	function edit_jenis($jenis_surat_id=NULL,$jenis_surat=NULL,$keterangan=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_surat_jenis 
				SET tbl_surat_jenis.jenis_surat='".$jenis_surat."', tbl_surat_jenis.keterangan='".$keterangan."'
				WHERE tbl_surat_jenis.jenis_surat='".$jenis_surat_id."'
				";
		
		return $this->db->query($sql);
	}
	
}
?>