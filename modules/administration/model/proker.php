<?php
class model_proker extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL,$view_as=NULL,$karyawanid=NULL,$fakultasid=NULL,$unitid=NULL){
		$sql = "SELECT 
				mid(md5(tbl_rencana_kerja.rencana_id),6,6) as rencana_id,
				tbl_rencana_kerja.rencana_id as rencanaid,
				mid(md5(tbl_rencana_kerja.karyawan_id),6,6) as karyawan_id,
				tbl_rencana_kerja.karyawan_id as karyawanid,
				tbl_rencana_kerja.periode,
				tbl_rencana_kerja.judul,
				tbl_rencana_kerja.unit_id,
				tbl_rencana_kerja.user_id,
				tbl_rencana_kerja.last_update,
				tbl_rencana_kerja.is_valid,
				tbl_rencana_kerja.validasi_by,
				tbl_rencana_kerja.tgl_validasi,
				tbl_rencana_kerja.st_proses,
                                
                tbl_unit_kerja.keterangan as unit,
                tbl_karyawan.nama as namakaryawan
                                
				FROM db_ptiik_apps.tbl_rencana_kerja
                LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_rencana_kerja.unit_id
                LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_rencana_kerja.karyawan_id
				WHERE 1
			   ";
		
		if($id){
			$sql .= " AND mid(md5(tbl_rencana_kerja.rencana_id),6,6) = '".$id."' ";
		}
		
		if($view_as=='dosen'){
			$sql .= " AND tbl_rencana_kerja.unit_id 
						IN (SELECT tbl_karyawan_unit.unit_id 
						    FROM db_ptiik_apps.tbl_karyawan_unit 
						    WHERE tbl_karyawan_unit.karyawan_id = '".$karyawanid."' ) ";
		}
		elseif($view_as=='kasubag'){
			$sql .= " AND tbl_rencana_kerja.unit_id = '".$unitid."' ";
		}
		elseif($view_as=='PD'){
			$sql .= " AND tbl_unit_kerja.fakultas_id = '".$fakultasid."' ";
		}
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_jabatan($karyawanid=NULL){
		$sql = "SELECT
				tbl_master_jabatan.keterangan as jabatan,
				tbl_karyawan_kenaikan.unit_id,
				tbl_karyawan_kenaikan.fakultas_id
				FROM db_ptiik_apps.tbl_karyawan_kenaikan
				LEFT JOIN db_ptiik_apps.tbl_master_jabatan ON tbl_master_jabatan.jabatan_id = tbl_karyawan_kenaikan.jabatan_id
				WHERE 1
				AND tbl_karyawan_kenaikan.karyawan_id = '".$karyawanid."'
				AND tbl_karyawan_kenaikan.is_aktif = '1'
			   ";
		// echo $sql;
		return $this->db->getRow( $sql );
	}
	
	function read_detail($id=NULL,$param=NULL,$parent=NULL,$get_kebutuhan=NULL){
		$sql = "SELECT
				mid(md5(tbl_rencana_kerja_detail.detail_id),6,6) as detail_id,
				tbl_rencana_kerja_detail.detail_id as detailid,
				mid(md5(tbl_rencana_kerja_detail.rencana_id),6,6) as rencana_id,
				tbl_rencana_kerja_detail.rencana_id as rencanaid,
				tbl_rencana_kerja_detail.kegiatan,
				mid(md5(tbl_rencana_kerja_detail.parent_id),6,6) as parent_id,
				tbl_rencana_kerja_detail.parent_id as parentid ";
		
		if($get_kebutuhan=='yes'){		
		$sql .=",(SELECT COUNT(tbl_rencana_kerja_kebutuhan.kebutuhan_id) 
				FROM db_ptiik_apps.tbl_rencana_kerja_kebutuhan 
				WHERE tbl_rencana_kerja_kebutuhan.detail_id = tbl_rencana_kerja_detail.detail_id ) as kebutuhan_count ";
				
		// $sql .=",(SELECT COUNT(a.detail_id) 
				// FROM db_ptiik_apps.tbl_rencana_kerja_detail as a 
				// WHERE a.parent_id = tbl_rencana_kerja_detail.detail_id AND a.parent_id IS NOT NULL) as child_count
				// ";
		}
				
		$sql .="FROM db_ptiik_apps.tbl_rencana_kerja_detail
				WHERE 1
			   ";
		
		if($id){
			if($param=='rencanaid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_detail.rencana_id),6,6) = '".$id."' ";
			}
			elseif($param=='parentid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_detail.parent_id),6,6) = '".$id."' ";
			}
			else{
				$sql .= " AND mid(md5(tbl_rencana_kerja_detail.detail_id),6,6) = '".$id."' ";
			}
		}
		
		if($parent=='parent'){
			$sql .= " AND tbl_rencana_kerja_detail.parent_id IS NULL ";
		}else{
			$sql .= " AND tbl_rencana_kerja_detail.parent_id IS NOT NULL ";
		}
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function read_kebutuhan($id=NULL,$param=NULL){
		$sql = "SELECT 
				mid(md5(tbl_rencana_kerja_kebutuhan.kebutuhan_id),6,6) as kebutuhan_id,
				tbl_rencana_kerja_kebutuhan.kebutuhan_id as kebutuhanid,
				mid(md5(tbl_rencana_kerja_kebutuhan.detail_id),6,6) as detail_id,
				tbl_rencana_kerja_kebutuhan.detail_id as detailid,
				tbl_rencana_kerja_kebutuhan.keterangan,
				tbl_rencana_kerja_kebutuhan.qty,
				tbl_rencana_kerja_kebutuhan.harga,
				tbl_rencana_kerja_kebutuhan.total_biaya,
				
				tbl_rencana_kerja_detail.kegiatan as detail_kegiatan
				
				FROM db_ptiik_apps.tbl_rencana_kerja_kebutuhan
				LEFT JOIN db_ptiik_apps.tbl_rencana_kerja_detail ON tbl_rencana_kerja_detail.detail_id = tbl_rencana_kerja_kebutuhan.detail_id 
				WHERE 1
			   ";
		
		if($id){
			if($param=='detailid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_kebutuhan.detail_id),6,6) = '".$id."' ";
			}else{
				$sql .= " AND mid(md5(tbl_rencana_kerja_kebutuhan.kebutuhan_id),6,6) = '".$id."' ";
			}
		}

		// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_unit_admin($param=NULL,$id=NULL){
		$sql = "SELECT 
					tbl_unit_kerja.unit_id,
					tbl_unit_kerja.keterangan
				FROM db_ptiik_apps.tbl_unit_kerja
				WHERE 1
			   ";
		
		if($param=='PD'){
			$sql .= " AND tbl_unit_kerja.fakultas_id = '".$id."' ";
		}elseif($param=='kasubag'){
			$sql .= " AND tbl_rencana_kerja.unit_id = '".$id."' ";
		}
			   
		return $this->db->query($sql);
	}
	
	function get_unit($karyawanid=NULL){
		$sql = "SELECT 
					tbl_karyawan_unit.unit_id,
					tbl_karyawan_unit.karyawan_id,
					tbl_karyawan_unit.periode_mulai,
					tbl_karyawan_unit.periode_selesai,
					tbl_karyawan_unit.is_aktif,
					
					tbl_unit_kerja.keterangan
				FROM db_ptiik_apps.tbl_karyawan_unit
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_karyawan_unit.unit_id
				WHERE 1
			   ";
			   
		if($karyawanid){
			// $sql .= " AND mid(md5(tbl_karyawan_unit.karyawan_id),6,6) = '".$karyawanid."' ";
			$sql .= " AND tbl_karyawan_unit.karyawan_id = '".$karyawanid."' ";
		}
		
		$sql .= " AND tbl_karyawan_unit.is_aktif = '1' ";
		
		return $this->db->query($sql);
		
	}
	
	function get_rencana_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(rencana_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_rencana_kerja";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_rencana_kerja($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_rencana_kerja',$datanya);
		//var_dump($datanya);
	}
	
	function get_detail_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(detail_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_rencana_kerja_detail";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_rencana_kerja_detail($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_rencana_kerja_detail',$datanya);
	}
	
	function get_kebutuhan_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kebutuhan_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_rencana_kerja_kebutuhan";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_rencana_kerja_kebutuhan($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_rencana_kerja_kebutuhan',$datanya);
	}
	
	function det_all_detail_child($detailid){
		$sql = "SELECT
				mid(md5(tbl_rencana_kerja_detail.detail_id),6,6) as detail_id,
				tbl_rencana_kerja_detail.detail_id as detailid,
				mid(md5(tbl_rencana_kerja_detail.rencana_id),6,6) as rencana_id,
				tbl_rencana_kerja_detail.rencana_id as rencanaid,
				tbl_rencana_kerja_detail.kegiatan,
				mid(md5(tbl_rencana_kerja_detail.parent_id),6,6) as parent_id,
				tbl_rencana_kerja_detail.parent_id as parentid
				FROM db_ptiik_apps.tbl_rencana_kerja_detail
				WHERE 1";
		
		if($detailid){
			$sql .= " AND tbl_rencana_kerja_detail.parent_id = '".$detailid."' ";
		}
		
		return $this->db->query($sql);
	}
	
	function delete_kebutuhan($kebutuhanid=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_kebutuhan WHERE kebutuhan_id = '".$kebutuhanid."' ";
		
		return $this->db->query($sql);
	}
	
	function delete_detail($detailid=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_detail WHERE detail_id = '".$detailid."' ";
		$sql2 = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_kebutuhan WHERE detail_id = '".$detailid."' ";
		$sql3 = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_detail WHERE parent_id = '".$detailid."' ";
		$this->db->query($sql3);
		$this->db->query($sql2);
		return $this->db->query($sql);
	}
	
	function set_validasi_proker($rencanaid=NULL, $stat=NULL, $date=NULL, $karyawanid=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_rencana_kerja 
				SET 
				tbl_rencana_kerja.is_valid = '".$stat."',
				tbl_rencana_kerja.validasi_by = '".$karyawanid."',
				tbl_rencana_kerja.tgl_validasi = '".$date."' 
				WHERE mid(md5(tbl_rencana_kerja.rencana_id),6,6) = '".$rencanaid."'
				";
		return $this->db->query($sql);
	}
	
	//===== EVALUASI ======
	function set_validasi_evaluasi($kegiatanid=NULL, $stat=NULL, $date=NULL, $karyawanid=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_rencana_kerja_kegiatan 
				SET 
				tbl_rencana_kerja_kegiatan.is_valid = '".$stat."',
				tbl_rencana_kerja_kegiatan.validasi_by = '".$karyawanid."',
				tbl_rencana_kerja_kegiatan.tgl_validasi = '".$date."' 
				WHERE mid(md5(tbl_rencana_kerja_kegiatan.kegiatan_id),6,6) = '".$kegiatanid."'
				";
		return $this->db->query($sql);
	}
	
	function delete_kegiatan($kegiatanid=NULL,$param=NULL){
		if($param=='target'){
			$sql = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_kegiatan WHERE kegiatan_id = '".$kegiatanid."' ";
		}else{
			$sql = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_kegiatan WHERE kegiatan_id = '".$kegiatanid."' ";
			
			$sql2 = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_baseline WHERE kegiatan_id = '".$kegiatanid."' ";
			$sql3 = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_kegiatan WHERE parent_id = '".$kegiatanid."' ";
			$this->db->query($sql3);
			$this->db->query($sql2);
		}
		
		return $this->db->query($sql);
		
	}
	
	function delete_baseline($baselineid=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_rencana_kerja_baseline WHERE baseline_id = '".$baselineid."' ";
		return $this->db->query($sql);
	}
	
	function read_detail_evaluasi($id=NULL,$param=NULL){
		$sql = "SELECT DISTINCT
				mid(md5(tbl_rencana_kerja_detail.detail_id),6,6) as detail_id,
				tbl_rencana_kerja_detail.detail_id as detailid,
				mid(md5(tbl_rencana_kerja_detail.rencana_id),6,6) as rencana_id,
				tbl_rencana_kerja_detail.rencana_id as rencanaid,
				tbl_rencana_kerja_detail.kegiatan,
				mid(md5(tbl_rencana_kerja_detail.parent_id),6,6) as parent_id,
				tbl_rencana_kerja_detail.parent_id as parentid 
				FROM db_ptiik_apps.tbl_rencana_kerja_detail
                INNER JOIN db_ptiik_apps.tbl_rencana_kerja_kegiatan ON tbl_rencana_kerja_detail.detail_id = tbl_rencana_kerja_kegiatan.detail_id
				WHERE 1
			   ";
		
		if($id){
			if($param=='rencanaid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_detail.rencana_id),6,6) = '".$id."' ";
			}
			elseif($param=='parentid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_detail.parent_id),6,6) = '".$id."' ";
			}
			else{
				$sql .= " AND mid(md5(tbl_rencana_kerja_detail.detail_id),6,6) = '".$id."' ";
			}
		}

		return $this->db->query($sql);
	}
	
	function read_evaluasi($id=NULL,$param=NULL,$kategori=NULL,$view_as=NULL,$karyawanid=NULL,$fakultasid=NULL,$unitid=NULL){
		$sql = "SELECT 
				mid(md5(tbl_rencana_kerja_kegiatan.kegiatan_id),6,6) as kegiatan_id,
				tbl_rencana_kerja_kegiatan.kegiatan_id as kegiatanid,
				
				mid(md5(tbl_rencana_kerja_kegiatan.detail_id),6,6) as detail_id,
				tbl_rencana_kerja_kegiatan.detail_id as detailid,
				
				tbl_rencana_kerja_kegiatan.kategori,
				tbl_rencana_kerja_kegiatan.judul,
				tbl_rencana_kerja_kegiatan.keterangan,
				tbl_rencana_kerja_kegiatan.capaian,
				tbl_rencana_kerja_kegiatan.kendala,
				tbl_rencana_kerja_kegiatan.doc_pendukung,
				tbl_rencana_kerja_kegiatan.tgl_rencana_mulai,
				tbl_rencana_kerja_kegiatan.tgl_target_selesai,
				tbl_rencana_kerja_kegiatan.tgl_pelaksanaan,
				tbl_rencana_kerja_kegiatan.tgl_selesai,
				tbl_rencana_kerja_kegiatan.is_valid,
				tbl_rencana_kerja_kegiatan.validasi_by,
				tbl_rencana_kerja_kegiatan.tgl_validasi,
				tbl_rencana_kerja_kegiatan.user_id,
				tbl_rencana_kerja_kegiatan.last_update,
				
				mid(md5(tbl_rencana_kerja_kegiatan.parent_id),6,6) as parent_id,
				tbl_rencana_kerja_kegiatan.parent_id as parentid,
				
				tbl_rencana_kerja_detail.kegiatan as kegiatan_detail,
				tbl_media_library.file_name
				
				FROM db_ptiik_apps.tbl_rencana_kerja_kegiatan 
				LEFT JOIN db_ptiik_apps.tbl_rencana_kerja_detail ON tbl_rencana_kerja_detail.detail_id = tbl_rencana_kerja_kegiatan.detail_id
				LEFT JOIN db_ptiik_apps.tbl_rencana_kerja ON tbl_rencana_kerja.rencana_id = tbl_rencana_kerja_detail.rencana_id
				LEFT JOIN db_ptiik_apps.tbl_media_library ON tbl_media_library.file_id = tbl_rencana_kerja_kegiatan.doc_pendukung
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_rencana_kerja.unit_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_rencana_kerja.karyawan_id
				WHERE 1
			   ";
		
		if($id){
			if($param=='detailid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_kegiatan.detail_id),6,6) = '".$id."' ";
			}
			elseif($param=='parentid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_kegiatan.parent_id),6,6) = '".$id."' ";
			}
			else{
				$sql .= " AND mid(md5(tbl_rencana_kerja_kegiatan.kegiatan_id),6,6) = '".$id."' ";
			}
			
		}
		
		if($kategori=='kegiatan'){
			$sql .= " AND tbl_rencana_kerja_kegiatan.kategori = 'kegiatan' ";
		}else{
			$sql .= " AND tbl_rencana_kerja_kegiatan.kategori = 'target' ";
		}
		
		if($view_as=='dosen'){
			$sql .= " AND tbl_rencana_kerja.unit_id 
						IN (SELECT tbl_karyawan_unit.unit_id 
						    FROM db_ptiik_apps.tbl_karyawan_unit 
						    WHERE tbl_karyawan_unit.karyawan_id = '".$karyawanid."' ) ";
		}
		elseif($view_as=='kasubag'){
			$sql .= " AND tbl_rencana_kerja.unit_id = '".$unitid."' ";
		}
		elseif($view_as=='PD'){
			$sql .= " AND tbl_unit_kerja.fakultas_id = '".$fakultasid."' ";
		}
		
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function read_baseline($id=NULL, $param=NULL){
		$sql = "SELECT 
				mid(md5(tbl_rencana_kerja_baseline.baseline_id),6,6) as baseline_id,
				tbl_rencana_kerja_baseline.baseline_id as baselineid,
				
				mid(md5(tbl_rencana_kerja_baseline.kegiatan_id),6,6) as kegiatan_id,
				tbl_rencana_kerja_baseline.kegiatan_id as kegiatanid,
				
				tbl_rencana_kerja_baseline.judul,
				tbl_rencana_kerja_baseline.periode,
				
				mid(md5(tbl_rencana_kerja_baseline.detail_id),6,6) as detail_id,
				tbl_rencana_kerja_baseline.detail_id as detailid
				
				FROM db_ptiik_apps.tbl_rencana_kerja_baseline
				WHERE 1
				";
		
		if($id){
			if($param=='kegiatanid'){
				$sql .= " AND mid(md5(tbl_rencana_kerja_baseline.kegiatan_id),6,6) = '".$id."' ";
			}else{
				$sql .= " AND mid(md5(tbl_rencana_kerja_baseline.baseline_id),6,6) = '".$id."' ";
			}
		}
		// echo $sql;
		return $this->db->query($sql);
		
	}
	
	function get_kegiatan_base_num($kegiatanid=NULL){
		$sql = "SELECT 
				COUNT(tbl_rencana_kerja_kegiatan.kegiatan_id) as targetnum,
				(SELECT COUNT(tbl_rencana_kerja_baseline.baseline_id) FROM db_ptiik_apps.tbl_rencana_kerja_baseline WHERE 1 AND mid(md5(tbl_rencana_kerja_baseline.kegiatan_id),6,6) = '".$kegiatanid."') as baselinenum
				FROM db_ptiik_apps.tbl_rencana_kerja_kegiatan
				WHERE 1
				AND mid(md5(tbl_rencana_kerja_kegiatan.parent_id),6,6) = '".$kegiatanid."'
			   ";
		// echo $sql;
		return $this->db->getRow($sql);
	}
	
	function get_detailId($id=NULL){
		$sql = "SELECT
				mid(md5(tbl_rencana_kerja_detail.detail_id),6,6) as detail_id,
				tbl_rencana_kerja_detail.detail_id as detailid,
				mid(md5(tbl_rencana_kerja_detail.rencana_id),6,6) as rencana_id,
				tbl_rencana_kerja_detail.rencana_id as rencanaid,
				tbl_rencana_kerja_detail.kegiatan,
				mid(md5(tbl_rencana_kerja_detail.parent_id),6,6) as parent_id,
				tbl_rencana_kerja_detail.parent_id as parentid 
				FROM db_ptiik_apps.tbl_rencana_kerja_detail
				WHERE 1
			   ";
		
		if($id){
			$sql .= " AND mid(md5(tbl_rencana_kerja_detail.detail_id),6,6) = '".$id."' ";
		}
		
		if($result = $this->db->getRow( $sql )){
			return $result;
		}else return FALSE;
		
	}
	
	function get_kegiatan_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kegiatan_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_rencana_kerja_kegiatan";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_rencana_kerja_kegiatan($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_rencana_kerja_kegiatan',$datanya);
	}
	
	function get_baseline_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(baseline_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_rencana_kerja_baseline";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_rencana_kerja_baseline($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_rencana_kerja_baseline',$datanya);
	}
	
	function read_folder($karyawanid=NULL, $whatfolder=NULL, $folderid=NULL, $param=NULL){
		$sql = "SELECT 
				mid(md5(tbl_media_library_folder.id),6,6) as folder_id,
				tbl_media_library_folder.id as folderid,
				tbl_media_library_folder.folder_name,
				mid(md5(tbl_media_library_folder.parent_id),6,6) as parentid,
				tbl_media_library_folder.parent_id,
				tbl_media_library_folder.mkditawarkan_id,
				tbl_media_library_folder.materi_id,
				tbl_media_library_folder.is_available,
				tbl_media_library_folder.karyawan_id,
				tbl_media_library_folder.user_id,
				tbl_media_library_folder.last_update
				
				FROM db_ptiik_apps.tbl_media_library_folder 
				WHERE 1
				";
		
		if($karyawanid){
			$sql .= " AND tbl_media_library_folder.karyawan_id = '".$karyawanid."' ";
		}
		
		if($whatfolder=='parent-folder'){
			$sql .= " AND tbl_media_library_folder.parent_id = '0' ";
		}
		
		if($folderid!=''){
			if($param=='get-parent'){
				$sql .= " AND mid(md5(tbl_media_library_folder.id),6,6) = '".$folderid."' ";
			}else{
				$sql .= " AND mid(md5(tbl_media_library_folder.parent_id),6,6) = '".$folderid."' ";
			}
		}
		
		$sql .= " AND tbl_media_library_folder.is_available = '1' ";
		
		// echo $sql;
		return $this->db->query($sql);
		
	}
	
	function read_file($whatfile=NULL, $folderid=NULL, $fileid=NULL){
		$sql = "SELECT 
				mid(md5(tbl_media_library.file_id),6,6) as file_id,
				tbl_media_library.file_id as fileid,
				tbl_media_library.jenis_file,
				tbl_media_library.judul,
				tbl_media_library.keterangan,
				
				tbl_media_library.file_name,
				tbl_media_library.file_loc,
				tbl_media_library.file_size,
				tbl_media_library.file_type,
				tbl_media_library.folder_id,
				tbl_media_library.user_id,
				tbl_media_library.last_update
				
				FROM db_ptiik_apps.tbl_media_library 
				WHERE 1
				";
		
		if($whatfile=='no-folder'){
			$sql .= " AND tbl_media_library.folder_id = '0' ";
		}
		
		if($folderid!=''){
			$sql .= " AND mid(md5(tbl_media_library.folder_id),6,6) = '".$folderid."' ";
		}
		
		if($fileid!=''){
			$sql .= " AND mid(md5(tbl_media_library.file_id),6,6) = '".$fileid."' ";
		}
		
		$sql .= " AND tbl_media_library.jenis_file != 'video' ";
		// echo $sql;
		return $this->db->query($sql);
		
	}
	
	function get_aktifitas_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(aktifitas_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_aktifitas";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_tbl_aktifitas($dataaktifitas){
		$this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$dataaktifitas);
	}
	
}
?>