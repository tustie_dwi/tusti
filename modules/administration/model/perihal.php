<?php
class model_perihal extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT tbl_surat_perihal.perihal_surat, 
					   tbl_surat_perihal.keterangan
				FROM db_ptiik_apps.tbl_surat_perihal 
				WHERE 1
			   ";
		
		if($id){
			$sql .= " AND tbl_surat_perihal.perihal_surat = '".$id."' ";
		}
		
		$sql .= " ORDER BY tbl_surat_perihal.perihal_surat ASC ";
		// echo $sql;
		return $this->db->query($sql);
	}
	
	function replace_perihal($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_surat_perihal',$datanya);
		//var_dump($datanya);
	}
	
	function edit_perihal($perihal_surat_id=NULL,$perihal_surat=NULL,$keterangan=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_surat_perihal 
				SET tbl_surat_perihal.perihal_surat='".$perihal_surat."', tbl_surat_perihal.keterangan='".$keterangan."'
				WHERE tbl_surat_perihal.perihal_surat='".$perihal_surat_id."'
				";
		// echo $sql;
		return $this->db->query($sql);
	}
	
}
?>