<?php
class administration_surat extends comsmodule {
	private $coms;
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		$role			= $this->coms->authenticatedUser->role;
		if($role!="mahasiswa"){
			$msurat 		= new model_surat();
			$fakultas		= $this->coms->authenticatedUser->fakultas;
			if(isset($_POST['kategori_index'])&&$_POST['kategori_index']!='0'){
				$kategori				= $_POST['kategori_index'];
				$data['kategori_index']	= $kategori;
				$data['posts'] 			= $msurat->read("",$kategori,"",$fakultas);
			}
			else{
				$data['posts'] = "";
			}
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');

			$this->add_script('js/surat.js');

			$this->view( 'surat/index.php', $data );
		}
	}
	
	function write(){
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		
		$role			= $this->coms->authenticatedUser->role;
		if($role!="mahasiswa"){
			$msurat = new model_surat();
			$mjenis = new model_jenis();
			$mperihal = new model_perihal();
			$mtemplate = new model_template();
			$fakultas		= $this->coms->authenticatedUser->fakultas;
			
			$data['perihal'] 	= $mperihal->read();
			$data['jenis'] 		= $mjenis->read();
			$data['urut']		= $msurat->urut(date('Y'));
			if($fakultas == "-"){
				$data['fakultas'] 	= $msurat->get_fakultas();
			}else $data['fakultas'] 	= $msurat->get_fakultas($fakultas);
			$data['cabang'] 	= $msurat->get_cabangub();
			$data['unit_kerja'] = $msurat->get_unit_kerja_surat();
			$data['template'] 	= $mtemplate->read("","umum");
			$data['posts'] 		= "";
					
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->add_script('js/surat.js');
			
			$this->view( 'surat/edit.php', $data );
		}
	}

	function edit($id){
		if(!$id){
			$this->redirect('module/administration/surat/');
			exit();			
		}
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		$role			= $this->coms->authenticatedUser->role;
		if($role!="mahasiswa"){
			$msurat 		= new model_surat();
			$mjenis 		= new model_jenis();
			$mperihal		= new model_perihal();
			$mtemplate 		= new model_template();
			$fakultas		= $this->coms->authenticatedUser->fakultas;
			
			$data['perihal'] 		= $mperihal->read();
			$data['jenis'] 			= $mjenis->read();
			$data['urut']			= $msurat->urut(date('Y'));
			if($fakultas == "-"){
				$data['fakultas'] 	= $msurat->get_fakultas();
			}else $data['fakultas'] = $msurat->get_fakultas($fakultas);
			$data['cabang'] 		= $msurat->get_cabangub();
			$data['unit_kerja'] 	= $msurat->get_unit_kerja_surat();
			$data['posts'] 			= $msurat->read($id,"","",$fakultas);
			$data['posts_single'] 	= $msurat->read($id,"","single",$fakultas);
			$data['template'] 		= $mtemplate->read("","umum");
			$data['id'] 			= $id;
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->add_script('js/surat.js');
			
			$this->view( 'surat/edit.php', $data );
		}
	}
	
	function display($surat=NULL){
		if(!$surat){
			$this->redirect('module/administration/surat/');
			exit();			
		}
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		$role			= $this->coms->authenticatedUser->role;
		if($role!="mahasiswa"){
			$msurat = new model_surat();	
			if($fakultas == "-"){
				$data['fakultas'] 	= $msurat->get_fakultas();
			}else $data['fakultas'] = $msurat->get_fakultas($fakultas);
			$data['cabang'] 		= $msurat->get_cabangub();
			$data['posts'] 			= $msurat->read($surat,"","",$fakultas);
			$data['posts_single'] 	= $msurat->read($surat,"","single",$fakultas);
			$data['id'] 			= $surat;
			$data['disposisi']		= $msurat->disposisi($surat);
			
			if(isset($_POST['id'])){
				$data['edit']		= $msurat->disposisi($surat,$_POST['id']);
			}
			
			//var_dump($data['disposisi']);
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->add_script('js/surat.js');
			
			$this->view( 'surat/display.php', $data );
		}
	}
	
	function save_surat(){
		ob_start();
		$msurat = new model_surat();
		$mjenis = new model_jenis();
		$mperihal = new model_perihal();
		$minstansi = new model_instansi();
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=""){
			$surat_id	= $_POST['hidId'];
		}
		else{
			$surat_id	= $msurat->surat_id();
		}
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		$perihal_surat	= $_POST['index_perihal'];
		$jenis_surat 	= $_POST['jenis'];
		$kategori_surat = $_POST['kategori'];
		$unit_kerja_surat = $_POST['unit_kerja'];
		$periode_surat 	= $_POST['periode'];
		$sifat_surat 	= $_POST['sifat'];
		$derajat_surat 	= $_POST['derajat'];
		$urut_surat 	= $_POST['urut'];
		$no_surat 		= $_POST['nosurat'];
		$perihal	 	= $_POST['perihal'];
		$tujuan_surat 	= $_POST['tujuan'];
		$dari_id 		= $_POST['dari_id'];
		$dari_nama 		= $_POST['hidden-dari'];
		$isi_surat 		= $_POST['isi_surat'];
		$tgl_surat		= $_POST['tanggal_surat'];
		$tgl_proses		= date("Y-m-d");
		$pengolah_id	= $_POST['pengolah_id'];
		$pengolah_nama 	= $_POST['hidden-pengolah'];
		$kurir_id		= $_POST['pengantar_id'];
		$kurir_nama 	= $_POST['hidden-pengantar'];
		$lampiran 		= $_POST['lampiran'];
		$status_proses 	= $_POST['status_proses'];
		if(isset($_POST['dinas'])&&$_POST['dinas']==1){
			$is_dinas	= 1;
		}
		else{
			$is_dinas	= 0;
		}
		$keterangan 	= $_POST['keterangan'];
		$user 			= $this->coms->authenticatedUser->id;
		$last_update	= date("Y-m-d H:i:s");
		
		$from 		= explode(',', $dari_nama);
		$from_id	= explode(',', $dari_id);
		$pengolah 	= explode(',', $pengolah_nama);
		$pengolahid	= explode(',', $pengolah_id);
		$kurir 		= explode(',', $kurir_nama);
		$kuririd	= explode(',', $kurir_id);
		if(isset($_POST['kepada_id'])){
			$kepadaid_ = $_POST['kepada_id'];
			$kepadaid  = explode(',', $kepadaid_);
		}
		if(isset($_POST['kirim_id'])){
			$kirimid_ = $_POST['kirim_id'];
			$kirim_id  = explode(',', $kirimid_);
		}
		
		if((count($from))==1 && (count($pengolah))==1 && (count($kurir))<2 && $tujuan_surat!='0'){ //syarat pengolah dan pengirim = 1
			
			//=====dari=====//
			$nama_pengirim 	= $from[0];
			$id_pengirim 	= $from_id[0];
			
			$nama_pengolah 	= $pengolah[0];
			$id_pengolah 	= $pengolahid[0];
			
			if($id_pengirim==""){ //dari luar
				$instansi_from		= $_POST['instansi_from'];
				$cek_from = $msurat->cek_instansi($instansi_from);
				if(!$cek_from){
					$ins = Array(
							   'instansi_surat'=>$instansi_from,
							   'alamat'=>"",
							   'telp'=>""
						   );
					$minstansi->replace_instansi($ins);
				}
			}
			
			//=====surat=====//
			if($_POST['file_loc']==""){
				$form 		= $_FILES['files'];
				$month 		= date('m');
				$year 		= date('Y');
				$id_staff	= $this->coms->authenticatedUser->staffid;
				$fakultasid	= $msurat->get_karyawan("", "","", $id_staff)->fakultas_id;
				$dir 		= 'assets/upload/file/'. $fakultasid . "/surat/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
				$dir_db 	= 'upload/file/'. $fakultasid . "/surat/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
				$loc 		= $this->upload_file_surat($form, $dir, $dir_db);
				if($loc!="" || $loc!="Maaf, tipe data file yang anda upload tidak diperbolehkan!"){
					$file_loc = $loc;
				}else $file_loc = "";
			}
			else{
				$file_loc = $_POST['file_loc'];
			}
			
			if($tujuan_surat=="kedalam"){
				if($_POST['file_kirim_loc']==""){
					$form_kirim	= $_FILES['file_kirim'];
					$month 		= date('m');
					$year 		= date('Y');
					$id_staff	= $this->coms->authenticatedUser->staffid;
					$fakultasid	= $msurat->get_karyawan("", "","", $id_staff)->fakultas_id;
					$dir_kirim	= 'assets/upload/file/'. $fakultasid . "/surat/bukti-terima/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
					$dir_kirim_db	= 'upload/file/'. $fakultasid . "/surat/bukti-terima/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
					$loc_kirim 		= $this->upload_file_surat($form_kirim, $dir_kirim, $dir_kirim_db);
					if($loc_kirim!="" || $loc_kirim!="Maaf, tipe data file yang anda upload tidak diperbolehkan!"){
						$file_loc_kirim = $loc_kirim;
					}else $file_loc_kirim = "";
				}
				else{
					$file_loc_kirim = $_POST['file_kirim_loc'];
				}
			}
			
			$data_surat		= array(
								"surat_id"=>$surat_id,
								"fakultas_id"=>$fakultas,
								"perihal_surat"=>$perihal_surat,
								"jenis_surat"=>$jenis_surat,
								"kategori_surat"=>$kategori_surat,
								"unit_kerja_surat"=>$unit_kerja_surat,
								"periode_surat"=>$periode_surat,
								"sifat_surat"=>$sifat_surat,
								"derajat_surat"=>$derajat_surat,
								"no_urut"=>$urut_surat,
								"no_surat"=>$no_surat,
								"perihal"=>$perihal,
								"tujuan_surat"=>$tujuan_surat,
								"dari_id"=>$id_pengirim,
								"dari_nama"=>$nama_pengirim,
								"isi_surat"=>$isi_surat,
								"tgl_surat"=>$tgl_surat,
								"tgl_proses"=>$tgl_proses,
								"pengolah_id"=>$id_pengolah,
								"pengolah_nama"=>$nama_pengolah,
								"lampiran"=>$lampiran,
								"file_loc"=>$file_loc,
								"status_proses"=>$status_proses,
								"is_dinas"=>$is_dinas,
								"keterangan"=>$keterangan,
								"user_id"=>$user,
								"last_update"=>$last_update
							  );
			$msurat->replace_surat($data_surat);
			
			//=====kepada=====//
			if($tujuan_surat=="kedalam"){
				if($_POST['kepada'] == "karyawan"){
					$to	= $_POST['karyawan'];
				}else{
					$to	= $_POST['mhs'];
				}
				$kepada = $to;
				// echo count($kepada);
				for($j=0;$j<count($kepada);$j++){
					$kpd 		= $kepada[$j];
					if($_POST['kepada'] == "karyawan"){
						$data_karyawan 	= $msurat->get_nama_unitkerja($kpd);
						$nama_kepada 	= $data_karyawan->fullname;
						$unit_kerja		= $data_karyawan->unit_kerja;
						$mhs_id			= "";
						$karyawan_id	= $kpd;
					}
					else if($_POST['kepada'] == "mahasiswa"){
						$data_mhs 		= $msurat->get_nama_mhs($kpd);
						$nama_kepada 	= $data_mhs->nama;
						$unit_kerja		= "";
						$mhs_id			= $kpd;
						$karyawan_id	= "";
					}
					
					if((!isset($_POST['kepada_id'])) || (isset($_POST['kepada_id']) && $_POST['kepada_id']=="")){
						$kpd_id	= $msurat->kepada_id();
					}
					else{
						if($kepadaid[$j]==""){
							$kpd_id	= $msurat->kepada_id();
						}else{
							$kpd_id	  = $kepadaid[$j];
						}
					}
					
					$data_kepada = array(
										  'kepada_id' => $kpd_id,
										  'karyawan_id' => $karyawan_id,
										  'mahasiswa_id' => $mhs_id,
										  'surat_id' => $surat_id,
										  'nama' => $nama_kepada,
										  'instansi_surat' => "",
										  'instansi_alamat' => "",
										  'unit_kerja' => $unit_kerja,
										  'tgl_proses' => date("Y-m-d H:i:s"),
										  'user_id' => $user,
										  'last_update' => $last_update
										);
					$msurat->replace_kepada($data_kepada);
					
					//kirim
					if($kategori_surat == "masuk"){
						if(isset($_POST['kirim_id'])&&$_POST['kirim_id']!=""){
							if($kirim_id[$j]==""){
								$kirimid	= $msurat->kirim_id();
							}
							else{
								$kirimid	  = $kirim_id[$j];
							}
						}else $kirimid = $msurat->kirim_id();
						$nama_kurir 	= $kurir[0];
						$id_kurir	 	= $kuririd[0];
						$tgl_surat_dikirim	= $_POST['tanggal_surat_kirim'];
						$data_kirim 	= array(
											  'kirim_id'=>$kirimid,
											  'kepada_id'=>$kpd_id,
											  'karyawan_id'=>$id_kurir,
											  'kirim_oleh'=>$nama_kurir,
											  'tgl_kirim'=>$tgl_surat_dikirim,
											  'diterima_oleh'=>$this->coms->authenticatedUser->name,
											  'tgl_terima'=>date("Y-m-d"),
											  'file_loc'=>$file_loc_kirim,
											  'user_id' => $user,
											  'last_update' => $last_update
											);
						$msurat->replace_kirim($data_kirim);
					}
					else{
						if(isset($_POST['kirim_id'])&&$_POST['kirim_id']!=""){
							$msurat->delete_kirim_id($kirim_id[$j]);
						}
					}
				}
			}
			else{ //tujuan keluar
					$to			= $_POST['nama'];
					$instansi_to= $_POST['instansi'];
					$kepada = $to;
					for($k=0;$k<count($to);$k++){
						$data_instansi	= $minstansi->read($instansi_to[$k],"","getrow");
						if($data_instansi){	
							$instansi_alamat = $data_instansi->alamat;
						}
						else{ // instansi belum ada di db
							$cek_from = $msurat->cek_instansi($instansi_to[$k]);
							if(!$cek_from){
								$ins = Array(
										   'instansi_surat'=>$instansi_to[$k],
										   'alamat'=>"",
										   'telp'=>""
									   );
								$minstansi->replace_instansi($ins);
								$instansi_alamat = "";
							}
						}
						if((!isset($_POST['kepada_id'])) || (isset($_POST['kepada_id']) && $_POST['kepada_id']=="")){
							$kpd_id	= $msurat->kepada_id();
						}
						else{
							if($kepadaid[$k]==""){
								$kpd_id	= $msurat->kepada_id();
							}else{
								$kpd_id	  = $kepadaid[$k];
							}
						}
						$data_kepada = array(
										  'kepada_id' => $kpd_id,
										  'karyawan_id' =>"",
										  'mahasiswa_id' =>"",
										  'surat_id' => $surat_id,
										  'nama' => $to[$k],
										  'instansi_surat' => $instansi_to[$k],
										  'instansi_alamat' => $instansi_alamat,
										  'unit_kerja' => "",
										  'tgl_proses' => date("Y-m-d H:i:s"),
										  'user_id' => $user,
										  'last_update' => $last_update
										);
						$msurat->replace_kepada($data_kepada);
					}
			}
			if(isset($_POST['kepada_id'])){ //DELETE KELEBIHAN KEPADA ID SAAT EDIT
				$count_kpd = count($kepada);
				$selisih = count($kepadaid) - $count_kpd;
				if(count($kepada)<count($kepadaid)){
					for($x=0;$x<$selisih;$x++){
						$y = $count_kpd;
						$delete = $msurat->delete_kepada_id($kepadaid[$y]);
						$y++;
					}
				}
			}
			
			if(isset($_POST['kirim_id'])){ //DELETE KELEBIHAN Kirim ID SAAT EDIT
				$count_kpd = count($kepada);
				$selisih = count($kirim_id) - $count_kpd;
				if(count($kepada)<count($kirim_id)){
					for($x=0;$x<$selisih;$x++){
						$y = $count_kpd;
						$delete = $msurat->delete_kirim_id($kirim_id[$y]);
						$y++;
					}
				}
			}
			echo "Berhasil";
		}
		else{
			echo "Pengirim dan pengolah surat tidak boleh lebih dari satu!";
		}
	}
	
	function kirim($id=NULL){
		if(!$id){
			$this->redirect('module/administration/surat/');
			exit();
		}
		if(($this->coms->authenticatedUser->role =='dosen') || ($this->coms->authenticatedUser->role =='mahasiswa')) $this->coms->no_privileges();
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		$role			= $this->coms->authenticatedUser->role;
		if($role!="mahasiswa"){
			$msurat 	= new model_surat();
			$mjenis 	= new model_jenis();
			$mperihal 	= new model_perihal();
			$minstansi 	= new model_instansi();
			
			$data['id'] 		= $id;
			$data['posts'] 		= $msurat->read($id,"","",$fakultas);
			$data['posts_single'] = $msurat->read($id,'',"single",$fakultas);
			$data['fakultas'] 	= $msurat->get_fakultas();
			$data['cabang'] 	= $msurat->get_cabangub();
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->add_script('js/surat.js');
			
			$this->view('surat/kirim.php', $data);
		}
	}

	function save_kirim(){
		ob_start();
		$msurat = new model_surat();
		$user 			= $this->coms->authenticatedUser->id;
		$last_update	= date("Y-m-d H:i:s");
		
		
		if(!isset($_POST['tanggal_surat_terima'])){
			$kurir_id		= $_POST['pengantar_id_all'];
			$kurir_nama 	= $_POST['hidden-pengantar'];
			$kurir 			= explode(',', $kurir_nama);
			$kuririd		= explode(',', $kurir_id);			
			if((count($kurir))=='1'){
				$kpd_id				= $_POST['kepada_id'];
				$kirim 				= $_POST['kirim_id'];
				$nama_kurir 		= $kurir[0];
				$id_kurir	 		= $kuririd[0];
				$tgl_surat_dikirim	= $_POST['tanggal_surat_kirim'];
				$keterangan			= $_POST['keterangan'];
				
				for($i=0;$i<count($kpd_id);$i++){
					if($kirim[$i]==""){
						$kirimid = $msurat->kirim_id();
					}else{
						$kirimid = $kirim[$i];
					}
					$data_kirim 	= array(
										  'kirim_id'=>$kirimid,
										  'kepada_id'=>$kpd_id[$i],
										  'karyawan_id'=>$id_kurir,
										  'kirim_oleh'=>$nama_kurir,
										  'tgl_kirim'=>$tgl_surat_dikirim,
										  'keterangan'=>$keterangan,
										  'user_id' => $user,
										  'last_update' => $last_update
										);
					// echo $kirimid;
					$send = $msurat->replace_kirim($data_kirim);
				}
				if($send){
					echo "Berhasil";
					exit();
				}
			}
			else{
				echo "Kurir tidak boleh lebih dari satu!";
				exit();
			}
		}
		else{
			$kirim_id 	= $_POST['kirim_id'];
			
			$form_kirim	= $_FILES['file_kirim'];
			$month 		= date('m');
			$year 		= date('Y');
			$id_staff	= $this->coms->authenticatedUser->staffid;
			$fakultasid	= $msurat->get_karyawan("", "","", $id_staff)->fakultas_id;
			$dir_kirim	= 'assets/upload/file/'. $fakultasid . "/surat/bukti-terima/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
			$dir_kirim_db	= 'upload/file/'. $fakultasid . "/surat/bukti-terima/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
			$loc_kirim 		= $this->upload_file_surat($form_kirim, $dir_kirim, $dir_kirim_db);
			if($loc_kirim!="" || $loc_kirim!="Maaf, tipe data file yang anda upload tidak diperbolehkan!"){
				$file_loc_kirim = $loc_kirim;
			}else $file_loc_kirim = "";
			
			$tgl_terima 	= $_POST['tanggal_surat_terima'];
			
			$terima = $msurat->update_terima($this->coms->authenticatedUser->name, $tgl_terima, $file_loc_kirim,  $user, $last_update, $kirim_id);
			if($terima){
				echo "Berhasil";
				exit();
			}
			else{
				echo "Gagal";
				exit();
			}
		}
	}

	function save_disposisi(){
		$msurat 			= new model_surat();
		$dari_id			= $_POST['dari_id'];
		$dari_nama			= $_POST['dari_nama'];
		$tgl_disposisi		= $_POST['tgl_disposisi'];
		$isi_disposisi		= $_POST['isi_disposisi'];
		$surat_id			= $_POST['hidid_surat'];
		$data_karyawan_id	= $_POST['karyawan_disposisi'];
		$jml_data			= count($data_karyawan_id);
		//echo $jml_data;
		for($i=0;$i<$jml_data;$i++){
			$karyawan_id 	= $data_karyawan_id[$i];
			$data_karyawan 	= $msurat->get_nama_unitkerja($karyawan_id);
			$nama_kepada 	= $data_karyawan->fullname;
			if(isset($_POST['hidid_disposisi'])&&$_POST['hidid_disposisi']!=""){
				$disposisi_id	= $_POST['hidid_disposisi'];
			}
			else{
				$disposisi_id	= $msurat->disposisi_id();
			}
			$data_disposisi	= array(
									'disposisi_id'=>$disposisi_id,
									'dari_id'=>$dari_id,
									'kepada_id'=>$karyawan_id,
									'surat_id'=>$surat_id,
									'catatan'=>$isi_disposisi,
									'tgl_disposisi'=>$tgl_disposisi,
									'dari_nama'=>$dari_nama,
									'kepada_nama'=>$nama_kepada
								   );
								   var_dump($data_disposisi);
			$save_disposisi	= $msurat->replace_disposisi($data_disposisi);
		}
		if($save_disposisi){
			echo "Berhasil";
		}
		else{
			echo "Gagal";
		}
	}

	function delete_disposisi(){
		$msurat 		= new model_surat();
		$disposisiid	= $_POST['id'];
		$delete			= $msurat->delete_disposisi($disposisiid);
		if($delete){
			echo "Berhasil";
		}
		else{
			echo "Gagal";
		}
	}

	function ijin(){ //=============================SURAT IJIN============================//
		$role			= $this->coms->authenticatedUser->role;
		if($role!="mahasiswa"){
			$msurat 		= new model_surat();
			$mijin 			= new model_ijin();
			$user 			= $this->coms->authenticatedUser->id;
			$kar_id			= $this->coms->authenticatedUser->staffid;

			if(isset($kar_id)&&$kar_id!=""){
				$data['user']	= $msurat->get_karyawan("", "", "",$kar_id);
			
				$data['role']	= $role;
				$data['ijin']			= $mijin->read();
				$data['validasi']		= $mijin->read_surat_ijin("",$kar_id);
				$data['urut']			= $mijin->urut(date('Ym'));
				if($role=="admin"){
					$data['posts']			= $mijin->read_surat_ijin();
				}else{
					$data['posts']			= $mijin->read_surat_ijin($kar_id);
				}
				
			}
			else{
				$data['validasi']		= "";
				$data['urut']			= $mijin->urut(date('Ym'));
				$data['posts']			= "";
			}
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->coms->add_script('ckeditor/ckeditor.js');
			
			$this->add_script('js/ijin.js');
			
			$this->view('ijin/index.php', $data);
		}
	}

	function lihat($str=NULL,$surat=NULL){
		if(!$surat){
			$this->redirect('module/administration/surat/');
			exit();			
		}
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		$role			= $this->coms->authenticatedUser->role;
		if($role!="mahasiswa"){
			$msurat = new model_surat();	
			
			$msurat 		= new model_surat();
			$mijin 			= new model_ijin();
			$user 			= $this->coms->authenticatedUser->id;
			$fakultas		= $this->coms->authenticatedUser->fakultas;
			
			$data['str']	= $str;
			
			if($str == "ijin"){
				$kar_id			= $this->coms->authenticatedUser->staffid;
				$data['header'] = 'Surat Izin';
				if(isset($kar_id)&&$kar_id!=""&&$kar_id!="-"){
					$data['user']			= $msurat->get_karyawan("", "", "",$kar_id);
					//var_dump($data['user']);
					$data['role']			= $role;
					if($role=="admin"){
						$data['posts']		= $mijin->read_surat_ijin('','',$surat);
					}else{
						$data['posts']		= $mijin->read_surat_ijin('','',$surat);
					}
					
				}
				else{
					$data['posts']			= "";
				}
			}
			else if($str == "umum"){
				$data['header'] = 'Surat Menyurat';
				$data['posts'] 	= $msurat->read("","","",$fakultas,$surat);
			}
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('css/datepicker/datepicker.css');
			$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');
			
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->add_script('js/ijin.js');
			
			$this->view( 'ijin/view.php', $data );
		}
	}

	function save_ijin(){
		$mijin = new model_ijin();

		$surat_id	= $mijin->surat_ijin_id();
		$karyawan_id	= $this->coms->authenticatedUser->staffid;
		$kategori_id	= $_POST['kategori_ijin'];
		$tgl_mulai		= $_POST['tgl_mulai'];
		$tgl_selesai	= $_POST['tgl_selesai'];
		$date1			= date_create($tgl_mulai);
		$date2			= date_create($tgl_selesai);
		$inf_jml_hari	= date_diff($date1,$date2)->d;
		$keterangan		= $_POST['keterangan'];
		$periode		= $_POST['periode'];
		$no_urut		= $_POST['urut'];
		$no_surat		= $_POST['urut'];
		$user			= $this->coms->authenticatedUser->id;
		$last_update	= date("Y-m-d H:i:s");
		$cetak_surat	= $_POST['surat'];
		
		$datanya		= array(
								'surat_id'=>$surat_id,
								'karyawan_id'=>$karyawan_id,
								'kategori_id'=>$kategori_id,
								'tgl_mulai'=>$tgl_mulai,
								'tgl_selesai'=>$tgl_selesai,
								'inf_jml_hari'=>$inf_jml_hari,
								'keterangan'=>$keterangan,
								'periode'=>$periode,
								'no_urut'=>$no_urut,
								'no_surat'=>$no_surat,
								'cetak_surat'=>$cetak_surat,
								'user_id'=>$user,
								'last_update'=>$last_update
							   );
		$save			= $mijin->replace_surat_ijin($datanya);
		//echo $save;
		if($save){
			echo "Berhasil!";
			exit();
		}
		else{
			echo "Gagal!";
			exit();
		}
	}

	function validate_ijin(){
		$mijin 			= new model_ijin();
		$urat_id		= $_POST['id'];
		$str			= $_POST['status'];
		if($str=="setuju") $is_valid = 1;
		else $is_valid = 0;
		$validasi_by 	= $this->coms->authenticatedUser->name;
		$tgl_validasi 	= date("Y-m-d");
		$user			= $this->coms->authenticatedUser->id;
		$last_update	= date("Y-m-d H:i:s");
		$update 		= $mijin->validate_ijin($urat_id,$is_valid,$validasi_by,$tgl_validasi,$user,$last_update);
		if($update){
			echo "Berhasil";
		}
		else echo "Gagal";
	}	
	
	
	//========INBOX DOSEN/KARYAWAN=========//
	function inbox(){
		$msurat 		= new model_surat();
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		$role			= $this->coms->authenticatedUser->role;
		if($role&&$role!="admin"&&$role!="mahasiswa"){
			$id	= $this->coms->authenticatedUser->staffid;
			$data['posts']	= $msurat->read("","","",$fakultas,"",$id);
		}
		elseif($role=="mahasiswa"){
			$id	= $this->coms->authenticatedUser->mhsid;
			$data['posts']	= $msurat->read("","","",$fakultas,"","",$id);
		}
		else{
			$this->redirect('module/administration/surat/');
			exit();		
		}
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->coms->add_script('js/jquery/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		
		// $this->coms->add_script('ckeditor/ckeditor.js');
		
		// $this->add_script('js/ijin.js');
		
		$this->view('surat/inbox/index.php', $data);
	}
	
	
	
	//===function tambahan===//
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_instansi(){
		$minstansi = new model_instansi();
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$data = $minstansi->read("",$str);
		
		if($data){
			$return_arr = array();
	
			foreach($data as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			
			$json_response = json_encode($return_arr);
			
			//$data['tes'] = $json_response;
			# Return the response
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}

	function get_prodi(){
		$msurat = new model_surat();
		
		$fakultas 	= $_POST['fakultas'];
		$data = $msurat->get_prodi($fakultas);
		
		echo "<option value='0'>Silahkan Pilih Program Studi</option>";
		if($data){
			foreach($data as $dt){
				echo "<option value='".$dt->hid_id."'>";
				echo "".$dt->keterangan."</option>";
			}
		}
	}
	
	function get_karyawan(){
		$msurat = new model_surat();
		
		$fakultas 	= $_POST['fakultas'];
		$cabang 	= $_POST['cabang'];
		$data = $msurat->get_karyawan("", $cabang, $fakultas);
		
		if($data){
			foreach($data as $dt){
				echo "<option value='".$dt->karyawan_id."'>";
				echo "".$dt->value."</option>";
			}
		}
	}
	
	function get_karyawan_AC(){
		$msurat = new model_surat();
		
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$fakultas 	= $_POST['fakultas'];
		$cabang 	= $_POST['cabang'];
		$data 		= $msurat->get_karyawan($str, $cabang, $fakultas);
		//var_dump($data);
		if($data){
			$return_arr = array();
	
			foreach($data as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			
			$json_response = json_encode($return_arr);
			
			//$data['tes'] = $json_response;
			# Return the response
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}
	
	function get_mhs(){
		$msurat = new model_surat();
		
		$prodi 		= $_POST['prodi'];
		$cabang 	= $_POST['cabang'];
		$data = $msurat->get_mhs($prodi,$cabang);
		// var_dump($data);
		if($data){
			foreach($data as $dt){
				echo "<option value='".$dt->mahasiswa_id."'>";
				echo "".$dt->value."</option>";
			}
		}
	}
	
	function get_template(){
		$mtemplate = new model_template();
		
		$id 	= $_POST['template'];
		
		if($id!="0"){
			$data 	= $mtemplate->read($id);
			if($data->kop_surat!=""){
				echo "<div><img width='100%' src='".$this->config->file_url_view.'/'.$data->kop_surat."'></div>";
			}
			echo $data->isi_surat;
		}
		else{
			
		}
	}
	
	function upload_file_surat($form=NULL, $dir=NULL, $dir_db=NULL){
		foreach ($form as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		if($cekicon!='error'){	
			$name 		= $form['name'];
			$ext		= $this->get_extension($name);
			$file_type	= $form["type"]; 
			$file_size	= $form["size"];
			
			$upload_dir = $dir;
			$upload_dir_db 	= $dir_db;
			$allowed_ext 	= array('jpg','jpeg','png', 'gif', 'pdf', 'ppt', 'pptx', 'doc', 'docx');
			if(!in_array($ext,$allowed_ext)){
				echo "Maaf, tipe data file yang anda upload tidak diperbolehkan!";
				exit();
			}
			else{
				// if(!file_exists($upload_dir)) {
					// mkdir($upload_dir, 0777, true);
					// chmod($upload_dir, 0777);
				// }
				$file_loc = $upload_dir_db . $name;
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $form['tmp_name'];
					$filename    = $name;
					$filenamenew = $name;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
					$ch = curl_init();
					$options = array(
						CURLOPT_URL => $url,
						CURLOPT_HEADER => true,
						CURLOPT_POST => 1,
						CURLOPT_HTTPHEADER => $headers,
						CURLOPT_POSTFIELDS => $postfields,
						CURLOPT_INFILESIZE => $filesize,
						CURLOPT_RETURNTRANSFER => true,
						// CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
						CURLOPT_SSL_VERIFYPEER => false,
						CURLOPT_SSL_VERIFYHOST => 2
					); // cURL options 
					curl_setopt_array($ch, $options);
					curl_exec($ch);
					if(!curl_errno($ch))
					{
						$info = curl_getinfo($ch);
						if ($info['http_code'] == 200)
						   $errmsg = "File uploaded successfully";
					}
					else
					{
						$errmsg = curl_error($ch);
					}
					curl_close($ch);
					//------UPLOAD USING CURL----------------
					//rename($form['tmp_name'], $upload_dir . $name);
				}
				
				return $file_loc;
			}
		}
	}
	
	function get_dari_surat(){
		$msurat = new model_surat();
		
		$surat	= $_POST['surat_id'];
		$fakultas		= $this->coms->authenticatedUser->fakultas;
		$data	= $msurat->read($surat,"","",$fakultas);
		//echo $data;
		// var_dump($data);
		if($data){
			$return_arr = array();
	
			foreach($data as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			
			$json_response = json_encode($return_arr);
			
			//$data['tes'] = $json_response;
			# Return the response
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}
	
	function get_karyawan_detail(){
		$msurat = new model_surat();
		$id	= $_POST['id'];
		$data		= $msurat->get_karyawan("","","",$id);
		
		if($data){
			$return_arr = array();
	
			//foreach($data as $row){
				foreach ($data as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			//}
			
			$json_response = json_encode($return_arr);
			
			//$data['tes'] = $json_response;
			# Return the response
			echo $json_response;
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
		}
	}
	
}
?>