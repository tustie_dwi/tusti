<?php
class administration_conf extends comsmodule {
	private $coms;
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
		
	//----- JENIS-------------------------------------------------------------
	function surat(){
		$mjenis = new model_jenis();
		
		$data['header'] = 'Jenis Surat';
		
		$data['param']	= 'New Jenis Surat';
		
		$data['posts'] 	= $mjenis->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');	
		$this->add_script('js/jenis.js');
		
		$this->view( 'jenis_surat/index.php', $data );
		
	}
	
	function save_jenis_ToDB(){
		$mjenis = new model_jenis();
		$result = '';
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$jenis_surat_id = $_POST['hidId'];
			$jenis_surat = $_POST['jenissurat'];
			$action = 'edit';
		}else{
			$jenis_surat = $_POST['jenissurat'];
			$action = 'new';
		}
		
		$keterangan = $_POST['keterangan'];
		
		if($action == 'new'){
			$datanya = Array(
					   'jenis_surat'=>$jenis_surat,
					   'keterangan'=>$keterangan
					   );
					   
			if($mjenis->replace_jenis($datanya)){
				$result = "Success";
			}else{
				$result = "Failed";
			};
		}else{
			
			if($mjenis->edit_jenis($jenis_surat_id,$jenis_surat,$keterangan)){
				$result = "Success";
			}else{
				$result = "Failed";
			};
			
		}
		echo $result;

	}
	//----- JENIS-------------------------------------------------------------
	
	//----- PERIHAL-----------------------------------------------------------
	function perihal(){
		$mperihal = new model_perihal();
		
		$data['header'] = 'Perihal Surat';
		
		$data['param']	= 'New Perihal Surat';
		
		$data['posts'] 	= $mperihal->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');
		$this->add_script('js/perihal.js');
		
		$this->view( 'perihal/index.php', $data );
	}
	
	function save_perihal_ToDB(){
		$mperihal = new model_perihal();
		$result = '';
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$perihal_surat_id = $_POST['hidId'];
			$perihal_surat = $_POST['perihalsurat'];
			$action = 'edit';
		}else{
			$perihal_surat = $_POST['perihalsurat'];
			$action = 'new';
		}
		
		$keterangan = $_POST['keterangan'];

		if($action == 'new'){
			$datanya = Array(
					   'perihal_surat'=>$perihal_surat,
					   'keterangan'=>$keterangan
					   );
					   
			if($mperihal->replace_perihal($datanya)){
				$result = "Success";
			}else{
				$result = "Failed";
			};
		}else{
			
			if($mperihal->edit_perihal($perihal_surat_id,$perihal_surat,$keterangan)){
				$result = "Success";
			}else{
				$result = "Failed";
			};
			
		}
		echo $result;
	}
	//----- PERIHAL-----------------------------------------------------------
	
	//----- INSTANSI----------------------------------------------------------
	function instansi(){
		$minstansi = new model_instansi();
		
		$data['header'] = 'Instansi';
		
		$data['param']	= 'New Instansi';
		
		$data['posts'] 	= $minstansi->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');	
		$this->add_script('js/instansi.js');
		
		$this->view( 'instansi/index.php', $data );
		
	}
	
	function save_instansi_ToDB(){
		$minstansi = new model_instansi();
		$result = '';
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$instansi_id = $_POST['hidId'];
			$instansi = $_POST['instansi'];
			$action = 'edit';
		}else{
			$instansi = $_POST['instansi'];
			$action = 'new';
		}
		
		$alamat = $_POST['alamat'];
		$tlp = $_POST['tlp'];
		
		if($action == 'new'){
			$datanya = Array(
					   'instansi_surat'=>$instansi,
					   'alamat'=>$alamat,
					   'telp'=>$tlp
					   );
					   
			if($minstansi->replace_instansi($datanya)){
				$result = "Success";
			}else{
				$result = "Failed";
			};
		}else{
			
			if($minstansi->edit_instansi($instansi_id,$instansi,$alamat,$tlp)){
				$result = "Success";
			}else{
				$result = "Failed";
			};
			
		}
		echo $result;

	}
	//----- INSTANSI----------------------------------------------------------
	
	//----- TEMPLATE----------------------------------------------------------
	function template($id=NULL){
		$mtemplate = new model_template();
		$mjenis = new model_jenis();
		$data['header'] = 'Template';
		$data['param']	= 'New Template';
		
		$data['jenis'] 	= $mjenis->read();
		if($id){
			$data['edit'] 	= $mtemplate->read($id);
		}
		$data['posts'] 	= $mtemplate->read();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
		
		$this->add_script('js/template.js');
		
		$this->view( 'template/index.php', $data );
	}
	
	function display($id=NULL){
		$mtemplate = new model_template();
		$mjenis = new model_jenis();
		$data['header'] = 'Template';
		
		$data['posts'] 	= $mtemplate->read($id);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/bootstrap-tree.css');
		$this->coms->add_script('bootstrap/js/bootstrap-tree.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
		
		$this->add_script('js/template.js');
		
		$this->view( 'template/view.php', $data );
	}
	
	function save_template(){
		ob_start();
		$mtemplate = new model_template();
		if(isset($_POST['hidId']) && $_POST['hidId']!=""){
			$template_id	= $_POST['hidId'];
		}
		else{
			$template_id	= $mtemplate->template_id();
		}
		
		$kategori 			= $_POST['kategori'];
		$isi_surat 			= $_POST['isi_surat'];
		if($_POST['jenis']=='0'){
			$jenis				= "Surat Izin";
		}else $jenis				= $_POST['jenis'];
		
		if(isset($_POST['file_loc'])&&$_POST['file_loc']!=""){
			$file_loc 		= $_POST['file_loc'];
		}
		else{
			$form 				= $_FILES['files'];
			$month 				= date('m');
			$year 				= date('Y');
			$id_staff			= $this->coms->authenticatedUser->staffid;
			$fakultasid			= $mtemplate->get_karyawan("", "","", $id_staff)->fakultas_id;
			$dir 				= 'assets/upload/file/'. $fakultasid . "/surat/template/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
			$dir_db 			= 'upload/file/'. $fakultasid . "/surat/template/" .$year. '_' .$month. DIRECTORY_SEPARATOR ;
			$loc 				= $this->upload_file($form, $dir, $dir_db);
			if($loc!="" || $loc!="Maaf, tipe data file yang anda upload tidak diperbolehkan!"){
				$file_loc 		= $loc;
			}else $file_loc 	= "";
		}
		
		$datanya		= array(
								'template_id'=>$template_id,
								'jenis_surat'=>$jenis,
								'kop_surat'=>$file_loc,
								'isi_surat'=>$isi_surat,
								'kategori'=>$kategori
							   );
		$mtemplate->replace_template($datanya);
		echo "Berhasil!";
	}
	
	function upload_file($form=NULL, $dir=NULL, $dir_db=NULL){
		foreach ($form as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
				else $cekicon = 'sukses';
			}
		}
		
		if($cekicon!='error'){	
			$name 		= $form['name'];
			$ext		= $this->get_extension($name);
			$file_type	= $form["type"]; 
			$file_size	= $form["size"];
			
			$upload_dir = $dir;
			$upload_dir_db 	= $dir_db;
			$allowed_ext 	= array('jpg','jpeg','png', 'gif', 'pdf', 'ppt', 'pptx', 'doc', 'docx');
			if(!in_array($ext,$allowed_ext)){
				echo "Maaf, tipe data file yang anda upload tidak diperbolehkan!";
				exit();
			}
			else{
				// if(!file_exists($upload_dir)) {
					// mkdir($upload_dir, 0777, true);
					// chmod($upload_dir, 0777);
				// }
				$file_loc = $upload_dir_db . $name;
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $form['tmp_name'];
					$filename    = $name;
					$filenamenew = $name;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
					$ch = curl_init();
					$options = array(
						CURLOPT_URL => $url,
						CURLOPT_HEADER => true,
						CURLOPT_POST => 1,
						CURLOPT_HTTPHEADER => $headers,
						CURLOPT_POSTFIELDS => $postfields,
						CURLOPT_INFILESIZE => $filesize,
						CURLOPT_RETURNTRANSFER => true,
						// CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
						CURLOPT_SSL_VERIFYPEER => false,
						CURLOPT_SSL_VERIFYHOST => 2
					); // cURL options 
					curl_setopt_array($ch, $options);
					curl_exec($ch);
					if(!curl_errno($ch))
					{
						$info = curl_getinfo($ch);
						if ($info['http_code'] == 200)
						   $errmsg = "File uploaded successfully";
					}
					else
					{
						$errmsg = curl_error($ch);
					}
					curl_close($ch);
					//------UPLOAD USING CURL----------------
					//rename($form['tmp_name'], $upload_dir . $name);
				}
				
				return $file_loc;
			}
		}
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function delete_template(){
		$mtemplate = new model_template();
		$id = $_POST['id'];
		$data 	= $mtemplate->delete($id);
		if($data){
			echo "Delete Template Berhasil!";
		}
		else echo "Delete Template Gagal!";
	}
	//----- TEMPLATE----------------------------------------------------------
	
	//----- KATEGORI SURAT IJIN----------------------------------------------------------
	function ijin($id=NULL){
		$mijin	= new model_ijin();
		$mtemplate 		= new model_template();
		$data['header'] = 'Kategori Surat Izin';
		
		if($id){
			$data['edit']	= $mijin->read($id);
			$data['param']	= 'Edit Kategori Surat Izin';
		}else $data['param']	= 'Tambah Kategori Surat Izin';
		$data['posts']	= $mijin->read();
		$data['template'] 		= $mtemplate->read("","izin");
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->add_script('js/ijin.js');
		
		$this->view( 'ijin/kategori/index.php', $data );
	}
	
	function save_ijin(){
		$mijin	= new model_ijin();
		
		$keterangan	= $_POST['keterangan'];
		$template	= $_POST['template'];
		
		if($keterangan!=""){
			if(isset($_POST['hidId'])&&$_POST['hidId']!=""){
				$kategori_id = $_POST['hidId'];
			}else $kategori_id = $mijin->kategori_id();
			
			$data = array(
						  'kategori_id'=>$kategori_id,
						  'template_id'=>$template,
						  'keterangan'=>$keterangan
						 );
			$save = $mijin->replace_kategori_ijin($data);
			if($save) echo "Berhasil"; exit();
		}
		else{
			echo "Silahkan Lengkapi Form!";
			exit();
		}
	}
	//----- KATEGORI SURAT IJIN----------------------------------------------------------
}
?>