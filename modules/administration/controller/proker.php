<?php
class administration_proker extends comsmodule {
	private $coms;
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
		
	function index(){
		$mproker = new model_proker();
		$role 	 = $this->coms->authenticatedUser->role;
		$karyawanid = $this->coms->authenticatedUser->staffid;

		$data['header'] = 'Program Kerja';
		
		if($role=='administrator'||$role=='student employee'){
			$data['posts'] 	= $mproker->read();
			$data['view_as']	= 'admin';
		}
		elseif($role=='dosen'){
			
			$jabatan = $mproker->get_jabatan($karyawanid);		
			if(!isset($jabatan)){		
				$data['posts'] 		= $mproker->read('','dosen',$karyawanid,'');
				$data['view_as']	= 'dosen';
			}
			else{
				$jabatankaryawan = explode(' ', $jabatan->jabatan);
				$jabatan_ = $jabatankaryawan[0];
				$unit_ 	  = $jabatan->unit_id;
				$fakultas = $jabatan->fakultas_id;
				
				if($jabatan_=='PD'||$jabatan_=='KTU'){
					$data['posts'] 		= $mproker->read('','PD','',$fakultas);
					$data['view_as']	= 'PD';
				}
				elseif($jabatan_=='Kasubag'){
					$data['posts'] 		= $mproker->read('','kasubag','','',$unit_);
					$data['view_as']	= 'kasubag';
				}
				
			}
			
		}
		
		$data['role'] = $role;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/proker.js');
		
		$this->view( 'proker/index.php', $data );
		
	}
	
	function set_validasi_proker(){
		$mproker = new model_proker();
		
		$rencana_id = $_POST['rencanaid'];
		
		if($_POST['stat']=='approve'){
			$stat = '1';
		}else $stat = '2';
		
		$date	= date("Y-m-d H:i:s");
		$userid = $this->coms->authenticatedUser->staffid;
		
		if($mproker->set_validasi_proker($rencana_id, $stat, $date, $userid)){
			echo 'success';
		}
		
	}
	
	function set_validasi_evaluasi(){
		$mproker = new model_proker();
		
		$kegiatanid = $_POST['kegiatanid'];
		
		if($_POST['stat']=='approve'){
			$stat = '1';
		}else $stat = '2';
		
		$date	= date("Y-m-d H:i:s");
		$userid = $this->coms->authenticatedUser->staffid;
		
		if($mproker->set_validasi_evaluasi($kegiatanid, $stat, $date, $userid)){
			echo 'success';
		}
	}
	
	function detail($str, $id){
		switch ($str) {
			case 'proker':
				$this->detail_proker($id);
			break;
			case 'evaluasi':
				$this->detail_evaluasi($id);
			break;
		}
	}
	
	function detail_proker($id){
		$mproker = new model_proker();
		
		$data['header'] = 'Program Kerja';
		$data['hidId']	= $id;
		$data['posts'] 	= $mproker->read($id);
		$data['detail'] = $mproker->read_detail($id, 'rencanaid', 'parent', 'yes');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/excellentexport.js');
		$this->add_script('js/proker.js');
		
		$this->view( 'proker/detail.php', $data );
	}
	
	function count_tr($detailid, $numbefore){
		$mproker = new model_proker();
		$data	= $mproker->read_detail($detailid, 'parentid', 'child', 'yes');
		
		$total = 0;
		$kebutuhan_child = 0;
		foreach ($data as $d) {
			$kebutuhan_child = $kebutuhan_child + $d->kebutuhan_count;
		}
		$total = $kebutuhan_child + $numbefore;
		return $total;
	}
	
	function get_detail_($detailid,$kegiatanparent,$no){
		$mproker = new model_proker();
		$i = 0;
		$keg = 0;
		$kegcur = '';
		$posts 	= $mproker->read_detail($detailid, 'parentid', 'child');
		if($posts){
			foreach ($posts as $p) {
				$data = $mproker->read_kebutuhan($p->detail_id,'detailid');
				$keg = 0;
				$kegcur = $p->kegiatan;
				if(isset($data)):
				foreach ($data as $d) {
					echo '<tr>
							<td>'; if($i==0){echo $no;} echo'</td>';
					echo '	<td>';
							if($i==0){echo $kegiatanparent;}
					echo '	</td>
							<td>';
							if($keg==0){
								echo $kegcur;
								$keg=1;
							};
							
					echo '	</td>
							<td>
							'.$d->qty.'
							</td>
							<td>
							'.$d->keterangan.'
							</td>
							<td>
							'. 'Rp. ' . number_format( $d->harga, 0 , '' , ',' ) .'
							</td>
							<td>
							'. 'Rp. ' . number_format( $d->total_biaya, 0 , '' , ',' ) .'
							</td>
						</tr>';
						$i++;
				}
				else:
					echo '<tr>
							<td>'; if($i==0){echo $no;} echo'</td>';
					echo '	<td>';
							if($i==0){echo $kegiatanparent;}
					echo '	</td>
							<td>';
							if($keg==0){
								echo $kegcur;
								$keg=1;
							};
							
					echo '	</td>
							<td>
							</td>
							<td>
							</td>
							<td>
							</td>
							<td>
							</td>
						</tr>';
						$i++;
				endif;
			}
		}
		
	}
	
	function write($str){
		switch ($str) {
			case 'proker':
				$this->write_proker();
			break;
			case 'evaluasi':
				$this->write_evaluasi();
			break;
		}
	}
	
	function write_proker(){
		$mproker = new model_proker();
		
		$role 	 = $this->coms->authenticatedUser->role;
		$karyawanid = $this->coms->authenticatedUser->staffid;
		
		$unit_sess 		= $this->coms->authenticatedUser->unit;
		$data['unit_sess'] = $unit_sess;
		
		if($role=='administrator'||$role=='student employee'){
			$data['unitkaryawan']	= $mproker->get_unit_admin();
		}
		elseif($role=='dosen'){
			$jabatan = $mproker->get_jabatan($karyawanid);		
			if(!isset($jabatan)){		
				$data['unitkaryawan']	= $mproker->get_unit($karyawanid);
			}
			else{
				$jabatankaryawan = explode(' ', $jabatan->jabatan);
				$jabatan_ = $jabatankaryawan[0];
				$unit_ 	  = $jabatan->unit_id;
				$fakultas = $jabatan->fakultas_id;
				
				if($jabatan_=='PD'||$jabatan_=='KTU'){
					$data['unitkaryawan'] 	= $mproker->get_unit_admin('PD',$fakultas);
				}
				elseif($jabatan_=='Kasubag'){
					$data['unitkaryawan'] 	= $mproker->get_unit_admin('kasubag',$unit_);
				}
				
			}
		}
		
		$data['header'] = 'Program Kerja';
		$data['param']	= 'New Program Kerja';
		$data['posts'] 	= '';
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/proker.js');
		
		$this->view( 'proker/edit.php', $data );
		
	}

	function edit($str,$id){
		switch ($str) {
			case 'proker':
				$this->edit_proker($id);
			break;
			case 'evaluasi':
				$this->edit_evaluasi($id);
			break;
		}
	}
	
	function edit_proker($id){
		$mproker = new model_proker();
		
		$role 	 = $this->coms->authenticatedUser->role;
		$karyawanid = $this->coms->authenticatedUser->staffid;
		
		$unit_sess 		= $this->coms->authenticatedUser->unit;
		$data['unit_sess'] = $unit_sess;
		
		$data['header'] = 'Program Kerja';
		$data['param']	= 'Edit Program Kerja';
		$data['posts'] 	= $mproker->read($id);
		$data['detail'] = $mproker->read_detail($id, 'rencanaid', 'parent');
		
		if($role=='administrator'||$role=='student employee'){
			$data['unitkaryawan']	= $mproker->get_unit_admin();
		}
		elseif($role=='dosen'){
			$jabatan = $mproker->get_jabatan($karyawanid);		
			if(!isset($jabatan)){		
				$data['unitkaryawan']	= $mproker->get_unit($karyawanid);
			}
			else{
				$jabatankaryawan = explode(' ', $jabatan->jabatan);
				$jabatan_ = $jabatankaryawan[0];
				$unit_ 	  = $jabatan->unit_id;
				$fakultas = $jabatan->fakultas_id;
				
				if($jabatan_=='PD'||$jabatan_=='KTU'){
					$data['unitkaryawan'] 	= $mproker->get_unit_admin('PD',$fakultas);
				}
				elseif($jabatan_=='Kasubag'){
					$data['unitkaryawan'] 	= $mproker->get_unit_admin('kasubag',$unit_);
				}
				
			}
		}
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/proker.js');
		
		$this->view( 'proker/edit.php', $data );
		
	}
	
	function get_kebutuhan($detailid,$param){
		$mproker = new model_proker();
		$data['posts'] 	= $mproker->read_kebutuhan($detailid,'detailid');
		$data['param']	= $param;
		$this->view( 'proker/child-kebutuhan.php', $data );
	}
	
	function get_detail_child($detailid){
		$mproker = new model_proker();
		$data['posts'] 	= $mproker->read_detail($detailid, 'parentid', 'child');
		$data['parent'] = $detailid;
		$data['view_'] = 'edit';
		$this->view( 'proker/child-detail.php', $data );
	}
	
	function delete_(){
		$result = '';
		$mproker 	= new model_proker();
		switch ($_POST['str']) {
			//---proker-----------------------------------
			case 'kebutuhan':
				$kebutuhanid = $_POST['kebutuhanid'];
				if($mproker->delete_kebutuhan($kebutuhanid)){
					$result = 'Success';
				}else $result = 'Failed';
			break;
			case 'child':
				$detailid = $_POST['detailid'];
				if($mproker->delete_detail($detailid)){
					$result = 'Success';
				}else $result = 'Failed';
			break;
			case 'parent':
				$detailid = $_POST['detailid'];
				$data = $mproker->det_all_detail_child($detailid);
				if($mproker->delete_detail($detailid)){
					$result = 'Success';
					foreach ($data as $d) {
						$mproker->delete_detail($d->detailid);
					}
				}else $result = 'Failed';
			break;
			//---evaluasi----------------------------------
			case 'target':
				$kegiatanid = $_POST['targetid'];
				if($mproker->delete_kegiatan($kegiatanid,'target')){
					$result = 'Success';
				}else $result = 'Failed';
			break;
			case 'baseline':
				$baselineid = $_POST['baselineid'];
				if($mproker->delete_baseline($baselineid)){
					$result = 'Success';
				}else $result = 'Failed';
			break;
			case 'kegiatan':
				$kegiatanid = $_POST['kegiatanid'];
				if($mproker->delete_kegiatan($kegiatanid,'kegiatan')){
					$result = 'Success';
				}else $result = 'Failed';
			break;
		}
		
		echo $result;
	}
	
	function save_proker_toDB(){
		$mproker 	= new model_proker();
		
		$user 	 	= $this->coms->authenticatedUser->role;
		$lastupdate	= date("Y-m-d H:i:s");
		$karyawanid = $this->coms->authenticatedUser->staffid;
		$userid		= $this->coms->authenticatedUser->id;
		
		$judul 	 = $_POST['judul'];
		$periode = $_POST['periode'];
		$unit	 = $_POST['select_unit'];
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$rencana_id = $_POST['hidId'];
			$action = 'edit';
		}else{
			$rencana_id = $mproker->get_rencana_id();
			$action = 'new';
		}
		
		$kegiatan  = $_POST['kegiatan'];
		$type 	   = $_POST['type'];
		
		if(isset($_POST['count-kebutuhan'])&&$_POST['count-kebutuhan']!=''){
			$ckebutuhan= $_POST['count-kebutuhan'];
		}else $ckebutuhan=0;
		
		if(isset($_POST['kebutuhan'])&&$_POST['kebutuhan']!=''){
			$kebutuhan= $_POST['kebutuhan'];
		}else $kebutuhan='';
		
		if(isset($_POST['qty'])&&$_POST['qty']!=''){
			$qty= $_POST['qty'];
		}else $qty='';
		
		if(isset($_POST['harga'])&&$_POST['harga']!=''){
			$harga= $_POST['harga'];
		}else $harga='';
		
		if(isset($_POST['total'])&&$_POST['total']!=''){
			$total= $_POST['total'];
		}else $total='';
		
		$stproses  = $_POST['stproses'];
		
		$datanya = Array(
				   'rencana_id'=>$rencana_id,
				   'karyawan_id'=>$karyawanid,
				   'periode'=>$periode,
				   'judul'=>$judul,
				   'unit_id'=>$unit,
				   'user_id'=>$userid,
				   'last_update'=>$lastupdate,
				   'st_proses'=>$stproses,
				   'is_valid'=>'0'
				   );
				   
		if($mproker->replace_rencana_kerja($datanya)){
			$result = "Success";
			$this->save_rencana_detail_toDB($action,$rencana_id,$kegiatan,$type,$kebutuhan,$qty,$harga,$total,$ckebutuhan);
		}else{
			$result = "Failed";
			echo $result;
			exit;
		};
		
		echo $result;
		exit;
		
	}
	
	function save_rencana_detail_toDB($action,$rencana_id,$kegiatan,$type,$kebutuhan,$qty,$harga,$total,$ckebutuhan){
		$mproker 	= new model_proker();
		$c = 0;
		for($i=0;$i<count($kegiatan);$i++){
			// if($action == 'new'){
			if(isset($_POST['hidId2'][$i])&&$_POST['hidId2'][$i]!=''){
				$detail_id = $_POST['hidId2'][$i];
			}else{
				$detail_id = $mproker->get_detail_id();
			}
			
			$datanya = Array(
							'detail_id'=>$detail_id,
						   	'rencana_id'=>$rencana_id,
						   	'kegiatan'=>$kegiatan[$i]
						   );
			
			switch ($type[$i]) {
				case 'parent':
					$parentid = $detail_id;
				break;
				case 'child':
					$datachild = Array('parent_id'=>$parentid);
					$datanya = $datanya + $datachild;
				break;
			}

			if($mproker->replace_rencana_kerja_detail($datanya)){
				$result = "Success";
				if((isset($ckebutuhan[$i]))&&$ckebutuhan[$i]!='0'){
					$c = $this->save_rencana_kebutuhan_toDB($action,$detail_id,$kebutuhan,$qty,$harga,$total,$ckebutuhan[$i],$c);
				}
			}else{
				$result = "Failed";
				echo $result;
				exit;
			};
			
		} /* end for */
		
	}
	
	function save_rencana_kebutuhan_toDB($action,$detail_id,$kebutuhan,$qty,$harga,$total,$ckebutuhan,$c){
		$mproker 	= new model_proker();
		
		for($x=0;$x<$ckebutuhan;$x++){
			// if($action == 'new'){
			if(isset($_POST['hidId3'][$c])&&$_POST['hidId3'][$c]!=''){
				$kebutuhan_id = $_POST['hidId3'][$c];
			}else{
				$kebutuhan_id = $mproker->get_kebutuhan_id();
			}
			
			$datanya = Array(
							'kebutuhan_id'=>$kebutuhan_id,
							'detail_id'=>$detail_id,
						   	'keterangan'=>$kebutuhan[$c],
						   	'qty'=>$qty[$c],
						   	'harga'=>$harga[$c],
						   	'total_biaya'=>$total[$c]
						   );
			
			if($mproker->replace_rencana_kerja_kebutuhan($datanya)){
				$result = "Success";
			}else{
				$result = "Failed";
				echo $result;
				exit;
			};
			// echo $c;
			$c++;
		} /* end for */
		
		return $c;
		
	}
	
	//===== EVALUASI ======
	
	function evaluasi(){
		$mproker 	= new model_proker();
		
		$data['header'] = 'Pelaksanaan Proker';
				
		$data['posts'] = $mproker->read_detail_evaluasi('','');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/evaluasi.js');
		
		$this->view( 'evaluasi/index.php', $data );
	}
	
	function read_evaluasi($detail_id){
		$mproker 	= new model_proker();
		
		$role 	 = $this->coms->authenticatedUser->role;
		$karyawanid = $this->coms->authenticatedUser->staffid;
		
		if($role=='administrator'||$role=='student employee'){
			$data['posts'] 	= $mproker->read_evaluasi($detail_id, 'detailid' ,'kegiatan');
			$data['view_as']	= 'admin';
		}
		elseif($role=='dosen'){
			
			$jabatan = $mproker->get_jabatan($karyawanid);		
			if(!isset($jabatan)){
				$data['posts'] 	= $mproker->read_evaluasi($detail_id, 'detailid' ,'kegiatan' ,'dosen', $karyawanid);		
				$data['view_as']	= 'dosen';
			}
			else{
				$jabatankaryawan = explode(' ', $jabatan->jabatan);
				$jabatan_ = $jabatankaryawan[0];
				$unit_ 	  = $jabatan->unit_id;
				$fakultas = $jabatan->fakultas_id;
				
				if($jabatan_=='PD'||$jabatan_=='KTU'){
					$data['posts'] 	= $mproker->read_evaluasi($detail_id, 'detailid' ,'kegiatan' ,'PD', '', $fakultas);	
					$data['view_as']	= 'PD';
				}
				elseif($jabatan_=='Kasubag'){
					$data['posts'] 	= $mproker->read_evaluasi($detail_id, 'detailid' ,'kegiatan' ,'kasubag', '', '',$unit_);	
					$data['posts'] 		= $mproker->read('','kasubag','','',$unit_);
					$data['view_as']	= 'kasubag';
				}
				
			}
			
		}
		
		$data['role'] = $role;
		// $data['posts'] 	= $mproker->read_evaluasi($detail_id, 'detailid' ,'kegiatan');
		$this->view( 'evaluasi/evaluasi_child.php', $data );
	}
	
	function write_evaluasi(){
		$mproker = new model_proker();
		
		$karyawanid 		= $this->coms->authenticatedUser->staffid;
		$role				= $this->coms->authenticatedUser->role;
		
		$data['header'] 	= 'Pelaksanaan Proker';
		$data['param']		= 'New Pelaksanaan Proker';
		$data['posts'] 		= '';
		
		$data['proker']		= $mproker->read();
		$data['detailid'] 	= '';
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js'); 
		$this->add_script('js/evaluasi.js');
		
		$this->view( 'evaluasi/edit.php', $data );
		
	}
	
	function get_library_content(){
		$mproker = new model_proker();
		
		$karyawanid 		= $this->coms->authenticatedUser->staffid;
		$role				= $this->coms->authenticatedUser->role;
		
		if(isset($_POST['folderid'])&&$_POST['folderid']!=''){
			
			$data['count_folder'] = ($_POST['count']+1);
			$folderid = $_POST['folderid'];
			
			$data['file']	= $mproker->read_file('', $folderid);
			if($role=='dosen'){
				$data['folder']		= $mproker->read_folder($karyawanid, '', $folderid);
			}elseif($role=='student employee'||$role=='admin'){
				$data['folder']		= $mproker->read_folder('', '', $folderid);
			}
		}else {
			$folderid = '';
			$data['file']	= $mproker->read_file('no-folder');
			if($role=='dosen'){
				$data['folder']		= $mproker->read_folder($karyawanid, 'parent-folder', $folderid);
			}elseif($role=='student employee'||$role=='admin'){
				$data['folder']		= $mproker->read_folder('', 'parent-folder', $folderid);
			}
			$data['count_folder'] = 1;
		}

		$data['folder_parent'] = $folderid;
		
		$this->view( 'evaluasi/media-library.php', $data );
	}
	
	function get_library_content_back(){
		$mproker = new model_proker();
		
		$karyawanid 		= $this->coms->authenticatedUser->staffid;
		$role				= $this->coms->authenticatedUser->role;	
		
		if(isset($_POST['param'])&&$_POST['param']=='content-view'){ //--BACK FROM IMAGE/CONTENT VIEW
			$data['count_folder'] = ($_POST['count']);
			$folderid = $_POST['folderid'];
			
			if($folderid==''){
				$data['file']	= $mproker->read_file('no-folder');
				if($role=='dosen'){
					$data['folder']		= $mproker->read_folder($karyawanid, 'parent-folder', $folderid);
				}elseif($role=='student employee'||$role=='admin'){
					$data['folder']		= $mproker->read_folder('', 'parent-folder', $folderid);
				}
			}else{
				$data['file']	= $mproker->read_file('', $folderid);
				if($role=='dosen'){
					$data['folder']		= $mproker->read_folder($karyawanid, '', $folderid);
				}elseif($role=='student employee'||$role=='admin'){
					$data['folder']		= $mproker->read_folder('', '', $folderid);
				}
			}
			
			$data['folder_parent'] = $folderid;
			
		}else{ //--BACK FROM FOLDER VIEW
			
			$data['count_folder'] = ($_POST['count']-1);
			$folderid = $_POST['folderid'];
			
			if(($_POST['count']-1)<2){
				$folderid = '';
				
				$data['file']	= $mproker->read_file('no-folder');
				if($role=='dosen'){
					$data['folder']		= $mproker->read_folder($karyawanid, 'parent-folder', $folderid);
				}elseif($role=='student employee'||$role=='admin'){
					$data['folder']		= $mproker->read_folder('', 'parent-folder', $folderid);
				}
				
				$data['folder_parent'] = '';
			}else{
				$pid1 = $mproker->read_folder('','',$folderid,'get-parent');
				$parent1 = $pid1[0]->parentid;
				
				if($parent1=='08495d'){
					$parentid = '';
				}else{
					$parentid = $parent1;
				}
				
				$data['file']	= $mproker->read_file('', $parentid);
				if($role=='dosen'){
					$data['folder']		= $mproker->read_folder($karyawanid, '', $parentid);
				}elseif($role=='student employee'||$role=='admin'){
					$data['folder']		= $mproker->read_folder('', '', $parentid);
				}
				
				$data['folder_parent'] = $parentid;
			}
		}
		
		$this->view( 'evaluasi/media-library.php', $data );
		
	}
	
	function get_media(){
		$mproker = new model_proker();
		
		$fileid = $_POST['fileid'];
		
		$data['file'] = $mproker->read_file('','',$fileid);
		$data['folder_parent'] = $_POST['folderparent'];
		$data['count_folder'] = $_POST['count'];
		
		$this->view( 'evaluasi/view-media-library.php', $data );
		
	}
	
	function download($id=NULL){
		$mproker = new model_proker();
		$karyawanid	= $this->coms->authenticatedUser->staffid; 
		$dt		= $mproker->read_file('','',$id);
		
		$path 	= "assets/".$dt[0]->file_loc;	
			
		if (file_exists($path)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename=' . urlencode(basename($path)));
			// header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path));
			ob_clean();
			flush();
			readfile($path);
			exit;
		}else{
			die('File Not Found');
		}
	}
	
	function edit_evaluasi($detailid){
		$mproker = new model_proker();
			
		$karyawanid 		= $this->coms->authenticatedUser->staffid;
		
		$data['header'] 	= 'Pelaksanaan Proker';
		$data['param']		= 'Edit Pelaksanaan Proker';
		$data['posts'] 		= $mproker->read_evaluasi($detailid,'detailid','kegiatan');
		
		$data['proker']		= $mproker->read();
		$data['detailid'] 	= $detailid;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js'); 
		$this->add_script('js/evaluasi.js');
		
		$this->view( 'evaluasi/edit.php', $data );
	}
	
	function detail_evaluasi($detail_id){
		$mproker = new model_proker();
		
		$data['header'] 	= 'Pelaksanaan Proker';
		$data['detailid']	= $detail_id;
		$judul 				= $mproker->read_detail($detail_id,'detailid','none','');
		$data['judul']		= $judul[0]->kegiatan;
		$data['posts'] 		= $mproker->read_evaluasi($detail_id,'detailid','kegiatan');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/excellentexport.js');
		$this->add_script('js/proker.js');
		
		$this->view( 'evaluasi/detail.php', $data );
	}
	
	function get_detail_evaluasi_($kegiatan_id_parent,$judulparent,$judulproker,$data){
		$mproker = new model_proker();
		$i=0;
		$number = 0;
		
		$allnum = $mproker->get_kegiatan_base_num($kegiatan_id_parent);
		$targetnum = $allnum->targetnum;
		$basenum = $allnum->baselinenum;
		
		$target  = $mproker->read_evaluasi($kegiatan_id_parent,'parentid','target');
		$base = $mproker->read_baseline($kegiatan_id_parent,'kegiatanid');
		
		$no = $data[0];
		$print = $data[1];
		if($targetnum>$basenum){
			foreach ($target as $t){
				echo '<tr>';
				echo '<td>';if($print==0){echo $judulproker;}echo '</td>';
				echo '<td>';if($number==0){echo $no;$number=1;$print=1;}echo '</td>';
				echo '<td>';if($i==0){echo $judulparent;}echo '</td>';
				echo '<td>';
					if(isset($base[$i]->judul))echo $base[$i]->judul;
				echo '</td>';
				echo '<td>'.$t->judul.'</td>';
				echo '<td>'.$t->capaian.'%</td>';
				echo '<td>';
					$http = explode('/', $t->doc_pendukung);
					if($http[0]=='http:'){ echo $t->doc_pendukung;}
					else{echo $t->file_name;}
				echo '</td>';
				echo '<td>'.$t->kendala.'</td>';
				echo '</tr>';
				$i++;
				// $no++;
			}
		}elseif($targetnum<$basenum){
			foreach ($base as $b){
				echo '<tr>';
				echo '<td>';if($print==0){echo $judulproker;}echo '</td>';
				echo '<td>';if($number==0){echo $no;$number=1;$print=1;}echo '</td>';
				echo '<td>';if($i==0){echo $judulparent;}echo '</td>';
				echo '<td>'.$b->judul.'</td>';
				echo '<td>';
					if(isset($target[$i]->judul))echo $target[$i]->judul;
				echo '</td>';
				echo '<td>';
					if(isset($target[$i]->capaian))echo $target[$i]->capaian."%";
				echo '</td>';	
				echo '<td>';
					if(isset($target[$i]->doc_pendukung)){
						$http = explode('/', $target[$i]->doc_pendukung);
						if($http[0]=='http:'){ echo $target[$i]->doc_pendukung;}
						else{echo $target[$i]->file_name;}
					}
				echo '</td>';
				echo '<td>';
					if(isset($target[$i]->kendala))echo $target[$i]->kendala;
				echo '</td>';	
				echo '</tr>';
				$i++;
				// $no++;
			}
		}elseif($targetnum==0&&$basenum==0){
			echo '<tr>';
			echo '<td>';if($print==0){echo $judulproker;}echo '</td>';
			echo '<td>';if($number==0){echo $no;$number=1;$print=1;}echo '</td>';
			echo '<td>';if($i==0){echo $judulparent;}echo '</td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '<td></td>';
			echo '</tr>';
		}
		elseif($targetnum==$basenum){
			foreach ($target as $t){
				echo '<tr>';
				echo '<td>';if($print==0){echo $judulproker;}echo '</td>';
				echo '<td>';if($number==0){echo $no;$number=1;$print=1;}echo '</td>';
				echo '<td>';if($i==0){echo $judulparent;}echo '</td>';
				echo '<td>';
					if(isset($base[$i]->judul))echo $base[$i]->judul;
				echo '</td>';
				echo '<td>'.$t->judul.'</td>';
				echo '<td>'.$t->capaian.'%</td>';
				echo '<td>';
					$http = explode('/', $t->doc_pendukung);
					if($http[0]=='http:'){ echo $t->doc_pendukung;}
					else{echo $t->file_name;}
				echo '</td>';
				echo '<td>'.$t->kendala.'</td>';
				echo '</tr>';
				$i++;
				// $no++;
			}
		}
		$no++;
		$data[0] = $no;
		$data[1] = $print;
		return $data;
	}
	
	function read_detail_child($rencanaid,$detailid){
		$mproker = new model_proker();
		
		$data = $mproker->read_detail($rencanaid,'rencanaid');
		
		foreach ($data as $d) {
			echo '<option value="'.$d->detail_id.'" ';
			if(isset($detailid)&&$detailid==$d->detail_id){
				echo "selected";
			}
			echo ' >'.$d->kegiatan.'</option>';
		}
		
	}
	
	function get_baseline($kegiatan_id){
		$mproker = new model_proker();
		
		$data['posts'] 		= $mproker->read_baseline($kegiatan_id,'kegiatanid');
		
		$this->view( 'evaluasi/edit_baseline.php', $data );
	}
	
	function get_target($kegiatan_id){
		$mproker = new model_proker();
		
		$data['posts'] 		= $mproker->read_evaluasi($kegiatan_id,'parentid','target');
		
		$this->view( 'evaluasi/edit_target.php', $data );
	}
	
	function save_evaluasi_toDB(){
		$mproker = new model_proker();
		
		$user 	 	= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$karyawanid = $this->coms->authenticatedUser->staffid;
		
		$detailid 		  = $mproker->get_detailId($_POST['select_proker'])->detailid;
		$periode_baseline = $_POST['periode-base'];
		
		$type = $_POST['type'];
		
		$judulkegiatan 		= $_POST['judul'];
		$keterangankegiatan = $_POST['keterangan-kegiatan'];
		
		$baseline_num		= $_POST['baseline-num'];
		$target_num_count	= $_POST['target-num'];
		
		$param		= Array();
		$tglnum 	= 0;
		$basenum	= 0;
		$targetnum	= 0;
		
		$param[0]	= $basenum;
		$param[1]	= $targetnum;
		$param[2]	= $tglnum;
		
		for ($i=0; $i < count($judulkegiatan); $i++) {
			
			if(isset($_POST['hidId'][$i])&&$_POST['hidId'][$i]!=''){
				$kegiatan_id = $_POST['hidId'][$i];
			}else{
				$kegiatan_id = $mproker->get_kegiatan_id();
				
				$aktifitas_id = $mproker->get_aktifitas_id();
				$datamulai = explode(' ', $_POST['tglrencanamulai'][$param[2]]);
				$dataselesai = explode(' ', $_POST['tgltargetselesai'][$param[2]]);
				$dataaktifitas = Array(
										'aktifitas_id'=>$aktifitas_id,
										'karyawan_id'=>$karyawanid,
										'jenis_kegiatan_id'=>'proker',
										'judul'=>$judulkegiatan[$i],
										'tgl'=>$datamulai[0],
										'tgl_selesai'=>$dataselesai[0],
										'jam_mulai'=>$datamulai[1],
										'jam_selesai'=>$dataselesai[1],
										'inf_kategori'=>'kegiatan'
									  );
				$mproker->replace_tbl_aktifitas($dataaktifitas);
				
			}
			// echo $kegiatan_id."<br>"; 
			$datanya = Array(
							'kegiatan_id'=>$kegiatan_id,
							'detail_id'=>$detailid,
						   	'kategori'=>'kegiatan',
						   	'judul'=>$judulkegiatan[$i],
						   	'keterangan'=>$keterangankegiatan[$i],
						   	'tgl_rencana_mulai'=> $_POST['tglrencanamulai'][$param[2]],
						   	'tgl_target_selesai'=>$_POST['tgltargetselesai'][$param[2]],
						   	'tgl_pelaksanaan'=>$_POST['tglpelaksanaan'][$param[2]],
						   	'tgl_selesai'=>$_POST['tglselesai'][$param[2]],
						   	'user_id'=>$user,
						   	'last_update'=>$lastupdate
						   );
			
			if($mproker->replace_rencana_kerja_kegiatan($datanya)){
				$result = "Success";
				// echo $baseline_num[$i]."\n";
				// echo $target_num_count[$i]."\n";
				
				if($baseline_num[$i]!='0'&&$target_num_count[$i]!='0'){
					// echo "base-target \n";
					$param[2]++;
					$param = $this->save_rencana_kerja_baseline_toDB($kegiatan_id, $detailid, $param, $baseline_num[$i],'both');
					$param = $this->save_rencana_kerja_target_toDB($kegiatan_id, $detailid, $user, $lastupdate, $param, $target_num_count[$i]);
				}
				elseif($baseline_num[$i]!='0'&&$target_num_count[$i]=='0'){
					// echo "base \n";
					$param = $this->save_rencana_kerja_baseline_toDB($kegiatan_id, $detailid, $param, $baseline_num[$i],'');
				}
				elseif($baseline_num[$i]=='0'&&$target_num_count[$i]!='0'){
					// echo "target \n";
					$param[2]++;
					$param = $this->save_rencana_kerja_target_toDB($kegiatan_id, $detailid, $user, $lastupdate, $param, $target_num_count[$i]);
				}else{
					// echo "no \n";
					$param[2]++;
				}

			}else{
				$result = "Failed";
				echo $result;
				exit;
			};
			
		}
		
		echo $result;
		
	}

	function save_rencana_kerja_baseline_toDB($kegiatan_id_parent, $detailid, $param, $baseline_num, $both){
		$mproker = new model_proker();
		
		$baseline	 		= $_POST['baseline'];
		$periode_baseline 	= $_POST['periode-base'];
		
		$target_num_count	= $_POST['target-num'];
		
		$basenum = $param[0];
		
		for($i=0;$i<$baseline_num;$i++){
			if(isset($_POST['hidId2'][$basenum])&&$_POST['hidId2'][$basenum]!=''){
				$baseline_id = $_POST['hidId2'][$basenum];
			}else{
				$baseline_id = $mproker->get_baseline_id();
			}
			// echo $baseline_id."\n";
			$datanya = Array(
							'baseline_id'=>$baseline_id,
							'kegiatan_id'=>$kegiatan_id_parent,
						   	'judul'=>$baseline[$basenum],
						   	'periode'=>$periode_baseline,
						   	'detail_id'=>$detailid
						   );
			
			if($mproker->replace_rencana_kerja_baseline($datanya)){
				$result = "Success";
				if($both!='both'){ $param[2]++; }
			}else{
				$result = "Failed";
				echo $result;
				exit;
			}
			
			$basenum++;
			
		}
			
			$param[0]	= $basenum;
			return $param;
	}

	function save_rencana_kerja_target_toDB($kegiatan_id_parent, $detailid, $user, $lastupdate, $param, $target_num_count){
		$mproker = new model_proker();
		
		$judultarget 		= $_POST['judul-target'];
		$keterangan_target	= $_POST['target'];
		$capaian			= $_POST['capaian'];
		$buktipendukung		= $_POST['buktipendukung'];
		$kendala			= $_POST['kendala'];
		
		$targetnum = $param[1];
		$tglnum = $param[2];
		
		for($i=0;$i<$target_num_count;$i++){
			
			if(isset($_POST['hidId3'][$targetnum])&&$_POST['hidId3'][$targetnum]!=''){
				$kegiatan_id = $_POST['hidId3'][$targetnum];
			}else{
				$kegiatan_id = $mproker->get_kegiatan_id();
			}
			
			$datanya = Array(
							'kegiatan_id'=>$kegiatan_id,
							'detail_id'=>$detailid,
						   	'kategori'=>'target',
						   	'judul'=>$judultarget[$targetnum],
						   	'keterangan'=>$keterangan_target[$targetnum],
						   	'capaian'=>$capaian[$targetnum],
						   	'kendala'=>$kendala[$targetnum],
						   	'doc_pendukung'=>$_POST['buktipendukung'][$targetnum],
						   	'tgl_rencana_mulai'=>$_POST['tglrencanamulai'][$tglnum],
						   	'tgl_target_selesai'=>$_POST['tgltargetselesai'][$tglnum],
						   	'tgl_pelaksanaan'=>$_POST['tglpelaksanaan'][$tglnum],
						   	'tgl_selesai'=>$_POST['tglselesai'][$tglnum],
						   	'user_id'=>$user,
						   	'last_update'=>$lastupdate,
						   	'parent_id'=>$kegiatan_id_parent
						   );
			
			if($mproker->replace_rencana_kerja_kegiatan($datanya)){
				$result = "Success";
			}else{
				$result = "Failed";
				echo $result;
				exit;
			};
			
			$targetnum++;
			$tglnum++;
		}

		$param[1]	= $targetnum;
		$param[2]	= $tglnum;
		return $param;
		
	}
	
}
?>