$(document).ready(function(){
	//INDEX
	$("select[name='kategori_index']").change(function(){
		$("#form-kategori-index").submit();
	});
	$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	$("select[name='karyawan[]']").select2({
		placeholder: "Masukkan Nama Karyawan"
	});
	$("select[name='mhs[]']").select2({
		placeholder: "Masukkan Nama Mahasiswa"
	});
	$("input[name='instansi_from']").autocomplete({ 
		source: base_url + "module/administration/surat/get_instansi",
		minLength: 0, 
		select: function(event, ui) { 
			$("input[name='instansi_from']").val(ui.item.value);
		} 
	});
	
	//=====FORM KIRI====//
	$("#kepada").hide();
	$("select[name='tujuan']").change(function(){
		var to = $(this).val();
		// alert(to);
		if(to!=='0'){
			$("#kepada").show();
			if(to == "kedalam"){
				$("#luar").empty();
				$("#dalam").show();
			}
			else{
				$("#dalam").hide();
				var output = "";
				var output1= "";
				i=1;
				
				output = "<div class='form-group'>";
				output += "<button type='button' id='add_kepada'><i class='fa fa-plus'></i></button></div>";
				
				
				output1 += "<div class='form-group'>";
				output1 += "<input type='text' class='form-control' name='nama[]' placeholder='Masukkan Nama'/></div>";
				output1 += "<div class='form-group'>";
				output1 += "<input class='form-control instansi typeahead' name='instansi[]' placeholder='Masukkan Nama Instansi'>";
				output1 += "</div>";
				
				$("#luar").html(output+output1);
				$(".instansi").autocomplete({ 
					source: base_url + "module/administration/surat/get_instansi",
					minLength: 0, 
					select: function(event, ui){ 
						$(this).val(ui.item.value);
					} 
				});
				
				//tambah kepada
				$("#add_kepada").click(function(){
					i+=1;
					var del = "&nbsp;<span name='delete_kepada' onclick='deleted(\""+i+"\")' style='cursor: pointer; color : red'>Delete <i class='fa fa-trash-o'></i></span>"
					var container1 = "<div class='del"+i+"'>";
					var container2 = "</div>";
					$("#luar").append(container1+"<br>"+del+output1+container2);
					$(".instansi").autocomplete({ 
						source: base_url + "module/administration/surat/get_instansi",
						minLength: 0, 
						select: function(event, ui) { 
							$(this).val(ui.item.value);
						} 
					});
				});
			}
		}
		else{
			$("#kepada").hide();
		}
	});
	
	$("#s2id_mhs").hide();
	$("select[name='fakultas']").change(function(){
		$("select[name='prodi']").val('0');
		$("select[name='cabang']").val('0');
		$("select[name='karyawan[]']").select2("val","");
		$("select[name='karyawan[]']").select2("enable",false);
		$("select[name='mhs[]']").select2("val","");
		$("select[name='mhs[]']").select2("enable",false);
		
		var fak = $("select[name='fakultas']").val();
		if(fak!=='0'){
			$("select[name='cabang']").removeAttr("disabled");
			$("select[name='prodi']").removeAttr("disabled");
			$.ajax({
				url : base_url + "module/administration/surat/get_prodi",
				type : "POST",
				data : $.param({
						fakultas : fak
				}),
				dataType : "html",
				success:function(data) 
				{
					// alert(data);
					 $("select[name='prodi']").html(data);
				}
			});
		}
		else{
			$("select[name='cabang']").attr("disabled",true);
			$("select[name='prodi']").attr("disabled",true);
		}
	});
	
	$("select[name='prodi']").change(function(){
		$("select[name='cabang']").val('0');
		$("select[name='mhs[]']").select2("val","");
	});
	
	$("select[name='cabang']").change(function(){
		$("select[name='karyawan[]']").select2("val","");
		$("select[name='mhs[]']").select2("val","");
		var kpd = $("input[name='kepada']").val();
		if(kpd == "karyawan"){
			$("select[name='karyawan[]']").select2("enable",true);
			var fakultas 	= $("select[name='fakultas']").val();
			var cbg 		= $("select[name='cabang']").val();
			$.ajax({
				url : base_url + "module/administration/surat/get_karyawan",
				type : "POST",
				data : $.param({
						fakultas : fakultas,
						cabang : cbg
				}),
				dataType : "html",
				success:function(data) 
				{
					// alert(data);
					 $("#karyawan").html(data);
				}
			});
		}
	});
	
	$("input[name='kepada']").change(function(){
		$("select[name='karyawan[]']").select2("val","");
		$("select[name='mhs[]']").select2("val","");
		$("select[name='cabang']").attr("disabled",true);
		$("select[name='prodi']").attr("disabled",true);
		$("select[name='fakultas']").val('0');
		$("select[name='cabang']").val('0');
		$("select[name='prodi']").val('0');
		
		var to = $(this).val();
		if(to == "karyawan"){
			$("select[name='prodi']").hide();
			$("#s2id_karyawan").show();
			$("#s2id_mhs").hide();
			$("select[name='karyawan[]']").select2("enable",false);
			$("select[name='cabang']").change(function(){
				$("select[name='karyawan[]']").select2("enable",true);
				var fakultas 	= $("select[name='fakultas']").val();
				var cbg 		= $("select[name='cabang']").val();
				$.ajax({
					url : base_url + "module/administration/surat/get_karyawan",
					type : "POST",
					data : $.param({
							fakultas : fakultas,
							cabang : cbg
					}),
					dataType : "html",
					success:function(data) 
					{
						// alert(data);
						 $("#karyawan").html(data);
					}
				});
			});
		}
		else{
			$("select[name='prodi']").show();
			$("#s2id_mhs").show();
			$("#s2id_karyawan").hide();
			$("select[name='mhs[]']").select2("enable",false);
			$("select[name='cabang']").change(function(){
				$("select[name='mhs[]']").select2("val","");
				var prodi 		= $("select[name='prodi']").val();
				var cbg 		= $("select[name='cabang']").val();
				$("select[name='mhs[]']").select2("enable",true);
				$.ajax({
					url : base_url + "module/administration/surat/get_mhs",
					type : "POST",
					data : $.param({
							prodi : prodi,
							cabang : cbg
					}),
					dataType : "html",
					success:function(data) 
					{
						// alert(data);
						 $("#mhs").html(data);
					}
				});
			});
		}
	});
	
	
	//=====FORM KANAN====//
	$("#pengolah_masuk").hide();
	$("#pengirim_masuk").hide();
	$("select[name='kategori']").change(function(){
		$("select[name='tujuan']").val('0');
		$("#luar").empty();
		$("#dalam").hide();
		$("#kepada").hide();
		var kategori = $("select[name='kategori']").val();
		if(kategori!=='masuk'){
			$("input[name='from'][value='luar']").attr("disabled",true);
			$("#tujuan_surat option[value='keluar']").show();
			$('#pengolah').tagsManager('empty');
			$("input[name='hidden-pengolah']").val("");
			$("input[name='pengolah_id']").val("");
			// $("input[name='tanggal_surat_kirim']").val("");
			// $("input[name='file_kirim_loc']").val("");
			// $(".file_kirim").html("");
			// $('#pengantar').tagsManager('empty');
			// $("input[name='hidden-pengantar']").val("");
			// $("input[name='pengantar_id']").val("");
			// $("input[name='kirim_id']").val("");
			$("#pengolah_masuk").hide();
			$("#pengirim_masuk").hide();
		}
		else{
			$("input[name='from'][value='luar']").attr("disabled",false);
			$("#tujuan_surat option[value='keluar']").hide();
			$("#pengolah_masuk").show();
			$("#pengirim_masuk").show();
		}
	});
	
	//--DARI START--//
	$("select[name='from_fakultas']").change(function(){
		$("select[name='from_cabang']").val('0');
		$("input[name='dari']").attr("disabled",true);
		$('#dari').tagsManager('empty');
		$("input[name='dari']").val("");
		var fak = $("select[name='from_fakultas']").val();
		if(fak!=='0'){
			$("select[name='from_cabang']").removeAttr("disabled");
		}
		else{
			$("select[name='from_cabang']").attr("disabled",true);
		}
	});
	
	$("select[name='from_cabang']").change(function(){
		var cab = $("select[name='from_cabang']").val();
		if(cab!=='0'){
			$("input[name='dari']").removeAttr("disabled");
			var fakultas 	= $("select[name='from_fakultas']").val();
			var cbg 		= $("select[name='from_cabang']").val();
			$.ajax({
				url : base_url + "module/administration/surat/get_karyawan_AC",
				type : "POST",
				data : $.param({
						fakultas : fakultas,
						cabang : cbg
				}),
				dataType : "json",
				success:function(data) 
				{
					//alert(data);
					$("#dari").autocomplete({ 
						source: data,
						minLength: 0, 
						select: function(event, ui) { 
							$('#dari').val(ui.item.value);
							$('#dari_id').val(ui.item.karyawan_id);
						} 
					});
				},
				error:function(data){
					$("#dari").autocomplete({ 
						source: "",
						minLength: 0, 
						select: function(event, ui) { 
							$('#dari').val(ui.item.value);
							$('#dari').val(ui.item.karyawan_id);
						} 
					});
				}
			});
			
		}
		else{
			$('#dari').tagsManager('empty');
			$("input[name='dari']").val("");
			$("input[name='dari']").attr("disabled",true);
		}
	});
	
	$("input[name='from']").change(function(){
		$('#dari').tagsManager('empty');
		$('#dari_id').val("");
		$('#instansi_from').val("");
		var from = $(this).val();
		if(from == "dalam"){
			$('#dari').attr("Placeholder","Masukkan Nama Karyawan Lalu Tekan Enter");
			$("input[name='dari']").attr("disabled",true);
			$('#instansi_from').hide();
			$("#from_dalam").show();
		}
		else{
			$("#dari").autocomplete({ 
				source: "",
				minLength: 0, 
				select: function(event, ui) { 
					$('#dari').val(ui.item.value);
					$('#dari').val(ui.item.karyawan_id);
				} 
			});
			$('#dari').attr("Placeholder","Masukkan Nama Pengirim Lalu Tekan Enter");
			$("input[name='dari']").removeAttr("disabled");
			$('#instansi_from').show();
			$("#from_dalam").hide();
		}
	});
	//--DARI END--//
	
	//--PENGOLAH START--//
	$("select[name='pengolah_fakultas']").change(function(){
		$("select[name='pengolah_cabang']").val('0');
		$("input[name='pengolah']").attr("disabled",true);
		$('#pengolah').tagsManager('empty');
		$("input[name='pengolah']").val("");
		var fak = $("select[name='pengolah_fakultas']").val();
		if(fak!=='0'){
			$("select[name='pengolah_cabang']").removeAttr("disabled");
		}
		else{
			$("select[name='pengolah_cabang']").attr("disabled",true);
		}
	});
	
	$("select[name='pengolah_cabang']").change(function(){
		var cab = $("select[name='pengolah_cabang']").val();
		if(cab!=='0'){
			// alert();
			$("input[name='pengolah']").removeAttr("disabled");
			var fakultas 	= $("select[name='pengolah_fakultas']").val();
			var cbg 		= $("select[name='pengolah_cabang']").val();
			$.ajax({
				url : base_url + "module/administration/surat/get_karyawan_AC",
				type : "POST",
				data : $.param({
						fakultas : fakultas,
						cabang : cbg
				}),
				dataType : "json",
				success:function(data) 
				{
					$("#pengolah").autocomplete({ 
						source: data,
						minLength: 0, 
						select: function(event, ui) { 
							$('#pengolah').val(ui.item.value);
							$('#pengolah_id').val(ui.item.karyawan_id);
						} 
					});
				},
				error:function(data){
					$("#pengolah").autocomplete({ 
						source: "",
						minLength: 0, 
						select: function(event, ui) { 
							$('#pengolah').val(ui.item.value);
							$('#pengolah_id').val(ui.item.karyawan_id);
						} 
					});
				}
			});
		}
		else{
			$('#pengolah').tagsManager('empty');
			$("input[name='pengolah']").val("");
			$("input[name='pengolah']").attr("disabled",true);
		}
	});
	//--PENGOLAH END--//
	
	//--PENGantar START--//
	$("select[name='pengantar_fakultas']").change(function(){
		$("select[name='pengantar_cabang']").val('0');
		var fak = $("select[name='pengantar_fakultas']").val();
		$('#pengantar').tagsManager('empty');
		$("input[name='pengantar']").val("");
		if(fak=='0'){
			$("input[name='pengantar']").removeAttr("disabled");
		}
		else{
			$("input[name='pengantar']").attr("disabled",true);
		}
	});
	
	$("select[name='pengantar_cabang']").change(function(){
		var cab = $("select[name='pengantar_cabang']").val();
		if(cab!=='0'){
			// alert();
			$("input[name='pengantar']").removeAttr("disabled");
			var fakultas 	= $("select[name='pengantar_fakultas']").val();
			var cbg 		= $("select[name='pengantar_cabang']").val();
			$.ajax({
				url : base_url + "module/administration/surat/get_karyawan_AC",
				type : "POST",
				data : $.param({
						fakultas : fakultas,
						cabang : cbg
				}),
				dataType : "json",
				success:function(data) 
				{
					$("#pengantar").autocomplete({ 
						source: data,
						minLength: 0, 
						select: function(event, ui) { 
							$('#pengantar').val(ui.item.value);
							$('#pengantar_id').val(ui.item.karyawan_id);
						} 
					});
				},
				error:function(data){
					$("#pengantar").autocomplete({ 
						source: "",
						minLength: 0, 
						select: function(event, ui) { 
							$('#pengantar').val(ui.item.value);
							$('#pengantar_id').val(ui.item.karyawan_id);
						} 
					});
				}
			});
		}
		else{
			$('#pengantar').tagsManager('empty');
			$("input[name='pengantar']").val("");
			$("input[name='pengantar']").attr("disabled",true);
		}
	});
	//--PENGIRIM END--//
	
	$("select[name='index_perihal']").change(function(){
		$("select[name='unit_kerja']").val('0');
		$("input[name='nosurat']").val("");
		var to = $(this).val();
		if(to!=='0'){
			$("select[name='unit_kerja']").removeAttr("disabled");
		}
		else{
			$("select[name='unit_kerja']").attr("disabled",true);
		}
	});
	
	$("select[name='unit_kerja']").change(function(){
		var no = $("input[name='urut']").val();
		var unit = $(this).val();
		var index_perihal = $("select[name='index_perihal']").val();
		var d = new Date();
		var year = d.getFullYear();
		
		if(unit!=='0'){
			$("input[name='nosurat']").val(no+"/"+unit+"/"+index_perihal+"/"+year);
		}
		else{
			$("input[name='nosurat']").val("");
		}
	});
	
	$("select[name='template']").change(function(){
		var template = $(this).val();
		//alert(template);
		$.ajax({
			url : base_url + "module/administration/surat/get_template",
			type : "POST",
			data : $.param({
					template : template
			}),
			dataType : "html",
			success:function(data) 
			{
				//alert(data);
				//$("#isi_surat").val(data);
				CKEDITOR.instances['isi_surat'].setData(data);
			}
		});
	});
	
	
	jQuery("#dari").tagsManager({
		maxTags: 1
	});
	jQuery("#pengolah").tagsManager({
		maxTags: 1
	});
	jQuery("#pengantar").tagsManager({
		maxTags: 1
	});
	
	
	//======ALL=====//
	$("#form-surat").submit(function(e){
		var isisurat = CKEDITOR.instances['isi_surat'].getData();
		$("#isi_surat").val(isisurat);
		var tujuan		= $("#tujuan_surat").val();
		var karyawan	= $("select[name='karyawan[]']").val();
		var mhs			= $("select[name='mhs[]']").val();
		var nama		= $("input[name='nama[]']").val();
		var dari		= $("input[name='hidden-dari']").val();
		var isi			= $("#isi_surat").val();
		
		if(tujuan!=='0' && (isi.trim()!=="" || isi.trim()!=="<p></p>") && dari.trim()!=="" && (nama!==null || karyawan!==null || mhs!==null)){
			//alert(tujuan);
			var postData = new FormData($(this)[0]);
			$.ajax({
				url : base_url + "module/administration/surat/save_surat",
				type: "POST",
				data : postData,
				async: false,
				success:function(msg) 
				{
					alert(msg);
				    location.reload();  
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
				    alert ('Upload Gagal!');      
				    // location.reload(); 
				},
			    cache: false,
				contentType: false,
				processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
	
	$("#form-kirim-surat").submit(function(e){
		var pengantar	= $("#pengantar").val();
		var tgl			= $("input[name='tanggal_surat_kirim']").val();

		if(pengantar!=null && tgl.trim()!==""){
			var postData = new FormData($(this)[0]);
			$.ajax({
				url : base_url + "module/administration/surat/save_kirim",
				type: "POST",
				data : postData,
				async: false,
				success:function(msg) 
				{
					alert(msg);
					window.location.href = base_url + "module/administration/surat"; 
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
				    alert ('Kirim Gagal!');      
				    // location.reload(); 
				},
			    cache: false,
				contentType: false,
				processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
	
	$("#form-terima-surat").submit(function(e){
		var id = $("input[name='kirimid']").val();
		var file = $("input[name='file_kirim']").val();
		var tanggal = $("input[name='tanggal_surat_terima']").val();
		if(file!=="" && tanggal.trim()!==""){
			var postData = new FormData($(this)[0]);
			$.ajax({
				url : base_url + "module/administration/surat/save_kirim",
				type: "POST",
				data : postData,
				async: false,
				success:function(msg) 
				{
					alert(msg);
					window.location.href = base_url + "module/administration/surat/display/"+id; 
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
				    alert ('Terima Failed!');      
				    // location.reload(); 
				},
			    cache: false,
				contentType: false,
				processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
	
	$(".terima").click(function(){
		$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
		var zIndexModal = $(".modal_terima").css('z-index');
		$('body').find('.datepicker').css('z-index', zIndexModal + 1);
		var kirimid = $(this).data("uri");
		$("input[name='kirim_id']").val(kirimid);
	});
	
	$(".close-terima").click(function(){
		$("input[name='kirim_id']").val("");
	})
	
	//====EDIT====//
	var hid = $("input[name='hidId']").val();
	if(hid!=""){
		//------KEPADA-------
		var tujuan = $("select[name='tujuan']").val();
		if(tujuan=="keluar"){
			$("#kepada").show();
			$("#dalam").hide();
			$(".instansi").autocomplete({ 
				source: base_url + "module/administration/surat/get_instansi",
				minLength: 0, 
				select: function(event, ui) { 
					$(this).val(ui.item.value);
				} 
			});
		}
		else{
			$("#kepada").show();
			$("#luar").empty();
			$("#dalam").show();
			
			var surat_id = $("input[name='surat_id_tag']").val();
			if(surat_id!==""){
				var kpd = $("input[name='kepada'][checked='checked']").val();
				$.ajax({
					url : base_url + "module/administration/surat/get_dari_surat",
					type : "POST",
					data : $.param({
							surat_id : surat_id
					}),
					dataType : "json",
					success:function(data) 
					{				
						//alert(data);   
						var prodi="";
						var to = new Array();
						var kpd_id = new Array();
    					i=0;
						$.each(data, function(event, ui) {
								kpd_id[i] = ui.kepada_id;
								// alert(kpd);
								if(kpd == "karyawan"){
									to[i] = ui.karyawan_id;
									fakultas = ui.fakultas_karyawan_id;
									cabang = ui.cabang_karyawan;
								}
								else{
									prodi = ui.prodi_id;
									cabang = ui.cabang_id;
									to[i] = ui.mahasiswa_id;
								}
								i++;	   
						});
						if(kpd == "karyawan"){
							$("select[name='prodi']").hide();
					    	$("#s2id_karyawan").show();
							$("#s2id_mhs").hide();
							$("select[name='fakultas']").val(fakultas);
							$("select[name='cabang']").val(cabang);
							$.ajax({
								url : base_url + "module/administration/surat/get_karyawan",
								type : "POST",
								data : $.param({
										fakultas : fakultas,
										cabang : cabang
								}),
								dataType : "html",
								success:function(data) 
								{
									// alert(data);
									 $("#karyawan").html(data);
									 $.each($("#karyawan"), function(){
										 $(this).select2("val", to);
										 $("input[name='kepada_id']").val(kpd_id);
									 });
									 
								}
							});
					    }
					    else{
					    	$("select[name='prodi']").show();
					    	$("#s2id_karyawan").hide();
							$("#s2id_mhs").show();
							var fak = $("select[name='fakultas']").val();
							$.ajax({
								url : base_url + "module/administration/surat/get_prodi",
								type : "POST",
								data : $.param({
										fakultas : fak
								}),
								dataType : "html",
								success:function(data) 
								{
									 $("select[name='prodi']").html(data);
									 setTimeout(function(){
										$("select[name='prodi']").val(prodi);
									 },300);
								}
							});
							$.ajax({
								url : base_url + "module/administration/surat/get_mhs",
								type : "POST",
								data : $.param({
										prodi : prodi,
										cabang : cabang
								}),
								dataType : "html",
								success:function(data) 
								{
									 $("#mhs").html(data);
								 	 $.each($("#mhs"), function(){
										 $(this).select2("val", to);
									 });
								
								}
							});
					    }
					}
				});
			}
		}
		
		//-------DARI-----
		var kategori = $("select[name='kategori']").val();
		if(kategori!=='masuk'){
			$("input[name='from'][value='luar']").attr("disabled",true);
			$("#tujuan_surat option[value='keluar']").show();
			$("#pengolah_masuk").hide();
			$("#pengirim_masuk").hide();
		}
		else{
			$("#tujuan_surat option[value='keluar']").hide();
			$("#pengolah_masuk").show();
			$("#pengirim_masuk").show();
		}
		
		var dari = $("input[name='from'][checked='checked']").val();
		if(dari=="dalam"){
			var dari_id = $("input[name='dari_id']").val();
			if(dari_id!==""){
				$.ajax({
					url : base_url + "module/administration/surat/get_karyawan_detail",
					type : "POST",
					data : $.param({
							id : dari_id
					}),
					dataType : "json",
					success:function(data) 
					{
						//alert(data);
					 	$.each(data, function(event, ui) {
							fakultas = ui.fakultas_id;
							cabang = ui.cabang_id;
							kepada = ui.nama;
							value = ui.value;
							karyawan_id = ui.karyawan_id;
							
							$("select[name='from_fakultas']").val(fakultas);
							$("select[name='from_cabang']").val(cabang);
							$.ajax({
								url : base_url + "module/administration/surat/get_karyawan_AC",
								type : "POST",
								data : $.param({
										fakultas : fakultas,
										cabang : cabang
								}),
								dataType : "json",
								success:function(data) 
								{
									$("#dari").autocomplete({ 
										source: data,
										minLength: 0, 
										select: function(event, ui) { 
											$('#dari').val(ui.item.value);
											$('#dari_id').val(ui.item.karyawan_id);
										} 
									});
								}
							});
							jQuery("#dari").tagsManager('pushTag', value);
							$("input[name='hidden-dari']").val(kepada);
						});	
					}
				});
			}
			
			//-----PENGOLAH----
			var pengolah_id = $("input[name='pengolah_id']").val();
			if(pengolah_id!==""){
				$.ajax({
					url : base_url + "module/administration/surat/get_karyawan_detail",
					type : "POST",
					data : $.param({
							id : pengolah_id
					}),
					dataType : "json",
					success:function(data) 
					{
						$.each(data, function(event, ui) {
							fakultas = ui.fakultas_id;
							cabang = ui.cabang_id;
							pengolah = ui.nama;
							value = ui.value;
							karyawan_id = ui.karyawan_id;
							$("select[name='pengolah_fakultas']").val(fakultas);
							$("select[name='pengolah_cabang']").val(cabang);
							$.ajax({
								url : base_url + "module/administration/surat/get_karyawan_AC",
								type : "POST",
								data : $.param({
										fakultas : fakultas,
										cabang : cabang
								}),
								dataType : "json",
								success:function(data) 
								{
									$("#pengolah").autocomplete({ 
										source: data,
										minLength: 0, 
										select: function(event, ui) { 
											$('#pengolah').val(ui.item.value);
											$('#pengolah_id').val(ui.item.karyawan_id);
										} 
									});
								}
							});
							jQuery("#pengolah").tagsManager('pushTag', value);
							$("input[name='hidden-pengolah']").val(pengolah);
						});	
					}
				});
			}
			
			
			//-----PENGANTAR----
			var pengantar_id = $("input[name='pengantar_id']").val();
			if(pengantar_id!==""){
				$.ajax({
					url : base_url + "module/administration/surat/get_karyawan_detail",
					type : "POST",
					data : $.param({
							id : pengantar_id
					}),
					dataType : "json",
					success:function(data) 
					{
						$.each(data, function(event, ui) {
							fakultas = ui.fakultas_id;
							cabang = ui.cabang_id;
							pengantar = ui.nama;
							value = ui.value;
							karyawan_id = ui.karyawan_id;
							$("select[name='pengantar_fakultas']").val(fakultas);
							$("select[name='pengantar_cabang']").val(cabang);
							$.ajax({
								url : base_url + "module/administration/surat/get_karyawan_AC",
								type : "POST",
								data : $.param({
										fakultas : fakultas,
										cabang : cabang
								}),
								dataType : "json",
								success:function(data) 
								{
									$("#pengantar").autocomplete({ 
										source: data,
										minLength: 0, 
										select: function(event, ui) { 
											$('#pengantar').val(ui.item.value);
											$('#pengantar_id').val(ui.item.karyawan_id);
										} 
									});
								}
							});
							jQuery("#pengantar").tagsManager('pushTag', value);
							$("input[name='hidden-pengantar']").val(karyawan_id);
						});	
					}
				});
			}
			else{
				var pengantar = $("input[name='pengantar-hide']").val();
				jQuery("#pengantar").tagsManager('pushTag', pengantar);
			}
		}
		else{
			
		}
		
		//----FILE----
		var file_lampiran = $("input[name='file_loc']").val();
		if(file_lampiran){
			var file_lampiran_loc = file_lampiran.replace(/^.*[\\\/]/, '');
			$(".file_lampiran").html(file_lampiran_loc);
			$("input[name='files']").change(function(){
				$(".file_lampiran").html("");
				$("input[name='file_loc']").val("");
			});
			
			var file_kirim = $("input[name='file_kirim_loc']").val();
			var file_kirim_loc = file_kirim.replace(/^.*[\\\/]/, '');
			$(".file_kirim").html(file_kirim_loc);
			$("input[name='file_kirim']").change(function(){
				$(".file_kirim").html("");
				$("input[name='file_kirim_loc']").val("");
			});
		}
		
		//===DISPOSISI===//
		$("#tambah_disposisi_btn").click(function(){
			$(".daftar_disposisi").hide();
			$(".tambah_disposisi").show();
		});
		var disposisi = $("select[name='karyawan_disposisi[]']").length;
		if(disposisi){
			$("select[name='karyawan_disposisi[]']").select2({
				placeholder: "Masukkan Nama Karyawan"
			});
			var fakultas 	= $("input[name='fakultas']").val();
			var cbg 		= $("input[name='cabang']").val();
			$.ajax({
				url : base_url + "module/administration/surat/get_karyawan",
				type : "POST",
				data : $.param({
						fakultas : fakultas,
						cabang : cbg
				}),
				dataType : "html",
				success:function(data) 
				{
					// alert(data);
					$("select[name='karyawan_disposisi[]']").html(data);
				}
			});
			
			$("#form-disposisi").submit(function(e){
				var surat = $("input[name='surat_id']").val();
				if(surat.trim()!==""){
					var postData = new FormData($(this)[0]);
					$.ajax({
						url : base_url + "module/administration/surat/save_disposisi",
						type: "POST",
						data : postData,
						async: false,
						success:function(msg) 
						{
							alert(msg);
						    window.location.href = base_url + "module/administration/surat/display/" + surat;
						},
						error: function(jqXHR, textStatus, errorThrown) 
						{
						    alert ('Penyimpanan Gagal!');      
						    // location.reload(); 
						},
					    cache: false,
						contentType: false,
						processData: false
					});
				}
				else{
					alert("Silahkan lengkapi form terlebih dahulu!");
				}
				e.preventDefault();
				return false;
			});
		};
		
		var hidid_disposisi = $("input[name='hidid_disposisi']").length;
		if(hidid_disposisi){
			var edit = $("input[name='hidid_disposisi']").val();
			if(edit!==""){
				$(".daftar_disposisi").hide();
				$(".tambah_disposisi").show();
				var kpd_disposisi = $("input[name='hidid_kpd']").val();
			    setTimeout(function(){
					$("#karyawan_disposisi").select2("val", kpd_disposisi);
				},300);
			}
		}
		
		$("#daftar_disposisi_btn").click(function(){
			$(".tambah_disposisi").hide();
			$(".daftar_disposisi").show();
			$("input[name='tgl_disposisi']").val("");
			$("select[name='karyawan_disposisi[]'] > option").removeAttr("selected");
         	$("select[name='karyawan_disposisi[]']").trigger("change");
         	$("textarea[name='isi_disposisi']").html("");
		});
	}
});
            
function deleted(i){
	$(".del"+i).remove();
};

function edit_disposisi(id){
	var form = document.createElement("form");
	var input = document.createElement("input");
	
	form.action = window.location.href;
	form.method = "post"
	
	input.name = "id";
	input.value = id;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function hapus_disposisi(id){
	var x = confirm("Are you sure want to delete?");
	if(x){
		$.ajax({
			url : base_url + "module/administration/surat/delete_disposisi",
			type : "POST",
			data : $.param({
					id : id
			}),
			dataType : "html",
			success:function(data) 
			{
				alert(data);
				location.reload();
			}
		});
	}
};  