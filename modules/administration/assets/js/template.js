$(document).ready(function(){
	$("input[name='files']").change(function(){
		$("img[name='kop_display']").remove();
		$("input[name='file_loc']").val("");
	});
	
	$(".delete").click(function(){
		var id = $(this).data("val");
		$.ajax({
			url : base_url + "module/administration/conf/delete_template",
			type : "POST",
			data : $.param({
					id : id
			}),
			dataType : "html",
			success:function(data) 
			{
				alert(data);
				$("#delete"+id).fadeOut();
			}
		});
	});
	
	$("select[name='kategori']").change(function(){
		var kat = $("select[name='kategori']").val();
		if(kat=="ijin"){
			$("#umum").hide();
		}
	});
	
	$("#form-template").submit(function(e){
		var isisurat = CKEDITOR.instances['isi_surat'].getData();
		$("#isi_surat").val(isisurat);
		var isi		= $("#isi_surat").val();
		var kat 	= $("select[name='kategori']").val();
		var jenis		= "";
		if(kat!=="izin"){
			jenis		= $("select[name='jenis']").val();
		}
		if((isi.trim()!="" || isi.trim()!="<p></p>") && jenis!=='0'){
			var postData = new FormData($(this)[0]);
			$.ajax({
					url : base_url + "module/administration/conf/save_template",
					type: "POST",
					data : postData,
					async: false,
					success:function(msg) 
					{
						alert(msg);
					    window.location.href = base_url + "module/administration/conf/template";
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
					    alert ('Proses Gagal!');      
					    // location.reload(); 
					},
				    cache: false,
					contentType: false,
					processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
});
