var periode_baseline = 0;
$(document).ready(function(){
	$("#select_proker").select2();
	$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	
	if($('.delete-kegiatan').length==1){
		$('.delete-kegiatan').hide();
	};
	
	//------------------------
	var edit = $("input[name='page-type']").val();
	if(edit=='edit'){
		var optgroup = $("#select_proker :selected").parent();
		var periode = optgroup.attr('periode');
		if(periode!=undefined){
			periode_baseline = (periode-1);
			$('.periode-baseline').val(periode_baseline);
		}else $('.periode-baseline').val('');
		
		var active = $('.page-list li.active').text();
		if($('.page-list li').length==1||$('.page-list li').length==0){
			$('.pager li.previous').attr('class', 'previous disabled');
			$('.pager li.next').attr('class', 'next disabled');
		}
		
		if($('.page-list li').length>1){
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next disabled');
		}
		
	}
	//------------------------
	
})

function set_validasi(kegiatanid, stat){
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/administration/proker/set_validasi_evaluasi',
		data : $.param({
			kegiatanid : kegiatanid,
			stat : stat
		}),
		success : function(msg) {
			if(msg=='success'){
				alert('Proses validasi berhasil!');
				location.reload();
			}
		}
	});
}

$("#export").click(function() {
	ExcellentExport.excel(this, 'myTable', 'progker');
});


$("#select_proker").change(function(){
	var optgroup = $("#select_proker :selected").parent();
	var periode = optgroup.attr('periode');
	if(periode!=undefined){
		periode_baseline = (periode-1);
		$('.periode-baseline').val(periode_baseline);
	}else $('.periode-baseline').val('');
})

function add_target(e){
	var parent = $(e).parent().parent().parent();
	var page_target = parent.find('.page-target');
	var page_target_child_num = page_target.find('.page-target-child').length;
	var output = '';
	
	output += '<div class="page-target-child page-target-'+(page_target_child_num+1)+'">';
	
	output += '	<div class="col-sm-1"></div><div class="col-sm-11"><legend></legend></div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">';
	output += '			<span class="num-page-target-child-identity label label-warning">'+(page_target_child_num+1)+'</span>';
	output += '			Target';
	output += '		</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="judul-target[]" class="form-control" required="" type="text">';
	output += '			<input name="type[]" required="required" class="form-control" value="target" type="hidden">';
	output += '			<input type="hidden" name="hidId3[]" value="" class="form-control">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Keterangan</label>';
	output += '		<div class="col-sm-10">';
	output += '			<textarea name="target[]" required="required" class="form-control"></textarea>';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Rencana Mulai</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tglrencanamulai[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Target Selesai</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tgltargetselesai[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Pelaksanaan</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tglpelaksanaan[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Selesai</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tglselesai[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Capaian</label>';
	output += '		<div class="col-sm-10">';
	output += '			<div class="input-group">';
	output += '				<input name="capaian[]" onkeypress="return isNumberKey(event)" required="required" class="form-control" type="text">';
	output += '				<div class="input-group-addon">%</div>';
	output += '			</div>';
	output += '		</div>';
	output += '	</div>';
	
	
	output += ' <div class="bukti-pendukung-group">';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Bukti Pendukung</label>';
	output += '		<div class="col-sm-10">';
	output += '			<span class="result-attach"></span>';
	output += '			<input name="buktipendukung[]" class="form-control" type="hidden">';
	output += '			<a onclick="show_modal(this, \'link\')" data-original-title="Attach Link" class="btn btn-success btn-xs pull-left btn-tooltip white" data-placement="bottom" title="Attach Link" >';
	output += '				<i class="fa fa-link"></i>';
	output += '			</a>';
	output += '			<a onclick="show_modal(this, \'library\')" data-original-title="Attach Library" style="margin: 0 2px" class="white btn btn-success btn-xs pull-left btn-tooltip" data-placement="bottom" title="Open Library">';
	output += '				<i class="fa fa-archive"></i>';
	output += '			</a>';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<!-- modal library -->';
	output += '	<div class="modal fade modal-library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	output += '		<div class="modal-dialog">';
	output += '			<div class="modal-content">';
	output += '				<div class="modal-header">';
	output += '					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
	output += '					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive"></i> Media Library</h4>';
	output += '				</div>';
	output += '				<div class="modal-body">';
	output += '					<div class="library-content"></div>';
	output += '				</div>';
	output += '				<div class="modal-footer">';
	output += '					<button type="button" class="close-button btn btn-default" data-dismiss="modal">Close</button>';
	output += '					<a type="button" class="back-button btn btn-primary pull-left" onclick="back_btn_(this)"><i class="fa fa-arrow-circle-left"></i> Back</a>';
	output += '					<a type="button" class="home-button btn btn-primary pull-left" onclick="folder_back(this)"><i class="fa fa-home"></i> Home</a>';
	output += '				</div>';
	output += '			</div>';
	output += '		</div>';
	output += '	</div>';
	output += '	<!-- modal library -->';
	
	output += '	<!-- modal link -->';
	output += '	<div class="modal fade modal-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	output += '		<div class="modal-dialog">';
	output += '			<div class="modal-content">';
	output += '				<div class="modal-header">';
	output += '					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
	output += '					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-link"></i> Attach Link</h4>';
	output += '				</div>';
	output += '				<div class="modal-body">';
	output += '					<input name="buktipendukung-link[]" class="form-control" type="text" onclick="sethttp(this)">';
	output += '				</div>';
	output += '				<div class="modal-footer">';
	output += '					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	output += '					<button type="button" class="btn btn-primary" onclick="attach_(this)"><i class="fa fa-check"></i> Attach</button>';
	output += '				</div>';
	output += '			</div>';
	output += '		</div>';
	output += '	</div>';
	output += '	<!-- modal link -->';
	
	output += ' </div>'; //---<div class="bukti-pendukung-group">
	
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Kendala</label>';
	output += '		<div class="col-sm-10">';
	output += '			<textarea name="kendala[]" required="required" class="form-control"></textarea>';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">&nbsp;</label>';
	output += '		<div class="col-sm-10">';
	output += '			<a href="javascript::" onclick="delete_(this,\'target\')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Target</a>';
	output += '		</div>';
	output += '	</div>';
	
	output += '</div>';
	
	page_target.append(output);
	
	$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	
	page_target.find('.target-num').val(page_target_child_num+1);
	
}

function add_baseline(e){
	var parent = $(e).parent().parent().parent();
	var page_baseline = parent.find('.page-baseline');
	var page_baseline_child_num = page_baseline.find('.page-baseline-child').length;
	var output = '';
	
	output += '<div class="page-baseline-child page-baseline-'+(page_baseline_child_num+1)+'">';
		
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">';
	output += '			<span class="num-page-baseline-child-identity label label-danger">'+(page_baseline_child_num+1)+'</span>';
	output += '			Baseline';
	output += '		</label>';
	output += '		<div class="col-sm-10">';
	output += '			<textarea name="baseline[]" required="required" class="form-control"></textarea>';
	output += '			<input name="type[]" required="required" class="form-control" value="baseline" type="hidden">';
	output += '			<input type="hidden" name="hidId2[]" value="" class="form-control">';
	output += '		</div>';
	output += '</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">&nbsp;</label>';
	output += '		<div class="col-sm-10">';
	output += '			<a href="javascript::" onclick="delete_(this,\'baseline\')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Baseline</a>';
	output += '		</div>';
	output += '	</div>';
	
	output += '</div>';
	
	page_baseline.append(output);
	
	$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	
	page_baseline.find('.baseline-num').val(page_baseline_child_num+1);
}

function add_kegiatan(e){
	var parent = $(e).parent();
	var page_kegiatan = parent.find('.page-kegiatan');
	var page_kegiatan_num = page_kegiatan.find('.page-kegiatan-parent').length;
	var output = '';
	
	var page_keg_parent = page_kegiatan.find('.page-kegiatan-parent');
	for(var i=0;i<page_kegiatan_num;i++){
		$(page_keg_parent[i]).attr('class', 'page-kegiatan-parent '+(i+1));
		$(page_keg_parent[i]).hide();
	}
	
	output += '<div class="page-kegiatan-parent '+(page_kegiatan_num+1)+' active" data-parent="'+(page_kegiatan_num+1)+'">';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">';
	output += '			<span class="num-identity label label-success">'+(page_kegiatan_num+1)+'</span>';
	output += '			Judul';
	output += '		</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="judul[]" required="required" class="form-control" type="text">';
	output += '			<input name="type[]" required="required" class="form-control" value="kegiatan" type="hidden">';
	output += '			<input type="hidden" name="hidId[]" value="" class="form-control">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Keterangan</label>';
	output += '		<div class="col-sm-10">';
	output += '			<textarea name="keterangan-kegiatan[]" required="required" class="form-control"></textarea>';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Rencana Mulai</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tglrencanamulai[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Target Selesai</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tgltargetselesai[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Pelaksanaan</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tglpelaksanaan[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">Tanggal Selesai</label>';
	output += '		<div class="col-sm-10">';
	output += '			<input name="tglselesai[]" class="form_datetime form-control" required="" type="text">';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="form-group">';
	output += '		<label class="col-sm-2 control-label">&nbsp;</label>';
	output += '		<div class="col-sm-10">';
	output += '			<a href="javascript::" onclick="add_baseline(this)" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Baseline</a>';
	output += '			<a href="javascript::" onclick="add_target(this)" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Target</a>';
	output += '			<a href="javascript::" onclick="delete_(this,\'kegiatan\')" class="delete-kegiatan btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Kegiatan</a>';
	output += '		</div>';
	output += '	</div>';
	
	output += '	<div class="page-baseline">';
	output += ' <legend></legend>';
	output += '	<input name="baseline-num[]" class="form-control baseline-num" value="1" type="hidden">';
	output += '		<div class="page-baseline-child page-baseline-1">';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">';
	output += '					<span class="num-page-baseline-child-identity label label-danger">1</span>';
	output += '					Baseline';
	output += '				</label>';
	output += '				<div class="col-sm-10">';
	output += '					<textarea name="baseline[]" required="required" class="form-control"></textarea>';
	output += '					<input name="type[]" required="required" class="form-control" value="baseline" type="hidden">';
	output += '					<input type="hidden" name="hidId2[]" value="" class="form-control">';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">&nbsp;</label>';
	output += '				<div class="col-sm-10">';
	output += '					<a href="javascript::" onclick="delete_(this,\'baseline\')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Baseline</a>';
	output += '				</div>';
	output += '			</div>';
	output += '		</div>'; /*end of <div class="page-baseline-1">*/
	
	output += '	</div>'; /*end of <div class="page-baseline">*/
	
	output += '	<div class="page-target">';
	output += ' <legend></legend>';
	output += '		<input name="target-num[]" class="form-control target-num" value="1" type="hidden">';
	output += '		<div class="page-target-child page-target-1">';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">';
	output += '					<span class="num-page-target-child-identity label label-warning">1</span>';
	output += '					Target';
	output += '				</label>';
	output += '				<div class="col-sm-10">';
	output += '					<input name="judul-target[]" class="form-control" required="" type="text">';
	output += '					<input name="type[]" required="required" class="form-control" value="target" type="hidden">';
	output += '					<input type="hidden" name="hidId3[]" value="" class="form-control">';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Keterangan</label>';
	output += '				<div class="col-sm-10">';
	output += '					<textarea name="target[]" required="required" class="form-control"></textarea>';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Tanggal Rencana Mulai</label>';
	output += '				<div class="col-sm-10">';
	output += '					<input name="tglrencanamulai[]" class="form_datetime form-control" required="" type="text">';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Tanggal Target Selesai</label>';
	output += '				<div class="col-sm-10">';
	output += '					<input name="tgltargetselesai[]" class="form_datetime form-control" required="" type="text">';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Tanggal Pelaksanaan</label>';
	output += '				<div class="col-sm-10">';
	output += '					<input name="tglpelaksanaan[]" class="form_datetime form-control" required="" type="text">';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Tanggal Selesai</label>';
	output += '				<div class="col-sm-10">';
	output += '					<input name="tglselesai[]" class="form_datetime form-control" required="" type="text">';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Capaian</label>';
	output += '				<div class="col-sm-10">';
	output += '					<div class="input-group">';
	output += '						<input name="capaian[]" onkeypress="return isNumberKey(event)" required="required" class="form-control" type="text">';
	output += '						<div class="input-group-addon">%</div>';
	output += '					</div>';
	output += '				</div>';
	output += '			</div>';
	
	output += ' 		<div class="bukti-pendukung-group">';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Bukti Pendukung</label>';
	output += '				<div class="col-sm-10">';
	output += '					<span class="result-attach"></span>';
	output += '					<input name="buktipendukung[]" class="form-control" type="hidden">';
	output += '					<a onclick="show_modal(this, \'link\')" data-original-title="Attach Link" class="btn btn-success btn-xs pull-left btn-tooltip white" data-placement="bottom" title="Attach Link" >';
	output += '						<i class="fa fa-link"></i>';
	output += '					</a>';
	output += '					<a onclick="show_modal(this, \'library\')" data-original-title="Attach Library" style="margin: 0 2px" class="white btn btn-success btn-xs pull-left btn-tooltip" data-placement="bottom" title="Open Library">';
	output += '						<i class="fa fa-archive"></i>';
	output += '					</a>';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<!-- modal library -->';
	output += '			<div class="modal fade modal-library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	output += '				<div class="modal-dialog">';
	output += '					<div class="modal-content">';
	output += '						<div class="modal-header">';
	output += '							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
	output += '							<h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive"></i> Media Library</h4>';
	output += '						</div>';
	output += '						<div class="modal-body">';
	output += '							<div class="library-content"></div>';
	output += '						</div>';
	output += '						<div class="modal-footer">';
	output += '							<button type="button" class="close-button btn btn-default" data-dismiss="modal">Close</button>';
	output += '							<a type="button" class="back-button btn btn-primary pull-left" onclick="back_btn_(this)"><i class="fa fa-arrow-circle-left"></i> Back</a>';
	output += '							<a type="button" class="home-button btn btn-primary pull-left" onclick="folder_back(this)"><i class="fa fa-home"></i> Home</a>';
	output += '						</div>';
	output += '					</div>';
	output += '				</div>';
	output += '			</div>';
	output += '			<!-- modal library -->';
	
	output += '			<!-- modal link -->';
	output += '			<div class="modal fade modal-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	output += '				<div class="modal-dialog">';
	output += '					<div class="modal-content">';
	output += '						<div class="modal-header">';
	output += '							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
	output += '							<h4 class="modal-title" id="myModalLabel"><i class="fa fa-link"></i> Attach Link</h4>';
	output += '						</div>';
	output += '						<div class="modal-body">';
	output += '							<input name="buktipendukung-link[]" class="form-control" type="text" onclick="sethttp(this)">';
	output += '						</div>';
	output += '						<div class="modal-footer">';
	output += '							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	output += '							<button type="button" class="btn btn-primary" onclick="attach_(this)"><i class="fa fa-check"></i> Attach</button>';
	output += '						</div>';
	output += '					</div>';
	output += '				</div>';
	output += '			</div>';
	output += '			<!-- modal link -->';
	
	output += ' 		</div>'; //---<div class="bukti-pendukung-group">
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">Kendala</label>';
	output += '				<div class="col-sm-10">';
	output += '					<textarea name="kendala[]" required="required" class="form-control"></textarea>';
	output += '				</div>';
	output += '			</div>';
	
	output += '			<div class="form-group">';
	output += '				<label class="col-sm-2 control-label">&nbsp;</label>';
	output += '				<div class="col-sm-10">';
	output += '					<a href="javascript::" onclick="delete_(this,\'target\')" class="btn btn-danger pull-right" style="margin-bottom: 10px;"><i class="fa fa-minus"></i> Delete Target</a>';
	output += '				</div>';
	output += '			</div>';
	
	output += '		</div>'; /*end of <div class="page-target-1">*/
	
	output += '	</div>'; /*end of <div class="page-target">*/
	
	output += '</div>';
	
	page_kegiatan.append(output);
	
	$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
	
	if($('.delete-kegiatan').length!=1){
		$('.delete-kegiatan').show();
	}else $('.delete-kegiatan').hide();
	
	$.each($('.page-list li'), function(i, f){
		$(this).removeAttr('class');
	});
	$('.page-list').append('<li class="active">'+(page_kegiatan_num+1)+'</li>');
	
	var active = $('.page-list li.active').text();
	if($('.page-list li').length==active){
		$('.pager li.previous').attr('class', 'previous');
		$('.pager li.next').attr('class', 'next disabled');
	}
	
}

function delete_(e,str){
	var edit = $("input[name='page-type']").val();
	var parent = $(e).parent().parent().parent().parent();
	var parent_rem = $(e).parent().parent().parent();
	
	if(str=='target'){
		if(edit=='edit'){
			var targetid = parent_rem.find('input[name="hidId3[]"]').val();
			if(targetid){
				var x = confirm('Delete this?');
				if(x){
					$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/administration/proker/delete_',
					data : $.param({
						str : str,
						targetid : targetid
					}),
					success : function(msg) {
						alert(msg);
						remove_(e,str);
					}
					});
				}
			}else{
				remove_(e,str);
			}
		}else{
			remove_(e,str);
		}
		
	}
	else if(str=='baseline'){
		if(edit=='edit'){
			var baselineid = parent_rem.find('input[name="hidId2[]"]').val();
			if(baselineid){
				var x = confirm('Delete this?');
				if(x){
					$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/administration/proker/delete_',
					data : $.param({
						str : str,
						baselineid : baselineid
					}),
					success : function(msg) {
						alert(msg);
						remove_(e,str);
					}
					});
				}
			}else{
				remove_(e,str);
			}
		}else{
			remove_(e,str);
		}
		
	}
	else if(str=='kegiatan'){
		if(edit=='edit'){
			var kegiatanid = parent_rem.find('input[name="hidId[]"]').val();
			if(kegiatanid){
				var x = confirm('Delete this?');
				if(x){
					$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/administration/proker/delete_',
					data : $.param({
						str : str,
						kegiatanid : kegiatanid
					}),
					success : function(msg) {
						alert(msg);
						remove_(e,str);
						set_pagination();
					}
					});
				}
			}else{
				remove_(e,str);
				set_pagination();
			}
		}else{
			remove_(e,str);
			set_pagination();
		}
		
		
	} //----------end kegiatan------------------------------
	
}

function remove_(e, str){
	var parent = $(e).parent().parent().parent().parent();
	var parent_rem = $(e).parent().parent().parent();
	if(str=='target'){
		parent_rem.remove();
		
		var target = parent.find('.page-target-child');
		var num = target.length;
		for(var i=0;i<num;i++){
			$(target[i]).attr('class', 'page-target-child page-target-'+(i+1));
			$(target[i]).find('.num-page-target-child-identity').text((i+1));
		}
		
		parent.find('.target-num').val(num);
	}
	else if(str=='baseline'){
		parent_rem.remove();
		
		var base = parent.find('.page-baseline-child');
		var num = base.length;
		for(var i=0;i<num;i++){
			$(base[i]).attr('class', 'page-baseline-child page-baseline-'+(i+1));
			$(base[i]).find('.num-page-baseline-child-identity').text((i+1));
		}
		
		parent.find('.baseline-num').val(num);
	}
	else if(str=='kegiatan'){
		parent_rem.remove();
		
		var kegiatan = parent.find('.page-kegiatan-parent');
		var num = kegiatan.length;
		for(var i=0;i<num;i++){
			$(kegiatan[i]).attr('class', 'page-kegiatan-parent '+(i+1)+' active');
			$(kegiatan[i]).attr('data-parent', (i+1));
			$(kegiatan[i]).find('.num-identity').text((i+1));
		}
		
		if($('.delete-kegiatan').length==1){
			$('.delete-kegiatan').hide();
		}
	}
}

function set_pagination(){
	/*--- SET PAGINATION ---*/
	var active = $('.page-list li.active').text();
	$('.page-list li.active').remove();
	if(active!=1){
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(active-1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}
			$(this).text(i+1);	
		});
		
		$.each($('.page-kegiatan-parent'), function(i, f){
			if($(this).data('parent')==(active-1)){
				$(this).show();
			}else{
				$(this).hide();
			}	
		});
	}else{
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(Number(active)+1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}
			$(this).text(i+1);	
		});
		
		$.each($('.page-kegiatan-parent'), function(i, f){
			// alert($(this).attr('class').split(' ')[1]);
			if($(this).attr('class').split(' ')[1]==(Number(active))){/*weird*/
				$(this).show();
			}else{
				$(this).hide(); 
			}	
		});
	}
	
	if((Number(active)+1)==1||(Number(active)-1)==1){
		$('.pager li.previous').attr('class', 'previous disabled');
		$('.pager li.next').attr('class', 'next');
	}
	
	if($('.page-list li').length==1){
		$('.pager li.previous').attr('class', 'previous disabled');
		$('.pager li.next').attr('class', 'next disabled');
	}
	/*--- SET PAGINATION ---*/
}

$(document).on('click', '.pager li.next', function(){
	var check = '';
	var class_ = $(this).attr('class').split(' ');
	for(var i=0;i<class_.length;i++){
		if(class_[i]=='disabled'){
			check = 'disabled';
		}
	}
	if(check==''){
		nextprev('next');
	}
})

$(document).on('click', '.pager li.previous', function(){
	var check = '';
	var class_ = $(this).attr('class').split(' ');
	for(var i=0;i<class_.length;i++){
		if(class_[i]=='disabled'){
			check = 'disabled';
		}
	}
	if(check==''){
		nextprev('previous');
	}
})

function nextprev(str){
	var active = $('.page-list li.active').text();
	
	if(str=='next'){
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(Number(active)+1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}	
		});
		
		$.each($('.page-kegiatan-parent'), function(i, f){
			if($(this).attr('class').split(' ')[1]==(Number(active)+1)){
				$(this).show();
			}else{
				$(this).hide();
			}	
		});
		
		if((Number(active)+1)==$('.page-list li').length){
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next disabled');
		}else if((Number(active)+1)==1){
			$('.pager li.previous').attr('class', 'previous disabled');
			$('.pager li.next').attr('class', 'next');
		}else{
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next');
		}
	}else{
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(Number(active)-1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}	
		});
		
		$.each($('.page-kegiatan-parent'), function(i, f){
			// alert($(this).attr('class').split(' ')[1]);
			if($(this).attr('class').split(' ')[1]==(Number(active)-1)){
				$(this).show();
			}else{
				$(this).hide();
			}	
		});
		
		if((Number(active)-1)==1){
			$('.pager li.previous').attr('class', 'previous disabled');
			$('.pager li.next').attr('class', 'next');
		}else if((Number(active)-1)==$('.page-list li').length){
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next disabled');
		}else{
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next');
		}
	}
}

function show_modal(e, str){
	var parent = $(e).parent().parent().parent();
	
	if(str=='link'){
		var modal_ = parent.find('.modal-link');
		$(modal_).modal('show');
	}
	else if(str=='library'){
		var modal_ = parent.find('.modal-library');
		
		var btn__ = '';
		btn__ += '<button type="button" class="close-button-2 btn btn-default pull-right" data-dismiss="modal">Close</button>';
		btn__ += '<a type="button" class="back-button-2 btn btn-primary pull-left" onclick="back_btn_(this)" ><i class="fa fa-arrow-circle-left"></i> Back</a>';
		btn__ += '<a type="button" class="home-button-2 btn btn-primary pull-left" onclick="home_btn_(this)"><i class="fa fa-home"></i> Home</a>';
		parent.find('.modal-library').find('.modal-footer').html(btn__);
		
		$.ajax({			
			type : "GET",
			dataType : "html",
			url : base_url + 'module/administration/proker/get_library_content',
			success : function(msg) {
				modal_.find('.library-content').html(msg);
				$(modal_).modal('show');
			}
		});
		
		
	}
}

function view_content(e, folderid, count){
	// var parent = $(e).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent();
	var parent = $(e).parents('.bukti-pendukung-group');
	
	var btn__ = '';
	btn__ += '<button type="button" class="close-button-2 btn btn-default pull-right" data-dismiss="modal">Close</button>';
	btn__ += '<a type="button" class="back-button-2 btn btn-primary pull-left" onclick="back_btn_(this)" ><i class="fa fa-arrow-circle-left"></i> Back</a>';
	btn__ += '<a type="button" class="home-button-2 btn btn-primary pull-left" onclick="home_btn_(this)"><i class="fa fa-home"></i> Home</a>';
	parent.find('.modal-library').find('.modal-footer').html(btn__);
	
	var modal_ = parent.find('.modal-library');
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/administration/proker/get_library_content',
			data : $.param({
				folderid : folderid,
				count : count
			}),
			success : function(msg) {
				modal_.find('.library-content').html(msg);
			}
		});
}

function select_(e, fileid, filename){
	var parent = $(e).parents('.bukti-pendukung-group');
	parent.find('input[name="buktipendukung[]"]').val(fileid);
	parent.find('.result-attach').html('<span class="btn btn-info btn-xs" style="margin-bottom: 2px;" >'+filename+'</span><br>');
	
	parent.find('.modal-library').modal('hide')
}

function sethttp(e){
	var value_ = $(e).val();
	if(value_==''){
		$(e).val('http://');
	}
}

function attach_(e){
	var parent = $(e).parent().parent();
	var value  = parent.find('input[name="buktipendukung-link[]"]').val();
	var parent_outer = parent.parent().parent().parent();
	
	parent_outer.find('input[name="buktipendukung[]"]').val(value);
	parent_outer.find('.result-attach').html('<a href="'+value+'" class="btn btn-info btn-xs" style="margin-bottom: 2px;" target="_blank">'+value+'</a><br>');
	parent_outer.find('.modal-link').modal('hide')
}

function home_btn_(e){
	var parent = $(e).parents('.bukti-pendukung-group');
	
	var btn__ = '';
	btn__ += '<button type="button" class="close-button-2 btn btn-default pull-right" data-dismiss="modal">Close</button>';
	btn__ += '<a type="button" class="back-button-2 btn btn-primary pull-left" onclick="back_btn_(this)" ><i class="fa fa-arrow-circle-left"></i> Back</a>';
	btn__ += '<a type="button" class="home-button-2 btn btn-primary pull-left" onclick="home_btn_(this)"><i class="fa fa-home"></i> Home</a>';
	parent.find('.modal-library').find('.modal-footer').html(btn__);
	
	var modal_content = $(e).parents('.modal-content');
	$.ajax({			
		type : "GET",
		dataType : "html",
		url : base_url + 'module/administration/proker/get_library_content',
		success : function(msg) {
			parent.find('.library-content').html(msg);
		}
	});
}

function back_btn_(e){
	var parent = $(e).parents('.bukti-pendukung-group');
	var modal_content = $(e).parents('.modal-content');
	
	var btn__ = '';
	btn__ += '<button type="button" class="close-button-2 btn btn-default pull-right" data-dismiss="modal">Close</button>';
	btn__ += '<a type="button" class="back-button-2 btn btn-primary pull-left" onclick="back_btn_(this)" ><i class="fa fa-arrow-circle-left"></i> Back</a>';
	btn__ += '<a type="button" class="home-button-2 btn btn-primary pull-left" onclick="home_btn_(this)"><i class="fa fa-home"></i> Home</a>';
	parent.find('.modal-library').find('.modal-footer').html(btn__);
	
	var folderid = parent.find('input[name="parent-folder-current"]').val();
	var count 	 = parent.find('input[name="count-folder"]').val();	
	if((count-1)!=0){
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/administration/proker/get_library_content_back',
			data : $.param({
				folderid : folderid,
				count : count
			}),
			success : function(msg) {
				parent.find('.library-content').html(msg);
			}
		});
	}else{
		parent.find('.modal-library').modal('hide')
	}
}

function back_btn_img(e, folderid, count){
	var parent = $(e).parents('.bukti-pendukung-group');
	
	var btn__ = '';
	btn__ += '<button type="button" class="close-button-2 btn btn-default pull-right" data-dismiss="modal">Close</button>';
	btn__ += '<a type="button" class="back-button-2 btn btn-primary pull-left" onclick="back_btn_(this)" ><i class="fa fa-arrow-circle-left"></i> Back</a>';
	btn__ += '<a type="button" class="home-button-2 btn btn-primary pull-left" onclick="home_btn_(this)"><i class="fa fa-home"></i> Home</a>';
	parent.find('.modal-library').find('.modal-footer').html(btn__);
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/administration/proker/get_library_content_back',
		data : $.param({
			folderid : folderid,
			count : count,
			param : 'content-view'
		}),
		success : function(msg) {
			parent.find('.library-content').html(msg);
		}
	});
	
}

function get_content(e, fileid, folderparent, count){
	var parent = $(e).parents('.bukti-pendukung-group');
	
	parent.find('.close-button').hide();
	parent.find('.back-button').hide();
	parent.find('.home-button').hide();
	
	var btn__ = '';
	btn__ += '<button type="button" class="close-button-2 btn btn-default pull-right" data-dismiss="modal">Close</button>';
	btn__ += '<a type="button" class="back-button-2 btn btn-primary pull-left" onclick="back_btn_img(this,\''+folderparent+'\',\''+count+'\')" ><i class="fa fa-arrow-circle-left"></i> Back</a>';
	btn__ += '<a type="button" class="home-button-2 btn btn-primary pull-left" onclick="home_btn_(this)"><i class="fa fa-home"></i> Home</a>';
	parent.find('.modal-library').find('.modal-footer').html(btn__);
	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/administration/proker/get_media',
			data : $.param({
				fileid : fileid,
				folderparent : folderparent,
				count : count
			}),
			success : function(msg) {
				parent.find('.library-content').html(msg);
			}
		});
}

function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 44 || charCode > 57))return false;

    return true;
};

$(document).on('click', '#b_evaluasi', function(e) {
	  // $('#form-evaluasi').submit(function (e) { 
	  	//syarat2
		var formData = new FormData($('#form-evaluasi')[0]);
		var URL = base_url + 'module/administration/proker/save_evaluasi_toDB';
	      $.ajax({
	        url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data) 
	        {
	        	alert(data);
	        	location.reload();
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	            location.reload();
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	  // });
});
