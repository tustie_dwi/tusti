$(document).ready(function(){
	
	$("#b_perihalsurat").click(function(e){
	  var perihalsurat = $('input[name="perihalsurat"]').val();
	  var keterangan = $('input[name="keterangan"]').val();
	  // alert(jenissurat);
	  if(perihalsurat.length!=0&&keterangan.length!=0){			
		var postData = new FormData($('#form-perihal')[0]);
		$.ajax({
			url : base_url + "module/administration/conf/save_perihal_ToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
			    // alert ('Upload Success!');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Upload Failed!');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
	  }else{
		alert("Please Complete the Form");
	  }
	  e.preventDefault(); //STOP default action
	  return false;
	});
});

function edit_perihal(perihal_surat,ket){
	$('#form-box').text('Edit Perihal Surat');
	
	$('input[name="perihalsurat"]').val(perihal_surat);
	$('input[name="keterangan"]').val(ket);
	$('input[name="hidId"]').val(perihal_surat);
	
	$('#cancel-jenisperihal').show();
	
}
