$(document).ready(function(){
	//kategori surat ijin
	$("#form-ijin-kategori").submit(function(e){
		var ket	= $("textarea[name='keterangan']").val();
		if(ket!==""){
			var postData = new FormData($(this)[0]);
			$.ajax({
					url : base_url + "module/administration/conf/save_ijin",
					type: "POST",
					data : postData,
					async: false,
					success:function(msg) 
					{
						alert(msg);
					    window.location.href = base_url + "module/administration/conf/ijin";
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
					    alert ('Proses gagal!');      
					    // location.reload(); 
					},
				    cache: false,
					contentType: false,
					processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
	
	//================================================surat ijin===============================================//
	var nowTemp = new Date();
	//var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var syarat = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate()-4, 0, 0, 0, 0);
	$(".form_datetime").datepicker({
		onRender: function(date) {
		return date.valueOf() < syarat.valueOf() ? 'disabled' : '';
		}, format: 'yyyy-mm-dd', viewMode: 2
		});
	
	$("select[name='kategori_ijin']").change(function(){
		var kat = $("select[name='kategori_ijin']").val();
		var temp = $('option:selected', this).attr('temp');
		if(kat!=='0'){
			$.ajax({
				url : base_url + "module/administration/surat/get_template",
				type : "POST",
				data : $.param({
						template : temp
				}),
				dataType : "html",
				success:function(data) 
				{
					CKEDITOR.instances['surat'].setData(data);
					var isisurat = CKEDITOR.instances['surat'].getData();
					$("#surat").val(isisurat);
				}
			});
		}
	});
	
	$("textarea[name='keterangan']").blur(function(){
		var urut 		= $("input[name='urut']").val(); // urut
		var alasan 		= $("textarea[name='keterangan']").val().toLowerCase(); // alasan
		var kat 		= $('option:selected', "select[name='kategori_ijin']").attr('ket').toLowerCase(); // kategori
		var text 		= $("textarea#surat").val();// template
		var tgl 		= $("input[name='tgl_mulai']").val();
		var tgl_mulai 	= reformatDate(tgl);
		var tgl_ 		= $("input[name='tgl_selesai']").val();
		var tgl_selesai = reformatDate(tgl_);
		
		var nama 		= $("input[name='nama']").val();
		var nik 		= $("input[name='nik']").val();
		var pangkat_gol = $("input[name='pangkat_gol']").val();
		var jabatan 	= $("input[name='jabatan']").val();
		var unit 		= $("input[name='unit']").val();
		var unit_singkat= $("input[name='unit_singkat']").val();
		var total_unit	= unit.split(",");
		var unit_pemohon = "";
		$.each(total_unit,function(i,f){
			unit_pemohon += "<br>- "+f;
		});
		text = text.replace('{nama pemohon}', nama);
		text = text.replace('{nama pemohon}', nama);
		text = text.replace('{nip pemohon}', nik);
		text = text.replace('{pangkat pemohon}', pangkat_gol);
		text = text.replace('{jabatan pemohon}', jabatan);
		text = text.replace('{unit pemohon}', unit_pemohon);
		text = text.replace('{atasan}', 'Kepala Tata Usaha');
		
		text = text.replace('{no_surat}', urut);
		text = text.replace('{kategori}', kat);
		text = text.replace('{tanggal_mulai}', tgl_mulai);
		text = text.replace('{tanggal_selesai}', tgl_selesai);
		text = text.replace('{keterangan}', alasan);
		
		CKEDITOR.instances['surat'].setData(text);
	});
	
	$("#form-surat-ijin").submit(function(){
		var ket			= $("textarea[name='keterangan']").val();
		var nama		= $("input[name='nama']").val();
		var nik			= $("input[name='nik']").val();
		var unit		= $("input[name='unit']").val();
		var pangkat_gol	= $("input[name='pangkat_gol']").val();
		var isisurat = CKEDITOR.instances['surat'].getData();
		$("#surat").val(isisurat);
		
		if(nama!==""&&nik!==""&&unit!==""&&pangkat_gol!==""){
			if(ket!==""){
				var postData = new FormData($(this)[0]);
				$.ajax({
						url : base_url + "module/administration/surat/save_ijin",
						type: "POST",
						data : postData,
						async: false,
						success:function(msg) 
						{
							alert(msg);
						    window.location.href = base_url + "module/administration/surat/ijin";
						},
						error: function(jqXHR, textStatus, errorThrown) 
						{
						    alert ('Proses gagal!');      
						    // location.reload(); 
						},
					    cache: false,
						contentType: false,
						processData: false
				});
			}
			else{
				alert("Silahkan lengkapi form terlebih dahulu!");
			}
		}
		else{
			alert('Syarat untuk membuat surat ijin tidak terpenuhi!')
		}
		e.preventDefault();
		return false;
	});
});

function printSurat(elem)
{
	 var printContents = document.getElementById(elem).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
};

function reformatDate(dateStr){
  dArr = dateStr.split("-");  // ex input "2010-01-18"
  return dArr[2]+ "-" +dArr[1]+ "-" +dArr[0]; //ex out: "18/01/10"
};

function validate(str,id){
	if(str == "setuju" || str == "tolak"){
		//alert(str);
		$.ajax({
			url : base_url + "module/administration/surat/validate_ijin",
			type : "POST",
			data : $.param({
					status : str,
					id : id
			}),
			dataType : "html",
			success:function(msg) 
			{
				alert(msg);
				location.reload();
			}
		});
	}
}
