$(document).ready(function() {
	$(".e9").select2();
	var edit_ = $('input[name="system-type"]').val();
	if(edit_=='edit'){
		var active = $('.page-list li.active').text();
		if($('.page-list li').length==1||$('.page-list li').length==0){
			$('.pager li.previous').attr('class', 'previous disabled');
			$('.pager li.next').attr('class', 'next disabled');
		}
		
		if($('.page-list li').length>1){
			$('.pager li.previous').attr('class', 'previous disabled');
			$('.pager li.next').attr('class', 'next');
		}
		
		show_delete();
	}
});

$("#export").click(function() {
	ExcellentExport.excel(this, 'myTable', 'progker');
});

function set_validasi(rencanaid, stat){
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/administration/proker/set_validasi_proker',
		data : $.param({
			rencanaid : rencanaid,
			stat : stat
		}),
		success : function(msg) {
			if(msg=='success'){
				alert('Proses validasi berhasil!');
				location.reload();
			}
		}
	});
}

function show_delete(){
	var cParent = $('.detail-parent').length;
	// alert(cParent);
	if(cParent>1&&cParent>0){
		$('.delete-parent').show();
	}else{
		$('.delete-parent').hide();
		$('.detail-parent').show();
	}
	
}

function add_parent(){
	var cParent = $('.detail-parent').length;
	var output = '';
	
	$.each($('.detail-parent'), function(i, f){
		$(this).hide();
	});
	
	output += '<div class="detail-parent '+(cParent+1)+'" data-parent="'+(cParent+1)+'">';
	output += '<div class="form-group">';
	output += '<label class="col-sm-2 control-label"><span class="num-identity label label-success">'+(cParent+1)+'</span> Detail Kegiatan</label>';
	output += '<div class="col-sm-9">';
	output += '<input name="kegiatan[]" required="required" class="form-control" type="text">';
	output += '<input name="type[]" required="required" class="form-control" value="parent" type="hidden">';
	output += '</div>';
	output += '<a href="javascript::" onclick="add_child(this)" class="btn btn-info" style="margin-right: 5px;"><i class="fa fa-plus"></i></a>';
	output += '<a style="display: none;" href="javascript::" onclick="delete_(\'parent\',this)" class="btn btn-danger delete-parent"><i class="fa fa-minus"></i></a>';
	output += '</div>';
	
	output += '<div class="parent-kebutuhan">';
	output += '<input type="hidden" class="form-control" name="count-kebutuhan[]" value="0" />';
	
	output += '<div class="kebutuhan-form">';
	// output += '<div class="form-group">';
	// output += '<label class="col-sm-2 control-label">Kebutuhan</label>';
	// output += '<div class="col-sm-7">';
	// output += '<input name="kebutuhan[]" required="required" class="form-control" type="text">';
	// output += '</div>';
	// output += '<a href="javascript::" onclick="delete_(\'kebutuhan\',this)" class="btn btn-danger delete-kebutuhan"><i class="fa fa-minus-circle"></i> Delete Kebutuhan</a>';
	// output += '</div>';
	
	// output += '<div class="form-group">';
	// output += '<label class="col-sm-2 control-label">&nbsp;</label>';
	// output += '<div class="col-sm-9">';
	// output += '<fieldset class="form-inline">';
	// output += '<div class="form-group" style="margin-left: 1px;">';
	// output += '<div class="input-group">';
	// output += '<div class="input-group-addon">Quantity</div>';
	// output += '<input name="qty[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-qty" type="text">';
	// output += '</div>';
	// output += '</div>';
	// output += '<div class="form-group" style="margin-left: 36px;">';
	// output += '<div class="input-group">';
	// output += '<div class="input-group-addon">Harga</div>';
	// output += '<input name="harga[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-harga" type="text">';
	// output += '</div>';
	// output += '</div>';
	// output += '<div class="form-group" style="margin-left: 36px;">';
	// output += '<div class="input-group">';
	// output += '<div class="input-group-addon">Total</div>';
	// output += '<input name="total[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-total" type="text">';
	// output += '</div>';
	// output += '</div>';
	// output += '</fieldset>';
	// output += '</div>';
	// output += '</div>';
	output += '</div>';
	
	output += '</div>';
	
	// output += '<div class="form-group">';
	// output += '<label class="col-sm-2 control-label"></label>';
	// output += '<div class="col-sm-9">';
	// output += '<a href="javascript::" onclick="add_kebutuhan(this)" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Kebutuhan</a>';
	// output += '</div>';
	// output += '</div>';
	
	output += '<ul class="child" style="list-style-type: none;">';
	output += '</ul>';
	
	output += '<legend></legend>';
	output += '</div>';
	
	$('.parent-form-group').append(output);
	show_delete();
	
	$.each($('.page-list li'), function(i, f){
		$(this).removeAttr('class');
	});
	$('.page-list').append('<li class="active">'+(cParent+1)+'</li>');
	
	var active = $('.page-list li.active').text();
	if($('.page-list li').length==active){
		$('.pager li.previous').attr('class', 'previous');
		$('.pager li.next').attr('class', 'next disabled');
	}
	
}

function add_child(e){
	var parent = $(e).parent().parent();
	var cParent = parent.data('parent');
	var output = '';
	
	var ul = parent.find('ul');
	
	output += '<li class="controls" style="margin-top: 5px">';
	
	output += '<div class="form-group">';
	output += '<label class="col-sm-2 control-label">Sub Detail Kegiatan</label>';
	output += '<div class="col-sm-9">';
	output += '<input name="kegiatan[]" required="required" class="form-control" type="text">';
	output += '<input name="type[]" required="required" class="form-control" value="child" type="hidden">';
	output += '</div>';
	output += '<a href="javascript::" onclick="delete_(\'child\',this)" class="btn btn-danger delete-child"><i class="fa fa-minus"></i></a>';
	output += '</div>';
	
	output += '<div class="child-kebutuhan">'; //<div class="child-kebutuhan">
	output += '<input type="hidden" class="form-control" name="count-kebutuhan[]" value="1" />';
	
	output += '<div class="kebutuhan-form">';
	output += '<div class="form-group">';
	output += '<label class="col-sm-2 control-label">Kebutuhan</label>';
	output += '<div class="col-sm-7">';
	output += '<input name="kebutuhan[]" required="required" class="form-control" type="text">';
	output += '</div>';
	output += '<a style="margin-left:-5px;" href="javascript::" onclick="delete_(\'kebutuhan\',this)" class="btn btn-danger delete-kebutuhan"><i class="fa fa-minus-circle"></i> Delete Kebutuhan</a>';
	output += '</div>';
	
	output += '<div class="form-group">';
	output += '<label class="col-sm-2 control-label">&nbsp;</label>';
	output += '<div class="col-sm-9">';
	output += '<fieldset class="form-inline">';
	output += '<div class="form-group" style="margin-left: 1px;">';
	output += '<div class="input-group">';
	output += '<div class="input-group-addon">Quantity</div>';
	output += '<input name="qty[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-qty" type="text">';
	output += '</div>';
	output += '</div>';
	output += '<div class="form-group" style="margin-left: 20px;">';
	output += '<div class="input-group">';
	output += '<div class="input-group-addon">Harga</div>';
	output += '<input name="harga[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-harga" type="text">';
	output += '</div>';
	output += '</div>';
	output += '<div class="form-group" style="margin-left: 20px;">';
	output += '<div class="input-group">';
	output += '<div class="input-group-addon">Total</div>';
	output += '<input name="total[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-total" type="text">';
	output += '</div>';
	output += '</div>';
	output += '</fieldset>';
	output += '</div>';
	output += '</div>';
	output += '</div>';
	
	output += '</div>';  //</div class="child-kebutuhan">
	
	output += '<div class="form-group">';
	output += '<label class="col-sm-2 control-label"></label>';
	output += '<div class="col-sm-9">';
	output += '<a href="javascript::" onclick="add_kebutuhan(this,\'child\')" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Kebutuhan</a>';
	output += '</div>';
	output += '</div>';
	
	output += '<legend></legend>';
	output += '</li>';
	
	ul.append(output);
	
}

function add_kebutuhan(e, str){
	var output = '';
	var margin_ = 0;
	
	var parent = $(e).parent().parent().parent();
	var cParent = parent.data('parent');
	// alert(cParent);
	if(str!='child'){
		margin_ = '36px';
	}else{
		margin_ = '20px';
	}
	
	output += '<div class="kebutuhan-form">';
	output += '<div class="form-group">';
	output += '<label class="col-sm-2 control-label">Kebutuhan</label>';
	output += '<div class="col-sm-7">';
	output += '<input name="kebutuhan[]" required="required" class="form-control" type="text">';
	output += '</div>';
	output += '<a ';
	if(str=='child'){ output += 'style="margin-left:-6px;"'; } 
	output +=' href="javascript::" onclick="delete_(\'kebutuhan\',this)" class="btn btn-danger delete-kebutuhan"><i class="fa fa-minus-circle"></i> Delete Kebutuhan</a>';
	output += '</div>';
	
	output += '<div class="form-group">';
	output += '<label class="col-sm-2 control-label">&nbsp;</label>';
	output += '<div class="col-sm-9">';
	output += '<fieldset class="form-inline">';
	output += '<div class="form-group" style="margin-left: 1px;">';
	output += '<div class="input-group">';
	output += '<div class="input-group-addon">Quantity</div>';
	output += '<input name="qty[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-qty" type="text">';
	output += '</div>';
	output += '</div>';
	output += '<div class="form-group" style="margin-left: '+margin_+';">';
	output += '<div class="input-group">';
	output += '<div class="input-group-addon">Harga</div>';
	output += '<input name="harga[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-harga" type="text">';
	output += '</div>';
	output += '</div>';
	output += '<div class="form-group" style="margin-left: '+margin_+';">';
	output += '<div class="input-group">';
	output += '<div class="input-group-addon">Total</div>';
	output += '<input name="total[]" required="required" onkeypress="return isNumberKey(event)" class="form-control cmb-total" type="text">';
	output += '</div>';
	output += '</div>';
	output += '</fieldset>';
	output += '</div>';
	output += '</div>';
	output += '</div>';
	
	var cKebutuhan = 0;
	
	if(str!='child'){
		parent.find('.parent-kebutuhan').append(output);
		cKebutuhan = parent.find('.parent-kebutuhan').find('input[name="count-kebutuhan[]"]').val();
		parent.find('.parent-kebutuhan').find('input[name="count-kebutuhan[]"]').val( Number(cKebutuhan)+1 );
	}else{
		parent.find('.child-kebutuhan').append(output);
		cKebutuhan = parent.find('.child-kebutuhan').find('input[name="count-kebutuhan[]"]').val();
		parent.find('.child-kebutuhan').find('input[name="count-kebutuhan[]"]').val( Number(cKebutuhan)+1 );
	}
	
}

function set_pagination(){
	/*--- SET PAGINATION ---*/
	var active = $('.page-list li.active').text();
	$('.page-list li.active').remove();
	if(active!=1){
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(active-1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}
			$(this).text(i+1);	
		});
		
		$.each($('.detail-parent'), function(i, f){
			if($(this).data('parent')==(active-1)){
				$(this).show();
			}else{
				$(this).hide();
			}	
		});
	}else{
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(Number(active)+1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}
			$(this).text(i+1);	
		});
		
		$.each($('.detail-parent'), function(i, f){
			// alert($(this).attr('class').split(' ')[1]);
			if($(this).attr('class').split(' ')[1]==(Number(active))){/*weird*/
				$(this).show();
			}else{
				$(this).hide(); 
			}	
		});
		
	}
			
	if((Number(active)+1)==1||(Number(active)-1)==1){
		$('.pager li.previous').attr('class', 'previous disabled');
		$('.pager li.next').attr('class', 'next');
	}
	
	if($('.page-list li').length==1){
		$('.pager li.previous').attr('class', 'previous disabled');
		$('.pager li.next').attr('class', 'next disabled');
	}
	/*--- SET PAGINATION ---*/
}

function delete_(str, e){
	var edit_ = $('input[name="system-type"]').val();
	if(str=='parent'){
		if(edit_=='edit'){
			var detailid = $(e).parent().find('input[name="hidId2[]"]').val();
			if(detailid){
				var x = confirm('Delete this?');
				if(x){
					$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/administration/proker/delete_',
					data : $.param({
						str : str,
						detailid : detailid
					}),
					success : function(msg) {
						alert(msg);
						remove_(e,str);
						set_pagination();
					}
					});
				}
			}else{
				remove_(e,str);
				set_pagination();
			}
		}else{
			remove_(e,str);
			set_pagination();
		}
		
		
	}else if(str=='child'){
		if(edit_=='edit'){
			var detailid = $(e).parent().find('input[name="hidId2[]"]').val();
			if(detailid){
				var x = confirm('Delete this?');
				if(x){
					$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/administration/proker/delete_',
					data : $.param({
						str : str,
						detailid : detailid
					}),
					success : function(msg) {
						alert(msg);
						remove_(e,str);
					}
					});
				}
			}else{
				remove_(e,str);
			}
		}else{
			remove_(e,str);
		}	
		
	}else if(str=='kebutuhan'){
		if(edit_=='edit'){
			var kebutuhanid = $(e).parent().find('input[name="hidId3[]"]').val();
			if(kebutuhanid){
				var x = confirm('Delete this?');
				if(x){
					$.ajax({			
					type : "POST",
					dataType : "html",
					url : base_url + 'module/administration/proker/delete_',
					data : $.param({
						str : str,
						kebutuhanid : kebutuhanid
					}),
					success : function(msg) {
						alert(msg);
						remove_(e,str);
					}
					});
				}
			}else{
				remove_(e,str);
			}
			
		}else{
			remove_(e,str);
		}		
	}
	
	function remove_(e, str){
		if(str=='kebutuhan'){
			var cKebutuhan = $(e).parent().parent().parent().find('div.kebutuhan-form').length;
			$(e).parent().parent().parent().find('input[name="count-kebutuhan[]"]').val( Number(cKebutuhan)-1 );
			
			var parent = $(e).parent().parent();
			parent.remove();
		}
		if(str=='child'){
			var parent = $(e).parent().parent();
			var cParent = parent.data('parent');
			parent.remove();
		}
		if(str=='parent'){
			var parent = $(e).parent().parent();
			var cParent = parent.data('parent');
			parent.remove();
			show_delete();
			set_num();
		}
	}
	
}

function set_num(){
	var parent = $('.detail-parent');
	for(var i =0;i<parent.length;i++){
		// $(parent[i]).data('parent');
		$(parent[i]).attr('data-parent', (i+1));
		$(parent[i]).attr('class', 'detail-parent '+(i+1));
		$(parent[i]).find('.num-identity').text(''+(i+1)+'');
	};
}

$(document).on('click', '.pager li.next', function(){
	var check = '';
	var class_ = $(this).attr('class').split(' ');
	for(var i=0;i<class_.length;i++){
		if(class_[i]=='disabled'){
			check = 'disabled';
		}
	}
	if(check==''){
		nextprev('next');
	}
})

$(document).on('click', '.pager li.previous', function(){
	var check = '';
	var class_ = $(this).attr('class').split(' ');
	for(var i=0;i<class_.length;i++){
		if(class_[i]=='disabled'){
			check = 'disabled';
		}
	}
	if(check==''){
		nextprev('previous');
	}
})

// $(document).on('propertychange keyup input paste', '.cmb-harga', function(){

$(document).on('propertychange keyup input paste', '.cmb-qty', function(){
	var parent	 = $(this).parent().parent().parent();
	var qty 	 = $(this).val();
	var harga 	 = parent.find('.cmb-harga').val();
	var result 	 = qty*harga;
	parent.find('.cmb-total').val(result);
})	

$(document).on('propertychange keyup input paste', '.cmb-harga', function(){
	var parent	 = $(this).parent().parent().parent();
	var harga 	 = $(this).val();
	var qty 	 = parent.find('.cmb-qty').val();
	var result 	 = qty*harga;
	parent.find('.cmb-total').val((result));
})

function toRp(a,b,c,d,e){
	e=function(f){
		return f.split('').reverse().join('')
	};
	b=e(parseInt(a,10).toString());
	for(c=0,d='';c<b.length;c++){
		d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}
	}
	return'Rp.\t'+e(d)+',00';
}

function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 44 || charCode > 57))return false;

    return true;
};

function nextprev(str){
	var active = $('.page-list li.active').text();
	// alert(active);
	if(str=='next'){
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(Number(active)+1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}	
		});
		
		$.each($('.detail-parent'), function(i, f){
			if($(this).attr('class').split(' ')[1]==(Number(active)+1)){
				$(this).show();
			}else{
				$(this).hide();
			}	
		});
		
		if((Number(active)+1)==$('.page-list li').length){
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next disabled');
		}else if((Number(active)+1)==1){
			$('.pager li.previous').attr('class', 'previous disabled');
			$('.pager li.next').attr('class', 'next');
		}else{
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next');
		}
		
	}else{	
		$.each($('.page-list li'), function(i, f){
			if($(this).text()==(Number(active)-1)){
				$(this).attr('class', 'active');
			}else{
				$(this).removeAttr('class');
			}	
		});
		
		$.each($('.detail-parent'), function(i, f){
			// alert($(this).attr('class').split(' ')[1]);
			if($(this).attr('class').split(' ')[1]==(Number(active)-1)){
				$(this).show();
			}else{
				$(this).hide();
			}	
		});
		
		if((Number(active)-1)==1){
			$('.pager li.previous').attr('class', 'previous disabled');
			$('.pager li.next').attr('class', 'next');
		}else if((Number(active)-1)==$('.page-list li').length){
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next disabled');
		}else{
			$('.pager li.previous').attr('class', 'previous');
			$('.pager li.next').attr('class', 'next');
		}
	}
}

$(document).on('click', 'input[name="b_proker"]', function() {
  $('#form-proker').submit(function (e) {
  	// var nilaiid = $('input[name="HidNilId"]').val();
  	
	var formData = new FormData($(this)[0]);
	var URL = base_url + 'module/administration/proker/save_proker_toDB';
      $.ajax({
        url : URL,
        type: "POST",
        data : formData,
        async: false,
        success:function(data) 
        {
        	alert(data);
        	location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!');      
            location.reload();
        },
        cache: false,
        contentType: false,
        processData: false
    });
    e.preventDefault(); //STOP default action
    return false;
  });
});
