$(document).ready(function(){
	
	$("#b_instansi").click(function(e){
	  var instansi = $('input[name="instansi"]').val();
	  var alamat = $('input[name="alamat"]').val();
	  var tlp = $('input[name="tlp"]').val();
	 
	  if(instansi.length!=0&&alamat.length!=0&&tlp.length!=0){			
		var postData = new FormData($('#form-instansi')[0]);
		$.ajax({
			url : base_url + "module/administration/conf/save_instansi_ToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
			    // alert ('Upload Success!');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Upload Failed!');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
	  }else{
		alert("Please Complete the Form");
	  }
	  e.preventDefault(); //STOP default action
	  return false;
	});
});

function edit_instansi(instansi_surat,alamat,telp){
	$('#form-box').text('Edit Instansi Surat');
	
	var instansi = $('input[name="instansi"]').val(instansi_surat);
	var alamat = $('input[name="alamat"]').val(alamat);
	var tlp = $('input[name="tlp"]').val(telp);
	
	$('input[name="hidId"]').val(instansi_surat);
	
	$('#cancel-instansi').show();
	
}
