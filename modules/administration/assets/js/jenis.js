$(document).ready(function(){
	
	$("#b_jenissurat").click(function(e){
	  var jenissurat = $('input[name="jenissurat"]').val();
	  var keterangan = $('input[name="keterangan"]').val();
	  // alert(jenissurat);
	  if(jenissurat.length!=0&&keterangan.length!=0){			
		var postData = new FormData($('#form-jenis')[0]);
		$.ajax({
			url : base_url + "module/administration/conf/save_jenis_ToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
				alert(data);
			    // alert ('Upload Success!');
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Upload Failed!');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
	  }else{
		alert("Please Complete the Form");
	  }
	  e.preventDefault(); //STOP default action
	  return false;
	});
});

function edit_jenis(jenis_surat,ket){
	$('#form-box').text('Edit Jenis Surat');
	
	$('input[name="jenissurat"]').val(jenis_surat);
	$('input[name="keterangan"]').val(ket);
	$('input[name="hidId"]').val(jenis_surat);
	
	$('#cancel-jenissurat').show();
	
}
