<?php
class model_publish extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
				
	function read($keyword, $page = 1, $perpage = 10, $id) {
	
		$keyword 	= $this->db->escape($keyword);
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
				mid(md5(`tbl_jurnal`.`jurnal_id`),5,5) as `id`, 
				`tbl_jurnal`.`jurnal_id`,
				`tbl_jurnal`.`jurnal_type`,
				`tbl_jurnal`.`periode`,
				`tbl_jurnal`.`edisi`,
				`tbl_jurnal`.`no`,
				`tbl_jurnal`.`volume`,
				`tbl_jurnal`.`issn`,
				`tbl_jurnal`.`page`,
				`tbl_jurnal`.`cover`,
				`tbl_jurnal`.`file`,
				`tbl_jurnal`.`keterangan`,
				`tbl_jurnal`.`is_publish`,
				`tbl_jurnal`.`user`,
				`tbl_jurnal`.`last_update`
				FROM
				`dbptiik_jurnal`.`tbl_jurnal`
				WHERE 1 = 1 
				";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_jurnal`.`jurnal_id`),5,5)='".$id."' ";
		}
		
		$sql= $sql . "ORDER BY `tbl_jurnal`.`last_update` DESC LIMIT $offset, $perpage";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function read_artikel($id, $ispub) {
			
		$sql = "SELECT
				mid(md5(`tbl_artikel`.`artikel_id`),5,5) as `id`, 
				`tbl_artikel`.`artikel_id`,
				`tbl_artikel`.`jurnal_id`,
				`tbl_artikel`.`periode`,
				`tbl_artikel`.`jurnal_type`,
				`tbl_artikel`.`reg_number`,
				`tbl_artikel`.`judul_in`,
				`tbl_artikel`.`abstrak_in`,
				`tbl_artikel`.`file_in`,
				`tbl_artikel`.`url_in`,
				`tbl_artikel`.`editor_choice`,
				`tbl_artikel`.`page`,
				`tbl_artikel`.`keyword_in`,
				`tbl_artikel`.`is_publish`,
				(SELECT `a`.nama FROM dbptiik_jurnal.tbl_author as `a` WHERE `a`.artikel_id = `tbl_artikel`.artikel_id AND `a`.author_ke='1') as `mhs`,
				`tbl_artikel`.`user`,
				`tbl_artikel`.`last_update`
				FROM
				 dbptiik_jurnal.`tbl_artikel`
				WHERE 1 = 1 
				";
		/*if($id!=""){
			$sql=$sql . " AND (`tbl_artikel`.`periode`='".$id."' OR `tbl_artikel`.`periode` IN (SELECT periode FROM dbptiik_jurnal.tbl_jurnal WHERE mid(md5(`tbl_jurnal`.`jurnal_id`),5,5)='".$id."'))";
		}*/
		
		switch($ispub){
			case 0:
				$sql=$sql . " AND (`tbl_artikel`.`periode`='".$id."' OR `tbl_artikel`.`periode` IN (SELECT periode FROM dbptiik_jurnal.tbl_jurnal WHERE mid(md5(`tbl_jurnal`.`jurnal_id`),5,5)='".$id."')) AND `tbl_artikel`.`is_publish`='0' ORDER BY `tbl_artikel`.`reg_number` ASC";
			break;
			
			case 1;
				$sql=$sql . " AND ( mid(md5(`tbl_artikel`.`jurnal_id`),5,5)='".$id."' OR `tbl_artikel`.`is_publish`='0')  							
								ORDER BY `tbl_artikel`.`reg_number` ASC";
			break;
			
			default;
				$sql=$sql . " AND (`tbl_artikel`.`periode`='".$id."' OR `tbl_artikel`.`periode` IN (SELECT periode FROM dbptiik_jurnal.tbl_jurnal WHERE mid(md5(`tbl_jurnal`.`jurnal_id`),5,5)='".$id."')) ORDER BY `tbl_artikel`.`reg_number` ASC";
			break;
		}
		/*if($ispub==1){
			//$sql = $sql. " AND `tbl_artikel`.`is_publish`='".$ispub."' ";
			$sql=$sql . " AND (`tbl_artikel`.`periode`='".$id."' OR `tbl_artikel`.`periode` IN (SELECT periode FROM dbptiik_jurnal.tbl_jurnal WHERE mid(md5(`tbl_jurnal`.`jurnal_id`),5,5)='".$id."'))";
		}else{
			$sql=$sql . " AND (`tbl_artikel`.`periode`='".$id."' OR `tbl_artikel`.`periode` IN (SELECT periode FROM dbptiik_jurnal.tbl_jurnal WHERE mid(md5(`tbl_jurnal`.`jurnal_id`),5,5)='".$id."')) ORDER BY `tbl_artikel`.`reg_number` ASC";
		}
		*/
		
		//$sql= $sql . "ORDER BY `tbl_artikel`.`reg_number` DESC ";
		
		//echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_periode_jurnal(){
		$sql = "SELECT DISTINCT periode FROM dbptiik_jurnal.`tbl_artikel` ORDER BY periode ASC ";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
			
	function replace_publish($datanya) {
		return $this->db->replace('dbptiik_jurnal`.`tbl_jurnal',$datanya);		
	}
	
	function update_publish($datanya,$idnya) {
		return $this->db->update('dbptiik_jurnal`.`tbl_jurnal',$datanya,$idnya);
	}
	
	function update_artikel($datanya,$idnya) {
		return $this->db->update('dbptiik_jurnal`.`tbl_artikel',$datanya,$idnya);
	}
	
	function delete_artikel($datanya,$idnya){
		return $this->db->update('dbptiik_jurnal`.`tbl_artikel',$datanya,$idnya);
	}
	
}