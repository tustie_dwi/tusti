<?php
class model_artikel extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
				
	function read($keyword, $page = 1, $perpage = 10, $id) {
	
		$keyword 	= $this->db->escape($keyword);
		$offset 	= ($page-1)*$perpage;
		
		$sql = "SELECT
				mid(md5(`tbl_artikel`.`artikel_id`),5,5) as `id`, 
				`tbl_artikel`.`artikel_id`,
				`tbl_artikel`.`jurnal_id`,
				`tbl_artikel`.`periode`,
				`tbl_artikel`.`jurnal_type`,
				`tbl_artikel`.`reg_number`,
				`tbl_artikel`.`judul_in`,
				`tbl_artikel`.`judul_en`,
				`tbl_artikel`.`abstrak_in`,
				`tbl_artikel`.`abstrak_en`,
				`tbl_artikel`.`full_file_in`,
				`tbl_artikel`.`file_in`,
				`tbl_artikel`.`file_en`,
				`tbl_artikel`.`url_full_file_in`,
				`tbl_artikel`.`url_in`,
				`tbl_artikel`.`page`,
				`tbl_artikel`.`keyword_in`,
				`tbl_artikel`.`keyword_en`,
				`tbl_artikel`.`is_publish`,
				(SELECT `a`.nama FROM dbptiik_jurnal.tbl_author as `a` WHERE `a`.artikel_id = `tbl_artikel`.artikel_id AND `a`.author_ke='1') as `mhs`,
				`tbl_artikel`.`user`,
				`tbl_artikel`.`last_update`
				FROM
				 dbptiik_jurnal.`tbl_artikel`
				WHERE 1 = 1 
				";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_artikel`.`artikel_id`),5,5)='".$id."' ";
		}
		
		$sql= $sql . "ORDER BY `tbl_artikel`.`last_update` DESC LIMIT $offset, $perpage";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_periode_by_id($id){
		$sql = "SELECT periode FROM dbptiik_jurnal.`tbl_artikel` WHERE artikel_id ='".$id."' ";
		$result = $this->db->query( $sql );
			
		foreach($result as $dt):
			$str	= $dt->periode;
		endforeach;
		
		return $str;
	}
	
	function get_author($id){
		$sql = "SELECT
					`tbl_author`.`author_id`,
					`tbl_author`.`artikel_id`,
					`tbl_author`.`nama`,
					`tbl_author`.`instansi`,
					`tbl_author`.`email`,
					`tbl_author`.`mahasiswa_id`,
					`tbl_author`.`karyawan_id`,
					`tbl_author`.`author_ke`,
					`tbl_author`.`user`,
					`tbl_author`.`last_update`
					FROM
						dbptiik_jurnal.`tbl_author`
					WHERE 1 =1 
					";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_author`.`artikel_id`),5,5)='".$id."'";
		}
		$sql= $sql . "ORDER BY `author_ke` ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
			
	function replace_artikel($datanya) {
		return $this->db->replace('dbptiik_jurnal`.`tbl_artikel',$datanya);
	}
	
	function replace_author($datanya) {
		return $this->db->replace('dbptiik_jurnal`.`tbl_author',$datanya);
	}
	
	function delete_author($datanya){
		if($this->db->delete('dbptiik_jurnal`.`tbl_author', $datanya))		
			return true;
		else $this->error = $this->db->getLastError();
			return false;
	}
	
	
	function update_artikel($datanya,$idnya) {
		return $this->db->update('dbptiik_jurnal`.`tbl_artikel',$datanya,$idnya);
	}
	
	function delete_artikel($datanya){
		$this->db->delete('dbptiik_jurnal`.`tbl_author', $datanya);
		if($this->db->delete('dbptiik_jurnal`.`tbl_artikel', $datanya))		
			return true;
		else $this->error = $this->db->getLastError();
			return false;
	}
	
	
	
	function get_no_periode($str){
		if($str < 7){
			$strno	= 1;
		}else{
			$strno	= 2;
		}
		
		return $strno;
	}
	
	function get_periode($str){
		if($str < 7){
			$m	= '06';
		}else{
			$m	= '12';
		}
		$strperiode = date("Y")."-".$m;
		
		return $strperiode;
	}
	
	function get_reg_number($str){
		$sql="SELECT concat('DR',RIGHT(concat( '00000' , CAST(IFNULL(MAX(CAST(right(reg_number,5) AS unsigned)), 0) + 1 AS unsigned)),5)) as `data` 
				FROM dbptiik_jurnal.tbl_artikel WHERE (periode='".$str."' AND jurnal_type='IN')";		
		$result = $this->db->query( $sql );
		
		foreach($result as $dt):
			$strresult = $dt->data;
		endforeach;
		
		return $strresult;
	}
	
	
}