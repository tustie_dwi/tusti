<?php
class model_conf extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	/* master konfigurasi */
	function read() {		
		$sql = "SELECT
					`dbptiik_master`.`tbl_config`.`tahun`,
					`dbptiik_master`.`tbl_config`.`is_ganjil`,
					`dbptiik_master`.`tbl_config`.`is_pendek`,
					`dbptiik_master`.`tbl_config`.`kode_skripsi`,
					`dbptiik_master`.`tbl_config`.`is_aktif`
				FROM
					`dbptiik_master`.`tbl_config`					
				WHERE is_aktif='1'
				";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jenjang pendidikan */
	function  get_jenjang_pendidikan(){
		$sql = "SELECT `dbptiik_master`.`tbl_jenjangpendidikan`.`jenjang_pendidikan` FROM `dbptiik_master`.`tbl_jenjangpendidikan`";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jabatan */
	function  get_jabatan(){
		$sql = "SELECT `dbptiik_master`.`tbl_jabatan`.`jabatan_id`, `dbptiik_master`.`tbl_jabatan`.`keterangan` FROM `dbptiik_master`.`tbl_jabatan` ORDER BY urut ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester aktif */
	function get_semester_aktif(){
		$sql = "SELECT
					`dbptiik_akademik`.`tbl_tahunakademik`.`tahun_akademik`,
					`dbptiik_akademik`.`tbl_tahunakademik`.`tahun`,
					`dbptiik_akademik`.`tbl_tahunakademik`.`is_ganjil`,
					`dbptiik_akademik`.`tbl_tahunakademik`.`is_aktif`,
					`dbptiik_akademik`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`dbptiik_akademik`.`tbl_tahunakademik`
					WHERE
					`dbptiik_akademik`.`tbl_tahunakademik`.`is_aktif` =  '1'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	/* master program studi atau jurusan*/
	function get_prodi($term){
		$sql= "SELECT prodi_id as `id`, keterangan as `value` FROM `dbptiik_master`.`tbl_prodi` WHERE keterangan like '%".$term."%' ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master unit kerja atau struktur*/
	function get_unit($term){
		$sql= "SELECT unit_id as `id`, keterangan as `value` FROM `dbptiik_master`.`tbl_unitkerja` WHERE keterangan like '%".$term."%'  ORDER BY jenis ASC, nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	/*master mahasiswa */
	function get_mahasiswa($term){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `value` FROM `dbptiik_master`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mahasiswa */
	function get_nama_mahasiswa($term){
		$sql= "SELECT nama FROM `dbptiik_master`.`tbl_mahasiswa` 
					WHERE mahasiswa_id = '".$term."'  ";
		$result = $this->db->query( $sql );
	
		if($result){
			foreach($result as $dt){
				$strid = $dt->nama;
			}
		}else{
			$strid="-";
		}
		
		return $strid;	
	}

/*master id dosen */
	function get_id_dosen($str){
		$sql= "SELECT karyawan_id FROM `dbptiik_master`.`tbl_karyawan` 
					WHERE nama = '".$str."' ";
		$result = $this->db->query( $sql );
		
		if($result){
			foreach($result as $dt){
				$strid = $dt->karyawan_id;
			}
		}else{
			$strid="-";
		}
		return $strid;			
	}
		
	
	/*master dosen */
	function get_dosen($term){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `dbptiik_master`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif='aktif' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master dosen */
	function get_staff($term){
		$sql= "SELECT nama as `tag` FROM `dbptiik_master`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_aktif='aktif' 
				ORDER BY nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_mhs($term){
		$sql= "SELECT nama as `tag` FROM `dbptiik_master`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master pengampu */
	function get_pengampu($term){
		$sql= "SELECT nama as `value`, karyawan_id as `id` FROM `dbptiik_master`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif='aktif' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master nama mk */
	function get_namamk($term){
		$sql= "SELECT namamk_id as `id`, keterangan as `value` FROM `dbptiik_akademik`.`tbl_namamk` 
					WHERE keterangan like '%".$term."%' 
				ORDER BY keterangan ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master mk */
	function get_mk($term){
		$sql= "SELECT `dbptiik_akademik`.`tbl_matakuliah`.`matakuliah_id` as `id`, 
				concat(`dbptiik_akademik`.`tbl_matakuliah`.`kode_mk`,' - ', `dbptiik_akademik`.`tbl_namamk`.`keterangan`,'(',`dbptiik_akademik`.`tbl_matakuliah`.`sks`,' sks)') as `value` FROM `dbptiik_akademik`.`tbl_matakuliah`
					Inner Join `dbptiik_akademik`.`tbl_namamk` ON `dbptiik_akademik`.`tbl_matakuliah`.`namamk_id` = `dbptiik_akademik`.`tbl_namamk`.`namamk_id` 
					WHERE concat(`dbptiik_akademik`.`tbl_matakuliah`.`kode_mk`, '-', `dbptiik_akademik`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
				ORDER BY `dbptiik_akademik`.`tbl_matakuliah`.`matakuliah_id` ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/* master jam mulai*/
	function  get_jam_mulai($term){
		$sql = "SELECT jam_mulai as `id`, jam_mulai as `value` FROM dbptiik_akademik.tbl_jam 
				WHERE jam_mulai like '%".$term."%'
				ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jam selesai*/
	function  get_jam_selesai($term){
		$sql = "SELECT jam_mulai as `id`, jam_selesai as `value` FROM dbptiik_akademik.tbl_jam  WHERE jam_selesai like '%".$term."%' ORDER BY jam_selesai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master hari*/
	function  get_hari($term){
		$sql = "SELECT hari as `id`, hari as `value` FROM dbptiik_akademik.tbl_hari WHERE hari like '%".$term."%' ORDER BY hari ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master ruang*/
	function  get_ruang($term){
		$sql = "SELECT ruang as `id`, ruang as `value` FROM dbptiik_master.tbl_ruang WHERE keterangan like '%".$term."%' ORDER BY ruang ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kelas*/
	function  get_kelas($term){
		$sql = "SELECT kelas_id as `id`, kelas_id as `value` FROM dbptiik_akademik.tbl_kelas WHERE keterangan like '%".$term."%' ORDER BY kelas_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master nama dosen */
	function get_nama_dosen($id){
		$sql= "SELECT nama , karyawan_id ,nik FROM `dbptiik_master`.`tbl_karyawan` 
					WHERE karyawan_id = '".$id."' ";
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
		
	/*master jadwal dosen */
	function get_jadwal_dosen(){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`
				FROM
				`dbptiik_akademik`.`vw_jadwaldosen`
				";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk ditawarkan */
	function get_mkditawarkan($ispraktikum,$semester,$term){
		$sql="SELECT
				`dbptiik_akademik`.`tbl_mkditawarkan`.`mkditawarkan_id` as `id`,
				concat(`dbptiik_akademik`.`tbl_matakuliah`.`kode_mk`,' - ' ,`dbptiik_akademik`.`tbl_namamk`.`keterangan`) as `value`			
				FROM
				`dbptiik_akademik`.`tbl_mkditawarkan`
				Inner Join `dbptiik_akademik`.`tbl_matakuliah` ON `dbptiik_akademik`.`tbl_mkditawarkan`.`matakuliah_id` = `dbptiik_akademik`.`tbl_matakuliah`.`matakuliah_id`
				Inner Join `dbptiik_akademik`.`tbl_namamk` ON `dbptiik_akademik`.`tbl_matakuliah`.`namamk_id` = `dbptiik_akademik`.`tbl_namamk`.`namamk_id`		
			  WHERE concat(`dbptiik_akademik`.`tbl_matakuliah`.`kode_mk`, '-', `dbptiik_akademik`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
			";
			
		if($ispraktikum!=""){
			$sql = $sql . " AND `dbptiik_akademik`.`tbl_mkditawarkan`.`is_praktikum`= '".$ispraktikum."' ";
		}
		if($semester!=""){
			$sql = $sql . " AND `dbptiik_akademik`.`tbl_mkditawarkan`.`tahun_akademik` ='".$semester."' ";
		}
				
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jenis kegiatan*/
	function  get_jenis_kegiatan(){
		$sql = "SELECT jenis_kegiatan_id as `id`, keterangan as `value` FROM dbptiik_master.tbl_jeniskegiatan  ORDER BY keterangan ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function replace_conf($datanya) {
		return $this->db->replace('dbptiik_master`.`tbl_config',$datanya);
	}
	
	function replace_semester($datanya) {
		return $this->db->replace('dbptiik_akademik`.`tbl_tahunakademik',$datanya);
	}
	
	function update_conf($datanya,$idnya) {
		return $this->db->update('dbptiik_master`.`tbl_config',$datanya,$idnya);
	}
		
	function update_semester($datanya,$idnya) {
		return $this->db->update('dbptiik_akademik`.`tbl_tahunakademik',$datanya,$idnya);
	}
	
	public function log($tablename, $datanya, $username, $action){
		$return_arr = array();
		array_push($return_arr,$datanya);		
			
		$data['user']		= $username;
		$data['session']	= session_id();
		$data['tgl']		= date("Y-m-d H:i:s");
		$data['reference']	= $_SERVER['HTTP_REFERER'];
		$data['table_name']	= $tablename;
		$data['field']		= json_encode($return_arr);	
		$data['action']		= $action;			
		
		$result = $this->db->insert("coms`.`coms_log", $data);
		
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) return false;
	}
	
	
}