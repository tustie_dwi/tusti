<?php $this->head(); ?>
<fieldset>
	<legend><a href="<?php echo $this->location('module/jurnal/publish/write'); ?>" class="btn btn-default pull-right">
    <i class="icon-pencil icon-white"></i> New Jurnal</a> Jurnal List
	</legend>
	<div id="demo">
	<?php 
	if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	?>
	<div class="col-md-12 block-box">
	<?php
	if( isset($posts) ) :	
	 
	$str="<table class='table table-hover example'>
				<thead>
					<tr>
						<th>Jurnal</th>
						<th>Operation</th>
					</tr>
				</thead>
				<tbody>";		
			$i = 1;			
			foreach ($posts as $dt): 
							
				$str.="	<tr id='post-".$dt->jurnal_id."' data-id='".$dt->jurnal_id."' valign=top valign=top>
							<td> ";	
				$str.= "<small>Edisi: </small><code>".$dt->edisi."</code> <small>Vol: </small><code>".$dt->volume."</code>  <small>No: </small><code>".$dt->no."</code>   <span class='label label-info'>".$dt->periode."</span> ";
					if($dt->is_publish==1){
						$str.= "<span class='label label-warning'>Published</span>";
					}
				$str.= "</td><td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href=".$this->location('module/jurnal/publish/edit/'.$dt->id)."><i class='icon-pencil'></i> Edit</a>	
									</li>														
								  </ul>
								</li>
							</ul>
						</td></tr>";
			endforeach; 
			
		$str.= "</tbody></table>";
		
		echo $str;
		
		echo "</div>";
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
	</div>
</fieldset>
<?php 
$this->foot(); 


?>