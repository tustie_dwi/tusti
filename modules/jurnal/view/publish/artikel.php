<?php
	if($artikel) :	
	
		$str="<table class='table table-hover example'>
			<thead>
				<tr>							
					<th><input type='checkbox'  id='sa_artikel' name='sa_artikel' data-checkbox-name='artikel[]' class='selectall' /></th>
					<th>Mahasiswa</th>
					<th>Judul</th>	
					<th>Editor Choice</th>	
				</tr>
			</thead>
			<tbody>";		
		$i = 1;			
		foreach ($artikel as $dt): 
			$filename= "assets/uploads/file/".$dt->periode."/IN/".$dt->file_in;
			
			$str.= "<tr valign=top>
					<td><input type='checkbox' name='artikel[]' value='".$dt->artikel_id."'  data-select-all='sa_artikel' ";
			if(($jurnalid==$dt->jurnal_id)&&($dt->is_publish==1)){
				$str.= "checked ";
			}
			$str.= ">
					<td>
						<b>".$dt->mhs."</b>
						<span class='badge badge-info'>".$dt->reg_number."</span> 
						<span class='label'>".$dt->periode."</span> ";	
			$str.= "</td><td><small>".$dt->judul_in."</small></td>";
			$str.= "<td><input type=radio name='choice[]' value='".$dt->artikel_id."' ";
			if(($jurnalid==$dt->jurnal_id)&&($dt->editor_choice==1)){
				$str.= "checked ";
			}
			$str.= "></td>";
			$str.= "</tr>";
		endforeach; 
			
		$str.= "</tbody></table>";
		
		echo $str;
		
	endif;
?>