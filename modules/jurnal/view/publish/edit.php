<?php
$this->head();

if($publish!=""){
	$header		= "Edit Jurnal";	
	$frmact		= $this->location('module/jurnal/publish/save');
	
	foreach ($publish as $dt):				
		$jurnalid	= $dt->jurnal_id;
		$periodeid	= $dt->periode;
		$edisi		= $dt->edisi;
		$volume		= $dt->volume;
		$no			= $dt->no;
		$issn		= $dt->issn;
		$hal		= $dt->page;
		$catatan	= $dt->keterangan;
		$ispub		= $dt->is_publish;
	endforeach;	
		
}else{
	$header		= "Write New Jurnal";
	$frmact		= $this->location('module/jurnal/publish/write');
	$jurnalid	= "";
	$edisi		= "";
	$volume		= "";
	$no			= "";
	$issn		= "";
	$hal		= "";
	$catatan	= "";
	$ispub		= 0;
}
?>

<div class="container">  
	<fieldset>
	<legend>
		<a href="<?php echo $this->location('module/jurnal/publish'); ?>" class="btn btn-default pull-right"><i class="icon-list"></i> Jurnal List</a> 
		<?php if($publish!=""){	?>
		<a href="<?php echo $this->location('module/jurnal/publish/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Jurnal</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12 block-box">

<?php		
			$str = "<form method=post  action=".$frmact." enctype='multipart/form-data' class='form-horizontal'>";						
						$str.= "<div class='form-group'>";
							$str.= "<label>Periode</label>";
							$str.= "<div class=controls>";
								$str.= "<select name='cmbperiode' onChange='form.submit();' class='col-md-5 form-control'>";
									$str.= "<option value=''>Please Select..</option>";
									foreach($periode as $dt):
										$str.= "<option value='".$dt->periode."' ";
										if($periodeid==$dt->periode){
											$str.= "selected ";
										}
										$str.= ">".$dt->periode."</option>";
									endforeach;
								$str.= "</select>";
							$str.= "</div>";
						$str.= "</div>";			
			echo $str;	
			
			if($periodeid!=""){
				$str = "<div class='form-group'>";
					$str.= "<label>Edisi</label>";
					$str.= "<div class=controls>";
						$str.= "<input type=text name='edisi' value='".$edisi."' class='col-md-5 form-control'>";
					$str.= "</div>";
				$str.= "</div>";
				
				$str.= "<div class='form-group'>"; 
					$str.= "<label>Volume</label>";
					$str.= "<div class=controls>";
						$str.= "<input type=text name='volume' value='".$volume."' class='col-md-5 form-control'><input type=hidden name='hidId'  value='".$jurnalid."'><br>";
					$str.= "</div>";
					
				$str.= "</div>";
					$str.= "<div class='form-group'>"; 
					$str.= "<label>Nomor</label>";
					$str.= "<div class=controls>";
						$str.= "<input type=text name='no' value='".$no."' class='col-md-5 form-control' required><input type=hidden name='hidId'  value='".$jurnalid."'><br>";
					$str.= "</div>";
				$str.= "</div>";
				
				/*$str.= "<div class='form-group'>"; 
					$str.= "<label>ISSN</label>";
					$str.= "<div class=controls>";
						$str.= "<input type=text name='issn' value='".$issn."'><input type=hidden name='hidId'  value='".$jurnalid."'><br>";
					$str.= "</div>";
				$str.= "</div>";*/
				
				$str.= "<div class='form-group'>"; 
					$str.= "<label>Halaman</label>";
					$str.= "<div class=controls>";
						$str.= "<input type=text name='hal' value='".$hal."' class='col-md-5 form-control'>";
					$str.= "</div>";
				$str.= "</div>";
				
				$str.= "<div class='form-group'>"; 
					$str.= "<label>Catatan</label>";
					$str.= "<div class=controls>";
						$str.= "<textarea name='catatan' class='form-control' rows=8>".$catatan."</textarea><br>";
						$str.= "<label class='checkbox inline'><input type=checkbox name='ispub' value=1 ";
						if($ispub==1){
							$str.= "checked ";
						}
					$str.= ">Publised</label></div>";
				$str.= "</div>";			
				
				echo $str;
					?>
					
					<div class="form-group">
						<ul class="nav nav-tabs" id="writeTab">
							<li class="active"><a href="#info">Daftar Artikel</a></li>
							<li><a href="#files">Upload File</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="info">
								<?php 
								$data['jurnalid'] = $jurnalid;
								
								$this->view('publish/artikel.php', $data);?>	
							</div>
							<div class="tab-pane" id="files">	
									 <?php  																				
										$str= "<div class='form-group'>"; 
										$str.= "<label><b>File PDF</b></label>";
										$str.= "<div class=controls>";
											$str.= "<input type=file name=file id=file class='col-md-5 form-control'><br><br>";
										$str.= "</div>";
									$str.= "</div>";	
									
									$str.= "<div class='form-group'>"; 
										$str.= "<label><b>File Cover</b></label>";
										$str.= "<div class=controls>";
											$str.= "<input type=file name=cover id=cover class='col-md-5 form-control'><br><br>";
										$str.= "</div>";
									$str.= "</div>";	
								
									echo $str;					
									 ?>               
								</div><!--/tab-abstract-->
						</div>
					</div>							
						
					<div class='form-group'>
						<label class="control-label">&nbsp;</label>
						<div class="controls">
							<input type=submit name='b_publish' id='b_publish' value='  Save   ' class='btn btn-primary'>
						</div>
					</div>
					<?php } ?>
				</form>
			</div>
		</div>
	</fieldset>
</div>
<?php
$this->foot();

?>

