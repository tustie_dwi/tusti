<?php $this->head(); ?>
<fieldset>
	<legend><a href="<?php echo $this->location('module/jurnal/artikel/write'); ?>" class="btn btn-primary pull-right">
    <i class="icon-pencil icon-white"></i> New Artikel</a> Artikel List
	</legend>
	<div id="demo" class="block-box">
	<?php 
	if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	if( isset($posts) ) :	
	 
	$str="<table class='table table-hover example'>
				<thead>
					<tr>
						<th>Mahasiswa</th>
						<th>Judul</th>		
						<th>Operation</th>
					</tr>
				</thead>
				<tbody>";		
			$i = 1;			
			foreach ($posts as $dt): 
				$filename= "assets/uploads/file/".$dt->periode."/IN/".$dt->file_in;
				if($dt->full_file_in){
					$ffile = "assets/uploads/file/".$dt->periode."/IN/".$dt->full_file_in;
				}else{
					$ffile="";
				}
				
				$str.="	<tr id='post-".$dt->artikel_id."' data-id='".$dt->artikel_id."' valign=top valign=top>
							<td>
							<b>".ucwords(strtolower($dt->mhs))."</b>
							<span class='badge badge-info'>".$dt->reg_number."</span> 
							<span class='label'>".$dt->periode."</span> ";	
				$str.= "</td>
							<td><b>".$dt->judul_in."</b><small><em><b>Abstrak:</b></em>
							".$this->stringToText($dt->abstrak_in)."<br><em><b>Keyword:</b></em>
							".$dt->keyword_in."<br><em><b>Last Update:</b></em>".$dt->last_update."</small><br>";	
							
					if(file_exists($ffile)){
						$str.= "<em>Full PDF:</em> <a href=".$this->location($ffile)." target='_blank'><i class='fa fa-file'></i></a> &nbsp;";
					}
					
					if(file_exists($filename)){
						$str.= "<em>Abstrak PDF:</em> <a href=".$this->location($filename)." target='_blank'><i class='fa fa-file-o'></i></a>";
					}
					//$str.= "<em>Last Update:</em><small>".$dt->last_update."</small>";
				$str.= "</td><td style='min-width: 80px;'>            
							<ul class='nav nav-pills'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href=".$this->location('module/jurnal/artikel/edit/'.$dt->id)."><i class='icon-pencil'></i> Edit</a>	
									</li>									
									<li>
										<a id='".$dt->artikel_id."' class='btn-delete-post' ><i class='icon-remove'></i> Delete</a>
									</li>									
								  </ul>
								</li>
							</ul>
						</td></tr>";
			endforeach; 
			
		$str.= "</tbody></table>";
		
		echo $str;
		
		echo "</div>";
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</fieldset>
<?php 
$this->foot(); 


?>