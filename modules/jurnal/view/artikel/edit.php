<?php
$this->head();

if($artikel!=""){
	$header		= "Edit Artikel";	
	
	foreach ($artikel as $dt):				
		$artikelid	= $dt->artikel_id;
		$periode	= $dt->periode;
		$regnumber	= $dt->reg_number;
		$judul 		= $dt->judul_in;
		$judulen	= $dt->judul_en;
		$abstrakin	= $dt->abstrak_in;
		$keywordin	= $dt->keyword_in;
		$abstraken	= $dt->abstrak_en;
		$keyworden	= $dt->keyword_en;
		$hal		= $dt->page;
	endforeach;	
	
	$kmhs			= "Mahasiswa PTIIK, Universitas Brawijaya";
	$kdos			= "Dosen PTIIK, Universitas Brawijaya";
		
}else{
	$header			= "Write New Artikel";
	$artikelid		= "";
	$kmhs			= "Mahasiswa PTIIK, Universitas Brawijaya";
	$kdos			= "Dosen PTIIK, Universitas Brawijaya";
	$periode		= $periode;
	$regnumber		= "";
	$hal			= "";
	$judul			= "";
	$judulen		= "";
	$abstrakin		= "";
	$keywordin		= "";
	$abstraken		= "";
	$keyworden		= "";
}
?>

<div class="container">  
	<fieldset>
	<legend>
		<a href="<?php echo $this->location('module/jurnal/artikel'); ?>" class="btn btn-default pull-right"><i class="icon-list"></i> Artikel List</a> 
		<?php if($artikel!=""){	?>
		<a href="<?php echo $this->location('module/jurnal/artikel/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Artikel</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12 block-box">

<?php		
			$str = "<form method=post  action=".$this->location('module/jurnal/artikel/save')."  enctype='multipart/form-data' class='form-horizontal'>";
						
						$str.= "<div class='form-group'>";
							$str.= "<label>Periode</label>";
							$str.= "<div class=controls>";
								$str.= "<input type=text class='form-control' name='periode' value='".$periode."' readonly><input type=hidden name='regnumber' value='".$regnumber."' >";
							$str.= "</div>";
						$str.= "</div>";
						
						/*$str.= "<div class='form-group'>";
							$str.= "<label>Reg Number</label>";
							$str.= "<div class=controls>";
								$str.= "<input type=text name='regnumber' value='".$regnumber."' readonly>";
							$str.= "</div>";
						$str.= "</div>";*/
						
											
						$str.= "<div class='form-group'>"; 
							$str.= "<label>Halaman</label>";
							$str.= "<div class=controls>";
								$str.= "<input type=text class='form-control' name='hal' value='".$hal."'><input type=hidden name='hidId'  value='".$artikelid."'>";
							$str.= "</div>";
						$str.= "</div>";
						
																	
						echo $str;
						?>
						<div class="form-group">
							<ul class="nav nav-tabs" id="writeTab">
								<li class="active"><a href="#info">Penulis</a></li>
								<li><a href="#abstrak">Abstrak</a></li>
								<li><a href="#abtract">Abstract</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="info">		
									<div class="col-md-12">
									<?php
									$str="";
									for ($i=1;$i<4;$i++){
									$str.= "<div class='form-group'>"; 
									$str.= "<label>Penulis # ".$i."</label>";
									$str.= "<div class=controls>";

									$skip=false;
									if(isset($author)){							
										foreach($author as $dt){								
											if($dt->author_ke == $i){
												if($i==1){
													$str.= "<input type=text name='mhs' id='mhs' class='col-md-5 form-control' value='".$dt->nama."'><input type=hidden name='mhsid' id='mhsid'><br><br>
																	<input type=text name='memail' class='col-md-5 form-control' value='".$dt->email."' placeholder='Email penulis #".$i."..'><br><br>
																	<input type=text name='minstansi' class='col-md-5 form-control' value='".$dt->instansi."' placeholder='Keterangan  #".$i."..'><br>
																   ";	
													}else{
														$str.= "<input type=text name='dosen[]' class='dosen col-md-5 form-control' value='".$dt->nama."'><input type=hidden name='dosenid[]'><br><br>
																<input type=text name='email[]' class='col-md-5 form-control' value='".$dt->email."' placeholder='Email penulis #".$i."..'><br><br>
																<input type=text name='instansi[]' class='col-md-5 form-control' value='".$dt->instansi."' placeholder='Keterangan  #".$i."..'><br>
														";	
													}
													
													$skip=true;
													break;										
													}
												}
												if(!$skip) if($i==1){
													$str.= "<input type=text name='mhs' id='mhs' class='col-md-5 form-control'><input type=hidden name='mhsid' id='mhsid'><br><br>
															<input type=text name='memail' class='col-md-5 form-control' placeholder='Email penulis #".$i."..'><br><br>
															<input type=text name='minstansi' class='col-md-5 form-control' placeholder='Keterangan  #".$i."..' value='".$kmhs."'><br>
														   ";	
													}else{
														$str.= "<input type=text name='dosen[]' class='dosen col-md-5 form-control'><input type=hidden name='dosenid[]'><br><br>
																<input type=text name='email[]' class='col-md-5 form-control' placeholder='Email penulis #".$i."..'><br><br>
																<input type=text name='instansi[]' class='col-md-5 form-control' placeholder='Keterangan  #".$i."..' value='".$kdos."'><br>
														";	
													}								
											}else{
												if($i==1){
													$str.= "<input type=text name='mhs' id='mhs' class='col-md-5 form-control'><input type=hidden name='mhsid' id='mhsid'><br><br>
															<input type=text name='memail' class='col-md-5 form-control' placeholder='Email penulis #".$i."..'><br><br>
															<input type=text name='minstansi' class='col-md-5 form-control' placeholder='Keterangan  #".$i."..' value='".$kmhs."'><br>
														   ";	
												}else{
													$str.= "<input type=text name='dosen[]' class='dosen col-md-5 form-control'><input type=hidden name='dosenid[]'><br><br>
															<input type=text name='email[]' class='col-md-5 form-control' placeholder='Email penulis #".$i."..'><br><br>
															<input type=text name='instansi[]' class='col-md-5 form-control' placeholder='Keterangan  #".$i."..' value='".$kdos."'><br>
													";	
												}
											}
									
											
											$str.= "</div>";
										$str.= "</div>";
									
										}												
								
									echo $str;
								?>
									</div>
								</div><!--/tab-abstrak-->
								<div class="tab-pane" id="abstrak">	
									<div class="col-md-12">
									<?php
									$str= "<div class='form-group'>"; 
									$str.= "<label>Judul</label>";
									$str.= "<div class=controls>";
											$str.= "<textarea name='judul' class='ckeditor'>".$judul."</textarea><br>";
										$str.= "</div>";
									$str.= "</div>";
						
									$str.= "<div class='form-group'>"; 
										$str.= "<label>Abstrak</label>";
										$str.= "<div class=controls>";
											$str.= "<textarea  class='ckeditor' id='keterangan' name='abstrak'>".$abstrakin."</textarea><br>";								
										$str.= "</div>";
									$str.= "</div>";
													
									$str.= "<div class='form-group'>"; 
										$str.= "<label>Kata Kunci</label>";
										$str.= "<div class=controls>";
											$str.= "<textarea  name='keyword' class='col-md-12' rows=5>".$keywordin."</textarea><br>";								
										$str.= "</div>";
									$str.= "</div>";
									
									$str.= "<div class='form-group'>"; 
										$str.= "<label><b>File Abstrak PDF</b></label>";
										$str.= "<div class=controls>";
											$str.= "<input type=file name=filein id=filein class='col-md-5'><br><br>";
										$str.= "</div>";
									$str.= "</div>";	

									$str.= "<div class='form-group'>"; 
										$str.= "<label><b>Full PDF</b></label>";
										$str.= "<div class=controls>";
											$str.= "<input type=file name=ffilein id=ffilein class='col-md-5'><br><br>";
										$str.= "</div>";
									$str.= "</div>";										
								
									echo $str;
								?>		
									</div>
								</div><!--/tab-abstrak-->
								<div class="tab-pane" id="abtract">	
									 <?php  
										$str= "<div class='form-group'>"; 
										$str.= "<label>Title</label>";
										$str.= "<div class=controls>";
												$str.= "<textarea name='judulen' class='ckeditor'>".$judulen."</textarea><br>";
											$str.= "</div>";
										$str.= "</div>";
									
										$str.= "<div class='form-group'>"; 
										$str.= "<label>Abstract</label>";
										$str.= "<div class=controls>";
											$str.= "<textarea  class='ckeditor' id='keterangan' name='abstraken'>".$abstraken."</textarea><br>";								
										$str.= "</div>";
										$str.= "</div>";
														
										$str.= "<div class='form-group'>"; 
											$str.= "<label>Keyword</label>";
											$str.= "<div class=controls>";
												$str.= "<textarea  name='keyworden' class='col-md-12' rows=5>".$keyworden."</textarea><br>";								
											$str.= "</div>";
										$str.= "</div>";
										
										$str.= "<div class='form-group'>"; 
										$str.= "<label><b>File PDF</b></label>";
										$str.= "<div class=controls>";
											$str.= "<input type=file name=fileen id=fileen class='col-md-5'><br><br>";
										$str.= "</div>";
									$str.= "</div>";	
								
									echo $str;					
									 ?>               
								</div><!--/tab-abstract-->
								
								
							</div>
						</div>
					<?php						
										
											
						$str= "<div class='form-group'>"; 
							$str.= "<label>&nbsp;</label>";
							$str.= "<div class=controls>";
							$str.= "<input type=submit name='b_artikel' id='b_artikel' value='  Save   ' class='btn btn-primary'>";
						$str.= "</div>";
						$str.= "</div>";
					$str.= "</form>";
					
					echo $str;
		echo "</div></div></fieldset></div>";

$this->foot();

?>

