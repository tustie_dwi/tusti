<?php
class jurnal_publish extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 		
	}
		
	function  index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		
		$mpublish = new model_publish();	
		
		$data['posts'] = $mpublish->read($keyword, $page, $perpage,"");	
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		if($by=='ok'){
			$data['status'] = 'OK';
			$data['statusmsg'] = 'OK, data telah diupdate.';
		}
			
		$this->view( 'publish/index.php', $data );
	}
	
	function ok(){
		$this->index('ok');
	}
	
	function write(){
		if(isset($_POST['b_publish'])){
			$this->save();
			exit();
		}
		
		$mpublish = new model_publish();	
		
		$data['publish']	= "";
		$data['periode']	= $mpublish->get_periode_jurnal();
		
		if(isset($_POST['cmbperiode'])){
			$data['periodeid']	= $_POST['cmbperiode'];
			$data['artikel']	= $mpublish->read_artikel($_POST['cmbperiode'],'0');
		}else{
			$data['periodeid']	= "";
			$data['artikel']	= "";
		}
		
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');		
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/jsFunction.js');
							
		$this->view('publish/edit.php', $data);
	}
	
		
	function edit($id){
		if(!$id) {
			$this->redirect('module/jurnal/publish');
			exit;
		}
		
		$mpublish = new model_publish();	
		
		$data['publish']	= $mpublish->read($keyword = NULL, $page = 1, $perpage = 1,$id);
		$data['periode']	= $mpublish->get_periode_jurnal();
		$data['artikel']	= $mpublish->read_artikel($id,1);
					
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');		
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/jsFunction.js');
							
		$this->view('publish/edit.php', $data);
	}
	

	
	function save(){
									
		ob_start();
		
		$mpublish = new model_publish();
		$mconf	  = new model_conf();	
		
		if(isset($_POST['b_publish'])&&($_POST['b_publish']!="")){
			$periode	= $_POST['cmbperiode'];
			$edisi		= $_POST['edisi'];
			$volume		= $_POST['volume'];
			$no			= $_POST['no'];
			//$issn		= $_POST['issn'];
			$hal		= $_POST['hal'];
			$catatan	= $_POST['catatan'];
			$lastupdate	= date("Y-m-d H:i:s");
			$user	 	= $this->coms->authenticatedUser->username;
			
			
			if(isset($_POST['ispub'])&&($_POST['ispub']!="")){
				$ispub	= 1;
			}else{
				$ispub	= 0;
			}
			
			if($_POST['hidId']==''){
				$kode	= $no.str_replace('-', '', $_POST['cmbperiode']);
				$id		= "";
				$action	= "insert";
			}else{
				$kode	= $_POST['hidId'];
				$id		= $_POST['hidId'];
				$action	= "update";
			}	
			
			$strfile	= $_FILES['file']['name'];
			$filein		= $kode.".pdf";
			$strcover	= $_FILES['cover']['name'];
						
			$lastupdate	= date("Y-m-d H:i:s");
			$user	 	= $this->coms->authenticatedUser->username;
			
			
			$datanya = array('jurnal_id'=>"", 'is_publish'=>0, 'user'=>$user, 'last_update'=>$lastupdate);
			$idnya	 = array('jurnal_id'=>$kode);
			
			$mpublish->delete_artikel($datanya, $idnya);
			
			if(isset($_POST['artikel'])&&($_POST['artikel']!="")){
				for($i=0;$i<count($_POST['artikel']);$i++){
					if($_POST['artikel'][$i]!=""){
						for($j=0;$j<count($_POST['choice']);$j++){
							if($_POST['choice'][$j]==$_POST['artikel'][$i]){
								$ischoice = 1;
							}else{
								$ischoice = 0;
							}
						
						}
						
						$datanya = array('jurnal_id'=>$kode,'is_publish'=>$ispub, 'editor_choice'=>$ischoice, 'user'=>$user, 'last_update'=>$lastupdate);
						$idnya	 = array('artikel_id'=>$_POST['artikel'][$i]);
						
						$mpublish->update_artikel($datanya, $idnya);
						$mconf->log("dbptiik_jurnal.tbl_artikel", $datanya, $user, $action);
					}
				}
			}					
			
			
			$datanya = array('jurnal_id'=>$kode, 'periode'=>$periode, 'edisi'=>$edisi, 'volume'=>$volume, 'no'=>$no, 'page'=>$hal, 'is_publish'=>$ispub, 'keterangan'=>$catatan, 'user'=>$user, 'last_update'=>$lastupdate);
			$mpublish->replace_publish($datanya);							
				
			$mconf->log("dbptiik_jurnal.tbl_publish", $datanya, $user, $action);
			
			if($strfile!=""){
				$filename = stripslashes(@$_FILES['file']['name']); 
				$extension = $this->getExtension($filename); 
				$extension = strtolower($extension); 
				
				if (($extension != "pdf")){ 
					$result['error'] = "Unknown extension! Please upload PDF only";
					print "<script> alert('Unknown extension! Please upload PDF only'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
				}
				
				
				$picname=$kode.".".$extension;  
				
				$dir = "assets/uploads/file/".$periode;
				
				if(!file_exists($dir) OR !is_dir($dir)){
					mkdir($dir, 0777);         
				} 
		
				$newname	= "assets/uploads/file/".$periode."/".$picname;  
				$picurl		= $this->location()."assets/uploads/file/".$periode."/".$picname;
				$copied = move_uploaded_file($_FILES['file']['tmp_name'], $newname); 
				
				if (!$copied){  
					print "<script> alert('Copy unsuccessfull!'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
					
				}else{
					$errors=0;
					
					$datanya = array('file'=>$picurl, 'user'=>$user, 'last_update'=>$lastupdate);
					$idnya  = array('jurnal_id'=>$kode);
					$mpublish->update_publish($datanya, $idnya);	
			
					print "<script> alert('Copy successfull!'); </script>"; 						
				}					
			}
			
			if($strcover!=""){
				$filename = stripslashes(@$_FILES['cover']['name']); 
				$extension = $this->getExtension($filename); 
				$extension = strtolower($extension); 
				
				if (($extension != "png") && ($extension != "jpg") && ($extension != "jpeg")){ 
					$result['error'] = "Unknown extension! Please upload image only";
					print "<script> alert('Unknown extension! Please upload image only'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
				}
				
				
				$picname="C".$kode.".".$extension;  
				
				$dir = "assets/uploads/file/".$periode;
				
				if(!file_exists($dir) OR !is_dir($dir)){
					mkdir($dir, 0777);         
				} 
		
				$newname	= "assets/uploads/file/".$periode."/".$picname;  
				$coverurl	= "https://175.45.187.243/assets/uploads/file/".$periode."/".$picname;
				
				$copied = move_uploaded_file($_FILES['cover']['tmp_name'], $newname); 
				
				if (!$copied){  
					print "<script> alert('Copy unsuccessfull!'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
					
				}else{
					$errors=0;
					$datanya = array('cover'=>$coverurl, 'user'=>$user, 'last_update'=>$lastupdate);
					$idnya  = array('jurnal_id'=>$kode);
					$mpublish->update_publish($datanya, $idnya);	
					//print "<script> alert('Copy successfull!'); </script>"; 						
				}					
			}
			

			
		}
		
		unset($_POST['b_publish']);
		
		$this->redirect('module/jurnal/publish/ok');

		exit();
	
	}
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		return $ext; 
	}  

	function delete($id){
		
		$mpublish = new model_publish();	
		
		$result   = array();
		
		$datanya  = array('publish_id'=>$id);	
		$periode  = $mpublish->get_periode_by_id($id);
		$dir 	  = "assets/uploads/file/".$periode;
		
		if($mpublish->delete_publish($datanya)){ 
			unlink($dir."/".$id.".pdf");
			$result['status'] = "OK";
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mpublish->error;
		}
		echo json_encode($result);		

	}
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 200) {

			// truncate string
			$stringCut = substr($string, 0, 200);

			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}

}
?>