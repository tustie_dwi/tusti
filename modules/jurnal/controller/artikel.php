<?php
class jurnal_artikel extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 		
	}
		
	function  index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		
		$martikel = new model_artikel();	
		
		$data['posts'] = $martikel->read($keyword, $page, $perpage,"");	
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->add_script('js/artikel.js');
		
		if($by=='ok'){
			$data['status'] = 'OK';
			$data['statusmsg'] = 'OK, data telah diupdate.';
		}
			
		$this->view( 'artikel/index.php', $data );
	}
	
	function ok(){
		$this->index('ok');
	}
	
	function write(){
		$martikel = new model_artikel();	
		
		$data['artikel']	= "";
		$data['periode']	= $martikel->get_periode(date("m"));
		//$data['regnumber']	= $martikel->get_reg_number($data['periode']);
		$data['noperiode']	= $martikel->get_no_periode(date("m"));
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/jsFunction.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
					
		$this->view('artikel/edit.php', $data);
	}
	
		
	function edit($id){
		if( !$id ) {
			$this->redirect('module/jurnal/artikel');
			exit;
		}
		
		$martikel = new model_artikel();	
		
		$data['artikel']= $martikel->read($keyword = NULL, $page = 1, $perpage = 1,$id);
		$data['author']	= $martikel->get_author($id);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/jsFunction.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
				
		$this->view('artikel/edit.php', $data);
	}
	
	function profile($id){
		if( !$id ) {
			$this->redirect('module/jurnal/artikel');
			exit;
		}
		
		$martikel = new model_artikel();	
		
		$data['artikel'] 		= $martikel->read($keyword = NULL, $page = 1, $perpage = 1, $id);	
						
		$this->view('artikel/view.php', $data);
	}
	
	function save(){
		$this->add_style('css/DT_bootstrap.css');
								
		ob_start();
		
		$martikel = new model_artikel();
		$mconf	  = new model_conf();	
		
		if(isset($_POST['b_artikel'])&&($_POST['b_artikel']!="")){
			$judul		= $_POST['judul'];
			$periode	= $_POST['periode'];
			
			if(isset($_POST['regnumber'])&&($_POST['regnumber']!="")){
				$regnumber	= $_POST['regnumber'];
			}else{
				$regnumber	= $martikel->get_reg_number($periode);
			}
			
			$abstrak	= $_POST['abstrak'];
			$keyword	= $_POST['keyword'];
			$abstraken	= $_POST['abstraken'];
			$keyworden	= $_POST['keyworden'];
			$hal		= $_POST['hal'];
			
			if($_POST['hidId']==''){
				$kode	= $regnumber.str_replace('-', '', $_POST['periode']);
				$id		= "";
				$action	= "insert";
			}else{
				$kode	= $_POST['hidId'];
				$id		= $_POST['hidId'];
				$action	= "update";
			}
			
						
			$strffile		= $_FILES['ffilein']['name'];
			$ffilein		= $kode."_FIN.pdf";
			$fpicurl		= "upload/file/PTIIK/doro/".$periode."/IN/".$ffilein;
			
			
			$strfile	= $_FILES['filein']['name'];
			$filein		= $kode."_IN.pdf";
			$picurl		= "upload/file/PTIIK/doro/".$periode."/IN/".$filein;
			
			
			$strfileen	= $_FILES['fileen']['name'];
			$fileen		= $kode."_EN.pdf";
			$picurlen	= "upload/file/PTIIK/doro/".$periode."/EN/".$fileen;
			
			$lastupdate	= date("Y-m-d H:i:s");
			$user	 	= $this->coms->authenticatedUser->username;
			
						
			if($strfile){
				$filename = stripslashes(@$_FILES['filein']['name']); 
				$extension = $this->getExtension($filename); 
				$extension = strtolower($extension); 
				
				if (($extension != "pdf")){ 
					$result['error'] = "Unknown extension! Please upload PDF only";
					print "<script> alert('Unknown extension! Please upload PDF only'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
				}
				
				
				$picname=$kode."_IN.".$extension;  
				
				
				$upload_dir = 'assets/upload/file/PTIIK/doro/'.$periode.'/IN/';		
				$upload_dir_db = 'upload/file/PTIIK/doro/'.$periode.'/IN/';	
				//$dir = "assets/uploads/file/".$periode."/IN";
				
				/*if(!file_exists($upload_dir) OR !is_dir($upload_dir)){
					mkdir($upload_dir, 0777, true);         
				} */
		
					
				$file_size = $_FILES['filein']['size'];
				
				$newname=$upload_dir.$picname;  
				
				$this->upload_file($_FILES['filein']['tmp_name'], $strfile	, $picname, $file_size , $upload_dir);
					
					
				
				/*$copied = move_uploaded_file($_FILES['filein']['tmp_name'], $newname); 
				
				if (!$copied){  
				
					print "<script> alert('Copy unsuccessfull!'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
					
				}else{
					$errors=0;
					print "<script> alert('Copy successfull!'); </script>"; 						
				}	*/				
			}
			
			if($strffile){
				$filename = stripslashes(@$_FILES['ffilein']['name']); 
				$extension = $this->getExtension($filename); 
				$extension = strtolower($extension); 
				
				if (($extension != "pdf")){ 
					$result['error'] = "Unknown extension! Please upload PDF only";
					print "<script> alert('Unknown extension! Please upload PDF only'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
				}
				
				
				$picname=$kode."_FIN.".$extension;  
				
				$dir = "assets/uploads/file/".$periode."/IN";
				
				/*if(!file_exists($dir) OR !is_dir($dir)){
					mkdir($dir, 0777, true);         
				} 
		
				$newname="assets/uploads/file/".$periode."/IN/".$picname;  */
				
				$upload_dir = 'assets/upload/file/PTIIK/doro/'.$periode.'/IN/';	
				
				$file_size = $_FILES['ffilein']['size'];
				
				$newname=$upload_dir.$picname;  
				
				$this->upload_file($_FILES['ffilein']['tmp_name'], $strffile, $picname, $file_size , $upload_dir);
				
				/*$copied = move_uploaded_file($_FILES['ffilein']['tmp_name'], $newname); 
				
				if (!$copied){  				
					print "<script> alert('Copy unsuccessfull!'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
					
				}else{
					$errors=0;
					print "<script> alert('Copy successfull!'); </script>"; 						
				}		*/			
			}
			
			if($strfileen){
				$filename = stripslashes(@$_FILES['fileen']['name']); 
				$extension = $this->getExtension($filename); 
				$extension = strtolower($extension); 
				
				if (($extension != "pdf")){ 
					$result['error'] = "Unknown extension! Please upload PDF only";
					print "<script> alert('Unknown extension! Please upload PDF only'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
				}
				
				
				$picname=$kode."_EN.".$extension;  
				
				$dir = "assets/uploads/file/".$periode."/EN";
				
				$upload_dir = 'assets/upload/file/PTIIK/doro/'.$periode.'/EN/';	
				
				$file_size = $_FILES['fileen']['size'];
				
				$newname=$upload_dir.$picname;  
				
				$this->upload_file($_FILES['fileen']['tmp_name'], $strfileen, $picname, $file_size , $upload_dir);
				
				/*if(!file_exists($dir) OR !is_dir($dir)){
					mkdir($dir, 0777);         
				} 
		
				$newname="assets/uploads/file/".$periode."/EN/".$picname;  
				
				$copied = move_uploaded_file($_FILES['fileen']['tmp_name'], $newname); 
				
				if (!$copied){  
					print "<script> alert('Copy unsuccessfull!'); </script>"; 
					$errors=1; 
					
					$this->index();
					exit();
					
				}else{
					$errors=0;
					//print "<script> alert('Copy successfull!'); </script>"; 						
				}		*/			
			}
			
			
		
			$idnya	= array('artikel_id'=>$kode);
			$martikel->delete_author($idnya);
			
			if($_POST['mhs']!=""){
				$ke = 1;
				
				$idauth  = $kode.$ke;
				if($_POST['mhsid']!=""){
					$idmhs   = $_POST['mhsid'];						
					$namamhs = $mconf->get_nama_mahasiswa($idmhs);
				}else{
					$idmhs	= "-";
					$namamhs= $_POST['mhs'];
				}
				
				if($_POST['minstansi']==""){
					$instansi = "-";
				}else{
					$instansi = $_POST['minstansi'];
				}
				
				if($_POST['memail']==""){
					$email = "-";
				}else{
					$email = $_POST['memail'];
				}
				
				$datanya = array('author_id'=>$idauth, 'artikel_id'=>$kode, 'nama'=>$namamhs, 'instansi'=>$instansi, 'email'=>$email, 'author_ke'=>1, 'mahasiswa_id'=>$idmhs, 'karyawan_id'=>'-','user'=>$user, 'last_update'=>$lastupdate);
				
				$martikel->replace_author($datanya);
				
				$mconf->log("dbptiik_jurnal.tbl_author", $datanya, $user, $action);
			}
			
			if($_POST['dosen']!=""){
				for($i=0;$i<count($_POST['dosen']);$i++){
					$ke = $i+2;
					if($_POST['dosen'][$i]!=""){
						$idauth  = $kode.$ke;
						
						$iddosen = $mconf->get_id_dosen($_POST['dosen'][$i]);
						
						if($_POST['instansi'][$i]==""){
							$instansi = "-";
						}else{
							$instansi = $_POST['instansi'][$i];
						}
						
						if($_POST['email'][$i]==""){
							$email = "-";
						}else{
							$email = $_POST['email'][$i];
						}
						
						$datanya = array('author_id'=>$idauth, 'artikel_id'=>$kode,'nama'=>$_POST['dosen'][$i], 'instansi'=>$instansi, 'author_ke'=>$ke, 'email'=>$email, 'mahasiswa_id'=>'-','karyawan_id'=>$iddosen, 'user'=>$user, 'last_update'=>$lastupdate);
						
						$martikel->replace_author($datanya);
						
						$mconf->log("dbptiik_jurnal.tbl_author", $datanya, $user, $action);
					}
				}
			}
				
				if($_POST['hidId']==''){
					$datanya=array('artikel_id'=>$kode, 'judul_in'=>$judul, 'judul_en'=>$_POST['judulen'], 'periode'=>$periode, 'reg_number'=>$regnumber, 'abstrak_in'=>$abstrak, 'keyword_in'=>$keyword, 'abstrak_en'=>$abstraken, 'keyword_en'=>$keyworden, 'page'=>$hal, 
									'full_file_in'=>$ffilein, 'url_full_file_in'=>$fpicurl, 'file_in'=>$filein, 'url_in'=>$picurl, 'file_en'=>$fileen, 'url_en'=>$picurlen, 'user'=>$user, 'last_update'=>$lastupdate);
					$martikel->replace_artikel($datanya);	
				}else{
					$datanya=array('judul_in'=>$judul, 'judul_en'=>$_POST['judulen'], 'periode'=>$periode, 'reg_number'=>$regnumber, 'abstrak_in'=>$abstrak, 'keyword_in'=>$keyword, 'abstrak_en'=>$abstraken, 'keyword_en'=>$keyworden, 'page'=>$hal, 
									'full_file_in'=>$ffilein, 'url_full_file_in'=>$fpicurl, 'file_in'=>$filein, 'url_in'=>$picurl, 'file_en'=>$fileen, 'url_en'=>$picurlen, 'user'=>$user, 'last_update'=>$lastupdate);
					$idnya	= array('artikel_id'=>$kode);
					$martikel->update_artikel($datanya, $idnya);	
				}					
					
				$mconf->log("dbptiik_jurnal.tbl_artikel", $datanya, $user, $action);
			}
		
		unset($_POST['b_artikel']);
		
		$this->redirect('module/jurnal/artikel/ok');
		exit();
	
	}
	
	function upload_file($file=NULL, $filename = NULL, $newname = NULL, $file_size = NULL, $upload_dir=NULL){
		//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $file;
					$filename    = $filename;
					$filenamenew = $picname;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		            $ch = curl_init();
		            $options = array(
						 CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
					
		            ); // cURL options 
					curl_setopt_array($ch, $options);
			        curl_exec($ch);
			        if(!curl_errno($ch))
			        {
			            $info = curl_getinfo($ch);
			            if ($info['http_code'] == 200)
			               $errmsg = "File uploaded successfully";
			        }
			        else
			        {
			            $errmsg = curl_error($ch);
			        }
			        curl_close($ch);
					//------UPLOAD USING CURL----------------
	}
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		return $ext; 
	}  

	function delete($id){
		
		$martikel = new model_artikel();	
		
		$result   = array();
		
		$datanya  = array('artikel_id'=>$id);	
		$periode  = $martikel->get_periode_by_id($id);
		$dir 	  = "assets/uploads/file/".$periode;
		
		if($martikel->delete_artikel($datanya)){ 
			unlink($dir."/".$id.".pdf");
			$result['status'] = "OK";
		}else {
			$result['status'] = "NOK";
			$result['error'] = $martikel->error;
		}
		echo json_encode($result);		

	}
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 200) {

			// truncate string
			$stringCut = substr($string, 0, 200);

			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
		}
		return $string;
	}

}
?>