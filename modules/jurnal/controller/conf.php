<?php
class jurnal_conf extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}
		
	function  index(){
		
		$mconf = new model_conf();		
		
		$data['posts'] = $mconf->read();			
			
		$this->view( 'conf/index.php', $data );
	}
	
	
	function save(){
		$mconf = new model_conf();
		
		ob_start();
				
		$tahun		= $_POST['tahun'];
		$semester	= $_POST['semester'];
		$pendek		= $_POST['pendek'];
		$skripsi	= $_POST['skripsi'];
		$lastupdate	= date("Y-m-d H:i:s");
		$user	 	= $this->coms->authenticatedUser->username;
		
		if($semester=='ganjil'){
			$kodesemester = "01";
		}else{
			$kodesemester = "02";
		}
		
		if($pendek!=""){
			$kode		= $tahun.$kodesemester.'01';
		}else{
			$kode		= $tahun.$kodesemester.'00';
		}
							
		$datanya		= array('tahun'=>$tahun, 'is_ganjil'=>$semester, 'is_pendek'=>$pendek, 'kode_skripsi'=>$skripsi, 'is_aktif'=>1, 'user'=>$user, 'last_update'=>$lastupdate);
		$dataakademik	= array('tahun_akademik'=>$kode, 'tahun'=>$tahun, 'is_ganjil'=>$semester, 'is_pendek'=>$pendek, 'is_aktif'=>1);
		$idnya			= array('is_aktif'=>1);
		$dataupdate		= array('is_aktif'=>0);
		
		$mconf->update_conf($dataupdate,$idnya);
		$mconf->update_semester($dataupdate,$idnya);
		$mconf->replace_conf($datanya);
		$mconf->replace_semester($dataakademik);
		
		$mconf->log("dbptiik_master.tbl_config", $datanya, $user, 'replace');
		
		$this->index();	
		
		exit();
	}
	
	function mhs(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_mahasiswa($str);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function dosen(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_dosen($str);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function pengampu(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_pengampu($str);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
					
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function staff(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_staff($str);
		
		$tags		= array();
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$tags['tags'] = $return_arr; 
					
		$json_response = json_encode($tags);

		echo $json_response;
	}
	
	function pesertamhs(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_mhs($str);
		
		$tags		= array();
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$tags['tags'] = $return_arr; 
					
		$json_response = json_encode($tags);

		echo $json_response;
	}
	
	
	function namamk(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_namamk($str);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function mk(){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_mk($str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function hari(){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_hari($str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = ucfirst($value);				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function ruang(){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_ruang($str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function kelas(){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_kelas($str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function prodi(){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_prodi($str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	
	
	function jammulai(){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_jam_mulai($str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function jamselesai(){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_jam_selesai($str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function mkditawarkan($id){	
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 		= new model_conf();
		$semester	= $mconf->get_semester_aktif();
		
		$semesterid	= $semester->tahun_akademik;

		$data		= $mconf->get_mkditawarkan($id,$semesterid,$str);
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		echo json_encode($return_arr);
	}
	
	function jadwaldosen(){	
				
		$mconf 	= new model_conf();
		$data	= $mconf->get_jadwal_dosen();
		
		$return_arr = array();
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		echo json_encode($return_arr);
	}

}
?>