$(function() {

	$('.undangan .btn').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $collapse = $this.closest('.collapse-group').find('.collapse');
		$collapse.collapse('toggle');
	});
	
	$(".tagPemateri").tagsManager({
		preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/jurnal/conf/staff",
		AjaxPush: base_url + "/module/jurnal/conf/staff/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidPemateri'
      });
	  
	  $(".tagUndangan").tagsManager({
		preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/jurnal/conf/staff",
		AjaxPush: base_url + "/module/jurnal/conf/staff/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidUndangan'
      });
	  
	  $(".tagStaff").tagsManager({		
		prefilled:"",
        preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/jurnal/conf/staff",
		AjaxPush: base_url + "/module/jurnal/conf/staff/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidStaff'
      });
	  
	   $(".tagMhs").tagsManager({		
		prefilled:"",
        preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/jurnal/conf/pesertamhs",
		AjaxPush: base_url + "/module/jurnal/conf/pesertamhs/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidMhs'
      });
	  
	   $(".tagPeserta").tagsManager({		
		prefilled:"",
        preventSubmitOnEnter: true,
         blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidPeserta'
      });
	 $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	
  });
	
	
