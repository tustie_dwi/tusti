<?php
class model_laboratorium extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	/* master semester  */
	function get_semester(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					ORDER BY 
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` DESC
					";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester aktif */
	function get_semester_aktif(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`,
					CONCAT(`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,' ',`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`) as semester
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					WHERE
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` =  '1'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	/* master kalender */
	function get_kalender_akademik($takademik=NULL,$jeniskegiatan=NULL){
		$sql = "SELECT
				 `tbl_kalenderakademik`.`kalender_id`,
				 `tbl_kalenderakademik`.`jenis_kegiatan_id`,
				 `tbl_kalenderakademik`.`tahun_akademik`,
				 `tbl_kalenderakademik`.`tgl_mulai`,
				 `tbl_kalenderakademik`.`tgl_selesai`,
				 `tbl_kalenderakademik`.`is_aktif`
				FROM
				 `db_ptiik_apps`.`tbl_kalenderakademik`
				WHERE 1 =  1
			   ";
		
		if($takademik){
			$sql = $sql . " AND `tbl_kalenderakademik`.`tahun_akademik` = '".$takademik."' ";
		}
		
		if($jeniskegiatan){
			$sql = $sql . " AND `tbl_kalenderakademik`.`jenis_kegiatan_id` = '".$jeniskegiatan."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master unit kerja atau struktur*/
	function get_unit($term=NULL, $jenis=NULL){
		$sql= "SELECT unit_id as `id`, mid(md5(unit_id),9,7) as unitid,
			         nama as `value` 
			   FROM `db_ptiik_apps`.`tbl_unitkerja` WHERE jenis = 'laboratorium'";
		
		if($term){
			$sql = $sql . "AND nama like '%".$term."%'  ";
		}
		
		if($jenis){
			$sql = $sql . "AND jenis = '".$jenis."'  ";
		}
		
		if($term || $jenis){
			$sql = $sql . "ORDER BY `db_ptiik_apps`.`tbl_unitkerja`.`jenis` ASC, `db_ptiik_apps`.`tbl_unitkerja`.`nama` ASC";
		}
		else{
			$sql = $sql . "ORDER BY `db_ptiik_apps`.`tbl_unitkerja`.`nama` ASC";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_unit_by_fakultas($fak=NULL){
		$sql= "SELECT unit_id as `id`, mid(md5(unit_id),9,7) as unitid,
			         keterangan as `value` 
			   FROM `db_ptiik_apps`.`tbl_unit_kerja` WHERE kategori = 'laboratorium'";
		if($fak){
			$sql = $sql . "AND tbl_unit_kerja.fakultas_id = '".$fak."'  ";
		}
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	/*master mahasiswa */
	function get_mahasiswa($term){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `value` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/* fungsi distinct mk praktikum*/
	function get_mk_praktikum($takademik=NULL,$unit=NULL){
		$sql = "SELECT 
				 DISTINCT(`tbl_praktikum`.`mkditawarkan_id`) as `mkid`,
				 `tbl_praktikum`.`nama_mk`,
				 `tbl_praktikum`.`kode_mk`
				FROM 
				 `db_ptiik_apps`.`tbl_praktikum` 
				LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_praktikum.mkditawarkan_id
				LEFT JOIN db_ptiik_apps.tbl_praktikum_lab ON tbl_praktikum_lab.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				 WHERE 1
			   ";
		
		if($takademik){
			$sql = $sql . " AND `tbl_praktikum`.`tahun_akademik` = '".$takademik."' ";
		}
		
		if($unit){
			$sql = $sql . " AND `tbl_praktikum_lab`.`unit_id` = '".$unit."' ";
		}
		
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	function read_asisten($id=NULL){
		$sql = "SELECT 
				mid(md5(`tbl_praktikum_asisten`.`asisten_id`),9,7) as `asisten_id`,
				`tbl_praktikum_asisten`.`asisten_id` as `hidId`,
				`tbl_praktikum_asisten`.`mahasiswa_id`,
				`tbl_praktikum_asisten`.`unit_id`,
				`tbl_praktikum_asisten`.`tahun_akademik`,
				`tbl_praktikum_asisten`.`mkditawarkan_id`,
				`tbl_praktikum_asisten`.`tgl_mulai`,
				`tbl_praktikum_asisten`.`tgl_selesai`,
				`tbl_praktikum_asisten`.`is_aktif`,
				`tbl_praktikum_asisten`.`keterangan`,
				`tbl_unit_kerja`.`keterangan` as `namalab`,
				`tbl_mahasiswa`.`nama` as `namamhs`,
				`tbl_mahasiswa`.`nim`,
				`tbl_tahunakademik`.`tahun`,
				`tbl_tahunakademik`.`is_ganjil`,
				`tbl_namamk`.`keterangan` as `namamk`
				FROM `db_ptiik_apps`.`tbl_praktikum_asisten` 
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa` ON `tbl_mahasiswa`.`mahasiswa_id` = `tbl_praktikum_asisten`.`mahasiswa_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_unit_kerja` ON `tbl_unit_kerja`.`unit_id` = `tbl_praktikum_asisten`.`unit_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` ON `tbl_tahunakademik`.`tahun_akademik` = `tbl_praktikum_asisten`.`tahun_akademik`
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` ON `tbl_mkditawarkan`.`mkditawarkan_id` = `tbl_praktikum_asisten`.`mkditawarkan_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` ON `tbl_matakuliah`.`matakuliah_id` = `tbl_mkditawarkan`.`matakuliah_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
				WHERE 1
			   ";
		if($id){
			$sql = $sql . "AND mid(md5(`tbl_praktikum_asisten`.`asisten_id`),9,7) = '".$id."' ";
		}

		$result = $this->db->query( $sql );

		return $result;
	}	
	
	function get_asistenId(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(asisten_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_praktikum_asisten"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_asisten($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum_asisten',$datanya);
	}
	
	//SETTING MK
	function read_setting_mk($unit=NULL,$mk=NULL){
		$sql	= "SELECT tbl_praktikum_lab.unit_id,
					   mid(md5(tbl_praktikum_lab.unit_id),9,7) as unitid, 
					   tbl_praktikum_lab.matakuliah_id,
					   tbl_matakuliah.kode_mk, 
					   tbl_matakuliah.matakuliah_id,
					   mid(md5(`tbl_matakuliah`.`matakuliah_id`),9,7) as mkid, 
					   tbl_namamk.namamk_id, 
					   tbl_namamk.keterangan as namamk
				   FROM db_ptiik_apps.tbl_praktikum_lab
				   LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_praktikum_lab .matakuliah_id
				   LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk .namamk_id
				   WHERE 1";
		if($unit){
	    	$sql .= " AND mid(md5(tbl_praktikum_lab.unit_id),9,7) ='".$unit."'";
	    }
	    if($mk){
	    	$sql .= " AND mid(md5(`tbl_matakuliah`.`matakuliah_id`),9,7) ='".$mk."'";
	    }
		
		if($unit && $mk){
			$result = $this->db->getRow( $sql );
		}
		else{
			$result = $this->db->query( $sql );
		}
		 // echo $sql;
		return $result;
	}
	
	function get_matakuliah(){
		$sql	= "SELECT tbl_matakuliah.kode_mk, 
					   tbl_matakuliah.matakuliah_id,
					   mid(md5(`tbl_matakuliah`.`matakuliah_id`),9,7) as mkid, 
					   tbl_namamk.namamk_id, 
					   tbl_namamk.keterangan as namamk
				   FROM db_ptiik_apps.tbl_matakuliah
				   INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk .namamk_id
				   WHERE 1";
		$sql	.= " ORDER BY namamk";
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;
	}
	
	function delete_settingmk($unit=NULL,$mk=NULL){
		$sql	= "DELETE FROM db_ptiik_apps.tbl_praktikum_lab
				   WHERE mid(md5(tbl_praktikum_lab.unit_id),9,7) = '".$unit."' AND mid(md5(tbl_praktikum_lab.matakuliah_id),9,7) = '".$mk."'";
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;
	}
	
	function cek_data($mkid=NULL, $unit=NULL){
		$sql	= "SELECT tbl_praktikum_lab.matakuliah_id
				   FROM db_ptiik_apps.tbl_praktikum_lab
				   WHERE tbl_praktikum_lab.matakuliah_id = '".$mkid."'
				   AND tbl_praktikum_lab.unit_id = '".strtoupper($unit)."'
				  ";
		$result = $this->db->getRow( $sql );
		//echo $sql;
		return $result;
	}
	
	function replace_praktikum_lab($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum_lab',$datanya);
	}	
}

?>