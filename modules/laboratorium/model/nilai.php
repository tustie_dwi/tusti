<?php
class model_nilai extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	/*Penilaian*/
	function get_nilaiId(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(nilai_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_praktikum_nilai WHERE LEFT (nilai_id, 6) = '".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_nilaiId_md5($nilai=NULL,$param=NULL){
		$sql = "SELECT mid(md5(tbl_praktikum_nilai.nilai_id),9,7) as nilaiid,
				tbl_praktikum_nilai.nilai_id,
				tbl_praktikum_nilai.judul,
				tbl_praktikum_nilai.materi_id,
				mid(md5(tbl_praktikum_nilai.materi_id),9,7) as materiid,
				tbl_praktikum_nilai.jadwal_id,
				mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) as jadwalid,
				tbl_praktikum_nilai.mkditawarkan_id
				FROM db_ptiik_apps.tbl_praktikum_nilai
				WHERE 1";
		
		if($param=='byid'){
			$sql .= " AND tbl_praktikum_nilai.nilai_id = '".$nilai."' ";
		}
		
		if($param=='byidmd5'){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.nilai_id),9,7) = '".$nilai."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		// $strresult = $dt->nilaiid;
		return $dt;
	}
	
	function get_penilaian($jadwal=NULL, $materi=NULL, $mk=NULL, $nilai=NULL){
		$sql = "SELECT DISTINCT tbl_praktikum.jadwal_id,
					   mid(md5(tbl_praktikum.jadwal_id),9,7) as jdwl_id,
					   tbl_praktikum.kode_mk, 
					   tbl_praktikum.mkditawarkan_id,
					   mid(md5(tbl_praktikum.mkditawarkan_id),9,7) as mk_id,
					   tbl_praktikum.nama_mk,
					   tbl_praktikum.kelas_id,
					   tbl_praktikum.prodi_id,
					   tbl_praktikum_materi.judul as judul_materi,
					   tbl_praktikum.tahun_akademik,
					   mid(md5(tbl_praktikum_nilai.nilai_id),9,7) as nilaiid,
					   mid(md5(tbl_praktikum_nilai.materi_id),9,7) as materiid,
					   tbl_praktikum_nilai.*,
					   pt11k_user.name
				FROM db_ptiik_apps.tbl_praktikum_nilai
				LEFT JOIN coms.pt11k_user ON pt11k_user.id = tbl_praktikum_nilai.user_id
				LEFT JOIN db_ptiik_apps.tbl_praktikum ON tbl_praktikum_nilai.jadwal_id = tbl_praktikum.jadwal_id 
				AND tbl_praktikum_nilai.mkditawarkan_id = tbl_praktikum.mkditawarkan_id				
				LEFT JOIN db_ptiik_apps.tbl_praktikum_materi ON tbl_praktikum_materi.materi_id = tbl_praktikum_nilai.materi_id
				WHERE 1";
		if($jadwal!=""){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwal."'";
		}
		if($materi!=""&&$materi!="proses"&&$materi!="list"&&$materi!="get"){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.materi_id),9,7) = '".$materi."'";
		}elseif($materi==""){
			$sql .= " AND tbl_praktikum_nilai.materi_id = '-'";
		}elseif($materi=="proses"){
			$sql .= " ORDER BY tbl_praktikum.mkditawarkan_id,tbl_praktikum_nilai.materi_id ASC ";
		}elseif($materi=="get"){
			
		}
		
		if($mk!=""){
			$sql .= " AND mid(md5(tbl_praktikum.mkditawarkan_id),9,7) = '".$mk."'";
		}
		if($nilai!=""){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.nilai_id),9,7) = '".$nilai."'";
			// echo $sql;
			$result = $this->db->getRow( $sql );
		}
		else{
			$result = $this->db->query( $sql );
		}
		
		// echo $sql;
		return $result;
	}
	
	function replace_penilaian($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum_nilai',$datanya);
	}
	
	function read_nilai_inf($jadwal,$param){
		$sql = "SELECT mid(md5(tbl_praktikum_nilai.nilai_id),9,7) as nilai_id,
				tbl_praktikum_nilai.nilai_id as nilaiid
				FROM db_ptiik_apps.tbl_praktikum_nilai
				WHERE 1";
				
		if($jadwal!=""){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwal."'";
		}
		
		if($param=="akhir"){
			$sql .= " AND tbl_praktikum_nilai.kategori = 'akhir' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function update_status($nilaiid=NULL, $approve=NULL, $approve_by=NULL, $tgl=NULL, $proses=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_praktikum_nilai";
		if($approve!=""){
			$sql .= " SET tbl_praktikum_nilai.is_approve = '".$approve."',
					  tbl_praktikum_nilai.approve_by = '".$approve_by."', 
					  tbl_praktikum_nilai.tgl_approve = '".$tgl."',
					  tbl_praktikum_nilai.last_update = '".$tgl."'";
		}
		elseif($proses!=""){
			$sql .= " SET tbl_praktikum_nilai.is_proses = '".$proses."',
					  tbl_praktikum_nilai.last_update = '".$tgl."'";
		}
		$sql .= " WHERE mid(md5(tbl_praktikum_nilai.nilai_id),9,7) = '".$nilaiid."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_nilai($nilai=NULL, $jdwl=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_praktikum_nilai
			 	WHERE mid(md5(`tbl_praktikum_nilai`.`nilai_id`),9,7) = '".$nilai."'
			   ";
		
		if($jdwl){
			$sql .= " AND  mid(md5(`tbl_praktikum_nilai`.`jadwal_id`),9,7) = '".$jdwl."' ";
		}
		
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;
	}
	
	function delete_praktikum_nilai_mhs($komponen=NULL,$nilaiid=NULL,$param=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_praktikum_nilai_mhs 
				WHERE mid(md5(`tbl_praktikum_nilai_mhs`.`komponen_id`),9,7) = '".$komponen."'
				AND mid(md5(`tbl_praktikum_nilai_mhs`.`nilai_id`),9,7) = '".$nilaiid."'
				AND tbl_praktikum_nilai_mhs.inf_kategori = '".$param."'
			   ";
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;
	}
	
	function get_jadwal_mk($id=NULL,$thn=NULL,$param=NULL,$param_join=NULL, $level=NULL){
		$sql = "SELECT DISTINCT `tbl_praktikum`.`jadwal_id`,
				mid(md5(`tbl_praktikum`.`jadwal_id`),9,7) as `jdwl_id`,
				`tbl_praktikum`.`mkditawarkan_id`,
				mid(md5(`tbl_praktikum`.`mkditawarkan_id`),9,7) as `mkid`,
				`tbl_praktikum_komponen_nilai`.`materi_id`
				FROM `db_ptiik_apps`.`tbl_praktikum` ";
		
		$sql .= " ".$param_join." JOIN `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` 
				ON `tbl_praktikum_komponen_nilai`.`jadwal_id` = `tbl_praktikum`.`jadwal_id`
				WHERE 1
				";
		
		if($thn){
			$sql .= " AND `tbl_praktikum`.`tahun_akademik` = '".$thn."' ";
		}
		
		if($id){
			if($param=='jadwal'){
				$sql .= " AND mid(md5(`tbl_praktikum`.`jadwal_id`),9,7) = '".$id."' ";
			}
			if($param=='mkid'){
				$sql .= " AND `tbl_praktikum`.`mkditawarkan_id` = '".$id."' ";
			}
			// echo $sql;
			$dt = $this->db->getRow( $sql );
			return $dt;
		}else return null;
		
	}
	
	function read_komponen_idx($jadwalid){
		$sql = "SELECT 
				tbl_praktikum_komponen_nilai.komponen_id,
				tbl_praktikum_komponen_nilai.nilai_id,
				tbl_praktikum_komponen_nilai.materi_id,
				tbl_praktikum_komponen_nilai.jadwal_id,
				tbl_praktikum_komponen_nilai.kategori,
				mid(md5(tbl_praktikum_komponen_nilai.materi_id),9,7) as materiid,
				mid(md5(tbl_praktikum_komponen_nilai.jadwal_id),9,7) as jadwalid
				
				FROM db_ptiik_apps.`tbl_praktikum_komponen_nilai` 
				WHERE 1
				AND mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) = '".$jadwalid."'
				GROUP BY tbl_praktikum_komponen_nilai.materi_id, tbl_praktikum_komponen_nilai.kategori, tbl_praktikum_komponen_nilai.nilai_id
				ORDER BY tbl_praktikum_komponen_nilai.komponen_id ASC
				";
				// echo $sql;
		$dt = $this->db->query( $sql );
		return $dt;
	}
	
	function get_namamk($jadwalid=NULL,$prodiid=NULL){
		$sql = "SELECT DISTINCT 
				tbl_praktikum.`nama_mk`, 
				tbl_praktikum.`kelas_id`, 
				tbl_praktikum.`mkditawarkan_id`,
				mid(md5(`tbl_praktikum`.`mkditawarkan_id`),9,7) as mkid,
				tbl_praktikum.`jadwal_id`
				FROM db_ptiik_apps.`tbl_praktikum` 
				WHERE 1
				AND mid(md5(`tbl_praktikum`.`jadwal_id`),9,7) = '".$jadwalid."'
				AND tbl_praktikum.`prodi_id` = '".$prodiid."'
				";
		// echo $sql;
		$dt = $this->db->getRow( $sql );
		return $dt;
	}
	
	function get_nilaiid_byjadwal($jadwalid=NULL){
		$sql = "SELECT `tbl_praktikum_nilai`.`nilai_id`,
				mid(md5(`tbl_praktikum_nilai`.`nilai_id`),9,7) as `nilaiid`
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai` 
				WHERE 1 AND tbl_praktikum_nilai.kategori != 'akhir'
			   ";
		
		if($jadwalid){
			$sql .= " AND mid(md5(`tbl_praktikum_nilai`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
				
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	 
	}
	
	function get_infocol($jadwalid){
		$sql ="SELECT COUNT(tbl_praktikum_komponen_nilai.komponen_id) as info
			  FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` 
			  WHERE 1
			  AND `tbl_praktikum_komponen_nilai`.`parent_id` != '0'
			  AND mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) = '".$jadwalid."'
		     ";
		$dt = $this->db->getRow( $sql );
		return $dt->info;
	}
	
	/*Komponen*/
	function read_komponen($id=NULL,$thn=NULL,$komponen=NULL,$materiid=NULL,$param=NULL,$jadwalid=NULL,$materi_nilai=NULL,$param2=NULL,$kategori=NULL,$id2=NULL,$nilai_id=NULL,$nilai_param=NULL){
		$sql = "SELECT DISTINCT 
				mid(md5(`tbl_praktikum_komponen_nilai`.`komponen_id`),9,7) as `kpn_id`,
				`tbl_praktikum_komponen_nilai`.`komponen_id`,
				`tbl_praktikum_komponen_nilai`.`materi_id`,
				mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) as `jadwalid`,
				`tbl_praktikum_komponen_nilai`.`jadwal_id`,
				`tbl_praktikum_komponen_nilai`.`judul`,
				`tbl_praktikum_komponen_nilai`.`bobot`,
				`tbl_praktikum_komponen_nilai`.`parent_id`,
				`tbl_praktikum_komponen_nilai`.`kategori`, ";
		if($param2=='view-mk'){		
		$sql .="`tbl_praktikum`.`judul` as `namamateri`,
				`tbl_praktikum`.`nama_mk` as `namamk`,
				
                `tbl_praktikum`.`mkditawarkan_id`, ";
		}else{
		$sql .="`tbl_praktikum_materi`.`judul` as `namamateri`,
				`tbl_praktikum_materi`.`nama_mk` as `namamk`,
				
                `tbl_praktikum_materi`.`mkditawarkan_id`, ";	
		}
      
				
		if($param=='list'){
			$sql .= "  (SELECT k.bobot FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` as k WHERE k.komponen_id = tbl_praktikum_komponen_nilai.`parent_id` ) as bobot_parent,
					   (SELECT COUNT(*) FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` as k WHERE k.`parent_id` = `tbl_praktikum_komponen_nilai`.`komponen_id`) as countchild, 
					   CASE (SELECT COUNT(*) FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` as k WHERE k.`parent_id` = `tbl_praktikum_komponen_nilai`.`komponen_id`) WHEN 0 THEN 1 ELSE 0 END as inforow,
					";
		}else{
			if($param=="parent"){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` = '0' ";
		}
		
		if($param=="child"){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` != '0' ";
		}
		}
		
		if($param2=='view-mk'){		
         $sql.="mid(md5(`tbl_praktikum`.`mkditawarkan_id`),9,7) as `mkid`

				FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai`
				LEFT JOIN `db_ptiik_apps`.`tbl_praktikum` ON `tbl_praktikum`.`jadwal_id` = `tbl_praktikum_komponen_nilai`.`jadwal_id`
				WHERE 1
			   ";
		}else{
		$sql.="mid(md5(`tbl_praktikum_materi`.`mkditawarkan_id`),9,7) as `mkid`

				FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai`
				LEFT JOIN `db_ptiik_apps`.`tbl_praktikum_materi` ON `tbl_praktikum_materi`.`materi_id` = `tbl_praktikum_komponen_nilai`.`materi_id`
				WHERE 1
			   ";	
		}
		
		
		
		
		if($thn){
			$sql .= " AND `tbl_praktikum_materi`.`tahun_akademik` = '".$thn."' ";
		}
		
		if($komponen){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` = '".$komponen."' ";
		}
		
		if($materiid){
			$sql .= " AND mid(md5(`tbl_praktikum_materi`.`materi_id`),9,7) = '".$materiid."' ";
		}
		
		if($materi_nilai){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`materi_id` = '".$materi_nilai."' ";
		}
		
		if($param2=='parent-kom'){
			 $sql .= " AND mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		if($nilai_id){
			if($nilai_param=='md5'){
				$sql .= " AND mid(md5(`tbl_praktikum_komponen_nilai`.`nilai_id`),9,7) = '".$nilai_id."' ";
			}else{
				$sql .= " AND `tbl_praktikum_komponen_nilai`.`nilai_id` = '".$nilai_id."' ";
			}
			 
		}
		
		if($kategori!='akhir'){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`kategori` != 'akhir' ";
		}else{
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`kategori` = 'akhir' ";
		}
		
		if($id){
			if($param=="komponen"){
				 $sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` = '0' ";
				 $sql .= " AND (mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) = '".$id."' OR  `tbl_praktikum_komponen_nilai`.`jadwal_id`='$id')";
			}
			else{
				if($param2=='view-mk'){
					$sql .= " AND mid(md5(`tbl_praktikum`.`mkditawarkan_id`),9,7) = '".$id."' ";
				}else{
					$sql .= " AND mid(md5(`tbl_praktikum_materi`.`mkditawarkan_id`),9,7) = '".$id."' ";
				}
			}
			
			if($param2=='view-mk'){
				$sql .= " GROUP BY `tbl_praktikum_komponen_nilai`.`komponen_id` ";
			}
			
			$result = $this->db->query( $sql );
			// echo $sql."<br>";
			return $result;
		}
		
		if($param=='list'){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`jadwal_id` = '".$jadwalid."' ";
			
			if($param2=='get-by-param'){
				$sql .= " AND `tbl_praktikum_komponen_nilai`.`komponen_id` = '".$id2."' ";
			}
			
			$result = $this->db->query( $sql );
			// echo $sql;
			return $result;	 
		}
		
	}
	
	function read_materi($id=NULL,$thn=NULL,$param=NULL,$param2=NULL,$jadwalid=NULL, $level=NULL){
		$sql = "SELECT mid(md5(`tbl_praktikum_materi`.`materi_id`),9,7) as `materi_id`,
				`tbl_praktikum_materi`.`materi_id` as `mtrid`,
				`tbl_praktikum_materi`.`jadwal_id`,
				`tbl_praktikum_materi`.`judul`,
				`tbl_praktikum_materi`.`parent_id`,
				`tbl_praktikum_materi`.`mkditawarkan_id`,
				`tbl_praktikum_materi`.`nama_mk`,
				`tbl_praktikum_materi`.`kode_mk`,
				`tbl_praktikum_materi`.`tahun_akademik`,
				 ";
		
		if($param2=='edit'){
			$sql.= "mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) as `jadwalid`,
                    `tbl_praktikum_komponen_nilai`.`jadwal_id`,
				   ";
		}
				
		$sql.= "mid(md5(`tbl_praktikum_materi`.`mkditawarkan_id`),9,7) as `mkid` 
				FROM `db_ptiik_apps`.`tbl_praktikum_materi` ";
		
		if($param2=='edit'){
			$sql.= "LEFT JOIN `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` 
			        ON `tbl_praktikum_komponen_nilai`.`materi_id` = `tbl_praktikum_materi`.`materi_id`
				   ";
		}	
				
		$sql.= "WHERE 1
                AND `tbl_praktikum_materi`.`materi_id` 
                IN (SELECT `tbl_praktikum_komponen_nilai`.`materi_id` 
                	FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` 
                	WHERE ";
		
		if($param2=='jadwal'){
			$sql .= " mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) = '".$jadwalid."' ) ";
		}else{
			$sql .= " 1 ) ";
		}
			   
		if($thn){
			$sql .= " AND `tbl_praktikum_materi`.`tahun_akademik` = '".$thn."' ";
		}
		
		if($id){
			if($param=='mkid'){
				$sql .= " AND `tbl_praktikum_materi`.`mkditawarkan_id` = '".$id."' ";
				
				$result = $this->db->query( $sql );
				// echo $sql;
				return $result;
				
			}
			if($param=='materiid'){
				$sql .= " AND mid(md5(`tbl_praktikum_materi`.`materi_id`),9,7) = '".$id."' ";
				
				if($param2=='edit'){
					$sql .= " GROUP BY `tbl_praktikum_materi`.`materi_id` ";
				}
				
				$result = $this->db->getRow( $sql );
				// echo $sql;
				return $result;
			}
			
		}
		
	}
	
	function get_materimd5($id=NULL){
		$sql = "SELECT tbl_praktikum_materi.materi_id
				FROM db_ptiik_apps.tbl_praktikum_materi
				WHERE mid(md5(`tbl_praktikum_materi`.`materi_id`),9,7) = '".$id."'
			   ";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->materi_id;
		return $strresult;
	}
	
	function get_jadwalmd5($id=NULL){
		$sql = "SELECT tbl_praktikum_nilai.jadwal_id, tbl_praktikum_nilai.mkditawarkan_id
				FROM db_ptiik_apps.tbl_praktikum_nilai
				WHERE mid(md5(`tbl_praktikum_nilai`.`jadwal_id`),9,7) = '".$id."'
			   ";
		$dt = $this->db->getRow( $sql );
		return $dt;
	}
	
	function get_komponenmd5($id=NULL,$param=NULL){
		$sql = "SELECT mid(md5(tbl_praktikum_komponen_nilai.komponen_id),9,7) as komponenid,
				tbl_praktikum_komponen_nilai.komponen_id
				FROM db_ptiik_apps.tbl_praktikum_komponen_nilai
				WHERE 1";
		
		if($param=='byid'){
			$sql .= " AND tbl_praktikum_komponen_nilai.komponen_id = '".$id."' ";
		}
		
		if($param=='byidmd5'){
			$sql .= " AND mid(md5(tbl_praktikum_komponen_nilai.komponen_id),9,7) = '".$id."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		// $strresult = $dt->nilaiid;
		return $dt;
	}
	
	function get_komponenId(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(komponen_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_praktikum_komponen_nilai WHERE LEFT (komponen_id, 6) = '".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_komponen($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum_komponen_nilai',$datanya);
	}
	
	function delete_komponen($komponenid=NULL,$str=NULL){
		$sql .= "DELETE FROM db_ptiik_apps.tbl_praktikum_komponen_nilai
			 	 WHERE mid(md5(`tbl_praktikum_komponen_nilai`.`komponen_id`),9,7) = '".$komponenid."' 
				";	
		
		if($str="parent"){
			$sql2 .= "DELETE FROM db_ptiik_apps.tbl_praktikum_komponen_nilai
				 	 WHERE mid(md5(`tbl_praktikum_komponen_nilai`.`parent_id`),9,7) = '".$komponenid."' 
					";
			$this->db->query( $sql2 );
		}		
		
		$this->db->query( $sql );
	}
	
	function delete_prktikum_nilai_mhs($komponenid=NULL,$nilaiid=NULL){
		$sql .= "DELETE FROM db_ptiik_apps.tbl_praktikum_nilai_mhs
			 	 WHERE mid(md5(`tbl_praktikum_nilai_mhs`.`komponen_id`),9,7) = '".$komponenid."' 
			 	 AND mid(md5(`tbl_praktikum_nilai_mhs`.`nilai_id`),9,7) = '".$nilaiid."' 
				";		
		
		$this->db->query( $sql );
	}
	
	function get_mahasiswa_praktikum($prodi=NULL, $mkid=NULL, $jadwalid=NULL,$param=NULL){
		$sql = "SELECT DISTINCT
				db_ptiik_apps.tbl_praktikum_mhs.mahasiswa_id,
				db_ptiik_apps.tbl_mahasiswa.nama as nama_mhs,
				db_ptiik_apps.tbl_mahasiswa.nim,
				db_ptiik_apps.tbl_praktikum.nama_mk,
				db_ptiik_apps.tbl_praktikum.kode_mk,
				db_ptiik_apps.tbl_praktikum.jadwal_id,
				db_ptiik_apps.tbl_praktikum.kelas_id,
				db_ptiik_apps.tbl_praktikum.prodi_id
				FROM
				db_ptiik_apps.tbl_praktikum_mhs
				INNER JOIN db_ptiik_apps.tbl_praktikum ON db_ptiik_apps.tbl_praktikum_mhs.mkditawarkan_id = db_ptiik_apps.tbl_praktikum.mkditawarkan_id AND db_ptiik_apps.tbl_praktikum_mhs.kelas_id = db_ptiik_apps.tbl_praktikum.kelas_id 
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_praktikum_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id AND db_ptiik_apps.tbl_praktikum.prodi_id = db_ptiik_apps.tbl_mahasiswa.prodi_id
				INNER JOIN db_ptiik_apps.tbl_praktikum_nilai ON db_ptiik_apps.tbl_praktikum_nilai.mkditawarkan_id = db_ptiik_apps.tbl_praktikum_mhs.mkditawarkan_id AND db_ptiik_apps.tbl_praktikum_nilai.jadwal_id = db_ptiik_apps.tbl_praktikum.jadwal_id
				WHERE 1
			   ";
		
		if($prodi){
			$sql .= " AND tbl_praktikum.prodi_id = '".$prodi."' ";
		}
		if($mkid){
			$sql .= " AND tbl_praktikum.mkditawarkan_id = '".$mkid."' ";
		}
		if($jadwalid){
			if($param=='jadwalmd5'){
				$sql .= " AND mid(md5(tbl_praktikum.jadwal_id),9,7) = '".$jadwalid."' ";
				$sql .= " ORDER BY tbl_mahasiswa.nama ";
			}
			else{
				$sql .= " AND tbl_praktikum.jadwal_id = '".$jadwalid."' ";
			}
		}
		
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function get_data_nilai_mhs($nilaiid=NULL,$mhsid=NULL,$param=NULL,$md5=NULL,$join=NULL){
		$sql = "SELECT 
				tbl_praktikum_nilai_mhs.*,
				mid(md5(tbl_praktikum_nilai_mhs.komponen_id),9,7) as komponenid,
				tbl_mahasiswa.nama
				FROM db_ptiik_apps.tbl_praktikum_nilai_mhs";
		if($join=='LEFT'){
			$sql .= " LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_praktikum_nilai_mhs.mahasiswa_id = tbl_mahasiswa.mahasiswa_id";
			$sql .= " LEFT JOIN db_ptiik_apps.tbl_praktikum_komponen_nilai ON tbl_praktikum_nilai_mhs.komponen_id = tbl_praktikum_komponen_nilai.komponen_id"; 
		}else{
			$sql .= " INNER JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_praktikum_nilai_mhs.mahasiswa_id = tbl_mahasiswa.mahasiswa_id";
			$sql .= " INNER JOIN db_ptiik_apps.tbl_praktikum_komponen_nilai ON tbl_praktikum_nilai_mhs.komponen_id = tbl_praktikum_komponen_nilai.komponen_id"; 
		}		
		$sql .= " AND tbl_praktikum_nilai_mhs.nilai_id = tbl_praktikum_komponen_nilai.nilai_id
				WHERE 1
			   ";
		
		if($nilaiid){
			if($md5=='nomd5'){
				$sql .= " AND tbl_praktikum_nilai_mhs.nilai_id = '".$nilaiid."' ";
			}else{
				$sql .= " AND mid(md5(tbl_praktikum_nilai_mhs.nilai_id),9,7) = '".$nilaiid."' ";
			}
		}
		
		if($mhsid){
			$sql .= " AND tbl_praktikum_nilai_mhs.mahasiswa_id = '".$mhsid."' ";
		}
		
		if($param=='bymhs'){
			$sql .= " GROUP BY tbl_praktikum_nilai_mhs.mahasiswa_id ORDER BY tbl_mahasiswa.nama ASC";
		}
		
		$result = $this->db->query( $sql );
		// echo $sql."<br>";
		return $result;
	}
	
	function get_nilai_proses($nilaiid=NULL,$mhsid=NULL,$param=NULL){
		$sql = "SELECT
				tbl_praktikum_nilai_mhs.nilai_id,
				tbl_praktikum_nilai_mhs.mahasiswa_id,
				tbl_praktikum_nilai.judul,
				tbl_mahasiswa.nama,
				
				SUM(CASE tbl_praktikum_nilai_mhs.parent_id
				  WHEN 0 THEN (tbl_praktikum_nilai_mhs.inf_bobot/100)*tbl_praktikum_nilai_mhs.nilai
				  ELSE ((tbl_praktikum_nilai_mhs.inf_bobot/100)*tbl_praktikum_nilai_mhs.nilai)*(tbl_praktikum_nilai_mhs.bobot_parent/100)
				END) as nilai_akhir
				
				FROM db_ptiik_apps.tbl_praktikum_nilai_mhs
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_praktikum_nilai_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
				INNER JOIN db_ptiik_apps.tbl_praktikum_nilai ON tbl_praktikum_nilai.nilai_id = tbl_praktikum_nilai_mhs.nilai_id
				WHERE 1
			   ";
		
		if($param=='NA'){
			$sql .= " AND tbl_praktikum_nilai_mhs.nilai_id = '".$nilaiid."' ";
		}
		
		if($param=='NA-mhs'){
			$sql .= " AND mid(md5(tbl_praktikum_nilai_mhs.nilai_id),9,7) = '".$nilaiid."'
                      AND tbl_praktikum_nilai_mhs.mahasiswa_id = '".$mhsid."'
					";
		}
		
		$sql .= " AND tbl_praktikum_nilai_mhs.inf_kategori = 'proses'
				  GROUP BY tbl_praktikum_nilai_mhs.mahasiswa_id
				  ORDER BY tbl_mahasiswa.nama ASC 
			   ";
		// echo $sql."<br>";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_prodi(){
		$sql = "SELECT 
				  `db_ptiik_apps`.`tbl_prodi`.`prodi_id`,
				  `db_ptiik_apps`.`tbl_prodi`.`keterangan`
				  FROM `db_ptiik_apps`.`tbl_prodi` WHERE 1
				 ";
				 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function replace_nilai_mhs($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum_nilai_mhs',$data);
	}
	
	function insert_nilai_mhs($komponenid,$nilai_id,$mhsid,$nilai,$user,$lastupdate,$kategori){
		$sql = "INSERT INTO `db_ptiik_apps`.`tbl_praktikum_nilai_mhs`(`komponen_id`, `nilai_id`, `mahasiswa_id`, `nilai`, `user_id`, `last_update`, `inf_kategori`) 
				VALUES ('".$komponenid."','".$nilai_id."','".$mhsid."','".$nilai."','".$user."','".$lastupdate."','".$kategori."')
				";
		return  $this->db->query( $sql );
	}
	
	function update_nilai_mhs($komponen,$nilaiid,$mhsid,$nilai){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_praktikum_nilai_mhs` 
				SET `nilai`='".$nilai."' 
				WHERE `komponen_id`= '".$komponen."'
				AND `nilai_id`= '".$nilaiid."'
				AND `mahasiswa_id`= '".$mhsid."'
				AND `inf_kategori` = 'proses'
			   ";
		$this->db->query( $sql );
	}
	
	function update_nilai_praktikan($mhsid,$mkid,$kelas,$nilaiakhir){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_praktikum_mhs` 
				SET `nilai`='".$nilaiakhir."' 
				WHERE `mahasiswa_id`= '".$mhsid."'
				AND `mkditawarkan_id`= '".$mkid."'
				AND `kelas_id`= '".$kelas."'
			   ";
		if($this->db->query( $sql ))return 'TRUE';
		else return 'FALSE';
	}
	
	function get_jadwal_kelas($jadwalid){
		$sql = "SELECT 
				tbl_praktikum.mkditawarkan_id,
				tbl_praktikum.kelas_id
				FROM db_ptiik_apps.tbl_praktikum
				WHERE 1
			   ";
				
		if($jadwalid){
			$sql .= " AND mid(md5(tbl_praktikum.jadwal_id),9,7) = '".$jadwalid."' ";
		}	
		
		$dt = $this->db->getRow( $sql );
		// $strresult = $dt->nilaiid;
		return $dt;
		
	}
	
	function update_komp_akhir($kpnid=NULL,$nilid=NULL,$mhsid=NULL,$nilai=NULL){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_praktikum_nilai_mhs` 
				SET `tbl_praktikum_nilai_mhs`.`nilai` = '".$nilai."'
				WHERE `tbl_praktikum_nilai_mhs`.`komponen_id` = '".$kpnid."'
				AND `tbl_praktikum_nilai_mhs`.`nilai_id` = '".$nilid."'
				AND `tbl_praktikum_nilai_mhs`.`mahasiswa_id` = '".$mhsid."'
			   ";
		$this->db->query( $sql );
	}
	
	function get_nilai_komp($komponenid,$nilaiid,$mhsid){
		$sql = "SELECT `tbl_praktikum_nilai_mhs`.`nilai`
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai_mhs` 
				WHERE `tbl_praktikum_nilai_mhs`.`komponen_id` = '".$komponenid."'
				AND `tbl_praktikum_nilai_mhs`.`nilai_id` = '".$nilaiid."'
				AND `tbl_praktikum_nilai_mhs`.`mahasiswa_id` = '".$mhsid."'
			   ";
		// echo $sql;	   
		$dt = $this->db->getRow( $sql );
		// $strresult = $dt->nilaiid;
		return $dt;
	}
	
	function update_tbl_prak_nilai($jadwalid){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_praktikum_nilai` 
				SET `tbl_praktikum_nilai`.`is_proses` = '1', `tbl_praktikum_nilai`.`is_approve` = '0'
				WHERE mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwalid."'
			   ";
		$this->db->query( $sql );
	}
	
	function set_NA($jadwalid,$val){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_praktikum_nilai` 
				SET `tbl_praktikum_nilai`.`is_proses` = '".$val."'
				WHERE mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwalid."'
				AND tbl_praktikum_nilai.judul = 'Nilai Akhir'
			   ";
		$this->db->query( $sql );
	}
	
	function cek_if_NA($jadwalid,$mkditawarkanid){
		$sql = "SELECT tbl_praktikum_nilai.nilai_id 
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai` 
				WHERE tbl_praktikum_nilai.judul = 'Nilai Akhir'
				AND tbl_praktikum_nilai.kategori='akhir'
				AND tbl_praktikum_nilai.jadwal_id = '".$jadwalid."'
				AND tbl_praktikum_nilai.mkditawarkan_id = '".$mkditawarkanid."'
				";
		
		return $dt = $this->db->getRow( $sql );
	}
	
	function cek_komponenId($jadwalid){
		$sql = "SELECT tbl_praktikum_komponen_nilai.komponen_id
				FROM tbl_praktikum_komponen_nilai 
				WHERE tbl_praktikum_komponen_nilai.jadwal_id = '".$jadwalid."'
				AND tbl_praktikum_komponen_nilai.judul = 'Nilai Akhir'
				AND tbl_praktikum_komponen_nilai.kategori = 'akhir'
			   ";
		
		$dt = $this->db->getRow( $sql );
		if($dt){
			return $dt->komponen_id;
		}else{
			return FALSE;
		}
		
	}
	
	function check_NA($jadwalid){
		$sql = "SELECT tbl_praktikum_nilai.nilai_id
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai` 
				WHERE mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwalid."'
				AND tbl_praktikum_nilai.kategori = 'akhir'
				AND tbl_praktikum_nilai.judul = 'Nilai Akhir'
			   ";
		// echo $sql;tbl_praktikum_nilai.is_proses
		$dt = $this->db->getRow( $sql );
		if($dt){
			return $dt->nilai_id;
		}else{
			return '';
		}
	}
	
	function check_NA_proses($jadwalid){
		$sql = "SELECT tbl_praktikum_nilai.is_proses
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai` 
				WHERE mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwalid."'
				AND tbl_praktikum_nilai.kategori = 'akhir'
				AND tbl_praktikum_nilai.judul = 'Nilai Akhir'
			   ";
		
		$dt = $this->db->getRow( $sql );
		if($dt){
			return $dt->is_proses;
		}else{
			return '';
		}
	}
	
	function check_klab($unit_sess=NULL){
		$sql = "SELECT tbl_master_jabatan.keterangan
				FROM  db_ptiik_apps.tbl_karyawan_kenaikan
				INNER JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_unit_kerja.unit_id = tbl_karyawan_kenaikan.unit_id
				INNER JOIN db_ptiik_apps.tbl_master_jabatan ON tbl_master_jabatan.jabatan_id = tbl_karyawan_kenaikan.jabatan_id
				WHERE 1
				AND tbl_unit_kerja.unit_id IN (".$unit_sess.")
				AND tbl_unit_kerja.kategori = 'laboratorium'
				AND tbl_karyawan_kenaikan.is_aktif = '1' ";
		
		$dt = $this->db->getRow( $sql );
		if($dt){
			return 'true';
		}else{
			return '';
		}
		
	}
	
}
?>