<?php
class model_materi extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function get_mk_jadwal($mk = NULL,$thn =NULL,$staff =NULL,$param=NULL,$jadwal=NULL,$prodiid=NULL, $unit_=NULL){
		$sql = "SELECT tbl_praktikum.kode_mk, 
					   tbl_praktikum.mkditawarkan_id,
					   mid(md5(tbl_praktikum.mkditawarkan_id),9,7) as mk_id,
					   tbl_praktikum.nama_mk,
					   tbl_praktikum.tahun_akademik,
					   tbl_praktikum.kelas_id,
					   tbl_praktikum.jadwal_id,
					   mid(md5(tbl_praktikum.jadwal_id),9,7) as jdwl_id 
				  FROM db_ptiik_apps.tbl_praktikum
				  LEFT JOIN db_ptiik_apps.tbl_praktikum_asisten 
				    ON tbl_praktikum_asisten.`mkditawarkan_id` = tbl_praktikum.`mkditawarkan_id`
				  LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_praktikum.mkditawarkan_id
				  LEFT JOIN db_ptiik_apps.tbl_praktikum_lab ON tbl_praktikum_lab.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				  WHERE 1
				";
		if($thn!=""){
			$sql .= " AND  tbl_praktikum.tahun_akademik = '".$thn."'";
		}
		
		if($mk!=""){
			$sql .= " AND mid(md5(tbl_praktikum.mkditawarkan_id),9,7) = '".$mk."'";
		}
		else{
			if(isset($staff)&&$staff!=""&&$staff!='-'){
				// $sql .= "AND tbl_praktikum_asisten.mahasiswa_id = '".$staff."' 
						 // AND tbl_praktikum_asisten.is_aktif = '1'
						// ";
			}
		}
		
		if($prodiid!=""){
			$sql .= " AND tbl_praktikum.prodi_id = '".$prodiid."'";
		}
		
		if($jadwal!=""){
			$sql .= " AND mid(md5(tbl_praktikum.jadwal_id),9,7) = '".$jadwal."'";
		}
		
		if($unit_!=""){
			$sql .= " AND tbl_praktikum_lab.unit_id IN (".$unit_.")";
		}
		
		if($param!="" || $jadwal!=""){
				$sql .= " GROUP BY tbl_praktikum.jadwal_id
						  ORDER BY tbl_praktikum.nama_mk, tbl_praktikum.mkditawarkan_id, tbl_praktikum.kelas_id";
	    }
		else{
			$sql .= " GROUP BY tbl_praktikum.mkditawarkan_id 
				  	  ORDER BY tbl_praktikum.nama_mk, tbl_praktikum.mkditawarkan_id, tbl_praktikum.kelas_id";
		}
		
		if($mk!="" || $jadwal!=""){
			$result = $this->db->getRow( $sql );
		}
		else{
			$result = $this->db->query( $sql );
		}
		
		// echo $sql;
		return $result;
	}
	
	function get_praktikum_materi($mk = NULL,$thn =NULL,$parent=NULL,$materi=NULL,$id=NULL){
		$sql = "SELECT tbl_praktikum_materi.materi_id,
				mid(md5(tbl_praktikum_materi.materi_id),9,7) as m_id,
				mid(md5(tbl_praktikum_materi.mkditawarkan_id),9,7) as mk_id,
				mid(md5(tbl_praktikum_materi.parent_id),9,7) as p_id,
				tbl_praktikum_materi.parent_id,
				tbl_praktikum_materi.tahun_akademik,
				tbl_praktikum_materi.judul
				FROM db_ptiik_apps.tbl_praktikum_materi
				WHERE 1
				";
		if($mk!=""){
			$sql .= " AND mid(md5(tbl_praktikum_materi.mkditawarkan_id),9,7) = '".$mk."'";
		}
		if($thn!=""){
			$sql .= " AND  tbl_praktikum_materi.tahun_akademik = '".$thn."'";
		}
		if($parent!=""){
			$sql .= " AND tbl_praktikum_materi.parent_id = '".$parent."'";
		}
		if($materi!=""){
			$sql .= " AND mid(md5(tbl_praktikum_materi.materi_id),9,7) = '".$materi."'";
			$result = $this->db->getRow( $sql );
			return $result;
		}
		if($id!=""){
			$sql .= " AND mid(md5(tbl_praktikum_materi.materi_id),9,7) != '".$id."'";
		}
		$sql .= " ORDER BY materi_id ASC";
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_materi_by_md5($id=NULL){
		$sql = "SELECT tbl_praktikum_materi.materi_id,
				mid(md5(tbl_praktikum_materi.materi_id),9,7) as m_id,
				tbl_praktikum_materi.judul
				FROM db_ptiik_apps.tbl_praktikum_materi
				WHERE 1
				";
		if($id!=""){
			$sql .= " AND mid(md5(tbl_praktikum_materi.materi_id),9,7) = '".$id."'";
		}
		$result = $this->db->getRow( $sql );
		// echo $sql;
		return $result;
	}
	
	function get_materi_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(materi_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_praktikum_materi
			where left(materi_id,6) = '".date("Ym")."'";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_materi($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_praktikum_materi', $data);
	}
}
?>