<?php
class model_absen extends model {

	public function __construct() {
		parent::__construct();	
	}
		
	public $error;
	public $id;
	public $modified;
	
	function getEvent($day=NULL,$month=NULL,$year=NULL, $runday=NULL, $user=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$this->addNol($month)."-".$this->addNol(($day+1));
		
		$hari=$this->get_namahari($runday);	
		
		$sql = "SELECT DISTINCT
				db_ptiik_apps.tbl_praktikum.tgl,
				db_ptiik_apps.tbl_praktikum.tgl as `tgl_selesai`,
				TIME_FORMAT(db_ptiik_apps.tbl_praktikum.jam_mulai,'%H:%i') as `jam_mulai`, 
				TIME_FORMAT(db_ptiik_apps.tbl_praktikum.jam_selesai,'%H:%i') as `jam_selesai`, 
				db_ptiik_apps.tbl_praktikum.nama_mk as `judul`,
				db_ptiik_apps.tbl_praktikum.prodi_id,
				db_ptiik_apps.tbl_praktikum.nama_mk,
				db_ptiik_apps.tbl_praktikum.kode_mk,
				db_ptiik_apps.tbl_praktikum.keterangan  as `keterangan`,
				db_ptiik_apps.tbl_praktikum.ruang as `ruang`,				
				db_ptiik_apps.tbl_praktikum.praktikum_id
				FROM
				db_ptiik_apps.tbl_praktikum
				WHERE
				 (tbl_praktikum.tgl ='".$tgl."') ";
		if($user){
			$sql = $sql. " AND (tbl_praktikum.user_id='".$user."' ) ";
		}
		$sql = $sql. " ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_praktikum`.jam_mulai,'%H:%i') ASC ";
		
		return $this->db->query( $sql );
	
	}
	
	function get_namahari($runday){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
			case '7': $hari='minggu'; break;
		}
		
		return $hari;
	}
	
	function potong_kalimat ($content, $length) {
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
	function addnol($string){
		if(strlen($string)==1){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	function  get_asisten($semester=NULL, $mk=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_praktikum_asisten.asisten_id,
					db_ptiik_apps.tbl_praktikum_asisten.tahun_akademik,
					db_ptiik_apps.tbl_praktikum_asisten.mkditawarkan_id,
					dbptiik_master.tbl_mahasiswa.mahasiswa_id,
					dbptiik_master.tbl_mahasiswa.nama
					FROM
					db_ptiik_apps.tbl_praktikum_asisten
					INNER JOIN dbptiik_master.tbl_mahasiswa ON db_ptiik_apps.tbl_praktikum_asisten.mahasiswa_id = dbptiik_master.tbl_mahasiswa.mahasiswa_id
				WHERE 1 
					";
		if($semester) $sql.= " AND db_ptiik_apps.tbl_praktikum_asisten.tahun_akademik = '$semester' ";
		if($mk) $sql.= " AND db_ptiik_apps.tbl_praktikum_asisten.mkditawarkan_id = '$mk' ";
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function get_absen_asisten($mk=NULL,$prodi=NULL, $kelas=NULL, $sesi=NULL){
	
	}
	
	function get_absen($mk=NULL,$prodi=NULL, $kelas=NULL, $sesi=NULL){
		$sql ="SELECT
				tbl_praktikum.praktikum_id,
				tbl_praktikum.jadwal_id,
				tbl_praktikum.mkditawarkan_id,
				tbl_praktikum.prodi_id,
				tbl_praktikum.kelas_id,
				tbl_praktikum.total_pertemuan,
				tbl_praktikum.tgl,
				tbl_praktikum.jam_masuk,
				tbl_praktikum.jam_selesai,
				tbl_praktikum.jumlah_jam,
				tbl_praktikum.materi,
				tbl_praktikum.sesi_ke,
				tbl_praktikum.last_update,
				tbl_praktikum.`user`
				FROM
					db_ptiik_apps.tbl_praktikum WHERE 1=1 ";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.prodi_id),5,5) = '".$prodi."') ) ";
		}
		
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.kelas_id = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.kelas_id),5,5) = '".$kelas."') ) ";
		}
		
		if($sesi){
			$sql = $sql . " AND (tbl_praktikum.sesi_ke = '".$sesi."') ";
		}
			
		$result = $this->db->getRow($sql);
		
		return $result;

	}
	
		
	function get_mk($praktikum=NULL, $semester=NULL, $staff=NULL){
		if($staff){
			$sqlx = "SELECT DISTINCT
					`tbl_namamk`.`keterangan` as `mk`,
					`tbl_matakuliah`.`kode_mk`,
					`tbl_mkditawarkan`.`tahun_akademik`,
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					`tbl_jadwalmk`.`mkditawarkan_id`,
					mid(md5(`tbl_jadwalmk`.`mkditawarkan_id`),5,5) as `id`
					FROM
						db_ptiik_apps.`tbl_jadwalmk`
						Inner Join db_ptiik_apps.`tbl_mkditawarkan` ON `tbl_jadwalmk`.`mkditawarkan_id` = `tbl_mkditawarkan`.`mkditawarkan_id`
						Inner Join db_ptiik_apps.`tbl_matakuliah` ON `tbl_mkditawarkan`.`matakuliah_id` = `tbl_matakuliah`.`matakuliah_id`
						Inner Join db_ptiik_apps.`tbl_namamk` ON `tbl_matakuliah`.`namamk_id` = `tbl_namamk`.`namamk_id`
						INNER JOIN db_ptiik_apps.tbl_dosenpengampu ON tbl_jadwalmk.dosen_id = tbl_dosenpengampu.dosen_id
					WHERE 1 = 1";
		}else{
			$sqlx = "SELECT DISTINCT
					`tbl_namamk`.`keterangan` as `mk`,
					`tbl_matakuliah`.`kode_mk`,
					`tbl_mkditawarkan`.`tahun_akademik`,
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					`tbl_jadwalmk`.`mkditawarkan_id`,
					mid(md5(`tbl_jadwalmk`.`mkditawarkan_id`),5,5) as `id`
					FROM
						db_ptiik_apps.`tbl_jadwalmk`
						Inner Join db_ptiik_apps.`tbl_mkditawarkan` ON `tbl_jadwalmk`.`mkditawarkan_id` = `tbl_mkditawarkan`.`mkditawarkan_id`
						Inner Join db_ptiik_apps.`tbl_matakuliah` ON `tbl_mkditawarkan`.`matakuliah_id` = `tbl_matakuliah`.`matakuliah_id`
						Inner Join db_ptiik_apps.`tbl_namamk` ON `tbl_matakuliah`.`namamk_id` = `tbl_namamk`.`namamk_id`
					WHERE 1 = 1 ";
		}
		
		$sql = "SELECT DISTINCT
					mid(md5(`tbl_praktikum`.`mkditawarkan_id`),5,5) as `id`,
					tbl_praktikum.mkditawarkan_id,
					tbl_praktikum.tahun_akademik,
					tbl_praktikum.nama_mk as `mk`,
					tbl_praktikum.total_pertemuan as `jumlah_pertemuan`,
					tbl_praktikum.kode_mk
					FROM
					db_ptiik_apps.tbl_praktikum WHERE 1 
					";
		
		if($praktikum){
			//$sql = $sql . " AND `tbl_jadwalmk`.`is_praktikum` = '".$praktikum."' ";
		}
					
		if($semester){
			$sql = $sql . " AND `tbl_praktikum`.`tahun_akademik` = '".$semester."' ";
		}
		
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_dosenpengampu.karyawan_id='".$staff."') ";
		}	
		
		$sql = $sql. "	ORDER BY tbl_praktikum.nama_mk ASC";
		
		$result = $this->db->query($sql);
		
				
		return $result;
	}
	
	function get_tgl($id=NULL){
		$sql = "SELECT DISTINCT 
					`tbl_praktikum`.`tgl`
					FROM
					db_ptiik_apps.`tbl_praktikum`
				WHERE 1 = 1
					";
		if($id){
			
		}
	}
	
	function  get_mhs_mk($mk=NULL,$prodi=NULL, $kelas=NULL, $dosen=NULL, $semester=NULL, $sesi=NULL){
		$sql = "SELECT DISTINCT 
					dbptiik_master.tbl_mahasiswa.mahasiswa_id,
					dbptiik_master.tbl_mahasiswa.nim,
					dbptiik_master.tbl_mahasiswa.nama,
					dbptiik_master.tbl_mahasiswa.angkatan,
					db_ptiik_apps.tbl_jadwalmk.kelas_id,
					db_ptiik_apps.tbl_jadwalmk.dosen_id,
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id,
					db_ptiik_apps.tbl_jadwalmk.is_praktikum
					FROM
					db_ptiik_apps.tbl_krsmhs
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_krsmhs.mkditawarkan_id = db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id 
						  AND db_ptiik_apps.tbl_krsmhs.kelas_id = db_ptiik_apps.tbl_jadwalmk.kelas_id
					INNER JOIN dbptiik_master.tbl_mahasiswa ON dbptiik_master.tbl_mahasiswa.jurusan = db_ptiik_apps.tbl_jadwalmk.prodi_id 
						AND db_ptiik_apps.tbl_krsmhs.mahasiswa_id = dbptiik_master.tbl_mahasiswa.mahasiswa_id
					";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.kelas_id = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.kelas_id),5,5) = '".$kelas."') ) ";
		}
		
		if($dosen){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.dosen_id),5,5)='".$dosen."' OR tbl_jadwalmk.dosen_id='".$dosen."') ";
		}	
		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_krsmhs.inf_semester),5,5)='".$semester."' OR tbl_krsmhs.inf_semester='".$semester."') ";
		}		

		$sql = $sql . " ORDER BY dbptiik_master.tbl_mahasiswa.mahasiswa_id ";
		
		$result = $this->db->query($sql);
		
		return $result;		
	}
	
	function get_is_hadir($mhs=NULL, $mk=NULL, $prodi=NULL, $kelas=NULL, $sesi=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_praktikum_absen_mhs.is_hadir
				FROM
				db_ptiik_apps.tbl_praktikum_absen_mhs
				FROM
				db_ptiik_apps.tbl_praktikum_absen_mhs
				INNER JOIN db_ptiik_apps.tbl_praktikum ON db_ptiik_apps.tbl_praktikum_absen_mhs.praktikum_id = db_ptiik_apps.tbl_praktikum.praktikum_id
				INNER JOIN db_ptiik_apps.tbl_praktikum_mhs ON db_ptiik_apps.tbl_praktikum_absen_mhs.praktikan_id = db_ptiik_apps.tbl_praktikum_mhs.praktikan_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_praktikum.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id WHERE 1 ";
		if($mhs){
			$sql = $sql. " AND db_ptiik_apps.tbl_praktikum_mhs.mahasiswa_id='".$mhs."' ";
		}
		
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.kelas_id = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.kelas_id),5,5) = '".$kelas."') ) ";
		}
		
		if($sesi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.sesi_ke = '".$sesi."' ) ";
		}
		
		$result = $this->db->getRow($sql);
		
		return $result;		
	}
	
	function get_jadwal_mk($mk=NULL, $dosen=NULL, $semester=NULL, $staff=NULL){
		$sql = "SELECT
					mid(md5(db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id),5,5) as `id`,
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					db_ptiik_apps.tbl_jadwalmk.jadwal_id,
					db_ptiik_apps.tbl_jadwalmk.kelas_id,
					db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id,
					db_ptiik_apps.tbl_jadwalmk.dosen_id,
					db_ptiik_apps.tbl_jadwalmk.prodi_id,
					db_ptiik_apps.tbl_jadwalmk.jam_mulai,
					db_ptiik_apps.tbl_jadwalmk.jam_selesai,
					db_ptiik_apps.tbl_jadwalmk.hari,
					db_ptiik_apps.tbl_jadwalmk.ruang,
					dbptiik_master.tbl_prodi.singkat as `prodi`,
					dbptiik_master.tbl_karyawan.nama
				FROM
					db_ptiik_apps.tbl_jadwalmk
					INNER JOIN dbptiik_master.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = dbptiik_master.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_dosenpengampu ON db_ptiik_apps.tbl_jadwalmk.dosen_id = db_ptiik_apps.tbl_dosenpengampu.dosen_id
					INNER JOIN dbptiik_master.tbl_karyawan ON db_ptiik_apps.tbl_dosenpengampu.karyawan_id = dbptiik_master.tbl_karyawan.karyawan_id
					WHERE 1= 1 AND db_ptiik_apps.tbl_jadwalmk.is_aktif='1' ";
		if($mk){
			$sql = $sql . " AND (tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR mid(md5(tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ";
		}
		
		if($dosen){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.dosen_id),5,5)='".$dosen."' OR tbl_jadwalmk.dosen_id='".$dosen."') ";
		}
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_dosenpengampu.karyawan_id='".$staff."') ";
		}		

		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_mkditawarkan.tahun_akademik),5,5)='".$semester."' OR tbl_mkditawarkan.tahun_akademik='".$semester."') ";
		}

		$sql = $sql . " ORDER BY dbptiik_master.tbl_prodi.singkat ASC, db_ptiik_apps.tbl_jadwalmk.kelas_id ASC";
		$result = $this->db->query($sql);
		
		
		return $result;

	}
	
	function get_prodi_mk($mk=NULL, $id=NULL, $semester=NULL, $staff=NULL){
		$sql = "SELECT DISTINCT
					mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) as `id`,	
					db_ptiik_apps.tbl_jadwalmk.prodi_id,					
					dbptiik_master.tbl_prodi.singkat as `prodi`
				FROM
					db_ptiik_apps.tbl_jadwalmk
					INNER JOIN dbptiik_master.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = dbptiik_master.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_dosenpengampu ON db_ptiik_apps.tbl_jadwalmk.dosen_id = db_ptiik_apps.tbl_dosenpengampu.dosen_id
					INNER JOIN dbptiik_master.tbl_karyawan ON db_ptiik_apps.tbl_dosenpengampu.karyawan_id = dbptiik_master.tbl_karyawan.karyawan_id
					WHERE 1= 1 AND db_ptiik_apps.tbl_jadwalmk.is_aktif='1' ";
		if($mk){
			$sql = $sql . " AND (tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR mid(md5(tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ";
		}
		
		if($id){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.dosen_id),5,5)='".$id."' OR tbl_jadwalmk.dosen_id='".$id."') ";
		}
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_dosenpengampu.karyawan_id='".$staff."') ";
		}		

		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_mkditawarkan.tahun_akademik),5,5)='".$semester."' OR tbl_mkditawarkan.tahun_akademik='".$semester."') ";
		}

		$sql = $sql . " ORDER BY dbptiik_master.tbl_prodi.singkat ASC, db_ptiik_apps.tbl_jadwalmk.kelas_id ASC";
		$result = $this->db->query($sql);
		
		return $result;

	}
	
	function get_kelas_mk($mk=NULL, $id=NULL, $semester=NULL, $prodi=NULL){
		$sql = "SELECT DISTINCT
					mid(md5(db_ptiik_apps.tbl_jadwalmk.kelas_id),5,5) as `id`,
					db_ptiik_apps.tbl_jadwalmk.kelas_id
				FROM
					db_ptiik_apps.tbl_jadwalmk
					INNER JOIN dbptiik_master.tbl_prodi ON db_ptiik_apps.tbl_jadwalmk.prodi_id = dbptiik_master.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_jadwalmk.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_dosenpengampu ON db_ptiik_apps.tbl_jadwalmk.dosen_id = db_ptiik_apps.tbl_dosenpengampu.dosen_id
					INNER JOIN dbptiik_master.tbl_karyawan ON db_ptiik_apps.tbl_dosenpengampu.karyawan_id = dbptiik_master.tbl_karyawan.karyawan_id
					WHERE 1= 1 AND db_ptiik_apps.tbl_jadwalmk.is_aktif='1' ";
		if($mk){
			$sql = $sql . " AND (tbl_jadwalmk.mkditawarkan_id = '".$mk."' OR mid(md5(tbl_jadwalmk.mkditawarkan_id),5,5) = '".$mk."') ";
		}
		
		if($id){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.dosen_id),5,5)='".$id."' OR tbl_jadwalmk.dosen_id='".$id."') ";
		}
		
		if($prodi){
			$sql=$sql . " AND (mid(md5(tbl_jadwalmk.prodi_id),5,5)='".$prodi."' OR tbl_jadwalmk.prodi_id='".$prodi."') ";
		}		

		if($semester){
			$sql=$sql . " AND (mid(md5(tbl_mkditawarkan.tahun_akademik),5,5)='".$semester."' OR tbl_mkditawarkan.tahun_akademik='".$semester."') ";
		}

		$sql = $sql . " ORDER BY db_ptiik_apps.tbl_jadwalmk.kelas_id ASC";
		$result = $this->db->query($sql);
	
		return $result;

	}
	
	function get_dosen_id($id,$mkid){
		$sql = "SELECT dosen_id FROM db_ptiik_apps.tbl_dosenpengampu where `db_ptiik_apps`.`tbl_dosenpengampu`.`karyawan_id`='".$id."' AND 
				`db_ptiik_apps`.`tbl_dosenpengampu`.`mkditawarkan_id`='".$mkid."' ";
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function get_dosen_mk($semester=NULL, $mk=NULL, $id=NULL, $staff=NULL){
		$sql = "SELECT
					`tbl_dosenpengampu`.`is_koordinator`,
					`tbl_dosenpengampu`.`dosen_id`,
					mid(md5(`tbl_dosenpengampu`.`dosen_id`),5,5) as `id`,
					`tbl_dosenpengampu`.`karyawan_id`,
					`tbl_dosenpengampu`.`mkditawarkan_id`,
					mid(md5(`tbl_dosenpengampu`.`mkditawarkan_id`),5,5) as `mkid`, 
					`tbl_mkditawarkan`.`jumlah_pertemuan`,
					`tbl_karyawan`.`nik`,
					`tbl_karyawan`.`nama`,
					`tbl_karyawan`.`gelar_awal`,
					`tbl_karyawan`.`gelar_akhir`
				FROM
					`db_ptiik_apps`.`tbl_dosenpengampu`
					Inner Join `dbptiik_master`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_dosenpengampu`.`karyawan_id` = `dbptiik_master`.`tbl_karyawan`.`karyawan_id`					
					Inner Join `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_dosenpengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
				WHERE 1 = 1 
				";
		if($semester){
			$sql = $sql . " AND `tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		
		if($mk){
			$sql = $sql . " AND (`tbl_mkditawarkan`.`mkditawarkan_id` = '".$mk."' OR mid(md5(`tbl_dosenpengampu`.`mkditawarkan_id`),5,5) = '".$mk."') ";
		}
		
		if($staff){
			$sql=$sql . " AND (db_ptiik_apps.tbl_dosenpengampu.karyawan_id='".$staff."') ";
		}	
		
		if($id){
			$sql=$sql . " AND (mid(md5(`db_ptiik_apps`.`tbl_dosenpengampu`.`dosen_id`),5,5)='".$id."' OR `db_ptiik_apps`.`tbl_dosenpengampu`.`dosen_id`='".$id."') ";
		}	
		
		$result = $this->db->query($sql);
		
		return $result;
	}
	
	function get_jumlah_pertemuan($semester=NULL, $mk=NULL, $id=NULL){
		$sql = "SELECT jumlah_pertemuan FROM db_ptiik_apps.tbl_mkditawarkan WHERE 1 = 1 ";
		
		if($semester){
			$sql = $sql . " AND `tbl_mkditawarkan`.`tahun_akademik` = '".$semester."' ";
		}
		
		if($mk){
			$sql = $sql . " AND (`tbl_mkditawarkan`.`mkditawarkan_id` = '".$mk."' OR mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),5,5) = '".$mk."') ";
		}
		
		$result = $this->db->getRow($sql);
		
		return $result->jumlah_pertemuan;
		
	}
	
	
	function get_hadir_mhs($mk=NULL, $prodi=NULL, $kelas=NULL, $mhs=NULL, $ishadir=NULL){
		$sql = "SELECT
					Count(db_ptiik_apps.tbl_praktikum_absen_mhs.is_hadir) AS jml,
					db_ptiik_apps.tbl_praktikum_absen_mhs.is_hadir
					FROM
					db_ptiik_apps.tbl_praktikum
					INNER JOIN db_ptiik_apps.tbl_praktikum_absen_mhs ON db_ptiik_apps.tbl_praktikum.praktikum_id = db_ptiik_apps.tbl_praktikum_absen_mhs.praktikum_id
					INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_praktikum.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id 
					GROUP BY
						db_ptiik_apps.tbl_praktikum.jadwal_id,
						db_ptiik_apps.tbl_praktikum_absen_mhs.praktikan_id,
						db_ptiik_apps.tbl_praktikum_absen_mhs.is_hadir
					HAVING 1 = 1 ";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.kelas_id = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.kelas_id),5,5) = '".$kelas."') ) ";
		}
		
		if($mhs){
			$sql = $sql . " AND db_ptiik_apps.tbl_praktikum_absen_mhs.praktikan_id = '".$mhs."' ";
		}
		
		if($ishadir){
			$sql = $sql . " AND db_ptiik_apps.tbl_praktikum_absen_mhs.is_hadir = '".$ishadir."' ";
		}
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$str= $result->jml;		
		}else{
			$str= 0;
		}
		
		return $str;
	}	
	
	function get_hadir_dosen($mk=NULL, $prodi=NULL, $kelas=NULL, $dosen=NULL, $ishadir=NULL){
		$sql = "SELECT
						Count(db_ptiik_apps.tbl_praktikum_asisten_jadwal.is_hadir) AS jml,
						db_ptiik_apps.tbl_praktikum_asisten_jadwal.asisten_id,
						db_ptiik_apps.tbl_praktikum.mkditawarkan_id,
						db_ptiik_apps.tbl_praktikum.kelas_id,
						db_ptiik_apps.tbl_jadwalmk.prodi_id
						FROM
						db_ptiik_apps.tbl_praktikum
						INNER JOIN db_ptiik_apps.tbl_praktikum_asisten_jadwal ON db_ptiik_apps.tbl_praktikum.praktikum_id = db_ptiik_apps.tbl_praktikum_asisten_jadwal.praktikum_id
						INNER JOIN db_ptiik_apps.tbl_jadwalmk ON db_ptiik_apps.tbl_praktikum.jadwal_id = db_ptiik_apps.tbl_jadwalmk.jadwal_id
						GROUP BY 
							tbl_praktikum_asisten_jadwal.asisten_id,
							db_ptiik_apps.tbl_praktikum.mkditawarkan_id,
							db_ptiik_apps.tbl_jadwalmk.prodi_id,
							db_ptiik_apps.tbl_praktikum.kelas_id
						HAVING 1 = 1
						 ";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.mkditawarkan_id = '".$mk."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.mkditawarkan_id),5,5) = '".$mk."') ) ";
		}
		
		if($prodi){
			$sql = $sql . " AND (db_ptiik_apps.tbl_jadwalmk.prodi_id = '".$prodi."' OR (mid(md5(db_ptiik_apps.tbl_jadwalmk.prodi_id),5,5) = '".$prodi."') ) ";
		}
		if($kelas){
			$sql = $sql . " AND (db_ptiik_apps.tbl_praktikum.kelas_id = '".$kelas."' OR (mid(md5(db_ptiik_apps.tbl_praktikum.kelas_id),5,5) = '".$kelas."') ) ";
		}
		
		if($dosen){
			$sql = $sql . " AND db_ptiik_apps.tbl_praktikum_asisten_jadwal.asisten_id = '".$dosen."' ";
		}
		
		if($ishadir){
			$sql = $sql . " AND db_ptiik_apps.tbl_praktikum_asisten_jadwal.is_hadir = '".$ishadir."' ";
		}
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$str= $result->jml;		
		}else{
			$str= 0;
		}
		
		return $str;
	}
		
				
	function replace_absen($datanya, $datadosen) {
		//return $this->db->replace('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_praktikum", $datanya);
			
		if( $result ) {		
			$this->replace_absen_dosen($datadosen);
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
			
	}
	
	function replace_absen_dosen($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_praktikum_asisten_jadwal", $datanya);
			
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
			
	}
	
	function replace_absen_mhs($datanya) {				
		$result = $this->db->replace("db_ptiik_apps`.`tbl_praktikum_absen_mhs", $datanya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
		
	
	function update_absen($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_jadwalmk',$datanya,$idnya);
	}
	
	function delete_absen($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_jadwalmk',$datanya);
	}
	
	function get_reg_number($mk=NULL, $prodi=NULL, $kelas=NULL, $sesi=NULL){
		$sql = "SELECT praktikum_id FROM db_ptiik_apps.tbl_praktikum WHERE mkditawarkan_id ='".$mk."' AND prodi_id='".$prodi."' AND kelas_id = '".$kelas."' AND sesi_ke='".$sesi."' ";
		$row = $this->db->getRow( $sql );	
		
		if($row){
			$result = $row->praktikum_id;
		}else{
		
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(praktikum_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_praktikum WHERE left(praktikum_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	function add_nol($str){
		if($str < 10){
			$result = "0".$str;
		}else{
			$result = $str;
		}
		
		return $result;
	}
	
	
}