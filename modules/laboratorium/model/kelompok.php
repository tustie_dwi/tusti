<?php
class model_kelompok extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_semester(){
		$sql = "SELECT 
					tbl_tahunakademik.tahun, 
					tbl_tahunakademik.is_ganjil, 
					tbl_tahunakademik.is_pendek,
					tbl_tahunakademik.tahun_akademik
				FROM db_ptiik_apps.tbl_tahunakademik
				ORDER BY tbl_tahunakademik.tahun_akademik DESC";
		
		return $this->db->query($sql);
	}	
	
	function get_prodi(){
		$sql = "SELECT * FROM db_ptiik_apps.tbl_prodi";
		return $this->db->query($sql);
	}
	
	function get_mk($tahun=NULL, $prodi=NULL){
		$sql = "SELECT DISTINCT tbl_namamk.keterangan, tbl_jadwalmk.kelas as kelas_id, tbl_mkditawarkan.mkditawarkan_id
				FROM db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				WHERE tbl_mkditawarkan.tahun_akademik = '$tahun' AND tbl_jadwalmk.prodi_id = '$prodi'
				ORDER BY tbl_namamk.keterangan, tbl_jadwalmk.kelas";
				
		return $this->db->query($sql);
	}
	
	function get_kelompok(){
		$sql = "SELECT tbl_praktikum_kelompok.kelompok_id, tbl_praktikum_kelompok.nama
				FROM db_ptiik_apps.tbl_praktikum_kelompok
				ORDER BY tbl_praktikum_kelompok.nama";
		return $this->db->query($sql);
	}
	
	function get_mhs($prodi=NULL, $mkd=NULL, $kelas=NULL){
		$sql = "SELECT tbl_mahasiswa.nama, tbl_mahasiswa.nim, tbl_praktikum_kelompok.nama kelompok, tbl_praktikum_mhs.praktikan_id
				FROM db_ptiik_apps.tbl_praktikum_mhs 
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_praktikum_mhs.mahasiswa_id 
				LEFT JOIN db_ptiik_apps.tbl_praktikum_kelompok ON tbl_praktikum_kelompok.kelompok_id = tbl_praktikum_mhs.kelompok_id
				WHERE tbl_praktikum_mhs.mkditawarkan_id = '$mkd'
					AND tbl_praktikum_mhs.kelas_id = '$kelas'
					AND tbl_mahasiswa.prodi_id = '$prodi'
				ORDER BY tbl_mahasiswa.nama";
		// echo $sql;		
		return $this->db->query($sql);
	}
	
	function set_kelompok($id, $kel){
		$sql = "UPDATE db_ptiik_apps.tbl_praktikum_mhs SET kelompok_id = '$kel' WHERE tbl_praktikum_mhs.praktikan_id = '$id'";
		if($this->db->query($sql)) return "true";
		else return "false";
	}
	
}