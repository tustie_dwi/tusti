<?php
class laboratorium_nilai extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
		
	function nok(){
		$mlaboratorium = new model_laboratorium();
		
		$data['posts'] 		 = $mlaboratorium->read_jadwal();
		
		$data['status'] 	= 'NOK';
		$data['statusmsg']  = 'Data gagal tersimpan.';
		
		$this->coms->add_style('bootstrap/css/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');

		$this->view( 'nilai/index.php', $data );
	}
	
	function ok(){
		$mlaboratorium = new model_laboratorium();

		$data['posts'] 		= $mlaboratorium->read_jadwal();

		$data['status'] 	= 'OK';
		$data['statusmsg']  = 'Data telah tersimpan.';

		$this->coms->add_style('bootstrap/css/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		$this->view( 'nilai/index.php', $data );			
	}
	
	function index(){	
		
		$mlaboratorium = new model_laboratorium();

		$data['posts'] = $mlaboratorium->read_jadwal();

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');

		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');

		$this->coms->add_script('ckeditor/ckeditor.js');

		$this->coms->add_script('js/jsall.js');

		$this->view('nilai/index.php', $data);
	}
	
	function write(){
	
		if(isset($_POST['b_save'])){
			$this->save();
			exit();
		}
		
		$mlaboratorium = new model_laboratorium();
		$mconf	  	   = new model_confinfo();
		
		if(isset($_POST['cmbsemester'])){
			$semester = $_POST['cmbsemester'];
		}else{
			$semester = "";
		}
		
		$data['semester'] 	= $mconf->get_semester();
		$data['lab']		= $mconf->get_unit('','laboratorium');
		$data['mk']			= $mlaboratorium->get_mk_by_semester($semester);
		$data['takademik']	= $semester;
		$data['posts']		= "";
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
						
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->coms->add_script('ckeditor/ckeditor.js');
		
		$this->coms->add_script('js/jsall.js');
		
		$this->view('nilai/edit.php', $data);
	}
	
	
		
	function edit($id){
		if( !$id ) {
			$this->redirect('module/laboratorium/nilai');
			exit;
		}
		
		$mconf 		= new model_confinfo();
		$mlaboratorium 	= new model_laboratorium();	
		
		$data['posts'] = $mlaboratorium->read($id);	
		$data['tahap'] = $mlaboratorium->get_tahap();
						
		$this->add_script('js/barang.js');
		
		$this->view('master/edit.php', $data);
	}
	
	function save(){	
		if(isset($_POST['b_save'])){
			$this->save_proses();
			exit();
		}else{
			$this->redirect('module/laboratorium/nilai');
			exit;
		}
	
	}
	
	function save_proses(){
		ob_start();
		
		$mlaboratorium 	= new model_laboratorium();
		
		if(isset($_POST['hidId'])&&($_POST['hidId'])){
			$id		= $_POST['hidId'];
			$nilai = $id;
			$action = "update";
		}else{
			$id 	= date("YmdHis");
			$action = "insert";
			$nilai = "";
		}
				
		$lastupdate	= date("Y-m-d H:i:s");
		$tahun		= date("y");
		$user		= $this->coms->authenticatedUser->username;
		
		$semester	= $_POST['cmbsemester'];
		$unit		= $_POST['cmbunit'];
						
		if(isset($_POST['cmbmhs'])){
			$cmhs = explode(",", $_POST['cmbmhs']);
			
			for($i=0;$i<count($cmhs);$i++){
				if(! $nilai){
					$kid = $id.$i;
				}else{
					$kid = $id;
				}
							
				$mhs = $mlaboratorium->read_asisten($cmhs[$i],"", $semester, $unit);
				
				$asisten = $mhs->asisten_id;
				
				if($asisten){
					
					$datanya = array('nilaiasisten_id'=>$kid, 'asisten_id'=>$asisten, 'nilai_id'=>$_POST['cmbnilai'], 'user'=>$user, 'last_update'=>$lastupdate);
					$mlaboratorium->replace_nilai_asisten($datanya);
				}else{
					
					$dataasisten = array('asisten_id'=>$kid, 'tahun_akademik'=>$semester, 'unit_id'=>$unit, 'mahasiswa_id'=>$cmhs[$i]);				
					$mlaboratorium->replace_asisten($dataasisten);
					
					$datanya = array('nilaiasisten_id'=>$kid, 'asisten_id'=>$kid, 'nilai_id'=>$_POST['cmbnilai'], 'user'=>$user, 'last_update'=>$lastupdate);
					$mlaboratorium->replace_nilai_asisten($datanya);
				}
			}
		}
						
		$this->redirect('module/laboratorium/nilai/ok');
		exit();	
			
	}
		
	
	function insert_file($id, $kodefile, $filename, $periode, $mlaboratorium, $user, $lastupdate){
		$extension = $this->getExtension($filename);
		$extension = strtolower($extension);
						
		if (($extension == "jpg") || ($extension == "jpeg") || ($extension == "png")|| ($extension == "gif")){ 
			$result['error'] = "Unknown extension! Please upload document only";
			print "<script> alert('Unknown extension! Please upload document only'); </script>";
			$errors=1; 
			
			$this->redirect('module/laboratorium/write/nok');
			exit;
			
		}
		
		$dir = "assets/uploads/file/laboratorium/".$periode;
		
		if(!file_exists($dir) OR !is_dir($dir)){
			mkdir($dir, 0777, true);
		} 
		
		$newname=$dir."/".$filename;
		
		$copied = move_uploaded_file($_FILES['file']['tmp_name'], $newname); 
		
			
		if (!$copied){  				
			print "<script> alert('Copy unsuccessfull!'); </script>";
			$errors=1; 
			
			$this->redirect('module/laboratorium/write/nok');
			exit;
			
		}else{
			$errors=0;
			
			$datanya = array('file_id'=>$kodefile, 'permintaan_id'=>$id, 'nama_file'=>$filename, 'lokasi'=>$newname, 'user'=>$user, 'last_update'=>$lastupdate);
			$mlaboratorium->replace_file($datanya);
			
			print "<script> alert('Copy successfull!'); </script>";
		}					
				
	}
	
		
	function delete(){
		$id = $_POST['id'];
		
		$mlaboratorium = new model_laboratorium();
		
		ob_start();
		
		$result = array();
		
		$datanya=array('kategori_id'=>$id);
				
		if($mlaboratorium->delete_komponen($datanya))
			$result['status'] = "OK";
		else {
			$result['status'] = "NOK";
			$result['error'] = $mlaboratorium->error;
		}
		echo json_encode($result);
	}
	
	function getExtension($str) { 
		$i = strrpos($str,".");
		if (!$i) { return ""; } 
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext; 
	}  
	
	
	function send_notifikasi($id=NULL, $stproses=NULL){
		require ('library/class.phpmailer.php');
		
		$mlaboratorium = new model_laboratorium();
		
		$mail     = new PHPMailer();
		
		$laboratorium  	= $mlaboratorium->read_jadwal('',$id);
		$permintaan	= $mlaboratorium->get_permintaan_id($id);
		
		//echo $permintaan->permintaan_id;
		//exit();
		
						
		$body='<br>Berikut adalah  nilai pekerjaan dari permintaan laboratorium yang tercatat pada PTIIK Apps :';
		
		$body.= '<table>';
			foreach ($laboratorium as $dt):			
				$rpic[]= array($dt->nama, $dt->email_user);
								
				$body.= '<tr>';
					$body.= '<td>Request By</td><td>:</td><td><b>'.$dt->request_by.'</b></td>';
				$body.= '</tr>';
				
				$body.= '<tr>';
					$body.= '<td>No Permintaan</td><td>:</td><td>'.$dt->no_permintaan.'</td>';
				$body.= '</tr>';
				$body.= '<tr>';
					$body.= '<td>Tgl Permintaan</td><td>:</td><td>'.$dt->tgl_permintaan.'</td>';
				$body.= '</tr>';
				$body.= '<tr valign=top>';
					$body.= '<td>Keterangan</td><td>:</td><td><b>'.$dt->judul.'</b><br>'.$dt->keterangan.'</td>';
				$body.= '</tr>';
				$body.= '<tr>';
					$body.= '<td>Tgl Deadline User*</td><td>:</td><td>'.$dt->tgl_harapan_selesai.'</td>';
				$body.= '</tr>';
				
				$body.= '<tr>';
					$body.= '<td>St Prosess</td><td>:</td><td>'.$dt->st_proses.'</td>';
				$body.= '</tr>';
				
				if($dt->disetujui_oleh){
					$rpic[]= array($dt->disetujui_oleh, $dt->email_persetujuan);
					
					$body.= '<tr>';
						$body.= '<td>'.$dt->st_proses.' By</td><td>:</td><td><b>'.$dt->disetujui_oleh.'</b></td>';
					$body.= '</tr>';
					$body.= '<tr>';
						$body.= '<td>Tgl Mulai Pengerjaan</td><td>:</td><td><b>'.$dt->tgl_mulai_pengerjaan.'</b></td>';
					$body.= '</tr>';
					$body.= '<tr>';
						$body.= '<td>Tgl Selesai</td><td>:</td><td><b>'.$dt->tgl_deadline.'</b></td>';
					$body.= '</tr>';
				}
			endforeach;
			
			$pelaksana= $mlaboratorium->get_pelaksana($permintaan->permintaan_id);
			
			if($pelaksana){
				$body.= '<tr valign=top>';
					$body.= '<td>PIC</td><td>:</td><td>';
					$i=0;
						$body.= "<ol>";
							//$rpic = array();
							foreach($pelaksana as $dt):
								$i++;
								$pic 	= $mlaboratorium->get_karyawan($dt->karyawan_id);
								
								$epic 	= $pic->email;
								$npic	= $pic->nama;
								
								$body.= "<li>".$npic."</li>";
								
								$rpic[]= array($epic, $npic);
							endforeach;
						$body.= "</ol>";
					$body.= '</td>';
				$body.= '</tr>';
			}
		$body.= '</table>';
		
		$efooter= '<br><br>Untuk informasi lebih lanjut, silahkan mengunjungi <a href="https://175.45.187.243/apps/info/laboratorium">http://175.45.187.243/apps</a><br><br><br>Terima kasih, <br><br><br><br>
					<hr><small>Badan Pengembangan Teknologi Informasi dan Komunikasi<br>
					<b>PROGRAM TEKNOLOGI INFORMASI DAN ILMU KOMPUTER</b><br>
					<b>UNIVERSITAS BRAWIJAYA</b><br>
					Ruang B1.1 Gedung B PTIIK UB<br>
					Jl. Veteran no 8 Malang, 65145, Indonesia<br>
					telp : (0341) 577911 Fax : (0341) 577911</small>';
		
		$nilai = $mlaboratorium->read_pelaksana_nilai($id);
		
		
		if($nilai){
			$body.= "<table style='border-collapse:collapse;'>";
			foreach($nilai as $dt){				
				$body.= "<thead><tr ><td colspan=7>nilai ".$dt->nama."</td></tr>";
				
				$detail = $mlaboratorium->read_detail_nilai($dt->pelaksana_id);
				
				if($detail){
					$body.= "<tr><th style='border: 1px solid #A3A3A3;'>No</th><th style='border: 1px solid #A3A3A3;'>Tgl Mulai</th>
							<th style='border: 1px solid #A3A3A3;padding:3px;'>Target Selesai</th><th style='border: 1px solid #A3A3A3;'>Tgl Selesai</th><th style='border: 1px solid #A3A3A3;'>Kegiatan</th>
							<th style='border: 1px solid #A3A3A3;'>Keterangan</th><th style='border: 1px solid #A3A3A3;'>Inf nilai</th></tr></thead><tbody>";
					
					$i=0;
					foreach ($detail as $data){
						$i++;
						$body.= "<tr>
									<td style='border: 1px solid #A3A3A3;padding:3px;'>".$i."</td>
									<td style='border: 1px solid #A3A3A3;padding:3px;'>".date("M d, Y", strtotime($data->tgl_mulai))."</td>
									<td style='border: 1px solid #A3A3A3;padding:3px;'>".date("M d, Y", strtotime($data->tgl_target_selesai))."</td>";
						if($data->tgl_selesai!='0000-00-00'){
							$body.= "<td style='border: 1px solid #A3A3A3;padding:3px;'>".date("M d, Y", strtotime($data->tgl_selesai))."</td>";
						}else{
							$body.= "<td style='border: 1px solid #A3A3A3;padding:3px;'>-</td>";
						}
						$body.= "<td style='border: 1px solid #A3A3A3;padding:3px;'>".$data->judul."</td>
									<td style='border: 1px solid #A3A3A3;padding:3px;'>".$data->keterangan."</td>
									<td style='border: 1px solid #A3A3A3;padding:3px;'>".$data->inf_nilai."%</td>
								</tr>";
					}
				}
			}		
			$body.= "</tbody></table>";
			
						
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = "mail.yourdomain.com"; // SMTP server
			$mail->SMTPDebug  = false;                     // enables SMTP debug information (for testing)
													   // 1 = errors and messages
													   // 2 = messages only
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
			$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
			$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
			$mail->Username   = "bptiik.ub@gmail.com";  // GMAIL username
			$mail->Password   = "bptiikok";            // GMAIL password

			$mail->SetFrom('bptiik.ptiik@ub.ac.id', 'BPTIK - PTIIK UB');
			$mail->AddReplyTo('bptiik.ptiik@ub.ac.id',"BPTIK - PTIIK UB");					

			$mail->Subject    = "nilai Permintaan laboratorium PTIIK Apps";

			$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test


			//$address = $email;
			
			foreach ($rpic as $data){
				if($data[1]){
					$mail->MsgHTML('Yth. '.$data[1]. ', <br>'. $body.$efooter);
					
					$mail->ClearAddresses();
					 
					$mail->AddAddress($data[0], $data[1]);
					
					if($mail->Send()){
						$error=0;
					}else{
						$error=1;
					}
				}
			}
		}
	}
	
}
?>