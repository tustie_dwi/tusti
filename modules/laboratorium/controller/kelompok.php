<?php
class laboratorium_kelompok extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mkelompok = new model_kelompok();	
		$this->add_script('js/kelompok.js');
		
		$data['tahun'] = $mkelompok->get_semester();
		$data['prodi'] = $mkelompok->get_prodi();
		$data['kelompok'] = $mkelompok->get_kelompok();
		$data['header'] = 'Kelompok Praktikum';
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->view( 'kelompok/index.php', $data );
	}
	
	function get_mk(){
		$mkelompok = new model_kelompok();	
		
		$mk = $mkelompok->get_mk($_POST['tahun'], $_POST['prodi']);
		if($mk) :
			foreach($mk as $key){
				$isi = $key->keterangan . ' - ' . $key->kelas_id;
				echo "<option value='".$isi.'|'.$key->mkditawarkan_id.'|'.$key->kelas_id."'>".$isi."</option>";
			}
		else :
			echo "<option value=''>Mata kuliah tidak ditemukan</option>";
		endif;
	}
	
	function get_mhs(){
		if(!empty($_POST['mk']) && !empty($_POST['prodi']) && !empty($_POST['tahun'])){
			$mkelompok = new model_kelompok();	
			$prodi = $_POST['prodi'];
			$mk = explode("|", $_POST['mk']);
			
			// $mk[1] = mkd
			// $mk[2] = kelas
			$data['mhs'] = $mkelompok->get_mhs($prodi, $mk[1], $mk[2]);
			$this->view('kelompok/mhs.php', $data);
		}
	}
	
	function set_kelompok(){
		if(!empty($_POST['mhs']) && !empty($_POST['kelompok'])){
			$mkelompok = new model_kelompok();	
			
			$mhs = $_POST['mhs'];
			$kel= explode("|", $_POST['kelompok']);
			
			echo $mkelompok->set_kelompok($_POST['mhs'], $kel[0]);
		}
		
	}
	
	
}