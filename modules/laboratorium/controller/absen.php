<?php
class laboratorium_absen extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		
		$coms->require_auth('auth'); 
	}
	
	function rekap(){
		if(isset($_POST['cmbjenis']) && ($_POST['cmbjenis']!='-')){
			$this->detail_rekap($_POST['cmbjenis']);
			exit();
		}
		
		$data['absen'] = "";
		
		$this->add_script('js/absen.js');	
		$this->view( 'absen/rekap.php',$data );
	}
	
	function data_rekap(){
		$sid = $_POST['cmbsemester'];
		$mk	 = $_POST['cmbmk'];
		$dosen = $_POST['cmbdosen'];
						
		$mabsen = new model_absen();	
		
		$data['jadwal']		= $mabsen->get_jadwal_mk($mk, $dosen, $sid);	
		$data['pertemuan']	= $mabsen->get_jumlah_pertemuan($sid, $mk);
					
		$this->view( 'absen/data_rekap_dosen.php', $data );
	}
	
	function data_rekap_mhs(){
		$sid = $_POST['cmbsemester'];
		$mk	 = $_POST['cmbmk'];
		$dosen = $_POST['cmbdosen'];
		$prodi = $_POST['cmbprodi'];
		$kelas = $_POST['cmbkelas'];
						
		$mabsen = new model_absen();	
		
		if($kelas!='0'){
			$data['mhs']		= $mabsen->get_mhs_mk($mk, $prodi, $kelas, $dosen, $sid);	
			$data['pertemuan']	= $mabsen->get_jumlah_pertemuan($sid, $mk);
		}else{
			$data['mhs']	= "";
			$data['pertemuan'] = 0;
		}
					
		$this->view( 'absen/data_rekap_mhs.php', $data );
	}
	
	
	function detail_rekap(){
		$mabsen = new model_absen();	
		$mconf	= new  model_confinfo();
		
		if(isset($_POST['cmbjenis'])){
			$type 	= $_POST['cmbjenis'];
		}else{
			$type	= "";
		}
		
		$data['type']	= $type;								
		$data['semester']= $mconf->get_semester();
		
		$staffid = $this->coms->authenticatedUser->staffid;
		$role = $this->coms->authenticatedUser->role;
		
		if(isset($_POST['cmbsemester'])){
			$sid = $_POST['cmbsemester'];
		}else{
			$sid = "";
		}		
						
		if(isset($_POST['cmbmk'])){
			$mk 			 = $_POST['cmbmk'];			
			$data['pertemuan']= $mabsen->get_jumlah_pertemuan($sid, $mk);
		}else{
			$mk				 = "";
			$data['pengampu']= "";
			$data['pertemuan']= '18';
		}
				
		$data['sid']		= $sid;
		
		if($type=='mhs'){
			$this->view( 'absen/rekap_mhs.php', $data );
		}else{
			$this->view( 'absen/rekap_dosen.php', $data );
		}
	}
		
	function  index($pid=NULL, $id=NULL){	
				
		$mabsen = new model_absen();	
		$mconf	= new  model_confinfo();
				
	/*	$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');		*/
		$this->coms->add_style('js/info/datepicker/css/bootstrap-datetimepicker.min.css');
		$this->coms->add_script('js/info/datepicker/js/jquery-1.10.2.min.js');	
		$this->coms->add_script('js/info/datepicker/js/moment-2.4.0.js');	
		$this->coms->add_script('js/info/datepicker/js/bootstrap-datetimepicker.min.js');	
		
		
		$staffid = $this->coms->authenticatedUser->staffid;
		$role = $this->coms->authenticatedUser->role;
		
		$data['semester']	= $mconf->get_semester();
		
		if(isset($_POST['cmbsemester'])){
			$data['sid'] = $_POST['cmbsemester'];
		}else{
			$data['sid'] = "";
		}	
		
		if(isset($_POST['cmbbulan'])){
			$data['month'] = $_POST['cmbbulan'];
		}else{
			$data['month'] = date("m");
		}	

		if(isset($_POST['cmbyear'])){
			$data['year'] = $_POST['cmbyear'];
		}else{
			$data['year'] = date("Y");
		}			
				
		$data['pertemuan']="18";		
		$data['absen']	= "";
				
		$this->add_script('js/absen.js');	
		$this->view( 'absen/absen_calendar.php', $data );
	}
	
	function view_calendar(){
		if(isset($_POST['cmbbulan'])){
			$data['month'] = $_POST['cmbbulan'];
		}else{
			$data['month'] = date("m");
		}	

		if(isset($_POST['cmbyear'])){
			$data['year'] = $_POST['cmbyear'];
		}else{
			$data['year'] = date("Y");
		}	
		
		$this->view( 'absen/calendar.php', $data );		
	}
	
	function get_data_calendar($list_day=NULL, $month=NULL, $year=NULL, $running_day=NULL){
		$mabsen = new model_absen();
		
		$staffid = $this->coms->authenticatedUser->staffid;
		$role = $this->coms->authenticatedUser->role;
		
		
		if($role=='mahasiswa'){
			$data = $mabsen->getEvent($list_day, $month, $year, $running_day);
		}else{
			$data = $mabsen->getEvent($list_day, $month, $year, $running_day);
		}
				 
		if(count($data)>0){
		$i=0;
		 foreach($data as $row):
			$i++;
			switch (strToLower($row->prodi_id)){
			
				case 'siskom':
					$sclass = "info";
					$tclass = 'text-putih';
				break;
				case 'si':
					$sclass = "uas";
					$tclass = 'text-putih';
				break;
				case 'ilkom':
					$sclass = "seminar";
					$tclass = 'text-putih';
				break;
				default:
					$sclass = "info";
					$tclass = 'text-putih';
				break;
				
			
			}
			$note = $mabsen->potong_kalimat($row->judul,5);
						
			echo '<div class="alert-agenda alert-agenda-'.$sclass.' popup-form';
			if($i>1)echo ' hide-it" style="display:none';
			echo '" data-cal="'.$i.'" data-popuptype="1"   data-calendaryear="'.$year.'" data-calendarmonth="'.$month.'" data-calendarday="'.($running_day).'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<a href=#><small><span class="'.$tclass.'"><i class="fa fa-clock-o"></i>&nbsp;'.$row->jam_mulai.', '.$note.'</span></small></a></div>';
			
		 endforeach;
		 if($i>1)echo '<div style="text-align:left"><small><a href="javascript::" class="show-calendar-it" data-mincal="0" data-maxcal="'.$i.'" class="text text-info"><span class="fa fa-angle-down"></span> More</a></small></div>';
		}
		//return $str;	
	}
	

	
	function get_frm_absen(){
		$sid = $_POST['cmbsemester'];
		$mk	 = $_POST['cmbmk'];
		$dosen = $_POST['cmbdosen'];
		$prodi = $_POST['cmbprodi'];
		$kelas = $_POST['cmbkelas'];
		$sesi  = $_POST['cmbsesi'];
						
		$mabsen = new model_absen();	
		
		$data['absen'] 		= $mabsen->get_absen($mk, $prodi, $kelas, $sesi);
		$data['aasisten'] 	= $mabsen->get_absen_asisten($mk, $prodi, $kelas, $sesi);
		$data['asisten'] 	= $mabsen->get_asisten($sid, $mk);
		$data['sesi']		= $sesi;
		$data['pertemuan']	="18";
			
		$this->view( 'absen/form_materi.php', $data );
	}
	
	function get_hadir(){
		$sid = $_POST['cmbsemester'];
		$mk	 = $_POST['cmbmk'];
		$dosen = $_POST['cmbdosen'];
		$prodi = $_POST['cmbprodi'];
		$kelas = $_POST['cmbkelas'];
		$sesi  = $_POST['cmbsesi'];
						
		$mabsen = new model_absen();	
		
		$data['mhs']	= $mabsen->get_mhs_mk($mk, $prodi, $kelas, $dosen, $sid, $sesi);	
		
		$data['sesi']	= $sesi;
			
		$this->view( 'absen/daftar_hadir.php', $data );
	}
	
	
	function get_absen_by_mk(){
		$staffid = $this->coms->authenticatedUser->staffid;
		$sid = $_POST['cmbsemester'];
		
		$mabsen = new model_absen();	
		
		if($sid){
			if(strtolower($role)=='dosen'){
				$mk = $mabsen->get_mk(1,$sid, $staffid);
			}else{
				$mk = $mabsen->get_mk(1,$sid);
			}
		}else{
			$mk	= "";
		}
		echo "<option value='0'>Please Select...</option>";
		foreach($mk as $dt):
			echo "<option value='".$dt->mkditawarkan_id."' ";
			
			echo ">".$dt->kode_mk." - ".$dt->mk."</option>";							
		endforeach;
							
	}
	
	function get_absen_by_dosen(){
		$staffid = $this->coms->authenticatedUser->staffid;
		$sid = $_POST['cmbsemester'];
		
		$mabsen = new model_absen();	
		
		if(isset($_POST['cmbmk'])){
			$mk 			 = $_POST['cmbmk'];
			
			if(strtolower($role)=='dosen'){
				$pengampu= $mabsen->get_dosen_mk($sid, $mk,"", $staffid);
			}else{
				$pengampu= $mabsen->get_dosen_mk($sid, $mk);
			}				
			
			$data['pertemuan']= $mabsen->get_jumlah_pertemuan($sid, $mk);
		}else{
			$pengampu	= "";
			$data['pertemuan'] = '18';
		}
		
		echo "<option value='0'>Please Select...</option>";
		if($pengampu){
			foreach ($pengampu as $dt):
				echo "<option  value='".$dt->dosen_id."'  ";
				
				echo ">".$dt->nama."</option>";
			endforeach;	
		}		
	}
	
	function get_absen_by_prodi(){
		$staffid = $this->coms->authenticatedUser->staffid;
		$sid 	= $_POST['cmbsemester'];
		$mk  	= $_POST['cmbmk'];
		$dosen 	= $_POST['cmbdosen'];
		
		$mabsen = new model_absen();	
			
		$prodi	= $mabsen->get_prodi_mk($mk, $dosen, $sid);
		echo "<option value='0'>Please Select...</option>";
		if($prodi){
			foreach ($prodi as $dt):
				echo "<option  value='".$dt->prodi_id."'  ";				
				echo ">".$dt->prodi."</option>";
			endforeach;	
		}		
		
	}
	
	function get_absen_by_kelas(){
		$staffid = $this->coms->authenticatedUser->staffid;
		$sid 	= $_POST['cmbsemester'];
		$mk  	= $_POST['cmbmk'];
		$dosen 	= $_POST['cmbdosen'];
		$prodi 	= $_POST['cmbprodi'];
		
		$mabsen = new model_absen();	
			
		$kelas	= $mabsen->get_kelas_mk($mk,$dosen,$sid,$prodi);
	
		echo "<option value='0'>Please Select...</option>";
		if($kelas){
			foreach ($kelas as $dt):
				echo "<option  value='".$dt->kelas_id."'  ";				
				echo ">".$dt->kelas_id."</option>";
			endforeach;	
		}		
	}
	
	function get_absen_by_sesi(){
		$staffid = $this->coms->authenticatedUser->staffid;
		$sid 	= $_POST['cmbsemester'];
		$mk  	= $_POST['cmbmk'];
		$dosen 	= $_POST['cmbdosen'];
		$prodi 	= $_POST['cmbprodi'];
		$kelas 	= $_POST['cmbkelas'];
		
		$mabsen = new model_absen();	
			
		$kelas	= $mabsen->get_kelas_mk($mk,$dosen,$sid,$prodi);
	
		echo "<option value='0'>Please Select...</option>";
		for($i=1;$i<15;$i++){
			echo "<option value='".$i."' ";			
			echo " >".$i."</option>";
		}
	}
	
		
	function vw($pid=NULL, $id=NULL){
		if($id=='none'){
			$this->index();
		}else{
			$this->index($pid, $id);
		}
	}
	
	function write(){
		$mconf = new model_confinfo();	
		
		$data['absen']		= "";
		$data['user'] 		= $this->coms->authenticatedUser->username;;
		$data['semester']	= $mconf->get_semester_aktif();
		$data['mk']			= "";
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/token-input.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/jquery.tokeninput.js');
		$this->add_script('js/jsFunction.js');
		
		$this->view('absen/write.php', $data);
	}
	
	function edit($id){
		if( !$id ) {
			$this->redirect('module/akademik/absen');
			exit;
		}
		
		$mabsen = new model_absen();	
				
		$data['user'] 	= $this->coms->authenticatedUser->username;;			
		$data['absen'] 	= $mabsen->absen($keyword = NULL, $page = 1, $perpage = 1, $id, "");	
									
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/token-input.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/jquery.tokeninput.js');
		$this->add_script('js/jsFunction.js');		
		
		$this->view('absen/edit.php', $data);
	}
		
	
	function save(){
		$mabsen = new model_absen();
		
		$mhs 	= $_POST['chkmhs'];
		//$jadwal	= $_POST['cmbjadwal'];
		//$dosen	= $_POST['cmbdosen'];
		$total	= $_POST['totalpertemuan'];
		$sesi	= $_POST['cmbsesi'];
		$materi	= $_POST['materi'];
		$sid 	= $_POST['cmbsemester'];
		$mk  	= $_POST['cmbmk'];
		$dosen 	= $_POST['cmbdosen'];
		$prodi 	= $_POST['cmbprodi'];
		$kelas 	= $_POST['cmbkelas'];
		
		$tgl	= date("Y-m-d", strtotime($_POST['tgl']));
		$jam	= date("H:i", strtotime($_POST['tgl']));
		$jmljam	= $_POST['jumlahjam'];
		$hadir	= $_POST['cmbhadir'];		
		
		
		$userid	= $this->coms->authenticatedUser->username;;
		$lastupdate = date("Y-m-d H:i");		
		
		$kode 	= $mabsen->get_reg_number($mk,$prodi,$kelas, $sesi);
						
		for($i=0;$i<count($mhs);$i++){
			$id 	= $mabsen->add_nol($i);
			
			$mhsid	= $kode.$id;
			
			$datanya1= array('absenmhs_id'=>$mhsid, 'mahasiswa_id'=>$mhs[$i], 'absen_id'=>$kode, 'is_hadir'=>$hadir[$i],  'last_update'=>$lastupdate);
			
			$mabsen->replace_absen_mhs($datanya1);				
		}
		
		$datanya 	= array('absen_id'=>$kode, 'mkditawarkan_id'=>$mk, 'prodi_id'=>$prodi, 'kelas_id'=>$kelas, 'tgl'=>$tgl, 'jam_masuk'=>$jam, 'materi'=>$materi, 'sesi_ke'=>$sesi,
						'jumlah_jam'=>$jmljam, 'total_pertemuan'=>$total, 'last_update'=>$lastupdate, 'user'=>$userid);
		$datadosen 	= array('absendosen_id'=>$kode, 'absen_id'=>$kode, 'dosen_id'=>$dosen, 'is_hadir'=>1, 'user'=>$userid, 'last_update'=>$lastupdate);
		
		$save = $mabsen->replace_absen($datanya, $datadosen);
				
		if( $save ) {
			$result['status'] = "OK";
			$result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
		} else {
			$result['status'] = "NOK";
			$result['error'] = $mabsen->error;
		}
		
		echo json_encode($result);
	}
	
	function delete(){
		
		$id 	 = $_POST['id'];
		
		$mabsen = new model_absen();	
		$mconf	 = new model_confinfo();
		
		$user	 = $this->coms->authenticatedUser->username;
		
		ob_start();
		
		$result = array();
		
		$datanya=array('absen_id'=>$id);				
		
		if($mabsen->delete_absen($datanya)){
			$mconf->log("dbptiik_akademik.tbl_absenmk", $datanya, $user, 'delete');
			
			$result['status'] = "OK";
		}else {
			$result['status'] = "NOK";
			$result['error'] = $mskripsi->error;
		}
		echo json_encode($result);
	}
	
	
	function dosen(){
	
		if(isset($_POST['dosenid'])){
			$id	= $_POST['dosenid'];
		}else{
			$id	= "";
		}
				
		$mconf 	= new model_confinfo();	
		$mabsen= new model_absen();	
			
		
		$data['absen']		= $id;
		$data['dosen']		= $mconf->get_nama_dosen($id);
		$data['user'] 		= $this->coms->authenticatedUser->username;;
		$data['semester']	= $mconf->get_semester_aktif();
		$data['prodi']		= $mconf->get_prodi("");
		$data['mk']			= $mabsen->get_mk_diampu($id);
				
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/token-input.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/jquery.tokeninput.js');
		$this->add_script('js/jsFunction.js');
		
		$this->view('absen/write.php', $data);
	
	}

}
?>