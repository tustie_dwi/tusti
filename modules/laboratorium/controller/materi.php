<?php
class laboratorium_materi extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($materi=NULL, $mk=NULL){
		$mmateri = new model_materi();
		$mlaboratorium = new model_laboratorium();	
		$mconf	  	   = new model_confinfo();
		
		$thn					= $mconf->get_semester_aktif()->tahun_akademik;
		$data['mk']				= $mmateri->get_mk_jadwal("",$thn);
		$data['posts']			= $mmateri->get_praktikum_materi();
		$data['thn_akademik']	= $mconf->get_semester_aktif();
		if($materi){
			$data['materi_edit']	= $mmateri->get_praktikum_materi("","","",$materi);
			$data['materi_praktikum']	= $mmateri->get_praktikum_materi($mk,"","","",$materi);
		}
		else{
			$data['materi_praktikum']	= $mmateri->get_praktikum_materi();
		}
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');		
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->add_script('js/materi/materi.js');
		$this->view('materi/index.php', $data);
	}
	
	function get_submateri_index(){
		$mkid = $_POST['mk'];
		$mmateri = new model_materi();
		$sub = $mmateri->get_praktikum_materi($mkid);
		if(isset($sub)){
			$data['posts'] = $sub;
			//echo "<option value='".$dt->m_id."'>".$dt->judul."</option>";
			$this->view('materi/viewselection.php',$data);
		}
		else{ ?>
			<div align="center" style="margin-top:20px;">
			    <div class="well">Sorry, no content to show</div>
			</div> <?php
		}
	}
	
	function get_child_materi($urut=NULL, $mkid=NULL, $spasi=NULL, $materi_id=NULL){
		$mmateri = new model_materi();
		$sub = $mmateri->get_praktikum_materi($mkid,"",$materi_id);
		if(isset($sub)){
			$spasi+=1;
			$tab = (15*$spasi)."px";
			$indeks_urut=1;
			foreach($sub as $dt){
				$urut_child = $urut.".".$indeks_urut; ?>
				<tr>
					<td>
						<ul class='nav nav-pills' style='margin:0;margin-left: <?php echo $tab ?>'>
							<li class='dropdown'>
							  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'><?php	echo $urut_child.". ".ucfirst($dt->judul);?> </a>
							    <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
							    	<li>
								  		<a href="<?php echo $this->location('module/laboratorium/materi/index/'.$dt->m_id.'/'.$dt->mk_id) ?>"><i class='fa fa-pencil'></i> Edit</a>
							  	  	</li>
						  	    </ul>
							</li>
						</ul>
					</td>
				</tr>
				<?php
				$indeks_urut++;
				$urut_grchild = $urut.".".($indeks_urut-1);
				echo $this->get_child_materi($urut_grchild, $dt->mk_id,$spasi, $dt->materi_id);
			}
		}
	}
	
	function get_submateri(){
		$mconf	  	    = new model_confinfo();
		$mmateri 		= new model_materi();
		
		$thn		 = $mconf->get_semester_aktif()->tahun_akademik;
		if(isset($_POST['jadwal'])){
			$jadwalid 	 = $_POST['jadwal'];
			$data 		 = $mmateri->get_mk_jadwal("",$thn,"","",$jadwalid);
			$parameter 	 = $data->mk_id;
		}
		else{
			$parameter 	 = $_POST['mk'];
		}
		
		
		if(isset($parameter)){
			$sub = $mmateri->get_praktikum_materi($parameter);
			echo "<option value='0'>Silahkan Pilih</option>";
			if(isset($sub)){
				foreach($sub as $dt){
					echo "<option value='".$dt->m_id."'>".ucfirst($dt->judul)."</option>";
				}
			}
		}
		else{
			echo "<option value='0'>Silahkan Pilih</option>";
		}
	}
	
	function save_materi(){
		if(isset($_POST['judul'])){
			$this->save_toDB();
			exit();
		}else{
			echo "Form yang anda submit belum lengkap!";
			$this->index();
			exit();
		}
	}
	
	function save_toDB(){
		$mmateri = new model_materi();

		$data = $mmateri->get_mk_jadwal($_POST['mk'], $_POST['thnid']);
		if(!isset($_POST['hidId']) || $_POST['hidId']==""){
			$materi_id		= $mmateri->get_materi_id();
		}
		else{
			$materi_id		= $_POST['hidId'];
		}
		//$jadwal_id		= $data->jadwal_id;
		$judul 				= $_POST['judul'];
		if($_POST['submateri']!='0'){
			$parent_id		= $mmateri->get_materi_by_md5($_POST['submateri'])->materi_id;
		}
		else{
			$parent_id		= 0;
		}
		$mk_id				= $data->mkditawarkan_id;
		$nama_mk			= $data->nama_mk;
		$kodemk				= $data->kode_mk;
		$thn_akademik		= $_POST['thnid'];
		//echo $materi_id."\n".$judul."\n".$parent_id."\n". $mk_id."\n".$nama_mk."\n".$kodemk."\n".$thn_akademik;
		
		$datanya = Array ('materi_id'=> $materi_id,
						  'judul'=> $judul,
						  'parent_id'=> $parent_id,
						  'mkditawarkan_id'=> $mk_id,
						  'nama_mk'=> $nama_mk,
						  'kode_mk'=> $kodemk,
						  'tahun_akademik'=> $thn_akademik
						);
		//var_dump($datanya);
		$mmateri->replace_materi($datanya);	
	    echo "OK, data telah diupdate.";
	    exit();
	}
}
?>