<?php
class laboratorium_conf extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function asisten($id=NULL){	
		
		$mlaboratorium = new model_laboratorium();	
		
		$data['semester']    = $mlaboratorium->get_semester();
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		$role		 	= $this->coms->authenticatedUser->role;
		if($role!="administrator" && $role!="psik"){
			$data['unit_login'] 	= $this->coms->authenticatedUser->unit;
		}
		$data['lab']	     = $mlaboratorium->get_unit_by_fakultas($fakultas_id);
		
		$takademik 			 = $mlaboratorium->get_semester_aktif();
		$takademik 			 = $takademik->tahun_akademik;
		$data['kalender']    = $mlaboratorium->get_kalender_akademik($takademik,'kuliah');
		
		if(!$id){
			$data['type']	     = "write";
			$data['takademik']   = $mlaboratorium->get_semester_aktif();
			$data['mkpraktikum'] = $mlaboratorium->get_mk_praktikum($takademik);
		}else{
			$data['type']	     = "edit";
			$data['edit'] = $mlaboratorium->read_asisten($id);
			
			$edit = $mlaboratorium->read_asisten($id);
			$takademik = $edit[0]->tahun_akademik;
			$data['takademik']   = $takademik;
			$data['mkpraktikum'] = $mlaboratorium->get_mk_praktikum($takademik);
		}
		
		
		/* data read asisten */
		$data['posts'] = $mlaboratorium->read_asisten();
				
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');		
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');

		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
		
		$this->coms->add_script('js/jquery.tokeninput.js');
		
		$this->add_script('js/asisten.js');
		
		$this->view('asisten/index.php', $data);
	}
	
	function get_mk_by_thn(){
		$mlaboratorium 	= new model_laboratorium();
		
		if(isset($_POST['thnid'])&&$_POST['thnid']!=''){
			$takademik   	= $_POST['thnid'];
		}else $takademik = NULL;
		
		if(isset($_POST['unitid'])&&$_POST['unitid']!=''){
			$unit   	= $_POST['unitid'];
		}else $unit = NULL;
		
		$mkpraktikum 	= $mlaboratorium->get_mk_praktikum($takademik,$unit);
		echo "<option value='0'>Select Mata Kuliah</option>" ;
		if($mkpraktikum){
			foreach($mkpraktikum as $mk )
			{
				echo "<option value='".$mk->mkid."'>"." [".ucwords($mk->kode_mk)."] ".ucWords($mk->nama_mk)."</option>" ;	
			}	
		}
	}
	
	function save(){
		$mlaboratorium 	= new model_laboratorium();
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=""){
			$asistenid = $_POST['hidId'];
		}else{
			$asistenid = $mlaboratorium->get_asistenId();
		}
		
		$unit = $_POST['cmbunit'];
		$takademik = $_POST['cmbtakademik'];
		$tglmulai = $_POST['cmbtglmulai'];
		$tglselesai = $_POST['cmbtglselesai'];
		$mhsid = $_POST['cmbmhsid'];
		$mkid = $_POST['cmbmk'];
		$ket = $_POST['cmbketerangan'];
		
		if(isset($_POST['cmbisaktif'])){
			$isaktif = 1;
		}else $isaktif = 0;
		
		$datanya 	= Array(
							'asisten_id'=>$asistenid, 
							'mahasiswa_id'=>$mhsid, 
							'unit_id'=>$unit, 
							'tahun_akademik'=>$takademik,
							'mkditawarkan_id'=>$mkid,
							'tgl_mulai'=>$tglmulai,
							'tgl_selesai'=>$tglselesai,
							'is_aktif'=>$isaktif,
							'keterangan'=>$ket
							);
		$mlaboratorium->replace_asisten($datanya);
		
		echo "Proses Simpan Berhasil!";
		exit();
		
	}
		
	function mhs(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mlaboratorium 	= new model_laboratorium();
		$data	= $mlaboratorium->get_mahasiswa($str);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}	
	
	//SETTING MK//
	
	function mk($unit=NULL,$mk=NULL){
		$mlaboratorium 	= new model_laboratorium();
		
		$fakultas_id 	= $this->coms->authenticatedUser->fakultas;
		$role		 	= $this->coms->authenticatedUser->role;
		if($role!="administrator" && $role!="psik"){
			$data['unit_login'] 	= $this->coms->authenticatedUser->unit;
		}
		
		$data['unit']	= $mlaboratorium->get_unit_by_fakultas($fakultas_id);
		$data['mk']		= $mlaboratorium->get_matakuliah();
		
		if($unit && $mk){
			$data['edit']		= $mlaboratorium->read_setting_mk($unit,$mk);
		}
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');		
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		
		$this->coms->add_script('js/select2/select2.js');
		$this->coms->add_style('js/select2/select2.css');	
		
		$this->add_script('js/mk/mk.js');
		
		$this->view('mk/index.php', $data);
	}
	
	function get_settingmk(){
		$unit = $_POST['unit'];
		$mlaboratorium 	= new model_laboratorium();
		$settingmk = $mlaboratorium->read_setting_mk($unit);
		if(isset($settingmk)){
			$data['posts'] = $settingmk;
			$this->view('mk/viewselection.php',$data);
		}
		else{ ?>
			<div class="col-sm-12" align="center" style="margin-top:20px;">
			    <div class="well">Maaf, data tidak ditemukan</div>
			</div> <?php
		}
	}
	
	function save_settingmk(){
		if(isset($_POST['unit'])&&isset($_POST['mk'])){
			$this->save_settingmktoDB();
			exit();
		}else{
			echo "Form yang anda submit belum lengkap!";
			$this->mk();
			exit();
		}
	}
	
	function save_settingmktoDB(){
		$mlaboratorium 	= new model_laboratorium();
		$unit	= $_POST['unit'];
		$mk		= $_POST['mk'];
		$count	= count($mk);
		// echo $count;
		for($i=0;$i<$count;$i++){
			$datanya = Array ('unit_id'=> $unit,
							  'matakuliah_id'=> $mk[$i]
							);
			$cek = $mlaboratorium->cek_data($mk[$i],$unit);
			if(!$cek){
				$mlaboratorium->replace_praktikum_lab($datanya);
			}
		}	
	    echo "OK, data telah diupdate.";
	    exit();
	}

	function delete_settingmk(){
		$mlaboratorium 	= new model_laboratorium();
		$unit = $_POST['unit'];
		$mk	  = $_POST['mk'];
		$del  = $mlaboratorium->delete_settingmk($unit,$mk);
		if($del==TRUE){
			echo "berhasil";
		}
	}
}
?>