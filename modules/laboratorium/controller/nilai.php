<?php
class laboratorium_nilai extends comsmodule {

	private $coms;

	function __construct($coms){
		parent::__construct($coms);
		$this->coms = $coms;

		$coms->require_auth('auth'); 
	}
	
	function index($str=NULL){
		if($this->coms->authenticatedUser->role =='mahasiswa') $this->coms->no_privileges();
		
		$mmateri 		= new model_materi();
		$mlaboratorium  = new model_laboratorium();
		$mconf	  	    = new model_confinfo();
		$mnilai			= new model_nilai();
		$user 			= $this->coms->authenticatedUser->role;
		$staff			= $this->coms->authenticatedUser->staffid;
		$level 			= $this->coms->authenticatedUser->level;
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;

		switch($str){
		  case 'list':		
			if(isset($_POST['nilai'])){
				$nilaiid = $_POST['nilai'];
			}else $nilaiid = '';  
			
			if(isset($_POST['prodi'])){
				$prodiid = $_POST['prodi'];
			}else $prodiid = '';
			
			if(isset($_POST['isproses'])){
				$isproses = $_POST['isproses'];
			}else $isproses = '';

			$data['isproses'] = $isproses;
			
			// echo $nilaiid." || ".$prodiid;
			
			$datanilai = $mnilai->get_penilaian('','list','',$nilaiid);
			// var_dump($datanilai);
			if(isset($datanilai->materi_id)){
				$materiid = $datanilai->materi_id;
			}else $materiid='';
			
			if(isset($datanilai->jadwal_id)){
				$jadwalid = $datanilai->jadwal_id;
			}else $jadwalid='';
			
			if(isset($datanilai->mkditawarkan_id)){
				$mkid = $datanilai->mkditawarkan_id;
			}else $mkid='';
			
			// var_dump($nilaiid) ;
			
			// echo $materiid . " " . $jadwalid." ".$prodiid." ".$mkid."<br>";
			// $data['mhs']	 = $mnilai->get_mahasiswa_praktikum($prodiid,$mkid,$jadwalid);
			$data['header']   = $mnilai->get_nilaiId_md5($nilaiid, 'byidmd5')->judul;
			$data['mhs'] 	  = $mnilai->get_data_nilai_mhs($nilaiid,'','bymhs');
			
			$data['komponen']   = $mnilai->read_komponen('','','','','list',$jadwalid,$materiid,'','','',$nilaiid,'md5');
			//$data['komponen']   = $mnilai->read_komponen('','','','','list','','','','','',$nilaiid,'md5');
			$data['komp_akhir'] = $mnilai->read_komponen($jadwalid,'','','','komponen','','','','akhir');			
			//$data['komp_akhir']   = $mnilai->read_komponen('','','','','list',$jadwalid,'','','','',$nilaiid,'md5');
			
			$data['nilai_id'] = $nilaiid;
			$data['thn'] 	  = $thn;
			$data['index']    = 'list/view_list.php';
			$data['param']    = 'list';
			$data['nilai']	  = $mnilai->get_penilaian('','proses','','');
			$this->add_script('js/nilai/proses/proses.js');
		  break;
		  case 'komponen':
			if(isset($_POST['nilai'])){
				
				$nilaiid = $_POST['nilai'];
				
				if(isset($_POST['prodi'])){
					$prodiid = $_POST['prodi'];
				}else $prodiid = '';
				$data['prodiid']=$prodiid;
				
				$datafromnilai = $mnilai->get_nilaiId_md5($nilaiid,'byidmd5');
				$jadwalid = $datafromnilai->jadwalid;
				$materiid = $datafromnilai->materi_id;
				$mkid 	  = $datafromnilai->mkditawarkan_id;
				
				$data['mtrid'] = $datafromnilai->materiid;
				$data['nilai']= $nilaiid;
				
			}else{
				if(isset($_POST['mk_index'])){
					$jadwalid = $_POST['mk_index'];
				}else $jadwalid = '';
				
				if(isset($_POST['cmb-prodi-index'])){
					$prodiid = $_POST['cmb-prodi-index'];
				}else $prodiid = '';
				
			}
			
			$data['jadwalid'] = $jadwalid;
			$data['prodiid']  = $prodiid;
			
			if($jadwalid!=''){
					$data['materi'] = $mnilai->read_komponen_idx($jadwalid);
			}
			
			// if($jadwalid!=''){
				// if(isset($materiid)&&$materiid!='-'){
					// $data['materi'] = $mnilai->read_materi($mkid,$thn,'mkid','jadwal',$jadwalid,$level);
				// }else{
					// $data['materi'] = $mnilai->read_komponen($jadwalid,'','','','komponen','','-','view-mk');
				// }
			// }
			
			$data['thn'] = $thn;
			$data['nav'] = 'komponen/komponen.php';
			$data['index'] = 'komponen/view_komponen.php';
			$data['header'] = 'Komponen';
			$data['param'] = 'komponen';
			$data['headerpanel'] = 'New Komponen';
			$this->add_script('js/nilai/komponen/komponen.js');
		  break;
		  default:
			$data['nav'] = 'penilaian/write.php';
		 	$data['index'] = 'penilaian/view_penilaian.php';
			$data['header'] = 'Daftar Penilaian Praktikum';
			$data['param'] = 'nilai';
		  break;
		}
		$data['prodi']			= $mnilai->get_prodi();
		$data['thn_akademik']	= $mconf->get_semester_aktif();
		
		
		if(($this->coms->authenticatedUser->role =='administrator') || ($this->coms->authenticatedUser->role =='psik')){
			$data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai");
			//$this->coms->no_privileges();
			// $data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai",'','',$unit);
		}else{
			$unit = $this->get_unit_sess();
			// $data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai");
			$data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai",'','',$unit);
		} 
		
		$data['role']			= $user;
		$data['userid']			= $staff;
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('bootstrap/css/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->add_script('js/nilai/nilai.js');
		$this->view('nilai/index.php',$data);
	}
	
	function get_unit_sess(){
		$unit_ = $this->coms->authenticatedUser->unit;
		$unit = '';$z=0;
		foreach ($unit_ as $u) {
			if($z==0){
				$unit = "'".$u."'";
			}else{
				$unit = $unit.",'".$u."'";
			}
			$z++;
		}
		return $unit;
	}
	
	function display_data_komponen($komponen_id,$nilai_id,$materi_id,$jadwal_id,$kategori,$prodiid){
		$mnilai			= new model_nilai();
		$mconf	  	    = new model_confinfo();
		$level 			= $this->coms->authenticatedUser->level;
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;
		
		$mkid = $mnilai->get_namamk($jadwal_id,$prodiid)->mkditawarkan_id;
		$mkmd5 = $mnilai->get_namamk($jadwal_id,$prodiid)->mkid;
		// echo $nilai_id;
		if(isset($mnilai->get_nilaiId_md5($nilai_id,'byid')->nilaiid)){
			$nilaimd5 = $mnilai->get_nilaiId_md5($nilai_id,'byid')->nilaiid;
			
			if($kategori=='materi'&&$materi_id!='-'){
				// echo '<a href="'.$this->location('module/laboratorium/nilai/edit/komponen/'.$materi_id."/".$jadwal_id).'" class="btn btn-info"><i class="fa fa-edit"></i> edit</a>';
				echo '<a href="javascript::" data-materi="'.$materi_id.'" data-jadwal="'.$jadwal_id.'" data-prodi="'.$prodiid.'" data-nilai="'.$nilaimd5.'" class="btn btn-info edit-komp-materi"><i class="fa fa-edit"></i> edit</a>';
				$data = $mnilai->read_materi($materi_id,$thn,'materiid','jadwal',$jadwal_id,$level);
				echo "<li>";
				echo "<span>".$data->judul."</span>";
				echo "<ul>";
				echo $this->get_komponen_parent($mkmd5, $thn, $data->materi_id,$jadwal_id); 
				echo "</ul>";
				echo "</li>";
			}else{
				// echo '<a href="'.$this->location('module/laboratorium/nilai/edit/komponen/'.$jadwal_id).'" class="btn btn-info"><i class="fa fa-edit"></i> edit</a>';
				echo '<a href="javascript::" data-jadwal="'.$jadwal_id.'" data-prodi="'.$prodiid.'" data-nilai="'.$nilaimd5.'" class="btn btn-info edit-komp"><i class="fa fa-edit"></i> edit</a>';
				$data = $mnilai->read_komponen($jadwal_id,'','','','komponen','','-','view-mk','','',$nilaimd5,'md5');
				if($data):
				foreach ($data as $d) {
					echo "<li>";
					echo "<span>".$d->judul."</span>";
					echo "<ul>";
					echo $this->get_komponen_child($mkmd5, $thn, $d->komponen_id,'','view','view-mk');
					echo "</ul>";
					echo "</li>";
				}
				endif;
			}
		}
	}
	
	function get_nilai_mhs_komponen($nilaiid,$mhsid,$param=NULL,$isproses=NULL){
		$mnilai			= new model_nilai();
		$data['mhs'] 	= $mnilai->get_data_nilai_mhs($nilaiid,$mhsid,'');
		$data['nilaiid']= $nilaiid;
		$data['mhsid']	= $mhsid;
		$data['isproses']	= $isproses;
		if($param=='view'){
			$this->view('nilai/proses/list.php',$data);
		}else{
			$this->view('nilai/list/list.php',$data);
		}
	}
	
	function get_mhs_NA($nilaiid,$mhsid){
		$mnilai	= new model_nilai();
		$nilai 	= $mnilai->get_nilai_proses($nilaiid,$mhsid,'NA-mhs');
		echo substr($nilai[0]->nilai_akhir, 0,5);
	}
	
	function get_komponen_parent($mkid,$thn,$materiid,$jadwalid){
		$mnilai			= new model_nilai();
		$data['thn']	= $thn;
		$data['param'] 	= 'view';
		$data['child']	= $mnilai->read_komponen($mkid,$thn,'',$materiid,'parent',$jadwalid,'','parent-kom');
		$this->view('nilai/komponen/child.php',$data);
	}
	
	function get_komponen_child($mkid,$thn,$komponen,$i,$param,$param2){
		$mnilai			= new model_nilai();
		$data['thn'] 	= $thn;
		$data['param'] 	= $param;
		$data['i'] 		= $i;
		if($param2=='view-mk'){
			$data['child']	= $mnilai->read_komponen($mkid,'',$komponen,'','child','','',$param2);
		}else{
			$data['child']	= $mnilai->read_komponen($mkid,$thn,$komponen,'','child');
		}
		$this->view('nilai/komponen/child.php',$data);
	}
	
	function delete_komponen(){
		$mnilai		= new model_nilai();
		$komponen	= $_POST['komponenid'];
		$nilaiid	= $_POST['nilaiid'];
		$str		= $_POST['str'];
		$mnilai->delete_komponen($komponen,$str);
		$mnilai->delete_prktikum_nilai_mhs($komponen,$nilaiid);
	}
	
	function edit($str=NULL, $id=NULL, $id2=NULL){
		switch($str){
		  case 'komponen':
			$this->edit_komponen($id,$id2);
		  break;
		  case 'config':
			$this->edit_nilai($id,$id2);
		  break;
		}
	}
	
	function edit_komponen(){
		$mmateri 		= new model_materi();
		$mlaboratorium  = new model_laboratorium();
		$mconf	  	    = new model_confinfo();
		$mnilai			= new model_nilai();
		
		$user 			= $this->coms->authenticatedUser->role;
		$staff			= $this->coms->authenticatedUser->staffid;
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;
		
		$nilaiid = $_POST['nilai'];
		
		if(isset($_POST['materi'])){			
			$materiid  = $mnilai->get_materimd5($_POST['materi']);
			$jadwalid  = $_POST['jadwal'];
			
		
			$mkid 	   = $mnilai->get_jadwal_mk($jadwalid,$thn,'jadwal','inner');
			
			if($mkid!=null){
				// $materiid = $mkid->materi_id;
				$mkid 	  = $mkid->mkditawarkan_id;
			}
			$data['mtrid']	  = $_POST['materi'];
			$data['komponen'] = $mnilai->read_komponen($jadwalid,'','','','komponen','',$materiid,'view-mk','','',$nilaiid,'md5');

		}else{
			$jadwalid = $_POST['jadwal'];
			
			$data_jadwal = $mnilai->read_komponen($jadwalid,'','','','komponen','','-','view-mk','','',$nilaiid,'md5');
			
			$data['komponen'] = $data_jadwal;
		}
		
		if($jadwalid!=''){
			$data['materi'] = $mnilai->read_komponen_idx($jadwalid);
		}
		
		$data['jadwalid'] = $jadwalid;
		$data['prodiid']  = $_POST['prodi'];;
		$data['nilai']= $_POST['nilai'];
		
		$data['thn'] 			= $thn;
		$data['thn_akademik']	= $mconf->get_semester_aktif();
		$data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai");
		$data['role']			= $user;
		$data['userid']			= $staff;
		$data['prodi']			= $mnilai->get_prodi();
		
		$data['nav'] 	= 'komponen/komponen.php';
		$data['index'] 	= 'komponen/view_komponen.php';
		$data['header'] = 'Komponen';
		$data['param'] 	= 'komponen';
		$data['headerpanel'] = 'Edit Komponen';
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('bootstrap/css/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/nilai/komponen/komponen.js');
		// $this->add_script('js/nilai/nilai.js');
		$this->view('nilai/index.php',$data);
	}
	
	function edit_nilai($nilaiid=NULL,$materiid=NULL){
		//echo $nilaiid." ".$materiid;
		$mmateri 		= new model_materi();
		$mlaboratorium  = new model_laboratorium();
		$mconf	  	    = new model_confinfo();
		$mnilai			= new model_nilai();
		$user 			= $this->coms->authenticatedUser->role;
		$staff			= $this->coms->authenticatedUser->staffid;
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;
		
		if(isset($materiid)){
			$materi = $materiid;
		}
		else{
			$materi = "";
		}
		$nilai = $mnilai->get_penilaian("",$materi,"",$nilaiid);
		$data['edit_nilai']  		= $nilai;
		$data['materi_praktikum']	= $mmateri->get_praktikum_materi($nilai->mk_id);
		$data['nav'] 			= 'penilaian/write.php';
	 	$data['index'] 			= 'penilaian/view_penilaian.php';
		$data['header'] 		= 'Daftar Penilaian Praktikum';
		$data['param'] 			= 'nilai';
		
		$data['prodi']			= $mnilai->get_prodi();
		$data['thn_akademik']	= $mconf->get_semester_aktif();
		$data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai");
		$data['role']			= $user;
		$data['userid']			= $staff;
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('bootstrap/css/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');		
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->add_script('js/nilai/nilai.js');
		
		$this->view('nilai/index.php',$data);
	}
	
	function proses(){
		$jadwalid = $_POST['jadwal'];
		$data['prodid'] = $_POST['prodi'];
		$nilaiid_ = $_POST['nilai'];
		$data['nilai_'] = $nilaiid_;
		
		$mnilai			= new model_nilai();
		$mconf	  	    = new model_confinfo();
		$user 			= $this->coms->authenticatedUser->role;
		$staff			= $this->coms->authenticatedUser->staffid;
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;
		
		$data['role']	= $user;
		$data['userid']	= $staff;
		
		$mk = $mnilai->get_namamk($jadwalid,$_POST['prodi'])->nama_mk;
		$kelas = $mnilai->get_namamk($jadwalid,$_POST['prodi'])->kelas_id;
		$mkditawarkanid = $mnilai->get_namamk($jadwalid,$_POST['prodi'])->mkditawarkan_id;
		
		$namajadwal = $mk." ".$kelas;
		$data['header'] = "Proses Nilai ".$namajadwal;
		$data['namajadwal'] = $namajadwal;
		
		$data['thn'] 	= $thn;
		$data['index']  = 'proses/view_proses.php';
		$data['param']  = 'list';
		$data['jadwalhid'] = $jadwalid;
		
		$data['komp_akhir']= $mnilai->read_komponen($jadwalid,'','','','komponen','','','','akhir');
		$data['nilai_id_inf'] = $mnilai->read_nilai_inf($jadwalid,'akhir');
		
		$nilai = $mnilai->get_nilaiid_byjadwal($jadwalid);

		foreach ($nilai as $n) {
			$nilproses = $mnilai->get_nilai_proses($n->nilai_id,'','NA');
			if(isset($nilproses)){
				$komp[] = $nilproses;
			}
			$nilai_id[]= $n->nilaiid;
			
		}
		
		$data['nilai'] = $nilai_id;
		$divider = count($komp);
		$datanya = array();
		foreach ($komp as $kom) {
			foreach ($kom as $k) {
				if(!isset($datanya[$k->mahasiswa_id])) $datanya[$k->mahasiswa_id] = 0;
				$datanya[$k->mahasiswa_id] += ($k->nilai_akhir/$divider);
		    }
		}

		if($nilaiid_==''){
			// echo "1";
			$check = $mnilai->check_NA($jadwalid);
			if(isset($check)&&$check!=''){
				$data['komponenby'] = $datanya;	
				$data['by'] 	  = 'after-save';
				$data['mhs'] 	  = $mnilai->get_data_nilai_mhs($check,'','bymhs','nomd5','LEFT');
			}else{
				$data['komponen'] = $datanya;
				$data['mhs'] 	  = $mnilai->get_data_nilai_mhs($nilai[0]->nilaiid,'','bymhs','','LEFT');
			}
		}else{
			// echo "2";
			$data['komponenby'] = $datanya;	
			$data['by'] 	  = 'after-save';
			$data['mhs'] 	  = $mnilai->get_data_nilai_mhs($nilaiid_,'','bymhs','nomd5','LEFT');
		}
			
		$data['NAproses']		= $mnilai->check_NA_proses($jadwalid);
		
		$this->add_script('js/nilai/excellentexport.js');
		$this->add_script('js/nilai/proses/proses-nilai.js');
		$this->coms->add_style('bootstrap/css/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');		
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->view('nilai/index.php',$data);
	}
	
	function get_nilai_komponen_akhir($komponenid,$nilaiid,$mhsid,$param=NULL,$isprosesNA=NULL){
		$mnilai			= new model_nilai();
		$nilai 			= $mnilai->get_nilai_komp($komponenid,$nilaiid,$mhsid)->nilai;
		
		if($param=='view'){
			echo "<td>".$nilai."</td>";
		}else{
			echo '<input type="text" name="nilaiakhir[]" value="'.$nilai.'" class="form-control" ';
			if(isset($isprosesNA)&&$isprosesNA!=0)echo "disabled";
			echo ' /> ';
		}
		
	}
	
	function view_nilai_table($nilaiid,$param){
		$mnilai			= new model_nilai();
		$mconf	  	    = new model_confinfo();
		$user 			= $this->coms->authenticatedUser->role;
		$staff			= $this->coms->authenticatedUser->staffid;
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;
		
	  foreach ($nilaiid as $nilaiid) {	
		$datanilai = $mnilai->get_penilaian('','list','',$nilaiid);
		
		if(isset($datanilai->materi_id)){
			$materiid = $datanilai->materi_id;
		}else $materiid='';
		
		if(isset($datanilai->jadwal_id)){
			$jadwalid = $datanilai->jadwal_id;
		}else $jadwalid='';
		
		if(isset($datanilai->mkditawarkan_id)){
			$mkid = $datanilai->mkditawarkan_id;
		}else $mkid='';
		
		$data['mhs'] 	  = $mnilai->get_data_nilai_mhs($nilaiid,'','bymhs');
		
		$data['komponen'] = $mnilai->read_komponen('','','','','list',$jadwalid,$materiid,'','','',$nilaiid,'md5');
		
		$data['nilai_id'] = $nilaiid;
		$data['thn'] 	  = $thn;
		$data['nilai']	  = $mnilai->get_penilaian('','proses','','');
		$data['param']	  = $param;
		
		$this->view('nilai/proses/header.php',$data);
	  }
	}
	
	function view_get_materi($nilaiid){
		$mnilai			= new model_nilai();
		foreach ($nilaiid as $nilaiid) {
			$data['nn'] = $mnilai->get_penilaian('', 'get', '', $nilaiid);
			
			$datanilai = $mnilai->get_penilaian('','list','',$nilaiid);
			
			if(isset($datanilai->materi_id)){
				$materiid = $datanilai->materi_id;
			}else $materiid='';
			
			if(isset($datanilai->jadwal_id)){
				$jadwalid = $datanilai->jadwal_id;
			}else $jadwalid='';
			
			if(isset($datanilai->mkditawarkan_id)){
				$mkid = $datanilai->mkditawarkan_id;
			}else $mkid='';
			
			$data['komponen'] = $mnilai->read_komponen('','','','','list',$jadwalid,$materiid,'','','',$nilaiid,'md5');
			
			$this->view('nilai/proses/view_materi.php',$data);
		}
	}
	
	function get_nilai_akhir($nilaiid){
		$mnilai			= new model_nilai();
		$nilai = $mnilai->get_nilai_proses($nilaiid,'','NA');
		foreach ($nilai as $n) {
			// echo "<td>";
			echo $n->nilai_akhir;
			// echo "</td>";
		}
	}
	
	function get_mk(){
		$mconf	  	 = new model_confinfo();
		$mmateri	 = new model_materi();
		$staff			= $this->coms->authenticatedUser->staffid;
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;
		
		if(isset($_POST['prodiid'])&&$_POST['prodiid']!='0'){
			$prodiid 	 	 = $_POST['prodiid'];
		}else $prodiid 	 	 = "";
		
		if(isset($_POST['jdwalid'])&&$_POST['jdwalid']!='0'){
			$jadwalid 	 	 = $_POST['jdwalid'];
		}else $jadwalid 	 	 = "";
		
		if(($this->coms->authenticatedUser->role =='administrator') || ($this->coms->authenticatedUser->role =='psik')){
			$mk				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai",'',$prodiid);
			//$this->coms->no_privileges();
			// $data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai",'','',$unit);
		}else{
			$unit = $this->get_unit_sess();
			// $data['mk']				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai");
			$mk				= $mmateri->get_mk_jadwal("",$thn,$staff,"nilai",'',$prodiid, $unit);
		} 
		
		echo "<option value='0'>Silahkan Pilih</option>";
		
		if($mk){
		  foreach($mk as $row):
			echo "<option value='".$row->jdwl_id."' ";
			if(isset($jadwalid)&&$jadwalid==$row->jdwl_id){
				echo "selected";
			}
			echo " data-mk='".$row->mk_id."' >[".$row->kode_mk."] ".$row->nama_mk." - ".$row->kelas_id."</option>";
		  endforeach;
		}
		//var_dump($mk);
		
	}
	
	function get_materi(){
		$mconf	  	 = new model_confinfo();
		$mmateri	 = new model_materi();
		$staff			= $this->coms->authenticatedUser->staffid;
		$level 			= $this->coms->authenticatedUser->level;
		$role			= $this->coms->authenticatedUser->role;
		
		
		
		if(isset($_POST['mk'])){
			$mkid 	 	 = $_POST['mk'];
		}else $mkid 	 	 = "";
		
		if(isset($_POST['jadwalid']) && $_POST['jadwalid']!='0'){
			$jadwalid 	 	 = $_POST['jadwalid'];
		}else $jadwalid 	 	 = "";
		
		$thn		 = $mconf->get_semester_aktif()->tahun_akademik;
		// $data 		 = $mmateri->get_mk_jadwal($mkid,$thn,$staff,"",$jadwalid,"",$level);
		$data 		 = $mmateri->get_mk_jadwal($mkid,$thn,$staff,"",$jadwalid,"");
		
		if(isset($_POST['mtrid'])&&$_POST['mtrid']!=""){
			$materiid = $_POST['mtrid'];
		}else $materiid='';
		
		if(isset($data->mk_id)){
			$sub 	 = $mmateri->get_praktikum_materi($data->mk_id);
			echo "<option value='0'>Silahkan Pilih</option>";
			if(isset($sub)){
				foreach($sub as $dt){
					echo "<option value='".$dt->m_id."' ";
					if(isset($materiid)&&$materiid==$dt->m_id){
						echo "selected";
					}
					echo " > ".$dt->judul."</option>";
				}
			}
		}
		else{
			echo "<option value='0'>Silahkan Pilih</option>";
		}
	}
	
	function cmb_get_penilaian(){
		$mnilai 		= new model_nilai();
		$mmateri 		= new model_materi();
		$mconf	  	    = new model_confinfo();
		
		$jadwalid 		= $_POST['jadwalid'];
		$mkid 			= $_POST['mk'];
		
		$peniliaian		= $mnilai->get_penilaian($jadwalid,"get",$mkid);
		
		echo "<option value='0' data-materi='0'>Silahkan Pilih</option>";
		if(isset($peniliaian)){
			foreach($peniliaian as $dt){
				if($dt->kategori!='akhir'){
					echo "<option data-materi='".$dt->materiid."' value='".$dt->nilaiid."' ";
					if(isset($nilaiid)&&$nilaiid==$dt->nilaiid){
						echo "selected";
					}
					echo " > ".$dt->judul."</option>";
				}
			}
		}
		
	}
	
	function get_penilaian(){
		$mnilai 		= new model_nilai();
		$mmateri 		= new model_materi();
		$mconf	  	    = new model_confinfo();
		
		$thn			= $mconf->get_semester_aktif()->tahun_akademik;
		$jadwalid 		= $_POST['jadwalid'];
		$data 			= $mmateri->get_mk_jadwal("",$thn,"","",$jadwalid);
		if($data){
			if(isset($_POST['materi'])){
				$materi 		= $_POST['materi'];
				$mk						= $data->mk_id;
				$peniliaian		= $mnilai->get_penilaian($jadwalid,$materi,$mk);
				$nilai['posts']	= $peniliaian;
			}
			else{
				$jadwalid 			    = $_POST['jadwalid'];
				$mk						= $data->mk_id;
				$peniliaian		= $mnilai->get_penilaian($jadwalid,"get",$mk);
				$nilai['posts']	= $peniliaian;
			}
			
			if(!isset($peniliaian)){ ?>
				<div class="span12" align="center" style="margin-top:20px;">
				    <div class="well">Sorry, no content to show</div>
				</div> 
				<?php
			}
			else{
				//----check if KLAB-----------------------------------
				$unit_sess 		= $this->coms->authenticatedUser->unit;
				$string_sess		= '';
				foreach ($unit_sess as $u) {
					if($string_sess==''){
						$string_sess = "'".$u."'";
					}else{
						$string_sess = $string_sess.",'".$u."'";
					}
				}
				$nilai['klab']	= $mnilai->check_klab($string_sess);
				//----check if KLAB-----------------------------------
				$this->view('nilai/penilaian/view_selection.php', $nilai);
			}
		}
		else{ ?>
			<div class="span12" align="center" style="margin-top:20px;">
			    <div class="well">Sorry, no content to show</div>
			</div> 
		<?php
		}
	}

	function delete_nilai(){
		$mnilai 	= new model_nilai();
		$nilai 		= $_POST['nilai'];
		$jdwl	  	= $_POST['jdwl'];
		$del  		= $mnilai->delete_nilai($nilai,$jdwl);
		if($del==TRUE){
			echo "berhasil";
		}
	}

	function save_penilaian(){
		$mnilai  = new model_nilai();	
		$mmateri = new model_materi();
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=""){
			$nilai_id	= $_POST['hidId'];
			$action		= 'edit';
		}
		else{
			$nilai_id	= $mnilai->get_nilaiId();
			$action		= 'new';
		}
		
		if(isset($_POST['jadwal']) || trim($_POST['jadwal_edit'])==""){
			$jadwal = $_POST['jadwal'];
		}
		else{
			$jadwal = $_POST['jadwal_edit'];
		}
		
		if(isset($_POST['materimk']) || trim($_POST['materimk_edit'])==""){
			$materi = $_POST['materimk'];
		}
		else{
			$materi = $_POST['materimk_edit'];
		}
		
		$prodiid = $_POST['prodi'];
		//echo $jadwal;
		
		$data 			= $mmateri->get_mk_jadwal("", $_POST['thnid'],"","",$jadwal);
		$jadwal_id		= $data->jadwal_id;
		$mk_id			= $data->mkditawarkan_id;
		$judul 			= $_POST['judul'];
		if(isset($materi)&&$materi!="0"&&$materi!="8f00b20"&&$materi!="5436534"){
			$materi_id		= $mmateri->get_materi_by_md5($materi)->materi_id;
		}
		else $materi_id		= '-';
		
		// echo $materi_id;
		//echo $jadwal_id."\n".$judul."\n".$materi_id;
		$datanya		= Array('nilai_id' => $nilai_id,
								'materi_id' => $materi_id,
								'jadwal_id' => $jadwal_id,
								'mkditawarkan_id' => $mk_id,
								'judul' => $judul,
								'user_id' => $user,
								'last_update' => $lastupdate
							   );
	    $mnilai->replace_penilaian($datanya);
		
		if($action=='new'){ //&&$materi_id!= '-'
		
			$data_mhs	= $mnilai->get_mahasiswa_praktikum($prodiid, $mk_id, $jadwal_id);
			$data_komp	= $mnilai->read_komponen("", "", "", "", 'list', $jadwal_id,$materi_id);
			
			$count = count($data_mhs);
			
			if(isset($count)&&isset($data_komp)){
				// $i_mhs = 0;
				foreach($data_mhs as $mhs){
					// $i_komp = 0;
					foreach($data_komp as $komp){
					  if($komp->parent_id=='0'&&$komp->inforow=='1'){
					  	$data_nilai_mhs = array('komponen_id'=>$komp->komponen_id,
											  'nilai_id'=>$nilai_id,
											  'mahasiswa_id'=>$mhs->mahasiswa_id,
											  'inf_bobot'=>$komp->bobot,
											  'parent_id'=>$komp->parent_id,
											  'bobot_parent'=>$komp->bobot_parent,
											  'nilai'=>'0',
											  'user_id'=>$user,
											  'last_update'=>$lastupdate
											  );
						$mnilai->replace_nilai_mhs($data_nilai_mhs);
					  }elseif($komp->parent_id=='0'&&$komp->inforow=='0'){
					  	
					  }else{
					  	$data_nilai_mhs = array('komponen_id'=>$komp->komponen_id,
											  'nilai_id'=>$nilai_id,
											  'mahasiswa_id'=>$mhs->mahasiswa_id,
											  'inf_bobot'=>$komp->bobot,
											  'parent_id'=>$komp->parent_id,
											  'bobot_parent'=>$komp->bobot_parent,
											  'nilai'=>'0',
											  'user_id'=>$user,
											  'last_update'=>$lastupdate
											  );
						$mnilai->replace_nilai_mhs($data_nilai_mhs);
					  }
						
					}
				}
				
			}
		
		}/*if*/
		
		//var_dump($data);
		// echo $data_komp;
		//var_dump($data_komp);
		// //echo $count;
		$nilaiid = $mnilai->get_nilaiId_md5($nilai_id,'byid')->nilaiid;
		if($action!='new'){
			echo $nilaiid."/list";
		}else{
			echo $nilaiid."/komponen";
		}
	}

	function status_penilaian(){
		$mnilai  = new model_nilai();
		$nilaiid = $_POST['nilai'];
		$status	 = $_POST['status'];
		$tgl 	 = date("Y-m-d H:i:s");
		//echo $nilaiid." ".$status;	
		if(isset($status)&&$status=="approve"){
			$approve 		= '1';
			$approveby 		= $this->coms->authenticatedUser->id;
			$update			= $mnilai->update_status($nilaiid, $approve, $approveby, $tgl, "");
		}
		if(isset($status)&&$status=="proses"){
			$proses 		= '1';
			$update			= $mnilai->update_status($nilaiid, "", "", $tgl, $proses);
		}
		
		if($update==true){
			echo "Status Berhasil Di Update!";
		}
		else{
			echo "Status Gagal Di Update!";
		}
	}
	
	function save_komponen(){
		$mnilai  = new model_nilai();
		$mmateri = new model_materi();
		$mconf	 = new model_confinfo();
		$thn	 = $mconf->get_semester_aktif()->tahun_akademik;
		
		if(isset($_POST['cmb-materi'])&&$_POST['cmb-materi']!=""&&$_POST['cmb-materi']!="0"){
			$materi  = $mmateri->get_materi_by_md5($_POST['cmb-materi'])->materi_id;
			$kategori = 'materi';
		}else{
			$materi = '-';
			$kategori = 'jadwal';
		}
		
		$content = $_POST['cmb-content'];
		$percent = $_POST['cmb-percent'];
		$type	 = $_POST['cmb-type'];
		
		$jadwal  = $mnilai->get_jadwal_mk($_POST['cmb-mk'],$thn,'jadwal','left')->jadwal_id;
		
		if(isset($_POST['HidNilId'])&&$_POST['HidNilId']!=''){
			$datafromnilai = $mnilai->get_nilaiId_md5($_POST['HidNilId'],'byidmd5');
		}else{
			$datafromnilai = $mnilai->get_nilaiId_md5($_POST['penilaian'],'byidmd5');
		}
		
		$nilai_id  = $datafromnilai->nilai_id;
		
		for($i=0;$i<count($content);$i++){
			
			if(isset($_POST['cmb-hidId'][$i])&&$_POST['cmb-hidId'][$i]!=''){
				$komponenid = $_POST['cmb-hidId'][$i];
			}else $komponenid = $mnilai->get_komponenId();
			$data	= Array(
						'komponen_id'=>$komponenid, 
						'nilai_id'=>$nilai_id,
						'materi_id' =>$materi,
						'jadwal_id' =>$jadwal,
						'judul' =>$content[$i], 
						'bobot' =>$percent[$i], 
						'kategori' =>$kategori
						);
			switch($type[$i]){
			  case 'parent':
				$parentid = $komponenid;
				$mnilai->replace_komponen($data);
			  break;
			  case 'child':
				$datachild = Array('parent_id'=>$parentid);
				$datanya = $data + $datachild;
				$mnilai->replace_komponen($datanya);
			  break;
			}/*end case*/
			
		}/*end for*/
		
		if(isset($nilai_id)&&$nilai_id!=''){
			if(isset($_POST['HidNilId'])&&$_POST['HidNilId']!=''){
				$this->add_mhs($_POST['HidNilId'],$_POST['cmb-prodi']);
			}else{
				$this->add_mhs($_POST['penilaian'],$_POST['cmb-prodi']);
			}
		}
		
		echo "Success";
		
	}
	
	function add_mhs($nilaiid,$prodiid){
		$mnilai			= new model_nilai();
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;
		
		$datafromnilai = $mnilai->get_nilaiId_md5($nilaiid,'byidmd5');
		$jadwal_id = $datafromnilai->jadwal_id;
		$materi_id = $datafromnilai->materi_id;
		$mk_id 	   = $datafromnilai->mkditawarkan_id;
		$nilai_id  = $datafromnilai->nilai_id;
		
		$data_mhs	= $mnilai->get_mahasiswa_praktikum($prodiid, $mk_id, $jadwal_id);
		$data_komp	= $mnilai->read_komponen("", "", "", "", 'list', $jadwal_id,$materi_id,'','','',$nilai_id);
		
		$count = count($data_mhs);
		
		if(isset($count)&&isset($data_komp)){
			// $i_mhs = 0;
			foreach($data_mhs as $mhs){
				// $i_komp = 0;
				foreach($data_komp as $komp){
				  if($komp->parent_id=='0'&&$komp->inforow=='1'){
				  	$data_nilai_mhs = array('komponen_id'=>$komp->komponen_id,
										  'nilai_id'=>$nilai_id,
										  'mahasiswa_id'=>$mhs->mahasiswa_id,
										  'inf_bobot'=>$komp->bobot,
										  'parent_id'=>$komp->parent_id,
										  'bobot_parent'=>$komp->bobot_parent,
										  'nilai'=>'0',
										  'user_id'=>$user,
										  'last_update'=>$lastupdate
										  );
					$mnilai->replace_nilai_mhs($data_nilai_mhs);
				  }elseif($komp->parent_id=='0'&&$komp->inforow=='0'){
				  	
				  }else{
				  	$data_nilai_mhs = array('komponen_id'=>$komp->komponen_id,
										  'nilai_id'=>$nilai_id,
										  'mahasiswa_id'=>$mhs->mahasiswa_id,
										  'inf_bobot'=>$komp->bobot,
										  'parent_id'=>$komp->parent_id,
										  'bobot_parent'=>$komp->bobot_parent,
										  'nilai'=>'0',
										  'user_id'=>$user,
										  'last_update'=>$lastupdate
										  );
					$mnilai->replace_nilai_mhs($data_nilai_mhs);
				  }
					
				}
			}
			
		}

	}
	
	function add_KA(){
		$mnilai			= new model_nilai();
		$komponen = $_POST['cmb-newKA'];
			
		$datas 	  = $mnilai->get_jadwalmd5($_POST['jdwl']);
		$jadwalid = $datas->jadwal_id;
		$mkid	  = $datas->mkditawarkan_id;
		$prodiid  = $_POST['prodid'];
		
		$kategori = 'akhir';
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;
		
		for($i=0;$i<count($komponen);$i++){
			$komponenid = $mnilai->get_komponenId();
			$data	= Array(
						'komponen_id'=>$komponenid, 
						'materi_id' =>'-',
						'jadwal_id' =>$jadwalid,
						'judul' =>$komponen[$i], 
						'bobot' =>'0', 
						'kategori' =>$kategori
						);
			$mnilai->replace_komponen($data);
			
			
			$nilai_id	= $mnilai->get_nilaiId();
			$datapenilaian		= Array('nilai_id' => $nilai_id,
								'materi_id' => '-',
								'jadwal_id' => $jadwalid,
								'mkditawarkan_id' => $mkid,
								'judul' => $komponen[$i],
								'is_proses'=>'1',
								'user_id' => $user,
								'last_update' => $lastupdate,
								'kategori'=>'akhir'
							   );
			$mnilai->replace_penilaian($datapenilaian);
			
			
		//-----------------------------------------------	
		$data_mhs	= $mnilai->get_mahasiswa_praktikum($prodiid, $mkid, $jadwalid);
		$data_komp	= $mnilai->read_komponen("", "", "", "", 'list', $jadwalid,'-','get-by-param','akhir',$komponenid);
		
		$count = count($data_mhs);
		
		if(isset($count)&&isset($data_komp)){
			// $i_mhs = 0;
			foreach($data_mhs as $mhs){
				// $i_komp = 0;
				foreach($data_komp as $komp){
				  if($komp->parent_id=='0'&&$komp->inforow=='1'){
				  	$data_nilai_mhs = array('komponen_id'=>$komp->komponen_id,
										  'nilai_id'=>$nilai_id,
										  'mahasiswa_id'=>$mhs->mahasiswa_id,
										  'inf_bobot'=>$komp->bobot,
										  'parent_id'=>$komp->parent_id,
										  'bobot_parent'=>$komp->bobot_parent,
										  'nilai'=>'0',
										  'user_id'=>$user,
										  'last_update'=>$lastupdate,
										  'inf_kategori'=>'akhir'
										  );
					$mnilai->replace_nilai_mhs($data_nilai_mhs);
				  }elseif($komp->parent_id=='0'&&$komp->inforow=='0'){
				  	
				  }else{
				  	$data_nilai_mhs = array('komponen_id'=>$komp->komponen_id,
										  'nilai_id'=>$nilai_id,
										  'mahasiswa_id'=>$mhs->mahasiswa_id,
										  'inf_bobot'=>$komp->bobot,
										  'parent_id'=>$komp->parent_id,
										  'bobot_parent'=>$komp->bobot_parent,
										  'nilai'=>'0',
										  'user_id'=>$user,
										  'last_update'=>$lastupdate,
										  'inf_kategori'=>'akhir'
										  );
					$mnilai->replace_nilai_mhs($data_nilai_mhs);
				  }
					
				}
			}
			
		}
		//-----------------------------------------------
			
			
		}
		
	}
	
	function delete_komponen_NA(){
		$mnilai			= new model_nilai();
		$komponen 		= $_POST['komponen'];
		$nilaiid		= $_POST['nilaiid'];
		$mnilai->delete_komponen($komponen,'');
		$mnilai->delete_nilai($nilaiid,'');
		$mnilai->delete_praktikum_nilai_mhs($komponen,$nilaiid,'akhir');
	}
	
	function save_praktikum_nilai_mhs(){
		$mnilai			= new model_nilai();
		
		$mhsid	  = $_POST['mhsid'];
		$nilai	  = $_POST['nilai'];
		
		$nilaiid  = $mnilai->get_nilaiId_md5($_POST['nilaiid'],'byidmd5')->nilai_id;
		
		for ($i=0; $i < count($_POST['komponen']); $i++) {
		  $komponen = $mnilai->get_komponenmd5($_POST['komponen'][$i],'byidmd5')->komponen_id;
		  // echo $i.") ".$komponen." ".$nilaiid." ".$mhsid[$i]." ".$nilai[$i]."<br>";
		  // $data_nilai_mhs = array('komponen_id'=>$komponen,
								  // 'nilai_id'=>$nilaiid,
							      // 'mahasiswa_id'=>$mhsid[$i],
							      // 'nilai'=>$nilai[$i]
							     // );
		  // $mnilai->replace_nilai_mhs($data_nilai_mhs);
		  $mnilai->update_nilai_mhs($komponen,$nilaiid,$mhsid[$i],$nilai[$i]);
		}
		
		echo $_POST['nilaiid'];
		
	}
	
	function save_nilai_mhs(){
		$mnilai			= new model_nilai();
		$mhsid			= $_POST['mhsid-komp'];
		$type 			= $_POST['type'];
		$kpnid 			= $_POST['kpnid'];
		$nilid 			= $_POST['nilid'];
		$nilai			= $_POST['nilaiakhir'];
		
		for($i=0;$i<count($mhsid);$i++){
			if($type[$i]=='komponenakhir'){
				$mnilai->update_komp_akhir($kpnid[$i],$nilid[$i],$mhsid[$i],$nilai[$i]);
			}
		}
		
		//----update other table---------
		$jadwalid = $_POST['hidId'];
		$mnilai->update_tbl_prak_nilai($jadwalid);
		$mnilai->set_NA($jadwalid,'0');
		//----update other table---------
		
		if($_POST['nilai_']==''){
			$this->save_prak_akhir($_POST['prodi_'],$jadwalid,$_POST['nilaiakhir'],$_POST['type'],$_POST['mhsid-komp']);
		}else{
			echo $jadwalid."/".$_POST['prodi_']."/".$_POST['nilai_'];
		}
	}
	
	function save_prak_akhir($prodiid,$jadwalid,$nilaiinput,$type,$mhskomp){
		$mnilai			= new model_nilai();
		$lastupdate		= date("Y-m-d H:i:s");
		$user			= $this->coms->authenticatedUser->id;
		
		// $nilai_id		= $mnilai->get_nilaiId();
		$jadwal_id 		= $mnilai->get_namamk($jadwalid,$prodiid)->jadwal_id;
		$mkditawarkanid = $mnilai->get_namamk($jadwalid,$prodiid)->mkditawarkan_id;
		
		$cekifNA = $mnilai->cek_if_NA($jadwal_id,$mkditawarkanid);
		if(!isset($cekifNA)){
		  $nilai_id		= $mnilai->get_nilaiId();
		  $datanya		= Array('nilai_id' => $nilai_id,
								'materi_id' => '-',
								'jadwal_id' => $jadwal_id,
								'mkditawarkan_id' => $mkditawarkanid,
								'judul' => 'Nilai Akhir',
								'kategori' => 'akhir',
								'is_proses' => '0',
								'user_id' => $user,
								'last_update' => $lastupdate
							   );
	      $mnilai->replace_penilaian($datanya);
	    }else $nilai_id = $cekifNA->nilai_id;
		
		//-----------------------------------------------
		$cek_komponenId = $mnilai->cek_komponenId($jadwal_id);
		if(!$cek_komponenId){
			$komponenid = $mnilai->get_komponenId();
			$data	= Array(
						'komponen_id'=>$komponenid, 
						'materi_id' =>'-',
						'jadwal_id' =>$jadwal_id,
						'judul' =>'Nilai Akhir', 
						'bobot' =>'0', 
						'kategori' =>'akhir'
						);
			$mnilai->replace_komponen($data);
		}else{
			$komponenid = $cek_komponenId;
		}
		// echo $komponenid;
		//-----------------------------------------------	
		
		if(isset($type)){ 
			for($i=0;$i<count($type);$i++){
				if($type[$i]=='nilaiakhir'){
			  		$data_nilai_mhs = array('komponen_id'=>$komponenid,
									  'nilai_id'=>$nilai_id,
									  'mahasiswa_id'=>$mhskomp[$i],
									  'nilai'=>$nilaiinput[$i],
									  'user_id'=>$user,
									  'last_update'=>$lastupdate,
									  'inf_kategori'=>'akhir'
									  );
					$mnilai->replace_nilai_mhs($data_nilai_mhs);
				}
							
			}
			
		}
		//-----------------------------------------------
		echo $jadwalid."/".$prodiid."/".$nilai_id;
	}
	
	function save_final_nilai_mhs(){
		$mnilai			= new model_nilai();
		$mhsid			= $_POST['mhsid'];
		$mhs			= $_POST['mhsid-komp'];
		$nilaiakhir 	= $_POST['nilaiakhir'];
		$divider		= $_POST['divider'];
		
		$databyjadwal	= $mnilai->get_jadwal_kelas($_POST['hidId']);
		$mkid 			= $databyjadwal->mkditawarkan_id;
		$kelas 			= $databyjadwal->kelas_id;
		$nil = Array();
		for($i=0;$i<count($mhs);$i++){
			if(!isset($nil[$mhs[$i]])) $nil[$mhs[$i]] = 0;
			$nil[$mhs[$i]] += $nilaiakhir[$i]/$divider;
		}
				
		for($j=0;$j<count($nil);$j++){
			// echo $nil[$mhs[$j]];
			// echo $mhsid[$j]." ".$mkid." ".$kelas." ".$nil[$mhsid[$j]]."<br>";
			$mnilai->update_nilai_praktikan($mhsid[$j],$mkid,$kelas,$nil[$mhsid[$j]]);
		}
		
		//----save tbl-praktikum-nilai-mhs---------
		$mhsidkomp		= $_POST['mhsid-komp'];
		$type 			= $_POST['type'];
		$kpnid 			= $_POST['kpnid'];
		$nilid 			= $_POST['nilid'];
		
		for($i=0;$i<count($mhsid);$i++){
			if($type[$i]=='komponenakhir'){
				$mnilai->update_komp_akhir($kpnid[$i],$nilid[$i],$mhsidkomp[$i],$nilaiakhir[$i]);
			}
		}
		//----save tbl-praktikum-nilai-mhs---------
		
		//----update other table---------
		$mnilai->update_tbl_prak_nilai($_POST['hidId']);
		//----update other table---------
		
		if($_POST['nilai_']==''){
			$this->save_prak_akhir($_POST['prodi_'],$_POST['hidId'],$nilaiakhir,$type,$mhs);
			$mnilai->set_NA($_POST['hidId'],'1');
		}else{
			echo $_POST['hidId']."/".$_POST['prodi_']."/".$_POST['nilai_'];
		}
		
	}
	
}
?>