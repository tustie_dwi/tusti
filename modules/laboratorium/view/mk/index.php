<?php $this->head(); ?>
<h2 class="title-page">Pengaturan Mata Kuliah Laboratorium</h2>
<div class="row">
	<div class="col-sm-8">
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/laboratorium/conf/mk'); ?>">Laboratorium</a></li>
		  <li class="active"><a href="#">Pengaturan MK Laboratorium</a></li>
		</ol>
		<div class="row">
			<div class="col-sm-7">
				<div class="form-group">
					<label class="control-label">Unit Kerja</label>
					<div class="controls">
						<select class="form-control e9" name="unit_index" id="unit_index">
							<option value="0">Silahkan Pilih</option>
							<?php
								foreach($unit as $row):
								if(isset($unit_login)){
									if(in_array($row->id, $unit_login)){
										echo "<option value='".$row->unitid."' ";
										echo ">".$row->value."</option>";
									}
								}
							endforeach;
							?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<table class="table">
		    	<div id="display">
		    		<div class="col-sm-12" align="center" style="margin-top:20px;">
					    <div class="well">Silahkan pilih unit kerja terlebih dahulu</div>
					</div>
				</div>
			</table>
		</div>
    </div>
    <div class="col-sm-4">
		<?php $this->view('mk/edit.php',$data); ?>
    </div>
</div>
<?php $this->foot(); ?>