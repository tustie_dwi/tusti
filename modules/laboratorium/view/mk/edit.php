<?php 
if(isset($edit)){
	$unitid		= $edit->unit_id;
	$mk_id		= $edit->matakuliah_id;
}
?>
<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-pencil-square"></i> Tambah Pengaturan MK Laboratorium</div>
	<div class="panel-body">
		<form method="post" id="setting_mk_form">
			<div class="form-group">
				<label class="control-label">Unit Kerja</label>
				<div class="controls">
					<select class="form-control e9" name="unit" id="unit">
						<option value="0">Silahkan Pilih</option>
						<?php
							foreach($unit as $row):
								if(isset($unit_login)){
									if(in_array($row->id, $unit_login)){
										echo "<option value='".$row->id."' ";
										if(isset($unitid) && $unitid == $row->id){
											echo "selected";
										}
										echo ">".$row->value."</option>";
									}
								}
							endforeach;
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">Mata Kuliah</label>
				<div class="controls">
					<select class="form-control e9" name="mk[]" id="mk" multiple="multiple">
						<?php
							foreach($mk as $row):
								echo "<option value='".$row->matakuliah_id."' ";
								if(isset($mk_id) && $mk_id == $row->matakuliah_id){
									echo "selected";
								}
								echo ">[".$row->kode_mk."] ".$row->namamk."</option>";
							endforeach;
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="controls">
					<input type="submit" value="Tambahkan" class="btn btn-primary">
					<input type="button" id="cancel-btn" onclick="location.href='<?php echo $this->location('module/laboratorium/conf/mk')?>'" value="Cancel" class="btn btn-danger">
				</div>
			</div>	
		</form>
	</div>	
</div>