<?php 


if($mhs){ ?>
	<div class="control-group">
			<table class="table table-hover">
				
				<thead>
					<tr>
						<th><!--<input type="checkbox" class="checkall">--></th>
						<th>Mahasiswa</th>
						<th>Hadir?</th>
					</tr>
				</thead>
				<tbody>					
				<?php
					$i=0;
					$mabsen = new model_absen();
					
					foreach($mhs as $dt):
						$i++;
						$row = $mabsen->get_is_hadir($dt->mahasiswa_id, $dt->mkditawarkan_id, $dt->prodi_id, $dt->kelas_id, $sesi);
						
						if($row){
							$ishadir = $row->is_hadir;
						}else{
							$ishadir = '1';
						}
						?>
						<tr>
							<td><input type="hidden" name="chkmhs[]" value="<?php echo $dt->mahasiswa_id; ?>"><?php  echo $i;?></td>
							<td><?php echo "<b>".$dt->nim."</b> - ".$dt->nama; ?></td>
							<td>
							<select name="cmbhadir[]">
							<option value="1" <?php if($ishadir=='1'){ echo "selected"; } ?> >Hadir</option>
							<option value="2" <?php if($ishadir=='2'){ echo "selected"; } ?>>Sakit</option>
							<option value="3" <?php if($ishadir=='3'){ echo "selected"; } ?>>Ijin</option>
							<option value="4" <?php if($ishadir=='4'){ echo "selected"; } ?>>Alpha</option></select>
							</td>
						</tr>
						<?php
					endforeach;
					?>
				</tbody>
				
			</table>
			</div>
			<?php }else{ echo "<div class=well><small>No content to show</small></div>"; }?>