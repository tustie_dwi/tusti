<?php 
if($absen){
	$total = $absen->total_pertemuan;
	$tgl   = $absen->tgl;
	$jam   = $absen->jam_masuk;
	$materi	= $absen->materi;
	$sesi   = $absen->sesi_ke;
	$jmljam	= $absen->jumlah_jam;
}else{
	$tgl   = "";
	$jam   = "";
	$materi= "";
	$sesi  = "";
	$jmljam= 0;
	$total = $pertemuan;
}


?>
	
	<div class="control-group">
				<div class="control-label">Tgl</div>
				<div class="controls">
					<input type="text" name="tgl" class="date span4" value="<?php echo $tgl." ".$jam; ?>" required>
				</div>
			</div>
				
			<div class="control-group">
				<div class="control-label">Materi</div>
				<div class="controls">
					<textarea id="materi" name="materi" class="span9" rows="4"><?php echo $materi; ?></textarea>	
				</div>
			</div>
				<!--<label>Jam</label>
				<input type="text" name="jammulai" class="input-small">-->
			<div class="control-group">
				<div class="control-label">Total Pertemuan</div>
				<div class="controls">
					<input type="text" id="totalpertemuan" name="totalpertemuan" class="span3" value="<?php echo $total; ?>">
				</div>
			</div>	
			<div class="form-tambah-asisten hide-from-print">
				<div class="form-group">
					<label class="control-label">Asisten</label>
					<div class="input-group">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" onclick="addasisten(document.getElementById('sel-asisten').value)">Pilih Asisten <i class="fa fa-check"></i></button>
						</span>
						<select class="col-md-8" id="sel-asisten">
							<?php 							
																
								foreach($asisten as $key){
									echo "<option value='".$key->asisten_id.'|'.$key->mahasiswa_id.'|'.$key->nama."'>" . '' . $key->mahasiswa_id . ' - '.$key->nama ."</option>";
								}
							?> 
						</select>						
					</div>				  
				</div>
			</div>
			<div class="control-group">
				<label><h4 id="wrap-asisten"></h4></label>
			</div>
				
		<script>
			function addasisten(nilai){
				$(".table-asisten").show();
			
				var data = nilai.split('|');
				
				if(cek_asisten(data[0])){
					var r_id = "'"+data[0]+"'";
					var m_id = "'"+data[2]+"'";					
					
					var elx = '<div class="asisten row" r_id="'+data[0]+'" style="padding:5px;"><div class="col-md-6"><input type="hidden" name="asisten[]" value="'+data[2]+'"><input type="hidden" name="asistenid[]" value="'+data[0]+'" class="asistenidtmp">'+data[2]+'</div><div class="col-md-1 del"><a href="#" onclick="del_data('+r_id+','+m_id+')"><i class="fa fa-trash-o"></i></a></div></div>';		
					
					var el = '<label style="margin-right: 2px" class="label label-default" r_id="'+data[0]+'" onclick="del_data('+r_id+','+m_id+')">'+data[2]+' <input type="hidden" name="asisten[]" value="'+data[0]+'" class="asistenidtmp"> <i class="fa fa-times"></i></label>';					
				
					$("input[type=submit]").removeAttr("disabled");
					$('#wrap-asisten').append(el);					
				}
				else{
					alert('Maaf, asisten sudah dipilih!');
				}
			}
			function cek_asisten(r_id){
					var init = true;
					$("#wrap-asisten label").each(function(){
						var asisten = $(this).find('.asistenidtmp').val();
						if(asisten == r_id) init = false;
					});
					
					return init;
				}

				function cek_asisten_choose(){
					var init = false;
					$("#wrap-asisten label").each(function(){
						init = true;
					});
					
					if(! init) alert('Silakan pilih asisten.');
					else{
						return confirm('Apakah Anda yakin dengan asisten ini?');	
					}
				}

				function del_data(asisten, name){
					if(confirm('Apakah Anda membatalkan asisten ' + name + '?')){						
						$(".label[r_id='"+asisten+"']").fadeOut();
						$(".label[r_id='"+asisten+"']").remove();
					}
				}
		</script>
