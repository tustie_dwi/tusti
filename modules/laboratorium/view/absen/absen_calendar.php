<?php $this->head();

 if($absen){
	$total = $absen->total_pertemuan;
	$tgl   = $absen->tgl;
	$jam   = $absen->jam_masuk;
	$materi	= $absen->materi;
	$jmljam	= $absen->jumlah_jam;
 }else{
	$total = $pertemuan;
	$tgl   = "";
	$jam   = "";
	$materi= "";
	$jmljam= 0;
 }
 ?>

	<legend>
	<a href="<?php echo $this->location('module/laboratorium/absen/rekap'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Rekapitulasi Kehadiran</a> 
	Absen Praktikum
    </legend>    	
		<form method="post" id="form-save-absen" action="<?php echo $this->location('module/laboratorium/absen/save'); ?>" >
		 <div class="row">  
		 <span class="status-save" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>				 
         <div class="span6" id="absen">
			<?php $this->view('absen/calendar.php', $data);?>
		</div>
		
		<div id="content-absen" class="span6"> 
			<form role="form">
				<h4><i class="fa fa-plus"></i> New Absensi</h4>
				<div class="form-group">
						<select id="cmbbulan">
							<?php
								
								
								for($x = 1; $x <= 12; $x++){								
									echo "<option value='".$x."' ";
										if($month==$x){
											echo "selected";
										}										
									echo ">".date('F',mktime(0,0,0,$x,1,$year))."</option>";								 
								}
								?>
						</select>
						<?php
						$year_range = 10;
						 $selectYear = '<select  class="span3" id="cmbyear" onChange="return get_absen();">';
						 for($x = ($year-floor($year_range/2)); $x <= ($year+floor($year_range/2)); $x++)
						 {
							 $selectYear.= '<option value="'.$x.'"'.($x != $year ? '' : ' selected="selected"').'>'.$x.'</option>';
						 }
						 $selectYear.= '</select>';
						 
						 echo $selectYear;
						?>
				</div>
				<div  id="calendarModal" >
					<div class="form-group">
						<input id="tanggal" name="tanggal" class="form-control input-datetime input-tgl form_date" data-format="YYYY-MM-DD" required >
					</div>
					
					<div class="form-group">
						<label class="control-label">Judul</label>
						<input type="text" class="form-control" id="judul" placeholder="Enter judul" name="judul">
					 </div>
					 
					  <div class="form-group">		
						<label class="control-label">Pelaksanaan</label>
						<div class="row">
							<div class="col-md-3">
								<input type="text" class="form-control input-jam" id="mulai" placeholder="00:00" name="mulai"  data-format="HH:mm">
							</div>
							<div class="col-md-3">
								<input type="text" class="form-control input-jam" id="selesai" placeholder="00:00" name="selesai" data-format="HH:mm">		
							</div>		
						</div>			
					 </div>
					 
					  <div class="form-group">
						<label class="control-label">Lab</label>
						<input type="text" class="form-control" id="judul" placeholder="Enter judul" name="judul">
					 </div>
					 
					 <div class="form-group">
						<label class="control-label">Prodi</label>
						<input type="text" class="form-control" id="judul" placeholder="Enter judul" name="judul">
					 </div>
					
					<div class="form-group">
						 <button type="button" class="btn btn-primary">Save changes</button>
						 <button type="button" class="btn btn-default" id="btn-modal-close">Close</button>
					</div>
				 
			  
				</div>
			</form>
		</div>
	</div>
	
	</div>
	</form>
	</div>

<?php $this->foot(); ?>