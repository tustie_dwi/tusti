<?php
$this->head();

if($jadwal !=""){
	$header		= "Write New Jadwal Kuliah";	
	
	foreach($dosen as $row):
		$id			= $row->karyawan_id;
		$nama		= $row->nama;
	endforeach;		
}else{
	$header		= "Write New Jadwal Kuliah";	
	
	$id		= "";
	$nama 	= "";
}

$strsemester   = $semester->tahun.' - '.strToUpper($semester->is_ganjil). ' '. strToUpper($semester->is_pendek);
$semesterid	= $semester->tahun_akademik;
?>

<div class="container-fluid">  
	<fieldset>
	<legend>
		<a href="<?php echo $this->location('module/akademik/jadwal'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> Jadwal Kuliah List</a> 
		<?php if($jadwal !=""){	?>
		<a href="<?php echo $this->location('module/akademik/jadwal/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Jadwal Kuliah</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row-fluid">    
        <div class="span12">
<?php		
			if($id !=""){
				$str = "<form method=post  action='".$this->location('module/akademik/jadwal/save')."' id=frmDosen>";		
			}else{
				$str = "<form method=post  action='".$this->location('module/akademik/jadwal/dosen')."' id=frmDosen>";		
			}
						$str.= "<label>Semester</label>";
						$str.= "<input type=text value='".$strsemester."' class='span2 uneditable-input' readonly><input type=hidden name=semesterid value='".$semesterid."'><br>";
						
						$str.= "<label>Dosen</label>";
						$str.= "<input type=text name='dosen' id='dosen' value='".$nama."' class='input-xlarge'  >
								<input type=hidden name='dosenid' id='dosenid' value='".$id."'>"; 
						if($mk !=""){
							$str.= "<label>MK Diampu</label>";
							$str.= "<select name='mkid'>";
								foreach($mk as $row):
									$str.= "<option value='".$row->mkditawarkan_id."'>".$row->namamk."&nbsp;</option>";
								endforeach;	
							$str.= "</select>";
							
							$str.= "<label>Prodi</label>";
							$str.= "<input type=text name='prodi' class='prodi'>
									<input type=hidden name='prodiid' id='prodiid'>
									<br>";
								
							$str.= "<label>Kelas</label>";
							$str.= "<input type=text name='kelas' class='kelas' >
									<input type=hidden name='kelasid' id='kelasid'>
									<br>";
							$str.= "<label>Hari</label>";
							$str.= "<input type=text name='hari' class='hari' >
								<input type=hidden name='hariid' id='hariid'>
									<br>";
							$str.= "<label>Ruang</label>";
							$str.= "<input type=text name='ruang' class='ruang' >
									<input type=hidden name='ruangid' id='ruangid'>
									<br>";
							$str.= "<label>Jam</label>";
							$str.= "<input type=text name='jammulai' class='jammulai' > s/d  
									<input type=text name='jamselesai' class='jamselesai' ><br>";
							$str.= "<input type=hidden name='jammulaiid' id='jammulaiid'>
									<input type=hidden name='jamselesaiid' id='jamselesaiid'>";				
						}
												
						$str.= "<input type=hidden name=hidId value=''><input type=hidden name=hiduser	value='".$user->username."' >";
						$str.= "<input type=submit name='b_mk' id='b_mk' value='  Save  ' class='btn btn-primary'>";
					$str.= "</form>";
					
					echo $str;
		echo "</div></div></fieldset></div>";
$this->foot();

?>