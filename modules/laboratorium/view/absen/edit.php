<?php
$this->head();

if($jadwal !=""){
	$header		= "Edit Jadwal Kuliah";	
	
	foreach ($jadwal as $row):	
		$id			= $row->karyawan_id;
		$nama		= $row->nama;
		$semester   = $row->tahun.' - '.strToUpper($row->is_ganjil);
		$semesterid	= $row->tahun_akademik;
		$jadwalid	= $row->jadwal_id;
		$hari		= $row->hari;
		$kelas		= $row->kelas_id;
		$jammulai	= $row->jam_mulai;
		$jamselesai	= $row->jam_selesai;
		$ruang		= $row->ruang;
		$pengampu	= $row->dosen_id;
		$namamk		= $row->namamk;
		$prodi		= $row->prodi_id;
		$mkid		= $row->mkditawarkan_id;
	endforeach;	

	/*foreach($pengampu as $row):
		$pengampu		= $row->nama;
	endforeach;*/
}else{

	$header			= "Write New Jadwal Kuliah";
	$id				= "";
	$nama			= "";
	$namamk			= "";
	$mkditawarkan	= "";
	$prodi			= "";
	
	foreach ($semester as $row):	
		$semester   = $row->tahun.' - '.strToUpper($row->is_ganjil);
		$semesterid	= $row->tahun_akademik;
	endforeach;
	
}
?>

<div class="container-fluid">  
	<fieldset>
	<legend>
		<a href="<?php echo $this->location('module/akademik/jadwal'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> Jadwal Kuliah List</a> 
		<?php if($jadwal !=""){	?>
		<a href="<?php echo $this->location('module/akademik/jadwal/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Jadwal Kuliah</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row-fluid">    
      
<?php		
			$str = "<form method=post  action='".$this->location('module/akademik/jadwal/save')."'>";								
						$str.= "<label>Semester</label>";
						$str.= "<input type=text value='".$semester."' class='span2 uneditable-input' readonly><input type=hidden name=semesterid value='".$semesterid."'><br>";
						
						$str.= "<label>Dosen</label>";
						$str.= "<input type=text name='dosen' id='dosen' value='".$nama."' class='input-xlarge uneditable-input' id='disabledInput' readonly >
								<input type=hidden name='dosenid' id='dosenid' value='".$id."'>"; 
						$str.= "<label>MK Diampu</label>";
						if($jadwal!=""){
							$str.= "<input type=hidden name=mkid value=".$mkid.">";
							$str.= "<input type=text value='".$namamk." (".$prodi.")' readonly>";
							
							$str.= "<label>Prodi</label>";
							$str.= "<input type=text name='prodi' class='prodi' value=".$prodi.">
									<input type=hidden name='prodiid' id='prodiid' value=".$prodi.">
									<br>";									
									
							$str.= "<label>Kelas</label>";
							$str.= "<input type=text name='kelas' class='kelas' value=".$kelas.">
									<input type=hidden name='kelasid' id='kelasid' value=".$kelas.">
									<br>";
							$str.= "<label>Hari</label>";
							$str.= "<input type=text name='hari' class='hari' value=".$hari.">
								<input type=hidden name='hariid' id='hariid' value=".$hari.">
									<br>";
							$str.= "<label>Ruang</label>";
							$str.= "<input type=text name='ruang' class='ruang' value=".$ruang.">
									<input type=hidden name='ruangid' id='ruangid' value=".$ruang.">
									<br>";
							$str.= "<label>Jam</label>";
							$str.= "<input type=text name='jammulai' class='jammulai' value=".$jammulai."> s/d  
									<input type=text name='jamselesai' class='jamselesai' value=".$jamselesai."><br>";
							$str.= "<input type=hidden name='jammulaiid' id='jammulaiid'>
									<input type=hidden name='jamselesaiid' id='jamselesaiid'>";			
						}
					
												
						$str.= "<input type=hidden name=hidId value='".$jadwalid."'><input type=hidden name=hiduser	value='".$user->username."' >";
						$str.= "<input type=submit name='b_mk' id='b_mk' value='  Save  ' class='btn btn-primary'>";
					$str.= "</form>";
					
					echo $str;
		echo "</div></div></fieldset></div>";
$this->foot();

?>