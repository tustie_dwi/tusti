<?php 

$total = $pertemuan; 
	if($jadwal){ ?>
		<div class="control-group">
		<table class="table table-hover">
			
			<thead>
				<tr>
					<th><!--<input type="checkbox" class="checkall">--></th>
					<th>Dosen</th>
					<th>Prodi</th>
					<th>Kelas</th>
					<th>Hadir</th>
					<th>% Hadir</th>
				</tr>
			</thead>
			<tbody>					
			<?php
				$mabsen = new model_absen();
									
				$i=0;
				$totalhadir =0;
				foreach($jadwal as $dt):
					$i++;
					
					$jml = $mabsen->get_hadir_dosen($dt->mkditawarkan_id, $dt->prodi_id, $dt->kelas_id, $dt->dosen_id);
						
					$totalhadir = $totalhadir + $jml;
					?>
					<tr>
						<td><input type="hidden" name="chkdosen[]" value="<?php echo $dt->dosen_id; ?>"><?php  echo $i;?></td>
						<td><?php echo $dt->nama; ?></td>
						<td><?php echo $dt->prodi; ?></td>
						<td><?php echo $dt->kelas_id; ?></td>
						<?php								
						
						echo "<td>".$jml."</td>";
							
						?>
						<td><?php $persentase = ($jml/$total) * 100; echo number_format($persentase,2);?> % <?php if ($persentase < 80){  echo "<span class='label label-danger'>*</span>";} ?></td>
					</tr>
					<?php
				endforeach;
				?>
			</tbody>
			
		</table>
		</div>
		<?php }else{ echo "<div class=well><small>No content to show</small></div>"; }?>