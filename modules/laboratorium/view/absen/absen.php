<?php $this->head();

 if($absen){
	$total = $absen->total_pertemuan;
	$tgl   = $absen->tgl;
	$jam   = $absen->jam_masuk;
	$materi	= $absen->materi;
	$jmljam	= $absen->jumlah_jam;
 }else{
	$total = $pertemuan;
	$tgl   = "";
	$jam   = "";
	$materi= "";
	$jmljam= 0;
 }
 ?>

	<legend>
	<a href="<?php echo $this->location('module/laboratorium/absen/rekap'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Rekapitulasi Kehadiran</a> 
	Absen Praktikum
    </legend>    	
		<form method="post" id="form-save-absen" action="<?php echo $this->location('module/laboratorium/absen/save'); ?>" >
		 <div class="row">  
		 <span class="status-save" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>				 
         <div class="span6" id="absen">
			<div class="control-group">
				<div class="control-label">Tahun Akademik</div>
				<div class="controls">
					<!--<input type="text" name="semester" value="<?php// echo $sval;?>">-->
					<select name="cmbsemester" id="cmbsemester" onChange="get_mk()">
						<option value="-">Semester</option>
						<?php
						foreach($semester as $dt):
							echo "<option value='".$dt->tahun_akademik."' ";
							if($sid==$dt->tahun_akademik){
								echo "selected";
							}
							echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
						endforeach;
						?>
					</select>
				</div>
			</div>	
			<div class="control-group">
				<div class="control-label">MK Ditawarkan</div>
				<div class="controls">
					<select name="cmbmk" id="cmbmk" onChange="get_dosen()">
						<option value='0'>Please Select...</option>						
					</select>
				</div>
			</div>
				
			<div class="control-group">
				<div class="control-label">Pengampu</div>
				<div class="controls">
					<select name="cmbdosen" id="cmbdosen" onChange="get_prodi()">
						<option value='0'>Please Select...</option>						
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<div class="control-label">Prodi</div>
				<div class="controls">
					<select name="cmbprodi" id="cmbprodi" onChange="get_kelas()">
						<option value='0'>Please Select...</option>						
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<div class="control-label">Kelas</div>
				<div class="controls">					
					<select name="cmbkelas" id="cmbkelas" onChange="get_sesi()">
						<option value='0'>Please Select...</option>						
					</select>
				</div>
			</div>
				
										
			<div class="control-group">
				<div class="control-label">Sesi</div>
				<div class="controls">
					<select name="cmbsesi" id="cmbsesi" class="span3" onChange="get_hadir()">
						<option value='0'>Please Select...</option>
						
					</select>
				</div>
			</div>
			<div id="form-content-absen"></div>
		
			<div class="form-actions">
				<input type="submit" value=" Save Kehadiran Dosen " class="btn btn-primary btn-save-absen" data-loading-text="Saving...">
				<span class="status-save" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>     
			</div>
		</div>
		
		<div id="content-absen" class="span6"></div>
	</div>
	
	</div>
	</form>
	</div>

<?php $this->foot(); ?>