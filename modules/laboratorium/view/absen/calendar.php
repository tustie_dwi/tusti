<?php

 
$style	="calendar";  
			 
if(($month == NULL) || ($year == NULL))
	 {
		 // Month in numbers with the leading 0
		 $month = date("m");    
		 $year  = date("Y");    
		 $title = date("F Y");
	 }else{
		 $title = date('F Y',mktime(0,0,0,$month,1,$year));
	 }

	/* We need to take the month value and turn it into one without a leading 0 */
	 if((substr($month, 0, 1)) == 0)
	 {
		 // if value is between 01 - 09, drop the 0
		 $tempMonth = substr($month, 1);                                                                                              
		 $month = $tempMonth;
	 }
		
	 echo '<h4>'.$title.'</h4><table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
	
	 $headings = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
	echo  '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
		 .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

	 /* days and weeks vars now ... */
	 $running_day = date('w',mktime(0,0,0,$month,1,$year));
	 $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	 $days_in_this_week = 1;
	 $day_counter = 0;
	 $dates_array = array();

	
	echo  '<tbody><tr class="'. $style .'-row">';
	   
	 for($x = 0; $x < $running_day; $x++):
		echo  '<td class="'. $style .'-day-np"> </td>';
		 $days_in_this_week++;
	 endfor;

	 $list_day=0;
	 $skip = false;
	 
	 $mabsen = new model_absen();
				
	 for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		$row = $mabsen->getEvent($list_day, $month, $year, $running_day);
		if($row): $strclass=""; else: $strclass="popup-form"; endif;
		
		 if($list_day == date("j",mktime(0,0,0,$month)))
		 {    
			echo  '<td class="'. $strclass ." ".  $style .'-current-day" data-calendaryear="'.$year.'" data-calendarmonth="'.$month.'" data-calendarday="'.($day_counter+1).'">';
			 $txtstyle = $style .'-current-day';
		 }
		 else            
		 {    
			 if(($running_day == "0") || ($running_day == "6"))
			 {
				echo  '<td class="'. $strclass ." ". $style .'-weekend-day" data-calendaryear="'.$year.'" data-calendarmonth="'.$month.'" data-calendarday="'.($day_counter+1).'">';
				  $txtstyle = $style .'-weekend-day';
			 }
			 else
			 {
				echo  '<td class="'. $strclass ." ". $style .'-day" data-calendaryear="'.$year.'" data-calendarmonth="'.$month.'" data-calendarday="'.($day_counter+1).'">';  
				 $txtstyle = $style ;	
			 }
		 }
		
		echo  '<div class="'. $strclass ." ". $style .'-day-number">'.$list_day.'</div>';
		echo  $this->get_data_calendar($list_day, $month, $year, $running_day);		 
		echo  '</td>';
		
		 if($running_day == 6):
			echo  '</tr>';
			 if(($day_counter+1) != $days_in_month):
				echo  '<tr class="'. $style .'-row">';
			 endif;
			 $running_day = -1;
			 $days_in_this_week = 0;
		 endif;
		 $days_in_this_week++; $running_day++; $day_counter++;
	 endfor;

	 if($days_in_this_week < 8) :
		 for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			echo  '<td class="'. $style .'-day-np"> </td>';
		 endfor;
	 endif;

	echo  '</tr>';
	echo  '</tbody></table>';
	?>
	
<script>
	$('.show-calendar-it').click(function(e){		
		e.preventDefault();
		$('content-absen').hide();
		$('#calendarModal').hide();		
		$(this).parent('small').parent('div').parent('td').children('.hide-it').fadeIn().removeClass('hide-it');
		$(this).hide();		
		$('#calendarModal').hide();
	});
	
	$('.popup-form').click(function(e){	
		e.preventDefault();
		$('content-absen').show();
		$('#calendarModal').show('slow');
		
		var $year=$(this).data('calendaryear');
		var $month=$(this).data('calendarmonth');
		var $day=$(this).data('calendarday')
		var day = ("0" + $day).slice(-2);
		var month = ("0" + ($month + 1)).slice(-2);
		var today = $year+"-"+(month)+"-"+(day);
		$('#calendarModal .input-datetime').val(today);
		$('#calendarModal .input-datetime').datepicker();
		//$('#calendarModal').modal('toggle');
	
		});
</script>