<?php 

$total = $pertemuan; 
if($mhs){ ?>
		<div class="control-group">
		<table class="table table-hover">
			
			<thead>
				<tr>
					<th><!--<input type="checkbox" class="checkall">--></th>
					<th>Mahasiswa</th>
					<th>Hadir</th>
					<th>Sakit</th>
					<th>Ijin</th>
					<th>Alpha</th>
					<th>% Hadir</th>
				</tr>
			</thead>
			<tbody>					
			<?php
				$mabsen = new model_absen();									
				$j=0;
				foreach($mhs as $dt):
					$j++;
					?>
					<tr>
						<td><input type="hidden" name="chkmhs[]" value="<?php echo $dt->mahasiswa_id; ?>"><?php  echo $j;?></td>
						<td><?php echo "<b>".$dt->nim."</b> - ".$dt->nama; ?></td>
						<?php
							$totalhadir =0;
							for ($i=1;$i<5;$i++){
								$jml = $mabsen->get_hadir_mhs($dt->mkditawarkan_id, $dt->prodi_id, $dt->kelas_id, $dt->mahasiswa_id, $i);
								if($i==1){
									$totalhadir = $totalhadir + $jml;
								}else{
									$totalhadir = $totalhadir;
								}
								echo "<td>".$jml."</td>";
							}
						?>
						<td><?php $persentase = ($totalhadir/$total) * 100; echo number_format($persentase,2);?> % <?php if ($persentase < 80){  echo "<span class='label label-danger'>*</span>";} ?></td>
					</tr>
					<?php
				endforeach;
				?>
			</tbody>
			
		</table>
		</div>
		<?php }else{ echo "<div class=well><small>No content to show</small></div>"; }?>