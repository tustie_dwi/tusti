<?php $this->head(); ?>
<div class="row">
	<div class="col-sm-12">
		<div class="row">
			<h2 class="title-page">Materi Praktikum</h2>
			<div class="row">
				<div class="col-sm-8">
					<ol class="breadcrumb">
					  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
					  <li><a href="<?php echo $this->location('module/laboratorium/conf/mk'); ?>">Laboratorium</a></li>
					  <li class="active"><a href="#">Penilaian Praktikum</a></li>
					</ol>
					<div class="row">
						<div class="col-sm-6">
							<div class="control-group">
								<label class="control-label">Mata Kuliah</label>
								<div class="controls">
									<select class="form-control" name="mk_index" id="mk_index">
										<option value="0">Silahkan Pilih</option>
										<?php
											foreach($mk as $row):
												echo "<option value='".$row->mk_id."' ";
												echo ">[".$row->kode_mk."] ".$row->nama_mk."</option>";
											endforeach;
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<table class="table">
				    	<div id="display">
				    		<div align="center" style="margin-top:20px;">
							    <div class="well">Silahkan pilih mata kuliah terlebih dahulu</div>
							</div>
						</div>
					</table>
			    </div>
			    <div class="col-sm-4">
					<?php $this->view('materi/edit.php',$data); ?>
			    </div>
			</div>
		</div>
	</div>
</div>
<?php $this->foot(); ?>