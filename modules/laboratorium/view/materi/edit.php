<?php 
if(isset($materi_edit)){
	$thn		= $materi_edit->tahun_akademik;
	$mk_id		= $materi_edit->mk_id;
	$parent_id	= $materi_edit->p_id;
	$judul		= $materi_edit->judul;
	$materi_id	= $materi_edit->materi_id;
}
?>
<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-pencil-square"></i> <span id="desc"></span> Materi Praktikum</div>
	<div class="panel-body">
		<form method="post" id="materi_praktikum_form">
			<div class="form-group">
				<label class="control-label">Tahun Akademik</label>
				<div class="controls">
					<?php if($thn_akademik->is_pendek == 1) $pendek = " Pendek"; 
						  else $pendek = ""; 
					?>
					<input class="form-control" type="text" name="thn" id="thn" value="<?php echo ucwords($thn_akademik->semester).$pendek ?>"/>
					<input type="hidden" name="thnid" id="thnid" value="<?php echo $thn_akademik->tahun_akademik ?>"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">Mata Kuliah</label>
				<div class="controls">
					<select class="form-control" name="mk" id="mk">
						<option value="0">Silahkan Pilih</option>
						<?php
							foreach($mk as $row):
								echo "<option value='".$row->mk_id."' ";
								if(isset($mk_id)&&$mk_id==$row->mk_id){
									echo "selected";
								}
								echo ">[".$row->kode_mk."] ".$row->nama_mk."</option>";
							endforeach;
						?>
					</select>				
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">Sub Materi</label>
				<div class="controls">
					<select class="form-control" name="submateri" id="submateri">
						<option value="0">Silahkan Pilih</option>
						<?php
							if(isset($materi_edit)){
								foreach($materi_praktikum as $dt){
									echo "<option value='".$dt->m_id."' ";
									if(isset($parent_id)&&$parent_id==$dt->m_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								}
							}
						?>
					</select>				
				</div>
			</div>
			<div class="form-group">
				<label class="control-label">Judul</label>
				<div class="controls">
					<input class="form-control" type="text" name="judul" id="judul" placeholder="Masukkan Judul Materi" value="<?php if(isset($judul)) echo $judul ?>"/>
				</div>
			</div>
			<div class="form-group">
				<div class="controls">
					<input type="hidden" id="hidId" name="hidId" value="<?php if(isset($materi_id)) echo $materi_id ?>">
					<input type="submit" value="Submit" class="btn btn-primary">
					<input type="button" id="cancel-btn" onclick="location.href='<?php echo $this->location('module/laboratorium/materi')?>'" value="Cancel" class="btn btn-danger">
				</div>
			</div>	
		</form>	
	</div>
</div>