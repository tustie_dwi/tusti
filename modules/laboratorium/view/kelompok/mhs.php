<?php if($mhs) : ?>
	<div class="panel panel-default">
		<!-- <div class="panel-heading">Biodata Pribadi</div> -->
		<div class="panel-body ">
			<table class="table table-condensed table-bordered">
				<tr>
					<td colspan="2" class="text-center">No.</td>
					<td>Nama</td>
					<td>NIM</td>
					<td>Kelompok</td>
				</tr>
				
				<?php $no=1; foreach($mhs as $key) { ?>
					<tr>
						<td class="text-center">
							<input style="cursor: pointer" onclick="set_kelompok(this.value)" type="checkbox" value="<?php echo $key->praktikan_id ?>">
						</td>
						<td><?php echo $no . '.'; $no++; ?></td>
						<td><?php echo $key->nama ?></td>
						<td><?php echo $key->nim ?></td>
						<td class="kelompok-<?php echo $key->praktikan_id ?>"><?php echo $key->kelompok ?></td>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>
<?php endif; ?>