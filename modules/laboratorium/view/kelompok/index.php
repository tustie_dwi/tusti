<?php $this->head(); ?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/laboratorium/kelompok'); ?>">Kelompok</a></li>
</ol>
<div class="breadcrumb-more-action">
</div>

<div class="row">
  <div class="col-md-12">
  	<div class="row">
	<div class="col-md-4 block-box">
		<i id="loading" class="fa fa-refresh fa-spin" style="position: absolute; right: 20px; display: none"></i>
		<form id="form-mhs" role="form" method="POST">
			<div class="form-group">
				<label>Tahun Akademik</label>
			    <select class="form-control e9" name="tahun">
			    	<option value="0">Pilih Tahun</option>
			    	<?php
			    		if($tahun) :
							foreach($tahun as $key) {
								if($key->is_pendek == '') $is_pendek = '';
								else $is_pendek = " (Pendek)";
								$isi = $key->tahun. ' - ' . ucfirst($key->is_ganjil) . $is_pendek;
								echo "<option value='".$key->tahun_akademik."'>".$isi ."</option>";
							}
						endif;
			    	?>
			    </select>
			</div>
			<div class="form-group">
				<label>Prodi</label>
			    <select class="form-control e9" name="prodi">
			    	<option value="0">Pilih Prodi</option>
			    	<?php
			    		if($prodi) :
							foreach($prodi as $key) {
								echo "<option value='".$key->prodi_id."'>".$key->keterangan ."</option>";
							}
						endif;
			    	?>
			    </select>
			</div>
			<div class="form-group">
				<label>Mata Kuliah</label>
			    <select class="form-control e9" name="mk">
			    	<option value="0">Pilih Kuliah</option>
			    </select>
			</div>
			<div class="form-group">
				<label>Kelompok</label>
			    <select class="form-control e9" name="kelompok">
			    	<option value="0">Pilih Kelompok</option>
			    	<?php
			    		if($kelompok) :
							foreach($kelompok as $key) {
								echo "<option value='".$key->kelompok_id.'|'.$key->nama."'>".$key->nama ."</option>";
							}
						endif;
			    	?>
			    </select>
			</div>
			<a onclick="get_mhs()" class="btn btn-primary"><i class="fa fa-search"></i> Lihat Mahasiswa</a>
		</form>
	</div>
	
	<div class="col-md-8" id="detail-box"></div>
	</div>
  </div>	
</div>
<?php $this->foot(); ?>