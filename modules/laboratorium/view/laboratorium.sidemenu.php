<li class="dropdown-submenu">
  <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-th-large"></i> Master Data
  </a>
	<ul class="dropdown-menu">
		<li><a href="<?php echo $this->location($module_url.'agenda');?>"><i class="icon-plus"></i> Agenda</a></li>
		<li><a href="<?php echo $this->location($module_url.'ruang');?>"><i class="icon-plus"></i> Ruang</a></li>
		<li><a href="<?php echo $this->location($module_url.'mhs');?>"><i class="icon-pencil"></i> Mahasiswa</a></li>
		<li><a href="<?php echo $this->location($module_url.'dosen');?>"><i class="icon-pencil"></i> Dosen</a></li>
		<li><a href="<?php echo $this->location($module_url.'mk');?>"><i class="icon-plus"></i> Matakuliah</a></li>
  </ul>
</li>