<?php 
$this->head(); 

if($posts){
	
}else{
	$unit		= "";
	//$takademik	= "";
	$id			= "";
}


?>

<div class="container-fluid">
	<legend><a href="<?php echo $this->location('module/laboratorium/jadwal'); ?>" class="btn btn-primary pull-right">
    <i class="icon-pencil icon-white"></i> Jadwal Asisten List</a> Write Jadwal Asisten
	</legend>
	
	<form method="post" action="<?php echo $this->location('module/laboratorium/jadwal/write'); ?>" >
		<div class="control-group">
			<label class="control-label">Laboratorium</label>
			<div class="controls">
				<select name="cmbunit" onChange="javascript:OpenPage(this.value);">
					<?php
						foreach($lab as $row):
							echo "<option value='".$row->id."' ";
							if($unit == $row->id){
								echo "selected";
							}
							echo ">".$row->value."</option>";
						endforeach;
					?>
				</select>				
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Tahun Akademik</label>
			<div class="controls">
				<select name="cmbsemester" onChange="form.submit();">
					<option value="-">Please Select</option>
					<?php
						foreach($semester as $row):
							echo "<option value='".$row->tahun_akademik."' ";
							if($takademik == $row->tahun_akademik){
								echo "selected";
							}
							echo ">".$row->tahun . " ". ucWords($row->is_ganjil) . " ". ucWords($row->is_pendek);
							
							echo "</option>";
						endforeach;
					?>
				</select>				
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Jadwal MK</label>
			<div class="controls">
				<select name="cmbjadwal" class='cmbmulti populate'>
					<?php					
						if($mk){
							foreach($mk as $row):
								echo "<option value='".$row->jadwal_id."' ";
								if($takademik == $row->tahun_akademik){
									echo "selected";
								}
								echo ">".ucWords($row->hari).", ".$row->kode_mk . " - ". ucWords($row->keterangan) . ", Kelas ".$row->kelas_id;
								
								echo "</option>";
							endforeach;
						}else{
							echo "<option value='-'>None</option>";
						}
					?>
				</select>				
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label">Mahasiswa</label>
			<div class="controls">
				<input type="hidden" name="cmbmhs" class="e7" style="width:600px" populate multiple/>
			</div>
		</div>
		
		<div class="control-group">
			<div class="controls">
				<input type="hidden" name="hidId" value="<?php echo $id; ?>">
				<input type="submit" name="b_save" value="Submit">
			</div>
		</div>
					
	</form>	
</div>
<?php $this->foot(); ?>