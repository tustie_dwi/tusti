<?php 
if($type=='edit'){
	$header = "Edit Asisten";
	foreach ($edit as $p) {
		$unit = $p->unit_id;
		$takademik = $p->tahun_akademik;
		$tgl_mulai = $p->tgl_mulai;
		$tgl_selesai = $p->tgl_selesai;
		$mhs = $p->nim." - ".$p->namamhs;
		$mhsid = $p->mahasiswa_id;
		$mkid = $p->mkditawarkan_id;
		$keterangan = $p->keterangan;
		$isaktif = $p->is_aktif;
		$id = $p->hidId;
	}
}else{
	$header = "New Asisten";
	$unit		 = "";
	$id			 = "";
	$mkid		 = "";
	$isaktif	 = "";
	$mhs		 = "";
	$mhsid		 = "";
	$keterangan	 = "";
	$takademik   = $takademik->tahun_akademik;
	if(isset($kalender)){
		$tgl_mulai   = $kalender[0]->tgl_mulai;
		$tgl_selesai = $kalender[0]->tgl_selesai;
	}else{
		$tgl_mulai = '';
		$tgl_selesai = '';
	}
	
}

?>
<div class="col-sm-5">
	<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-pencil-square"></i> <?php echo $header ?></div>
	<div class="panel-body">
		<form id="form">
			<div class="form-group">
				<label class="control-label">Laboratorium</label>
				<div class="controls">
					<select name="cmbunit" class="form-control e9">
						<option value="0">Pilih Laboratorium</option>
						<?php
							foreach($lab as $row):
								if(isset($unit_login)){
									if(in_array($row->id, $unit_login)){
										echo "<option value='".$row->id."' ";
										if($unit == $row->id){
											echo "selected";
										}
										echo ">".$row->value."</option>";
									}
								}
							endforeach; 
						?>
					</select>				
				</div>
			</div>
	
			<div class="form-group">
				<label class="control-label">Tahun Akademik</label>
				<div class="controls">
					<select name="cmbtakademik" class="form-control e9">
						<?php
							foreach($semester as $row):
								echo "<option value='".$row->tahun_akademik."' ";
								if($takademik == $row->tahun_akademik){
									echo "selected";
								}
								echo ">".$row->tahun . " ". ucWords($row->is_ganjil) . " ". ucWords($row->is_pendek);
								
								echo "</option>";
							endforeach;
						?>
					</select>				
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label">Tanggal Mulai</label>
				<div class="controls">			
					<input type="text" name="cmbtglmulai" class="form-control form_datetime" value="<?php echo $tgl_mulai; ?>"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label">Tanggal Selesai</label>
				<div class="controls">
					<input type="text" name="cmbtglselesai" class="form-control form_datetime" value="<?php echo $tgl_selesai; ?>"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label">Mahasiswa</label>
				<div class="controls">
					<input type="text" required="required" name="cmbmhs" id="mahasiswa" value="<?php echo $mhs; ?>" class="form-control"/>
					<input type="hidden" name="cmbmhsid" id="mahasiswaid" value="<?php echo $mhsid; ?>" class="form-control"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label">Mata Kuliah</label>
				<div class="controls">
					<select name="cmbmk" class="form-control e9">
						<option value='0'>Select Mata Kuliah</option>
						<?php
							foreach($mkpraktikum as $row):
								echo "<option value='".$row->mkid."' ";
								if($mkid == $row->mkid){
									echo "selected";
								}
								echo ">". "[" . ucWords($row->kode_mk) . "] ". ucWords($row->nama_mk);
								
								echo "</option>";
							endforeach;
						?>
					</select>				
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label">Keterangan</label>
				<div class="controls">
					<textarea class="form-control" name="cmbketerangan" style="height: 50px"><?php echo $keterangan ?></textarea>
				</div>
			</div>
			
			<div class="form-group">	
				<div class="controls">
					<label class="checkbox"><input type="checkbox" name="cmbisaktif" value="1" <?php if ($isaktif==1) { echo "checked"; } ?>><span id="aktif-text">Tidak Aktif</span></label>			
				</div>	
	       	</div>
			
			<div class="form-group">
				<div class="controls">
					<input type="hidden" name="hidId" value="<?php echo $id; ?>">
					<input type="submit" class="btn btn-primary" name="b_save" value="Submit">
					<?php if($type=='edit'){ ?>
					<a href="<?php echo $this->location('module/laboratorium/conf/asisten'); ?>" class="btn btn-danger">
			    	<i class="icon-pencil icon-white"></i> Cancel</a>
			    	<?php } ?>
				</div>
			</div>
						
		</form>
	</div>
</div>