<?php $this->head(); ?>
<h2 class="title-page">Asisten Laboratorium</h2>
<div class="row">
	<div class="col-sm-7">
	 <ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/laboratorium/conf/mk'); ?>">Laboratorium</a></li>
	  <li class="active"><a href="#">Asisten Laboratorium</a></li>
	 </ol>
		 <?php
		 
		 if(isset($status) and $status) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $statusmsg; ?>
			</div>
		<?php 
		endif; 
		if( $posts ) :	
		?>	
		<table class="table table-hover" id="example">
			<thead>
				<tr>
					<th>Asisten</th>
					<th>&nbsp;</th>				
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
				$i = 1;
				if($posts > 0){
					foreach ($posts as $dt): 
					?>
					<tr id='post-<?php echo $dt->asisten_id;?>' data-id='<?php echo $dt->asisten_id; ?>' valign=top>
						<td>
							<?php echo $dt->namamhs; ?><br> <code><?php echo $dt->nim ?></code><?php if($dt->is_aktif==0){ ?><small><span class="label label-info">*)</span></small><?php } ?>
						</td>			
						<td>
							<?php echo $dt->namamk ?><br>
							<span class="label label-success"><?php echo $dt->namalab; ?></span>											
						</td>					
						<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>									
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/laboratorium/conf/asisten/'.$dt->asisten_id); ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>									
								  </ul>
								</li>
							</ul>
						</td>
						
						</tr>
						<?php
					 endforeach; 
				 }
				 ?>
			</tbody></table>
		<?php
		 else: 
		 ?>
	    <div class="col-sm-12" align="center" style="margin-top:20px;">
		    <div class="well">Sorry, no content to show</div>
	    </div>
	    <?php endif; ?>
	 <!-- </div>  -->
	</div>
	<?php $this->view('asisten/edit.php', $data); ?>
</div>
<?php $this->foot(); ?>