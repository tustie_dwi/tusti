<?php $this->head(); 
?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/laboratorium/nilai'); ?>">Nilai</a></li>
</ol>
<div class="breadcrumb-more-action">
	<legend>
		<?php if($param=='nilai'){ ?>
		<a href="<?php echo $this->location('module/laboratorium/nilai/index/komponen'); ?>" class="btn btn-info pull-right"><i class="fa fa-inbox"></i> Komponen</a>
		<?php }
		elseif ($param=='komponen') { ?>
		<a href="<?php echo $this->location('module/laboratorium/nilai'); ?>" class="btn btn-primary pull-right"><i class="icon-pencil icon-white"></i> Penilaian</a>
		<?php }
		elseif ($param=='list') {
		?>
		<a href="<?php echo $this->location('module/laboratorium/nilai'); ?>" class="btn btn-primary pull-right"><i class="icon-pencil icon-white"></i> Penilaian</a>
		<?php }
		?>
	</legend>
</div>

<div class="row">
	<div class="col-md-12">
			<div class="row">
			<div class="col-md-<?php if($param=='list')echo "12";else echo "6" ?>">
				<?php 
				if(isset($index)){
					$this->view('nilai/'.$index, $data); 
				}
				?>
			</div>
	
			<div class="col-md-<?php if($param=='list')echo "0";else echo "6" ?>">
				<?php 
				if(isset($nav)){
					$this->view('nilai/'.$nav, $data); 
				}
				?>
		    </div>
		    </div>
    </div>
</div>
<?php $this->foot(); ?>