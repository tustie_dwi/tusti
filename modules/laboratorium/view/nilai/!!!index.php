<?php $this->head(); ?>
<div class="row-fluid">
	<legend><a href="<?php echo $this->location('module/laboratorium/nilai/write'); ?>" class="btn btn-primary pull-right">
    <i class="icon-pencil icon-white"></i> New Nilai Praktikum</a> Nilai Praktikum List
	</legend>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	if( $posts ) :	
	?>	
	<table class="table table-hover" id="example" data-id='module/layanan'>
		<thead>
			<tr>
				<th width="18%">NIM</th>
				<th>&nbsp;</th>				
				<th>Act</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
				?>
				<tr id='post-<?php echo $dt->asisten_id;?>' data-id='<?php echo $dt->asisten_id; ?>' valign=top>
					<td><span class='text-info'><?php echo $dt->nama; ?></span>	</td>			
					<td>
						<span class='label label-success'><?php echo $dt->namamk; ?></span><br>
						<code><?php echo $dt->ruang; ?></code>&nbsp;<small>
						<span class="text-warning"><?php echo date("H:i",strtotime($dt->jam_mulai));?> - <?php echo date("H:i",strtotime($dt->jam_selesai));?></span></small>
					</td>					
					<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>									
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/laboratorium/nilai/edit/'.$dt->id); ?>"><i class='icon-pencil'></i> Edit</a>	
									</li>									
									<!--<li>
										<a class='btn-delete-post'><i class='icon-remove'></i> Delete</a>
									</li>-->
								  </ul>
								</li>
							</ul>
						</td>
					
					</tr>
					<?php
				 endforeach; 
			 }
			 ?>
		</tbody></table>
	<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</div>
<?php $this->foot(); ?>