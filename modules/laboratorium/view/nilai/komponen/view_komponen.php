<div class="panel panel-default">
<div class="panel-heading">Komponen</div>
<div class="panel-body">
<div class="row">
	<div class="col-md-12">
	
	<form id="form-index" action="<?php echo $this->location('module/laboratorium/nilai/index/komponen'); ?>" role="form" method="POST">
	<div class="form-group">
		<label class="control-label">Prodi</label>
		<div class="controls">
			<select name="cmb-prodi-index" class="form-control e9">
				<option value="0">Silahkan Pilih</option>
				<?php
					foreach($prodi as $row):
						echo "<option value='".$row->prodi_id."' ";
						if(isset($prodiid)&&$prodiid==$row->prodi_id){
							echo "selected";
						}
						echo " >".$row->keterangan."</option>";
					endforeach;
				?>
			</select>
		</div>
	</div>
	
	<input type="hidden" name="selected-mk" value="<?php if(isset($jadwalid)) echo $jadwalid ?>" />
	<div id="form-index-submit">
	 <div class="form-group">
		<label class="control-label">Mata Kuliah</label>
		<div class="controls">
			<select name="mk_index" disabled class="form-control e9">
				<option value="0">Silahkan Pilih</option>
				<?php
					foreach($mk as $row):
						echo "<option value='".$row->jdwl_id."' ";
						if(isset($jadwalid)&&$jadwalid==$row->jdwl_id){
							echo "selected";
						}
						echo " data-mk='".$row->mk_id."' >[".$row->kode_mk."] ".$row->nama_mk." - ".$row->kelas_id."</option>";
					endforeach;
				?>
			</select>
		</div>
	 </div>
	</div>
	
	</form>
	
	<?php if(isset($materi)){ ?>
	<div class="tree well">
		<ul>
		<?php 
		foreach ($materi as $m) {
			echo $this->display_data_komponen($m->komponen_id,$m->nilai_id,$m->materiid,$m->jadwalid,$m->kategori,$prodiid);
		} ?>
		</ul>
	</div>
	<?php }else{ ?>
	
	<div class="well" align="center">No Data Show</div>
	
	<?php } ?>
	</div>
</div>
</div>
</div>

<style>
	.tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:1px solid #999;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}
.tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:25px;
    width:25px
}
.tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:30px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}
</style>