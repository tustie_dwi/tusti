<div class="panel panel-default">
<div class="panel-heading">
	<?php echo $headerpanel ?>
</div>
<div class="panel-body">
<form id="form">
	<input type="hidden" name="HidNilId" value="<?php if(isset($nilai))echo $nilai ?>" />
	<div class="form-group">
		<label class="control-label">Prodi</label>
		<div class="controls">
			<select name="cmb-prodi" class="form-control e9">
				<option value="0">Silahkan Pilih</option>
				<?php
					foreach($prodi as $row):
						echo "<option value='".$row->prodi_id."' ";
						if(isset($prodiid)&&$prodiid==$row->prodi_id){
							echo "selected";
						}
						echo " >".$row->keterangan."</option>";
					endforeach;
				?>
			</select>
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label">Mata Kuliah</label>
		<div class="controls">
			<select name="cmb-mk" disabled class="form-control e9">
				<option value="0">Silahkan Pilih</option>
				<?php
					foreach($mk as $row):
						echo "<option value='".$row->jdwl_id."' ";
						if(isset($jadwalid)&&$jadwalid==$row->jdwl_id){
							echo "selected";
						}
						echo " data-mk='".$row->mk_id."' >[".$row->kode_mk."] ".$row->nama_mk." - ".$row->kelas_id."</option>";
					endforeach;
				?>
			</select>
		</div>
	</div>
	
	 <div class="form-group">
		<label class="control-label">Penilaian</label>
		<div class="controls">
			<select name="penilaian" disabled class="form-control e9">
				<option value="0">Silahkan Pilih Mata Kuliah</option>
			</select>
		</div>
	 </div>
	
	<div class="form-group">
		<label class="control-label">Materi</label>
		<div class="controls">
			<select name="cmb-materi" disabled class="form-control e9">
				<option value="0">Silahkan Pilih Mata Kuliah</option>
			</select>
		</div>
	</div>
	<input type="hidden" class="form-control" name="hid-materi" value="<?php if(isset($mtrid))echo $mtrid ?>"/>
	
	<span class="form-add-group">
	<label class="control-label">Komponen</label><br>
	<a href="javascript::" onclick="add_parent()" class="btn btn-info" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Tambah Komponen</a>
	<?php if(isset($komponen)){ //edit 
		$i = 0;
		foreach ($komponen as $k) { 
	?>
	<div class="form-group">
		<div class="controls-parent parent-<?php echo ($i+1) ?>" data-num="<?php echo ($i+1) ?>">
			<input type="hidden" name="cmb-type[]" class="form-control" value="parent"/>
			<div class="col-md-6" style="padding-left: 0px;padding-right: 0px;">
			<input type="text" required="required" name="cmb-content[]" class="form-control" style="float: left;" value="<?php echo $k->judul ?>"/>
			</div>
			<div class="col-md-3 input-group">
			 <input type="text" required="required" name="cmb-percent[]" onkeypress="return isNumberKey(event)" value="<?php echo $k->bobot ?>" class="form-control percent parent-percent"/><span class="input-group-addon">%</span>
			</div>
			<input type="hidden" required="required" name="cmb-hidId[]" class="form-control" style="float: left;" value="<?php echo $k->komponen_id ?>"/>
			<a href="javascript::" onclick="add_child(this)" class="btn btn-info"><i class="fa fa-plus"></i></a>
			<a href="javascript::" onclick="delete_('parent',this)" data-del="<?php if(isset($k->kpn_id))echo $k->kpn_id ?>" class="btn btn-danger delete-parent"><i class="fa fa-minus"></i></a>
			<ul class="child" style="list-style-type: none;">
			<?php echo $this->get_komponen_child($k->mkid, $thn, $k->komponen_id,$i,'edit','view-mk'); ?>
			</ul>
		</div>
	</div>
	<?php $i++; }
	 }else{ 
	?>
	<div class="form-group">
		<div class="controls-parent parent-1" data-num="1">
			<input type="hidden" name="cmb-type[]" class="form-control" value="parent"/>
			<div class="col-md-6" style="padding-left: 0px;padding-right: 0px;">
				<input type="text" required="required" name="cmb-content[]" class="form-control" style="float: left;"/>
			</div>
			<div class="col-md-3 input-group">
			 <input type="text" required="required" name="cmb-percent[]" onkeypress="return isNumberKey(event)" value="100" class="form-control percent parent-percent"/><span class="input-group-addon">%</span>
			</div>
			
			<a href="javascript::" onclick="add_child(this)" class="btn btn-info"><i class="fa fa-plus"></i></a>
			<a href="javascript::" onclick="delete_('parent',this)" class="btn btn-danger delete-parent"><i class="fa fa-minus"></i></a>
			<ul class="child" style="list-style-type: none;">
				
			</ul>
		</div>
	</div>	
	<?php } ?>
	</span>
	
	<div class="form-group">
		<div class="controls">
			<input type="hidden" name="hidId" value="<?php if(isset($id))echo $id; ?>">
			<input type="submit" class="btn btn-primary" name="b_save" value="Submit">
			<!-- New Komponen -->
			<?php if($headerpanel=='Edit Komponen'){ ?>
				<a class="btn btn-danger" href="<?php echo $this->location('module/laboratorium/nilai/index/komponen'); ?>">Cancel</a>
			<?php } ?>
		</div>
	</div>
</form>
</div>
</div>
