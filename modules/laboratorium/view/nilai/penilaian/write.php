<?php
	if(isset($edit_nilai)){
		$jadwalid	= $edit_nilai->jdwl_id;
		$materiid	= $edit_nilai->materiid;
		$prodi_id	= $edit_nilai->prodi_id;
		$judul		= $edit_nilai->judul;
		$is_approve	= $edit_nilai->is_approve;
		$approve_by = $edit_nilai->approve_by;
		$is_proses	= $edit_nilai->is_proses;
		$last_update= $edit_nilai->last_update;
		$tgl_approve= $edit_nilai->tgl_approve;
		$nilai_id	= $edit_nilai->nilai_id;
	}else{
		$is_proses = '';
	}
?>

<!-- <div class="row"> -->
	<div class="panel panel-default">
	<div class="panel-heading"><span id="desc"></span> Penilaian Praktikum</div>
	<div class="panel-body">
	<!-- <legend><span id="desc"></span> Penilaian Praktikum</legend> -->
	<form method="post" id="penilaian-form">
		<div class="form-group">
			<label class="control-label">Tahun Akademik</label>
			<div class="controls">
				<?php if($thn_akademik->is_pendek == 1) $pendek = " Pendek"; 
					  else $pendek = ""; 
				?>
				<input class="form-control" type="text" name="thn" id="thn" value="<?php echo ucwords($thn_akademik->semester).$pendek ?>"/>
				<input class="form-control" type="hidden" name="thnid" id="thnid" value="<?php echo $thn_akademik->tahun_akademik ?>"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Program Studi</label>
			<div class="controls">
				<select name="prodi" id="prodi" <?php if($is_proses=='1')echo "disabled" ?> class="form-control e9" >
					<option value="0">Silahkan Pilih</option>
					<?php
						foreach($prodi as $row):
							echo "<option value='".$row->prodi_id."'";
							if(isset($prodi_id)&&$prodi_id==$row->prodi_id){
								echo "selected";
							}
							echo ">".$row->keterangan."</option>";
						endforeach;
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Mata Kuliah</label>
			<div class="controls">
				<select name="jadwal" id="jadwal" class="form-control e9">
					<option value="0">Silahkan Pilih</option>
					<?php
						foreach($mk as $row):
							echo "<option value='".$row->jdwl_id."' ";
							if(isset($jadwalid)&&$jadwalid==$row->jdwl_id){
								echo "selected";
							}
							echo ">[".$row->kode_mk."] ".$row->nama_mk." - ".$row->kelas_id."</option>";
						endforeach;
					?>
				</select>
				<?php if(isset($jadwalid)){ ?>
					<input type="hidden" name="jadwal_edit" id="jadwal_edit" value='<?php echo $jadwalid ?>'/>
				<?php } ?>			
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Materi</label>
			<div class="controls">
				<select name="materimk" id="materimk" class="form-control e9">
					<option value="0">Silahkan Pilih</option>
					<?php
						if(isset($edit_nilai)){
							foreach($materi_praktikum as $dt){
								echo "<option value='".$dt->m_id."' ";
								if(isset($materiid)&&$materiid==$dt->m_id){
									echo "selected";
								}
								echo ">".$dt->judul."</option>";
							}
						}
					?>
				</select>
				<?php if(isset($materiid)){ ?>
					<input type="hidden" class="form-control" name="materimk_edit" id="materimk_edit" value='<?php echo $materiid ?>'/>
				<?php } ?>				
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Judul</label>
			<div class="controls">
				<input type="text" class="form-control" name="judul" <?php if($is_proses=='1')echo "disabled" ?> id="judul" placeholder="Masukkan Judul Materi" value="<?php if(isset($judul)) echo $judul ?>"/>
			</div>
		</div>
		<div class="form-group">
			<div class="controls">
				<input type="hidden" class="form-control" id="hidId" name="hidId" value="<?php if(isset($nilai_id)) echo $nilai_id ?>">
				<?php if($is_proses!='1'){ ?>
				<input type="submit" value="Submit" class="btn btn-primary">
				<?php } ?>
				<input type="button" id="cancel-btn" onclick="location.href='<?php echo $this->location('module/laboratorium/nilai')?>'" value="Cancel" class="btn btn-danger">
			</div>
		</div>	
	</form>	
	</div>
	</div>
<!-- </div> -->