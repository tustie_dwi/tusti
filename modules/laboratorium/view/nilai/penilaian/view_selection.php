<table class="table table-hover example">
	<thead>
		<th>Judul</th>
		<th>Detail</th>
		<th>&nbsp;</th>
	</thead>
	<tbody>
		<?php if(isset($posts)){
					foreach($posts as $dt){ 
						if($dt->kategori!='akhir'){ ?>
						<tr class="penilaian-<?php echo $dt->nilaiid  ?>">
							<td style="vertical-align: middle"><?php echo $dt->judul ?></td>
							<td style="vertical-align: middle">
								<?php echo "[".$dt->kode_mk."] ".$dt->nama_mk ?>
								<?php if($dt->is_approve == '1'){
											echo "<br>Approved by ";
											echo" <label class='label label-success'>".$dt->name."</label>";
											echo "<br>at ".$dt->tgl_approve;
									  }
									  if($dt->is_proses == '1'){
											echo "<br><code>Published</code>";
									  } 
								?><br>
							</td>
							<td style="vertical-align: middle">            
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown pull-right'>
									  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>									
										
										<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/laboratorium/nilai/edit/config/'.$dt->nilaiid."/".$dt->materiid); ?>">
												<?php if($dt->is_proses != '1'){ ?>
												<i class='fa fa-edit'></i> Edit Penilaian
												<?php }else{ ?>
												<i class='fa fa-eye'></i> Lihat Penilaian
												<?php } ?>
											</a>	
										</li>
										
										<li>
											<a class='btn-edit-post' href="#" onclick="edit_nilai_mhs('<?php echo $dt->nilaiid ?>', '<?php echo $dt->is_proses ?>')">
												<?php if($dt->is_proses != '1'){ ?>
												<i class='fa fa-edit'></i> Edit Nilai Mahasiswa
												<?php }else{ ?>
												<i class='fa fa-eye'></i> Lihat Nilai Mahasiswa
												<?php } ?>
											</a>	
										</li>
										<?php if($klab=='true'){ ?>
											<?php if($dt->is_proses == '0'&&$dt->is_approve == '0'){ ?>
											<li>
												<a class='btn-edit-post' href="#" onclick="update_status('<?php echo $dt->nilaiid ?>','approve')"><i class='fa fa-check'></i> Terima</a>	
											</li>
											<?php } ?>
											<?php if($dt->is_proses == '0'&&$dt->is_approve == '1'){ ?>
											<li>
												<a class='btn-edit-post' href="#" onclick="update_status('<?php echo $dt->nilaiid ?>','proses')"><i class='fa fa-refresh'></i> Publish</a>	
											</li>
											<?php } ?>
											<?php if($dt->is_proses == '0'){ ?>
											<li>
												<a class='btn-edit-post' href="#" onclick="doDelete('<?php echo $dt->nilaiid ?>','<?php echo $dt->jdwl_id ?>')"><i class='fa fa-trash-o'></i> Delete</a>	
											</li>
											<?php } ?>	
										<?php } ?>								
									  </ul>
									</li>
								</ul>
							</td>
						</tr>
		<?php 			}
							 }
					
					echo '<a href="javascript::" class="btn btn-info pull-right proses-nilai"><i class="fa fa-inbox"></i> Proses Nilai Akhir</a>';
					
			  }?>
	</tbody>
</table>