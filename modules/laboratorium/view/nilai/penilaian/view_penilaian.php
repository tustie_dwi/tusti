<!-- <div class="row"> -->
<div class="panel panel-default">
<div class="panel-heading">Penilaian</div>
<div class="panel-body">
<div class="row">
	<div class="col-md-8">
		
		<div class="form-group">
			<label class="control-label">Jenis</label>
			<div class="controls">
			    <label class="radio-inline">
			     	<input name="jenis" value="mk" type="radio" checked>Mata Kuliah
			    </label>
			    <label class="radio-inline">
			     	<input name="jenis" value="materi" type="radio">Materi
			    </label>
		    </div>
	    </div>
	    <div class="form-group">
			<label class="control-label">Program Studi</label>
			<div class="controls">
				<select name="prodi_index" id="prodi_index" class="form-control e9">
					<option value="0">Silahkan Pilih</option>
					<?php
						foreach($prodi as $row):
							echo "<option value='".$row->prodi_id."'";
							echo ">".$row->keterangan."</option>";
						endforeach;
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Mata Kuliah</label>
			<div class="controls">
				<select name="jadwal_index" id="jadwal_index" class="form-control e9">
					<option value="0">Silahkan Pilih</option>
					<!-- <?php
						foreach($mk as $row):
							echo "<option value='".$row->jdwl_id."'";
							echo ">[".$row->kode_mk."] ".$row->nama_mk." - ".$row->kelas_id."</option>";
						endforeach;
					?> -->
				</select>
			</div>
		</div>
		<div class="form-group" id="materi-option"></div>
	</div>
</div>
<div class="row">
	<div id="display">
		<div class="col-md-12" align="center" style="margin-top:20px;">
		    <div class="well">Silahkan lengkapi form diatas terlebih dahulu</div>
		</div>
	</div>
</div>
</div>
</div>
<!-- </div> -->