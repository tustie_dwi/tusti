<?php $i=0; ?>
<?php if(isset($NAproses)&&$NAproses==0){ ?>

<form method=post name="form" id="form-add-NA" class="form-horizontal" style="background: white; padding: 10px;">
	Komponen NA <a href="javascript::" onclick="add_KA()" class="btn btn-info" style="margin-left: 5px;margin-bottom: 5px;"><i class="fa fa-plus"></i></a>
	<div id="new-KA">
	<div class="form-group">
		<div class="controls">
			<div class="col-md-6">
			<input type="text" required="required" name="cmb-newKA[]" class="form-control" style="float: left;"/> 
			</div>
			<a href="javascript::" onclick="delete_KA(this)" class="del-button btn btn-danger" style="margin-left: 5px;"><i class="fa fa-minus"></i></a>
		</div>
	</div>
	</div>
	
	<!-- <div class="form-group"> -->
		<!-- <div class="controls"> -->
			<input type="hidden" name="jdwl" value="<?php echo $jadwalhid ?>" />
			<input type="hidden" name="nilai_" value="<?php echo $nilai_ ?>" />
			<input type="hidden" name="prodid" value="<?php echo $prodid ?>" />
			<input type="submit" value="Submit" name="b_add" class="btn btn-primary ">
		<!-- </div> -->
	<!-- </div> -->
</form>

<?php } ?>
<!-- action="<?php echo $this->location('module/laboratorium/nilai/save_nilai_mhs'); ?>" -->
<form method=post name="form" id="form-nilai-mhs" class="form-horizontal" style="background: white; padding: 10px;">
	<?php if(isset($komponen)){ ?>
	<table class="table table-bordered" id="myTable">
		<thead>
		<tr>
			<th>Mahasiswa</th>
			<th>Nilai Akhir</th>
			<?php 
			 if(isset($komp_akhir)){  $j=0;
				foreach ($komp_akhir as $ka) {
			?>
			<th>
				<?php echo $ka->judul ?> 
				<a href="javascript::" onclick="delete_KA_view('<?php echo $ka->kpn_id ?>','<?php echo $nilai_id_inf[$j]->nilai_id ?>')" class="del-komponenAkhir" style="margin-left: 5px;">
					<i class="fa fa-minus-circle"></i>
				</a>
			</th>
			<?php	$j++;
				}	
			  };
			?>
		</tr>
		</thead>
		<tbody>
			<?php foreach ($komponen as $m) { ?>
			<tr>
				<td>
					<small><?php echo $mhs[$i]->mahasiswa_id ?></small><br>
					<?php echo $mhs[$i]->nama ?>
					<input type="hidden" name="mhsid[]" value="<?php echo $mhs[$i]->mahasiswa_id; ?>" class="form-control" />
				</td>
				<td>
					<input type="text" name="nilaiakhir[]" value="<?php echo substr($m, 0,5); ?>" class="form-control" <?php if(isset($NAproses)&&$NAproses!=0)echo "disabled" ?> />
					<input type="hidden" name="type[]" value="nilaiakhir" class="form-control" />
					<input type="hidden" name="kpnid[]" value="<?php //echo $ka->komponen_id ?>" />
					<input type="hidden" name="nilid[]" value="<?php //echo $nilai_id_inf[$k]->nilaiid ?>" />
					<input type="hidden" name="mhsid-komp[]" value="<?php echo $mhs[$i]->mahasiswa_id; ?>" class="form-control" />
				</td>
				<?php 
				 if(isset($komp_akhir)){ $k=0;
					foreach ($komp_akhir as $ka) {
				?>
				<td>
					<?php $this->get_nilai_komponen_akhir($ka->komponen_id, $nilai_id_inf[$k]->nilaiid,$mhs[$i]->mahasiswa_id,'',$NAproses); ?>
					<!-- <input type="text" name="nilaiakhir[]" value="0" class="form-control" /> -->
					<input type="hidden" name="type[]" value="komponenakhir" class="form-control" />
					<input type="hidden" name="kpnid[]" value="<?php echo $ka->komponen_id ?>" />
					<input type="hidden" name="nilid[]" value="<?php echo $nilai_id_inf[$k]->nilaiid ?>" />
					<input type="hidden" name="mhsid-komp[]" value="<?php echo $mhs[$i]->mahasiswa_id; ?>" class="form-control" />
				</td>
				<?php	$k++;
					}	
				  };
				?>
			</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	<input type="hidden" name="divider" value="<?php echo (count($komp_akhir)+1) ?>" />
	<input type="hidden" name="hidId" value="<?php echo $jadwalhid ?>" />
	<input type="hidden" name="nilai_" value="<?php echo $nilai_ ?>" />
	<input type="hidden" name="prodi_" value="<?php echo $prodid ?>" />
	<?php if(isset($NAproses)&&$NAproses==0){ ?>
	<input type="submit" class="btn btn-primary" name="b_save_data" value="Save Nilai">
	<input type="submit" class="btn btn-primary" name="b_save" value="Publish Nilai Akhir Mahasiswa">
	<?php } ?>
	<a download="<?php echo $namajadwal ?>.xls" class="btn btn-primary" id="export" href="#" > Export table to Excel</a>
	<?php }elseif(isset($komp_akhir)){ ?>
	<!-- kompakhir -->
	<table class="table table-bordered" id="myTable">
		<thead>
		<tr>
			<th>Mahasiswa</th>
			<?php 
			 if(isset($komp_akhir)){  $j=0;
				foreach ($komp_akhir as $ka) {
			?>
			<th>
				<?php echo $ka->judul ?> 
				<?php if($ka->judul!='Nilai Akhir'){ ?>
				<a href="javascript::" onclick="delete_KA_view('<?php echo $ka->kpn_id ?>','<?php echo $nilai_id_inf[$j]->nilai_id ?>')" class="del-komponenAkhir" style="margin-left: 5px;<?php if(isset($NAproses)&&$NAproses!=0)echo "display: none;" ?>">
					<i class="fa fa-minus-circle"></i>
				</a>
				<?php } ?>
			</th>
			<?php	$j++;
				}	
			  };
			?>
		</tr>
		</thead>
		<tbody>
			<?php foreach ($mhs as $m) { ?>
			<tr>
				<td>
					<small><?php echo $m->mahasiswa_id ?></small><br>
					<?php echo $m->nama ?>
					<input type="hidden" name="mhsid[]" value="<?php echo $m->mahasiswa_id; ?>" class="form-control" />
				</td>
				<?php 
				 if(isset($komp_akhir)){ $k=0;
					foreach ($komp_akhir as $ka) {
				?>
				<td>
					<?php $this->get_nilai_komponen_akhir($ka->komponen_id, $nilai_id_inf[$k]->nilaiid,$m->mahasiswa_id,'',$NAproses); ?>
					<input type="hidden" name="type[]" value="komponenakhir" class="form-control" />
					<input type="hidden" name="kpnid[]" value="<?php echo $ka->komponen_id ?>" />
					<input type="hidden" name="nilid[]" value="<?php echo $nilai_id_inf[$k]->nilaiid ?>" />
					<input type="hidden" name="mhsid-komp[]" value="<?php echo $m->mahasiswa_id; ?>" class="form-control" />
				</td>
				<?php	$k++;
					}	
				  };
				?>
			</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	<input type="hidden" name="divider" value="<?php echo (count($komp_akhir)) ?>" />
	<input type="hidden" name="hidId" value="<?php echo $jadwalhid ?>" />
	<input type="hidden" name="nilai_" value="<?php echo $nilai_ ?>" />
	<input type="hidden" name="prodi_" value="<?php echo $prodid ?>" />
	<?php if(isset($NAproses)&&$NAproses==0){ ?>
	<input type="submit" class="btn btn-primary" name="b_save_data" value="Save Nilai">
	<input type="submit" class="btn btn-primary" name="b_save" value="Publish Nilai Akhir Mahasiswa">
	<?php } ?>
	<a download="<?php echo $namajadwal ?>.xls" class="btn btn-primary" id="export" href="#" > Export table to Excel</a>
	<!-- kompakhir -->	
	<?php }else{ ?>
		<div class="well" align="center">
			Belum terdapat komponen untuk jadwal ini.
		</div>
	<?php } ?>
</form>
<!-- style="display: none;"  -->
<table class="table table-bordered" id="my-view-Table" style="display: none;">
	<thead>
	<tr>
		<th rowspan="3">NIM</th>
		<th rowspan="3">Mahasiswa</th>
		<?php 
			$this->view_get_materi($nilai);
		?>
		<th rowspan="3">NA <br> tugas</th>
		<?php 
			if(isset($komp_akhir)){
				foreach ($komp_akhir as $kompa) {
					if($kompa->judul!='Nilai Akhir'){
						echo "<th rowspan='3'>".$kompa->judul."</th>";
					}					
				}
			}
		?>
	</tr>
	<tr>
		<?php 
			$this->view_nilai_table($nilai,'header');
		?>
	</tr>
	<tr>
		<?php 
			$this->view_nilai_table($nilai,'komponen');
		?>
	</tr>
	</thead>
	<tbody>
		<?php if(isset($komponen)){ ?>
			<?php $x=0; foreach ($komponen as $m) { ?>
			<tr>
				<td><small><?php echo $mhs[$x]->mahasiswa_id; ?></small></td>
				<td>
					<?php echo $mhs[$x]->nama; ?>
				</td>
				<?php 
					foreach ($nilai as $n) {
						$this->get_nilai_mhs_komponen($n,$mhs[$x]->mahasiswa_id,'view');
					} 
				?>
				<td>
					<?php echo substr($m, 0,5); ?>
				</td>
				<?php 
				if(isset($komp_akhir)){ $k=0;
					foreach ($komp_akhir as $kompa) {
						if($kompa->judul!='Nilai Akhir'){
						echo $this->get_nilai_komponen_akhir($kompa->komponen_id, $nilai_id_inf[$k]->nilaiid,$mhs[$x]->mahasiswa_id,'view');
						}
						$k++;
					}
				}
			?>	
			</tr>
			<?php $x++; } ?>
		<?php }elseif(isset($komponenby)){ ?>
			<?php $x=0; foreach ($komponenby as $m) { ?>
			<tr>
				<td><small><?php echo $mhs[$x]->mahasiswa_id; ?></small></td>
				<td>
					<?php echo $mhs[$x]->nama; ?>
				</td>
				<?php 
					foreach ($nilai as $n) {
						$this->get_nilai_mhs_komponen($n,$mhs[$x]->mahasiswa_id,'view');
					} 
				?>
				<td>
					<?php echo substr($m, 0,5); ?>
				</td>
				<?php 
				if(isset($komp_akhir)){ $k=0;
					foreach ($komp_akhir as $kompa) {
						if($kompa->judul!='Nilai Akhir'){
						echo $this->get_nilai_komponen_akhir($kompa->komponen_id, $nilai_id_inf[$k]->nilaiid,$mhs[$x]->mahasiswa_id,'view','');
						}
						$k++;
					}
				}
			?>	
			</tr>
			<?php $x++; } ?>
		<?php } ?>
	</tbody>
</table>