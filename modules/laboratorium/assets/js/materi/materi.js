$(document).ready(function(){
	$('#desc').html("");
	$('#desc').html("Tambah");
	if($("#hidId").length > 0) {
		var edit = $('#hidId').val();
		if(edit.trim()!==""){
			$('#cancel-btn').show();
			$('#desc').html("");
			$('#desc').html("Edit");
		}else{
			$('#cancel-btn').hide();
			$('#desc').html("");
			$('#desc').html("Tambah");
		}
		
		$('#judul,#mk').change(function(){
			var judul	= $('#judul').val();
			var mk		= $('#mk').val();
			
			if(judul.trim()!=="" || mk !== '0'){
				$('#cancel-btn').show();
			}
			else{
				$('#cancel-btn').hide();
			}
		})
	}
	
	$('#mk').change(function(){
		var mkid 	= $('#mk').val();
		var URL		= base_url + 'module/laboratorium/materi/get_submateri';
		$.ajax({
			url : URL,
			type : "POST",
			dataType : "HTML",
	        data : $.param({
				mk : mkid
			}),
	        async: false,
	        success:function(msg) 
	        {
	            if(msg !== ""){
	            	$('#submateri').html(msg);
	            }
	        }
		})
	});
	
	$('#mk_index').change(function(){
		var mkid 	= $('#mk_index').val();
		var URL		= base_url + 'module/laboratorium/materi/get_submateri_index';
		$.ajax({
			url : URL,
			type : "POST",
			dataType : "HTML",
	        data : $.param({
				mk : mkid
			}),
	        async: false,
	        success:function(msg) 
	        {
	            if(msg !== ""){
	            	//$('#display').empty();
	            	$('#display').html(msg);
	            }
	        }
		})
	});
	
	$("#materi_praktikum_form").submit(function(e){
		var mkid 		= $('#mk').val();
		var submateri 	= $('#submateri').val();
		var judul 		= $('#judul').val();
		
		// alert(mkid+"\n"+submateri+"\n"+judul);
		
		if(mkid !== '0' && judul.trim() !== ""){
			//alert("TES");
			var formData = new FormData($(this)[0]);
			var URL = base_url + 'module/laboratorium/materi/save_materi';
			$.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert(msg);
		            location.href = base_url + 'module/laboratorium/materi';
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});
		}
		else{
			alert("Form yang anda submit belum lengkap!");
		}
		e.preventDefault(); //STOP default action
		return false;
	});
	
	$('#thn').attr("disabled","disabled");
})
