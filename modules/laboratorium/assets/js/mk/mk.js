$( window ).resize(function() {
	setTimeout(function() {
        var width_mk	= $("#unit").outerWidth(true);
		$("#mk").css('width', width_mk+'px');
		$("#s2id_mk").css('width', width_mk+'px');
	}, 1000);
});

$(document).ready(function(){
	var edit = $('#mk').val();
	// alert(edit);
	$('#mk').attr("disabled","disabled");
	$('#cancel-btn').hide();
	
	var width_mk	= $("#unit").outerWidth(true);
	$("#mk").css('width',width_mk+'px');
	
	if(edit !== null){
		if(edit.trim()!=="0"){
			$('#cancel-btn').show();
			$('#mk').removeAttr("disabled");
		}else{
			$('#mk').attr("disabled","disabled");
			$('#cancel-btn').hide();
		}
	}
	$('#unit,#mk').change(function(){
		var unit	= $('#unit').val();
		var mk		= $('#mk').val();
		
		if(unit.trim()!=="0" || mk !== '0'){
			$('#cancel-btn').show();
		}
		else{
			$('#cancel-btn').hide();
		}
	})
	
	$('#unit').change(function(){
		var unit 	= $('#unit').val();
		if(unit!=='0'){
			$('#mk').removeAttr("disabled");
		}
		else{
			$('#mk').val('0');
			$('#mk').attr("disabled","disabled");
			$('#cancel-btn').hide();
		}
	});
	
	$(".e9").select2();
	
	$("#setting_mk_form").submit(function(e){
		var unit	= $('#unit').val();
		var mk		= $('#mk').val();
		if(unit.trim()!=="0" && mk !== null){
			var formData = new FormData($(this)[0]);
			var URL = base_url + 'module/laboratorium/conf/save_settingmk';
			$.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert(msg);
		            location.href = base_url + 'module/laboratorium/conf/mk';
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});
		}
		else{
			alert("Form yang anda submit belum lengkap!");
		}
		e.preventDefault(); //STOP default action
		return false;
	});
	
	
	//index
	$('#unit_index').change(function(){
		var unit	= $('#unit_index').val();
		var URL		= base_url + 'module/laboratorium/conf/get_settingmk';
		if(unit!=='0'){
			$.ajax({
				url : URL,
				type : "POST",
				dataType : "HTML",
		        data : $.param({
					unit : unit
				}),
		        async: false,
		        success:function(msg) 
		        {
		            if(msg !== ""){
		            	$('#display').html(msg);
		            }
		        }
			})
		}
		else{
			$('#display').empty();
			var output = '<div class="col-sm-12" align="center" style="margin-top:20px;">';
			output += '<div class="well">Silahkan pilih unit kerja terlebih dahulu</div>';
			output += '</div>';
			$('#display').html(output);
		}
	});
});

function doDelete(unit,mk){
	var x = confirm("Are you sure you want to delete?");
	var url = base_url + 'module/laboratorium/conf/delete_settingmk';
 	if (x){
	  	$.ajax({
			type : "POST",
			dataType : "html",
			url : url,
			data : $.param({
				unit : unit,
				mk : mk
			}),
			success : function(msg) {
				if(msg!==""){
					$('.sett'+unit+"-"+mk).fadeOut();
				}
			}
		});
	}
}
