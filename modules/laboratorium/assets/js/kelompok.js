$(".e9").select2();

$(document).on( 'change', '[name=tahun], [name=prodi]', function(){
	var uri = base_url + 'module/laboratorium/kelompok/get_mk';
	var tahun_ = $("[name=tahun]").val();
	var prodi_ = $("[name=prodi]").val();
	
	if(tahun_ != '' && prodi_ != ''){
		$("#loading").show();
		$.ajax({
	        url : uri,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({tahun : tahun_, prodi : prodi_}),
	        success:function(msg) 
	        {
	            $("[name=mk]").html(msg);
	            get_mhs();
	            $("#loading").fadeOut();
	        },
	        error: function(msg) 
	        {
	        	$("#loading").fadeOut();
				alert("Proses gagal, mohon ulangi lagi !!"); 
	        }
	    });
    }
});

$(document).on( 'change', '[name=mk]', function(){
	get_mhs();
});

function get_mhs(){
	$("#loading").show();
	var formData = new FormData($('#form-mhs')[0]);
	var URL = base_url + 'module/laboratorium/kelompok/get_mhs';
	
    $.ajax({
	    url : URL,
	    type: "POST",
	    dataType : "HTML",
	    data : formData,
	    async: false,
	    success:function(msg) 
	    {
	    	// alert(msg);
	    	$("#detail-box").html(msg);
	       	$("#loading").fadeOut();
	    },
	    error: function(msg) 
	    {
	    	$("#loading").fadeOut();
	        alert("Proses gagal, mohon ulangi lagi !!"); 
	    },
	    cache: false,
	    contentType: false,
	    processData: false
    });
}

function set_kelompok(mhs_){
	var kelompok_ = $('[name=kelompok]').val();
	var URL = base_url + 'module/laboratorium/kelompok/set_kelompok';
	
	if(kelompok_ != ''){
		kel = kelompok_.split("|");
		$("#loading").show();
		$.ajax({
	        url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({kelompok : kelompok_, mhs : mhs_}),
	        success:function(msg) 
	        {
	        	if(msg == 'true'){
	        		$(".kelompok-"+mhs_).html(kel[1]);
	        	}
	        	$("[value='"+mhs_+"']").removeAttr("checked");
	            $("#loading").fadeOut();
	        },
	        error: function(msg) 
	        {
	        	$("#loading").fadeOut();
				alert("Proses gagal, mohon ulangi lagi !!"); 
	        }
	    });
	}
}
