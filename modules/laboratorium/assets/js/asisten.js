$(document).ready(function(){
	$(".form_datetime").datetimepicker({
		format: 'yyyy-mm-dd', 
		showSecond: true
	});
	
	$(".e9").select2();
	
	if($('input[name="cmbisaktif"]').is(":checked")){
		$('#aktif-text').text("Aktif");
	}else{
		$('#aktif-text').text("Tidak Aktif");
	}
	
	var thnid = $('select[name="cmbtakademik"]').val();
	var unitid = $('[name="cmbunit"]').val();
	$('select[name="cmbmk"]').select2().select2('val', '0');
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/conf/get_mk_by_thn',
		data : $.param({
			thnid : thnid,
			unitid : unitid
		}),
		success : function(msg) {
			$('select[name="cmbmk"]').html(msg);
		}
	});
	
});

$(function() {
	$("#mahasiswa").autocomplete({ 
		source: base_url + "/module/laboratorium/conf/mhs",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mahasiswaid').val(ui.item.id); 
				$('#mahasiswa').val(ui.item.value);
			} 
	}); 
	
	$('#mahasiswa').blur(function() {
		if($(this).val().length==0){
			$('#mahasiswaid').val('');
		}
	});
	
	$('input[name="cmbisaktif"]').click(function(){
		if($(this).is(":checked")){
			$('#aktif-text').text("Aktif");
		}else{
			$('#aktif-text').text("Tidak Aktif");
		}
	});
	
	$('select[name="cmbtakademik"]').change(function(){
		var thnid = $(this).val();
		var unitid = $('[name="cmbunit"]').val();
		$('select[name="cmbmk"]').select2().select2('val', '0');
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/laboratorium/conf/get_mk_by_thn',
			data : $.param({
				thnid : thnid,
				unitid : unitid
			}),
			success : function(msg) {
				$('select[name="cmbmk"]').html(msg);
			}
		});
	});
	
	$(document).on('click', 'input[name="b_save"]', function() {
	  $('#form').submit(function (e) {
		var formData = new FormData($(this)[0]);
		var URL = base_url + 'module/laboratorium/conf/save';
	      $.ajax({
	        url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data) 
	        {
	        	alert(data);
	        	location.href = base_url + 'module/laboratorium/conf/asisten';
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Proses Simpan Gagal!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	  });
	});
	
});

$(document).on('change', '[name="cmbunit"]', function(){
	var unitid = $(this).val();
	var thnid = $('select[name="cmbtakademik"]').val();
	$('select[name="cmbmk"]').select2().select2('val', '0');
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/conf/get_mk_by_thn',
		data : $.param({
			thnid : thnid,
			unitid : unitid
		}),
		success : function(msg) {
			// alert(msg);
			$('select[name="cmbmk"]').html(msg);
		}
	});
});
