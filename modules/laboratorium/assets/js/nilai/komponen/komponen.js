$(document).ready(function() {
	$(".e9").select2();
	if($('.delete-parent').length==1){
		$('.delete-parent').hide();
	};
	$('input[name="b_save"]').hide();
	/*newmk*/
    var out = '';
    var jdwl =  $('select[name="cmb-mk"]').val();
 	var mkid = $('select[name="cmb-mk"]').find(':selected').data('mk');
 	var mtrid = $('input[name="hid-materi"]').val()
 	
  	if(mkid=='0'||!mkid){
  		out = '<option value="0">Silahkan Pilih Mata Kuliah</option>';
		$('select[name="cmb-materi"]').html(out);
  		$('select[name="cmb-materi"]').attr("disabled", true);
  		$('select[name="cmb-materi"]').select2().select2("enable", false);
  	}else{
  	  $.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/nilai/cmb_get_penilaian',
		data : $.param({
			jadwalid : jdwl,
			mk : mkid
		}),
		success : function(msg) {
		  if(msg=="<option value='0' data-materi='0'>Silahkan Pilih</option>"){
		  	out = '<option value="0">Tidak Ada Penilaian</option>';
		  	$('select[name="penilaian"]').html(out);
			$('select[name="penilaian"]').attr("disabled", true);
			$('select[name="penilaian"]').select2().select2("enable", false);	
			$('input[name="b_save"]').hide();	
		  }else{
			$('select[name="penilaian"]').html(msg);
			$('select[name="penilaian"]').attr("disabled", false);
			$('select[name="penilaian"]').select2().select2("enable", true);
			
			$('select[name="penilaian"] option').each(function() {
				if($(this).val()==$('input[name="HidNilId"]').val()){
					var val_ = $(this).val();
					$(this).prop('selected', true);
					$('select[name="penilaian"]').select2().select2('val', val_);
				}
			});
			$('input[name="b_save"]').show();
		  }
		}
	});
		
	  $.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/nilai/get_materi',
		data : $.param({
			mk : mkid,
			mtrid : mtrid
		}),
		success : function(msg) {
		  if(msg=="<option value='0'>Silahkan Pilih</option>"){
		  	out = '<option value="0">Tidak Ada Materi</option>';
		  	$('select[name="cmb-materi"]').html(out);
			$('select[name="cmb-materi"]').attr("disabled", true);
			$('select[name="cmb-materi"]').select2().select2("enable", false);		
		  }else{
			$('select[name="cmb-materi"]').html(msg);
			$('select[name="cmb-materi"]').attr("disabled", false);
			$('select[name="cmb-materi"]').select2().select2("enable", true);	
		  }
		}
	  });
 	}
	
	//index prodi
	var prodiid = $('select[name="cmb-prodi-index"]').val();
	var jdwalid = $('input[name="selected-mk"]').val();
	
	if(prodiid=='0'||!prodiid){
	  	out = '<option value="0">Silahkan Pilih Prodi</option>';
	  	// $('select[name="mk_index"]').html(out);
	  	$('select[name="mk_index"]').attr("disabled", true);
  	}else{
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/laboratorium/nilai/get_mk',
			data : $.param({
				prodiid : prodiid,
				jdwalid : jdwalid
			}),
			success : function(msg) {
			  if(msg=="<option value='0'>Silahkan Pilih</option>"){
			  	out = '<option value="0">Tidak Ada Mata Kuliah</option>';
				$('select[name="mk_index"]').html(out);
				$('select[name="mk_index"]').attr("disabled", true);	
				$('select[name="mk_index"]').select2().select2("enable", false);	
			  }else{
				$('select[name="mk_index"]').html(msg);
				$('select[name="mk_index"]').attr("disabled", false);
				$('select[name="mk_index"]').select2().select2("enable", true);
				
			  }
			}
		});
  	}
	
	//new prodi
  	if(prodiid=='0'||!prodiid){
	  	out = '<option value="0">Silahkan Pilih Prodi</option>';
		// $('select[name="cmb-mk"]').html(out);
	  	$('select[name="cmb-mk"]').attr("disabled", true);
  	}else{
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/laboratorium/nilai/get_mk',
			data : $.param({
				prodiid : prodiid,
				jdwalid : jdwalid
			}),
			success : function(msg) {
			  if(msg=="<option value='0'>Silahkan Pilih</option>"){
			  	out = '<option value="0">Tidak Ada Mata Kuliah</option>';
			  	$('select[name="cmb-mk"]').html(out);
				$('select[name="cmb-mk"]').attr("disabled", true);	
				$('select[name="cmb-mk"]').select2().select2("enable", false);
			  }else{
				$('select[name="cmb-mk"]').html(msg);
				$('select[name="cmb-mk"]').attr("disabled", false);
				$('select[name="cmb-mk"]').select2().select2("enable", true);
			  }
			}
		});
  	}
	
	if(jdwalid){
		$('select[name="cmb-mk"]').prop("disabled", false);
		$('select[name="cmb-mk"]').select2().select2("enable", true);
	}
	
});

/*index*/
$("#form-index-submit").change(function() {
	$('#form-index').submit(); //SUBMIT FORM
});

$(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
});

/*write*/
function add_parent(){
	var num = $('.controls-parent').length;
	var output = '';
	output += '<div class="control-group">';
	output += '<div class="controls-parent parent-'+(num+1)+'" data-num="'+(num+1)+'">';
	output += '<input type="hidden" name="cmb-type[]" class="form-control" value="parent"/>';
	output += '<div class="col-md-6" style="padding-left: 0px;padding-right: 0px;">';
	output += '<input type="text" required="required" name="cmb-content[]" class="form-control" style="float: left;"/>';
	output += '</div>';
	output += '<div class="col-md-3 input-group">';
	output += '<input type="text" required="required" name="cmb-percent[]" onkeypress="return isNumberKey(event)" value="100" class="form-control percent parent-percent"/><span class="input-group-addon">%</span>';
	output += '</div>';
	output += '<a href="javascript::" onclick="add_child(this)" class="btn btn-info" style="margin-right: 5px;"><i class="fa fa-plus"></i></a>';
	output += '<a href="javascript::" onclick="delete_(\'parent\',this)" class="btn btn-danger delete-parent"><i class="fa fa-minus"></i></a>';
	output += '<ul class="child" style="list-style-type: none;">';
	output += '</ul>';
	output += '</div>';
	output += '</div>';
	$('.form-add-group').append(output);
	$('.delete-parent').show();
	get_percent('parent','','');
}

function add_child(e){
	var parent = $(e).parent();
	var child = parent.find('ul.child');
	var num = parent.data('num');
	var output = '';
	output += '<li class="controls" style="margin-top: 5px" data-parent="'+num+'">';
	output += '<input type="hidden" name="cmb-type[]" class="form-control" value="child"/>';
	output += '<div class="col-md-6" style="padding-left: 0px;padding-right: 0px;">';
	output += '<input type="text" required="required" name="cmb-content[]" class="form-control" style="float: left;"/>';
	output += '</div>';
	output += '<div class="col-md-3 input-group">';
	output += '<input type="text" required="required" name="cmb-percent[]" onkeypress="return isNumberKey(event)" value="" class="form-control percent child-percent"/><span class="input-group-addon">%</span>';
	output += '</div>';
	output += '<a href="javascript::" onclick="delete_(\'child\',this)" class="btn btn-danger"><i class="fa fa-minus"></i></a>';
	output += '</li>';
	$(child).append(output);
	get_percent('child',e,'');
};

function delete_(str,e){
	if($(e).data('del')){
		var x = confirm("Delete Komponen ? ");
		if(x){
			$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/laboratorium/nilai/delete_komponen',
				data : $.param({
					nilaiid	   : $('input[name="HidNilId"]').val(),
					komponenid : $(e).data('del'),
					str		   : str
				}),
				success : function(msg) {
					var parent = $(e).parent();
					if(str=='parent'){
					  parent.parent().remove();
					  if($('.delete-parent').length==1){
						 $('.delete-parent').hide();
					  };
					}else parent.remove();
					get_percent(str,e,'del');
					set_num(str,e);
				}
			});
		}
	}else{ /*if*/
		var parent = $(e).parent();
		if(str=='parent'){
		  parent.parent().remove();
		  if($('.delete-parent').length==1){
			 $('.delete-parent').hide();
		  };
		}else parent.remove();
		get_percent(str,e,'del');
		set_num(str,e);
	}
};

function get_percent(str,e,param){
	if(str=='parent'){
		var length = $('.parent-percent').length;
		$.each( $('.parent-percent'), function(i, f) {
			$(f).val(100/length);
		});
	}else{
		if(param!='del'){
			var parent = $(e).parent();
		}else{
			var num = $(e).parent().data('parent');
			var parent = $('.parent-'+num);
		}
		var length = parent.find('.child-percent').length;
		$.each(parent.find('.child-percent'), function(i, f) {
			$(f).val(100/length);
		});
	}
};

function set_num(str,e){
  if(str=='parent'){
  	var parent = $('.controls-parent');
	for(var i=0;i<parent.length;i++){
		$(parent[i]).removeAttr('class');
		$(parent[i]).addClass('controls-parent parent-'+(i+1));
		$(parent[i]).attr('data-num', (i+1));
		for( var x=0;x<$(parent[i]).find('.controls').length;x++ ){
			var li = $(parent[i]).find('.controls');
			$(li[x]).attr('data-parent', (i+1));
		}		
	}
  }
};

$(document).on('blur', '.percent', function() {
	if( $(this).val()>100 ){
		alert("Max value 100%");
		$(this).val('100');
	}
	else if( $(this).val()<0 ){
		alert("Min value 0%");
		$(this).val('0');
	}
});

function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 44 || charCode > 57))return false;

    return true;
};

$('select[name="cmb-prodi"]').change(function() {
  var out = '';
  var prodiid = $(this).val();
	
  if(prodiid=='0'||!prodiid){
  	out = '<option value="0">Silahkan Pilih Prodi</option>';
	$('select[name="cmb-mk"]').html(out);
  	$('select[name="cmb-mk"]').attr("disabled", true);
  }else{
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/nilai/get_mk',
		data : $.param({
			prodiid : prodiid,
			jdwalid : ''
		}),
		success : function(msg) {
		  if(msg=="<option value='0'>Silahkan Pilih</option>"){
		  	out = '<option value="0">Tidak Ada Mata Kuliah</option>';
		  	$('select[name="cmb-mk"]').html(out);
			$('select[name="cmb-mk"]').attr("disabled", true);	
			$('select[name="cmb-mk"]').select2().select2("enable", false);	
			
		  }else{
			$('select[name="cmb-mk"]').html(msg);
			$('select[name="cmb-mk"]').attr("disabled", false);
			$('select[name="cmb-mk"]').select2().select2("enable", true);	
		  }
		}
	});
  }
});

$('select[name="cmb-prodi-index"]').change(function() {
  var out = '';
  var prodiid = $(this).val();
	
  if(prodiid=='0'||!prodiid){
  	out = '<option value="0">Silahkan Pilih Prodi</option>';
  	$('select[name="mk_index"]').html(out);
  	$('select[name="mk_index"]').attr("disabled", true);
  }else{
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/nilai/get_mk',
		data : $.param({
			prodiid : prodiid,
			jdwalid : ''
		}),
		success : function(msg) {
		  if(msg=="<option value='0'>Silahkan Pilih</option>"){
		  	out = '<option value="0">Tidak Ada Mata Kuliah</option>';
			$('select[name="mk_index"]').html(out);
			$('select[name="mk_index"]').attr("disabled", true);	
			$('select[name="mk_index"]').select2().select2("enable", false);	
		  }else{
			$('select[name="mk_index"]').html(msg);
			$('select[name="mk_index"]').attr("disabled", false);
			$('select[name="mk_index"]').select2().select2("enable", true);	
			
		  }
		}
	});
  }
});

$('select[name="cmb-mk"]').change(function() {
  var out = '';
  var mkid = $(this).find(':selected').data('mk');

  if(mkid=='0'||!mkid){
  	out = '<option value="0">Silahkan Pilih Mata Kuliah</option>';
	$('select[name="cmb-materi"]').html(out);
  	$('select[name="cmb-materi"]').attr("disabled", true);
  	$('select[name="penilaian"]').html(out);
  	$('select[name="penilaian"]').attr("disabled", true);
  }else{
  	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/nilai/cmb_get_penilaian',
		data : $.param({
			jadwalid : $(this).val(),
			mk : mkid
		}),
		success : function(msg) {
			// alert(msg);
		  if(msg=="<option value='0' data-materi='0'>Silahkan Pilih</option>"){
		  	out = '<option value="0">Tidak Ada Penilaian</option>';
		  	$('select[name="penilaian"]').html(out);
			$('select[name="penilaian"]').attr("disabled", true);	
			$('select[name="penilaian"]').select2().select2("enable", false);	
		  }else{
			$('select[name="penilaian"]').html(msg);
			$('select[name="penilaian"]').attr("disabled", false);
			$('select[name="penilaian"]').select2().select2("enable", true);
		  }
		}
	});
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/nilai/get_materi',
		data : $.param({
			mk : mkid,
			mtrid : ''
		}),
		success : function(msg) {
			// alert(msg);
		  if(msg=="<option value='0'>Silahkan Pilih</option>"){
		  	out = '<option value="0">Tidak Ada Materi</option>';
		  	$('select[name="cmb-materi"]').html(out);
			$('select[name="cmb-materi"]').attr("disabled", true);	
			$('select[name="cmb-materi"]').select2().select2("enable", false);		
		  }else{
			$('select[name="cmb-materi"]').html(msg);	
			// $('select[name="cmb-materi"]').attr("disabled", false);
		  }
		}
	});
 }
});

$('select[name="penilaian"]').change(function() {
	var nilaiid = $(this).val();
	var materiid = $(this).find(':selected').data('materi');
	if(nilaiid!='0'){
		$('select[name="cmb-materi"]').attr("disabled", false);
		$('select[name="cmb-materi"]').select2().select2("enable", true);
		$('input[name="b_save"]').show();
	}else{
		$('input[name="b_save"]').hide();
		$('select[name="cmb-materi"]').attr("disabled", true);		
		$('select[name="cmb-materi"]').select2().select2("enable", false);
	}
	
	if(materiid=='5436534'){materiid='0'}
	
	$('select[name="cmb-materi"] option').each(function() {
		// alert($(this).val());
		if($(this).val()==materiid){
			var val_ = $(this).val();
			$(this).prop('selected', true);
			$('select[name="cmb-materi"]').select2().select2('val', val_);
		}
	});
});

$('.edit-komp-materi').click(function(){
	var materiid =  $(this).data('materi');
	var jadwalid =  $(this).data('jadwal');
	var prodiid =  $(this).data('prodi');
	var nilaiid =  $(this).data('nilai');
	
	var form = document.createElement("form");
    var materi = document.createElement("input");
    var jadwal = document.createElement("input");
    var prodi = document.createElement("input");
    var nilai = document.createElement("input");
	
	form.action = base_url + 'module/laboratorium/nilai/edit_komponen';
	form.method = "post"
	
	materi.name = "materi";
	materi.value = materiid;
	form.appendChild(materi);
	
	jadwal.name = "jadwal";
	jadwal.value = jadwalid;
	form.appendChild(jadwal);
	
	prodi.name = "prodi";
	prodi.value = prodiid;
	form.appendChild(prodi);
	
	nilai.name = "nilai";
	nilai.value = nilaiid;
	form.appendChild(nilai);
	
	document.body.appendChild(form);
	form.submit();
});

$('.edit-komp').click(function(){
	var jadwalid =  $(this).data('jadwal');
	var prodiid =  $(this).data('prodi');
	var nilaiid =  $(this).data('nilai');
	
	var form = document.createElement("form");
    var jadwal = document.createElement("input");
    var prodi = document.createElement("input");
    var nilai = document.createElement("input");
	
	form.action = base_url + 'module/laboratorium/nilai/edit_komponen';
	form.method = "post"
	
	jadwal.name = "jadwal";
	jadwal.value = jadwalid;
	form.appendChild(jadwal);
	
	prodi.name = "prodi";
	prodi.value = prodiid;
	form.appendChild(prodi);
	
	nilai.name = "nilai";
	nilai.value = nilaiid;
	form.appendChild(nilai);
	
	document.body.appendChild(form);
	form.submit();
	
});

$(document).on('click', 'input[name="b_save"]', function(e) {
	var prodi 		= $('[name="cmb-prodi"]').val();
	var mk			= $('[name="cmb-mk"]').val();
	var penilaian	= $('[name="penilaian"]').val();
	
	if(prodi!=0 && mk!=0 && penilaian!=0){
		// alert();
		submit_komponen(e);
	}else{
		alert('Lengkapi data anda!');
	}
	
});

function submit_komponen(e){
	var nilaiid = $('input[name="HidNilId"]').val();
  	
	var formData = new FormData($('#form')[0]);
	var URL = base_url + 'module/laboratorium/nilai/save_komponen';
      $.ajax({
        url : URL,
        type: "POST",
        data : formData,
        async: false,
        success:function(data) 
        {
        	// alert(data);
        	alert('Data Berhasil Disimpan!');
        	if(nilaiid==''){
        		// alert(data);
        		// alert("Success");
        		location.href = base_url + 'module/laboratorium/nilai/index/komponen';
        	}else{
        		var form = document.createElement("form");
			    var input = document.createElement("input");
				
				form.action = base_url + 'module/laboratorium/nilai/index/list';
				form.method = "post"
				
				input.name = "nilai";
				input.value = nilaiid;
				form.appendChild(input);
				
				document.body.appendChild(form);
				form.submit();
        	}
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!');      
        },
        cache: false,
        contentType: false,
        processData: false
    });
    e.preventDefault(); //STOP default action
    return false;
}
