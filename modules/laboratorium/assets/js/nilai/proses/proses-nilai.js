$(document).ready(function() {
	check_delnum();
	
});

function add_KA(){
	var output = '';
	output += '<div class="form-group">';
	output += '<div class="controls">';
	output += '<div class="col-md-6">';
	output += '<input type="text" required="required" name="cmb-newKA[]" class="form-control" style="float: left;"/> ';
	output += '</div>';
	output += '<a href="javascript::" onclick="delete_KA(this)" class="del-button btn btn-danger" style="margin-left: 5px;"><i class="fa fa-minus"></i></a>';
	output += '</div>';
	output += '</div>';
	
	$('#new-KA').append(output);
	
	check_delnum();
};

function delete_KA(e){
	var del = $(e).parent().parent();
	del.remove();
	check_delnum();
}

function delete_KA_view(kpn_id,nilai_id){
	var x = confirm("Delete Komponen ? ");
	if(x){
		$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/laboratorium/nilai/delete_komponen_NA',
			data : $.param({
				komponen : kpn_id,
				nilaiid	 : nilai_id
			}),
			success : function(msg) {
			  location.reload();
			}
		  });
	}
}

function check_delnum(){
	var del_num = $('.del-button').length;
	if(del_num=='1'){
		$('.del-button ').hide();
	}else{
		$('.del-button ').show();
	}
}

$(document).on('click', 'input[name="b_add"]', function(v) {
  var check = 0;
  var jadwalid = $('input[name="jdwl"]').val();
  var nilaiid = $('input[name="nilai_"]').val();
  var prodiid = $('input[name="prodid"]').val();
  $('input[name="cmb-newKA[]"]').each(function(){
  	if($(this).val()=='Nilai Akhir'||$(this).val()=='nilai akhir'){
  		check += 1;
  		// alert('Sudah Terdapat Komponen Nilai AKhir');
  		// linktoProses(jadwalid,prodiid,nilaiid);
  	};
  });
  
  if(check>0){
  	alert('Sudah Terdapat Komponen Nilai AKhir');
  	v.preventDefault(); //STOP default action
    return false;
  }else{
  	$('#form-add-NA').submit(function (e) {
	var formData = new FormData($(this)[0]);
	var URL = base_url + 'module/laboratorium/nilai/add_KA';
      $.ajax({
        url : URL,
        type: "POST",
        data : formData,
        async: false,
        success:function(data) 
        {
        	// alert(data);
        	alert("Success");
        	// // location.href = base_url + 'module/laboratorium/nilai/index/komponen';
        	// location.reload();
        	linktoProses(jadwalid,prodiid,nilaiid);
        	
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!'); 
            linktoProses(jadwalid,prodiid,nilaiid);     
        },
        cache: false,
        contentType: false,
        processData: false
    });
    e.preventDefault(); //STOP default action
    return false;
  });
  }
  
});

$(document).on('click', 'input[name="b_save_data"]', function() {
  $('#form-nilai-mhs').submit(function (e) {
	var formData = new FormData($(this)[0]);
	var URL = base_url + 'module/laboratorium/nilai/save_nilai_mhs';
      $.ajax({
        url : URL,
        type: "POST",
        data : formData,
        async: false,
        success:function(data) 
        {
        	// alert(data);
        	var datas = data.split('/')
        	var jadwalid = datas[0];
        	var prodiid = datas[1];
        	var nilaiid = datas[2];
        	// alert(jadwalid+prodiid+nilaiid);
        	alert("Success");
        	// // location.href = base_url + 'module/laboratorium/nilai/index/komponen';
        	// location.reload();
        	linktoProses(jadwalid,prodiid,nilaiid);
        	
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!');      
        },
        cache: false,
        contentType: false,
        processData: false
    });
    e.preventDefault(); //STOP default action
    return false;
  });
});

function linktoProses(jadwalid,prodiid,nilaiid){
	var form = document.createElement("form");
    var input1 = document.createElement("input");
    var input2 = document.createElement("input");
    var input3 = document.createElement("input");
    
	form.action = base_url + 'module/laboratorium/nilai/proses';
	form.method = "post"
	
	input1.name = "jadwal";
	input1.value = jadwalid;
	form.appendChild(input1);
	
	input2.name = "prodi";
	input2.value = prodiid;
	form.appendChild(input2);
	
	input3.name = "nilai";
	input3.value = nilaiid;
	form.appendChild(input3);
	
	document.body.appendChild(form);
	form.submit();
}

$(document).on('click', 'input[name="b_save"]', function() {
  $('#form-nilai-mhs').submit(function (e) {
	var formData = new FormData($(this)[0]);
	var URL = base_url + 'module/laboratorium/nilai/save_final_nilai_mhs';
      $.ajax({
        url : URL,
        type: "POST",
        data : formData,
        async: false,
        success:function(data) 
        {
        	// alert(data);
        	alert("Success");
        	// // location.href = base_url + 'module/laboratorium/nilai/index/komponen';
        	// location.reload();
        	var datas = data.split('/')
        	var jadwalid = datas[0];
        	var prodiid = datas[1];
        	var nilaiid = datas[2];
        	
        	linktoProses(jadwalid,prodiid,nilaiid);
        	
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!');      
        },
        cache: false,
        contentType: false,
        processData: false
    });
    e.preventDefault(); //STOP default action
    return false;
  });
});

$("#export").click(function() {
	ExcellentExport.excel(this, 'my-view-Table', 'nilai');
	nilaiprocess_(mk,jadwal,'proses')
});