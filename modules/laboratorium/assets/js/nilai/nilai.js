$(document).ready(function(){
	$(".e9").select2();
	$('#jadwal_index').attr("disabled","disabled");
	$('#jadwal').attr("disabled","disabled");
	$('#materimk').attr("disabled",true);
	$('#thn').attr("disabled","disabled");
	$('#desc').html("");
	$('#desc').html("Tambah");
	
	$('#prodi').change(function(){
		var prodiid = $('#prodi').val();
		var URL		= base_url + 'module/laboratorium/nilai/get_mk';
		//alert(prodiid);
		if(prodiid !== '0'){
			$.ajax({
				url : URL,
				type : "POST",
				dataType : "HTML",
		        data : $.param({
					prodiid : prodiid
				}),
		        async: false,
		        success:function(msg) 
		        {
		        	// alert(msg);
		        	if(msg !== ""){
		        		$('#jadwal').attr("disabled",false);
		            	$('#jadwal').html(msg);
		            	$('#jadwal_edit').val("");
		            }
		        }
			})
		}
		else{
			$('#jadwal').attr("disabled",true);
			$('#jadwal').html('<option value="0">Silahkan Pilih</option>');
		}
	});
	
	$('#jadwal').change(function(){
		var jadwal 	= $('#jadwal').val();
		var URL		= base_url + 'module/laboratorium/materi/get_submateri';
		$.ajax({
			url : URL,
			type : "POST",
			dataType : "HTML",
	        data : $.param({
				jadwal : jadwal
			}),
	        async: false,
	        success:function(msg) 
	        {
	        	//alert(msg);
	            if(msg !== ""){
	            	$('#materimk').attr("disabled",false);
	            	$('#materimk').html(msg);
	            	$('#materimk_edit').val("");
	            }
	        }
		})
	});
	
	if($("#hidId").length > 0) {
		var edit = $('#hidId').val();
		if(edit.trim()!==""){
			$('#cancel-btn').show();
			$('#desc').html("");
			$('#desc').html("Edit");
		}else{
			$('#cancel-btn').hide();
			$('#desc').html("");
			$('#desc').html("Tambah");
		}
		
		$('#judul,#materimk,#jadwal,#prodi').change(function(){
			var judul	= $('#judul').val();
			var mk		= $('#mk').val();
			
			if(judul.trim()!=="" || mk !== '0'){
				$('#cancel-btn').show();
			}
			else{
				$('#cancel-btn').hide();
			}
		})
	}
	
	$("#penilaian-form").submit(function(e){
		// alert("masuk");
		//var jadwal 		= $('#jadwal').val();
		if($('#jadwal').val()!=='0' || $('#jadwal_edit').val().trim()==""){
			var jadwal 		= $('#jadwal').val();
		}
		else{
			var jadwal 		= $('#jadwal_edit').val();
		}
		
		if($("#hidId").val() !== ""){
			// alert("A");
			if($('#materimk_edit').val().trim() !==''){
				var materimk 		= $('#materimk_edit').val();
			}
			else{
				var materimk 		= $('#materimk').val();
			}
		}
		else{
			// alert("B");
			var materimk 		= $('#materimk').val();
		}
		
		var judul 		= $('#judul').val();
		var edit 		= $('#hidId').val();
		var prodiid 	= $('#prodi').val();
		
		//alert(materimk);
		if(jadwal.length > 0 && materimk.length > 0 && judul.trim() !== ""){
			var formData = new FormData($(this)[0]);
			var URL = base_url + 'module/laboratorium/nilai/save_penilaian';
			$.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(nilaiid) 
		        {
		            alert("OK, data telah diupdate.");
		            // alert(nilaiid);
		            var data = nilaiid.split('/');
		            // alert(data[0]);
		            // alert(data[1]);
		            if(edit.trim() == ""){
			            var form = document.createElement("form");
					    var input = document.createElement("input");
					    var input2 = document.createElement("input");
						
						form.action = base_url + 'module/laboratorium/nilai/index/'+data[1];
						form.method = "post"
						
						input.name = "nilai";
						input.value = data[0];
						form.appendChild(input);
						
						input2.name = "prodi";
						input2.value = prodiid;
						form.appendChild(input2);
						
						document.body.appendChild(form);
						form.submit();
					}
					else{
						location.href=base_url + 'module/laboratorium/nilai';
					}
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});
		}
		else{
			alert("Form yang anda submit belum lengkap!");
		}
		e.preventDefault(); //STOP default action
		return false;
	});

	//INDEX
	var radio = "mk";
	$('#prodi_index').change(function(){
		var prodiid = $('#prodi_index').val();
		var URL		= base_url + 'module/laboratorium/nilai/get_mk';
		//alert(prodiid);
		if(prodiid !== '0'){
			$.ajax({
				url : URL,
				type : "POST",
				dataType : "HTML",
		        data : $.param({
					prodiid : prodiid
				}),
		        async: false,
		        success:function(msg) 
		        {
		        	//alert(msg);
		        	if(msg !== ""){
		        		$('#jadwal_index').attr("disabled",false);
		            	$('#jadwal_index').html(msg);
		            }
		        }
			})
		}
		else{
			$('#jadwal_index').attr("disabled",true);
			$('#jadwal_index').html('<option value="0">Silahkan Pilih</option>');
		}
	});
	
	$('#jadwal_index').change(function(){
		if(radio == "mk"){
			var jadwalid 	= $('#jadwal_index').val();
			var URL		= base_url + 'module/laboratorium/nilai/get_penilaian';
			$.ajax({
				url : URL,
				type : "POST",
				dataType : "HTML",
		        data : $.param({
					jadwalid : jadwalid
				}),
		        async: false,
		        success:function(msg) 
		        {
		        	if(msg !== ""){
		        		$('#display').empty();
		            	$('#display').html(msg);
		            }
		        }
			})
		}
	});
	
	$("input[name='jenis']").change(function(){
		var jenis = $(this).val();
		$('#jadwal_index').val('0');
		$('#prodi_index').val('0');
		
		$('#jadwal_index').attr("disabled","disabled");
		if(jenis == "materi"){
			radio = "materi";
			
			//alert(radio);
			var output = "<label class='control-label'>Materi</label>";
			 output += "<div class='controls'>";
			 output += "<select name='materi_index' id='materi_index' class='form-control e8'>";
			 output += "<option value='0'>Silahkan Pilih</option>";
			 output += "</select>";
			 output += "</div>";
			$('#materi-option').html(output);
			$(".e8").select2();
			$('#materi_index').attr("disabled",true);
			
			$('#jadwal_index').change(function(){
				var jadwalid 	= $('#jadwal_index').val();
				var URL			= base_url + 'module/laboratorium/nilai/get_materi';
				
				$.ajax({
					url : URL,
					type : "POST",
					dataType : "HTML",
			        data : $.param({
						jadwalid : jadwalid
					}),
			        async: false,
			        success:function(msg) 
			        {
			        	//alert(msg);
			            if(msg !== ""){
			            	$('#materi_index').attr("disabled",false);
			            	$('#materi_index').html(msg);
			            }
			        }
				})
			});
			
			$('#materi_index').change(function(){
				var jadwalid 	= $('#jadwal_index').val();
				var materiid 	= $('#materi_index').val();
				var URL			= base_url + 'module/laboratorium/nilai/get_penilaian';
				$.ajax({
					url : URL,
					type : "POST",
					dataType : "HTML",
			        data : $.param({
						jadwalid : jadwalid,
						materi : materiid
					}),
			        async: false,
			        success:function(msg) 
			        {
			        	$('#display').empty();
			        	$('#display').html(msg);
			        }
				})
			});
		}
		else{
			radio = "mk";
			$('#materi-option').empty();
		}
	});
});

$(document).on('click', '.proses-nilai', function(){
	var jadwalid = $('select[name="jadwal_index"]').val();
	var prodiid  = $('select[name="prodi_index"]').val();
	// location.href= base_url + 'module/laboratorium/nilai/proses/' + jadwalid;
	var form = document.createElement("form");
    var input1 = document.createElement("input");
    var input2 = document.createElement("input");
    var input3 = document.createElement("input");
    
	form.action = base_url + 'module/laboratorium/nilai/proses';
	form.method = "post"
	
	input1.name = "jadwal";
	input1.value = jadwalid;
	form.appendChild(input1);
	
	input2.name = "prodi";
	input2.value = prodiid;
	form.appendChild(input2);
	
	input3.name = "nilai";
	input3.value = '';
	form.appendChild(input3);
	
	document.body.appendChild(form);
	form.submit();
});

function doDelete(nilai,jdwl){
	var x = confirm("Are you sure you want to delete?");
	var url = base_url + 'module/laboratorium/nilai/delete_nilai';
 	if (x){
	  	$.ajax({
			type : "POST",
			dataType : "html",
			url : url,
			data : $.param({
				nilai : nilai,
				jdwl : jdwl
			}),
			success : function(msg) {
				if(msg!==""){
					$('.penilaian-'+nilai).fadeOut();
				}
			}
		});
	}
};

function update_status(nilai,status){
	var url = base_url + 'module/laboratorium/nilai/status_penilaian';
	// alert(nilai);
	$.ajax({
		url : url,
		type : "POST",
		dataType : "HTML",
        data : $.param({
			nilai : nilai,
			status : status
		}),
        async: false,
        success:function(msg) 
        {
        	alert(msg);
        	location.reload();
        }
	})
}

function edit_nilai_mhs(nilai,is_proses){
	var form = document.createElement("form");
    var input = document.createElement("input");
    var input2 = document.createElement("input");
    
	form.action = base_url + 'module/laboratorium/nilai/index/list';
	form.method = "post"
	
	input.name = "nilai";
	input.value = nilai;
	form.appendChild(input);
	
	input2.name = "isproses";
	input2.value = is_proses;
	form.appendChild(input2);
	
	document.body.appendChild(form);
	form.submit();
}
