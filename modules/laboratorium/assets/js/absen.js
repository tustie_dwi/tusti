$(document).ready(function() {
	$('#calendarModal').hide();
	
	$(".tgl").datetimepicker({format: 'yyyy-mm-dd',pickTime: false});
	$(".jam").datetimepicker({format: 'hh:ii',pickDate: false});
	$(".date").datetimepicker({format: 'yyyy-mm-dd hh:ii',pickTime: false});
	
	$('.input-tgl').datetimepicker({
       pickTime : false,
	   icons: {
					time: "fa fa-clock-o",
					date: "fa fa-calendar",
					up: "fa fa-arrow-up",
					down: "fa fa-arrow-down"
				}
    });
	
    $('.input-jam').datetimepicker({
		pickDate:false,
		pickTime : true,
		icons: {
					time: "fa fa-clock-o",
					date: "fa fa-calendar",
					up: "fa fa-arrow-up",
					down: "fa fa-arrow-down"
				}
    });
	
	
	
	$('.show-calendar-it').click(function(e){		
		e.preventDefault();
		$('content-absen').hide();
		$('#calendarModal').hide();		
		$(this).parent('small').parent('div').parent('td').children('.hide-it').fadeIn().removeClass('hide-it');
		$(this).hide();		
		$('#calendarModal').hide();
	});
		
	$('.popup-form').click(function(e){	
		e.preventDefault();
		$('content-absen').show();
		$('#calendarModal').show('slow');
		
		var $year=$(this).data('calendaryear');
		var $month=$(this).data('calendarmonth');
		var $day=$(this).data('calendarday')
		var day = ("0" + $day).slice(-2);
		var month = ("0" + ($month + 1)).slice(-2);
		var today = $year+"-"+(month)+"-"+(day);
		$('#calendarModal .input-datetime').val(today);
		$('#calendarModal .input-datetime').datepicker();
		//$('#calendarModal').modal('toggle');
	
		});
		
	$('#btn-modal-close').click(function(e){
		e.preventDefault();	
		$('#calendarModal').hide();
	});
	
	$('.btn-save-absen').on('click',function() {
		
		$('#status-save').html("&nbsp;");
		btn = $(this);
		btn.button('loading');
		
		$.post(			
			base_url + 'module/laboratorium/absen/save',
			$('#form-save-absen').serialize(),
			
			function(data1){
				btn.button('reset');
				if(data1.status.trim() == "OK") {
					$('.status-save').html(data1.modified);
				}
				else {
					$('.status-save').html();
					alert(data1.error);
				}
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
		});
		
		return false;
	});
	
	$("#cmbbulan, #cmbyear").on('change',function() {
		
		var month	= $(document.getElementById("cmbbulan")).val();		
		var year	= $(document.getElementById("cmbyear")).val();

			$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/laboratorium/absen/view_calendar',
				data : $.param({
					cmbbulan : month,
					cmbyear  : year
				}),
				success : function(msg) {
					$("#absen").html(msg);					
				}
			});	
			return false;
	});
	
	
	$('#absens').change(function() {
		
		var semester 	= document.getElementById("cmbsemester");
		var semesterid	= $(semester).val();
				
		var mk 			= document.getElementById("cmbmk");
		var mkid		= $(mk).val();
		
		var pengampu	= document.getElementById("cmbdosen");
		var pengampuid	= $(pengampu).val();
		
		var prodi		= document.getElementById("cmbprodi");
		var prodiid		= $(prodi).val();
		
		var kelas		= document.getElementById("cmbkelas");
		var kelasid		= $(kelas).val();
				
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'module/laboratorium/absen',
				data : $.param({
					cmbsemester : semesterid,
					cmbmk 		: mkid,
					cmbdosen 	: pengampuid,
					cmbprodi 	: prodiid,
					cmbkelas 	: kelasid
				}),
				success : function(msg) {
					if (msg == '') {
						$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					} else {
						$("#content").html(msg);
					}
				}
			});
	
	});
});



function get_jenis_rekap(){
	
	var jenis 	= document.getElementById("cmbjenis");
	var jenisid	= $(jenis).val();
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/detail_rekap',
		data : $.param({
			cmbjenis : jenisid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {
				$("#form-rekap-absen").html(msg);
				$("#content-rekap").html("");
			}
		}
	});
}

function get_mk(){
	
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_absen_by_mk',
		data : $.param({
			cmbsemester : semesterid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {
				$("#cmbmk").html(msg);
			}
		}
	});
}

function get_dosen(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var mk 			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_absen_by_dosen',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {
				$("#cmbdosen").html(msg);
			}
		}
	});
}

function get_prodi(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var mk 			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var pengampu	= document.getElementById("cmbdosen");
	var pengampuid	= $(pengampu).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_absen_by_prodi',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {
				$("#cmbprodi").html(msg);
			}
		}
	});
}

function get_kelas(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var mk 			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var pengampu	= document.getElementById("cmbdosen");
	var pengampuid	= $(pengampu).val();
	
	var prodi		= document.getElementById("cmbprodi");
	var prodiid		= $(prodi).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_absen_by_kelas',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid,
			cmbprodi	: prodiid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {
				$("#cmbkelas").html(msg);
			}
		}
	});
}

function get_sesi(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var mk 			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var pengampu	= document.getElementById("cmbdosen");
	var pengampuid	= $(pengampu).val();
	
	var prodi		= document.getElementById("cmbprodi");
	var prodiid		= $(prodi).val();
	
	var kelas		= document.getElementById("cmbkelas");
	var kelasid		= $(kelas).val();
	
	var sesi		= document.getElementById("cmbsesi");
	var sesiid		= $(sesi).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_absen_by_sesi',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid,
			cmbprodi	: prodiid,
			cmbkelas	: kelasid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {
				$("#cmbsesi").html(msg);
			}
		}
	});
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_hadir',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid,
			cmbprodi	: prodiid,
			cmbkelas	: kelasid,
			cmbsesi		: sesiid
		}),
		success : function(msg) {
			if (msg == '') {
				$("#content-absen").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
			} else {
				
				$("#content-absen").html(msg);
			}
		}
	});
	
}

function get_hadir(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var mk 			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var pengampu	= document.getElementById("cmbdosen");
	var pengampuid	= $(pengampu).val();
	
	var prodi		= document.getElementById("cmbprodi");
	var prodiid		= $(prodi).val();
	
	var kelas		= document.getElementById("cmbkelas");
	var kelasid		= $(kelas).val();
	
	var sesi		= document.getElementById("cmbsesi");
	var sesiid		= $(sesi).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_frm_absen',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid,
			cmbprodi	: prodiid,
			cmbkelas	: kelasid,
			cmbsesi		: sesiid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {				
				$("#form-content-absen").html(msg);
				$('.date').datetimepicker({
					format: 'yyyy-mm-dd hh:ii', pickTime: true
				});
			}
		}
	});
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/get_hadir',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid,
			cmbprodi	: prodiid,
			cmbkelas	: kelasid,
			cmbsesi		: sesiid
		}),
		success : function(msg) {
			if (msg == '') {
				$("#content-absen").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
			} else {
				
				$("#content-absen").html(msg);
			}
		}
	});
}

function get_rekap_dosen(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var mk 			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var pengampu	= document.getElementById("cmbdosen");
	var pengampuid	= $(pengampu).val();
	
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/data_rekap',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid
		}),
		success : function(msg) {
			if (msg == '') {
				$("#content-rekap").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
			} else {
				
				$("#content-rekap").html(msg);
			}
		}
	});
}

function get_rekap_mhs(){
	var semester 	= document.getElementById("cmbsemester");
	var semesterid	= $(semester).val();
	
	var mk 			= document.getElementById("cmbmk");
	var mkid		= $(mk).val();
	
	var pengampu	= document.getElementById("cmbdosen");
	var pengampuid	= $(pengampu).val();
	
	var prodi		= document.getElementById("cmbprodi");
	var prodiid		= $(prodi).val();
	
	var kelas		= document.getElementById("cmbkelas");
	var kelasid		= $(kelas).val();
	
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/laboratorium/absen/data_rekap_mhs',
		data : $.param({
			cmbsemester : semesterid,
			cmbmk		: mkid,
			cmbdosen	: pengampuid,
			cmbprodi	: prodiid,
			cmbkelas	: kelasid
		}),
		success : function(msg) {
			if (msg == '') {
				$("#content-rekap").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
			} else {
				$("#content-rekap").html(msg);
			}
		}
	});
}