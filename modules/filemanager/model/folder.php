<?php
class model_folder extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_folder_name_mutu($folder_id=NULL){
		$sql = "SELECT tbl_gjm_kategori.keterangan as folder_name, parent_id, tbl_gjm_kategori.kategori_id as id 
				FROM db_ptiik_apps.tbl_gjm_kategori
				WHERE 1";
		if($folder_id!=""){
			$sql .= " AND tbl_gjm_kategori.kategori_id = '".$folder_id."'";
		}
		$dt = $this->db->getRow( $sql );
		return $dt;
	}
	
	/* file */
	function read_file_mutu($id=NULL, $unit=NULL, $file=NULL) {		
		$sql = "SELECT DISTINCT
				MID( MD5( db_ptiik_apps.tbl_gjm_file.file_id), 6, 6) AS id,
				db_ptiik_apps.tbl_gjm_file.file_id,
				tbl_gjm_kategori.kategori_id as folder_id,
				tbl_gjm_kategori.kategori_id,
				tbl_gjm_kategori.parent_id,
				tbl_gjm_kategori.urut,
				if(ISNULL(tbl_unit_kerja.keterangan), 'PTIIK', tbl_unit_kerja.keterangan) AS folder_name,
				tbl_gjm_file.file_loc,
				tbl_gjm_file.file_size,
				tbl_gjm_file.file_name,
				tbl_gjm_file.file_type,
				tbl_gjm_file.jenis_file,
				tbl_gjm_file.last_update
				FROM
				db_ptiik_apps.tbl_gjm_kategori
				INNER JOIN db_ptiik_apps.tbl_gjm_file ON tbl_gjm_kategori.kategori_id = tbl_gjm_file.kategori_id
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_gjm_file.unit_id = tbl_unit_kerja.unit_id
				WHERE 1
			   ";
		if($id){
			$sql .= " AND tbl_gjm_file.kategori_id = '".$id."'";
		}
		if($unit){
				$sql .= " AND tbl_gjm_file.unit_id = '".$unit."'";
		}
			
		if($file){
			$sql .= " AND tbl_gjm_file.file_id = '".$file."'";
		}
		
		//echo $sql;
		
		if($file){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
	}
	
	
	
	function read_child_mutu($id=NULL){
		$sql = "SELECT DISTINCT
					MID( MD5( db_ptiik_apps.tbl_gjm_file.kategori_id), 6, 6) AS folder_id,
					tbl_gjm_kategori.kategori_id as id,
					tbl_unit_kerja.unit_id,
					tbl_gjm_kategori.parent_id,
					tbl_gjm_kategori.urut,
					if(ISNULL(tbl_unit_kerja.keterangan), 'PTIIK', tbl_unit_kerja.keterangan) AS folder_name
					FROM db_ptiik_apps.tbl_gjm_kategori INNER JOIN db_ptiik_apps.tbl_gjm_file ON tbl_gjm_kategori.kategori_id = tbl_gjm_file.kategori_id LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_gjm_file.unit_id = tbl_unit_kerja.unit_id
					WHERE 1 ";
		if($id){
			$sql .= " AND tbl_gjm_kategori.kategori_id = '".$id."'";
		}
		else{
			$sql .= " AND tbl_gjm_kategori.parent_id = '0'";
		}
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function read_mutu($id=NULL){
		$sql = "SELECT DISTINCT
					MID( MD5( db_ptiik_apps.tbl_gjm_kategori.kategori_id), 6, 6) as folder_id,
					db_ptiik_apps.tbl_gjm_kategori.kategori_id as id,
					db_ptiik_apps.tbl_gjm_kategori.keterangan as folder_name,
					db_ptiik_apps.tbl_gjm_kategori.parent_id,
					db_ptiik_apps.tbl_gjm_kategori.urut
					FROM
					db_ptiik_apps.tbl_gjm_kategori WHERE 1 ";
		if($id){
			$sql .= " AND tbl_gjm_kategori.parent_id = '".$id."'";
		}
		else{
			$sql .= " AND tbl_gjm_kategori.parent_id = '0'";
		}
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;
	}
	
	/* folder */
	function read($id=NULL) {		
		$sql = "SELECT MID( MD5( tbl_folder.id), 6, 6) as folder_id,
					   tbl_folder.*
				FROM db_ptiik_apps.tbl_folder
				WHERE 1
			   ";
		if($id){
			$sql .= " AND tbl_folder.parent_id = '".$id."'";
		}
		else{
			$sql .= " AND tbl_folder.parent_id = '0'";
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function read_parent($id=NULL) {		
		$sql = "SELECT MID( MD5( tbl_folder.id), 6, 6) as folder_id,
					   tbl_folder.*
				FROM db_ptiik_apps.tbl_folder
				WHERE 1 
			   ";
		if($id && ($id!=0)){
			$sql .= " AND tbl_folder.id = '".$id."'";
			$result = $this->db->getRow( $sql );
		}else{
			$result="";
		}
		
		
		return $result;
	}
	
	function folder_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_folder
			where left(id,6) = '".date("Ym")."'";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_folder($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_folder', $data);
	}
	
	/* file */
	function read_file($id=NULL, $file=NULL) {		
		$sql = "SELECT MID( MD5( tbl_folder_file.file_id), 6, 6) as id, tbl_folder_file.*
				FROM db_ptiik_apps.tbl_folder_file
				LEFT JOIN db_ptiik_apps.tbl_folder ON tbl_folder.id = tbl_folder_file.folder_id
				WHERE 1
			   ";
		if($id){
			$sql .= " AND tbl_folder_file.folder_id = '".$id."'";
		}
		else{
			if($file){
				$sql .= " AND tbl_folder_file.file_id = '".$file."'";
			}else{	
				$sql .= " AND tbl_folder_file.folder_id = '0'";
			}
		}
		
		
		if($file){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
	}
	
	
	
	function file_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_folder_file WHERE left(file_id,6) = '".date("Ym")."' ";
		
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_id_from_parent($folder_id){
		$sql = "SELECT MID( MD5(tbl_folder.id), 6, 6) as id
				FROM db_ptiik_apps.tbl_folder
				WHERE 1";
		if($folder_id!=""){
			$sql .= " AND MID( MD5(parent_id), 6, 6) = '".$folder_id."'";
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_folder($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_folder
				WHERE MID( MD5(id), 6, 6) = '".$id."'";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_folder_name($folder_id=NULL){
		$sql = "SELECT tbl_folder.folder_name, parent_id, tbl_folder.id
				FROM db_ptiik_apps.tbl_folder
				WHERE 1";
		if($folder_id!=""){
			$sql .= " AND tbl_folder.id = '".$folder_id."'";
		}
		$dt = $this->db->getRow( $sql );
		return $dt;
	}
	
	function replace_file($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_folder_file', $data);
	}
	
	function delete_file($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_folder_file
				WHERE MID( MD5(file_id), 6, 6) = '".$id."'";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function delete_file_by_folder($id){
		$sql = "DELETE 
				FROM db_ptiik_apps.tbl_folder_file
				WHERE MID( MD5(folder_id), 6, 6) = '".$id."'";	
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function rename_folder($id, $folder_name){
		$sql = "UPDATE db_ptiik_apps.tbl_folder
				SET folder_name = '".$folder_name."'
				WHERE id = '".$id."'";
		$result = $this->db->query( $sql );
		return $result;
	}
}
?>