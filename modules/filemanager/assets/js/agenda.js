$(function() {
	 $("#ruang").autocomplete({ 
			source: base_url + "/module/filemanager/conf/ruang",
			minLength: 0, 
			select: function(event, ui) { 
				$('#ruangid').val(ui.item.id); 
				$('#ruang').val(ui.item.value);
			} 
		});  
	
		$("#prodi").autocomplete({ 
			source: base_url + "/module/filemanager/conf/prodi",
			minLength: 0, 
			select: function(event, ui) { 
				$('#prodiid').val(ui.item.id); 
				$('#prodi').val(ui.item.value);
			} 
		});  
		
	 $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	 
	

	$('#writeTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});

	$('.undangan .btn').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $collapse = $this.closest('.collapse-group').find('.collapse');
		$collapse.collapse('toggle');
	});
	
	// $('.multiselect').multiselect();
		 $("#cmbmulti").select2();
		 $("#cmbunit").select2();
		 $("#cmbruang").select2();
	
	  $(".tagPemateri").tagsManager({
		prefilled: $('.tmppemateri').val(),
		deleteTagsOnBackspace: true,
		preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/filemanager/conf/staff",
		AjaxPush: base_url + "/module/filemanager/conf/staff/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidPemateri'
      });
	  
	 $(".tagPanitia").tagsManager({
		prefilled: $('.tmppanitia').val(),
		preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/filemanager/conf/staff",
		AjaxPush: base_url + "/module/filemanager/conf/staff/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidPanitia'
      });
	  
	  $(".tagUndangan").tagsManager({
		prefilled: $('.tmpundangan').val(),
		deleteTagsOnBackspace: true,
		preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/filemanager/conf/staff",
		AjaxPush: base_url + "/module/filemanager/conf/staff/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidUndangan'
      });
	  
	  $(".tagStaff").tagsManager({
		prefilled: $('.tmpstaff').val(),	
		deleteTagsOnBackspace: true,	
        preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/filemanager/conf/staff",
		AjaxPush: base_url + "/module/filemanager/conf/staff/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidStaff'
      });
	  
	   $(".tagMhs").tagsManager({		
		prefilled: $('.tmpmhs').val(),
		deleteTagsOnBackspace: true,
        preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: base_url + "/module/filemanager/conf/pesertamhs",
		AjaxPush: base_url + "/module/filemanager/conf/pesertamhs/push",
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidMhs'
      });
	  
	   $(".tagPeserta").tagsManager({	
		deleteTagsOnBackspace: true,
		prefilled: $('.tmppeserta').val(),
        preventSubmitOnEnter: true,
         blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidPeserta'
      }); 
	 
	 $(".tagAngkatan").tagsManager({
		deleteTagsOnBackspace: true,
		prefilled: $('.tmpangkatan').val(),
        preventSubmitOnEnter: true,
         blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidAngkatan'
      });
	
  });
	
	
