$(function(){
	
	$(".btn-delete-post").click(function(){
		
			var pid = $(this).parents('tr').data("id");
			/*var aRow = $(this).closest('tr');
			var nRow = aRow[0];*/
			
			var oTable = $('#example').dataTable();
			var nRow = $(this).closest("tr");
			var rowNumber = nRow.index();
			   
		  // Immediately remove the first row
			//oTable.fnDeleteRow(oTable.fnGetPosition(rowNumber));
			//alert ("a"+rowNumber);
			//_row = $(this).parents('tr')[0];
			//$('#example').dataTable().remove( $(this).parents('tr')[0], '123', false, false );
			
			if(confirm("Delete this post? Once done, this action can not be undone.")) {
			
			var row = $(this).parents('tr');
								
			$.post(
				base_url + 'module/filemanager/dosen/delete/' + pid,
				function(data){
					if(data.status.trim() == "OK")
						row.fadeOut();
					else alert(data.error);
				},
				"json"
				).error(function(xhr) {
					alert(xhr.responseText);
				});
				
			}
		});
	
	    $('.myModal').modal({
		keyboard: false
		});
});