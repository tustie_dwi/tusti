$(document).ready(function(){
	$(".close_btn").click(function(){
		$("#parent").val("0");
		location.href = base_url + 'module/filemanager/folder';
	});
	
	$(".close_btn_file").click(function(){
		$("#folder").val("0");
		location.href = base_url + 'module/filemanager/folder';
	});
	
	$(".close_btn_file_upload").click(function(){
		var folder 	= document.getElementById("folder");
		var folderid	= $(folder).val();
		
		view_content(folderid);
		//location.href = base_url + 'module/filemanager/folder';
	});
	
	$("#newfolder_form").submit(function(e){
		var name = $("#folder_name").val();
		if(name.length > 0){
			var formData = new FormData($(this)[0]);
			var URL = base_url + 'module/filemanager/folder/save';
			$.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.href = base_url + 'module/filemanager/folder';
		        },
		        error: function(msg) 
		        {
		            alert ("ERROR!");     
		            location.href = base_url + 'module/filemanager/folder';
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});
			e.preventDefault(); //STOP default action
			return false;
		}
		else{
			alert("Create folder process failed!\nPlease check your input data!");
		}
	});
	
	var countfile = 1;
	$("#add_file_form").click(function(){
		countfile += 1;
		var newfile = "<div class='row'><div class='col-sm-6'><div class='form-group'>";
			newfile += "<input type='text' required='required' name='title[]' autocomplete='off' class='form-control' id='title' value='-' /></div></div>";
			newfile += "<div class='col-sm-6'><div class='form-group'>";
		    newfile += "<input type='file' required='required' name='uploads[]' multiple='multiple' autocomplete='off'";
		    newfile += "id='uploads'/></div></div></div>";
		$("#newform").append(newfile);
		$('#count_new_file').val(countfile);
	});
	
	$("#newfile_form").submit(function(e){
		var folder 	= document.getElementById("folder");
		var folderid	= $(folder).val();
		
		
				
		var formData = new FormData($(this)[0]);
		var URL = base_url + 'module/filemanager/folder/save_file';
		$.ajax({
            url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert(msg);
				view_content(folderid);
	           // location.href = base_url + 'module/filemanager/folder';
	        },
	        error: function(msg) 
	        {
	            alert ("ERROR!");     
				view_content(folderid);
	            //location.href = base_url + 'module/filemanager/folder';
	        },
	        cache: false,
	        contentType: false,
	        processData: false
		});
		e.preventDefault(); //STOP default action
		return false;
	});
});

$(function () {
	$('.tree li').hide();
    $('.tree li:first').show();
   // $('.tree li:first span i').removeClass("fa-folder-open-o");
   // $('.tree li:first span i').addClass("fa-folder-o");
    
    
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
		// $(this).parent().removeClass('active');
		//$('.tree li.parent_li > span').addClass('badge');
		   
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
        	$(this).find("i").removeClass("fa-folder-open-o");
        	$(this).find("i").addClass("fa-folder-o");
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
        	$(this).find("i").removeClass("fa-folder-o");
        	$(this).find("i").addClass("fa-folder-open-o");
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
	
	
   var treeHeight=$(window).height()-$('header').height()-200;
   $('.tree > ul > li.parent_li').css('max-height',treeHeight);
   
  });

function newfolder_parent(id){
	$("#parent").val(id);
}

function newfile_parent(id, loc){
	//alert(loc);
	$("#folder").val(id);
	$("#folder_loc").val(loc);
}

function view_content(id){
	
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/filemanager/folder/view_content',
		data : $.param({
			id : id
		}),
		success : function(msg) {
			//$(".tree_li[f_id='"+id+"']").addClass('badge');
			$(".tree li.parent_li > span").removeClass('badge');
			$(".tree li.parent_li > span[f_id='"+id+"']").addClass('badge');
			$("#content-table").show();
			$("#content-file").hide();
			$("#view_content").html(msg);
		}
	});
}

function view_content_mutu(id, unit){
	
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/filemanager/folder/view_content_mutu',
		data : $.param({
			id : id,
			unit : unit
		}),
		success : function(msg) {
			//$(".tree_li[f_id='"+id+"']").addClass('badge');
			$(".tree li.parent_li > span").removeClass('badge');
			$(".tree li.parent_li > span[f_id='"+id+"']").addClass('badge');
			$("#content-table").show();
			$("#content-file").hide();
			$("#view_content").html(msg);
		}
	});
}

function view_content_mutu_child(id, unit){
	
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/filemanager/folder/view_content_mutu_child',
		data : $.param({
			id : id,
			unit : unit
		}),
		success : function(msg) {
			//$(".tree_li[f_id='"+id+"']").addClass('badge');
			$(".tree li.parent_li > span").removeClass('badge');
			$(".tree li.parent_li > span[f_id='"+id+"']").addClass('badge');
			$("#content-table").show();
			$("#content-file").hide();
			$("#view_content").html(msg);
		}
	});
}

function doView(id, folder){	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/filemanager/folder/views/'+id+'/'+folder,
			success : function(msg) {	
				$("#content-table").hide();
				$("#content-file").show();
				$("#content-file").html(msg);
				//location.reload();				
			}
		});
}

function doViewMutu(id, folder){	
	$.ajax({			
			type : "POST",
			dataType : "html",
			url : base_url + 'module/filemanager/folder/views_mutu/'+id+'/'+folder,
			success : function(msg) {	
				$("#content-table").hide();
				$("#content-file").show();
				$("#content-file").html(msg);
				//location.reload();				
			}
		});
}

function doDelete(i,jenis_del) {
	var del_id = i;
	var jenis_del = jenis_del;
	var x = confirm("Are you sure you want to delete?");
 	if (x){
 		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/filemanager/folder/delete',
			data : $.param({
				delete_id : del_id,
				jenis_del : jenis_del
			}),
			success : function(msg) {
				if (msg) {
					alert("Data Berhasil Terhapus!");
					if(jenis_del == 'file'){
						$(".file"+del_id).fadeOut();
					}
					else if(jenis_del == 'folder'){
						location.reload();
					}
				}else {
					alert('gagal');
				}
			}
		});
	}
	else {
	   location.reload();
	}
};


function rename(id,name){
	//alert(id+"\n"+name);
	$("#id").val(id);
	$("#folder_name").val(name);
};
