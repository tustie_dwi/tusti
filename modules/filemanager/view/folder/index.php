<?php $this->head(); ?>
<style>
.tree {
	
	font-size:100%;
    min-height:20px;
    margin-bottom:20px;
    background-color:#fff;
 
}

.tree li {
    list-style-type:none;
    margin:0;
    padding:4px 2px 0 4px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-10px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:10px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:10px;
    top:15px;
    width:15px
}
.tree li span {
    border:1px solid #999;
    display:inline-block;
    padding:2px 4px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:15px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}

.tree>ul>li.parent_li{
max-height:200px;
overflow:auto;
}

.tree>ul{
	padding:0;
}
</style>
<?php
$level = $this->coms->authenticatedUser->level; 

if(($level=="1")){
?>
<div class="row">
	<div class="col-sm-3">
		<a href="#" data-toggle="modal" data-target="#newfolder" class="btn btn-primary"><i class="fa fa-folder-o"></i> New Folder</a>
		<a href="#" data-toggle="modal" data-target="#newfile" class="btn btn-primary"><i class="fa fa-cloud-upload"></i> Upload File</a>
	</div>
</div>
<?php } ?>
<div class="row">
	<!--BODY--->
	<div class="col-sm-6">
		<div class="tree">
			
			<ul>
				<li>				 	
				 	<span class="span" onclick="view_content('0')"><i class="fa fa-folder-open-o"></i> PTIIK Files</span>
		 			<ul>
						<?php 
						
						if( isset($posts) ){
							
							 foreach ($posts as $d) { 
								if(($level=="1")||($level=="47")||($level=="46")){
							 ?>
							 	<li>							 		
							 		<span class="span text text-default" f_id="<?php echo $d->id ?>" onclick="view_content('<?php echo $d->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $d->folder_name ?></span>&nbsp;
									<a title="rename" class='btn-edit-post btn-add' href="#" data-toggle="modal" data-target="#newfolder" onclick="rename('<?php echo $d->id ?>','<?php echo $d->folder_name ?>')"><i class="fa fa-gear"></i></a>&nbsp;
							 		<a title="create new folder" class='btn-edit-post btn-add' data-toggle="modal" data-target="#newfolder" onclick="newfolder_parent('<?php echo $d->id ?>')" href="#"><i class="fa fa-plus"></i></a>&nbsp;
							 		<a title="upload file" class='btn-edit-post btn-add' data-toggle="modal" data-target="#newfile" href="#" onclick="newfile_parent('<?php echo $d->id ?>','<?php echo $d->folder_name; ?>')"><i class="fa fa-cloud-upload"></i></a>&nbsp;
							 		<a title="delete folder" class='btn-edit-post btn-add' href="#" onclick="doDelete('<?php echo $d->folder_id ?>','folder')"><i class="fa fa-trash-o"></i></a>&nbsp;
							 		<?php echo $this->detailchild( $d->id, $d->folder_name) ?>
							 	</li>
							 <?php
								}else{
									?>
									<li><span class="span text text-default" f_id="<?php echo $d->id ?>" onclick="view_content('<?php echo $d->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $d->folder_name ?></span>
									<?php echo $this->detailchild( $d->id, $d->folder_name) ?></li>
									<?php
								}
							 } 
						}
						
						
						?>
						
						<?php
					if(isset($mutu)):
							 foreach ($mutu as $key) { 							
								?>
								<li><span class="span text text-default" f_id="<?php echo $key->id ?>" onclick="view_content_mutu('<?php echo $key->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $key->folder_name ?></span>
								<?php echo $this->detailchild_mutu( $key->id, $key->folder_name) ?></li>
								<?php
							 } 
						endif;
					?>
				    </ul>
					
				</li>
	    	
		     </ul>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="block-box">
			<div id="content-table">
				<div id="view_content">
				
				</div>
			
			</div>
		
			<div id="content-file"></div>
		</div>
	</div>
</div>

<!-- Modal Folder-->
<div class="modal fade" id="newfolder" tabindex="-1" role="dialog" aria-labelledby="newfolderLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_btn" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="newfolderLabel">Edit/Create Folder</h4>
      </div>
      <form id="newfolder_form">
	      <div class="modal-body">
        	  <div class="form-group">
	        	<label for="folder_name">Folder Name</label>
	        	<input type="text" name="folder_name" autocomplete="off" class="form-control" id="folder_name"/>
	        	<input type="hidden" name="parent" class="form-control" id="parent" value="0"/>
        	  </div>
	      </div>
	      <div class="modal-footer">
			<input type="hidden" name="id" class="form-control" id="id"/>
	        <button type="button" class="btn btn-default close_btn" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
       </form>
    </div>
  </div>
</div>

<!-- Modal File-->
<div class="modal fade" id="newfile" tabindex="-1" role="dialog" aria-labelledby="newfolderLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_btn_file_upload" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="newfolderLabel">Upload New File</h4>
      </div>
      <form id="newfile_form" enctype="multipart/form-data">
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="col-sm-12">
	      			<button type="button" id="add_file_form"><i class="fa fa-plus"></i></button>
      			</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-sm-6">
		        	<div class="form-group">			        
			        	<input type="text" name="title[]" required="required" autocomplete="off" class="form-control" id="title" value="-" />
		        	</div>
	        	</div>
	        	<div class="col-sm-6">
		        	<div class="form-group">
			        	<input type="file" name="uploads[]" required="required" autocomplete="off" class="span12" id="uploads"/>
		        	</div>
	        	</div>	
			</div>     
	        	<div id="newform">
	        		
	        	</div>
        	   	
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="count_new_file" class="form-control" id="count_new_file" value="1"/>
	      	<input type="hidden" name="folder" class="form-control" id="folder" value="0"/>
			<input type="hidden" name="folder_loc" class="form-control" id="folder_loc"/>
	        <button type="button" class="btn btn-default close_btn_file_upload" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
       </form>
    </div>
  </div>
</div>

<?php $this->foot(); ?>