<?php
class filemanager_folder extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth');
	}
	
	function index(){
		$mfolder = new model_folder();
		
		$data['posts'] = $mfolder->read();
		$data['mutu'] = $mfolder->read_mutu();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->add_script('js/folder.js');
		
		$this->view( 'folder/index.php', $data );
	}
	
	function detailchild_mutu($id=NULL, $str=NULL){
		$mfolder = new model_folder();
		
		$child = $mfolder->read_mutu($id);
		
		$level = $this->coms->authenticatedUser->level; 
		
			
		echo "<ul>";
			if(isset($child)){ 
				$n = 1;
				$folder = $str;
				foreach ($child as $dt) {
					$folder = $str."/".$dt->folder_name;

					?>
					<li><span class='span text text-default' f_id="<?php echo $dt->id ?>" onclick="view_content_mutu('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span>
					<?php echo $this->detailchild_mutu( $dt->id, $folder) ?></li>
					<?php
				} 
			}
		echo "</ul>";
	}
	
	
	function view_content_mutu(){
		$mfolder = new model_folder();
		
		$folder_id = $_POST['id'];
		$unit_id = $_POST['unit'];
		
		$child = $mfolder->read_child_mutu($folder_id);
		$childfile = $mfolder->read_file_mutu($folder_id, $unit_id);
		$link = $mfolder->get_folder_name_mutu($folder_id, $unit_id);
		
		$level = $this->coms->authenticatedUser->level; 
		
		
		?>
		<legend><h4><?php if($link): $this->parent_link($link->parent_id)?><a href="#" onclick="view_content_mutu_child('<?php echo $link->id ?>')"><span class="text text-default"><?php echo $link->folder_name; ?></span></a><?php else: echo "PTIIK Files"; endif;?></h4></legend>
		<table class="table table-hover example">
			<thead>
				<tr>
					<th><small>Name</small></th>
					<th><small>Size</small></th>
					<th><small>Last Modified</small></th>
					<?php if(($level=="1")||($level=="4")){ ?>
					<th>&nbsp;</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
						
		<?php
		if(isset($child)){?>			
			<?php
			foreach ($child as $dt) {?>
				<tr class="folder<?php echo $dt->folder_id ?>">
					<td><span class='span' style="cursor: pointer" onclick="view_content_mutu_child('<?php echo $dt->id ?>','<?php echo $dt->unit_id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span></td>
					<td></td>
					<td></td>					
				</tr>
			<?php 
			} 
		}else{
		
		if(isset($childfile)){
		
			foreach ($childfile as $d) {?>
				<tr class="file<?php echo $d->id ?>">
					<td>
						<span style="cursor: pointer">
							<?php if($d->jenis_file == 'document'){ ?>
								<i class="fa fa-file-text-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'image'){ ?>
								<i class="fa fa-picture-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'video'){ ?>
								<i class="fa fa-video-camera"></i>&nbsp;
							<?php } ?>
							<a class="" href="#" onClick="doViewMutu('<?php echo $d->file_id;?>','<?php echo $d->folder_id;?>')";><span class="text text-default"><?php echo $d->file_name ?></span></a>
						</span>
					</td>
					<td><small><?php echo $this->formatSizeUnits($d->file_size) ?></small></td>
					<td><small><i class="fa fa-clock-o"></i>&nbsp;<?php echo $d->last_update ?></small></td>
					
				</tr>
			<?php 
			} 
		}
		}
		
		?>				
			</tbody>
		</table>
		<script>
			$(document).ready( function() {
			  $('.example').dataTable( {
				"fnInitComplete": function(oSettings, json) {
				  //alert( 'DataTables has finished its initialisation.' );
				},
				//"sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
				"sDom": "<'row'<'col-md-9'><'col-md-3 pull-right'f>><'table-responsive't><'row'<'col-md-6'><'col-md-6'p>>",
					"sPaginationType": "bootstrap",
					"aaSorting": [],
					//"aaSorting": [[0, 'desc']],
					"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"sSearch": ""
					}
			  } );
			} )

		</script>
		<?php
	}
	
	
	function view_content_mutu_child(){
		$mfolder = new model_folder();
		
		$folder_id = $_POST['id'];
		$unit_id = $_POST['unit'];
		
		$child = $mfolder->read_child_mutu($folder_id);
		$childfile = $mfolder->read_file_mutu($folder_id, $unit_id);
		$link = $mfolder->get_folder_name_mutu($folder_id, $unit_id);
		
		$level = $this->coms->authenticatedUser->level; 
		
		
		?>
		<legend><h4><?php if($link): $this->parent_link($link->parent_id)?><a href="#" onclick="view_content_mutu_child('<?php echo $link->id ?>')"><span class="text text-default"><?php echo $link->folder_name; ?></span></a><?php else: echo "PTIIK Files"; endif;?></h4></legend>
		<table class="table table-hover example">
			<thead>
				<tr>
					<th><small>Name</small></th>
					<th><small>Size</small></th>
					<th><small>Last Modified</small></th>
					<?php if(($level=="1")||($level=="4")){ ?>
					<th>&nbsp;</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
						
		<?php
		
		if(isset($childfile)){
		
			foreach ($childfile as $d) {?>
				<tr class="file<?php echo $d->id ?>">
					<td>
						<span style="cursor: pointer">
							<?php if($d->jenis_file == 'document'){ ?>
								<i class="fa fa-file-text-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'image'){ ?>
								<i class="fa fa-picture-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'video'){ ?>
								<i class="fa fa-video-camera"></i>&nbsp;
							<?php } ?>
							<a class="" href="#" onClick="doViewMutu('<?php echo $d->file_id;?>','<?php echo $d->folder_id;?>')";><span class="text text-default"><?php echo $d->file_name ?></span></a>
						</span>
					</td>
					<td><small><?php echo $this->formatSizeUnits($d->file_size) ?></small></td>
					<td><small><i class="fa fa-clock-o"></i>&nbsp;<?php echo $d->last_update ?></small></td>
					
				</tr>
			<?php 
			} 
		}
		?>				
			</tbody>
		</table>
		<script>
			$(document).ready( function() {
			  $('.example').dataTable( {
				"fnInitComplete": function(oSettings, json) {
				  //alert( 'DataTables has finished its initialisation.' );
				},
				//"sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
				"sDom": "<'row'<'col-md-9'><'col-md-3 pull-right'f>><'table-responsive't><'row'<'col-md-6'><'col-md-6'p>>",
					"sPaginationType": "bootstrap",
					"aaSorting": [],
					//"aaSorting": [[0, 'desc']],
					"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"sSearch": ""
					}
			  } );
			} )

		</script>
		<?php
	}
	
	function detailchild($id=NULL, $str=NULL){
		$mfolder = new model_folder();
		
		$child = $mfolder->read($id);
		
		$level = $this->coms->authenticatedUser->level; 
		
			
		echo "<ul>";
			if(isset($child)){ 
				$n = 1;
				$folder = $str;
				foreach ($child as $dt) {
					$folder = $str."/".$dt->folder_name;
						if(($level=="1")||($level=="47")||($level=="46")){
				?>
						<li>							
							<span f_id="<?php echo $dt->id ?>" class='span text text-default' onclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span>&nbsp;
							<a title="rename" class='btn-edit-post btn-add' href="#" data-toggle="modal" data-target="#newfolder" onclick="rename('<?php echo $dt->id ?>','<?php echo $dt->folder_name ?>')"><i class="fa fa-gear"></i></a>&nbsp;
							<a title="create new folder" class='btn-edit-post btn-add' href="#" data-toggle="modal" data-target="#newfolder" onclick="newfolder_parent('<?php echo $dt->id ?>')"><i class="fa fa-plus"></i></span></a>&nbsp;
							<a title="upload file" class='btn-edit-post btn-add' href="#" data-toggle="modal" data-target="#newfile" onclick="newfile_parent('<?php echo $dt->id ?>','<?php echo $folder; ?>')"><i class="fa fa-cloud-upload"></i></a>&nbsp;
							<a title="delete folder" class='btn-edit-post btn-add' href="#" onclick="doDelete('<?php echo $dt->folder_id ?>','folder')"><i class="fa fa-trash-o"></i></a>
							<?php echo $this->detailchild( $dt->id, $folder) ?>
						</li>
					<?php 
					}else{
						?>
						<li><span class='span text text-default' f_id="<?php echo $dt->id ?>" onclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span>
						<?php echo $this->detailchild( $dt->id, $folder) ?></li>
						<?php
					}
				} 
			}
		echo "</ul>";
	}
	
	function save(){
		$mfolder = new model_folder();
		$id = $_POST['id'];
		
		if($id != ""){
			$folder_name = $_POST['folder_name'];
			$mfolder->rename_folder($id, $folder_name);
			echo "Rename folder process success!";
			exit();
		}
		else{
			$parent = $_POST['parent'];
			$folder_name = $_POST['folder_name'];
			$folder_id = $mfolder->folder_id();
			
			$datanya 	= Array(
								'id' => $folder_id,
								'folder_name' => $folder_name,
								'parent_id' => $parent
							   );
			$mfolder->replace_folder($datanya);
			echo "Create folder process success!";
			exit();
		}
	}
	
	function save_file(){
		$mfolder = new model_folder();
		
		$user	= $this->coms->authenticatedUser->id;
		
		$folder_id  = $_POST['folder'];
		$folder	= $_POST['folder_loc'];
			
		$title		= $_POST['title'];
		$uploads	= $_FILES['uploads'];
		// $count		= $_POST['count_new_file'];
		$i=0;
		foreach ($_FILES['uploads']['name'] as $id => $file){
			$lastupdate	= date("Y-m-d H:i:s");
			$month = date('m');
			$year  = date('Y');
			$name  = str_replace('&', 'dan',$_FILES['uploads']['name'][$id]);
			$ext   = $this->get_extension($name);
			
			switch(strToLower($ext)){
				case 'jpg':
				case 'jpeg':
				case 'png':
				case 'gif':
					$jenisfile = "image";
					break;
				case 'rar':
				case 'zip':
					$jenisfile = "rar";
					break;
				case 'doc':
				case 'ppt':
				case 'pdf':
				case 'xls':
				case 'docx':
				case 'pptx':
				case 'xlsx':
					$jenisfile = "document";
					break;
				case 'webm':
				case 'mp4':
					$jenisfile = "video";
			}
			$file_type=$_FILES["uploads"]["type"][$id];
			$file_size=$_FILES["uploads"]["size"][$id];
			
			$upload_dir = 'assets/upload/filemanager/'.$folder. "/";
			$upload_dir_db = 'upload/filemanager/'.$folder. "/";
			
			$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx', 'webm', 'mp4', 'rar', 'zip');
			
			if(!in_array($ext,$allowed_ext)){
				echo "Sorry, your data type is not allowed!";
				exit();
			}
			else{
				if (!file_exists($upload_dir)) {
					mkdir($upload_dir, 0777, true);
					chmod($upload_dir, 0777);
				}
				$file_loc = $upload_dir_db . $name;
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $_FILES['uploads']['tmp_name'][$id];
					$filename    = $name;
					$filenamenew = $name;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		            $ch = curl_init();
		            $options = array(
						 CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
					
		            ); // cURL options 
					curl_setopt_array($ch, $options);
			        curl_exec($ch);
			        if(!curl_errno($ch))
			        {
			            $info = curl_getinfo($ch);
			            if ($info['http_code'] == 200)
			               $errmsg = "File uploaded successfully";
			        }
			        else
			        {
			            $errmsg = curl_error($ch);
			        }
					
					echo $errmsg;
			        curl_close($ch);
					//------UPLOAD USING CURL----------------
					
					rename($_FILES['uploads']['tmp_name'][$id], $upload_dir . $name);
				}
				
				$file_id = $mfolder->file_id();
				
				$datanya 	= Array('file_id' => $file_id,
									'judul' => $title[$i],
									'file_loc' => $file_loc,
									'file_name' => $name,
									'file_type' => $file_type,
									'file_size' => $file_size,
									'jenis_file' => $jenisfile,
									'folder_id' => $folder_id,
									'user_id' => $user,
									'last_update' => $lastupdate
									);
				$mfolder->replace_file($datanya);
				
				//echo $title[$i]."\n".$name."\n".$file_type."\n".$file_size."\n".$jenisfile."\n";
				$i++;
			}
		}
		echo "Upload file Success!";
		exit();
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_title($file_name){
		$name = explode('.', $file_name);
		array_pop($name);
		return implode('.', $name);
	}
	
	function views($id=NULL, $folder=NULL){
		$mfolder = new model_folder();
		$data['detail']	= $mfolder->read_file("",$id);
		
		$link = $mfolder->get_folder_name($folder);
			
		?>
		<legend><h4><?php if($link): $this->parent_link($link->parent_id)?><a href="#" onclick="view_content('<?php echo $link->id ?>')"><?php echo $link->folder_name; ?></a><?php else: echo "PTIIK Files"; endif;?>/<?php echo $data['detail']->file_name ?></h4></legend> <?php
		$this->view( 'folder/view.php', $data );
	}
	
	function views_mutu($id=NULL, $folder=NULL){
		$mfolder = new model_folder();
		$data['detail']	= $mfolder->read_file_mutu("","",$id);
		
		$link = $mfolder->get_folder_name_mutu($folder);
			
		?>
		<legend><h4><?php if($link): $this->parent_link($link->parent_id)?><a href="#" onclick="view_content('<?php echo $link->id ?>')"><?php echo $link->folder_name; ?></a><?php else: echo "PTIIK Files"; endif;?>/<?php echo $data['detail']->file_name ?></h4></legend> <?php
		$this->view( 'folder/view.php', $data );
	}
	
	function download($id=NULL){
		$mfile = new model_folder();
		
		$dt=$mfile->read_file("",$id);
		
		//$path = "assets/".$dt->file_loc;	
		$path = $this->config->file_url_view.'/'.str_replace("uploads",'upload',$dt->file_loc);
			
		/*if (file_exists($path)) {
			//$url=$this->location($path);
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename=' . urlencode(basename($path)));
			// header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path));
			ob_clean();
			flush();
			readfile($path);
			exit();
			
			/*header("Content-disposition: attachment; filename=$path");
			readfile($path);
			exit();*/
			//header("Location: $url");
		/*}else{
			die('File Not Found');
		}*/
		
		//$ch = curl_init();
			//$timeout = 5;
			/*curl_setopt($ch, CURLOPT_URL, $path);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;*/
			//Here is the file we are downloading, replace spaces with %20
			
			/*$fp = fopen ($dt->file_name, 'w+');
			$ch = curl_init(str_replace(" ","%20",$path));
			 
			curl_setopt($ch, CURLOPT_TIMEOUT, 50);
			 
			//give curl the file pointer so that it can write to it
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			 
			$data = curl_exec($ch);//get curl response
			 
			//done
			curl_close($ch);*/
			
			header("Pragma: public");
				header('Content-disposition: attachment; filename='.$path);
				header("Content-type: ".mime_content_type($path));
				header('Content-Transfer-Encoding: binary');
				ob_clean();
				flush();
				readfile($path); 
				exit();
			/*header("Content-disposition: attachment; filename=$path");
			readfile($path);
			exit();*/
			//header("Location: $url");
		
		
	}
	
	
	function parent_link($id=NULL){
		$mfolder = new model_folder();
		
		$parent = $mfolder->read_parent($id);
		
		if($parent){
			$this->parent_link($parent->parent_id);
			?>
			<a href="#" onclick="view_content('<?php echo $parent->id ?>')">
			<?php
			echo $parent->folder_name."</a>/";			
		}
		
	}
	
	
	function view_content(){
		$mfolder = new model_folder();
		
		$folder_id = $_POST['id'];
		
		$child = $mfolder->read($folder_id);
		$childfile = $mfolder->read_file($folder_id);
		$link = $mfolder->get_folder_name($folder_id);
		
		$level = $this->coms->authenticatedUser->level; 
		
		
		?>
		<legend><h4><?php if($link): $this->parent_link($link->parent_id)?><a href="#" onclick="view_content('<?php echo $link->id ?>')"><span class="text text-default"><?php echo $link->folder_name; ?></span></a><?php else: echo "PTIIK Files"; endif;?></h4></legend>
		<table class="table table-hover example">
			<thead>
				<tr>
					<th><small>Name</small></th>
					<th><small>Size</small></th>
					<th><small>Last Modified</small></th>
					<?php if(($level=="1")||($level=="4")){ ?>
					<th>&nbsp;</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
						
		<?php
		if(isset($child)){?>			
			<?php
			foreach ($child as $dt) {?>
				<tr class="folder<?php echo $dt->folder_id ?>">
					<td><span class='span' style="cursor: pointer" onclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name ?></span></td>
					<td></td>
					<td></td>
					<?php if(($level=="1")||($level=="4")){ ?>
					<td>
						<span onclick="doDelete('<?php echo $dt->folder_id ?>','folder')"><i class="fa fa-trash-o" style="color: red; cursor: pointer"></i></span>
					</td>
					<?php }?>	
				</tr>
			<?php 
			} 
		}
		if(isset($childfile)){?>
			<?php
			foreach ($childfile as $d) {?>
				<tr class="file<?php echo $d->id ?>">
					<td>
						<span style="cursor: pointer">
							<?php if($d->jenis_file == 'document'){ ?>
								<i class="fa fa-file-text-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'image'){ ?>
								<i class="fa fa-picture-o"></i>&nbsp;
							<?php }else if ($d->jenis_file == 'video'){ ?>
								<i class="fa fa-video-camera"></i>&nbsp;
							<?php } ?>
							<a class="" href="#" onClick="doView('<?php echo $d->file_id;?>','<?php echo $d->folder_id;?>')";><span class="text text-default"><?php echo $d->file_name ?></span></a>
						</span>
					</td>
					<td><small><?php echo $this->formatSizeUnits($d->file_size) ?></small></td>
					<td><small><i class="fa fa-clock-o"></i>&nbsp;<?php echo $d->last_update ?></small></td>
					<?php if(($level=="1")||($level=="4")){ ?>
					<td>					
						<span onclick="doDelete('<?php echo $d->id ?>','file')"><i class="fa fa-trash-o" style="color: red; cursor: pointer"></i></span>						
					</td>
					<?php }?>	
				</tr>
			<?php 
			} 
		}
		?>				
			</tbody>
		</table>
		<script>
			$(document).ready( function() {
			  $('.example').dataTable( {
				"fnInitComplete": function(oSettings, json) {
				  //alert( 'DataTables has finished its initialisation.' );
				},
				//"sDom": "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
				"sDom": "<'row'<'col-md-9'><'col-md-3 pull-right'f>><'table-responsive't><'row'<'col-md-6'><'col-md-6'p>>",
					"sPaginationType": "bootstrap",
					"aaSorting": [],
					//"aaSorting": [[0, 'desc']],
					"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"sSearch": ""
					}
			  } );
			} )

		</script>
		<?php
	}
	
	function formatSizeUnits($bytes){
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}
	
	function delete($folder_id=NULL){ //folder
		$mfolder = new model_folder();
		
		$jenis_del = $_POST['jenis_del'];
		
		if($jenis_del == 'file'){
			$id = $_POST['delete_id'];
			$del_file = $mfolder->delete_file($id);
			if($del_file == TRUE){
				echo "Delete file success!";
			}
		}
		else{
			
			if (!$folder_id){
				$id = $_POST['delete_id'];
			}
			else $id = $folder_id;
			
			//echo $id."\n";
			
			$get_child_id = $mfolder->get_id_from_parent($id);
			$del_parent = $mfolder->delete_folder($id);
			$cek_file_in_folder = $mfolder->read_file($id);
			if(isset($cek_file_in_folder)){
				$del_file = $mfolder->delete_file_by_folder($id);
			}
			if(isset($get_child_id)){
				foreach ($get_child_id as $c) {
					$folderid=$c->id;
					$this->delete($folderid);
				}
			}
			if($del_parent == TRUE) echo "Delete file success!";
		}
		
	}
}
?>