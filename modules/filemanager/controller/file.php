<?php
class filemanager_file extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}
	
	function index() {
		
		$this->head();
		$this->view( 'home.php' );
		$this->foot();
	}
	
	
}
?>