$(document).ready(function() {
	$("#select_kategori").change(function() {
		var kategori_id_ = $(this).val();
		
		if (kategori_id_=='0'){
			$("#answer").remove();
		}
		else{
			$.ajax({
				  type : "POST",
				  dataType: "html",
	               url: base_url + "/module/akademik/test/answer_option",
	               data : $.param({
					kategori_id : kategori_id_
				}),
	               success: function(data){
	               		var output = "<div id='answer'>";
	               
	                    if(data=='radio'){
	                    	output += "<input type='radio' id='1' name='answer' onclick='isbenar(this.id)' > <input type='text' id='answer1'>&nbsp;<i id='benar1'></i><br><br>";
	                    	output += "<input type='radio' id='2' name='answer' onclick='isbenar(this.id)' > <input type='text' id='answer2'>&nbsp;<i id='benar2'></i><br><br>";
	                    	output += "<input type='radio' id='3' name='answer' onclick='isbenar(this.id)' > <input type='text' id='answer3'>&nbsp;<i id='benar3'></i><br><br>";
	                    	//output += "<input type='radio' id='4' name='answer' onclick='isbenar(this.id)' > <input type='text' id='answer4'>&nbsp;<i id='benar4'></i><br><br>";
	                    	output += "<div id='newanswer'></div><button type='button' class='btn btn-danger' onclick='addradioanswer()'><i class='fa fa-plus'></i> Tambah Jawaban</button>";
	                    }
	                    else if(data=='checkbox'){
	                    	output += "<input type='checkbox' id='1' name='answer' onclick='ischeck(this.id)'> <input type='text' id='answer1'>&nbsp;<i id='benar1'></i><br><br>";
	                    	output += "<input type='checkbox' id='2' name='answer' onclick='ischeck(this.id)'> <input type='text' id='answer2'>&nbsp;<i id='benar2'></i><br><br>";
	                    	output += "<input type='checkbox' id='3' name='answer' onclick='ischeck(this.id)'> <input type='text' id='answer3'>&nbsp;<i id='benar3'></i><br><br>";
	                    	//output += "<input type='checkbox' id='4' name='answer' onclick='ischeck(this.id)'> <input type='text' id='answer4'>&nbsp;<i id='benar4'></i><br><br>";
	                    	output += "<div id='newanswer'></div><button type='button' class='btn btn-danger' onclick='addcheckboxanswer()'><i class='fa fa-plus'></i> Tambah Jawaban</button>";
	                    }
	                    else if(data=='textarea'){
	                    	output += "<textarea id='answer1' class='form-control' ></textarea>";
	                    }
	                    
	                    output += "</div>";
	                    $("#answertype").html(output);
	               }
	         });
       }
	});
	
});

function addradioanswer(){
	var radio_length = $('input:radio[name="answer"]').length;
	var id = radio_length+1;
	var output='';
	output += "<div id='removelink"+id+"'><input type='radio' id='"+id+"' name='answer' onclick='isbenar(this.id)' > <input type='text' id='answer"+id+"'>&nbsp;<i id='benar"+id+"'></i>";
	output += "<a onclick='removeattr("+id+")' >&nbsp;<i style='font-size:20px' class='fa fa-times-circle text text-default' title='Remove'></i></a><br><br></div>";
	$('#newanswer').append(output);
}

function addcheckboxanswer(){
	var check_length = $('input:checkbox[name="answer"]').length;
	var id = check_length+1;
	var output='';
	output += "<div id='removelink"+id+"'><input type='checkbox' id='x"+id+"' name='answer' onclick='ischeck(this.id)' > <input type='text' id='answer"+id+"'>&nbsp;<i id='benarx"+id+"'></i>";
	output += "<a onclick='removeattr("+id+")' >&nbsp;<i style='font-size:20px' class='fa fa-times-circle text text-default' title='Remove'></i></a><br><br></div>";
	$('#newanswer').append(output);
}

function isbenar(id){
	var radio_length = $('input:radio[name="answer"]').length;
	for(i=1;i<=radio_length;i++){
		isrem(i);
	}
    $("#benar"+id).html("<i id='rem"+id+"'><span class='label label-success'>Jawaban Benar</span></i>");
}

function ischeck(id){
	if (document.getElementById(id).checked) {
		$("#benar"+id).html("<i id='rem"+id+"'><span class='label label-success'>Jawaban Benar</span></i>");
    } 
    else{
    	isrem(id);
    }
}

function removeattr(id){
	$('#removelink'+id+'').remove();
}

function isrem(id){
	$("#rem"+id).remove();
}
