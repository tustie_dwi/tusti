$(document).ready(function() {
	$(".e9").select2();
	
		var test_id_ = $("#select_test").val();
		var uri = $("#select_test").data("uri");
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				test_id : test_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#detail").html(msg);
				} else {
					//alert(uri);
					$("#detail").html(msg);
					
				}
			}
		});
	
	
	
	//------------------------
	var acount = $('a[class="rema"]').length;
	if(acount=='3'){
		$('a[class="rema"]').remove();
	};
	//---------------------------
	
	//$("#soal-soal p").attr('style','margin: 0px 0px -22px 0px; width: 850px;');
	//$("#soal-soal a").attr('style','margin: 0px 0px 0px 900px;');
	
	var cek_new = $('input:hidden[name="newsoal"]').val();
	var bank_soal = $('input:hidden[name="banksoal"]').val();
	//alert(bank_soal);
	if(bank_soal==1){
	 CKEDITOR.on( 'instanceReady', function( ev )
	 {CKEDITOR.instances['pertanyaan'].setReadOnly(false);});
	}else if(cek_new==1){
	 CKEDITOR.on( 'instanceReady', function( ev )
	 {CKEDITOR.instances['pertanyaan'].setReadOnly(true);});
	}
	
	$("#select_kategori").change(function() {
		
		var kategori_id_ = $(this).val();
		var hid_soal_id_ = $('input:hidden[name="hidId"]').val();
		//alert(kategori_id_);alert(hid_soal_id_);
		if (kategori_id_=='0'){
			$("#answer").remove();
			$("#jawab-note").remove();
			$('#randomizeanswer').remove();
			$('#editdelparam').remove();
			$('#israndomjawab').hide();
			CKEDITOR.instances['pertanyaan'].setReadOnly(true);
		}
		else{
			$('#israndomjawab').show();
			CKEDITOR.instances['pertanyaan'].setReadOnly(false);
			$.ajax({
				  type : "POST",
				  dataType: "html",
	               url: base_url + "/module/content/test/answer_option",
	               data : $.param({
					kategori_id : kategori_id_
				}),
	               success: function(data){
	               		var output = "<div id='answer'>";
	               		var randomanswer = "";
	                    if(data=='radio'){
	                    	randomanswer += "<div id='randomizeanswer' class='form-group'><label class='col-sm-2 control-label'>Jawaban Diacak </label><div class='col-md-10'>";
	                    	randomanswer += "<label class='checkbox'><input type='checkbox' name='israndomjawaban' value='' >Ya</label>";
	                    	output += "<input type='radio' id='1' name='answertype' onclick='isbenar(this.id)' > <input type='text' name='answer[]' id='answer1'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;<i id='benar1'></i><input type='hidden' id ='isbenarcheck1' name='isvalbenar[]' value='' ><br><br>";
	                    	output += "<input type='radio' id='2' name='answertype' onclick='isbenar(this.id)' > <input type='text' name='answer[]' id='answer2'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;<i id='benar2'></i><input type='hidden' id ='isbenarcheck2' name='isvalbenar[]' value='' ><br><br>";
	                    	output += "<input type='radio' id='3' name='answertype' onclick='isbenar(this.id)' > <input type='text' name='answer[]' id='answer3'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;<i id='benar3'></i><input type='hidden' id ='isbenarcheck3' name='isvalbenar[]' value='' ><br><br>";
	                    	//output += "<input type='radio' id='4' name='answer' onclick='isbenar(this.id)' > <input type='text' id='answer4'>&nbsp;<i id='benar4'></i><br><br>";
	                    	output += "<div id='newanswer'></div><button type='button' class='btn btn-danger' onclick='addradioanswer()'><i class='fa fa-plus'></i> Tambah Jawaban</button>";
	                    	randomanswer += "</div></div>";
	                    }
	                    else if(data=='checkbox'){
	                    	randomanswer += "<div id='randomizeanswer' class='form-group'><label class='col-sm-2 control-label'>Jawaban Diacak </label><div class='col-md-10'>";
	                    	randomanswer += "<label class='checkbox'><input type='checkbox' name='israndomjawaban' value='' >Ya</label>";
	                    	output += "<input type='checkbox' id='1' name='answertype' onclick='ischeck(this.id)'> <input type='text' name='answer[]' id='answer1'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;<i id='benar1'></i><input type='hidden' id ='isbenarcheck1' name='isvalbenar[]' value='' ><br><br>";
	                    	output += "<input type='checkbox' id='2' name='answertype' onclick='ischeck(this.id)'> <input type='text' name='answer[]' id='answer2'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;<i id='benar2'></i><input type='hidden' id ='isbenarcheck2' name='isvalbenar[]' value='' ><br><br>";
	                    	output += "<input type='checkbox' id='3' name='answertype' onclick='ischeck(this.id)'> <input type='text' name='answer[]' id='answer3'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;<i id='benar3'></i><input type='hidden' id ='isbenarcheck3' name='isvalbenar[]' value='' ><br><br>";
	                    	//output += "<input type='checkbox' id='4' name='answer' onclick='ischeck(this.id)'> <input type='text' id='answer4'>&nbsp;<i id='benar4'></i><br><br>";
	                    	output += "<div id='newanswer'></div><button type='button' class='btn btn-danger' onclick='addcheckboxanswer()'><i class='fa fa-plus'></i> Tambah Jawaban</button>";
	                    	randomanswer += "</div></div>";
	                    }
	                    else if(data=='textarea'){
	                    	output += "<textarea id='answer1' name='essayanswer' class='form-control' ></textarea>";
	                    	$('#randomizeanswer').remove();
	                    }
	                    output += "</div>";
	                    $("#answertype").html(output);
	                    $("#randomanswer").html(randomanswer);
	                    $("#label-jawab").html("<div id='jawab-note'>Jawaban</div>");
	               }
	         });
	         
	         //--------------ajax edit--------------------------------------
	         if (hid_soal_id_=='') {
	         	var hid_soal_id_ = $('input:hidden[name="getId"]').val();
	         };
	         
			 var cek_bank = $('input:hidden[name="banksoal"]').val();
	       	 if(cek_bank==''){
		     	var url_ = base_url + "/module/content/test/change_answerByParam";
	         }else{
	         	var url_ = base_url + "/module/content/test/change_answerByParam_byBank";
	         }
			
	         $.ajax({
				  type : "POST",
				  dataType: "html",
	              url: url_,
	              data : $.param({
					kategori_id : kategori_id_,
					soal_id		: hid_soal_id_
				  }),
	              success: function(data){
	              	var output = '<span id="editdelparam" ><div class="form-group"><label class="col-sm-2 control-label" ></label><div class="col-md-10">';
					var msgs = JSON.parse(data);
					for (var i = 0, l = msgs.length; i < l; i++) {
						 var msg = msgs[i];
						 var hasil = msg.hasil;
						 if(msg.param!='textarea'){
						 	 $('#israndomjawab').show();
							 for (var j = 0, k = hasil.length; j < k; j++) {
							 	 var res = hasil[j];
							 	 var jid = (j+1);
							 	 if(msg.param=='checkbox'&&jid=='x6'){jid='6'}
							 	 if(msg.param=='checkbox'&&jid==5){jid='x5'}
								  output += "<div id='removelink"+jid+"'><span id='removedel"+jid+"'><input type='"+msg.param+"' id='"+jid+"' name='answertype' ";
								  if(res.is_benar==1){
								  	output += " checked ";
								  }
								  if (msg.param=='radio') {
								  output += " onclick='isbenar(this.id)' ";
								  }
								  else{
								  output += " onclick='ischeck(this.id)' ";
								  }
								  output += " >&nbsp;";
								  output += "<input type='text' name='answer[]' id='answer"+jid+"' value='"+res.keterangan+"' >&nbsp;<input type='text' name='skor[]' value='"+res.skor+"' placeholder='skor..' style='width:40px;'>";
								  output += "&nbsp;<i id='benar"+jid+"'>";
								  if(res.is_benar==1){
								  output += "<i id='rem"+jid+"'><span class='label label-warning'>Jawaban Benar</span></i>";
								  }
								  output += "</i>";
								  output += "<input type='hidden' id ='isbenarcheck"+jid+"' name='isvalbenar[]' value='"+res.is_benar+"' >";
								  output += "<input type='hidden' id = 'jawabanid"+jid+"' name='hidJawabId[]' value='"+res.jawab_id+"' >";
								  output += "<span id='appendrem'><a class='rema' onclick='removeattr("+jid+")' >&nbsp;<i style='font-size:20px' class='fa fa-times-circle text text-default' title='Remove'></i></a></span><br><br></span></div>";
							 }
							 output += "<div id='newanswer'></div><button type='button' class='btn btn-danger' ";
							 if (msg.param=='radio') {
							 	output += "onclick='addradioanswer()'";
							 }
							 else{
							 	output += "onclick='addcheckboxanswer()'";
							 }
							 output += " ><i class='fa fa-plus'></i> Tambah Jawaban</button>";
						 }
						else {
								 $('#israndomjawab').hide();
						 	 	 output += "<div id='removelink1'>";
						 	 	 output += "<textarea id='answer1' name='essayanswer' class='form-control' placeholder='Jawaban...' ></textarea>";
						 	 	 output += "</div>";
						 	 }
					}
					output += '</div></div></span>';
					$('#editparam').html(output);	
	              }
	         });
       }
	});
});

function submit_tes_soal(){
	var cek_bank = $('input:hidden[name="banksoal"]').val();
	
    $('#upload-test-soal-form').submit(function (e) {
    	//var posdata = $(this).serializeArray();
    	var formData = new FormData($(this)[0]);
    	var URL = base_url + 'module/content/test/savesoal';
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	        	if(data=='soal-sudah-ada'){
	        		alert('Soal sudah ada pada test ini.');
	        	}
	        	else{
	        		alert ('Upload Soal Success!');
	        	}
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Soal Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
	
	if(cek_bank==''){
		location.reload();
	}else{
		var soal_id = $('input:hidden[name="repo"]').val();
		var test_id = $('input:hidden[name="hidTestId"]').val();
		
		var form = document.createElement("form");
	    var input = document.createElement("input");
	
		form.action = base_url + "module/content/test/writesoal/"+test_id;
		form.method = "post"
		
		input.name = "bankselect";
		input.value = soal_id;
		form.appendChild(input);
		
		document.body.appendChild(form);
		form.submit();
	}
};

function addradioanswer(){
	var radio_length = $('input:radio[name="answertype"]').length;
	var id = radio_length+1;
	var output='';
	
	output += "<div id='removelink"+id+"'><input type='radio' name='answertype' id='"+id+"' onclick='isbenar(this.id)' > <input type='text' name='answer[]' id='answer"+id+"'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;<i id='benar"+id+"'></i>";
	output += "<a onclick='removeattr("+id+")' >&nbsp;<i style='font-size:20px' class='fa fa-times-circle text text-default' title='Remove'></i></a><input type='hidden' id ='isbenarcheck"+id+"' name='isvalbenar[]' value='' ><br><br></div>";
	$('#newanswer').append(output);
}

function addcheckboxanswer(){
	var check_length = $('input:checkbox[name="answertype"]').length;
	var id = check_length+1;
	var output='';

	output += "<div id='removelink"+id+"'><input type='checkbox' name='answertype' ";
	if (id==5) {id='x5'}
	output += "id='"+id+"'";
	output += "onclick='ischeck(this.id)' >&nbsp;<input type='text' name='answer[]' id='answer"+id+"'>&nbsp;<input type='text' name='skor[]' placeholder='skor..' style='width:40px;'>&nbsp;";
	output += "<i id='benar"+id+"'></i>";
	if (id=='x5') {id=5}
	output += "<a onclick='removeattr("+id+")' >&nbsp;<i style='font-size:20px' class='fa fa-times-circle text text-default' title='Remove'></i></a>";
	if (id==5) {id='x5'}
	output += "<input type='hidden' id ='isbenarcheck"+id+"' name='isvalbenar[]' value='' ><br><br></div>";
	$('#newanswer').append(output);
}

function isbenar(id){
	var radio_length = $('input:radio[name="answertype"]').length;
	for(i=1;i<=radio_length;i++){
		isrem(i);
	}
	var output='';
	output += "<i id='rem"+id+"'><span class='label label-warning'>Jawaban Benar</span></i>";
	//output += "<input type='hidden' name='isradiobenar' value='1' ></i>";
    $("#benar"+id).html(output);
    $("#isbenarcheck"+id).val('1');
}

function ischeck(id){
	if (document.getElementById(id).checked) {
		var output='';
		output += "<i id='rem"+id+"'><span class='label label-warning'>Jawaban Benar</span></i>";
		//output += "<input type='hidden' name='ischeckbenar[]' value='1' >";
		$("#benar"+id).html(output);
		$("#isbenarcheck"+id).val('1');
    } 
    else{
    	isrem(id);
    }
}

function removeattr(id){
	var jawab_id_ = $('#jawabanid'+id).val();
	//alert(id);
	$.ajax({
            url : base_url + 'module/content/test/deletejawaban',
	        type: "POST",
	        data : $.param({
	        	jawabid : jawab_id_
	        }),
	        success:function(data) 
	        {
	        	if(data==''){
	    
	        		$('#removelink'+id+'').remove();
	   				//alert("gagal");
	   			}
	   			else{
	   				//alert(data);
	   				//location.reload();
	   				$('#removelink'+id+'').hide();
	   				$('#removedel'+id+'').remove();
	   			}
	        }
	    });
	
	
	// var acount = $('a[class="rema"]').length;
	// if(acount=='3'){
		// $('a[class="rema"]').remove();
	// };
}

function isrem(id){
	$("#rem"+id).remove();
	$("#isbenarcheck"+id).val('');
}