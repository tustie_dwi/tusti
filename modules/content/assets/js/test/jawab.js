$(document).ready(function(){
	
	
	$(".radio-link").click(function(){
		var mhsid = $("#mhsid").val();
		var soalid = $(this).data("soal");	
		var jawabanid = $(this).val();
		
		document.cookie = mhsid + "["+ soalid +"]" + "=1|" + soalid + "|" + jawabanid;
	});
	
	$(".checkbox-link").click(function(){
		var mhsid = $("#mhsid").val();
		var soalid = $(this).data("soal");	
		var jawabanid = '';
		
		$('[name^="pilihan'+soalid+'"]').map(function(){
			if($(this).prop('checked') == true){
				jawabanid = jawabanid + $(this).val() + "-";
			}
		});
		document.cookie = mhsid + "["+ soalid +"]" + "=2|" + soalid + "|" + jawabanid;
	});
	
	$(".textarea-link").keyup(function(){
		var mhsid = $("#mhsid").val();
		var soalid = $(this).data("soal");
		var nilai = $(this).val();
		
		nilai = nilai.replace(/\n/g, '<br>');
		document.cookie = mhsid + "["+ soalid +"]" + "=3|" + soalid + "|" + nilai;
	});
	
	$("#jawab-btn").click(function(){
		if(confirm('Apakah sudah yakin telah menyelesaikan test dan ingin mengirim jawaban Anda ?')){
			var ca = document.cookie.split(';');
			var mhsid = $("#mhsid").val();
			var jawaban = '';
			
			for(var i=0; i<ca.length; i++){
		      name = ca[i].split('=')[0];
		      value = ca[i].split('=')[1];
		      if(name.indexOf(mhsid) > -1){
		      	jawaban = jawaban + value + ",";
		      	document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
		      }
		    }
		   	
		   $("#jawaban-wrap").val(jawaban);
		   document.getElementById("form-jawab").submit();
	  }
	});
	
	$(".page").click(function(){
		
		page = $(this).data('nilai');
		$("#page").val(page-1);
		
		$('.nomor-soal').html(page);
		$('.dropdown-pilih-soal li:eq('+(page-1)+') a').tab('show');
		$(".page").parent('li').removeClass('active');
		$(this).parent('li').addClass('active');
		
	});
	
	$("#hasil-link").click(function(){
		document.getElementById("hasil-form").submit();
	});
    

    
    $("#next,#prev").click(function(){
    	var page = $("#page").val();
    	var limit = $("#limit").val();
		if($(this).attr('id')=="next"){
    		page = parseInt(page)+1;
		}else{
			page = parseInt(page)-1;
		}
    	
    	if(page >= 0 && page <= limit){
			$('.dropdown-pilih-soal li').removeClass('active');
	    	$('.dropdown-pilih-soal li:eq('+page+') a').tab('show');
	    	$("#page").val(page);
			$('.nomor-soal').html(page+1);
    	}
    });
    
   
});
