$(function(){
	$(".cmbmulti").select2();	
	$("#cmbmulti").select2();
		
	$("#e7").select2({		
		placeholder: "Search for content",
		minimumInputLength: 1,
		multiple:false,
		ajax: {
			//url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
			url:base_url + "/module/content/conf/content",
			dataType: 'json',
			type: "GET",
			data: function (term) { // page is the one-based page number tracked by Select2
				return {
					q: term, //search term
					page: 100
				};
			},
			results: function (data, page) {
				//alert(data.mhs)
				//var more = (page * 10) < data.total; // whether or not there are more results available

				// notice we return the value of more so Select2 knows if more results can be loaded
				return {results: data.results, more: false};
			}
		},
		formatResult: optionFormatResult, // omitted for brevity, see the source of this page
		formatSelection: optionFormatSelection,
		/*initSelection: function(element, callback) {
            var data = [];
            $(element.val().split(",")).each(function(i) {
                var item = this.split(':');
                data.push({
                    id: item[0],
                    value: item[1]
                });
            });
            //$(element).val('');
            callback(data);
        }*/
	});

});

function optionFormatResult(mhs) {	
        var markup = "<table class='movie-result'><tr>";       
        markup += "<td>"+mhs.text+"";         
        markup += "</td></tr></table>"

        return markup;
    }

function optionFormatSelection(mhs) {	
	return mhs.text;
}
	