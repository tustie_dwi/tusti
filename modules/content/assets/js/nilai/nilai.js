$(document).ready(function() {
	$(".e9").select2();
	if( $('#param-disabled').val() == '1'){
		$('input').attr('disabled', true);
		$('#submit-proses').hide();
	}
	$('.real-score').hide();		
});

function nilaiprocess_(id,jadwal,str){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/content/course/nilai/"+str+"/"+id;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jadwal;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

var x = 1;
function addTest(mkid, komponenid, jadwal){
	var output = '';
	var outputPercentage = '';
	$.ajax({
			type : "POST",
			dataType: "html",
			url: base_url + "module/content/course/get_test",
			data : $.param({
				mkid : mkid,
				jadwalid : jadwal
			}),
			success: function(msg){
				if(msg!="error"){
					output += "<span class='span-"+komponenid+"-test-"+x+"'><select id='' class='form-control e9' name='komponen[]' >";
					var msg = JSON.parse(msg);
					for (var i = 0; i < msg.length; i++) {
						var data = msg[i];
						output += "<option value="+data.test_id+" data-uri='1'>"+data.judul+"</option>";
					}
					output += "</select></span>";
					$('#'+komponenid+'-select').append(output);
					
					outputPercentage += "<span class='span-"+komponenid+"-test-"+x+"'>";
					outputPercentage += '<div class="input-group"><span class="input-group-addon">%</span>';
					outputPercentage += '<input name="percent[]" required="required" type=text onkeypress="return isNumberKey(event)" onblur="chiledPercent(';
					outputPercentage += "'"; outputPercentage += komponenid; outputPercentage += "',";
					outputPercentage += "'"; outputPercentage += x; outputPercentage += "'";
					outputPercentage += ')" class="form-control percentage child-percent '+komponenid+'-child-percent child-'+x+'" value="">';
					outputPercentage += '<span class="input-group-btn"><a onclick="deletekomponen('+"'"+''+komponenid+'-test-'+x+''+"'"+')" class="btn btn-default"><i class="fa fa-trash-o"></i></a></span>';
					outputPercentage += '<input name="type[]" required="required" type="hidden"  class="form-control" value="test">';
					outputPercentage += '<input name="hidId[]" required="required" type="hidden"  class="form-control" value="">';
					outputPercentage += '</div>';					
					outputPercentage += '</span>';
					$('#'+komponenid+'-percent').append(outputPercentage);
					x = x + 1;
				}else{alert("Belum terdapat test untuk matakuliah ini!")}
			}
	});
}

function addTugas(jadwal, komponenid){
	var output = '';
	var outputPercentage = '';
	$.ajax({
			type : "POST",
			dataType: "html",
			url: base_url + "module/content/course/get_tugas",
			data : $.param({
				jadwal : jadwal
			}),
			success: function(msg){
				if(msg!="error"){
					output += "<span class='span-"+komponenid+"-tugas-"+x+"'><select id='' class='form-control e9' name='komponen[]' >";
					var msg = JSON.parse(msg);
					for (var i = 0; i < msg.length; i++) {
						var data = msg[i];
						output += "<option value="+data.tugas_id+" data-uri='1'>"+data.judul+"</option>";
					}
					output += "</select></span>";
					$('#'+komponenid+'-select').append(output);
					
					outputPercentage += "<span class='span-"+komponenid+"-tugas-"+x+"'>";
					outputPercentage += '<div class="input-group"><span class="input-group-addon">%</span>';
					outputPercentage += '<input name="percent[]" required="required" type=text onkeypress="return isNumberKey(event)" onblur="chiledPercent(';
					outputPercentage += "'"; outputPercentage += komponenid; outputPercentage += "',";
					outputPercentage += "'"; outputPercentage += x; outputPercentage += "'";
					outputPercentage += ')" class="form-control percentage child-percent '+komponenid+'-child-percent child-'+x+'" value="">';
					outputPercentage += '<span class="input-group-btn"><a onclick="deletekomponen('+"'"+''+komponenid+'-tugas-'+x+''+"'"+')" class="btn btn-default"><i class="fa fa-trash-o"></i></a></span>';
					outputPercentage += '<input name="type[]" required="required" type="hidden"  class="form-control" value="tugas">';
					outputPercentage += '<input name="hidId[]" required="required" type="hidden"  class="form-control" value="">';
					outputPercentage += '</div>';					
					outputPercentage += '</span>';
					$('#'+komponenid+'-percent').append(outputPercentage);
					x = x + 1;
				}else{alert("Belum terdapat tugas untuk matakuliah ini!")}
			}
	});
}

function deletekomponen(id, prosesid){
	if(prosesid!=null){
		var x = confirm("Are you sure you want to delete?");
 		if (x){
			$.ajax({
				type : "POST",
				dataType: "html",
				url: base_url + "module/content/course/delete_proses",
				data : $.param({
					prosesid : prosesid
				}),
				success: function(msg){
					if(msg==1){
						$(".span-"+id).remove();
					}
				}
			});
		}
	}else{
		$(".span-"+id).remove();
	}
}

$(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
});

$('.parent-percent').blur(function() {
	var percent = 0;
	var total = 0;
	$('.parent-percent').each(function(){
        percent = $(this).val();
        total = (total + Number(percent) );
    });
	if(total>100){
		alert("Max value 100%");
		$(this).val('');
	}
});

function chiledPercent(id, i){
	var percent = 0;
	var total = 0;
	$('.'+id+'-child-percent').each(function(){
        percent = $(this).val();
        total = (total + Number(percent) );
    });
	if(total>100){
		alert("Max value 100%");
		$('.child-'+i).val('');
	}
};

$('.percentage').blur(function() {
	if( $(this).val()>100 ){
		alert("Max value 100%");
		$(this).val('100');
	}
	else if( $(this).val()<0 ){
		alert("Min value 0%");
		$(this).val('0');
	}
});

$('.skor').blur(function() {
	if( $(this).val()>100 ){
		alert("Max value 100");
		$(this).val('100');
	}
	else if( $(this).val()<0 ){
		alert("Min value 0");
		$(this).val('0');
	}
});

function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 44 || charCode > 57))return false;

    return true;
}

$("#submit").click(function(e){
	var jadwalid		= $('input:hidden[name="jadwalid"]').val();
	var mkid			= $('input:hidden[name="mkid"]').val();
	
	var parent_percent 	= 0;
	var total_parent   	= 0;
	var child_percent 	= 0;
	var total_child		= 0;
	var count_child		= 0;
	var i				= 0;
	$('.parent-percent').each(function(){
        parent_percent 	= $(this).val();
        total_parent 	= total_parent + Number(parent_percent);
        i++;
    });
    $('.child-percent').each(function(){
        child_percent 	= $(this).val();
        total_child		= total_child + Number(child_percent);	
    });
    count_child		= total_child/total_parent;

    if(total_parent==100){
	    var postData = new FormData($('#form')[0]);
		$.ajax({
			url : base_url + "module/content/course/saveNilaiProsesToDB",
			type: "POST",
			data : postData,
			async: false,
			success:function(data, textStatus, jqXHR) 
			{
			    alert ('Upload Success!');
			    // location.href = base_url + "module/content/course";  
			    nilai_(jadwalid, mkid);
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Upload Failed!');      
			    },
			    cache: false,
				contentType: false,
				processData: false
			  });
			e.preventDefault(); //STOP default action
			return false;
    }else{
    	if(total_parent!=100){
    		alert("Persentase komponen belum 100%");
    	}else{
    		alert("Persentase sub komponen belum 100%");
    	}
    }
});

$("#submit-proses").click(function(e){
	var jadwalid		= $('input:hidden[name="jadwalid"]').val();
	var mkid			= $('input:hidden[name="mkid"]').val();
	
	var postData = new FormData($('#form')[0]);
	$.ajax({
		url : base_url + "module/content/course/saveNilaiProsesMhsToDB",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
		    alert ('Upload Success!');
		    // location.href = base_url + "module/content/course";  
		    nilai_(jadwalid, mkid);
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Upload Failed!');      
		    },
		    cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault(); //STOP default action
		return false;
});

function processKHS_(mk, jadwal){
	var x = confirm("Proses nilai ke KHS?");
	if (x){
		$.ajax({
			type : "POST",
			dataType: "html",
			url: base_url + "module/content/course/proses_nilai_krs",
			data : $.param({
				mk_id : mk,
				jadwal_id : jadwal
			}),
			success: function(msg){
				alert('Upload Success');
				// location.reload();
				nilai_(jadwal, mk);
			}
		});
	}
	else {}
}

function showImportForm(){
	var style = $('#import-form').attr('style').slice(9);
	if(style=='none;'){
		$('#import-form').show();
	}else{
		$('#import-form').hide();
	}
}

$("#submit-import").click(function(e){	
	var postData = new FormData($('#form-import')[0]);
	$.ajax({
		url : base_url + "module/content/course/import_file",
		type: "POST",
		data : postData,
		async: false,
		success:function(result, textStatus, jqXHR) 
		{
			var nilai = new Array;
		    var data = result.split(",");
		    for(var i=0; i<data.length; i++){
		    	if(!isNaN(data[i])){
		    		if(data[i]>=0&&data[i]<=100){
		    			// nilai = data[i];
		    			nilai.push(data[i]);
		    		}
		    	}
		    }
		    // alert( nilai.length );
		    // alert( $('input[name="skor[]"]').length );
		    var i = 0;
		    $('input[name="skor[]"]').each(function() {
		    	$(this).val(nilai[i]);
		    	i++;
		    });
		    alert('Import finished');
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Upload Failed!');      
		    },
		    cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault(); //STOP default action
		return false;
});

// $("#export-nilai").click(function(e) {
    // window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#nilai-tables').html()));
    // e.preventDefault();
// });

$("#export").click(function() {
	var jadwal		= $('input:hidden[name="jadwalid"]').val();
	var mk			= $('input:hidden[name="mkid"]').val();
  	$('input').remove();
	$('.real-score').show();
	ExcellentExport.excel(this, 'myTable', 'nilai');
	nilaiprocess_(mk,jadwal,'proses')
});
