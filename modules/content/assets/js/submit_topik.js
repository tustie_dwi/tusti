function submit_topik(){
    $('#form-save-topik').submit(function (e) {
    	//var posdata = $(this).serializeArray();
    	var formData = new FormData($(this)[0]);
    	var URL = base_url + 'module/content/course/save';
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Materi Success!');
	            location.reload();
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Materi Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
	
	// $("#form-save-topik").submit();
};