//$(".form-content-silabus").hide();

$(".btn-collapsed").click(function(){	
	var idcollapse = $(this).attr("href");
	var kelas = $(idcollapse).attr("class");
	//alert(kelas);
	if(kelas=="panel-collapse collapse"){
		$(this).html("close");
		// alert(idcollapse.substr(1));
		$("#form-silabus"+idcollapse.substr(1)).hide();
		$("button[name='b_silabus"+idcollapse.substr(1)+"']").hide();
		$("button[name='b_cancel"+idcollapse.substr(1)+"']").hide();
		var collapse_title = $(this).closest(".panel-title").find("span.collapse-title").html();
		if(collapse_title!=="Course Overview"){ 				
			get_komponen(idcollapse.substr(1));
		}
	}
	else{
		$(this).html("show");
	}
});
$("button[name='edit_sil']").click(function(){
	var id = $(this).data("uri");
	$("#form-silabus"+id).show();
	$("button[name='b_silabus"+id+"']").show();
	$("button[name='b_cancel"+id+"']").show();
	$("#label-silabus"+id).hide();
});

$("button.cancel").click(function(){
	var id = $(this).data("uri");
	$("#form-silabus"+id).hide();
	$("button[name='b_silabus"+id+"']").hide();
	$("button[name='b_cancel"+id+"']").hide();
	$("#label-silabus"+id).show();
});

function get_komponen(kmp_id){
	var komponenid	= kmp_id;
	
	var mk 	= document.getElementById("hidid");
	var mkid= $(mk).val();
	
	var jadwal 	= document.getElementById("jadwalid");
	var jadwalid= $(jadwal).val();
	
	var tes = $( "textarea[name='silabus']").length;
	
	// alert(mkid+"\n"+jadwalid+"\n"+tes);
	if(tes){
		$( "textarea[name='silabus']").val('');
		CKEDITOR.instances['silabus'+komponenid].setData("");
		$( "input[name='silabusid']").val('');
		$( "input[name='detailid']").val('');
	}
		$( ".silabus"+komponenid).html(" - ");	
	
	
	var aaa = $.post(
		base_url + "module/content/course/get_frm_silabus",
        {
			cmbkomponen : komponenid,
			mkid		: mkid,
			jadwal		: jadwalid
		},"json");
	aaa.done(
		function(data){
			var obj = jQuery.parseJSON(data);
			if(tes){
				CKEDITOR.instances['silabus'+komponenid].setData(obj.keterangan);
				$( "textarea[name='silabus']").html(obj.keterangan);
				$( "input[name='silabusid']").val(obj.id);
				$( "input[name='detailid']").val(obj.detail);
				
			}
				$( ".silabus"+komponenid).html(obj.keterangan);
    	});
    	$("#form-content-silabus").fadeIn(100);
};

function submit_silabus_content(id){
    $('#silabus-form'+id).submit(function (e) {
    	var postData = $(this).serializeArray();
    	var formURL = base_url + 'module/content/course/save_silabus';
    	var ck = CKEDITOR.instances['silabus'+id].getData();
    	var ket = $('#silabus'+id).val(ck);
    	var link = $("#hidid").val();
    	//alert(ck);
    	if(ket!=="" && ket){
          $.ajax({
            url : formURL,
	        type: "POST",
	        data : postData,
	        success:function(msg) 
	        {
	            alert (msg);
	            location.href = base_url + 'module/content/course/detail/silabus/'+link;
	            //location.reload();  
	        },
	        error: function(msg) 
	        { 
	            alert ("Update silabus gagal!");    
	        }
	      });
	     }
	    e.preventDefault(); //STOP default action
	    return false;
	});
};