function detail(jdwl,mk){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	form.action = base_url + "module/content/course/detail/materi/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function newtugas(jdwl,mk){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/tugas/write/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function edittugas(jdwl,mk,tugas){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/tugas/edit/"+tugas+"/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function list(jdwl,mk,tugas){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/tugas/mhs/"+tugas+"/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function nilai(jdwl,mk,upload){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/tugas/nilai/"+upload+"/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function addmateri(jdwl,mk){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/topik/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function file(jdwl,mk){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/file/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function silabus(jdwl,mk){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/silabus/"+mk;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function view_materi(jdwl,mk,materi){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/read/content/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function view_file(jdwl,file){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/read/file/"+file;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};


function edit_materi(jdwl,mk,materi){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/edit/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function edit_file(jdwl,materi,file){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/editfile/"+materi+"/"+file;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};

function add_sub_materi(jdwl,mk,materi){
	var form = document.createElement("form");
    var input = document.createElement("input");
	
	// alert(jdwl);
	// alert(mk);
	form.action = base_url + "module/content/course/topik/"+mk+"/"+materi;
	form.method = "post"
	
	input.name = "jadwal_id";
	input.value = jdwl;
	form.appendChild(input);
	
	document.body.appendChild(form);
	form.submit();
};