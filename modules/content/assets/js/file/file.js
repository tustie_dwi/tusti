
	$("#select_kategori").change(function() {
		var jenisfile_id_ = $(this).val();
		var uri = $(this).data("uri");
		// alert(jenisfile_id_);
		$("#select_materi").select2('val', "0");
		$("#display").empty();
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				jenis_file_id : jenisfile_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_materi").attr("disabled","disabled");
					$("#select_kategori").select2("enable", false);					
				}
				 else {
					$("#select_materi").html(msg);
					$("#select_materi").removeAttr("disabled");
					$("#select_materi").select2("enable", true);
				}
			}
		});
	});
	
	$("#select_materi").change(function() {
		var jenisfile_id_ = $("#select_kategori").val();
		var jadwal_id_ = $("#jadwal_id").val();
		
		var materi_id_ = $(this).val();
		var uri = $(this).data("uri");
		// //alert(jenis_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				materi_id : materi_id_,
				jenis_file_id : jenisfile_id_,
				jadwal_id : jadwal_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#display").html(' <br><br><br><br><br><div class="span3" align="center" style="margin-top:20px;"><div class="well">Sorry, no content to show</div></div>');					
					//$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					// // $("#select_mk").attr("disabled","disabled");
				} else {
					// alert(uri);
					$("#display").html(msg);
				}
			}
		});
	});
	
	$(".e9").select2();
