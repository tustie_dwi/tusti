$(document).ready(function() {
	$(".e9").select2();
	$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:mm:ss', showSecond:true});
	$("#soal-soal p").attr('style','margin: 0px 0px -22px 0px; width: 850px;');
	$(".editsoal").attr('style','margin: 0px 0px 0px 840px;');
	// $(".form_datetime").datetimepicker({timeFormat: 'hh:mm:ss', dateFormat: 'yyyy-mm-dd', showSecond:true});
	// $('#myTab a').click(function (e) {
	  // e.preventDefault();
	  // $(this).tab('show');
	// });
	
});

function save(){
	$('#form').submit(function (e) {
		var judul = $('#judul').val().length;
		var instruksi = $('#instruksi').val().length;
		var keterangan = $('#keterangan').val().length;
		var tanggalmulai = $('#tanggalmulai').val().length;
		var tanggalselesai = $('#tanggalselesai').val().length;
		
		if(judul > 0 && instruksi > 0 && keterangan > 0 && tanggalmulai > 0 && tanggalselesai > 0){		
	   	var formData = new FormData($(this)[0]);
    	var URL = base_url + "module/akademik/test/saveToDB";
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Success!');
	            window.location.href = base_url + "module/akademik/test/writesoal/"+data;
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
		}
		else{
		  alert("Please fill the form");
		}
	});
};

function doDelete(i) {
  	var x = confirm("Are you sure you want to delete?");
 	if (x){
		var del_id = i;
		var url = base_url + "module/akademik/test/deletesoal";

  		//alert(del_id);
	  	$.ajax({
				type : "POST",
				dataType : "html",
				url : url,
				data : $.param({
					delete_id : del_id
				}),
				success : function(msg) {
					if (msg) {
						alert("Data Berhasil Terhapus!");
					}else {
						alert('gagal');
					}
					location.reload();
				}
		});
  }
  else {
  	location.reload();
  }
  	
};

function ubah_nilai(i){
	
//$("#submit"+i).submit(function(e){
	
		var detail_id__ = $('#detail_id'+i).val().length;
		var inf_skor__ = $('#inf_skor'+i).val().length;
		
		 if(detail_id__ > 0 && inf_skor__ > 0){
		 var detail_id_ = $('#detail_id'+i).val();
		var inf_skor_ = $('#inf_skor'+i).val();
		var hasil_id_ = $('#hasil_id'+i).val();
		var catatan_ = $('#catatan'+i).val();
          $.ajax({
            url : base_url + "module/content/test/update_nilai",
	         type: "POST",
	        data : $.param({
						detail_id : detail_id_ , 
						inf_skor : inf_skor_,
						hasil_id : hasil_id_,
						catatan : catatan_
					}),
	        async: false,
	        success:function(msg) 
	        {
	            alert ('Upload Success!');
	            location.reload();  
	        },
	        error: function(msg) 
	        {
	            alert ('Ubah Nilai Gagal!');      
	        }
	      });
	    //e.preventDefault(); //STOP default action
	    
		}
		else{
			alert("Please fill the form");
		}
	//});
};