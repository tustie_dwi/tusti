function submit_silabus(){
    $('#silabus-form').submit(function (e) {
    	var postData = $(this).serializeArray();
    	var formURL = base_url + 'module/content/course/save_silabus';
          $.ajax({
            url : formURL,
	        type: "POST",
	        data : postData,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Silabus Success!');  
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Silabus Failed!');      
	        }
	      });
	    e.preventDefault(); //STOP default action
	});
	// $("#silabus-form").submit();
	location.reload();
};


function get_komponen{
	var komponen 	= document.getElementById("cmbkomponen");
	var komponenid	= $(komponen).val();
	alert(komponen);
	
	var mk 	= document.getElementById("mkid");
	var mkid= $(mk).val();
	
	var jadwal 	= document.getElementById("jadwalid");
	var jadwalid= $(jadwal).val();
		
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'module/content/course/get_frm_silabus',
		data : $.param({
			cmbkomponen : komponenid,
			mkid		: mkid,
			jadwal		: jadwalid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {				
				$("#form-content-silabus").html(msg);
			}
		}
	});
}