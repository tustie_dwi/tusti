function submit_file(){
    $('#upload-form').submit(function (e) {
    	//var postDt = $(this).serializeArray();
    	var formData = new FormData($(this)[0]);
    	var addURL = base_url + 'module/content/course/save_file';
          $.ajax({
            url : addURL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload File Success!');  
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload File Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	      });
	    e.preventDefault(); //STOP default action
	    return false;
	});
	// $("#silabus-form").submit();
	location.reload();
};