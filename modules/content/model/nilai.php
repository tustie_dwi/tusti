<?php
class model_nilai extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_nilai_proses($id=NULL,$mkid=NULL,$jadwalid=NULL){
		$sql= "SELECT 
			   mid(md5(`tbl_nilai_proses`.`proses_id`),9,7) as `proses_id`,
			   tbl_nilai_proses.`proses_id` as hid_id,
			   tbl_nilai_proses.`tahun_akademik`,
			   tbl_nilai_proses.`mkditawarkan_id`,
			   tbl_nilai_proses.`jadwal_id`,
			   tbl_nilai_proses.`kelas`,
			   tbl_nilai_proses.`test_id`,
			   tbl_nilai_proses.`tugas_id`,
			   tbl_nilai_proses.`keterangan`,
			   tbl_nilai_proses.`persentase_total`,
			   tbl_nilai_proses.`persentase_detail`,
			   tbl_nilai_proses.`user_id`,
			   tbl_nilai_proses.`last_update`,
			   tbl_nilai_proses.`is_proses`,
			   tbl_nilai_proses.`parent_id`,
			   (SELECT a.username FROM `db_coms`.`coms_user` as a WHERE a.id =  `tbl_nilai_proses`.user_id) as user
			   FROM `db_ptiik_apps`.`tbl_nilai_proses`
			   WHERE 1
			  ";
		
		if($id){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`proses_id`),9,7) = '".$id."' ";
		}
		
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		$result = $this->db->query( $sql );
		
		// echo $sql;
		return $result;	
	}
	
	function get_komponen($mk=NULL,$id=NULL){
		$sql= "SELECT mid(md5(`tbl_komponen_nilai`.`komponen_id`),9,7) as komponen_id,
			   `db_ptiik_apps`.`tbl_komponen_nilai`.komponen_id as hid_id,
			   mid(md5(`tbl_komponen_nilai`.`mkditawarkan_id`),9,7) as mk_id,
			   `db_ptiik_apps`.`tbl_komponen_nilai`.`mkditawarkan_id`,
			   `db_ptiik_apps`.`tbl_komponen_nilai`.`keterangan`,
			   `db_ptiik_apps`.`tbl_komponen_nilai`.`bobot`,
			   `db_ptiik_apps`.`tbl_komponen_nilai`.`parent_id`,
			
			   `db_ptiik_apps`.`tbl_namamk`.`keterangan` as namamk
			   FROM 
			   `db_ptiik_apps`.`tbl_komponen_nilai`
			   LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_komponen_nilai`.`mkditawarkan_id`
			   LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id`
			   LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_namamk`.`namamk_id` = `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id`
			   WHERE 1
			  ";
		
		if($id){
			$sql = $sql . " AND mid(md5(`tbl_komponen_nilai`.`komponen_id`),9,7) = '".$id."' ";
		}
		
		if($mk){
			$sql = $sql . " AND mid(md5(`tbl_komponen_nilai`.`mkditawarkan_id`),9,7) = '".$mk."' ";
		}
		
		$result = $this->db->query( $sql );
		
		// echo $sql;
		return $result;	
	}
		
	function get_proses_nilai($mkid=NULL,$jadwalid=NULL){
		$sql	="SELECT *
				  FROM `db_ptiik_apps`.`tbl_nilai_proses` 
				  WHERE 1
				";
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		if($jadwalid){
			 $sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		$result = $this->db->query( $sql );
		
		return $result;
		
	}
	
	function get_mk_thn_kelas($mkid=NULL,$jadwalid=NULL){
		$sql   ="SELECT 
				 `tbl_mkditawarkan`.`mkditawarkan_id`,
				 `tbl_mkditawarkan`.`tahun_akademik`,
				
				 `tbl_jadwalmk`.`jadwal_id`,
				 `tbl_jadwalmk`.`kelas`
				 FROM `db_ptiik_apps`.`tbl_mkditawarkan`
				 LEFT JOIN `db_ptiik_apps`.`tbl_jadwalmk` ON `tbl_jadwalmk`.`mkditawarkan_id` = `tbl_mkditawarkan`.`mkditawarkan_id`
				 WHERE 1
				";
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_jadwalmk`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		$result = $this->db->query( $sql );
		
		// echo $sql;
		return $result;	
	}
	
	function cek_if_parent($id=NULL){
		$sql= "SELECT `komponen_id`
			   FROM 
			   `db_ptiik_apps`.`tbl_komponen_nilai`
			   WHERE 1
			  ";
		
		if($id){
			$sql = $sql . " AND parent_id = '".$id."' ";
		}
		
		if(count($this->db->query( $sql ))!=0){return TRUE;}
		else {return FALSE;}
	}
	
	function get_test($mkid=NULL,$testid=NULL, $jadwalid=NULL){
		$sql="SELECT
			  `db_ptiik_apps`.`tbl_test`.`test_id`,
			  `db_ptiik_apps`.`tbl_test`.`jadwal_id`,
			  `db_ptiik_apps`.`tbl_test`.`materi_id`,
			  mid(md5(`tbl_test`.`mkditawarkan_id`),9,7) as `mkid`,
			  `db_ptiik_apps`.`tbl_test`.`mkditawarkan_id`,
			  `db_ptiik_apps`.`tbl_test`.`judul`
			  FROM `db_ptiik_apps`.`tbl_test` 
			  WHERE 1
			 ";
		
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_test`.`mkditawarkan_id`),9,7) = '".$mkid."' 
							OR `tbl_test`.`mkditawarkan_id`= '".$mkid."'
						  ";
		}
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_test`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		$result = $this->db->query( $sql );
		
		// echo $sql;
		return $result;	
	}
	
	function get_tugas($jadwalid=NULL,$tugasid=NULL){
		$sql="SELECT 
			  `tbl_tugas`.`tugas_id`,
			  `tbl_tugas`.`materi_id`,
			  `tbl_tugas`.`jadwal_id`,
			  `tbl_tugas`.`judul`
			  FROM `db_ptiik_apps`.`tbl_tugas` 
			  WHERE 1
			 ";
		
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_tugas`.`jadwal_id`),9,7) = '".$jadwalid."' 
							OR `tbl_tugas`.`jadwal_id` = '".$jadwalid."' 
						  ";
		}
		
		$result = $this->db->query( $sql );
		
		// echo $sql;
		return $result;
	}
	
	function get_komponenName($id=NULL){
		$sql= "SELECT 
			   `db_ptiik_apps`.`tbl_komponen_nilai`.`keterangan`
			   FROM 
			   `db_ptiik_apps`.`tbl_komponen_nilai`
			   WHERE 1
			  ";
		
		if($id){
			$sql = $sql . " AND `tbl_komponen_nilai`.`komponen_id` = '".$id."' ";
		}
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->keterangan;
		return $strresult;
	}
	function get_testName($id=NULL){
		$sql="SELECT
			  `db_ptiik_apps`.`tbl_test`.`judul`
			  FROM `db_ptiik_apps`.`tbl_test` 
			  WHERE 1
			 ";
		
		if($id){
			$sql = $sql . " AND `tbl_test`.`test_id` = '".$id."' ";
		}
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->judul;
		return $strresult;
	}
	function get_tugasName($id=NULL){
		$sql="SELECT 
			  `tbl_tugas`.`judul`
			  FROM `db_ptiik_apps`.`tbl_tugas` 
			  WHERE 1
			 ";
		
		if($id){
			$sql = $sql . " AND `tbl_tugas`.`tugas_id` = '".$id."' ";
		}
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->judul;
		return $strresult;
	}
	
	function get_reg_number(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(proses_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_nilai_proses"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_proses_nilai($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_nilai_proses',$datanya);
	}
	
	function update_proses_nilai($prosesid, $komponen, $percent) {
		$sql = "UPDATE db_ptiik_apps.tbl_nilai_proses
			    SET keterangan = '".$komponen."',
			    `persentase_total`=(CASE WHEN `persentase_total` != '0' then '".$percent."' end),
			    `persentase_detail`=(CASE WHEN `persentase_detail` != '0' then '".$percent."' end)
			    WHERE proses_id = '".$prosesid."'			   
			   ";
		if($this->db->query( $sql )){return TRUE;}
		else {return FALSE;}
	}
	
	function delete_proses($prosesid){
		$sql="DELETE FROM db_ptiik_apps.tbl_nilai_proses 
			  WHERE proses_id= '".$prosesid."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;
		}else return FALSE;
	}
	
	function get_is_proses_tblproses($mkid=NULL,$jadwalid=NULL){
		$sql="SELECT DISTINCT `is_proses`
			  FROM db_ptiik_apps.tbl_nilai_proses
			  WHERE 1"; 
		
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		if($dt){
		$strresult = $dt->is_proses;
		return $strresult;
		}
	}
	
	function get_is_proses_tblprosesmhs($mkid=NULL,$jadwalid=NULL){
		$sql="SELECT DISTINCT `is_proses`
			  FROM db_ptiik_apps.tbl_nilai_proses_mhs
			  WHERE 1"; 
		
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses_mhs`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses_mhs`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		// echo $sql;
		
		$dt = $this->db->getRow( $sql );
		if($dt){
		$strresult = $dt->is_proses;
		return $strresult;
		}
	}
	
	//-------NILAI---------------------------------------------------------------------
	function get_nilai($mkid=NULL,$jadwalid=NULL, $mhsid=NULL, $parent=NULL){
		$sql = "SELECT 
			    tbl_nilai_proses.`proses_id`,
			    tbl_nilai_proses.`test_id`,
			    tbl_nilai_proses.`tugas_id`,
			    tbl_nilai_proses.`keterangan`,
			    tbl_nilai_proses.`persentase_detail`,
			    tbl_nilai_proses.`parent_id`,
                           
                SUM(CASE
               	 WHEN tbl_nilai_proses.`test_id`  
                    	 THEN CASE 
                            	   WHEN `tbl_test_hasil`.`total_skor` 
                                       	THEN (`tbl_test_hasil`.`total_skor`)*(tbl_nilai_proses.`persentase_detail`/100)
                                       ELSE '0'
                                   END
                     WHEN tbl_nilai_proses.`tugas_id` 
                       	 THEN CASE 
                              	   WHEN `tbl_tugas_mhs`.`total_skor` 
                                      	 THEN (`tbl_tugas_mhs`.`total_skor`)*(tbl_nilai_proses.`persentase_detail`/100)
                            	   ELSE '0' 
                                  END       
                END) as skor,
               
                CASE
               	 WHEN tbl_nilai_proses.`test_id`  THEN tbl_test_hasil.mahasiswa_id
                     WHEN tbl_nilai_proses.`tugas_id` THEN tbl_tugas_mhs.mahasiswa_id
                END as mhs_id,
                
				(SELECT a.`persentase_total` FROM `db_ptiik_apps`.`tbl_nilai_proses` as a WHERE a.`proses_id` = `db_ptiik_apps`.`tbl_nilai_proses`.`parent_id`) as persentase_total
                           
			    FROM `db_ptiik_apps`.`tbl_nilai_proses`
	            LEFT JOIN `db_ptiik_apps`.`tbl_test_hasil` ON `tbl_test_hasil`.`test_id` = `tbl_nilai_proses`.`test_id`
	            LEFT JOIN `db_ptiik_apps`.`tbl_tugas_mhs` ON `tbl_tugas_mhs`.`tugas_id` = `tbl_nilai_proses`.`tugas_id`
	           
	            WHERE 1
	            AND tbl_nilai_proses.`parent_id` != '0'                          
			   ";
			   
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		if($mhsid){
			$sql = $sql . " AND 
							CASE
			               	 WHEN tbl_nilai_proses.`test_id`  THEN tbl_test_hasil.mahasiswa_id = '".$mhsid."' 
			                     WHEN tbl_nilai_proses.`tugas_id` THEN tbl_tugas_mhs.mahasiswa_id = '".$mhsid."' 
			                END
						  ";
		}
		
		if($parent){
			$sql = $sql . " AND tbl_nilai_proses.`parent_id` = '".$parent."' ";
		}
		
		$sql	= $sql . " GROUP BY `tbl_nilai_proses`.`parent_id`, mhs_id";
		
		$result = $this->db->query( $sql );
		
		// echo $sql."<br><br>";
		return $result;	
	}
	
	function get_proses_nilai_mhs($mkid=NULL,$jadwalid=NULL){
		$sql = "SELECT 
				tbl_nilai_proses_mhs.`nilai_id`,
				tbl_nilai_proses_mhs.`mahasiswa_id` as mhs_id,
				tbl_nilai_proses_mhs.`skor`,
				tbl_nilai_proses_mhs.`proses_id`,
				tbl_nilai_proses_mhs.`persentase`
				FROM `db_ptiik_apps`.`tbl_nilai_proses_mhs` WHERE 1
			   ";
			   
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses_mhs`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses_mhs`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		$result = $this->db->query( $sql );
		
		// echo $sql."<br><br>";
		return $result;	
	}
	
	function get_nilaiid(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(nilai_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_nilai_proses_mhs"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_proses_mhs_nilai($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_nilai_proses_mhs',$datanya);
	}
	
	function update_proses_tbl_nilai_proses($mkid, $jadwalid){
		$sql="UPDATE db_ptiik_apps.tbl_nilai_proses 
			  SET is_proses = 1
			  WHERE mkditawarkan_id	= '".$mkid."'
			  AND jadwal_id	= '".$jadwalid."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;
		}else return FALSE;
	}
	
	function update_proses_mhs_nilai($nilaiid, $skor){
		$sql="UPDATE db_ptiik_apps.tbl_nilai_proses_mhs
			  SET skor = '".$skor."'
			  WHERE nilai_id= '".$nilaiid."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;
		}else return FALSE;
	}
	
	function get_nilai_akhir($mkid=NULL,$jadwalid=NULL){
		$sql= "SELECT
			  `tbl_nilai_proses_mhs`.`mahasiswa_id`,
			  `tbl_nilai_proses_mhs`.`mkditawarkan_id`,
			  `tbl_nilai_proses_mhs`.`jadwal_id`,
			  `tbl_nilai_proses_mhs`.`krs_id`,
			  `tbl_nilai_proses_mhs`.`kelas`,
			  `tbl_nilai_proses_mhs`.`tahun_akademik`,
              SUM((`tbl_nilai_proses_mhs`.`skor`)*(`tbl_nilai_proses_mhs`.`persentase`/100)) as skor            
		      FROM `db_ptiik_apps`.`tbl_nilai_proses_mhs`	           
	          WHERE 1
			  ";
		
		if($mkid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses_mhs`.`mkditawarkan_id`),9,7) = '".$mkid."' ";
		}
		
		if($jadwalid){
			$sql = $sql . " AND mid(md5(`tbl_nilai_proses_mhs`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		$sql = $sql . " GROUP BY `tbl_nilai_proses_mhs`.`mahasiswa_id` ";
		
		$result = $this->db->query( $sql );
		
		// echo $sql;
		return $result;	
	}
	
	function update_nilai_krs($krsid, $mhsid, $skor, $inf_bobot, $inf_huruf, $user, $lastupdate){
		$sql="UPDATE db_ptiik_apps.tbl_krs
			  SET 
			  	nilai_akhir = '".$skor."',
			  	inf_bobot = '".$inf_bobot."',
			  	inf_huruf = '".$inf_huruf."',
			  	user_id = '".$user."',
			  	last_update = '".$lastupdate."'
			  WHERE krs_id = '".$krsid."'
			  AND mahasiswa_id = '".$mhsid."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;
		}else return FALSE;
	}
	
	function update_proses_tbl_nilai_proses_mhs($mkid=NULL, $jadwalid=NULL){
		$sql="UPDATE db_ptiik_apps.tbl_nilai_proses_mhs 
			  SET is_proses = 1
			  WHERE mkditawarkan_id	= '".$mkid."'
			  AND jadwal_id	= '".$jadwalid."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;
		}else return FALSE;
	}
	
}