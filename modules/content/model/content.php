<?php
class model_content extends model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public $error;
	public $id;
	public $modified;
	
	public function save($content_page, $content_title, $content_date, 
							$content_content, $content_status,
							$content_author_id, $content_category, $content_parent,$content_urut, $id,$newname, $content_comment) {
		
		$this->error = NULL;
		
		$data['content_page']		= $content_page;
		$data['content_modified'] 	= date('Y-m-d H:i:s'); //$content_date;
		$data['content_title'] 		= $content_title;
		$data['content_content'] 	= $content_content;
		$data['content_status']		= $content_status;
		$data['content_category']	= $content_category;
		$data['parent_id']			= $content_parent;
		$data['urut']				= $content_urut;
		$data['thumb_img']			= $newname;
		$data['comment_status']		= $content_comment;
			
		//$cats['content_categories'] = $content_categories;
		//$tags['content_tags'] = $content_tags;

		if($id == NULL) { // saving
		
			$data['content_author_id']	= $content_author_id;
			$data['content_page'] 		= $content_page;
		
			if( $affectedRows = $this->db->insert('contents', $data) ) {
				$this->id = $this->db->getLastInsertId();
				//$this->saveCategories($this->id, $content_categories);
				//$this->saveTags($this->id, $content_tags);
				$this->modified = $data['content_modified'];
				return true;
			} else $this->error = $this->db->getLastError();
			return false;
		} else { // updating
		
			$where['id'] = $id;
			$this->id = $id;
			$this->modified = $data['content_modified'];			
						
			if( $affected_rows = $this->db->update('contents', $data, $where) ) {
				return true;
			} else if( $this->error = $this->db->getLastError() ) {
				return false;
			}
			return true;
		}

	}
	
	function save_slide($content_page, $content_title, $content_date, 
							$content_content, $content_status,
							$content_author_id, $id,$newname, $filename,$extension) {
		
		$this->error = NULL;
		
		$data['img_link']			= $content_page;
		$data['content_modified'] 	= date('Y-m-d H:i:s'); //$content_date;
		$data['content_title'] 		= $content_title;
		$data['keterangan'] 		= $content_content;
		$data['content_status']		= $content_status;
		$data['file_loc']			= $newname;
		$data['file_name']			= $filename;
		$data['file_type']			= $extension;
		
		if($id == NULL) { // saving
		
			$data['user_id']	= $content_author_id;
			$data['img_link'] 	= $content_page;
		
			if( $affectedRows = $this->db->insert('content_slide', $data) ) {
				$this->id = $this->db->getLastInsertId();
				$this->modified = $data['content_modified'];
				return true;
			} else $this->error = $this->db->getLastError();
			return false;
		} else { // updating
		
			$where['id'] = $id;
			$this->id = $id;
			$this->modified = $data['content_modified'];			
						
			if( $affected_rows = $this->db->update('content_slide', $data, $where) ) {
				return true;
			} else if( $this->error = $this->db->getLastError() ) {
				return false;
			}
			return true;
		}

	}
	
	private function saveCategories($content_id, $content_cats) {
		if(is_array($content_cats)) {
			$pc = array();
			foreach($content_cats as $c)
				array_push($pc, "('".$content_id."','".$c."')");
			
			$sql = "INSERT IGNORE INTO content_category VALUES ".implode(", ", $pc) ;			
			return $this->db->query( $sql );
		} else return 0;
	}
	
	private function saveTags($content_id, $content_tags) {
		
		$this->db->query("DELETE FROM contents_tags WHERE content_id = '".$content_id."'");
		if(is_array($content_tags)) {
			$tag_tag = array();
			foreach($content_tags as $name) {
				$tag = helper::getTag($name);
				array_push($tag_tag, $tag);
				$this->db->query("INSERT IGNORE INTO tags VALUES ('".$name."','".$tag."')");
			}
	
			$pt = array();
			foreach($tag_tag as $tag)
				array_push($pt, "('".$content_id."','".$tag."')");
			
			$sql = "INSERT IGNORE INTO contents_tags VALUES ".implode(", ", $pt);
			
			return $this->db->query( $sql );
		} else return 0;
	}
	
	public function getList($page = 1, $perpage = 10){
		$offset = ($page-1)*$perpage;
		
		$sql = "SELECT id, content_page, content_title, parent_id, urut, 
				 content_modified, 
				SUBSTRING(content_content, 1,100) AS content_content, 
				content_status, content_author_id, content_category, thumb_img, comment_status
				FROM db_coms.contents 
				ORDER BY content_modified DESC
				LIMIT $offset, $perpage ";
				
		$result = $this->db->getResults( $sql ); //var_dump($this->db);
		return $result;
	}
	
	public function getListByCat($category, $page = 1, $perpage = 10){
		
		$category = $this->db->escape($category);
		$offset = ($page-1)*$perpage;
		
		$sql = "SELECT id, content_page, content_title, content_subtitle, DATE_FORMAT(content_date, '%d/%m/%Y %H:%i') AS content_date, 
				content_modified, 
				SUBSTRING(content_content, 1,100) AS content_content, 
				content_status, content_excerpt, content_author_id, 
				comment_status,
				thumb_img, 
				(SELECT COUNT(*) FROM contents_comments c WHERE c.content_id = p.id) as comment_count,
				hits
				FROM contents p LEFT JOIN content_category pc ON p.content_category = pc.content_category
				WHERE pc.keterangan = '$category'
				ORDER BY content_modified DESC 
				LIMIT $offset, $perpage ";
				
		$result = $this->db->getResults( $sql ); //var_dump($this->db);
		return $result;
	}
	
	public function getListByTag($tag, $page = 1, $perpage = 10){
		
		$category = $this->db->escape($tag);
		$offset = ($page-1)*$perpage;
		
		$sql = "SELECT id, content_page, content_title, content_subtitle, DATE_FORMAT(content_date, '%d/%m/%Y %H:%i') AS content_date, 
				content_modified, thumb_img, 
				SUBSTRING(content_content, 1,100) AS content_content, 
				content_status, content_excerpt, content_author_id, 
				comment_status,
				(SELECT COUNT(*) FROM contents_comments c WHERE c.content_id = p.id) as comment_count,
				hits
				FROM contents p LEFT JOIN contents_tags pt ON p.id = pt.content_id
				WHERE pt.tag_tag = '$tag'
				ORDER BY content_modified DESC 
				LIMIT $offset, $perpage ";
				
		$result = $this->db->getResults( $sql ); //var_dump($this->db);
		return $result;
	}
	
	public function delete($id) {
		if($this->db->delete('contents', array('id'=>$id)))
			return true;
		else $this->error = $this->db->getLastError();
		return false;
	}
	
	public function delete_slide($id, $url) {
		$sql = "SELECT file_loc FROM db_coms.content_slide WHERE id = '".$id."' ";
		$data = $this->db->getRow($sql);
		
		$filename = $data->file_loc;
			
		if (file_exists($filename)) {
			unlink($filename);
		  } 
  
		if($this->db->delete('content_slide', array('id'=>$id)))
			return true;
		else $this->error = $this->db->getLastError();
		return false;
	}
	
	// get a single post by a post id
	public function getContent($postid) {
		
		$columns = array("id", "content_page", "content_title", "content_category", "parent_id", "urut", 
				"DATE_FORMAT(content_modified, '%d/%m/%Y') AS content_date",
				"DATE_FORMAT(content_modified, '%H:%i:%s') AS content_time",
				"DATE_FORMAT(content_modified, '%d/%m/%Y %H:%i:%s') AS content_modified",				
				"content_content", 
				"content_status", "content_author_id", "thumb_img", "comment_status");
		
		$result = $this->db->select("contents", $columns, array("id"=>$postid));
		if($result)
			return $result[0];
	}	
	
	function get_category(){
		$sql = "SELECT content_category, keterangan FROM db_coms.content_category ORDER BY keterangan";
		return $this->db->query( $sql );
	}
	
	public function getCategories($postid) {
		$sql = "SELECT ct.name AS name, ct.tag AS tag " .
				"FROM content_category pct LEFT JOIN categories ct ON pct.category_tag = ct.tag " .
				"WHERE pct.content_id = '".$postid."'";
		return $this->db->query( $sql );
	}
	
	public function getTags($postid) {
		$sql = "SELECT tt.name AS name, tt.tag AS tag " .
				"FROM contents_tags ptt LEFT JOIN tags tt ON ptt.tag_tag = tt.tag " .
				"WHERE ptt.content_id = '".$postid."'";
		return $this->db->query( $sql );
	}
	
	public function getPostCount() {
		$sql = "SELECT count(*) FROM contents";
		return $this->db->getVar( $sql );
	}
	
	
	function  get_content_slide($id=NULL){
		$sql = "SELECT
					db_coms.content_slide.id,
					db_coms.content_slide.img_link,
					db_coms.content_slide.content_title,
					db_coms.content_slide.keterangan,
					db_coms.content_slide.content_status,
					db_coms.content_slide.file_name,
					db_coms.content_slide.file_loc,
					db_coms.content_slide.file_size,
					db_coms.content_slide.user_id,
					db_coms.content_slide.content_modified
				FROM
					db_coms.content_slide
				WHERE 1 = 1 
					";
		if($id){
			$sql = $sql. " AND db_coms.content_slide.id = '".$id."' ";
		}
		
		$sql = $sql. " ORDER BY content_slide.content_modified DESC";
		
		if($id){
			return $this->db->getRow( $sql );
		}else{
			return $this->db->query( $sql );
		}
		
		
	}
}