<?php

/*
<!---//---------------------------------------------------------------------
		Author			: 
		Creation Date	: 10/12/13 (Initial version)
		Purpose			: course module
		Revision Log:
		TDP 12/02/14 	edit sql, edit parameter, please do not join to db coms	
----------------------------------------------------------------------//--->
*/

class model_tugas extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	//====TUGAS====//
	function get_md5_tugas_id($id=NULL){
		$sql= "SELECT  mid(md5(`tugas_id`),9,7) as tugas_id
		       FROM db_ptiik_apps.`tbl_tugas` 
		       WHERE tugas_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->tugas_id;
			return $strresult;
		}
	}
	
	function get_tugas_id_by_md5($id=NULL){
		$sql= "SELECT tugas_id as tugas_id
		       FROM db_ptiik_apps.`tbl_tugas` 
		       WHERE mid(md5(`tugas_id`),9,7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->tugas_id;
			return $strresult;
		}
	}
	
	function get_materi_id_by_md5($id=NULL){
		$sql= "SELECT materi_id as materi_id
		       FROM db_ptiik_apps.`tbl_materimk` 
		       WHERE mid(md5(`materi_id`),9,7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->materi_id;
			return $strresult;
		}
	}
	
	function get_jadwal_id_by_md5($id=NULL){
		$sql= "SELECT jadwal_id as jadwal_id
		       FROM db_ptiik_apps.`tbl_jadwalmk` 
		       WHERE mid(md5(`jadwal_id`),9,7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->jadwal_id;
			return $strresult;
		}
	}
	
	function read($id=NULL,$mk=NULL,$jadwal=NULL,$staffid=NULL,$mhsid=NULL,$semester=NULL){
		$sql = "SELECT
					MID( MD5( tbl_tugas.tugas_id ) , 9,7 ) AS tugasid,
					MID( MD5( tbl_tugas.jadwal_id ) , 9,7 ) AS jadwalid,
					MID( MD5( tbl_tugas.materi_id ) , 9,7 ) AS materiid,
					db_ptiik_apps.tbl_tugas.jadwal_id,
					db_ptiik_apps.tbl_tugas.tugas_id,
					db_ptiik_apps.tbl_materimk.judul AS materi,
					db_ptiik_apps.tbl_tugas.materi_id,
					MID( MD5(db_ptiik_apps.tbl_materimk.mkditawarkan_id) , 9, 7 ) AS mk,
					db_ptiik_apps.vw_mk_by_dosen.namamk,
					db_ptiik_apps.vw_mk_by_dosen.kode_mk,
					db_ptiik_apps.tbl_tugas.judul,
					db_ptiik_apps.tbl_tugas.instruksi,
					db_ptiik_apps.tbl_tugas.keterangan,
					db_ptiik_apps.tbl_tugas.tgl_mulai,
					db_ptiik_apps.tbl_tugas.tgl_selesai,
					db_ptiik_apps.tbl_tugas.user_id AS userid,
					db_ptiik_apps.tbl_tugas.last_update,
					db_coms.coms_user.`name` AS `user`,
					db_ptiik_apps.tbl_materimk.mkditawarkan_id					
					FROM
										db_ptiik_apps.tbl_tugas
										LEFT JOIN db_ptiik_apps.tbl_materimk ON db_ptiik_apps.tbl_materimk.materi_id = db_ptiik_apps.tbl_tugas.materi_id
										LEFT JOIN db_ptiik_apps.vw_mk_by_dosen ON db_ptiik_apps.vw_mk_by_dosen.jadwal_id = db_ptiik_apps.tbl_tugas.jadwal_id
										LEFT JOIN db_coms.coms_user ON db_ptiik_apps.tbl_tugas.user_id = db_coms.coms_user.id
					WHERE 1 = 1

			   ";
		if($mk){
			$sql .= " AND MID( MD5(db_ptiik_apps.tbl_materimk.mkditawarkan_id) , 9, 7 ) = '".$mk."'";
		}
		
		if($jadwal){
			$sql .= " AND MID( MD5( tbl_tugas.jadwal_id ) , 9,7 ) = '".$jadwal."'";
		}
		
		if($id){
			$sql .= " AND MID( MD5(db_ptiik_apps.`tbl_tugas`.tugas_id) , 9,7 ) = '".$id."'";
		}
		
		if($staffid){
			$sql .= " AND tbl_tugas.jadwal_id IN (SELECT vw_mk_by_dosen.jadwal_id FROM `db_ptiik_apps`.`vw_mk_by_dosen` WHERE vw_mk_by_dosen.karyawan_id = '".$staffid."' 
						AND vw_mk_by_dosen.tahun_akademik = '".$semester."')";
		
		}
		
		if($mhsid){
			$sql .= " AND tbl_tugas.jadwal_id IN (SELECT vw_mk_by_mhs.jadwal_id FROM `db_ptiik_apps`.`vw_mk_by_mhs` WHERE vw_mk_by_mhs.mahasiswa_id = '".$mhsid."' 
						AND vw_mk_by_mhs.tahun_akademik = '".$semester."')
					  ";
		
		}
		
		//$sql .= " ORDER BY namamk, tugas_id ASC";
		//echo $sql;
		$result = $this->db->query( $sql );
		
		
		return $result;
	}
	
	function get_jadwal(){
		$sql = "SELECT MID( MD5( jadwal_id ) , 9,7 ) as jadwal_id,
					   MID( MD5( mkditawarkan_id ) , 9,7 ) as mkid,
					   mkditawarkan_id, 
					   kelas, 
					   namamk,
					   nama,
					   CONCAT (namamk , ' ' , kelas , ' - ' , nama) as jadwal
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				";
		$result = $this->db->query( $sql );
	
		return $result;
	}
	
	function get_materi_by_mk($id=NULL){
		$sql = "SELECT MID( MD5( `materi_id` ) , 9,7 ) as `materi_id`, `judul` 
			    FROM db_ptiik_apps.`tbl_materimk` 
			    WHERE db_ptiik_apps.tbl_materimk.is_valid = 1
			   ";
		if($id){
			$sql .= "AND MID( MD5( `mkditawarkan_id` ) ,9,7 ) = '".$id."'";
		}
		$result = $this->db->query( $sql );
		
		return $result;
	}
	

	function get_mk_by_jadwal($id=NULL){
		$sql = "SELECT MID( MD5( jadwal_id ) , 9,7 ) as jadwal_id,
					   MID( MD5( mkditawarkan_id ) , 9,7 ) as mkid,
					   mkditawarkan_id, 
					   kelas, 
					   namamk,
					   nama,
					   CONCAT (namamk , ' ' , kelas , ' - ' , nama) as jadwal
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				";
		//echo $sql;
		if($id){
			$sql .= "WHERE MID( MD5( `jadwal_id` ) , 9,7 ) = '".$id."'";
		}
		
		$result = $this->db->query( $sql );
		
		return $result;
	}
	

	
	function tugas_id(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tugas_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_tugas WHERE left(tugas_id,6)='".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function cek_new_tugas_by_judul($ket=NULL){
		$sql = "SELECT judul 
				from `db_ptiik_apps`.`tbl_tugas` 
				where judul = '".$ket."' ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_id_tugas_by_id($ket=NULL){
		$sql = "SELECT tugas_id 
				from `db_ptiik_apps`.`tbl_tugas` 
				where judul = '".$ket."' ";
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->tugas_id;
			}
			return $id;
		}
	}
	
	function replace_tugas($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tugas',$datanya);
	}

	//====END TUGAS====//
	
	//====TUGAS MHS====//
	
	function read_tugas_mhs($id=NULL,$upload=NULL, $mhs=NULL){
		$sql = "SELECT tbl_tugas_mhs.mahasiswa_id as mhs_id,
				tbl_mahasiswa.nama as nama_mhs,
				tbl_mahasiswa.nim,
                tbl_tugas_mhs.tugas_id as tgs_id,
                MID( MD5(tbl_tugas_mhs.tugas_id) , 9,7 ) as tugas_id,
                tbl_tugas_mhs.upload_id as uploadid,
                MID( MD5(tbl_tugas_mhs.upload_id) , 9,7 ) as upload,
                tbl_tugas.judul as judul_tugas,
                tbl_tugas.instruksi as instruksi,
                tbl_tugas.keterangan as keterangan,
                tbl_tugas_mhs.tgl_upload,
                tbl_tugas_mhs.catatan,
                tbl_file.file_loc as tugas_loc,
                MID( MD5(tbl_file.file_id) , 9,7 ) as fileid,
                tbl_file.file_name as file_name,
                tbl_tugas_mhs.total_skor
                FROM db_ptiik_apps.`tbl_tugas_mhs`
                LEFT JOIN db_ptiik_apps.tbl_tugas ON tbl_tugas.tugas_id = tbl_tugas_mhs.tugas_id
                LEFT JOIN db_ptiik_apps.tbl_file ON tbl_file.upload_id = tbl_tugas_mhs.upload_id
                LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_tugas_mhs.mahasiswa_id
				WHERE 1 = 1
				";
		if($id){
			$sql = $sql . "	AND MID( MD5(tbl_tugas_mhs.tugas_id) , 9,7 ) = '".$id."'";
		}
		
		if($upload){
			$sql = $sql . "	AND MID( MD5(tbl_tugas_mhs.upload_id) , 9,7 ) = '".$upload."'";
		}
		
		if($mhs){
			$sql = $sql . " AND tbl_tugas_mhs.mahasiswa_id = '".$mhs."' ";
		}
		
		$sql = $sql. " ORDER BY tbl_tugas_mhs.last_update DESC ";
		
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function upload_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(upload_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_tugas_mhs WHERE left(upload_id,6) = '".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_mhs_id_by_username($id=NULL){
		$sql= "SELECT mahasiswa_id as mhs_id
		       FROM db_ptiik_apps.`tbl_mahasiswa` 
		       WHERE email = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->mhs_id;
			return $strresult;
		}
	}
	
	function get_tugas_mhs($tgs=NULL, $mhs=NULL){
		$sql= "SELECT upload_id as upload_id
		       FROM db_ptiik_apps.`tbl_tugas_mhs` 
		       WHERE MID( MD5(tugas_id) , 9,7 ) = '".$tgs."'";
		       
		if($mhs){       
			$sql.="	 AND mahasiswa_id = '".$mhs."' ";
		}
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->upload_id;
			return $strresult;
		}
	}
	
	function get_skor_by_tgs_mhs_id($tgs_id=NULL, $mhs_id=NULL){
		$sql= "SELECT total_skor as grade
		       FROM db_ptiik_apps.`tbl_tugas_mhs` 
		       WHERE MID( MD5(tugas_id) , 9,7 ) = '".$tgs_id."'
		       		 AND mahasiswa_id = '".$mhs_id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->grade;
			return $strresult;
		}
	}
	
	function update_skor($skor=NULL, $id=NULL){
		$sql= "UPDATE db_ptiik_apps.`tbl_tugas_mhs`
			   SET total_skor = '".$skor."'  
		       WHERE MID( MD5(upload_id) , 9,7 ) = '".$id."'
			 "; 
		$result = $this->db->query( $sql );
		if(isset($result)){
			return TRUE;
		}
	}
	
	function replace_tugas_mhs($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tugas_mhs',$datanya);
	}
	
	function replace_log_file($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_log_file',$datanya);
	}
	
	//====END TUGAS MHS====//
	
}
?>