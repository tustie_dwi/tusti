<?php
class model_krs extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_mkd($hari=NULL, $mkd_id=NULL, $fak_id=NULL, $cabang_id=NULL){ //melihat detail mkd yang bisa dipilih
		$sql = "SELECT 
					tbl_jadwalmk.kelas, 
					tbl_jadwalmk.hari, 
					tbl_jadwalmk.ruang_id ruang, 
					tbl_jadwalmk.jam_mulai, 
					tbl_jadwalmk.jam_selesai, 
					tbl_mkditawarkan.kuota, 
					tbl_namamk.keterangan as namamk, 
					tbl_matakuliah.kode_mk,
					tbl_matakuliah.sks,
					mid(md5(tbl_jadwalmk.jadwal_id),6,6) jadwal_id,
					mid(md5(tbl_mkditawarkan.mkditawarkan_id),6,6) mkditawarkan_id,
					tbl_kapasitas_kelas.terisi
				FROM `db_ptiik_apps`.`tbl_jadwalmk` tbl_jadwalmk
				INNER JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				INNER JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN `db_ptiik_apps`.`tbl_namamk` tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_mkditawarkan.tahun_akademik = tbl_tahunakademik.tahun_akademik
				LEFT JOIN `db_ptiik_apps`.`tbl_kapasitas_kelas` tbl_kapasitas_kelas ON tbl_kapasitas_kelas.jadwal_id = tbl_jadwalmk.jadwal_id
				WHERE tbl_tahunakademik.is_aktif = 1
		";
		if($fak_id) $sql .= " AND tbl_namamk.fakultas_id = '".$fak_id."'";
		if($cabang_id) $sql .= " AND tbl_jadwalmk.cabang_id = '".$cabang_id."'";
		
		if($hari) $sql .= " AND tbl_jadwalmk.hari = '".$hari."'";
		else $sql .= " AND tbl_jadwalmk.hari = 'senin'";
		
		if($mkd_id) $sql .= " AND mid(md5(tbl_mkditawarkan.mkditawarkan_id),6,6) = '".$mkd_id."'";
		
		$sql .= " ORDER BY tbl_jadwalmk.hari DESC, tbl_jadwalmk.jam_mulai ASC, tbl_namamk.keterangan ASC, tbl_jadwalmk.kelas ASC";
		
		return $this->db->query($sql);
	}
	
	function get_mk_for_select($fak_id=NULL, $cabang_id=NULL){ //melihat list mkd yang di select option
		$sql = "SELECT 
					DISTINCT
					mid(md5(tbl_mkditawarkan.mkditawarkan_id),6,6) mkditawarkan_id, 
					tbl_namamk.keterangan
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan
				INNER JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_mkditawarkan.tahun_akademik = tbl_tahunakademik.tahun_akademik 
				INNER JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN `db_ptiik_apps`.`tbl_namamk` tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				WHERE tbl_tahunakademik.is_aktif = 1
				";
		if($fak_id!=NULL) $sql .= " AND tbl_namamk.fakultas_id = '".$fak_id."'";
		if($cabang_id!=NULL) $sql .= " AND tbl_mkditawarkan.cabang_id = '".$cabang_id."'";
		
		$sql .= " ORDER by tbl_namamk.keterangan";
		return $this->db->query($sql);
	}
	
	function get_fakultas_id_by_mhs($mhs_id=NULL){ //mendapatkan fakultas_id dari mhs_id yang diinputkan
		$sql = "SELECT tbl_prodi.fakultas_id, tbl_mahasiswa.cabang_id
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				WHERE mahasiswa_id = '".$mhs_id."'";
		
		$dt = $this->db->getRow( $sql );
		// $dt = $dt->fakultas_id;
		return $dt;
	}
	
	function cek_kuota($jadwal_id=NULL){ //melakukan pengecekan kuota saat fungsi tambah krs
		$sql = "SELECT 
					COUNT(tbl_jadwalmk.jadwal_id) jml_peserta,
					tbl_jadwalmk.kelas, 
					tbl_mkditawarkan.kuota,
					tbl_mkditawarkan.mkditawarkan_id,
					tbl_matakuliah.sks,
					tbl_namamk.keterangan namamk,
					tbl_matakuliah.kode_mk
					
				FROM `db_ptiik_apps`.`tbl_krs` tbl_krs
				LEFT JOIN `db_ptiik_apps`.`tbl_jadwalmk` ON tbl_krs.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id AND tbl_krs.kelas = tbl_jadwalmk.kelas
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				LEFT JOIN `db_ptiik_apps`.`tbl_namamk` tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				INNER JOIN `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa ON tbl_krs.mahasiswa_id = tbl_mahasiswa.mahasiswa_id AND tbl_jadwalmk.prodi_id = tbl_mahasiswa.prodi_id
				INNER JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_krs.inf_semester
				WHERE MID(MD5(tbl_jadwalmk.jadwal_id),6,6) = '".$jadwal_id."' AND tbl_tahunakademik.is_aktif = 1
				GROUP BY tbl_jadwalmk.kelas, tbl_jadwalmk.jadwal_id, tbl_jadwalmk.prodi_id";
		$data = $this->db->getRow($sql);
		//echo $sql;
		if($data) {
			return $data;
		}
		else{
			$sql = "SELECT 
						'0' jml_peserta, 
						tbl_jadwalmk.kelas, 
						tbl_mkditawarkan.kuota, 
						tbl_mkditawarkan.mkditawarkan_id, 
						tbl_matakuliah.sks, 
						tbl_namamk.keterangan namamk, 
						tbl_matakuliah.kode_mk
					FROM `db_ptiik_apps`.`tbl_jadwalmk` tbl_jadwalmk
					LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id 
					LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id 
					LEFT JOIN `db_ptiik_apps`.`tbl_namamk` tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id 
					WHERE MID(MD5(tbl_jadwalmk.jadwal_id),6,6) = '".$jadwal_id."' ";
			return $this->db->getRow($sql);
		}
	}
	
	function cek_jml_sks($mhs_id=NULL){ //mengecek apakah bisa nambah sks lagi a? dengan nilai ip terakhir mhs
		$sql = "SELECT tbl_mhs_ipk.ip, tbl_mhs_ipk.total_sks jml_sks, tbl_tahunakademik.tahun_akademik, tbl_mhs_ipk.inf_semester
				FROM `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_mhs_ipk.tahun_akademik
				WHERE tbl_tahunakademik.is_aktif <> 1 AND tbl_mhs_ipk.mahasiswa_id = '".$mhs_id."'
				ORDER BY tbl_mhs_ipk.tahun_akademik DESC
				LIMIT 0,1";
		$data = $this->db->getRow($sql);
		if($data) return $data;
		else {
			$sql = "SELECT 
					tbl_krs.inf_semester, 
					SUM(tbl_matakuliah.sks * tbl_krs.inf_bobot)/SUM(tbl_matakuliah.sks) ip, 
					SUM(tbl_matakuliah.sks) jml_sks
				FROM `db_ptiik_apps`.`tbl_krs` tbl_krs
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_krs.mkditawarkan_id
				LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik =  tbl_krs.inf_semester
				WHERE tbl_krs.mahasiswa_id = '".$mhs_id."' AND tbl_tahunakademik.is_aktif <> 1
				GROUP BY tbl_krs.inf_semester
				ORDER BY tbl_mkditawarkan.tahun_akademik DESC";		
				
			$data = $this->db->query($sql);
			
			$ipk = 0;
			$total_ip = 0;
			$count = 0;
			foreach($data as $key){ //proses hitung ipk
				if($key->ip != ''){
					$count++;
					$total_ip += $key->ip;
					$ipk = $total_ip / $count;
					
					$data_save = array( //simpan ke tabel mhs_ipk
						'mahasiswa_id' => $mhs_id,
						'tahun_akademik' => $key->inf_semester,
						'inf_semester' => ($count),
						'ipk' => $ipk,
						'ip' => $key->ip,
						'total_sks' => $key->jml_sks
					);	
					
					$this->db->replace('db_ptiik_apps`.`tbl_mhs_ipk', $data_save);
				}
			}
			
			$sql = "SELECT tbl_mhs_ipk.ip, tbl_mhs_ipk.total_sks jml_sks, tbl_tahunakademik.tahun_akademik, tbl_mhs_ipk.inf_semester
				FROM `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_mhs_ipk.tahun_akademik
				WHERE tbl_tahunakademik.is_aktif <> 1 AND tbl_mhs_ipk.mahasiswa_id = '".$mhs_id."'
				ORDER BY tbl_mhs_ipk.tahun_akademik DESC
				LIMIT 0,1";
			return $data = $this->db->getRow($sql); //mengembalikan nilai yang baru saja disimpan
		}
	}

	function rekap_ipk($mhs_id=NULL){
		$sql = "SELECT tbl_mhs_ipk.ip
				FROM `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_mhs_ipk.tahun_akademik
				WHERE tbl_tahunakademik.is_aktif <> 1 AND mid(md5(tbl_mhs_ipk.mahasiswa_id),6,6) = '".$mhs_id."'
				ORDER BY tbl_mhs_ipk.tahun_akademik DESC
				LIMIT 0,1";
		$data = $this->db->getRow($sql);
		if($data) return $data;
		else {
			$sql = "SELECT 
					tbl_krs.inf_semester, 
					SUM(tbl_matakuliah.sks * tbl_krs.inf_bobot)/SUM(tbl_matakuliah.sks) ip, 
					SUM(tbl_matakuliah.sks) jml_sks
				FROM `db_ptiik_apps`.`tbl_krs` tbl_krs
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_krs.mkditawarkan_id
				LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik =  tbl_krs.inf_semester
				WHERE mid(md5(tbl_krs.mahasiswa_id),6,6) = '".$mhs_id."' AND tbl_tahunakademik.is_aktif <> 1
				GROUP BY tbl_krs.inf_semester
				ORDER BY tbl_mkditawarkan.tahun_akademik DESC";		
				
			$data = $this->db->query($sql);
			
			$mhs_id = $this->get_mhsid($mhs_id);
			
			$ipk = 0;
			$total_ip = 0;
			$count = 0;
			foreach($data as $key){ //proses hitung ipk
				if($key->ip != ''){
					$count++;
					$total_ip += $key->ip;
					$ipk = $total_ip / $count;
					
					$data_save = array( //simpan ke tabel mhs_ipk
						'mahasiswa_id' => $mhs_id,
						'tahun_akademik' => $key->inf_semester,
						'inf_semester' => ($count),
						'ipk' => $ipk,
						'ip' => $key->ip,
						'total_sks' => $key->jml_sks
					);	
					
					$this->db->replace('db_ptiik_apps`.`tbl_mhs_ipk', $data_save);
				}
			}
		}
	}
	
	function get_mhsid($mhs_id){
		$sql = "SELECT mahasiswa_id 
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa 
				WHERE mid(md5(tbl_mahasiswa.mahasiswa_id),6,6) = '".$mhs_id."'";
		
		$data = $this->db->getRow($sql);
		return $data->mahasiswa_id;
	}

	function get_sks_curr($mhsid){
		$sql = "SELECT SUM(tbl_krs_tmp.sks) jml
				FROM `db_ptiik_apps`.`tbl_krs_tmp` tbl_krs_tmp
				WHERE tbl_krs_tmp.mahasiswa_id = '".$mhsid."'
					AND tbl_krs_tmp.is_approve = 0";
		$data = $this->db->getRow($sql);
		return $data->jml;
	}
	
	function get_krs_id(){ //mendapatkan krs_id yang baru
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(krs_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_krs_tmp WHERE left(krs_id,6)='".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_tahunakademik_aktif(){ //mendapatkan tahun akademik yang aktif saat ini
		$sql = "SELECT tbl_tahunakademik.tahun_akademik 
			FROM `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik 
			WHERE tbl_tahunakademik.is_aktif = '1'";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->tahun_akademik;
		return $strresult;
	}
	
	function get_krs_id_all($mhs_id=NULL){ //untuk melihat mkd_id yang sudah diambil oleh mhs
		$sql = "SELECT 
					MID(MD5(tbl_krs_tmp.mkditawarkan_id),6,6) mkditawarkan_id, tbl_krs_tmp.krs_id, tbl_krs_tmp.tahun_akademik
				FROM `db_ptiik_apps`.`tbl_krs_tmp` tbl_krs_tmp
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_krs_tmp.tahun_akademik 
				WHERE tbl_krs_tmp.mahasiswa_id = '".$mhs_id."' AND tbl_tahunakademik.is_aktif = 1
				ORDER BY tbl_krs_tmp.inf_semester DESC
		";
		
		return $this->db->query($sql);
	}
	
	function cek_parent_id($jadwal_id, $mhs_id){ //untuk mengecek mata kuliah prasyarat
		$sql = "SELECT tbl_mkditawarkan.parent_id
				FROM `db_ptiik_apps`.`tbl_jadwalmk` tbl_jadwalmk
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				WHERE MID(MD5(tbl_jadwalmk.jadwal_id),6,6) = '".$jadwal_id."'";
		
		$data = $this->db->getRow($sql);
		$parent_id = $data->parent_id;
		
		if($parent_id == '' || $parent_id == NULL || $parent_id == 0) {
			return TRUE;
			exit;
		}
		
		$sql = "SELECT tbl_mkditawarkan.mkditawarkan_id
				FROM `db_ptiik_apps`.`tbl_krs`
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_krs.inf_semester
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_krs.mkditawarkan_id
				WHERE tbl_krs.mahasiswa_id = '".$mhs_id."' AND tbl_tahunakademik.is_aktif <> 1
				AND tbl_mkditawarkan.mkditawarkan_id = '".$parent_id."'";
		
		$data = $this->db->query($sql);
		if($data > 0) return TRUE;
		else return FALSE;
	}
	
	function get_waktu($mhsid=NULL){
		$sql = "SELECT DISTINCT tbl_jadwalmk.hari, tbl_jadwalmk.jam_mulai, tbl_jadwalmk.jam_selesai
				FROM `db_ptiik_apps`.`tbl_krs_tmp` tbl_krs_tmp
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik 
					ON tbl_tahunakademik.tahun_akademik = tbl_krs_tmp.tahun_akademik 
					AND tbl_tahunakademik.is_aktif = 1
				LEFT JOIN `db_ptiik_apps`.`tbl_jadwalmk` tbl_jadwalmk 
					ON tbl_jadwalmk.mkditawarkan_id = tbl_krs_tmp.mkditawarkan_id
					AND tbl_jadwalmk.kelas = tbl_krs_tmp.kelas
				WHERE tbl_krs_tmp.mahasiswa_id = '".$mhsid."'
				GROUP BY tbl_krs_tmp.mkditawarkan_id
				ORDER BY tbl_jadwalmk.hari";
				
		return $this->db->query($sql);
	}
	
	function add_krs($data=NULL){ //simpan krs
		$this->db->replace('db_ptiik_apps`.`tbl_krs_tmp',$data);
	}
	
	function del_krs($krs_id=NULL){ //hapus krs
		$sql ="DELETE FROM `db_ptiik_apps`.`tbl_krs_tmp` WHERE mid(md5(`tbl_krs_tmp`.`krs_id`),6,6) = '".$krs_id."'";
		return $this->db->query($sql);
	}
	
	function get_tgl_krs(){ //mendapatkan tgl_mulai dan tgl_selesainya KRS-an
		$sql ="SELECT 
				tbl_kalenderakademik.tgl_mulai tgl_mulai, 
				tbl_kalenderakademik.tgl_selesai tgl_selesai
			FROM `db_ptiik_apps`.`tbl_kalenderakademik` tbl_kalenderakademik
			LEFT JOIN `db_ptiik_apps`.`tbl_jeniskegiatan` tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_kalenderakademik.jenis_kegiatan_id
			WHERE tbl_kalenderakademik.is_aktif = '1'
				AND tbl_jeniskegiatan.keterangan = 'KRS'";
	return $this->db->getRow($sql);
	}
	
	function update_kapasitas_kelas($jadwal_id, $kelas){ //update kapasitas kelas
		$sql = "UPDATE `db_ptiik_apps`.`tbl_kapasitas_kelas` SET `tbl_kapasitas_kelas`.`terisi` = `tbl_kapasitas_kelas`.`terisi`+1
				WHERE mid(md5(`tbl_kapasitas_kelas`.`jadwal_id`),6,6) = '".$jadwal_id."' AND `tbl_kapasitas_kelas`.`kelas` = '".$kelas."'";
		
		if($this->db->query($sql));
		else {
			$sql = "SELECT tbl_jadwalmk.kelas, tbl_jadwalmk.jadwal_id, tbl_mkditawarkan.kuota
					FROM `db_ptiik_apps`.`tbl_jadwalmk` tbl_jadwalmk
					LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
					WHERE mid(md5(tbl_jadwalmk.jadwal_id),6,6) = '".$jadwal_id."'";
			$key = $this->db->getRow($sql);
			
			$data = array(
				'kelas' => $key->kelas,
				'jadwal_id' => $key->jadwal_id,
				'inf_kuota' => $key->kuota,
				'terisi' => 1
			);
			
			$this->db->replace('db_ptiik_apps`.`tbl_kapasitas_kelas',$data);
		}
	}
	
	function decrease_kapasitas_kelas($krs_id){ //upadte kuota kelas saat mhs melakukan drop mk
		$sql = "SELECT 
					tbl_jadwalmk.jadwal_id, 
					tbl_jadwalmk.kelas
				FROM `db_ptiik_apps`.`tbl_krs_tmp` tbl_krs_tmp
				LEFT JOIN `db_ptiik_apps`.`tbl_jadwalmk` tbl_jadwalmk ON tbl_krs_tmp.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id 
					AND tbl_jadwalmk.kelas = tbl_krs_tmp.kelas
				WHERE MID(MD5(tbl_krs_tmp.krs_id),6,6) = '".$krs_id."'";
		$data = $this->db->query($sql);
		
		foreach ($data as $key) {
			$jadwal_id = $key->jadwal_id;
			$kelas = $key->kelas;
			
			$sql = "UPDATE `db_ptiik_apps`.`tbl_kapasitas_kelas` SET `tbl_kapasitas_kelas`.`terisi` = `tbl_kapasitas_kelas`.`terisi`-1
					WHERE `tbl_kapasitas_kelas`.`jadwal_id` = '".$jadwal_id."' AND `tbl_kapasitas_kelas`.`kelas` = '".$kelas."'";
			$this->db->query($sql);
		}
	}
		
	function get_identitas_mhs($mhs_id){ //mendapatkan identitas mhs
		$sql = "SELECT tbl_mahasiswa.nama, tbl_mahasiswa.mahasiswa_id, tbl_mahasiswa.angkatan
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				WHERE tbl_mahasiswa.mahasiswa_id = '".$mhs_id."' OR mid(md5(tbl_mahasiswa.mahasiswa_id),6,6) = '".$mhs_id."'
			";
		return $this->db->getRow($sql);
	}
		
	function get_mk_krs($mhsid){ //melihat mata kuliah yg sementara
		$sql = "SELECT DISTINCT
					mid(md5(tbl_krs_tmp.krs_id),6,6) krs_id,
					CONCAT (tbl_matakuliah.kode_mk, ' - ', tbl_namamk.keterangan) as matakuliah,
					tbl_matakuliah.kode_mk,
					tbl_namamk.keterangan as `nama_mk`,
					tbl_krs_tmp.kelas,
					tbl_matakuliah.sks,
					tbl_krs_tmp.is_approve,
					mid(md5(concat(tbl_krs_tmp.krs_id, '01')),6,6) hidId,
					tbl_jadwalmk.jam_mulai,
					tbl_jadwalmk.jam_selesai,
					tbl_jadwalmk.ruang_id,
					tbl_jadwalmk.hari
					
				FROM `db_ptiik_apps`.`tbl_krs_tmp` tbl_krs_tmp
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_krs_tmp.tahun_akademik
				
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_krs_tmp.mkditawarkan_id
				LEFT JOIN `db_ptiik_apps`.`tbl_jadwalmk` tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
					AND tbl_jadwalmk.kelas = tbl_krs_tmp.kelas
				LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				LEFT JOIN `db_ptiik_apps`.`tbl_namamk` tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				
				WHERE (tbl_krs_tmp.mahasiswa_id = '".$mhsid."' OR mid(md5(tbl_krs_tmp.mahasiswa_id),6,6) = '".$mhsid."')
					AND tbl_tahunakademik.is_aktif = '1'
				GROUP BY tbl_krs_tmp.krs_id
				ORDER by tbl_namamk.keterangan
		";

		return $this->db->query($sql);
	}

	function get_semester($mhs_id){ //mendapatkan semester mhs saat ini
		$sql = "SELECT tbl_mhs_ipk.inf_semester
				FROM `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_mhs_ipk.tahun_akademik
				WHERE tbl_mhs_ipk.mahasiswa_id = '".$mhs_id."' OR mid(md5(tbl_mhs_ipk.mahasiswa_id),6,6) = '".$mhs_id."'
					AND tbl_tahunakademik.is_aktif <> 1
				ORDER BY tbl_mhs_ipk.tahun_akademik
				LIMIT 0,1";
				
		return $this->db->getRow($sql);
	}
	
	function get_ip_terakhir($mhs_id){ //mendapatkan semester mhs saat ini
		$sql = "SELECT tbl_mhs_ipk.ip, tbl_mhs_ipk.ipk
				FROM `db_ptiik_apps`.`tbl_mhs_ipk` tbl_mhs_ipk
				WHERE mid(md5(tbl_mhs_ipk.mahasiswa_id),6,6) = '".$mhs_id."'
				ORDER BY tbl_mhs_ipk.tahun_akademik
				LIMIT 0,1";
		return $this->db->getRow($sql);
	}
	
	//------------------------------ approve dosen --------------------------------------
	
	function get_mhs_by_pa($dosenid){ //mendapatkan mahasiswa yg diampu oleh dosen yg login
		$sql = "SELECT 
					tbl_mahasiswa.nama, 
					tbl_mahasiswa.nim, 
					tbl_mahasiswa.angkatan, 
					tbl_mahasiswa.tmp_lahir, 
					tbl_mahasiswa.tgl_lahir, 
					tbl_mahasiswa.alamat,
					tbl_mahasiswa.hp,
					mid(md5(tbl_mahasiswa.mahasiswa_id),6,6) mahasiswa_id,					
					tbl_mahasiswa.foto as foto_mhs,
					tbl_mahasiswa.is_aktif as status,
					(SELECT tbl_prodi.keterangan FROM db_ptiik_apps.tbl_prodi tbl_prodi WHERE tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id) as prodi,
					(SELECT tbl_prodi.strata FROM db_ptiik_apps.tbl_prodi tbl_prodi WHERE tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id) as strata,
					(SELECT tbl_cabang.keterangan FROM db_ptiik_apps.tbl_cabang tbl_cabang WHERE tbl_cabang.cabang_id = tbl_mahasiswa.cabang_id) as cabang
				FROM `db_ptiik_apps`.`tbl_dosen_pembimbing`
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa`ON  tbl_mahasiswa.mahasiswa_id = tbl_dosen_pembimbing.mahasiswa_id
				WHERE tbl_dosen_pembimbing.karyawan_id = '".$dosenid."'
				ORDER by tbl_mahasiswa.nama";
		return $this->db->query($sql);
	}
	
	function approve_krs($dosenid){ //untuk krs yg disetujui oleh dosen
		if(isset($_POST['krs_id'])) :
			foreach($_POST['krs_id'] as $key){
				$sql = "UPDATE `db_ptiik_apps`.`tbl_krs_tmp` SET `tbl_krs_tmp`.`is_approve` = 1 
						WHERE mid(md5(`tbl_krs_tmp`.`krs_id`),6,6) = '".$key."'";
				
				$this->db->query($sql);
				
				$sql = "INSERT INTO `db_ptiik_apps`.`tbl_krs` (SELECT 
							CONCAT (tbl_krs_tmp.krs_id, '01'),
							tbl_krs_tmp.mahasiswa_id,
							tbl_krs_tmp.mkditawarkan_id,
							tbl_krs_tmp.tahun_akademik,
							tbl_krs_tmp.nilai_akhir,
							'0',
							tbl_krs_tmp.kelas,
							tbl_krs_tmp.inf_huruf,
							'".$dosenid."',
							CURRENT_TIMESTAMP()
						FROM `db_ptiik_apps`.`tbl_krs_tmp` tbl_krs_tmp
						WHERE mid(md5(tbl_krs_tmp.krs_id),6,6) = '".$key."')";
						
				$this->db->query($sql);	
			}
		endif;
		
	}
	
	function batal_krs(){ //query untuk membatalkan krs
		$krsid = $_POST['krsid'];
		$sql = "UPDATE `db_ptiik_apps`.`tbl_krs_tmp` SET `tbl_krs_tmp`.`is_approve` = 0 
				WHERE mid(md5(`tbl_krs_tmp`.`krs_id`),6,6) = '".$krsid."'";
		
		$this->db->query($sql);
		
		$krsid = $_POST['hidId'];
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_krs`
				WHERE mid(md5(`tbl_krs`.`krs_id`),6,6) = '".$krsid."'";
		
		$this->db->query($sql);
	}
	
}

?>