<?php
class model_khs extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function read($semester=NULL,$mhsid=NULL, $nama=NULL, $semesterke=NULL){
		$sql = "SELECT vw_khs_mhs.mahasiswa_id,
					   vw_khs_mhs.inf_semester,
					   vw_khs_mhs.nilai_akhir,
					   vw_khs_mhs.kelas,
					   CONCAT(vw_khs_mhs.kode_mk,' - ',vw_khs_mhs.nama_mk) as matakuliah,
					   vw_khs_mhs.sks,
					   vw_khs_mhs.nama,
					   vw_khs_mhs.angkatan,
					   vw_khs_mhs.inf_bobot,
					   tbl_tahunakademik.is_ganjil,
					   tbl_tahunakademik.tahun,
					   tbl_tahunakademik.is_pendek
				FROM db_ptiik_apps.`vw_khs_mhs`
				LEFT JOIN db_ptiik_apps.`tbl_tahunakademik` ON vw_khs_mhs.tahun_akademik = tbl_tahunakademik.tahun_akademik
				WHERE db_ptiik_apps.`vw_khs_mhs`.mahasiswa_id = '".$mhsid."'";
			
		if($semester!=""){	
			$sql .= " AND db_ptiik_apps.`vw_khs_mhs`.tahun_akademik = '".$semester."'";
		}
		
		if($semesterke!=""){	
			$sql .= " GROUP BY vw_khs_mhs.inf_semester";
		}
		
		if($nama!=""){
			$sql .= " GROUP BY vw_khs_mhs.mahasiswa_id";
			$sql .= " ";
		}
		
		if($semester!=""){	
			$sql .= " ORDER BY vw_khs_mhs.nama_mk";
		}
			
			// echo $sql;   
		$result = $this->db->query( $sql );
		if(isset($result)){
			return $result;
		}
	}
	
	// function get_semester(){
		// $sql = "SELECT
					// db_ptiik_apps.tbl_tahunakademik.tahun_akademik,
					// db_ptiik_apps.tbl_tahunakademik.tahun,
					// db_ptiik_apps.tbl_tahunakademik.is_ganjil,
					// db_ptiik_apps.tbl_tahunakademik.is_aktif,
					// db_ptiik_apps.tbl_tahunakademik.is_pendek
					// FROM
					// db_ptiik_apps.tbl_tahunakademik 
					// ORDER BY db_ptiik_apps.tbl_tahunakademik.tahun_akademik DESC";
		// $result = $this->db->query($sql);
// 
		// return $result;
	// }
	
	function get_semester($mhsid=NULL){
		$sql = "SELECT vw_khs_mhs.inf_semester,
					   tbl_tahunakademik.tahun,
					   tbl_tahunakademik.is_ganjil,
					   tbl_tahunakademik.is_aktif,
					   tbl_tahunakademik.is_pendek,
					   tbl_tahunakademik.tahun_akademik
				FROM db_ptiik_apps.vw_khs_mhs
				LEFT JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = vw_khs_mhs.inf_semester
				WHERE 1
			   ";
		
		if($mhsid){
		 $sql .= " AND vw_khs_mhs.mahasiswa_id = '".$mhsid."'";
		}
		
		$sql .= " GROUP BY vw_khs_mhs.inf_semester ORDER BY tahun_akademik DESC";
		
		$result = $this->db->query($sql);
		
		// echo $sql;
		return $result;
	}
	
	function get_semester_aktif(){
		$sql = "SELECT
					db_ptiik_apps.tbl_tahunakademik.tahun_akademik,
					db_ptiik_apps.tbl_tahunakademik.tahun,
					db_ptiik_apps.tbl_tahunakademik.is_ganjil,
					db_ptiik_apps.tbl_tahunakademik.is_aktif,
					db_ptiik_apps.tbl_tahunakademik.is_pendek
					FROM
					db_ptiik_apps.tbl_tahunakademik WHERE is_aktif='1'";
		$dt = $this->db->getRow($sql);		
		
		$result = $dt->tahun_akademik;
	
		return $result;

	}
	
	function get_sks_all_count($mhsid=NULL){
		$sql = "SELECT SUM(vw_khs_mhs.sks*vw_khs_mhs.inf_bobot) as sks_nilai, 
					   ((SUM(vw_khs_mhs.sks*vw_khs_mhs.inf_bobot)) / SUM(vw_khs_mhs.sks)) as IPK
				FROM db_ptiik_apps.`vw_khs_mhs`
				WHERE db_ptiik_apps.`vw_khs_mhs`.mahasiswa_id = '".$mhsid."'";
		$result = $this->db->query( $sql );
		if(isset($result)){
			return $result;
		}
	}
	
	function get_nilai_all_count($mhsid=NULL){
		$sql = "SELECT SUM(vw_khs_mhs.sks) as sks_all
					FROM db_ptiik_apps.`vw_khs_mhs`
					WHERE db_ptiik_apps.`vw_khs_mhs`.mahasiswa_id = '".$mhsid."'";
		$dt = $this->db->getRow($sql);
		$result = $dt->sks_all;
		return $result;
	}
}
?>