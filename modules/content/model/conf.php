<?php
class model_conf extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	
	function get_json_content($term=NULL){
		$sql= "SELECT id as `id`, content_title as `text` FROM contents
					WHERE content_title like '%".$term."%' 
				ORDER BY id DESC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_semester(){
		$sql = "SELECT
					db_ptiik_apps.tbl_tahunakademik.tahun_akademik as inf_semester,
					db_ptiik_apps.tbl_tahunakademik.tahun,
					db_ptiik_apps.tbl_tahunakademik.is_ganjil,
					db_ptiik_apps.tbl_tahunakademik.is_aktif,
					db_ptiik_apps.tbl_tahunakademik.is_pendek
					FROM
					db_ptiik_apps.tbl_tahunakademik
					ORDER BY tahun_akademik DESC
			   ";
		
		$result = $this->db->query($sql);

		return $result;
	}
	
	public function log($tablename, $datanya, $username, $action){
		$return_arr = array();
		array_push($return_arr,$datanya);		
			
		$data['user']		= $username;
		$data['session']	= session_id();
		$data['tgl']		= date("Y-m-d H:i:s");
		$data['reference']	= $_SERVER['HTTP_REFERER'];
		$data['table_name']	= $tablename;
		$data['field']		= json_encode($return_arr);	
		$data['action']		= $action;			
		
		$result = $this->db->insert("coms`.`coms_log", $data);
		
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) return false;
	}
	
	function get_fakultas(){
		$sql = "SELECT mid(md5(fakultas_id),6,6) as fakultasid, keterangan, fakultas_id as hid_id
				FROM `db_ptiik_apps`.`tbl_fakultas`";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_cabangub(){
		$sql = "SELECT `cabang_id`, `keterangan`
				FROM `db_ptiik_apps`.`tbl_cabang` 
				WHERE 1
				";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_jenis_kegiatan($str=NULL){
		$sql = " SELECT
					tbl_jeniskegiatan.jenis_kegiatan_id AS id,
					tbl_jeniskegiatan.keterangan AS `value`,
					tbl_jeniskegiatan.kategori,
					tbl_jeniskegiatan.kode_kegiatan
					FROM db_ptiik_apps.tbl_jeniskegiatan WHERE 1=1 ";
		if($str){
			$sql = $sql. " AND kategori = '".$str."' ";
		}
		
		//$sql = $sql. " ORDER BY value  ASC ";
		
		$result = $this->db->query( $sql );
		
		return $result;		
	}
	
	function get_kegiatan(){
		$sql = "SELECT 
					tbl_jeniskegiatan.jenis_kegiatan_id AS id, 
					tbl_jeniskegiatan.keterangan AS `value`,
					tbl_jeniskegiatan.kategori,
					tbl_jeniskegiatan.kode_kegiatan 
					FROM db_ptiik_apps.tbl_jeniskegiatan ";
		
		$result = $this->db->query( $sql );
	
		return $result;		
	}
	
	
	function get_hari(){
		$sql = "SELECT hari as `value`, id FROM db_ptiik_apps.tbl_hari ORDER BY id ASC";
		$result = $this->db->query( $sql );
		
		return $result;		
	}
	
	function get_ruang($kategori=NULL, $fakultas=NULL, $cabang=NULL){
		$sql = "SELECT ruang_id as `id`, kode_ruang, keterangan  as `value`, kapasitas, kategori_ruang FROM db_ptiik_apps.tbl_ruang WHERE 1 = 1 ";
		
		if($kategori){
			$sql = $sql. " AND kategori_ruang = '".$kategori."' ";
		}
		
		if(($fakultas)&&($fakultas!="-")){
			$sql = $sql. " AND fakultas_id = '".$fakultas."' ";
		}
		
		if($cabang){
			$sql = $sql. " AND cabang_id = '".$cabang."' ";
		}
		
		$sql = $sql. " ORDER BY kode_ruang ASC ";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	
	}
	
	function get_unit($fakultas=NULL, $cabang=NULL){
		$sql = "SELECT
				tbl_unit_kerja.unit_id as `id`,
				tbl_unit_kerja.fakultas_id,
				tbl_unit_kerja.keterangan as `value`,
				tbl_unit_kerja.kode,
				tbl_unit_kerja.parent_id
				FROM
				db_ptiik_apps.tbl_unit_kerja WHERE 1=1 ";
		if(($fakultas)&&($fakultas!="-")){
			$sql = $sql. " AND fakultas_id = '".$fakultas."' ";
		}
		
		if($cabang){
			$sql = $sql. " AND cabang_id = '".$cabang."' ";
		}
		$sql = $sql. " ORDER BY keterangan ASC ";
		
		$result = $this->db->query( $sql );
		//var_dump($result);
		return $result;	
		
	}
	
	/*master dosen */
	function get_dosen($fakultas=NULL, $term=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE is_status='dosen' AND is_aktif NOT IN ('keluar','meninggal')  ";
		if($fakultas){
			$sql = $sql. " AND fakultas_id = '".$fakultas."' ";
		}
		
		if($term){
			$sql = $sql. " AND (concat(nik,'-',nama)) like '%".$term."%' ";
		} 
		$sql = $sql . " ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_staff($fakultas=NULL, $term=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE  is_aktif NOT IN ('keluar','meninggal')  ";
		if($fakultas){
			$sql = $sql. " AND fakultas_id = '".$fakultas."' ";
		}
		
		if($term){
			$sql = $sql. " AND (concat(nik,'-',nama)) like '%".$term."%' ";
		} 
		$sql = $sql . " ORDER BY nama ASC limit 0,20 ";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_id_dosen($str=NULL){
		$sql= "SELECT karyawan_id FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE nama like '%".$str."%' ";
		$result = $this->db->getRow( $sql );	
		
		return $result;	
	}
	
}