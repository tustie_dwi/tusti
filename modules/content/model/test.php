<?php
class model_test extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
		
	function update_nilai($detail_id,$inf_skor,$catatan){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_test_hasil_detail` 
				SET `inf_skor` = $inf_skor, `catatan` = '".$catatan."'
				WHERE `detail_id` = '".$detail_id."'";
		$this->db->query($sql);
	}
	
	function update_total_skor($hasil_id,$total_skor){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_test_hasil` 
				SET `total_skor` = $total_skor
				WHERE `hasil_id` = '".$hasil_id."'";
		$this->db->query($sql);
	}	
	
	function get_total_skor($hasil_id){
		$sql = "SELECT sum(tbl_test_hasil_detail.inf_skor) as nilai
				FROM `db_ptiik_apps`.`tbl_test_hasil`
				LEFT JOIN `db_ptiik_apps`.`tbl_test_hasil_detail` ON tbl_test_hasil.hasil_id = tbl_test_hasil_detail.hasil_id
				WHERE tbl_test_hasil.hasil_id = '".$hasil_id."'";			
		return $this->db->query($sql);
	}
	
	function replace_nilai($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_hasil_detail',$datanya);
	}
	
	function get_test_hasil($id=NULL){
		$sql = "SELECT 
					mid(md5(tbl_test.test_id),9,7) test_id,
					tbl_test.test_id as hid_id,
					tbl_test.jadwal_id,
					tbl_test.materi_id,
					tbl_test.mkditawarkan_id,
					tbl_test.judul judul_test,
					tbl_test.tgl_mulai,
					tbl_test.tgl_selesai,
					tbl_test.instruksi,
					tbl_test.keterangan,
					tbl_test.is_random,
					tbl_test.is_publish,
					tbl_test.user_id,
					tbl_test.last_update,
					tbl_materimk.judul											
				FROM `db_ptiik_apps`.`tbl_test`
				LEFT JOIN `db_ptiik_apps`.`tbl_materimk` ON tbl_test.materi_id = tbl_materimk.materi_id
			";
		if($id){
			$sql .= " WHERE mid(md5(tbl_test.test_id),9,7) = '".$id."'";
		}
		return $this->db->query($sql);
	}	
	
	//--------------------------------------------------------- list mk --------------------------------------------------
	// function get_mk($mhsid=NULL){
		// $sql = "SELECT 
				// mid(md5(tbl_krs.mkditawarkan_id),9,7) mkditawarkan_id, tbl_namamk.keterangan, tbl_matakuliah.sks, tbl_matakuliah.kurikulum
			// FROM `db_ptiik_apps`.`tbl_krs` `tbl_krs`
			// RIGHT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` `tbl_mkditawarkan` ON tbl_mkditawarkan.mkditawarkan_id = tbl_krs.mkditawarkan_id
			// RIGHT JOIN `db_ptiik_apps`.`tbl_matakuliah` `tbl_matakuliah` ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
			// RIGHT JOIN `db_ptiik_apps`.`tbl_namamk` `tbl_namamk` ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
			// WHERE tbl_krs.mahasiswa_id = '".$mhsid."'
				// AND tbl_mkditawarkan.tahun_akademik = (SELECT tahun_akademik FROM `db_ptiik_apps`.`tbl_tahunakademik` WHERE tbl_tahunakademik.is_aktif = '1')";
		// return $this->db->query($sql);
	// }
	
	// function get_mk_by_dosen($dosen_id=NULL){
		// $sql = "SELECT DISTINCT mid(md5(vw_mk_by_dosen.mkditawarkan_id),9,7) mkditawarkan_id, vw_mk_by_dosen.namamk keterangan, vw_mk_by_dosen.sks
			// FROM `db_ptiik_apps`.`vw_mk_by_dosen`
			// WHERE vw_mk_by_dosen.karyawan_id = '".$dosen_id."'
				// AND vw_mk_by_dosen.tahun_akademik = (SELECT tahun_akademik FROM `db_ptiik_apps`.`tbl_tahunakademik` WHERE tbl_tahunakademik.is_aktif = '1')";
		// return $this->db->query($sql);
	// }
	
	function get_test_by_mk($mkditawarkan_id=NULL, $mhsid=NULL,$semester=NULL){
		$sql = "SELECT DISTINCT 
				mid(md5(tbl_test.test_id),9,7) AS test_id,
				db_ptiik_apps.tbl_test.test_id AS hidId,
				db_ptiik_apps.tbl_materimk.judul,
				db_ptiik_apps.tbl_test.tgl_mulai,
				db_ptiik_apps.tbl_test.tgl_selesai,
				db_ptiik_apps.tbl_test.instruksi,
				db_ptiik_apps.tbl_test.is_publish,
				db_ptiik_apps.tbl_test.is_random,
				db_ptiik_apps.tbl_test.keterangan,
				db_ptiik_apps.tbl_test.judul AS judul_test,
				db_ptiik_apps.vw_mk_by_dosen.namamk as `nama_mk`,
				db_ptiik_apps.vw_mk_by_dosen.kode_mk
				FROM
				db_ptiik_apps.tbl_test
				LEFT JOIN db_ptiik_apps.tbl_materimk ON db_ptiik_apps.tbl_test.materi_id = db_ptiik_apps.tbl_materimk.materi_id
				INNER JOIN db_ptiik_apps.vw_mk_by_dosen ON db_ptiik_apps.tbl_test.mkditawarkan_id = db_ptiik_apps.vw_mk_by_dosen.mkditawarkan_id WHERE 1 = 1
			";
			
			if($mkditawarkan_id){
				$sql = $sql . " AND mid(md5(tbl_test.mkditawarkan_id),9,7) = '".$mkditawarkan_id."'";
			}
			
			if($mhsid){
			$sql = $sql . " AND tbl_test.mkditawarkan_id IN (SELECT tbl_krs.mkditawarkan_id FROM `db_ptiik_apps`.`tbl_krs` WHERE tbl_krs.mahasiswa_id = '".$mhsid."')
							AND tbl_test.is_publish = '1'";
			}
			
			if($semester){
			$sql = $sql. " AND vw_mk_by_dosen.tahun_akademik = '".$semester."' ";
			}
			
			// echo $sql;
		return $this->db->query($sql);
	}
	
	function get_test_by_dosen($mkditawarkan_id=NULL, $dosenid=NULL,$semester=NULL){
		$sql = "SELECT DISTINCT 
				mid(md5(tbl_test.test_id),9,7) AS test_id,
				db_ptiik_apps.tbl_test.test_id AS hidId,
				db_ptiik_apps.tbl_materimk.judul,
				db_ptiik_apps.tbl_test.tgl_mulai,
				db_ptiik_apps.tbl_test.tgl_selesai,
				db_ptiik_apps.tbl_test.instruksi,
				db_ptiik_apps.tbl_test.is_publish,
				db_ptiik_apps.tbl_test.is_random,
				db_ptiik_apps.tbl_test.keterangan,
				db_ptiik_apps.tbl_test.judul AS judul_test,
				db_ptiik_apps.vw_mk_by_dosen.namamk as `nama_mk`,
				db_ptiik_apps.vw_mk_by_dosen.kode_mk
				FROM
				db_ptiik_apps.tbl_test
				LEFT JOIN db_ptiik_apps.tbl_materimk ON db_ptiik_apps.tbl_test.materi_id = db_ptiik_apps.tbl_materimk.materi_id
				INNER JOIN db_ptiik_apps.vw_mk_by_dosen ON db_ptiik_apps.tbl_test.mkditawarkan_id = db_ptiik_apps.vw_mk_by_dosen.mkditawarkan_id WHERE 1 = 1
				";
			
			if($mkditawarkan_id){
				$sql = $sql . " AND mid(md5(tbl_test.mkditawarkan_id),9,7) = '".$mkditawarkan_id."'";
			}
			
			if($dosenid){
			$sql = $sql . " AND tbl_test.mkditawarkan_id = vw_mk_by_dosen.mkditawarkan_id 
							AND vw_mk_by_dosen.karyawan_id = '".$dosenid."' ";
			}
			
			if($semester){
			$sql = $sql. " AND vw_mk_by_dosen.tahun_akademik = '".$semester."' ";
			}
		
		// echo $sql;
		return $this->db->query($sql);
	}
	
	//------------------------------------------------------- mahasiswa function -------------------------------------------------------------//
	
	function get_test_mhs($id=NULL){
		$sql = "SELECT 
					mid(md5(tbl_test.test_id),9,7) test_id,
					tbl_test.test_id hidId,
					tbl_materimk.judul,
					tbl_test.tgl_mulai,
					tbl_test.tgl_selesai,
					tbl_test.instruksi,
					tbl_test.keterangan,
					tbl_test.judul judul_test,
					(	
						SELECT tbl_namamk.`keterangan`
						FROM `db_ptiik_apps`.`tbl_mkditawarkan`  
						LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` ON tbl_mkditawarkan.`matakuliah_id` = tbl_matakuliah.`matakuliah_id`
						LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON tbl_namamk.`namamk_id` = tbl_matakuliah.`namamk_id`
						WHERE tbl_mkditawarkan.`mkditawarkan_id` = tbl_test.`mkditawarkan_id`
					) as nama_mk
				FROM `db_ptiik_apps`.`tbl_test`
				LEFT JOIN `db_ptiik_apps`.`tbl_materimk` ON tbl_test.materi_id = tbl_materimk.materi_id WHERE  1 = 1 
			";
		if($id){
			$sql .= " AND  mid(md5(tbl_test.test_id),9,7) = '".$id."'";
		}
		return $this->db->getRow($sql);
	}	
	
	function get_test_detail($mhsid=NULL){
		$sql = "SELECT 
					tbl_test.test_id hidId,
					tbl_test.mkditawarkan_id,
					tbl_test.materi_id,
					tbl_test.judul
				FROM `db_ptiik_apps`.`tbl_test`
			";
		if($mhsid){
			$sql .= " WHERE mid(md5(tbl_test.test_id),9,7) = '".$mhsid."'";
		}
		return $this->db->query($sql);
	}
	
	function get_test_aktif($mhsid=NULL){
		$sql = "SELECT 
					mid(md5(tbl_test.test_id),9,7) test_id,
					tbl_test.test_id hidId,
					tbl_materimk.judul,
					tbl_test.tgl_mulai,
					tbl_test.tgl_selesai,
					tbl_test.instruksi,
					tbl_test.keterangan,
					tbl_test.judul judul_test,
					(	
						SELECT tbl_namamk.`keterangan`
						FROM `db_ptiik_apps`.`tbl_mkditawarkan`  
						LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` ON tbl_mkditawarkan.`matakuliah_id` = tbl_matakuliah.`matakuliah_id`
						LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON tbl_namamk.`namamk_id` = tbl_matakuliah.`namamk_id`
						WHERE tbl_mkditawarkan.`mkditawarkan_id` = tbl_test.`mkditawarkan_id`
					) as nama_mk
				FROM `db_ptiik_apps`.`tbl_test`
				LEFT JOIN `db_ptiik_apps`.`tbl_materimk` ON tbl_test.materi_id = tbl_materimk.materi_id
				WHERE tbl_test.mkditawarkan_id IN (SELECT tbl_krs.mkditawarkan_id FROM `db_ptiik_apps`.`tbl_krs` WHERE tbl_krs.mahasiswa_id = '".$mhsid."')
			";
		return $this->db->query($sql);
	}
	
	function cek_hasil($id, $mhsid){
		$sql = "SELECT 
					tbl_test_hasil.max_post, 
					mid(md5(tbl_test_hasil.hasil_id),9,7) hasil_id, 
					tbl_test_hasil.jam_selesai
				FROM `db_ptiik_apps`.`tbl_test_hasil`
				WHERE tbl_test_hasil.mahasiswa_id = '".$mhsid."'
					AND mid(md5(tbl_test_hasil.test_id),9,7) = '".$id."'";
		if($data = $this->db->getRow( $sql )) return $data;
	}	
	
	function get_hasilid(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(hasil_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM `db_ptiik_apps`.`tbl_test_hasil`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_hasil($hasilid){
		$sql = "SELECT 
					tbl_test_hasil.jam_mulai, 
					tbl_test_hasil.jam_selesai, 
					tbl_test_hasil.tgl_test
				FROM `db_ptiik_apps`.`tbl_test_hasil`
				WHERE mid(md5(tbl_test_hasil.hasil_id),9,7) = '".$hasilid."'";
		return $this->db->getRow($sql);
	}
	
	function save_hasil($data){
		$this->db->replace('db_ptiik_apps`.`tbl_test_hasil',$data);
	}
	
	function cek_soal($hasilid){
		$sql = "SELECT tbl_test_hasil.jam_selesai as data 
				FROM `db_ptiik_apps`.`tbl_test_hasil` 
				WHERE mid(md5(tbl_test_hasil.hasil_id),9,7) = '".$hasilid."'";
				
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_soal_mhs($id){
		$sql = "SELECT 
					mid(md5(tbl_test_soal.test_id),9,7) test_id,
					tbl_test_soal.soal_id,
					tbl_test_soal.pertanyaan,
					tbl_test_kategori.parameter_input kategori
				FROM `db_ptiik_apps`.`tbl_test_soal`
				LEFT JOIN `db_ptiik_apps`.`tbl_test_kategori` ON tbl_test_soal.kategori_id = tbl_test_kategori.kategori_id
				LEFT JOIN `db_ptiik_apps`.`tbl_test` ON tbl_test.test_id = tbl_test_soal.test_id
				WHERE mid(md5(tbl_test_soal.test_id),9,7) = '".$id."'
				ORDER BY case when tbl_test.is_random = '1' then rand()
				else 0	end";
		return $this->db->query($sql);
	}
	
	function get_jawaban_mhs($id){
		$sql = "SELECT 
			        tbl_test_soal.soal_id,
			        tbl_test_jawab.keterangan,
			        tbl_test_jawab.is_benar,
			        tbl_test_jawab.jawaban_id
				FROM `db_ptiik_apps`.`tbl_test_soal` , `db_ptiik_apps`.`tbl_test_jawab` 
				WHERE mid(md5(tbl_test_soal.test_id),9,7) = '".$id."'
					AND tbl_test_soal.soal_id = tbl_test_jawab.soal_id
				ORDER BY case when tbl_test_jawab.is_random = '1' then rand()
				else 0 end";
		return $this->db->query($sql);
	}
	
	function decrypt_hasilid($hasilid){
		$sql = "SELECT tbl_test_hasil.hasil_id FROM `db_ptiik_apps`.`tbl_test_hasil` WHERE mid(md5(tbl_test_hasil.hasil_id),9,7) = '".$hasilid."'";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->hasil_id;
		return $strresult;
	}
	
	function get_detail_jawaban($jawabid){
		$sql = "SELECT 
			        tbl_test_jawab.keterangan,
			        tbl_test_jawab.skor
				FROM `db_ptiik_apps`.`tbl_test_jawab`
				WHERE jawaban_id = '".$jawabid."'";
		$dt = $this->db->getRow( $sql );
		return $dt;
	}
	
	function get_detailid(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(detail_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM `db_ptiik_apps`.`tbl_test_hasil_detail`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_jawab($hasil_id){
		$sql = "SELECT 
					mid(md5(tbl_test_hasil_detail.soal_id),9,7) soal_id,
					tbl_test_hasil_detail.inf_jawab,
					tbl_test_hasil_detail.inf_skor
				FROM `db_ptiik_apps`.`tbl_test_hasil_detail`
				WHERE mid(md5(tbl_test_hasil_detail.hasil_id),9,7) = '".$hasil_id."'";
				
		return $this->db->query($sql);
	}
	
	function save_detail($data){
		$this->db->replace('db_ptiik_apps`.`tbl_test_hasil_detail',$data);
	}
	
	function update_skor($hasil_id, $skor, $count_jawab){
		$jam = date("H:i:s");
		$sql = "UPDATE `db_ptiik_apps`.`tbl_test_hasil` 
				SET `total_skor` = '" . $skor . "', `max_post` = '".$count_jawab."', `jam_selesai` ='".$jam."'
				WHERE `hasil_id` = '".$hasil_id."'";
		$this->db->query($sql);
	}
	
	function udpate_hasil_detail($hasilid){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_test_hasil_detail` WHERE `tbl_test_hasil_detail`.`hasil_id` = '".$hasilid."'";
		$this->db->query($sql);
	}
	
	function save_log_hasil($datanya){
		$this->db->replace('db_ptiik_apps`.`tbl_log_test',$datanya);
	}
	
	//------------------------------------------------------- mahasiswa end of function -------------------------------------------------------------//
	
	function get_soal_hasil($id){
		$sql = "SELECT 
					mid(md5(s.test_id),9,7) test_id,
					mid(md5(s.soal_id),9,7) soalid,
					s.soal_id,
					s.pertanyaan,
					k.parameter_input kategori
				FROM `db_ptiik_apps`.`tbl_test_soal` `s`
				LEFT JOIN `db_ptiik_apps`.`tbl_test` `t` ON s.test_id = t.test_id
				LEFT JOIN `db_ptiik_apps`.`tbl_test_kategori` `k` ON s.kategori_id = k.kategori_id
				WHERE mid(md5(s.test_id),9,7) = '".$id."'";
			
		return $this->db->query($sql);
	}
	
	function get_test_id_by_hasil_id($hasil_id){
		$sql = "SELECT mid(md5(tbl_test_hasil.test_id),9,7) data 
				FROM `db_ptiik_apps`.`tbl_test_hasil` WHERE mid(md5(tbl_test_hasil.hasil_id),9,7) = '".$hasil_id."'";
		$dt = $this->db->getRow( $sql );
		return $dt->data;
	}
	
	function get_jawaban_hasil($id){
		$sql = "SELECT 
			        s.soal_id,
			        j.keterangan,
			        j.is_benar,
			        j.jawaban_id
				FROM `db_ptiik_apps`.`tbl_test_soal` `s`, `db_ptiik_apps`.`tbl_test_jawab` `j`
				WHERE mid(md5(s.test_id),9,7) = '".$id."'
					AND s.soal_id = j.soal_id";
				
		return $this->db->query($sql);
	}
	
	
	function get_peserta_hasil($id){
		$sql = "SELECT 
					mid(md5(tbl_test_hasil.test_id),9,7) test_id,
					mid(md5(tbl_test_hasil.hasil_id),9,7) hasil_id,
					tbl_test_hasil.hasil_id hid_hasilid,
					tbl_test_hasil.mahasiswa_id,
					tbl_test_hasil.tgl_test,
					tbl_test_hasil.jam_mulai,
					tbl_test_hasil.jam_selesai,
					tbl_test_hasil.max_post,
					tbl_test_hasil.total_skor,
					tbl_mahasiswa.nama
				FROM `db_ptiik_apps`.`tbl_test_hasil`
				LEFT JOIN `db_ptiik_apps`.`tbl_test` ON tbl_test_hasil.test_id = tbl_test.test_id
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa` ON tbl_test_hasil.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
				WHERE mid(md5(tbl_test_hasil.test_id),9,7) = '".$id."'";
			
		return $this->db->query($sql);
	}
	
	function get_peserta_detail_hasil($id){
		$sql = "SELECT 
					mid(md5(tbl_test_hasil.test_id),9,7) test_id,
					mid(md5(tbl_test_hasil.hasil_id),9,7) hasil_id,
					tbl_test_hasil.hasil_id hid_hasilid,
					tbl_test_hasil.mahasiswa_id,
					tbl_test_hasil.tgl_test,
					tbl_test_hasil.jam_mulai,
					tbl_test_hasil.jam_selesai,
					tbl_test_hasil.max_post,
					tbl_test_hasil.total_skor,
					tbl_mahasiswa.nim,
					tbl_mahasiswa.nama,
					tbl_mahasiswa.jenis_kelamin,
					tbl_test_hasil_detail.detail_id,
					sum(tbl_test_hasil_detail.inf_skor) as nilai
				FROM `db_ptiik_apps`.`tbl_test_hasil`
				LEFT JOIN `db_ptiik_apps`.`tbl_test` ON tbl_test_hasil.test_id = tbl_test.test_id
				LEFT JOIN `db_ptiik_apps`.`tbl_test_hasil_detail` ON tbl_test_hasil.hasil_id = tbl_test_hasil_detail.hasil_id
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa` ON tbl_test_hasil.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
				WHERE mid(md5(tbl_test_hasil.hasil_id),9,7) = '".$id."'";
			
		return $this->db->query($sql);
	}
	
	function get_detail_hasil($id){
		$sql = "SELECT 
				tbl_test_hasil_detail.soal_id,
				tbl_test_hasil_detail.detail_id,
				tbl_test_soal.pertanyaan,
				tbl_test_hasil_detail.jawaban_id,
				tbl_test_hasil_detail.hasil_id,
				tbl_test_hasil_detail.inf_jawab,
				tbl_test_hasil_detail.inf_skor,
				tbl_test_hasil_detail.catatan
				FROM `db_ptiik_apps`.`tbl_test_hasil_detail`
				LEFT JOIN `db_ptiik_apps`.`tbl_test_hasil` ON tbl_test_hasil_detail.hasil_id = tbl_test_hasil.hasil_id
				LEFT JOIN `db_ptiik_apps`.`tbl_test_soal` ON tbl_test_hasil_detail.soal_id = tbl_test_soal.soal_id
				WHERE mid(md5(tbl_test_hasil.hasil_id),9,7) = '".$id."'";
			
		return $this->db->query($sql);
	}
	
	
	function get_total_peserta_hasil($id){
		$sql = "SELECT count(*) as total_peserta FROM `db_ptiik_apps`.`tbl_test_hasil` `s`
				LEFT JOIN `db_ptiik_apps`.`tbl_test` `t` ON s.test_id = t.test_id
				WHERE mid(md5(s.test_id),9,7) = '".$id."'";
		return $this->db->query($sql);
	}
	/*------------------------------------------*/
	
	function get_mdtestid($id){
		$sql= "SELECT  mid(md5(`test_id`),9,7) as test_id
		       FROM db_ptiik_apps.`tbl_test` 
		       WHERE test_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->test_id;
	
		return $strresult;}
	}
	
	function read($id=NULL, $mkid=NULL, $jadwalid=NULL){
		$sql = "SELECT  mid(md5(`test_id`),9,7) as test_id,
				test_id as hid_id,
				mid(md5(`mkditawarkan_id`),9,7) as mkditawarkan_id,
				mid(md5(`materi_id`),9,7) as materi_id,
				mid(md5(`jadwal_id`),9,7) as jadwal_id,
				`judul`,
				`tgl_mulai`,
				`tgl_selesai`,
				`instruksi`,
				`keterangan`,
				`is_random`,
				`is_publish`,
				
				(SELECT 
				`db_ptiik_apps`.`tbl_materimk`.judul 
				FROM `db_ptiik_apps`.`tbl_materimk` 
				WHERE `db_ptiik_apps`.`tbl_materimk`.materi_id = `db_ptiik_apps`.`tbl_test`.materi_id ) as materi,
				
				(SELECT 
				`db_ptiik_apps`.`tbl_namamk`.keterangan 
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah`, 
				`db_ptiik_apps`.`tbl_mkditawarkan` 
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id 
				AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_test`.mkditawarkan_id ) as namamk,
				
				last_update,
              	substring(`tbl_test`.last_update, 1,10) as YMD,
        	  	substring(`tbl_test`.last_update, 12,8) as waktu,
        	  	
        	  	(SELECT a.username FROM `coms`.`coms_user` as a WHERE a.id =  `tbl_test`.user_id) as user
				
				FROM `db_ptiik_apps`.`tbl_test` 
				WHERE 1
				";

		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_test`.`test_id`),9,7)='".$id."' ";
		}
		
		if($mkid){
				$sql = $sql . " AND mid(md5(tbl_test.mkditawarkan_id),9,7) = '".$mkid."'";
		}
		
		if($jadwalid){
				$sql = $sql . " AND mid(md5(tbl_test.jadwal_id),9,7) = '".$jadwalid."'";
		}
		
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	
	function get_namamk($id, $cek){
		$sql = "SELECT 
				(SELECT 
				`db_ptiik_apps`.`tbl_namamk`.keterangan 
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah`
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk
				FROM `db_ptiik_apps`.`tbl_mkditawarkan`
				WHERE 1
				";
				
		if($cek==""){
			$sql=$sql . " AND `mkditawarkan_id` = '".$id."' ";
		}
		else {
			$sql=$sql . " AND MID( MD5( `mkditawarkan_id` ) , 9, 7 ) = '".$id."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		
		$strresult = $dt->namamk;
	
		return $strresult;}
	}
	
	function get_jadwal_data($id){
		$sql = "SELECT MID( MD5( jadwal_id ) , 9, 7 ) as hid_jadwal,
					   MID( MD5( mkditawarkan_id ) , 9, 7 ) as hid_mkid,
					   mkditawarkan_id, 
					   kelas, 
					   namamk,
					   nama
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				WHERE MID( MD5( `vw_mk_by_dosen`.`jadwal_id` ) , 9, 7 ) = '".$id."'
				";
		$result = $this->db->query( $sql );
		
		//echo $sql;
		if(isset($result)){
		return $result;
		}
	}
	
	function get_materi($id, $cek){
		$sql = "SELECT MID( MD5( `materi_id` ) , 9, 7 ) as `materi_id`, `judul` 
			    FROM db_ptiik_apps.`tbl_materimk` 
			    WHERE `parent_id` =  '0' AND db_ptiik_apps.tbl_materimk.is_valid = 1
				";
		
		if($cek==""){
			$sql=$sql . " AND `mkditawarkan_id` = '".$id."' ";
		}
		else {
			$sql=$sql . " AND MID( MD5( `mkditawarkan_id` ) , 9, 7 ) = '".$id."' ";
		}
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		return $result;
		}
	}
	
	function get_mkid_by_materi($id){
		$sql= "SELECT MID( MD5(mkditawarkan_id), 9, 7) as mkditawarkan_id
		       FROM db_ptiik_apps.`tbl_materimk` 
		       WHERE MID( MD5(materi_id), 9, 7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
		return $strresult;}
	}
	
	function get_reg_number(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(test_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_test"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_test($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_test',$datanya);
	}
	
	function get_jadwal_id($id){
		$sql= "SELECT jadwal_id 
			   FROM db_ptiik_apps.`tbl_jadwalmk` 
			   WHERE MID( MD5(jadwal_id), 9, 7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		
		$strresult = $dt->jadwal_id;
	
		return $strresult;}
	}
	
	function get_mkid($id){
		$sql= "SELECT mkditawarkan_id 
		       FROM db_ptiik_apps.`tbl_mkditawarkan` 
		       WHERE MID( MD5(mkditawarkan_id), 9, 7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
	
		return $strresult;}
	}
	
	function get_materi_id($id){
		$sql= "SELECT materi_id 
		       FROM db_ptiik_apps.`tbl_materimk` 
		       WHERE MID( MD5(materi_id), 9, 7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->materi_id;
	
		return $strresult;}
	}
	//=========KATEGORI=========//
	
	function readkategori($id=NULL){
		$sql = "SELECT MID( MD5(kategori_id), 6, 6) as kat_id,
					   kategori_id as hid_id,
					   keterangan as kategori,
					   parameter_input as jenis_input
				FROM `db_ptiik_apps`.`tbl_test_kategori`";
		if($id!=""){
			$sql = $sql . " WHERE  MID( MD5(kategori_id), 6, 6) = '".$id."' ";
		}

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function getparam_by_soal($id=NULL){
		$sql="SELECT (SELECT a.parameter_input FROM `db_ptiik_apps`.`tbl_test_kategori` as a WHERE a.`kategori_id` = `db_ptiik_apps`.`tbl_test_soal`.`kategori_id`) as param
			  FROM `db_ptiik_apps`.`tbl_test_soal` 
			  WHERE MID( MD5(soal_id), 9, 7) = '".$id."'"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->param;
		
		return $strresult;
	}
	
	function id_testkategori($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kategori_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_test_kategori`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function ceknewkategori($ket){
		$sql = "SELECT keterangan from `db_ptiik_apps`.`tbl_test_kategori` where keterangan = '".$ket."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cekbyidkategori($ket){
		$sql = "SELECT kategori_id from `db_ptiik_apps`.`tbl_test_kategori` where keterangan = '".$ket."'";
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		foreach($result as $dt){
			$id=$dt->kategori_id;
		}
		return $id;
		}
	}
	
	function replace_test_kategori($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_kategori',$datanya);
	}
	
	//=========END-KATEGORI========//
	
	//------SOAL-----------------------------------------------------//
	function readsoal($id){
		$sql = "SELECT MID( MD5(tbl_test_soal.soal_id), 9, 7) as soal_id,
					tbl_test_soal.soal_id as hid_id,
					tbl_test_soal.jadwal_id,
					tbl_test_soal.materi_id,
					tbl_test_soal.mkditawarkan_id,
					tbl_test_soal.test_id,
					MID( MD5(tbl_test_soal.test_id), 9, 7) as testid,
					tbl_test_soal.pertanyaan,
					tbl_test_soal.kategori_id,
					tbl_test.judul as judul_tes,
				    tbl_namamk.keterangan as mata_kuliah,
				    (select judul from `db_ptiik_apps`.`tbl_materimk` where materi_id = tbl_test_soal.materi_id) as materi,
				    (select concat(mata_kuliah, ' ', kelas) from `db_ptiik_apps`.`tbl_jadwalmk` where jadwal_id = tbl_test_soal.jadwal_id) as jadwal,
				    tbl_test_kategori.keterangan as kategori,
				    (select username from coms.coms_user where id = tbl_test_soal.user_id) as user
				FROM `db_ptiik_apps`.`tbl_test_soal`,
				     `db_ptiik_apps`.`tbl_mkditawarkan`,
				     `db_ptiik_apps`.`tbl_matakuliah`,
				     `db_ptiik_apps`.`tbl_namamk`,
				     `db_ptiik_apps`.`tbl_test_kategori`,
				     `db_ptiik_apps`.`tbl_test`
				WHERE tbl_test_soal.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
				      AND tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				      AND tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				      AND tbl_test_soal.kategori_id = tbl_test_kategori.kategori_id
				      AND tbl_test_soal.test_id = tbl_test.test_id";
		if($id!=""){
			$sql = $sql . " AND  MID( MD5(soal_id), 9, 7) = '".$id."' ";
		}
			$sql = $sql . " ORDER BY hid_id ASC ";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_test(){
		$sql = "SELECT MID( MD5(test_id), 9, 7) as test_id,
				       test_id as hid_id,
				       jadwal_id,
				       judul
				FROM `db_ptiik_apps`.`tbl_test`
			";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function id_soal($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(soal_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_test_soal`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_soal($id){
		$sql =	"SELECT	MID( MD5(soal_id), 9, 7) as soal_id,
						`pertanyaan`,
						(select a.is_random from `db_ptiik_apps`.`tbl_test` as a where a.test_id = `db_ptiik_apps`.`tbl_test_soal`.`test_id` ) as is_random,
                        (SELECT `parameter_input` FROM `db_ptiik_apps`.`tbl_test_kategori` as a WHERE a.`kategori_id` = `db_ptiik_apps`.`tbl_test_soal`.`kategori_id` ) as kategori
				FROM `db_ptiik_apps`.`tbl_test_soal`
				WHERE MID( MD5(`test_id`), 9, 7) = '".$id."'
				order by case when `is_random` = '1' then rand()
                        		ELSE 0
						  END
				";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function delete_soal($id){
		$sql =	"DELETE FROM `db_ptiik_apps`.`tbl_test_soal` WHERE MID( MD5(soal_id), 9, 7) = '".$id."'
				";
		$result = $this->db->query( $sql );
		if($result){
			$this->deleteBySoalId($id);
			return TRUE;
		}
	}
	
	function ceknewsoal($ket, $testid){
		$sql = "SELECT pertanyaan from `db_ptiik_apps`.`tbl_test_soal` where pertanyaan = '".$ket."' AND test_id='".$testid."' ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cekbyidsoal($ket, $testid){
		$sql = "SELECT soal_id from `db_ptiik_apps`.`tbl_test_soal` where pertanyaan = '".$ket."' AND test_id='".$testid."' ";
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		foreach($result as $dt){
			$id=$dt->soal_id;
		}
		return $id;
		}
	}
	
	function replace_test_soal($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_soal',$datanya);
	}
	
	function get_detail_test($id=NULL){
		$sql = "SELECT MID( MD5(t.test_id), 6, 6) as test_id,
					t.test_id as hid_id,
					t.jadwal_id,
					t.materi_id,
					t.mkditawarkan_id,
					t.judul,
				    nmk.keterangan as mata_kuliah,
				    (select judul from `db_ptiik_apps`.`tbl_materimk` where materi_id = t.materi_id) as materi,
				    (select concat(mata_kuliah, ' ', kelas) from `db_ptiik_apps`.`tbl_jadwalmk` where jadwal_id = t.jadwal_id) as jadwal
				FROM `db_ptiik_apps`.`tbl_test` t,
				     `db_ptiik_apps`.`tbl_mkditawarkan` mkd,
				     `db_ptiik_apps`.`tbl_matakuliah` mk,
				     `db_ptiik_apps`.`tbl_namamk` nmk
				WHERE t.mkditawarkan_id = mkd.mkditawarkan_id 
				      AND mkd.matakuliah_id = mk.matakuliah_id
				      AND mk.namamk_id = nmk.namamk_id
				      AND t.test_id = '".$id."'
			";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_namamkid($id=NULL){
		$sql = "SELECT 
				(SELECT `db_ptiik_apps`.`tbl_namamk`.namamk_id
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah` 
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk_id
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
				WHERE mkditawarkan_id = '".$id."'
			   ";
			   
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->namamk_id;
	
		return $strresult;}
	}
	//------SOAL-----------------------------------------------------//
	
	//------JAWAB-----------------------------------------------------//	
	function get_jawaban(){
		$sql =	"SELECT MID( MD5(soal_id), 9, 7) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_test_jawab` 
			     WHERE 1
			     order by case when `is_random` = '1' then rand()
                        		ELSE 0
						  END
				";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_jawaban_edit($id){
		$sql =	"SELECT MID( MD5(jawaban_id), 9, 7) as jawab_id, MID( MD5(soal_id), 9, 7) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_test_jawab` 
			     WHERE MID( MD5(soal_id), 9, 7) = '".$id."'
			     OR soal_id = '".$id."';
				";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_kategori_jawab(){
		$sql = "SELECT 
					MID( MD5(`kategori_id`), 6, 6) as kategoriid,`keterangan` 
					FROM `db_ptiik_apps`.`tbl_test_kategori` 
				WHERE 1
				";
		//echo $sql;
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		return $result;
		}
	}
	
	function get_is_random($id){
		$sql= "SELECT DISTINCT `is_random` FROM `db_ptiik_apps`.`tbl_test_jawab` WHERE MID( MD5(`soal_id`), 9, 7) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->is_random;
	
		return $strresult;}
	}
	
	function getparam_input($id){
		$sql= "SELECT `parameter_input`
		       FROM db_ptiik_apps.`tbl_test_kategori` 
		       WHERE MID( MD5(`kategori_id`), 6, 6) = '".$id."'
		       OR kategori_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->parameter_input;
	
		return $strresult;}
	}
	
	function deleteById($id){
		$sql= "DELETE FROM `db_ptiik_apps`.`tbl_test_jawab` WHERE MID( MD5(`jawaban_id`), 6, 6) = '".$id."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;}
		else {
			return FALSE;
		}
	}
	
	function deleteBySoalId($id){
		$sql= "DELETE FROM `db_ptiik_apps`.`tbl_test_jawab` WHERE `soal_id` = '".$id."' OR MID( MD5(`soal_id`), 9, 7) = '".$id."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;}
		else {
			return FALSE;
		}
	}
	
	function get_jawabid($id=NULL, $soalid=NULL){
		$sql= "SELECT jawaban_id 
		       FROM `db_ptiik_apps`.`tbl_test_jawab`
		       WHERE MID( MD5(jawaban_id), 9, 7) = '".$id."' AND soal_id = '".$soalid."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if($dt){
			$strresult = $dt->jawaban_id;
		}
		else{
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jawaban_id,4) AS 
				  unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				  FROM db_ptiik_apps.tbl_test_jawab";
			
			$dt = $this->db->getRow( $sql );
		
			$strresult = $dt->data;
		}
		return $strresult;
	}
	
	function get_id_jawaban(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jawaban_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_test_jawab"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function replace_test_jawab($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_jawab',$datanya);
	}
	//------JAWAB-----------------------------------------------------//
	
	//------TEST BANK-----------------------------------------------------//
	function read_bank_soal($id){
		$sql = "SELECT MID( MD5(tbl_bank_soal.soal_id), 9, 7) as soal_id,
					tbl_bank_soal.soal_id as hid_id,
					tbl_bank_soal.jadwal_id,
					tbl_bank_soal.materi_id,
					tbl_bank_soal.mkditawarkan_id,
					tbl_bank_soal.pertanyaan,
					tbl_bank_soal.kategori_id,
				    tbl_namamk.keterangan as mata_kuliah,
				    (select judul from `db_ptiik_apps`.`tbl_materimk` where materi_id = tbl_bank_soal.materi_id) as materi,
				    (select concat(mata_kuliah, ' ', kelas) from `db_ptiik_apps`.`tbl_jadwalmk` where jadwal_id = tbl_bank_soal.jadwal_id) as jadwal,
				    tbl_test_kategori.keterangan as kategori
				FROM `db_ptiik_apps`.`tbl_bank_soal`,
				     `db_ptiik_apps`.`tbl_mkditawarkan`,
				     `db_ptiik_apps`.`tbl_matakuliah`,
				     `db_ptiik_apps`.`tbl_namamk`,
				     `db_ptiik_apps`.`tbl_test_kategori`
				WHERE tbl_bank_soal.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
				      AND tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				      AND tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				      AND tbl_bank_soal.kategori_id = tbl_test_kategori.kategori_id
                      ";
		if($id!=""){
			$sql = $sql . " AND  MID( MD5(tbl_bank_soal.soal_id), 9, 7) = '".$id."' ";
		}
			$sql = $sql . " ORDER BY hid_id ASC ";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_bank_jawaban_by_soalid($id){
		$sql =	"SELECT MID( MD5(jawaban_id), 9, 7) as jawab_id, MID( MD5(soal_id), 9, 7) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_bank_jawab` 
			     WHERE MID( MD5(soal_id), 9, 7) = '".$id."'
			     OR soal_id = '".$id."';
				";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_bank($id=NULL){
		$sql = "SELECT nmk.keterangan as namamk, nmk.namamk_id
				FROM `db_ptiik_apps`.`tbl_test` t,
				     `db_ptiik_apps`.`tbl_mkditawarkan` mkd,
				     `db_ptiik_apps`.`tbl_matakuliah` mk,
				     `db_ptiik_apps`.`tbl_namamk` nmk
				WHERE t.mkditawarkan_id = mkd.mkditawarkan_id 
				      AND mkd.matakuliah_id = mk.matakuliah_id
				      AND mk.namamk_id = nmk.namamk_id
				      AND MID( MD5(t.test_id), 9, 7) = '".$id."'
				";
				
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_bank_soal($id=NULL){
		$sql = "SELECT  
				MID( MD5(`db_ptiik_apps`.`tbl_bank_soal`.`soal_id`), 9, 7) as soalid,
				`db_ptiik_apps`.`tbl_bank_soal`.`kategori_id`,
				`db_ptiik_apps`.`tbl_bank_soal`.`pertanyaan`,
				
				(SELECT `db_ptiik_apps`.`tbl_test_kategori`.`parameter_input`
				FROM `db_ptiik_apps`.`tbl_test_kategori`
				WHERE `db_ptiik_apps`.`tbl_test_kategori`.`kategori_id` = `db_ptiik_apps`.`tbl_bank_soal`.`kategori_id`) as kategori
				
				FROM `db_ptiik_apps`.`tbl_bank_soal` 
				WHERE `db_ptiik_apps`.`tbl_bank_soal`.`namamk_id` = '".$id."'
			   ";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_bank_jawaban(){
		$sql = "SELECT 
				MID( MD5(`db_ptiik_apps`.`tbl_bank_jawab`.`soal_id`), 9, 7) as soalid,
				MID( MD5(`db_ptiik_apps`.`tbl_bank_jawab`.`jawaban_id`), 9, 7) as jawabanid,
				`db_ptiik_apps`.`tbl_bank_jawab`.`keterangan`,
				`db_ptiik_apps`.`tbl_bank_jawab`.`is_benar`
				FROM `db_ptiik_apps`.`tbl_bank_jawab` 
				WHERE 1
			   ";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function getparam_by_bank_soal($id=NULL){
		$sql="SELECT (SELECT a.parameter_input FROM `db_ptiik_apps`.`tbl_test_kategori` as a WHERE a.`kategori_id` = `db_ptiik_apps`.`tbl_bank_soal`.`kategori_id`) as param
			  FROM `db_ptiik_apps`.`tbl_bank_soal` 
			  WHERE MID( MD5(soal_id), 9, 7) = '".$id."'"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->param;
		
		return $strresult;
	}
	
	function get_jawaban_edit_bank($id){
		$sql =	"SELECT MID( MD5(jawaban_id), 9, 7) as jawab_id, MID( MD5(soal_id), 9, 7) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_bank_jawab` 
			     WHERE MID( MD5(soal_id), 9, 7) = '".$id."'
			     OR soal_id = '".$id."';
				";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function replace_bank_soal($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_bank_soal',$datanya);
	}
	
	function replace_bank_jawab($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_bank_jawab',$datanya);
	}
	//------TEST BANK-----------------------------------------------------//
	
}
?>