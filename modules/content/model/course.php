<?php
/*
<!---//---------------------------------------------------------------------
		Author			: Thusti Dwi  P. (TDP)
		Creation Date	: 10/12/13 (initial version)
		Purpose			: model for course
		Revision Log:
		TDP 10/02/14 add method for silabus (get_silabus), comment unnecessary silabus method, rename& edit id_silabus_detail, rename & edit all silabus method
	----------------------------------------------------------------------//--->
*/

class model_course extends model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public $error;
	public $id;
	public $modified;
	
	function  read_mk_by_dosen($semester=NULL, $id=NULL, $mk=NULL, $pengampu=NULL){
		
		$sql = "SELECT
					db_ptiik_apps.vw_mk_by_dosen.namamk,
					mid(md5(db_ptiik_apps.vw_mk_by_dosen.mkditawarkan_id),9,7) as  `mkid`,
					mid(md5(db_ptiik_apps.vw_mk_by_dosen.jadwal_id),9,7) as  `jadwalid`,
					db_ptiik_apps.vw_mk_by_dosen.mkditawarkan_id,
					db_ptiik_apps.vw_mk_by_dosen.karyawan_id,
					db_ptiik_apps.vw_mk_by_dosen.pengampu_id,
					db_ptiik_apps.vw_mk_by_dosen.nik,
					db_ptiik_apps.vw_mk_by_dosen.nama,
					db_ptiik_apps.vw_mk_by_dosen.gelar_awal,
					db_ptiik_apps.vw_mk_by_dosen.gelar_akhir,
					db_ptiik_apps.vw_mk_by_dosen.is_koordinator,
					db_ptiik_apps.vw_mk_by_dosen.kode_mk,
					db_ptiik_apps.vw_mk_by_dosen.sks,
					db_ptiik_apps.vw_mk_by_dosen.fakultas,
					db_ptiik_apps.vw_mk_by_dosen.prodi,
					db_ptiik_apps.vw_mk_by_dosen.is_praktikum,
					db_ptiik_apps.vw_mk_by_dosen.is_blok,
					db_ptiik_apps.vw_mk_by_dosen.kelas,
					db_ptiik_apps.vw_mk_by_dosen.jam_mulai,
					db_ptiik_apps.vw_mk_by_dosen.jam_selesai,
					db_ptiik_apps.vw_mk_by_dosen.hari,
					db_ptiik_apps.vw_mk_by_dosen.ruang_id,
					db_ptiik_apps.vw_mk_by_dosen.prodi_id,
					db_ptiik_apps.vw_mk_by_dosen.jadwal_id,
					db_ptiik_apps.vw_mk_by_dosen.namamk_id,
					db_ptiik_apps.vw_mk_by_dosen.tahun_akademik,
					db_ptiik_apps.vw_mk_by_dosen.tahun,
					db_ptiik_apps.vw_mk_by_dosen.is_ganjil,
					db_ptiik_apps.vw_mk_by_dosen.is_aktif,
					db_ptiik_apps.vw_mk_by_dosen.is_pendek
					FROM
					db_ptiik_apps.vw_mk_by_dosen WHERE 1 = 1 ";
		if($semester){
			$sql = $sql. " AND db_ptiik_apps.vw_mk_by_dosen.tahun_akademik = '".$semester."' ";
		}	
		if($id){
			$sql = $sql. " AND db_ptiik_apps.vw_mk_by_dosen.karyawan_id = '".$id."' ";
		}	
		
		if($mk){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.vw_mk_by_dosen.mkditawarkan_id),9,7)='".$mk."' OR db_ptiik_apps.vw_mk_by_dosen.mkditawarkan_id = '".$mk."' ) ";
		}
		
		if($pengampu){
			$sql = $sql. " AND (db_ptiik_apps.vw_mk_by_dosen.pengampu_id = '".$pengampu."' OR db_ptiik_apps.vw_mk_by_dosen.karyawan_id='".$pengampu."') ";
		}
		
		if($semester){
			$result = $this->db->query($sql);	
		}else{
			$result = $this->db->getRow($sql);
		}		
	
		return $result;
	}
	
	function  read_mk_by_mhs($semester=NULL, $id=NULL, $mk=NULL, $pengampu=NULL,$jadwalid=NULL){
		
			$sql = "SELECT
					db_ptiik_apps.vw_mk_by_mhs.mahasiswa_id,
					mid(md5(db_ptiik_apps.vw_mk_by_mhs.jadwal_id),9,7) as  `jadwalid`,
					db_ptiik_apps.vw_mk_by_mhs.krs_id,
					db_ptiik_apps.vw_mk_by_mhs.karyawan_id,
					db_ptiik_apps.vw_mk_by_mhs.pengampu_id,
					db_ptiik_apps.vw_mk_by_mhs.nik,
					db_ptiik_apps.vw_mk_by_mhs.nama,
					db_ptiik_apps.vw_mk_by_mhs.gelar_awal,
					db_ptiik_apps.vw_mk_by_mhs.gelar_akhir,
					db_ptiik_apps.vw_mk_by_mhs.is_koordinator,
					db_ptiik_apps.vw_mk_by_mhs.namamk,
					db_ptiik_apps.vw_mk_by_mhs.kode_mk,
					db_ptiik_apps.vw_mk_by_mhs.sks,
					db_ptiik_apps.vw_mk_by_mhs.fakultas,
					db_ptiik_apps.vw_mk_by_mhs.prodi,
					db_ptiik_apps.vw_mk_by_mhs.is_praktikum,
					db_ptiik_apps.vw_mk_by_mhs.is_blok,
					db_ptiik_apps.vw_mk_by_mhs.kelas,
					db_ptiik_apps.vw_mk_by_mhs.jam_mulai,
					db_ptiik_apps.vw_mk_by_mhs.jam_selesai,
					db_ptiik_apps.vw_mk_by_mhs.hari,
					db_ptiik_apps.vw_mk_by_mhs.ruang_id,
					mid(md5(db_ptiik_apps.vw_mk_by_mhs.mkditawarkan_id),9,7) as  `mkid`,
					db_ptiik_apps.vw_mk_by_mhs.mkditawarkan_id,
					db_ptiik_apps.vw_mk_by_mhs.prodi_id,
					db_ptiik_apps.vw_mk_by_mhs.jadwal_id,
					db_ptiik_apps.vw_mk_by_mhs.namamk_id,
					db_ptiik_apps.vw_mk_by_mhs.tahun_akademik,
					db_ptiik_apps.vw_mk_by_mhs.tahun,
					db_ptiik_apps.vw_mk_by_mhs.is_ganjil,
					db_ptiik_apps.vw_mk_by_mhs.is_aktif,
					db_ptiik_apps.vw_mk_by_mhs.is_pendek,
					db_ptiik_apps.vw_mk_by_mhs.mhs,
					db_ptiik_apps.vw_mk_by_mhs.angkatan,
					db_ptiik_apps.vw_mk_by_mhs.prodi_mhs,
					db_ptiik_apps.vw_mk_by_mhs.nim
					FROM
					db_ptiik_apps.vw_mk_by_mhs WHERE 1 = 1 
					";
		
		if($semester){
			$sql = $sql. " AND db_ptiik_apps.vw_mk_by_mhs.tahun_akademik = '".$semester."' ";
		}	
		if($id){
			$sql = $sql. " AND db_ptiik_apps.vw_mk_by_mhs.mahasiswa_id = '".$id."' ";
		}
		if($jadwalid!=""){
			$sql = $sql. " AND mid(md5(db_ptiik_apps.vw_mk_by_mhs.jadwal_id),9,7) = '".$jadwalid."' ";
		}
		if($mk){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.vw_mk_by_mhs.mkditawarkan_id),9,7)='".$mk."' OR db_ptiik_apps.vw_mk_by_mhs.mkditawarkan_id = '".$mk."' ) ";
		}
		
		if($pengampu!="-"&&$pengampu!=""){
			$sql = $sql. " AND (db_ptiik_apps.vw_mk_by_mhs.pengampu_id = '".$pengampu."' OR db_ptiik_apps.vw_mk_by_mhs.karyawan_id='".$pengampu."') ";
		}
		
		$sql = $sql. "ORDER BY db_ptiik_apps.vw_mk_by_mhs.nim ASC";

		
		
		if($mk&&!$jadwalid){
			$result = $this->db->getRow($sql);
		}
		else {
			// echo $sql;
			$result = $this->db->query($sql);	
		}
		
		return $result;
	}
	
	function get_semester_aktif(){
		$sql = "SELECT
					db_ptiik_apps.tbl_tahunakademik.tahun_akademik,
					db_ptiik_apps.tbl_tahunakademik.tahun,
					db_ptiik_apps.tbl_tahunakademik.is_ganjil,
					db_ptiik_apps.tbl_tahunakademik.is_aktif,
					db_ptiik_apps.tbl_tahunakademik.is_pendek
					FROM
					db_ptiik_apps.tbl_tahunakademik WHERE is_aktif='1'";
		$dt = $this->db->getRow($sql);		
		
		$result = $dt->tahun_akademik;

		return $result;

	}
	
	function get_statusmateri(){
		$sql = "SELECT
				db_ptiik_apps.tbl_statusmateri.`status`,
				db_ptiik_apps.tbl_statusmateri.keterangan
				FROM
				db_ptiik_apps.tbl_statusmateri
				";
		$result = $this->db->query($sql);		

		return $result;
	}
	
	function get_semester(){
		$sql = "SELECT
					db_ptiik_apps.tbl_tahunakademik.tahun_akademik,
					db_ptiik_apps.tbl_tahunakademik.tahun,
					db_ptiik_apps.tbl_tahunakademik.is_ganjil,
					db_ptiik_apps.tbl_tahunakademik.is_aktif,
					db_ptiik_apps.tbl_tahunakademik.is_pendek
					FROM
					db_ptiik_apps.tbl_tahunakademik ORDER BY tahun_akademik DESC";
		$result = $this->db->query($sql);

		return $result;
	}
	
	function get_pengampu($mk=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_pengampu.pengampu_id,
				db_ptiik_apps.tbl_pengampu.karyawan_id,
				db_ptiik_apps.tbl_pengampu.mkditawarkan_id,
				db_ptiik_apps.tbl_karyawan.nama,
				db_ptiik_apps.tbl_karyawan.gelar_awal,
				db_ptiik_apps.tbl_karyawan.gelar_akhir,
				db_ptiik_apps.tbl_pengampu.is_koordinator
				FROM
				db_ptiik_apps.tbl_pengampu
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				WHERE 1= 1 
				";
		if($mk){
			$sql = $sql . " AND (db_ptiik_apps.tbl_pengampu.mkditawarkan_id = '".$mk."' OR mid(md5(db_ptiik_apps.tbl_pengampu.mkditawarkan_id),9,7)='".$mk."') ";
		}		
		$result = $this->db->query($sql);		

		return $result;
	}
	
	function get_krs($mk=NULL, $kelas=NULL, $prodi=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_krs.krs_id,
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_mahasiswa.angkatan,
					db_ptiik_apps.tbl_mahasiswa.prodi_id,
					db_ptiik_apps.tbl_krs.mkditawarkan_id,
					db_ptiik_apps.tbl_krs.kelas
					FROM
					db_ptiik_apps.tbl_krs
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_krs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					WHERE 1 = 1 
					";
		if($mk){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_krs.mkditawarkan_id),9,7)='".$mk."' OR db_ptiik_apps.tbl_krs.mkditawarkan_id = '".$mk."' ) ";
		}
		
		if($kelas){
			$sql = $sql . " AND db_ptiik_apps.tbl_krs.kelas = '".$kelas."' ";
		}
		
		if($prodi){
			$sql = $sql . " AND db_ptiik_apps.tbl_mahasiswa.prodi_id = '".$prodi."' ";
		}
		
		$sql = $sql. "ORDER BY db_ptiik_apps.tbl_mahasiswa.nim ASC";
		$result = $this->db->query($sql);		

		return $result;
	}
	
	function get_materi($mk=NULL, $parent=NULL, $id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_statusmateri.`status` AS statusid,
					db_ptiik_apps.tbl_statusmateri.keterangan AS `status`,
					mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7) as `id`,
					mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7) as `materiid`,
					mid(md5(db_ptiik_apps.tbl_materimk.mkditawarkan_id),9,7) as `mk`,
					db_ptiik_apps.tbl_materimk.materi_id,
					db_ptiik_apps.tbl_materimk.mkditawarkan_id,
					db_ptiik_apps.tbl_materimk.judul,
					db_ptiik_apps.tbl_materimk.keterangan,
					db_ptiik_apps.tbl_materimk.is_publish,
					db_ptiik_apps.tbl_materimk.urut,
					db_ptiik_apps.tbl_materimk.parent_id,
					db_ptiik_apps.tbl_materimk.is_campus,
					db_ptiik_apps.tbl_materimk.user_id,
					db_ptiik_apps.tbl_materimk.last_update,
					db_ptiik_apps.tbl_materimk.icon
					FROM
					db_ptiik_apps.tbl_statusmateri
					INNER JOIN db_ptiik_apps.tbl_materimk ON db_ptiik_apps.tbl_materimk.`status` = db_ptiik_apps.tbl_statusmateri.`status`
					WHERE 1 = 1 AND db_ptiik_apps.tbl_materimk.is_valid = 1";
		if($mk){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_materimk.mkditawarkan_id),9,7)='".$mk."' OR db_ptiik_apps.tbl_materimk.mkditawarkan_id = '".$mk."' ) ";
		}
		
		if($parent){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_materimk.parent_id),9,7)='".$parent."' OR db_ptiik_apps.tbl_materimk.parent_id = '".$parent."' ) ";
		}
		
		if($id){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7)='".$id."' OR db_ptiik_apps.tbl_materimk.materi_id = '".$id."' ) ";
		}
		
		$sql = $sql. "ORDER BY db_ptiik_apps.tbl_materimk.urut ASC";
		
		if($id){
			$result = $this->db->getRow($sql);	
		}else{
			$result = $this->db->query($sql);
		}
		// echo $sql;
		
		return $result;
	}
	
	function get_nourut_parent($id=NULL){
		$sql = "SELECT urut
				FROM  db_ptiik_apps.`tbl_materimk` 
				WHERE mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7) =  '".$id."' 
				AND is_valid = 1
				";
		if($id){
			$result = $this->db->getRow($sql);
			if(isset($result)){
				return $tes = $result->urut;
			}	
		}
		
	}
	
	function get_submateri_count($id=NULL){
		$sql = "SELECT COUNT( * ) as data
				FROM  db_ptiik_apps.`tbl_materimk` 
				WHERE mid(md5(db_ptiik_apps.tbl_materimk.parent_id),9,7) =  '".$id."' 
				AND is_valid = 1
				";
		if($id){
			$result = $this->db->getRow($sql);
			if(isset($result)){
			return $tes = $result->data;
			}	
		}
		
	}
	
	function get_materibymk_count($id=NULL){
		$sql = "SELECT COUNT( materi_id ) as data
				FROM db_ptiik_apps.tbl_materimk
				WHERE parent_id =  '0'
				AND mid(md5(mkditawarkan_id),9,7) =  '".$id."'
				AND is_valid = 1
				";
		if($id){
			$result = $this->db->getRow($sql);
			if(isset($result)){
			return $tes = $result->data;
			}
		}
	}
	
	function get_file($mk=NULL, $materi=NULL, $id=NULL, $role=NULL){
		$sql = "SELECT DISTINCT 
					db_ptiik_apps.tbl_file.file_id,
					db_ptiik_apps.tbl_file.jenis_file_id,
					mid(md5(db_ptiik_apps.tbl_file.file_id),9,7) as `id`,
					mid(md5(db_ptiik_apps.tbl_file.materi_id),9,7) as `materiid`,
					mid(md5(db_ptiik_apps.tbl_file.mkditawarkan_id),9,7) as `mk`, 
					db_ptiik_apps.tbl_file.materi_id,
					db_ptiik_apps.tbl_file.mkditawarkan_id,
					db_ptiik_apps.tbl_file.judul,
					db_ptiik_apps.tbl_file.keterangan,
					db_ptiik_apps.tbl_file.file_name,
					db_ptiik_apps.tbl_file.file_type,
					db_ptiik_apps.tbl_file.file_loc,
					db_ptiik_apps.tbl_file.file_size,
					db_ptiik_apps.tbl_file.file_content,
					db_ptiik_apps.tbl_file.tgl_upload,
					db_ptiik_apps.tbl_file.upload_by,
					db_ptiik_apps.tbl_file.is_publish,
					db_ptiik_apps.tbl_file.user_id,
					db_ptiik_apps.tbl_file.last_update,
					db_ptiik_apps.tbl_jenisfile.keterangan AS jenis
					FROM
					db_ptiik_apps.tbl_file
					INNER JOIN db_ptiik_apps.tbl_jenisfile ON db_ptiik_apps.tbl_file.jenis_file_id = db_ptiik_apps.tbl_jenisfile.jenis_file_id
				WHERE 1 = 1 AND db_ptiik_apps.tbl_file.tugas_id IS NULL";
				
		if($role=='mahasiswa'){
			$sql = $sql . " AND db_ptiik_apps.tbl_file.is_publish='1' ";
		}
		
		if($mk){
			//$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_file.mkditawarkan_id),9,7)='".$mk."' OR db_ptiik_apps.tbl_file.mkditawarkan_id = '".$mk."' ) ";
		}		
		
		if($materi){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_file.materi_id),9,7)='".$materi."' OR db_ptiik_apps.tbl_file.materi_id = '".$materi."' ) ";
		}
		
		if($id){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_file.file_id),9,7)='".$id."' OR db_ptiik_apps.tbl_file.file_id = '".$id."' ) ";
		}
		
		$sql = $sql. "ORDER BY db_ptiik_apps.tbl_file.tgl_upload ASC";
		
		if($id){
			$result = $this->db->getRow($sql);	
		}else{
			$result = $this->db->query($sql);	
		}	
		
		// echo $sql;
		return $result;
	}

	public function save($mk, $judul, $keterangan, $ispublish, $urut, $parent,$status, $iscampus,$icon, $link,$materiid = NULL, $staffid, $lastupdate) {
		if(isset($icon) && $icon!=""){
			$data['mkditawarkan_id'] = $this->db->escape($mk);
			$data['judul'] = $this->db->escape($judul);
			$data['keterangan'] = $this->db->escape($keterangan);
			$data['is_publish'] = $this->db->escape($ispublish);
			$data['status'] = $this->db->escape($status);
			$data['is_campus'] = $this->db->escape($iscampus);
			$data['icon'] = $this->db->escape($icon);
			$data['urut'] = $this->db->escape($urut);
			$data['page_link'] = $this->db->escape($link);
			$data['parent_id'] = $this->db->escape($parent);
			$data['last_update'] = $this->db->escape($lastupdate);
			$data['user_id'] = $this->db->escape($staffid);
			$data['materi_id'] = $this->db->escape($materiid);
		}
		else{
			$data['mkditawarkan_id'] = $this->db->escape($mk);
			$data['judul'] = $this->db->escape($judul);
			$data['keterangan'] = $this->db->escape($keterangan);
			$data['is_publish'] = $this->db->escape($ispublish);
			$data['status'] = $this->db->escape($status);
			$data['is_campus'] = $this->db->escape($iscampus);
			$data['urut'] = $this->db->escape($urut);
			$data['page_link'] = $this->db->escape($link);
			$data['parent_id'] = $this->db->escape($parent);
			$data['last_update'] = $this->db->escape($lastupdate);
			$data['user_id'] = $this->db->escape($staffid);
			$data['materi_id'] = $this->db->escape($materiid);
		}
		
				
		
		// if(!$id) {
			// $data['materi_id']= $this->get_id();
		// }else{
			// $data['materi_id'];
		// }		
		$result = $this->db->replace("db_ptiik_apps`.`tbl_materimk", $data);
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
		
	}
	
	function replace_saved($datanya) {//save edit
		return $this->db->replace('db_ptiik_apps`.`tbl_materimk',$datanya);
	}
	
	
	function get_id(){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(materi_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_materimk`"; 
		$dt = $this->db->getRow( $sql );
		
		$result = $dt->data;
		
		return $result;
	}
	
	function cek_nama_icon($term){
		$sql = "SELECT icon
				FROM `db_ptiik_apps`.`tbl_materimk`
				WHERE icon = '".$term."'
				";
		$rs = $this->db->getRow( $sql );
		if(isset($rs)){
			$result = $rs->icon;
			return $result;
		}
		
	}
	
	function cek_nama_file($term){
		$sql = "SELECT file_name
				FROM `db_ptiik_apps`.`tbl_file`
				WHERE file_name = '".$term."'
				";
		$rs = $this->db->getRow( $sql );
		if(isset($rs)){
			$result = $rs->file_name;
			return $result;
		}
		
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, keterangan, extention, max_size FROM `db_ptiik_apps`.`tbl_jenisfile` WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		return $result;

	}
	
	function mkditawarkan_id_md5($id=NULL){
		$sql="SELECT mkditawarkan_id
			  FROM `db_ptiik_apps`.`tbl_mkditawarkan`
			  WHERE mid(md5(mkditawarkan_id),9,7) = '".$id."'"; 
		$dt = $this->db->getRow( $sql );
		
		$result = $dt->mkditawarkan_id;
		
		return $result;
	}
	
	function id_file($semester=NULL){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_file`"; 
		$dt = $this->db->getRow( $sql );
		
		$result = $dt->data;
		
		return $result;
	}
	
	function replace_file($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_file', $datanya);
	}
	
	function delete($id=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_materimk 
				SET is_valid = 0 
				WHERE materi_id = '".$id."'
				OR parent_id = '".$id."'
				";
		$result = $this->db->query($sql);
		return $result;
	}
	
	function get_id_from_parent($id=NULL){
		$sql = "SELECT materi_id
				FROM db_ptiik_apps.tbl_materimk 
				WHERE parent_id = '".$id."' AND is_valid = 1
				";
		$result = $this->db->query($sql);
		return $result;
	}
	
	/*-- SILABUS --*/
	
	function get_silabus_id($mk=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_silabus.silabus_id,
				db_ptiik_apps.tbl_silabus.mkditawarkan_id
				FROM
				db_ptiik_apps.tbl_silabus WHERE 1 = 1 ";
		if($mk){
			$sql = $sql . " AND (mid(md5(tbl_silabus.mkditawarkan_id),9,7) = '".$mk."' OR tbl_silabus.mkditawarkan_id='".$mk."') ";
		}
		
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_silabus($mk=NULL, $komponen=NULL, $id=NULL){
		$sql = "SELECT
				mid(md5(db_ptiik_apps.vw_silabus.silabus_id),9,7) as `id`,
				db_ptiik_apps.vw_silabus.silabus_id,
				db_ptiik_apps.vw_silabus.mkditawarkan_id,
				mid(md5(db_ptiik_apps.vw_silabus.mkditawarkan_id),9,7) as `mkid`,
				db_ptiik_apps.vw_silabus.detail_id,
				db_ptiik_apps.vw_silabus.komponen_id,
				db_ptiik_apps.vw_silabus.keterangan,
				db_ptiik_apps.vw_silabus.user_id,
				db_ptiik_apps.vw_silabus.last_update,
				db_ptiik_apps.vw_silabus.komponen
				FROM
				db_ptiik_apps.vw_silabus WHERE 1 = 1 ";
		if($mk){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.vw_silabus.mkditawarkan_id),9,7) = '".$mk."' OR db_ptiik_apps.vw_silabus.mkditawarkan_id='".$mk."') ";
		}
		
		if($komponen){
			$sql = $sql . " AND db_ptiik_apps.vw_silabus.komponen_id = '".$komponen."' ";
		}
		
		if($id){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.vw_silabus.silabus_id),9,7) = '".$id."' OR db_ptiik_apps.vw_silabus.silabus_id='".$id."') ";
		}
		
		/*$sql = "SELECT
				mid(md5(tbl_silabus.silabus_id),9,7) as `id`,
				mid(md5(tbl_silabus.mkditawarkan_id),9,7) as `mkid`,
				db_ptiik_apps.tbl_silabus.silabus_id,
				db_ptiik_apps.tbl_silabus.mkditawarkan_id,
				db_ptiik_apps.tbl_silabus_detail.detail_id,
				db_ptiik_apps.tbl_silabus_detail.komponen_id,
				db_ptiik_apps.tbl_silabus_detail.keterangan,
				db_ptiik_apps.tbl_silabus_detail.user_id,
				db_ptiik_apps.tbl_silabus_detail.last_update
				FROM
				db_ptiik_apps.tbl_silabus
				INNER JOIN db_ptiik_apps.tbl_silabus_detail ON db_ptiik_apps.tbl_silabus.silabus_id = db_ptiik_apps.tbl_silabus_detail.silabus_id
				WHER 1  = 1 
				";
		if($mk){
			$sql = $sql . " AND (mid(md5(tbl_silabus.mkditawarkan_id),9,7) = '".$mk."' OR tbl_silabus.mkditawarkan_idd='".$mk."') ";
		}
		
		if($komponen){
			$sql = $sql . " AND tbl_silabus_detail.komponen_id = '".$komponen."' ";
		}*/
		
		if($id){
			$sql = $sql . " AND (mid(md5(tbl_silabus.silabus_id),9,7) = '".$id."' OR tbl_silabus.silabus_id='".$id."') ";
		}
		
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_silabus_reg_id($mk=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_silabus.silabus_id
				FROM
				db_ptiik_apps.tbl_silabus WHERE 1 = 1 ";
		if($mk){
			$sql = $sql . " AND (mid(md5(tbl_silabus.mkditawarkan_id),9,7) = '".$mk."' OR tbl_silabus.mkditawarkan_id='".$mk."') ";
		}
		$data = $this->db->getRow( $sql );
		
		if($data){
			$result = $data->silabus_id;
		}else{

			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(silabus_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_silabus` WHERE left(silabus_id,4) = '".date("Ym")."'"; 
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;

		
	}
	
	function get_silabus_detail($silabus=NULL, $komponen=NULL){
	
		$sql = "SELECT detail_id FROM db_ptiik_apps.tbl_silabus_detail WHERE silabus_id = '".$silabus."' AND komponen_id = '".$komponen."' ";
		$data = $this->db->getRow( $sql );
		
		if($data){
			$result = $data->detail_id;
		}else{

			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(detail_id,4) AS 
					unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM `db_ptiik_apps`.`tbl_silabus_detail` WHERE left(detail_id,6) = '".date("Ym")."' "; 
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	function get_komponen_silabus(){
		$sql = "SELECT
					`tbl_silabus_komponen`.`komponen_id`,
					`tbl_silabus_komponen`.`keterangan` as `keterangan`,
					`tbl_silabus_komponen`.`is_aktif`
					FROM
					`db_ptiik_apps`.`tbl_silabus_komponen`
					WHERE `tbl_silabus_komponen`.`is_aktif` = 1 ORDER BY `tbl_silabus_komponen`.`urut` ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* -- SILABUS UNUSED 
	function cek_silabus_detail($str=NULL){
		$sql = "SELECT keterangan FROM `db_ptiik_apps`.`tbl_silabus_detail` where 1 = 1 ";
		if($str){
			$sql = $sql = "  AND keterangan = '".$str."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_silabus($komponen = NULL,$id = NULL){
		$sql = "SELECT komponen_id FROM `db_ptiik_apps`.`tbl_silabus_detail` WHERE  1 = 1 ";
		if($id){
			$sql = $sql . " AND silabus_id = '".$id."' ";
		}
		
		if($komponen){
			$sql = $sql . " AND  AND komponen_id ='".$komponen."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}

	function cek_silabus_by_id($mkd_id){
		$sql = "SELECT silabus_id from `db_ptiik_apps`.`tbl_silabus` where mkditawarkan_id = '".$mkd_id."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_silabus_detail_by_id($ket){
		$sql = "SELECT detail_id from `db_ptiik_apps`.`tbl_silabus_detail` where keterangan = '".$ket."'";
		
		$result = $this->db->query( $sql );
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->detail_id;
			}
			return $id;
		}
	}
	*/ 
	
	function replace_silabus($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_silabus',$datanya);
	}
	
	function replace_detail($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_silabus_detail',$datanya);
	}
	/* -- end silabus -- */
	
}