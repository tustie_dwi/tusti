<?php
class content_pembimbing extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpemb 	= new model_pembimbing();
		if($staff!=""&&$staff!="-"){
			$staff = $staff;
		}else $staff = "0";
		if($mhs!=""&&$mhs!="-"){
			$mhs = $mhs;
		}else $mhs = "0";
		$data['staff'] = $staff;
		$data['mhs'] = $mhs;
		$data['posts'] = $mpemb->read($staff,$mhs);
		
		if($staff){ //khusus halaman dosen
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->add_script('js/pembimbing.js');
		}
		$this->view("pembimbing/index.php", $data);
	}
	
	function detail(){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mhsid = $_POST['mhs_id'];
		$data['staff'] = $staff;
		$data['mhs'] = $mhs;
		
		$mpemb 	= new model_pembimbing();
		//$mmhs	= new model_mhs();
		//$data['posts'] = $mpemb->read($staff,$mhsid);
		
		$data['posts'] = $mpemb->get_data_akademik($mhsid);
		$data['biodata'] = $mpemb->get_data_mhs($mhsid);
		
		$this->add_script('js/nav.js');
		
		$this->view("pembimbing/detail.php", $data);
	}
	
}
?>