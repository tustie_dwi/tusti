<?php
class content_upload extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}

	function write() {
		
		$this->coms->add_script('ckeditor/ckeditor.js');
				
		$this->view('slide/edit.php');
	}
	
		
	
	function save(){
		$mpost = new model_content();
				
		$id = $_POST['id'];
		if(!$id) $id = NULL;
				
		//$content_page = $_POST['content_page'];
		$content_title = $_POST['content_title'];
		$content_page  = trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($content_title))))));

		$content_date = implode("-", array_reverse(explode("/",trim($_POST['content_date']))));
		$content_date .= " " . trim($_POST['content_time']);
		
		$content_content = $_POST['content_content'];
		
		if(isset($_POST['b_pub'])){
			$content_status = 'published';
		}else{
			$content_status = 'draft';
		}
			
		$content_author_id 	= $this->coms->authenticatedUser->id;
		
		
		if($_FILES['files']['name']){
							
				$filename = stripslashes(@$_FILES['files']['name']); 
				$extension = $this->getExtension($filename); 
				
				
				
				$newname="assets/upload/slide/".$filename;  
				$copied = move_uploaded_file($_FILES['files']['tmp_name'], $newname); 
				
				
				if (!$copied){  
					print "<script> alert('Copy unsuccessfull!'); </script>"; 
					$errors=1; 
					
					$this->redirect('module/content/upload');
					exit();
					
				}else{
					$errors=0;
					print "<script> alert('Copy successfull!'); </script>"; 
					
							
				}					
			}else{
				if(isset($_POST['hidimg'])){
					$newname = $_POST['hidimg'];
				}else{
					$newname = "";
				}
			}
			
			
		
		$save = $mpost->save_content_file($content_page, $content_title, $content_date, $content_content, $content_status, $content_author_id, $id, $newname, $filename,$extension );
		$result = array();
							
			
		$this->redirect('module/content/upload');
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 100){
	
		if(isset($_POST['b_pub'])||(isset($_POST['b_draft']))){
			$this->save();
			exit();
		}

		$mpost = new model_content();
				
		$data['posts'] = $mpost->get_content_file();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
						
		$this->add_script('js/index.js');
		
		$this->view("slide/index.php", $data);
			
	}
	
	function delete() {
		$url = $this->location();
		
		$id = $_POST['id'];
		$mpost = new model_content();
		$result = array();
		if($mpost->delete_slide($id, $url))
			$result['status'] = "OK";
		else {
			$result['status'] = "NOK";
			$result['error'] = $mpost->error;
		}
		echo json_encode($result);
	}
	
	public function discard() {
		$this->delete();
	}
	
	function edit($contentid) {
		if( !$contentid ) {
			$this->redirect('content');
			exit;
		}
		
		$mcontent = new model_content();
		
		$data['content']	= $mcontent->get_content_file($contentid);
		
		$this->coms->add_script('ckeditor/ckeditor.js');
	
		$this->view('slide/edit.php', $data);
	}
	
	function getExtension($str) { 
		$i = strrpos($str,"."); 
		if (!$i) { return ""; } 
		$l = strlen($str) - $i; 
		$ext = substr($str,$i+1,$l); 
		
		return $ext; 
	}  
	
	
}
?>