<?php
class content_krs extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($msg=NULL){ //halaman index krs
		$mkrs = new model_krs();
		
		$user = $this->coms->authenticatedUser->role;
		$data['msg'] = $msg;
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		if ($user!='mahasiswa'){
			$dosenid	= $this->coms->authenticatedUser->staffid;
			
			$data['posts'] = $mkrs->get_mhs_by_pa($dosenid);
			$this->view('krs/indexdosen.php', $data);
		} 
		else {
			$mhsid	= $this->coms->authenticatedUser->mhsid; 
			$data['sks_max'] = $this->get_sks_max();
			$data['mhsid'] = $mhsid;
			
			$data['post']	= $mkrs->get_mk_krs($mhsid);
			$data['identitas']	= $mkrs->get_identitas_mhs($mhsid);
			$data['semesterke']	= $mkrs->get_semester($mhsid);
			
			$this->add_script('js/krs/krs_index.js');
			
			$this->view('krs/indexmhs.php', $data);			
		}
	}

	function cek_tgl(){ //cek apakah krs bisa diisi berdasarkan tgl sekarang
		$mkrs = new model_krs();
		$data = $mkrs->get_tgl_krs();
		
		$today = date("Y-m-d");
		$today = strtotime($today);
		$exp = strtotime($data->tgl_selesai);
		$start = strtotime($data->tgl_mulai);
		
		if($exp < $today || $start > $today) {
			$this->redirect('module/content/krs/index/mt');
		    exit();
		}
		
	}
	 	
	function kekata($x) { //convert huruf ke kata
		$x = abs($x);
		$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"); 
		$temp = "";
		if ($x <12) { 
			$temp = "". $angka[$x];
		} else if ($x <20) { 
			$temp = kekata($x - 10). " belas";
		} 
		return $temp; 
	}
	
	function get_sks_max($str=NULL){
		$mkrs = new model_krs();
		
		if($str){
			$mhsid=$str;
		}else{
			$mhsid = $this->coms->authenticatedUser->mhsid;
		}
		
		$data = $mkrs->cek_jml_sks($mhsid);
		
		if(isset($data->ip)){
			switch ($data->ip) {
				case $data->ip >= 3:
						$sks = 24;
					break;
				
				case $data->ip >= 2.5:
						$sks = 21;
					break;
					
				case $data->ip >= 2:
						$sks = 18;
					break;
					
				case $data->ip >= 1.5:
						$sks = 15;
					break;
				
				default:
						$sks = 12;
					break;
			}
		}
		else{
			$sks = 21;
		}
		
		return $sks;
	}
	
	function add($msg=NULL){
		$this->cek_tgl();
		$mkrs = new model_krs();
		$user = $this->coms->authenticatedUser->role;
		$mhsid = $this->coms->authenticatedUser->mhsid;
		
		if(isset($_POST['hari'])) {
			if($_POST['hari'] != '') $hari = $_POST['hari'];
			else $hari = '';
		}
		else $hari = '';
		
		if(isset($_POST['mk_select'])) $mk_id = $_POST['mk_select'];
		else $mk_id = NULL;
		
		$data_mhs = $mkrs->get_fakultas_id_by_mhs($mhsid);
		$fak_id = $data_mhs->fakultas_id;
		$cabang_id = $data_mhs->cabang_id;
		
		$data['posts'] = $mkrs->get_mkd($hari, $mk_id, $fak_id, $cabang_id); //data mata kuliah yg bisa diambil
		$data['hari'] = $hari; //hari yang sedang dipilih
		$data['mk'] = $mkrs->get_mk_for_select($fak_id, $cabang_id); //list mk yang bisa diseleksi
		$data['mk_id'] = $mk_id; //mk_id yg sedang dipilih
		$data['msg'] = $msg; //pesan
		
		$sisa = $this->get_sks_max() - $mkrs->get_sks_curr($mhsid);
		$data['sisa_sks'] = $sisa; //sisa sks mhs sekarang
		$data['krs'] = $mkrs->get_krs_id_all($mhsid); //list mk_id yang sudah diambil
		$data['waktu'] = $mkrs->get_waktu($mhsid); //list jadwal mk, untuk seleksi agar tidak bentrok
		
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		// $this->coms->add_script('select/select2.js');
		// $this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->add_script('js/krs/krs_index.js');
		
		$this->view('krs/entri.php', $data);
	}
	
	function save(){
		$jadwal_id = $_POST['jadwal_id'];
		
		// $jadwal_id = 'a6095e';
		
		$this->cek_tgl();
		$mkrs = new model_krs();
		$mhs_id = $this->coms->authenticatedUser->mhsid;
		$user_id = $this->coms->authenticatedUser->id;
		
		if(! $mkrs->cek_parent_id($jadwal_id, $mhs_id)){ //untuk mengecek parent id
			echo "prasyarat tdk memenuhi";
			exit();
		}
		
		$data = $mkrs->cek_kuota($jadwal_id); //melihat kuota kelas dan detail jadwal mk
		
		if($data->jml_peserta >= $data->kuota) { //cek kuota
			// echo $data->jml_peserta . ' ' . $data->kuota;
			echo "full";
			exit();
		}
		else{
			$key = $mkrs->cek_jml_sks($mhs_id); //mengecek krs mhs
			
			$kelas = $data->kelas;
			$sks = $data->sks;
			$mkditawarkan_id = $data->mkditawarkan_id;
			$kode_mk = $data->kode_mk;
			$nama_mk = $data->namamk;
			$tahun_akademik = $mkrs->get_tahunakademik_aktif();
			
			if($key){
				$ip = $key->ip;
				$inf_semester = ($key->inf_semester + 1);
				$jml_sks = $key->jml_sks;
			}
			else{
				$ip = 2.5;
				$jml_sks = 20;
				$inf_semester = 1;
			}
			$sks_curr = $mkrs->get_sks_curr($mhs_id);
			$is_penuh = 1;
			$sisa = 0;
			
			switch ($ip) { //mengecek range ip apakah cocok dengan batas sks max
				case $ip >= 3:
					if($jml_sks < 24) {
						$is_penuh = 0;
						$sisa = 24 - $sks_curr;
					}
					break;
				
				case $ip >= 2.5:
					if($jml_sks < 21) {
						$is_penuh = 0;
						$sisa = 21 - $sks_curr;
					}
					break;
					
				case $ip >= 2:
					if($jml_sks < 18) {
						$is_penuh = 0;
						$sisa = 18 - $sks_curr;
					}
					break;
					
				case $ip >= 1.5:
					if($jml_sks < 15) {
						$is_penuh = 0;
						$sisa = 15 - $sks_curr;
					}
					break;
				
				default:
					if($jml_sks < 12) {
						$is_penuh = 0;
						
						$sisa = 12 - $sks_curr;
					}
					break;
			}
			
			if($is_penuh == 1) { //jika sks max sudah penuh
				echo "sks_max_penuh";
				exit();
			}
			else {
				echo "ok";
				if($sisa >= $sks) { //jika sisa sks masih cukup dengan sisa sks mk yang dipilih
					$data = array(
						'krs_id' => $mkrs->get_krs_id(),
						'mahasiswa_id' => $mhs_id,
						'mkditawarkan_id' => $mkditawarkan_id,
						'nama_mk' => $nama_mk,
						'kode_mk' => $kode_mk,
						'sks' => $sks,
						'tahun_akademik' => $tahun_akademik,
						'inf_semester' => $inf_semester,
						'nilai_akhir' => 0,
						'kelas' => $kelas,
						'is_approve' => 0,
						'user_id' => $user_id,
						'last_update' => date('Y-m-d H:i:s')
					);
					
					$mkrs->add_krs($data);
					$mkrs->update_kapasitas_kelas($jadwal_id, $kelas);
					
					exit();
				}
				else {
					echo "sks_penuh";
					exit();
				}
			} //end of if(jika sks cukup)
			
		} //end of cek kuota
		
	} //end of function add krs
	
	function del(){
		$this->cek_tgl();
		$mkrs = new model_krs();
		$krs_id = $_POST['krs_id'];
		$mkrs->decrease_kapasitas_kelas($krs_id);
		$mkrs->del_krs($krs_id);
		
		exit();
	}
	
	function approve($mhsid=NULL){
		$mkrs = new model_krs();
		$mkrs->rekap_ipk($mhsid);
		$data['posts'] = $mkrs->get_mk_krs($mhsid);
		$data['identitas']	= $mkrs->get_identitas_mhs($mhsid);
		$data['semesterke']	= $mkrs->get_semester($mhsid);
		$data['ip'] = $mkrs->get_ip_terakhir($mhsid);
		$data['mhsid'] = $mhsid;
		
		$this->add_script('js/krs/krs_index.js');
		$this->view('krs/approve.php', $data);
	}	
	
	function save_approve(){
		$mkrs = new model_krs();
		$mhsid = $_POST['mhsid'];
		$dosenid = $this->coms->authenticatedUser->staffid;
		$mkrs->approve_krs($dosenid);
		
		$this->redirect('module/content/krs/approve/' . $mhsid);
	}
	
	function batal_krs(){
		$mkrs = new model_krs();
		$mkrs->batal_krs();
	}

	
}
?>