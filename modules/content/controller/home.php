<?php
class controller_home extends comscontroller {

	function __construct() {
		
		parent::__construct();
		$this->require_auth('page');
		
	}
	
	function index($page = '', $hal = '', $start = '') {
		
		
		$id = $this->authenticatedUser->staffid;
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		
		$this->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_style('css/bootstrap/token-input.css');
		$this->add_style('css/bootstrap/tagmanager.css');
		
		$this->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/jquery/jquery.autogrowtextarea.min.js');
		$this->add_script('js/home/jquery-ias.js');
		$this->add_script('js/home/home.js');
		$this->add_script('js/user/index.js');
		$this->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jquery/tagmanager.js');
		

		$data['kegiatan']	= "";
		$data['tasks']		= "";
		
		$mhome= new model_home();		
		
		$data['posts'] 		= "";
		//$data['lists'] = $mhome->read('');
		
		$data['shares'] = $mhome->share();
		
		
		if($role=="Mahasiswa"){
			$data['viewby'] = $mhome->view_by_mhs($mhs);
		}else if ($role=="Dosen") {
			$data['viewby'] = $mhome->view_by_dosen($staff);
		}else{
			$data['viewby'] = "";
		}
					
		$page = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9\-]/', '', urldecode(html_entity_decode(strip_tags(strtolower($page)))))));
		if(empty($page)) {
			//$this->redirect('notfound');
			$page = 'view';
		}
		
		$start = trim(preg_replace('/ +/', ' ', preg_replace('/[^0-9+]/', '', urldecode(html_entity_decode(strip_tags($start))))));
		if($hal != 'view') {
			$hal = 'view';
		}
		if(($start == '') || (!is_numeric($start))) {
			$start = 1;
		} else {
			$start = intval($start);
		}
		
		$limit = 10;		
		$base = $this->location('home/index/').$page;
		//$base = $this->location('home/index/');
		$total = $mhome->total();

		$data['lists'] = $mhome->read($start,$limit);
		$data['pagination'] = $this->pagination($base, $start, $total, $limit);
		$data['comments'] = $mhome->comment('');
	
		$this->head();
		$this->view('home.php', $data );
		$this->foot();
	}



function comments($status_id=NULL) { 
	
	$mhome= new model_home();
	
	$comments = $mhome->comment($status_id);
	
	?>
	
	<?php 
	
	$count=0;
	
	if( isset($comments)) {
		$commc=1;
		
			if(count($comments)>5){ ?>
                <div class="media comment-wrap" data-id="64fb82" data-showall=<?php echo $status_id ?>>
			  	<div class="media-body">
			    	<div class="keterangan"><a class='show-all-comment' href="javascript::" onclick="show_all_comment('<?php echo $status_id;?>')" data-commentclick=<?php echo $status_id;?> style="text-align:center;display:block"><i class='fa fa-refresh'></i> Show All</a></div>							
									</div>
				</div>
            
                
                <?php }
		foreach ($comments as $com): 
		// if ($com->parent_id==$status_id) {
			?>
			
			<div class="media comment-wrap" data-id="<?php echo $com->status_id ?>" data-showit=<?php echo $status_id ?> <?php if(count($comments)>5 && $commc<=5)echo 'style="display:none"'; ?>>
				<a class="pull-left" href="#">
					<?php
					if($com->foto){
					?>
						<img class="media-object" src="<?php echo $this->location($com->foto); ?>" alt="..." height="54px" width="54px">
					<?php
					}else{
					?>
			    	<img class="media-object" src="http://placehold.it/54x54" alt="..." height="54px" width="54px">
					<?php } ?>
				</a>
			  	<div class="media-body">
			    	<h4 class="media-heading"><a class="timeline-user" style="cursor:pointer;"><?php echo $com->name; ?></a></h4>
                    <div class="status-action">
            <small class="timeline-time"><?php echo date('M d, Y H:i',strtotime($com->last_update)); ?></small>&nbsp;
	    	<?php if ($this->authenticatedUser->id==$com->user_id) { ?>
				<a class='btn-edit-post delete-comment' onclick='delete_comment_status("<?php echo $this->location('home/delete/'.$com->status_id); ?>","<?php echo $com->status_id ?>","comment")'data-id="<?php echo $com->status_id ?>" href="javascript::" data-uri="<?php echo $this->location('home/delete/'.$com->status_id); ?>"><i class='fa fa-trash-o'></i></a>	&nbsp;									
			<?php }?>
            </div>
			    	<div class="keterangan"><?php echo strip_tags($com->keterangan); ?></div>
			    	
				</div>
			</div>
                
			<?php 
				$commc++; 
			//}
			endforeach; ?>
<?php  } ?>
<?php }


function pagination($base_location, $page, $total, $limit) {

		// Initial page num setup
		if ($page == 0){ $page = 1; }
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil($total / $limit);
		$LastPagem1 = $lastpage - 1;
		$stages = 2;

		$paginate = '';
		if($lastpage > 1) {
			$paginate .= "<ul class='pagination pagination-sm'>";
			if ($page > 1) {
				$paginate.= "<li><a title='Previous' href='$base_location/page/$prev'>&laquo;</a></li>";
			} else {
				$paginate.= "<li class='disabled'><span>&laquo;</span></li>";
			}
			if ($lastpage < 7 + ($stages * 2)) {
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page){
						$paginate.= "<li class='active'><span>$counter <span class='sr-only'>(current)</span></span></li>";
					} else {
						$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
				}
			}
			elseif($lastpage > 5 + ($stages * 2)) {
				if($page < 1 + ($stages * 2)) {
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
						if ($counter == $page){
							$paginate.= "<li class='active'><span>$counter</span></li>";
						}else{
							$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
					}
					$paginate.= "<li><span>...</span></li>";
					$paginate.= "<li><a title='Page $LastPagem1' href='$base_location/page/$LastPagem1'>$LastPagem1</a></li>";
					$paginate.= "<li><a title='Page $lastpage' href='$base_location/page/$lastpage'>$lastpage</a></li>";
				}
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
				{
					$paginate.= "<li><a title='Page 1' href='$base_location/page/1'>1</a></li>";
					$paginate.= "<li><a title='Page 2' href='$base_location/page/2'>2</a></li>";
					$paginate.= "<li><span>...</span></li>";
					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
					{
						if ($counter == $page){
							$paginate.= "<li class='active'><span>$counter</span></li>";
						}else{
							$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
					}
					$paginate.= "<li><span>...</span></li>";
					$paginate.= "<li><a title='Page $LastPagem1' href='$base_location/page/$LastPagem1'>$LastPagem1</a></li>";
					$paginate.= "<li><a title='Page $lastpage' href='$base_location/page/$lastpage'>$lastpage</a></li>";
				}
				else
				{
					$paginate.= "<li><a title='Page 1' href='$base_location/page/1'>1</a></li>";
					$paginate.= "<li><a title='Page 2' href='$base_location/page/2'>2</a></li>";
					$paginate.= "<li><span>...</span></li>";
					for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
						if ($counter == $page){
							$paginate.= "<li class='active'><span>$counter</span></li>";
						} else {
							$paginate.= "<li><a title='Page $counter' href='$base_location/page/$counter'>$counter</a></li>";}
					}
				}
			}
			if ($page < $counter - 1) {
				$paginate.= "<li><a title='Next' href='$base_location/page/$next' class='next-page'>&raquo;</a></li>";
			} else {
				$paginate.= "<li class='disabled'><span>&raquo;</span></li>";
			}
			$paginate.= "</ul>";
		}
		 return $paginate;
	}

	function save(){
		if(isset($_POST['b_status'])!=""){
			$this->saveToDB();
			//$this->redirect('module/content/status/write');
			exit();
		}else{
			$this->write();
			exit;
		}
	}
	
	function delete($id){
		$mhome = new model_home();
		$mhome->del_comment($id);
	}
	
	function delete_status($id){
		$mhome = new model_home();
		$mhome->del_parent($id);
		$mhome->del_status($id);
	}
	
	function saveToDB(){
		ob_start();
		$mhome= new model_home();	
						
		if($_POST['hidId']!=""){
			$status_id 	= $_POST['hidId'];
			$action 	= "update";
		}else{
			$status_id	= $mhome->get_reg_number();
			$action 	= "insert";	
		}
		
		$keterangan		= $_POST['keterangan'];
		
		$parent_id		= $_POST['parent_id'];
// 		
		// if ($this->authenticatedUser->role == "mahasiswa"){
			// $user_id		= $this->authenticatedUser->mhsid;
		// } else {
			// $user_id		= $this->authenticatedUser->staffid;
		// }
		
		$user_id		= $this->authenticatedUser->id;
		
		$last_update	= date("Y-m-d H:i:s");	
		
		
		//$jadwal_id		= $_POST['share-jadwal-id'];
		//$mk_id			= $_POST['share-mk-id'];	
		
		$datanya 	= Array(
						'status_id'=>$status_id, 
						'keterangan'=>$keterangan,
						'parent_id'=>$parent_id,
						'user_id'=>$user_id,
						'last_update'=>$last_update, 
						);
		$mhome->replace_status($datanya);
		
		/*Insert Comment*/
		if($_POST['jadwal_id']!=""){
			$comjadwal_id=$_POST['jadwal_id'];
		}else{
			$comjadwal_id="0";
		}
		
		if($_POST['mkditawarkan_id']!=""){
			$commkditawarkan_id=$_POST['mkditawarkan_id'];
		}else{
			$commkditawarkan_id="0";
		}
		
		$share_id	= $mhome->get_reg_share();
		$datasharecom 	= Array(
						'share_id'=>$share_id,
						'jadwal_id'=>$comjadwal_id, 
						'mkditawarkan_id'=>$commkditawarkan_id,
						'status_id'=>$status_id, 
					);
		$mhome->replace_share($datasharecom);
		/*End Insert Comment*/
		
		
		
		$hidjadwal	= $_POST['hidjadwal'];
		$hidmk	= $_POST['hidmk'];
		
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		//echo $hidjadwal;
		if ($hidjadwal){
			$j=explode(",",$hidjadwal);
			for ($i=0; $i<count($j); $i++){
				//echo $j[$i];
				$j1[$i] = explode("-",$j[$i]);
				for ($k=0; $k<count($j[$i]); $k++){
					//echo $i.'-'.$k.'-'.$j1[$i][$k]; echo '<br />';
					$namamk = $j1[$i][0];
					$kelas = $j1[$i][1];
					
					if($role=="mahasiswa"){
						$idjadwal = $mhome->get_jadwal_id_mhs($namamk, $kelas);
					}else if ($role=="Dosen") {
						$idjadwal = $mhome->get_jadwal_id_dosen($namamk, $kelas);
					} 	
					
					//echo $idjadwal;
					$share_id	= $mhome->get_reg_share();
					
					$datashare 	= Array(
						'share_id'=>$share_id,
						'jadwal_id'=>$idjadwal, 
						'status_id'=>$status_id, 
					);
					$mhome->replace_share($datashare);
				}	
			}
		} 
		
		if ($hidmk){
			$j=explode(",",$hidmk);
			for ($i=0; $i<count($j); $i++){
				//echo $j[$i];
				//$j1[$i] = explode("-",$j[$i]);
				//for ($k=0; $k<count($j[$i]); $k++){
					//echo $i.'-'.$k.'-'.$j1[$i][$k]; echo '<br />';
					$namamk = $j[$i];
					//$kelas = $j1[$i][1];
					if($role=="mahasiswa"){
						$idmk = $mhome->get_mk_id_mhs($namamk);
					}else if ($role=="Dosen") {
						$idmk = $mhome->get_mk_id_dosen($namamk);
					} 	
					
					
					//echo $idjadwal;
					$share_id	= $mhome->get_reg_share();
					
					$datashare 	= Array(
						'share_id'=>$share_id,
						'mkditawarkan_id'=>$idmk, 
						'status_id'=>$status_id, 
					);
					$mhome->replace_share($datashare);
				//}	
			}
		}
		// foreach ($hidjadwal as $j) {
			// $i=0;
			// $j1=explode(",",$j);
			// foreach ($j1 as $a){
				// echo $a;	
				// $i++;
			// }
// 				
		// }
		
		
		// foreach ($jadwal as $j) {
			// $i=0;
		 	// $j1=explode(",",$j);
		 		// foreach ($j1 as $a){
					// $j2=explode("-",$a);
		 			// foreach ($j2 as $b){
		 				// echo $j2;
				// }				
			// }
			// $i++;
		// }		
		
		
		$this->redirect('home');
		exit();
	}

	function mk(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mhome 	= new model_home();
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		if($role=="mahasiswa"){
			$data	= $mhome->get_mk_mhs($str,$mhs);
		}else if ($role=="Dosen") {
			$data	= $mhome->get_mk_dosen($str,$staff);
		} 	
		
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function jadwal(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mhome 	= new model_home();
		
		$role	= $this->authenticatedUser->role;
		$user	= $this->authenticatedUser->id;
		$staff	= $this->authenticatedUser->staffid;
		$mhs	= $this->authenticatedUser->mhsid;
		
		if($role=="mahasiswa"){
			$data	= $mhome->get_jadwal_mhs($str,$mhs);
		}else if ($role=="Dosen") {
			$data	= $mhome->get_jadwal_dosen($str,$staff);
		} 	
	
		
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
}
?>