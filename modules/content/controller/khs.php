<?php
class content_khs extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	//========KHS=======//
	
	function index(){
		$mkhs = new model_khs();
		
		$user = $this->coms->authenticatedUser->role;
		$staff	= $this->coms->authenticatedUser->staffid;
		 
		if($user=="mahasiswa" || $user=="dosen" || $user=="admin"){
			
			$data['user']=$user;
			if(isset($_POST['mahasiswa_id'])){
				$mhsid = $_POST['mahasiswa_id'];
			}
			else{
				$mhsid	= $this->coms->authenticatedUser->mhsid;
			}
			
			if(isset($_POST['cmbsemester'])&&($_POST['cmbsemester'])){
				$semester = $_POST['cmbsemester'];			
			}else{
				$semester = $mkhs->get_semester_aktif();
			}
			
						
			$data['semester']	= $mkhs->get_semester($mhsid);
			$data['semesterid'] = $semester;
			$data['mhsid'] = $mhsid;
			$data['staff_id'] = $staff;
			
			$data['post']	= $mkhs->read($semester, $mhsid, '', '');
			$data['identitas']	= $mkhs->read($semester, $mhsid, '1', ''); //1 adalah syarat saja supaya tidak null
			$data['semesterke']	= $mkhs->read('', $mhsid, '', '1'); //1 adalah syarat saja supaya tidak null
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->view('khs/index.php', $data);
		}
	}
	
	function convertTohuruf($angka){
		if($angka > 80){
			$huruf = 'A';
		}
		elseif($angka > 75 && $angka <= 80){ 
			$huruf = 'B+';
		}
		elseif($angka > 69 && $angka <= 75){
			$huruf = 'B';
		}
		elseif($angka > 60 && $angka <= 69){
			$huruf = 'C+';
		}
		elseif($angka > 55 && $angka <= 60){
			$huruf = 'C';
		}
		elseif($angka > 50 && $angka <= 55){
			$huruf = 'D+';
		}
		elseif($angka > 44 && $angka <= 50){
			$huruf = 'D';
		}
		elseif($angka <= 44){
			$huruf = 'E';
		}
		return $huruf;
	}
	
	function IPK($mhsid){
		$mkhs = new model_khs();
		$ipk = $mkhs->get_sks_all_count($mhsid);
		if(isset($ipk)){
			foreach($ipk as $dt){
				$hasil = $dt->IPK;
				return $hasil;
			}
		}
	}
	
	function kekata($x) { 
		$x = abs($x);
		$angka = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"); 
		$temp = "";
		if ($x <12) { 
			$temp = "". $angka[$x];
		} else if ($x <20) { 
			$temp = kekata($x - 10). " belas";
		} 
		return $temp; 
	}
	
	function test(){
		echo $_POST['cmbsemester'];
		echo $_POST['mahasiswa_id'];
	}
	
	
}
?>