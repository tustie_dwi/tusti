<?php

/*
<!---//---------------------------------------------------------------------
		Author			: SYF
		Creation Date	: 10/12/13 (Initial version)
		Purpose			: tugas
		Revision Log:
		TDP 12/02/14 	edit method, rename $user to $role, edit mhs_id from user name to user id, remove unnecessary method
----------------------------------------------------------------------//--->
*/

class content_tugas extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($mk=NULL, $by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mtest = new model_test();
		$mtugas = new model_tugas();
		
		$role = $this->coms->authenticatedUser->role;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$data['role']=$role;
		
		if($role=='mahasiswa'){
			$mkhs = new model_khs();
			
			$data['semester']	= $mkhs->get_semester($mhs);
		}else {
			$mconf = new model_conf();
			
			$data['semester']	= $mconf->get_semester();
		}
		
		
		if(isset($_POST['cmbsemester'])){
			$semester = $_POST['cmbsemester'];			
		}else{
			$mpost 	= new model_course();
			
			$semester = $mpost->get_semester_aktif();
		}
		$data['semesterid'] = $semester;

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->coms->add_script('js/jsall.js');
		$this->add_script('js/nav.js');
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
		}

		if($role=='mahasiswa'){
			$data['test']	= $mtest->get_test_by_mk("", $mhs, $semester);
			$data['tugas']  = $mtugas->read('','','','',$mhs, $semester);
			//$data['tugas']	= "";
		}else{
			$data['test']	= $mtest->get_test_by_dosen("", $staff, $semester);
			$data['tugas']  = $mtugas->read('','','',$staff,'', $semester);
		}
		
		$this->view('exam/index.php', $data);
		
	}

	//========TUGAS=======//
	
	function write($mk=NULL){
		$role = $this->coms->authenticatedUser->role;
		
		
		$mtugas = new model_tugas();
		$data['role'] 	= $role;
		$data['mk'] 	= $mk;
		$data['jadwal_id']=$_POST['jadwal_id'];
		$data['posts'] 	= "";
		$data['jadwal'] = $mtugas->get_jadwal();
		$data['materi'] = $mtugas->get_materi_by_mk($mk);
		
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
		$this->add_script('js/tugas/tugas.js');
		$this->add_script('js/nav.js');

		$this->view( 'tugas/edit.php', $data );
		
	}
	
	function edit($id=NULL,$mk=NULL){
		if( !$id ) {
			$this->redirect('module/content/tugas/');
			exit;
		}
		$role = $this->coms->authenticatedUser->role;
		
		$mtugas = new model_tugas();
		$data['role'] = $role;
		$jadwalid 	= $_POST['jadwal_id'];
		
		$data['posts'] 		= $mtugas->read($id,$mk,$jadwalid);
		$data['jadwal'] 	= $mtugas->get_jadwal();
		$data['tgs_mhs'] 	= $mtugas->get_tugas_mhs($id,'');
		$data['materi'] 	= $mtugas->get_materi_by_mk($mk);
		$data['mk'] 		= $mk;
		
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
		$this->add_script('js/tugas/tugas.js');
		$this->add_script('js/nav.js');

		$this->view( 'tugas/edit.php', $data );
		
	}
	
	function detail($id=NULL,$mk=NULL,$jadwal=NULL){
		if( !$id ) {
			$this->redirect('module/content/tugas/');
			exit;
		}
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		
		$data['post'] 		= $mpost->read_mk_by_dosen("","",$mk);
		$data['pengampu'] 	= $mpost->get_pengampu($mk);
		
		$data['role'] = $role;
		
		$mtugas = new model_tugas();
		$data['tugas'] = $mtugas->read($id);
		
		if(isset($_POST['jadwal_id'])){
			$data['jadwalid']	= $_POST['jadwal_id'];
		}else{
			$data['jadwalid']	= "";
		}
		
		if(isset($_POST['jenis'])){
			$data['jenis']	= $_POST['jenis'];
		}else{
			$data['jenis']	= "";
		}
		
		$data['mk']				= $mk;
		$data['tugasid']		= $id;
		$data['mtugas']			= $mtugas;
		$data['mhs']			= $mhs;
		
		$this->add_script('js/tugas/tugas.js');
		$this->add_script('js/nav.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
		$this->coms->add_script('ckeditor/ckeditor.js');

		$this->view( 'tugas/detail.php', $data );
		
	}
	
	function tampilkan_materi(){
		$mtugas = new model_tugas();
		$jadwal_id = $_POST['jadwal_id'];
		$mk_id = $mtugas->get_mk_by_jadwal($jadwal_id);
		if($mk_id){
			foreach ($mk_id as $dt){
				$mk = $dt->mkid;
			}
			//echo $mk;
			$materi = $mtugas->get_materi_by_mk($mk);
			echo "<option value='0'>Select Materi</option>" ;
			if($materi){
				foreach($materi as $p )
				{
					echo "<option value='".$p->materi_id."'";
					echo ">".$p->judul."</option>" ;
				}
			}
		}
	}

		
	function save(){
		if((isset($_POST['judul'])) && $_POST['judul']!=""){
			$this->saveToDB();
			exit();
		}else{
			echo "Maaf, data tidak dapat tersimpan";
			exit();
		}
	}
	
	function saveToDB(){
		ob_start();
		$mtugas = new model_tugas();
		if($_POST['hidtmp']){
			$tugasid 		= $_POST['hidtmp'];
			$action 		= "update";
		}else{
			$tugasid		= $mtugas->tugas_id();
			$action 		= "insert";			
		}
		
			
		if(isset($_POST['materi'])){
			$materi_id	= $mtugas->get_materi_id_by_md5($_POST['materi']);
		}

		if(isset($_POST['jadwal'])){
			$jadwal_id	= $mtugas->get_jadwal_id_by_md5($_POST['jadwal']);
		}
		
		$judul		= $_POST['judul'];
		$instruksi	= $_POST['instruksi'];
		$keterangan	= $_POST['keterangan'];
		$tgl_mulai	= $_POST['tanggalmulai'];
		$tgl_selesai= $_POST['tanggalselesai'];
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$new	= $_POST['newtugas'];
		
		if(isset($judul) && isset($materi_id) && isset($jadwal_id)){			
				
					$datanya 	= Array('tugas_id'=>$tugasid, 
										'materi_id'=>$materi_id, 
										'jadwal_id'=>$jadwal_id, 
										'judul'=>$judul,
										'instruksi'=>$instruksi, 
										'keterangan'=>$keterangan, 
										'tgl_mulai'=> $tgl_mulai, 
										'tgl_selesai'=> $tgl_selesai, 
										'user_id'=>$user,
										'last_update'=>$lastupdate
									   );
					$mtugas->replace_tugas($datanya);
					
				echo "OK, data telah diupdate";
			exit();		
		}
		else {
			echo "Maaf, data tidak dapat tersimpan.";
			exit();
		}
	}
	//=====END TUGAS=====//
	
	//====TUGAS MHS====//
	
	function save_tgs_mhs(){
	
		if($_POST['catatan']){
			$this->save_tugas_mhs_to_db();
			exit();
		}else{
			echo "Maaf, data tidak dapat tersimpan";
			exit();
		}
	
	}
	
	function save_tugas_mhs_to_db(){
		ob_start();
		
		$mtugas = new model_tugas();
		$mfile = new model_file();

		$fileid			= $mfile->id_file();
		$uploadid		= $mtugas->upload_id();

		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;

		$judul		= "";
		$keterangan	= "";
		$tugasid 	= $_POST['tugasid'];
		$catatan 	= $_POST['catatan'];
		$ispublish 	= 0;
		$materi_id	= $_POST['materiid'];
		$mkd_id		= $_POST['mkid'];
		$mhs_id		= $_POST['mhsid'];
		
		if(isset($catatan)){
			$uploadby 	= $this->coms->authenticatedUser->username;
			
			// //=============FILE UPLOAD START==================
			$this->upload_file($tugasid, $uploadid, $fileid, $judul, $keterangan, $ispublish, $materi_id, $mkd_id, $uploadby, $lastupdate, $user);
			// //=============FILE UPLOAD END====================
			$datanya 	= Array('upload_id'=>$uploadid,
								'mahasiswa_id'=>$mhs_id,
								'tugas_id'=>$tugasid,
								'tgl_upload'=>$lastupdate,
								'catatan'=>$catatan,
							    'user_id'=>$user,
							    'last_update'=>$lastupdate
								  );
			$mtugas->replace_tugas_mhs($datanya);
			
			$this->log_upload("upload_tugas", $fileid);
			
			echo "OK, data telah diupdate";
			exit();
			
		}
		else{
			echo "Maaf, data tidak dapat tersimpan";
			exit();
		}
	}

	function log_upload($action, $fileid){
		$mfile = new model_file();
		$user = $this->coms->authenticatedUser->id;
		$result = $mfile->log_data($user, $action, $fileid);
	}
	
	function upload_nilai(){
		$mtugas = new model_tugas();
		$skor = $_POST['nilai'];
		$upload_id = $_POST['uploadid'];
		$skor = $mtugas->update_skor($skor,$upload_id);
		if($skor==TRUE){
			echo "Berhasil Memberi Skor!";
		}
		else echo "Gagal Memberi Skor!";
	}
	
	function upload_file($tugasid, $uploadid,$fileid, $judul, $keterangan, $ispublish, $materi_id=NULL, $mkd_id=NULL, $uploadby, $lastupdate, $user){
		$mfile = new model_file();
		$mtugas = new model_tugas();
		$month = date('m');
		$year = date('Y');
		foreach($_FILES["uploads"]['name'] as $id => $name){
			$ext	= $this->get_extension($name);
			$seleksi_ext = $mfile->get_ext($ext);

			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
				
				switch(strToLower($jenis)){
					case 'image':
						$extpath = "image";
					break;
					case 'presentation':
						$extpath = "presentation";
					break;
					case 'document':
						$extpath = "document";
					break;
					case 'video':
						$extpath = "video";
					break;
					case 'spreadsheet':
						$extpath = "spreadsheet";
					break;
				}
			}
			else{
				echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
				exit();
			}

			$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx', 'webm');
			//$fileid = $mfile->id_file();

			$file_name = $name;
			$ceknamafile = $mfile->cek_nama_file($file_name);
			//$file = addslashes(file_get_contents($_FILES["uploads"]["tmp_name"][$id]));

			$file = "";
			$file_type=$_FILES["uploads"]["type"][$id] ; 
			$file_size=$_FILES["uploads"]["size"][$id] ; 
			$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
			$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;

			if (!file_exists($upload_dir)) {
				mkdir($upload_dir, 0777, true);
				chmod($upload_dir, 0777);
			}
			$file_loc = $upload_dir_db . $name;
			
			$tgl_upload = date("Y-m-d H:i:s");
			
			if(!in_array($ext,$allowed_ext)){
				echo "Maaf, tipe file yang di upload salah";
				exit();
			}
			elseif (($ceknamafile==NULL) && ($file_size <= $maxfilesize)){			
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
							rename($_FILES['uploads']['tmp_name'][$id], $upload_dir . $name);
				}
					$datanya2 	= Array('file_id'=>$fileid,
										'jenis_file_id'=>$jenis_file_id,
										'materi_id'=>$materi_id,
										'mkditawarkan_id'=>$mkd_id,
										'tugas_id'=>$tugasid,
										'upload_id'=>$uploadid,
									    'judul'=>$judul,
									    'keterangan'=>$keterangan,
									    'file_name'=>$file_name,
									    'file_type'=>$file_type,
									    'file_loc'=>$file_loc,
									    'file_size'=>$file_size,
									    'file_content'=>$file,
									    'tgl_upload'=>$tgl_upload,
									    'upload_by'=>$uploadby,
									    'is_publish'=>$ispublish,
									    'user_id'=>$user,
									    'last_update'=>$lastupdate
								  );
					$mfile->replace_file($datanya2);
					$mtugas->replace_log_file($datanya2);
	
			}
			else{
				echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
				exit();
			}
		}			
	}

	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_title($file_name){
		$name = explode('.', $file_name);
		array_pop($name);
		return implode('.', $name);
	}
	
	function get_diff_time($date){
		$date1 = strtotime($date);
		$date2 = time();
		
		$subTime = $date1 - $date2;
		$y = ($subTime/(60*60*24*365));
		$d = ($subTime/(60*60*24))%365;
		$h = ($subTime/(60*60))%24;
		$m = ($subTime/60)%60;
		
		if($subTime > 0){
			echo "<p>Assignment is Remaining ";
			echo $d." days \n";
			echo $h." hours \n";
			echo $m." mins \n </p>";
			return 1;
		}
		else{
			$this->get_diff_time_overdue($date);
			return 0;
		}
	}
	
	function get_diff_time_overdue($date){
		$date1 = strtotime($date);
		$date2 = time();
		
		$subTime = $date2 - $date1;
		$y = ($subTime/(60*60*24*365));
		$d = ($subTime/(60*60*24))%365;
		$h = ($subTime/(60*60))%24;
		$m = ($subTime/60)%60;

		echo "<code>Assignment is overdue by ";
		echo $d." days \n";
		echo $h." hours \n";
		echo $m." mins \n </code>";
	}

	
	//====END TUGAS MHS====//
}
?>