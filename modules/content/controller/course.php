<?php
/*
<!---//---------------------------------------------------------------------
		Author			: Thusti Dwi  P. (TDP)
		Creation Date	: 10/12/13 (Initial version)
		Purpose			: course module
		Revision Log:
		TDP 10/02/14 	edit detail, save silabus, rename send_jadwal_id.js -- nav.js 	
----------------------------------------------------------------------//--->
*/
class content_course extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}

	function index(){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		
		$data['semester']	= $mpost->get_semester();
		
		if(isset($_POST['cmbsemester'])){
			$semester = $_POST['cmbsemester'];			
		}else{
			$semester = $mpost->get_semester_aktif();
		}
		$data['semesterid'] = $semester;
		
		if($role=='mahasiswa'){
			$data['post']	= $mpost->read_mk_by_mhs($semester, $mhs);
		}else{
			$data['post']	= $mpost->read_mk_by_dosen($semester, $staff);
		}
		
		$data['role']	= $role;
		$this->add_script('js/nav.js');
		$this->view("course/index.php", $data);			
	}
	
	function get_frm_silabus(){
		$mpost = new model_course();
		
		$mkid		= $_POST['mkid'];
		$komponen	= $_POST['cmbkomponen'];
		//$data['silabus'] = $mpost->get_silabus($mkid, $komponen);
		$silabus = $mpost->get_silabus($mkid, $komponen);
		$result = array();
		$result['keterangan'] = $silabus->keterangan;
		$result['id']			= $silabus->silabus_id;
		$result['detail']		= $silabus->detail_id;
		echo json_encode($result);
		
		//return json_encode($result);
		//$this->coms->add_script('ckeditor/ckeditor.js');
		//$this->view("course/silabus/form_silabus.php", $data);	
	}
	
	function detail($str=NULL,$id=NULL, $jadwal=NULL){	
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		
		if($role!="mahasiswa"){
			$data['post'] 		= $mpost->read_mk_by_dosen("","",$id,$staff);
		}
		else{
			$data['post'] 		= $mpost->read_mk_by_mhs("","",$id);
		}
		
		$data['pengampu'] 	= $mpost->get_pengampu($id);		
	
		$data['role']		= $role;
				
		if(isset($_POST['jadwal_id'])){
			$jadwalid 	= $_POST['jadwal_id'];
		}else{
			if($jadwal){			
				$jadwalid	= $jadwal;
			}else{
				$jadwalid	= "";
			}
		}
		$data['jadwal_id']  = $jadwalid;
		$data['mhs'] 		= $mpost->read_mk_by_mhs("","",$id,$staff,$jadwalid);
		$data['user'] = $role;
		$data['mk'] = $id;
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datepicker.js');
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->add_script('js/nav.js');
		$this->add_script('js/delete.js');
		$this->coms->add_script('js/submit.js');
		
		
		switch($str){
			case 'silabus':
				$data['materi'] = $mpost->get_materi($id);
				$data['komponen']	= $mpost->get_komponen_silabus();
				$this->add_script('js/silabus/silabus.js');
				$this->coms->add_script('js/jsall.js');
				$this->view("course/silabus/index.php", $data);
			break;
			case 'file':
				$mfile = new model_file();
				$this->add_script('js/file/file.js');
				$this->coms->add_script('js/jsall.js');
				$this->coms->add_script('js/log.js');
				
				$data['kategori'] = $mfile->get_kategori_by_materimk($id);
				$data['materi'] = $mpost->get_materi($id);
				
				$this->view("course/file/index.php", $data);
			break;
			case 'tugas':
				$mtugas = new model_tugas();
				
				$data['posts'] = $mtugas->read("",$id,$jadwalid);
		
				$this->view("course/tugas/index.php", $data);
			break;
			case 'test':
				$mtest = new model_test();
				$data['jadwal']		= $jadwalid;
				$data['posts'] 		= $mtest->read("",$id,$jadwalid);
				
				$this->view("course/test/index.php", $data);
			break;
			case 'nilai':
				$mnilai 			= new model_nilai();
				$data['role']  		= $role;
				$this->add_script('js/nilai/nilai.js');
				$data['posts'] 		= $mnilai->get_nilai_proses('', $id, '');
				$data['jadwal']		= $jadwalid;
				
				$data['isproses_tblproses']		= $mnilai->get_is_proses_tblproses($id,$jadwalid);	
				$data['isproses_tblprosesmhs']	= $mnilai->get_is_proses_tblprosesmhs($id,$jadwalid);
				$data['cektblprosesmhs']		= $mnilai->get_proses_nilai_mhs($id,$jadwalid);
				
				$this->view("course/nilai/index.php", $data);
			break;
			case 'mhs';
				$this->view("course/mhs/index.php", $data);
			break;
			case 'edit_nilai':
			
				$mnilai = new model_nilai();
				
				$this->add_script('js/nilai/nilai.js');
				
				$data['mkid']		= $id;
				$data['jadwal']		= $jadwalid;
				$data['action'] 	= 'write';
				$prosesnilai		= $mnilai->get_proses_nilai($id, $jadwalid);
				
				if(isset($prosesnilai)){
					$data['posts']		= $mnilai->get_proses_nilai($id, $jadwalid);
					$data['kategori']	= 'edit';
					//$this->view("course/nilai/edit.php", $data);
				}else{
					$data['posts'] 		= $mnilai->get_komponen($id,'');
					$data['kategori']	= 'write';
					//$this->view("course/nilai/write.php", $data);
				}
		
				$this->view("course/nilai/index.php", $data);
			break;
			
			case 'proses_nilai':
				//$this->write_proses_nilai($id,$jadwalid);
				$mpost 	= new model_course();		
				$mnilai = new model_nilai();
				
				$this->add_script('js/nilai/nilai.js');
				$this->add_script('js/nilai/excellentexport.js');
				
				$data['mkid']		= $id;
				$data['jadwal']		= $jadwalid;
				$data['kategori']	= 'proses';
				$data['nilai'] 		= $mnilai->get_nilai_proses('',$id,$jadwalid);
				$data['mhs'] 		= $mpost->read_mk_by_mhs("","",$id,$staff);
				
				$data['isproses_tblprosesmhs']	= $mnilai->get_is_proses_tblprosesmhs($id,$jadwalid);
				
				$prosesnilaimhs		= $mnilai->get_proses_nilai_mhs($id, $jadwalid);
				if(isset($prosesnilaimhs)){
					$data['nilaiprosesmhs']	  = $prosesnilaimhs;
					$data['action'] 		  = 'edit';
				}else{
					$data['nilaiprosesmhs'] = null;
					$data['action'] 	    = 'write';
				}
				$this->view("course/nilai/index.php", $data);
			break;
			default:
				$data['status']	= $mpost->get_statusmateri();
				$data['materi'] = $mpost->get_materi($id);
				$this->coms->add_script('js/jsall.js');
				$this->view("course/materi/index.php", $data);
			break;
			
		}
	}
	
	
	function file($id=NULL,$materiid=NULL){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		
		$data['post'] 		= $mpost->read_mk_by_dosen("","",$id,$staff);
		$data['materi'] 	= $mpost->get_materi($id);
		$data['pengampu'] 	= $mpost->get_pengampu($id);
		$data['status']		= $mpost->get_statusmateri();
		$data['jumlahmatbymk']	= $mpost->get_materibymk_count($id);
		$data['jadwal_id']      = $_POST['jadwal_id'];
		$data['komponen']	= $mpost->get_komponen_silabus();
		$data['posts']		= "";
		$data['materiid']	= $materiid;
		$data['role']	= $role;
		$data['mk']	= $id;
		
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/jsall.js');
		$this->add_script('js/save.js');
		$this->coms->add_script('js/submit.js');
		$this->add_script('js/nav.js');
		
		$this->view("course/file/write.php", $data);	
	}
	
	function tampilkan_materi()
	{
		$mfile = new model_file();
		$jenis_fileid = $_POST['jenis_file_id'];

		//echo "MATERI";
		$parent = $mfile->get_materi_by_kategori($jenis_fileid);
		echo "<option value='0'>Select Materi</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->mat_id."'>".$p->nama_materi."</option>";	
		}

	}
	
	function tampilkan_file()
	{
		$mfile = new model_file();
		$id = $_POST['jenis_file_id'];
		$matid = $_POST['materi_id'];
		$jadwalid = $_POST['jadwal_id'];
		
		$parentmmk = $mfile->get_file_by_kategori_jenis_materimk($id,$matid);
		$data ['post'] = $parentmmk;
		$data ['jadwal_id'] = $jadwalid;
		

		$this->view( 'course/file/viewselection.php', $data );
			
	}
	
	function save_file(){
	
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		
		if(isset($_POST['hidid'])){
			$mk = $mpost->mkditawarkan_id_md5($_POST['hidid']);
		}else{
			$mk = "";
		}
				
		$materiid 	= $_POST['cmbmateri'];
		$judul 		= $_POST['judul_file'];
		$keterangan = $_POST['keterangan_file'];
		if(isset($_POST['b_filepublish'])){
			$ispublish	= 1;
		}
		elseif(isset($_POST['b_filedraft'])){
			$ispublish	= 0;
		}
		$tgl_upload = date("Y-m-d H:i:s");
		$lastupdate	= date("Y-m-d H:i:s");
		$month 		= date('m');
		$year 		= date('Y');
		$uploadby 	= $this->coms->authenticatedUser->username;

		foreach($_FILES['uploads']['name'] as $id => $name) {
			$ext	= $this->get_extension($name);
			$seleksi_ext = $mpost->get_ext($ext);
			
			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
				
				switch(strToLower($jenis)){
					case 'image':
						$extpath = "image";
					break;
					case 'presentation':
						$extpath = "presentation";
					break;
					case 'document':
						$extpath = "document";
					break;
					case 'video':
						$extpath = "video";
					break;
					case 'spreadsheet':
						$extpath = "spreadsheet";
					break;
				}
				
				$file = NULL;
				$fileid = $mpost->id_file();
				$file_name = $fileid.".".$ext;
				$file_type=$_FILES["uploads"]["type"][$id] ; 
				$file_size=$_FILES["uploads"]["size"][$id] ; 
				$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
				$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
				
				// if (!file_exists($upload_dir)) {
					// mkdir($upload_dir, 0777, true);
					// chmod($upload_dir, 0777);
				// }
				$file_loc = $upload_dir_db . $file_name;
			
				if (($file_size <= $maxfilesize)){
				
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
								//rename($_FILES['uploads']['tmp_name'][$id], $upload_dir . $file_name);
								//------UPLOAD USING CURL----------------
								//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
								$url	     = $this->config->file_url;
								$filedata    = $_FILES["uploads"]['tmp_name'][$id];
								$filename    = $file_name;
								$filenamenew = $file_name;
								$filesize    = $file_size;
							
								$headers = array("Content-Type:multipart/form-data");
								$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
								$ch = curl_init();
								$options = array(
									CURLOPT_URL => $url,
									CURLOPT_HEADER => true,
									CURLOPT_POST => 1,
									CURLOPT_HTTPHEADER => $headers,
									CURLOPT_POSTFIELDS => $postfields,
									CURLOPT_INFILESIZE => $filesize,
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
									CURLOPT_SSL_VERIFYPEER => false,
									CURLOPT_SSL_VERIFYHOST => 2
								); // cURL options 
								curl_setopt_array($ch, $options);
								curl_exec($ch);
								if(!curl_errno($ch))
								{
									$info = curl_getinfo($ch);
									if ($info['http_code'] == 200)
									   $errmsg = "File uploaded successfully";
								}
								else
								{
									$errmsg = curl_error($ch);
								}
								curl_close($ch);
								//------UPLOAD USING CURL----------------
					}
						$datanya 	= Array('file_id'=>$fileid,
											'jenis_file_id'=>$jenis_file_id,
											'materi_id'=>$materiid,
											'mkditawarkan_id'=>$mk,
											'judul'=>$judul,
											'keterangan'=>$keterangan,
											'file_name'=>$file_name,
											'file_type'=>$file_type,
											'file_loc'=>$file_loc,
											'file_size'=>$file_size,
											'file_content'=>$file,
											'tgl_upload'=>$tgl_upload,
											'upload_by'=>$uploadby,
											'is_publish'=>$ispublish,
											'user_id'=>$user,
											'last_update'=>$lastupdate
											);
								$mpost->replace_file($datanya);			
				}
				
			}else{
				$result['error'] = "Unknown extension!";
				print "<script> alert('Unknown extension!'); </script>"; 
				$errors=1; 
				
				$this->index();
				exit();
			}
		}
		
	}
	
	function save(){
		
		$user	= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$mpost 	= new model_course();

		if(isset($_POST['id'])&& $_POST['id']!=''){
			$materiid = $_POST['id'];
		}else{
			//$id = "";
			$materiid = $mpost->get_id();
		}
		
		if(isset($_POST['hidid'])){
			$mk = $_POST['hidid'];
		}else{
			$mk = "";
		}
		
		if(isset($_POST['iscampus'])!=""){
			$campusonly	= $_POST['iscampus'];
		}else{
			$campusonly	= 0;
		}
		
		if(isset($_POST['cmbmateri'])){
			$parent	= $_POST['cmbmateri'];
		}else{
			$parent	= 0;
		}
		
		if(isset($_POST['b_savepublish'])){
			$ispublish	= 1;
		}
		elseif(isset($_POST['b_draft'])){
			$ispublish	= 0;
		}
		$ket = $_POST['keterangan_topik'];
		$url  = trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($_POST['judul']))))));
		
		$result = $this->upload_file($materiid,'icon'); //urutan hasilnya array($file_name,$file_type,$file_loc,$file_size,$file_content,$jenis_file_id)
		if($result == FALSE){
			echo "Upload icon gagal!";
		}
		elseif($result == "not_upload"){
			if(isset($_POST['uploads'])){
					$icon_loc			= $_POST['uploads'];
			}
			else $icon_loc = NULL;
		}
		else{
			for($j=0;$j<count($result);$j++){
				switch($j){
					case 0: $icon_name = $result [$j];
						    break;
					case 1: $icon_type = $result [$j];
						    break;
					case 2: $icon_loc = $result [$j];
						    break;
					case 3: $icon_size = $result [$j];
						    break;
					case 4: $icon_content = $result [$j];
						    break;
					case 5: //$jenis_icon_id = $result [$j];
						    break;
				}
			}
		}

		$save = $mpost->save($mk, $_POST['judul'], $ket, $ispublish, $_POST['urut'], $parent,$_POST['cmbstatus'], $campusonly,$icon_loc, $url, $materiid, $user, $lastupdate);
		// if( $save ) {
			// $result['status'] = "OK";
			// $result['modified'] = "Last saved on " . date('d/m/Y H:i:s');
		// } else {
			// $result['status'] = "NOK";
			// $result['error'] = $mpost->error;
		// }
		if($save!=FALSE){
			echo "Upload Materi Berhasil!";
		}
	}
	
	function topik($id=NULL,$matid=NULL){
		
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		
		$mpost 	= new model_course();
		
		$data['post'] 			= $mpost->read_mk_by_dosen("","",$id,$staff);
		if($matid){
			$data['materi'] 		= $mpost->get_materi($id, "", $matid);
		}else{
			$data['materi'] 		= $mpost->get_materi($id);
		}
		$data['pengampu'] 		= $mpost->get_pengampu($id);
		$data['status']			= $mpost->get_statusmateri();
		$data['posts']			= "";
		$data['jumlahsub']		= $mpost->get_submateri_count($matid);
		$data['jumlahmatbymk']	= $mpost->get_materibymk_count($id);
		
		$data['komponen']	    = $mpost->get_komponen_silabus();
		$data['matid']			= $matid;
		$data['role']			= $role;
		$data['jadwal_id']      = $_POST['jadwal_id'];
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/jsall.js');
		//$this->add_script('js/save.js');
		$this->add_script('js/nav.js');
		$this->coms->add_script('js/submit.js');
		
		
		$this->view("course/materi/edit.php", $data);
		
	}
	
	function read($type=NULL,$id=NULL, $materi=NULL, $str=NULL,$parent=NULL){	
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid; 

		$this->coms->add_script('js/log.js');
		$this->add_script('js/nav.js');
		
		$mpost 	= new model_course();
		
		$data['jadwal_id']      = $_POST['jadwal_id'];
		$data['materi'] 	    = $mpost->get_materi($id, "",$materi);
		
		if($type=='content'){
			$data['file'] 		= $mpost->get_file($id, $materi,"", $role);
			$data['role']		= $role;
			$this->view("course/read.php", $data);
		}else{
			$data['rfile'] 		= $mpost->get_file("", "",$id, $role);
			$data['role']		= $role;
			$this->view("course/read.php", $data);
		}
	}
	
	function edit($mk=NULL,$id=NULL){
		if( !$id ) {
			$this->redirect('module/content/course/');
			exit;
		}
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		$data['jadwal_id']      = $_POST['jadwal_id'];
		$data['materi'] 	= $mpost->get_materi($mk, "", $id);
		$data['status']			= $mpost->get_statusmateri();
		$data['user']		= $role;
		
		//$this->coms->add_script('js/jsall.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->add_script('js/nav.js');
		$this->coms->add_script('js/submit.js');
		
		$this->view("course/edit.php", $data);
	}
	
	function editfile($matid=NULL,$id=NULL){
		if( !$id ) {
			$this->redirect('module/content/course/');
			exit;
		}
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		$data['jadwal_id']      = $_POST['jadwal_id'];
		//$data['materi'] 	= $mpost->get_materi($mk, "", $id);
		$data['file'] 		= $mpost->get_file("", $matid, $id, $role);
		$data['user']		= $role;
		
		//$this->coms->add_script('js/jsall.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/submit.js');
		$this->add_script('js/nav.js');
		
		$this->view("course/editfile.php", $data);
	}
	
	function saved(){//save edit
		ob_start();
		$mpost 	= new model_course();
		if($_POST['edit']==0){
			$month = date('m');
			$year = date('Y');
			$materiid 		= $_POST['hidmatId'];
			$mkditawarkan_id	= $_POST['mkditawarkan'];
			$judul	= $_POST['judul'];
			$keterangan	= $_POST['keterangan'];
			
			if(isset($_POST['b_savepublish'])){
				$ispublish = 1;
			}
			elseif(isset($_POST['b_draft'])){
				$ispublish = 0;
			}
			
			$statmateri	= $_POST['statmateri'];
			$urut	= $_POST['urut'];
			$parent_id	= $_POST['parent'];
			$campusonly	= $_POST['campusonly'];
			$user		= $this->coms->authenticatedUser->id;
			$lastupdate	= date("Y-m-d H:i:s");
			$url  = trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($_POST['judul']))))));
			
			$mk	= $_POST['mk'];
			$id	= $_POST['id'];
			
			//icon start
			$result = $this->upload_file($materiid,'icon'); //urutan hasilnya array($file_name,$file_type,$file_loc,$file_size,$file_content,$jenis_file_id)
			if($result == FALSE){
				echo "Upload icon gagal!";
			}
			elseif($result == "not_upload"){
				$icon_loc			= $_POST['uploads'];
			}
			else{
				for($j=0;$j<count($result);$j++){
					switch($j){
						case 0: $icon_name = $result [$j];
							    break;
						case 1: $icon_type = $result [$j];
							    break;
						case 2: $icon_loc = $result [$j];
							    break;
						case 3: $icon_size = $result [$j];
							    break;
						case 4: $icon_content = $result [$j];
							    break;
						case 5: //$jenis_icon_id = $result [$j];
							    break;
					}
				}
			}
			
			if(isset($icon_loc)){
				$datanya 	= Array('materi_id'=>$materiid, 
										    'mkditawarkan_id'=>$mkditawarkan_id,
										    'page_link'=>$url,
										    'judul'=>$judul,
										    'keterangan'=>$keterangan,
										    'is_publish'=>$ispublish,
										    'status'=>$statmateri,
										    'urut'=>$urut,
										    'icon'=>$icon_loc,
										    'parent_id'=>$parent_id,
										    'is_campus'=>$campusonly,
										    'user_id'=>$user,
										    'last_update'=>$lastupdate
											);
				$mpost->replace_saved($datanya);
				echo "Update materi berhasil!";
				exit();
			}
			else{
				$datanya 	= Array('materi_id'=>$materiid, 
										    'mkditawarkan_id'=>$mkditawarkan_id,
										    'page_link'=>$url,
										    'judul'=>$judul,
										    'keterangan'=>$keterangan,
										    'is_publish'=>$ispublish,
										    'status'=>$statmateri,
										    'urut'=>$urut,
										    'parent_id'=>$parent_id,
										    'is_campus'=>$campusonly,
										    'user_id'=>$user,
										    'last_update'=>$lastupdate
											);
				$mpost->replace_saved($datanya);
				echo "Update materi berhasil!";
				exit();
			}
		}
		elseif($_POST['edit']==1){
			$file_id 			= $_POST['hidfileId'];
			
			$materi_id			= $_POST['materi_id'];
			$mkditawarkan_id	= $_POST['mkditawarkan_id'];
			
		
			$result = $this->upload_file($file_id,'',''); //urutan hasilnya array($file_name,$file_type,$file_loc,$file_size,$file_content,$jenis_file_id)
			if($result == FALSE){
				echo "Upload file gagal!";
			}
			elseif($result == "not_upload"){
				$file_name			= $_POST['file_name'];
				$file_type			= $_POST['file_type'];
				$file_loc			= $_POST['file_loc'];
				$file_size			= $_POST['file_size'];
				$file_content		= NULL;
				
				$tgl_upload			= $_POST['tgl_upload'];
				$upload_by			= $_POST['upload_by'];
				$jenis_file_id		= $_POST['jenis_file_id'];
			}
			else{
				for($j=0;$j<count($result);$j++){
					switch($j){
						case 0: $file_name = $result [$j];
							    break;
						case 1: $file_type = $result [$j];
							    break;
						case 2: $file_loc = $result [$j];
							    break;
						case 3: $file_size = $result [$j];
							    break;
						case 4: $file_content = $result [$j];
							    break;
						case 5: $jenis_file_id = $result [$j];
							    break;
					}
				}
				$tgl_upload = date("Y-m-d H:i:s");
				$upload_by 	= $this->coms->authenticatedUser->username;
			}
			
			if(isset($_POST['b_savepublish'])){
				$ispublish = 1;
			}
			elseif(isset($_POST['b_draft'])){
				$ispublish = 0;
			}
			
			$judul				= $_POST['judul'];
			$keterangan			= $_POST['keterangan'];
			$user				= $this->coms->authenticatedUser->id;
			$lastupdate			= date("Y-m-d H:i:s");
			
			// $id 				= $_POST['id'];
			if(($judul)){
				$datanya 	= Array(		'file_id'=>$file_id,
											'jenis_file_id'=>$jenis_file_id,
											'materi_id'=>$materi_id,
											'mkditawarkan_id'=>$mkditawarkan_id,
										    'judul'=>$judul,
										    'keterangan'=>$keterangan,
										    'file_name'=>$file_name,
										    'file_type'=>$file_type,
										    'file_loc'=>$file_loc,
										    'file_size'=>$file_size,
										    'file_content'=>$file_content,
										    'tgl_upload'=>$tgl_upload,
										    'upload_by'=>$upload_by,
										    'is_publish'=>$ispublish,
										    'user_id'=>$user,
										    'last_update'=>$lastupdate
											);
				$mpost->replace_file($datanya);
				echo "Update file berhasil!";
				exit();
			}
		}
		
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_title($file_name){
		$name = explode('.', $file_name);
		array_pop($name);
		return implode('.', $name);
	}
	
	function detailmateri($mk, $id, $p, $i=NULL, $jadwal_id=NULL){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mmateri = new model_course();
		$materi = $mmateri->get_materi($mk, $id);
		$par = $p; //ambil urutan looping parent
		
		if($materi){
			$i+=1;//jumlahnya nambah tiap manggil detail materi atau bila punya child
			$tab = str_repeat("&nbsp;&nbsp;&nbsp;", $i);//tab untuk child
			$a=1;
			foreach ($materi as $dt):
				$n=$par.$a;//urutan parent+self
				// $i++;
				// echo $i;
				?>
					<tr id="del_materi<?php echo $dt->materi_id ?>">							
												
						<?php $uri_location = $this->location('module/content/course/delete'); ?>
						<td style='min-width: 80px;'>        
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
									<a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'><?php echo $tab.$par.".".$a.". ".$dt->judul;?>&nbsp;&nbsp;</a>
										<ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
											<li>
												<a style='cursor:pointer' onclick="view_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-search'></i> View</a>	
												<?php
												if($role!='mahasiswa'){
												?>
												<a style='cursor:pointer' onclick="edit_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-pencil'></i> Edit</a>
												<a style='cursor:pointer' onclick="add_sub_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-plus'></i> Tambah Sub Materi</a>
												
											
												<a class='btn-edit-post'  href="#" onclick="doDelete(<?php echo $dt->materi_id ?>)"><i class='fa fa-trash-o'></i> Delete Materi</a>		
												<input type='hidden' class='deleted<?php echo $dt->materi_id ?>' value="<?php echo $dt->materi_id ?>" uri = "<?php echo $uri_location ?>"/>
												<?php
												}
												?>													
											</li>
										</ul>
								</li>
							</ul>
						</td>
					</tr>
				<?php
				$a++;
				$cont = $par.".".($a-1);
				echo $this->detailmateri($dt->mkditawarkan_id, $dt->materi_id,$cont, $i, $jadwal_id);
			endforeach;
		}
	}

	function delete($mat=NULL){
		$mmateri = new model_course();
		if (!$mat){
			$id = $_POST['delete_id'];
		}
		else $id=$mat;
		// echo $id;
		// exit();
		$getid = $mmateri->get_id_from_parent($id);
		$delpar = $mmateri->delete($id);
		if(isset($getid)){
			foreach ($getid as $c) {
				$matid=$c->materi_id;
				$this->delete($matid);
			}
		}
		echo "Data telah berhasil terhapus!!";
	}
	
	function silabus($id=NULL,$materiid=NULL){
		
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();
		
		$data['post'] 		= $mpost->read_mk_by_dosen("","",$id,$staff);
		$data['materi'] 	= $mpost->get_materi($id);
		$data['pengampu'] 	= $mpost->get_pengampu($id);
		$data['status']		= $mpost->get_statusmateri();
		$data['jumlahmatbymk']	= $mpost->get_materibymk_count($id);
		$data['komponen']	= $mpost->get_komponen_silabus();
		$data['posts']		= "";
		$data['materiid']	= $materiid;
		$data['role']	= $role;
		$data['jadwal_id']      = $_POST['jadwal_id'];
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('js/jsall.js');
		$this->add_script('js/save.js');
		$this->coms->add_script('js/submit/submit.js');
		$this->add_script('js/nav.js');
		
		$this->view("course/silabus.php", $data);	
	}
	
	function save_silabus(){
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$mpost 		= new model_course();
		
		$mkid				= $_POST['hidid'];
		$komponen			= $_POST['cmbkomponen'];
		$keterangan			= $_POST['silabus'];
		
		$silabusid		= $mpost->get_silabus_reg_id($mkid);
		$data_silabus 	= Array('silabus_id'=>$silabusid, 'mkditawarkan_id'=>$mkid);
		$mpost->replace_silabus($data_silabus);
		
		$detailid		= $mpost->get_silabus_detail($silabusid, $komponen);
		
		 if($keterangan){
				
			$data_detail 	= Array('detail_id'=>$detailid, 'silabus_id'=>$silabusid,
								 'komponen_id'=>$komponen, 'keterangan'=>$keterangan, 'user_id'=>$user, 'last_update'=>$lastupdate);
			$mpost->replace_detail($data_detail);
			
			echo "Update silabus berhasil!";
			exit();
		 }
	}	
	
	//========FUNGSI UPLOAD SINGLE FILE START========//
	//call upload_file(file_id,icon/not)
	function upload_file($file_id = NULL,$icon = NULL){
		ob_start();
		$mfile = new model_course();
		
		foreach ($_FILES['uploads'] as $id => $file) {
			if($id == 'error'){
				if($file!=0){
					$cekfile = 'error';
				}
				else $cekfile = 'sukses';
			}
		}
		
		if($cekfile!='error'){
				$month = date('m');
				$year = date('Y');
				$name = $_FILES['uploads']['name'];
				$ext	= $this->get_extension($name);
				$seleksi_ext = $mfile->get_ext($ext);
				if($seleksi_ext!=NULL){
						$jenis_file_id 	= $seleksi_ext->jenis_file_id;
						$jenis			= $seleksi_ext->keterangan;
						$maxfilesize	= $seleksi_ext->max_size;
						
						switch(strToLower($jenis)){
							case 'image':
								$extpath = "image";
							break;
							case 'presentation':
								$extpath = "presentation";
							break;
							case 'document':
								$extpath = "document";
							break;
							case 'video':
								$extpath = "video";
							break;
							case 'spreadsheet':
								$extpath = "spreadsheet";
							break;
						}
				}
				else{
						return FALSE;
				}
// 				
				
				$file_type=$_FILES["uploads"]["type"]; 
				$file_size=$_FILES["uploads"]["size"]; 
				if(isset($icon)&& $icon!=''){
					$allowed_ext = array('jpg','jpeg','png','gif');
					$ceknamafile = $mfile->cek_nama_icon($name);
					$upload_dir = 'assets/upload/icon/materi/';
					$upload_dir_db = 'upload/icon/materi/';
				}
				else{
					$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx', 'webm');
					$ceknamafile = $mfile->cek_nama_file($name);
					$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
					$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
				}
				
				
				// if (!file_exists($upload_dir)) {
							// mkdir($upload_dir, 0777, true);
							// chmod($upload_dir, 0777);
				// }
				$file_name = $file_id.".".$ext;
				$file_loc = $upload_dir_db . $file_name;
				if(!in_array($ext,$allowed_ext)){
						return FALSE;
				}
				elseif (($file_size <= $maxfilesize)){
						// //echo $file_size;				
						if($_SERVER['REQUEST_METHOD'] == 'POST') {
							//rename($_FILES['uploads']['tmp_name'], $upload_dir . $file_name);
							//------UPLOAD USING CURL----------------
							//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
							$url	     = $this->config->file_url;
							$filedata    = $_FILES["uploads"]['tmp_name'];
							$filename    = $file_name;
							$filenamenew = $file_name;
							$filesize    = $file_size;
						
							$headers = array("Content-Type:multipart/form-data");
							$postfields = array("filedata" => new CurlFile($filedata), "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
							$ch = curl_init();
							$options = array(
								CURLOPT_URL => $url,
								CURLOPT_HEADER => true,
								CURLOPT_POST => 1,
								CURLOPT_HTTPHEADER => $headers,
								CURLOPT_POSTFIELDS => $postfields,
								CURLOPT_INFILESIZE => $filesize,
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
								CURLOPT_SSL_VERIFYPEER => false,
								CURLOPT_SSL_VERIFYHOST => 2
							); // cURL options 
							curl_setopt_array($ch, $options);
							curl_exec($ch);
							if(!curl_errno($ch))
							{
								$info = curl_getinfo($ch);
								if ($info['http_code'] == 200)
								   $errmsg = "File uploaded successfully";
							}
							else
							{
								$errmsg = curl_error($ch);
							}
							curl_close($ch);
							//------UPLOAD USING CURL----------------
						}
				}
				$file_content		= NULL;
				$hasil = array($file_name,$file_type,$file_loc,$file_size,$file_content,$jenis_file_id); 
				return $hasil;
		}
		else{
			return "not_upload";
		}
	}
	//========FUNGSI UPLOAD FILE END========//
	
	//--------KOMPONEN NILAI---------------------------------------------------
	function cek_if_parent($komponen_id){
		$mnilai = new model_nilai();
		if(($mnilai->cek_if_parent($komponen_id))==TRUE){return TRUE;}
		else {return FALSE;}
	}
	
	function nilai($str=NULL,$id=NULL){
		
		if(isset($_POST['jadwal_id'])){
			$jadwalid  = $_POST['jadwal_id'];
		}else $jadwalid=null;
		
				
		switch($str){
			case 'write':
				$this->detail('edit_nilai', $id, $jadwalid);
				//$this->write_nilai($id,$jadwalid);
			break;
			case 'proses':
				$this->detail('proses_nilai', $id, $jadwalid);
				//$this->write_proses_nilai($id,$jadwalid);
			break;
		}
	}
	
	function write_nilai($id,$jadwalid){
		$mnilai = new model_nilai();
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/nilai/nilai.js');
		$this->add_script('js/nav.js');
		
		$data['mkid']		= $id;
		$data['jadwal']		= $jadwalid;
		$data['action'] 	= 'write';
		$prosesnilai		= $mnilai->get_proses_nilai($id, $jadwalid);
		
		if(isset($prosesnilai)){
			$data['posts']		= $mnilai->get_proses_nilai($id, $jadwalid);
			$data['kategori']	= 'edit';
			//$this->view("course/nilai/edit.php", $data);
		}else{
			$data['posts'] 		= $mnilai->get_komponen($id,'');
			$data['kategori']	= 'write';
			//$this->view("course/nilai/write.php", $data);
		}
	}
	
	function get_test(){
		$mnilai = new model_nilai();
		
		if(isset($_POST['mkid'])){
			$mkid  = $_POST['mkid'];
		}else $mkid=null;
		
		if(isset($_POST['jadwalid'])){
			$jadwalid  = $_POST['jadwalid'];
		}else $jadwalid=null;
		
		$test		= $mnilai->get_test($mkid, '', $jadwalid);
		
		if(isset($test)){
			$data = array();
			foreach($test as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			header('Content-type: application/json');
			echo json_encode($data);
		}
		else {
			echo ("error");
		}
	}

	function get_tugas(){
		$mnilai = new model_nilai();
		
		if(isset($_POST['jadwal'])){
			$jadwal  = $_POST['jadwal'];
		}else $jadwal=null;
		
		$tugas		= $mnilai->get_tugas($jadwal, '');
		
		if(isset($tugas)){
			$data = array();
			foreach($tugas as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($data,$arr);
			}
			header('Content-type: application/json');
			echo json_encode($data);
		}
		else {
			echo ("error");
		}
	}
	
	function delete_proses(){
		$mnilai = new model_nilai();
		
		if(isset($_POST['prosesid'])){
			$prosesid  = $_POST['prosesid'];
		}else $prosesid=null;
		
		if(($mnilai->delete_proses($prosesid))==TRUE){echo TRUE;}
		else {echo FALSE;}
	}
	
	function saveNilaiProsesToDB(){
		$mnilai 	= new model_nilai();
		
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
				
		$data		= $mnilai->get_mk_thn_kelas($_POST['mkid'], $_POST['jadwalid']);
		
		foreach ($data as $d) {
			$mkid 			= $d->mkditawarkan_id;
			$jadwalid  		= $d->jadwal_id;
			$thnakademik	= $d->tahun_akademik;
			$kelas			= $d->kelas;
		}
		
		$data1 	= Array(
					   'tahun_akademik'=>$thnakademik,
					   'mkditawarkan_id'=>$mkid,
					   'jadwal_id'=>$jadwalid,
					   'kelas'=>$kelas,
					   );
		
		$komponen 	= $_POST['komponen'];
		$percent  	= $_POST['percent'];
		if(isset($_POST['hidId'])&&(count($_POST['hidId'])!=0)){
			$proseshidid	= $_POST['hidId'];
		}
		$prosesid	= '';
		
		for ($i=0; $i < count($percent); $i++) { 			
			switch($_POST['type'][$i]){
				case 'proses-parent':
					$prosesid = $proseshidid[$i];
					// echo $prosesid." - ".$proseshidid[$i]." - ".$komponen[$i]." - ".$percent[$i]." - ".$_POST['type'][$i]."<br>";
					$mnilai->update_proses_nilai($proseshidid[$i], $komponen[$i], $percent[$i]);
				break;
				case 'proses-child':
					// echo $prosesid." - ".$proseshidid[$i]." - ".$komponen[$i]." - ".$percent[$i]." - ".$_POST['type'][$i]."<br>";
					$mnilai->update_proses_nilai($proseshidid[$i], $komponen[$i], $percent[$i]);
				break;
				case 'komponen':
					$prosesid = $mnilai->get_reg_number();
					$data2 	= Array(
								   'proses_id'=>$prosesid,
								   'keterangan'=>$komponen[$i],
								   'persentase_total'=>$percent[$i],
								   'user_id'=>$user,
								   'last_update'=>$lastupdate,
								   'is_proses'=>'0'
								   );
					$datanya = $data1 + $data2;
					$mnilai->replace_proses_nilai($datanya);	
				break;
				case 'test':
					// echo $prosesid." - ".$komponen[$i]." - ".$percent[$i]." - ".$_POST['type'][$i]."<br>";
					$data2 	= Array(
								   'proses_id'=>$mnilai->get_reg_number(),
								   'test_id'=>$komponen[$i],
								   'keterangan'=>$mnilai->get_testName($komponen[$i]),
								   'persentase_detail'=>$percent[$i],
								   'user_id'=>$user,
								   'last_update'=>$lastupdate,
								   'is_proses'=>'0',
								   'parent_id'=>$prosesid
								   );
					$datanya = $data1 + $data2;
					$mnilai->replace_proses_nilai($datanya);
				break;
				case 'tugas':
					// echo $prosesid." - ".$komponen[$i]." - ".$percent[$i]." - ".$_POST['type'][$i]."<br>";
					$data2 	= Array(
								   'proses_id'=>$mnilai->get_reg_number(),
								   'tugas_id'=>$komponen[$i],
								   'keterangan'=>$mnilai->get_tugasName($komponen[$i]),
								   'persentase_detail'=>$percent[$i],
								   'user_id'=>$user,
								   'last_update'=>$lastupdate,
								   'is_proses'=>'0',
								   'parent_id'=>$prosesid
								   );
					$datanya = $data1 + $data2;
					$mnilai->replace_proses_nilai($datanya);
				break;
			}

		}
		
	}

	function write_proses_nilai($id,$jadwalid){
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$staff	= $this->coms->authenticatedUser->staffid;
		$mhs	= $this->coms->authenticatedUser->mhsid;
		
		$mpost 	= new model_course();		
		$mnilai = new model_nilai();
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/nilai/nilai.js');
		
		$data['mkid']		= $id;
		$data['jadwal']		= $jadwalid;
		$data['kategori']	= 'proses';
		$data['nilai'] 		= $mnilai->get_nilai_proses('',$id,$jadwalid);
		$data['mhs'] 		= $mpost->read_mk_by_mhs("","",$id,$staff);
		
		$data['isproses_tblprosesmhs']	= $mnilai->get_is_proses_tblprosesmhs($id,$jadwalid);
		
		$prosesnilaimhs		= $mnilai->get_proses_nilai_mhs($id, $jadwalid);
		if(isset($prosesnilaimhs)){
			$data['nilaiprosesmhs']	  = $prosesnilaimhs;
			$data['action'] 		  = 'edit';
		}else{
			$data['nilaiprosesmhs'] = null;
			$data['action'] 	    = 'write';
		}
		
		//$this->view("course/nilai/proses-nilai/edit.php", $data);
	}
	
	function datanilai($mhsid, $mkid, $jadwal, $nilai, $i, $parent, $persentparent, $nilaiprosesmhs){
		$mnilai = new model_nilai();
		
		if(isset($nilaiprosesmhs)){//------EDIT-----------------------
			foreach ($nilaiprosesmhs as $np) {
				if($mhsid==$np->mhs_id){
					echo '<td>';
					echo '	<input name="skor[]" required="required" type="text" onkeypress="return isNumberKey(event)" class="form-control skor" value="'.$np->skor.'">';
					echo '  <span class="real-score">'.$np->skor.'</span>';
					echo '	<input name="nilai_id[]" required="required" type="hidden" class="form-control" value="'.$np->nilai_id.'">';
					echo '	<input name="proses_id[]" required="required" type="hidden" class="form-control" value="'.$np->proses_id.'">';
					echo '	<input name="persentase[]" required="required" type="hidden" class="form-control" value="'.$np->persentase.'">';
					echo '</td>';		
				}
			}
		}else{//--------WRITE NEW---------------------------
			$datanilai	= $mnilai->get_nilai($mkid,$jadwal, $mhsid, '');
			if(count($datanilai)!=0){
				for ($x=0; $x < $i; $x++) {
					$dn 	= $mnilai->get_nilai($mkid,$jadwal, $mhsid, $parent[$x]);
					
					if(isset($dn[0]->parent_id)){
						if($dn[0]->mhs_id) {
							if($mhsid==$dn[0]->mhs_id){
								echo '<td>';
								echo '	<input name="skor[]" required="required" type="text" onkeypress="return isNumberKey(event)" class="form-control skor" value="'.$dn[0]->skor.'">';
								echo '  <span class="real-score">'.$dn[0]->skor.'</span>';
							}
						}
					}else{
						echo '<td>';
						echo '	<input name="skor[]" required="required" type="text" onkeypress="return isNumberKey(event)" class="form-control skor" value="0">';
						echo '  <span class="real-score">0</span>';
					}
					echo '	<input name="proses_id[]" required="required" type="hidden" class="form-control" value="'.$parent[$x].'">';
					echo '	<input name="persentase[]" required="required" type="hidden" class="form-control" value="'.$persentparent[$x].'">';
					echo '</td>';
				}//------END FOR----------------------------------------------------
			}else{
				foreach ($nilai as $n){
					if(!$n->parent_id){
						echo '<td>';
						echo '	<input name="skor[]" required="required" type="text" onkeypress="return isNumberKey(event)" class="form-control skor" value="0">';
						echo '  <span class="real-score">0</span>';
						echo '	<input name="proses_id[]" required="required" type="hidden" class="form-control" value="'.$n->hid_id.'">';
						echo '	<input name="persentase[]" required="required" type="hidden" class="form-control" value="'.$n->persentase_total.'">';
						echo '</td>';
					}
				}
			}//---else-------
		}
	}
	
	function saveNilaiProsesMhsToDB(){
		$mnilai 	= new model_nilai();
		
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$action		= $_POST['action'];
		
		$data		= $mnilai->get_mk_thn_kelas($_POST['mkid'], $_POST['jadwalid']);
		
		foreach ($data as $d) {
			$mkid 			= $d->mkditawarkan_id;
			$jadwalid  		= $d->jadwal_id;
			$thnakademik	= $d->tahun_akademik;
			$kelas			= $d->kelas;
		}
		
		$data1 	= Array(
					   'tahun_akademik'=>$thnakademik,
					   'mkditawarkan_id'=>$mkid,
					   'jadwal_id'=>$jadwalid,
					   'kelas'=>$kelas,
					   );
		
		$mhsid 			= $_POST['mhsid'];
		$krsid 			= $_POST['krsid'];
		$skor 			= $_POST['skor'];
		$prosesidparent	= $_POST['proses_id-parent'];
		$prosesid 		= $_POST['proses_id'];
		$persentase 	= $_POST['persentase'];
		
		$a = 0;
		if($action=='write'){
			for($i=0;$i<count($mhsid);$i++){
				for($x=0;$x<count($prosesidparent);$x++){
					// echo $mhsid[$i]." - ".$krsid[$i]." - ".$skor[$a]." - ".$prosesid[$a]." - ".$persentase[$a++]."<br>";
					$nilaiid	= $mnilai->get_nilaiid();
					$data2		= Array(
									    'nilai_id'=>$nilaiid,
									    'mahasiswa_id'=>$mhsid[$i],
									    'proses_id'=>$prosesid[$a],
									    'krs_id'=>$krsid[$i],
									    'skor'=>$skor[$a],
									    'persentase'=>$persentase[$a++],
									    'user_id'=>$user,
									    'last_update'=>$lastupdate,
									    'is_proses'=>'0'
									   );
					$datanya	= $data1 + $data2;
					$mnilai->replace_proses_mhs_nilai($datanya);
					$mnilai->update_proses_tbl_nilai_proses($mkid, $jadwalid);
				}
			}
		}else{
			$nilaiid = $_POST['nilai_id'];
			for($i=0;$i<count($mhsid);$i++){
				for($x=0;$x<count($prosesidparent);$x++){
					// echo $nilaiid[$a]." - ".$mhsid[$i]." - ".$prosesid[$a]." - ".$thnakademik." - ".$mkid." - ".$jadwalid." - ";
					// echo $krsid[$i]." - ".$kelas." - ".$skor[$a]." - ".$persentase[$a++]."<br>";
					$mnilai->update_proses_mhs_nilai($nilaiid[$a], $skor[$a++]);
				}
			}
		}
	}
	
	function import_file(){
		include 'excel_reader/reader.php';
		$excel = new Spreadsheet_Excel_Reader();
		$mnilai 	= new model_nilai();
		$filename	= $_FILES['file']['tmp_name'];
		$excel->read($filename);
		$x=1;
	    while($x<=$excel->sheets[0]['numRows']) {
	      $y=1;
	      while($y<=$excel->sheets[0]['numCols']) {
	        echo $cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y]."," : '';
	        $y++;
	      } 
	      $x++;
	    }
	}
	
	function proses_nilai_krs(){
		
		if(isset($_POST['mk_id'])){
			$mkid	= $_POST['mk_id'];
		}else{
			$mkid	= "";
		}
		
		if(isset($_POST['jadwal_id'])){
			$jadwalid 	= $_POST['jadwal_id'];
		}else{
			$jadwalid	= "";
		}
		
		$mnilai 	= new model_nilai();
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$krs		= $mnilai->get_nilai_akhir($mkid,$jadwalid);
		$i=0;
		foreach ($krs as $k) {
			$krsid[]		= $k->krs_id;
			$mkid			= $k->mkditawarkan_id;
			$jadwalid		= $k->jadwal_id;
			$mhsid[]		= $k->mahasiswa_id;
			$skor[]			= $k->skor;
			$inf_huruf[]	= $this->convertTohuruf($k->skor);
			$inf_bobot[]	= $this->convertTobobot($k->skor);
			$i++;
		}
		
		for ($x=0; $x < $i; $x++) { 
			// echo $krsid[$x]." - ".$mhsid[$x]." - ".$mkid." - ".$jadwalid." - ".$skor[$x]." - ".$inf_bobot[$x]." - ".$inf_huruf[$x]." - ".$user." - ".$lastupdate."<br>";
			$mnilai->update_nilai_krs($krsid[$x], $mhsid[$x], $skor[$x], $inf_bobot[$x], $inf_huruf[$x], $user, $lastupdate);
		}
		$mnilai->update_proses_tbl_nilai_proses_mhs($mkid, $jadwalid);
		//$this->view("course/nilai/proses-nilai/edit.php", $data);
		
	}
	
	function convertTohuruf($angka){
		if($angka > 80){
			$huruf = 'A';
		}
		elseif($angka > 75 && $angka <= 80){ 
			$huruf = 'B+';
		}
		elseif($angka > 69 && $angka <= 75){
			$huruf = 'B';
		}
		elseif($angka > 60 && $angka <= 69){
			$huruf = 'C+';
		}
		elseif($angka > 55 && $angka <= 60){
			$huruf = 'C';
		}
		elseif($angka > 50 && $angka <= 55){
			$huruf = 'D+';
		}
		elseif($angka > 44 && $angka <= 50){
			$huruf = 'D';
		}
		elseif($angka <= 44){
			$huruf = 'E';
		}
		return $huruf;
	}
	
	function convertTobobot($angka){
		if($angka > 80){
			$bobot = '4';
		}
		elseif($angka > 75 && $angka <= 80){ 
			$bobot = '3.5';
		}
		elseif($angka > 69 && $angka <= 75){
			$bobot = '3';
		}
		elseif($angka > 60 && $angka <= 69){
			$bobot = '2.5';
		}
		elseif($angka > 55 && $angka <= 60){
			$bobot = '2';
		}
		elseif($angka > 50 && $angka <= 55){
			$bobot = '1.5';
		}
		elseif($angka > 44 && $angka <= 50){
			$bobot = '1';
		}
		elseif($angka <= 44){
			$bobot = '0';
		}
		return $bobot;
	}
	
}
?>