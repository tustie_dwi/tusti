<?php
class content_page extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}

	function write() {
		
		//var_dump($this->coms);
		$data['user'] 	= $this->coms->session->get("user");
		
		$mcontent = new model_content();
		
		$data['category'] = $mcontent->get_category();
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
				
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->add_script('js/edit.js');			
				
		$this->view('edit.php', $data);
	}
	
	function create($what, $value = NULL) {
		switch($what) {
			case 'tag':
				$mtags = new model_posts_tags();
				if( $mtags->insert($value) )
					echo "OK"; else echo "NOK";
				break;
			case 'category':
				$mcats = new model_posts_categories();
				$value = $_POST['category'];
				if( $cat = $mcats->insert($value) ) 
					echo '<label class="checkbox span3">
                    	<input type="checkbox" name="content_categories[]" checked="checked" value="'.$cat->tag.'" />
                        '.$cat->name.'
                    </label>'; 
				else echo "NOK";
				break;
		}
	}
	
	function getAllTags(){
		$mtags = new model_posts_tags();
		$tags = $mtags->getAllTags();
		$atags = array();
		foreach($tags as $tag)
			array_push( $atags, $tag->name );
		echo json_encode($atags);
	}
	
	function getAllCategories(){
		$mcats = new model_posts_categories();
		$cats = $mcats->getAllCategories();
		$acats = array();
		foreach($cats as $cat)
			array_push( $acats, $cat->name );
		echo json_encode($acats);
	}
	
	function save(){
		$mpost = new model_content();
				
		$id = $_POST['id'];
		if(!$id) $id = NULL;
				
		//$content_page = $_POST['content_page'];
		$content_title = $_POST['content_title'];
		$content_page  = trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($content_title))))));

		$content_date = implode("-", array_reverse(explode("/",trim($_POST['content_date']))));
		$content_date .= " " . trim($_POST['content_time']);
		
		$content_content = $_POST['content_content'];
		
		if(isset($_POST['b_pub'])){
			$content_status = 'published';
		}else{
			$content_status = 'draft';
		}
		
		$content_parent		= $_POST['content_parent'];
		$content_urut		= $_POST['content_urut'];
		$content_author_id 	= $_POST['content_author_id'];
		$content_category	= $_POST['content_category'];
		if(isset($_POST['comment'])){
			$content_comment= $_POST['comment'];
		}else{
			$content_comment= 0;
		}
		
		
		if($_FILES['files']['name']){
							
				$filename = stripslashes(@$_FILES['files']['name']); 
				
				
				
				$newname="assets/upload/thumb/".$filename;  
				$copied = move_uploaded_file($_FILES['files']['tmp_name'], $newname); 
				
				
				$dst_x = 0;
				$dst_y = 0;
				$src_x = 100; // Crop Start X
				$src_y = 100; // Crop Srart Y
				$dst_w = 150; // Thumb width
				
				$src_image = imagecreatefromstring(file_get_contents($newname));
				$src_w = ImageSX($src_image);
				$src_h = ImageSY($src_image);
				
				$ratio = $dst_w / $src_w;
				$dst_h = $src_h * $ratio;
				
				/*$dst_h = 160; // Thumb height*/
				//$src_w = 220; // $src_x + $dst_w
				//$src_h = 260; // $src_y + $dst_h

				$dst_image = imagecreatetruecolor($dst_w,$dst_h);				
	  
				$img = file_get_contents($newname);
				//imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
				imagecopyresampled($dst_image, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);
				imagejpeg($dst_image, "assets/upload/thumb/".$filename);
								
				if (!$copied){  
					print "<script> alert('Copy unsuccessfull!'); </script>"; 
					$errors=1; 
					
					$this->redirect('module/content/page');
					exit();
					
				}else{
					$errors=0;
					print "<script> alert('Copy successfull!'); </script>"; 
					
							
				}					
			}else{
				if(isset($_POST['hidimg'])){
					$newname = $_POST['hidimg'];
				}else{
					$newname = "";
				}
			}
			
			
		
		$save = $mpost->save($content_page, $content_title, $content_date, $content_content, 
							$content_status, $content_author_id, $content_category,$content_parent,$content_urut, $id, $newname, $content_comment);
		$result = array();
							
		/*if( $save ) {
			$result['status'] = "OK";
			$result['id'] = $mpost->id;
			$result['modified'] = "Last saved on " . date('d/m/Y H:i:s', strtotime($mpost->modified));
		} else {
			$result['status'] = "NOK";
			$result['id'] = $mpost->id;
			$result['error'] = $mpost->error;
		}
		
		echo json_encode($result);
		*/
		
		$this->redirect('module/content/page');
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 100){
	
		if(isset($_POST['b_pub'])||(isset($_POST['b_draft']))){
			$this->save();
			exit();
		}

		$mpost = new model_content();
		
		switch($by) {
			case 'all':		
				$data['posts'] = $mpost->getList($page, $perpage);
				break;
			case 'cat':
				$data['posts'] = $mpost->getListByCat($keyword, $page, $perpage);
				break;
			case 'tag':
				$data['posts'] = $mpost->getListByTag($keyword, $page, $perpage);
				break;				
			case 'search':
				$data['posts'] = $mpost->getListBySearch($keyword, $page, $perpage);
				break;
		}

		$data['page'] = $page;
		$data['by'] = $by;
		$data['keyword'] = $keyword;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
		$this->add_script('js/index.js');
		
		$this->view("index.php", $data);
			
	}
	
	function delete() {
		$id = $_POST['id'];
		$mpost = new model_content();
		$result = array();
		if($mpost->delete($id))
			$result['status'] = "OK";
		else {
			$result['status'] = "NOK";
			$result['error'] = $mpost->error;
		}
		echo json_encode($result);
	}
	
	public function discard() {
		$this->delete();
	}
	
	function edit($contentid = NULL) {
		
		if( !$contentid ) {
			$this->redirect('content');
			exit;
		}
		
		$mcontent = new model_content();
				
		$data['user'] 		= $this->coms->session->get("user");		
		$data['content']	= $mcontent->getContent($contentid);
		$data['category'] 	= $mcontent->get_category();
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_script('ckeditor/ckeditor.js');
		
		$this->add_script('js/edit.js');
		
		$this->view('edit.php', $data);
	}
	
	public function tags() {
		
		$mtags = new model_posts_tags();
		$mcats = new model_posts_categories();

		$data['tags'] = $mtags->getAllTags();
		$data['categories'] = $mcats->getAllCategories();
		
		$this->add_script('js/posts/tags.js');
		
		$this->view('posts/tags.php', $data);
	}
	
	public function deltag() {
		
		$tagname = $_POST['tagname'];
		$mtags = new model_posts_tags();

		$result = array();
		if( $mtags->delete( $tagname ) )
			$result['status'] = "OK";
		else {
			$result['status'] = "NOK";
			$result['error'] = $mtags->error;
		}
		echo json_encode($result);
	
	}
	
	public function delcat() {
		
		$catname = $_POST['catname'];
		$mtags = new model_posts_categories();

		$result = array();
		if( $mtags->delete( $catname ) )
			$result['status'] = "OK";
		else {
			$result['status'] = "NOK";
			$result['error'] = $mtags->error;
		}
		echo json_encode($result);
	
	}
	
	//======================comment=======================//
	function comments(){
		
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
	
			$mpage = new model_page();
			
			if(isset($_POST['category_index']) && $_POST['category_index']!="0"){
				$category 				= $_POST['category_index'];
				$data['category_index'] = $category;
				$data['comment'] 		= $mpage->read_comment("","",$category);
			}else $data['comment'] 		= "";
						
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->add_script('js/comment/comment.js');
			
			$this->view('comment/index.php', $data);
	}
	
	function delete_comments(){
		
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		
		
			$id = $_POST['id'];
			$mpage = new model_page();
			$hapus = $mpage->delete_comments($id);
			if($hapus == TRUE){
				echo "Komen berhasil dihapus!";
				exit();
			}
			else {
				echo "Komen gagal dihapus!";
				exit();
			}
		
		
	}
	
	function update_status_comments(){
		
		$role	= $this->coms->authenticatedUser->role;
		$user	= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
			$id = $_POST['id'];
			$status = $_POST['status_id'];
			$mpage = new model_page();
			$update = $mpage->update_status_comments($id,$status,$lastupdate);
			if($update == TRUE){
				echo "Komen berhasil di-update!";
				exit();
			}
			else {
				echo "Komen gagal di-update!";
				exit();
			}
		
		
	}
	
}
?>