<?php
class content_conf extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}
	
		
	function content(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_json_content($str);
		
		$tags		= array();
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$tags['results'] = $return_arr; 
					
		$json_response = json_encode($tags);

		echo $json_response;
	
	}
	
	function mhs(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_mahasiswa($str);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function dosen(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_dosen($str);
		
		$return_arr = array();
		
		foreach($data as $row){
			foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
			
		$json_response = json_encode($return_arr);

		# Optionally: Wrap the response in a callback function for JSONP cross-domain support
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		# Return the response
		echo $json_response;
	}
	
	function staff(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf 	= new model_conf();
		$data	= $mconf->get_staff($str);
		
		$tags		= array();
		$return_arr = array();
		
		foreach($data as $row){
			$arr= $row->tag;
			/*foreach ($row as $key => $value) {
				 $arr[$key] = $value;				 
			}*/
			array_push($return_arr,$arr);
		}
		
		$tags['tags'] = $return_arr; 
					
		$json_response = json_encode($return_arr);

		echo $json_response;
	}
	
}
?>