<?php
if($role!="mahasiswa"){
$this->head();

if($posts){
	$header		= "Edit Tugas";
	foreach ($posts as $dt):
		$tugas 		= $dt->tugasid;
		$id 		= $dt->tugas_id;
		$jadwalid 	= $dt->jadwalid;
		//$idjadwal 	= $dt->jadwal_id;
		$materiid 	= $dt->materiid;
		//$idmateri	= $dt->materi_id;
		$judul	 	= $dt->judul;
		$instruksi 	= $dt->instruksi;
		$keterangan = $dt->keterangan;
		$tgl_mulai 	= $dt->tgl_mulai;
		$tgl_selesai= $dt->tgl_selesai;
	endforeach;
	$frmact 	= $this->location('module/content/tugas/save');		
	
}else{
	$tugas		= "";
	$header		= "Write New Tugas";
	$id			="";
	$materiid	="";
	//$idjadwal	= "";
	//$idmateri	= "";
	$jadwalid	= $jadwal_id;
	$judul		="";
	$instruksi	="";
	$keterangan	="";
	$tgl_mulai	="";
	$tgl_selesai="";
	$newtugas	= 1;
	$frmact 	= $this->location('module/content/tugas/save');		
}


?>

<h2 class="title-page">Tugas</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
	   <li><a href="<?php echo $this->location('module/content/tugas/'); ?>">Tugas & Test</a></li>
	  <li class="active">Write</li>
	</ol>
	<div class="breadcrumb-more-action">
		<?php if($posts){ ?>
		<a onclick="newtugas('<?php echo $jadwalid ?>','<?php echo $mk ?>')" class="btn btn-primary pull-right" style="margin:0px 5px"><i class="fa fa-pencil"></i> New </a>
		<?php } ?>
		<a onclick="tugas_('<?php echo $jadwalid ?>','<?php echo $mk ?>')" class="btn btn-default pull-right"><i class="fa fa-bars"></i> List</a>
		
	</div>
	
	<div class="row">   
         <div class="col-md-12"> 	
			<form method=post id="upload-tugas-form" class="form-horizontal">
				 
				 <div class="form-group">
					<label class="col-sm-2 control-label">Jadwal</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php $uri_parent = $this->location('module/content/tugas/tampilkan_materi'); ?>
							<select class="form-control e9" name="jadwal" disabled="disabled" id="select_jadwal"  data-uri="<?php echo $uri_parent ?>">
								<option value='0'>Select Jadwal</option>
							<?php 
								foreach($jadwal as $dt):
									echo "<option class='sub_".$dt->jadwal_id."' value='".$dt->jadwal_id."'";
									if($jadwalid == $dt->jadwal_id){
										echo "selected";
									}
									echo ">".$dt->jadwal."</option>";
								endforeach;
							?>					
							</select>
							<?php if($jadwalid) {?>
								<input type="hidden" class="form-control" id="jadwal" name='jadwal' value="<?php if(isset($jadwalid)) echo $jadwalid; ?>">
							<?php } ?>
						</div>
					</div>
				</div>
				 
				 <?php if(!isset($tgs_mhs)){ ?>
				 <div class="form-group">
					<label class="col-sm-2 control-label">Materi</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php echo '<select id="select_materi" class="form-control e9" name="materi" >'; ?>
								<option value='0'>Select Materi</option>
							<?php 
								foreach($materi as $dt):
									echo "<option class='sub_".$dt->materi_id."' value='".$dt->materi_id."'";
									if($materiid == $dt->materi_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								endforeach;
							?>					
						</select>
						</div>
					</div>
				</div>
				<?php } else {
					    if($materiid){?>
							<input type="hidden" class="form-control" id="select_materi" name='materi' value="<?php if(isset($materiid)) echo $materiid; ?>">
				<?php   } 
					  } ?>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Judul</label>
					<div class="controls">
						<div class="col-sm-10">
							<input type="text" class="form-control" id="judul" name='judul' value="<?php if(isset($judul)) echo $judul; ?>">
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Instruksi</label>
					<div class="controls">
						<div class="col-sm-10">
							<textarea class="form-control ckeditor" id="instruksi" name='instruksi' ><?php if(isset($instruksi)) echo $instruksi; ?></textarea>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
							<textarea class="form-control ckeditor" id="keterangan" name='keterangan' ><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
						</div>
					</div>
				</div>
				
				<div class="form-group">	
					 <label class="col-sm-2 control-label">Tanggal Pengerjaan</label>
					 <div class="col-sm-10">
						<input type="text" id="tanggalmulai" name="tanggalmulai"  class="form_datetime form-control" value="<?php if(isset($tgl_mulai))echo $tgl_mulai;?>"> 
						sampai 
						<input type="text" id="tanggalselesai" name="tanggalselesai" onchange='validate_tanggal()' class="form_datetime form-control" value="<?php if(isset($tgl_selesai))echo $tgl_selesai;?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="hidden" name="hidId" value="<?php if(isset($tugas)) echo $tugas;?>">
						<input type="hidden" name="hidtmp" value="<?php if(isset($id)) echo $id;?>">
						<input type="hidden" name="newtugas" value="<?php if(isset($newtugas)) echo $newtugas;?>">
						<input type="submit" name="b_jenis" onclick="submit_tugas()" value=" Save & Publish " class="btn btn-primary">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
<?php
$this->foot();
}
?>