<?php 


$now = new DateTime();
$time = $now->format('Y-m-d H:i:s');
//echo "<em class='pull-right'>".$time."</em><br>";
$jdwl = $jadwal_id;

?>
 <div class="row">
	<h2 class="title-page">Matakuliah</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
	</ol>
	
	<legend>
	<?php if($role!="mahasiswa"){ ?>
    	<a onclick="newtugas('<?php echo $jadwal ?>','<?php echo $mk ?>');" class="btn btn-primary pull-right" style="margin:0px 5px">
    		<i class="fa fa-pencil"></i> New Tugas</a>
    <?php } ?>
    Tugas List
	</legend>
		
	<?php
			
	 if( isset($post) ) :	?>
		<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>Judul Tugas</th>
                        <th>&nbsp;</th>
						<th>Last Modified</th>
						<?php if($role!="mahasiswa"){ ?>
						<th>Act</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
			
			<?php 
				$i = 1;
				if($posts > 0){
					foreach ($posts as $dt): 
			?>
				<tr valign=top>
					<td>
						<?php echo "<a class='btn-edit-post' href=".$this->location('module/content/tugas/detail/'.$dt->tugasid.'/'.$dt->mk.'/'.$dt->jadwalid).">
						<span class='text text-default'><strong>".$dt->judul."</span></strong></a>" ?>
							<?php
							echo "<br><small>".$dt->materi."</small>";
							
							
							echo "<br><span class='fa fa-clock-o'></span> ".date("M d, Y h:m",strtotime($dt->tgl_mulai))." - ".date("M d, Y h:m",strtotime($dt->tgl_selesai))."";
 							?>
					</td>
                    <td>
                    <?php
					if(strtotime($dt->tgl_selesai) > time() && strtotime($dt->tgl_mulai) < time()){
								echo "<small><span class='label label-success'>Open</span></small>";
							}
							elseif(strtotime($dt->tgl_mulai) > time()){
								echo "<small><span class='label label-default'>Not Started Yet</span></small>";
							}
							else {
								echo "<small><span class='label label-danger'>Closed</span></small>";
							}
					?>
                    </td>
					<td>
						<?php echo str_replace('-', '/',$dt->last_update) ?><br>
							<small> by :
							<em style='color : orange'><?php echo $dt->user ?></em><br>
							</small>
					</td>
					<?php if($user!="mahasiswa" || $user=="dosen"){ ?>
					<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a style='cursor:pointer' class='btn-edit-post' onclick="edittugas('<?php echo $jdwl ?>','<?php echo $mk ?>','<?php echo $dt->tugasid ?>')"  ><i class='fa fa-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td>
					<?php } ?>
				</tr>
		<?php 
		$i++;
		endforeach; 
		} ?>
		</tbody></table>
		
		
		
	 <?php else: ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</div>
	<?php

?>