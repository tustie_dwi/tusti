<?php
if($posts){
	foreach ($posts as $p) {
	?>
		
			<div class="form-group">
				<div class="controls">
					<textarea class="form-control ckeditor" id="catatan" name='catatan' ><?php if(isset($p->catatan)) echo $p->catatan; ?></textarea><br>
					<a onclick="doDownloadTugas('<?php echo $p->fileid ?>')" href="<?php echo $this->location('module/akademik/file/download/'.$p->file_name); ?>" class="btn btn-default">Download This File</a>
				</div>
			</div>
			<form method=post id="upload-skor-mhs" class="form-horizontal">
				<div class="form-group">
					<div class="controls">
						<div class="col-sm-6">
							<input type="text" name="skor" id="skor" class="form-control" value="<?php if(isset($p->total_skor)){ echo $p->total_skor; }else{ echo "0"; } ?>" />
						</div>
					</div>
				</div>
				<?php if($role!="mahasiswa") { ?>
				<div class="form-group">
					<div class="controls">
						<div class="col-sm-10">
							<input type="hidden" id="upload_id" name="upload_id" value="<?php if(isset($p->upload)) echo $p->upload;?>">
							<input type="submit" name="b_jenis" onclick="submit_skor_mhs()" value="Submit" class="btn btn-primary">
						</div>
					</div>
				</div>
				<?php } ?>
			</form>
		
	<?php 
	} 
}
 ?>