<?php

if(strtotime(date("Y-m-d H:i")) > strtotime($tgl)){
	?>
	<div class="alert alert-danger">Ma'af batas waktu pengumpulan tugas sudah habis. Pengumpulan tugas terakhir adalah <span class="label label-danger"><?php echo date("F d, Y H:i", strtotime($tgl)); ?></span>Silahkan hubungi dosen Anda untuk informasi lebih lanjut</div>
	<?php
}else{
?>
<form method=post id="upload-tugas-mhs" class="form-horizontal">
	<div class="form-group">
		<div class="controls">
			<div class="col-sm-12">
				<textarea class="form-control ckeditor" id="catatan" name='catatan' ><?php if(isset($catatan)) echo $catatan; ?></textarea>
			</div>
		</div>
	</div>
	<div class="form-group">
			<div class="controls col-sm-12">									
				<input type="file" class="form-control" name="uploads[]" id="uploads" multiple>									
			</div>
	</div>
	<div class="form-group">
		<div class="controls">
			<div class="col-sm-10">
				<input type="hidden" name="tugasid" value="<?php if(isset($tugas)) echo $tugas;?>">
				<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
				<input type="hidden" name="mhsid" value="<?php echo $mhsid;?>">
				<input type="hidden" name="materiid" value="<?php if(isset($materiid)) echo $materiid;?>">
				<input type="submit" name="b_jenis" onclick="submit_tugas_mhs()" value="Submit" class="btn btn-primary">
			</div>
		</div>
	</div>
</form>
<?php } ?>