	
	<?php if(isset($status) and $status) : ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $statusmsg; ?>
	</div>
	<?php endif; ?>
		
	<?php
			
	 if( isset($posts) ) :	?>
		<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th width="20%">Modified</th>
						<th>&nbsp;</th>
						<th width="15%">Nilai</th>
					</tr>
				</thead>
				<tbody>
			
			<?php 
				$i = 1;
				if($posts){
					foreach ($posts as $dt){
			?>
					<tr>
						<td>
							<i class="fa fa-clock-o"></i> <?php echo $dt->tgl_upload; ?>
						</td>
						<td>
							<?php 
							if($role!='mahasiswa'){
								echo "<a style='cursor:pointer' onclick=nilai('".$jadwalid."','".$mk."','".$tugasid."','".$dt->upload."')><span class='text text-default'><b>".$dt->nama_mhs."</b></span></a>";
							}else{
								echo "<b>".$dt->nama_mhs."</b>";
							}
							echo "&nbsp;<code>".$dt->nim."</code>" ; 
								 ?>
						</td>						
						<td>
							<?php if(isset($dt->total_skor)){
								echo $dt->total_skor;
								}
								else echo "*) Belum Dinilai" ?>
						</td>						
					</tr>
				<?php 
				  $i++;
				  }
				}
				 ?>
			</tbody>
		</table>
		
	 <?php else: ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
