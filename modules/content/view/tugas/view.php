	<?php
	if($tugas){			
		foreach($tugas as $dt):
			?>
			<div class="head-title-box">
			<h3><?php echo $dt->judul;?></h3>
				<span class="option-menu">
					<?php if($role!="mahasiswa"){ ?>
						<a onclick="edittugas('<?php echo $jadwalid; ?>','<?php echo $mk; ?>','<?php echo $tugasid; ?>')"  title="Edit Tugas" href="#"><i class="fa fa-pencil"></i></a>
					<?php } ?>
				</span>
			</div>
			
			<blockquote><?php echo $dt->instruksi; ?>
			<?php if($dt->keterangan){ echo "<small>Keterangan</small>". $dt->keterangan;} ?></blockquote>
			<small>Schedule</small> <i class="fa fa-clock-o"></i> <?php echo date("M d, Y H:i", strtotime($dt->tgl_mulai)); ?> - <?php echo date("M d, Y H:i", strtotime($dt->tgl_selesai)); ?>
            
            <div class="row">
			<div class="col-md-10">
			<h4 class="no-full">Daftar Pengumpulan Tugas <small><?php echo $dt->judul;?></small></h4>
			<?php	
			$data['tgl']	= $dt->tgl_selesai;
			if($role!="mahasiswa"){
				if($jenis){
					$data['posts'] = $mtugas->read_tugas_mhs("",$jenis);
					?></div>
                    <div class="col-md-2">
					<a class="btn btn-gray pull-right with-margin" onclick="vw_tugas_('<?php echo $jadwalid; ?>','<?php echo $mk; ?>','<?php echo $tugasid; ?>');" ><i class="fa fa-search"></i> Lihat Tugas</a>
                    </div>
                    </div>
					<?php
					$this->view("tugas/nilai.php", $data);
				}else{
					$data['posts'] = $mtugas->read_tugas_mhs($tugasid);
					?> 
                    </div>
                    </div>
                    <?php
					$this->view("tugas/list_mhs_upload.php", $data);
				}
			}else{
				$data['mhsid']		= $mhs;
				$data['tugas']		= $dt->tugas_id;
				$data['mkid']		= $dt->mkditawarkan_id;
				$data['materiid']	= $dt->materi_id;
				
				$data['posts'] = $mtugas->read_tugas_mhs($tugasid, "", $mhs);
				
				if($data['posts']){
					if($jenis){
						$data['posts'] = $mtugas->read_tugas_mhs("",$jenis, $mhs);
						?></div>
                        <div class="col-md-2">
						<a class="btn btn-gray pull-right with-margin" onclick="vw_tugas_('<?php echo $jadwalid; ?>','<?php echo $mk; ?>','<?php echo $tugasid; ?>');" ><i class="fa fa-search"></i> Lihat Tugas</a>
                        
                    </div>
                    </div>
						<?php
						$this->view("tugas/nilai.php", $data);
					}else{
						?> 
                    </div>
                    </div>
                    <?php
						$this->view("tugas/list_mhs_upload.php", $data);
					}
				}else{
					?> 
                    </div>
                    </div>
                    <?php
					$this->view("tugas/upload_tugas_mhs.php", $data);
				}
			}
		endforeach;
	}else{
		?><div class="well">Sorry, no content to show</div><?php
	}
	?>

			
		
						