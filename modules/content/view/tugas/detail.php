<?php
$this->head();
?> 
	<h2 class="title-page">Tugas</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
	  <li><a href="<?php echo $this->location('module/content/tugas'); ?>">Tugas & Test</a></li>
	  <li class="active">Data</li>
	</ol>
	<!--<div class="breadcrumb-more-action">
		<?php //if($role!="mahasiswa"){ ?>
		<a href="newtugas('<?php //echo $jadwal ?>','<?php //echo $mk ?>');" class="btn btn-primary">
		<i class="fa fa-pencil icon-white"></i> New </a> 
		<?php //} ?>
		<a href="detail('<?php //echo $jadwal ?>','<?php //echo $mk ?>');" class="btn btn-default"><i class="fa fa-bars"></i> List</a> 
		
	</div>-->
	
	<div class="row">
		<?php if($post){ ?>	
			<div class="col-md-12">
				<?php echo $this->view('course/header.php', $data); ?>
				
				<div class="row">
					<div class="col-md-12">
						<div class="tab-content">
							<div class="tab-pane active" id="materi">
								<?php
									echo $this->view('tugas/view.php',$data);	
								?>
							</div>							
						</div>					
					</div>
					
				</div>
			</div>
	<?php } ?>
	
	</div>
</div>
	<?php
$this->foot();
?>