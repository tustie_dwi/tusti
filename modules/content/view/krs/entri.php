<?php $this->head(); 
$header="Tambah KRS";
?>
<div class="row">
	<h2 class="title-page"><?php echo $header; ?></h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/content/krs'); ?>">KRS</a></li>
		  <li class="active"><a href="#"><?php echo $header;  ?></a></li>
		</ol>
		<div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/content/krs'); ?>" class="btn btn-default"><i class="fa fa-bars"></i> Lihat KRS</a> 
	</div>
	<div id="warning"></div>
	
	<div class="row">
		<div id="parameter" class="col-md-3">
			<div class="block-box">
			<form action="<?php echo $this->location('module/content/krs/add'); ?>" role="form" id="param" method="POST">
			
			<div class="form-group">	
				<label class="control-label" >Hari</label>
				
					<select id="select_hari" class="form-control" name="hari" >
						<option value="" data-uri='1'>Pilih Hari</option>
						<option <?php if($hari == 'senin') echo "selected"; ?> value="senin">Senin</option>
						<option <?php if($hari == 'selasa') echo "selected"; ?> value="selasa">Selasa</option>
						<option <?php if($hari == 'rabu') echo "selected"; ?> value="rabu">Rabu</option>
						<option <?php if($hari == 'kamis') echo "selected"; ?> value="kamis">Kamis</option>
						<option <?php if($hari == 'jumat') echo "selected"; ?> value="jumat">Jumat</option>
						<option <?php if($hari == 'sabtu') echo "selected"; ?> value="sabtu">Sabtu</option>
						<option <?php if($hari == 'minggu') echo "selected"; ?> value="minggu">Minggu</option>
					</select>
			</div>
			
			<div class="form-group">	
				<label class="control-label" >Mata Kuliah</label>
				
					<?php $uri_parent = $this->location('module/content/jadwalmk/show_by_prodi'); ?>
					<select id="select_mk" class="form-control e9" name="mk_select" >
						<option value="0" data-uri='1'>Pilih Mata Kuliah</option>
						<?php if($mk > 0) {
							foreach($mk as $dt) :
								if($dt->keterangan != ''){
									echo '<option '; 
									if($mk_id == $dt->mkditawarkan_id) echo "selected";
									echo ' value="'.$dt->mkditawarkan_id.'">'.$dt->keterangan.'</option>';
								}
							endforeach;
						} ?>
					</select>
			</div>			
		</form>
		</div>
	</div>
	
	<?php
		$rep = array();
		if($krs > 0){
			foreach($krs as $key) array_push($rep, $key->mkditawarkan_id);
		}
		
		function cek_jam_kuliah($hari, $jam_mulai, $jam_selesai, $waktu){
			if($waktu){
				foreach ($waktu as $key) {
					if($key->hari == $hari){
						// echo $key->jam_mulai . ' ' . $jam_mulai . ' ' . $jam_selesai;
						if( ($key->jam_mulai >= $jam_mulai && $key->jam_mulai <= $jam_selesai) || ($key->jam_selesai >= $jam_mulai && $key->jam_selesai <= $jam_selesai)){
							return FALSE;
							break;
						}
					}
				}
			}
			return TRUE;
		}
	?>
	
	<!------------------------TABLE------------------------------------------->
	<!-- <div id="content"></div> -->
	<div class="col-md-9">
	<div class="block-box">
	<?php if( isset($posts) ) :	?>
	
		<table class="table table-hover example">
			<thead>
				<tr>
					<th>&nbsp;</th>
		    	</tr>
			</thead>
			<tbody id='content'>
			
		<?php if($posts){
			$no=1;
			foreach ($posts as $dt){ ?>
				<?php if($dt->namamk){ ?>
					<tr>		
						<td>
							<div class="col-md-6">
								<span class="text text-danger"><?php echo ucWords($dt->hari) . ' <i class="fa fa-clock-o"></i> ' . date("H:i", strtotime($dt->jam_mulai)) . ' - ' . date("H:i", strtotime($dt->jam_selesai)) ?></span>
								<br>
								<strong><?php echo $dt->kode_mk ?></strong> - <?php echo $dt->namamk; ?>
								<span class="label label-normal">Kelas <?php echo $dt->kelas ?></span>
								<i class="fa fa-map-marker"></i> R. <?php echo $dt->ruang?>
							</div>
							
					
					
							<div class="col-md-4">
								<small>Kapasitas : <?php echo $dt->kuota; ?> orang</small>
								<?php
									if($dt->terisi==null) $dt->terisi = 0; 
									if($dt->kuota <= 0) $dt->kuota = 1;
									$prog = ($dt->terisi/$dt->kuota) * 100; 
									$sisa = $dt->kuota - $dt->terisi;
									if($sisa < 0) $sisa = 0;
								?>
								<div class="progress">
									<?php if($sisa > 0) { ?>
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $prog ?>%;">
											<?php echo $dt->terisi ?> mahasiswa
										</div>
									<?php } else { ?>
										<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
											Penuh
										</div>
									<?php } ?>									
								</div>
							</div>
						
							<div class="col-md-2">
								<?php
									$is_ada = 0;
									str_replace($rep, '', $dt->mkditawarkan_id, $is_ada);
								?>
								<?php if( ! cek_jam_kuliah($dt->hari, $dt->jam_mulai, $dt->jam_selesai, $waktu) ) : ?>
									<a href="#" class="btn btn-danger disabled pull-right"><i class="fa fa-ban"></i> Bentrok</a>
								<?php else : ?>
									<?php if($sisa_sks >= $dt->sks && $is_ada == 0 && $sisa > 0) { ?>
										<a href="#" class="btn btn-default add-mk pull-right" onclick="add('<?php echo $dt->jadwal_id ?>')">
											<i class="fa fa-plus"></i> 
											Tambah
										</a>
									<?php } else { ?>
										<a href="#" class="btn btn-default disabled pull-right"><i class="fa fa-plus"></i> Tambah</a>
									<?php } ?>
								<?php endif; ?>
							</div>
						</td>
					</tr>
				<?php }  ?>
			<?php } } ?>
			</tbody>
		</table>
		<?php 
	
	 else: 
	 ?> <div class="well">Sorry, no content to show.</div>
    <?php endif; ?>
    
    </div>
	</div>
    <!------------------------TABLE------------------------------------------->
	</div>
</div>
<?php $this->foot(); ?>