<?php $this->head();
$header="KRS ";
if($posts > 0){
				foreach ($posts as $dt): 
					$matkul =	$dt->keterangan;
					break;
				 endforeach; 
			 }
$header.=$matkul." List";

	
 ?>
	<div class="row">
<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/content/krs'); ?>">KRS</a></li>
	  <li class="active"><a href="#"><?php $str_breadcrumb = str_replace('KRS ','',$header);
	  echo "List ".str_replace(' List','',$str_breadcrumb);
	  ?></a></li>
	</ol>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	
		$str="<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>No</th>				
						<th>NIM</th>
						<th>Mahasiswa</th>
						<th>Act</th>
					</tr>
				</thead>
				<tbody>";
		
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
					$str.=	"<tr id='post-".$dt->mahasiswa_id."' data-id='".$dt->mahasiswa_id."' valign=top>
								<td>".$i++."</td>
								<td>".$dt->nim."</td>
								<td>".$dt->nama."</td>";
										
					$str.= "<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href=".$this->location('module/content/krs/edit/'. $dt->krs_id)."><i class='icon-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td></tr>";
				 endforeach; 
			 }
		$str.= "</tbody></table>";
		
		echo $str;
	
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</fieldset>
<?php $this->foot(); ?>