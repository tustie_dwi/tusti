<?php
$this->head();

if($posts !=""){
	$header		= "Edit KRS";
	
	foreach ($posts as $dt):
		$mkditawarkan_id	= $dt->mkditawarkan_id;
		$matakuliah	= $dt->keterangan;
		$mahasiswa_id = $dt->mahasiswa_id;
		$mahasiswa = $dt->nama;
		$kelas = $dt->kelas;
		$nilai_akhir = $dt->nilai_akhir;
		$id			= $dt->hid_id;
	endforeach;
	//$ceknew			= 0;
	$frmact 	= $this->location('module/content/krs/save');		
	
}else{
	$header			= "Write New KRS";
	$id				= "";
	$mkditawarkan_id		= "";
	$matakuliah		= "";
	$mahasiswa_id 	= "";
	$mahasiswa 	= "";
	$kelas 	= "";
	$nilai_akhir = "";
	//$ceknew			= 1;
	$frmact 		= $this->location('module/content/krs/save');		
}


?>

<div class="container-fluid">  
	
	<legend>
		<a href="<?php echo $this->location('module/content/krs'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> KRS List</a> 
		<?php if($id !=""){	?>
		<a href="<?php echo $this->location('module/content/krs/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Fakultas</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post  action="<?php echo $this->location('module/content/krs/save'); ?>" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Mata Kuliah</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mkditawarkan_id" value="<?php echo $mkditawarkan_id; ?>"> -->
						<input class="form-control" class="form-control" type="text" name="matakuliah" id="matakuliah" class="span6" value="<?php echo $matakuliah; ?>">
						<input type="hidden" name="mkditawarkan_id" id="mkditawarkan_id" value="<?php  echo $mkditawarkan_id;?>">
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Mahasiswa</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>"> -->
						<input class="form-control" type="text" name="mahasiswa" id="mahasiswa" class="span6" value="<?php echo $mahasiswa; ?>">
						<input type="hidden" name="mahasiswa_id" id="mahasiswa_id" value="<?php  echo $mahasiswa_id;?>">
					</div>
				</div>	
				<div class="form-group">	
					<label class="col-sm-2 control-label">Kelas</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>"> -->
						<input class="form-control" type="text" name="kelas" id="kelas" class="span6" value="<?php echo $kelas; ?>">
						<!-- <input type="hidden" name="mahasiswa_id" id="mahasiswa_id" value="<?php  echo $mahasiswa_id;?>"> -->
					</div>
				</div>	
				<div class="form-group">	
					<label class="col-sm-2 control-label">Nilai Akhir</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>"> -->
						<input class="form-control" type="text" name="nilai_akhir" class="span6" value="<?php echo $nilai_akhir; ?>">
						
					</div>
				</div>	
				<div class="form-group">		
					<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<!-- <input type="hidden" name="ceknew" value="<?php echo $ceknew;?>">	 -->
						<input type="submit" name="b_krs" value="Submit" class="btn btn-primary">
					</div>
				</div>		
							
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
?>