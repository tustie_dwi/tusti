<?php
$this->head();

if($posts !=""){
	$header		= "Edit KRS";
	
	foreach ($posts as $dt):
		$mkditawarkan_id	= $dt->mkditawarkan_id;
		$matakuliah	= $dt->keterangan;
		$mahasiswa_id = $dt->mahasiswa_id;
		$mahasiswa = $dt->nama;
		$kelas = $dt->kelas;
		$nilai_akhir = $dt->nilai_akhir;
		$id			= $dt->hid_id;
		$tahun_akademik = $dt->tahun_akademik;
	endforeach;
	
	//$ceknew			= 0;
	$frmact 	= $this->location('module/content/krs/save');		
	
}else{
	$header			= "Write New KRS";
	$id				= "";
	$mkditawarkan_id		= "";
	$matakuliah		= "";
	$mahasiswa_id 	= "";
	$mahasiswa 	= "";
	$kelas 	= "";
	$nilai_akhir = "";
	$tahun_akademik = "";
	//$ceknew			= 1;
	$frmact 		= $this->location('module/content/krs/save');		
}


?>

	<div class="row">
<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/content/krs'); ?>">KRS</a></li>
	  <li class="active"><a href="#"><?php $str_breadcrumb = str_replace('KRS','',$header);
	  $str_breadcrumb=str_replace('Write','',$str_breadcrumb);
	  echo $str_breadcrumb;
	  ?></a></li>
	</ol>
	
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post  action="<?php echo $this->location('module/content/krs/save'); ?>" class="form-horizontal">
				<div class="form-group">
					<label for="tahunakademik" class="col-sm-2 control-label">Tahun Akademik</label>
					  <div class="col-sm-10">
						<?php $uri_parent = $this -> location('module/content/krs/tampilkan_parent'); ?>
						<?php echo '<select class="form-control" id="select_tahun" name="select_tahun" data-uri="' . $uri_parent . '">'; ?>
						<option value="">Pilih Tahun Akademik</option>
						<?php
						if (count($tahun) > 0) {
							foreach ($tahun as $dt) :
								echo "<option value='" . $dt -> tahun_akademik . "' ";
								if ($tahun_akademik == $dt -> tahun_akademik) {
									echo "selected";
								}
								echo ">" . $dt -> tahun . "</option>";
							endforeach;
						}
						?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="matakuliah" class="col-sm-2 control-label">Mata Kuliah</label>
					  <div class="col-sm-10">
						<select id="select_parent" class="form-control e9" name="select_parent" <?php if($matakuliah != '') echo ""; else echo "disabled" ?> >
							<option value="0">Pilih Mata Kuliah</option>
							<?php
								if($matakuliah != '') echo "<option selected value='". $mkditawarkan_id ."'>". $matakuliah ."</option>";					
							?>
						</select>
					</div>
				</div>
				
				<!-- <div class="form-group">	
					<label class="col-sm-2 control-label">Mata Kuliah</label>
					<div class="col-sm-10">
						<input class="form-control" disabled="disabled" class="form-control" type="text" name="matakuliah" id="matakuliah" class="span6" value="<?php echo $matakuliah; ?>">
						<input type="hidden" name="mkditawarkan_id" id="mkditawarkan_id" value="<?php  echo $mkditawarkan_id;?>">
					</div>
				</div> -->
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Kelas</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>"> -->
						<input class="form-control" type="text" name="kelas" id="kelas" class="span6" value="<?php echo $kelas; ?>">
						<!-- <input type="hidden" name="mahasiswa_id" id="mahasiswa_id" value="<?php  echo $mahasiswa_id;?>"> -->
					</div>
				</div>	
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Mahasiswa</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>"> -->
						<input class="form-control" type="text" name="mahasiswa" id="mahasiswa" class="span6" value="<?php echo $mahasiswa; ?>">
						<input type="hidden" name="mahasiswa_id" id="mahasiswa_id" value="<?php  echo $mahasiswa_id;?>">
					</div>
				</div>	
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Nilai Akhir</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>"> -->
						<input class="form-control" type="text" name="nilai_akhir" class="span6" value="<?php echo $nilai_akhir; ?>">
						
					</div>
				</div>	
				<div class="form-group">		
					<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<!-- <input type="hidden" name="ceknew" value="<?php echo $ceknew;?>">	 -->
						<input type="submit" name="b_krs" value="Submit" class="btn btn-primary">
					</div>
				</div>		
							
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
?>