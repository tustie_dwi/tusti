<?php $this->head(); 
$header="Daftar Mahasiswa Bimbingan";?>
	<div class="row">
<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="#"><?php echo $header;  ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
    </div>
	
	
	<div class="block-box">
		 <?php
		 
		 if(isset($status) and $status) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $statusmsg; ?>
			</div>
		<?php 
		endif; 
		
		 if( isset($posts) ) :	?>
			<table class='table table-hover example'>
					<thead>
						<tr>			
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
			
				<?php if($posts > 0){ ?>
					<?php foreach ($posts as $dt): ?>
						<tr>
							
							<td>
								<div class="col-md-9">
									<div class="media">
										<a class="pull-left" style="text-decoration:none" href="#" onClick = "detail(<?php echo $dt->mahasiswa_id?>)">
											<img class="media-object img-thumbnail" src="<?php echo $this->asset($foto); ?>"  width="50" height="50">
										</a>
									  <div class="media-body">
										<?php
										echo "<a href='".$this->location('module/content/krs/approve/' . $dt->mahasiswa_id) ."' class='text text-default'><b>".ucWords(strToLower($dt->nama))."</b></a>
											<small><span class='text text-danger'>".$dt->prodi."</span></small>
											&nbsp;<span class='label label-success'>".$dt->angkatan."</span><br>";
										echo $dt->nim;
										?>
									  </div>
									</div>
							     </div>
								<div class="col-md-3">
									<a class="btn btn-default pull-right" href="<?php echo $this->location('module/content/krs/approve/' . $dt->mahasiswa_id) ?>"><i class="fa fa-check"></i> Lihat KRS</a>
								</div>
							</td>
						</tr>
					 <?php endforeach; ?>
				 <?php } ?>
			</tbody></table>
		
		 <?php else: 
		 ?>
	    <div class="span3" align="center" style="margin-top:20px;">
		    <div class="well">Sorry, no content to show</div>
	    </div>
	    <?php endif; ?>
    </div>
</fieldset>
<?php $this->foot(); ?>