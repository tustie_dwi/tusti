<?php $this->head();?>
<div class="row">
<h2 class="title-page">Kartu Rencana Studi</h2>
<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/content/krs'); ?>">KRS</a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<a href="#" class="btn btn-default add-krs">
	    <i class="fa fa-pencil icon-white"></i> Tambah KRS</a> 
    </div>
	
	
	<div class="col-md-12">
		<div class="block-box">
			<?php
			if(isset($post)) :	
			?>
				<table width="100%">
					<?php if(isset($identitas)){
							 ?>
					<tr>
						<td>Nama</td>
						<td>:</td><td><?php echo $identitas->nama ?></td>
						<td width="50%">&nbsp;</td>
						<td>Angkatan</td>
						<td>:</td><td><?php echo $identitas->angkatan ?></td>
					</tr>
					<tr>
						<td>NIM</td>
						<td>:</td><td><?php echo $identitas->mahasiswa_id ?></td>
						<td width="50%">&nbsp;</td>
						<td>Semester</td>
						<?php 
							if(isset($semesterke->inf_semester)) $semester = $semesterke->inf_semester + 1; 
							else $semester = 1;
						?>
						<td>:</td><td><?php echo $semester . ' ('. $this->kekata($semester) . ')' ?></td>
					</tr>
							<?php } ?>
					
				</table><p>&nbsp;</p>
				
				<?php 
					$sks_max = $this->get_sks_max();
					switch($msg) {
						case 'ok' :
							echo '<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									Ok, mata kuliah baru berhasil ditambahkan.
								</div>';
							break;
						
						case 'mt' :
							echo '<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									Maaf, KRS online belum bisa dimulai sekarang.
								</div>';
							break;
						
						case 'dl' :
							echo '<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									Ok, mata kuliah berhasil dibatalkan.
								</div>';
							break;
					}
				?>
				
				<table class="table table-hover">
					<thead>
						<tr>
							<th width="2%">No</th>
							<th>Matakuliah</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 1;
							$nilai = 0;
							$sks = 0;
							$sks_lulus = 0;
							foreach($post as $dt){ ?>
								<tr>
									<td><?php echo $i ?>. </td>
									<td><?php echo "<b>".$dt->kode_mk."</b> - ".$dt->nama_mk;
											if($dt->is_approve == '1') echo ' <span class="label label-success"><i class="fa fa-check"></i> Disetujui</span>';
											else echo ' <span class="label label-danger">* Belum Disetujui</span>';
										?><br>
										<small>
										<span class="text text-danger"><?php echo ucfirst($dt->hari) . ' <i class="fa fa-clock-o"></i> ' . date("H:i", strtotime($dt->jam_mulai)) . ' - ' . date("H:i", strtotime($dt->jam_selesai)) ?></span>
										<span class="label label-normal">Kelas <?php echo $dt->kelas ?></span>
										<i class="fa fa-map-marker"></i> R. <?php echo $dt->ruang_id ?></small>
									</td>
									
									<td><?php echo $dt->sks ?> sks &nbsp;
										<?php if($dt->is_approve == 0) : ?>
											<a class='btn btn-danger drop-mk' href="#" data-id="<?php echo $dt->krs_id ?>" 
												data-mk="<?php echo $dt->matakuliah ?>"
												title="Batalkan mata kuliah" />
												<i class='fa fa-trash-o'></i>
											</a>
										<?php endif; ?>
									</td>
								</tr>
						<?php $i++;
							  
							  $sks += $dt->sks;
							  
							} ?>
								
								<tr>
									<td>&nbsp;</td>
									<td>Total SKS</td>
									<td><strong><?php echo $sks ?> sks</strong></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>SKS Maksimum</td>									
									<td><strong><?php echo $sks_max ?> sks</strong></td>
								</tr>
								
					</tbody>
				</table>
			<?php else: $sks = 0; ?>
				<?php 
					switch($msg) {
						case 'mt' :
							echo '<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									Maaf, layanan KRS Online masih ditutup.
								</div>';
							break;
					}
				?>
				<div class="span3" align="center" style="margin-top:20px;">
					<div class="well">Maaf daftar KRS Anda masih kosong</div>
				</div>
			<?php endif; ?>
			
			<input id="sks-curr" value="<?php echo $sks ?>" type="hidden" />
			<input id="sks-max" value="<?php echo $sks_max ?>" type="hidden" />
		</div>
	</div>

</div>
<?php $this->foot(); ?>