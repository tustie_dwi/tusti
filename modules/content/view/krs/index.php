<?php $this->head(); 
$header="KRS";?>
	<div class="row">
<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="#"><?php echo $header;  ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/content/krs/write'); ?>" class="btn btn-primary pull-right">
    <i class="fa fa-pencil"></i> New KRS</a>
    </div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	
		$str="<table class='table table-hover' id='example'>
				<thead>
					<tr>			
						<th>Mata Kuliah</th>
						<th></th>
						<th>Tahun MK</th>
						<th>Diambil Oleh</th>
						<th>Act</th>
					</tr>
				</thead>
				<tbody>";
		
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
					$str.=	"<tr valign=top>
								<td>".$dt->keterangan."<br /></td>";
					$str.="<td><code>".$dt->kode_mk."</code></td>";
								
					$tahun = substr($dt->tahun_akademik,0,4);
					$str .= "<td>".$tahun." ";
					
					if(substr($dt->tahun_akademik,4,2) == '01') {
						$str .= "<span class='label label-info'>Ganjil</span>";
					}
					else $str .= "<span class='label label-primary'>Genap</span>";
					
					$str .= "</td><td><span class='label label-default'>".$dt->total." Mahasiswa</span></td>";
										
					$str.= "<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href=".$this->location('module/content/krs/showmk/'.$dt->hidId)."><i class='fa fa-pencil'></i> Lihat Detail</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td></tr>";
				 endforeach; 
			 }
		$str.= "</tbody></table>";
		
		echo $str;
	
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</fieldset>
<?php $this->foot(); ?>