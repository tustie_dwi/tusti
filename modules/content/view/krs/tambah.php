<?php
$this->head();

if($posts !=""){
	$header		= "Edit KRS";
	
	foreach ($posts as $dt):
		$mkditawarkan_id	= $dt->mkditawarkan_id;
		$matakuliah	= $dt->keterangan;
		$mahasiswa_id = $dt->mahasiswa_id;
		$mahasiswa = $dt->nama;
		$kelas = $dt->kelas;
		$nilai_akhir = $dt->nilai_akhir;
		$id			= $dt->hid_id;
		$tahun_akademik = $dt->tahun_akademik;
	endforeach;
	
	//$ceknew			= 0;
	$frmact 	= $this->location('module/content/krs/save');		
	
}else{
	$header			= "Tambah KRS";
	$id				= "";
	$mkditawarkan_id		= "";
	$matakuliah		= "";
	$mahasiswa_id 	= "";
	$mahasiswa 	= "";
	$kelas 	= "";
	$nilai_akhir = "";
	$tahun_akademik = "";
	//$ceknew			= 1;
	$frmact 		= $this->location('module/content/krs/save');		
}


?>

	<div class="row">
<h2 class="title-page">Tambah KRS</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/krs'); ?>">KRS</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/content/krs/tambah'); ?>">Tambah</a></li>
	</ol>
	
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post  action="<?php echo $this->location('module/content/krs/save'); ?>" class="form-horizontal">
				<div class="form-group">
					<label for="matakuliah" class="col-sm-2 control-label">Mata Kuliah</label>
					  <div class="col-sm-10">
						<select id="matakuliah" class="form-control e9" name="matakuliah">
						<?php	foreach ($mkd as $dt) {
								echo "<option value='" . $dt -> mkditawarkan_id . "'>" . $dt -> namamk . "</option>";
								}
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Kelas</label>
					<div class="col-sm-10">
						<!-- <input required="required" type=text  class="form-control" name="mahasiswa_id" value="<?php echo $mahasiswa_id; ?>"> -->
						<input class="form-control" type="text" name="kelas" id="kelas" class="span6" value="<?php echo $kelas; ?>">
						<!-- <input type="hidden" name="mahasiswa_id" id="mahasiswa_id" value="<?php  echo $mahasiswa_id;?>"> -->
					</div>
				</div>	
		
				<div class="form-group">		
					<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<!-- <input type="hidden" name="ceknew" value="<?php echo $ceknew;?>">	 -->
						<input type="submit" name="b_krs" value="Submit" class="btn btn-primary">
					</div>
				</div>		
							
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
?>