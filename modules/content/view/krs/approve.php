<?php $this->head();?>
<div class="row">
<h2 class="title-page">Daftar KRS Mahasiswa</h2>
<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/content/krs'); ?>">KRS</a></li>
	</ol>
	
	<div class="row">
	<div class="col-md-12">
		<div class="block-box">
			<?php
			
			if(isset($posts)) :	
			?>
				<table width="100%">
					<?php if(isset($identitas)){
							
								$sks_max = $this->get_sks_max($identitas->mahasiswa_id);
							?>
					<tr>
						<td>Nama</td>
						<td>:</td><td><?php echo $identitas->nama ?></td>
						<td width="50%">&nbsp;</td>
						<td>Angkatan</td>
						<td>:</td><td><?php echo $identitas->angkatan ?></td>
					</tr>
					<tr>
						<td>NIM</td>
						<td>:</td><td><?php echo $identitas->mahasiswa_id ?></td>
						<td width="50%">&nbsp;</td>
						<td>Semester</td>
						<?php 
							if(isset($semesterke->inf_semester)) $semester = $semesterke->inf_semester + 1; 
							else $semester = 1;
						?>
						<td>:</td><td><?php echo $semester . ' ('. $this->kekata($semester) . ')' ?></td>
					</tr>
							<?php } ?>
				
				</table><p>&nbsp;</p>
				
				<table class="table table-hover">
					<form id="form-approve" action="<?php echo $this->location('module/content/krs/save_approve')?>" method="post">
					<thead>
						<tr>
							<th width="2%">No</th>
							<th>Matakuliah</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						
							$i = 1;
							$nilai = 0;
							$sks = 0;
							$sks_lulus = 0;
							foreach($posts as $dt){ ?>
								<tr>
									<td><?php echo $i ?>. </td>
									<td><?php echo "<b>".$dt->kode_mk."</b> - ".$dt->nama_mk;
											if($dt->is_approve == '1') echo ' <span class="label label-success"><i class="fa fa-check"></i> Disetujui</span>';
											else echo ' <span class="label label-danger">* Belum Disetujui</span>';
										?><br>
										<small>
										<span class="text text-danger"><?php echo ucfirst($dt->hari) . ' <i class="fa fa-clock-o"></i> ' . date("H:i", strtotime($dt->jam_mulai)) . ' - ' . date("H:i", strtotime($dt->jam_selesai)) ?></span>
										<span class="label label-normal">Kelas <?php echo $dt->kelas ?></span>
										<i class="fa fa-map-marker"></i> R. <?php echo $dt->ruang_id ?></small>
									</td>
									<td><?php echo $dt->sks ?> sks 
									
										<?php if($dt->is_approve == 0){ ?>
										    <label>
										      <input type="checkbox" name="krs_id[]" value="<?php echo $dt->krs_id ?>"> setujui
										    </label>
									    <?php } else{ ?>
									    	<a class="btn btn-danger btn-xs btn-btl-krs" data-hidid="<?php echo $dt->hidId ?>" data-id="<?php echo $dt->krs_id ?>">Batalkan</a>
								    	<?php } ?>
									</td>
								</tr>
						<?php $i++;
							  
							  $sks += $dt->sks;
							  
							} ?>
						<input id="mhsid" value="<?php echo $mhsid?>" type="hidden" name="mhsid" />
						<tr>
							<td colspan="3"><div class="col-md-2">Total SKS</div><div class="col-md-10">:&nbsp;&nbsp;<?php echo $sks ?> sks</div></td>
						</tr>
						<tr>
							<td colspan="3"><div class="col-md-2">Maksimum SKS</div><div class="col-md-10">:&nbsp;&nbsp;<?php echo $sks_max ?> sks</div></td>
						</tr>
						<tr>
							<td colspan="3"><div class="col-md-2">IP</div><div class="col-md-10">:&nbsp;&nbsp;<?php 
								if(isset($ip->ip)) echo number_format($ip->ip,2);
								else echo "-";
							?></div></td>
						</tr>
						<tr>
							<td colspan="3">
							<div class="col-md-2">IPK</div>
							<div class="col-md-10">:&nbsp;&nbsp;<?php 
								if(isset($ip->ip)) echo number_format($ip->ipk,2);
								else echo "-";
							?></div>
							<div class="col-md-12"><p>&nbsp;</p>
							<a id="btn-approve" class="btn btn-primary"><i class="fa fa-check"></i> Proses KRS Mahasiswa</a>
							</div>
							</td>
						</tr>
						
					</tbody>
					</form>
				</table>
				
				
			<?php
			 else: 
			 ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">KRS masih belum diisi.</div>
			</div>
			<?php 
			endif; 
			?>
		</div>
	</div>
	</div>
</div>
<?php $this->foot(); ?>