	
<?php
$this->head();
?>
 <div class="row">
	<h2 class="title-page">Matakuliah</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
	</ol>
            
    <div class="row">
		<div class="col-md-12">
	 		<div class="block-box">
	 			<div class="">
					<form name="frmDosen" id="frmDosen" class="form-inline" method=post action="<?php echo $this->location('module/content/course')?>" >	
						<div class="">						
							<div class='group'>
								<div class="controls" style="text-align:right">
									<select name="cmbsemester" onChange='form.submit();' class='cmbmulti span12 populate form-control'>
										<!--<option value="-">Semester</option>-->
										<?php
										foreach($semester as $dt):
											echo "<option value='".$dt->tahun_akademik."' ";
											if($semesterid==$dt->tahun_akademik){
												echo "selected";
											}
											echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
										endforeach;
										?>
									</select><br>
								</div>
							</div>		
		                    </div>				
					</form>			
				</div>
			<?php
			if(isset($post)) :	
			?>
				<table class="table" id="example">
					<thead>
						<tr>
							<th>&nbsp;</th>
							
						</tr>
					</thead>
					<tbody>
					<?php
						$mpost = new model_course();
						
						foreach($post as $dt):
							$pengampu = $mpost->get_pengampu($dt->mkditawarkan_id);
						?>
						<tr>
							<td>
							<div class="col-md-4">
								<?php echo "<a class='text text-default' style='cursor:pointer' onclick=detail('".$dt->jadwalid."','".$dt->mkid."')><b>".$dt->kode_mk."</b> ".$dt->namamk;?></a> <span class="badge"><?php echo $dt->sks;?> sks</span> <span class='label label-danger'><?php echo $dt->kelas;?></span>
								<br><?php echo "<span class='text text-danger'><small>".$dt->prodi."</small></span> "; ?>
							</div>
							<div <?php
							if($role!='mahasiswa'){
							?> class="col-md-5" <?php } else{ echo 'class="col-md-8"'; } ?>>
							<?php 
							
								foreach($pengampu as $row):
									echo "<span class='label label-normal inline-block'><i class='fa fa-user'></i> ".$row->nama;
									if($row->is_koordinator=='1'){
										echo " *)";
									}
									echo "</span> ";
								endforeach;
							
							?>
							</div>
							<?php
							if($role!='mahasiswa'){
							?>
								<div class="col-md-3">
								<ul class="nav nav-pills" style="margin:0;">
									<li class="dropdown pull-right">
									  <a class="dropdown-toggle btn btn-table" id="drop4" role="button" data-toggle="dropdown" href="#">Action <b class="caret"></b></a>
									  <ul id="menu1" class="dropdown-menu pull-right" role="menu" aria-labelledby="drop4">
										<li><a style='cursor:pointer' onclick="addmateri('<?php echo $dt->jadwalid ?>','<?php echo $dt->mkid?>')"><i class="fa fa-edit"></i> Tambah Topik</a></li>
										<li><a style='cursor:pointer' onclick="detail('<?php echo $dt->jadwalid ?>','<?php echo $dt->mkid?>')"><i class="fa fa-search"></i> View</a></li>
										<li><a class="btn-edit-post" href="<?php echo $this->location('module/content/test/by/jadwal/'.$dt->jadwalid); ?>"><i class="fa fa-file-text-o"></i> Test by Jadwal</a></li>
									  </ul>
									</li>
                                    </ul>
								</div>
							<?php } ?>
							</td>
						</tr>
						<?php
						
						endforeach;
					?>
					</tbody>
				</table>
				<small><em>*) dosen koordinator</em></small>
			<?php
			 else: 
			 ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php 
			endif; 
			?>
           </div>
		</div>
        </div>
</div>
	<?php
$this->foot();
?>
