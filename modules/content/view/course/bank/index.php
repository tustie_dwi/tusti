<?php $this->head(); ?>

<h2 class="title-page"> Bank soal <?php echo $banknamamk ?></h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
		<li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
		<li><a href="<?php echo $this->location('module/content/tugas/'); ?>">Tugas & Test</a></li>
		<li><a href="<?php echo $this->location('module/content/test/writesoal/'.$testid); ?>">Soal</a></li>
		<li class="active">Data</li>
	</ol>

	<div class="row">
		<?php if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
		<?php endif; ?>
		
		<?php if(isset($banksoal)){ ?>
		<table class='table table-hover' id='example'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Pertanyaan</th>
					<!-- <th></th> -->
				</tr>
			</thead>
			<tbody>
			<?php $x = 1; foreach ($banksoal as $bs) : ?>
				<tr>
					<td><input type="checkbox" name="bankselect" value="<?php echo $bs->soalid ?>" /></td>
					
					<td><?php echo $bs->pertanyaan; 
							if($bs->kategori != 'textarea'){ ?>
								<ol type='A'>
									<?php foreach ($bankjawaban as $bj) { 
										if($bs->soalid==$bj->soalid){ ?>
										<li>
											<?php 
											echo "<input type='".$bs->kategori."' name='soalx".$x."' "; 
											if($bj->is_benar==1){
												echo " checked='checked' ";
											}
											echo " > &nbsp;".$bj->keterangan;
											if($bj->is_benar==1){
												echo "<span class='label label-warning'><small>*) Benar</small></span>";
											}
											?>
										</li>
										<?php 	} 
											} ?>
								</ol>
								<?php } else {
									 foreach ($bankjawaban as $bj) { 
										if($bs->soalid==$bj->soalid){ ?>
										<textarea class='form-control'><?php echo $bj->keterangan ?></textarea><br>
								<?php 	}
									 }
								 } ?>
						
					</td>
					
					<!-- <td style='max-width: 60px;'>            
						<ul class='nav nav-pills' style='margin:0;'>
							<li class='dropdown'>
								<a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								<ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/test/edit/'.$dt->test_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
								</ul>
							</li>
						</ul>
					</td> -->
				</tr>
			<?php $x++; endforeach; ?>
			</tbody></table>
			
			<div class="span3" align="left">
			<a class="btn btn-primary col-sm-12" id="addsoal"><i class='fa fa-plus'></i> Add</a>
			<input type="hidden" name="hidTestId" value="<?php echo $testid ?>" />
			</div>
			
		<?php } else { ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Belum terdapat bank soal untuk mata kuliah ini.</div>
			</div>
		<?php }?>
	</div>
<?php $this->foot(); ?>