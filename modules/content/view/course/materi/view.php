<?php if(!isset($komponen)){ ?>
<div class="head-title-box">
    <h3>Topik / Materi</h3>
	<?php if($role!="mahasiswa"){ ?>
    <span class="option-menu">	
    	<a onclick="addmateri('<?php echo $jadwal_id; ?>','<?php echo $mk; ?>')" class="" title="New Materi" href="#">
    		<i class="fa fa-pencil"></i> New Materi</a>    
    </span>
	<?php } ?>
    </div>
<?php }else{ ?>
		<a onclick="addmateri('<?php echo $jadwal_id; ?>','<?php echo $mk; ?>')" class="btn btn-primary pull-right" title="New Materi" href="#">
    		<i class="fa fa-pencil"></i> New Materi</a><br><br>
<?php } ?>
	<table class='table table-nobordered'>
		<tbody>
			<?php
			if($materi){
			
			$i=0;
			$a=1;
			foreach($materi as $dt):
				if($dt->parent_id=='0'){
					?>
					<tr id="del_materi<?php echo $dt->materi_id ?>">
						<?php $uri_location = $this->location('module/content/course/delete'); ?>
						<td>         
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'><?php	echo $a.". ".$dt->judul;?> </a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
									<a style='cursor:pointer' onclick="view_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-search'></i> View</a>	
									<?php
									if($role!='mahasiswa'){
									?>
									<a style='cursor:pointer' onclick="edit_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-pencil'></i> Edit</a>
									<?php if(!isset($komponen)){ ?>
										<a style='cursor:pointer' onclick="add_sub_materi('<?php echo $jadwal_id ?>','<?php echo $dt->mk ?>','<?php echo $dt->id ?>')"><i class='fa fa-plus'></i> Tambah Sub Materi</a>
										<a class='btn-edit-post' href="<?php echo $this->location('module/content/test/by/materi/'.$dt->materiid); ?>"><i class='fa fa-file-text-o'></i> Test by Materi</a>
										
										<a class='btn-edit-post' href="#" onclick="doDelete(<?php echo $dt->materi_id ?>)"><i class='fa fa-trash-o'></i> Delete Materi</a>		
										<input type='hidden' class='deleted<?php echo $dt->materi_id ?>' value="<?php echo $dt->materi_id ?>" uri = "<?php echo $uri_location ?>"/>				
									<?php } ?>
									</li>
									<?php
									}
									?>											
								  </ul>
								</li>
							</ul>
						</td>								
					</tr>
					<?php
					$a++;									
					echo $this->detailmateri($dt->mkditawarkan_id, $dt->materi_id, $a-1, $i,$jadwal_id);
				}
				// $i++;
				
			endforeach;
			}
			?>
			
		</tbody>
	</table>
						