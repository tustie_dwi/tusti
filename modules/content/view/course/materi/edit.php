<?php
$this->head();

if($posts){
	$header		= "Edit Topik/Materi";
	
	foreach ($posts as $dt):
		$mkditawarkan_id	= $dt->mkditawarkan_id;
		$parent_id			= $dt->parent_id;
		$judul				= $dt->judul;
		$hidid				= $dt->mkditawarkan_id;
		$keterangan			= $dt->keterangan;
		$materiid			= $dt->materi_id;
		$ispublish			= $dt->is_publish;
		$urut				= $dt->urut;
		$icon				= $dt->icon;
		$tahun_id			= $dt->tahun_id;
	endforeach;
	//$frmact 	= $this->location('module/akademik/materimk/savematerimk');		
	
}else{
	$header				= "Write New Topik/Materi";
	$parent_id			= "";
	$mkditawarkan_id	= "";
	$judul				= "";
	$hidid				= $post->mkditawarkan_id;
	$keterangan			= "";
	$mkid				= $post->mkid;
	if(isset($matid)){
		$jumlah=$jumlahsub+1;
			if($materi){
				$materiid			= $materi->materi_id;
				$id					= "";
				$urut				= $materi->urut.".".$jumlah;
				$a					= $materi->mk;
			}
	}
	else{
		$a	= "";
		
		$countmatbymk = $jumlahmatbymk+1;
		$materiid			= "";
		$id					= "";
		$urut				= $countmatbymk;
	} 
	$ispublish			= 1;
	$icon				= "";
	$new				= 1;
	$iscampus			= 0;
	$statusid			= 'insession';
	$komponen_id		= "";
	//$frmact 			= $this->location('module/akademik/materimk/savematerimk');		
}


?>
<h2 class="title-page">Matakuliah</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
	   <li><a href="#" onclick="detail('<?php echo $jadwal_id ?>','<?php echo $post->mkid ?>')";>Materi</a></li>
	  <li class="active">Write</li>
	</ol>
	
	
    <div class="row">    
        <div class="col-md-12">
        	<div class="block-box">
			<?php echo $this->view('course/header.php', $data); ?>
			
			
				<ul class="nav nav-tabs" id="writeTab">
					<li class="active"><a href="#info">Topik/Materi</a></li>
					<li><a href="#file">File</a></li>
					<li><a href="#silabus">Silabus</a></li>
				</ul>
				
				<div class="tab-content">
					<!-- MATERI -->
					<div class="tab-pane active" id="info">
						
					<form  method="post" id="form-save-topik" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal " enctype="multipart/form-data">					
						 <?php if(isset($materiid)&&$materiid!=""){?>
						 <div class="form-group">	
							<label class="col-sm-2 control-label">Sub Topik dari</label>
							<div class="controls">
								<div class="col-sm-10">
								<select name="cmbmateri" id="parent" class="form-control" disabled>
									<option value="0">Please Select..</option>			
									<?php
																		
										//foreach($materi as $dt):
											echo "<option  value='".$materi->materi_id."' ";
											if($materiid==$materi->materi_id){
												echo "selected";
											}
											echo ">".$materi->judul."</option>";
										//endforeach;
									?>
								</select>
								<input type="hidden" name="cmbmateri" value="<?php if(isset($materiid)) echo $materiid;?>">
								</div>
							</div>
						</div>
						<?php }?>
						
						<div class="form-group">	
							<label class="col-sm-2 control-label">Judul</label>
							<div class="controls">
								<div class="col-sm-10">
								<input type="text" class="form-control" name='judul' id='judul_topik' required="required" value="<?php if(isset($judul)) echo $judul; ?>">
								</div>
							</div>
						</div>
						 <div class="form-group">	
							<label class="col-sm-2 control-label">Keterangan</label>
							<div class="controls">
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" name='keterangan_topik' id='keterangan'><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label"><!-- No Urut --></label>
							<div class="controls">
								<div class="col-sm-10">
								<input type="hidden" class="form-control" name='urut' id='urut_topik' required="required" value="<?php if(isset($urut)) echo $urut; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Status</label>
							<div class="controls">
								<div class="col-sm-10">
								<select name="cmbstatus" id="parent" class="form-control">
									<?php								
										foreach($status as $dt):
											echo "<option  value='".$dt->status."' ";
											if($statusid==$dt->status){
												echo "selected";
											}
											echo ">".$dt->keterangan."</option>";
										endforeach;
									?>
								</select>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Campus Only ?</label>
							<div class="controls">
								<div class="col-sm-10">
									<label class="checkbox"><input type="checkbox" name="iscampus" value="1" <?php if ($iscampus==1) { echo "checked"; } ?>>Ya</label>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Upload Icon</label>
							<div class="controls">
								<div class="col-sm-10">
									<?php 
									if(!isset($new)){
										if(isset($icon)){ ?>
											<div class='well'>
												<img style="width: 100px; height: auto;" src="<?php echo $this->config->file_url_view."/".$icon; ?>"/>
												<input type="hidden" name="uploads" id="uploads" value="<?php if(isset($icon)) echo $icon; ?>">
											</div>
										<?php } else { ?>
											<div class='well'>
												<p>Icon Belum Tersedia</p>
											</div>
										<?php } 
									}?>
									<input type="file" class="form-control" name="uploads" id="uploads">
								</div>
							</div>
						</div>
						 <div class="form-group">
							<div class="controls">
								<div class="col-sm-offset-2 col-sm-10">
								<input type="hidden" name="hidid" id="mkid" value="<?php if(isset($hidid)) echo $hidid;?>">
								<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
								<input type="hidden" name="id" value="<?php echo $id;?>">
								<input  type="submit" id="btn-submit-publish" name="b_savepublish" onclick="submit_topik_content('publish')" value="Save and Publish" class="btn btn-primary">
								<input  type="submit" id="btn-submit-draft" name="b_draft" onclick="submit_topik_content('draft')" value="Save as Draft" class="btn btn-warning">
								<span id="status-save" style="margin-left:1em;">&nbsp;</span>    
								</div>
							</div>
							
						</div>				
					</form>
					
					</div>
					
					<!-- FILE -->
					
					<div class="tab-pane" id="file">	
						<form method="post" id="upload-form" enctype="multipart/form-data" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal">
							<div class="form-group">	
							<label class="col-sm-2 control-label">Materi MK</label>
							<div class="controls">
								<div class="col-sm-10">
								<select name="cmbmateri" id="parent" class="form-control">
									<option value="0">Please Select..</option>			
									<?php
										if(isset($matid) && ($matid)){
											echo "<option  value='".$materi->materi_id."' ";
											if($materiid==$materi->materi_id){
												echo "selected";
											}
											echo ">".$materi->judul."</option>";
										}else{										
											foreach($materi as $dt):
												echo "<option  value='".$dt->materi_id."' ";
												if($materiid==$dt->materi_id){
													echo "selected";
												}
												echo ">".$dt->judul."</option>";
											endforeach;
										}
									?>
								</select>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Judul</label>
							<div class="controls">
								<div class="col-sm-10">
								<input type="text" class="form-control" name='judul' id='judul_file' required="required" value="<?php if(isset($judul)) echo $judul; ?>">
								</div>
							</div>
						</div>
						 <div class="form-group">	
							<label class="col-sm-2 control-label">Keterangan</label>
							<div class="controls">
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" name='keterangan' id='keterangan_file'><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">	
								<label class="col-sm-2 control-label">Upload File</label>
								<div class="controls col-sm-10">									
										<input type="file" class="form-control" name="uploads[]" id="uploads" multiple>
										<small><em>*)support multiple file upload</em></small>									
								</div>
						</div>
						<div class="form-group">
								<div class="controls">
									<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="hidid" value="<?php if(isset($hidid)) echo $hidid;?>">
									<input  type="submit" name="b_filepublish" value="Upload and Publish File" onclick="submit_file_content('publish')" class="btn btn-primary btn-save-file" data-loading-text="Saving...">
									<input  type="submit" name="b_filedraft" value="Upload as Draft" onclick="submit_file_content('draft')" class="btn btn-warning btn-save-file" data-loading-text="Saving...">
									<span id="status-save-file" style="margin-left:1em;">&nbsp;</span>    
									</div>
								</div>
						</div>				
						</form>
					</div>
					
					
					<!-- SILABUS -->
					
					<div class="tab-pane" id="silabus">	
						<form method="post" id="silabus-form" enctype="multipart/form-data" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal">
							<div class="form-group">	
								<label class="col-sm-2 control-label">Komponen</label>
								<div class="controls">
									<div class="col-sm-10">
									<select class="form-control" name="cmbkomponen" id="parent">
										<option class="sub_01" value="0">Please Select..</option>			
										<?php
																			
											foreach($komponen as $dt):
												echo "<option class='sub_".$dt->komponen_id."' value='".$dt->komponen_id."' ";
												if($komponen_id==$dt->komponen_id){
													echo "selected";
												}
												echo ">".$dt->keterangan."</option>";
											endforeach;
										?>
									</select>
									</div>
								</div>
							</div>
						  <div class="form-group">	
								<label class="col-sm-2 control-label">Keterangan</label>
								<div class="controls">
									<div class="col-sm-10">
									<textarea class="form-control ckeditor" name='silabus' id='silabus'><?php echo $keterangan; ?></textarea><br>
									</div>
								</div>
						  </div>
						  <div class="form-group">
								<div class="controls">
									<div class="col-sm-offset-2 col-sm-10">
										<input type="hidden" name="hidsilId" value="<?php if(isset($silid)) echo $silid;?>">
										<input type="hidden" name="hiddetId" value="<?php if(isset($detid)) echo $detid;?>">
										<input type="hidden" name="hidid" value="<?php if(isset($hidid)) echo $hidid;?>">
										<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
										<input type="hidden" name="new" value="<?php if(isset($new)) echo $new;?>">
										<input  type="submit" name="b_silabus" value="Submit" onclick="submit_silabus_content()" class="btn btn-primary">
									</div>
								</div>
						  </div>				
						</form>
					</div>
				</div>
			
		</div>
			<div>
	</div>
</div>
<?php
$this->foot();

?>