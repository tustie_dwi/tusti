<?php
$this->head();

if($posts!=""){
	$header		= "Edit Topik/Materi";
	
	foreach ($posts as $dt):
		$mkditawarkan_id	= $dt->mkditawarkan_id;
		$parent_id			= $dt->parent_id;
		$judul				= $dt->judul;
		$hidid				= $dt->mkditawarkan_id;
		$keterangan			= $dt->keterangan;
		$materiid			= $dt->materi_id;
		$ispublish			= $dt->is_publish;
		$urut				= $dt->urut;
		$icon				= $dt->icon;
		$tahun_id			= $dt->tahun_id;
	endforeach;
	//$frmact 	= $this->location('module/akademik/materimk/savematerimk');		
	
}else{
	$header				= "Write New Topik/Materi";
	$parent_id			= "";
	$mkditawarkan_id	= "";
	$judul				= "";
	$hidid				= $post->mkditawarkan_id;
	$keterangan			= "";
	$mkid				= $post->mkid;
	if(isset($matid)){
		$jumlah=$jumlahsub+1;
		foreach($materi as $dt){
			$materiid			= $dt->materi_id;
			$id					= "";
			$urut				= $dt->urut.".".$jumlah;
			$a					= $dt->mk;
		}
	}
	else{
		foreach($materi as $dt){
			$a					= $dt->mk;
		}
		$countmatbymk = $jumlahmatbymk+1;
		$materiid			= "";
		$id					= "";
		$urut				= $countmatbymk;
	} 
	$ispublish			= 1;
	$icon				= "";
	$new				= 1;
	$iscampus			= 0;
	$statusid			= 'insession';
	$komponen_id		= "";
	//$frmact 			= $this->location('module/akademik/materimk/savematerimk');		
}


?>
<div class="row-fluid">  
	
	<legend>
		<a style='cursor:pointer' onclick=detail('<?php echo $jadwal_id ?>','<?php echo $post->mkid ?>') class="btn btn-default pull-right"><i class="fa fa-bars"></i> Materi List</a>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			
				<table class='table table-bordered'>
					<tbody>
						<tr>
							<td width="20%"><em>Matakuliah</em></td>
							<td><?php echo $post->namamk; ?></td>
						</tr>
						<tr>
							<td width="20%"><em>Pengampu</em></td>
							<td><?php echo "<ul>";
								if($pengampu){
										foreach($pengampu as $dt):
											echo "<li class='label label-info'>".$dt->nama;
											if($dt->is_koordinator=='1'){
												echo " *)";
											}
											echo "</li> ";
										endforeach;
								}
						echo "</ul>"; ?></td>
						</tr>
						
					</tbody>
				</table>
				<ul class="nav nav-tabs" id="writeTab">
					<li class="active"><a href="#info">Topik/Materi</a></li>
					<li><a href="#file">File</a></li>
					<li><a href="#silabus">Silabus</a></li>
				</ul>
				<div class="tab-content">
					<!-- MATERI -->
					<div class="tab-pane active" id="info">
						
					<form  method="post" id="form-save-topik" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal " enctype="multipart/form-data">					
						 <?php if(isset($materiid)&&$materiid!=""){?>
						 <div class="form-group">	
							<label class="col-sm-2 control-label">Sub Topik dari</label>
							<div class="controls">
								<div class="col-sm-10">
								<select name="cmbmateri" id="parent" class="form-control" disabled>
									<option value="0">Please Select..</option>			
									<?php
																		
										foreach($materi as $dt):
											echo "<option  value='".$dt->materi_id."' ";
											if($materiid==$dt->materi_id){
												echo "selected";
											}
											echo ">".$dt->judul."</option>";
										endforeach;
									?>
								</select>
								<input type="hidden" name="cmbmateri" value="<?php if(isset($materiid)) echo $materiid;?>">
								</div>
							</div>
						</div>
						<?php }?>
						
						<div class="form-group">	
							<label class="col-sm-2 control-label">Judul</label>
							<div class="controls">
								<div class="col-sm-10">
								<input type="text" class="form-control" name='judul' required="required" value="<?php if(isset($judul)) echo $judul; ?>">
								</div>
							</div>
						</div>
						 <div class="form-group">	
							<label class="col-sm-2 control-label">Keterangan</label>
							<div class="controls">
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" name='keterangan'><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label"><!-- No Urut --></label>
							<div class="controls">
								<div class="col-sm-10">
								<input type="hidden" class="form-control" name='urut' required="required" value="<?php if(isset($urut)) echo $urut; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Status</label>
							<div class="controls">
								<div class="col-sm-10">
								<select name="cmbstatus" id="parent" class="form-control">
									<?php								
										foreach($status as $dt):
											echo "<option  value='".$dt->status."' ";
											if($statusid==$dt->status){
												echo "selected";
											}
											echo ">".$dt->keterangan."</option>";
										endforeach;
									?>
								</select>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Campus Only ?</label>
							<div class="controls">
								<div class="col-sm-10">
									<label class="checkbox"><input type="checkbox" name="iscampus" value="1" <?php if ($iscampus==1) { echo "checked"; } ?>>Ya</label>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Upload Icon</label>
							<div class="controls">
								<div class="col-sm-10">
									<?php 
									if(!isset($new)){
										if(isset($icon)){ ?>
											<div class='well'>
												<img style="width: 100px; height: auto;" src="<?php echo $this->asset($icon); ?>"/>
												<input type="hidden" name="icon" id="icon" value="<?php if(isset($icon)) echo $icon; ?>">
											</div>
										<?php } else { ?>
											<div class='well'>
												<p>Icon Belum Tersedia</p>
											</div>
										<?php } 
									}?>
									<input type="file" class="form-control" name="icon" id="icon">
								</div>
							</div>
						</div>
						 <div class="form-group">
							<div class="controls">
								<div class="col-sm-offset-2 col-sm-10">
								<input type="hidden" name="hidid" id="mkid" value="<?php if(isset($hidid)) echo $hidid;?>">
								<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
								<input type="hidden" name="id" value="<?php echo $id;?>">
								<input  type="submit" id="btn-submit-publish" name="b_savepublish" onclick="submit_topik('publish')" value="Save and Publish" class="btn btn-primary">
								<input  type="submit" id="btn-submit-draft" name="b_draft" onclick="submit_topik('draft')" value="Save as Draft" class="btn btn-warning">
								<span id="status-save" style="margin-left:1em;">&nbsp;</span>    
								</div>
							</div>
							
						</div>				
					</form>
					
					</div>
					
					<!-- FILE -->
					
					<div class="tab-pane" id="file">	
						<form method=post id="upload-form" enctype="multipart/form-data" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal">
							<div class="form-group">	
							<label class="col-sm-2 control-label">Materi MK</label>
							<div class="controls">
								<div class="col-sm-10">
								<select name="cmbmateri" id="parent" class="form-control">
									<option value="0">Please Select..</option>			
									<?php
																		
										foreach($materi as $dt):
											echo "<option  value='".$dt->materi_id."' ";
											if($materiid==$dt->materi_id){
												echo "selected";
											}
											echo ">".$dt->judul."</option>";
										endforeach;
									?>
								</select>
								</div>
							</div>
						</div>
						<div class="form-group">	
							<label class="col-sm-2 control-label">Judul</label>
							<div class="controls">
								<div class="col-sm-10">
								<input type="text" class="form-control" name='judul' required="required" value="<?php if(isset($judul)) echo $judul; ?>">
								</div>
							</div>
						</div>
						 <div class="form-group">	
							<label class="col-sm-2 control-label">Keterangan</label>
							<div class="controls">
								<div class="col-sm-10">
									<textarea class="form-control ckeditor" name='keterangan'><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
								</div>
							</div>
						</div>
						<!-- <div class="form-group">	
							<label class="col-sm-2 control-label">Is Publish ?</label>
							<div class="controls">
								<div class="col-sm-10">
									<label class="checkbox"><input type="checkbox" name="ispublish" value="1" <?php if ($ispublish==1) { echo "checked"; } ?>>Ya</label>
								</div>
							</div>
						</div> -->
						<div class="form-group">	
								<label class="col-sm-2 control-label">Upload File</label>
								<div class="controls col-sm-10">									
										<input type="file" class="form-control" name="uploads[]" id="uploads" multiple>									
								</div>
						</div>
						<div class="form-group">
								<div class="controls">
									<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="hidid" value="<?php if(isset($hidid)) echo $hidid;?>">
									<input  type="submit" name="b_filepublish" value="Upload and Publish File" onclick="submit_file('publish')" class="btn btn-primary btn-save-file" data-loading-text="Saving...">
									<input  type="submit" name="b_filedraft" value="Upload as Draft" onclick="submit_file('draft')" class="btn btn-warning btn-save-file" data-loading-text="Saving...">
									<span id="status-save-file" style="margin-left:1em;">&nbsp;</span>    
									</div>
								</div>
						</div>				
						</form>
					</div>
					
					
					<!-- SILABUS -->
					
					<div class="tab-pane" id="silabus">	
						<form method=post id="silabus-form" enctype="multipart/form-data" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal">
							<div class="form-group">	
								<label class="col-sm-2 control-label">Komponen</label>
								<div class="controls">
									<div class="col-sm-10">
									<select class="form-control" name="cmbkomponen" id="parent">
										<option class="sub_01" value="0">Please Select..</option>			
										<?php
																			
											foreach($komponen as $dt):
												echo "<option class='sub_".$dt->komponen_id."' value='".$dt->komponen_id."' ";
												if($komponen_id==$dt->komponen_id){
													echo "selected";
												}
												echo ">".$dt->keterangan."</option>";
											endforeach;
										?>
									</select>
									</div>
								</div>
							</div>
						  <div class="form-group">	
								<label class="col-sm-2 control-label">Keterangan</label>
								<div class="controls">
									<div class="col-sm-10">
									<textarea class="form-control ckeditor" name='silabus'><?php echo $keterangan; ?></textarea><br>
									</div>
								</div>
						  </div>
						  <div class="form-group">
								<div class="controls">
									<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="hidsilId" value="<?php if(isset($silid)) echo $silid;?>">
									<input type="hidden" name="hiddetId" value="<?php if(isset($detid)) echo $detid;?>">
									<input type="hidden" name="hidid" value="<?php if(isset($hidid)) echo $hidid;?>">
									<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
									<input type="hidden" name="new" value="<?php if(isset($new)) echo $new;?>">
									<input  type="submit" name="b_silabus" value="Submit" onclick="submit_silabus()" class="btn btn-primary">
									</div>
								</div>
						  </div>				
						</form>
					</div>
				</div>
				
		</div>
	</div>
</div>
<?php
$this->foot();

?>