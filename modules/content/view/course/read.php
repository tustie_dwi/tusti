	
<?php
$this->head();

?>
<style type="text/css">
/* Style the second URL with a red border */
#test-gdocsviewer {
	border: 5px red solid;
	padding: 20px;
	width: 650px;
	background: #ccc;
	text-align: center;
}
/* Style all gdocsviewer containers */
.gdocsviewer {
	margin:10px;
}
</style>

<?php
if(isset($materi)){
	$mk = $materi->mk;
}else{
	$mk	= $rfile->mk;
}
?>

<h2 class="title-page">Materi</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
		<li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
		<li><a href="#" onclick="detail('<?php echo $jadwal_id ?>','<?php echo $mk ?>')">Materi</a></li>
		<li class="active">Data</li>
	</ol>
	
		<div class="row">					
			<div class="col-md-12">
				<?php if(isset($materi) && ($materi)){
						$judul=$materi->judul;
				?>	
					<h3><?php echo $judul;?>
					<?php
					if($role!='mahasiswa'){
					?>
					<a style='cursor:pointer' onclick="edit_materi('<?php echo $jadwal_id ?>','<?php echo $materi->mk ?>','<?php echo $materi->id ?>');" class="btn btn-primary" style="margin:0px 5px">
					<i class="fa fa-pencil"></i> Edit Materi</a><?php
					}
					?>
					</h3>
					
				
				<p><?php echo $materi->keterangan;?></p>
				<?php } ?>	
								
						
						<?php
						
						if(isset($file) && ($file)){	
							?>
							<h3>File</h3>	
							<table class="table"><tbody>
							<?php
							foreach($file as $dt):							
									
									?>
									<tr>
										<td><?php echo $dt->jenis;?></td>
										<td><a style='cursor:pointer' onclick="doView('<?php echo $dt->id ?>'); view_file('<?php echo $jadwal_id ?>','<?php echo $dt->id ?>')"><?php	echo $dt->judul;?></a></td>	
										<td><?php if($dt->is_publish=='1'){ echo "Published"; } ?></td>										
									</tr>
									<?php								
							endforeach;
							?>
								
								</tbody>
							</table>
							<?php
						}
						
						
						if(isset($rfile) && ($rfile)){
							?>	
							<?php
							echo "<h3>".$rfile->judul."";?>
							<?php
							if($role!='mahasiswa'){
							?>
								<a style='cursor:pointer' onclick="edit_file('<?php echo $jadwal_id ?>','<?php echo $rfile->materiid ?>','<?php echo $rfile->id ?>')" class="btn btn-primary" style="margin:0px 5px">
									<i class="fa fa-pencil"></i> Edit File</a>
							<?php
							}
							
							echo "</h3>";
							echo "<p>".$rfile->keterangan."</p>";
							switch ($rfile->jenis){
								case 'Video':
									echo '<video width="480" height="360" id="video" preload="none" title="'.$rfile->judul.'" controls>
												<source src="'.$this->config->file_url_view."/".$rfile->file_loc.'" type="video/webm">
												<source src="'.$this->config->file_url_view."/".$rfile->file_loc.'" type="video/mp4">
												<source src="'.$this->config->file_url_view."/".$rfile->file_loc.'" type="video/ogg">
											</video>';
									break;
								
								case 'Image':
									echo '<img src="'.$this->config->file_url_view."/".$rfile->file_loc.'" width="480" height="360" class="img-thumbnail" />';
									break;
									
								case 'Presentation' || 'Document' || 'Spreadsheet':
									echo '<iframe src="https://docs.google.com/gview?url='.$this->config->file_url_view."/".$rfile->file_loc.'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';		
									
									break;
							}

							echo "<p><i class='fa fa-calendar'></i> ".date("M d, Y", strtotime($rfile->tgl_upload))."&nbsp; <i class='fa fa-clock-o'></i> ".date("H:i", strtotime($rfile->tgl_upload))." by ".$rfile->upload_by."</p>";
										/*$str="<br><table class='table table-bordered' id='example'>
												<tr>
													<td><em>Judul</em></td>
													<td>".$rfile->judul."</td>
												</tr>
													";
										$str.=" <tr>
													<td><em>Keterangan</em></td>
													<td>".$rfile->keterangan."</td>
												</tr>
													";
										$str.=" <tr>
													<td><em>Jenis File</em></td>
													<td>".$rfile->jenis."</td>
												</tr>
													";
													
										$str.=" <tr>
													<td><em>Tanggal Upload</em></td>
													<td>".$rfile->tgl_upload."&nbsp; <br>Upload By <span class='label label-info'>".$rfile->upload_by."</span></td>
												</tr>
													";
										$str.= "</table>";	
										echo $str;	*/
										
									
									echo '<a onclick=doDownload("'.$rfile->id.'") href="'.$this->location('module/akademik/file/download/'.$rfile->file_name).'" class="btn btn-default"><i class="fa fa-download"></i> Download This File</a>';
									

						}
						?>
					
				
				
			</div>		
		</div>

	<?php
	
	
$this->foot();
?>
