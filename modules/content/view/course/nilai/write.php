<?php
/*$this->head();
$header		= "Nilai";
$id			= "";
?>
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course'); ?>">Matakuliah</a></li>
	  <li><a href="#">nilai</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/content/course'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Matakuliah List</a>
	</div>
   <?php */ ?>
	<div class="head-title-box">
		<h3>Komponen Penilaian</h3>
		<?php if($role!="mahasiswa"){ ?>
			<?php if( !isset($isproses_tblproses)||$isproses_tblproses==0 ) { ?>
				<span class="option-menu">	
					<a href="#nilaiprocess" onclick="nilaiprocess_('<?php echo $mk ?>','<?php echo $jadwal ?>', 'write')" >
					<i class="fa fa-pencil"></i> New Komponen Nilai</a>
				</span>
			<?php } ?>
		<?php } ?>
	</div>
     

	<?php if( isset($posts) ) :?>
	<form method=post name="form" id="form" class="form-horizontal">
		<?php foreach ($posts as $d) { ?>
		<?php 	if($this->cek_if_parent($d->hid_id)!=TRUE){ ?>
		<div class="form-group">	
			 <div class="controls">
				<div class="col-md-6">
					<input name="komponen[]" required='required' type=text  class='form-control' value='<?php if(isset($d->keterangan))echo $d->keterangan ?>'>
					<input type="hidden"  class='form-control' value='<?php if(isset($d->hid_id))echo $d->hid_id ?>' required>
					<input name="type[]" required='required' type="hidden"  class='form-control' value='komponen'>
				</div>
				
				<div class="col-md-2">
					<div class="input-group">							 
					 <input name="percent[]" type=text onkeypress="return isNumberKey(event)" class='form-control percentage parent-percent' required >
					 <span class="input-group-addon">%</span>
					</div>														
				</div>						
				<div class="col-md-4">
				<a onclick="addTest('<?php echo $d->mk_id ?>', '<?php echo $d->komponen_id ?>', '<?php echo $jadwal ?>')" class="btn btn-danger"><i class="fa fa-plus-circle"></i> Test</a>
				<a onclick="addTugas('<?php echo $jadwal ?>', '<?php echo $d->komponen_id ?>')" class="btn btn-danger"><i class="fa fa-plus-circle"></i> Tugas</a>
				</div>
			</div>	
			 <div class="controls">
				<div class="col-md-6" id="<?php echo $d->komponen_id ?>-select"></div>
				<div class="col-md-2" id="<?php echo $d->komponen_id ?>-percent"></div>						
			</div>					
			
		</div>
		<?php 	} ?>
		<?php } ?>
		<div class="controls">					
			<input type="submit" id="submit" name="b_nilai" value="Save & Publish Komponen Nilai" class="btn btn-primary">
			<input type="hidden" name="jadwalid" value="<?php if(isset($jadwal)) echo $jadwal;?>">
			<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
		</div>
	</form>
	
	<?php else: ?>
	
		<div class="well">Belum terdapat komponen nilai untuk mata kuliah ini</div>
	<?php endif; ?>

