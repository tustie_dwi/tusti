<?php
$this->head();

if($posts !=""){
	$header		= "Edit Nilai";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$namamk			= $dt->namamk;
		$keterangan		= $dt->keterangan;
		$bobot			= $dt->bobot;
		$mkid			= $dt->hid_mkid;
	endforeach;
	$edit = "1";
}else{
	$header		= "Nilai";
	$id			= "";
	foreach ($komponen as $dt):
		$komponenid		= $dt->hid_id;
		$namamk			= $dt->namamk;
		$keterangan		= $dt->keterangan;
		$bobot			= $dt->bobot;
		$mkid			= $dt->mkditawarkan_id;
	endforeach;
}


?>
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course'); ?>">Matakuliah</a></li>
	  <li><a href="#">nilai</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/content/course'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Matakuliah List</a>
	</div>
    
    <div class="row">    
        <div class="col-md-12">
			<form method=post name="form" id="form" class="form-horizontal">
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Mata Kuliah</label>
					 <div class="col-sm-10">
						<input id='namamk' required='required' type=text  class='col-md-9 form-control typeahead' disabled value='<?php if(isset($namamk))echo $namamk ?>'>
					</div>
				</div><?php echo $jadwal ?>
				
				<div class="form-group">	
				 	<label for="keterangan" class="col-sm-2 control-label">Komponen Nilai</label>
					 <div class="col-sm-10">
					 	<input required="required" type=text id='keterangan' class='col-md-9 form-control' name="keterangan" value="<?php if(isset($keterangan))echo $keterangan; ?>"><br>
						<input type="hidden" name="hidId" value="<?php echo $id;?>">				
						<br><br><input type="submit" name="b_komponen" id="submit" value="Submit" class="btn btn-primary">
					</div>
				</div>
				
			</form>
		</div>
	</div>
<?php
$this->foot();
?>