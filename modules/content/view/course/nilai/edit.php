	<div class="head-title-box">
		<h3>Komponen Penilaian</h3>
		<?php if($role!="mahasiswa"){ ?>
			<?php if( !isset($isproses_tblproses)||$isproses_tblproses==0 ) { ?>
				<span class="option-menu">	
					<a href="#nilaiprocess" onclick="nilaiprocess_('<?php echo $mk ?>','<?php echo $jadwal ?>', 'write')" >
					<i class="fa fa-pencil"></i> New Komponen Nilai</a>
				</span>
			<?php } ?>
		<?php } ?>
	</div>
     

	 
<form method=post name="form" id="form" class="form-horizontal">
	<?php foreach ($posts as $d) { ?>
	<?php 	if($d->parent_id==0){ ?>
	<div class="form-group">	
		<div class="controls">
			<div class="col-md-6">
				<input name="komponen[]" required='required' type=text  class='form-control' value='<?php if(isset($d->keterangan))echo $d->keterangan ?>'>
				<input name="hidId[]" required='required' type="hidden"  class='form-control' value='<?php if(isset($d->proses_id))echo $d->proses_id ?>'>
				<input name="type[]" required="required" type="hidden"  class="form-control" value="proses-parent">
			</div>
							
			<div class="col-md-2">
				<div class="input-group">
				<input name="percent[]" required='required' type=text onkeypress="return isNumberKey(event)" class='form-control percentage parent-percent' value='<?php if(isset($d->persentase_total))echo $d->persentase_total ?>'>
				 <span class="input-group-addon">%</span>
				</div>														
			</div>						
			<div class="col-md-4">
				<a onclick="addTest('<?php echo $d->mk_id ?>', '<?php echo $d->komponen_id ?>', '<?php echo $jadwal ?>')" class="btn btn-success"><i class="fa fa-plus"></i> Test</a>
				<a onclick="addTugas('<?php echo $d->jadwal_id ?>', '<?php echo $d->proses_id ?>')" class="btn btn-info"><i class="fa fa-plus"></i> Tugas</a>
			</div>
		
			
			<?php $data['parent'] = $d->proses_id; 
				  echo $this->view("course/nilai/edit-child.php", $data); ?>
			
		</div>
	</div>
	<?php 	} ?>
	<?php } ?>
	
	<div class="controls">					
		<input type="submit" id="submit" name="b_nilai" value="Save & Publish Komponen Nilai" class="btn btn-primary">
		<input type="hidden" name="jadwalid" value="<?php if(isset($jadwal)) echo $jadwal;?>">
		<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
			
	</div>
</form>
        