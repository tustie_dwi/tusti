<div class="head-title-box">
    <h3>Komponen Penilaian</h3>
	<?php if($role!="mahasiswa"){ ?>
		<?php if( !isset($isproses_tblproses)||$isproses_tblproses==0 ) { ?>
			<span class="option-menu">	
				<a href="#nilaiprocess" onclick="nilaiprocess_('<?php echo $mk ?>','<?php echo $jadwal ?>', 'write')" >
				<i class="fa fa-pencil"></i> New Komponen Nilai</a>
			</span>
		<?php } ?>
	<?php } ?>
</div>

<?php if( isset($posts) ) :
		 $i =0; 
?>
<div class="tree">
	<ul>
	<?php foreach ($posts as $d) { 
			if(!($d->parent_id)){
	?>
		<li>
			<span class="span"><?php echo $d->keterangan ?></span>
			<a class='btn-edit-post btn-add'><i><?php if(($d->persentase_total))echo $d->persentase_total; else echo $d->persentase_detail?>%</i><span class='text'> </span></a>
			<?php
				$data['parent'] = $d->hid_id; 
				echo $this->view("course/nilai/view-child.php", $data);
			?>
		</li>
		 	<?php
			}
		} ?>
	 </ul>
	 <em><small>Last Update by : <?php echo $d->user ?> <i class="fa fa-clock-o"></i> <?php echo $d->last_update ?></small></em>
</div>			
<?php else: ?>

    <div class="well">Sorry, no content to show</div>

<?php endif; ?>
<?php if($role!='mahasiswa'){ ?>

	<!--<?php //if( !isset($isproses_tblproses)||$isproses_tblproses==0 ) { ?>
	<a href="#nilaiprocess" class='btn btn-danger' onclick="nilaiprocess_('<?php //echo $mk ?>','<?php //echo $jadwal ?>', 'write')" >
	<i class='fa fa-pencil'></i> Write Nilai</a>
	<?php //} ?>-->
	<?php if( isset($posts) ) { ?>
	<a href="#nilaiprocess" class='btn btn-default' onclick="nilaiprocess_('<?php echo $mk ?>','<?php echo $jadwal ?>', 'proses')" >
	<i class='fa fa-pencil'></i> Input Nilai Mahasiswa</a>
	<?php } ?>
	<?php if( isset($posts)&& (!isset($isproses_tblprosesmhs)||$isproses_tblprosesmhs==0) && isset($cektblprosesmhs) ) { ?>
	<a href="#nilaiprocess" class='btn btn-danger' onclick="processKHS_('<?php echo $mk ?>','<?php echo $jadwal ?>')" >
	<i class='fa fa-pencil'></i> Proses Nilai KHS</a>
	<?php } ?>
<?php } ?>