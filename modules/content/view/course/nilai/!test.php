<?php $this->head(); 
foreach ($posts as $bs) :
	$namamk = $bs->namamk;
	$mkid	= $bs->mkid;
endforeach;
?>
<fieldset>
	<div class="row">
	<h2 class="title-page">Test <?php echo $namamk ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course'); ?>">Matakuliah</a></li>
	  <li class="active">Data</li>
	</ol>
	
	<?php if(isset($posts)){ ?>
	<table class='table table-hover' id='example'>
		<thead>
			<tr>
				<th>No</th>
				<th>Test</th>
				<!-- <th></th> -->
			</tr>
		</thead>
		<tbody>
		<?php $x = 1; foreach ($posts as $bs) : ?>
			<tr>
				<td><?php echo $x ?>&nbsp;<input type="checkbox" name="select" value="<?php echo $bs->test_id ?>" /></td>
				
				<td>
				<div class="col-md-3">
					<?php if($role == 'mahasiswa'){ ?>
						<?php echo "<b>".$bs->kode_mk."</b>"; ?>
					<?php } else{ ?>
						<?php echo "<a class='btn-edit-post' href=".$this->location('module/content/test/detail/'.$bs->test_id)."><span class='text text-default'><strong>" . $bs->kode_mk . '</span></strong></a>' ?>
					<?php } 
					echo  $bs->namamk;
					if($bs->judul){
						echo " <br> <code>".$bs->judul."</code>";
					}
					 ?>
				</div>
				<div class="col-md-9">
					<?php if($role == 'mahasiswa'){ ?>
					<a title="Kerjakan Test" href="<?php echo $this->location('module/content/test/confirm/') . $bs->test_id ?>"><span class='text text-info'><?php echo $bs->judul ?></span></a>
					<?php }else{ ?>
					<a title="Lihat Soal" href="<?php echo $this->location('module/content/test/soal/') . $bs->test_id ?>"><span class='text text-info'><?php echo $bs->judul ?></span></a>
					<?php }
						if($bs->is_publish == 1) echo " <small><span class='label label-success'>Published</span></small>"; else echo "<small><span class='label label-warning'>Not Pulished</span></small>";
					if($bs->is_random == 1) echo " <small><span class='label label-normal'><b>*</b> soal acak</span></small>";
						echo "<br><small><i class='fa fa-clock-o'></i>&nbsp;" . date("M d, Y H:i", strtotime($bs->tgl_mulai)) . "</i> - ";
						echo  date("M d, Y H:i", strtotime($bs->tgl_selesai))."&nbsp; <span class='text text-danger'><i class='fa fa-bookmark'></i></span> </small>  " ;
						get_diff_time($bs->tgl_selesai);
					?>				
					
				</div>						
				</td>
				
			</tr>
		<?php $x++; endforeach; ?>
		</tbody></table>
		
		<div class="span3" align="left">
		<a class="btn btn-primary col-sm-12" id="addtest"><i class='fa fa-plus'></i> Add</a>
		<input type="hidden" name="mkId" value="<?php echo $mkid ?>" />
		<input type="hidden" name="jadwalId" value="<?php echo $jadwal ?>" />
		<input type="text" name="komponenId" value="<?php echo $komponen_id ?>" />
		</div>
		
	<?php } else { ?>
		<div class="span3" align="center" style="margin-top:20px;">
	    	<div class="well">Belum test untuk mata kuliah ini.</div>
   		</div>
	<?php }?>
					
</fieldset>
<?php
	function get_diff_time($date1){
		$date1 = strtotime($date1);
		$date2 = time();
		
		$subTime = $date1 - $date2;
		$y = ($subTime/(60*60*24*365));
		$d = ($subTime/(60*60*24))%365;
		$h = ($subTime/(60*60))%24;
		$m = ($subTime/60)%60;
		
		if($subTime > 0){
			echo "<small><span class='text text-danger'>";
			echo $d." hari,\n";
			echo $h." jam,\n";
			echo $m." menit lagi\n";
			echo "</span></small>";
			return 1;
		}
		else{
			echo "<small><span class='label label-danger'>*</span></small>";
			return 0;
		}
	}
?>
<?php $this->foot(); ?>