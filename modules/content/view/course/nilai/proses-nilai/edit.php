	<div class="head-title-box">
		<h3>Komponen Penilaian</h3>
		<?php if($role!="mahasiswa"){ ?>
			<?php if( !isset($isproses_tblproses)||$isproses_tblproses==0 ) { ?>
				<span class="option-menu">	
					<a href="#nilaiprocess" onclick="nilaiprocess_('<?php echo $mk ?>','<?php echo $jadwal ?>', 'write')" >
					<i class="fa fa-pencil"></i> New Komponen Nilai</a>
				</span>
			<?php } ?>
		<?php } ?>
	</div>
    	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; ?>

	<?php $i = 0;
		foreach ($nilai as $n) {
			if(!$n->parent_id)$i++;
		} 
		?>
	<form method=post name="form" id="form" class="form-horizontal">
	<div class="form-group">
		<div class="controls col-md-12">
			<div id="nilai-tables">
			<table class="table table-bordered" id="myTable">
				<thead>
				<tr>
					<th rowspan="2">Mahasiswa</th>
					
					<th colspan="<?php echo $i ?>">Nilai</th>
				</tr>
				<tr>		
					<?php foreach ($nilai as $n) {
						if(!$n->parent_id) { ?>
							<th>
								<?php echo $n->keterangan; ?><br>
								<?php echo "[".$n->persentase_total." %]"?>
								<input name="proses_id-parent[]" required="required" type="hidden"  class="form-control" value="<?php echo $n->hid_id ?>">
								<?php $parent[] = $n->hid_id ?>
								<?php $persentparent[] = $n->persentase_total ?>
							</th>
					<?php }
					} ?>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($mhs as $m) { ?>
					<tr>
						<td>
							<b><small><?php echo $m->nim; ?></small></b>
							<br><?php echo $m->mhs; ?>
							<input name="krsid[]" type="hidden"  class="form-control" value="<?php echo $m->krs_id; ?>">
							<input name="mhsid[]" type="hidden"  class="form-control" value="<?php echo $m->mahasiswa_id; ?>">
						</td>
						
						<?php $this->datanilai($m->nim, $mkid, $jadwal, $nilai, $i, $parent, $persentparent, $nilaiprosesmhs);?>
					</tr>
				<?php } ?>
				</tbody>
			</table>	
			</div>
			
			<a type="submit" id="submit-proses" name="b_nilai" class="btn btn-primary">Save Nilai Mahasiswa</a>
			<!-- <input type="submit" id="export-nilai" value="export" class="btn btn-primary"> -->
			<a download="<?php echo $post->namamk."-".$post->kelas; ?>.xls" class="btn btn-primary" id="export" href="#" > Export table to Excel</a>
			<a id="ImportFile" class="btn btn-primary" onclick="showImportForm()">Import Excel File</a>
			<input type="hidden" name="jadwalid" value="<?php if(isset($jadwal)) echo $jadwal;?>">
			<input type="hidden" name="mkid" value="<?php if(isset($mkid)) echo $mkid;?>">
			<input type="hidden" name="action" value="<?php if(isset($action)) echo $action;?>" />
			<input type="hidden" id="param-disabled" value="<?php if(isset($isproses_tblprosesmhs)) echo $isproses_tblprosesmhs;?>" />
		</div>
	</div>
	</form>
	
	<div id="import-form" style="display: none;">
	<form method="post" name="form-import" id="form-import" class="form-horizontal" >
		Import Excel File : <input type="file" name="file" class="form-control"><br>
		<input type="submit" id="submit-import" value="Import Nilai Mahasiswa" class="btn btn-success">
	</form>
	</div>
	<table class="table table-bordered" id="tes"></table>
	<div id="tes2"></div>
