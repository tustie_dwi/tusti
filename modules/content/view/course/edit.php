<?php
if($user!="Mahasiswa"){ 
$this->head();

if($materi!=""){
	$header		= "Edit Materi MK";
	$statusid	= $materi->statusid;
	$icon	= $materi->icon;

}else{
	$this->redirect('module/content/course/');	
}

if(isset($materi)){
	$mk = $materi->mk;
}

?>
<h2 class="title-page">Materi</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
		<li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
		<li><a href="#" onclick="detail('<?php echo $jadwal_id ?>','<?php echo $mk ?>')">Materi</a></li>
		<li class="active">Edit Topik/Materi</li>
	</ol>

	<?php if($materi){
	
    		?>
	
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post id="upload-form-edit-topik" enctype="multipart/form-data" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Judul</label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="text" class="form-control" name='judul' id="judul" required="required" value="<?php if(isset($materi->judul)) echo $materi->judul; ?>">
						</div>
					</div>
				</div>
				 <div class="form-group">	
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
						<textarea class="form-control ckeditor" name='keterangan'><?php if(isset($materi->keterangan)) echo $materi->keterangan; ?></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Status</label>
					<div class="controls">
						<div class="col-sm-10">
						<select name="statmateri" id="parent" class="form-control">
							<?php								
								foreach($status as $dt):
									echo "<option  value='".$dt->status."' ";
									if($statusid==$dt->status){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Campus Only ?</label>
					<div class="controls">
						<div class="col-sm-10">
							<label class="checkbox"><input type="checkbox" name="campusonly" value="1" <?php if(isset($materi->is_campus)) { echo "checked"; } ?>>Ya</label>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Upload Icon</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php 
							if(!isset($new)){
								if(isset($icon)){ ?>
									<div class='well'>
										<img style="width: 100px; height: auto;" src="<?php echo $this->config->file_url_view."/".$icon; ?>"/>
										<input type="hidden" name="uploads" id="uploads" value="<?php if(isset($icon)) echo $icon; ?>">
									</div>
								<?php } else { ?>
									<div class='well'>
										<p>Icon Belum Tersedia</p>
									</div>
								<?php } 
							}?>
							<input type="file" class="form-control" name="uploads" id="uploads">
						</div>
					</div>
				</div>
				 <div class="form-group">
					<div class="controls">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="hidden" name="hidmatId" value="<?php if(isset($materi->materi_id)) echo $materi->materi_id;?>">
							<input type="hidden" name="mkditawarkan" value="<?php if(isset($materi->mkditawarkan_id)) echo $materi->mkditawarkan_id;?>">
							
							<input type="hidden" name="urut" value="<?php if(isset($materi->urut)) echo $materi->urut;?>">
							<input type="hidden" name="parent" value="<?php if(isset($materi->parent_id)) echo $materi->parent_id;?>">
							
							<input type="hidden" name="mk" value="<?php if(isset($materi->mk)) echo $materi->mk;?>">
							<input type="hidden" name="id" value="<?php if(isset($materi->id)) echo $materi->id;?>">
							<input type="hidden" name="edit" value="0">
							<input  type="submit" id="btn-submit" name="b_publish" value="Save and Publish" onclick="submit_edit_topik_content('publish')" class="btn btn-primary">
							<input  type="submit" id="btn-submit" name="b_draft" value="Save as Draft" onclick="submit_edit_topik_content('draft')" class="btn btn-warning">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
	<?php } ?>

<?php
$this->foot();
}
?>