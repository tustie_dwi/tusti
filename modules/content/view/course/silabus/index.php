<?php
$this->head();
?> 
	<h2 class="title-page">Matakuliah</h2>
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
	  <li class="active">Data</li>
	</ol>
	
	<div class="row">
		<?php if($post){ ?>	
			<div class="col-md-12">
				<?php echo $this->view('course/header.php', $data); ?>
								
				<!-- <h3>Topik/Materi</h3> -->
				<div class="row">
					<div class="col-md-10">
						<div class="tab-content">
						<div class="tab-pane active" id="silabus">
							<?php
							echo $this->view('course/silabus/view.php',$data);	
							?>
							</div>
							
						</div>
					
					</div>
				
					<!-- sidebar -->
                    <?php echo $this->view('course/sidebar.php',$data);?>
                    <!-- end sidebar -->
				</div>
			</div>
	<?php } ?>
	
	</div>
</div>
	<?php
	
	
$this->foot();
?>
