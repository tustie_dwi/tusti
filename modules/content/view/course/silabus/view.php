<div class="head-title-box">
<h3>Silabus</h3>
</div>
    
    <div class="action-group" style="margin-left: 5px;">
		  <div id="accordion">
		  	<div class="panel panel-success">
			  	<?php foreach($komponen as $dt): ?>
					    <div class="panel-heading" style="border-radius: 0px;">
					      <h4 class="panel-title" style="color: black">
					      	<span class="collapse-title"><?php echo $dt->keterangan; ?></span>
					      	<?php if($dt->keterangan!="Course Overview"){ ?>
					      	<small><a class="btn-collapsed pull-right" data-toggle="collapse" href="#<?php echo $dt->komponen_id ?>">show</a></small>
					      	<?php } ?>
					      </h4>
					    </div>
					    <div id="<?php echo $dt->komponen_id ?>" class="panel-collapse collapse <?php if($dt->keterangan=="Course Overview"){ echo 'in';} ?>">
			    			<div class="panel-body">
			    				<?php if($role!="mahasiswa"){ ?>
			    					  <form role="form" method="post" id="silabus-form<?php echo $dt->komponen_id ?>">
										  <input type="hidden" id="hidid" name="hidid" value="<?php echo $mk; ?>">
										  <input type="hidden" id="jadwalid" name="jadwalid" value="<?php echo $jadwal_id; ?>">
								          <?php if($dt->keterangan!="Course Overview"){ ?>
								          <div class="form-content-silabus">
								          	<input type="hidden" name="cmbkomponen" value="<?php echo $dt->komponen_id ?>">
								          	<div class="form-group">
					    					  <div id="label-silabus<?php echo $dt->komponen_id ?>">
												<p class="silabus<?php echo $dt->komponen_id ?>"></p>
												<button type="button" data-uri="<?php echo $dt->komponen_id ?>" class="btn btn-primary" name="edit_sil">Edit</button>
											  </div>
											  <div id="form-silabus<?php echo $dt->komponen_id ?>" style="display: none">
												<label>Note</label>
												<textarea class="form-control ckeditor" name='silabus' id="silabus<?php echo $dt->komponen_id ?>"></textarea>
												<input type="hidden" name="silabusid" value="">
												<input type="hidden" name="detailid" value="">
											  </div>
										   </div>
										   <?php if($role!="mahasiswa"): ?>		
										  	  <button style="display: none" type="submit" class="btn btn-primary"  name="b_silabus<?php echo $dt->komponen_id ?>" value="Save & Publish" onclick="submit_silabus_content('<?php echo $dt->komponen_id ?>')">Save & Publish</button>
										  	  <button style="display: none" type="button" class="btn btn-info cancel" data-uri="<?php echo $dt->komponen_id ?>" name="b_cancel<?php echo $dt->komponen_id ?>">Cancel</button>
										   <?php endif; ?>
										  </div>
										  <?php }else{ 										
								   	  		$this->view('course/materi/view.php', $data);
								   	      } ?>
									  </form>		
			    				<?php }else{ ?>
			    					<input type="hidden" id="jadwalid" name="jadwalid" value="<?php echo $jadwal_id; ?>">
								    <input type="hidden" id="mkid" name="mkid" value="<?php echo $mkd->hidId; ?>">
								   	<?php if($dt->keterangan!="Course Overview"){ ?>
											<p class="silabus<?php echo $dt->komponen_id ?>"></p>
											<?php }else{ 										
										   	  		$this->view('course/materi/view.php', $data);
										   	      } ?>
		    					<?php } ?>
			    			</div>
					    </div>
			    <?php endforeach; ?>
		    </div>
	  	  </div>
	</div>
    
	<!-- <form method=post id="silabus-form" enctype="multipart/form-data" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal">
		<div class="form-group">	
			<label class="col-sm-2 control-label">Komponen</label>
			<div class="controls">
				<div class="col-sm-10">
				<select class="form-control" id="cmbkomponen" name="cmbkomponen" onChange="get_komponen()" >
					<option value="0">Please Select..</option>			
					<?php
														
						foreach($komponen as $dt):
							echo "<option  value='".$dt->komponen_id."' ";
							echo ">".$dt->keterangan."</option>";
						endforeach;
					?>
				</select>
				<input type="hidden" id="hidid" name="hidid" value="<?php echo $post->mkditawarkan_id; ?>">
				<input type="hidden" id="jadwalid" name="jadwalid" value="<?php echo $jadwal_id; ?>">
				<input type="hidden" id="mkid" name="mkid" value="<?php echo $post->mkid; ?>">
				</div>
			</div>
		</div>
		 <div id="form-content-silabus">
			 <div class="form-group">	
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
						<textarea class="form-control ckeditor" name='silabus'></textarea>
						<input type="hidden" name="silabusid" value="">
						<input type="hidden" name="detailid" value="">
						</div>
					</div>
			  </div>
			  <?php if($role!="mahasiswa"){ ?>
		
			  <div class="form-group">
				<div class="controls">
					<div class="col-sm-offset-2 col-sm-10">	
					<input  type="submit" name="b_silabus" value="Save & Publish" onclick="submit_silabus_content()" class="btn btn-primary">
					</div>
				</div>
			  </div>
			  <?php } ?>
	  </div>		  
	    	  
	</form> -->
				