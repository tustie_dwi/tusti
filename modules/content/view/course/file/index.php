<?php
$this->head();
?> 
	<h2 class="title-page">Matakuliah</h2>
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
	  <li class="active">Data</li>
	</ol>
	
	<div class="row">
		<?php if($post){ ?>	
			<div class="col-md-12">
				<?php echo $this->view('course/header.php', $data); ?>
								
				<!-- <h3>Topik/Materi</h3> -->
				<div class="row">
					<div class="col-md-10">
						<div class="tab-content">
						<div class="tab-pane active" id="file">
							<!--content-->
							<div class="head-title-box">
						   		<h3>File</h3>
						   		<?php if($role!="mahasiswa"){ ?>
							    <span class="option-menu">	
							    	<a onclick="file('<?php echo $jadwal_id; ?>','<?php echo $mk; ?>')" class="" title="New File" href="#">
							    		<i class="fa fa-pencil"></i> New File</a>
							    </span>
								<?php } ?>
						    </div>
							<div class="col-md-4">	 	
								<div class="form-group">	
									<label>Category</label>			
									<?php $uri_parent = $this->location('module/content/course/tampilkan_materi'); ?>
									<?php echo '<select class="form-control e9" name="kategori" id="select_kategori"  data-uri="'.$uri_parent.'">' ?>
										<option class="sub_01" value="0">Select Category</option>
										<?php foreach($kategori as $k){
											echo "<option value=".$k->jenis_file_id.">".$k->keterangan."</option>"; 
										}
										?>			
									</select>
											
								</div>		
								<div class="form-group">	
									<label>Nama Materi</label>				
									<?php $uri_parent = $this->location('module/content/course/tampilkan_file'); ?>
									<?php echo '<select class="form-control e9" disabled name="materi" id="select_materi"  data-uri="'.$uri_parent.'">' ?>
										<option class="sub_01" value="0">Select Materi</option>	
									</select>					
								</div>
								<input type="hidden" name="jadwal_id" id="jadwal_id" value="<?php if(isset($jadwal_id)) echo $jadwal_id ?>"/>	
							</div>
							<div class="col-md-8" id="display"></div>
							<!--content-->
						</div>							
						</div>
					
					</div>
				
					<!-- sidebar -->
                    <?php echo $this->view('course/sidebar.php',$data);?>
                    <!-- end sidebar -->
				</div>
			</div>
	<?php } ?>
	
	</div>
</div>
	<?php
	
	
$this->foot();
?>
