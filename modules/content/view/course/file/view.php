<div class="head-title-box">
    <h3>File</h3>
    </div>
	<form method="post" id="upload-form" enctype="multipart/form-data"  class="form-horizontal">
		<div class="form-group">	
		<label class="col-sm-2 control-label">Materi MK</label>
		<div class="controls">
			<div class="col-sm-10">
			<select name="cmbmateri" id="parent" class="form-control">
				<option value="0">Please Select..</option>			
				<?php
													
					foreach($materi as $dt):
						echo "<option  value='".$dt->materi_id."' ";
						if(isset($materiid)==$dt->materi_id){
							echo "selected";
						}
						echo ">".$dt->judul."</option>";
					endforeach;
				?>
			</select>
			</div>
		</div>
	</div>
	<div class="form-group">	
		<label class="col-sm-2 control-label">Judul</label>
		<div class="controls">
			<div class="col-sm-10">
			<input type="text" class="form-control" name='judul_file' id="judul_file" required="required" value="<?php if(isset($judul)) echo $judul; ?>">
			</div>
		</div>
	</div>
	 <div class="form-group">	
		<label class="col-sm-2 control-label">Keterangan</label>
		<div class="controls">
			<div class="col-sm-10">
				<textarea class="form-control ckeditor" id="keterangan_file" name='keterangan_file'><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
			</div>
		</div>
	</div>
	<div class="form-group">	
			<label class="col-sm-2 control-label">Upload File</label>
			<div class="controls col-sm-10">									
					<input type="file"  name="uploads[]" id="uploads" multiple>
					<small><em>*)support multiple file upload</em></small>									
			</div>
	</div>
	<div class="form-group">
			<div class="controls">
				<div class="col-sm-offset-2 col-sm-10">
				<input type="hidden" name="hidid" value="<?php if(isset($mk)) echo $mk;?>">
				<input  type="submit" name="b_filepublish" value="Upload and Publish File" onclick="submit_file_content('publish')" class="btn btn-primary btn-save-file" data-loading-text="Saving...">
				<input  type="submit" name="b_filedraft" value="Upload as Draft" onclick="submit_file_content('draft')" class="btn btn-warning btn-save-file" data-loading-text="Saving...">
				<span id="status-save-file" style="margin-left:1em;">&nbsp;</span>    
				</div>
			</div>
			
	</div>				
	</form>
					
					
					