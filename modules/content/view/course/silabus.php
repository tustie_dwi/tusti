
<h3 class="h3-small">Silabus</h3>
	<form method=post id="silabus-form" enctype="multipart/form-data" action="<?php echo $this->location('module/content/course'); ?>" class="form-horizontal">
		<div class="form-group">	
			<label class="col-sm-2 control-label">Komponen</label>
			<div class="controls">
				<div class="col-sm-10">
				<select class="form-control" name="cmbkomponen" id="parent">
					<option class="sub_01" value="0">Please Select..</option>			
					<?php
														
						foreach($komponen as $dt):
							echo "<option class='sub_".$dt->komponen_id."' value='".$dt->komponen_id."' ";
							if($komponen_id==$dt->komponen_id){
								echo "selected";
							}
							echo ">".$dt->keterangan."</option>";
						endforeach;
					?>
				</select>
				</div>
			</div>
		</div>
	  <div class="form-group">	
			<label class="col-sm-2 control-label">Keterangan</label>
			<div class="controls">
				<div class="col-sm-10">
				<textarea class="form-control ckeditor" name='silabus'><?php echo $keterangan; ?></textarea><br>
				</div>
			</div>
	  </div>
	  <div class="form-group">
			<div class="controls">
				<div class="col-sm-offset-2 col-sm-10">									
				<input type="hidden" name="hidid" value="<?php echo $post->mkditawarkan_id; ?>">
				<input type="hidden" name="mkid" value="<?php echo $post->mkid; ?>">
				<input  type="submit" name="b_silabus" value="Submit" onclick="submit_silabus()" class="btn btn-primary">
				</div>
			</div>
	  </div>				
	</form>
				