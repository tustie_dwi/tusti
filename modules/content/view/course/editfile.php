<?php
if($user!="mahasiswa"){ 
$this->head();

if($file!=""){
	$header		= "Edit File";	
	
}else{
	$this->redirect('module/content/course/');	
}


?>
	<h2 class="title-page">File</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
		<li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
		<li><a href="#" onclick="detail('<?php echo $jadwal_id ?>','<?php echo $mk ?>')">Materi</a></li>
		<li><a href="#" onclick="view_file('<?php echo $jadwal_id ?>','<?php echo $file->id ?>')">File</a></li>
		<li class="active">Edit</li>
	</ol>
		
    <div class="row">    
        <div class="col-md-12">		
			<form method=post id="upload-form-edit-file" enctype="multipart/form-data" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Judul</label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="text" class="form-control" name='judul' id="judul" required="required" value="<?php if(isset($file->judul)) echo $file->judul; ?>">
						</div>
					</div>
				</div>
				 <div class="form-group">	
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
						<textarea class="form-control ckeditor" name='keterangan'><?php if(isset($file->keterangan)) echo $file->keterangan; ?></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Upload File</label>
					<div class="controls col-sm-10">									
						<?php if(!isset($new)){
							if(isset($file->file_loc)){ ?>
								<div class='well'>
									<p>*) File telah dilampirkan</p>
								</div>
							<?php } else { ?>
								<div class='well'>
									<p>*) File belum dilampirkan</p>
								</div>
							<?php } 
						}?>
						<input type="file" class="form-control" name="uploads" id="uploads">									
					</div>
				</div>
				
				 <div class="form-group">
					<div class="controls">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="hidden" name="hidfileId" value="<?php if(isset($file->file_id)) echo $file->file_id;?>">
							<input type="hidden" name="jenis_file_id" value="<?php if(isset($file->jenis_file_id)) echo $file->jenis_file_id;?>">
							<input type="hidden" name="materi_id" value="<?php if(isset($file->materi_id)) echo $file->materi_id;?>">
							<input type="hidden" name="mkditawarkan_id" value="<?php if(isset($file->mkditawarkan_id)) echo $file->mkditawarkan_id;?>">
							<input type="hidden" name="file_name" value="<?php if(isset($file->file_name)) echo $file->file_name;?>">
							<input type="hidden" name="file_type" value="<?php if(isset($file->file_type)) echo $file->file_type;?>">
							<input type="hidden" name="file_loc" value="<?php if(isset($file->file_loc)) echo $file->file_loc;?>">
							<input type="hidden" name="file_size" value="<?php if(isset($file->file_size)) echo $file->file_size;?>">
							<!-- <input type="hidden" name="file_content" value="<?php //if(isset($file->file_content)) echo $file->file_content;?>"> -->
							<input type="hidden" name="tgl_upload" value="<?php if(isset($file->tgl_upload)) echo $file->tgl_upload;?>">
							<input type="hidden" name="upload_by" value="<?php if(isset($file->upload_by)) echo $file->upload_by;?>">
							<!-- <input type="hidden" name="is_publish" value="<?php if(isset($file->is_publish)) echo $file->is_publish;?>"> -->
							<input type="hidden" name="id" value="<?php if(isset($file->id)) echo $file->id;?>">
							<input type="hidden" name="edit" value="1">
							<input  type="submit" id="btn-submit" name="b_publish" value="Save and Publish" onclick="submit_edit_file_content('publish')" class="btn btn-primary">
							<input  type="submit" id="btn-submit" name="b_draft" value="Save as Draft" onclick="submit_edit_file_content('draft')" class="btn btn-warning">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>

<?php
$this->foot();
}
?>