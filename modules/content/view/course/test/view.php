<div class="head-title-box">
    <h3>Test</h3>
    <span class="option-menu">
    <?php if($user!="mahasiswa"){ ?>
    	<a onclick="newtest('<?php echo $jadwal ?>', '<?php echo $mk ?>')" class="" title="New Tugas" href="#">
    		<i class="fa fa-pencil"></i> New Test</a>
    <?php } ?>
    </span>
    </div>    
	<?php
		
	 if( isset($posts) ) :	?>
		<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>Modified</th>
						<th>Judul</th>
						<?php if($user!="mahasiswa"){ ?>
						<th>Act</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
			
			<?php 
				$i = 1;
				if($posts > 0){
					foreach ($posts as $dt): 
			?>
				<tr valign=top>
				
					<td>
						<i class='fa fa-clock-o'></i> <?php echo $dt->YMD; ?> <?php echo $dt->waktu; ?><br>
						<small>by <?php echo $dt->user ?></small>
					</td>
					
					<td><?php echo "<span class='text text-default'>".$dt->judul."</span> ";
					
					if($dt->is_publish=='1'){
								echo "<small><span class='label label-success'>Published</span></small> ";
							}
					if($dt->is_random=='1'){
								echo "<small><span class='label label-danger'>*)</span></small>";
							}
					?></td>
					<?php if($user!="mahasiswa"){ ?>
					<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/content/test/detail/'.$dt->test_id) ?>"><i class='fa fa-search'></i> View</a>	
									</li>
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/content/test/edit/'.$dt->test_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/content/test/writesoal/'.$dt->test_id) ?>"><i class='fa fa-plus'></i> Tambah Soal</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td>
					<?php } ?>
				</tr>
		<?php 
		$i++;
		endforeach; 
		} ?>
		</tbody></table>
		
		
	 <?php else: ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>