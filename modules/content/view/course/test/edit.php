<?php
$this->head();

if($posts){
	$header		= "Edit Test";
	if(isset($cek)){
		foreach ($datajadwal as $c):
			$jadwalid		= $c->hid_jadwal;
			$mkid			= $c->hid_mkid;
			$kelas			= $c->kelas;
			$namamk			= $c->namamk;
			$nama			= $c->nama;
		endforeach;
	}
	else {
		$cek= "";
	}
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$materiid		= $dt->materi_id;
		$jadwalid		= $dt->jadwal_id;
		$mkid			= $dt->mkditawarkan_id;
		$judul			= $dt->judul;
		$instruksi		= $dt->instruksi;
		$keterangan		= $dt->keterangan;
		$tanggalmulai	= $dt->tgl_mulai;
		$tanggalselesai	= $dt->tgl_selesai;
		$israndom		= $dt->is_random;
		$ispublish		= $dt->is_publish;
	endforeach;
	
}else{
	$header		= $head;
	$id			= "";
	$israndom	= "";
	$ispublish	= "";
	
	if($cek=='1'){
		$materiid = "";
		foreach ($datajadwal as $c):
			$jadwalid		= $c->hid_jadwal;
			$mkid			= $c->hid_mkid;
			$kelas			= $c->kelas;
			$namamk			= $c->namamk;
			$nama			= $c->nama;
		endforeach;
	}
	elseif($cek=='2'){
		$materiid = "";
		$mkid	  = $mkditawarkanid;
	}
	elseif($cek=='3'){
		$materiid = $materiid;
		$mkid	  = $mkditawarkanid;
	}
}


?>
	<h2 class="title-page">Test</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
		<li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
		<li><a href="<?php echo $this->location('module/content/tugas/'); ?>">Tugas & Test</a></li>
		<li class="active">Edit</li>
	</ol>
    <div class="row">    
        <div class="col-md-12">
		  <div class="block-box">
			<form method="post" name="form" id="form" class="form-horizontal">
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Mata Kuliah</label>
					 <div class="col-sm-10">
						<input disabled required="required" type=text  class='col-md-9 form-control' id="namamk" name="namamk" value="<?php if(isset($namamk))echo $namamk; ?>">
					</div>
				</div>
				
				<?php if($cek=='1'){ ?>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Kelas</label>
					 <div class="col-sm-10">
						<input disabled required="required" type=text  class='col-md-9 form-control' id="kelas" name="kelas" value="<?php if(isset($kelas))echo $kelas; ?>">
					</div>
				</div>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Dosen</label>
					 <div class="col-sm-10">
						<input disabled required="required" type=text  class='col-md-9 form-control' id="dosen" name="dosen" value="<?php if(isset($nama))echo $nama; ?>">
					</div>
				</div>
				<?php } ?>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Materi</label>
					<div class="col-sm-10">
						<select id="materi" name="materi" class='col-md-9 form-control e9'>
							<option value="0" class="sub_01">Please Select..</option>			
							<?php														
								foreach($materi as $dt):
									echo "<option value='".$dt->materi_id."' ";
									if($materiid==$dt->materi_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Judul</label>
					 <div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="judul" name="judul" value="<?php if(isset($judul))echo $judul; ?>">
					</div>
				</div>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Instruksi</label>
					 <div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="instruksi" name="instruksi" value="<?php if(isset($instruksi))echo $instruksi; ?>">
					</div>
				</div>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Keterangan</label>
					 <div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="keterangan" name="keterangan" value="<?php if(isset($keterangan))echo $keterangan; ?>">
					</div>
				</div>
				
				<div class="form-group">	
						 <label class="col-sm-2 control-label">Tanggal</label>
						 <div class="col-sm-10">
							<input type="text" id="tanggalmulai" name="tanggalmulai" class="form_datetime form-control" value="<?php if(isset($tanggalmulai))echo $tanggalmulai;?>"> 
							sampai 
							<input type="text" id="tanggalselesai" name="tanggalselesai" onchange='validate_tanggal()' class="form_datetime form-control" value="<?php if(isset($tanggalselesai))echo $tanggalselesai;?>">
						</div>
					</div>
				
				<div class="form-group">	
				 	<label class="col-sm-2 control-label">Soal Diacak </label>
					<div class="col-md-10">
						<label class="checkbox"><input type="checkbox" name="israndom" value="1" <?php if ($israndom==1) { echo "checked"; } ?>>Ya</label>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"><!-- Is Publish --> </label>
					<div class="col-md-10">
						<!-- <label class="checkbox"><input type="checkbox" name="ispublish" value="1" <?php if ($ispublish==1) { echo "checked"; } ?>>Ya</label> --><br>
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="hidjadwalid" value="<?php if(isset($jadwalid))echo $jadwalid;?>">
						<input type="hidden" name="hidmkid" value="<?php if(isset($mkid))echo $mkid;?>">	
						<input type="submit" name="b_savepublish" onclick="save('publish')" id="submit-publish" value="Save and Publish" class="btn btn-primary">
						<input type="submit" name="b_draft" onclick="save('draft')" id="submit-draft" value="Save as Draft" class="btn btn-warning">
					</div>
				</div>
				
			</form>
		  </div>
		</div>
	</div>
<?php
$this->foot();
?>