<div id="soal-soal">
	<?php 
	$i = 0;
	$str = '';
	if(isset($soal)){
		
		foreach ($soal as $s) {						
			$str .= "<div class='col-md-8'>".$s->pertanyaan."";
			
					
			if($s->kategori != 'textarea'){
				$str .= "<ol type='A'>";
				foreach ($jawab as $j) {
					if($s->soal_id==$j->soal_id){
						$str .= "<li><input type='".$s->kategori."' name='soal".$i."' ";
						if($j->is_benar==1){
							$str .= "checked ";
						}
						$str .= " > &nbsp;".$j->keterangan." ";
						if($j->is_benar==1){
							$str .= "<span class='label label-warning'><small>*) Benar</small></span>";
						}
						$str .= "</li>";
					}
				}
				$str .= "</ol><br>";
			}
			else {
				if($jawab){
					foreach ($jawab as $j) {
						if($s->soal_id==$j->soal_id){
							$str .= "<textarea class='form-control'>".$j->keterangan."</textarea><br></div>";
						}
					}
				}
			}
			$str .= "</div><div class='col-md-4'><div class='dropdown pull-right'>
						<a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#' >Action <b class='caret'></b></a>
						<ul id='menu1' class='dropdown-menu ' role='menu' >
							<li>
							<a class='btn-edit-post' href=".$this->location('module/content/test/editsoal/'.$s->soal_id)." ><i class='fa fa-edit'></i> Edit</a>	
							</li>
							<li>
							<a onclick=doDelete('".$s->soal_id."') class='btn-edit-post' href='#'><i class='fa fa-trash-o'></i> Delete</a>	
							</li>
						</ul>
					</div></div>&nbsp;
					";
			$i++;
		}
		
	}
	else {
		$str .= "<div class='well'>Belum Terdapat Soal Untuk Mata Kuliah ini</div>";
	}
	
	echo $str;
	?>
</div>