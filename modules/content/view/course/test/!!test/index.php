<?php $this->head(); ?>

	
	<?php if(isset($status) and $status) : ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $statusmsg; ?>
	</div>
	<?php endif; ?>
		
	<?php
		
	 if( isset($posts) ) :	?>
		<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>No</th>
						<th>Last Modified</th>
						<th>Mata Kuliah</th>
						<th>Judul</th>
						<?php if($user!="Mahasiswa"){ ?>
						<th>Act</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
			
			<?php 
				$i = 1;
				if($posts > 0){
					foreach ($posts as $dt): 
			?>
				<tr valign=top>
					<td><?php echo $i ?></td>
					<td>
						<?php echo str_replace('-', '/',$dt->YMD) ?><br>
						<?php echo substr($dt->waktu, 0, -3) ?><br>
							<small> by :
							<em style='color : orange'><?php echo $dt->user ?></em><br>
							</small>
					</td>
					<td>
						<?php echo "<a class='btn-edit-post' href=".$this->location('module/content/test/detail/'.$dt->test_id)."><span class='text text-default'><strong>".$dt->namamk."</span></strong></a>" ?>
							<?php
							if($dt->is_publish=='1'){
								echo "<small><span class='label label-success'>Published</span></small>";
							}
							else {
								echo "<small><span class='label label-warning'>Not Published</span></small>";
							}
 							?>
							<?php
							if($dt->is_random=='1'){
								echo "<code>Random</code>";
							}
							
 							?>
					</td>
					<td><?php echo $dt->judul ?></td>
					<?php if($user!="Mahasiswa"){ ?>
					<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/content/test/edit/'.$dt->test_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td>
					<?php } ?>
				</tr>
		<?php 
		$i++;
		endforeach; 
		} ?>
		</tbody></table>
		
		
		
	 <?php else: ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>

<?php $this->foot(); ?>