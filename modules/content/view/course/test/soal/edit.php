<?php
if($user!="mahasiswa"){
$this->head();

if(isset($posts)&&$posts!=""){
	$header		= "Edit Question Test";
	
	foreach ($posts as $dt):
	$id			= $dt->hid_id;
	$testid		= $dt->testid;
	$katid		= $dt->kategori_id;
	$pertanyaan	= $dt->pertanyaan;
	$mkid		= $dt->mkditawarkan_id;
	$matkul		= $dt->mata_kuliah;
	$matid		= $dt->materi_id;
	$mat		= $dt->materi;
	$jadwalid	= $dt->jadwal_id;
	$jadwal		= $dt->jadwal;
	$soal_id	= $dt->soal_id;
	endforeach;
	$frmact 	= $this->location('module/akademik/test/soal/save');
	$edit		= 1;		
	
}elseif(isset($postsfrombank)&&$postsfrombank!=""){
	$header		= "Write New Question Test";
	
	foreach ($postsfrombank as $dt):
	$id			= "";
	$getid		= $dt->hid_id;
	$katid		= $dt->kategori_id;
	$pertanyaan	= $dt->pertanyaan;
	$mkid		= $dt->mkditawarkan_id;
	$matkul		= $dt->mata_kuliah;
	$matid		= $dt->materi_id;
	$mat		= $dt->materi;
	$jadwalid	= $dt->jadwal_id;
	$jadwal		= $dt->jadwal;
	$soal_id	= $dt->soal_id;
	endforeach;
	$newsoal	= 1;
	$bybank		= 1;
	
}else{
	$header		= "Write New Question Test";
	
	$id			= "";
	$katid		= "";
	//$testid		= "";
	$pertanyaan	= "";
	$newsoal	= 1;
	$israndomjawaban	=	"";
	$frmact 	= $this->location('module/akademik/test/soal/save');		
}


?>
<h2 class="title-page">Soal</h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
		<li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
		<li><a href="<?php echo $this->location('module/content/tugas/'); ?>">Tugas & Test</a></li>
		<li class="active">Write</li>
	</ol>
	<?php if(isset($newsoal)&&$newsoal==1){	?>
	<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/content/test/bank/soal/'.$testid); ?>" class="btn btn-primary">
    <i class="fa fa-plus icon-white"></i> Tambah Soal dari Bank Soal</a> 
    </div>
	<?php } ?>
	
	<div class="row">		
   	 	<div class="block-box">	
			<form id="upload-test-soal-form" class="form-horizontal">
				 
				 <div class="form-group">
					<label class="col-sm-2 control-label">Test</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php $uri_parent = $this->location('module/akademik/test/tampilkan_detail_test'); ?>
							<select class="form-control e9" name="test" id="select_test"  data-uri="<?php echo $uri_parent ?>">
								<option value='0'>Select Test</option>
							<?php 
								foreach($get_test as $dt):
									echo "<option class='sub_".$dt->test_id."' value='".$dt->hid_id."' ";
									if($testid == $dt->test_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								endforeach;
							?>					
							</select>
						</div>
					</div>
				</div>
				 
				<div id="detail">
					<?php 
						if(isset($matkul)){
							echo '<div class="form-group">
									<label class="col-sm-2 control-label">Mata Kuliah</label>
									<div class="controls">
										<div class="col-sm-10">
											<input type="text" class="form-control" disabled="disabled" value="'.$matkul.'"/>
											<input type="hidden" class="form-control" name="matakuliah" value="'.$mkid.'"/>
										</div>
									</div>
								 </div>';
						}
						if(isset($mat)){
							echo '<div class="form-group">
									<label class="col-sm-2 control-label">Materi</label>
									<div class="controls">
										<div class="col-sm-10">
											<input type="text" class="form-control" disabled="disabled" value="'.$mat.'"/>
											<input type="hidden" class="form-control" name="materi" value="'.$matid.'"/>
										</div>
									</div>
								 </div>';
						}
						
						if(isset($jadwal)){
							echo '<div class="form-group">
									<label class="col-sm-2 control-label">Jadwal Kelas</label>
									<div class="controls">
										<div class="col-sm-10">
											<input type="text" class="form-control" disabled="disabled" value="'.$jadwal.'"/>
											<input type="hidden" class="form-control" name="jadwal" value="'.$jadwalid.'"/>
										</div>
									</div>
								 </div>';
						}
					?>
				</div>
				 
				 <div class="form-group">
					<label class="col-sm-2 control-label">Test Category</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php echo '<select id="select_kategori" class="form-control e9" name="kategori" >'; ?>
								<option value='0'>Select Test Kategori</option>
							<?php 
								foreach($get_category as $dt):
									echo "<option class='sub_".$dt->kat_id."' value='".$dt->hid_id."' ";
									if($katid==$dt->hid_id){
										echo "selected";
									}
									echo ">".$dt->kategori."</option>";
								endforeach;
							?>					
						</select>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Pertanyaan</label>
					<div class="controls">
						<div class="col-sm-10">
							<textarea class="form-control ckeditor" id="pertanyaan" name='pertanyaan' ><?php if(isset($pertanyaan)) echo $pertanyaan; ?></textarea>
						</div>
					</div>
				</div>
				
				
				<?php if(isset($edit)||isset($bybank)){
					 if($paraminput!='textarea'){	
				?>
				<div class='form-group' id='israndomjawab' >
					<label class='col-sm-2 control-label'>Jawaban Diacak </label>
						<div class='col-md-10'>
							<label class='checkbox'><input type='checkbox' name='israndomjawaban' value='1' <?php if($randomanswer=='1'){echo "checked";} ?>>Ya</label>
						</div>
				</div>

				<span id='editparam'>
				<span id='editdelparam'>
				<div class="form-group">	
					<label class="col-sm-2 control-label"></label>
					<div class="col-md-10" >
				<?php 
				$i = 1;
				foreach ($jawaban as $j) {
					if($soal_id==$j->soal_id){ ?>
							<?php if($paraminput=='checkbox'&&$i=='x6')$i='6'; ?>
							<div id='removelink<?php echo $i; ?>'><span id='removedel<?php echo $i; ?>'>
							<?php if($paraminput=='checkbox'&&$i==5)$i='x5'; ?>
							<input type='<?php echo $paraminput; ?>' id='<?php echo $i; ?>' name='answertype' <?php if($paraminput == 'radio'){ echo "onclick='isbenar(this.id)'"; }elseif($paraminput == 'checkbox'){ echo "onclick='ischeck(this.id)'";}?>  <?php if($j->is_benar=='1')echo "checked" ?>> 
							<input type='text' name='answer[]' id='answer<?php echo $i; ?>' value="<?php echo $j->keterangan ?>">&nbsp;
							<input type='text' name='skor[]' placeholder='skor..' value="<?php echo $j->skor ?>" style='width:40px;'>&nbsp;
							<i id='benar<?php echo $i; ?>'>
								<?php if($j->is_benar=='1'){ ?>
								<i id="rem<?php echo $i; ?>"><span class="label label-warning">Jawaban Benar</span></i>
								<?php } ?>
							</i>
							<input type='hidden' id ='isbenarcheck<?php echo $i; ?>' name='isvalbenar[]' value='<?php if($j->is_benar=='1')echo $j->is_benar; ?>' >
							<?php if($paraminput=='checkbox'&&$i=='x5')$i='5'; ?>
							<input type='hidden' id = 'jawabanid<?php echo $i; ?>' name='hidJawabId[]' value='<?php if(isset($j->jawab_id))echo $j->jawab_id; ?>' >
							<span id='appendrem'>
							<a class='rema' onclick='removeattr(<?php echo $i; ?>)' >&nbsp;<i style='font-size:20px' class='fa fa-times-circle text text-default' title='Remove'></i></a>
							</span>
							<br><br>
							</span></div>
				<?php	}
					$i++;
					}
					?>
					<div id='newanswer'></div><button type='button' class='btn btn-danger' <?php if($paraminput == 'radio'){ echo "onclick='addradioanswer()'"; }elseif($paraminput == 'checkbox'){ echo "onclick='addcheckboxanswer()'";}?> ><i class='fa fa-plus'></i> Tambah Jawaban</button>
					</div>
				</div>
				</span></span>
				<?php } else { //---else param textarea?>
					<span id='editparam'>
					<div class="form-group" id='editdelparam'>
					<label class="col-sm-2 control-label"></label>
					<div class="controls">
						<div class="col-sm-10">
							<textarea id='answer1' name='essayanswer' class='form-control' ><?php foreach ($jawaban as $j) { echo $j->keterangan; } ?></textarea>
							<input type='hidden' name='hidJawabId' value='<?php if(isset($j->jawab_id))echo $j->jawab_id; ?>' >
						</div>
						</div>
					</div>	
					</span>			
				<?php }	?>
				
				<?php } elseif(isset($newsoal)) { ?>
					<div id='randomanswer'>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label"><span id="label-jawab"></span></label>
					<div class="col-md-10" >
						<div id="answertype"></div>
					</div>
				</div>	
				
				<?php } ?>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="submit" name="b_jenis" onclick="submit_tes_soal()" value="Submit" class="btn btn-primary">
						<input type="hidden" name="hidId" value="<?php if(isset($id)) echo $id;?>">
						<input type="hidden" name="newsoal" value="<?php if(isset($newsoal)) echo $newsoal;?>">
						<input type="hidden" name="banksoal" value="<?php if(isset($bybank)) echo $bybank;?>">
						<?php if(isset($bybank)){ ?>
						<input type="hidden" name="getId" value="<?php if(isset($getid)) echo $getid;?>">
						<input type="hidden" name="repo" value="<?php if(isset($repo)) echo $repo;?>">
						<input type="hidden" name="hidTestId" value="<?php echo $testid ?>" />
						<?php } ?>
						</div>
					</div>
				</div>				
			</form>
		  </div>
		</div>
		
		
		<?php if(isset($newsoal)){ ?>	
		<div class="head-title-box">
    <h3>
			<?php echo "SOAL"; ?>
		</h3>
    </div>
		<div id="soal-soal">
		<?php $this->view('course/test/test/soal.php', $data); ?>
		</div>
		<?php } elseif (isset($bybank)) { ?>
		<div class="head-title-box">
    <h3>
		<?php echo "SOAL"; ?>
		</h3>
    </div>
		<div id="soal-soal">
		<?php $this->view('course/test/test/soal.php', $data); ?>
		</div>
		<?php }
		?>
	</div>

<?php
$this->foot();
}
?>