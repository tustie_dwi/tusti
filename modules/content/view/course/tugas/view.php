<?php //$this->head(); 
$now = new DateTime();
$time = $now->format('Y-m-d H:i:s');


?>

	<div class="head-title-box">
    <h3>Tugas</h3>
	<?php if($role!="mahasiswa"){ ?>
    <span class="option-menu">
    	<a onclick="newtugas('<?php echo $jadwal_id ?>','<?php echo $mk ?>')" class="" title="New Tugas" href="#">
    	<i class="fa fa-pencil"></i> New Tugas</a>
    </span>
	 <?php } ?>
    </div>
   
		
	<?php
			
	 if( isset($posts) ) :	?>
		<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>&nbsp;</th>                  
					</tr>
				</thead>
				<tbody>
			
			<?php 
				$i = 1;
				if($posts > 0){
					foreach ($posts as $dt): 
			?>
				<tr>
					<td>
						<div class="col-md-3">
						<i class="fa fa-clock-o"></i>&nbsp;<?php echo $dt->last_update; ?><br>
							<small>by <?php echo $dt->user ?></small>
						</div>
					
						<div class="col-md-6">
						<a style='cursor:pointer' onclick="vw_tugas_('<?php echo $dt->jadwalid; ?>','<?php echo $dt->mk; ?>','<?php echo $dt->tugasid; ?>');" >
						<?php 
							echo "<span class='text text-default'><strong>".$dt->judul."</span></strong></a>" ;
							
							if(strtotime($dt->tgl_selesai) > time() && strtotime($dt->tgl_mulai) < time()){
								echo " <small><span class='label label-success'>Active</span></small>";
							}
							elseif(strtotime($dt->tgl_mulai) > time()){
								echo " <small><span class='label label-default'>Not Started Yet</span></small>";
							}
							else {
								echo " <small><span class='label label-danger'>Closed</span></small>";
							}
							
							echo "<br><small>".$dt->materi;							
							echo "&nbsp;<span class='text text-danger'><span class='fa fa-clock-o'></span> ".date("M d, Y H:i",strtotime($dt->tgl_mulai))." - ".date("M d, Y H:i",strtotime($dt->tgl_selesai))."</span></small>";
 							?>
						
						<?php
							
					?>
						</div>
						<?php if($role!="mahasiswa"){ ?>
						<div class="col-md-3">					
							<ul class='nav nav-pills'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a style='cursor:pointer' class='btn-edit-post' onclick="edittugas('<?php echo $dt->jadwalid; ?>','<?php echo $dt->mk; ?>','<?php echo $dt->tugasid; ?>');" ><i class='fa fa-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</div>
						<?php } ?>
					</td>		
				</tr>
		<?php 
		$i++;
		endforeach; 
		} ?>
		</tbody></table>
		
		
		
	 <?php else: ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>