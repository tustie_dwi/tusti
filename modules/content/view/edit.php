<?php 
$this->head();
if(isset($content)){
	$content_id		= $content->id;
	$content_page 	= $content->content_page;
	$content_title	= $content->content_title;
	$content_content= $content->content_content;
	$content_status	= $content->content_status;
	$content_date	= $content->content_date;
	$content_time	= $content->content_time;
	$content_category	= $content->content_category;
	$content_parent	= $content->parent_id;
	$content_urut	= $content->urut;
	$img			= $content->thumb_img;
	$comment		= $content->comment_status;
}else{
	$content_id		= "";
	$content_page 	= "";
	$content_title	= "";
	$content_content= "";
	$content_status	= "";
	$content_date	= "";
	$content_time	= "";
	$content_category='page';
	$content_parent	= 0;
	$content_urut	= 1;
	$img			= "";
	$comment		= 0;
}
?>
<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }
</style>


	<h2 class="title-page">Content</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/page'); ?>">Content</a></li>
	  <li class="active"><a href="#">Write</a></li>
	</ol>
	 <div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/content/page/write'); ?>" class="btn btn-primary">
		<i class="fa fa-pencil icon-white"></i> New Content</a> 
    </div>

<div class="row">
      <div class="col-md-12">  
		<form role="form" id="write-form" name="auth" method="post" action="<?php echo $this->location('module/content/page'); ?>"  enctype="multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $content_id; ?>" />
		<input type="hidden" name="content_author_id" id="content-author-id" value="<?php echo $user->username; ?>" />
       
            <div class="form-group">
                <div class="hide" id="notification-container"></div>
            </div>            
           <!-- <div class="form-group">
                <label  for="content_subtitle" >Content Identifier</label>
                <div class="controls">
                <input type="text" name="content_page" id="content_page" class="input-xlarge" value="<?php echo $content_page; ?>" placeholder="Content Identifier" style="width:98%" />     
                </div>
            </div><!-- /form-group -->                
            
			<div class="form-group">
                <label  for="content_title" >Category</label>               
				   <select name="content_category" class="form-control cmbmulti">
					<?php
						foreach($category as $dt):
							echo "<option value='".$dt->content_category."' ";
							if($content_category==$dt->content_category){ echo 'selected'; } 
							echo " >".$dt->keterangan."</option>";
						endforeach;
					?>
				   </select>
            </div>
			
			<div class="form-group">
                <label  for="content_title" >Parent Page</label>                
					<input type="hidden" class="form-control" id="e7" name="content_parent" value ="<?php echo $content_parent; ?>" />	
            </div>	

			<div class="form-group">
                <label  for="content_title" >No urut</label>              
					<input type="text" class="form-control" name="content_urut" value ="<?php echo $content_urut; ?>" />
            </div>				
			
            <div class="form-group">
                <label  for="content_title" >Title</label>             
                <input type="text" name="content_title" id="content_title" class="form-control" value="<?php echo $content_title; ?>" placeholder="Title"  />
            </div><!-- /form-group -->
            
            <div class="form-group">  				              
					<textarea name="content_content" class="ckeditor" id="keterangan" style="height:300px;" ><?php echo $content_content; ?></textarea>
            </div><!-- /form-group --> 
			
			 <div class="checkbox">
				<label>
				  <input type="checkbox" name="comment" value="1" <?php if($comment==1) echo "checked";?> > <b>Comments ON</b>
				</label>
			  </div>
			
			<?php
			if($img){
			?>
			<div class="form-group">  
				 <label  for="content_title" >Thumbnail Image</label>				                   
					<input type="hidden" id="hidimg" name="hidimg" value="<?php echo $img; ?>" />
					<img src="<?php echo $this->location($img); ?>" class="img-thumbnail">
            </div><!-- /form-group -->    
			<?php } ?>

			<div class="form-group">  
				 <label  for="content_title" >New Thumbnail Image</label>				                
					<input type="file" id="files" name="files"  />
					<output id="list"></output>
            </div><!-- /form-group -->         
			
            
            <div class="form-group">            
              	<div class="accordion-group">
                	<div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseDateTime">
                            <i class="icon-time"></i> <strong>Date and Time</strong>
                        </a>
                    </div>
                    <div id="collapseDateTime" class="accordion-body collapse">
                    	<div class="accordion-inner">
                            <div class="form-group">
                                
                                <div style="margin-right:10px;">
                                <label>Date
                                    <input type="text" name="content_date" size="12" value="<?php echo $content_date; ?>" id="date-time" class="date" />
                                </label>
                                </div>
                                <div>
                                <label>Time
                                    <input type="text" name="content_time" size="12" value="<?php echo $content_time; ?>" />
                                </label>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div><!-- /accordion-group -->
                        
            </div><!-- /form-group -->
            
   
               <div class="form-actions">
                
                	<div class="btn-group pull-right">
                	  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                	    Action
                	    <span class="caret"></span>
                	  </a>
                	  <ul class="dropdown-menu">
                	  
                	    <!--<li><a id="btn-discard"><i class="icon-minus"></i> Discard</a></li>-->
                	    <li><a href="<?php echo $this->location('module/content/page'); ?>"><i class="icon-list"></i> Back to Content List</a></li>
                	    <li><a href="<?php echo $this->location('module/content/page/write'); ?>"><i class="icon-pencil"></i> Write New Content</a></li>
                	    
                	  </ul>
                	</div>      	                          
                    
                    
                    <button class="btn btn-primary" data-loading-text="Publishing..." id="btn-publish" name="b_pub">Save and Publish</button>
                    <button class="btn btn-warning" data-loading-text="Saving..." id="btn-draft" name="b_draft">Save as Draft</button>
                </div>    
           
    
        	
      </form>
	  </div>    
    </div><!-- /container -->
	<script>
  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
	var f = files[0];
        var reader = new FileReader();
         
          reader.onload = (function(theFile) {
                return function(e) {
                  document.getElementById('list').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="80" />'].join('');
                };
          })(f);
           
          reader.readAsDataURL(f);
    }
  document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

<?php $this->foot(); ?>