<?php 
 $this->head();	  
 $header="Detail Mahasiswa";
 if($staff==""){
 	$breadcrumb="Pembimbing Akademik";
 }
 else $breadcrumb="Mahasiswa Bimbingan";
 
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<div class="row">
		<div class="col-md-12">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/content/pembimbing'); ?>"><?php echo $breadcrumb ?></a></li>
		  <li class="active"><a href="#"><?php echo $header;?></a></li>
		</ol>
		 <?php
		 
		 if(isset($status) and $status) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $statusmsg; ?>
			</div>
		<?php 
		endif; 
		if(isset($biodata) && ($biodata)){			
				
				
			if($biodata->foto){ $foto=$this->config->file_url_view."/".$biodata->foto; }else{ $foto=$this->config->default_pic;}
			
			if($biodata->jenis_kelamin == 'P'){
				$jenis_kelamin = 'Perempuan';
			}else $jenis_kelamin = 'Laki-laki';
					?>
					<div class="media">
						<a class="pull-left" style="text-decoration:none" href="#" >
							<img class="media-object img-thumbnail" src="<?php echo $foto; ?>"  width="200" >
						</a>
					  <div class="media-body">
							<?php
							echo "<h2><a href='#' class='text text-default'>".ucWords(strToLower($biodata->nama))."</a>
									<span class='label label-success'>".$biodata->angkatan."</span> <span class='label label-danger'>".ucWords($biodata->is_aktif)."</span>
									<span class='label label-default'>".$biodata->cabang_id."</span></h2><small>";
							echo "<code>".$biodata->nim."</code>&nbsp;<span class='text text-default'>".$biodata->fakultas."</span>&nbsp; <span class='text text-danger'>".$biodata->prodi."</span> <br>";
							echo "<i class='fa fa-map-marker'></i> ".$biodata->alamat."&nbsp; ";
							if($biodata->telp){
								echo "<i class='fa fa-phone'></i> ".$biodata->telp."&nbsp; ";
							}
							
							if($biodata->hp){
								echo "<i class='fa fa-mobile'></i> ".$biodata->hp."&nbsp; ";
							}
							
							if($biodata->email){
								echo "<i class='fa fa-envelope'></i> ".$biodata->email."&nbsp; ";
							}
							echo "</small>";
							?>
							<br>
							<?php if($biodata->nama_ortu){ ?>
							
							<h4><span class="text text-info"><?php echo ucWords($biodata->nama_ortu) ?></span> <small>orang tua</small></h4>
							<small>
							<?php if($biodata->alamat_ortu){
								echo "<i class='fa fa-map-marker'></i> ".$biodata->alamat_ortu."&nbsp; ";
							}
							
							if($biodata->telp_ortu){
								echo "<i class='fa fa-phone'></i> ".$biodata->telp_ortu."&nbsp; ";
							}
							
							if($biodata->hp_ortu){
								echo "<i class='fa fa-mobile'></i> ".$biodata->hp_ortu."&nbsp; ";
							}
							
							if($biodata->email_ortu){
								echo "<i class='fa fa-envelope'></i> ".$biodata->email_ortu."&nbsp; ";
							} ?>
							</small>
							<?php } ?>
							<?php
							if($posts){
								?>
								<h3>Informasi Akademik</h3>								
								<table class='table table-hover'>
								<thead>
									<tr>
										<th>Semester</th>
										<th>Jumlah SKS</th>
										<th>IP</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									
										foreach ($posts as $dt): 
										?>
											<tr id="post-<?php echo $dt->mhs_id; ?>" data-id="<?php echo $dt->mhs_id; ?>" valign=top>
												<?php if($staff){ ?>
													<td>
														<?php 
															echo "<a style='cursor: pointer;text-decoration:none;' onclick=khs('".$dt->tahun_akademik."','".$dt->mahasiswa_id."')>".$dt->tahun."</a>"; 
														?>
													</td>
													<td>
														<?php if($dt->sks!=""){
																echo $dt->sks;
															  }else echo "0"; ?>
													</td>
													<td>								
														<?php
															echo number_format($dt->ipk,2);
														?>
													</td>
													
												<?php } ?>
											</tr>
											<?php
											$i++;
										 endforeach; 
									 ?>
								</tbody>
								</table>
								<?php
							}
							
							?>
					  </div>
				</div>
					
		<?php	
			}
				
		?>
    </div>
    </div> <!-- row -->
<?php
$this->foot();
?>