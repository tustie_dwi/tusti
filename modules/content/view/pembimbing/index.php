<?php 
 $this->head();	  
 if($staff=="0"){
 	$header="Pembimbing Akademik";
 }
 else $header="Mahasiswa Bimbingan";
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<div class="row">
		<div class="col-md-12">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li class="active"><a href="#"><?php echo $header;?></a></li>
		</ol>
		 <?php				
		 if( isset($posts) ) :	?>			
			<table class="table table-hover example">
					<thead>
						<tr>							
							<th>&nbsp;</th>							
						</tr>
					</thead>
					<tbody>
					<?php 	
					if($posts){
					foreach ($posts as $dt):  ?>
						<tr>
							<td>
							<?php 
							if($staff){
								if($dt->foto_mhs){ $foto=$this->config->file_url_view."/".$dt->foto_mhs; }else{ $foto=$this->config->default_pic;}
									
									?>
									
									<div class="media">
										<a class="pull-left" style="text-decoration:none" href="#" onclick = "detail('<?php echo $dt->mahasiswa_id?>')">
											<img class="media-object img-thumbnail" src="<?php echo $foto; ?>"  width="50" height="50">
										</a>
									  <div class="media-body">
										<?php
										echo "<a href='#' onclick = 'detail(".$dt->mahasiswa_id.")' class='text text-default'><b>".ucWords(strToLower($dt->nama))."</b></a>
											<small><span class='text text-danger'>".$dt->prodi."</span></small>
											&nbsp;<span class='label label-success'>".$dt->angkatan."</span><br>";
										echo $dt->nim. "<span class='label label-normal'>".ucWords($dt->tahun_mulai)."</span>";
										?>
									  </div>
									</div>
								
									
									<?php
									
								}else{
									if($dt->foto_dosen){ $foto=$this->config->file_url_view."/".$dt->foto_dosen; }else{ $foto=$this->config->default_pic;} 
									?>
									
									<div class="media">
										<a class="pull-left" style="text-decoration:none" href="#" onClick = "#">
											<img class="media-object img-thumbnail" src="<?php echo $foto; ?>"  width="50" height="50">
										</a>
									  <div class="media-body">
										<?php
										echo "<a href='#' class='text text-default'><b>".ucWords(strToLower($dt->dosen))."</b></a>
											<small><span class='text text-danger'>".$dt->email_dosen."</span></small>
											&nbsp;<span class='label label-success'>".$dt->tlp_dosen."</span><br>";
										echo $dt->nik. "<span class='label label-normal'>".ucWords($dt->alamat_dosen)."</span>";
										?>
									  </div>
									</div>								
							
							
							<?php } ?>
							</td>
						</tr>
						<?php endforeach; 
						}?>
					</tbody>
			</table>
		<?php
		 else: 
		 ?>
		 	<div class="well">Sorry, no content to show</div>
		<?php endif; ?>
    </div>
    </div> <!-- row -->
<?php
$this->foot();
?>