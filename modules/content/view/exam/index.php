<?php $this->head();
$header			= "Tugas & Test";	
?>
  
	<div class="row">
	<h2 class="title-page"><?php echo $header?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/tugas'); ?>">Tugas & Test</a></li>
	  <li class="active">Data</li>
	</ol>

	<div class="row">
		<div class="col-md-12">	
			<form name="frmDosen" id="frm" class="form-inline" method=post action="<?php echo $this->location('module/content/tugas')?>" >	
									
					<div class='control-group'>
						<div class="controls" style="text-align:right">Semester 
							<select name="cmbsemester" onChange='form.submit();' class='cmbmulti span12 populate'>
								<option value="-">Semester</option>
								<?php
								foreach($semester as $dt):
									echo "<option value='".$dt->inf_semester."' ";
									if($semesterid==$dt->inf_semester){
										echo "selected";
									}
									echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
								endforeach;
								?>
							</select><br>
						</div>					
			</form>	
		</div>
		
		<?php if(isset($test) || isset($tugas)){ ?>
				<ul class="nav nav-tabs" id="writeTab">
					<li class="active"><a href="#test">Test</a></li>
					<li><a href="#tugas">Tugas</a></li>
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane active" id="test"><br>
						<?php 
							if(isset($test)){
							$this->view('exam/test.php', $data); 
							}else{ ?>
							<div class="span3" align="center" style="margin-top:20px;">
								<div class="well">Sorry, no content to show</div>
							</div>	
						<?php }
						?>
					</div>
					<div class="tab-pane" id="tugas"><br>
						<?php 
							if(isset($tugas)){
							$this->view('exam/tugas.php', $data);
							}else{ ?>
							<div class="span3" align="center" style="margin-top:20px;">
								<div class="well">Sorry, no content to show</div>
							</div>	
						<?php }
						?>
					</div>
				</div>
			</div>
		<?php }else{ ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php 
			}
			?>
		
		</div>
</div>
<?php
	function get_diff_time($date1){
		$date1 = strtotime($date1);
		$date2 = time();
		
		$subTime = $date1 - $date2;
		$y = ($subTime/(60*60*24*365));
		$d = ($subTime/(60*60*24))%365;
		$h = ($subTime/(60*60))%24;
		$m = ($subTime/60)%60;
		
		if($subTime > 0){
			echo "<small><span class='text text-danger'>";
			echo $d." hari,\n";
			echo $h." jam,\n";
			echo $m." menit lagi\n";
			echo "</span></small>";
			return 1;
		}
		else{
			echo "<small><span class='label label-danger'>*</span></small>";
			return 0;
		}
	}
?>
<?php $this->foot(); ?>