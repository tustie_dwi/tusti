<table class='table table-hover example' >
	<thead>
		<tr>
			<th>&nbsp;</th>
			
			<?php if($role != 'mahasiswa'){ ?>
				<th>&nbsp;</th>
			<?php } ?>
		</tr>
	</thead>

	<tbody>
		<?php foreach($test as $dt){ ?>
			<tr><td>
				<div class="col-md-3">
					<?php if($role == 'mahasiswa'){ ?>
						<?php echo "<b>".$dt->kode_mk."</b>"; ?>
					<?php } else{ ?>
						<?php echo "<a class='btn-edit-post' href=".$this->location('module/content/test/detail/'.$dt->test_id)."><span class='text text-default'><strong>" . $dt->kode_mk . '</span></strong></a>' ?>
						
					<?php } 
					echo  $dt->nama_mk;
					if($dt->judul){
						echo " <br> <code>".$dt->judul."</code>";
					}
					 ?>
				</div>
				<div class="col-md-9">
					<?php if($role == 'mahasiswa'){ ?>
					<a title="Kerjakan Test" href="<?php echo $this->location('module/content/test/confirm/') . $dt->test_id ?>"><span class='text text-info'><?php echo $dt->judul_test ?></span></a>
					<?php }else{ ?>
					<a title="Lihat Soal" href="<?php echo $this->location('module/content/test/soal/') . $dt->test_id ?>"><span class='text text-info'><?php echo $dt->judul_test ?></span></a>
					<?php }
						if($dt->is_publish == 1) echo " <small><span class='label label-success'>Published</span></small>"; else echo "<small><span class='label label-warning'>Not Pulished</span></small>";
					if($dt->is_random == 1) echo " <small><span class='label label-normal'><b>*</b> soal acak</span></small>";
						echo "<br><small><i class='fa fa-clock-o'></i>&nbsp;" . date("M d, Y H:i", strtotime($dt->tgl_mulai)) . "</i> - ";
						echo  date("M d, Y H:i", strtotime($dt->tgl_selesai))."&nbsp; </small>  " ;
						get_diff_time($dt->tgl_selesai);
					?>				
					
				</div>						
				</td>
				<?php if($role != 'mahasiswa'){ ?>
					<td><a class="btn btn-default" href="<?php echo $this->location('module/content/test/peserta/') . $dt->test_id ?>"><i class="fa fa-share-square"></i> Lihat Peserta</a></td>	
				<?php } ?>
			</tr>
		<?php } ?>
	</tbody>
</table>