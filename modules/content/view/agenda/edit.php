<?php
$this->head();


if(isset($event)&&($event)){
	$header		= "Edit Agenda";	

	//foreach ($agenda as $dt):
		$id			= $event->agenda_id;
		$jenisid	= $event->jenis_kegiatan_id;
		$mulai		= $event->tmulai;
		$selesai	= $event->tselesai;
		$judul		= $event->judul;
		$note		= $event->keterangan;
		$lokasi		= $event->lokasi;
		$ruang		= $event->inf_ruang;
		$ruangid	= $event->inf_ruang;
		$infpeserta	= $event->inf_peserta;
		$infhari	= $event->inf_hari;
		$isallday	= $event->is_allday;
		$isdelete	= $event->is_delete;
		$jenisagenda= $event->jenis_agenda;
		$penyelenggara=$event->penyelenggara;
		$unitid		= $event->unit_id;
		$infundangan= $event->inf_undangan;
		$gruppeserta= $event->group_peserta;
		$img		= "";
	//endforeach;
}else{
	$header		= "Write New Agenda";	

	
	$id			= "";
	$jenisid	= "";
	$mulai		= "";
	$selesai	= "";
	$judul		= "";
	$note		= "";
	$ruangid	= "";
	$ruang		= "";
	$infpeserta="";
	$infhari	= "";
	$lokasi		= "";
	$isallday	= 0;
	$isprivate	= 0;
	$isdelete	= 0;
	$penyelenggara="";
	$unitid		= "-";
	$infundangan= "";
	$gruppeserta= "";
	$jenisagenda= "fakultas";
	$img		= "";
}

$data['infundangan'] = $infundangan;
$data['gruppeserta'] = $gruppeserta;
	
?>
<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }
</style>

<h2 class="title-page">Event</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/content/page'); ?>">Content</a></li>
  <li><a href="<?php echo $this->location('module/content/event'); ?>">Event</a></li>
  <li class="active"><a href="#">Data</a></li>
</ol>
 <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/content/event/write'); ?>" class="btn btn-primary">
	<i class="fa fa-pencil icon-white"></i> New Event</a> 
</div>

<div class="row">	
	<form method="post"  action="<?php echo $this->location('module/content/event/save') ?>"  enctype="multipart/form-data" >
		<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="fa fa-gears"></i> General Info</div>
			 <div class="panel-body">
				<div class="form-group">
					<label>Jenis Kegiatan</label>
					<select name="cmbjenis" class="form-control"><option value="lain">Please Select</option>
						<?php
						if($jenis){
							foreach($jenis as $dt):
								echo "<option value='".$dt->id."' ";
								if($jenisid==$dt->id){
									echo "selected";
								}
								echo " > ".$dt->value."</option>";
							endforeach;		
						}						
						?>
					</select>
				</div>
				
				<div class="form-group">
					<label>Tgl</label>
					<input type="text" class="form-control form_datetime" name="from" value="<?php echo $mulai ?>" > sampai  
					<input type="text" name="to" value="<?php echo $selesai ?>" class="form-control form_datetime">	
					<label class="checkbox">
					<input type="checkbox" value="1" name="allday" <?php if($isallday==1){ echo "checked"; } ?>>All Day Event</label>
				</div>
				<div class="form-group">
					<label>Day of Event</label>
					<div>
					<?php
					$chari = explode(",", $infhari);			
								
					$i=0;
					$skip = false;
					
					for($i = 0; $i < count($hari); $i++){						
						$skip=false;
						if($infhari){								
							for($j=0;$j<count($chari);$j++){	
								
								if($chari[$j] == $hari[$i]->value){
									echo "<label><input type=checkbox name='chkhari[]' value='".$hari[$i]->value."' checked";
									
									$skip=true;
									break;										
								}
							}
							if(!$skip) echo "<label><input type=checkbox name='chkhari[]' value='".$hari[$i]->value."' ";								
						}else{
							echo "<label><input type=checkbox name='chkhari[]' value='".$hari[$i]->value."' ";
						}
						
						echo "> ".ucFirst($hari[$i]->value)."</label> ";
					}
					?>
					</label>
					</div>
				</div>

				<div class="form-group">
					<label>Judul</label>
					<input type="text" name="judul" value="<?php echo $judul; ?>" class="form-control">
				</div>
				
				<div class="form-group">
					<label>Jenis Agenda</label>
					<div>
						<label><input type="radio" name="jenis" value="pribadi" <?php if($jenisagenda=='pribadi') echo "checked"; ?>> Pribadi</label>&nbsp;
						<label><input type="radio" name="jenis" value="unit" <?php if($jenisagenda=='unit') echo "checked"; ?>> Unit</label>&nbsp;
						<label><input type="radio" name="jenis" value="fakultas" <?php if($jenisagenda=='fakultas') echo "checked"; ?>> Fakultas</label>
					</div>
				</div>
				
				<?php
				if($img){
				?>
				<div class="form-group">  
					 <label  for="content_title" >Thumbnail Image</label>				                   
						<input type="hidden" id="hidimg" name="hidimg" value="<?php echo $img; ?>" />
						<img src="<?php echo $this->location($img); ?>" class="img-thumbnail">
				</div><!-- /form-group -->    
				<?php } ?>

				<div class="form-group">  
					 <label  for="content_title" >New Thumbnail Image</label>				                
						<input type="file" id="files" name="files"  />
						<output id="list"></output>
				</div><!-- /form-group -->       
				
			</div>
		</div>
		</div>
		<div class=" col-md-6">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#lokasi" data-toggle="tab">Keterangan</a></li>
				<li><a href="#pemateri" data-toggle="tab">Pemateri</a></li>
				<li><a href="#peserta" data-toggle="tab">Peserta</a></li>
				<li><a href="#undangan" data-toggle="tab">Undangan</a></li>
				<li><a href="#panitia" data-toggle="tab">Panitia</a></li>
			</ul>
			<div class="tab-content">
				
				<div class="tab-pane active" id="lokasi">	
					<div class="form-group">
						<label>Penyelenggara</label>
						<textarea name='penyelenggara' cols='80' class='form-control' required><?php echo $penyelenggara ?></textarea>
					</div>

					<div class="form-group">
						<label>Unit Penyelenggara</label>
						<select name='cmbunit' id='cmbunit'  class='form-control select2-multiple'>
						<option value='-'>None</option>
						<?php
							foreach($unit as $dt):
								echo "<option value='".$dt->id."' ";
								if($unitid==$dt->id){
									echo "selected";
								}
								echo ">".$dt->value."</option>";
							endforeach;
						?>							
						</select>
					</div>

					<div class="form-group">
						<label>Keterangan</label>
						<textarea cols='80' class='ckeditor' id='keterangan' name='keterangan' rows='10'><?php echo $note ?></textarea>
					</div>
					

					<?php 							
					$data['namaruang']	= $ruang;
					$data['hidruang'] = $ruangid;
					$data['lokasi']  = $lokasi;
					
					$this->view('agenda/lokasi.php', $data);	
					
					?>
				</div>
				<div class="tab-pane" id="pemateri">	
					<?php 
					
					$this->view('agenda/pemateri.php', $data);	
					
					?>
				</div>
				<div class="tab-pane" id="peserta">	
					<?php 
					
					$data['infpeserta']	= $infpeserta;
					
					$this->view('agenda/peserta.php', $data);	
					
					?>
				</div>
				<div class="tab-pane" id="undangan">	
					<?php 
					$this->view('agenda/undangan.php', $data);										
					?>
				</div>
				<div class="tab-pane" id="panitia">	
					<?php 
					
					$this->view('agenda/panitia.php', $data);	
					
					?>
				</div>
			</div>
		
		<div class="form-group">
			<div class="form-actions">
				<input type="hidden" name="hidid" value="<?php echo $id ?>">
				<input type="submit" name="b_agenda" id="b_agenda" value="Save & Published" class='btn btn-primary'> &nbsp; 
				<input type="submit" name="b_draft" id="b_draft" value="Save as Draft" class='btn btn-warning'> &nbsp;
				<a class='btn btn-default' href="<?php echo $this->location('module/content/event')?>"><i class="fa fa-ban"></i> Cancel</a>
			</div>
		</div>
		</div>
	</form>		
</div>
	<script>
	  function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object
		var f = files[0];
			var reader = new FileReader();
			 
			  reader.onload = (function(theFile) {
					return function(e) {
					  document.getElementById('list').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="80" />'].join('');
					};
			  })(f);
			   
			  reader.readAsDataURL(f);
		}
	  document.getElementById('files').addEventListener('change', handleFileSelect, false);
	</script>
<?php
$this->foot();

?>
