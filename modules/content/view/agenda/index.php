	<?php $this->head(); ?>


<h2 class="title-page">Event</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/content/page'); ?>">Content</a></li>
  <li><a href="<?php echo $this->location('module/content/event'); ?>">Event</a></li>
  <li class="active"><a href="#">Data</a></li>
</ol>
 <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/content/event/write'); ?>" class="btn btn-primary">
	<i class="fa fa-pencil icon-white"></i> New Event</a> 
</div>

<div class="row">
	<div class="col-md-12">
	<?php
	if( isset($event) ) :		
	 ?>
		<table class="table table-hover" id="example"  data-id="module/content/event">
				<thead>
					<tr>
						<th>Modified</th>
						<th>Kegiatan</th>
					</tr>
				</thead>
				<tbody>
    <?php				
			$i = 1;			
			foreach ($event as $dt): 
				?>
				<tr id="post-<?php echo $dt->agenda_id ?>" data-id="<?php echo $dt->agenda_id ?>" valign="top">
				<td><small><i class='fa fa-clock-o'></i>&nbsp;<?php echo date("M d, Y H:i", strtotime($dt->last_update)); ?><br>by <?php echo $dt->name; ?></small></td>
				<td>
				<div class="col-md-9">
					<a href="<?php echo $this->location('module/content/event/viewevent/'.$dt->id)?>" class='text text-default'><b><?php echo $dt->judul; ?></b></a>
					<?php
					switch($dt->status_agenda){
						case 'publish':
							echo "<span class='label label-success'>Published</span> ";
						break;
						case 'draft':
							echo "<span class='label label-warning'>* Draft</span> ";
						break;
					}
					
					echo "<span class='label label-danger'>".ucWords($dt->jenis_agenda)."</span> ";
					
					if($dt->is_delete==1){
						echo "<span class='label label-default'>* Need to delete</span> ";
					}
					
					echo "<br><small><i class='fa fa-map-marker'></i> ".$dt->lokasi;
					if($dt->inf_ruang){
						echo " ".$dt->inf_ruang."</small>";
					}else{
						echo "</small>&nbsp;";
					}
					
					echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-calendar-o'></i>&nbsp;";
							if(date("M d, Y",strtotime($dt->tmulai)) == date("M d, Y",strtotime($dt->tselesai))){
								echo date("M d, Y",strtotime($dt->tmulai));		
							
							}else{
								if(date("M, Y",strtotime($dt->tmulai)) == date("M, Y",strtotime($dt->tselesai))){
									echo date("M d",strtotime($dt->tmulai))." - ".date("d, Y",strtotime($dt->tselesai));
								}else{
									echo date("M d",strtotime($dt->tmulai))." - ".date("M d, Y",strtotime($dt->tselesai));
								}
							}
					
					echo "&nbsp; <i class='fa fa-clock-o'></i> ".date("H:i",strtotime($dt->tmulai))." - ".date("H:i",strtotime($dt->tselesai))."</span></small>";
					
					
					echo "<br><small>".$dt->unit_penyelenggara."</small>";
					
					?>
				</div>
				
				<div class="col-md-3">
					<?php
					if($this->coms->authenticatedUser->level==1 || $this->coms->authenticatedUser->level==6){
					echo "
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href=".$this->location('module/content/event/edit/'.$dt->id)."><i class='fa fa-pencil'></i> Edit</a>	
									</li>	
									<li>
										<a id='".$dt->agenda_id."' class='btn-delete-post' ><i class='fa fa-trash-o'></i> Delete</a>
									</li>										
								  </ul>
								</li>
							</ul>";
					}else{
						//if(($this->coms->authenticatedUser->username==$dt->user)||($this->coms->authenticatedUser->level==$dt->level)){
							echo "
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href=".$this->location('module/content/event/edit/'.$dt->id)."><i class='fa fa-pencil'></i> Edit</a>	
									</li>									
								  </ul>
								</li>
							</ul>";
						//}
					}
					?>
				</div>				
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<?php
	 else: 
	 ?>   
	    <div class="well">Sorry, no content to show</div>
    <?php endif; ?>
</div>
</div>

<?php $this->foot(); ?>