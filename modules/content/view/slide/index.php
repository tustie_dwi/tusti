<?php $this->head(); ?>


	<h2 class="title-page">Content Slide</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/page'); ?>">Content</a></li>
	    <li><a href="<?php echo $this->location('module/content/webslide'); ?>">Slide</a></li>
	  <li class="active"><a href="#">Data</a></li>
	</ol>
	 <div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/content/webslide/write'); ?>" class="btn btn-primary">
		<i class="fa fa-pencil icon-white"></i> New Slide</a> 
    </div>

	<div class="row">
		<div class="col-md-12">
			<?php if( isset($posts) ) : ?>
			<table class="table example">
				<thead>
					<tr>
					<th>Modified</th>
					<th>Slide</th>
					<th>&nbsp;</th>
					</tr>
				</thead>
			<?php if(true) : foreach($posts as $post) { ?>
				<tr id="post-<?php echo $post->id; ?>" data-id="<?php echo $post->id; ?>">
					<td><small><i class="fa fa-clock-o"></i> <?php echo $post->content_modified; ?></small></td>
					<td> 
						<div class="media">
						  <a class="pull-left" href="#">
							<img class="media-object img-thumbnail" src="<?php echo $this->location($post->file_loc); ?>" width="60" >
						  </a>
						  <div class="media-body">
							<b class="media-heading"><?php echo $post->content_title; ?></b>
							<code><?php echo $post->img_link; ?></code>
							<?php if($post->content_status == 'published') 
								echo '<span class="label label-success">Published</span>'; 
								else echo '<span class="label label-warning">Draft</span>';
						
								?>
						  </div>
						</div>				
					</td>
					<td>					
						<ul class="nav nav-pills" style="margin:0;">
						<li class="dropdown">
						  <a class="dropdown-toggle btn btn-table" id="drop4" role="button" data-toggle="dropdown" href="#">Action <b class="caret"></b></a>
						  <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
							<li>
							<a class="btn-edit-post" href="<?php echo $this->location('module/content/webslide/edit/'.$post->id); ?>"><i class="fa fa-pencil"></i> Edit</a>	
							</li>
							<li>
							<a class="btn-delete-slide"><i class="fa fa-trash-o"></i> Delete</a>
							</li>
						  </ul>
						</li>
						</ul>  
				   
					</td>
				</tr>
				<?php	
				} endif;
			?>
			</table>
			<?php else: ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php endif; ?>
		</div>
	</div>

<!--</div>-->

<?php $this->foot(); ?>