<?php 
$this->head();
if(isset($content)){
	$content_id		= $content->id;
	$content_page 	= $content->img_link;
	$content_title	= $content->content_title;
	$content_content= $content->keterangan;
	$img			= $content->file_loc;
	$content_status	= $content->content_status;
	$content_date	= date("Y-m-d", strtotime($content->content_modified));
	$content_time	= date("H:i", strtotime($content->content_modified));
}else{
	$content_id		= "";
	$content_page 	= "";
	$content_title	= "";
	$content_content= "";
	$img			= "";
	$content_status	= "";
	$content_date	= "";
	$content_time	= "";
	
}
?>
<style>
  .thumb {
    height: 75px;
    border: 1px solid #000;
    margin: 10px 5px 0 0;
  }
</style>


	<h2 class="title-page">Content Slide</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/page'); ?>">Content</a></li>
	    <li><a href="<?php echo $this->location('module/content/webslide'); ?>">Slide</a></li>
	  <li class="active"><a href="#">Write</a></li>
	</ol>
	 <div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/content/webslide/write'); ?>" class="btn btn-primary">
		<i class="fa fa-pencil icon-white"></i> New Slide</a> 
    </div>

<div class="row">
      <div class="col-md-12">  
		<form id="write-form" name="auth" method="post" action="<?php echo $this->location('module/content/webslide'); ?>" class="form-inline" enctype="multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?php echo $content_id; ?>" />
		      
            <div class="control-group">
                <div class="hide" id="notification-container"></div>
            </div>            
          			
            <div class="control-group">
                <label for="content_title" >Title</label>
                <div class="controls">
                <input type="text" name="content_title" id="content_title" class="span12" value="<?php echo $content_title; ?>" placeholder="Title" style="width:98%" /> <br><br>
                </div>
            </div><!-- /control-group -->
            
            <div class="control-group">  
				<label>Note</label>
				<div class="controls">                    
					<textarea name="content_content"  class="ckeditor" id="keterangan" cols="100" rows="2"><?php echo $content_content; ?></textarea>
				</div>                 
            </div><!-- /control-group --> 
			
			<?php
			if($img){
			?>
			<div class="control-group">  
				 <label class="control-label" for="content_title" >Slide</label>
				<div class="controls">                    
					<input type="hidden" id="hidimg" name="hidimg" value="<?php echo $img; ?>" />
					<img src="<?php echo $this->location($img); ?>" class="img-thumbnail">
				</div>                 
            </div><!-- /control-group -->    
			<?php } ?>

			<div class="control-group">  
				 <label class="control-label" for="content_title" >New Slide</label>
				<div class="controls">                    
					<input type="file" id="files" name="files"  />
					<output id="list"></output>
				</div>                 
            </div><!-- /control-group -->         
			
            
            <div class="control-group">            
              	<div class="accordion-group">
                	<div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseDateTime">
                            <i class="icon-time"></i> <strong>Date and Time</strong>
                        </a>
                    </div>
                    <div id="collapseDateTime" class="accordion-body collapse">
                    	<div class="accordion-inner">
                            <div class="control-group">
                                <div class="controls">
                                <div style="margin-right:10px;">
                                <label>Date
                                    <input type="text" name="content_date" size="12" value="<?php echo $content_date; ?>" id="date-time" />
                                </label>
                                </div>
                                <div>
                                <label>Time
                                    <input type="text" name="content_time" size="12" value="<?php echo $content_time; ?>" />
                                </label>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div><!-- /accordion-group -->
                        
            </div><!-- /control-group -->
            
   
               <div class="form-actions">
                
                	<div class="btn-group pull-right">
                	  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                	    Action
                	    <span class="caret"></span>
                	  </a>
                	  <ul class="dropdown-menu">
                	  
                	    <!--<li><a id="btn-discard"><i class="icon-minus"></i> Discard</a></li>-->
                	    <li><a href="<?php echo $this->location('module/content/webslide'); ?>"><i class="icon-list"></i> Back to Content List</a></li>
                	    <li><a href="<?php echo $this->location('module/content/webslide/write'); ?>"><i class="icon-pencil"></i> Write New Content</a></li>
                	    
                	  </ul>
                	</div>      	                          
                    
                    
                    <button class="btn btn-primary" data-loading-text="Publishing..." id="btn-publish" name="b_pub">Save and Publish</button>
                    <button class="btn btn-warning" data-loading-text="Saving..." id="btn-draft" name="b_draft">Save as Draft</button>
                </div>    
           
    
        	<div class="col-md-12" id="save-status">
            Content status: 
			<?php 
				if($content_status == 'published')
					echo '<span class="label label-success">Published</span>';
				else echo '<span class="label label-warning">Draft</span>';
			?>
            </div>
      </form>
	  </div>    
    </div><!-- /container -->
	<script>
  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
	var f = files[0];
        var reader = new FileReader();
         
          reader.onload = (function(theFile) {
                return function(e) {
                  document.getElementById('list').innerHTML = ['<img src="', e.target.result,'" title="', theFile.name, '" width="400" />'].join('');
                };
          })(f);
           
          reader.readAsDataURL(f);
    }
  document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

<?php $this->foot(); ?>