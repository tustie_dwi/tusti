<?php $this->head(); 

		foreach($test as $key){
			$materi = $key->judul;
			$judul = $key->judul_test;
		}
?>
<h2 class="title-page">Test <?php echo $materi ?> - <?php echo $judul ?></h2>
	<ol class="breadcrumb">
	   <li><a href="<?php echo $this->location('apps');?>">Home</a></li>
		<li><a href="<?php echo $this->location('module/content/course/'); ?>">Matakuliah</a></li>
		<li><a href="<?php echo $this->location('module/content/tugas/'); ?>">Tugas & Test</a></li>
		<li class="active">Data</li>
	</ol>
	
		
	<!-- <table>
	<?php
	if( isset($soal)) :	
		$i = 1;
		foreach($soal as $key){	?>			
			<tr>
				<td><?php echo $i++ ?>.</td>
				<td><?php echo $key->pertanyaan ?></td>
			</tr>
			<tr>
				<td></td>
				<td>
				<?php if($key->kategori=="radio"){ ?>
				<input type="radio" name="group1" value="Milk"> Milk
				<?php } else if ($key->kategori=="checkbox") { ?>
				<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
				<input type="checkbox" name="vehicle" value="Car">I have a car
				<?php } else if ($key->kategori=="textarea") { ?>
				<textarea></textarea>
				<?php } ?>
				</td>
			</tr>
			
	<?php	} ?>
	</table>
	<?php
	else: 
	echo "Maaf Tidak Soal";	
	endif;
	?> -->
	
	<?php
	if( isset($soal) && isset($jawaban)) :	?>
	<form action="" method="post">
		<input name="test_id" value="<?php echo $test_id ?>" type="hidden" />
		<input name="jam_mulai" value="<?php echo date("H:m") ?>" type="hidden" />
		<ol>
			<?php foreach($soal as $key){ ?>
				<li>
					<?php echo $key->pertanyaan ?>
					<?php if($key->kategori != 'textarea'){ ?>
						<ol type="A">
							<?php foreach($jawaban as $dt){ ?>
								<?php if($key->soal_id == $dt->soal_id){ ?>
									<input name="soal[]" value="<?php echo $key->soal_id ?>" type="hidden">
									<li><input name="jawaban[]" type="<?php echo $key->kategori ?>" value="<?php echo $dt->jawaban_id ?>">
										<?php //echo ' ' . $dt->keterangan ?>
										<?php if($dt->is_benar==1){ ?>
											<?php echo ' ' . $dt->keterangan; ?> <span class="label label-warning"><small>*) Benar</small></span>
										<?php } else {
											echo ' ' . $dt->keterangan; } ?>
									</li>
								<?php } ?>
							<?php } ?>
						</ol>
					<?php } else{ ?>
						<input name="soal[]" value="<?php echo $key->soal_id ?>" type="hidden">
						<textarea name="jawaban[]" ></textarea>
					<?php } ?>
				</li>
				<br>
			<?php } ?>
		</ol>
		<div class="forms-action">
		<input type="submit" value=" Submit "class="btn btn-primary" />
		</div>
	</form>
	<?php
	else: 
		echo "Maaf Tidak Soal";	
	endif;

$this->foot();
?>