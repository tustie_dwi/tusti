<?php $this->head(); ?>
<fieldset>
	<?php
		foreach($test as $key){
			$materi = $key->judul;
			$judul = $key->judul_test;
			$test = $key->test_id;
		}
		
		foreach($peserta as $key){
			$nim = $key->nim;
			$nama= $key->nama;
			$jk=$key->jenis_kelamin;
			$nilai=$key->nilai;
		}
	?>
	<legend>
		Test <?php echo $materi ?> - <?php echo $judul ?>
		<?php if($role == 'mahasiswa') { ?>
			<a href="<?php echo $this->location('module/content/tugas') ?>" class="btn btn-primary pull-right"><i class="fa fa-list"></i> Test List</a>
		<?php }?>
	</legend>
	
	<ol class="breadcrumb">
		<li><a href="<?php echo $this->location('module/content/tugas') ?>"><i class="fa fa-home"></i> Daftar Test</a></li>
		<li><a href="<?php echo $this->location('module/content/test/peserta/' . $test) ?>">Daftar Peserta</a></li>
		<li><a href="#">Detail Jawaban</a></li>
	</ol>
	
	<table>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><?php echo $nama; ?></td>
		</tr>
		<tr>
			<td>NIM</td>
			<td>:</td>
			<td><?php echo $nim; ?></td>
		</tr>
		<tr>
			<td>Tanggal Test</td>
			<td>:</td>
		</tr>
		<tr>
			<td>Nilai</td>
			<td>:</td>
			<td><?php echo $nilai; ?></td>
		</tr>
	</table>

	<hr />

	
	<?php 
	function cek_hasil_input($soalid,$jawabanid,$hasil){
		foreach($hasil as $hs){
			if($hs->soal_id==$soalid){
				if($hs->jawaban_id==$jawabanid){
					echo "checked";
				}
			}
		}
	}
	
	function cek_hasil_text($soalid,$hasil){
		foreach($hasil as $hs){
			if($hs->soal_id==$soalid){
				if($hs->jawaban_id==''){
					echo $hs->inf_jawab;
				}
			}
		}
	}
	
	function cek_skor_text($soalid,$hasil){
		foreach($hasil as $hs){
			if($hs->soal_id==$soalid){
				if($hs->jawaban_id==''){
					echo $hs->inf_skor;
				}
			}
		}
	}
	
	function cek_ctt_text($soalid,$hasil){
		foreach($hasil as $hs){
			if($hs->soal_id==$soalid){
				if($hs->jawaban_id==''){
					echo $hs->catatan;
				}
			}
		}
	}
	
	function detailid($soalid,$hasil){
		foreach($hasil as $hs){
			if($hs->soal_id==$soalid){
				if($hs->jawaban_id==''){
					echo $hs->detail_id;
				}
			}
		}
	}
	
	
	function hasilid($soalid,$hasil){
		foreach($hasil as $hs){
			if($hs->soal_id==$soalid){
				if($hs->jawaban_id==''){
					echo $hs->hasil_id;
				}
			}
		}
	}
	?>
	
	<?php
	// if( isset($soal) && isset($jawaban)) :	?>
	<!-- <form action="" method="post" id="form"> -->
		<input name="test_id" value="<?php echo $test_id ?>" type="hidden" />
		<input name="jam_mulai" value="<?php echo date("H:m") ?>" type="hidden" />
		<ol><?php $i=0; ?>
			<?php foreach($soal as $key){ ?>		 
				<li>
			<?php echo $key->pertanyaan ?>
			<?php if($key->kategori != 'textarea'){ ?>
				<ol type="A">
					<?php if($jawaban){ ?>
						<?php foreach($jawaban as $dt){ ?>
							<?php if($key->soal_id == $dt->soal_id){ ?>
								<li><input disabled="disabled" <?php cek_hasil_input($key->soal_id, $dt->jawaban_id, $hasil) ?> name="pilihan<?php echo $key->soal_id ?>" data-tipe="choice" class="jawab_<?php echo $key->soal_id ?>" type="<?php echo $key->kategori ?>" data-soal="<?php echo $key->soal_id ?>" value="<?php echo $dt->jawaban_id ?>">
								
									<?php if($dt->is_benar==1){ ?>
											<?php echo ' ' . $dt->keterangan; ?> <span class="label label-warning"><small>*) Benar</small></span>
										<?php } else {
											echo ' ' . $dt->keterangan; } ?>
								</li>
							<?php } ?>
						<?php } ?>
					<?php } else { ?>
						Tidak ada jawaban tersedia
					<?php } ?>
				</ol>				
			<?php }else { ?>
				
				<form action="" method="post" id="form">
					<input id="hasil_id<?php echo $i; ?>" name="hasil_id" value="<?php hasilid($key->soal_id,$hasil) ?>" type="hidden" />
					<input id="total_skor<?php echo $i; ?>" name="total_skor" value="<?php echo $nilai; ?>" type="hidden" />
					<textarea readonly="readonly" class="jawab_<?php echo $key->soal_id ?> form-control" rows="3" style="resize: vertical" data-tipe="text" data-soal="<?php echo $key->soal_id ?>" name="jawaban_text[]"><?php cek_hasil_text($key->soal_id,$hasil) ?></textarea>
					<br />
					<input name="detail_id" id="detail_id<?php echo $i; ?>" value="<?php detailid($key->soal_id,$hasil) ?>" type="hidden" />
					Nilai : <input <?php if ($role=='mahasiswa'){echo "readonly='readonly'";} ?> required="required" type="number"  class='col-md-9 form-control' id="inf_skor<?php echo $i; ?>" value="<?php cek_skor_text($key->soal_id,$hasil) ?>" /> <br />
					
					Catatan : <textarea <?php if ($role=='mahasiswa'){echo "readonly='readonly'";} ?> class='form-control' id="catatan<?php echo $i; ?>" rows="3" style="resize: vertical" name="catatan"><?php cek_ctt_text($key->soal_id,$hasil) ?></textarea><br />
					
					<?php if ($role!='mahasiswa'){ ?>
						<input type="submit" id="submit<?php echo $i; ?>" onclick="ubah_nilai(<?php echo $i ?>)" value="Ubah"class="btn btn-primary" />
					<?php } ?>
				</form>
			<?php }?>		
				</li>
				<br />
				<?php $i++; ?>
			<?php } ?>
		</ol>
	<!-- </form> -->
	<?php
	// else: 
	// echo "Maaf Tidak Soal Ok";	
	// endif;
	?>
	
</fieldset>
<?php
$this->foot();
?>