<?php $this->head(); ?>
<fieldset>
	<legend>
		Daftar Test
		<a href="<?php echo $this->location('module/content/test') ?>" class="pull-right btn btn-default"><i class="fa fa-list"></i> List Mata Kuliah</a>
	</legend>
	
	<?php if(isset($test)) : ?>
		<table class='table table-hover' id='example' data-id='module/skripsi/aspek'>
			<thead>
				<tr>
					<th width="25%">Mata Kuliah</th>
					<th>Judul</th>
					<th width="25%">Tgl Pengerjaan</th>
					<th width="30%">Instruksi</th>
					<?php if($role != 'mahasiswa'){ ?>
						<th>Detail</th>
					<?php } ?>
				</tr>
			</thead>
			
			<tbody>
				<?php foreach($test as $key){ ?>
					<tr><td>
							<?php if($role == 'mahasiswa'){ ?>
								<?php echo $key->nama_mk?>
								<br> <code> <?php echo $key->judul; ?></code>
							<?php } else{ ?>
								<?php echo "<a class='btn-edit-post' href=".$this->location('module/content/test/detail/'.$key->test_id)."><span class='text text-default'><strong>" . $key->nama_mk . '</span></strong></a>' ?>	
								<br> <code> <?php echo $key->judul; ?></code> <br /> 
								<?php if($key->is_publish == 1) echo "<span class='label label-success'>Pulished</span>"; else echo "<span class='label label-danger'>Not Pulished</span>"; ?>
								<?php if($key->is_random == 1) echo "<span class='label label-primary'>Soal Acak</span>"; else echo "<span class='label label-primary'>Soal Tdk Acak</span>"; ?>
							<?php } ?>
						</td>
						<?php if($role == 'mahasiswa'){ ?>
							<td><a title="Kerjakan Test" href="<?php echo $this->location('module/content/test/confirm/') . $key->test_id ?>"><?php echo $key->judul_test ?></a></td>
						<?php }else{ ?>
							<td><a title="Lihat Soal" href="<?php echo $this->location('module/content/test/soal/') . $key->test_id ?>"><?php echo $key->judul_test ?></a></td>
						<?php } ?>
						
						<td>
							<?php 
								echo "<abbr title='Tanggal Mulai'><span class='label label-success'>" . $key->tgl_mulai . "</span></abbr>";
								echo " <abbr title='Tanggal Selesai'><span class='label label-danger'>" . $key->tgl_selesai . "</span></abbr><br><br>";
								get_diff_time($key->tgl_selesai);
							?>
						</td>
						<td style="cursor: pointer" title="<?php echo $key->keterangan ?>"><?php echo $key->instruksi ?></td>
						<?php if($role != 'mahasiswa'){ ?>
							<td><a class="btn btn-default" href="<?php echo $this->location('module/content/test/peserta/') . $key->test_id ?>"><i class="fa fa-share-square"></i> Lihat Peserta</a></td>	
						<?php } ?>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	<?php else : ?>
		<div class="well">Daftar tes kosong !!</div>
	<?php endif; ?>
	
</fieldset>
	
<?php
	function get_diff_time($date1){
		$date1 = strtotime($date1);
		$date2 = time();
		
		$subTime = $date1 - $date2;
		$y = ($subTime/(60*60*24*365));
		$d = ($subTime/(60*60*24))%365;
		$h = ($subTime/(60*60))%24;
		$m = ($subTime/60)%60;
		
		if($subTime > 0){
			echo "<code>";
			echo $d." hari : \n";
			echo $h." jam : \n";
			echo $m." menit lagi\n";
			echo "</code>";
			return 1;
		}
		else{
			echo "<code>Waktu habis</code>";
			return 0;
		}
	}
?>



<?php
$this->foot();
?>