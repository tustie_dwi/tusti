<?php 
$this->head(); 

$materi	= $test->judul;
$judul	= $test->judul_test;
$mk		= $test->nama_mk;
?>
<h2 class="title-page">Test</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/content/tugas'); ?>">Tugas & Test</a></li>
  <li class="active">Data</li>
</ol>
<div class="breadcrumb-more-action">
    <div class="dropdown">
            <a class="dropdown-toggle btn btn-primary" href="#" data-toggle="dropdown">Soal Ke <span class="nomor-soal">1</span></a>
          
            <ul class="dropdown-menu dropdown-pilih-soal pull-right" role="menu"  aria-labelledby="dLabel">
            	<?php $i=1; foreach($soal as $test){ ?>
					<li <?php if($i==1)echo 'class="active"';?>><a class="page" data-nilai="<?php echo ($i); ?>" href="#<?php echo $test->soalid; ?>" data-toggle="tab"><?php echo $i++; ?></a></li>
				<?php } ?>
          	</ul>
          </div>
        
  
    </div>
  
<div class="row">
	<div class="col-md-12">	
    <div class="row">
    
    
		<div class="col-md-6" >
			<h2><?php echo $mk; ?> <small><?php  echo $materi; ?></small></h2>
			<h3><?php echo $judul; ?></h3>			
		</div>
			
		<div class="col-md-6">	
<!--		<h1><?php echo $mk; ?> <small><?php  echo $materi; ?></small></h1>
		<h2><?php echo $judul; ?></h2>-->
		
		<!--<ul class="pagination" id="myTab">
		<?php $i=1; foreach($soal as $test){ ?>
			<li><a href="#<?php echo $test->soalid; ?>" data-toggle="tab"><?php echo $i++; ?></a></li>
		 <?php } ?>
		</ul>-->

		<input id="page" value="0" type="hidden" />
		<input id="limit" value="<?php echo $i-2 ?>" type="hidden" />

		<div class="tab-content">
		<?php if(isset($soal)): ?>	
			<?php $i=1; foreach($soal as $test){ ?>
			   <div class="tab-pane <?php if($i==1)echo 'active';$i++;?>" id="<?php echo $test->soalid; ?>">
					<?php echo $test->pertanyaan ?>
					<?php if($test->kategori != 'textarea'){ ?>
						<ol type="A">
							<?php if($jawaban){ ?>
								<?php foreach($jawaban as $dt){ ?>
									<?php if($test->soal_id == $dt->soal_id){ ?>
										<li><input name="pilihan<?php echo $test->soal_id ?>" data-tipe="choice" class="jawab_<?php echo $test->soal_id ?>" type="<?php echo $test->kategori ?>" data-soal="<?php echo $test->soal_id ?>" value="<?php echo $dt->jawaban_id ?>"><?php echo ' ' . $dt->keterangan ?></li>
									<?php } ?>
								<?php } ?>
							<?php } else { ?>
								Jawaban Kosong !!
							<?php } ?>
						</ol>
					<?php } else{ ?>
						<textarea class="jawab_<?php echo $test->soal_id ?> form-control" rows="3" style="resize: vertical" data-tipe="text" data-soal="<?php echo $test->soal_id ?>" name="jawaban_text[]"></textarea>
					<?php } ?>
			   </div>
			<?php } ?>
		<?php endif; ?>	
		</div>

		<ul class="pager">
		  <li class="previous" id="prev"><a href="#">&larr; Prev</a></li>
		  <li class="next" id="next"><a href="#">Next &rarr;</a></li>
		</ul>
	</div>
</div>
<?php
$this->foot();
?>