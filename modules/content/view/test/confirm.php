<?php $this->head(); ?>

<h2 class="title-page">Test <small>Detail Test</small></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/tugas'); ?>">Tugas & Test</a></li>
	  <li class="active"><a href="#">Data</a></li>
	</ol>
	
<div class="row">
	<div class="col-md-12">

	<?php 
	
	$confirm = 1;
	if($test){
		$test_id =$test->test_id;
	?>
		<div class="col-md-6">
			<h2><?php echo $test->nama_mk; ?> <small><?php echo$test->judul ?></small></h2>
			<h3><?php echo $test->judul_test; ?></h3>
			<blockquote>
			<?php echo $test->instruksi; ?>
			<small><?php echo $test->keterangan ?></small>
			</blockquote>
		</div>
		<div class="col-md-6">
			<dl class="dl-horizontal">
			  <dt>Tanggal Mulai</dt>
			  <dd><?php echo$test->tgl_mulai ?></dd>
			  
			  <dt>Tanggal Selesai</dt>
			  <dd><?php echo$test->tgl_selesai ?></dd>
			  
			  <dt>Waktu Tersisa</dt>
			  <dd>
				<?php 
					if(get_diff_time($test->tgl_selesai) == 0) $confirm = 0;
				?>
			  </dd>
			<dd>
			<?php
			if(isset($hasil)){
			if($hasil->jam_selesai != '00:00:00'){
				?>
					<a href="#" class="btn btn-success" disabled><i class='fa fa-check-square-o'></i> Tes sudah dikerjakan</a>
					<a href="<?php echo $this->location('module/content/test/ceksoal/') . $hasil->hasil_id ?>" class="btn btn-primary">
						<i class="fa fa-check"></i> Lihat Hasil
					</a>
				<?php
			} else{
				if($confirm == '1'){
					?>
						<a href="<?php echo $this->location("module/content/test/save_hasil/") . $test_id ?>" class="btn btn-primary">
							<i class='fa fa-check'></i> Mulai mengerjakan
						</a>
					<?php
				}
				else{
					?>
						<a href="#" class="btn btn-danger" disabled><i class='fa fa-ban'></i> Waktu sudah habis</a>
					<?php
				}
			}
		}
		else{
			if($confirm == '1'){
				?>
					<a href="<?php echo $this->location("module/content/test/save_hasil/") . $test_id ?>" class="btn btn-primary">
						<i class='fa fa-check'></i> Mulai mengerjakan
					</a>
				<?php
			}
			else{
				?>
					<a href="#" class="btn btn-danger" disabled><i class='fa fa-ban'></i> Waktu sudah habis</a>
				<?php
			}
		}
		
			?>
            </dd>
            </dl>
		</div>
	<?php
	}
	
		
	?>
	</div>
</div>
<?php

function get_diff_time($date1){
	$date1 = strtotime($date1);
	$date2 = time();
	
	$subTime = $date1 - $date2;
	$y = ($subTime/(60*60*24*365));
	$d = ($subTime/(60*60*24))%365;
	$h = ($subTime/(60*60))%24;
	$m = ($subTime/60)%60;
	
	if($subTime > 0){
		echo $d." hari : \n";
		echo $h." jam : \n";
		echo $m." menit\n";
		return 1;
	}
	else{
		echo "<p class='text-danger'>Maaf, waktu mengerjakan sudah habis.</p>";
		return 0;
	}
}
		
$this->foot();
?>