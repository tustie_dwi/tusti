<?php $this->head(); 

	$materi = $test->judul;
	$judul 	= $test->judul_test;
	$test_id = $test->test_id;
	$mk 	= $test->nama_mk;
		
	$mhsid = $mhsid;
		
	function cek_history($mhsid, $soalid, $jawabanid){
		if(isset($_COOKIE[$mhsid])){
			foreach($_COOKIE[$mhsid] as $test => $value ){
				$data = explode('|', $value);
				if($data[1] == $soalid){
					if (strpos($data[2],$jawabanid) !== false) {
						echo 'checked';
					}
				}
			} //end of foreach
		} //end of if
	} //end of function
	
	function cek_history_textarea($mhsid, $soalid){
		if(isset($_COOKIE[$mhsid])){
			foreach($_COOKIE[$mhsid] as $test => $value ){
				$data = explode('|', $value);
				if($data[1] == $soalid){
					echo $data[2];
				}
			} //end of foreach
		} //end of if
	} //end of function
		
	?>
<div class="row">
	<div class="col-md-12">					
		<h2 class="title-page">Test <?php echo $judul; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/content/tugas'); ?>">Tugas & Test</a></li>
	  <li class="active">Test <?php echo $judul; ?></li>
	</ol>
    <div class="breadcrumb-more-action">
    <div class="dropdown">
            <a class="dropdown-toggle btn btn-primary" href="#" data-toggle="dropdown">Soal Ke <span class="nomor-soal">1</span></a>
          
            <ul class="dropdown-menu dropdown-pilih-soal" role="menu"  aria-labelledby="dLabel">
            	<?php $i=1; foreach($soal as $test){ ?>
					<li <?php if($i==1)echo 'class="active"';?>><a class="page" data-nilai="<?php echo ($i); ?>" href="#<?php echo $test->soal_id; ?>" data-toggle="tab"><?php echo $i++; ?></a></li>
				<?php } ?>
          	</ul>
          </div>
        <a class="btn btn-more-send" href="#" id="jawab-btn"><span class="fa fa-check"></span> Kirim</a>
        
  
    </div>
    
		<input id="page" value="0" type="hidden" />				
		
	<div class="row">
    
    
		<div class="col-md-6" >
			<h2><?php echo $mk; ?> <small><?php  echo $materi; ?></small></h2>
			<h3><?php echo $judul; ?></h3>			
		</div>
			
		<div class="col-md-6">		
			<span id="content-wrap">
				<form action="<?php echo $this->location('module/content/test/save') ?>" method="post" id="form-jawab">
					<input name="test_id" value="<?php echo $test_id ?>" type="hidden" />
					<input name="hasil_id" value="<?php echo $hasil_id ?>" type="hidden" />
					<input id="jawaban-wrap" name="jawaban" value="" type="hidden" />
					<input id="mhsid" value="<?php echo $mhsid ?>" type="hidden" />
				</form>	
				
				<div class="tab-content">
					<?php $no = 1; foreach($soal as $test){ ?>
						<div class="tab-pane <?php if($no==1)echo 'active';?>" id="<?php echo $test->soal_id; ?>">
							
							<?php 
								echo '<ol start="'.$no.'"><li>' . $test->pertanyaan . '</li>';
								$no++;
							?>
							<?php if($test->kategori != 'textarea'){ ?>
								<ol type="A">
									<?php if($jawaban){ ?>
										<?php foreach($jawaban as $dt){ ?>
											<?php if($test->soal_id == $dt->soal_id){ ?>
												<li>
													<div class="<?php echo $test->kategori ?>">
														<label>
															<input data-tipe="choice" class="<?php echo $test->kategori ?>-link" name="pilihan<?php echo $test->soal_id ?>"
															type="<?php echo $test->kategori ?>" data-soal="<?php echo $test->soal_id ?>" 
															value="<?php echo $dt->jawaban_id ?>" <?php cek_history($mhsid, $test->soal_id, $dt->jawaban_id); ?> />
															<?php echo ' ' . $dt->keterangan ?>
														</label>
													</div>
												</li>
											<?php } ?>
										<?php } ?>
									<?php } else { ?>
										Jawaban Kosong !!
									<?php } ?>
								</ol>
							<?php } else{ ?>
								<textarea class="textarea-link form-control" rows="3" style="resize: vertical" data-soal="<?php echo $test->soal_id ?>"><?php cek_history_textarea($mhsid, $test->soal_id); ?></textarea>
							<?php } ?>
							</ol>
						<br>
						</div>
					<?php } ?>
				</div>
			</span>
		
			<input id="limit" value="<?php echo $i-2 ?>" type="hidden" />
			<ul class="pager">
			  <li class="previous" id="prev"><a href="#">&larr; Back</a></li>
			  <li class="next" id="next"><a href="#">Next &rarr;</a></li>
			</ul>
		</div>
	</div>
    </div>
</div>
<?php
$this->foot();
?>