<?php $this->head(); ?>
<fieldset>
	<legend>Daftar Mata Kuliah</legend>
	
	<?php if(isset($mk)) : ?>
		<table class='table table-hover table-striped' id='example' data-id='module/skripsi/aspek'>
			<thead>
				<tr>
				</tr>
			</thead>
			
			<tbody>
				<?php $no = 1; foreach($mk as $key){ ?>
					<tr>
						<td><?php echo $no; $no++ ?></td>
						<td>
							<?php echo $key->keterangan?>
							<code> <?php echo $key->sks . ' sks'; ?></code>
						</td>
						<td><a href="<?php echo $this->location('module/content/test/read/') . $key->mkditawarkan_id ?>" class="btn btn-default">Lihat <i class="fa fa-angle-double-right fa-lg"></i></a></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	<?php else : ?>
		<div class="well">Daftar tes kosong !!</div>
	<?php endif; ?>
	
</fieldset>
	
<?php
	function get_diff_time($date1){
		$date1 = strtotime($date1);
		$date2 = time();
		
		$subTime = $date1 - $date2;
		$y = ($subTime/(60*60*24*365));
		$d = ($subTime/(60*60*24))%365;
		$h = ($subTime/(60*60))%24;
		$m = ($subTime/60)%60;
		
		if($subTime > 0){
			echo "<code>";
			echo $d." hari : \n";
			echo $h." jam : \n";
			echo $m." menit lagi\n";
			echo "</code>";
			return 1;
		}
		else{
			echo "<code>Waktu habis</code>";
			return 0;
		}
	}
?>



<?php
$this->foot();
?>