<?php $this->head(); ?>
<fieldset>
	<legend>
		Daftar Test
		<!-- <a href="<?php echo $this->location('module/content/test') ?>" class="pull-right btn btn-default"><i class="fa fa-list"></i> List Mata Kuliah</a> -->
		<a href="<?php echo $this->location('module/content/test/by/mk/'.$mkdid) ?>" class="pull-right btn btn-default"><i class="fa fa-list"></i> New Test</a>
		<a href="<?php echo $this->location('module/content/course') ?>" class="pull-right btn btn-default" style="margin:0px 5px"><i class="fa fa-list"></i> List Mata Kuliah</a>
	</legend>
	
	<?php
	foreach($test as $key){
	$matakuliah = $key->nama_mk;		
	} 
	?>
	
	<em><strong>Mata Kuliah</strong></em>
		<?php 
			echo $matakuliah."<br><br>";
		?>
	
	<?php if(isset($test)) : ?>
		<table class='table table-hover' id='example' data-id='module/skripsi/aspek'>
			<thead>
				<tr>
					
					<th>Judul</th>
					<th>Materi</th>
					<th width="25%">Waktu Pengerjaan</th>
					<th>Instruksi</th>
					<?php if($role != 'mahasiswa'){ ?>
						<th>Status</th>
						<th>Detail</th>
					<?php } ?>
				</tr>
			</thead>
			
			<tbody>
				<?php foreach($test as $key){ ?>
					<tr>
						<?php if($role == 'mahasiswa'){ ?>
							<td><a title="Kerjakan Test" href="<?php echo $this->location('module/content/test/confirm/') . $key->test_id ?>"><?php echo $key->judul_test ?></a></td>
						<?php }else{ ?>
							<td><a title="Lihat Soal" href="<?php echo $this->location('module/content/test/soal/') . $key->test_id ?>"><?php echo $key->judul_test ?></a></td>
						<?php } ?>
						<td><?php echo $key->judul; ?></td>
						<td>
							<?php 
								echo "<small class='fa fa-clock-o'>Mulai " . $key->tgl_mulai . "</small></br>";
								echo "<small class='fa fa-clock-o'>Selesai " . $key->tgl_selesai . "</small></br>";
								get_diff_time($key->tgl_selesai);
							?>
						</td>
						<td style="cursor: pointer" title="<?php echo $key->keterangan ?>"><?php echo $key->instruksi ?></td>
						<?php if($role != 'mahasiswa'){ ?>
							
							<td>
								<?php if($key->is_publish == 1) echo "<span class='label label-success'>Pulished</span>"; else echo "<span class='label label-danger'>Not Pulished</span>"; ?>
								<?php if($key->is_random == 1) echo "<span class='label label-primary'>Soal Acak</span>"; else echo "<span class='label label-primary'>Soal Tdk Acak</span>"; ?>
						</td>
						<td><a class="btn btn-default" href="<?php echo $this->location('module/content/test/peserta/') . $key->test_id ?>"><i class="fa fa-share-square"></i> Lihat Peserta</a></td>	
						<?php } ?>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	<?php else : ?>
		<div class="well">Daftar tes kosong !!</div>
	<?php endif; ?>
	
</fieldset>
	
<?php
	function get_diff_time($date1){
		$date1 = strtotime($date1);
		$date2 = time();
		
		$subTime = $date1 - $date2;
		$y = ($subTime/(60*60*24*365));
		$d = ($subTime/(60*60*24))%365;
		$h = ($subTime/(60*60))%24;
		$m = ($subTime/60)%60;
		
		if($subTime > 0){
			echo "*)";
			echo $d." hari : \n";
			echo $h." jam : \n";
			echo $m." menit lagi\n";
			echo "";
			return 1;
		}
		else{
			echo "*) Waktu habis";
			return 0;
		}
	}
?>



<?php
$this->foot();
?>