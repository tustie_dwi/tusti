<?php $this->head(); ?>
	<legend>Hasil Tes Sementara</legend>	
	<div class="col-md-12">
			<dl class="dl-horizontal">
			  <dt>Mata Kuliah : </dt>
			  <dd><?php echo $test->nama_mk;?></dd>
			  
			  <dt>Bab Materi : </dt>
			  <dd><?php echo $test->judul ?></dd>
			  
			  <dt>Judul Tes : </dt>
			  <dd><?php echo $test->judul_test ?></dd>
			</dl>
		
			<dl class="dl-horizontal">
			  <dt>Tanggal Pengerjaan : </dt>
			  <dd><?php echo $hasil->tgl_test ?></dd>
			  
			  <dt>Jam Mulai : </dt>
			  <dd><?php echo $hasil->jam_mulai ?></dd>
			  
			  <dt>Jam Selesai : </dt>
			  <dd><?php echo $hasil->jam_selesai ?></dd>
			</dl>
	</div>
	<hr />
	<div class="col-md-12">
		<ol class="list-group">
		  <?php $total_skor = 0; foreach($soal as $key){ ?>
		  	<h4>
			  	<li>
			  		<?php
			  			$skor = 0; 
						$jawaban = '';
			  			echo $key->pertanyaan;
				  		foreach($jawab as $dt){
							if($key->soal_id == $dt->soal_id) {
								$skor += $dt->inf_skor;
								$jawaban .= $dt->inf_jawab . ', ';
							}
						}
						echo "<h5>Nilai : <span class='label label-info'>".$skor."</span>";
						echo ' <span class="fa fa-caret-right"></span> ' . rtrim($jawaban, ', ') . '</h5>';
						$total_skor += $skor;
				  	?>
			  	</li>
		  	</h4>
		  <?php } ?>
		</ol>
		
		<hr style="margin-bottom: 10px">
			<legend>
			<a class="btn btn-primary" href="<?php echo $this->location('module/content/tugas') ?>">Selesai <span class="fa fa-check-square"></span></a>
				<h4 class="pull-right">
				<span class='fa fa-check-circle'></span> Total Skor : <?php echo "<span class='label label-primary'>".$total_skor."</span>" ?>
				</h4>
			</legend>
			
<?php $this->foot(); ?>