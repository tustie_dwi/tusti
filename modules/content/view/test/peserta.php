<?php $this->head(); ?>

<fieldset>
	<legend>Daftar Peserta Test</legend>
	
	<ol class="breadcrumb">
		<li><a href="<?php echo $this->location('module/content/tugas') ?>"><i class="fa fa-home"></i> Daftar Test</a></li>
		<li><a href="#">Daftar Peserta</a></li>
	</ol>
	
	<?php foreach($totalpeserta as $key){ ?>
		<?php echo "<h4><span class='label label-info'>Jumlah Peserta : ".$key->total_peserta . '</span></h4>' ?>
	<?php } ?>
	
<?php if(isset($peserta)) :	?>	
	<table class='table table-hover' id='example' data-id='module/skripsi/aspek'>
		<thead>
			<tr>
				<th>NIM</th>
				<th>Nama</th>
				<th>Tanggal Test</th>
				<th>Jam Mulai</th>
				<th>Jam Selesai</th>
				<th>Total Jawab</th>
				<th>Nilai</th>
				<th>Lihat Test</th>
			</tr>
		</thead>
		
		<tbody>
			<?php foreach($peserta as $key){ ?>
				<tr>
					<td><?php echo $key->mahasiswa_id ?></td>
					<td><?php echo $key->nama ?></td>
					<td><?php echo $key->tgl_test ?></td>
					<td><?php echo $key->jam_mulai ?></td>
					<td><?php echo $key->jam_selesai ?></td>
					<td><?php echo $key->max_post ?></td>
					<td><?php echo $key->total_skor ?></td>
					<td><a class="btn btn-default" href="<?php echo $this->location('module/content/test/ceksoal/') . $key->hasil_id ?>"><i class="fa fa-share-square"></i> Cek</a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
<?php
	else: 
	echo "Maaf Tidak Peserta yang Mengambil Test Ini";	
	endif;
?>	
</fieldset>
<?php
$this->foot();
?>