<?php $this->head();?>
<fieldset>
	<legend>
    Kartu Hasil Studi
	</legend>
	<div class="span6">
			<form name="frmDosen" id="frmDosen" class="form-inline" method=post action="<?php echo $this->location('module/content/khs')?>" >	
				
				<div class="well">						
					<div class='control-group'>
						<div class="controls">
							<select name="cmbsemester" onChange='form.submit();' class='cmbmulti span12 populate'>
								<option value="-">Semester</option>
								<?php
								foreach($semester as $dt):
									echo "<option value='".$dt->inf_semester."' ";
									if($semesterid==$dt->inf_semester){
										echo "selected";
									}
									echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
								endforeach;
								?>
							</select>
							<input type="hidden" name="mahasiswa_id" value="<?php  echo $mhsid; ?>" >
						</div>
					</div>
				</div>		
					
			</form>			
	</div>
	<div class="span12">
			<?php
			if(isset($post)) :	
			?>
				<table>
					<?php if(isset($identitas)){
							foreach($identitas as $i){ ?>
					<tr>
						<td style="width: 150px;">Nama</td>
						<td style="width: 250px;">: <?php echo $i->nama ?></td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 100px;">&nbsp;</td>
						<td style="width: 150px;">Angkatan</td>
						<td style="width: 150px;">: <?php echo $i->angkatan ?></td>
					</tr>
					<tr>
						<td style="width: 150px;">NIM</td>
						<td style="width: 150px;">: <?php echo $i->mahasiswa_id ?></td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">&nbsp;</td>
						<td style="width: 150px;">Semester</td>
						<td style="width: 150px;">: <?php 
							$j=1;
							if(isset($semesterke)){
								foreach($semesterke as $d){
									if($semesterid==$d->inf_semester) echo $j." (".$this->kekata($j).")";
									$j++;
								}
							}
						
						?>
						</td>
					</tr>
					<?php 	}
						  } ?>
				</table><br><br>
				<table class="table table-hover">
					<thead>
						<tr>
							<th style="width: 50px;">No</th>
							<th>Matakuliah</th>
							<th>&nbsp;</th>
							<th style="width: 50px; text-align: center;">Kelas</th>
							<th style="width: 50px;">SKS</th>
							<th style="width: 50px;">Nilai</th>
							<th style="width: 100px;">SKS x Nilai</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 1;
							$nilai = 0;
							$sks = 0;
							$sks_lulus = 0;
							foreach($post as $dt){ ?>
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo $dt->matakuliah ?></td>
									<td>&nbsp;</td>
									<td style="text-align: center;"><?php echo $dt->kelas ?></td>
									<td style="text-align: center;"><?php echo $dt->sks ?></td>
									<td style="text-align: center;"><?php echo $this->convertTohuruf($dt->nilai_akhir) ?></td>
									<td style="text-align: center;"><?php echo ($dt->sks * $dt->inf_bobot) ?></td>
								</tr>
						<?php $i++;
							  $nilai += ($dt->sks * $dt->inf_bobot);
							  $sks += $dt->sks;
							  if($this->convertTohuruf($dt->nilai_akhir)!='E'){
							  	$sks_lulus += $dt->sks;
							  }
							} ?>
								<tr>
									<td>&nbsp;</td>
									<td><strong>TOTAL</strong></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td style="text-align: center;"><strong><?php echo $sks ?></strong></td>
									<td>&nbsp;</td>
									<td style="text-align: center;"><strong><?php echo $nilai ?></strong></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>SKS beban : <?php echo $sks ?></td>
									<td>SKS lulus : <?php echo $sks_lulus ?></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>IP Semester : <?php echo number_format(($nilai/$sks), 2)?></td>
									<td>IP Kumulatif : <?php $ip_kom = number_format(($this->IPK($mhsid)), 2);
															 if($ip_kom != ""){
																echo number_format(($this->IPK($mhsid)), 2);
															 } else echo "0";
														?></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
					</tbody>
				</table>
			<?php
			 else: 
			 ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php 
			endif; 
			?>
	</div>
</fieldset>
<?php $this->foot(); ?>