<?php 
	$hidId			= $posts->hid_id;
	$nama			= $posts->nama;
	$fakultas_id	= $posts->fakultas_id;
	$cabangid		= $posts->cabang_id;
?>

<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading">Karyawan Unit</div>
				<div class="panel-body ">
					<?php if(isset($kunit)): ?>
					<table class='table table-hover' id='example'>
					<thead>
						<tr>
							<th>&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
					<?php foreach ($kunit as $k) { ?>
					<tr>
						<td>
						  <div class="col-md-9">
							<span class='text text-default'><strong>
							<?php echo $k->keteranganunit ?>
							</strong></span>
							<?php if($k->is_aktif=='1')echo "<span class='label label-success'>Aktif</span>";else echo "<span class='label label-danger'>Tidak Aktif</span>";  ?>
							<br>
							<i class="fa fa-calendar-o"></i> 
							<?php echo str_replace('-', '/', $k->periode_mulai) ?> - 
							<?php if($k->periode_selesai=='0000-00-00')echo "sekarang";else echo str_replace('-', '/', $k->periode_selesai); ?>
						  </div>
						  <div class="col-md-3">
							<ul class='nav nav-pills'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop-unit' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop-unit'>
									<li>
									<a class='btn-edit-post' onclick="edit_unit('<?php echo $k->unitid ?>','<?php echo $k->karyawanid ?>','<?php echo $k->periode_mulai ?>','<?php echo $k->periode_selesai ?>','<?php echo $k->is_aktif ?>','<?php echo $k->unit_id ?>')" href="javascript::"><i class='fa fa-edit'></i> Edit</a>	
									</li>
									<!-- <li>
									<a class='btn-edit-post' onclick="delete_unit('<?php echo $k->unit_id ?>','<?php echo $k->karyawanid ?>','<?php echo $k->periode_mulai ?>','<?php echo $k->periode_selesai ?>','<?php echo $k->is_aktif ?>', this)" href="javascript::"><i class='fa fa-trash-o'></i> Delete</a>	
									</li> -->
								  </ul>
								</li>
							</ul>
						  </div>
						</td>
					</tr>
					<?php } ?>
					</tbody>
					</table>
					<?php else:	?>
					<div class="well" align="center">No Data</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<form method=post name="form-unit" id="form-unit">
			<div class="panel panel-default">
				<div class="panel-heading">Data Pendukung</div>
				<div class="panel-body ">
					
					<div class="form-group">
						<label class="control-label">Nama</label>								
						<input name="nama" disabled class="form-control" value="<?php if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label">Fakultas</label>								
						<select id="select_fakultas_kunit" class="e9 form-control" name="fakultas">
							<option value="-">Select Fakultas</option>
							<?php if(count($fakultas)> 0) {
								foreach($fakultas as $f) :
									echo "<option value='".$f->hid_id."' ";
									if($fakultas_id==$f->hid_id){
										echo "selected";
									}
									echo " >".$f->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
					
					<div class="form-group">
						<label class="control-label">Cabang</label>								
						<select id="select_cabang_kunit" class="e9 form-control" name="cabang">
							<option value="-">Select Cabang</option>
							<?php if(count($cabang)> 0) {
								foreach($cabang as $c) :
									echo "<option value='".$c->cabang_id."' ";
									if($cabangid==$c->cabang_id){
										echo "selected";
									}
									echo " >".$c->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
					
					<div class="form-group">
						<label class="control-label">Unit</label>								
						<select id="select_unit_kunit" class="e9 form-control" name="unit">
							<option value="-">Select Unit</option>
						</select>
					</div>
					
					<div class="form-group">
						<label class="control-label">Periode Mulai</label>								
						<input name="periodemulai" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group">
						<label class="control-label periode-akhir">Periode Selesai</label>								
						<input name="periodeselesai" class="form-control form_datetime" value="<?php //if(isset($nama))echo $nama?>" type="text">
					</div>
					
					<div class="form-group checkbox">
						<label><input name="current" <?php //if($is_status=="dosen")echo " checked "; ?> value="1" type="checkbox"><b>Current</b></label>
						<span class="help-block">centang jika aktif sampai sekarang</span>
					</div>
					
					<div class="form-group checkbox">
						<label><input name="isaktif" <?php //if($is_status=="dosen")echo " checked "; ?> value="1" type="checkbox"><b>Tidak Aktif</b></label>
						<span class="help-block help-isaktif">centang jika aktif</span>
					</div>
					
					<div class="form-actions">
						<label class="control-label">&nbsp;</label>
						<input type="hidden" name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>">
						<input type="hidden" name="hidIdval" value="">
						<input name="b_karyawanunit" id="b_karyawanunit" value=" Data Valid &amp; Save " class="btn btn-primary" type="submit">&nbsp;
						<a href="javascript::>" id="cancel-unit" class="btn btn-default" style="display: none;">Cancel</a>
					</div>
				</div>
			</div>
		</form>
		</div>
</div>