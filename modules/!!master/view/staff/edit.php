<?php
$this->head();

if($posts !=""){
	$header		= "Edit Staff/Dosen";
	$hidId		= $posts->hid_id;
	
}else{
	$header			= "Write New Staff/Dosen";
	$hidId			= "";
}


?> 
	
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/staff'); ?>">Staff/Dosen</a></li>
	  <li><a href="#">Write</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<?php if($hidId !=""){	?>
	<a href="<?php echo $this->location('module/master/staff/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write new Staff/Dosen</a>
	<?php } ?>
	</div>
	
	<ul class="nav nav-tabs" role="tablist"style="margin-bottom: 10px;" id="myTab">
	  <li class="active"><a href="#karyawan" role="tab" data-toggle="tab">Biodata</a></li>
	  <?php if($hidId !=""){ ?>
	  <li><a href="#unit" role="tab" data-toggle="tab">Unit Kerja</a></li>
	  <li><a href="#pendidikan" role="tab" data-toggle="tab">Pendidikan</a></li>
	   <li><a href="#kenaikan" role="tab" data-toggle="tab">Kenaikan</a></li>
		<li><a href="#pekerjaan" role="tab" data-toggle="tab">Pekerjaan</a></li>
	  <li><a href="#dokumen" role="tab" data-toggle="tab">Dokumen</a></li>
	 
	  <?php } ?>
	</ul>
	
	<div class="tab-content">
	  <div class="tab-pane active" id="karyawan"><?php echo $this->view('staff/karyawan/karyawan-tab.php', $data ); ?></div>
	  <div class="tab-pane" id="unit"><?php echo $this->view('staff/unit/unit-tab.php', $data ); ?></div>
	  <div class="tab-pane" id="pendidikan"><?php echo $this->view('staff/pendidikan/pendidikan-tab.php', $data ); ?></div>
	  <div class="tab-pane" id="kenaikan"><?php echo $this->view('staff/kenaikan/kenaikan-tab.php', $data ); ?></div>	  
	  <div class="tab-pane" id="pekerjaan"><?php echo $this->view('staff/pekerjaan/pekerjaan-tab.php', $data ); ?></div>	  
	  <div class="tab-pane" id="dokumen"><?php echo $this->view('staff/document/document-tab.php', $data ); ?></div>
	   
	</div>
<?php
$this->foot();
?>