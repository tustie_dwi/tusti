<?php 
	$hidId			= $posts->hid_id;
	$nama			= $posts->nama;
	$fakultas_id	= $posts->fakultas_id;
	$cabangid		= $posts->cabang_id;
	$golonganid		= $posts->golongan;
	$tgl_masuk		= $posts->tgl_masuk;
?>

<div class="row">	
	<div class="col-md-12">	
		<div class="panel panel-default">
			<div class="panel-heading">Rekap Absen</div>
			<div class="panel-body ">
				<table class="table example">
					<thead>
						<tr>
							<th>Periode</th>
							<th>Hadir</th>
							<th>Sakit</th>
							<th>Ijin</th>
							<th>Alpha</th>
						</tr>
					</thead>
					<tbody>
						<?php
						
						$absen = $mdosen->get_rekap_absen($posts->hid_id);
						
						if($absen):
							foreach($absen as $dt):
								?>
								<tr>
									<td><?php echo substr($dt->periode,0,4) . "-".substr($dt->periode,-2,2)  ?></td>
									<td><?php echo $dt->hadir ?></td>
									<td><?php echo $dt->sakit ?></td>
									<td><?php echo $dt->ijin ?></td>
									<td><?php echo $dt->alpha ?></td>
								</tr>
								<?php
							endforeach;
						endif;						
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
