<?php 
	$hidId			= $posts->hid_id;
	$nama			= $posts->nama;
	$fakultas_id	= $posts->fakultas_id;
	$cabangid		= $posts->cabang_id;
?>

<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading">Daftar Prestasi</div>
				<div class="panel-body ">
					
					<table class="table table-hover example">
					<thead>
						<tr>
							<th>&nbsp;</th>	
						</tr>
					</thead>
					<tbody>
					<?php
					$prestasi = $mdosen->get_rekap_prestasi($posts->hid_id);
						
					if($prestasi):
						foreach($prestasi as $dt):
							
						?>
					<tr>
						<td>
							<div class="col-md-8">
								<small><span class="text text-info"> <i class='fa fa-clock-o'></i>&nbsp;<?php 
									echo date("M d, Y", strtotime($dt->tgl_mulai)); 
									if(strtotime($dt->tgl_selesai)!=strtotime($dt->tgl_mulai)){
										echo " - ". date("M d, Y", strtotime($dt->tgl_selesai));
									}
								?>
								</span></small><br>
								<?php echo "<b>".$dt->judul."</b> &nbsp; ";  if($dt->penghargaan) echo "<span class='text text-success'><i class='fa fa-bookmark'></i> ".$dt->penghargaan."</span>"; ?><br>
								 
								<small><i class="fa fa-check-square-o"></i> <?php echo $dt->penyelenggara ?>
								&nbsp; <i class="fa fa-map-marker"></i> <?php echo $dt->lokasi ?></small>
								<?php if($dt->link_berita) echo "&nbsp;<small><span class='text text-danger'><i class='fa fa-search'></i> <b><a href=".$dt->link_berita." target='_blank' class='text text-danger'>Baca Berita</a></b></span></small>" ?>
							
							</div>
							<div class="col-md-4">
							
							<?php 
							
							$peserta = explode(",", $dt->peserta);
							
							for($j=0;$j<count($peserta);$j++){ 
								$k = $j+1;
								if($k==count($peserta)){
									$str = " ";
								}else{
									$str = ", ";
								}
									
								echo "<span class='text text-danger'>".$peserta[$j]."</span>".$str;
							}								
							?><br><small><?php echo $dt->inf_prestasi; ?></small>&nbsp;<span class="label label-danger"><?php echo ucwords($dt->tingkat); ?></span>
							<span class="label label-default"><?php echo ucwords($dt->jenis_prestasi); ?></span></div>
						</td>
					</tr>
					<?php endforeach ?>
					</tbody>
						<?php endif; ?>
					</table>					
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<form method=post name="form-prestasi" id="form-prestasi">
			<div class="panel panel-default">
				<div class="panel-heading">Tambah Data Prestasi</div>
				<div class="panel-body ">
					<div class="form-group">
						<label class="control-label">Jenis Prestasi</label>
						<div class="controls">
							<label class="radio-inline"><input type="radio" name="rjenis" value="akademik" <?php //if ($jprestasi=='akademik'){ echo "checked";} ?> >Akademik</label>
							<label class="radio-inline"><input type="radio" name="rjenis" value="non" <?php //if ($jprestasi=='non'){ echo "checked";} ?>>Non Akademik</label>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label">Tgl Kegiatan</label>
						<div class="row">
							<div class="col-md-6">
								<input type="text" name="mulai" class="form-control form_datetime" value="<?php  //echo $mulai; ?>" placeholder="Tgl mulai">
							</div>
							<div class="col-md-6">
								<input type="text" name="selesai" value="<?php  //echo $selesai; ?>" class="form-control form_datetime" placeholder="Tgl selesai">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label">Judul Kegiatan</label>
						<div class="controls">
							<textarea name="judul" class="form-control" rows="2"><?php //echo $judul; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label">Penyelenggara</label>					
						<input type="text" name="penyelenggara" class="form-control" value="<?php  //echo $penyelenggara; ?>">					
					</div>

					<div class="form-group">
						<label class="control-label">Lokasi Kegiatan</label>					
						<textarea name="lokasi" class="form-control" rows="2"><?php //echo $lokasi; ?></textarea>				
					</div>

					<div class="form-group">
						<label class="control-label">Link Berita</label>					
						<input type="text" name="linkberita" class="form-control" value="<?php  //echo $linkberita; ?>">					
					</div>

					<div class="form-group">
						<label class="control-label">Prestasi</label>					
						<input type="text" name="prestasi" class="form-control" value="<?php  //echo $linkberita; ?>">					
					</div>
					
					<div class="form-group">
						<label class="control-label">Tingkat</label>					
						<input type="text" name="tingkat" class="form-control" value="<?php  //echo $linkberita; ?>" placeholder="e.g: Nasional">					
					</div>
					<div class="form-group">
						<label class="control-label">Nama Penghargaan</label>					
						<input type="text" name="penghargaan" class="form-control" value="<?php  //echo $linkberita; ?>">					
					</div>
					
					<div class="form-group">
						<label class="control-label">Keterangan</label>					
						<textarea name="catatan" class="form-control" rows="2"><?php //echo $lokasi; ?></textarea>				
					</div>
					
									
					<div class="form-actions">
						<label class="control-label">&nbsp;</label>
						<input type="hidden" name="hidId" value="<?php if(isset($hidId))echo $hidId; ?>">
						<input type="hidden" name="hidNama" value="<?php if(isset($nama)) echo $nama; ?>">
						<input type="hidden" name="hidIdval" value="">
						<input name="b_karyawanprestasi" id="b_karyawanprestasi" value=" Data Valid &amp; Save " class="btn btn-primary" type="submit">&nbsp;
						<a href="javascript::>" id="cancel-prestasi" class="btn btn-default" style="display: none;">Cancel</a>
					</div>
				</div>
			</div>
		</form>
		</div>
</div>