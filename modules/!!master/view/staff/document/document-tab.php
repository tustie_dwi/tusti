<style>
.tree {
	
	font-size:85%;
    min-height:20px;
    padding:2px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:0px solid #999;
    -webkit-border-radius:2px;
    -moz-border-radius:2px;
    border-radius:2px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}

.tree li {
    list-style-type:none;
    margin:0;
    padding:4px 2px 0 4px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-10px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:10px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:10px;
    top:15px;
    width:15px
}
.tree li span {
    -moz-border-radius:2px;
    -webkit-border-radius:2px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:2px 4px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:15px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}

.tree>ul>li.parent_li{
max-height:200px;
overflow:auto;
}
</style>
<?php
$level = $this->coms->authenticatedUser->level; 
if($level!="3"){
?>
<div class="row">
	<div class="col-sm-4">
		<a href="#" data-toggle="modal" data-target="#newfolder" class="btn btn-primary"><i class="fa fa-folder-o"></i> New Folder</a>
		<a href="#" data-toggle="modal" data-target="#newfile" class="btn btn-primary"><i class="fa fa-cloud-upload"></i> Upload File</a>
	</div>
</div>
<br>
<?php } ?>
<div class="row">
	<!--BODY--->
	<div class="col-sm-5">
		<div class="tree">
			<ul>
				<li>				 	
				 	<span class="span text text-default" onclick="view_content('0','<?php echo $id ?>')"><i class="fa fa-folder-open-o"></i> Library</span>
		 			<ul>
		 				<?php
		 				if(isset($tes)){ 
		 					  foreach($tes as $dt){ 
		 						if($dt->is_available == '1'){
		 							$aktif = "";
								}
								else{
									$aktif = "<code><small>*)tidak aktif</small></code>";
								}
								if($level!=="3"){	  
		 				?>
			 						<li>
			 							<span class="span text text-default" onclick="view_content('<?php echo $dt->id ?>','<?php echo $id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name."<br>".$aktif ?></span>
			 							<a title="setting" class='btn-add' href="#" data-toggle="modal" data-target="#newfolder" onclick="rename('<?php echo $dt->id ?>','<?php echo $dt->folder_name ?>','<?php echo $dt->is_available ?>')"><span class='text'><i class="fa fa-gear"></i></span></a>
								 		<a title="create new folder" class='btn-add' data-toggle="modal" data-target="#newfolder" onclick="newfolder_parent('<?php echo $dt->id ?>')" href="#"><span class='text'><i class="fa fa-plus"></i></span></a>
								 		<a title="upload file" class='btn-add' data-toggle="modal" data-target="#newfile" href="#" onclick="newfile_parent('<?php echo $dt->id ?>','<?php echo $dt->folder_name; ?>')"><span class='text'><i class="fa fa-cloud-upload"></i></span></a>
							 		<?php if($level=="1"){ ?>
								 		<a title="delete folder" class='btn-add' href="#" onclick="doDelete('<?php echo $dt->folderid ?>','folder')"><span class='text'><i class="fa fa-trash-o"></i></span></a>
		 							<?php } ?>
		 								<?php echo $this->detailchild( $dt->id, $dt->folder_name) ?>
		 							</li>
	 					<?php 	}else{ ?>
	 								<li>
	 									<span class="span text text-default" onclick="view_content('<?php echo $dt->id ?>')"><i class="fa fa-folder-open-o"></i> <?php echo $dt->folder_name." ".$aktif ?></span>
										<?php echo $this->detailchild( $dt->id, $dt->folder_name) ?>
									</li>
						<?php   }
							 }
						} 
		 				?>
		 				
		 			</ul>
	 			</li>
 			</ul>
		</div>
	</div>
	<div class="col-sm-7" >
		<div class="content-table">
			<div class="view_content">
			
			</div>
		</div>
		<div class="content-file"></div>
	</div>
</div>

<!-- Modal Folder-->
<div class="modal fade" id="newfolder" tabindex="-1" role="dialog" aria-labelledby="newfolderLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_btn" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="newfolderLabel">Edit/Create Folder</h4>
      </div>
      <form id="newfolder_form">
	      <div class="modal-body">
        	  <div class="form-group">
	        	<label for="folder_name">Folder Name</label>
	        	<input type="text" name="folder_name" autocomplete="off" class="form-control" id="folder_name"/>
	        	<input type="hidden" name="parent" class="form-control" id="parent" value="0"/>
        	  </div>
        	  <div class="form-group">
	        	<input type="checkbox" name="aktif" autocomplete="off" id="aktif" value="1"/>
	        	<label for="aktif">Aktif</label>
        	  </div>
	      </div>
	      <div class="modal-footer">
			<input type="hidden" name="id" class="form-control" id="id"/>
			<input type="hidden" name="kar_id" id="kar_id" value="<?php echo $id ?>"/>
	        <button type="button" class="btn btn-default close_btn" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
       </form>
    </div>
  </div>
</div>

<!-- Modal File-->
<div class="modal fade" id="newfile" tabindex="-1" role="dialog" aria-labelledby="newfolderLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close_btn_file_upload" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="newfolderLabel">Upload New File</h4>
      </div>
      <form id="newfile_form" enctype="multipart/form-data">
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="col-sm-12">
	      			<button type="button" id="add_file_form"><i class="fa fa-plus"></i></button>
      			</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-sm-12">
		        	<div class="form-group">			        
			        	<input type="text" name="title[]" required="required" autocomplete="off" class="form-control" id="title" placeholder="Judul"/>
		        	</div>
		        	<div class="form-group">			        
			        	<textarea class="form-control" name="keterangan[]" id="keterangan" placeholder="Keterangan"></textarea>
		        	</div>
		        	<div class="form-group">
			        	<input type="file" name="uploads[]" required="required" autocomplete="off" class="form-control" id="uploads"/>
		        	</div>
	        	</div>
			</div>
        	<div id="newform">
        		
        	</div>
        	   	
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="count_new_file" class="form-control" id="count_new_file" value="1"/>
	      	<input type="hidden" name="folder" class="form-control" id="folder" value="0"/>
			<input type="hidden" name="folder_loc" class="form-control" id="folder_loc"/>
	        <button type="button" class="btn btn-default close_btn_file_upload" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
       </form>
    </div>
  </div>
</div>