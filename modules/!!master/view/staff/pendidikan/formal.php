<?php 
	if(isset($jenis_pend)&&$jenis_pend=="formal"){
		//echo "FORMAL";
		$hidid				= $edit_pend->formal_id;
		$jenjang			= $edit_pend->jenjang;
		$thn_lulus			= $edit_pend->tahun_lulus;
		$program_studi		= $edit_pend->program_studi;
		$latar_belakang_ilmu= $edit_pend->latar_belakang_ilmu;
		$nama_sekolah		= $edit_pend->nama_sekolah;
		$alamat_sekolah		= $edit_pend->alamat_sekolah;
		$document_id		= $edit_pend->document_id;
		$file_name			= $edit_pend->file_name;
	}
?>
<form method="post" name="form" id="form_pendidikan_formal" enctype="multipart/form-data">
		<div class="panel panel-default">
			<div class="panel-heading">Pendidikan Formal</div>
			<div class="panel-body ">
				<div class="form-group">
					<label class="control-label">Jenjang</label>
					<div class="controls">
						<select class="form-control e9" name="jenjang" id="jenjang">
							<option value="0">Silahkan Pilih</option>
							<option value="SD">SD/Sederajat</option>
							<option value="SMP">SMP/Sederajat</option>
							<option value="SMA">SMA/Sederajat</option>
							<option value="D3">Diploma [D3]</option>
							<option value="S1">Sarjana [S1]</option>
							<option value="S2">Magister [S2]</option>
							<option value="S3">Doktor [S3]</option>
						</select>
						<input type="hidden" name="jenjang_edit" value="<?php if(isset($jenjang)) echo $jenjang ?>" />
					</div>
				</div>
				<div class="form-group">
					<l></i> Select File</button>
					<div class="document-place">
						<?php
						if(isset($hidid)){
							if(isset($file_name)) echo $file_name 
						?>
							<input type="hidden" name="docid" value="<?php if(isset($document_id)) echo $document_id ?>" />
							&nbsp;<a href="javascript::" onclick="remove_selected()"><i class="fa fa-trash-o"></i></a>
						<?php
						}
						?>
						
					</div>
					<input type="hidden" name="document_formal" id="document_formal" class="form-control" value="<?php if(isset($document_id)) echo $document_id ?>"/>
				</div>
				<div class="form-group">
					<input type="hidden" name="hidid" id="hidid" value="<?php if(isset($hidid)) echo $hidid ?>"/>
					<input type="hidden" name="kar_id" id="kar_id" value="<?php echo $id ?>"/>
					<!-- <input type="text" name="doc_id" id="doc_id" value="<?php if(isset($document_id)) echo $document_id ?>"/> -->
					<button type="submit" class="btn btn-primary">Simpan</button>
					<input type="button" id="cancel-btn" onclick="location.href='<?php echo $this->location('module/master/staff/edit/'.$id )?>'" value="Batal" class="btn btn-danger">
				</div>
			</div>
		</div>
</form>