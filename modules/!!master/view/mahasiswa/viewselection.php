<?php if( isset($mhs) ) :	?>		
	<table class="table table-hover" id="example">
			<thead>
				<tr>						
					<th>&nbsp;</th>						
				</tr>
			</thead>
			<tbody>
	<?php
		$i = 1;
		if($mhs){
			foreach ($mhs as $dt): 
			if($dt->is_aktif=="aktif"){
				$label = "success";
			}
			else{
				$label = "warning";
			}
			
			if($dt->foto){ $foto=$dt->foto; }else{ $foto='upload/foto/no_foto.png';}
			?>
				<tr>
					<td>
						<div class="col-md-10">
							<div class="media">
								<a class="pull-left" style="text-decoration:none" href="<?php echo $this->location('module/master/mhs/detail/'.$dt->mhs_id); ?>" >
									<img class="media-object img-thumbnail" src="<?php echo $this->asset($foto); ?>"  width="40" >
								</a>
								<div class="media-body">
							  
									<?php 
									echo "<a href='".$this->location('module/master/mhs/detail/'.$dt->mhs_id)."' class='text text-default'><b>".$dt->nim."</b> ".ucWords(strToLower($dt->nama))."</a>";
									?>
									<span class='label label-<?php echo $label ?>'><?php echo ucWords($dt->is_aktif) ?></span>
									<br>
									<small><span class="text text-danger"><?php echo $dt->prodi;?></span>
									<?php
									if($dt->alamat){
									echo "<i class='fa fa-map-marker'></i> ".$dt->alamat."&nbsp; ";
									}
									
									if($dt->telp){
										echo "<i class='fa fa-phone'></i> ".$dt->telp."&nbsp; ";
									}
									
									if($dt->hp){
										echo "<i class='fa fa-mobile'></i> ".$dt->hp."&nbsp; ";
									}
									
									if($dt->email){
										echo "<i class='fa fa-envelope'></i> ".$dt->email."&nbsp; ";
									}
									?>
									</small>
								</div>
							</div>
						</div>
						<?php
						$uri_location = $this->location('module/akademik/pembimbing/delete');
						?>
						<div class="col-md-2">
							<ul class='nav nav-pills'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/master/mhs/edit/'.$dt->mhs_id)?>"><i class='fa fa-edit'></i> Edit</a>
									</li>
									<!-- <li>
									<a class='btn-edit-post' href="#" onclick="doDelete(<?php echo $dt->mahasiswa_id ?>)"><i class='fa fa-trash-o'></i> Hapus</a>	
									<input type="hidden" value="<?php echo $dt->mahasiswa_id ?>" class='deleted<?php echo $dt->mahasiswa_id ?>' uri = "<?php echo $uri_location ?>"/>
									</li>	 -->
								  </ul>
								</li>
							</ul>
						</div>
					</td>
				</tr>
				<?php
			 endforeach; 
		 } ?>
			</tbody>
	</table>
<?php
 else: 
 ?>
	
	<div class="well">Sorry, no content to show</div>
<?php endif; ?>