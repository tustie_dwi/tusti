<?php
$this->head();


if($posts){
		$aid	= $posts->id;
		$id		= $posts->agenda_id;
		$jenis	= $posts->jenis;
		$mulai	= $posts->tgl_mulai;
		$selesai= $posts->tgl_selesai;
		$judul	= $posts->judul;
		$note	= $posts->keterangan;
		$lokasi	= $posts->lokasi;
		$ruang	= $posts->inf_ruang;
		$penyelenggara	= $posts->penyelenggara;
		$upenyelenggara	=  $posts->unit_penyelenggara;
		$infpeserta	= $posts->inf_peserta;
		$userid	= $posts->user_id;
	
}else{
	$aid	= "";
	$id		= "";
	$jenis	= "";
	$mulai	= "";
	$selesai= "";
	$judul	= "";
	$note	= "";
	$lokasi	= "";
	$ruang	= "-";
	$penyelenggara="";
	$infpeserta="";
	$upenyelenggara="";
	$useris	= $this->coms->authenticatedUser->id;
}

?>

<h2 class="title-page">Event</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/content/page'); ?>">Content</a></li>
  <li><a href="<?php echo $this->location('module/content/event'); ?>">Event</a></li>
  <li class="active"><a href="#">Data</a></li>
</ol>
 <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/content/event/write'); ?>" class="btn btn-primary">
	<i class="fa fa-pencil icon-white"></i> New Event</a> 
</div>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo $this->location('module/content/agenda/edit/'.$hidid); ?>" method="post">
				
				<h3>General Info</h3>
				<table class='table table-bordered'>
					<tbody>
						<tr>
							<td width="20%"><em>Jenis Kegiatan</em></td>
							<td><?php if($jenis){ echo $jenis;} else{ echo "Lain-lain";} ?></td>
						</tr>
						<tr>
							<td width="20%"><em>Judul</em></td>
							<td><?php echo $judul; ?></td>
						</tr>
						<tr>
							<td width="20%"><em>Penyelenggara</em></td>
							<td><?php echo $penyelenggara;
							
							if($upenyelenggara){  echo " - ".$upenyelenggara;} ?></td>
						</tr>
						<tr>
							<td width="20%"><em>Keterangan</em></td>
							<td><?php if($note){ echo $note;} else {echo "-";} ?></td>
						</tr>
						<tr>
							<td width="20%"><em>Informasi Peserta</em></td>
							<td><?php if($infpeserta){ echo $infpeserta; } else{ echo "-"; } ?></td>
						</tr>
					</tbody>
				</table>
				
				<h3>Pelaksanaan</h3>
				<table class='table table-bordered'>
					<tbody>
						<tr>
							<td width="20%"><em>Tgl Kegiatan</em></td>
							<td><?php 
							if(date("M d, Y",strtotime($mulai)) == date("M d, Y",strtotime($selesai))){
									echo date("M d, Y",strtotime($mulai));		
								
								}else{
									echo date("M d",strtotime($mulai))." - ".date("d, Y",strtotime($selesai));
								}
								echo "</span>&nbsp;<i class='icon-time'></i> <span class='text text-warning'>".date("H:i",strtotime($posts->tmulai))." - ".date("H:i",strtotime($posts->tselesai))."</span>"; ?></td>
						</tr>
						<tr>
							<td width="20%"><em>Lokasi</em></td>
							<td><?php if($lokasi){echo $lokasi; } else{ echo "-"; } ?></td>
						</tr>
						<tr>
							<td width="20%"><em>Ruang</em></td>
							<td><?php if($ruang){echo $ruang;  } else{ echo "-"; } ?></td>
						</tr>
						
					</tbody>
				</table>
				
			<?php if($pemateri){ ?>
			<h3>Pemateri</h3>
			<table class='table table-bordered'>
					<tbody>
						<tr>
							<td width="20%"><em>Pemateri</em></td>
							<td><?php 
							$nama="";
							if($pemateri){
								$i=0;
								foreach($pemateri as $dt):
									$i++;
									$nama = $nama . $dt->nama .",";
									echo "<span class='label label-info' style='font-weight:normal'>".$dt->nama."</span>&nbsp;";		
									if($dt->instansi){
										echo "<code>".$dt->instansi."</code>&nbsp;";
									}
								endforeach;
								
							}else{
								echo "-";
							}?></td>
						</tr>							
					</tbody>
				</table>
			<?php
			}
			?>
			
			<?php if($undangan){ ?>
			<h3>Undangan</h3>
			<table class='table table-bordered'>
				<tbody>
					<tr>
						<td width="20%"><em>Undangan</em></td>
						<td width="80%">
					
						<ul class="list-inline">
						<?php 
						$nama="";
						if($undangan){
							$i=0;
							foreach($undangan as $dt):
								$i++;
								if($i % 8){
									$str = "&nbsp;";
								}else{
									$str = "</br>";
								}
								
								$nama = $nama . $dt->nama .",";
								echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama."";		
								if($dt->instansi){
									echo "<code>".$dt->instansi."</code>";
								}
								
								echo "</li> ";
							endforeach;
							
						}else{
							echo "-";
						}?></ul></td>
					</tr>							
				</tbody>
			</table>
			<?php
			}
			?>
			
			<?php if($panitia){ ?>
			<h3>Panitia</h3>
			<table class='table table-bordered'>
				<tbody>
					<tr>
						<td width="20%"><em>Panitia</em></td>
						<td><ul class="list-inline"><?php 
						if($panitia){
							$i=0;
							foreach($panitia as $dt):
								$i++;	
								if($i % 8){
									$str = "&nbsp;";
								}else{
									$str = "</br>";
								}
								
								echo "<li class='label label-warning' style='font-weight:normal'>".$dt->nama;		
								if($dt->instansi){
									echo "<code>".$dt->instansi."</code>";
								}	
								echo "</li> ";
							endforeach;							
						}else{
							echo "-";
						}?></ul></td>
					</tr>							
				</tbody>
			</table>
			<?php
			}
			?>
			<h3>Peserta</h3>
			<table class='table table-bordered'>
				<tbody>
					<tr>
						<td width="20%"><em>Dosen/Staff</em></td>
						<td>	<ul class="list-inline"><?php 
						if($staff){
							$i=0;
							foreach($staff as $dt):
								$i++;		
								if($i % 8){
									$str = "&nbsp;";
								}else{
									$str = "</br>";
								}
								
								echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama;		
								if($dt->instansi){
									echo "<code>".$dt->instansi."</code>";
								}	
								echo "</li> ";
							endforeach;
							
						}else{
							echo "-";
						}?></ul></td>
					</tr>		

					<tr>
						<td colspan=2><b>Mahasiswa</b></td></tr>
					<tr>
						<td><em>&nbsp;&nbsp;By Angkatan</em></td>
						<td><ul class="list-inline"><?php 
						
						$nangkatan="";							
						if($mangkatan){										
							$i=0;										
							foreach($mangkatan as $dt):
								$i++;											
								
								//$nangkatan = $nangkatan .$dt->group_by.",";		
								echo "<li class='label label-info' style='font-weight:normal'>".$dt->group_by."</li> ";	
							endforeach;										
						}
							
						?>
					</ul></td></tr>
					<tr>
						<td><em>&nbsp;&nbsp;By Nama</em></td>
						<td width="80%"><ul class="list-inline"><?php 
						if($mhs){
							$i=0;
							foreach($mhs as $dt):
								$i++;	
								if($i % 8){
									$str = "&nbsp;";
								}else{
									$str = "</br>";
								}
								echo "<li class='label label-info' style='font-weight:normal'>".ucwords(strTolower($dt->nama));		
								if($dt->instansi){
									echo "<code>".$dt->instansi."</code>";
								}		
								echo "</li> ";
							endforeach;							
						}else{
							echo "-";
						}?></ul></td>
					</tr>	

					<tr>
						<td width="20%"><em>Peserta Luar</em></td>
						<td width="80%"><ul class="list-inline"><?php 
						if($peserta){
						$i=0;
						foreach($peserta as $dt):
							$i++;							
							echo "<li class='label label-info' style='font-weight:normal'>".$dt->nama;		
							if($dt->instansi){
								echo "<code>".$dt->instansi."</code>";
							}	
							echo "</li> ";
						endforeach;							
					}else{
						echo "-";
					}?></ul></td>
					</tr>								
				</tbody>
			</table>
			
			<?php if($aid): ?>
			<div class="form-actions">
				
				<?php if($this->coms->authenticatedUser->level==1 || $this->coms->authenticatedUser->level==6){ ?>
					<a href="<?php echo $this->location('module/content/event/edit/'.$aid); ?>" class="btn btn-info"><i class="fa fa-edit"></i> Edit Agenda</a>&nbsp;
					<a href="<?php echo $this->location('module/content/event'); ?>" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
				<?php }else { 
						if($this->coms->authenticatedUser->username==$userid){
					?>
					<a href="<?php echo $this->location('module/content/event/edit/'.$aid); ?>" class="btn btn-info"><i class="fa fa-edit"></i> Edit Agenda</a>&nbsp;
					<a href="<?php echo $this->location('module/content/event'); ?>" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
				<?php } } ?>	
			</div>
			<?php endif; ?>
		</form>
	</div>
</div>

<?php		

$this->foot();
?>