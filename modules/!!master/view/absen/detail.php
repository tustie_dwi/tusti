<?php $this->head(); ?>
<fieldset id="cetak">
	<legend>
	<button class="btn btn-primary pull-right btn_cetak" onclick="cetak('<?php echo $thnid ?>', '<?php echo $mk ?>', '<?php echo $kelas?>', '<?php echo $dosen?>', '<?php echo $datemulai?>', '<?php echo $dateselesai?>')"><i class="fa fa-print"></i> Cetak</button>
	<h3>
		Rekap Absen
	</h3>
	</legend>
	<div class="col-md-12">
		<?php 			
			if($prodiid){
				$this->get_rekap($thnid, $prodiid, $mk, $kelas, $dosen, $datemulai, $dateselesai, $view_);
			}else{
				if($view_=='view'){
					$this->get_rekap($thnid, '', $mk, $kelas, $dosen, $datemulai, $dateselesai, $view_);
				}
				else{
					?>
					<div id="content_print">
						<?php
						foreach ($prodi as $p){
							$this->get_rekap($thnid, $p->prodi_id, $mk, $kelas, $dosen, $datemulai, $dateselesai, $view_);
						}
						?>
					</div>
					<?php
				} //will group by prodi for print
			}
		?>
	</div>
</fieldset>
<?php $this->foot(); ?>