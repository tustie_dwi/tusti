<?php $this->head(); 
if($content_view=='content'){
	foreach ($posts as $dt){
		$content_id			= $dt->content_id;
		$hidId				= $dt->contentid;
		$contentcategory	= $dt->content_category;
		$category			= $dt->category;
		$menuorder			= $dt->menu_order;
		$unitid				= $dt->unit_id;
		$fakultasid 		= $dt->fakultas_id;
		$cabangid 			= $dt->cabang_id;
		$title				= $dt->content_title;
		$content_link		= $dt->content_link;
		$content			= $dt->content;
		$listdata			= $dt->content_data;
		$lang				= $dt->content_lang;
		$contentupload		= $dt->content_upload;
		$comment			= $dt->content_comment;
		$img				= $dt->content_thumb_img;
		$parentid			= $dt->parent_id;
		$contentexcerpt		= $dt->content_excerpt;
		$issticky			= $dt->is_sticky;
		$parent_title		= $dt->parent_title;
	}
}else{
	foreach ($posts as $dt){
		$file_id			= $dt->file_id;
		$hidFileId			= $dt->fileid;
		$contentcategory	= $dt->content_category;
		$category			= $dt->category;
		$unitid				= $dt->unit_id;
		$fakultasid 		= $dt->fakultas_id;
		$cabangid 			= $dt->cabang_id;
		$filetitle 			= $dt->file_title;
		$filenote			= $dt->file_note;
		$filedata			= $dt->file_data;
		$filegroup 			= $dt->file_group;
		$file_file_loc		= $dt->file_loc;
		
		$file_file_type		= $dt->file_type;
		$file_file_name		= $dt->file_name;
		$file_file_size		= $dt->file_size;
		$ispublish			= $dt->is_publish;
	}	
	$content			= $filenote;
}
	//---------CHANGE CONTENT-------------------------------
	$src 	= Array();
	$result = Array();
	$string = explode('<', $content);
	foreach ($string as $s) {
		if (strpos($s,'img') !== false) {
			$imgsrc = explode('src', $s);
			preg_match_all('/".*?"|\'.*?\'/', $imgsrc[1], $source);
			// echo substr(str_replace('%20', ' ', $source[0][0]), 1, -1)."<br><br>";
			$src[] = substr(str_replace('%20', ' ', $source[0][0]), 1, -1);
		}
	};
	foreach ($src as $s) {
		$file_tmp_name = $s;
		$file_loc = $this->config->file_url_view."/".$file_tmp_name;
		$result[] = $file_loc;
	}
	$content = str_replace($src, $result, $content);
	if($content_view=='file'){$filenote=$content;};
	//---------CHANGE CONTENT-------------------------------
?>

<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/master/content/page'); ?>">Content</a></li>
  <li class="active"><a href="#">data</a></li>
</ol>
<div class="breadcrumb-more-action">
	<?php if($user!="mahasiswa"&&$user!="dosen"){ ?>
	<a href="<?php echo $this->location('module/master/content/write/page'); ?>" class="btn btn-primary">
	<i class="fa fa-pencil icon-white"></i> New Content</a> 
	<?php } ?>
</div>

<div class="row">
	<div class="col-md-4">
    	<?php $this->view('content/page/sidebar.php', $data); ?>
	</div>
	
	<div id="isi" class="col-md-8">
	  <div class="block-box">
	  	<?php if($content_view=='content'){ ?>
		<table class="table table-bordered">
			<tr>
				<td><b>Judul</b></td>
				<td><p>
					<?php echo $title ?> <code><?php echo $content_link ?></code>
					<?php if($issticky=='1'){ ?>
					<span class="label label-warning"><?php echo 'Sticky' ?></span>
					<?php } ?>
				</p></td>
			</tr>
			<tr>
				<td><b>Kategori</b></td>
				<td><p>
					<?php echo $contentcategory ?>
					<?php if($dt->content_status == 'publish') 
						echo '<span class="label label-success">Published</span>'; 
						else echo '<span class="label label-warning">Draft</span>';
						if($dt->content_comment == '1') 
							echo ' <span class="label label-danger">*</span>';
					?>
				</p></td>
			</tr>
			<tr>
				<td><b>No Urut</b></td>
				<td><?php echo $menuorder ?></td>
			</tr>
			<tr>
				<td><b>Bahasa</b></td>
				<td><?php if($lang=='idn')echo 'Indonesia';else echo 'English'; ?></td>
			</tr>
			<tr>
				<td><b>Konten</b></td>
				<td><?php if($content!=''){ ?>
					<blockquote>
						<?php echo $content ?>
						<?php echo '<small>';$this->get_tag_content($content_id,'view');echo '</small>'; ?>
					</blockquote>
					<?php }else echo '-'; ?>
				</td>
			</tr>
			<tr>
				<td><b>Ringkasan</b></td>
				<td><?php if($contentexcerpt!='')echo $contentexcerpt;else echo '-'; ?></td>
			</tr>
			<tr>
				<td><b>List Data</b></td>
				<td><?php if($listdata!='')echo $listdata;else echo '-'; ?></td>
			</tr>
			<tr>
				<td><b>Tanggal Upload</b></td>
				<td><?php echo $contentupload; ?></td>
			</tr>
			<?php if($parent_title){ ?>
			<tr>
				<td><b>Parent</b></td>
				<td><?php echo $parent_title; ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td><b>Thumbnail</b></td>
				<td><?php if($img!=''){ ?><img class="img-thumbnail" src="<?php echo $this->config->file_url_view."/".$img ?>" /><?php }else echo '-' ?></td>
			</tr>
			<tr>
				<td><b>Attached File</b></td>
				<td><?php $this->get_attach_file($content_id,'view'); ?></td>
			</tr>
		</table>
		<?php }else{ ?>
		<table class="table table-bordered">
			<tr>
				<td><b>Judul</b></td>
				<td><p>
					<?php echo $filetitle ?>
					<code>
						<?php 
						if($file_file_type=='image/jpeg'){
							echo 'Image';
						}else{
							echo 'Video';
						} ?>
					</code>
				</p></td>
			</tr>
			<tr>
				<td><b>Kategori</b></td>
				<td><p>
					<?php echo $contentcategory ?>
					<?php if($ispublish == '1') 
						echo '<span class="label label-success">Published</span>'; 
						else echo '<span class="label label-warning">Draft</span>';
					?>
				</p></td>
			</tr>
			<tr>
				<td><b>File Grup</b></td>
				<td><?php echo $filegroup ?></td>
			</tr>
			<tr>
				<td><b>File Data</b></td>
				<td><?php if($filedata)echo $filedata;else echo '-'; ?></td>
			</tr>
			<tr>
				<td><b>File Note</b></td>
				<td><?php if($filenote!=''){ ?><blockquote><?php echo $filenote ?></blockquote><?php }else echo '-'; ?></td>
			</tr>
			<tr>
				<td><b>File</b></td>
				<td>
					<?php 
					if($file_file_type=='image/jpeg'){
						echo '<img src="'.$this->config->file_url_view."/".$file_file_loc.'" width="400" />';
					}else{
						echo '<video width="400" height="320" controls>
							   <source src="'.$this->config->file_url_view."/".$file_file_loc.'" type="'.$file_file_type.'">
							  </video>';
					} ?><br>
					
				</td>
			</tr>
		</table>
		<?php } ?>
	  </div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="attach-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h3 class="modal-title" id="attach-modal-title">Modal title</h3>
      </div>
      <div class="modal-body">
        <div id="attach-modal-content"></div>
        <h4><b>Note</b></h4>
        <div id="attach-modal-note"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php $this->foot(); ?>