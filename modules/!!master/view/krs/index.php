<?php $this->head(); ?>

<h2 class="title-page">Ruang</h2>
 
<div class="row">
		<div class="col-md-12">	
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="#>">Akademik</a></li>
			  <li class="active"><a href="#">KRS</a></li>
			</ol>
			
		</div>
</div>

<div class="row">
	<div class="col-md-4">
		<div class="panel panel-default">
			  <div class="panel-heading">
			  		<i class="fa fa-pencil"></i> Lihat KRS
			  		<i id="loading" style="display: none" class="fa fa-refresh pull-right fa-spin"></i>
			  	</div>
				<div class="panel-body">	
					<form method="post" id="form-krs">
						<input id="mhs-temp" value="" type="hidden">
						<div class="form-group">	
							<select name="tahun" class="form-control">
								<?php
						    		if($tahun) :
										foreach($tahun as $key) {
											if($key->is_pendek == '') $is_pendek = '';
											else $is_pendek = " (Pendek)";
											$isi = $key->tahun. ' - ' . ucfirst($key->is_ganjil) . $is_pendek;
											echo "<option value='".$isi . '|' .$key->tahun_akademik."'>".$isi ."</option>";
										}
									endif;
						    	?>
							</select>				
						</div>
						
						<div class="form-group">	
							<select name="fakultas" class="form-control">
								<option value="">Pilih Fakultas</option>
								<?php
						    		if($fakultas) :
										foreach($fakultas as $key) {
											$isi = $key->keterangan . ' ('. $key->kode. ')';
											echo "<option value='".$isi . '|' .$key->fakultas_id."'>".$isi ."</option>";
										}
									endif;
						    	?>
							</select>				
						</div>
						
						<div class="form-group">	
							<select name="prodi" class="form-control">
								<option value="">Pilih Prodi</option>
							</select>				
						</div>
						
						<div class="form-group">	
							<input name="angkatan" type="number" class="form-control" value="<?php echo date('Y') ?>" max="<?php echo date('Y') ?>">		
						</div>
					</form>
				</div>
			</div>
	</div>
	
	<div class="col-md-8">
		<div class="panel panel-default" id="krs-box" style="display: none"></div>
		<div class="panel panel-default" id="mhs-box">
			  <div class="panel-heading">
			  		<i class="fa fa-users"></i> Daftar Mahasiswa
			  	</div>
				<div class="panel-body">	
					<div class="input-group">
			          <input type="text" class="form-control input-sm" placeholder="Masukkan NIM / nama" onkeyup="get_mhs(this.value)">
			          <span class="input-group-btn">
			            <button class="btn btn-primary btn-sm" type="button"><i class="fa fa-search"></i></button>
			          </span>
			        </div>
			        <br>
					<table class="table-data table table-hover table-condensed table-bordered" style="margin: 0px">
						<thead>
						<tr>
							<td><i class="fa fa-user"></i> Nama</td>
							<td><i class="fa fa-bookmark"></i> NIM</td>
							<td><i class="fa fa-check"></i> Angkatan</td>
							<td><i class="fa fa-briefcase"></i> Prodi</td>
						</tr>
						</thead>
						
						<tbody>
							<td class="text-center" colspan="4">Daftar Kosong</td>
						</tbody>
					</table>
			        
				</div>
			</div>
		</div>
	</div>
	
</div>
<?php $this->foot(); ?>