<div class="row">
	<?php if($keterangan!="Publikasi"){ ?>
	<div class="col-sm-12">
	<?php }else{ ?>
	<div class="col-sm-6">
	<?php } ?>
		<div class="panel-body well">
			<h3>Progress <?php echo $keterangan?></h3>
			<input type="hidden" name="progress_id" class="form-control" value="<?php if(isset($progress)) echo $progress->progress_id ?>"/>
			<div class="form-group">
				<label class="control-label">Tanggal Pelaksanaan</label>								
				<input type="text" name="tgl_pelaksanaan" class="form-control form_datetime" autocomplete="off" value="<?php if(isset($progress)) echo $progress->tgl_pelaksanaan ?>"/>
			</div>
			<div class="form-group">
				<label class="control-label">Lokasi</label>								
				<input type="text" name="lokasi" class="form-control" autocomplete="off" value="<?php if(isset($progress)) echo $progress->lokasi ?>"/>
			</div>		
			<div class="form-group">
				<label class="control-label">Sumber Dana <?php echo $keterangan?></label>								
				<input type="text" name="sumber_dana_progress" class="form-control" autocomplete="off" value="<?php if(isset($progress)) echo $progress->sumber_dana_progress ?>"/>
			</div>
			<div class="form-group">
				<label class="control-label">Jumlah Dana <?php echo $keterangan?></label>								
				<input type="text" name="jumlah_dana_progress" class="form-control" autocomplete="off" value="<?php if(isset($progress)) echo $progress->jumlah_dana ?>"/>
			</div>
			<div class="form-group">
				<label class="control-label">No. Dokumen</label>								
				<input type="text" name="no_dokumen_progress" class="form-control" autocomplete="off" value="<?php if(isset($progress)) echo $progress->no_dokumen ?>"/>
			</div>
			<div class="form-group">
				<label class="control-label">Dokumen Pendukung <?php echo $keterangan?></label>
				<div id="preview_file_progress" class="well">
					<?php
					if(isset($progress->dokumen_pendukung)){
						$file_name = basename($progress->dokumen_pendukung);
						echo $file_name; 
					?>
						<input type="hidden" name="file_loc_progress" id="file_loc_progress" value="<?php if(isset($progress->dokumen_pendukung)) echo $progress->dokumen_pendukung; ?>">
						&nbsp;<a href="javascript:" onclick="remove_selected('preview_file_progress')"><i class="fa fa-trash-o"></i></a>
					<?php
					}else echo "Belum ada dokumen pendukung";
					?>		
				</div>							
				<input type="file" name="dokumen_progress" class="form-control"/>
			</div>
			<div class="form-group">
				<label class="control-label">Keterangan <?php echo $keterangan?></label>								
				<textarea class="form-control" name="keterangan_progress"><?php if(isset($progress)) echo $progress->keterangan_progress ?></textarea>
			</div>
		</div>
	</div>
	<?php if($keterangan=="Publikasi"){ ?>
	<div class="col-sm-6">
		<div class="panel-body well">
			<h3>Detail <?php echo $keterangan?></h3>
			<div class="form-group">
				<label class="control-label">Kategori Publikasi</label>								
				<select class="form-control e8" name="kategori_publikasi">
					<option value="0">Silahkan Pilih</option>
					<option value="jurnal">Jurnal</option>
					<option value="seminar">Seminar</option>
					<option value="buku">Buku</option>
				</select>
			</div>
			<div id="publikasi_form"></div>
		</div>
	</div>
	<?php } ?>
</div>