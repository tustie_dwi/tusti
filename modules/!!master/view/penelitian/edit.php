<?php $this->head();?>
<h2 class="title-page">Penelitian dan Pengabdian</h2>
<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/master/penelitian'); ?>">Penelitian</a></li>
		  <li><a href="<?php if($param=="Tambah Penelitian") echo $this->location('module/master/penelitian/write');
					         else echo $this->location('module/master/penelitian/edit/'.$id); ?>"><?php echo $param ?></a>
		  </li>
		</ol>
		<form method="post" name="form-penelitian" id="form-penelitian">
			<div class="panel panel-default">
				<div class="panel-heading" id="form-box"><?php echo $param ?></div>
				<div class="panel-body ">
					<div class="form-group">
						<label class="control-label">Fakultas</label>								
						<select class="form-control e9" name="fakultas">
							<option value="0">Silahkan Pilih</option>
							<?php
								foreach($fakultas as $f){
									echo "<option value='".$f->hid_id."' hidid='".$f->fakultasid."' ";
									if(isset($posts_single)&&$posts_single->fakultas_id==$f->hid_id){
										echo "selected";
									}
									echo " >".ucfirst($f->keterangan)."</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Cabang</label>								
						<select class="form-control e9" name="cabang">
							<option value="0">Silahkan Pilih</option>
							<?php
								foreach($cabang as $c){
									echo "<option value='".$c->cabang_id."'";
									if(isset($posts_single)&&$posts_single->cabang_id==$c->cabang_id){
										echo "selected";
									}
									echo ">".ucfirst($c->keterangan)."</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Kategori</label>								
						<select class="form-control e9" name="kategori">
							<option value="0">Silahkan Pilih</option>
							<option value="penelitian" <?php if(isset($posts_single)&&$posts_single->kategori=="penelitian") echo "selected"; ?>>Penelitian</option>
							<option value="pengabdian" <?php if(isset($posts_single)&&$posts_single->kategori=="pengabdian") echo "selected"; ?>>Pengabdian</option>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Status</label>								
						<select class="form-control e9" name="status">
							<option value="0">Silahkan Pilih</option>
							<?php 
							if(isset($posts_single)){
								foreach($status as $s){
									echo "<option value='".$s->status_id."' name='".$s->keterangan."'";
									if(isset($posts_edit) && ($posts_edit->st_publikasi ==$s->status_id)) echo "selected";
									echo " >".ucfirst($s->keterangan)."</option>";
								}
							}
							?>
						</select>
					</div>
					<div class="form-group form_progress">
					</div>
					<div class="form-group">
						<label class="control-label">Tahun Akademik</label>								
						<select class="form-control e9" name="thn_akademik">
							<option value="0">Silahkan Pilih</option>
							<?php
								foreach($thn as $t){
									echo "<option value='".$t->tahun_akademik."'";
									if(isset($posts_single)&&$posts_single->tahun_akademik==$t->tahun_akademik){
										echo "selected";
									}
									echo ">".$t->tahun." ".ucfirst($t->is_ganjil)." ".ucfirst($t->is_pendek)."</option>";
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Judul</label>								
						<input type="text" name="judul" class="form-control" autocomplete="off" value="<?php if(isset($posts_single)) echo $posts_single->judul ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Keterangan</label>								
						<textarea class="form-control" name="keterangan"><?php if(isset($posts_single)) echo $posts_single->keterangan_penelitian ?></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Sumber Dana</label>								
						<input type="text" name="sumber_dana" class="form-control" maxlength="20"  autocomplete="off" value="<?php if(isset($posts_single)) echo $posts_single->inf_sumber_dana ?>"/>
					</div>
					<div class="form-group">
						<label class="control-label">Dokumen Pendukung</label>
						<div id="preview_file_penelitian" class="well">
							<?php
							if(isset($posts_single)&&isset($posts_single->file_pendukung)){
								$file_name = basename($posts_single->file_pendukung);
								echo $file_name; 
							?>
								<input type="hidden" name="file_loc_penelitian" id="file_loc_penelitian" value="<?php if(isset($posts_single->file_pendukung)) echo $posts_single->file_pendukung; ?>">
								&nbsp;<a href="javascript:" onclick="remove_selected('preview_file_penelitian')"><i class="fa fa-trash-o"></i></a>
							<?php
							}else echo "Belum ada dokumen pendukung";
							?>
						</div>								
						<input type="file" name="dokumen_penelitian" class="form-control"/>
					</div>
					<div class="form-group">
						<label class="control-label">Peserta</label>
						<div id="btn-tambah">
							<a href="javascript:" class="btn btn-info add_peserta" data-id="dosen">Tambah Dosen</a>
							<a href="javascript:" class="btn btn-info add_peserta" data-id="mhs">Tambah Mahasiswa</a>
						</div><br>
						<?php if(isset($posts_single)){ ?>
						<div class="edit_peserta">
							<?php
								$i=1;
								foreach($peserta as $ps){ ?>
									<div class='list-peserta del<?php echo $i ?>'><?php echo $i.". " ?>
										<span name="delete_kepada" onclick="deleted('<?php echo $i ?>','<?php echo $ps->peserta_id ?>')" style="cursor: pointer; color : red">Delete <i class="fa fa-trash-o"></i></span>
										<div class="form-group">
											<?php if($ps->karyawan_id!=""){ ?>
												<div class="checkbox">
													<label><input type="radio" name="isketua" onclick="set_ketua('<?php echo $i ?>')" <?php if($ps->is_ketua==1) echo "checked" ?>>&nbsp;Ketua?</label>
													<input type="hidden" class="is_ketua<?php echo $i ?>" name="is_ketua[]" value="<?php echo $ps->is_ketua ?>">
												</div>
												<input type="text" name="dosen[]" class="form-control dosen nama_dosen<?php echo $i ?>" placeholder="Masukkan Nama Dosen" value="<?php if($ps->karyawan_id!="") echo $ps->nama ?>">
												<input type="hidden" name="dosen_id[]" class="form-control dosen_id<?php echo $i ?>" value="<?php if($ps->karyawan_id!="") echo $ps->karyawan_id ?>">
												<input type="hidden" name="peserta_id_dosen[]" class="form-control" value="<?php if($ps->karyawan_id!="") echo $ps->peserta_id ?>">
											<?php }else{ ?>
												<input type="text" name="mhs[]" class="form-control mhs nama_mhs<?php echo $i ?>" placeholder="Masukkan Nama Mahasiswa" value="<?php if($ps->mahasiswa_id!="") echo $ps->nama ?>">
												<input type="hidden" name="mhs_id[]" class="form-control mhs_id<?php echo $i ?>" value="<?php if($ps->mahasiswa_id!="") echo $ps->mahasiswa_id ?>">
												<input type="hidden" name="peserta_id_mhs[]" class="form-control" value="<?php if($ps->mahasiswa_id!="") echo $ps->peserta_id ?>">
											<?php } ?>
										</div>
									</div>
							<?php 
									$i++;
								} ?>
						</div>
						<?php }?>
						<div class="peserta"></div>
					</div>
					<div class="form-group">
						<input type="hidden" name="count" class="form-control" value="<?php if(isset($posts_single)) echo count($peserta); ?>" />
						<input type="hidden" name="periode" class="form-control" value="<?php echo date("Ym") ?>" />
						<input type="hidden" name="id" value="<?php if(isset($posts_single)) echo $posts_single->penelitianid; ?>">
						<input type="hidden" name="hidId" value="<?php if(isset($posts_single)) echo $posts_single->penelitian_id; ?>">
						<button type="submit" class="btn btn-primary">Data Valid &amp; Save</button>
						<button type="button" class="btn btn-danger" style="display: none">Cancel</button>			
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php $this->foot(); ?>