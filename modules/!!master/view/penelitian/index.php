<?php $this->head(); ?>
<h2 class="title-page">Penelitian dan Pengabdian</h2>

<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/master/penelitian'); ?>">Penelitian</a></li>
</ol>
<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/master/penelitian/write'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Penelitian</a>
	<?php if($role=="student employee" || $role=="admin"){ ?>
	&nbsp;<a href="<?php echo $this->location('module/master/penelitian/status'); ?>" class="btn btn-default"><i class="fa fa-gear"></i> Setting Status</a>
	<?php } ?>
</div>
<div class="row">
  <div class="col-md-4">
	<div class="block-box">
		<div class="content">
			<form method="post" id="form-penelitian-index" action="<?php echo $this->location("module/master/penelitian"); ?>">
				<div class="form-group">
					<label class="control-label">Fakultas</label>								
					<select name="fakultas_index" class="form-control e9">
						<option value="0">Silahkan Pilih Fakultas</option>
						<?php
							foreach($fakultas as $f){
								echo "<option value='".$f->hid_id."' hidid='".$f->fakultasid."'";
									if(isset($fakultas_ind)&&$fakultas_ind==$f->hid_id) echo "selected";
								echo ">".ucfirst($f->keterangan)."</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Tahun Akademik</label>								
					<select name="tahunakademik_index" class="form-control e9" <?php if(!isset($thn_ind)){ ?> disabled="disabled" <?php } ?>>
						<option value="0">Silahkan Pilih Tahun Akademik</option>
						<?php
							foreach($thn as $t){
								echo "<option value='".$t->tahun_akademik."'";
									if(isset($thn_ind)&&$thn_ind==$t->tahun_akademik) echo "selected";
								echo ">".$t->tahun." ".ucfirst($t->is_ganjil)." ".ucfirst($t->is_pendek)."</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Kategori</label>								
					<select name="kategori_index" class="form-control e9" <?php if(!isset($kat_ind)){ ?> disabled="disabled" <?php } ?>>
						<option value="0">Silahkan Pilih Kategori</option>
						<option value="penelitian" <?php if(isset($kat_ind)&&$kat_ind=="penelitian") echo "selected" ?>>Penelitian</option>
						<option value="pengabdian" <?php if(isset($kat_ind)&&$kat_ind=="pengabdian") echo "selected" ?>>Pengabdian</option>
					</select>
				</div>
			</form>
			<br>
			
		</div>
	</div>
  </div>
  <div class="col-sm-8">
  	<div class="block-box">
		<div class="content">
		  	<div id="data-post">
				<?php $this->view('penelitian/view_selection.php', $data); ?>
			</div>
			<div class="well" align="center" id="change_select">
				No Data Available
			</div>
		</div>
	</div>
  </div>
</div>

<?php $this->foot(); ?>