<?php $this->head(); 
$header="Mata Kuliah";
?>
<h2 class="title-page"><?php echo $header; ?></h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
  <li class="active"><a href="<?php echo $this->location('module/akademik/mk'); ?>">Mata Kuliah</a></li>
</ol>
<div class="breadcrumb-more-action">
<?php if($user!="mahasiswa"&&$user!="dosen"){ ?>
<a href="<?php echo $this->location('module/master/akademik/write/mk'); ?>" class="btn btn-primary">
<i class="fa fa-pencil icon-white"></i> New Mata Kuliah</a> 
<?php } ?>
</div>

<div class="row">
	<div class="col-md-4">
		<?php $this->view('mk/sidebar.php', $data); ?>
	</div>  
	
	<div id="isi" class="col-md-8">
		<?php 	
		 if(isset($posts)):	?>
		<div class='block-box'><table class='table table-hover' id='example' data-id='module/akademik/mk'>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Mata Kuliah</th>
						<th>Fakultas</th>
			<?php if($user!="mahasiswa"&&$user!="dosen"){				
			echo "		<th>Act</th>";
			} ?>
					</tr>
				</thead>
				<tbody>
			<?php	
				if($posts > 0){
					$i= 1;
					foreach ($posts as $dt): ?>
						<tr>
							<td><?php echo $i ?></td>
							<td><span class='text text-default'><strong><?php echo $dt->namamk ?></strong></span>
								<code>Kode : <?php echo $dt->kode_mk ?></code>&nbsp;
								<span class='label label-success'>Kurikulum : <?php echo $dt->kurikulum ?></span>&nbsp;
								<span class='badge'>SKS : <?php echo $dt->sks ?></span>
								<br>	 <small><em>English Version : <?php echo $dt->engver ?></em></small>				  
							</td>
									
							<td><?php echo $dt->fakultas ?></td>
						<?php										
						if($user!="mahasiswa"&&$user!="dosen"){ ?>
							<td style='min-width: 80px;'>   
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/master/akademik/edit/mk/'.$dt->matakuliah_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
										</li>
									  </ul>
									</li>
								</ul>
							</td></tr>
					<?php	}
						$i++;
					 endforeach; 
				 }
			?>
			</tbody></table></div>	
		 <?php
		 else: 
		 ?>
		<div class="row">
	    <div class="col-md-12" align="center" style="margin-top:20px;">
		    <div class="well">Sorry, no content to show</div>
	    </div>
	    </div>
	    <?php endif; ?>
	</div>
</div>

<?php $this->foot(); ?>