<?php $this->head(); ?>


<div class="row">
		<div class="col-md-8">	
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="<?php echo $this->location('module/master/general/fakultas'); ?>">Fakultas</a></li>
			  <li class="active"><a href="#">Data</a></li>
			</ol>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 
				
			 if( isset($posts) ) :	?>
             
                <div class="block-box">
				<table class='table table-hover' id='example' data-id='module/master/general/fakultas'>
						<thead>
							<tr>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
					
					<?php 
						$i = 1;
						if($posts > 0){
							foreach ($posts as $dt): 
					?>
						<tr valign=top>
							<td>
							<div class="col-md-9">
								<div class="media">
									<a class="pull-left" href="#"><img class="media-object img-thumbnail" width="40" src="<?php echo $this->asset($dt->image); ?>"></a>
									<div class="media-body">
									<?php echo $dt->keterangan ?> <code><?php echo $dt->kode_fakultas ?></code>
									</div>
								</div>
							</div>							
							<div class="col-md-3">
								<ul class='nav nav-pills pull-right'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
										<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/master/general/fakultas/edit/'.$dt->fakultas_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
										</li>
										
									  </ul>
									</li>
								</ul>
							</div>
							</td>
						</tr>
				<?php endforeach; } ?>
				</tbody></table>
				</div>
				
				
			 <?php else: ?>
			<div class="span3" align="center" style="margin-top:20px;">
				<div class="well">Sorry, no content to show</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="col-md-4">
			
				<?php echo $this->view("fakultas/edit.php", $data); ?>
				
		</div>	
	</div>
<?php $this->foot(); ?>