<?php

if(isset($edit)){
	$header		= "Edit Fakultas";
	
	$keterangan	= $edit->keterangan;
	$kode_fakultas = $edit->kode_fakultas;
	$image = $edit->image;
	$id			= $edit->hid_id;
	
	$ceknew			= 0;
	$frmact 	= $this->location('module/master/general/fakultas/save');		
	
}else{
	$header			= "Write New Fakultas";
	$id				= "";
	$keterangan		= "";
	$kode_fakultas 	= "";
	$image			= "";
	$ceknew			= 1;
	$frmact 		= $this->location('module/master/general/fakultas/save');		
}


?>

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil"></i> <?php echo $header ?></div>
	<div class="panel-body">
			<form id="form-fakultas" method="post" >
				<div class="form-group">	
					<label class="control-label">Kode Fakultas</label>					
						<input id="kode_fakultas" type="text"  class="form-control" <?php if(isset($edit)){echo 'disabled="disabled"';} ?> name="kode_fakultas" value="<?php echo $kode_fakultas; ?>">
						<input id="kode_fakultas" type="hidden"  class="form-control" name="kode_fakultas" value="<?php echo $kode_fakultas; ?>">
				</div>
				<div class="form-group">	
					<label class="control-label">Nama Fakultas</label>					
					<input id="keterangan" type=text  class="form-control" name="keterangan" value="<?php echo $keterangan; ?>">
				</div>
				<div class="form-group">
					 <label class="control-label" for="content_title">Foto</label>
					 <div class="controls">                    
						<input id="files" name="files" type="file">
						<output id="list"></output>
						<?php 
							if($ceknew == 0){
								if(isset($image)){ ?>
									<div class='well'>
										<img style="width: 100px; height: auto;" src="<?php echo $this->asset($image); ?>"/>
										<input name="hidimg" value="<?php echo $image ?>" type="hidden">
									</div>
								<?php } else { ?>
									<div class='well'>
										<p>Icon Belum Tersedia</p>
									</div>
								<?php } 
							}
						?>
					</div>       
				</div>										
				<div class="form-group">				
					<input type="hidden" name="hidId" value="<?php echo $id;?>">
					<input type="hidden" name="ceknew" value="<?php echo $ceknew;?>">	
					
					<button class="btn btn-primary" name="b_fakultas" id="submit" data-value="1"> Simpan </button>
					<a href="<?php echo $this->location("module/master/general/fakultas"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>
				</div>		
							
			</form>
		</div>
	</div>