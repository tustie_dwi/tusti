<?php $this->head(); 
$header="gjm/Dosen";
?>
<div class="row">
    
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/master/gjm'); ?>">Jaminan Mutu</a></li>
	  <li class="active"><a href="#">Data</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/master/gjm/write'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Upload Jaminan Mutu</a> 
    </div>
	
	<div class="row">
		<div class="col-md-4">	
			<div id="parameter" class="block-box">
			   <div class="content">
				<form action="<?php echo $this->location('module/master/gjm'); ?>" role="form" id="param" method="POST">
						
						<div class="form-group">
							<label class="control-label">Fakultas</label>								
							<select id="select_fakultas" class="e9 form-control" name="fakultas">
								<option value="-">Select Fakultas</option>
								<?php if(count($fakultas)> 0) {
									foreach($fakultas as $f) :
										echo "<option value='".$f->hid_id."' ";
											if(isset($fakultas_id)&&$fakultas_id==$f->hid_id){
												echo "selected";
											}
										echo " >".ucWords($f->keterangan)."</option>";
									endforeach;
								} ?>
							</select>
						</div>
						
						<div class="form-group">	
							<label class="control-label">Cabang</label>					
								<?php echo '<select id="select_cabang" class="form-control e9" name="cabang" >'; ?>
								<option value="0" data-uri='1'>Select Cabang</option>
								<?php if(count($cabang)> 0) {
									foreach($cabang as $c) :
										echo "<option value='".$c->cabang_id."' ";
										if(isset($cabangid)){
											if($cabangid==$c->cabang_id){
												echo "selected";
											}
										}
										echo " >".ucWords($c->keterangan)."</option>";
									endforeach;
								} ?>
								</select>
						</div>
						
						<input type="hidden" id="unit-select" value="<?php if(isset($unitid))echo $unitid ?>" />
							<div class="form-group">	
								<label class="control-label">Unit</label>					
									<select id="select_unit" class="form-control e9" name="unit" >
									<option value="0">Pilih Unit</option>
									<?php if(count($unit)> 0) {
										foreach($unit as $u) :
											echo '<optgroup label="'.$u->fakultas.'">';
											$this->read_unit_child($u->fakultas_id,$unitid);
											echo '</optgroup>';
										endforeach;
									} ?>
									</select>
							</div>					
					</form>
			  </div>
			</div>
		</div>
	
		<div class="col-md-8">			
			
				 <?php
				 if(isset($form)): 
					$this->view('gjm/edit.php', $data);
				 else:
					?>
					<div id="isi" class="block-box">
					<?php
					 if(isset($status) and $status) : ?>
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $statusmsg; ?>
						</div>
					<?php 
					endif; 
					
					if( isset($posts) ) :	?>
					<table class='table table-hover example'>
						<thead>
							<tr>
								<th>&nbsp;</th>	
							</tr>
						</thead>
						<tbody>
						<?php if($posts > 0){
							foreach ($posts as $dt): ?>
							<tr>
								<td>
								<div class="col-md-9">
								<span class='text text-default'><strong>
									<a href="<?php echo $this->location('module/master/gjm/detail/'.$dt->file_id) ?>"><span class="text text-default"><b><?php echo $dt->judul; ?>
									</strong></span></b></span></a>
									<span class="label label-info"><?php echo $dt->kategori ?></span> <?php if($dt->unit) echo "<span class='label label-danger'>$dt->unit</span>";?> <?php if($dt->is_publish==1) echo "<small><span class='text text-danger'>* publish</span></small>"; ?>
									<br>
									<small><em><?php echo $dt->file_name ?>&nbsp;<code><?php echo $dt->file_type ?></code></em></small>
								</div>
								<div class="col-md-3">
									<ul class='nav nav-pills'>
										<li class='dropdown pull-right'>
										  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
										  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
											<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/master/gjm/edit/'.$dt->file_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
											</li>
										  </ul>
										</li>
									</ul>
								</div>
								</td>
							</tr>
						<?php endforeach; 
							 } ?>
						</tbody>
						</table>
					
					 <?php else: 
					 ?>
					
						<div class="well">Sorry, no content to show</div>
					<?php endif; ?>
					</div>
					<?php
				endif;
				?>
		
		</div>
    </div>
</div>
<?php $this->foot(); ?>