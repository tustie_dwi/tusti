<?php 
if(isset($edit)){
	$header		= "Edit Jaminan Mutu";
	
	$id				= $posts->hid_id;
	$kategoriid		= $posts->kategori_id;	
	$fakultas_id	= $posts->fakultas_id;
	$judul			= $posts->judul;
	$judul_en		= $posts->english_version;
	$file_name		= $posts->file_name;
	$file_loc		= $posts->file_loc;
	$file_type		= $posts->file_type;
	$jenis_file		= $posts->jenis_file;
	$no_dok			= $posts->no_dokumen;
	$ispublish		= $posts->is_publish;
	$unit_id			= $posts->unit_id;
	$cabangid		= $posts->cabang_id;
	$siklus			= $posts->siklus;

}else{
	$header		= "Write New Jaminan Mutu";
	$siklus     = 0;
}
?>

<div class="panel panel-default">
  <div class="panel-heading"><?php echo $header; ?></div>
  <div class="panel-body ">
	<form method=post  name="form" action="<?php echo $this->location('module/master/gjm'); ?>" id="form-gjm" enctype="multipart/form-data">
		
		<div class="form-group">	
			<label >Fakultas</label>
			<select id="select_fak" class="e9 form-control" name="fakultas" >
				<option value="-">Select Fakultas</option>
				<?php if(count($fakultas)> 0) {
					foreach($fakultas as $f) :
						echo "<option value='".$f->hid_id."' ";
							if(isset($fakultas_id)&&$fakultas_id==$f->hid_id){
								echo "selected";
							}
						echo " >".ucWords($f->keterangan)."</option>";
					endforeach;
				} ?>
			</select>
		</div>
		
		<div class="form-group">	
			<label >Cabang</label>
			<select id="select_cab" class="e9 form-control" name="cabang" >
				<option value="0">Select Cabang</option>
				<?php if(count($cabang)> 0) {
					foreach($cabang as $c) :
						echo "<option value='".$c->cabang_id."' ";
						if(isset($cabangid)){
							if($cabangid==$c->cabang_id){
								echo "selected";
							}
						}
						echo " >".ucWords($c->keterangan)."</option>";
					endforeach;
				} ?>
			</select>
		</div>	

		<div class="form-group">	
			<input type="hidden" id="unit-select-id" value="<?php if(isset($unit_id))echo $unit_id ?>" />
			<label class="control-label">Unit</label>					
				<select id="select_unit_id" class="form-control e9" name="unit" >
				<option value="0">Pilih Unit</option>
				<?php if(count($unit)> 0) {
					foreach($unit as $u) :
						echo '<optgroup label="'.$u->fakultas.'">';
						$this->read_unit_child($u->fakultas_id,$unit_id);
						echo '</optgroup>';
					endforeach;
				} ?>
				</select>
		</div>		
		
		<div class="form-group">
			<label>Kategori</label>			
			<select name="cmbkategori" class="e9 form-control" >
				<option value="0">Select Kategori</option>
				<?php if($kategori) {
					foreach($kategori as $dt) :
						echo "<option value='".$dt->kategori_id."' ";
						if(isset($kategoriid)){
							if($kategoriid==$dt->kategori_id){
								echo "selected";
							}
						}
						echo " >";
						if($dt->parent_id=='0') echo strtoUpper($dt->keterangan)."</option>";						
						else echo "  -- ".$dt->keterangan."</option>";
					endforeach;
				} ?>
			</select>			
		</div>
		<div class="form-group">
			<label>No Dokumen</label>
			<input id="no_dok" name='no_dok' required='required' type='text' value ='<?php if(isset($no_dok))echo $no_dok ?>' class="form-control">	
		</div>
		
		<div class="form-group">
			<label>Siklus ke</label>
			<input id="siklus" name='siklus' required='required' type='text' value ='<?php if(isset($siklus))echo $siklus ?>' class="form-control">	
		</div>
				
		<div class="form-group">
			<label>Judul</label>
			<input id="judul" name='judul' required='required' type='text' value ='<?php if(isset($judul))echo $judul ?>' class="form-control">	
		</div>	
		
		<div class="form-group">
			<label>English version</label>
			<input id="judul" name='judul_en' required='required' type='text' value ='<?php if(isset($judul_en))echo $judul_en ?>' class="form-control">	
		</div>	

		<div class="form-group">
			<label>Keterangan</label>
			<textarea name="keterangan" cols="5" class="form-control"><?php if(isset($edit)) echo $posts->keterangan;?></textarea>
		</div>

		<div class="form-group">		
			<input name='uploads[]' type='file' class='form-control'><?php if(isset($file_name)) echo $file_name; ?>				
		</div>
		<div class="form-group">
			<div class="checkbox">
				<label>
				  <input type="checkbox" name="chkpublish" value="1" <?php if(isset($ispublish) && ($ispublish==1)) echo "checked" ?>> Publish
				</label>
			  </div>
		</div>
		<div class="form-group">
				<input type=hidden name="hidid" value="<?php if(isset($edit)) echo $posts->hid_id; ?>">
				<input type="submit" name="b_submit" value="  Submit " class="btn btn-primary" id="b_gjms">
				<a href="<?php echo $this->location("module/master/gjm"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>
			</div>
		</div>			
	</form>
  </div>
</div>