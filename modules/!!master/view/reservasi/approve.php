<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Reservasi</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Reservasi Ruang</a></li>
		  <li class="active"><a href="#">Approval</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-4">
		<div class='block-box'>
			<div class="list-group list-reservasi" style="margin-bottom: 0px">
			  	<?php 
			  		if($reservasi) :
						foreach($reservasi as $key){
							echo '<a dataid="'.$key->jadwal_id.'" onclick="detail_reservasi(\''.$key->jadwal_id.'\')" href="#" class="list-group-item">'.$key->kegiatan.'</a>';
						}
					endif;
				?>
			</div>
			
			<br>
			<div class="btn-group btn-group-justified">
		      <a class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> Sebelumnya</a>
		      <a class="btn btn-default" role="button">Selanjutnya <i class="fa fa-arrow-right"></i></a>
		    </div>
		</div>
		
	</div>
	
	<div class="col-md-8">
		<div class='block-box detail-reservasi'>
			<i id="loading" class="fa fa-refresh fa-spin" style="position: absolute; display: none"></i>
			<h3 style="margin-top: 0; text-align: center">Reservasi Ruangan</h3>
			
		</div>
	</div>
</div>
<?php 
	$this->foot();
?>

<style type="text/css" media="screen">
	#ruang-field .btn-xs{
		margin-bottom: 3px;
		margin-left: 3px;
	}
</style>
































