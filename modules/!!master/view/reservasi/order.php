<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Reservasi</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li class="active"><a href="#>">Reservasi Ruang</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class='block-box'>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo $this->location('module/master/reservasi/booking') ?>">
				<div class="row">
					<div class="col-md-12">
						<h3 class="text-center" style="margin-top: 0px">Reservasi Ruangan</h3>
					</div>
					<div class="col-md-9">
						<div class="form-group">
						    <label class="col-sm-3 control-label">Jenis Kegiatan</label>
						    <div class="col-sm-9">
						      	<select type="text" class="form-control" name="jenis_kegiatan">
						      		<option value="">Jenis Kegiatan</option>
						      		<?php
						      			if($jenis_kegiatan) :
							      			foreach($jenis_kegiatan as $key):
												echo "<option value='".$key->jenis_kegiatan_id."'>".$key->keterangan."</option>";
											endforeach;
										endif;
						      		?>
						    	</select>
						    </div>
						</div>
						<div class="form-group">
						    <label class="col-sm-3 control-label">Kegiatan</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="kegiatan">
						    </div>
						</div>
						<div class="form-group">
						    <label class="col-sm-3 control-label">Tanggal</label>
						    <div class="col-sm-4">
						      <input name="tgl_mulai" value="<?php echo $tgl_mulai_select ?>" class="form-control" readonly>
						    </div>
						     <div class="col-sm-5">
						       <input name="tgl_selesai" value="<?php echo $tgl_selesai_select ?>"  class="form-control" readonly>
						    </div>
						</div>
						<div class="form-group">
						    <label class="col-sm-3 control-label">Jam</label>
						    <div class="col-sm-4">
						      <input type="text" data-format="HH:mm:ss" class="form-control pick-time" placeholder="Jam Mulai" name="jam_mulai">
						    </div>
						    <div class="col-sm-5">
						      <input type="text" data-format="HH:mm:ss" class="form-control pick-time" placeholder="Jam Selesai" name="jam_selesai">
						    </div>
						</div>
						<div class="form-group">
						    <label class="col-sm-3 control-label">Catatan</label>
						    <div class="col-sm-9">
						    	<textarea class="form-control" name="catatan"></textarea>
						    </div>
						</div>
						<div class="form-group">
						    <label class="col-sm-3 control-label"></label>
						    <div class="col-sm-9">
						    	<button class="btn btn-primary btn-block">Pesan</button>
						    </div>
						</div>
					</div>
					
					<div class="col-md-3">
						<span id="ruang-field" style="margin-top: 0">
						
						</span>
					</div>
				</div>
			</form>
		</div>
		<div class='block-box'>
			<form class="form-inline" id="form-reservasi" role="form" method="POST" action="<?php echo $this->location("module/master/reservasi") ?>">
				<div class="form-group">
				    <input type="text" value="<?php echo $tgl_mulai_select ?>" name="tgl_mulai" class="pick-date form-control" data-format="YYYY-MM-DD" value="" placeholder="Tanggal Kegiatan" >
				</div>
				<div class="form-group">
				    <input type="text" value="<?php echo $tgl_selesai_select ?>" name="tgl_selesai" class="pick-date form-control" data-format="YYYY-MM-DD" value="" placeholder="Tanggal Kegiatan" >
				</div>
				<div class="form-group">
				    <select class="form-control ruang-select" name="ruang">
				    	<option value="">Semua Ruang</option>
				    	<?php 
				    		if($ruang_all) :
					    		foreach($ruang_all as $key) :
									echo "<option value='".$key->ruang_id."'>".$key->kode_ruang."</option>";
								endforeach;
							endif;
						?>
				    </select>
				</div>
				<div class="form-group">
				    <button class="btn btn-primary filter-room">Cari</button>
				</div>
			</form>
			
			<br>
			<?php
				$kegiatan_arr = array();
				if($kegiatan) :
					foreach($kegiatan as $key){
						$kegiatan_arr[$key->ruang] = array();
					}
					
					foreach($kegiatan as $key){
						$tmp["jam_mulai"] = $key->jam_mulai;
						$tmp["jam_selesai"] = $key->jam_selesai;
						$tmp["kegiatan"] = $key->kegiatan;
						
						array_push($kegiatan_arr[$key->ruang], $tmp);
					}
				endif;
			?>
			
			<!-- <pre><?php echo print_r($kegiatan_arr); ?></pre> -->
			<?php if($ruang) : ?>
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<td>Ruang</td>
							<?php 
								for($i=7; $i<=18; $i++){
									if(strlen($i) == 1) $jam = '0'.$i;
									else $jam = $i;
									
									echo "<td>".$jam.":00</td>";
									echo "<td>".$jam.":30</td>";
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php foreach($ruang as $key) : ?>
							<tr row-ruang="<?php echo $key->ruang_id ?>">
								<td><?php echo $key->kode_ruang ?></td>
								<?php 
								for($i=7; $i<=18; $i++){
									if(strlen($i) == 1) $jam = '0'.$i;
									else $jam = $i;
									
									if(strlen(($i+1)) == 1) $selesai = '0'.($i+1);
									else $selesai = $i;
									
									echo "<td style='text-align: center'>".get_kegiatan($kegiatan_arr, $jam.':00:00', $selesai . ':29:00', $key->ruang_id)."</td>";
									echo "<td style='text-align: center'>".get_kegiatan($kegiatan_arr, $jam.':30:00', $selesai . ':00:00', $key->ruang_id)."</td>";
								}
							?>
							</tr>	
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else : ?>
				<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
		</div>
	</div>
	
</div>
<?php 
	$this->foot();
	
	function get_kegiatan($kegiatan, $jam, $selesai, $ruang){
		$nilai = "order_ruang(\"$ruang\",\"$jam\",\"$selesai\")";
		$isi = "<span style='cursor: pointer' class='label label-success' onclick='".$nilai."'><i class='fa fa-check'></i></span>";
		if(! isset($kegiatan[$ruang])) return $isi;
		else{
			if($kegiatan[$ruang]) :
				foreach($kegiatan[$ruang] as $key){
					$date1 = DateTime::createFromFormat('H:i:s', $jam);
					$date_end = DateTime::createFromFormat('H:i:s', $selesai);
					$date2 = DateTime::createFromFormat('H:i:s', $key["jam_mulai"]);
					$date3 = DateTime::createFromFormat('H:i:s', $key["jam_selesai"]);
					
					// $isi = $jam . ' ' . $selesai . ' ' .  $key["jam_mulai"] . ' ' .  $key["jam_selesai"];
					$isi = '';
					if ( ($date1 >= $date2 && $date1 <= $date3) || ($date_end >= $date2 && $date_end <= $date3) )
					{
						$title = $key['kegiatan'] . ' - ' . $ruang . ' (' . substr($key["jam_mulai"],0,5) . ' - ' . substr($key["jam_selesai"],0,5) . ')';
					   	$isi .= '<span style="cursor: pointer" class="label label-danger" title="'.$title.'"><i class="fa fa-times"></i></span>';
						return $isi;
					}
				}
			endif;
			
			$isi = "<span style='cursor: pointer' class='label label-success'  onclick='".$nilai."'><i class='fa fa-check'></i></span>";
			return $isi;
		}
	}
?>

<style>
	#ruang-field .btn-xs{
		margin-bottom: 3px;
	}
	.del-ruang{
		cursor: pointer;
	}
</style>
































