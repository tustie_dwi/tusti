<?php
	 if( isset($posts) ) :	?>
	
		<div class='block-box'><table class='table table-hover' id='example'>
			<thead>
				<tr>
					<th>&nbsp;</th>							
					
				</tr>
			</thead>
			<tbody>
			
	<?php	if($posts > 0){
				$i = 1;
				foreach ($posts as $dt): ?>
					<tr class="tr-<?php echo $dt->mkditawarkan_id ?>">							
					<td>
						<div class="col-md-3">
						<small><i class="fa fa-clock-o"></i> <?php echo str_replace('-', '/',$dt->YMD)."  ".$dt->waktu."<br>"; ?>
							 <span class="text text-danger"><?php echo ucWords($dt->user_id) ?></span></small>
					</div>
					<div class="col-md-6">
						<a href="<?php echo $this->location('module/master/detail/mkditawarkan/'.$dt->mkditawarkan_id) ?>"><span class="text text-default"><b><?php echo $dt->kodemk ?></b></span></a>
						<?php echo $dt->namamk ?>
						<span class='label label-default' >Kuota <?php echo $dt->kuota ?></span>&nbsp;
						<?php if($dt->is_blok == '1'){ ?>
						<span class='label label-success'>*<?php echo $dt->parentmk ?></span>
						<?php } ?>
						<code><?php echo ucwords($dt->thnakademik) ?> </code><br>
						<?php
						$mmk = new model_mk();
						
						$pengampu = $mmk->get_dosen_pengampu($dt->mkditawarkan_id);
						if($pengampu){
							foreach($pengampu as $dk):
								echo "<span class='label label-normal label-inline-block'><i class='fa fa-user'></i> ".$dk->nama."</span>&nbsp;";
							endforeach;
						}
						?>
					</div>
				
				
					<div class="col-md-3">   
						<ul class='nav nav-pills'>
							<li class='dropdown pull-right'>
								 <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								 <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/master/akademik/edit/mkditawarkan/'.$dt->mkditawarkan_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/master/akademik/detail/mkditawarkan/'.$dt->mkditawarkan_id) ?>"><i class='fa fa-eye'></i> Detail</a>	
									</li>
									<li>
									<a class='btn-edit-post' onclick="delete_mk_(this, '<?php echo $dt->mkditawarkan_id ?>')" href="#"><i class='fa fa-trash-o'></i> Delete</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</div>
						</td></tr>
						<?php 
					 $i++; 
				 endforeach;
				
			 } ?>
		</tbody></table></div>
		
		
	 <?php	
	 else: 
	 ?>
	<div class="well" align="center">Sorry, no content to show</div>
	<?php endif; ?>