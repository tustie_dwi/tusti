<?php $this->head(); ?>

<h2 class="title-page">Matakuliah Ditawarkan</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
  <?php if($view_body=='index'){ ?>
  <li class="active"><a href="<?php echo $this->location('module/master/akademik/mkditawarkan'); ?>">Matakuliah Ditawarkan</a></li>
  <?php }elseif($view_body=='write'){ ?>
  <li><a href="<?php echo $this->location('module/master/akademik/mkditawarkan'); ?>">Matakuliah Ditawarkan</a></li>
  <li class="active"><a href="#">New</a></li>	
  <?php }elseif($view_body=='edit'){ ?>
  <li><a href="<?php echo $this->location('module/master/akademik/mkditawarkan'); ?>">Matakuliah Ditawarkan</a></li>
  <li class="active"><a href="#">Edit</a></li>
  <?php } ?>
  
</ol>

<div class="breadcrumb-more-action">
<?php if($view_body=='index'){ ?>	
<a href="<?php echo $this->location('module/master/akademik/write/mkditawarkan'); ?>" class="btn btn-primary">
<i class="fa fa-pencil icon-white"></i> New Mata Kuliah Ditawarkan</a> 
<?php } ?>
</div>

<div class="row">
 <div class="col-md-12">
  <div class="row">
  	
	<div class="col-md-4">
    	<?php $this->view('mkditawarkan/sidebar.php', $data); ?>
	</div>  
	
	<div id="isi" class="col-md-8">
		<?php switch($view_body){
			case 'index';
				$this->view('mkditawarkan/view-index.php', $data);
			break;
			case 'write';
				$this->view('mkditawarkan/view-edit.php', $data);
			break;
			case 'edit';
				$this->view('mkditawarkan/view-edit.php', $data);
			break;
		} ?>
	</div>
	 
  </div>
 </div>
</div>

<?php $this->foot(); ?>