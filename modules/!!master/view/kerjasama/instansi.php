<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Kerjasama</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Kerjasama</a></li>
		  <li class="active"><a href="#">Instansi</a></li>
		</ol>
		<div class="breadcrumb-more-action">
			<a href="#" class="btn btn-primary" onclick="tambah()">
    		<i class="fa fa-pencil icon-white"></i> Tambah Instansi</a> 
        </div>
	</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="panel panel-default" >
			  	<div class="panel-heading">
			  		<i class="fa fa-search"></i> Pencarian Instansi
			  	</div>
				<div class="panel-body">	
					<form id="form-instansi" role="form" method="post" action="<?php echo $this->location("module/master/kerjasama/instansi") ?>">
					  	<div class="form-group">
					  		<select class="form-control" name="fakultas">
					    		<option value="">Semua Fakultas</option>
					    		<?php
									if($fakultas) :
										foreach($fakultas as $key) :
											if($sel_fakultas == $key->fakultas_id) $sel = "selected";
											else $sel = '';
											
											echo "<option ".$sel." value='".$key->fakultas_id."'>".$key->keterangan."</option>";
										endforeach;
									endif;						    		
					    		?>
					    	</select>
						</div>
						<div class="form-group">
							<select class="form-control" name="kategori">
					    		<option value="">Semua Kategori</option>
					    		<?php
									if($kategori) :
										foreach($kategori as $key) :
											if($sel_kategori == $key->kategori_id) $sel = "selected";
											else $sel = '';
											
											echo "<option ".$sel." value='".$key->kategori_id."'>".$key->keterangan."</option>";
										endforeach;
									endif;						    		
					    		?>
					    	</select>
						</div>
					</form>
				</div>
		</div>
	</div>
	<div class="col-md-9" id="form-data">
		<div class='block-box'>
			<?php if($instansi) : ?>
			<table class='table table-hover example'>
				<thead>
					<tr>						
						<th>&nbsp;</th>
						<th width="10%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($instansi as $key) : ?>
						<tr>
							
							<td>
								<img style="margin-right: 10px" class="img img-thumbnail pull-left" height="50" width="150" src="<?php echo $this->location('assets/'.$key->logo) ?>">
								<?php 
									echo "<b>".$key->nama_instansi . "</b> <code>".$key->kategori."</code>";
									echo "<br><i style='color: #16a085' class='fa fa-briefcase'></i> <small>".$key->fakultas."</small>";
									echo "<br><small>";
									echo "<i class='fa fa-user'></i> " . $key->contact_person;
									echo " <i style='color:#2980b9' class='fa fa-phone-square'></i> " . $key->telp;
									echo " <br><i style='color:#e74c3c' class='fa fa-map-marker'></i> " . $key->alamat;
									echo "</small>";
									echo '<br><small><em>'.$key->keterangan.'.</em></small>';
								?>
							</td>
							<td>
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
											<a class='btn-edit-instansi' data-kategori_id="<?php echo $key->kategori_id ?>" data-id="<?php echo $key->id ?>"
												data-fakultas_id="<?php echo $key->fakultas_id ?>" data-nama_instansi="<?php echo $key->nama_instansi ?>"
												data-contact_person="<?php echo $key->contact_person ?>" data-telp="<?php echo $key->telp ?>"
												data-alamat="<?php echo $key->alamat ?>" data-keterangan="<?php echo $key->keterangan ?>"
											>
												<i class='fa fa-edit'></i> Edit
											</a>
											<a class='btn-edit-post' onclick="return confirm('Apakah Anda ingin menghapus data ini?')" href="<?php echo $this->location('module/master/kerjasama/del_instansi/'.$key->id) ?>">
												<i class='fa fa-trash-o'></i> Hapus
											</a>	
										</li>
									  </ul>
									</li>
								</ul>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php else : ?>
				<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="col-md-9" style="display: none" id="form-manage">
		<div class="panel panel-default" >
			  	<div class="panel-heading">
			  		<i class="fa fa-plus"></i> Tambah Instansi
			  	</div>
				<div class="panel-body">	
					<form enctype="multipart/form-data" class="form-horizontal" role="form" method="post" action="<?php echo $this->location('module/master/kerjasama/save_instansi') ?>">
					  	<div class="form-group">
					  		<input name="id" type="hidden">
						    <label class="col-sm-2 control-label">Fakultas</label>
						    <div class="col-sm-10">
						    	<select class="form-control" name="fakultas_id">
						    		<option value="">Pilih Fakultas</option>
						    		<?php
										if($fakultas) :
											foreach($fakultas as $key) :
												echo "<option value='".$key->fakultas_id."'>".$key->keterangan."</option>";
											endforeach;
										endif;						    		
						    		?>
						    	</select>
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-2 control-label">Kategori</label>
						    <div class="col-sm-10">
						    	<select class="form-control" name="kategori_id">
						    		<option value="">Pilih Kategori</option>
						    		<?php
										if($kategori) :
											foreach($kategori as $key) :
												echo "<option value='".$key->kategori_id."'>".$key->keterangan."</option>";
											endforeach;
										endif;						    		
						    		?>
						    	</select>
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-2 control-label">Nama</label>
						    <div class="col-sm-10">
						    	<input class="form-control" name="nama_instansi" type="text">
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-2 control-label">Contact Person</label>
						    <div class="col-sm-10">
						    	<input class="form-control" name="contact_person" type="text">
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-2 control-label">Telepon</label>
						    <div class="col-sm-10">
						    	<input class="form-control" name="telp" type="text">
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-2 control-label">Alamat</label>
						    <div class="col-sm-10">
						    	<textarea class="form-control" name="alamat"></textarea>
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-2 control-label">Keterangan</label>
						    <div class="col-sm-10">
						    	<textarea class="form-control" name="keterangan"></textarea>
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label class="col-sm-2 control-label">Logo</label>
						    <div class="col-sm-10">
						    	<input class="form-control" type="file" name="logo">
						    </div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label for="inputEmail3" class="col-sm-2 control-label"></label>
						    <div class="col-sm-10">
						      <button class="btn btn-primary">Simpan</button>
						      <a onclick="batal()" class="btn btn-default">Batal</a>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
	</div>
</div>
<?php $this->foot(); ?>