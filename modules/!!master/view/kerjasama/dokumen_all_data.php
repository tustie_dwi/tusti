<div class='block-box'>
	<?php if(isset($detail)) : ?>
		<h3 class="text-center" style="margin-top: 5px"><?php echo $detail->judul ?></h3>
		<p style="text-align: center">
			<i class="fa fa-briefcase"></i> <?php echo $detail->nama_instansi ?> 
			<i class="fa fa-map-marker"></i> <?php echo $detail->lokasi ?> 
			<i class="fa fa-calendar"></i> <?php echo $detail->tgl_mulai . ' s/d '. $detail->tgl_selesai ?> 
			<br>
			<i class="fa fa-tags"></i> <?php echo str_replace(",", ", ", $detail->ruang_ket) ?> 
		</p>
	<?php else  : ?>
		<h3 class="text-center" style="margin-top: 5px">
			<?php echo $fakultas->keterangan ?>
		</h3>
		<p style="text-align: center">
			<i class="fa fa-user"></i> <?php echo $fakultas->contact_person ?>
			<i class="fa fa-phone-square"></i> <?php echo $fakultas->telp ?> 
			<br>
			<i class="fa fa-map-marker"></i> <?php echo $fakultas->alamat ?> 
		</p>
	<?php endif; ?>
</div>
<div class='block-box'>
	<?php if($dokumen) : ?>
	<table class='table table-hover' id='example' style="font-size: 100%">
		<thead>
			<tr>
				<th width="10%">No.</th>
				<th width="40%">Dokumen</th>
				<th width="50%">File</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; foreach($dokumen as $key) : ?>
				<tr>
					<td><?php echo $no; $no++ ?></td>
					<td>
						<?php 
							echo $key->judul;
							echo "<br><code><i class='fa fa-check-square'></i> ".$key->keterangan."</code>";
						?>
					</td>
					<td>
						<?php
							$confirm = 'return confirm("Apakah Anda ingin menghapus file ini?")';
							foreach(explode(",", $key->file_name) as $dt){
								$file = explode("|", $dt);
								echo "<a target='_blank' href='".$this->location('assets/'.$file[1])."'>
									<i class='fa fa-cloud-download'></i></a> ";
								echo "<a onclick='".$confirm."' style='color: #e74c3c' href='".$this->location('module/master/kerjasama/del_file/'.$kegiatan.'/'.substr(md5($file[2]),7,6) )."'><i class='fa fa-trash-o'></i></a> ";
								echo $file[0];
								echo "<br>";
							}
						?>	
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php else : ?>
		<div class="well">Sorry, no content to show</div>
	<?php endif; ?>
</div>
<script src="<?php echo $this->location() ?>assets/js/datatables/DT_bootstrap.js"></script>