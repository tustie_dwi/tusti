<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Kerjasama</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="<?php echo $this->location('module/master/kerjasama/instansi') ?>">Kerjasama</a></li>
		  <li class="active"><a href="#">Dokumen</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-8">
		<div class='block-box'>
			<h3 class="text-center" style="margin-top: 5px"><?php echo $detail->judul ?></h3>
			<p style="text-align: center">
				<i class="fa fa-briefcase"></i> <?php echo $detail->nama_instansi ?> 
				<i class="fa fa-map-marker"></i> <?php echo $detail->lokasi ?> 
				<i class="fa fa-calendar"></i> <?php echo $detail->tgl_mulai . ' s/d '. $detail->tgl_selesai ?> 
				<br>
				<i class="fa fa-tags"></i> <?php echo str_replace(",", ", ", $detail->ruang_ket) ?> 
			</p>
		</div>
		<div class='block-box'>
			<?php if($dokumen) : ?>
			<table class='table table-hover' id='example' style="font-size: 100%">
				<thead>
					<tr>
						<th width="10%">No.</th>
						<th width="40%">Dokumen</th>
						<th width="50%">File</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($dokumen as $key) : ?>
						<tr>
							<td><?php echo $no; $no++ ?></td>
							<td>
								<?php 
									echo $key->judul;
									echo "<br><code><i class='fa fa-check-square'></i> ".$key->keterangan."</code>";
								?>
							</td>
							<td>
								<?php
									$confirm = 'return confirm("Apakah Anda ingin menghapus file ini?")';
									foreach(explode(",", $key->file_name) as $dt){
										$file = explode("|", $dt);
										echo "<a target='_blank' href='".$this->location('assets/'.$file[1])."'>
											<i class='fa fa-cloud-download'></i></a> ";
										echo "<a onclick='".$confirm."' style='color: #e74c3c' href='".$this->location('module/master/kerjasama/del_file/'.$kegiatan.'/'.substr(md5($file[2]),7,6) )."'><i class='fa fa-trash-o'></i></a> ";
										echo $file[0];
										echo "<br>";
									}
								?>	
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php else : ?>
				<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel panel-default">
			  	<div class="panel-heading">
			  		<i class="fa fa-plus"></i> Tambah Dokumen
			  	</div>
				<div class="panel-body">	
					<form enctype="multipart/form-data" role="form" method="post" action="<?php echo $this->location('module/master/kerjasama/upload_dokumen') ?>">
						<div class="form-group">
						    <label >
						    	<i class="fa fa-briefcase"></i> <?php echo $detail->nama_instansi ?> 
						    	<br>
						    	<i class="fa fa-bullhorn"></i> <?php echo $detail->judul ?> 
						    </label>
						    <input name="kegiatan_id" type="hidden" value="<?php echo $detail->kegiatan_id ?>">
						    <input name="instansi_id" type="hidden" value="<?php echo $detail->instansi_id ?>">
						    <input name="fakultas" type="hidden" value="<?php echo $detail->kode_fakultas ?>">
						</div>
						<div class="form-group">
						    <label >Judul</label>
						    <input type="text" name="judul" required="" class="form-control" >
						</div>
						<div class="form-group">
						    <label >Keterangan</label>
						    <textarea name="keterangan" class="form-control" ></textarea>
						</div>
						<div class="form-group">
						    <input type="file" multiple="" name="dokumen[]" required="" class="form-control" >
						</div>
						
						<div class="form-group">
						    <button class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>
<?php $this->foot(); ?>