<?php $this->head(); ?>

<div class="row">
	<div class="col-md-12">	
	<h2 class="title-page">Kerjasama</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Kerjasama</a></li>
		  <li class="active"><a href="#">Dokumen</a></li>
		</ol>
		
		<div class="breadcrumb-more-action">
			<a href="#" class="btn btn-primary" onclick="tambah()">
    		<i onclick="tambah()" class="fa fa-pencil icon-white"></i> Tambah Dokumen</a> 
        </div>
	</div>
</div>

<div class="row">
	<div class="col-md-4">
		<div class='block-box'>
			<h3 class="text-center" style="margin-top: 5px">Fakultas</h3>
			
			<div class="list-group" id="fakultas-wrap">
		    </div>
		</div>
	</div>
	
	<div class="col-md-8" style="display: none"  id="data-wrap">
		<div class='block-box'>
			<h3 class="text-center" style="margin-top: 5px">Daftar Dokumen</h3>
		</div>
	</div>
	
	<div class="col-md-8" style="display: nonex" id="add-wrap">
		<div class="panel panel-default">
			  	<div class="panel-heading">
			  		<i class="fa fa-plus"></i> Tambah Dokumen
			  	</div>
				<div class="panel-body">	
					<form enctype="multipart/form-data" role="form" method="post" action="<?php echo $this->location('module/master/kerjasama/upload_dokumen') ?>">
						<div class="form-group">
						    <label >
						    	<i class="fa fa-briefcase"></i> <?php echo $detail->nama_instansi ?> 
						    	<br>
						    	<i class="fa fa-bullhorn"></i> <?php echo $detail->judul ?> 
						    </label>
						    <input name="kegiatan_id" type="hidden" value="<?php echo $detail->kegiatan_id ?>">
						    <input name="instansi_id" type="hidden" value="<?php echo $detail->instansi_id ?>">
						</div>
						<div class="form-group">
						    <label >Fakultas</label>
						    <select type="text" class="form-control" id="select-fakultas" >
						    	<option>Pilih Produk</option>
						    </select>
						</div>
						<div class="form-group">
						    <label >Instansi</label>
						    <select type="text" class="form-control" id="select-instansi" >
						    	<option>Pilih Instansi</option>
						    </select>
						</div>
						<div class="form-group">
						    <label >Judul</label>
						    <input type="text" name="judul" required="" class="form-control" >
						</div>
						<div class="form-group">
						    <label >Keterangan</label>
						    <textarea name="keterangan" class="form-control" ></textarea>
						</div>
						<div class="form-group">
						    <input type="file" multiple="" name="dokumen[]" required="" class="form-control" >
						</div>
						
						<div class="form-group">
						    <button class="btn btn-primary">Simpan</button>
						    <button onclick="batal()" class="btn btn-default">Batal</button>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>
<?php $this->foot(); ?>
<script>
	$(document).ready(function(){
		var temp = '';
		var instansi = '';
		
		set_fakultas();
		get_fakultas();
		get_data_instansi();
	});
	
	function tambah(){
		$("#data-wrap").hide();
		$("#add-wrap").show();
	}
	
	function batal(){
		$("#data-wrap").show();
		$("#add-wrap").hide();
	}
	
	function get_dokumen_data(id){
		var URL = base_url + 'module/master/kerjasama/get_all_data';
		$.ajax({
		    url : URL,
		    type: "POST",
		    dataType : "html",
		    data : $.param({
		    	kegiatan : id
		    }),
		    success:function(msg) 
		    {
		    	$("#all-data-wrap").html(msg);
		    }
	    });
	}
	
	function set_fakultas(){
		for	(index = 0; index < fakultas.length; index++) {
		    temp = fakultas[index];
		    data = temp.split("|");
		    
		   $("#select-fakultas").append('<option value="'+data[0]+'">'+data[1]+'</option>');
		}
	}
	
	function get_fakultas(){
		$("#fakultas-wrap").html("");
		for	(index = 0; index < fakultas.length; index++) {
		    temp = fakultas[index];
		    data = temp.split("|");
		    
		    $("#fakultas-wrap").append('<a dataid="'+data[0]+'" onclick="get_kegiatan(\''+data[0]+'\')" href="#" class="list-group-item">'+data[1]+'</a>');
		}
	}
	
	function hover(id){
		$("#fakultas-wrap a").attr("class","list-group-item");
		$("#fakultas-wrap a[dataid='"+id+"']").attr("class","list-group-item active");
	}
	
	function get_detail(id){
		hover(id);
		var URL = base_url + 'module/master/kerjasama/get_all_kegiatan';
		$.ajax({
		    url : URL,
		    type: "POST",
		    dataType : "html",
		    data : $.param({
		    	instansi : id
		    }),
		    success:function(msg) 
		    {
		    	get_dokumen_data(id+"|ins");
		    	$("#data-wrap").html(msg);
		    }
	    });
	}
	
	function get_kegiatan(id){
		hover(id);
		$("#fakultas-wrap").html('<a onclick="get_fakultas()" href="#" class="list-group-item"><i class="fa fa-arrow-circle-o-up"></i> Daftar Fakultas ...</a>');
		if(instansi[id]){
			for(var i=0; i<instansi[id].length; i++){
				// console.log(instansi[jadwal][i]["instansi"]);
				$("#fakultas-wrap").append('<a dataid="'+instansi[id][i]["instansi_id"]+'" onclick="get_detail(\''+instansi[id][i]["instansi_id"]+'\')" href="#" class="list-group-item">'+instansi[id][i]["instansi"]+'</a>');
			}
		}
		else{
			console.log("empty");
		}
	}
	
	function get_data_instansi(){
		var URL = base_url + 'module/master/kerjasama/get_fakultas_instansi';
		$.ajax({
		    url : URL,
		    type: "POST",
		    dataType : "json",
		    async: false,
		    success:function(msg) 
		    {
		    	instansi = msg;
		    },
		    cache: false,
		    contentType: false,
		    processData: false
	    });
	    
	}
	
	<?php if($fakultas): ?>
		var fakultas = [<?php foreach ($fakultas as $key) echo "'".$key->fakultas_id . "|". $key->keterangan."',"; ?>];
	<?php else: ?>
		var fakultas = [];
	<?php endif; ?>	
</script>














