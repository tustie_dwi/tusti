<?php $this->head(); ?>

<div class="row">

	<div class="col-md-12">	
	<h2 class="title-page">Kerjasama</h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="#>">Master</a></li>
		  <li><a href="#>">Kerjasama</a></li>
		  <li class="active"><a href="#">Kategori</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-md-8">
		<div class='block-box'>
			<?php if($kategori) : ?>
			<table class='table table-hover' id='example' style="font-size: 100%">
				<thead>
					<tr>
						<th width="10%">No.</th>
						<th>Kategori</th>
						<th width="20%">Menu</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($kategori as $key) : ?>
						<tr>
							<td><?php echo $no; $no++ ?></td>
							<td><?php echo $key->keterangan ?></td>
							<td>
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
										<li>
											<a class='btn-edit-post' onclick="edit_kategori(<?php echo "'". $key->keterangan . "','" . $key->id . "'" ?>)">
												<i class='fa fa-edit'></i> Edit
											</a>
											<a class='btn-edit-post' onclick="return confirm('Apakah Anda ingin menghapus data ini?')" href="<?php echo $this->location('module/master/kerjasama/del_kategori/'.$key->id) ?>">
												<i class='fa fa-trash-o'></i> Hapus
											</a>	
										</li>
									  </ul>
									</li>
								</ul>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php else : ?>
				<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel panel-default">
			  	<div class="panel-heading">
			  		<i class="fa fa-plus"></i> Tambah Kategori
			  	</div>
				<div class="panel-body">	
					<form role="form" method="post" action="<?php echo $this->location('module/master/kerjasama/save_kategori') ?>">
						<div class="form-group">
						    <label >Kategori</label>
						    <input name="id_kategori" type="hidden">
						    <input autocomplete="off" name="keterangan" required="" type="text" class="form-control" placeholder="Kategori Kerjasama">
						</div>
						<div class="form-group">
						    <button class="btn btn-primary">Simpan</button>
						    <a onclick="batal_kategori()" class="btn btn-default">Batal</a>
						</div>
					</form>
				</div>
			</div>
	</div>
</div>
<?php $this->foot(); ?>