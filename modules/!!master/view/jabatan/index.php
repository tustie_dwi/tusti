<?php $this->head();

if(isset($edit)){
	foreach ($edit as $e) {
		$hidId = $e->jabatanid;
		$ket = $e->keterangan;
		$parentid = $e->parent_id;
	}
}else{
	$hidId = '';
	$ket = '';
	$parentid = '';
}

?>

<style>
	.btn-edit{
		border : none;
		padding: 3px 5px;
		padding-left: 10px;
	}
</style>

<div class="row">
		<div class="col-md-7">	
			<ol class="breadcrumb">
			  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
			  <li><a href="<?php echo $this->location('module/master/general/jabatan'); ?>">Jabatan</a></li>
			</ol>
		
		<?php if(isset($posts)){ ?>
		<div class="tree">
			<ul>
				<?php foreach($posts as $row): ?>
				<li>
					<span><i class='fa fa-minus-square'></i> <?php echo $row->keterangan ?></span>
					
					<div class="btn-group">
					    <button type="button" class="btn btn-default dropdown-toggle btn-edit" data-toggle="dropdown">
					      Action
					      <span class="fa fa-caret-down"></span>
					    </button>
					    <ul class="dropdown-menu">
					      <li><a class="" href="<?php echo $this->location('module/master/general/jabatan/edit/'.$row->jabatan_id) ?>"><i class='fa fa-edit'></i> Edit</a></li>
					    </ul>
					</div>	
					
					<?php $this->child($row->jabatan_id); ?>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
		
		
		<?php }else{ ?>
		<div class="well" align="center">No Data Show.</div>
		<?php } ?>
		</div>
		
	<div class="col-sm-5">
	<div class="panel panel-default">
	 <div class="panel-heading"><i class="fa fa-pencil-square"></i> <?php echo $panel ?></div>
	  <div class="panel-body">
		
		<form id="form">
			
			<div class="form-group">
		      <label>Keterangan</label>
		      <input type="text" value="<?php echo $ket ?>" class="form-control" id="keterangan" name="keterangan" required="required">
		    </div>
			
			<?php if(isset($posts)){ ?>
			<div class="form-group">
		      <label>Parent</label>
		      <select name="parent" class="form-control">
		      	<option value="0">Pilih Parent</option>
		      	<?php
					foreach($cmbparent as $p):
						echo "<option value='".$p->jabatan_id."' ";
						if(isset($parentid)&&$parentid==$p->jabatan_id){
							echo "selected";
						}
						echo " >".$p->keterangan."</option>";
					endforeach;
				?>
		      </select>
		    </div>
			<?php } ?>
			
			<div class="form-group">
			  <input type="hidden" name="hidId" value="<?php echo $hidId ?>" />
		      <input type="submit" class="btn btn-primary" name="b_save" value="Submit">
		      <a href="<?php echo $this->location("module/master/general/jabatan"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>	
		    </div>
					
		</form>
		
	  </div>
	 </div>
	</div>
		
	</div>
<?php $this->foot(); ?>