<?php
class model_penelitian extends model {

	public function __construct() {
		parent::__construct();	
	}
	//=============PENELITIAN=============//
	function read($id=NULL,$fakultas=NULL,$thn=NULL,$kategori=NULL,$singlerow=NULL,$status=NULL,$peserta=NULL,$publish=NULL,$progress=NULL){
		$sql = "SELECT tbl_penelitian.*,
					   tbl_penelitian_progress.*,
					   tbl_penelitian_peserta.*,
					   mid(md5(tbl_penelitian.penelitian_id),6,6) as penelitianid,
					   tbl_master_status.keterangan as status,
					   CONCAT(tbl_tahunakademik.tahun,' ',tbl_tahunakademik.is_ganjil,' ',tbl_tahunakademik.is_pendek) as thn_akademik,
					   tbl_penelitian_progress.keterangan as keterangan_progress,
					   tbl_penelitian_progress.sumber_dana as sumber_dana_progress,
					   tbl_penelitian.keterangan as keterangan_penelitian,
					   tbl_penelitian_publish.tgl_pelaksanaan as tgl_publish,
					   tbl_penelitian_publish.lokasi as lokasi_publish,
					   tbl_penelitian_publish.kategori as kategori_publish,
					   tbl_penelitian_publish.publish_id,
					   tbl_penelitian_publish.jurnal_jenis,
					   tbl_penelitian_publish.jurnal_edisi,
					   tbl_penelitian_publish.jurnal_nama,
					   tbl_penelitian_publish.jurnal_link,
					   tbl_penelitian_publish.buku_penerbit,
					   tbl_penelitian_publish.buku_isbn,
					   tbl_penelitian_publish.buku_judul,
					   tbl_penelitian_publish.buku_file,
					   tbl_fakultas.keterangan as fakultas,
					   tbl_cabang.keterangan as cabang
				FROM `db_ptiik_apps`.`tbl_penelitian`
				LEFT JOIN `db_ptiik_apps`.`tbl_penelitian_peserta` ON `tbl_penelitian_peserta`.`penelitian_id` = `tbl_penelitian`.`penelitian_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_penelitian_progress` ON `tbl_penelitian_progress`.`penelitian_id` = `tbl_penelitian`.`penelitian_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_master_status` ON `tbl_master_status`.`status_id` = `tbl_penelitian_progress`.`status_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` ON `tbl_tahunakademik`.`tahun_akademik` = `tbl_penelitian`.`tahun_akademik`
				LEFT JOIN `db_ptiik_apps`.`tbl_penelitian_publish` ON `tbl_penelitian_publish`.`penelitian_id` = `tbl_penelitian`.`penelitian_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas` ON `tbl_fakultas`.`fakultas_id` = `tbl_penelitian`.`fakultas_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_cabang` ON `tbl_cabang`.`cabang_id` = `tbl_penelitian`.`cabang_id`
				WHERE 1";
		if($id){
			$sql .= " AND mid(md5(tbl_penelitian.penelitian_id),6,6) = '".$id."'";
			if($progress){
				$sql .= " GROUP BY tbl_penelitian_progress.status_id";
			}
			if($peserta){
				$sql .= " GROUP BY tbl_penelitian_peserta.peserta_id
						  ORDER BY tbl_penelitian_peserta.urut
						";
			}
			if($publish){
				$sql .= " AND tbl_penelitian_publish.kategori = '".$publish."'";
			}
			if($status){
				$sql .= " AND tbl_penelitian_progress.status_id = '".$status."'";
				// echo $sql;
				return $this->db->getRow($sql);
			}
		}
		if($kategori){
			$sql .= " AND tbl_penelitian.kategori = '".$kategori."'";
		}
		if($fakultas!="-"&&$fakultas!=""){
			$sql .= " AND tbl_penelitian.fakultas_id = '".$fakultas."'";
		}
		if($thn){
			$sql .= " AND tbl_penelitian.tahun_akademik = '".$thn."'";
		}
		if($singlerow){
			//$sql .= " GROUP BY tbl_penelitian.penelitian_id";
			$sql .= " ORDER BY `tbl_penelitian_progress`.`status_id` DESC LIMIT 0,1";
			// echo $sql;
			if($id){
			return $this->db->getRow($sql);
			}
		}
		// echo $sql;
		$result = $this->db->query($sql);
		return $result;
	}
	
	function read_selection($id=NULL,$fakultas=NULL,$thn=NULL,$kategori=NULL,$singlerow=NULL,$status=NULL,$peserta=NULL,$publish=NULL,$progress=NULL){
		$sql = "SELECT tbl_penelitian.*,
					   mid(md5(tbl_penelitian.penelitian_id),6,6) AS penelitianid,
					CONCAT(tbl_tahunakademik.tahun,' ',tbl_tahunakademik.is_ganjil,' ',tbl_tahunakademik.is_pendek) AS thn_akademik,
					tbl_fakultas.keterangan AS fakultas,
					tbl_cabang.keterangan AS cabang,
					tbl_master_status.keterangan as status
					FROM
					`db_ptiik_apps`.tbl_penelitian				
					LEFT JOIN `db_ptiik_apps`.tbl_tahunakademik ON tbl_tahunakademik.tahun_akademik = tbl_penelitian.tahun_akademik
					LEFT JOIN `db_ptiik_apps`.tbl_fakultas ON tbl_fakultas.fakultas_id = tbl_penelitian.fakultas_id
					LEFT JOIN `db_ptiik_apps`.tbl_cabang ON tbl_cabang.cabang_id = tbl_penelitian.cabang_id
					LEFT JOIN `db_ptiik_apps`.tbl_master_status ON tbl_penelitian.st_publikasi = tbl_master_status.status_id
				WHERE 1";
		if($id){
			$sql .= " AND mid(md5(tbl_penelitian.penelitian_id),6,6) = '".$id."'";
			if($progress){
				$sql .= " GROUP BY tbl_penelitian_progress.status_id";
			}
			if($peserta){
				$sql .= " GROUP BY tbl_penelitian_peserta.peserta_id
						  ORDER BY tbl_penelitian_peserta.urut
						";
			}
			if($publish){
				$sql .= " AND tbl_penelitian_publish.kategori = '".$publish."'";
			}
			if($status){
				$sql .= " AND tbl_penelitian_progress.status_id = '".$status."'";
				// echo $sql;
				return $this->db->getRow($sql);
			}
		}
		if($kategori){
			$sql .= " AND tbl_penelitian.kategori = '".$kategori."'";
		}
		if($fakultas!="-"&&$fakultas!=""){
			$sql .= " AND tbl_penelitian.fakultas_id = '".$fakultas."'";
		}
		if($thn){
			$sql .= " AND tbl_penelitian.tahun_akademik = '".$thn."'";
		}
		if($singlerow){
			//$sql .= " GROUP BY tbl_penelitian.penelitian_id";
			$sql .= " ORDER BY `tbl_penelitian_progress`.`status_id` DESC LIMIT 0,1";
			// echo $sql;
			if($id){
			return $this->db->getRow($sql);
			}
		}
		if($id) $result = $this->db->getRow($sql);
		else $result = $this->db->query($sql);
		return $result;
	}
	
	function penelitian_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(penelitian_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_penelitian`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_penelitian($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_penelitian', $data);
	}
	
	function delete_penelitian($id=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_penelitian`
				WHERE mid(md5(tbl_penelitian.penelitian_id),6,6) = '".$id."'";
		$result = $this->db->query($sql);
		return $result;
	}
	
	//=============PROGRESS=============//
	function progress_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(progress_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_penelitian_progress`";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_penelitian_progress($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_penelitian_progress', $data);
	}
	
	function delete_progress($id=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_penelitian_progress`
				WHERE mid(md5(tbl_penelitian_progress.penelitian_id),6,6) = '".$id."'";
		$result = $this->db->query($sql);
		return $result;
	}
	
	//=============PUBLIKASI===========//
	function publikasi_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(publish_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_penelitian_publish`";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_penelitian_publish($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_penelitian_publish', $data);
	}
	
	function delete_publish($id=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_penelitian_publish`
				WHERE mid(md5(tbl_penelitian_publish.penelitian_id),6,6) = '".$id."'";
		$result = $this->db->query($sql);
		return $result;
	}
	
	//=============PESERTA=============//
	function peserta_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(peserta_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_penelitian_peserta`";
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_penelitian_peserta($data){
		return $this->db->replace('db_ptiik_apps`.`tbl_penelitian_peserta', $data);
	}
	
	function hapus_peserta($id=NULL,$penelitian=NULL){
		$sql = "DELETE
				FROM `db_ptiik_apps`.`tbl_penelitian_peserta`
				WHERE 1";
		if($id){
			$sql .= " AND tbl_penelitian_peserta.peserta_id = '".$id."'";
		}
		if($penelitian){
			$sql .= " AND mid(md5(tbl_penelitian_peserta.penelitian_id),6,6) = '".$penelitian."'";
		}
		$result = $this->db->query($sql);
		return $result;
	}
	
	//=============SETTING STATUS=============//
	function read_status($id=NULL,$kategori=NULL){
		$sql = "SELECT *,
					   mid(md5(tbl_master_status.status_id),6,6) as statusid
				FROM `db_ptiik_apps`.`tbl_master_status`
				WHERE 1";
		if($id){
			$sql .= " AND mid(md5(tbl_master_status.status_id),6,6) = '".$id."'";
			return $this->db->getRow( $sql );
		}
		if($kategori){
			$sql .= " AND tbl_master_status.kategori = '".$kategori."'";
		}
		$result = $this->db->query($sql);
		return $result;
	}
	
	function status_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(status_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_master_status`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_status($data=NULL){
		return $this->db->replace('db_ptiik_apps`.`tbl_master_status', $data);
	}
	
}
?>