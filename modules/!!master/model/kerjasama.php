<?php
class model_kerjasama extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_kategori(){
		$sql = "SELECT MID(MD5(tbl_master_kerjasama.kategori_id),8,6) id, tbl_master_kerjasama.keterangan, tbl_master_kerjasama.kategori_id
				FROM db_ptiik_apps.tbl_master_kerjasama";
				
		return $this->db->query($sql);
	}
	
	function get_fakultas(){
		$sql = "SELECT tbl_fakultas.fakultas_id, tbl_fakultas.keterangan
				FROM db_ptiik_apps.tbl_fakultas";
		return $this->db->query($sql);
	}
	
	function get_kategori_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kategori_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_master_kerjasama WHERE left(kategori_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_kategori($data){
		$this->db->replace('db_ptiik_apps`.`tbl_master_kerjasama',$data);
	}
	
	function edit_kategori($data, $where){
		$this->db->update('db_ptiik_apps`.`tbl_master_kerjasama', $data, $where);
	}
	
	function del_kategori($id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_master_kerjasama WHERE MID(MD5(tbl_master_kerjasama.kategori_id),8,6) = '$id'";
		$this->db->query($sql);
	}
	
	//instansi
	function get_instansi($fakultas, $kategori){
		$sql = "SELECT 
					MID(MD5(tbl_kerjasama_instansi.instansi_id),8,6) id,
					tbl_master_kerjasama.keterangan kategori,
					tbl_fakultas.keterangan fakultas,
					tbl_kerjasama_instansi.nama_instansi,
					tbl_kerjasama_instansi.fakultas_id,
					tbl_kerjasama_instansi.kategori_id,
					tbl_kerjasama_instansi.contact_person,
					tbl_kerjasama_instansi.telp,
					tbl_kerjasama_instansi.alamat,
					tbl_kerjasama_instansi.keterangan,
					tbl_kerjasama_instansi.logo
				FROM db_ptiik_apps.tbl_kerjasama_instansi
				LEFT JOIN db_ptiik_apps.tbl_fakultas ON tbl_fakultas.fakultas_id = tbl_kerjasama_instansi.fakultas_id
				LEFT JOIN db_ptiik_apps.tbl_master_kerjasama ON tbl_master_kerjasama.kategori_id = tbl_kerjasama_instansi.kategori_id
				WHERE 1=1
				";
		
		if($fakultas != '') $sql .= " AND tbl_kerjasama_instansi.fakultas_id = '$fakultas'";
		if($kategori != '') $sql .= " AND tbl_kerjasama_instansi.kategori_id = '$kategori'";
		
		$sql .= " ORDER BY tbl_fakultas.keterangan, tbl_kerjasama_instansi.nama_instansi";
		return $this->db->query($sql);
	}
	function get_instansi_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(instansi_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_kerjasama_instansi WHERE left(instansi_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_instansi($data){
		$this->db->replace('db_ptiik_apps`.`tbl_kerjasama_instansi',$data);
	}
	
	function edit_instansi($data, $where){
		$this->db->update('db_ptiik_apps`.`tbl_kerjasama_instansi', $data, $where);
	}
	
	function del_instansi($id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_kerjasama_instansi WHERE MID(MD5(tbl_kerjasama_instansi.instansi_id),8,6) = '$id'";
		$this->db->query($sql);
	}
	
	//kegiatan
	function get_ruangan($fak=NULL, $cabang=NULL){
		$sql = "SELECT tbl_ruang.ruang_id, tbl_ruang.kode_ruang, tbl_ruang.keterangan
				FROM db_ptiik_apps.tbl_ruang
				WHERE tbl_ruang.cabang_id = '$cabang' AND tbl_ruang.fakultas_id = '$fak'";
				
		return $this->db->query($sql);
	}

	function get_kegiatan_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kegiatan_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_kerjasama_kegiatan WHERE left(kegiatan_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_kegiatan($data){
		$this->db->replace('db_ptiik_apps`.`tbl_kerjasama_kegiatan',$data);
	}
	
	function edit_kegiatan($data, $where){
		$this->db->update('db_ptiik_apps`.`tbl_kerjasama_kegiatan',$data, $where);
	}
	
	function get_instansi_list(){
		$sql = "SELECT 
					tbl_kerjasama_instansi.nama_instansi, 
					tbl_kerjasama_instansi.instansi_id
				FROM db_ptiik_apps.tbl_kerjasama_instansi";
		return $this->db->query($sql);
	}
	
	function get_instansi_by_fakultas($fak=NULL){
		$sql = "SELECT 
					tbl_kerjasama_instansi.nama_instansi, 
					tbl_kerjasama_instansi.instansi_id,
					tbl_kerjasama_instansi.kategori_id
				FROM db_ptiik_apps.tbl_kerjasama_instansi
				WHERE tbl_kerjasama_instansi.fakultas_id = '$fak'
				ORDER BY tbl_kerjasama_instansi.nama_instansi";
		return $this->db->query($sql);
	}
	
	function get_kegiatan($ins=NULL){
		$sql = "SELECT 
					MID(MD5(tbl_kerjasama_kegiatan.kegiatan_id),8,6) id,
					tbl_kerjasama_kegiatan.kegiatan_id,
					tbl_kerjasama_instansi.instansi_id,
					tbl_kerjasama_kegiatan.judul,
					tbl_kerjasama_kegiatan.keterangan,
					tbl_kerjasama_kegiatan.lokasi,
					tbl_kerjasama_kegiatan.ruang,
					DATE(tbl_kerjasama_kegiatan.tgl_mulai) tgl_mulai,
					DATE(tbl_kerjasama_kegiatan.tgl_selesai) tgl_selesai,
					tbl_kerjasama_instansi.nama_instansi,
					tbl_kerjasama_instansi.instansi_id,
					GROUP_CONCAT(tbl_ruang.kode_ruang, ' - ', tbl_ruang.keterangan) ruang_ket,
					GROUP_CONCAT(tbl_ruang.kode_ruang) ruang
				FROM db_ptiik_apps.tbl_kerjasama_kegiatan
				INNER JOIN db_ptiik_apps.tbl_kerjasama_instansi ON tbl_kerjasama_instansi.instansi_id = tbl_kerjasama_kegiatan.instansi_id
				LEFT JOIN db_ptiik_apps.tbl_ruang ON tbl_kerjasama_kegiatan.ruang LIKE CONCAT('%',tbl_ruang.ruang_id,'%')
				WHERE tbl_kerjasama_kegiatan.instansi_id = '$ins'
				GROUP BY tbl_kerjasama_kegiatan.kegiatan_id
				ORDER BY tbl_kerjasama_kegiatan.tgl_mulai DESC";
		
		return $this->db->query($sql);
	}
	
	function del_kegiatan($id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_kerjasama_kegiatan WHERE MID(MD5(tbl_kerjasama_kegiatan.kegiatan_id),8,6) = '$id'";
		$this->db->query($sql);
	}
	
	
	//dokumen----
	function get_detail_kegiatan($kegiatan){
		$sql = "SELECT 
					MID(MD5(tbl_kerjasama_kegiatan.kegiatan_id),8,6) id,
					tbl_kerjasama_kegiatan.kegiatan_id,
					tbl_kerjasama_instansi.instansi_id,
					tbl_kerjasama_kegiatan.judul,
					tbl_kerjasama_kegiatan.keterangan,
					tbl_kerjasama_kegiatan.lokasi,
					DATE(tbl_kerjasama_kegiatan.tgl_mulai) tgl_mulai,
					DATE(tbl_kerjasama_kegiatan.tgl_selesai) tgl_selesai,
					tbl_kerjasama_instansi.nama_instansi,
					tbl_kerjasama_instansi.instansi_id,
					GROUP_CONCAT(tbl_ruang.kode_ruang, ' - ', tbl_ruang.keterangan) ruang_ket,
					tbl_fakultas.kode_fakultas
				FROM db_ptiik_apps.tbl_kerjasama_kegiatan
				INNER JOIN db_ptiik_apps.tbl_kerjasama_instansi ON tbl_kerjasama_instansi.instansi_id = tbl_kerjasama_kegiatan.instansi_id
				INNER JOIN db_ptiik_apps.tbl_fakultas ON tbl_fakultas.fakultas_id = tbl_kerjasama_instansi.fakultas_id
				LEFT JOIN db_ptiik_apps.tbl_ruang ON tbl_kerjasama_kegiatan.ruang LIKE CONCAT('%',tbl_ruang.ruang_id,'%')
				WHERE MID(MD5(tbl_kerjasama_kegiatan.kegiatan_id),8,6) = '$kegiatan'
				GROUP BY tbl_kerjasama_kegiatan.kegiatan_id
				ORDER BY tbl_kerjasama_kegiatan.tgl_mulai DESC";
				
		return $this->db->getRow($sql);
	}
	
	function cek_file_type($file_ext){
		$sql = "SELECT jenis_file_id, kode_jenis, max_size, keterangan
				FROM db_ptiik_apps.tbl_jenisfile
				WHERE extention LIKE '%".$file_ext."%'";
				
		return $this->db->getRow($sql);
	}
	
	function get_dokumen_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(dokumen_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_kerjasama_dokumen WHERE left(dokumen_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_dokumen($data){
		$this->db->replace('db_ptiik_apps`.`tbl_kerjasama_dokumen',$data);
	}
	
	function get_dokumen($kategori, $is_global=0){
		if($is_global == 0) :
			$sql = "SELECT 
						tbl_kerjasama_dokumen.judul,
						tbl_kerjasama_dokumen.keterangan,
						GROUP_CONCAT(tbl_kerjasama_dokumen.file_name, '|', tbl_kerjasama_dokumen.file_loc, '|', tbl_kerjasama_dokumen.dokumen_id) file_name
					FROM db_ptiik_apps.tbl_kerjasama_dokumen
					WHERE MID(MD5(tbl_kerjasama_dokumen.kegiatan_id),8,6) = '$kategori' 
					GROUP BY tbl_kerjasama_dokumen.judul
					ORDER BY tbl_kerjasama_dokumen.last_update DESC";	
		else :
			$sql = "SELECT 
						tbl_kerjasama_dokumen.judul,
						tbl_kerjasama_dokumen.keterangan,
						GROUP_CONCAT(tbl_kerjasama_dokumen.file_name, '|', tbl_kerjasama_dokumen.file_loc, '|', tbl_kerjasama_dokumen.dokumen_id) file_name
					FROM db_ptiik_apps.tbl_kerjasama_dokumen
					WHERE tbl_kerjasama_dokumen.instansi_id = '$kategori' AND tbl_kerjasama_dokumen.kegiatan_id = ''
					GROUP BY tbl_kerjasama_dokumen.judul
					ORDER BY tbl_kerjasama_dokumen.last_update DESC";
		endif;
				
		return $this->db->query($sql);
	}
	
	function del_file($id){
		$sql = "DELETE FROM db_ptiik_apps.tbl_kerjasama_dokumen WHERE MID(MD5(tbl_kerjasama_dokumen.dokumen_id),8,6) = '$id'";
		$this->db->query($sql);
	}
	
	
	//dokumen all
	function get_fakultas_instansi(){
		$sql = "SELECT 
					tbl_fakultas.fakultas_id,
					tbl_kerjasama_instansi.nama_instansi,
					tbl_kerjasama_instansi.instansi_id
				FROM db_ptiik_apps.tbl_fakultas
				INNER JOIN db_ptiik_apps.tbl_kerjasama_instansi ON tbl_kerjasama_instansi.fakultas_id = tbl_fakultas.fakultas_id
				ORDER BY tbl_kerjasama_instansi.nama_instansi";
		return $this->db->query($sql);
	}
	
	function get_all_kegiatan($id){
		$sql = "SELECT 
					tbl_kerjasama_kegiatan.judul,
					MID(MD5(tbl_kerjasama_kegiatan.kegiatan_id),8,6) kegiatan_id,
					DATE(tbl_kerjasama_kegiatan.tgl_mulai) tgl
				FROM db_ptiik_apps.tbl_kerjasama_kegiatan
				WHERE tbl_kerjasama_kegiatan.instansi_id = '$id'
				ORDER BY tbl_kerjasama_kegiatan.tgl_mulai DESC";
		return $this->db->query($sql);
	}
	
	function get_fakultas_detail($id){
		$sql = "SELECT tbl_kerjasama_instansi.nama_instansi keterangan, tbl_kerjasama_instansi.alamat, tbl_kerjasama_instansi.telp, tbl_kerjasama_instansi.contact_person
				FROM db_ptiik_apps.tbl_kerjasama_instansi
				WHERE tbl_kerjasama_instansi.instansi_id = '$id'";
								
		return $this->db->getRow($sql);
	}
	
	
	
	
	
	
	
	
	
}
?>