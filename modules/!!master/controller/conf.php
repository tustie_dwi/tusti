<?php
class master_conf extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}	
	
	function namamk(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf	= new model_conf();
		$mmk	= new model_mk();
		$fakid = $mmk->get_fakultas_id($_POST['fakultasid']);
		
		$result = $mconf->get_namamk($str, $fakid);
		
		$return_arr = array();
		
		if($result){
			foreach($result as $row){
				foreach ($row as $key => $value) {
					$arr[$key] = $value;				 
				}
				array_push($return_arr,$arr);
			}
			
			$json_response = json_encode($return_arr);
			if(isset($_GET["callback"])) {
				$json_response = $_GET["callback"] . "(" . $json_response . ")";
			}
			echo $json_response;
		}else{
			echo json_encode('failed');
		}
		
	}
	
	function namamk_from_matakuliah(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		$fakid = $mconf->get_fakultas_id($_POST['fakultasid']);
		$result = $mconf->get_namamk_from_matakuliah($str, $fakid);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		echo $json_response;
		
	}
	
	function namamkfrommkditawarkan(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if($_POST['fakultasid']){
			$fakid = $_POST['fakultasid'];
		}else $fakid = '';
		
		if($_POST['cabangid']){
			$cabangid = $_POST['cabangid'];
		}else $cabangid = '';
		
		if($_POST['thnakademikid']){
			$thnakademikid = $_POST['thnakademikid'];
		}else $thnakademikid = '';
		
		$mconf= new model_conf();
		
		$result = $mconf->get_namamkfrommkditawarkan($str, $fakid, $cabangid, $thnakademikid);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key]= $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}

		echo $json_response;
		
	}
	
	function namamk_from_mkditawarkan_by_pengampu(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		$mconf= new model_conf();
		
		$result = $mconf->get_all_namamk_from_mkditawarkan($str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key]= $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		//$this->view( 'mk/tes.php', $data );
	}
	
	function dosen(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if(isset($_POST['fakultas_id'])){
			$fakultas_id = $_POST['fakultas_id'];
		}else $fakultas_id = "";
		
		$mconf= new model_conf();
		
		$result = $mconf->get_nama_dosen($str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		//$this->view( 'mk/tes.php', $data );

	}
	
	
	function staff(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		$mconf= new model_conf();
		
		$result = $mconf->get_nama_staff($str);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		//$this->view( 'mk/tes.php', $data );

	}
	
	function ruang(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if(isset($_POST['cabang_id'])){
			$cabangid = $_POST['cabang_id'];
		}else $cabangid = "";
		
		$mconf= new model_conf();
		
		$result = $mconf->get_ruang($str, $cabangid);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;				 
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$this->view( 'mk/tes.php', $data );
	}
	
	function mhs(){
		if(isset($_GET['term'])){
			$str = $_GET['term'];
		}else{
			$str = "";
		}
		
		if(isset($_POST['fakultas_id'])){
			$fakultas_id = $_POST['fakultas_id'];
		}else $fakultas_id = "";
		
		$mconf= new model_conf();
		
		$result = $mconf->get_allmhs($str,$fakultas_id);
		
		$return_arr = array();

		foreach($result as $row){
			foreach ($row as $key => $value) {
				$arr[$key] = $value;
			}
			array_push($return_arr,$arr);
		}
		
		$json_response = json_encode($return_arr);
		
		//$data['tes'] = $json_response;
		# Return the response
		echo $json_response;
		if(isset($_GET["callback"])) {
			$json_response = $_GET["callback"] . "(" . $json_response . ")";
		}
		//$this->view( 'mk/tes.php', $data );
	}
	
	
}
?>