<?php
class master_krs extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mkrs = new model_krs();			
		
		$data['tahun']		= $mkrs->get_tahun();
		$data['fakultas']	= $mkrs->get_fakultas();
		
		$this->add_script('js/krs.js');
		$this->view('krs/index.php', $data);
	}
	
	function get_prodi(){
		$mkrs = new model_krs();	
		
		$prodi	= $mkrs->get_prodi($_POST['fakultas']);
		
		if($prodi) :
			echo "<option value=''>Pilih Prodi</option>";
			foreach($prodi as $key){
				echo "<option value='".$key->prodi_id."'>" . $key->keterangan."</option>";
			}
		else :
			echo "<option value=''>Prodi tidak ditemukan</option>";
		endif;
	}
	
	function get_mhs(){
		if(1 ||! empty($_POST['angkatan']) && !empty($_POST['tahun']) && !empty($_POST['fakultas']) && !empty($_POST['prodi'])){
			$mkrs = new model_krs();	
			$mhs_data = $mkrs->get_mhs($_POST['angkatan'], $_POST['prodi']);
			
			$mhs = array();
			
			if($mhs_data){
				foreach($mhs_data as $key){
					$temp = array(
						"nama"=>$key->nama, 
						"nim"=>$key->nim, 
						"angkatan"=>$key->angkatan,
						"prodi"=> $key->prodi_id,
						"gender"=>$key->jenis_kelamin,
						"mhs_id"=>$key->mahasiswa_id
					);
					array_push($mhs, $temp);
				}
			}
			else{
				$mhs = array("tipe"=>"kosong");
			}
			
			echo json_encode($mhs);
		}
		else {
			$data = array("tipe"=>"false");
			echo json_encode($data);
		}
		
	}
	
	function get_krs(){
		$mkrs = new model_krs();	
		$data['krs'] = $mkrs->get_krs($_POST['mhsid'], $_POST['tahun']);
		$data['mhs'] = $mkrs->get_detail_mhs($_POST['mhsid']);
		$this->view('krs/krs.php', $data);
	}

	
}
?>
	