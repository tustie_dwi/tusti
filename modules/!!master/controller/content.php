<?php
class master_content extends comsmodule {
		
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function write($str){
		switch ($str) {
			case 'page':
				$this->write_page();
			break;
		}
	}
	
	function edit($str, $id){
		switch ($str) {
			case 'page':
				$this->edit_page($id);
			break;
			case 'file':
				$this->edit_file($id);
			break;
		}
	}
	
	function detail($str, $id){
		switch ($str) {
			case 'page':
				$this->detail_page($id);
			break;
			case 'file':
				$this->detail_file($id);
			break;
		}
	}
	
	/*
	 * SETTING===================================================
	 * */
	
	function setting(){
		$mcontent = new model_content();
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		$data['header'] = 'Setting Content Category';
		$data['posts'] 	= $mcontent->read_content_category('');
		$data['user'] 	= $user;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/content/content.js');
		
		$this->view('content/setting/index.php', $data );
	}
	
	function save_Setting(){
		$mcontent = new model_content();
		
		if($_POST['hidcategory']!=""&&$_POST['hidcontent']!=""){
			$action 	= "update";
			
			$content_category = $_POST['content'];		
			$note 	  		  = $_POST['note'];
			$category 		  = $_POST['category'];
			
			$content_cparam   = $_POST['hidcontent'];
			$category_param	  = $_POST['hidcategory'];
			
			if($mcontent->update_content_category($content_category,$note,$category,$content_cparam,$category_param)){
				echo "sukses";
			}else echo "gagal";
			
		}else{
			$action 	= "insert";
			
			$content_category = $_POST['content'];
			$note 	 		  = $_POST['note'];
			$category 		  = $_POST['category'];
			
			$datanya 	= Array(
								'content_category'=>$content_category,
								'note'=>$note, 
								'category'=>$category
								);
			
			if($mcontent->replace_content_category($datanya)){
				echo "sukses";
			}else echo "gagal";
			
		}
	}
	
	/*
	 * END SETTING========================== START COMS_CONTENT
	 * */
	
	function page(){
		$mcontent 	= new model_content();
		$mconf	    = new model_conf();	
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		if(isset($_POST['fakultas'])){
			$fakultas = $_POST['fakultas'];
			$data['fakultasid']=$fakultas;
		}else {
			$fakultas=NULL;
			$data['fakultasid']= $fakultasid;
		}
		
		if(isset($_POST['cabang'])){
			$cabang = $_POST['cabang'];
			$data['cabangid']=$cabang;
		}else $cabang=NULL;
		
		if(isset($_POST['unit'])){
			$unit = $_POST['unit'];
			$data['unitid']=$unit;
		}else $unit=NULL;
		
		if(isset($_POST['lang'])){
			$lang = $_POST['lang'];
			$data['lang']=$lang;
		}else $lang=NULL;
		
		if(isset($_POST['category'])&&$_POST['category']!='0'){
			$category = $_POST['category'];
			$data['contentcategory']=$category;
		}else $category=NULL;
		
		if(isset($_POST['index-category'])){
			$index_category = $_POST['index-category'];
			$data['index_category']=$index_category;
		}else $index_category=NULL;
		
		$data['content_view'] = 'content';
		
		if($index_category=='upload'){
			$data['posts'] 	= $mcontent->read_file('',$fakultas,$cabang,$unit,$category);
			$data['content_view'] = 'upload';
		}else{
			if($category!=NULL){
				$data['posts'] 	= $mcontent->read_content('',$fakultas,$cabang,$unit,$category,'parent',$lang);
				$data['content_view'] = 'content';
			}
		}
		
		
		$data['header'] = 'Content';
		$data['user'] 	= $user;
		
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			$data['unit']			= $mconf->get_unit($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
			$data['unit']			= $mconf->get_unit('','getfakultas');
		}
		$data['cabang']				= $mconf->get_cabangub();
		$data['content_category'] 	= $mcontent->read_content_category('read-by-category');
		// $data['fakultasid']			= $fakultasid;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/content/content.js');
		
		$this->view('content/page/index.php', $data );
	}
	
	function detail_page($id){
		$mcontent 	= new model_content();
		$mconf	    = new model_conf();	
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		$data['posts'] 	= $mcontent->read_content($id);
		
		$data['header'] = 'Detail Content';
		$data['user'] 	= $user;
		
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			$data['unit']			= $mconf->get_unit($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
			$data['unit']			= $mconf->get_unit('','getfakultas');
		}
		$data['cabang']				= $mconf->get_cabangub();
		$data['content_category'] 	= $mcontent->read_content_category('read-by-category');
		$data['content_view'] 		= 'content';
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/content/content.js');
		
		$this->view('content/page/detail.php', $data );
	}
	
	function detail_file($id){
		$mcontent 	= new model_content();
		$mconf	    = new model_conf();	
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		$data['posts'] 	= $mcontent->read_file($id);
		
		$data['header'] = 'Detail Content';
		$data['user'] 	= $user;
		
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			$data['unit']			= $mconf->get_unit($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
			$data['unit']			= $mconf->get_unit('','getfakultas');
		}
		$data['cabang']				= $mconf->get_cabangub();
		$data['content_category'] 	= $mcontent->read_content_category('read-by-category');
		$data['content_view'] 		= 'file';
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/content/content.js');
		
		$this->view('content/page/detail.php', $data );
	}
	
	function get_content_child($content_id, $type){
		$mcontent = new model_content();
		switch ($type) {
			case 'content-menu':
				$data['posts'] 	= $mcontent->read_content($content_id,'','','','','child');
				$this->view('content/page/view-child.php', $data );
			break;
		}
	}
	
	function write_page(){
		$mcontent = new model_content();
		$mconf	    = new model_conf();	
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		$data['posts']  = '';
		$data['header'] = 'New Content';
		$data['user'] 	= $user;
		
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			$data['unit']			= $mconf->get_unit($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
			$data['unit']			= $mconf->get_unit('','getfakultas');
		}
		$data['cabang']				= $mconf->get_cabangub();
		$data['content_category'] 	= $mcontent->read_content_category('read-by-category');
		$data['fakultasid']			= $fakultasid;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/jquery/tagmanager.js');
		$this->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/content/content.js');
		$this->add_script('js/jasny-bootstrap.min.js');
		$this->add_style('css/bootstrap/jasny-bootstrap.min.css');
		
		$this->view('content/page/edit.php', $data );
		
	}
	
	function edit_page($id){
		$mcontent   = new model_content();
		$mconf	    = new model_conf();	
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		$data['posts']  = $mcontent->read_content($id);
		$data['header'] = 'Edit Content';
		$data['user'] 	= $user;
		$data['edit_view']  = 'content';
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			$data['unit']			= $mconf->get_unit($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
			$data['unit']			= $mconf->get_unit('','getfakultas');
		}
		$data['cabang']				= $mconf->get_cabangub();
		$data['content_category'] 	= $mcontent->read_content_category('read-by-category');
		$data['fakultasid']			= $fakultasid;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/jquery/tagmanager.js');
		$this->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/content/content.js');
		$this->add_script('js/jasny-bootstrap.min.js');
		$this->add_style('css/bootstrap/jasny-bootstrap.min.css');
		
		$this->view('content/page/edit.php', $data );
	}
	
	function read_unit(){
		$mconf	    	= new model_conf();
		
		$fakultasid 	= $_POST['fakultasid'];
		$unitid 		= $_POST['unit_id'];
		$unit	= $mconf->get_unit($fakultasid,'getfakultas');
		echo '<option value="0">Pilih Unit</option>';
		if(isset($unit)){
			foreach($unit as $u) :
				echo '<optgroup label="'.$u->fakultas.'">';
				$this->read_unit_child($u->fakultas_id,$unitid);
				echo '</optgroup>';
			endforeach;
		}
	}
	
	function read_unit_child($fakultasid,$unitid){
		$mconf	    = new model_conf();	
		$unit 		= $mconf->get_unit($fakultasid,'');
		
		foreach($unit as $u) :
			echo "<option value='".$u->unit_id."' ";
			if(isset($unitid)){
				if($unitid==$u->unit_id){
					echo "selected";
				}
			}
			echo " >".ucWords($u->keterangan)."</option>";
		endforeach;
	}
	
	function read_content(){
		$mcontent 	= new model_content();
		
		$category_content = $_POST['content_category'];
		$contentid = $_POST['content_parent_id'];
		$content_id = $_POST['content_content_id'];
		$lang = $_POST['lang'];
		
		$parent		= $mcontent->read_content('','','','',$category_content,'',$lang);
		
		echo '<option value="0">Pilih Parent</option>';
		if(count($parent)> 0) {
			foreach($parent as $p) :
				if($content_id!=$p->content_id){
					echo "<option value='".$p->content_id."' ";
					if(isset($contentid)){
						if($contentid==$p->content_id){
							echo "selected";
						}
					}
					echo " >".ucWords($p->content_title)."</option>";
				}
			endforeach;
		}
	}
	
	function read_category_child($category,$contentcategory){
		$mcontent   		= new model_content();
		$content_category 	= $mcontent->read_content_category('',$category);
		
		foreach($content_category as $cc) :
			echo "<option value='".$cc->content_category."' data-category='".$cc->category."' ";
			if(isset($contentcategory)){
				if($contentcategory==$cc->content_category){
					echo "selected";
				}
			}
			echo " >".ucWords($cc->content_category)."</option>";
		endforeach;
	}
	
	function get_attach_file($content_id,$view){
		$mcontent   = new model_content();
		$data 		= $mcontent->get_content_upload($content_id,'content','file');
		if(isset($data)){
			foreach ($data as $d) {
				echo '<p>';
				if($view=='view'){
					echo '<a href="javascript::" data-toggle="modal" onclick="view_attach(\''.$d->title.'\',\''.$d->file_type.'\',\''.$d->note.'\',\''.$this->asset($d->file_loc).'\')">
						  <i class="fa fa-paperclip">&nbsp;</i>'.$d->title.'
						  </a>';
				}
				elseif($view=='edit'){
					echo $d->title;
					echo '<a href="javascript::" onclick="delete_file_attach(this,\''.$d->upload_id.'\')">
						  <span class="label label-danger"><i class="fa fa-trash-o">&nbsp;</i>Delete</span>
						  </a>';
				}
				echo '</p>';
			}
		}else{
			if($view=='view'){echo '-';}
		}
	}
	
	function save_coms_content(){
		$mcontent 	= new model_content();
		$userid		= $this->coms->authenticatedUser->id;
		
		if(isset($_POST['hidId'])&&$_POST['hidId']!=''){
			$content_id = $_POST['hidId'];
			$action		= 'edit';
		}else{
			$content_id = $mcontent->get_content_reg_number();
			$action		= 'new';
		}
		
		$content_category 	= $_POST['category'];
		$menu_order			= $_POST['urut'];
		$unitid 			= $_POST['unit'];
		$fakultas_id 		= $_POST['fakultas'];
		$cabang				= $_POST['cabang'];
		$c_title 			= $_POST['title'];
		$c_link  			= trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($c_title))))));
		$c_data				= $_POST['list-data'];
		$c_status			= $_POST['status'];
		$c_lang				= $_POST['lng'];
		$c_author			= $userid;
		$c_upload			= $_POST['content-upload'];
		$c_excerpt			= $_POST['contentexcerpt'];
		
		if(isset($_POST['comment'])!=""){
			$c_comment	= $_POST['comment'];
		}else{
			$c_comment	= 0;
		}
		
		if(isset($_POST['issticky'])!=""){
			$issticky	= $_POST['issticky'];
		}else{
			$issticky	= 0;
		}
			
		$lastupdate		= date("Y-m-d H:i:s");
		
		if($_POST['parent-page']!='0'){
			$parentid 		= $mcontent->get_content_id($_POST['parent-page'],'')->content_id;
		}else{
			$parentid		= $_POST['parent-page'];
		}
		
		
		$c_content = $this->content_image_upload($_POST['content'],$fakultas_id,$content_id,'content');
		
		if(isset($_POST['hidimg'])&&$_POST['hidimg']!=""){
			$file_loc 		= $this->content_thumb_upload($fakultas_id,$_POST['hidimg']);
		}else{
			$file_loc 		= $this->content_thumb_upload($fakultas_id,'');
		}
		
		if(isset($_POST['attach-name'])){
			$this->content_attach_file($content_id,$fakultas_id);
		}
		
		if(isset($_POST['hidden-tag'])&&$_POST['hidden-tag']!=''){
			$this->save_content_tag($content_id, $_POST['hidden-tag'],$action);
		}
		
		$datanya 	= Array(
							'content_id'=>$content_id,
							'content_category'=>$content_category,
							'menu_order'=>$menu_order, 
							'unit_id'=>$unitid,
							'fakultas_id'=>$fakultas_id, 
							'cabang_id'=>$cabang,
							'content_title'=>$c_title, 
							'content_excerpt'=>$c_excerpt,
							'content'=>$c_content,
							'content_link'=>$c_link,
							'content_data'=>$c_data,
							'content_status'=>$c_status,
							'content_lang'=>$c_lang,
							'content_author'=>$c_author,
							'content_upload'=>$c_upload,
							'content_comment'=>$c_comment,
							'content_thumb_img'=>$file_loc,
							'is_sticky'=>$issticky,
							'user_id'=>$userid,
							'last_update'=>$lastupdate,
							'parent_id'=>$parentid
							);
		if($mcontent->replace_content($datanya)){
			echo "sukses";
		}else echo "gagal";
		
	}
	
	function save_content_tag($content_id, $tag, $action){
		$mcontent 		= new model_content();
		
		// 
		$tag = explode(',', $tag);
		
		if($action == 'edit'){
			$data = $mcontent->read_content_tag($content_id,'');
			$tag_db = array();
			$not_in_tag = array();
			
			foreach ($data as $d) {
				$tag_db[]=$d->tag;
			}
			
			for($i=0;$i<count($tag);$i++){
				if (in_array($tag[$i], $tag_db) || !in_array($tag[$i], $tag_db)){
					$datanya 	= Array(
									'content_id'=>$content_id,
									'tag'=>$tag[$i],
									);
					$mcontent->replace_content_tag($datanya);
				}
			}
			
			for($i=0;$i<count($tag_db);$i++){
				if (!in_array($tag_db[$i], $tag)){
					$not_in_tag[] = $tag_db[$i];
				}
			}
			
			// print_r($not_in_tag);
			
			if(count($not_in_tag)>0){
				for($i=0;$i<count($not_in_tag);$i++){
					$mcontent->delete_tag($content_id,$not_in_tag[$i],'');
				}
			}
			
		}else{
			for($i=0;$i<count($tag);$i++){
				$datanya 	= Array(
								'content_id'=>$content_id,
								'tag'=>$tag[$i],
								);
				$mcontent->replace_content_tag($datanya);
			}
		}

	}
	
	function content_attach_file($content_id,$fakultas_id){
		$mcontent 		= new model_content();
		$month 			= date('m');
		$year 			= date('Y');
		$cekicon		= '';
		
		$attach_title 	= $_POST['attach-name'];
		$attach_note 	= $_POST['attach-note'];
		// $attach_sys	  	= $_POST['attach-sys'];
		
		for($i=0;$i<count($attach_title);$i++){
			foreach ($_FILES['attach-file'] as $id => $icon) {
				$brokeext			= $this->get_extension($icon[$i]);
				if($id == 'name'){
					$ext 	= $brokeext;
					$file	= $icon[$i];
				}
				if($id == 'type'){
					$file_type = $_FILES['attach-file']['type'][$i];
				}
				if($id == 'size'){
					$file_size = $_FILES['attach-file']['size'][$i];
				}
				if($id == 'tmp_name'){
					$file_tmp_name = $_FILES['attach-file']['tmp_name'][$i];
				}
				if($id == 'error'){
					if($icon!=0){
						$cekicon = 'error';
					}
				}
			}

			$upload_dir = 'assets/upload/file/'.$fakultas_id.'/konten/'.$year. '-' .$month.'/';		
			$upload_dir_db = 'upload/file/'.$fakultas_id.'/konten/'.$year. '-' .$month.'/';	
			
			// if (!file_exists($upload_dir)) {
				// mkdir($upload_dir, 0777, true);
				// chmod($upload_dir, 0777);
			// }
			$file_loc = $upload_dir_db . $file;
			
			$filetype = explode('.', $file);
			$type	  = $filetype[1];
				
			$file_type_doc = array('doc','docx','pdf');
			$file_type_spread = array('xls','xlsx');
			if(in_array($type,$file_type_doc)){
				$type = 'document';
			}elseif(in_array($type,$file_type_spread)){
				$type = 'spreadsheet';
			}
			
			$allowed_ext = array('doc', 'docx', 'pdf', 'xls', 'xlsx');
			if(!in_array($ext,$allowed_ext)){
				echo "wrong attach type";
				exit;
			}
			
			if($_SERVER['REQUEST_METHOD'] == 'POST') {
				//------UPLOAD USING CURL----------------
				//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
				$url	     = $this->config->file_url;
				$filedata    = $file_tmp_name;
				$filename    = $file;
				$filenamenew = $file;
				$filesize    = $file_size;
			
				$headers = array("Content-Type:multipart/form-data");
				$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
	            $ch = curl_init();
	            $options = array(
	                CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
	            ); // cURL options 
				curl_setopt_array($ch, $options);
		        curl_exec($ch);
		        if(!curl_errno($ch))
		        {
		            $info = curl_getinfo($ch);
		            if ($info['http_code'] == 200)
		               $errmsg = "File uploaded successfully";
		        }
		        else
		        {
		            $errmsg = curl_error($ch);
		        }
		        curl_close($ch);
				//------UPLOAD USING CURL----------------
				
				// rename($file_tmp_name, $upload_dir . $file);
				// unlink($upload_dir . $file);
			}
			
			$upload_id = $mcontent->get_content_upload_reg_number();
			$datanya 	= Array(
						'upload_id'=>$upload_id,
						'content_id'=>$content_id,
						'title'=>$attach_title[$i], 
						'note'=>$attach_note[$i], 
						'file_loc'=>$file_loc, 
						'file_type'=>$type,
						'kategori'=>'file'
						);
			$mcontent->replace_content_upload($datanya);
			
		};
		
	}
	
	function content_image_upload($c_content,$fakultas_id,$content_id,$param){
		$mcontent 	= new model_content();
		// echo htmlspecialchars($c_content)."<br><br>";
		$src 	= Array();
		$result = Array();
		
		$string = explode('<', $c_content);
		foreach ($string as $s) {
			if (strpos($s,'img') !== false) {
				$imgsrc = explode('src', $s);
				preg_match_all('/".*?"|\'.*?\'/', $imgsrc[1], $source);
				// echo substr(str_replace('%20', ' ', $source[0][0]), 1, -1)."<br><br>";
				
				$src[] = substr(str_replace('%20', ' ', $source[0][0]), 1, -1);
			}
		};
		
		foreach ($src as $s) {
			$file_tmp_name = $s;
			// echo $file_tmp_name."<br><br>";
			//----------------------------------------------------
			$month 			= date('m');
			$year 			= date('Y');
			$upload_dir		= 'assets/upload/file/'.$fakultas_id.'/konten/'.$year. '-' .$month.'/';		
			$upload_dir_db  = 'upload/file/'.$fakultas_id.'/konten/'.$year. '-' .$month.'/';	
			
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$file_tmp_name)) {
				// echo $file_tmp_name."<br>";
				$filename = explode('/', $file_tmp_name);
				
				if($filename[4]!='images'){
					$file 	  = $filename[4];
				}else $file 	  = $filename[5];
				
			    $filetype = explode('.', $file);
				$type	  = $filetype[1];
				
				$img_type = array('jpg','jpeg','png','gif');
				if(in_array($type,$img_type)){
					$type = 'image';
				}
				$file_loc = $upload_dir_db . $file;
				$result[] = $file_loc;
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					// copy($_SERVER['DOCUMENT_ROOT'].$file_tmp_name, $upload_dir . $file);
					
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $_SERVER['DOCUMENT_ROOT'].$file_tmp_name;
					$filename    = $file;
					$filenamenew = $file;
					$filesize    = '';
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		            $ch = curl_init();
		            $options = array(
						 CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
					
		            ); // cURL options 
					curl_setopt_array($ch, $options);
			        curl_exec($ch);
			        if(!curl_errno($ch))
			        {
			            $info = curl_getinfo($ch);
			            if ($info['http_code'] == 200)
			               $errmsg = "File uploaded successfully";
			        }
			        else
			        {
			            $errmsg = curl_error($ch);
			        }
			        curl_close($ch);
					//------UPLOAD USING CURL----------------
					
				} 
				
				$upload_id = $mcontent->get_content_upload_reg_number();
				if($param=='content'){
					$datanya 	= Array(
								'upload_id'=>$upload_id,
								'content_id'=>$content_id,
								'title'=>$file, 
								'file_loc'=>$file_loc, 
								'file_type'=>$type
								);
				}else{
					$datanya 	= Array(
								'upload_id'=>$upload_id,
								'file_id'=>$content_id,
								'title'=>$file, 
								'file_loc'=>$file_loc, 
								'file_type'=>$type
								);
				}
				$mcontent->replace_content_upload($datanya);
								
			}
			else{
				$filedatac = explode('/', $file_tmp_name);
				$wpcontent = $filedatac[4];
				if($wpcontent=='wp-content'){
					$name 	  = $filedatac[8];
					
					$filetype = explode('.', $name);
					$type	  = $filetype[1];
					
					if($_SERVER['REQUEST_METHOD'] == 'POST') {
						copy($file_tmp_name, $upload_dir . $name);
						//------UPLOAD USING CURL----------------
						//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
						$url	     = $this->config->file_url;
						$filedata    = $upload_dir . $name;
						$filename    = $name;
						$filenamenew = $name;
						$filesize    = '';
					
						$headers = array("Content-Type:multipart/form-data");
						$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
			            $ch = curl_init();
			            $options = array(
							CURLOPT_URL => $url,
			                CURLOPT_HEADER => true,
							CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
			                CURLOPT_POST => 1,
			                CURLOPT_HTTPHEADER => $headers,
			                CURLOPT_POSTFIELDS => $postfields,
			                CURLOPT_INFILESIZE => $filesize,
			                CURLOPT_RETURNTRANSFER => true,
			                CURLOPT_SSL_VERIFYPEER => false,
			  				CURLOPT_SSL_VERIFYHOST => 2
						
			            ); // cURL options 
						curl_setopt_array($ch, $options);
				        curl_exec($ch);
				        if(!curl_errno($ch))
				        {
				            $info = curl_getinfo($ch);
				            if ($info['http_code'] == 200)
				              $errmsg = "File uploaded successfully";
				        }
				        else
				        {
				            $errmsg = curl_error($ch);
				        }
				        curl_close($ch);
						unlink($upload_dir . $name);
						//------UPLOAD USING CURL----------------
					}
					$img_type = array('jpg','jpeg','png','gif');
					if(in_array($type,$img_type)){
						$type = 'image';
					}
					$file_loc = $upload_dir_db . $name;
					$result[] = $file_loc;
					
					$upload_id = $mcontent->get_content_upload_reg_number();
					if($param=='content'){
						$datanya 	= Array(
									'upload_id'=>$upload_id,
									'content_id'=>$content_id,
									'title'=>$name, 
									'file_loc'=>$file_loc, 
									'file_type'=>$type
									);
					}else{
						$datanya 	= Array(
									'upload_id'=>$upload_id,
									'file_id'=>$content_id,
									'title'=>$name, 
									'file_loc'=>$file_loc, 
									'file_type'=>$type
									);
					}		
					$mcontent->replace_content_upload($datanya);
					
					
				}else{
					$filename = explode('/', $file_tmp_name);
					$file 	  = $filename[11];
					
				    $filetype = explode('.', $file);
					$type	  = $filetype[1];
					
					$file_loc = $upload_dir_db . $file;
					$result[] = $file_loc;
				}
				
			}
			//----------------------------------------------------
		}
		// var_dump(str_replace(' ', '%20', $src));
		// echo "<br><br>";
		// var_dump($result);
		$newcontent = str_replace(str_replace(' ', '%20', $src), $result, $c_content);
		// echo htmlspecialchars($newcontent);
		return $newcontent;
	}
	
	function content_thumb_upload($fakultas_id,$hidimg){
		$month 			= date('m');
		$year 			= date('Y');
		$cekicon		= '';
		
		// print_r($_FILES);
		
		if(isset($_FILES['files']) || isset($_FILES['file-file'])){
			foreach ($_FILES['files'] as $id => $icon) {
				if($id == 'error'){
					if($icon!=0){
						$cekicon = 'error';
					}
				}
			}
			//------------------UPLOAD FILE START---------------------------------------------
			if($cekicon!='error'){
				foreach ($_FILES['files'] as $id => $icon) {
					$brokeext			= $this->get_extension($icon);
					if($id == 'name'){
						$ext 	= $brokeext;
						$file	= $icon;
					}
					if($id == 'type'){
						$file_type = $_FILES['files']['type'];
					}
					if($id == 'size'){
						$file_size = $_FILES['files']['size'];
					}
					if($id == 'tmp_name'){
						$file_tmp_name = $_FILES['files']['tmp_name'];
					}
				}
	
				$extpath = "image";
	
				$upload_dir = 'assets/upload/file/'.$fakultas_id.'/konten/'.$year. '-' .$month.'/';		
				$upload_dir_db = 'upload/file/'.$fakultas_id.'/konten/'.$year. '-' .$month.'/';	
				
				if (!file_exists($upload_dir)) {
					mkdir($upload_dir, 0777, true);
					chmod($upload_dir, 0777);
				}
				$file_loc = $upload_dir_db . $file;
				
				$allowed_ext = array('jpg','jpeg','png','gif');
				if(!in_array($ext,$allowed_ext)){
					echo "wrong type";
					exit;
				}
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					rename($file_tmp_name, $upload_dir . $file);
					
					$newname= $upload_dir.$file;  
					$dst_x = 0;
					$dst_y = 0;
					$src_x = 200; // Crop Start X
					$src_y = 200; // Crop Srart Y
					$dst_w = 350; // Thumb width
					
					$src_image = imagecreatefromstring(file_get_contents($newname));
					$src_w = ImageSX($src_image);
					$src_h = ImageSY($src_image);
					
					$ratio = $dst_w / $src_w;
					$dst_h = $src_h * $ratio;
					
					/*$dst_h = 160; // Thumb height*/
					//$src_w = 220; // $src_x + $dst_w
					//$src_h = 260; // $src_y + $dst_h
	
					$dst_image = imagecreatetruecolor($dst_w,$dst_h);				
		  
					$img = file_get_contents($newname);
					//imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
					imagecopyresampled($dst_image, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);
					imagejpeg($dst_image, $upload_dir.$file);
					
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $newname;
					$filename    = $file;
					$filenamenew = $file;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		            $ch = curl_init();
		            $options = array(
		                CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
		            ); // cURL options 
					curl_setopt_array($ch, $options);
			        curl_exec($ch);
			        if(!curl_errno($ch))
			        {
			            $info = curl_getinfo($ch);
			            if ($info['http_code'] == 200)
			               $errmsg = "File uploaded successfully";
			        }
			        else
			        {
			            $errmsg = curl_error($ch);
			        }
			        curl_close($ch);
					//------UPLOAD USING CURL----------------
					unlink($upload_dir . $file);
				}
			}
			else {
				if($hidimg!=''){
					$file_loc	= $hidimg;
				}else{
					$file_loc	= NULL;
				}
					
			}			
			//------------------UPLOAD FILE END-----------------------------------------------------
		}else {
			if($hidimg!=''){
				$file_loc	= $hidimg;
			}else{
				$file_loc	= NULL;
			}
		}
		
		return $file_loc;
	}
	
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_tag_content($content_id,$view){
		$mcontent 	= new model_content();
		$data 		= $mcontent->read_content_tag($content_id,'md5');
		$result		= '';
		$i			= 0;
		
		if(isset($data)){
			foreach ($data as $d) {
				if($view!='edit'){	
					echo '<span class="tag-content">';
					echo '<i class="fa fa-tag"></i>'.$d->tag;
					// if($view=='edit'){
						// echo '<a href="javascript::" onclick="delete_tag(this,\''.$content_id.'\',\''.$d->tag.'\')" ><i class="fa fa-times"></i></a>';
					// }
					echo '&nbsp;&nbsp;</span>';
				}
				else{
						if($i==0){
							$result = $d->tag;
						}else{
							$result = $result.','.$d->tag;
						}
						$i++;
				}
			}
			if($view=='edit'){
				// echo '<input type="text" id="tag-value" value="'.$result.'" />';
				echo "<span class='tag-result' style='display:none;'>".$result."</span>";
			}
		}
	}
	
	function delete_tag(){
		$mcontent 	= new model_content();
		
		$contentid	= $_POST['contentid'];
		$tag		= $_POST['tag'];
		
		$mcontent->delete_tag($contentid,$tag,'md5');
		
	}
	
	function delete_content(){
		$mcontent 	= new model_content();
		
		$contentid	= $_POST['contentid'];
		
		$contentdel = $mcontent->get_content_id($contentid,'parent');
		if(isset($contentdel)){
			foreach ($contentdel as $c) {
				$mcontent->delete_tag($c->content_id_md5,'','md5');
			}
		}
		
		if($mcontent->delete_content($contentid)){
			echo "sukses";
		}else echo "gagal";
	}
	
	function delete_content_upload(){
		$mcontent 	= new model_content();
		
		$uploadid	= $_POST['uploadid'];
		
		if($mcontent->delete_content_upload($uploadid)){
			echo "sukses";
		}else echo "gagal";
	}
	
	/*
	 * END COMS_CONTENT========================== START COMS_FILE
	 * */
	 function edit_file($id){
	 	$mcontent   = new model_content();
		$mconf	    = new model_conf();	
		$user 		= $this->coms->authenticatedUser->role;
		$fakultasid = $this->coms->authenticatedUser->fakultas;
		
		$data['posts']  	= $mcontent->read_file($id);
		$data['edit_view']  = 'file';
		$data['header'] 	= 'Edit Content';
		$data['user'] 		= $user;
		
		if($user=='dosen'||$user=='mahasiswa'){
			$data['get_fakultas']	= $mconf->get_fakultas($fakultasid);
			$data['unit']			= $mconf->get_unit($fakultasid);
		}else{
			$data['get_fakultas']	= $mconf->get_fakultas();
			$data['unit']			= $mconf->get_unit('','getfakultas');
		}
		$data['cabang']				= $mconf->get_cabangub();
		$data['content_category'] 	= $mcontent->read_content_category('read-by-category');
		$data['fakultasid']			= $fakultasid;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/jquery/tagmanager.js');
		$this->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/content/content.js');
		$this->add_script('js/jasny-bootstrap.min.js');
		$this->add_style('css/bootstrap/jasny-bootstrap.min.css');
		
		$this->view('content/page/edit.php', $data );
	 }
	 
	 function delete_file(){
	 	$mcontent 	= new model_content();
		
		$fileid	= $_POST['fileid'];
		
		if($mcontent->delete_file($fileid)){
			echo "sukses";
		}else echo "gagal";
	 }
	 
	 function save_coms_upload(){
	 	$mcontent 	= new model_content();
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		$month 			= date('m');
		$year 			= date('Y');
		
		if(isset($_POST['hidFileId'])&&$_POST['hidFileId']!=''){
			$file_id = $_POST['hidFileId'];
		}else{
			$file_id = $mcontent->get_file_reg_number();
		}
		
		$fakultas_id 		= $_POST['fakultas'];
		$cabang				= $_POST['cabang'];
		$unitid 			= $_POST['unit'];
		$content_category 	= $_POST['category'];

		$f_title 			= $_POST['filetitle'];
		$f_note 			= $this->content_image_upload($_POST['filenote'],$fakultas_id,$file_id,'file');
		$f_data				= $_POST['filedata'];
		$f_group			= $_POST['file-group'];
		$f_lang				= $_POST['lng'];
		
		if(isset($_POST['status'])&&$_POST['status']=='publish'){
			$ispublish	= 1;
		}else{
			$ispublish	= 0;
		}
		
		$hidimg = '';
		$cekicon = '';
		
		if($_FILES){
			foreach ($_FILES['file-file'] as $id => $icon) {
				if($id == 'error'){
					if($icon!=0){
						$cekicon = 'error';
					}
				}
			}
			//------------------UPLOAD FILE START---------------------------------------------
			if($cekicon!='error'){
				foreach ($_FILES['file-file'] as $id => $icon) {
					$brokeext			= $this->get_extension($icon);
					if($id == 'name'){
						$ext 	= $brokeext;
						$file	= $icon;
					}
					if($id == 'type'){
						$file_type = $_FILES['file-file']['type'];
					}
					if($id == 'size'){
						$file_size = $_FILES['file-file']['size'];
					}
					if($id == 'tmp_name'){
						$file_tmp_name = $_FILES['file-file']['tmp_name'];
					}
				}
	
				$extpath = "image";
	
				$upload_dir = 'assets/upload/file/'.$fakultas_id.'/konten/'.$content_category.'/'.$year. '-' .$month.'/';		
				$upload_dir_db = 'upload/file/'.$fakultas_id.'/konten/'.$content_category.'/'.$year. '-' .$month.'/';	
				
				if (!file_exists($upload_dir)) {
					mkdir($upload_dir, 0777, true);
					chmod($upload_dir, 0777);
				}
				$file_loc = $upload_dir_db . $file;
				
				$allowed_ext = array('jpg','jpeg','png','gif','mp4','webm','ogg');
				if(!in_array($ext,$allowed_ext)){
					echo "wrong type";
					exit;
				}
				
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					
					//------UPLOAD USING CURL----------------
					//$url	   = 'https://175.45.187.253/beta/file_upload/upload.php';
					$url	     = $this->config->file_url;
					$filedata    = $file_tmp_name;
					$filename    = $file;
					$filenamenew = $file;
					$filesize    = $file_size;
				
					$headers = array("Content-Type:multipart/form-data");
					$postfields = array("filedata" => "@$filedata", "filename" => $filename, "filedir"=>$upload_dir, "filenamenew"=>$filenamenew);
		            $ch = curl_init();
		            $options = array(
						 CURLOPT_URL => $url,
		                CURLOPT_HEADER => true,
						CURLOPT_SAFE_UPLOAD => $this->config->safe_upload,
		                CURLOPT_POST => 1,
		                CURLOPT_HTTPHEADER => $headers,
		                CURLOPT_POSTFIELDS => $postfields,
		                CURLOPT_INFILESIZE => $filesize,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_SSL_VERIFYPEER => false,
		  				CURLOPT_SSL_VERIFYHOST => 2
					
		            ); // cURL options 
					curl_setopt_array($ch, $options);
			        curl_exec($ch);
			        if(!curl_errno($ch))
			        {
			            $info = curl_getinfo($ch);
			            if ($info['http_code'] == 200)
			               $errmsg = "File uploaded successfully";
			        }
			        else
			        {
			            $errmsg = curl_error($ch);
			        }
			        curl_close($ch);
					//------UPLOAD USING CURL----------------
					
					// rename($file_tmp_name, $upload_dir . $file);
					// unlink($upload_dir . $file);
				}
			}
			else {
				if(isset($_POST['hid-file-img'])&&$_POST['hid-file-img']!=""){
					$file_loc 	= $_POST['hid-file-img'];
					$file_type	= $_POST['hid-file-type'];
					$file		= $_POST['hid-file-name'];
					$file_size	= $_POST['hid-file-size'];
				}else{
					$file_loc	= NULL;
					$file_type	= NULL;
					$file		= NULL;
					$file_size	= NULL;
				}
					
			}			
			//------------------UPLOAD FILE END-----------------------------------------------------
		}else {
			if(isset($_POST['hid-file-img'])&&$_POST['hid-file-img']!=""){
				$file_loc 	= $_POST['hid-file-img'];
				$file_type	= $_POST['hid-file-type'];
				$file		= $_POST['hid-file-name'];
				$file_size	= $_POST['hid-file-size'];
			}else{
				$file_loc	= NULL;
				$file_type	= NULL;
				$file		= NULL;
				$file_size	= NULL;
				
			}
		}
		
		$datanya 	= Array(
							'file_id'=>$file_id,
							'content_category'=>$content_category,
							'unit_id'=>$unitid,
							'fakultas_id'=>$fakultas_id, 
							'cabang_id'=>$cabang,
							'file_lang'=>$f_lang,
							'file_title'=>$f_title, 
							'file_note'=>$f_note,
							'file_data'=>$f_data,
							'file_group'=>$f_group,
							'file_type'=>$file_type,
							'file_name'=>$file,
							'file_size'=>$file_size,
							'file_loc'=>$file_loc,
							'is_publish'=>$ispublish,
							'user_id'=>$userid,
							'last_update'=>$lastupdate
							);
		if($mcontent->replace_file_upload($datanya)){
			echo "sukses";
		}else echo "gagal";
		
	 }

}
?>