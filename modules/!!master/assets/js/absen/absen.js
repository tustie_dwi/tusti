$(document).ready(function() {
	$('.e9').select2();
	
	$("select[name='jadwal-mk']").change(function(){
		var jadwal = $(this).val();
		var start = $('option:selected', this).attr('start');
		var finish = $('option:selected', this).attr('finish');
		$("input[name='jam_mulai']").val(start);
		$("input[name='jam_selesai']").val(finish);
	});
	
	$("#form-absen-dosen").submit(function(e){
		var postData = new FormData($(this)[0]);
		$.ajax({
			url : base_url + "module/master/akademik/save_absen",
			type: "POST",
			data : postData,
			async: false,
			success:function(msg) 
			{
				alert(msg);
			    location.reload();  
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
			    alert ('Proses Gagal!');      
			    // location.reload(); 
			},
		    cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault();
		return false;
	});

	var edit = $("input[name='hidId']").length;
	var date_index = $("#dateMulai").length;
	if(edit || date_index){
		$('#dateMulai, #dateSelesai, .date').datetimepicker({
		  pickTime : false,
		  format : 'YYYY-MM-DD'
		});
		
		$('.time').datetimepicker({
		  pickTime : true,
		  pickDate : false,
		  format : 'HH:mm:s'
		});
	};
	
	$(".close").click(function(){
		$("select[name='jadwal-mk']").select2("val","0");
		$("input[name='tgl_pertemuan']").val("");
		$("input[name='jam_mulai']").val("");
		$("input[name='jam_selesai']").val("");
		$("input[name='materi']").val("");
		$("input[name='total']").val("");
		$("select[name='sesi']").select2("val","0");
		$("input[name='hidId_absen']").val("");
		$("input[name='hidId_absendosen']").val("");
		$("input[name='dosen']").val("");
	});
	
	if($("#content_print").length){
		$("button.btn_cetak, .action_btn, .tambah_absen").hide();
		 
		var printContents = document.getElementById("cetak").innerHTML;
	    var originalContents = document.body.innerHTML;
	
	    document.body.innerHTML = printContents;
	    window.print();
	    document.body.innerHTML = originalContents;
	}
});
$(document).on('change', '#inp_tahun', function(){
	var thn_id = $(this).val();
	var tipe = $("[name=tipe]").val();
	
	// ajax_dosen(thn_id, tipe, '');
	
	$.ajax({
        url : base_url + 'module/master/akademik/get_mk',
        type: "POST",
        dataType : "HTML",
        data : $.param({
        	tahunId : thn_id, 
        	tipe : tipe
        }),
        success:function(msg) 
        {
        	// alert(msg);
            $("#inp_mk").html(msg);
            filter_prodi();
            
            // $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	// $("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
	
});

$(document).on( 'change', '#inp_mk', function(){
	var id = $(this).val();
	var tipe_ = $("[name=tipe]").val();
	ajax_dosen('', tipe_, id);
});

function ajax_dosen(thn_id, tipe, mkid){
	$.ajax({
        url : base_url + 'module/master/akademik/get_dosen',
        type: "POST",
        dataType : "HTML",
        data : $.param({
        	tahunId : thn_id, 
        	tipe : tipe,
        	periodeId : mkid
        }),
        success:function(msg) 
        {
        	// alert(msg);
           $("#inp_dosen").html(msg);
            // filter_prodi();
            // $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	// $("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
}

function filter_prodi(){
	var prodi = $("#inp_prodi").val();
	
	$.each($("#inp_mk").find("[data-prodi]"), function(){
		if($(this).text().indexOf("Pilih Matakuliah") < 0 && prodi != ''){
			if($(this).data("prodi") == prodi) $(this).show();
			else $(this).hide();
		}
		else{
			$(this).show();
		}
	});
}

function get_absen(){
	var thnid = $("#inp_tahun").val();
	var prodiid = $("#inp_prodi").val();
	var mk = $("#inp_mk").val().split("|")[3];
	var kelas = $("#inp_mk").val().split("|")[1];
	var dosen = $("#inp_dosen").val();
	
	var datemulai = $("#dateMulai").val();
	var dateselesai = $("#dateSelesai").val();
	
	if(datemulai.length!=0 && dateselesai.length!=0){	
		var form = document.createElement("form");
		var input = document.createElement("input");
		var input2 = document.createElement("input");
		var input3 = document.createElement("input");
		var input4 = document.createElement("input");
		var input5 = document.createElement("input");
		var input6 = document.createElement("input");
		var input7 = document.createElement("input");
		
		form.action = base_url + 'module/master/akademik/detail_absen';
		form.method = "post"
		
		input.name = "thnid";
		input.value = thnid;
		form.appendChild(input);
		
		input2.name = "prodi";
		input2.value = prodiid;
		form.appendChild(input2);
		
		input3.name = "mk";
		input3.value = mk;
		form.appendChild(input3);
		
		input4.name = "dosen";
		input4.value = dosen;
		form.appendChild(input4);
		
		input5.name = "datemulai";
		input5.value = datemulai;
		form.appendChild(input5);
		
		input6.name = "dateselesai";
		input6.value = dateselesai;
		form.appendChild(input6);
		
		input7.name = "kelas";
		input7.value = kelas;
		form.appendChild(input7);
		
		document.body.appendChild(form);
		form.submit();
	}else{
		alert("Lengkapi Data Anda!");
	}
}

function cetak(thn, mk, kls, dosen, tgl_mulai, tgl_selesai)
{
	var form = document.createElement("form");
	var input = document.createElement("input");
	var input2 = document.createElement("input");
	var input3 = document.createElement("input");
	var input4 = document.createElement("input");
	var input5 = document.createElement("input");
	var input6 = document.createElement("input");
	var input7 = document.createElement("input");
	
	form.action = base_url + 'module/master/akademik/detail_absen';
	form.method = "post"
	
	input.name = "thnid";
	input.value = thn;
	form.appendChild(input);
	
	input2.name = "mk";
	input2.value = mk;
	form.appendChild(input2);
	
	input3.name = "dosen";
	input3.value = dosen;
	form.appendChild(input3);
	
	input4.name = "datemulai";
	input4.value = tgl_mulai;
	form.appendChild(input4);
	
	input5.name = "dateselesai";
	input5.value = tgl_selesai;
	form.appendChild(input5);
	
	input6.name = "kelas";
	input6.value = kls;
	form.appendChild(input6);
	
	input7.name = "view";
	input7.value = "cetak";
	form.appendChild(input7);
	
	document.body.appendChild(form);
	form.submit();
};

function add_absen(karyawan,dosen,jadwal){
	$("#myModalLabel").html("Tambah Absen");
	$("input[name='dosen']").val(dosen);
	$.ajax({
        url : base_url + 'module/master/akademik/get_jadwal',
        type: "POST",
        dataType : "html",
        data : $.param({
        	karyawan : karyawan
        }),
        success:function(data) 
        {
        	$("select[name='jadwal-mk']").html(data);
        	$("select[name='jadwal-mk']").select2("val",jadwal);
        }
    });
    $('#dateMulai, #dateSelesai, .date').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
	
	$('.time').datetimepicker({
	  pickTime : true,
	  pickDate : false,
	  format : 'HH:mm:s'
	});
}

function edit_absen(absen,karyawan){
	$("#myModalLabel").html("Edit Absen");
	$.ajax({
        url : base_url + 'module/master/akademik/get_jadwal',
        type: "POST",
        dataType : "html",
        data : $.param({
        	karyawan : karyawan
        }),
        success:function(data) 
        {
        	$("select[name='jadwal-mk']").html(data);
        }
    });
	$.ajax({
        url : base_url + 'module/master/akademik/et_absen',
        type: "POST",
        dataType : "json",
        data : $.param({
        	absen : absen
        }),
        success:function(data) 
        {
        	$.each(data, function(i, f){
				$("select[name='jadwal-mk']").select2("val",f.jadwal_id);
				$("input[name='tgl_pertemuan']").val(f.tgl_pertemuan);
				$("input[name='jam_mulai']").val(f.jam_masuk);
				$("input[name='jam_selesai']").val(f.jam_selesai);
				$("input[name='materi']").val(f.materi);
				$("input[name='total']").val(f.total);
				$("select[name='sesi']").select2("val",f.sesi);
				$("input[name='hidId_absen']").val(f.hidId);
				$("input[name='hidId_absendosen']").val(f.absendosen_id);
				$("input[name='dosen']").val(f.dosen_id);
			});
        }
    });
    $('#dateMulai, #dateSelesai, .date').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
	
	$('.time').datetimepicker({
	  pickTime : true,
	  pickDate : false,
	  format : 'HH:mm:s'
	});
};

function delete_absen(absen){
	var x =  confirm("Apakah Anda yakin ingin menghapus data ini?");
	if(x){
		$.ajax({
	        url : base_url + 'module/master/akademik/hapus_absen',
	        type: "POST",
	        dataType : "html",
	        data : $.param({
	        	absen : absen
	        }),
	        success:function(msg) 
	        {
	        	alert(msg);
	        	location.reload();
	        }
	    });
   }
}

$(document).on('change','#inp_prodi', function(){
	filter_prodi();
});