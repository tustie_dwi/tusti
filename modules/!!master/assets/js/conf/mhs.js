$(document).ready(function() {
	
	$("#select_indexfakultas").change(function() {
		var fakultas_id = $("#select_indexfakultas").val();
		//alert(fakultas_id);
		$("#select_indexcabang").select2("val","0");
		$("#display").empty();
		
		$("#select_indexangkatan").select2("val","0");
		$("#select_indexangkatan").attr("disabled");
		$("#select_indexangkatan").select2("enable", false);
		
		if(fakultas_id == 0){
			$("#select_indexcabang").attr("disabled");
			$("#select_indexcabang").select2("enable", false);
		}
		else{
			$("#select_indexcabang").removeAttr("disabled");
			$("#select_indexcabang").select2("enable", true);
		}
	});
	
	$("#select_indexcabang").change(function() {
		$("#display").empty();
		var cabang_ = $("#select_indexcabang").val();
		var fakultas_id_ = $("#select_indexfakultas").val();
		var uri = $(this).data("uri");
		
		$("#select_indexangkatan").select2("val","0");
		
		if(cabang_ == 0){
			$("#select_indexangkatan").attr("disabled");
			$("#select_indexangkatan").select2("enable", false);
		}
		else{
			//alert("TES");
			$.ajax({
				type : "POST",
				dataType : "html",
				url : uri,
				data : $.param({
					fakultas_id : fakultas_id_,
					cabang : cabang_
				}),
				success : function(msg) {
					//alert(msg);
					if (msg == '') {
						$("#select_indexangkatan").html('<option value="0">Select Angkatan</option>');
						$("#select_indexangkatan").attr("disabled","disabled");
					} else {
						//alert(uri);
						$("#select_indexangkatan").removeAttr("disabled");
						$("#select_indexangkatan").html(msg);
						$("#select_indexangkatan").select2("enable", true);
						$("#select_indexangkatan").append('<option value="*">Tampilkan Semua Angkatan</option>');
					}
				}
			});
		}
		
	});
	
	$("#select_indexangkatan").change(function() {
		var fakultas_id_ = $("#select_indexfakultas").val();
		var cabang_ = $("#select_indexcabang").val();
		var angkatan_ = $("#select_indexangkatan").val();
		var uri = $(this).data("uri");
		//alert(uri);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id  : fakultas_id_,
				cabang 		 : cabang_,
				angkatan	 : angkatan_
			}),
			success : function(msg) {
				
				if (msg == "<table class='table table-bordered'><tbody></tbody></table>") {
					$("#display").html('<div class="span3" align="center" style="margin-top:20px;"><div class="well">Sorry, no content to show</div></div>');
				} else {
					$("#display").html(msg);
					$("#example").dataTable({ 
						"sDom": "<'row'<'col-sm-12'<'pull-right'f><'pull-left'>r<'clearfix'>>>t<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>",
						"sPaginationType": "bootstrap",
						"aaSorting": [],
						"bSort":false,
						"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"sSearch": ""
						},
						bRetrieve : true}).fnDraw();
				}
			}
		});
	});
	
	$(".e9").select2();
	
	$("#cabang_mhs").change(function() {
		var cabang_id = $("#cbg_mhs").val();
		//alert(fakultas_id);
		$("#prodi_mhs").select2("val","0");
		
		if(cabang_id == 0){
			$("#prodi_mhs").attr("disabled");
			$("#prodi_mhs").select2("enable", false);
		}
		else{
			$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/masterdata/mhs/getprodi',
			data : $.param({
				cabang 		 : cabang_id
			}),
			success : function(msg) {
				if (msg == "") {
				} else {
					$("#prodi_mhs").removeAttr("disabled");
					$("#prodi_mhs").html(msg);
					$("#prodi_mhs").select2("enable", true);
				}
			}
		});
			
		}
	});
	
	$(".form_datetime").datepicker({format: 'yyyy-mm-dd', viewMode: 2});
	
	$("input[name='files']").change(function(){
		$('#list').html('<img style="width: 100px; height: auto;" id="ava" src="#" alt="your image" />');
        readURL(this);
        $('.well').hide();
    });
    
    if( document.getElementById("is_aktif").checked ){
		$('.tidak-aktif').hide();
	}else{
		$('.tidak-aktif').show();
	}
	
	$('#is_aktif').click(function() {
		if( this.checked ){
			$('.tidak-aktif').hide();
		}else{
			$('.tidak-aktif').show();
		}
	});
	
	$("#form-mhs").submit(function(){
		var nama_mhs 		= $("#nama_mhs").val().length;
		var tmpt_lahir_mhs 	= $("#tmpt_lahir_mhs").val().length;
		var tgl_lahir_mhs 	= $("#tgl_lahir_mhs").val().length;
		var angkatan_mhs 	= $("#angkatan_mhs").val().length;
		var prodi_mhs 		= $("#prodi_mhs").val().length;
		var email_mhs 		= $("#email_mhs").val().length;
		var alamat_mhs 		= $("#alamat_mhs").val().length;
		var nama_ortu 		= $("#nama_ortu").val().length;
		var alamat_ortu 	= $("#alamat_ortu").val().length;
		var tlp_ortu 		= $("#tlp_ortu").val().length;
		var alamat_surat 	= $("#alamat_surat").val().length;
		
		if(nama_mhs > 0 && tmpt_lahir_mhs > 0 && tgl_lahir_mhs > 0 && angkatan_mhs > 0 && prodi_mhs > 0 && email_mhs > 0 && alamat_mhs > 0 && nama_ortu > 0 && alamat_ortu > 0 && tlp_ortu > 0 && alamat_surat > 0){
			var formData = new FormData($(this)[0]);
			 $.ajax({
	            url : base_url + "module/masterdata/mhs/saveToDB",
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.href = base_url + "module/masterdata/mhs/"; 
		        },
		        error: function(jqXHR, textStatus, errorThrown) 
		        {
		            alert ('Upload Failed!');      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		      });
		    e.preventDefault(); //STOP default action
		}
		else{
			alert("Lengkapi form sebelum di submit!");
		}
	});
});

function alamat(){
	var alamat_ortu = $("#alamat_ortu").val();
	$("#alamat_surat").val(alamat_ortu);
};

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#ava').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
};
