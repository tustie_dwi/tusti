$(document).ready(function(){
	$("#fakultas_data").hide();
	$("input[name='data']").change(function(){
		$("select[name='fakultas_id']").val('0');
		var data = $(this).val();
		if(data=="pusat"){
			$("select[name='cmbkategori'] option[value='fakultas']").show();
			$("#fakultas_data").hide();
		}
		else{
			$("select[name='cmbkategori'] option[value='fakultas']").hide();
			$("#fakultas_data").show();
		}
	});
	
	$("#strata").hide();
	$("select[name='cmbkategori']").change(function(){
		var data = $(this).val();
		if(data=="prodi"){
			$("#strata").show();
		}
		else{
			$("#strata").hide();
		}
	});
	
	$("#btn-fakultas").change(function(){
		var nilai = $('#btn-fakultas').val();
		var uri = $(this).data("uri");
		
		if(nilai != '0') {
			$.ajax({
				type	: 'POST',
				dataType: 'html',
				url		: uri,
				data	: $.param({fakultasid : nilai}),
				success	: function(msg){
					if(msg != ''){
						$("#btn-unit").removeAttr("disabled");
						$("#btn-unit").html(msg);
					}
					else{
						$("#btn-unit").attr("disabled","disabled");
					}
				}
			});
		}
		else {
			$("#btn-unit").attr("disabled","disabled");
			$("#btn-unit").html("<option value='0'>Silahkan Pilih</option>");
		}
	});
	
	$(".btn-del").click(function(){
		var uri_ = $(this).data("uri");
		var kode_ = $(this).data("kode");
		var id_ = $(this).data("id");
		var kategori_ = $(this).data("kategori");
		
		if(confirm("Apakah Anda yakin ingin menghapus data unit kerja "+kode_+" ?")){
			$.ajax({
				type : 'POST',
				dataType : 'html',
				url : uri_,
				data : $.param({
						id : id_,
						kategori : kategori_
					}),
				success : function(msg){
					$("#notif").html("<div class='alert alert-warning'><button type='button' class='close' data-dismiss='alert'>&times;</button>OK, data unit kerja "+ kode_ +" telah berhasil dihapus.</div>");
					$(".unit-"+id_).fadeOut(500);
				}
			});
		}
	});
	
	$("input[name='uploads']").change(function(){
		$("input[name='hidimg']").val("");
	});
	
	$("#form-unitkerja").submit(function(e){
		var fakultas_id_ = $('#btn-fakultas').val();
		var kode_ = $("#form-unitkerja [name='kode']").val();
		var parent_id_ = $("#form-unitkerja [name='parent_id']").val();
		var keterangan_ = $("#form-unitkerja [name='keterangan']").val();
		var hidId_ = $("#form-unitkerja [name='hidId']").val().trim();
		var chklab_ = $("#form-unitkerja [name='cmbkategori']").val();
		var uri_ = $("#uri").val();
		
		if(hidId_== ''){
			var init = 'disimpan';
		}
		else{
			var init = 'diubah';
		}
		
		var postData = new FormData($(this)[0]);
		$.ajax({
				url : uri_,
				type: "POST",
				data : postData,
				async: false,
				success:function(msg) 
				{
					// alert(msg);
					if(msg == 'err'){
						$("#notif").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>Oops, data tidak tersimpan. Kode Unit Kerja sudah ada!!</div>");
					}
					else{
						alert("Ok, data berhasil "+ init +".");
						window.location.href = base_url + "module/master/general/unit";
					}
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
				    alert ('Proses gagal!');      
				    // location.reload(); 
				},
			    cache: false,
				contentType: false,
				processData: false
		});
		e.preventDefault();
		return false;
	});
	
	var edit = $("input[name='hidId']").val().length;
	if(edit>1){
		$("#fakultas_data").show();
		$("#strata").show();
	}
});
