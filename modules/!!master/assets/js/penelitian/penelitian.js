i=0;
$(document).ready(function(){
	$(".e9").select2();
	//===status===//
	$("#form-status").submit(function(e){
		var kat 	= $("select[name='kategori']").val();
		var ket		= $("texarea[name='keterangan']").val();
		
		if(kat!=='0' && ket!==''){
			var postData = new FormData($(this)[0]);
			$.ajax({
					url : base_url + "module/master/penelitian/save_status",
					type: "POST",
					data : postData,
					async: false,
					success:function(msg) 
					{
						alert(msg);
					    window.location.href = base_url + "module/master/penelitian/status";
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
					    alert ('Proses Gagal!');      
					    // location.reload(); 
					},
				    cache: false,
					contentType: false,
					processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
	
	//===penelitian===//
	//peserta
	$(".add_peserta").click(function(){
		i+=1;
		var form = $(this).data("id");
		var output = "<div class='list-peserta del"+i+"'>"+i+". <span name='delete_kepada' onclick='deleted(\""+i+"\")' style='cursor: pointer; color : red'>Delete <i class='fa fa-trash-o'></i></span>"
		output += "<div class='form-group'>";
		if(form=="dosen"){
			output += "<div class='checkbox'><label><input type='radio' name='isketua' onclick='set_ketua(\""+i+"\")'>&nbsp;Ketua?</label>";
			output += "<input type='hidden' class='is_ketua"+i+"' name='is_ketua[]' value='0'></div>";
			output += "<input type='text' name='dosen[]' class='form-control dosen nama_dosen"+i+"' placeholder='Masukkan Nama Dosen'>";
			output += "<input type='hidden' name='dosen_id[]' class='form-control dosen_id"+i+"'>";
			output += "<input type='hidden' name='peserta_id_dosen[]' class='form-control' value=''>";
			
		}
		else if(form=="mhs"){
			output += "<input type='text' name='mhs[]' class='form-control mhs nama_mhs"+i+"' placeholder='Masukkan Nama Mahasiswa'>";
			output += "<input type='hidden' name='mhs_id[]' class='form-control mhs_id"+i+"'>";
			output += "<input type='hidden' name='peserta_id_mhs[]' class='form-control' value=''>";
		}
		output += "</div></div>";
		
		var cek_dosen = $(".nama_dosen"+(i-1)).val();
		var cek_mhs = $(".nama_mhs"+(i-1)).val();
		if(cek_dosen || cek_mhs || i==1){
			$(".peserta").append(output);
			var fakultas = $("select[name='fakultas']").val();
			if(form=="dosen"){
				$.ajax({
					url : base_url + "module/master/conf/dosen",
					type : "POST",
					data : $.param({
							fakultas_id : fakultas
					}),
					dataType : "json",
					success:function(data) 
					{
						$(".dosen").autocomplete({ 
							source: data,
							minLength: 0, 
							select: function(event, ui) { 
								$(this).val(ui.item.value);
								$(".dosen_id"+i).val(ui.item.id);
							} 
						});
					}
				});
				
			}
			else if(form=="mhs"){
				var fakultas_enc = $('option:selected', "select[name='fakultas']").attr('hidid');
				$.ajax({
					url : base_url + "module/master/conf/mhs",
					type : "POST",
					data : $.param({
							fakultas_id : fakultas_enc
					}),
					dataType : "json",
					success:function(data) 
					{
						$(".mhs").autocomplete({ 
							source: data,
							minLength: 0, 
							select: function(event, ui) { 
								$(this).val(ui.item.value);
								$(".mhs_id"+i).val(ui.item.nim);
							} 
						});
					}
				});
			}
		}
		else{
			alert("Isi form peserta terlebih dahulu!");
			i-=1;
		}
		
		$(".delete_kepada").click(function(){
			alert();
		});
	});
	
	$("select[name='kategori']").change(function(){
		$("select[name='status']").val('0');
		$("select[name='status']").select2('val','0');
		$(".form_progress").empty();
		$("#publikasi_form").empty();
		var cek	= $(this).val();
		if(cek!=='0'){
			$.ajax({
					url : base_url + "module/master/penelitian/get_status",
					type : "POST",
					data : $.param({
							kategori : cek
					}),
					dataType : "html",
					success:function(data) 
					{
						$("select[name='status']").html(data);
					}
				});
		}
	});
	
	$("select[name='status']").change(function(){
		var cek		= $(this).val();
		var hidId	= $("input[name='id']").val();
		var ket = $('option:selected', "select[name='status']").attr('name');
		if(cek!=='0'){
			$.ajax({
					url : base_url + "module/master/penelitian/view_progress_form",
					type : "POST",
					data : $.param({
							keterangan : ket,
							status_id : cek,
							penelitian : hidId
					}),
					dataType : "html",
					success:function(data) 
					{
						$(".form_progress").html(data);
						$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss', viewMode: 2});
						$(".e8").select2();
						if(ket=="Publikasi"){
							$("#form-jurnal").hide();
							$("#form-seminar").hide();
							$("#form-buku").hide();
							var kat = $("select[name='kategori_publikasi']").val();
							if(kat=="jurnal"){
								$("#form-seminar").hide();
								$("#form-buku").hide();
								$("#form-jurnal").show();
							}
							else if(kat=="seminar"){
								$("#form-jurnal").hide();
								$("#form-buku").hide();
								$("#form-seminar").show();
							}
							else if(kat=="buku"){
								$("#form-jurnal").hide();
								$("#form-seminar").hide();
								$("#form-buku").show();
							}
							else{
								$("#form-jurnal").hide();
								$("#form-seminar").hide();
								$("#form-buku").hide();
							}
							
							$("select[name='kategori_publikasi']").change(function(){
								var kat_publish = $(this).val();
								if(kat_publish!=='0'){
									$.ajax({
										url : base_url + "module/master/penelitian/get_publish_data",
										type : "POST",
										data : $.param({
												kategori_publish : kat_publish,
												status_id : cek,
												penelitian : hidId
										}),
										dataType : "html",
										success:function(data) 
										{
											$("#publikasi_form").html(data);
											if(kat_publish=="jurnal"){
												$("#form-seminar").hide();
												$("#form-buku").hide();
												$("#form-jurnal").show();
											}
											else if(kat_publish=="seminar"){
												$("#form-jurnal").hide();
												$("#form-buku").hide();
												$("#form-seminar").show();
											}
											else if(kat_publish=="buku"){
												$("#form-jurnal").hide();
												$("#form-seminar").hide();
												$("#form-buku").show();
											}
											else{
												$("#form-jurnal").hide();
												$("#form-seminar").hide();
												$("#form-buku").hide();
											}
											$(".e7").select2();
											$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss', viewMode: 2});
										}
									});
								}
								else{
									$("#publikasi_form").empty();
								}
								
							});
						}
					}
				});
		}
		else{
			$(".form_progress").empty();
		}
	});
	
	$("#data-post").show();
	$("#change_select").hide();
	//INDEX
	$("select[name='fakultas_index']").change(function(){
		var fakultas = $(this).val();
		//alert(fakultas);
		$("select[name='tahunakademik_index']").val('0');
		$("select[name='tahunakademik_index']").select2("val","0");
		if(fakultas=='0'){
			$("select[name='tahunakademik_index']").attr("disabled",true);
			$("select[name='tahunakademik_index']").select2("enable",false);
		}
		else {
			$("select[name='tahunakademik_index']").removeAttr("disabled");
			$("select[name='tahunakademik_index']").select2("enable",true);
		}
		$("select[name='kategori_index']").val('0');
		$("select[name='kategori_index']").select2("val","0");
		$("select[name='kategori_index']").attr("disabled",true);
		$("select[name='kategori_index']").select2("enable",false);
		$("#data-post").hide();
		$("#change_select").show();
	});
	
	$("select[name='tahunakademik_index']").change(function(){
		var thn = $(this).val();
		$("select[name='kategori_index']").val('0');
		$("select[name='kategori_index']").select2("val","0");
		if(thn=='0'){
			$("select[name='kategori_index']").attr("disabled",true);
			$("select[name='kategori_index']").select2("enable",false);
		}
		else {
			$("select[name='kategori_index']").removeAttr("disabled");
			$("select[name='kategori_index']").select2("enable",true);
		}
		$("#data-post").hide();
		$("#change_select").show();
	});
	
	$("select[name='kategori_index']").change(function(){
		$("#data-post").show();
		$("#change_select").hide();
		$("#form-penelitian-index").submit();
	});
	
	
	
	$("#form-penelitian").submit(function(e){
		var fak 		= $("select[name='fakultas']").val();
		var cab 		= $("select[name='cabang']").val();
		var judul 		= $("input[name='judul']").val();
		var kategori 	= $("select[name='kategori']").val();
		var thn_akademik= $("select[name='thn_akademik']").val();
		var status	 	= $("select[name='status']").val();
		var keterangan 	= $("textarea[name='keterangan']").val();
		var sumber_dana = $("input[name='sumber_dana']").val();
		var dosen 		= $("input[name='dosen_id[]']").val();
		var mhs 		= $("input[name='mhs_id[]']").val();
		var ket_status	= $('option:selected', "select[name='status']").attr('name');
		if(ket_status=="Publikasi"){
			var kat_publikasi = $("select[name='kategori_publikasi']").val();
			var syarat_extra  = kat_publikasi!=='0';
		}else var syarat_extra  = 1==1;
		
		if(fak!=="0" && cab!=='0' && judul.trim()!=="" && kategori!=="0" && thn_akademik!=='0'){
			var postData = new FormData($(this)[0]);
			$.ajax({
				url : base_url + "module/master/penelitian/save_penelitian",
				type: "POST",
				data : postData,
				async: false,
				success:function(msg) 
				{
					alert ('Simpan berhasil');
					location.reload();
					//window.location.href = base_url + "module/master/penelitian"; 
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
				    alert ('Proses Gagal!');      
				    // location.reload(); 
				},
			    cache: false,
				contentType: false,
				processData: false
			});
		}
		else{
			alert("Silahkan lengkapi form terlebih dahulu!");
		}
		e.preventDefault();
		return false;
	});
	
	var id = $("input[name='id']").val();
	if(id!==""){
		i = $("input[name='count']").val();
		i = Number(i);
		var fakultas = $("select[name='fakultas']").val();
		$.ajax({
			url : base_url + "module/master/conf/dosen",
			type : "POST",
			data : $.param({
					fakultas_id : fakultas
			}),
			dataType : "json",
			success:function(data) 
			{
				$(".dosen").autocomplete({ 
					source: data,
					minLength: 0, 
					select: function(event, ui) { 
						$(this).val(ui.item.value);
						$(".dosen_id"+i).val(ui.item.id);
					} 
				});
			}
		});
		var fakultas_enc = $('option:selected', "select[name='fakultas']").attr('hidid');
		$.ajax({
			url : base_url + "module/master/conf/mhs",
			type : "POST",
			data : $.param({
					fakultas_id : fakultas_enc
			}),
			dataType : "json",
			success:function(data) 
			{
				$(".mhs").autocomplete({ 
					source: data,
					minLength: 0, 
					select: function(event, ui) { 
						$(this).val(ui.item.value);
						$(".mhs_id"+i).val(ui.item.nim);
					} 
				});
			}
		});
		
		$("input[name='dokumen_penelitian']").change(function(){
			$("#preview_file_penelitian").hide();
			$("input[name='file_loc_penelitian']").val("");
		});
		
		$("input[name='dokumen_progress']").change(function(){
			$("#preview_file_progress").hide();
			$("input[name='file_loc_progress']").val("");
		});
	};	
});

function remove_selected(id){
	$('#'+id).remove();
};

function deleted(urut,id_peserta){
	if(urut==i){
		i-=1;
	}
	if(id_peserta){
		var x = confirm('Apakah anda yakin ingin menghapus peserta ini?');
		if(x){
			$.ajax({
				url : base_url + "module/master/penelitian/del_peserta",
				type : "POST",
				data : $.param({
						pesertaid : id_peserta
				}),
				dataType : "html",
				success:function(data) 
				{
					alert(data);
					location.reload();
				}
			});
		}
	}
	else{
		$(".del"+urut).remove();
	}
	
}; 

function set_ketua(id){
	var radio_length = $('input:radio[name="isketua"]').length;
	for(k=1;k<=radio_length;k++){
		$(".is_ketua"+k).val('');
	}
	$(".is_ketua"+id).val('1');
};

function delete_penelitian(id){
	var x = confirm('Apakah anda yakin ingin menghapus penelitian ini?');
	if(x){
		$.ajax({
			url : base_url + "module/master/penelitian/hapus",
			type : "POST",
			data : $.param({
					id : id
			}),
			dataType : "html",
			success:function(data) 
			{
				alert(data);
				window.location.href = base_url + "module/master/penelitian";
			}
		});
	}
};
