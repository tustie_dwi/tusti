jQuery(".tagPemateri").tagsManager({
prefilled: $('.tmppemateri').val(),
	deleteTagsOnBackspace: true,
	preventSubmitOnEnter: true,
	typeahead: true,
	typeaheadAjaxSource: base_url + "/module/content/conf/staff",
	hiddenTagListName: 'hidPemateri'});
	
jQuery(".tagPanitia").tagsManager({
	prefilled: $('.tmppanitia').val(),
	preventSubmitOnEnter: true,
	typeahead: true,
	hiddenTagListName: 'hidPanitia'
});
jQuery(".tagUndangan").tagsManager({
	prefilled: $('.tmpundangan').val(),
		deleteTagsOnBackspace: true,
		preventSubmitOnEnter: true,
        typeahead: true,
        hiddenTagListName: 'hidUndangan'
});
jQuery(".tagStaff").tagsManager({
	prefilled: $('.tmpstaff').val(),	
	deleteTagsOnBackspace: true,	
	preventSubmitOnEnter: true,
	typeahead: true,
	hiddenTagListName: 'hidStaff'

});
jQuery(".tagMhs").tagsManager({
	prefilled: $('.tmpmhs').val(),
	deleteTagsOnBackspace: true,
	preventSubmitOnEnter: true,
	typeahead: true,   
	hiddenTagListName: 'hidMhs'
});

jQuery(".tagPeserta").tagsManager({
	deleteTagsOnBackspace: true,
	prefilled: $('.tmppeserta').val(),
	preventSubmitOnEnter: true,
	hiddenTagListName: 'hidPeserta'
});

jQuery(".tagAngkatan").tagsManager({
	deleteTagsOnBackspace: true,
	prefilled: $('.tmpangkatan').val(),
	preventSubmitOnEnter: true,
	hiddenTagListName: 'hidAngkatan'
});
	  
$(function() {
	
	$("#cmbmulti").select2();
	$(".form_datetime").datetimepicker({
		format: 'yyyy-mm-dd hh:ii', 
		showSecond: false
	  });
	$(".tagPemateri").autocomplete({ 
			source: base_url + "/module/akademik/conf/staff",
			minLength: 0, 
			select: function(event, ui) { 
				$('.tmppemateri').val(ui.item.id); 
				$('.tagPemateri').val(ui.item.value);
			} 
		});
		
	$(".tagPanitia").autocomplete({ 
		source: base_url + "/module/akademik/conf/staff",
		minLength: 0, 
		select: function(event, ui) { 
			$('.tmppanitia').val(ui.item.id); 
			$('.tagPanitia').val(ui.item.value);
		} 
	});
	
	$(".tagUndangan").autocomplete({ 
		source: base_url + "/module/akademik/conf/staff",
		minLength: 0, 
		select: function(event, ui) { 
			$('.tmpundangan').val(ui.item.id); 
			$('.tagUndangan').val(ui.item.value);
		} 
	});
	
	$(".tagStaff").autocomplete({ 
		source: base_url + "/module/akademik/conf/staff",
		minLength: 0, 
		select: function(event, ui) { 
			$('.tmpstaff').val(ui.item.id); 
			$('.tagStaff').val(ui.item.value);
		} 
	});
	
	$(".tagMhs").autocomplete({ 
		source: base_url + "/module/akademik/conf/mhs",
		minLength: 0, 
		select: function(event, ui) { 
			$('.tmpmhs').val(ui.item.id); 
			$('.tagMhs').val(ui.item.value);
		} 
	});
	
	
  });
	
	
