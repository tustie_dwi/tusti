<?php $this->head() ?>
<h2 class="title-page">Gallery PTIIK</h2>
<ol class="breadcrumb">
  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
  <li><a href="<?php echo $this->location('module/gallery/gallery'); ?>">Gallery</a></li>
</ol>

<div class="row">
  	<div class="col-sm-12">
		<div class="block-box">
			<div class="content">
				<div class="row">
					<div class="col-sm-12">
						<form id="search_gallery">
							<div class="form-group">
								<div class="input-group">
							      <input type="text" name="search" class="form-control" placeholder="Search.." />
							      <span class="input-group-btn">
							        <button class="btn btn-default" type="button" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;">Go!</button>
							      </span>
							    </div><!-- /input-group -->
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php if($posts){
						echo '<div class="header">Most Popular</div>';
							foreach($posts as $p){?>
								<div class="col-sm-3 col-xs-4">
						            <a class="thumbnail" href="<?php echo $p->file_data ?>" title="<?php echo ucwords($p->file_title) ?>">
						            	<div class="gallery-item"> 
											<div class="img-holder"> 
												<img class="img img-responsive img-gallery" src="<?php echo $this->config->file_url_view."/".$p->file_loc ?>"/> 
											</div> 
											<div class="name-holder"><?php echo ucwords($p->file_title) ?></div> 
										</div>
						            </a>
						        </div>
			        <?php 	} 
						  }?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
					<?php if($posts){
						echo '<div class="header">Langganan</div>';
							foreach($posts as $p){?>
								<div class="col-sm-3 col-xs-4">
						            <a class="thumbnail" href="<?php echo $p->file_data ?>" title="<?php echo ucwords($p->file_title) ?>">
						            	<div class="gallery-item"> 
											<div class="img-holder"> 
												<img class="img img-responsive img-gallery" src="<?php echo $this->config->file_url_view."/".$p->file_loc ?>"/> 
											</div> 
											<div class="name-holder"><?php echo ucwords($p->file_title) ?></div> 
										</div>
						            </a>
						        </div>
			        <?php 	} 
						  }?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		
	</div>
</div>
<?php $this->foot() ?>