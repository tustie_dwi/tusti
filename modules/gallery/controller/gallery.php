<?php
class gallery_gallery extends comsmodule {
	private $coms;
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index(){
		$mgallery = new model_gallery();
		
		$data['lang'] 	= $this->config->lang;//'EN';
		$data['posts']	= $mgallery->read($data['lang']);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		
		$this->view('index.php', $data);
	}
}
?>