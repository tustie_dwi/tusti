$(document).ready(function(){   	
    $("#select_unit").change(function(){
        var id_unit_ = $(this).val();
        var nim_ = $("#mhsid").val();
        var uri = $(this).attr("uri_");
         $.ajax({
             type: "POST",
             dataType: "html",
             url: uri ,
             data: $.param({id_unit:id_unit_,nim : nim_}),
             success: function(msg){
                 if(msg == ''){
                     $("span#nilai_list").html('');                         
                 }
                 else
                 {
                     $("span#nilai_list").removeAttr("hidden");
					 $("span#nilai_list").html(msg);                                                     
                 }                                                                     
             }
         }); 
     });
     
     $("#select_unit_skripsi").change(function(){
        var id_unit_ = $(this).val();
        var skripsi_id_ = $("#skripsi").val();
        var uri = $(this).attr("uri_");
         $.ajax({
             type: "POST",
             dataType: "html",
             url: uri ,
             data: $.param({id_unit:id_unit_, id_skripsi : skripsi_id_}),
             success: function(msg){
                 if(msg == ''){
                     $("span#nilai_list").html('');                         
                 }
                 else
                 {
                     $("span#nilai_list").removeAttr("hidden");
					 $("span#nilai_list").html(msg);                                                     
                 }                                                                     
             }
         }); 
     });
});