$(document).ready(function() {
		
		$("#namamkpengampu").autocomplete({ 
			source: base_url + "/module/akademik/conf/namamk_from_mkditawarkan_by_pengampu",
			minLength: 0, 
			select: function(event, ui) { 
				$('#namamk').val(ui.item.id); 
				$('#namamkpengampu').val(ui.item.value);
				// $('#frmDosen').submit(); 
			} 
		});
		
		$("#submit-edit").click(function(e){
		  var postData = $('#form-editpengampu').serializeArray();
          $.ajax({
            url : base_url + "module/akademik/pengampu/saveToDBpengampu",
	        type: "POST",
	        data : postData,
	        success:function(data, textStatus, jqXHR) 
	        {
	        	if(data=='exist'){
	        		alert ('Sudah terdapat koordinator untuk matakuliah ini');
	        		location.reload();
	        	}else{
		            alert ('Upload Success!');
		            location.href= base_url + "module/akademik/pengampu/index";
	           }
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        }
	      });
	    e.preventDefault(); //STOP default action
	});					
});