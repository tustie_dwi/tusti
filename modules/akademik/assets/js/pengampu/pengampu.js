$(document).ready(function() {
	jQuery("#addnamamk").tagsManager();
	jQuery("#addnamamk-id").tagsManager();
		
	var x = 0;
	
	$('#select_cabang').change(function() {
		var cbg_id = $(this).val();
		if(cbg_id!=0)
		{
			$("#addnamamk").removeAttr("disabled");
			//----------------ajax---------------------------------------
			var uri = base_url + "/module/akademik/conf/namamk_from_mkditawarkan_by_pengampu";
			$.ajax({
				type : "POST",
				dataType : "json",
				url : uri,
				data : $.param({
					cbg_id : cbg_id
				}),
				success : function(msg) {
					$("#addnamamk").autocomplete({ 
						source: msg,
						minLength: 0, 
						select: function(event, ui) { 
							// $('input:text[name="namamkid['+x+']"]').val(ui.item.id); 
							$('input:text[name="namamkid[]"]').val(ui.item.id); 
							$('#addnamamk').val(ui.item.value);
							// $('#frmDosen').submit(); 
						} 
					});
				}
			});
			//----------------ajax---------------------------------------
		}
		else
		{
			$("#addnamamk").attr("disabled", true);
		}
	});
	
	
	$("#submit-mkpengampu").click(function(e){
	  var postData = $('#form-addmk').serializeArray();
      $.ajax({
        url : base_url + "module/akademik/pengampu/saveToDBmkpengampu",
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            alert ('Upload Success!');
            location.reload();  
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!');      
        }
      });
    e.preventDefault(); //STOP default action
	});			

});