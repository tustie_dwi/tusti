$(document).ready(function() {
	$(".e9").select2();
	$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss', showSecond:true});
	$("#soal-soal p").attr('style','margin: 0px 0px -22px 0px; width: 850px;');
	$(".editsoal").attr('style','margin: 0px 0px 0px 840px;');
	// $(".form_datetime").datetimepicker({timeFormat: 'hh:mm:ss', dateFormat: 'yyyy-mm-dd', showSecond:true});
	
	
});

function save(i){
	var publish = i;
	$('#form').submit(function (e) {
		var judul = $('#judul').val().length;
		var instruksi = $('#instruksi').val().length;
		var keterangan = $('#keterangan').val().length;
		var tanggalmulai = $('#tanggalmulai').val().length;
		var tanggalselesai = $('#tanggalselesai').val().length;
		
		if(judul > 0 && instruksi > 0 && keterangan > 0 && tanggalmulai > 0 && tanggalselesai > 0){		
			var formData = new FormData($(this)[0]);
	   	
		   	if(publish=='publish'){
		   		formData.append("b_savepublish", "1");
		   	}else{
		   		formData.append("b_draft", "1");
		   	}
    	var URL = base_url + "module/akademik/test/saveToDB";
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Success!');
	            window.location.href = base_url + "module/akademik/test/soal/write/"+data;
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
		}
		else{
		  alert("Please fill the form");
		}
	});
};

function doDelete(i) {
  	var x = confirm("Are you sure you want to delete?");
 	if (x){
		var del_id = i;
		var url = base_url + "module/akademik/test/deletesoal";

  		//alert(del_id);
	  	$.ajax({
				type : "POST",
				dataType : "html",
				url : url,
				data : $.param({
					delete_id : del_id
				}),
				success : function(msg) {
					if (msg) {
						alert("Data Berhasil Terhapus!");
					}else {
						alert('gagal');
					}
					location.reload();
				}
		});
  }
  else {
  	location.reload();
  }
  	
};

function validate_tanggal(){
	var tgl_mulai_ = $('#tanggalmulai').val();
	var tgl_selesai_ = $('#tanggalselesai').val();

	// var tgl_mulai = tgl_mulai_.substr(0, 10);
	// var tgl_selesai = tgl_selesai_.substr(0, 10);
	
	if(tgl_mulai_ > tgl_selesai_) {
		alert('Tanggal Mulai harus lebih kecil dari tanggal akhir.');
		$('#tanggalselesai').val('');
	}else if(tgl_selesai_=='') {
		$('#tanggalselesai').val('');
	}
	else {
		
	}
};
