$(document).ready(function() {
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var uri = $(this).attr("uri_");
		//alert(fakultas_id_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					//$("#select_thn").html(msg);
					$("#select_prodi").attr("disabled", true);
					$("#addnamamk").attr("disabled", true);
				} else {
					$("#select_prodi").removeAttr("disabled");
					$("#addnamamk").removeAttr("disabled");
					$("#select_prodi").html('<option value="0">Select Prodi</option>');
					$("#select_prodi").html(msg);
				}
			}
		});
		
		$.ajax({
			  type : "POST",
			  dataType: "json",
               url: base_url + "/module/akademik/conf/namamkfrommkditawarkan",
               data : $.param({
				fakultasid : fakultas_id_
			}),
               success: function(data){
                     $('#addnamamk').autocomplete(
                     {
                           source: data,
                           minLength: 0,
                           select: function(event, ui) { 
							$('#addnamamk').val(ui.item.value);
							}    
                     });
               }
          });
		
	});
	
	$(function () {
		$("#ruang").autocomplete({ 
			source: base_url + "/module/akademik/conf/ruang",
			minLength: 0, 
			select: function(event, ui) { 
				$('#ruang').val(ui.item.value);
			} 
		});
		
		$("#dosen").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosen-id').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
			} 
		});
							
	});
	$(".e9").select2();
});