$(document).ready(function() {
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var uri = $(this).attr("uri_");
		$.ajax({
			  type : "POST",
			  dataType: "json",
              url: uri,
              data : $.param({
				fakultasid : fakultas_id_
			}),
               success: function(data){
               		 $("#namamk").removeAttr("disabled");
                     $('#namamk').autocomplete(
                     {
                           source: data,
                           minLength: 0,
                           select: function(event, ui) { 
							$('#namamk').val(ui.item.value);
							}    
                     });
                     //------------------------------------------ kalo pake ini ubah dulu di config
                     // $(function () {
						// $('#namamk').typeahead([
							// {
								// name: 'namamk',
								// local: data
							// }
						// ]);
					// });
					  //------------------------------------------
               }
          });  
		
	});
	
	$(".e9").select2();
});
