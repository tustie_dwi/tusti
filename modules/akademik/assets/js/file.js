$(document).ready(function() {
	$("#select_jenis").change(function() {
		var jenis_ = $(this).val();
		var uri = $(this).data("uri");
		$("#select_materi").select2('val', "0");
		$("#select_kategori").select2('val', "0");
		$("#display").empty();
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				jenis : jenis_
			}),
			success : function(msg) {
				if (msg == '') {
					//$("#display").html(' <div class="span3" align="center" style="margin-top:20px;"><div class="well">Sorry, no content to show</div></div>');
					$("#select_kategori").attr("disabled","disabled");
					$("#select_kategori").select2("enable", false);
					//alert(uri);
				} else {
					//alert(msg);
					//$("#select_thn").removeAttr("disabled");
					$("#select_kategori").html(msg);
					$("#select_kategori").removeAttr("disabled");
					$("#select_kategori").select2("enable", true);
					if(jenis_ == 1){
						$("#tes").hide();
					}
					else{
						$("#tes").show();
					}
				}
			}
		});
	});
	
	$("#select_kategori").change(function() {
		var jenis_ = $("#select_jenis").val();
		var jenisfile_id_ = $(this).val();
		var uri = $(this).data("uri");
		//alert(jenisfile_id_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				jenis_file_id : jenisfile_id_,
				jenis : jenis_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_materi").attr("disabled","disabled");
					$("#select_kategori").select2("enable", false);
					if(jenis_ == 1){
						$("#display").html(' <br><br><br><br><br><div class="span3" align="center" style="margin-top:20px;"><div class="well">Sorry, no content to show</div></div>');
					}//alert(uri);
				}
				 else {
					//alert(uri);
					$("#select_materi").html(msg);
					$("#select_materi").removeAttr("disabled");
					$("#select_materi").select2("enable", true);
					if(jenis_ == 1){
						$("#tes").hide();
						$("#display").html(msg);
					}
					else{
						$("#tes").show();
					}
				}
			}
		});
	});
	
	$("#select_materi").change(function() {
		//alert("tes");
		//$("#display").html("msg");
		var jenis_ = $("#select_jenis").val();
		var jenisfile_id_ = $("#select_kategori").val();
		var materi_id_ = $(this).val();
		var uri = $(this).data("uri");
		//alert(jenis_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				jenis : jenis_,
				materi_id : materi_id_,
				jenis_file_id : jenisfile_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#display").html(' <br><br><br><br><br><div class="span3" align="center" style="margin-top:20px;"><div class="well">Sorry, no content to show</div></div>');					
					//$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					// // $("#select_mk").attr("disabled","disabled");
				} else {
					// alert(uri);
					$("#display").html(msg);
				}
			}
		});
	});
	
	$(".e9").select2();
}); 