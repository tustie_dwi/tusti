$(function() {
		$("#mahasiswa").autocomplete({ 
			source: base_url + "/module/akademik/krs/mhs",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mahasiswa_id').val(ui.item.id); 
				$('#mahasiswa').val(ui.item.value);
				// $('#frmDosen').submit(); 
			} 
		});
		
		$("#matakuliah").autocomplete({ 
			source: base_url + "/module/akademik/krs/mk",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mkditawarkan_id').val(ui.item.id); 
				$('#matakuliah').val(ui.item.value);
				// $('#frmDosen').submit(); 
			} 
		});
		  				
    });
    

$(document).ready(function() {

	$("#select_tahun").change(function() {
		var tahun_akademik_ = $(this).val();
		var uri = $(this).data("uri");
		
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				tahun_akademik : tahun_akademik_
			}),
			success : function(msg) {
				if (msg == '') {
					$("select#select_parent").html('<option value="0">Pilih Mata Kuliah</option>');
					$("select#select_parent").attr("disabled","disabled");
				} else {
					$("select#select_parent").removeAttr("disabled");
					$("select#select_parent").html(msg);
				}
			}
		});
		
		// $.ajax({
			  // type : "POST",
			  // dataType: "json",
               // url: base_url + "/module/akademik/krs/mk",
               // data : $.param({
				// tahun_akademik : tahun_akademik_
			// }),
               // success: function(data){
               	// //alert(data);
               	// if (data == '') {
               		 // $('#matakuliah').attr("disabled","disabled");
               	// }else{
               		// //alert(data);
               		// $("#matakuliah").removeAttr("disabled");
                     // $('#matakuliah').autocomplete(
                     // {
                           // source: data,
                           // minLength: 0,
                           // select: function(event, ui) { 
							// $('#mkditawarkan_id').val(ui.item.id); 
							// $('#matakuliah').val(ui.item.value);
							// }    
                     // });
                 // }
//                
               // }
          // });  
	});
	
	$(".e9").select2();
}); 