$(document).ready(function(){
	$("#button-setting").click(function(){
		var tahunid_ = $("#form-setting [name='tahunid']").val();
		var tahun_ = $("#form-setting [name='tahun']").val();
		var isganjil_ = $("#form-setting [name='isganjil']").val();
		var uri_ = $("#uri").val();
		
		if(document.getElementById("ispendek").checked){
			var ispendek_ = 'Pendek';
		}
		else{
			var ispendek_ = 'Panjang';
		}
		
		var kalender_ = $("#form-setting [name='kalender[]']").val();
		var kegiatan_ = $("#form-setting [name='kegiatan[]']").val();
		var tglselesai_ = $("#form-setting [name='tglselesai[]']").val();
		
		var tglmulai_ = new Array();
		$(".tglmulai").each(function(){
			tglmulai_.push($(this).val());
		});
		
		var tglselesai_ = new Array();
		$(".tglselesai").each(function(){
			tglselesai_.push($(this).val());
		});
		
		var kalender_ = new Array();
		$(".kalender").each(function(){
			kalender_.push($(this).val());
		});
		
		var kegiatan_ = new Array();
		$(".kegiatan").each(function(){
			kegiatan_.push($(this).val());
		});
		
		$.ajax({
			type	 : 'POST',
			dataType : 'html',
			url		: uri_,
			data	: $.param({
						tahunid : tahunid_ , 
						tahun : tahun_ , 
						isganjil : isganjil_ , 
						ispendek : ispendek_ , 
						tglmulai : tglmulai_, 
						tglselesai : tglselesai_,
						kalender : kalender_,
						kegiatan : kegiatan_
					}),
			success:function(msg){
				$("#notif").html("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>OK, data telah diupdate.</div>");
			}
		});
		
	});
});
