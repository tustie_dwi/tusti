$(document).ready(function(){

	$(".radio-link").click(function(){
		var mhsid = $("#mhsid").val();
		var soalid = $(this).data("soal");	
		var jawabanid = $(this).val();
		
		document.cookie = mhsid + "["+ soalid +"]" + "=1|" + soalid + "|" + jawabanid;
	});
	
	$(".checkbox-link").click(function(){
		var mhsid = $("#mhsid").val();
		var soalid = $(this).data("soal");	
		var jawabanid = '';
		
		$('[name^="pilihan'+soalid+'"]').map(function(){
			if($(this).prop('checked') == true){
				jawabanid = jawabanid + $(this).val() + "-";
			}
		});
		document.cookie = mhsid + "["+ soalid +"]" + "=2|" + soalid + "|" + jawabanid;
	});
	
	$(".textarea-link").keyup(function(){
		var mhsid = $("#mhsid").val();
		var soalid = $(this).data("soal");
		var nilai = $(this).val();
		
		document.cookie = mhsid + "["+ soalid +"]" + "=3|" + soalid + "|" + nilai;
	});
	
	$("#jawab-btn").click(function(){
		var uri = $("#uri").val();
		
		var test_id_ = $("#form-jawab [name='test_id']").val();
		var hasil_id_ = $("#form-jawab [name='hasil_id']").val();
		var jawaban_ = Array();
				
		$('[class^="jawab_"]').map(function(){
			if( $(this).val() != ''){
				if($(this).data('tipe') == 'choice'){
					if($(this).prop('checked') == true){
						var jawaban_text = '1|' + $(this).data('soal') + '|' + $(this).val();
						jawaban_.push(jawaban_text);
					}
				}
				else{
					var jawaban_text = '0|' + $(this).data('soal') + '|' + $(this).val();
					jawaban_.push(jawaban_text);
				}
			}
		});
		
	});
	
	$("#jawab-btn").click(function(){
		if(confirm('Apakah sudah yakin telah menyelesaikan test dan ingin mengirim jawaban Anda ?')){
			var ca = document.cookie.split(';');
			var mhsid = $("#mhsid").val();
			var jawaban = '';
			
			for(var i=0; i<ca.length; i++){
		      name = ca[i].split('=')[0];
		      value = ca[i].split('=')[1];
		      if(name.indexOf(mhsid) > -1){
		      	jawaban = jawaban + value + ",";
		      	document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
		      }
		    }
		   	
		   	$("#jawaban-wrap").val(jawaban);
		   document.getElementById("form-jawab").submit();
	  }
	});
	
	$("#hasil-link").click(function(){
		document.getElementById("hasil-form").submit();
	});
    
    $('#myTab li:eq(1) a').tab('show');
    
    $("#next").click(function(){
    	var page = $("#page").val();
    	var limit = $("#limit").val();
    	page = parseInt(page)+1;
    	
    	if(page <= limit){
	    	$('#myTab li:eq('+page+') a').tab('show');
	    	$("#page").val(page);
    	}
    });
    
    $("#prev").click(function(){
    	var page = $("#page").val();
    	var limit = $("#limit").val();
    	page = parseInt(page)-1;
    	
    	if(page >= 1){
	    	$('#myTab li:eq('+page+') a').tab('show');
	    	$("#page").val(page);
    	}
    });
});
