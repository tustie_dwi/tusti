$(document).ready(function() {
	$(".e9").select2();
	$(".form_datetime").datetimepicker({
										format: 'yyyy-mm-dd hh:ii:ss', 
										showSecond: true
									  });
	$("#select_jadwal").change(function (){
		var jadwal_id_ = $("#select_jadwal").val();
		var uri = $("#select_jadwal").data("uri");
		//alert(jadwal_id);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				jadwal_id : jadwal_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_materi").html('<option value="0">Select Materi</option>');
					$("#select_materi").attr("disabled","disabled");
				} else {
					$("#select_materi").removeAttr("disabled");
					$("#select_materi").html(msg);
					$("#select_materi").select2("enable", true);
					
				}
			}
		});
	});
});

function validate_tanggal(){
	var tgl_mulai_ = $('#tanggalmulai').val();
	var tgl_selesai_ = $('#tanggalselesai').val();

	var tgl_mulai = tgl_mulai_.substr(0, 10);
	var tgl_selesai = tgl_selesai_.substr(0, 10);
	
	if(new Date(tgl_mulai_) > new Date(tgl_selesai_)) {
		alert('Tanggal Mulai harus lebih kecil dari tanggal akhir.');
		$('#tanggalselesai').val('');
	}else if(tgl_selesai_=='') {
		$('#tanggalselesai').val('');
	}
	else {
		
	}
};

function submit_tugas(){
    $('#upload-tugas-form').submit(function (e) {
    	//var posdata = $(this).serializeArray();
    	var jadwal_id_ = $("#select_jadwal").val().length;
    	var materi_id_ = $("#select_materi").val().length;
    	if(jadwal_id_ > 1 && materi_id_ > 1){
	    	var formData = new FormData($(this)[0]);
	    	var URL = base_url + 'module/akademik/tugas/save';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        dataType : "HTML",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		        },
		        error: function(msg) 
		        {
		            alert (msg);      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
		}
		else {
			alert("Jadwal dan Materi tidak boleh kosong!");
			location.reload();
		}
	});
	location.reload();
};