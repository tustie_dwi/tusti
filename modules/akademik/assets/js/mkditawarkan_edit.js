$(document).ready(function() {
		var fkid			= document.getElementById("select_fakultas");
		var fakultas_id_	= $(fkid).val();

		//----------------ajax is blok---------------------------------------
		var uri2 = base_url + "/module/akademik/mkditawarkan/blok_mk";
		//alert(uri2);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri2,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_mk").attr("disabled", true);
					$("#form").fadeOut();
					$("#isblok").attr("checked", false);
					
				} else {
					$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
				}
			}
		});
});