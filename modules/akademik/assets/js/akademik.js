$(function() {
	$(".cmbmk").select2();	
		$(".cmbdosen").select2();	
		$(".cmbmulti").select2();	
		$("#cmbmulti").select2();	
		$("#cmbmk").select2();	
		
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
		
        $( ".date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
		
		$(".btn-delete-new").click(function(){
		
			var pid = $(this).data("id");
			var mod	= $(this).parents('li').data("id");
			
			if(confirm("Delete this post? Once done, this action can not be undone.")) {
			
			var row = $(this).parents('li');
					
			$.post(
				base_url + 'module/akademik/generate/deleteujiannew/'+pid,
				function(data){
					if(data.status.trim() == "OK")
						row.fadeOut();
					else alert(data.error);
				},
				"json"
				).error(function(xhr) {
					alert(xhr.responseText);
				});
				
			}
		});
	
		
		$("#mhs").autocomplete({ 
			source: base_url + "/module/akademik/conf/mhs",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mhsid').val(ui.item.id); 
				$('#mhs').val(ui.item.value);
			} 
		});  
		
		$("#mk").autocomplete({ 
			source: base_url + "/module/akademik/conf/mk",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mkid').val(ui.item.id); 
				$('#mk').val(ui.item.value);
			} 
		});  
		
		$("#mkditawarkan").autocomplete({ 
			source: base_url + "/module/akademik/conf/mkditawarkan/1",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mkditawarkanid').val(ui.item.id); 
				$('#mkditawarkan').val(ui.item.value);
			} 
		});  
		
		$("#namamk").autocomplete({ 
			source: base_url + "/module/akademik/conf/namamk",
			minLength: 0, 
			select: function(event, ui) { 
				$('#namamkid').val(ui.item.id); 
				$('#namamk').val(ui.item.value);
			} 
		});  
		
		$("#dosen1").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenid1').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
			} 
		});  
		
		$("#dosen2").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenid2').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
			} 
		});  
		
		$("#dosen").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenid').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
				$('#frmDosen').submit(); 
			} 
		});  				
		
		$("#pengampu").autocomplete({
				
                minLength: 0,
				source: base_url + "/module/akademik/conf/dosen",
				minLength: 0, 
				select: function(event, ui) {
					var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( ", " );                 
					
					$('#pengampuid').val(ui.item.id).split(/,\s*/); 
					$('#pengampu').val(ui.item.value).split(/,\s*/);
					return false;
				} 
			});  
		
		$("#demo-input-prevent-duplicates").tokenInput( base_url + "/module/akademik/conf/pengampu", {
                preventDuplicates: true
            });
			
		$("#token-krs").tokenInput( base_url + "/module/akademik/conf/mkditawarkan/-", {
			preventDuplicates: true
		});
		
		 $(".demo-hari").tokenInput( base_url + "/module/akademik/conf/hari");
		 
			
		$(".hari").autocomplete({ 
			source: base_url + "/module/akademik/conf/hari",
			minLength: 0, 
			select: function(event, ui) { 
				$('#hariid').val(ui.item.id); 
				$('#hari').val(ui.item.value);
			} 
		});  
			
		$(".kelas").autocomplete({ 
			source: base_url + "/module/akademik/conf/kelas",
			minLength: 0, 
			select: function(event, ui) { 
				$('#kelasid').val(ui.item.id); 
				$('#kelas').val(ui.item.value);
			} 
		});  
		
		$(".ruang").autocomplete({ 
			source: base_url + "/module/akademik/conf/ruang",
			minLength: 0, 
			select: function(event, ui) { 
				$('#ruangid').val(ui.item.id); 
				$('#ruang').val(ui.item.value);
			} 
		});  
		
		$(".jammulai").autocomplete({ 
			source: base_url + "/module/akademik/conf/jammulai",
			minLength: 0, 
			select: function(event, ui) { 
				$('#jammulaiid').val(ui.item.id); 
				$('#jammulai').val(ui.item.value);
			} 
		});  
		
		$(".jamselesai").autocomplete({ 
			source: base_url + "/module/akademik/conf/jamselesai",
			minLength: 0, 
			select: function(event, ui) { 
				$('#jamselesaiid').val(ui.item.id); 
				$('#jamselesai').val(ui.item.value);
			} 
		});  
		
		$(".prodi").autocomplete({ 
			source: base_url + "/module/akademik/conf/prodi",
			minLength: 0, 
			select: function(event, ui) { 
				$('#prodiid').val(ui.item.id); 
				$('#prodi').val(ui.item.value);
			} 
		});  	
		
		
		$("#tabs").tabs();
		$("#accordion").accordion({autoHeight: true,fillSpace: false});
		$("#tabs-child").tabs();
		$('#example').dataTable();		
		$(".typeahead").typeahead();
    });