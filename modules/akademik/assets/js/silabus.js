$(document).ready(function() {
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var uri = $(this).data("uri");
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_thn").html('<option value="0">Select Tahun Akademik</option>');
					$("#select_thn").attr("disabled","disabled");
					$("#select_thn").select2("enable", false);
				} else {
					//alert(uri);
					$("#select_thn").removeAttr("disabled");
					$("#select_thn").html(msg);
					$("#select_thn").select2("enable", true);
				}
			}
		});
	});
	
	$("#select_thn").change(function() {
		var fakultas_id_ = $("#select_fakultas").val();
		var thn_id_ = $(this).val();
		var uri = $(this).data("uri");
		// alert (fakultas_id_);
		// alert (thn_id_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				hid_id : fakultas_id_,
				thn_id : thn_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					$("#select_mk").attr("disabled","disabled");
					$("#select_mk").select2("enable", false);
				} else {
					
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
					$("#select_mk").select2("enable", true);
				}
			}
		});
	});
	
	
	
	$(".e9").select2();
}); 