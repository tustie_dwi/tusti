$(document).ready(function() {
	
	var fkid			= document.getElementById("hidfakultasid");
	var fakultas_id_	= $(fkid).val();
	
	var pdid			= document.getElementById("hidprodi");
	var prodi_id_		= $(pdid).val();
	$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + 'module/akademik/jadwalmk/tampilkan_prodi',
			data : $.param({
				fakultas_id : fakultas_id_,
				prodi_id : prodi_id_
			}),
			success : function(msg) {
				if (msg == '') {
					alert("tes");
				} else {
					$("#select_prodi").html(msg);
				}
			}
	});
	
	
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var uri = $(this).attr("uri_");
		//alert(fakultas_id_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					//$("#select_thn").html(msg);
					alert("tes");
				} else {
					$("#select_prodi").removeAttr("disabled");
					//$("#addnamamk").removeAttr("disabled");
					$("#select_prodi").html('<option value="0">Select Prodi</option>');
					$("#select_prodi").html(msg);
				}
			}
		});
		
		//----------------ajax dosen pengampu---------------------------------------
		var uri2 = base_url + "/module/akademik/jadwalmk/get_dosenbyfakultas";
		//alert(uri2);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri2,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					//$("#select_thn").html(msg);
					alert("tes");
				} else {
					$("#select_pengampu").html('<option value="0">Select Dosen Pengampu</option>');
					$("#select_pengampu").removeAttr("disabled");
					$("#select_pengampu").html(msg);
				}
			}
		});
		//----------------ajax dosen pengampu---------------------------------------
		
	});
	
	$(function () {
		$("#ruang").autocomplete({ 
			source: base_url + "/module/akademik/conf/ruang",
			minLength: 0, 
			select: function(event, ui) { 
				//$('#namamk_id').val(ui.item.id); 
				$('#ruang').val(ui.item.value);
				// $('#frmDosen').submit(); 
			} 
		});
		
		// $("#dosen").autocomplete({ 
			// source: base_url + "/module/akademik/conf/dosen",
			// minLength: 0, 
			// select: function(event, ui) { 
				// $('#dosen-id').val(ui.item.id); 
				// $('#dosen').val(ui.item.value);
				// // $('#frmDosen').submit(); 
			// } 
		// });
							
	});
	
	$(".e9").select2();
});