$(document).ready(function() {
	$('#parameter').change(function() {
		var thnakademik		= document.getElementById("select_thnakademik");
		var thnakademikid	= $(thnakademik).val();
		
		var fakultas		= document.getElementById("pilih_fakultas");
		var fakultasid	    = $(fakultas).val();
		//----------gunakan di bawah kalo post lewat javascript-------------------------
		/*$("#param").submit(function(e)
		{
			$.ajax({
				type : "POST",
				url : base_url + "module/akademik/mkditawarkan/index",
				data : thnakademikid,
				success:function(data, textStatus, jqXHR) 
				{
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
				}
			});
		});*/
		//--------------------------------------------------------------------------------
		
		// $("#param").submit(); //SUBMIT FORM
	});	
	
	/*$('#parameter').change(function() {
		var thnakademik		= document.getElementById("select_thnakademik");
		var thnakademikid	= $(thnakademik).val();
		
		$.ajax({
				type : "POST",
				dataType : "html",
				url : base_url + "module/akademik/mkditawarkan/show_by_parameter",
				data : $.param({
					thnakademik : thnakademikid
				}),
				success : function(msg) {
					if (msg == '') {
						$("#content").html("<tr><td>Sorry, no content to show</td><td></td><td></td><td></td><td></td></tr>");
					} else {
						$("#content").html(msg);
						//window.location.href=base_url + "module/akademik/mkditawarkan/show_by_parameter";
						//window.onload=msg;
					}
				}
			});
	});*/
	
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var uri = $(this).attr("uri_");
		//alert(fakultas_id_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_thn").html(msg);
				} else {
					//$("#select_thn").html('<option value="0">Select Tahun Akademik</option>');
					$("#select_thn").removeAttr("disabled");
					//$("#mk").html(msg);
					$("#namamk").removeAttr("disabled");
					//---------------------function--------------------------------
									
					// $(function () {
						// $('#namamk').typeahead([
							// {
								// name: 'namamk',
								// prefetch: base_url + "/module/akademik/conf/namamkfrommatakuliah"
							// }
						// ]);
					// });
					//---------------------function--------------------------------
					
				}
			}
		});
		
		//----------------ajax is blok---------------------------------------
		var uri2 = base_url + "/module/akademik/mkditawarkan/blok_mk";
		//alert(uri2);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri2,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_mk").attr("disabled", true);
					$("#form").fadeOut();
					$("#isblok").attr("checked", false);
					
				} else {
					$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
				}
			}
		});
		//----------------ajax is namamk---------------------------------------
		
				$.ajax({
			  type : "POST",
			  dataType: "json",
               url: base_url + "/module/akademik/conf/namamkfrommatakuliah",
               data : $.param({
				fakultasid : fakultas_id_
			}),
               success: function(data){
               	//alert(data);
                     $('#namamk').autocomplete(
                     {
                           source: data,
                           minLength: 0,
                           select: function(event, ui) { 
							$('#namamk').val(ui.item.value);
							}    
                     });
                     //------------------------------------------ kalo pake ini ubah dulu di config
                    /* $(function () {
						$('#namamk').typeahead([
							{
								name: 'namamk',
								local: data
							}
						]);
					});*/
					  //------------------------------------------
               }
          });  
		
		
	});
	
	
	
	jQuery("#dosenpengampu").tagsManager();
	
	
	$(function () {
		
		$("#dosenkoor").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				//$('#namamk_id').val(ui.item.id); 
				$('#dosenkoor').val(ui.item.value);
				// $('#frmDosen').submit(); 
			} 
		});
		
		$("#dosenpengampu").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				//$('#namamk_id').val(ui.item.id); 
				$('#dosenpengampu').val(ui.item.value);
				// $('#frmDosen').submit(); 
			} 
		});
						
	});
	
	$(".e9").select2();
	
});