$(function() {
	//$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	
	$('#writeTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});
	
	/* $(".bdate").datetimepicker({
		format: 'yyyy-mm-dd', 
		 autoclose: true,
		todayBtn: true
	});*/
	//alert(base_url);
		 function split( val ) {
			return val.split( /,\s*/ );
		}

		function extractLast( term ) {
			return split( term ).pop();
		}
		
		
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
		
        $( ".date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true
		});
		
		$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
		
		$('.time').timepicker();
		
		$("#mhs").autocomplete({ 
			source: base_url + "/module/skripsi/conf/mhs",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mhsid').val(ui.item.id); 
				$('#mhs').val(ui.item.value);
			} 
		});  
		
		$("#mhsskripsi").autocomplete({ 
			source: base_url + "/module/skripsi/conf/mhs",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mhsid').val(ui.item.id); 
				$('#mhs').val(ui.item.value);
				$('#frmSkripsi').submit();
			} 
		});  
		
		
		$("#mk").autocomplete({ 
			source: base_url + "/module/skripsi/conf/mk",
			minLength: 0, 
			select: function(event, ui) { 
				$('#mkid').val(ui.item.id); 
				$('#mk').val(ui.item.value);
			} 
		});  
		
		$("#namamk").autocomplete({ 
			source: base_url + "/module/skripsi/conf/namamk",
			minLength: 0, 
			select: function(event, ui) { 
				$('#namamkid').val(ui.item.id); 
				$('#namamk').val(ui.item.value);
			} 
		});  
		
		$("#dosen").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenid').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
				$('#frmDosen').submit(); 
			} 
		});
		
		$("#dosen1").autocomplete({ 
			source: base_url + "/module/skripsi/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenid1').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
			} 
		});  
		
		$("#dosen2").autocomplete({ 
			source: base_url + "/module/skripsi/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenid2').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
			} 
		});  
		
		  
		
		$(".pembimbing").autocomplete({ 
			source: base_url + "/module/skripsi/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('.pembimbingid').val(ui.item.id); 
				$('.dosen').val(ui.item.value);
			} 
		});  
		
		$(".penguji").autocomplete({ 
			source: base_url + "/module/skripsi/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('.pengujiid').val(ui.item.id); 
				$('.dosen').val(ui.item.value);
			} 
		});  
		
		
		
		$("#dosen").autocomplete({ 
			source: base_url + "/module/skripsi/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenid').val(ui.item.id); 
				$('#dosen').val(ui.item.value);
				$('#frmDosen').submit(); 
			} 
		});  				
		
		$("#pengampu").autocomplete({
				
                minLength: 0,
				source: base_url + "/module/skripsi/conf/dosen",
				minLength: 0, 
				select: function(event, ui) {
					var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( ", " );                 
					
					$('#pengampuid').val(ui.item.id).split(/,\s*/); 
					$('#pengampu').val(ui.item.value).split(/,\s*/);
					return false;
				} 
			});  
		
		$("#demo-input-prevent-duplicates").tokenInput( base_url + "/module/skripsi/conf/pengampu", {
                preventDuplicates: true
            });
			
		$("#demo-bidangminat").tokenInput( base_url + "/module/skripsi/conf/bidangminat", {
			preventDuplicates: true
		});
		
		 $(".demo-hari").tokenInput( base_url + "/module/skripsi/conf/hari");
		 
			
		$(".hari").autocomplete({ 
			source: base_url + "/module/skripsi/conf/hari",
			minLength: 0, 
			select: function(event, ui) { 
				$('#hariid').val(ui.item.id); 
				$('#hari').val(ui.item.value);
			} 
		});  
			
		$(".kelas").autocomplete({ 
			source: base_url + "/module/skripsi/conf/kelas",
			minLength: 0, 
			select: function(event, ui) { 
				$('#kelasid').val(ui.item.id); 
				$('#kelas').val(ui.item.value);
			} 
		});  
		
		$(".ruang").autocomplete({ 
			source: base_url + "/module/skripsi/conf/ruang",
			minLength: 0, 
			select: function(event, ui) { 
				$('#ruangid').val(ui.item.id); 
				$('#ruang').val(ui.item.value);
			} 
		});  
		
		$(".jammulai").autocomplete({ 
			source: base_url + "/module/skripsi/conf/jammulai",
			minLength: 0, 
			select: function(event, ui) { 
				$('#jammulaiid').val(ui.item.id); 
				$('#jammulai').val(ui.item.value);
			} 
		});  
		
		$(".jamselesai").autocomplete({ 
			source: base_url + "/module/skripsi/conf/jamselesai",
			minLength: 0, 
			select: function(event, ui) { 
				$('#jamselesaiid').val(ui.item.id); 
				$('#jamselesai').val(ui.item.value);
			} 
		});  
		
		$(".prodi").autocomplete({ 
			source: base_url + "/module/skripsi/conf/prodi",
			minLength: 0, 
			select: function(event, ui) { 
				$('#prodiid').val(ui.item.id); 
				$('#prodi').val(ui.item.value);
			} 
		});  
		
		
		$("#tabs").tabs();
		$("#accordion").accordion({autoHeight: true,fillSpace: false});
		$("#tabs-child").tabs();
		$('#example').dataTable();		
		$(".typeahead").typeahead();		
    });