$(document).ready(function() {
	var head = $("#head-form").val();
	if(head == "Edit Pembimbing"){
		$("#select_dosen").removeAttr("disabled");
		$("#select_thn").removeAttr("disabled");
		$("#select_dosen").select2("enable", true);
		$("#select_thn").select2('enable', true);
	}
	
	$("#select_fakultas").change(function(){
		var fakultas_id_ = $(this).val();
		$("#select_dosen").select2('val', "0");
		if(fakultas_id_=='0'){
			$("#select_dosen").select2('enable', false);
			$("#select_mhs").select2('enable', false);
			$("#select_thn").select2('enable', false);
		}
		else{
			$("#select_dosen").select2('enable', true);
			var uri = $(this).data("uri");
			$.ajax({
				type : "POST",
				dataType : "html",
				url : uri,
				data : $.param({
					fakultas_id : fakultas_id_
				}),
				success : function(msg) {
					if (msg == '') {
						
					} else {
						$("#select_dosen").html(msg);
						$("#select_dosen").select2("enable", true);
					}
				}
			});
			
			$.ajax({
				type : "POST",
				dataType : "json",
				url : base_url + "/module/akademik/conf/mhs",
				data : $.param({
					fakultas_id : fakultas_id_
				}),
				success : function(data) {
					$("#select_mhs").autocomplete({ 
						source: data,
						minLength: 0, 
						select: function(event, ui) { 
							$('#select_mhs').val(ui.item.value);
						} 
					});
				}
			});
			
		}
		
	});
	
	$("#select_dosen").change(function(){
		$("#select_mhs").select2('data', null);
		$("#select_thn").select2('val', "0");
		var fakultas_id_ = $("#select_fakultas").val();
		var dosen_id_ = $(this).val();
		if(dosen_id_=='0'){
			$("#select_mhs").select2('enable', false);
			$("#select_thn").select2('enable', false);
		}
		else{
			$("#select_mhs").select2('enable', true);
			$("#select_thn").select2('enable', true);
			var uri = $(this).data("uri");
			$.ajax({
				type : "POST",
				dataType : "html",
				url : uri,
				data : $.param({
					fakultas_id : fakultas_id_
				}),
				success : function(msg) {
					if (msg == '') {
						alert();
					} else {
						$("#select_mhs").html(msg);
						$("#select_mhs").select2("enable", true);
					}
				}
			});
		}
		
	});
	
	$(".e9").select2();
	
	jQuery("#select_mhs").tagsManager();
	
});

