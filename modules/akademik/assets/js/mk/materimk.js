$(document).ready(function() {
	$("#select_fakultas").change(function() {
		var fakultas_id = $("#select_indexfakultas").val();
		//alert(fakultas_id);
		$("#select_cabang").select2("val","0");
		
		$("#select_thn").select2("val","0");
		$("#select_thn").attr("disabled");
		$("#select_thn").select2("enable", false);
		
		$("#select_mk").select2("val","0");
		$("#select_mk").attr("disabled");
		$("#select_mk").select2("enable", false);
		
		if(fakultas_id == 0){
			$("#select_cabang").attr("disabled");
			$("#select_cabang").select2("enable", false);
		}
		else{
			$("#select_cabang").removeAttr("disabled");
			$("#select_cabang").select2("enable", true);
		}
		
	});
	
	$("#select_cabang").change(function() {
		var cabang = $(this).val();
		var fakultas_id_ = $("#select_fakultas").val();
		var uri = $(this).data("uri");
		
		$("#select_thn").select2("val","0");
		
		$("#select_mk").select2("val","0");
		$("#select_mk").attr("disabled");
		$("#select_mk").select2("enable", false);
		
		$("#select_submk").select2("val","0");
		$("#select_submk").attr("disabled");
		$("#select_submk").select2("enable", false);
		
		if(cabang == 0){
			$("#select_thn").attr("disabled");
			$("#select_thn").select2("enable", false);
		}
		else{
			$.ajax({
				type : "POST",
				dataType : "html",
				url : uri,
				data : $.param({
					fakultas_id : fakultas_id_
				}),
				success : function(msg) {
					if (msg == '') {
						$("#select_thn").html('<option value="0">Select Tahun Akademik</option>');
						$("#select_thn").attr("disabled","disabled");
					} else {
						//alert(uri);
						$("#select_thn").removeAttr("disabled");
						$("#select_thn").html(msg);
						$("#select_thn").select2("enable", true);
					}
				}
			});
		}
	});
	
	$("#select_thn").change(function() {
		var fakultas_id_ = $("#select_fakultas").val();
		var cabang_id_ = $("#select_cabang").val();
		var thn_id_ = $(this).val();
		var uri = $(this).data("uri");
		//alert(uri);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				hid_id : fakultas_id_,
				thn_id : thn_id_,
				cabang_id : cabang_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					$("#select_mk").attr("disabled","disabled");
				} else {
					//alert(uri);
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
					$("#select_mk").select2("enable", true);
				}
			}
		});
	});
	
	$("#select_mk").change(function() {
		var fakultas_id_ = $("#select_fakultas").val();
		var thn_id_ = $("#select_thn").val();
		var mk_ = $(this).val();
		var uri = $(this).data("uri");
		var uri2 = $(this).data("jum");
		//alert(uri);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				hid_id : fakultas_id_,
				thn_id : thn_id_,
				mkditawar_id : mk_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_submk").html('<option value="0">Select Sub Materi</option>');
					$("#select_submk").attr("disabled","disabled");
					$("#select_statmk").html('<option value="0">Select Status</option>');
					$("#select_statmk").attr("disabled","disabled");
				} else {
					//alert(uri);
					$("#select_statmk").removeAttr("disabled");
					$("#select_submk").removeAttr("disabled");
					$("#select_submk").html(msg);
					$("#select_statmk").select2("enable", true);
					$("#select_submk").select2("enable", true);
				}
			}
		});
		
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri2,
			data : $.param({
				hid_id : fakultas_id_,
				thn_id : thn_id_,
				mkditawar_id : mk_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_submk").html('<option value="0">Select Sub Materi</option>');
					$("#select_submk").attr("disabled","disabled");
					$("#select_statmk").html('<option value="0">Select Status</option>');
					$("#select_statmk").attr("disabled","disabled");
				} else {
					//alert(mk_);
					$("#urut").val(msg);
				}
			}
		});
	});
	
	$("#select_submk").change(function() {
		var fakultas_id_ = $("#select_fakultas").val();
		var thn_id_ = $("#select_thn").val();
		var mk_ = $("#select_mk").val();
		var mat_ = $(this).val();
		var uri = $(this).data("jum");
		//alert(mat_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				hid_id : fakultas_id_,
				thn_id : thn_id_,
				mkditawar_id : mk_,
				materi_id : mat_
			}),
			success : function(msg) {
				if (msg == '') {
					alert("DATA NOT FOUND");
					// $("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					// $("#select_mk").attr("disabled","disabled");
				} else {
					//alert(uri);
					$("#urut").val(msg);
				}
			}
		});
	});
	
	//================INDEX================
	$("#select_indexfakultas").change(function() {
		var fakultas_id = $("#select_indexfakultas").val();
		//alert(fakultas_id);
		$("#select_indexcabang").select2("val","0");
		
		$("#select_indexthn").select2("val","0");
		$("#select_indexthn").attr("disabled");
		$("#select_indexthn").select2("enable", false);
		
		$("#select_indexmk").select2("val","0");
		$("#select_indexmk").attr("disabled");
		$("#select_indexmk").select2("enable", false);
		
		if(fakultas_id == 0){
			$("#select_indexcabang").attr("disabled");
			$("#select_indexcabang").select2("enable", false);
		}
		else{
			$("#select_indexcabang").removeAttr("disabled");
			$("#select_indexcabang").select2("enable", true);
		}
	});
	
	$("#select_indexcabang").change(function() {
		var cabang = $("#select_indexcabang").val();
		var fakultas_id_ = $("#select_indexfakultas").val();
		var uri = $(this).data("uri");
		
		$("#select_indexthn").select2("val","0");
		$("#select_indexmk").select2("val","0");
		$("#select_indexmk").attr("disabled");
		$("#select_indexmk").select2("enable", false);
		if(cabang == 0){
			$("#select_indexthn").attr("disabled");
			$("#select_indexthn").select2("enable", false);
		}
		else{
			$.ajax({
				type : "POST",
				dataType : "html",
				url : uri,
				data : $.param({
					fakultas_id : fakultas_id_
				}),
				success : function(msg) {
					//alert(msg);
					if (msg == '') {
						$("#select_indexthn").html('<option value="0">Select Tahun Akademik</option>');
						$("#select_indexthn").attr("disabled","disabled");
					} else {
						//alert(uri);
						$("#select_indexthn").removeAttr("disabled");
						$("#select_indexthn").html(msg);
						$("#select_indexthn").select2("enable", true);
					}
				}
			});
		}
		
	});
	
	$("#select_indexthn").change(function() {
		var fakultas_id_ = $("#select_indexfakultas").val();
		var thn_id_ = $(this).val();
		var cabang_ = $("#select_indexcabang").val();
		var uri = $(this).data("uri");
		//alert(cabang_);
		$("#select_indexmk").select2("val","0");
		if(thn_id_ == 0){
			$("#select_indexmk").attr("disabled");
			$("#select_indexmk").select2("enable", false);
		}
		else {
			$.ajax({
				type : "POST",
				dataType : "html",
				url : uri,
				data : $.param({
					hid_id : fakultas_id_,
					thn_id : thn_id_,
					cabang : cabang_
				}),
				success : function(msg) {
					if (msg == '') {
						$("#select_indexmk").html('<option value="0">Select Mata Kuliah</option>');
						$("#select_indexmk").attr("disabled","disabled");
					} else {
						//alert(uri);
						$("#select_indexmk").removeAttr("disabled");
						$("#select_indexmk").html(msg);
						$("#select_indexmk").select2("enable", true);
					}
				}
			});
		}
	});
	$("#select_indexmk").change(function() {
		var fakultas_id_ = $("#select_indexfakultas").val();
		var thn_id_ = $("#select_indexthn").val();
		var cabang_ = $("#select_indexcabang").val();
		var mk_ = $(this).val();
		var uri = $(this).data("uri");
		//alert(uri);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				hid_id : fakultas_id_,
				thn_id : thn_id_,
				mkditawar_id : mk_,
				cabang : cabang_
			}),
			success : function(msg) {
				if (msg == "<table class='table table-bordered'><tbody></tbody></table>") {
					$("#display").html(' <br><br><br><br><br><div class="span3" align="center" style="margin-top:20px;"><div class="well">Sorry, no content to show</div></div>');
				} else {
					$("#display").html(msg);
				}
			}
		});
	});
	
	$(".e9").select2();
});

function doDelete(i) {
  	var x = confirm("Are you sure you want to delete?");
 	if (x){
    	var a=document.getElementsByClassName("deleted"+i);
  		var del_id=a[0].getAttribute('value');
  		var url=a[0].getAttribute('uri');
  		//alert(url);
	  	$.ajax({
				type : "POST",
				dataType : "html",
				url : url,
				data : $.param({
					delete_id : del_id
				}),
				success : function(msg) {
					if (msg) {
						alert("Data Berhasil Terhapus!");
					}else {
						alert('gagal');
					}
					location.reload();
				}
		});
  }
  else {
  	location.reload();
  }	
};