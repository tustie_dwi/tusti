$(document).ready(function() {
	$(".e9").select2();
	
	var fakultas_		= document.getElementById("pilih_fakultas");
	var fakultasid_	    = $(fakultas_).val();
	
	var cabang_			= document.getElementById("pilih_cabang");
	var cabangid_	    = $(cabang_).val();
	
	if (fakultasid_ == '0') {
		$("#pilih_cabang").attr("disabled", true);
		$("#pilih_cabang").select2('enable', false);
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
	}else {
		$("#pilih_cabang").removeAttr("disabled");
		$("#pilih_cabang").select2('enable', true);
	}
	
	if (cabangid_ == '0') {
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
	}else {
		$("#select_thnakademik").removeAttr("disabled");
		$("#select_thnakademik").select2('enable', true);
	}
	
	$('#pilih_fakultas').change(function() {	
		var fakultas		= document.getElementById("pilih_fakultas");
		var fakultasid	    = $(fakultas).val();
		
		if (fakultasid == '0') {
			$("#pilih_cabang").attr("disabled", true);
			$("#pilih_cabang").select2('enable', false);
			$("#select_thnakademik").attr("disabled", true);
			$("#select_thnakademik").select2('enable', false);
		}else {
			$("#pilih_cabang").removeAttr("disabled");
			$("#pilih_cabang").select2('enable', true);
			$("#pilih_cabang").select2().select2("val", '0');
			$("#select_thnakademik").select2().select2("val", '0');
		var fakultas_id_ = $(this).val();
		}
		
		if(fakultas_id_=='0'){
			$("#select_cabang").attr("disabled", true);
			$("#select_cabang").select2('enable', false);
			$("#select_thn").attr("disabled", true);
			$("#select_thn").select2('enable', false);
			$("#namamk").attr("disabled", true);
			$("#select_mk").attr("disabled", true);
			$("#select_mk").select2('enable', false);
		}
		else{
			$("#select_cabang").removeAttr("disabled");
			$("#select_cabang").select2('enable', true);
			$("#select_thn").removeAttr("disabled");
			$("#select_thn").select2('enable', true);
			$("#namamk").removeAttr("disabled");
			datalist_edit(fakultas_id_);
		}
	});
	
	function datalist_edit(fakultas_id_){
		//----------------ajax is blok---------------------------------------
		var uri2 = base_url + "/module/akademik/mkditawarkan/blok_mk";
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri2,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_mk").attr("disabled", true);
					$("#form").fadeOut();
					$("#isblok").attr("checked", false);
					
				} else {
					$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
					$("#select_mk").select2('enable', true);
				}
			}
		});
		//----------------ajax is namamk--------');
		}
	});	
	
	$('#pilih_cabang').change(function() {
		var cabang_			= document.getElementById("pilih_cabang");
		var cabangid_	    = $(cabang_).val();
		if (cabangid_ == '0') {
			$("#select_thnakademik").attr("disabled", true);
			$("#select_thnakademik").select2('enable', false);
		}else {
			$("#select_thnakademik").removeAttr("disabled");
			$("#select_thnakademik").select2('enable', true);
			$("#select_thnakademik").select2().select2("val", '0');
		}
	});

	$("#select_thnakademik").change(function() {
		$("#param").submit(); //SUBMIT FORM
	});
	//------------------------------INDEX-------------------------------------------
	
	//------------------------------EDIT/WRITE-------------------------------------------
	var editfakultasid_	    = $("#select_fakultas").val();
	if (editfakultasid_ == '0') {
		$("#select_cabang").attr("disabled", true);
		$("#select_cabang").select2('enable', false);
		$("#select_thn").attr("disabled", true);
		$("#select_thn").select2('enable', false);
		$("#namamk").attr("disabled", true);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
	}else {
		$("#select_cabang").removeAttr("disabled");
		$("#select_cabang").select2('enable', true);
		$("#select_thn").removeAttr("disabled");
		$("#select_thn").select2('enable', true);
		$("#namamk").removeAttr("disabled");
		datalist_edit(editfakultasid_);
	}
	
	$("#select_fakultas").chan-------------------------------
		$.ajax({
			type : "POST",
			dataType: "json",
			url: base_url + "/module/akademik/conf/namamk_from_matakuliah",
			data : $.param({
				fakultasid : fakultas_id_
			}),
			success: function(data){
			$('#namamk').autocomplete(
			{
				source: data,
				minLength: 0,
				select: function(event, ui) { 
				$('#namamk').val(ui.item.value);
				}    
			});
			}
		});
	}
	//------------------------------EDIT/WRITE-------------------------------------------
	
	$(function () {
		$("#dosenkoor").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenkoor').val(ui.item.value);
			} 
		});
		
		$("#dosenpengampu").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenpengampu').val(ui.item.value);
				$('#dosenpengampuid').val(ui.item.id);
			} 
		});			
	});
	
	//------DISABLE ENTER SUBMIT-----------------------
	$(document).on("keypress", 'form', function (e) {
	    var code = e.keyCode || e.which;
	    if (code == 13) {
	        e.preventDefault();
	        return false;
	    }
	});
	
	$("#submit").click(function(e){			
		var fakultas = $('#select_fakultas').val();
		var select_thn = $('#select_thn').val();
		var namamk = $('#namamk').val().length;
		var kuota = $('#kuota').val().length;
		
		if(fakultas!=0 && select_thn!=0 && namamk > 0 && kuota > 0){
			var edit = $('#edit-cek').val();
			if(edit!='1'){
				var uri = base_url + "module/akademik/mkditawarkan/saveToDB";
			}
			else{
				var uri = base_url + "module/akademik/mkditawarkan/saveToDBedit";
			}
 
			//------SET KOORDINATOR---------------------
			var radio_length = $('input:radio[name="koor"]').length;
			for(i=1;i<=radio_length;i++){
				if($('#koor'+i+':checked').val())var pengampu_id = $('#pengampuid'+i).val();
				if($('#pengampukoor'+i).val()=='1')var last_koor_pengampu_id = $('#pengampuid'+i).val();
			}
			koor_pengampu(last_koor_pengampu_id, '0');
			koor_pengampu(pengampu_id, '1');
			
			//------END SET KOORDINATOR---------------------
			var postData = new FormData($('#form-mkditawarkan')[0]);
			$.ajax({
				url : uri,
				type: "POST",
				data : postData,
				async: false,
				success:function(data, textStatus, jqXHR) 
				{
				    alert ('Upload Success!');
				    location.reload();  
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
				    alert ('Upload Failed!');      
				    },
				    cache: false,
					contentType: false,
					processData: false
				  });
				e.preventDefault(); //STOP default action
				return false;
		}
		else{
			alert("Please fill the form");
		}
	});
	jQuery("#dosenpengampu").tagsManager();
});

function validate() {
	if (document.getElementById('isblok').checked) {
    	$("#form").fadeIn();
    } 
    else {
       $("#form").fadeOut();
    }
}

function koor_pengampu(pengampu_id, koor){
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + "module/akademik/mkditawarkan/koor_pengampu",
		data : $.param({
			pengampuid : pengampu_id,
			iskoor	   : koor
		}),
		success : function(msg) {
			//location.reload();
		}
	});
}