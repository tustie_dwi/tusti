$(document).ready(function() {
	$(".e9").select2();
	
	var fakultas_id_ = $('#select_fakultas').val();
	if(fakultas_id_=='0'){
		$("#namamk").attr("disabled", true);
	}else{
		$("#namamk").removeAttr("disabled");
		datalist_edit(fakultas_id_);
	} 
		
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		
		if(fakultas_id_=='0'){
			$("#namamk").attr("disabled", true);
		}else{
			$("#namamk").removeAttr("disabled");
			datalist_edit(fakultas_id_);
		} 
	});
	
	function datalist_edit(fakultas_id_){
		$.ajax({
		  type : "POST",
		  dataType: "json",
          url: base_url + "/module/akademik/conf/namamk",
          data : $.param({
			fakultasid : fakultas_id_
		}),
           success: function(data){
             $('#namamk').autocomplete(
             {
                   source: data,
                   minLength: 0,
                   select: function(event, ui) { 
					$('#namamk').val(ui.item.value);
					}    
             });
             
           }
	    });
	}
	
	$("#submit").click(function(e){
		var fakultas = $('#fakultas').val();
		var namamk = $('#namamk').val().length;
		var namamkeng = $('#namamkeng').val().length;

		if(fakultas!=0 && namamk > 0 && namamkeng > 0){
		  var postData = $('#form').serializeArray();
          $.ajax({
            url : base_url + "module/akademik/mk/saveToDB",
	        type: "POST",
	        data : postData,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Success!');
	            location.reload();  
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        }
	      });
	    e.preventDefault(); //STOP default action
	    
		}
		else{
			alert("Please fill the form");
		}
	});
	
	$("#submitmk").click(function(e){
		var namamk = $('#namamk').val().length;
		var namamkeng = $('#namamkeng').val().length;
		var kodemk = $('#kodemk').val().length;
		var kurikulum = $('#kurikulum').val().length;
		var sks = $('#sks').val().length;

		if(namamk > 0 && namamkeng > 0 && kodemk > 0 && kurikulum > 0 && sks > 0){
		  var postData = $('#form').serializeArray();
          $.ajax({
            url : base_url + "module/akademik/mk/saveToDBmk",
	        type: "POST",
	        data : postData,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Success!');
	            location.reload();  
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Failed!');      
	        }
	      });
	    e.preventDefault(); //STOP default action
	    
		}
		else{
			alert("Please fill the form");
		}
	});

});
