$(document).ready(function() {
	$("#select_fakultas").change(function() {
		var fakultas_id_ = $(this).val();
		var uri = $(this).data("uri");
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas_id : fakultas_id_
			}),
			success : function(msg) {
				// alert(msg);
				if (msg == '') {
					$("#select_thn").html('<option value="0">Select Tahun Akademik</option>');
					$("#select_thn").attr("disabled","disabled");
					$("#select_thn").select2("enable", false);
				} else {
					//alert(uri);
					$("#select_thn").removeAttr("disabled");
					$("#select_thn").html(msg);
					$("#select_thn").select2("enable", true);
				}
			}
		});
	});
	
	$("#select_thn").change(function() {
		var fakultas_id_ = $("#select_fakultas").val();
		var thn_id_ = $(this).val();
		var uri = $(this).data("uri");
		// alert (fakultas_id_);
		// alert (thn_id_);
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				hid_id : fakultas_id_,
				thn_id : thn_id_
			}),
			success : function(msg) {
				if (msg == '') {
					$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
					$("#select_mk").attr("disabled","disabled");
					$("#select_mk").select2("enable", false);
				} else {
					
					$("#select_mk").removeAttr("disabled");
					$("#select_mk").html(msg);
					$("#select_mk").select2("enable", true);
				}
			}
		});
	});
	
	//=======================INDEX======================//
	
	$("#select_indexfakultas").change(function() {
		var fakultas_id = $("#select_indexfakultas").val();
		//alert(fakultas_id);
		$("#select_indexcabang").select2("val","0");
		$("#display").empty();
		
		$("#select_indexthn").select2("val","0");
		$("#select_indexthn").attr("disabled");
		$("#select_indexthn").select2("enable", false);
		
		$("#select_indexmk").select2("val","0");
		$("#select_indexmk").attr("disabled");
		$("#select_indexmk").select2("enable", false);
		
		if(fakultas_id == 0){
			$("#select_indexcabang").attr("disabled");
			$("#select_indexcabang").select2("enable", false);
		}
		else{
			$("#select_indexcabang").removeAttr("disabled");
			$("#select_indexcabang").select2("enable", true);
		}
	});
	
	$("#select_indexcabang").change(function() {
		var cabang_id = $("#select_indexcabang").val();
		//alert(fakultas_id);
		
		$("#select_indexthn").select2("val","0");
		$("#display").empty();
		$("#select_indexmk").select2("val","0");
		$("#select_indexmk").attr("disabled");
		$("#select_indexmk").select2("enable", false);
		
		if(cabang_id == 0){
			$("#select_indexthn").attr("disabled");
			$("#select_indexthn").select2("enable", false);
		}
		else{
			$("#select_indexthn").removeAttr("disabled");
			$("#select_indexthn").select2("enable", true);
		}
	});
	
	
	$("#select_indexthn").change(function() {
		$("#display").empty();
		$("#select_indexmk").select2("val","0");
		var fakultas_id_ = $("#select_indexfakultas").val();
		var cabang_ = $("#select_indexcabang").val();
		var thn_ = $(this).val();
		var uri = $(this).data("uri");
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas : fakultas_id_,
				cabang : cabang_,
				thn : thn_
			}),
			success : function(msg) {
				//alert(msg);
				if (msg == '') {
					$("#select_indexmk").html('<option value="0">Select Mata Kuliah</option>');
					$("#select_indexmk").attr("disabled","disabled");
					$("#select_indexmk").select2("val","0");
					$("#select_indexmk").select2("enable", false);
				} else {
					
					$("#select_indexmk").removeAttr("disabled");
					$("#select_indexmk").html(msg);
					$("#select_indexmk").select2("enable", true);
				}
			}
		});
	});
	
	$("#select_indexmk").change(function() {
		var fakultas_id_ = $("#select_indexfakultas").val();
		var cabang_ = $("#select_indexcabang").val();
		var thn_ = $("#select_indexthn").val();
		var mk_ = $(this).val();
		var uri = $(this).data("uri");
		$.ajax({
			type : "POST",
			dataType : "html",
			url : uri,
			data : $.param({
				fakultas : fakultas_id_,
				cabang : cabang_,
				thn : thn_,
				mk : mk_
			}),
			success : function(msg) {
				//alert(msg);
				if (msg == "") {
					$("#display").html(' <br><br><br><br><br><div class="span3" align="center" style="margin-top:20px;"><div class="well">Sorry, no content to show</div></div>');
				} else {
					$("#display").html(msg);
					$("#example").dataTable({ 
						"sDom": "<'row'<'col-md-9'><'col-md-2'f>><'table-responsive't><'row'<'col-md-6'><'col-md-6'p>>",
						"sPaginationType": "bootstrap",
						"aaSorting": [],
						"bSort":false,
						"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"sSearch": ""
						},
						bRetrieve : true}).fnDraw();
				}
			}
		});
	});
	
	$(".e9").select2();
}); 