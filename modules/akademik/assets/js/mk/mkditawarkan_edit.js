$(document).ready(function() {
	var fkid			= document.getElementById("select_fakultas");
	var fakultas_id_	= $(fkid).val();
	
	$("#select_fakultas").removeAttr("disabled");
	$("#select_fakultas").select2('enable', true);
	$("#select_thn").removeAttr("disabled");
	$("#select_thn").select2('enable', true);
	
	//----------------ajax is blok---------------------------------------
	var uri2 = base_url + "/module/akademik/mkditawarkan/blok_mk";
	//alert(uri2);
	$.ajax({
		type : "POST",
		dataType : "html",
		url : uri2,
		data : $.param({
			fakultas_id : fakultas_id_
		}),
		success : function(msg) {
			if (msg == '') {
				$("#select_mk").attr("disabled", true);
				$("#form").fadeOut();
				$("#isblok").attr("checked", false);
				
			} else {
				$("#select_mk").html('<option value="0">Select Mata Kuliah</option>');
				$("#select_mk").removeAttr("disabled");
				$("#select_mk").select2('enable', true);
				$("#select_mk").html(msg);
			}
		}
	});
	
	$("#namedosenpengampu").autocomplete({ 
			source: base_url + "/module/akademik/conf/dosen",
			minLength: 0, 
			select: function(event, ui) { 
				$('#dosenpengampu').val(ui.item.value);
				$('#dosenpengampuid').val(ui.item.id);
			} 
		});
		
});

function deletePengampu(i){
	var x = confirm("Are you sure you want to delete?");
 	if (x){
 		$.ajax({
			type : "POST",
			dataType : "html",
			url : base_url + "module/akademik/mkditawarkan/delete_pengampu",
			data : $.param({
				delete_id : i
			}),
			success : function(msg) {
				if (msg=='sukses') {
					location.reload();
				} else {
					alert('gagal');
				}
			}
		});
 	}
}

function iskoor(id){
	var radio_length = $('input:radio[name="koor"]').length;
	for(i=1;i<=radio_length;i++){
		isrem(i);
	}
	var output='';
	output += "<i id='rem"+id+"'><small><span class='label label-danger'>Koordinator</span></small></i>";
    $("#benar"+id).html(output);
    $("#isbenarcheck"+id).val('1');
}

function isrem(id){
	$("#rem"+id).remove();
	$("#isbenarcheck"+id).val('');
}

//-------ADD DOSEN------------------------------------------
$('#addDosenPengampu').click(function() {
	var karyawan_id = $('#dosenpengampuid').val();
	var mk_id = $('input:hidden[name="hidId"]').val();
	addPengampu(karyawan_id, mk_id)
	
});

function postpengampu(e){
	if (e.keyCode == 13) {
        var karyawan_id = $('#dosenpengampuid').val();
		var mk_id = $('input:hidden[name="hidId"]').val();
		addPengampu(karyawan_id, mk_id)
    }
}

function addPengampu(karyawan_id, mk_id){
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + "module/akademik/mkditawarkan/add_pengampu",
		data : $.param({
			karyawan_id : karyawan_id,
			mk_id		: mk_id
		}),
		success : function(msg) {
			location.reload();
		}
	});
}
