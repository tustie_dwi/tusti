$(document).ready(function() {
	
	$(".e9").select2();
	
	var cabang_			= document.getElementById("pilih_cabang");
	var cabangid_	    = $(cabang_).val();
	
	var fakultas_		= document.getElementById("pilih_fakultas");
	var fakultasid_	    = $(fakultas_).val();
	
	var thn				= document.getElementById("select_thnakademik");
	var thnid_	    	= $(thn).val();
	
	var mk				= document.getElementById("select_mk");
	var mkid_	    	= $(mk).val();
	
	//datalist_edit(fakultasid, thnid, cabangid_, mkid_);
	
	if (fakultasid_ == '0') {
		$("#pilih_cabang").attr("disabled", true);
		$("#pilih_cabang").select2('enable', false);
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
	}else {
		$("#pilih_cabang").removeAttr("disabled");
		$("#pilih_cabang").select2('enable', true);
	}
	
	if (cabangid_ == '0') {
		$("#select_thnakademik").attr("disabled", true);
		$("#select_thnakademik").select2('enable', false);
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
	}else {
		$("#select_thnakademik").removeAttr("disabled");
		$("#select_thnakademik").select2('enable', true);
	}
	
	if (thnid_ == '0') {
		$("#select_mk").attr("disabled", true);
		$("#select_mk").select2('enable', false);
	}else {
		$("#select_mk").removeAttr("disabled");
		$("#select_mk").select2('enable', true);
	}
	
	$('#pilih_fakultas').change(function() {
		var fakultasid	    = $(this).val();
		
		var cabang_			= document.getElementById("pilih_cabang");
		var cabangid_	    = $(cabang_).val();
		
		var thn				= document.getElementById("select_thnakademik");
		var thnid_	    	= $(thn).val();
		
		var mk				= document.getElementById("select_mk");
		var mkid_	    	= $(mk).val();
	
		if (fakultasid == '0') {
			$("#pilih_cabang").attr("disabled", true);
			$("#pilih_cabang").select2('enable', false);
			$("#select_thnakademik").attr("disabled", true);
			$("#select_thnakademik").select2('enable', false);
			$("#select_mk").attr("disabled", true);
			$("#select_mk").select2('enable', false);
		}else {
			$("#pilih_cabang").removeAttr("disabled");
			$("#pilih_cabang").select2('enable', true);
			$("#pilih_cabang").select2().select2("val", '0');
			$("#select_thnakademik").select2().select2("val", '0');
			$("#select_mk").select2().select2("val", '0');
		}
		
		datalist_edit(fakultasid, thnid, cabangid_, mkid_);
		
	});
	
	$('#pilih_cabang').change(function() {
		var cabangid_	    = $(this).val();
		
		if (cabangid_ == '0') {
			$("#select_thnakademik").attr("disabled", true);
			$("#select_thnakademik").select2('enable', false);
			$("#select_mk").attr("disabled", true);
			$("#select_mk").select2('enable', false);
		}else {
			$("#select_thnakademik").removeAttr("disabled");
			$("#select_thnakademik").select2('enable', true);
			$("#select_thnakademik").select2().select2("val", '0');
			$("#select_mk").select2().select2("val", '0');
		}
	});
	
	$('#select_thnakademik').change(function() {
		var thnid	    = $(this).val();
		
		var cabang_			= document.getElementById("pilih_cabang");
		var cabangid_	    = $(cabang_).val();
		
		var fakultas_		= document.getElementById("pilih_fakultas");
		var fakultasid	    = $(fakultas_).val();
		
		var mk				= document.getElementById("select_mk");
		var mkid_	    	= $(mk).val();
		
		if (thnid == '0') {
			$("#select_mk").attr("disabled", true);
			$("#select_mk").select2('enable', false);
		}else {
			$("#select_mk").removeAttr("disabled");
			$("#select_mk").select2('enable', true);
			$("#select_mk").select2().select2("val", '0');
		}
		
		datalist_edit(fakultasid, thnid, cabangid_, mkid_);
		
	});
	
	$('#select_mk').change(function() {
		var cabang_			= document.getElementById("pilih_cabang");
		var cabangid_	    = $(cabang_).val();
		
		var thn				= document.getElementById("select_thnakademik");
		var thnid	    	= $(thn).val();
		
		var fakultas_		= document.getElementById("pilih_fakultas");
		var fakultasid	    = $(fakultas_).val();
		
		var mkid_	    	= $(this).val();
		
		datalist_edit(fakultasid, thnid, cabangid_, mkid_);
		
	});
	
	$("#parameter").change(function() {
		$("#param").submit(); //SUBMIT FORM
	});
	url + "module/akademik/komponennilai/saveToDB",
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            alert ('Upload Success!');
            location.reload();  
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!');      
        }
      });
    e.preventDefault(); //STOP default action
	}
	else{
		alert("Please fill the form");
	}
});
	
$("#submit-add").click(function(e){
	var bobot = $('#bobot').val().length;
	var keterangan = $('#keterangan').val().length;
	
	if(bobot > 0 && keterangan > 0){
	  var postData = $('#form').serializeArray();
      $.ajax({
        url : base_url + "module/akademik/komponennilai/saveToDBtambahan",
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            alert ('Upload Success!');
            location.reload();  
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert ('Upload Failed!');      
        }
      });
    e.preventDefault(); //STOP default action
	}
	else{
		alert("Please fill the form");
	}
});
	$(function () {
	    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
	    $('.tree li.parent_li > span').on('click', function (e) {
	        var children = $(this).parent('li.parent_li').find(' > ul > li');
	        if (children.is(":visible")) {
	            children.hide('fast');
	            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
	        } else {
	            children.show('fast');
	            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
	        }
	        e.stopPropagation();
	    });
	});
	
});

function datalist_edit(fakultasid, thnid, cabangid_, mkid_){
	$.ajax({
		type : "POST",
		dataType : "html",
		url : base_url + 'module/akademik/komponennilai/get_nama_mk',
		data : $.param({
			fakultas_id : fakultasid,
			thnid : thnid,
			cabang : cabangid_,
			mkid : mkid_
		}),
		success : function(msg) {
			if (msg == '') {
				
			} else {
				$("#select_mk").html(msg);
			}
		}
	});
};

//--------submit----------------------------------------------
	
$("#submit").click(function(e){
	var bobot = $('#bobot').val().length;
	var keterangan = $('#keterangan').val().length;
	
	if(bobot > 0 && keterangan > 0){
	  var postData = $('#form').serializeArray();
      $.ajax({
        url : base_