<?php
class model_tahunakademik extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;
		
	public function read(){
		$que = "SELECT mid(md5(tahun.tahun_akademik),6,6) tahun_akademik, tahun.tahun_akademik hidId, tahun.tahun, tahun.is_ganjil, tahun.is_pendek, tahun.is_aktif FROM `db_ptiik_apps`.`tbl_tahunakademik` `tahun`";
		$que .= "WHERE tahun.is_aktif = '1'";
		
		$que .= "ORDER BY tahun.tahun desc";
		return $this->db->query($que);
	}
	
	public function get_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tahun_akademik,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM `db_ptiik_apps`.`tbl_tahunakademik`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	public function replace_tahun($data){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_tahunakademik` SET `is_aktif` = '0'";
		$this->db->query($sql);
// 		
		// $sql = "DELETE FROM `db_ptiik_apps`.`tbl_kalenderakademik`";
		// $this->db->query($sql);
		$this->db->replace('db_ptiik_apps`.`tbl_tahunakademik',$data);
	}
	
	public function delete($id){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_tahunakademik` WHERE mid(md5(`tahun_akademik`),6,6) = '" . $id ."'";
		$this->db->query($sql);
	}
}