<?php
class model_silabus extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL, $fakultas=NULL, $cabang=NULL, $mk=NULL, $thn=NULL){
		$sql = "SELECT MID( MD5( `tbl_silabus`.silabus_id), 6, 6) as silabus_id,
					   `tbl_silabus`.silabus_id as hidsil, 
					   `tbl_namamk`.keterangan as namamk,
					   `tbl_namamk`.english_version as english,
					   `tbl_mkditawarkan`.mkditawarkan_id as idmk,
					   MID( MD5( `tbl_silabus_detail`.detail_id), 6, 6) as detail_id,
					   `tbl_silabus_detail`.detail_id as hiddet, 
					   `tbl_silabus_komponen`.keterangan as komponen,
					   `tbl_silabus_komponen`.komponen_id, 
					   `tbl_silabus_detail`.keterangan as silabus,
					   `tbl_fakultas`.fakultas_id,
					   `tbl_fakultas`.keterangan as fak,
					   `tbl_tahunakademik`.tahun,
					   `tbl_tahunakademik`.tahun_akademik as tahun_id
				FROM `db_ptiik_apps`.`tbl_namamk`, 
					 `db_ptiik_apps`.`tbl_matakuliah`, 
					 `db_ptiik_apps`.`tbl_mkditawarkan`, 
					 `db_ptiik_apps`.`tbl_silabus`, 
					 `db_ptiik_apps`.`tbl_silabus_detail`, 
					 `db_ptiik_apps`.`tbl_tahunakademik`,
					 `db_ptiik_apps`.`tbl_fakultas`, 
					 `db_ptiik_apps`.`tbl_silabus_komponen`
				WHERE `tbl_silabus`.mkditawarkan_id = `tbl_mkditawarkan`.mkditawarkan_id
					  AND `tbl_mkditawarkan`.matakuliah_id = `tbl_matakuliah`.matakuliah_id
					  AND `tbl_matakuliah`.namamk_id = `tbl_namamk`.namamk_id
				      AND `tbl_silabus`.silabus_id=`tbl_silabus_detail`.silabus_id
				      AND `tbl_mkditawarkan`.tahun_akademik=`tbl_tahunakademik`.tahun_akademik
				      AND `tbl_fakultas`.fakultas_id=`tbl_namamk`.fakultas_id
				      AND `tbl_silabus_detail`.komponen_id=`tbl_silabus_komponen`.komponen_id";
		if($id!=""){
			$sql = $sql . " AND MID( MD5(`tbl_silabus_detail`.detail_id), 6, 6) = '".$id."' ";
		}
		
		if($fakultas!=""){
			$sql = $sql . " AND tbl_namamk.fakultas_id = '".$fakultas."' ";
		}
		
		if($cabang!=""){
			$sql = $sql . " AND tbl_mkditawarkan.cabang_id = '".$cabang."' ";
		}
		
		if($mk!=""){
			$sql = $sql . " AND tbl_silabus.mkditawarkan_id = '".$mk."' ";
		}
		
		if($thn!=""){
			$sql = $sql . " AND tbl_mkditawarkan.tahun_akademik = '".$thn."'";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function tahun_edit(){
		$sql = "SELECT MID( MD5(tahun_akademik), 6, 6) as mktahun_akademik,
					   concat(tahun,' - ', is_ganjil, ' ', is_pendek) as tahun,
					   tahun_akademik as thn_id
				FROM `db_ptiik_apps`.`tbl_tahunakademik`
				ORDER BY tahun_akademik DESC";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function tahun_akademik(){
		$sql = "SELECT `tbl_tahunakademik`.tahun_akademik AS thn_id,
						CONCAT( `tbl_tahunakademik`.tahun,  ' - ', `tbl_tahunakademik`.is_ganjil,  ' ', `tbl_tahunakademik`.is_pendek ) AS tahun
				FROM `db_ptiik_apps`.`tbl_tahunakademik`
				WHERE tbl_tahunakademik.is_aktif =1";
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function mkditawarkan($cabang=NULL, $fakultas=NULL, $thn=NULL){
		$sql = "SELECT MID( MD5(`tbl_mkditawarkan`.mkditawarkan_id), 6, 6) as mkditawarkan_id,
					   `tbl_mkditawarkan`.mkditawarkan_id as hid_id,
					   `tbl_namamk`.keterangan
				FROM `db_ptiik_apps`.`tbl_mkditawarkan`,
					 `db_ptiik_apps`.`tbl_matakuliah`,
					 `db_ptiik_apps`.`tbl_namamk`
				WHERE `tbl_mkditawarkan`.matakuliah_id = `tbl_matakuliah`.matakuliah_id AND
					  `tbl_matakuliah`.namamk_id = `tbl_namamk`.namamk_id";
		
		if($cabang!=""){
			$sql .= " AND tbl_mkditawarkan.cabang_id = '".$cabang."'";
		}
		
		if($fakultas!=""){
			$sql .= " AND tbl_namamk.fakultas_id = '".$fakultas."'";
		}
		
		if($thn!=""){
			$sql .= " AND tbl_mkditawarkan.tahun_akademik = '".$thn."'";
		}
		$sql .= " ORDER BY keterangan ASC";
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_komponen_aktif(){
		$sql = "SELECT `tbl_silabus_komponen`.`komponen_id`,
					   `tbl_silabus_komponen`.`keterangan` as `keterangan`,
					   `tbl_silabus_komponen`.`is_aktif`
				FROM `db_ptiik_apps`.`tbl_silabus_komponen`
				WHERE `tbl_silabus_komponen`.`is_aktif` = 1";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function id_silabus($semester=NULL){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(silabus_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_silabus`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function id_silabus_detail($semester=NULL){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(detail_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_silabus_detail`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function cek_new($ket){
		$sql = "SELECT keterangan from `db_ptiik_apps`.`tbl_silabus_detail` where keterangan = '".$ket."'";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_new_sil($komp_id,$sil_id){
		$sql = "SELECT komponen_id from `db_ptiik_apps`.`tbl_silabus_detail` where silabus_id = '".$sil_id."' AND komponen_id ='".$komp_id."'";
		$result = $this->db->query( $sql );
		return $result;
	}

	function cek_sil_id($mkd_id){
		$sql = "SELECT silabus_id from `db_ptiik_apps`.`tbl_silabus` where mkditawarkan_id = '".$mkd_id."'";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_new_by_id($ket){
		$sql = "SELECT detail_id from `db_ptiik_apps`.`tbl_silabus_detail` where keterangan = '".$ket."'";
		$result = $this->db->query( $sql );
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->detail_id;
			}
			return $id;
		}
	}
	
	public function tampilkan_mk($id,$thn){
		$sql = "SELECT tbl_namamk.keterangan, tbl_mkditawarkan.mkditawarkan_id 
				FROM `db_ptiik_apps`.`tbl_fakultas`,
				     `db_ptiik_apps`.`tbl_namamk`,
				     `db_ptiik_apps`.`tbl_matakuliah`,
				     `db_ptiik_apps`.`tbl_mkditawarkan`,
				     `db_ptiik_apps`.`tbl_tahunakademik` 
				WHERE tbl_fakultas.fakultas_id = '".$id."' 
					  AND tbl_fakultas.fakultas_id = tbl_namamk.fakultas_id
					  AND tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
					  AND tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
					  AND tbl_tahunakademik.tahun_akademik = '".$thn."'
					  AND tbl_mkditawarkan.tahun_akademik = tbl_tahunakademik.tahun_akademik";
		$result = $this->db->query( $sql );
		return $result;
	}
		
	function replace_silabus($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_silabus',$datanya);
	}
	
	function replace_detail($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_silabus_detail',$datanya);
	}
	
	function read_komponen($id){
		$sql = "SELECT MID( MD5(a.komponen_id), 6, 6) as komponen_id, 
				a.parent_id, 
				a.komponen_id as hidden_id, 
				a.is_aktif, 
				a.keterangan as keterangan, 
				b.keterangan as parent 
				FROM `db_ptiik_apps`.`tbl_silabus_komponen` a 
				left join `db_ptiik_apps`.`tbl_silabus_komponen` b on b.komponen_id = a.parent_id";
		if($id!=""){
			$sql = $sql . " WHERE  MID( MD5(a.komponen_id), 6, 6) = '".$id."' ";
		}
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function id_silabus_komponen($semester=NULL){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(komponen_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_silabus_komponen`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_komponen(){
		$sql = "SELECT `tbl_silabus_komponen`.`komponen_id`, `tbl_silabus_komponen`.`keterangan` as `keterangan`, `tbl_silabus_komponen`.`is_aktif`
				FROM `db_ptiik_apps`.`tbl_silabus_komponen`";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function cek_new_komp($ket){
		$sql = "SELECT keterangan from `db_ptiik_apps`.`tbl_silabus_komponen` where keterangan = '".$ket."'";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_new_komp_by_id($ket){
		$sql = "SELECT komponen_id from `db_ptiik_apps`.`tbl_silabus_komponen` where keterangan = '".$ket."'";
		$result = $this->db->query( $sql );
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->komponen_id;
			}
			return $id;
		}
	}
	
	function replace_komponen($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_silabus_komponen',$datanya);
	}
}
?>