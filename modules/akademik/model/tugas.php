<?php
class model_tugas extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function get_md5_tugas_id($id){
		$sql= "SELECT  mid(md5(`tugas_id`),6,6) as tugas_id
		       FROM db_ptiik_apps.`tbl_tugas` 
		       WHERE tugas_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->tugas_id;
			return $strresult;
		}
	}
	
	function get_tugas_id_by_md5($id){
		$sql= "SELECT tugas_id as tugas_id
		       FROM db_ptiik_apps.`tbl_tugas` 
		       WHERE mid(md5(`tugas_id`),6,6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->tugas_id;
			return $strresult;
		}
	}
	
	function get_materi_id_by_md5($id){
		$sql= "SELECT materi_id as materi_id
		       FROM db_ptiik_apps.`tbl_materimk` 
		       WHERE mid(md5(`materi_id`),6,6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->materi_id;
			return $strresult;
		}
	}
	
	function get_jadwal_id_by_md5($id){
		$sql= "SELECT jadwal_id as jadwal_id
		       FROM db_ptiik_apps.`tbl_jadwalmk` 
		       WHERE mid(md5(`jadwal_id`),6,6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
			$strresult = $dt->jadwal_id;
			return $strresult;
		}
	}
	
	function read($id){
		$sql = "SELECT MID( MD5( tbl_tugas.tugas_id ) , 6, 6 ) as tugasid,
					   MID( MD5( tbl_tugas.jadwal_id ) , 6, 6 ) as jadwalid,
					   MID( MD5( tbl_tugas.materi_id ) , 6, 6 ) as materiid,
					   tbl_tugas.tugas_id as tugas_id,
					   tbl_materimk.judul materi,
					   vw_mk_by_dosen.namamk namamk,
					   tbl_tugas.judul,
					   tbl_tugas.instruksi,
					   tbl_tugas.keterangan,
					   tbl_tugas.tgl_mulai,
					   tbl_tugas.tgl_selesai,
					   tbl_tugas.user_id as userid,
					   coms_user.username user,
					   tbl_tugas.last_update
				FROM db_ptiik_apps.`tbl_tugas`
				LEFT JOIN db_ptiik_apps.tbl_materimk ON tbl_materimk.materi_id = tbl_tugas.materi_id
				LEFT JOIN `db_ptiik_apps`.`vw_mk_by_dosen` ON vw_mk_by_dosen.jadwal_id = tbl_tugas.jadwal_id
				LEFT JOIN coms.coms_user ON tbl_tugas.user_id = coms_user.id
			   ";
		if($id!=""){
			$sql .= " WHERE MID( MD5(tugas_id) , 6, 6 ) = '".$id."'";
		}
			$sql .= " ORDER BY namamk, tugas_id ASC";
		$result = $this->db->query( $sql );
		if(isset($result)){
			return $result;
		}
	}
	
	function get_jadwal(){
		$sql = "SELECT MID( MD5( vw_mk_by_dosen.jadwal_id ) , 6, 6 ) as jadwal_id,
					   vw_mk_by_dosen.jadwal_id as jad,
					   MID( MD5( vw_mk_by_dosen.mkditawarkan_id ) , 6, 6 ) as mkid,
					   vw_mk_by_dosen.mkditawarkan_id, 
					   vw_mk_by_dosen.kelas, 
					   vw_mk_by_dosen.namamk,
					   vw_mk_by_dosen.nama,
					   CONCAT (vw_mk_by_dosen.namamk , ' ' , vw_mk_by_dosen.kelas , ' - ' , vw_mk_by_dosen.nama, ' - ', tbl_mkditawarkan.cabang_id) as jadwal
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan`
					ON vw_mk_by_dosen.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id
				";
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		return $result;
		}
	}
	
	function get_mk_by_jadwal($id=NULL){
		$sql = "SELECT MID( MD5( jadwal_id ) , 6, 6 ) as jadwal_id,
					   MID( MD5( mkditawarkan_id ) , 6, 6 ) as mkid,
					   mkditawarkan_id, 
					   kelas, 
					   namamk,
					   nama,
					   CONCAT (namamk , ' ' , kelas , ' - ' , nama) as jadwal
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				";
		//echo $sql;
		if($id!=""){
			$sql .= "WHERE MID( MD5( `jadwal_id` ) , 6, 6 ) = '".$id."'";
		}
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
			return $result;
		}
	}
	
	function get_materi_by_mk($id=NULL){
		$sql = "SELECT MID( MD5( `materi_id` ) , 6, 6 ) as `materi_id`, `judul` 
			    FROM db_ptiik_apps.`tbl_materimk` 
			    WHERE db_ptiik_apps.tbl_materimk.is_valid = 1
			   ";
		if($id!=""){
			$sql .= "AND MID( MD5( `mkditawarkan_id` ) , 6, 6 ) = '".$id."'";
		}
		$result = $this->db->query( $sql );
		if(isset($result)){
			return $result;
		}
	}
	
	function tugas_id(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tugas_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_tugas"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function cek_new_tugas_by_judul($ket){
		$sql = "SELECT judul 
				from `db_ptiik_apps`.`tbl_tugas` 
				where judul = '".$ket."' ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_id_tugas_by_id($ket){
		$sql = "SELECT tugas_id 
				from `db_ptiik_apps`.`tbl_tugas` 
				where judul = '".$ket."' ";
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->tugas_id;
			}
			return $id;
		}
	}
	
	function replace_tugas($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tugas',$datanya);
	}
	
}
?>