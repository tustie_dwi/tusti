<?php
class model_jeniskegiatan extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id){
		$sql = "SELECT mid(md5(`tbl_jeniskegiatan`.`jenis_kegiatan_id`),6,6) as `jenis_kegiatan_id`,
				`tbl_jeniskegiatan`.`jenis_kegiatan_id` as `hid_id`,
				`tbl_jeniskegiatan`.`keterangan`,
				`tbl_jeniskegiatan`.`kode_kegiatan`,
				`tbl_jeniskegiatan`.`kategori`
				FROM `db_ptiik_apps`.`tbl_jeniskegiatan`";
		if($id!=""){
			$sql=$sql . " WHERE mid(md5(`tbl_jeniskegiatan`.`jenis_kegiatan_id`),6,6)='".$id."' ";
		}
		$sql = $sql . "ORDER BY `tbl_jeniskegiatan`.`jenis_kegiatan_id` DESC";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_reg_number(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jenis_kegiatan_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_jeniskegiatan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function replace_jeniskegiatan($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_jeniskegiatan',$datanya);
	}
	
	function clean(){
		 $sql = "DELETE FROM `db_ptiik_apps`.`tbl_jeniskegiatan`";
		 $this->db->query( $sql );
	}

	function cekketjeniskegiatan($ket){
		$sql = "SELECT `tbl_jeniskegiatan`.`jenis_kegiatan_id`
				FROM `db_ptiik_apps`.`tbl_jeniskegiatan`
				WHERE `tbl_jeniskegiatan`.`keterangan` = '".$ket."'";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
			return $result = $id->jenis_kegiatan_id;
		}
	}
	
	function cekkodejeniskegiatan($kode){
		$sql = "SELECT `tbl_jeniskegiatan`.`jenis_kegiatan_id`
				FROM `db_ptiik_apps`.`tbl_jeniskegiatan`
				WHERE `tbl_jeniskegiatan`.`kode_kegiatan` = '".$kode."'";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
			return $result = $id->jenis_kegiatan_id;
		}
	}
	
	function cekkatjeniskegiatan($kat){
		$sql = "SELECT `tbl_jeniskegiatan`.`jenis_kegiatan_id`
				FROM `db_ptiik_apps`.`tbl_jeniskegiatan`
				WHERE `tbl_jeniskegiatan`.`kategori` = '".$kat."'";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
			return $result = $id->jenis_kegiatan_id;
		}
	}
}
?>