<?php
class model_komponennilai extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=null, $parent=null, $child=null, $cabang=NULL, $fakultas=NULL, $thnakademik=NULL, $nmk=NULL)
	{
		$sql="SELECT 
			  mid(md5(`tbl_komponen_nilai`.`komponen_id`),6,6) as komponen_id,
			  `db_ptiik_apps`.`tbl_komponen_nilai`.komponen_id as hid_id,
			  
			  (SELECT 
			  `db_ptiik_apps`.`tbl_namamk`.keterangan 
			  FROM 
			  `db_ptiik_apps`.`tbl_namamk`, 
			  `db_ptiik_apps`.`tbl_matakuliah`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan` 
			  WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			  AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id 
			  AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_komponen_nilai`.mkditawarkan_id ) as namamk,
			 
			  `db_ptiik_apps`.`tbl_komponen_nilai`.bobot,
			  `db_ptiik_apps`.`tbl_komponen_nilai`.keterangan,
			  `db_ptiik_apps`.`tbl_komponen_nilai`.parent_id,
			  `db_ptiik_apps`.`tbl_komponen_nilai`.mkditawarkan_id,
              mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) as hid_mkid,
              
               (SELECT 
              komponen.keterangan 
              FROM `db_ptiik_apps`.`tbl_komponen_nilai` as komponen 
              WHERE komponen.komponen_id = `tbl_komponen_nilai`.parent_id) as parentmk
			 
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai`
			  LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_komponen_nilai`.`mkditawarkan_id` 
			  WHERE 1
			 ";
		
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_komponen_nilai`.`komponen_id`),6,6)='".$id."' ";
		}
		
		if($parent!=""){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_komponen_nilai`.parent_id = '' ";
		}
		
		if($child!=""){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_komponen_nilai`.parent_id != '' ";
		}
		
		if($cabang!=""){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.cabang_id = '".$cabang."' ";
		}
		
		if($fakultas!=""){
			$sql=$sql . " AND (SELECT 
	        			    	mid(md5(`db_ptiik_apps`.`tbl_namamk`.fakultas_id),6,6) as fakultas_id
			        	 	   FROM 
			        	 		`db_ptiik_apps`.`tbl_namamk`, 
			        	 		`db_ptiik_apps`.`tbl_matakuliah` 
			        	 	   WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			        	 	   AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) = '".$fakultas."' ";
		}
		
		if($thnakademik!=""){
			$sql=$sql . " AND `tbl_mkditawarkan`.`tahun_akademik` LIKE '%".$thnakademik."%' ";
		}
		
		if($nmk!=""){
			$sql=$sql . " AND mid(md5(`tbl_komponen_nilai`.`mkditawarkan_id`),6,6) = '".$nmk."' ";
		}
		
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function cek_if_parent($id){
		$sql="SELECT `komponen_id` 
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai`
			  WHERE `parent_id` = '".$id."'
			 ";	 
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_all_mk($cabang=NULL, $fakultas=NULL, $thnakademik=NULL, $nmk=NULL){
		$sql="SELECT 
			  (SELECT 
			  `db_ptiik_apps`.`tbl_namamk`.keterangan 
			  FROM 
			  `db_ptiik_apps`.`tbl_namamk`, 
			  `db_ptiik_apps`.`tbl_matakuliah`, 
			  `db_ptiik_apps`.`tbl_mkditawarkan` 
			  WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			  AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id 
			  AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_komponen_nilai`.mkditawarkan_id ) as namamk,
			  
			  mid(md5(`tbl_komponen_nilai`.`mkditawarkan_id`),6,6) as mkid,
			  `db_ptiik_apps`.`tbl_komponen_nilai`.mkditawarkan_id,
			  `db_ptiik_apps`.`tbl_komponen_nilai`.`komponen_id`
			  
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai` 
			  LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_komponen_nilai`.`mkditawarkan_id` 
			  WHERE 1
			 ";
		if($cabang!=""){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.cabang_id = '".$cabang."' ";
		}
		
		if($fakultas!=""){
			$sql=$sql . " AND (SELECT 
	        			    	mid(md5(`db_ptiik_apps`.`tbl_namamk`.fakultas_id),6,6) as fakultas_id
			        	 	   FROM 
			        	 		`db_ptiik_apps`.`tbl_namamk`, 
			        	 		`db_ptiik_apps`.`tbl_matakuliah` 
			        	 	   WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			        	 	   AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) = '".$fakultas."' ";
		}
		
		if($thnakademik!=""){
			$sql=$sql . " AND `tbl_mkditawarkan`.`tahun_akademik` LIKE '%".$thnakademik."%' ";
		}
		
		if($nmk!=""){
			$sql=$sql . " AND mid(md5(`tbl_komponen_nilai`.`mkditawarkan_id`),6,6) = '".$nmk."' ";
		}
		
		$sql = $sql . " GROUP BY namamk ";
		// echo $sql;
		
		if($cabang==NULL&&$fakultas==NULL&&$thnakademik==NULL&&$nmk==NULL){
			return null;
		}
		else{
			$result = $this->db->query( $sql );
			return $result;
		}
	}
	
	function get_namamk($term){
		$sql="SELECT 
			  (SELECT 
			  `db_ptiik_apps`.`tbl_namamk`.keterangan 
			  FROM 
			  `db_ptiik_apps`.`tbl_namamk`, 
			  `db_ptiik_apps`.`tbl_matakuliah` 
			  WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id =  `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
			  AND  `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk,
			  mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) as `mkditawarkan_id`
			  
			  FROM `db_ptiik_apps`.`tbl_mkditawarkan`
		      WHERE mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) = '".$term."'
			 "; 
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function cek_namamk($term){
		$sql="SELECT mkditawarkan_id 
			  FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
			  WHERE 
			  mid(md5(`tbl_mkditawarkan`.`mkditawarkan_id`),6,6) = '".$term."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
		return $strresult;}
	}
	
	function cekkomponen($term){
		$sql="SELECT keterangan
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai` 
			  WHERE keterangan = '".$term."'
			  "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->keterangan;
		return $strresult;}
	}
	
	function cek_parent($term){
		$sql="SELECT parent_id 
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai` 
			  WHERE mid(md5(`tbl_komponen_nilai`.`komponen_id`),6,6) = '".$term."'
			  "; 
		// echo $sql;
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->parent_id;
		return $strresult;}
	}
	
	function get_komponen(){
		$sql="SELECT keterangan, komponen_id 
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai` 
			  WHERE 1
			 ";
		$result = $this->db->query($sql);
		return $result;
		
	}
	
	function get_all_thn_akademik(){
		$sql = "SELECT tahun_akademik, CONCAT(tahun , ' ' , is_ganjil , ' ' , is_pendek) as thnakademik FROM `db_ptiik_apps`.`tbl_tahunakademik` ORDER BY tahun_akademik DESC
				";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_reg_number(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(komponen_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_komponen_nilai"; 
	$dt = $this->db->getRow( $sql );
	
	$strresult = $dt->data;
	
	return $strresult;
	}
	
	function replace_komponennilai($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_komponen_nilai',$datanya);
	}
	
}