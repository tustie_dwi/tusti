<?php
class model_mk extends model {

	public function __construct() {
		parent::__construct();	
	}

//--------------------------------------------read-------------------------------------------	
	function read_namamk($id=NULL){
		$sql = "SELECT mid(md5(`db_ptiik_apps`.`tbl_namamk`.`namamk_id`),6,6) as namamk_id,
				`tbl_namamk`.`namamk_id` as hid_id,
				mid(md5(fakultas_id),6,6) as fakultas_id,
				`tbl_namamk`.`keterangan`,
				`tbl_namamk`.`english_version`,
                (select `db_ptiik_apps`.`tbl_fakultas`.keterangan 
                from `db_ptiik_apps`.`tbl_fakultas` 
                where `db_ptiik_apps`.`tbl_fakultas`.fakultas_id = `db_ptiik_apps`.`tbl_namamk`.`fakultas_id`) as fakultas
                
				FROM `db_ptiik_apps`.`tbl_namamk`
				
				WHERE 1
				";

		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_namamk`.`namamk_id`),6,6)='".$id."' ";
		}
		
		//echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function read_matakuliah($id=NULL){
		$sql = "SELECT mid(md5(`db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`),6,6) as matakuliah_id,
				`tbl_matakuliah`.`matakuliah_id` as hid_id,
				`tbl_matakuliah`.`namamk_id`,
				`tbl_matakuliah`.`kode_mk`,
				`tbl_matakuliah`.`kurikulum`,
                `tbl_matakuliah`.`sks`,
                
                (SELECT `db_ptiik_apps`.`tbl_namamk`.keterangan 
                	FROM `db_ptiik_apps`.`tbl_namamk` 
                	WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id`) as namamk,
                
                (SELECT `db_ptiik_apps`.`tbl_namamk`.english_version 
                	FROM `db_ptiik_apps`.`tbl_namamk` 
                	WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id`) as engver,
                
                (SELECT mid(md5(`db_ptiik_apps`.`tbl_fakultas`.fakultas_id),6,6) 
               		FROM `db_ptiik_apps`.`tbl_fakultas`, `db_ptiik_apps`.`tbl_namamk` 
                	WHERE `db_ptiik_apps`.`tbl_fakultas`.fakultas_id = `db_ptiik_apps`.`tbl_namamk`.`fakultas_id` 
                	AND `db_ptiik_apps`.`tbl_namamk`.`namamk_id` = `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id`) as fakultas_id,
                
                (SELECT `db_ptiik_apps`.`tbl_fakultas`.keterangan 
               		FROM `db_ptiik_apps`.`tbl_fakultas`, `db_ptiik_apps`.`tbl_namamk` 
                	WHERE `db_ptiik_apps`.`tbl_fakultas`.fakultas_id = `db_ptiik_apps`.`tbl_namamk`.`fakultas_id` 
                	AND `db_ptiik_apps`.`tbl_namamk`.`namamk_id` = `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id`) as fakultas
				
				FROM `db_ptiik_apps`.`tbl_matakuliah`
				WHERE 1
				";

		if($id!=""){
			$sql=$sql . " AND mid(md5(`db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`),6,6)='".$id."' ";
		}
		
		//echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
//--------------------------------------------end read-------------------------------------------	
	
	function replace_namamk($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_namamk',$datanya);
	}
	
	function replace_mk($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_matakuliah',$datanya);
	}
	
//-------------------------------------------------------------------------------------------------	

	function get_reg_number(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(namamk_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_namamk"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_reg_numbermk(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(matakuliah_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_matakuliah"; 
	$dt = $this->db->getRow( $sql );
	
	$strresult = $dt->data;
	
	return $strresult;
	}
	
	//-------------------------------------------------------------------------------------------------	
	
	function cek_mk_from_namamk($term=NULL){
		$sql = "SELECT `tbl_namamk`.`namamk_id`
				FROM `db_ptiik_apps`.`tbl_namamk`
				WHERE `tbl_namamk`.`keterangan` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->namamk_id;}
	}
	
	function cek_kode_mk_by_mkid($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`kode_mk`
				FROM `db_ptiik_apps`. `tbl_matakuliah`
				WHERE `tbl_matakuliah`.`matakuliah_id` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->kode_mk;}
	}
	
	function cek_kode_mk($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`kode_mk`
				FROM `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_matakuliah`.`kode_mk` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->kode_mk;
		}
	}
	
	function cek_kurikulum_mk($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`kurikulum`
				FROM `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_matakuliah`.`kurikulum` =  '".$term."'
				";
		$id = $this->db->getrow( $sql );
		return $result = $id->kurikulum;
	}
	
	function cek_nama_mk($term=NULL){
		$sql = "SELECT `tbl_namamk`.`namamk_id`
				FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
                AND `tbl_namamk`.`keterangan` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->namamk_id;}
	}
	
	function cek_mata_kuliah_id($term=NULL){
		$sql = "SELECT `tbl_matakuliah`.`matakuliah_id`
				FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
                AND `tbl_namamk`.`keterangan` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->matakuliah_id;}
	}
	
	function get_all_fakultas(){
		$sql = "SELECT mid(md5(fakultas_id),6,6) as fakultasid, keterangan
				FROM `db_ptiik_apps`.`tbl_fakultas`
				";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_fakultas_id($id=NULL){
		$sql = "SELECT fakultas_id
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE mid(md5(fakultas_id),6,6) = '".$id."'
				";		
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->fakultas_id;}
	}
	
}
?>