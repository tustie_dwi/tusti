<?php
class model_kalenderakademik extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	public $error;
	public $id;
	public $modified;

	public function read(){
		$que = "SELECT jenis.keterangan jenis_kegiatan, 
					tahun.tahun, 
					kalender.tgl_mulai, 
					kalender.tgl_selesai tgl_selesai, 
					kalender.is_aktif, 
					mid(md5(kalender.kalender_id),6,6) kalender_id,
					kalender.kalender_id hidId,
					kalender.jenis_kegiatan_id,
					kalender.tahun_akademik
			FROM `db_ptiik_apps`.`tbl_kalenderakademik` `kalender`, `db_ptiik_apps`.`tbl_jeniskegiatan` `jenis`, `db_ptiik_apps`.`tbl_tahunakademik` `tahun`
			WHERE kalender.jenis_kegiatan_id = jenis.jenis_kegiatan_id 
				AND kalender.tahun_akademik = tahun.tahun_akademik
				AND kalender.is_aktif = '1'";
		return $this->db->query($que);
	}
	
	// function clean(){
		// $que = "delete from `db_ptiik_apps`.`tbl_kalenderakademik`";
		// return $this->db->query($que);
	// }
	
	public function get_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kalender_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM `db_ptiik_apps`.`tbl_kalenderakademik`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	public function get_kegiatan(){
		$sql ="SELECT * FROM `db_ptiik_apps`.`tbl_jeniskegiatan` `k` WHERE k.kategori = 'akademik'";
		return $this->db->query($sql);
	}
	
	public function get_tahun(){
		$sql = "SELECT tahun.tahun, tahun.tahun_akademik FROM `db_ptiik_apps`.`tbl_tahunakademik` `tahun` WHERE tahun.is_aktif = '1'";
		return $this->db->query($sql);
	}
	
	public function get_jeniskegiatan(){
		$sql = "SELECT * FROM `db_ptiik_apps`.`tbl_jeniskegiatan` `jenis`";
		return $this->db->query($sql);
	}
	
	public function update_kalender(){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_kalenderakademik` SET `is_aktif` = '0'";
		$this->db->query($sql);
	}
	
	public function replace_kalender($data){
		$this->db->replace('db_ptiik_apps`.`tbl_kalenderakademik',$data);
	}
}