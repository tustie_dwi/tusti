<?php
class model_pembimbing extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT MID( MD5(tbl_dosen_pembimbing.mahasiswa_id), 6, 6) as mhs_id,
				MID( MD5(tbl_dosen_pembimbing.karyawan_id), 6, 6) as kar_id,
				tbl_dosen_pembimbing.tahun_akademik,
				tbl_dosen_pembimbing.karyawan_id,
				tbl_dosen_pembimbing.mahasiswa_id,
				tbl_mahasiswa.nim,
				tbl_mahasiswa.nama as nama,
				MID( MD5(tbl_karyawan.fakultas_id), 6, 6) as fakultas_id,
				CONCAT (IFNULL(tbl_karyawan.gelar_awal,''),' ',tbl_karyawan.nama,' ',IFNULL(tbl_karyawan.gelar_akhir,'')) as dosen,
				CONCAT (tbl_tahunakademik.tahun,' ',tbl_tahunakademik.is_ganjil,' ',tbl_tahunakademik.is_pendek) as tahun
				FROM `db_ptiik_apps`.`tbl_dosen_pembimbing` tbl_dosen_pembimbing
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa 
					ON tbl_dosen_pembimbing.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik 
					ON tbl_dosen_pembimbing.tahun_akademik = tbl_tahunakademik.tahun_akademik
				LEFT JOIN `db_ptiik_apps`.`tbl_karyawan` tbl_karyawan 
					ON tbl_dosen_pembimbing.karyawan_id = tbl_karyawan.karyawan_id
					WHERE 1
				";
		if($id!=""){
			$sql = $sql . " AND MID( MD5(tbl_dosen_pembimbing.mahasiswa_id), 6, 6) = '".$id."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_dosen($fakultas_id=NULL, $id=NULL){
		$sql = "SELECT MID( MD5(karyawan_id), 6, 6) as kar_id,
				karyawan_id as hid_id,
				CONCAT (IFNULL(tbl_karyawan.gelar_awal,''),' ',tbl_karyawan.nama,' ',IFNULL(tbl_karyawan.gelar_akhir,'')) as nama
				FROM `db_ptiik_apps`.`tbl_karyawan`";
		
		if($fakultas_id!=""){
			$sql .= " WHERE  MID( MD5(fakultas_id), 6, 6) = '".$fakultas_id."' ";
			$result = $this->db->query( $sql );
			return $result;	
		}
		elseif($id!=""){
			$sql .= " WHERE  MID( MD5(karyawan_id), 6, 6) = '".$id."' ";
			$s = $this->db->getRow( $sql );
			$result = $s->hid_id;
			return $result;	
		}
		else{
			 $result = $this->db->query( $sql );
		     return $result;
		}
		
	}
	
	function get_mhs_nama($id=NULL){
		$sql = "SELECT tbl_mahasiswa.nama as nama
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				WHERE tbl_mahasiswa.mahasiswa_id = '".$id."' ";
				
		$s = $this->db->getRow( $sql );
		if(isset($s)){
			$result = $s->nama;
			return $result;
		}
	}
	
	function get_mhs_by_fakultas($id=NULL,$term=NULL){
		$sql = "SELECT concat(nim, ' - ', nama) as value,
					   nim,
					   mahasiswa_id as hid_id,
					   MID( MD5(mahasiswa_id), 6, 6) as mhs_id
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				WHERE MID( MD5(tbl_prodi.fakultas_id), 6, 6) = '".$id."' 
					  AND tbl_mahasiswa.is_aktif = 'aktif'
					  AND nama LIKE '%".$term."%'
				";
				
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	
	}
	
	function get_thn_akademik(){
		$sql = "SELECT tahun_akademik as hid_id, 
					   MID( MD5(tahun_akademik), 6, 6) as thn_id,
					   concat(tahun, ' ', is_ganjil, ' ', is_pendek) as tahun
				FROM `db_ptiik_apps`.`tbl_tahunakademik` tbl_tahunakademik
				WHERE 1
				ORDER BY tahun_akademik DESC";
				
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function delete($id){
		$sql = "DELETE FROM `db_ptiik_apps`.`tbl_dosen_pembimbing` 
				WHERE `db_ptiik_apps`.`tbl_dosen_pembimbing`.`mahasiswa_id` = '".$id."'
				";
		$result = $this->db->query($sql);
		return $result;
	}
	
	function cekbyid($mahasiswa_id=NULL){
		$sql = "SELECT mahasiswa_id 
				from `db_ptiik_apps`.`tbl_dosen_pembimbing` 
				where mahasiswa_id = '".$mahasiswa_id."'";

		$s = $this->db->getRow( $sql );
		if(isset($s)){
			$result = $s->mahasiswa_id;
			return $result;
		}
	}
	
	function cek_by_id_before_save($mahasiswa_id=NULL){
		$sql = "SELECT mahasiswa_id 
				from `db_ptiik_apps`.`tbl_mahasiswa` 
				where mahasiswa_id = '".$mahasiswa_id."'";

		$s = $this->db->getRow( $sql );
		if(isset($s)){
			$result = $s->mahasiswa_id;
			return $result;
		}
	}
	
	function replace_pembimbing($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_dosen_pembimbing',$datanya);
	}
}