<?php
class model_jenisfile extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id){
		$sql = "SELECT MID( MD5(jenis_file_id), 6, 6) as jenis_id,
				jenis_file_id as hid_id,
				keterangan as jenis,
				kode_jenis as kode,
				extention as ext,
				max_size as max
				FROM `db_ptiik_apps`.`tbl_jenisfile`";
		if($id!=""){
			$sql = $sql . " WHERE  MID( MD5(jenis_file_id), 6, 6) = '".$id."' ";
		}
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function id_jenisfile($semester=NULL){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jenis_file_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_jenisfile`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function ceknew($ket){
		$sql = "SELECT keterangan 
				from `db_ptiik_apps`.`tbl_jenisfile` 
				where keterangan = '".$ket."'";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cekbyid($ket){
		$sql = "SELECT jenis_file_id 
				from `db_ptiik_apps`.`tbl_jenisfile` 
				where keterangan = '".$ket."'";
		$result = $this->db->query( $sql );
		if(isset($result)){
		foreach($result as $dt){
			$id=$dt->jenis_file_id;
		}
		return $id;
		}
	}
	
	function replace_jenisfile($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_jenisfile',$datanya);
	}
}