<?php
class model_komponen extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id){
		$sql = "SELECT mid(md5(`tbl_komponen_nilai`.`komponen_id`),6,6) as komponen_id,
				`tbl_komponen_nilai`.`komponen_id` as hid_id,
				`db_ptiik_apps`.`tbl_komponen_nilai`.keterangan,
				bobot,
				(SELECT a.keterangan FROM 
				`db_ptiik_apps`.`tbl_namamk` as a,
				`db_ptiik_apps`.`tbl_matakuliah` as b,
				`db_ptiik_apps`.`tbl_mkditawarkan` as c
				WHERE 
				a.namamk_id = b.namamk_id
				AND b.matakuliah_id=c.matakuliah_id
				AND c.mkditawarkan_id = `db_ptiik_apps`.`tbl_komponen_nilai`.mkditawarkan_id) as namamk,
				(SELECT a.mkditawarkan_id FROM `db_ptiik_apps`.`tbl_mkditawarkan` as a WHERE a.mkditawarkan_id = `db_ptiik_apps`.`tbl_komponen_nilai`.mkditawarkan_id) as mkditawarkanid
				FROM `db_ptiik_apps`.`tbl_komponen_nilai`
				WHERE 1
				";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_komponen_nilai`.`komponen_id`),6,6) = '".$id."' ";
		}
		
		$sql = $sql . "ORDER BY `tbl_komponen_nilai`.`komponen_id` DESC";
		
		//echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_reg_number(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(komponen_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_komponen_nilai"; 
	$dt = $this->db->getRow( $sql );
	
	$strresult = $dt->data;
	
	return $strresult;
	}
	
	function getdatamk($id){
		$sql="SELECT a.mkditawarkan_id as id,
			 (SELECT c.keterangan FROM `db_ptiik_apps`.`tbl_matakuliah` as b,`db_ptiik_apps`.`tbl_namamk` as c WHERE c.namamk_id = b.namamk_id AND b.matakuliah_id = a.matakuliah_id) as namamk
			 FROM `db_ptiik_apps`.`tbl_mkditawarkan` as a
			 WHERE mid(md5(a.`mkditawarkan_id`),6,6) = '".$id."'
			 "; 
	$result = $this->db->query( $sql );
	return $result;
	}
	
	function cekkomponennilai($id){
		$sql="SELECT komponen_id
				FROM `db_ptiik_apps`.`tbl_komponen_nilai`
				WHERE mid(md5(`tbl_komponen_nilai`.`mkditawarkan_id`),6,6)='".$id."'
			"; 
	$dt = $this->db->getRow( $sql );
	if(isset($dt)){
	$strresult = $dt->komponen_id;
	
	return $strresult;}
	}
	
	function replace_komponen($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_komponen_nilai',$datanya);
	}
	
	function ceknamamk($term){
		$sql = "SELECT `tbl_namamk`.`namamk_id`
				FROM `db_ptiik_apps`.`tbl_namamk`, `db_ptiik_apps`.`tbl_matakuliah`
				WHERE `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
                AND `tbl_namamk`.`keterangan` = '".$term."'
				";
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->namamk_id;}
	}
	
}

?>