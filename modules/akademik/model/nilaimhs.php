<?php
class model_nilaimhs extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function read($id)
	{
		$sql="SELECT mid(md5(`tbl_nilai_mhs`.`nilai_id`),6,6) as nilai_id,
			  nilai_id as hid_id,
			  (SELECT a.keterangan FROM `db_ptiik_apps`.`tbl_namamk` as a, `db_ptiik_apps`.`tbl_matakuliah` as b, `db_ptiik_apps`.`tbl_mkditawarkan` as c, `db_ptiik_apps`.`tbl_komponen_nilai` as d WHERE a.namamk_id = b.namamk_id AND b.matakuliah_id = c.matakuliah_id AND c.mkditawarkan_id = d.mkditawarkan_id AND d.komponen_id = `tbl_nilai_mhs`.komponen_id) as namamk,
			  (SELECT a.keterangan FROM `db_ptiik_apps`.`tbl_komponen_nilai` as a WHERE a.komponen_id = `tbl_nilai_mhs`.komponen_id ) as komponen_nilai,
              (SELECT concat(a.nim, ' - ', a.nama) FROM `db_ptiik_apps`.`tbl_mahasiswa` as a, `db_ptiik_apps`.`tbl_krs` as b WHERE a.mahasiswa_id = b.mahasiswa_id AND b.krs_id = `tbl_nilai_mhs`.krs_id ) as namamhs,
			  komponen_id,
			  nilai,
			  (SELECT a.username FROM `coms`.`coms_user` as a WHERE a.id =  `tbl_nilai_mhs`.user_id) as user_id,
			  `tbl_nilai_mhs`.last_update,
			  substring(`db_ptiik_apps`.`tbl_nilai_mhs`.last_update, 1,10) as YMD,
			  substring(`db_ptiik_apps`.`tbl_nilai_mhs`.last_update, 12,8) as waktu
		      FROM `db_ptiik_apps`.`tbl_nilai_mhs` 
			  WHERE 1";
		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_nilai_mhs`.`nilai_id`),6,6)='".$id."' ";
		}
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function ceknamamk($term){
		$sql="SELECT mkditawarkan_id 
			  FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
			  WHERE (SELECT a.keterangan 
				     FROM `db_ptiik_apps`.`tbl_namamk` as a, `db_ptiik_apps`.`tbl_matakuliah` as b 
				     WHERE a.namamk_id = b.namamk_id AND b.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id ) = '".$term."'"; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
		return $strresult;}
	}
	
	function get_userid($term){
		$sql="SELECT user_id 
			  FROM `tbl_user` 
			  WHERE user_name = '".$term."'"; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->user_id;
		return $strresult;}
	}
	
	function get_idmhs($nim, $nama){
		$sql="SELECT mahasiswa_id 
			  FROM `db_ptiik_apps`.`tbl_mahasiswa` 
			  WHERE nim = '".$nim."'
              AND nama = '".$nama."'"; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mahasiswa_id;
		return $strresult;}
	}
	
	function get_krsid($mk, $id){
		$sql="SELECT krs_id 
			  FROM `db_ptiik_apps`.`tbl_krs` 
			  WHERE mkditawarkan_id = '".$mk."'
			  AND mahasiswa_id = '".$id."'"; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->krs_id;
		return $strresult;}
	}
	
	function getkomponen($term){
		$sql="SELECT a.keterangan, a.komponen_id 
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai` as a, `db_ptiik_apps`.`tbl_jadwalmk` as b, `db_ptiik_apps`.`tbl_mkditawarkan` as c
			  WHERE mid(md5(b.`jadwal_id`),6,6)= '".$term."'
              AND b.mkditawarkan_id = c.mkditawarkan_id
              AND c.mkditawarkan_id = a.mkditawarkan_id
			 ";
		$result = $this->db->query($sql);
		return $result;
		
	}
	
	function getkomponenfromnilai($term){
		$sql="SELECT a.keterangan, a.komponen_id 
			  FROM `db_ptiik_apps`.`tbl_komponen_nilai` as a, `db_ptiik_apps`.`tbl_jadwalmk` as b, `db_ptiik_apps`.`tbl_mkditawarkan` as c, `db_ptiik_apps`.`tbl_nilai_mhs` as d
			  WHERE mid(md5(d.`nilai_id`),6,6)= '".$term."'
              AND b.mkditawarkan_id = c.mkditawarkan_id
              AND c.mkditawarkan_id = a.mkditawarkan_id
              AND b.jadwal_id = d.jadwal_id
			 ";
		$result = $this->db->query($sql);
		return $result;
		
	}
	
	function getjadwal($id){
		$sql="SELECT jadwal_id,
			  mkditawarkan_id,
			  concat(hari, ' ' ,kelas,  ' [' ,jam_mulai, '-' ,jam_selesai, ']') as jadwal,
			  (SELECT a.keterangan FROM `db_ptiik_apps`.`tbl_namamk` as a, `db_ptiik_apps`.`tbl_matakuliah` as b, `db_ptiik_apps`.`tbl_mkditawarkan` as c WHERE a.namamk_id = b.namamk_id AND b.matakuliah_id = c.matakuliah_id AND c.mkditawarkan_id = `db_ptiik_apps`.`tbl_jadwalmk`.mkditawarkan_id ) as namamk
			  FROM `db_ptiik_apps`.`tbl_jadwalmk` 
			  WHERE mid(md5(`tbl_jadwalmk`.`jadwal_id`),6,6) = '".$id."'
			 ";
		$result = $this->db->query($sql);
		return $result;
		
	}
	
	function getjadwalfromnilai($id){
		$sql="SELECT a.jadwal_id,
			  b.mkditawarkan_id,
			  concat(hari, ' ' ,kelas,  ' [' ,jam_mulai, '-' ,jam_selesai, ']') as jadwal,
			  (SELECT a.keterangan FROM `db_ptiik_apps`.`tbl_namamk` as a, `db_ptiik_apps`.`tbl_matakuliah` as b, `db_ptiik_apps`.`tbl_mkditawarkan` as c WHERE a.namamk_id = b.namamk_id AND b.matakuliah_id = c.matakuliah_id AND c.mkditawarkan_id = b.mkditawarkan_id ) as namamk
			  FROM `db_ptiik_apps`.`tbl_nilai_mhs` as a, `db_ptiik_apps`.`tbl_jadwalmk` as b 
			  WHERE b.jadwal_id = a.jadwal_id
                          AND mid(md5(a.nilai_id),6,6) = '".$id."'
			 ";
		$result = $this->db->query($sql);
		return $result;
		
	}
	
	function get_reg_number(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(nilai_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_nilai_mhs"; 
	$dt = $this->db->getRow( $sql );
	
	$strresult = $dt->data;
	
	return $strresult;
	}
	
	function replace_nilaimhs($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_nilai_mhs',$datanya);
	}
	
}