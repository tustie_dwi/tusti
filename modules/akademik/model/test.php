<?php
class model_test extends model {
	private $coms;

	public function __construct() {
		parent::__construct();	
	}
	
	function get_mdtestid($id){
		$sql= "SELECT  mid(md5(`test_id`),6,6) as test_id
		       FROM db_ptiik_apps.`tbl_test` 
		       WHERE test_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->test_id;
	
		return $strresult;}
	}
	
	function read($id){
		$sql = "SELECT  mid(md5(`test_id`),6,6) as test_id,
				test_id as hid_id,
				mid(md5(`mkditawarkan_id`),6,6) as mkditawarkan_id,
				mid(md5(`materi_id`),6,6) as materi_id,
				mid(md5(`jadwal_id`),6,6) as jadwal_id,
				`judul`,
				`tgl_mulai`,
				`tgl_selesai`,
				`instruksi`,
				`keterangan`,
				`is_random`,
				`is_publish`,
				
				(SELECT 
				`db_ptiik_apps`.`tbl_materimk`.judul 
				FROM `db_ptiik_apps`.`tbl_materimk` 
				WHERE `db_ptiik_apps`.`tbl_materimk`.materi_id = `db_ptiik_apps`.`tbl_test`.materi_id ) as materi,
				
				(SELECT 
				`db_ptiik_apps`.`tbl_namamk`.keterangan 
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah`, 
				`db_ptiik_apps`.`tbl_mkditawarkan` 
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id 
				AND `db_ptiik_apps`.`tbl_mkditawarkan`.mkditawarkan_id = `db_ptiik_apps`.`tbl_test`.mkditawarkan_id ) as namamk,
				
				last_update,
              	substring(`tbl_test`.last_update, 1,10) as YMD,
        	  	substring(`tbl_test`.last_update, 12,8) as waktu,
        	  	
        	  	(SELECT a.username FROM `coms`.`coms_user` as a WHERE a.id =  `tbl_test`.user_id) as user
				
				FROM `db_ptiik_apps`.`tbl_test` 
				WHERE 1
				";

		if($id!=""){
			$sql=$sql . " AND mid(md5(`tbl_test`.`test_id`),6,6)='".$id."' ";
		}
		
		//echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	function get_namamk($id, $cek){
		$sql = "SELECT 
				(SELECT 
				`db_ptiik_apps`.`tbl_namamk`.keterangan 
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah`
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id 
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk
				FROM `db_ptiik_apps`.`tbl_mkditawarkan`
				WHERE 1
				";
				
		if($cek==""){
			$sql=$sql . " AND `mkditawarkan_id` = '".$id."' ";
		}
		else {
			$sql=$sql . " AND MID( MD5( `mkditawarkan_id` ) , 6, 6 ) = '".$id."' ";
		}
		
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		
		$strresult = $dt->namamk;
	
		return $strresult;}
	}
	
	function get_jadwal_data($id){
		$sql = "SELECT MID( MD5( jadwal_id ) , 6, 6 ) as hid_jadwal,
					   MID( MD5( mkditawarkan_id ) , 6, 6 ) as hid_mkid,
					   mkditawarkan_id, 
					   kelas, 
					   namamk,
					   nama
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				WHERE MID( MD5( `vw_mk_by_dosen`.`jadwal_id` ) , 6, 6 ) = '".$id."'
				";
		//echo $sql;
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		return $result;
		}
	}
	
	function get_materi($id, $cek){
		$sql = "SELECT MID( MD5( `materi_id` ) , 6, 6 ) as `materi_id`, `judul` 
			    FROM db_ptiik_apps.`tbl_materimk` 
			    WHERE `parent_id` =  '0' AND db_ptiik_apps.tbl_materimk.is_valid = 1
				";
		
		if($cek==""){
			$sql=$sql . " AND `mkditawarkan_id` = '".$id."' ";
		}
		else {
			$sql=$sql . " AND MID( MD5( `mkditawarkan_id` ) , 6, 6 ) = '".$id."' ";
		}
		//echo $sql;
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		return $result;
		}
	}
	
	function get_mkid_by_materi($id){
		$sql= "SELECT MID( MD5(mkditawarkan_id), 6, 6) as mkditawarkan_id
		       FROM db_ptiik_apps.`tbl_materimk` 
		       WHERE MID( MD5(materi_id), 6, 6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
	
		return $strresult;}
	}
	
	function get_reg_number(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(test_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_test"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function replace_test($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_test',$datanya);
	}
	
	
	/*-----------------------------------------------------------------*/
	
	function get_jadwal_id($id){
		$sql= "SELECT jadwal_id 
			   FROM db_ptiik_apps.`tbl_jadwalmk` 
			   WHERE MID( MD5(jadwal_id), 6, 6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		
		$strresult = $dt->jadwal_id;
	
		return $strresult;}
	}
	
	function get_mkid($id){
		$sql= "SELECT mkditawarkan_id 
		       FROM db_ptiik_apps.`tbl_mkditawarkan` 
		       WHERE MID( MD5(mkditawarkan_id), 6, 6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->mkditawarkan_id;
	
		return $strresult;}
	}
	
	function get_materi_id($id){
		$sql= "SELECT materi_id 
		       FROM db_ptiik_apps.`tbl_materimk` 
		       WHERE MID( MD5(materi_id), 6, 6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->materi_id;
	
		return $strresult;}
	}
	//=========KATEGORI=========//
	
	function readkategori($id=NULL){
		$sql = "SELECT MID( MD5(kategori_id), 6, 6) as kat_id,
					   kategori_id as hid_id,
					   keterangan as kategori,
					   parameter_input as jenis_input
				FROM `db_ptiik_apps`.`tbl_test_kategori`";
		if($id!=""){
			$sql = $sql . " WHERE  MID( MD5(kategori_id), 6, 6) = '".$id."' ";
		}

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function getparam_by_soal($id=NULL){
		$sql="SELECT (SELECT a.parameter_input FROM `db_ptiik_apps`.`tbl_test_kategori` as a WHERE a.`kategori_id` = `db_ptiik_apps`.`tbl_test_soal`.`kategori_id`) as param
			  FROM `db_ptiik_apps`.`tbl_test_soal` 
			  WHERE MID( MD5(soal_id), 6, 6) = '".$id."'"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->param;
		
		return $strresult;
	}
	
	function id_testkategori($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kategori_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_test_kategori`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function ceknewkategori($ket){
		$sql = "SELECT keterangan from `db_ptiik_apps`.`tbl_test_kategori` where keterangan = '".$ket."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cekbyidkategori($ket){
		$sql = "SELECT kategori_id from `db_ptiik_apps`.`tbl_test_kategori` where keterangan = '".$ket."'";
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		foreach($result as $dt){
			$id=$dt->kategori_id;
		}
		return $id;
		}
	}
	
	function replace_test_kategori($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_kategori',$datanya);
	}
	
	//=========END-KATEGORI========//
	
	//------SOAL-----------------------------------------------------//
	function readsoal($id){
		$sql = "SELECT MID( MD5(tbl_test_soal.soal_id), 6, 6) as soal_id,
					tbl_test_soal.soal_id as hid_id,
					tbl_test_soal.jadwal_id,
					tbl_test_soal.materi_id,
					tbl_test_soal.mkditawarkan_id,
					tbl_test_soal.test_id,
					MID( MD5(tbl_test_soal.test_id), 6, 6) as testid,
					tbl_test_soal.pertanyaan,
					tbl_test_soal.kategori_id,
					tbl_test.judul as judul_tes,
				    tbl_namamk.keterangan as mata_kuliah,
				    (select judul from `db_ptiik_apps`.`tbl_materimk` where materi_id = tbl_test_soal.materi_id) as materi,
				    (select concat(mata_kuliah, ' ', kelas) from `db_ptiik_apps`.`tbl_jadwalmk` where jadwal_id = tbl_test_soal.jadwal_id) as jadwal,
				    tbl_test_kategori.keterangan as kategori,
				    (select username from coms.coms_user where id = tbl_test_soal.user_id) as user
				FROM `db_ptiik_apps`.`tbl_test_soal`,
				     `db_ptiik_apps`.`tbl_mkditawarkan`,
				     `db_ptiik_apps`.`tbl_matakuliah`,
				     `db_ptiik_apps`.`tbl_namamk`,
				     `db_ptiik_apps`.`tbl_test_kategori`,
				     `db_ptiik_apps`.`tbl_test`
				WHERE tbl_test_soal.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
				      AND tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				      AND tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				      AND tbl_test_soal.kategori_id = tbl_test_kategori.kategori_id
				      AND tbl_test_soal.test_id = tbl_test.test_id";
		if($id!=""){
			$sql = $sql . " AND  MID( MD5(soal_id), 6, 6) = '".$id."' ";
		}
			$sql = $sql . " ORDER BY hid_id ASC ";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_test(){
		$sql = "SELECT MID( MD5(test_id), 6, 6) as test_id,
				       test_id as hid_id,
				       jadwal_id,
				       judul
				FROM `db_ptiik_apps`.`tbl_test`
			";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function id_soal($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(soal_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_test_soal`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_soal($id){
		$sql =	"SELECT	MID( MD5(soal_id), 6, 6) as soal_id,
						`pertanyaan`,
						(select a.is_random from `db_ptiik_apps`.`tbl_test` as a where a.test_id = `db_ptiik_apps`.`tbl_test_soal`.`test_id` ) as is_random,
                        (SELECT `parameter_input` FROM `db_ptiik_apps`.`tbl_test_kategori` as a WHERE a.`kategori_id` = `db_ptiik_apps`.`tbl_test_soal`.`kategori_id` ) as kategori
				FROM `db_ptiik_apps`.`tbl_test_soal`
				WHERE MID( MD5(`test_id`), 6, 6) = '".$id."'
				order by case when `is_random` = '1' then rand()
                        		ELSE 0
						  END
				";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function delete_soal($id){
		$sql =	"DELETE FROM `db_ptiik_apps`.`tbl_test_soal` WHERE MID( MD5(soal_id), 6, 6) = '".$id."'
				";
		$result = $this->db->query( $sql );
		if($result){
			$this->deleteBySoalId($id);
			return TRUE;
		}
	}
	
	function ceknewsoal($ket, $testid){
		$sql = "SELECT pertanyaan from `db_ptiik_apps`.`tbl_test_soal` where pertanyaan = '".$ket."' AND test_id='".$testid."' ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cekbyidsoal($ket, $testid){
		$sql = "SELECT soal_id from `db_ptiik_apps`.`tbl_test_soal` where pertanyaan = '".$ket."' AND test_id='".$testid."' ";
		
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		foreach($result as $dt){
			$id=$dt->soal_id;
		}
		return $id;
		}
	}
	
	function replace_test_soal($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_soal',$datanya);
	}
	
	function get_detail_test($id=NULL){
		$sql = "SELECT MID( MD5(t.test_id), 6, 6) as test_id,
					t.test_id as hid_id,
					t.jadwal_id,
					t.materi_id,
					t.mkditawarkan_id,
					t.judul,
				    nmk.keterangan as mata_kuliah,
				    (select judul from `db_ptiik_apps`.`tbl_materimk` where materi_id = t.materi_id) as materi,
				    (select concat(mata_kuliah, ' ', kelas) from `db_ptiik_apps`.`tbl_jadwalmk` where jadwal_id = t.jadwal_id) as jadwal
				FROM `db_ptiik_apps`.`tbl_test` t,
				     `db_ptiik_apps`.`tbl_mkditawarkan` mkd,
				     `db_ptiik_apps`.`tbl_matakuliah` mk,
				     `db_ptiik_apps`.`tbl_namamk` nmk
				WHERE t.mkditawarkan_id = mkd.mkditawarkan_id 
				      AND mkd.matakuliah_id = mk.matakuliah_id
				      AND mk.namamk_id = nmk.namamk_id
				      AND t.test_id = '".$id."'
			";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_namamkid($id=NULL){
		$sql = "SELECT 
				(SELECT `db_ptiik_apps`.`tbl_namamk`.namamk_id
				FROM 
				`db_ptiik_apps`.`tbl_namamk`, 
				`db_ptiik_apps`.`tbl_matakuliah` 
				WHERE `db_ptiik_apps`.`tbl_namamk`.namamk_id = `db_ptiik_apps`.`tbl_matakuliah`.namamk_id
				AND `db_ptiik_apps`.`tbl_matakuliah`.matakuliah_id = `db_ptiik_apps`.`tbl_mkditawarkan`.matakuliah_id) as namamk_id
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` 
				WHERE mkditawarkan_id = '".$id."'
			   ";
			   
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->namamk_id;
	
		return $strresult;}
	}
	//------SOAL-----------------------------------------------------//
	
	//------JAWAB-----------------------------------------------------//	
	function get_jawaban(){
		$sql =	"SELECT MID( MD5(soal_id), 6, 6) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_test_jawab` 
			     WHERE 1
			     order by case when `is_random` = '1' then rand()
                        		ELSE 0
						  END
				";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_jawaban_edit($id){
		$sql =	"SELECT MID( MD5(jawaban_id), 6, 6) as jawab_id, MID( MD5(soal_id), 6, 6) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_test_jawab` 
			     WHERE MID( MD5(soal_id), 6, 6) = '".$id."'
			     OR soal_id = '".$id."';
				";
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function get_kategori_jawab(){
		$sql = "SELECT 
					MID( MD5(`kategori_id`), 6, 6) as kategoriid,`keterangan` 
					FROM `db_ptiik_apps`.`tbl_test_kategori` 
				WHERE 1
				";
		//echo $sql;
		$result = $this->db->query( $sql );
		
		if(isset($result)){
		return $result;
		}
	}
	
	function get_is_random($id){
		$sql= "SELECT DISTINCT `is_random` FROM `db_ptiik_apps`.`tbl_test_jawab` WHERE MID( MD5(`soal_id`), 6, 6) = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->is_random;
	
		return $strresult;}
	}
	
	function getparam_input($id){
		$sql= "SELECT `parameter_input`
		       FROM db_ptiik_apps.`tbl_test_kategori` 
		       WHERE MID( MD5(`kategori_id`), 6, 6) = '".$id."'
		       OR kategori_id = '".$id."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if(isset($dt)){
		$strresult = $dt->parameter_input;
	
		return $strresult;}
	}
	
	function deleteById($id){
		$sql= "DELETE FROM `db_ptiik_apps`.`tbl_test_jawab` WHERE MID( MD5(`jawaban_id`), 6, 6) = '".$id."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;}
		else {
			return FALSE;
		}
	}
	
	function deleteBySoalId($id){
		$sql= "DELETE FROM `db_ptiik_apps`.`tbl_test_jawab` WHERE `soal_id` = '".$id."' OR MID( MD5(`soal_id`), 6, 6) = '".$id."'
			 "; 
		if($this->db->query( $sql )){
			return TRUE;}
		else {
			return FALSE;
		}
	}
	
	function get_jawabid($id=NULL, $soalid=NULL){
		$sql= "SELECT jawaban_id 
		       FROM `db_ptiik_apps`.`tbl_test_jawab`
		       WHERE MID( MD5(jawaban_id), 6, 6) = '".$id."' AND soal_id = '".$soalid."'
			 "; 
		$dt = $this->db->getRow( $sql );
		if($dt){
			$strresult = $dt->jawaban_id;
		}
		else{
			$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jawaban_id,4) AS 
				  unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				  FROM db_ptiik_apps.tbl_test_jawab";
			
			$dt = $this->db->getRow( $sql );
		
			$strresult = $dt->data;
		}
		return $strresult;
	}
	
	function get_id_jawaban(){
	$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(jawaban_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_test_jawab"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function replace_test_jawab($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_test_jawab',$datanya);
	}
	//------JAWAB-----------------------------------------------------//
	
	
	//------TEST HASIL-----------------------------------------------------//
	
	function get_test_hasil($id){
		$que = "SELECT 
					mid(md5(t.test_id),6,6) test_id,
					t.test_id as hid_id,
					t.jadwal_id,
					t.materi_id,
					t.mkditawarkan_id,
					t.judul judul_test,
					t.tgl_mulai,
					t.tgl_selesai,
					t.instruksi,
					t.keterangan,
					t.is_random,
					t.is_publish,
					t.user_id,
					t.last_update,
					m.judul									
				FROM `db_ptiik_apps`.`tbl_test` `t`
				LEFT JOIN `db_ptiik_apps`.`tbl_materimk` `m` ON t.materi_id = m.materi_id
			";
		if($id != ''){
			$que .= " WHERE mid(md5(t.test_id),6,6) = '".$id."'";
		}
		return $this->db->query($que);
	}	
	
	
	function get_soal_hasil($id){
		$que = "SELECT 
					mid(md5(s.test_id),6,6) test_id,
					mid(md5(s.soal_id),6,6) soalid,
					s.soal_id,
					s.pertanyaan,
					k.parameter_input kategori
				FROM `db_ptiik_apps`.`tbl_test_soal` `s`
				LEFT JOIN `db_ptiik_apps`.`tbl_test` `t` ON s.test_id = t.test_id
				LEFT JOIN `db_ptiik_apps`.`tbl_test_kategori` `k` ON s.kategori_id = k.kategori_id
				WHERE mid(md5(s.test_id),6,6) = '".$id."'";
			
		return $this->db->query($que);
	}
	
	function get_jawaban_hasil($id){
		$que = "SELECT 
			        s.soal_id,
			        j.keterangan,
			        j.is_benar,
			        j.jawaban_id
				FROM `db_ptiik_apps`.`tbl_test_soal` `s`, `db_ptiik_apps`.`tbl_test_jawab` `j`
				WHERE mid(md5(s.test_id),6,6) = '".$id."'
					AND s.soal_id = j.soal_id";
				
		return $this->db->query($que);
	}
	
	function get_peserta_hasil($id){
		$que = "SELECT 
					mid(md5(s.test_id),6,6) test_id,
					s.mahasiswa_id,
					s.tgl_test,
					s.jam_mulai,
					s.jam_selesai,
					s.max_post,
					s.total_skor
				FROM `db_ptiik_apps`.`tbl_test_hasil` `s`
				LEFT JOIN `db_ptiik_apps`.`tbl_test` `t` ON s.test_id = t.test_id
				WHERE mid(md5(s.test_id),6,6) = '".$id."'";
			
		return $this->db->query($que);
	}
	
	
	function get_total_peserta_hasil($id){
		$que = "SELECT count(*) as total_peserta FROM `db_ptiik_apps`.`tbl_test_hasil` `s`
				LEFT JOIN `db_ptiik_apps`.`tbl_test` `t` ON s.test_id = t.test_id
				WHERE mid(md5(s.test_id),6,6) = '".$id."'";
			
		return $this->db->query($que);
	}
	//------TEST HASIL-----------------------------------------------------//
	
	//------TEST BANK-----------------------------------------------------//
	function read_bank_soal($id){
		$sql = "SELECT MID( MD5(tbl_bank_soal.soal_id), 6, 6) as soal_id,
					tbl_bank_soal.soal_id as hid_id,
					tbl_bank_soal.jadwal_id,
					tbl_bank_soal.materi_id,
					tbl_bank_soal.mkditawarkan_id,
					tbl_bank_soal.pertanyaan,
					tbl_bank_soal.kategori_id,
				    tbl_namamk.keterangan as mata_kuliah,
				    (select judul from `db_ptiik_apps`.`tbl_materimk` where materi_id = tbl_bank_soal.materi_id) as materi,
				    (select concat(mata_kuliah, ' ', kelas) from `db_ptiik_apps`.`tbl_jadwalmk` where jadwal_id = tbl_bank_soal.jadwal_id) as jadwal,
				    tbl_test_kategori.keterangan as kategori
				FROM `db_ptiik_apps`.`tbl_bank_soal`,
				     `db_ptiik_apps`.`tbl_mkditawarkan`,
				     `db_ptiik_apps`.`tbl_matakuliah`,
				     `db_ptiik_apps`.`tbl_namamk`,
				     `db_ptiik_apps`.`tbl_test_kategori`
				WHERE tbl_bank_soal.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
				      AND tbl_mkditawarkan.matakuliah_id = tbl_matakuliah.matakuliah_id
				      AND tbl_matakuliah.namamk_id = tbl_namamk.namamk_id
				      AND tbl_bank_soal.kategori_id = tbl_test_kategori.kategori_id
                      ";
		if($id!=""){
			$sql = $sql . " AND  MID( MD5(tbl_bank_soal.soal_id), 6, 6) = '".$id."' ";
		}
			$sql = $sql . " ORDER BY hid_id ASC ";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_bank_jawaban_by_soalid($id){
		$sql =	"SELECT MID( MD5(jawaban_id), 6, 6) as jawab_id, MID( MD5(soal_id), 6, 6) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_bank_jawab` 
			     WHERE MID( MD5(soal_id), 6, 6) = '".$id."'
			     OR soal_id = '".$id."';
				";
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function get_bank($id=NULL){
		$sql = "SELECT nmk.keterangan as namamk, nmk.namamk_id
				FROM `db_ptiik_apps`.`tbl_test` t,
				     `db_ptiik_apps`.`tbl_mkditawarkan` mkd,
				     `db_ptiik_apps`.`tbl_matakuliah` mk,
				     `db_ptiik_apps`.`tbl_namamk` nmk
				WHERE t.mkditawarkan_id = mkd.mkditawarkan_id 
				      AND mkd.matakuliah_id = mk.matakuliah_id
				      AND mk.namamk_id = nmk.namamk_id
				      AND MID( MD5(t.test_id), 6, 6) = '".$id."'
				";
				
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_bank_soal($id=NULL){
		$sql = "SELECT  
				MID( MD5(`db_ptiik_apps`.`tbl_bank_soal`.`soal_id`), 6, 6) as soalid,
				`db_ptiik_apps`.`tbl_bank_soal`.`kategori_id`,
				`db_ptiik_apps`.`tbl_bank_soal`.`pertanyaan`,
				
				(SELECT `db_ptiik_apps`.`tbl_test_kategori`.`parameter_input`
				FROM `db_ptiik_apps`.`tbl_test_kategori`
				WHERE `db_ptiik_apps`.`tbl_test_kategori`.`kategori_id` = `db_ptiik_apps`.`tbl_bank_soal`.`kategori_id`) as kategori
				
				FROM `db_ptiik_apps`.`tbl_bank_soal` 
				WHERE `db_ptiik_apps`.`tbl_bank_soal`.`namamk_id` = '".$id."'
			   ";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_bank_jawaban(){
		$sql = "SELECT 
				MID( MD5(`db_ptiik_apps`.`tbl_bank_jawab`.`soal_id`), 6, 6) as soalid,
				MID( MD5(`db_ptiik_apps`.`tbl_bank_jawab`.`jawaban_id`), 6, 6) as jawabanid,
				`db_ptiik_apps`.`tbl_bank_jawab`.`keterangan`,
				`db_ptiik_apps`.`tbl_bank_jawab`.`is_benar`
				FROM `db_ptiik_apps`.`tbl_bank_jawab` 
				WHERE 1
			   ";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function getparam_by_bank_soal($id=NULL){
		$sql="SELECT (SELECT a.parameter_input FROM `db_ptiik_apps`.`tbl_test_kategori` as a WHERE a.`kategori_id` = `db_ptiik_apps`.`tbl_bank_soal`.`kategori_id`) as param
			  FROM `db_ptiik_apps`.`tbl_bank_soal` 
			  WHERE MID( MD5(soal_id), 6, 6) = '".$id."'"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->param;
		
		return $strresult;
	}
	
	function get_jawaban_edit_bank($id){
		$sql =	"SELECT MID( MD5(jawaban_id), 6, 6) as jawab_id, MID( MD5(soal_id), 6, 6) as soal_id,`keterangan`, `is_benar`, skor 
			     FROM `db_ptiik_apps`.`tbl_bank_jawab` 
			     WHERE MID( MD5(soal_id), 6, 6) = '".$id."'
			     OR soal_id = '".$id."';
				";
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function replace_bank_soal($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_bank_soal',$datanya);
	}
	
	function replace_bank_jawab($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_bank_jawab',$datanya);
	}
	//------TEST BANK-----------------------------------------------------//
}
?>