<?php
class model_materimk extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT MID( MD5( materimk.materi_id), 6, 6) as materi_id,
					   MID( MD5( materimk.mkditawarkan_id), 6, 6) as mk_id,
					   materimk.mkditawarkan_id,
					   materimk.materi_id as hidmat,
					   materimk.judul as judul,
					   materimk.keterangan as keterangan,
					   materimk.urut as urut,
					   materimk.icon as icon,
					   materimk.parent_id as parent_id,
					   materimk.is_publish, 
                       materimk.status as status,
                       materimk.is_campus as campus,
					   namamk.keterangan as namamk,
					   namamk.english_version as english,
					   mkditawarkan.mkditawarkan_id as idmk,
					   mkditawarkan.cabang_id as id_cabang,
					   fakultas.fakultas_id,
					   fakultas.keterangan as fak,
					   tahunakademik.tahun,
					   tahunakademik.tahun_akademik as tahun_id,
					   (SELECT a.judul AS parent
							FROM  `db_ptiik_apps`.`tbl_materimk` a
							WHERE a.materi_id = materimk.parent_id) as parent
				FROM `db_ptiik_apps`.`tbl_namamk` namamk, 
					 `db_ptiik_apps`.`tbl_matakuliah` matakuliah, 
					 `db_ptiik_apps`.`tbl_mkditawarkan` mkditawarkan,
					 `db_ptiik_apps`.`tbl_tahunakademik` tahunakademik,
					 `db_ptiik_apps`.`tbl_fakultas` fakultas,
					 `db_ptiik_apps`.`tbl_materimk` materimk,
                     `db_ptiik_apps`.`tbl_statusmateri` statusmateri
				WHERE materimk.mkditawarkan_id = mkditawarkan.mkditawarkan_id
					  AND mkditawarkan.matakuliah_id = matakuliah.matakuliah_id
					  AND matakuliah.namamk_id = namamk.namamk_id
				      AND mkditawarkan.tahun_akademik=tahunakademik.tahun_akademik
				      AND fakultas.fakultas_id=namamk.fakultas_id
                      AND statusmateri.status = materimk.status
                      AND materimk.is_valid = 1";
		if($id){
			$sql = $sql . " AND MID( MD5(materimk.materi_id), 6, 6) = '".$id."' ";
		}
		
		$sql = $sql . " ORDER BY hidmat ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function id_materimk($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(materi_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_materimk`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function tahun_edit(){
		$sql = "SELECT MID( MD5(tahun_akademik), 6, 6) as mktahun_akademik, 
					   concat(tahun,' - ', is_ganjil) as tahun, 
					   tahun_akademik as thn_id 
				from `db_ptiik_apps`.`tbl_tahunakademik`";
		$result = $this->db->query( $sql );

		return $result;
	}
	
	function get_status_materi(){
		$sql = "SELECT keterangan, 
					   status
				FROM `db_ptiik_apps`.`tbl_statusmateri`";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function tahun_akademik(){
		$sql = "SELECT tahunakademik.tahun_akademik AS thn_id, 
					   CONCAT( tahunakademik.tahun,  ' - ', tahunakademik.is_ganjil,  ' ', tahunakademik.is_pendek ) AS tahun
				FROM  `db_ptiik_apps`.`tbl_tahunakademik` tahunakademik
				ORDER BY tahun_akademik DESC
				";
				
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;	
	}

	public function tampilkan_mk($id=NULL,$thn=NULL,$cabang = NULL){
		$sql = "SELECT DISTINCT namamk.keterangan, mkditawarkan.mkditawarkan_id, tahunakademik.tahun_akademik
					FROM
					db_ptiik_apps.tbl_fakultas AS fakultas
					INNER JOIN db_ptiik_apps.tbl_namamk AS namamk ON namamk.fakultas_id = fakultas.fakultas_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah AS matakuliah ON matakuliah.namamk_id = namamk.namamk_id
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan AS mkditawarkan ON matakuliah.matakuliah_id = mkditawarkan.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_tahunakademik AS tahunakademik ON mkditawarkan.tahun_akademik = tahunakademik.tahun_akademik 
				WHERE 1 = 1 
					";
		if($id){
			$sql = $sql . " AND fakultas.fakultas_id = '".$id."' ";
		}
		
		if($thn){
			$sql = $sql . " AND tahunakademik.tahun_akademik = '".$thn."' ";
		}
		
		if($cabang){
			$sql = $sql . " AND mkditawarkan.cabang_id = '".$cabang."' ";
		}
		
		
		$sql = $sql. " ORDER BY namamk.keterangan ASC ";
		//echo $sql;
		$result = $this->db->query( $sql );
		return $result;
	}
	
	public function tampilkan_submateri($id,$thn,$mkid,$cabang){
		$sql = "SELECT materimk.materi_id,
					   materimk.judul,
					   materimk.urut,
					   materimk.parent_id,
					   materimk.mkditawarkan_id
				FROM `db_ptiik_apps`.`tbl_fakultas` fakultas,
					 `db_ptiik_apps`.`tbl_namamk` namamk,
					 `db_ptiik_apps`.`tbl_matakuliah` matakuliah,
					 `db_ptiik_apps`.`tbl_mkditawarkan` mkditawarkan,
					 `db_ptiik_apps`.`tbl_tahunakademik` tahunakademik,
				     `db_ptiik_apps`.`tbl_materimk` materimk
				WHERE fakultas.fakultas_id = '".$id."' 
					AND fakultas.fakultas_id = namamk.fakultas_id
					AND namamk.namamk_id = matakuliah.namamk_id
					AND mkditawarkan.cabang_id = '".$cabang."'
					AND matakuliah.matakuliah_id = mkditawarkan.matakuliah_id
					AND tahunakademik.tahun_akademik = '".$thn."'
					AND mkditawarkan.tahun_akademik = tahunakademik.tahun_akademik
				    AND mkditawarkan.mkditawarkan_id = materimk.mkditawarkan_id
				    AND materimk.mkditawarkan_id = '".$mkid."'
				    AND materimk.is_valid = 1
				";
				
		$result = $this->db->query( $sql );
		return $result;
	}
	
	public function tampilkan_index($id,$thn,$mkid){
		$sql = "SELECT materimk.materi_id,materimk.judul,
				       materimk.urut,
				       materimk.parent_id,
				       materimk.mkditawarkan_id,
					   MID( MD5( materimk.materi_id), 6, 6) as id,
					   MID( MD5( materimk.mkditawarkan_id), 6, 6) as mk
				FROM `db_ptiik_apps`.`tbl_fakultas` fakultas,
					`db_ptiik_apps`.`tbl_namamk` namamk,
					`db_ptiik_apps`.`tbl_matakuliah` matakuliah,
					`db_ptiik_apps`.`tbl_mkditawarkan` mkditawarkan,
					`db_ptiik_apps`.`tbl_tahunakademik` tahunakademik,
				     `db_ptiik_apps`.`tbl_materimk` materimk
				WHERE fakultas.fakultas_id = '".$id."' 
					AND fakultas.fakultas_id = namamk.fakultas_id
					AND namamk.namamk_id = matakuliah.namamk_id
					AND matakuliah.matakuliah_id = mkditawarkan.matakuliah_id
					AND tahunakademik.tahun_akademik = '".$thn."'
					AND mkditawarkan.tahun_akademik = tahunakademik.tahun_akademik
				    AND mkditawarkan.mkditawarkan_id = materimk.mkditawarkan_id
				    AND materimk.mkditawarkan_id = '".$mkid."'
				    AND materimk.is_valid = 1
				";
				
		$result = $this->db->query( $sql );
		return $result;
	}

	function mkditawarkan(){
		$sql = "SELECT MID( MD5(mkditawarkan.mkditawarkan_id), 6, 6) as mkditawarkan_id, 
					   mkditawarkan.mkditawarkan_id as hid_id, 
					   namamk.keterangan
				FROM `db_ptiik_apps`.`tbl_mkditawarkan` mkditawarkan, 
					 `db_ptiik_apps`.`tbl_matakuliah` matakuliah, 
					 `db_ptiik_apps`.`tbl_namamk` namamk
				WHERE mkditawarkan.matakuliah_id=matakuliah.matakuliah_id AND matakuliah.namamk_id=namamk.namamk_id";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_materi($mk=NULL,$id=NULL,$par=NULL){
		$sql = "SELECT  MID( MD5(`tbl_materimk`.`materi_id`), 6, 6) as materi_id,
						MID( MD5( tbl_materimk.mkditawarkan_id), 6, 6) as mk,
						`tbl_materimk`.`materi_id` as hid_id,
						`tbl_materimk`.`judul` as `judul`,
						`tbl_materimk`.`mkditawarkan_id` as `mkditawarkan_id`,
						`tbl_materimk`.`urut` as `urut`
				FROM `db_ptiik_apps`.`tbl_materimk`
				WHERE 1=1 AND db_ptiik_apps.tbl_materimk.is_valid=1";
		if($mk){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_materimk.mkditawarkan_id), 6, 6)='".$mk."' OR db_ptiik_apps.tbl_materimk.mkditawarkan_id = '".$mk."' ) ";
		}
		if($id){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_materimk.materi_id), 6, 6)='".$id."' OR db_ptiik_apps.tbl_materimk.materi_id = '".$id."' ) ";
		}
		if($par){
			$sql = $sql . " AND (mid(md5(db_ptiik_apps.tbl_materimk.parent_id), 6, 6)='".$par."' OR db_ptiik_apps.tbl_materimk.parent_id = '".$par."' ) ";
		}
		
		//$sql = $sql . "ORDER BY `tbl_skripsi_tahap`.`urut` ASC";
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;	
	}
	
	function get_submateri_count($id=NULL){
		$sql = "SELECT COUNT( * ) as data
				FROM  db_ptiik_apps.`tbl_materimk` 
				WHERE (mid(md5(db_ptiik_apps.tbl_materimk.parent_id), 6, 6) =  '".$id."' OR db_ptiik_apps.tbl_materimk.parent_id = '".$id."')
				";
		if($id){
			$result = $this->db->getRow($sql);
			if(isset($result)){
			return $tes = $result->data;
			}
			//echo $result;	
			//echo $sql;	
		}
		
	}
	
	function get_materibymk_count($id=NULL){
		$sql = "SELECT COUNT( materi_id ) as data
				FROM db_ptiik_apps.tbl_materimk
				WHERE parent_id =  '0'
				AND (mid(md5(mkditawarkan_id), 6, 6) =  '".$id."' OR mkditawarkan_id = '".$id."')
				";
		if($id){
			$result = $this->db->getRow($sql);
			if(isset($result)){
			return $tes = $result->data;
			}
			//echo $result;	
			//echo $sql;	
		}
	}
	
	function cek_new($jud){
		$sql = "SELECT judul 
				from `db_ptiik_apps`.`tbl_materimk` 
				where judul = '".$jud."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_new_by_id($jud){
		$sql = "SELECT materi_id 
				from `db_ptiik_apps`.`tbl_materimk` 
				where judul = '".$jud."'";
		
		$result = $this->db->query( $sql );
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->materi_id;
			}
			return $id;
		}
	}
	
	function cek_urut($term,$mk){
		$sql = "SELECT urut
				FROM `db_ptiik_apps`.`tbl_materimk`
				WHERE urut = '".$term."' AND mkditawarkan_id = '".$mk."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->urut;
			return $result;
		}
		
	}
	
	function cek_urut_by_id($term){
		$sql = "SELECT materi_id
				FROM `db_ptiik_apps`.`tbl_materimk`
				WHERE urut = '".$term."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->materi_id;
			return $result;
		}
		
	}
	
	function replace_materimk($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_materimk',$datanya);
	}
	
	function delete($id){
		$sql = "UPDATE db_ptiik_apps.tbl_materimk 
				SET is_valid = 0 
				WHERE materi_id = '".$id."'
				OR parent_id = '".$id."'
				";
		$result = $this->db->query($sql);
		//echo $sql;
		return $result;
	}
	
	function get_id_from_parent($id){
		$sql = "SELECT materi_id
				FROM db_ptiik_apps.tbl_materimk 
				WHERE parent_id = '".$id."' AND is_valid = 1
				";
		$result = $this->db->query($sql);
		return $result;
	}
	
	//=============================FILE UPLOAD=================================
	
	function id_file($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_file`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, 
					   keterangan, extention, 
					   max_size 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		return $result;

	}
	
	function cek_file_jenis($cons) {
		$sql = "SELECT jenis_file_id 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE keterangan = '".$cons."'";
		$result = $this->db->getRow( $sql );
		//echo $result;
		$strresult = $result->jenis_file_id;
		//echo $strresult;
		return $strresult;
		
	}
	
	function cek_nama_icon($term){
		$sql = "SELECT icon
				FROM `db_ptiik_apps`.`tbl_materimk`
				WHERE icon = '".$term."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->icon;
			return $result;
		}
		
	}
	
	function replace_file($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_file',$datanya);
	}
}
?>