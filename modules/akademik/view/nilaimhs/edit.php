<?php
$this->head();

if($posts !=""){
	$header		= "Edit Nilai Mahasiswa";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$namamk			= $dt->namamk;
		$komponenid		= $dt->komponen_id;
		$nilai			= $dt->nilai;
		$namamhs		= $dt->namamhs;
	endforeach;
	
	foreach ($jadwal as $dt):
		$jadwalmk		= $dt->jadwal;
		$jadwalid		= $dt->jadwal_id;
		$mkditawarkanid	= $dt->mkditawarkan_id;
	endforeach;
	
}else{
	$header		= "Write New Nilai Mahasiswa";
	
	foreach ($jadwal as $dt):
		$jadwalmk		= $dt->jadwal;
		$namamk			= $dt->namamk;
		$jadwalid		= $dt->jadwal_id;
		$mkditawarkanid	= $dt->mkditawarkan_id;
	endforeach;
	$edit 		= "";
	$id			= "";
	$komponenid	= "";
	
}


?>
    <div class="row">    
    <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/nilaimhs'); ?>">Nilai Mahasiswa</a></li>
	  <li class="active"><a href="#"><?php echo str_replace(' Nilai Mahasiswa','',str_replace('Write ','',$header));  ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if(isset($add)) { ?>
		<a href="<?php echo $this->location('module/akademik/jadwalmk'); ?>" class="btn btn-default"><i class="fa fa-list"></i> Jadwal Mata Kuliah List</a> 
		<?php } ?> 
    </div>
        <div class="">
		
			<form method=post  action="<?php echo $this->location('module/akademik/nilaimhs/save'); ?>" class="form-horizontal">
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Nama Mata Kuliah</label>
					 <div class="col-sm-10">
						<input disabled required="required" type=text  class='col-md-9 form-control' name="namamk" value="<?php if(isset($namamk))echo $namamk; ?>">
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Jadwal</label>
					<div class="col-sm-10">
						<input disabled required="required" type=text  class='col-md-9 form-control' name="namamk" value="<?php if(isset($jadwalmk))echo $jadwalmk; ?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Nama Mahasiswa </label>
						<div class="col-md-10">
							<input id="namamhs" name='namamhs' required='required' type='text' value ='<?php if(isset($namamhs))echo $namamhs ?>' class='col-md-6 form-control typeahead'>		
							<?php if($edit == '1') { ?>
							<input type="hidden" name='namamk' value ='<?php echo $namamhs ?>' >
							<?php } ?>
						</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Komponen Nilai</label>
					<div class="col-sm-10">
						<?php if(isset($komponen)){ ?>
						<select name="komponen" id="parent" class='col-md-9 form-control' >
								<option class="sub_01" value="0">Please Select..</option>
							<?php														
								foreach($komponen as $dt):
									echo "<option class='sub_".$dt->komponen_id."' value='".$dt->komponen_id."' ";
									if($komponenid==$dt->komponen_id){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
						<?php }else {
							$str="<div class='form-actions'>
							<a class='btn-edit-post' href=".$this->location('module/akademik/komponennilai/write/').">
							<button type='button' class='btn btn-info'><i class='fa fa-edit'></i> Tambah Komponen Mata Kuliah</button></a>
							</div>
							";
							echo $str;
						} ?>
					</div>
				</div>
				
				<div class="form-group">	
				 	<label for="keterangan" class="col-sm-2 control-label">Nilai</label>
					 <div class="col-sm-10">
					 	<input required="required" type=text  class='col-md-9 form-control' name="nilai" value="<?php if(isset($nilai))echo $nilai; ?>"><br>
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<!-- <input type="hidden" name="namamk" value="<?php echo $namamk;?>"> -->
						<input type="hidden" name="jadwalid" value="<?php echo $jadwalid;?>">
						<input type="hidden" name="mkditawarkanid" value="<?php echo $mkditawarkanid;?>">				
						<br><br><input type="submit" name="b_nilaimhs" value="Submit" class="btn btn-primary">
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
$this->foot();
?>