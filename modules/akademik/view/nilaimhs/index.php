<?php $this->head(); 
$header="Nilai Mahasiswa";?>
<div class="row">
    <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/akademik/nilaimhs'); ?>">Nilai Mahasiswa</a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/akademik/jadwalmk'); ?>" class="btn btn-default"><i class="fa fa-list"></i> Jadwal Mata Kuliah List</a> 
	</div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	
	
		$str="<table class='table table-hover' id='example' data-id='module/akademik/mkditawarkan'>
				<thead>
					<tr>
						<th>Nama Mahasiswa</th>
						<th>Nama Mata Kuliah</th>
						<th>Komponen Nilai</th>
						<th>Nilai</th>
						<th>Last Update</th>";
		$str.="			<th>Act</th>";
		$str.="		</tr>
				</thead>
				<tbody>";
			
			if($posts > 0){
				foreach ($posts as $dt): 
					$str.=	"<tr><td>".$dt->namamhs."
								</td>
								";
								
					$str.=	"<td>".$dt->namamk."
								</td>
								";
					
					$str.=	"<td>".$dt->komponen_nilai."</td>
								";
								

					$str.= "<td>".$dt->nilai."</td>
							";
					
					$str.= "<td><em style='color : orange'>By :".$dt->user_id."</em><br>
							<i class='fa fa-time'></i>&nbsp;".$dt->YMD."<br>
							".$dt->waktu."</td>";
					
					$str.= "<td style='min-width: 80px;'>   
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href=".$this->location('module/akademik/nilaimhs/edit/'.$dt->nilai_id)."><i class='fa fa-pencil'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td></tr>";
					
				 endforeach; 
			 }
		$str.= "</tbody></table>";
		
		echo $str;
		
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
    
</div>
<?php $this->foot(); ?>