<?php
$this->head();

if($posts !=""){
	$header		= "Edit Mata Kuliah";
	
	foreach ($posts as $dt):
		$id					= $dt->hid_id;
		$fakultas_id		= $dt->fakultas_id;
		$namamk				= $dt->namamk;
		$namamkid			= $dt->namamk_id;
		$kodemk				= $dt->kode_mk;
		$kurikulum			= $dt->kurikulum;
		$sks				= $dt->sks;
		$english_version	= $dt->engver;
	endforeach;
	
	$header.=" ".$namamk;
	
	$edit		= "1";
	
}else{
	$header		= "Write New Mata Kuliah";
	$id			= "";
	$ceknew		= '1';
	$fakultas_id= "";
}


?> 
	
    <div class="row">   
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/mk'); ?>">Mata Kuliah</a></li>
	  <li class="active"><a href="#"><?php echo str_replace('Write ','',str_replace('Mata Kuliah','',$header)); ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if($id !=""){	?>
		<a href="<?php echo $this->location('module/akademik/mk/writemk'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Mata Kuliah</a>
		<?php } ?>
        </div> 
        <div class="">
		
			<form method=post name="form" id="form" class="form-horizontal block-box">
				<div class="form-group">	
					<label for="fakultas" class="col-sm-2 control-label">Fakultas</label>
					<div class="col-sm-10">
						<select id="select_fakultas" class="form-control e9" name="fakultas" <?php if(isset($edit))echo "disabled"; ?> >
							<option value="0" data-uri='1'>Select Fakultas</option>
							<?php if(count($fakultas)> 0) {
								foreach($fakultas as $f) :
									echo "<option value='".$f->fakultasid."' ";
									if($fakultas_id==$f->fakultasid||$fakultasid==$f->hid_id){
										echo "selected";
									}
									echo " >".$f->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="namamk" class="col-sm-2 control-label">Nama Mata Kuliah </label>
						<div class="col-sm-10">
							<input disabled id="namamk" id="namamk" name='namamk' required='required' type='text' value ='<?php if(isset($namamk))echo $namamk ?>' class='col-md-6 form-control typeahead'>	
							<!-- <input type="hidden" name='namamk_id' id='namamk_id' value ='<?php echo $namamk_id ?>' > -->
						</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">English Version</label>
						<div class="col-sm-10">
							<input type=text  class='col-md-9 form-control' id="namamkeng" name="namamkeng" value="<?php if(isset($english_version))echo $english_version; ?>">	
						</div>
				</div>
				
				<div class="form-group">	
					<label for="kodemk" class="col-sm-2 control-label">Kode Mata Kuliah</label>
					<div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="kodemk" name="kodemk" value="<?php if(isset($kodemk))echo $kodemk; ?>">
						</div>
				</div>
				
				<div class="form-group">	
					<label for="kurikulum" class="col-sm-2 control-label">Kurikulum</label>
					<div class="col-sm-10">
						<input required="required" type="number" class='col-md-9 form-control' id="kurikulum" name="kurikulum" min="2012" max="2013" value="<?php if(isset($kurikulum))echo $kurikulum; ?>">
					</div>
				</div>
				
				<div class="form-group">	
					<label for="sks" class="col-sm-2 control-label">SKS</label>
					<div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="sks" name="sks" value="<?php if(isset($sks))echo $sks; ?>">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="ceknew" value="<?php echo $ceknew;?>">
						<?php if(isset($edit)) { ?>
							<input type="hidden" name="fakultas" value="<?php echo $fakultas_id;?>">
							<input type="hidden" name="namamkid" value="<?php echo $namamkid;?>">
						<?php } ?>						
						<br><br><input type="submit" name="b_mk" value="Submit" id="submitmk" class="btn btn-primary">
					</div>
				</div>
		
			</form>
		</div>
	</div>
<?php
$this->foot();
?>