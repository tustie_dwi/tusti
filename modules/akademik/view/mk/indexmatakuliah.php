<?php $this->head(); 
$header="Mata Kuliah";
?>
<div class="row">
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/akademik/mk'); ?>">Mata Kuliah</a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if($user!="mahasiswa"&&$user!="dosen"){ ?>
	<a href="<?php echo $this->location('module/akademik/mk/writemk'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Mata Kuliah</a> 
    <?php } ?>
    </div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	
	
		$str="<div class='block-box'><table class='table table-hover' id='example' data-id='module/akademik/mk'>
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Mata Kuliah</th>
						<th>Fakultas</th>
						";
		if($user!="mahasiswa"&&$user!="dosen"){				
		$str.="			<th>Act</th>";
		}
		$str.="		</tr>
				</thead>
				<tbody>";
			
			if($posts > 0){
				$i= 1;
				foreach ($posts as $dt): 
					$str.=	"<tr>
								<td>".$i."</td>
								<td><span class='text text-default'><strong>".$dt->namamk."</strong></span>
								  <code>Kode : ".$dt->kode_mk."</code>&nbsp;
								  <span class='label label-success'>Kurikulum : ".$dt->kurikulum."</span>&nbsp;
								  <span class='badge'>SKS : ".$dt->sks."</span>
								 <br>	 <small><em>English Version : ".$dt->engver."</em></small>				  
								</td>";
								
					$str.=	"<td>".$dt->fakultas."</td>";
													
					/* $str.=	"<td> Kurikulum : ".$dt->kurikulum."<br>
							 SKS : ".$dt->sks."</td>
							 ";	*/			
					if($user!="mahasiswa"&&$user!="dosen"){
					$str.= "<td style='min-width: 80px;'>   
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href=".$this->location('module/akademik/mk/editmk/'.$dt->matakuliah_id)."><i class='fa fa-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td></tr>";
					}
					$i++;
				 endforeach; 
			 }
		$str.= "</tbody></table></div>";
		
		echo $str;
	
	 else: 
	 ?>
    <div class="col-md-3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</div>
<?php $this->foot(); ?>