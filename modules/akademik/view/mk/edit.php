<?php
$this->head();

if($posts !=""){
	$header		= "Edit Nama Mata Kuliah";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$fakultas_id	= $dt->fakultas_id;
		$keterangan		= $dt->keterangan;
		$english_version= $dt->english_version;
	endforeach;
	
}else{
	$header		= "Write New Nama Mata Kuliah";
	$id			= "";
	$fakultas_id	= "";
	$ceknew		= '1';
}


?>
	<legend>
		<a href="<?php echo $this->location('module/akademik/mk'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Nama Mata Kuliah List</a> 
		<?php if($id !=""){	?>
		<a href="<?php echo $this->location('module/akademik/mk/write'); ?>" class="btn btn-primary pull-right" style="margin:0px 5px"><i class="fa fa-pencil"></i> Write New Nama Mata Kuliah</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post name="form" id="form" class="form-horizontal block-box">
				<div class="form-group">
					 <label for="fakultas" class="col-sm-2 control-label">Fakultas</label>
					 <div class="col-md-10">
						<select id="fakultas" name="fakultas" class="form-control e9">
							<option value="0">Please Select..</option>
							<?php
								foreach($fakultas as $dk):
									echo "<option value='".$dk->fakultasid."' ";
									if($fakultas_id==$dk->fakultasid){
										echo "selected";
									}
									echo " >".$dk->keterangan."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Nama Mata Kuliah</label>
					 <div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="namamk" name="namamk" value="<?php if(isset($keterangan))echo $keterangan; ?>">
					</div>
				</div>
				
				<div class="form-group">	
				 	<label for="namamkeng" class="col-sm-2 control-label">English Version</label>
					 <div class="col-sm-10">
						<input required="required" type=text  class='col-md-9 form-control' id="namamkeng" name="namamkeng" value="<?php if(isset($english_version))echo $english_version; ?>">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="ceknew" value="<?php echo $ceknew;?>">						
						<br><br><input type="submit" name="b_namamk" id="submit" value="Submit" class="btn btn-primary">
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
$this->foot();
?>