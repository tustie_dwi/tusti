<?php $this->head(); ?>
<fieldset>
	<legend>Daftar Test</legend>
	
	<table class='table table-hover' id='example' data-id='module/skripsi/aspek'>
		<thead>
			<tr>
				<th>Materi</th>
				<th>Judul</th>
				<th>Tgl Mulai</th>
				<th>Tgl Selesai</th>
				<th>Instruksi</th>
				<th>Keterangan</th>
				<th>Detail</th>
			</tr>
		</thead>
		
		<tbody>
			<?php foreach($test as $key){ ?>
				<tr>
					<td><?php echo $key->judul ?></td>
					<td><a href="<?php echo $this->location('module/akademik/testhasil/soal/') . $key->test_id ?>"><?php echo $key->judul_test ?></a></td>
					<td><?php echo $key->tgl_mulai ?></td>
					<td><?php echo $key->tgl_selesai?></td>
					<td><?php echo $key->instruksi ?></td>
					<td><?php echo $key->keterangan ?></td>
					<td><a href="<?php echo $this->location('module/akademik/testhasil/peserta/') . $key->test_id ?>">Lihat Peserta</a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	
</fieldset>
<?php
$this->foot();
?>