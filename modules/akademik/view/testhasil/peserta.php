<?php $this->head(); ?>

<fieldset>
	<legend>Daftar Peserta Test</legend>
	
	<?php foreach($totalpeserta as $key){ ?>
		<?php echo "Jumlah Peserta : ".$key->total_peserta ?>
	<?php } ?>
	<br />
	
<?php if(isset($peserta)) :	?>	
	<table class='table table-hover' id='example' data-id='module/skripsi/aspek'>
		<thead>
			<tr>
				<th>NIM</th>
				<th>Tanggal Test</th>
				<th>Jam Mulai</th>
				<th>Jam Selesai</th>
				<th>Total Jawab</th>
				<th>Nilai</th>
				<th>Lihat Test</th>
			</tr>
		</thead>
		
		<tbody>
			<?php foreach($peserta as $key){ ?>
				<tr>
					<td><?php echo $key->mahasiswa_id ?></td>
					<td><?php echo $key->tgl_test ?></td>
					<td><?php echo $key->jam_mulai ?></td>
					<td><?php echo $key->jam_selesai ?></td>
					<td><?php echo $key->max_post ?></td>
					<td><?php echo $key->total_skor ?></td>
					<td><a href="<?php echo $this->location('module/akademik/testhasil/ceksoal/') . $key->test_id ?>">Cek</a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
<?php
	else: 
	echo "Maaf Tidak Peserta yang Mengambil Test Ini";	
	endif;
?>	
</fieldset>
<?php
$this->foot();
?>