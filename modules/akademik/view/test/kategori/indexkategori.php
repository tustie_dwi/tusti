<?php if($user!="Mahasiswa"){ $this->head(); ?>
<fieldset>
	<legend>
			<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
    	<!-- <a href="<?php echo $this->location('module/akademik/test'); ?>" class="btn btn-default pull-right" style="margin:0px 5px">
    		<i class="fa fa-bars"></i> Test List</a> -->
		<a href="<?php echo $this->location('module/akademik/test/kategori/write'); ?>" class="btn btn-primary pull-right" style="margin:0px 5px">
    		<i class="fa fa-pencil"></i> New Test Category</a><?php } ?> Test Category List
	</legend>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	
		
		$str="<table class='table table-hover' id='example' data-id='module/akademik/test/'>
				<thead>
					<tr>
						<th>No</th>
						<th>Test Category</th>
						<th>Jenis Input</th>";
		if($user!="mahasiswa" && $user!="dosen"){				
		$str.="				<th>Act</th>";
		}
		$str.="
					</tr>
				</thead>
				<tbody>";
		
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
					$str.=	"<tr id='post-".$dt->kat_id."' data-id='".$dt->kat_id."' valign=top>
								<td>".$i++."</td>
								<td>".$dt->kategori."&nbsp;<span class='label pull-right label-info' style='text-align:center;width:150px;font-weight:normal;'></span>
								<span class='label label-success'></span>
								";
					
					//$str.= "<br><code>Jenis_input ".$dt->jenis_input."</code>";

					$str.= "</td>";
					$str.= "<td><span class='label label-success'>".$dt->jenis_input."</span>
							</td>";
					if($user!="mahasiswa" && $user!="dosen"){
						$str.= "<td style='min-width: 80px;'>         
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
										<li>
										<a class='btn-edit-post' href=".$this->location('module/akademik/test/kategori/edit/'.$dt->kat_id)."><i class='fa fa-edit'></i> Edit</a>	
										</li>	
										
									  </ul>
									</li>
								</ul>
							</td>";
					}
					$str.= "</tr>";
				 endforeach; 
			 }
		$str.= "</tbody></table>";
		
		echo $str;
	
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</fieldset>
<?php
$this->foot();
}
?>