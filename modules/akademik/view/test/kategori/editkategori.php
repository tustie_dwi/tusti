<?php
if($user!="mahasiswa" && $user!="dosen"){
$this->head();

if($posts!=""){
	$header		= "Edit Test Category";
	
	foreach ($posts as $dt):
	$id			= $dt->hid_id;
	$keterangan	= $dt->kategori;
		
	endforeach;
	$frmact 	= $this->location('module/akademik/test/kategori/save');		
	
}else{
	$header		= "Write New Test Category";
	
	$id			= "";
	$keterangan	= "";
	$newjen		= 1;
	
	$frmact 	= $this->location('module/akademik/test/kategori/save');		
}


?>

<div class="container-fluid">  
	
	<legend>
		<a href="<?php echo $this->location('module/akademik/test/kategori/view'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Test Category List</a> 
		<?php if($posts!=""){	?>
			<a href="<?php echo $this->location('module/akademik/test/kategori/write'); ?>" class="btn btn-primary pull-right" style="margin:0px 5px"><i class="fa fa-pencil"></i> Write New Test Category</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post id="upload-test-kategori-form" action="<?php echo $this->location('module/akademik/test/kategori/save'); ?>" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Test Kategori</label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="text" required="required" autocomplete="off" class="form-control" name='keterangan' value="<?php if(isset($keterangan)) echo $keterangan; ?>">
						</div>
					</div>
				</div>
				 <div class="form-group">	
					<label class="col-sm-2 control-label">Jenis Input</label>
					<div class="controls">
						<div class="col-sm-10">
						<select class="form-control e9" name="jenis_input">
							<?php 
							echo '<option class="sub_01" value="radio"';
									if(isset($dt->jenis_input)&&"radio"==$dt->jenis_input){
										echo "selected";
									}
									echo '>Radio</option>';
							echo '<option class="sub_01" value="checkbox"';
									if(isset($dt->jenis_input)&&"checkbox"==$dt->jenis_input){
										echo "selected";
									}
									echo '>Checkbox</option>';
							echo '<option class="sub_01" value="textarea"';
									if(isset($dt->jenis_input)&&"textarea"==$dt->jenis_input){
										echo "selected";
									}
									echo '>Textarea</option>';
							?>					
						</select>
						</div>
					</div>
				</div>
				 <div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="hidden" name="hidId" value="<?php if(isset($id)) echo $id;?>">
						<input type="hidden" name="newjen" value="<?php if(isset($newjen)) echo $newjen;?>">
						<input type="submit" name="b_jenis" onclick="submit_tes_kategori()" value="Submit" class="btn btn-primary">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
}
?>