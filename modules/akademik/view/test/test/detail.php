<?php $this->head(); ?>
<fieldset>
	<legend>GENERAL INFO
	&nbsp;
	<a href="<?php echo $this->location('module/akademik/test'); ?>" class="btn btn-default pull-right">
    <i class="fa fa-bars"></i> Test List</a>
    <?php //if($user!="mahasiswa"&&$user!="Dosen"){ ?>
	<!-- <a href="<?php echo $this->location('module/akademik/mkditawarkan/write'); ?>" class="btn btn-primary pull-right" style="margin:0px 5px">
	<i class="fa fa-pencil"></i> Write New Mata Kuliah Ditawarkan</a> -->
	<?php //} ?>
	</legend>
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 

	if( isset($posts) ) :
		if($posts > 0){
		foreach ($posts as $dt): ?>
		<ul class="nav nav-tabs" id="writeTab" >
			<li class='active'><a href="#detail-test" data-toggle="tab">Detail Test</a></li>
			<li ><a href="#soal" data-toggle="tab">Soal</a></li>
		</ul>
		<div class='tab-content'>
			<div class="tab-pane active" id="detail-test">
				<br>
				<table class='table table-bordered' id='example'>
					<tr>
						<td><em>Nama Mata Kuliah</em></td>
						<td><?php echo $dt->namamk ?>
							<?php
							if($dt->is_random=='1'){
								echo "<code>Random</code>";
							}
							
 							?>
						</td>
					</tr>
					<tr>
						<td><em>Judul</em></td>
						<td><?php echo $dt->judul ?></td>
					</tr>
					<tr>
						<td><em>Tanggal Mulai</em></td>
						<td><?php echo substr(str_replace('-', '/', $dt->tgl_mulai), 0,10) ?> jam <?php echo substr($dt->tgl_mulai, 11) ?> </td>
					</tr>
					<tr>
						<td><em>Tanggal Selesai</em></td>
						<td><?php echo substr(str_replace('-', '/', $dt->tgl_selesai), 0,10) ?> jam <?php echo substr($dt->tgl_selesai, 11) ?></td>
					</tr>
					<tr>
						<td><em>Instruksi</em></td>
						<td><?php echo $dt->instruksi ?></td>
					</tr>
					<tr>
						<td><em>Keterangan</em></td>
						<td><?php echo $dt->keterangan ?></td>
					</tr>
					<?php
						if($dt->materi!=NULL){ ?>
							<tr>
								<td><em>Materi</em></td>
								<td><?php echo $dt->materi ?></td>
							</tr>
					<?php	}
		 			?>
					<tr>
						<td><em>Status</em></td>
						<td>
						<?php
							if($dt->is_publish=='1'){
								echo "Published";
							}
							else {
								echo "Not Published";
							}
		 				?>	
						</td>
					</tr>
					<?php endforeach; 
					} ?>
				</table>
				<?php if($user!="mahasiswa"){ ?>		 
				<div class='well'>
					<a href=<?php echo $this->location('module/akademik/test/edit/'.$dt->test_id) ?> class='btn btn-info'>
					<i class='fa fa-edit'></i> Edit Test</a>
				</div>
				
				<?php } ?>
			</div>
			<div class="tab-pane" id="soal">
				<br>
				<a href="<?php echo $this->location('module/akademik/test/soal/write/'.$testid); ?>" class="btn btn-primary pull-right" style='margin-top: -64px;'>
				<i class="fa fa-plus"></i> Tambah Soal</a>
				<?php $this->view('test/test/soal.php', $data);?>
			</div>
		</div>
		
		
	<?php
	else: 
	?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
	<?php endif; ?>
</fieldset>
<?php $this->foot(); ?>