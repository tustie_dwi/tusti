<?php
$this->head();
$header = "Jawab";
$kategori_id = ""
?>
	<legend>
		<a href="<?php echo $this->location('module/akademik/test'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Test List</a> 
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post name="form" id="form" class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label"> </label>
					<div class="col-md-10">
						INI SOAL
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label" >Kategori Jawab</label>
					<div class="col-md-10" >
						<?php echo '<select id="select_kategori" class="form-control e9" name="kategori" >'; ?>
						<option value="0" data-uri='1'>Select Kategori</option>
						<?php if(count($kategori)> 0) {
							foreach($kategori as $kt) :
								echo "<option value='".$kt->kategoriid."' ";
								if(isset($kategori_id)){
									if($kategori_id==$kt->kategoriid){
										echo "selected";
									}
								}
								echo " >".$kt->keterangan."</option>";
							endforeach;
						} ?>
						</select>
					</div>
				</div>
				
				
				<div class="form-group">	
					<label class="col-sm-2 control-label"> </label>
					<div class="col-md-10" >
						<div id="answertype"></div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"> </label>
					<div class="col-md-10">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="submit" id="submit" value="Submit" class="btn btn-primary">
					</div>
				</div>
				
			</form>
		</div>
	</div>
<?php
$this->foot();
?>