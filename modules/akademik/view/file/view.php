<?php $this->head(); 
$header="Detail File";
?>
<div class="row">
	<div class="col-md-12">
    <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/file'); ?>">File Materi</a></li>
	  <li class="active"><a href="#"><?php echo $header;?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
    	<a href="<?php echo $this->location('module/akademik/file/write'); ?>" class="btn btn-primary">
    		<i class="fa fa-pencil"></i> New File Materi</a>
    	<?php } ?>
	</div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) {	
		 foreach($posts as $p ){?>
				<div class="col-md-6">
					<h3><?php echo $p->judul?></h3>
					<blockquote>
						<?php echo $p->keterangan; ?>
						<code><i class="fa fa-tags"></i> <?php echo $p->jen_file?></code>&nbsp;<span class="label label-info"><i class="fa fa-paperclip"></i> <?php echo $p->file_size ?> B </span><br>
						<span class="small"><i class="fa fa-clock-o"></i> <?php echo $p->tgl_upload ?> by <span class="text text-default"><?php echo $p->name ?> </span></span>
					</blockquote>
					<a onclick="doDownload('<?php echo $p->fileid ?>')" href="<?php echo $this->location('module/akademik/file/download/'.$p->file_name); ?>" class="btn btn-default">
					<i class="fa fa-download"></i> Download This File</a>
							
				</div>
				
				<div class="col-md-6">			
						
					<?php switch ($p->jen_file) {
						case 'Video':
							echo '<video width="480" style="border: 1px solid #444; background : #333" height="360" id="video" preload="none" title="'.$p->judul.'" controls>
										<source src="'.$this->asset($p->file_loc).'" type="video/webm">
										<source src="'.$this->asset($p->file_loc).'" type="video/mp4">
										<source src="'.$this->asset($p->file_loc).'" type="video/ogg">
									</video>';
							break;
						
						case 'Image':
							echo '<img src="'.$this->asset($p->file_loc).'"  class="thumbnail"/>';
							break;
							
						case 'Presentation' || 'Document' || 'Spreadsheet':
							echo '<iframe src="//docs.google.com/viewer?url='.$this->asset($p->file_loc).'&embedded=true" width="550"
									height="780"
									style="border: none;"></iframe>';
							break;
					} ?>								
							
				</div>	
		<?php
		}
	 }
	 else{ 
	 ?>
		
			<div class="well">Sorry, no content to show</div>
    <?php } ?>
	</div>
</div>
<?php $this->foot(); ?>