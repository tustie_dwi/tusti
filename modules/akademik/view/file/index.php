<?php $this->head(); 
	
$header="File Materi";
?>
<div class="row">
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="#">File Materi</a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
    	<a href="<?php echo $this->location('module/akademik/file/write'); ?>" class="btn btn-primary">
    		<i class="fa fa-pencil"></i> New File Materi</a>
    	<?php } ?> 
	</div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	?>
    <div class="row">
		<div class="col-md-4">
        <div class="block-box">
			<div class="form-group">	
				<label>Jenis File</label>			
				<?php $uri_parent = $this->location('module/akademik/file/tampilkan_kategori'); ?>
				<?php echo '<select class="form-control e9" name="jenis" id="select_jenis"  data-uri="'.$uri_parent.'">' ?>
					<option class="sub_01" value="0">Select Jenis</option>
					<option class="sub_02" value="1">General</option>
					<option class="sub_03" value="2">Materi MK</option>			
				</select>			
			</div>	 	
			<div class="form-group">	
				<label>Category</label>			
				<?php $uri_parent = $this->location('module/akademik/file/tampilkan_materi'); ?>
				<?php echo '<select class="form-control e9" disabled name="kategori" id="select_kategori"  data-uri="'.$uri_parent.'">' ?>
					<option class="sub_01" value="0">Select Category</option>			
				</select>
						
			</div>		
			<div class="form-group" id="tes">	
				<label>Nama Materi</label>				
				<?php $uri_parent = $this->location('module/akademik/file/tampilkan_file'); ?>
				<?php echo '<select class="form-control e9" disabled name="materi" id="select_materi"  data-uri="'.$uri_parent.'">' ?>
					<option class="sub_01" value="0">Select Materi</option>	
				</select>					
			</div>	
            </div>
		</div>
		<div class="col-md-8" id="display"></div>
	
	</div>
</div>
<?php $this->foot(); ?>