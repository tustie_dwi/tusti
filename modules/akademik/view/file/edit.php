<?php
if($user!="mahasiswa" && $user!="dosen"){ 
$this->head();

if($posts!=""){
	$header		= "Edit File Materi";
	
	foreach ($posts as $dt):
	$hid_id				= $dt->file_id;
	$jen_file_id		= $dt->jenis_file_id;
	$materiid			= $dt->materi_id;
	$judul				= $dt->judul;
	$keterangan			= $dt->keterangan;
	$file_name			= $dt->file_name;
	$file_type			= $dt->file_type;
	$file_loc			= $dt->file_loc;
	$file_size			= $dt->file_size;
	$file_content		= $dt->file_content;
	$tgl_upload			= $dt->tgl_upload;
	$upload_by			= $dt->upload_by;
	$ispublish			= $dt->is_publish;
	endforeach;
	$frmact 	= $this->location('module/akademik/file/save');		
	
}else{
	$header				= "Write New File Materi";
	$judul				= "";
	$hid_id				= "";
	$keterangan			= "";
	$materiid			= "";
	$ispublish			= 0;
	$new				= 1;
	$frmact 			= $this->location('module/akademik/file/save');		
}


?>

<div>  
    <div class="row">   
     
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/file'); ?>">File Materi</a></li>
	  <li class="active"><a href="#"><?php echo str_replace('Write ','',str_replace('File Materi','',$header));?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if($posts!=""){	?>
		<a href="<?php echo $this->location('module/akademik/file/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New File Materi</a>
		<?php } ?>
	</div>
        <div class="col-md-12">
		
			<form method=post id="upload-form-file" enctype="multipart/form-data" action="<?php echo $this->location('module/akademik/file'); ?>" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Pilih Materi MK</label>
					<div class="controls">
						<div class="col-sm-10">
						<select class="form-control e9" name="materi" id="select_materi">
							<option class="sub_01" value="0">Select Materi</option>			
							<?php								
								foreach($materi as $dt):
									echo "<option class='sub_".$dt->materi_id."' value='".$dt->hid_id."' ";
									if($materiid==$dt->hid_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								endforeach;
							?>
						</select>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Judul</label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="text" class="form-control" name='judulx' required="required" value="<?php if(isset($judul)) echo $judul; ?>">
						</div>
					</div>
				</div>
				 <div class="form-group">	
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
						<textarea class="form-control ckeditor" name='keterangan'><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
						</div>
					</div>
				</div>
				<?php if (isset($new) && $new== 1) { ?>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Upload File</label>
					<div class="controls">
						<div class="col-sm-10">
							<input type="file" required="required" class="form-control" name="uploads[]" id="uploads" multiple>
						</div>
					</div>
				</div>
				<?php } ?>
				 <div class="form-group">
					<div class="controls">
						<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="jenis_file_id" value="<?php if(isset($jen_file_id)) echo $jen_file_id;?>">
						
						<input type="hidden" name="file_name" value="<?php if(isset($file_name)) echo $file_name;?>">
						<input type="hidden" name="file_type" value="<?php if(isset($file_type)) echo $file_type;?>">
						<input type="hidden" name="file_loc" value="<?php if(isset($file_loc)) echo $file_loc;?>">
						<input type="hidden" name="file_size" value="<?php if(isset($file_size)) echo $file_size;?>">
						<!-- <img src="<?php if(isset($file_content)) echo base64_encode($file_content);?>" /> -->
						<!-- <input type="hidden" name="file_content" value="<?php if(isset($file_content)) echo base64_decode($file_content);?>"> -->
						<input type="hidden" name="tgl_upload" value="<?php if(isset($tgl_upload)) echo $tgl_upload;?>">
						<input type="hidden" name="upload_by" value="<?php if(isset($upload_by)) echo $upload_by;?>">
						<input type="hidden" name="hidId" value="<?php if(isset($hid_id)) echo $hid_id;?>">
						<input type="hidden" name="new" value="<?php if(isset($new)) echo $new;?>">
						<input  type="submit" id="btn-submit" onclick="submit_file('publish')" name="b_publish" value="Upload and Publish" class="btn btn-primary">
						<input  type="submit" id="btn-submit"onclick="submit_file('draft')" name="b_draft" value="Upload as Draft" class="btn btn-warning">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
}
?>