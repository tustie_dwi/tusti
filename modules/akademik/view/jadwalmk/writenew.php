<?php
$this->head();

$header			= "Write New Jadwal Mata Kuliah";
$fakultas_id	= "";
$thnakademikid	= "";
$id				= "";
$isaktif		= "";
$isonline		= "";
$prodiid		= "";
$pengampuid		= "";
$cekonline 		= "";
$cabangid		= "";


?>

    <div class="row">    
    <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/jadwalmk'); ?>">Jadwal Mata Kuliah</a></li>
	  <li class="active"><a href="#"><?php echo str_replace(' Jadwal Mata Kuliah','',str_replace('Write ','',$header));  ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		
		<?php if(isset($write)) { ?>
		<a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>" class="btn btn-default" ><i class="fa fa-bars"></i> Mata Kuliah Ditawarkan List</a> 
		<?php } ?>
    </div> 
        <div class="col-md-12">
        <div class="row">
		<div class="block-box">
			<form method=post name="form" id="form-jadwal-addnew" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Fakultas</label>
					<div class="col-md-10">
						<?php echo '<select id="select_fakultas" class="form-control e9" name="fakultas">'; ?>
							<option value="0" >Select Fakultas</option>
							<?php if(count($fakultas)> 0) {
								foreach($fakultas as $f) :
									echo "<option value='".$f->fakultasid."' ";
									if($fakultas_id==$f->fakultasid||$fakultasid==$f->hid_id){
										echo "selected";
									}
									echo " >".$f->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Cabang</label>
					<div class="col-md-10">
						<?php echo '<select id="select_cabang" class="e9 form-control" name="cabang" >'; ?>
							<option value="0">Select Cabang</option>
							<?php if(count($cabang)> 0) {
								foreach($cabang as $c) :
									echo "<option value='".$c->cabang_id."' ";
									if($cabangid==$c->cabang_id){
										echo "selected";
									}
									echo " >".$c->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
				</div>

				<div class="form-group">	
					<label class="col-sm-2 control-label">Tahun Akademik</label>
					<div class="col-sm-10">
						<select name="thn_akademik" id="thn_akademik" class='col-md-9 form-control e9'>
							<option value="0" class="sub_01">Select Tahun Akademik</option>			
							<?php														
								foreach($thnakademik as $dt):
									echo "<option value='".$dt->tahun_akademik."' ";
									if($thnakademikid==$dt->tahun_akademik){
										echo "selected";
									}
									echo ">".$dt->tahun."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Prodi</label>
					<div class="col-sm-10">
						<select disabled id='select_prodi' name="prodi" id="parent" class='col-md-9 form-control e9'>
							<option value="0" class="sub_01">Select Prodi</option>			
							<?php														
								foreach($prodi as $dt):
									echo "<option class='sub_".$dt->komponen_id."' value='".$dt->prodi_id."' ";
									if($prodiid==$dt->prodi_id){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
				
				
				<div class="form-group">	
					 <label class="col-sm-2 control-label">Nama Mata Kuliah</label>
					 <div class="col-sm-10">
						<input onblur="cekisonline()" required="required" disabled type=text name="addnamamk" id='addnamamk' class='col-md-9 form-control typeahead' value="<?php if(isset($namamk))echo $namamk; ?>" >
						<input type="hidden" name="namamk" value="" id="namamk" />
						<em><small><div id="msgmk"></div></small></em>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Dosen Pengampu</label>
						<div class="col-md-10" >
							<input required='required' type='text' id ='dosen' name ='dosennama' value ='<?php if(isset($dosen))echo $dosen?>' class='col-md-6 form-control typeahead' >	
							<input type="hidden" id="dosen-id" name="dosen"/>
						</div>
				</div>
			
				<div class="form-group">	
					 <label class="col-sm-2 control-label">Kelas</label>
					 <div class="col-sm-10">
						<input required="required" type=text id='kelas' class='col-md-9 form-control' name="kelas" value="<?php if(isset($kelas))echo $kelas; ?>">
					</div>
				</div>
				
				<!-- IS COURSE -->
				<?php echo "<div id='form_no_is_online' "; 
					  if($isonline!='1'){
					  echo "style='display: none;' "; 
					  }
					  echo " > "; ?>
					<div class="form-group">	
						 <label class="col-sm-2 control-label">Ruang</label>
						 <div class="col-sm-10">
							<input type=text id="ruang"  class='col-md-9 form-control typeahead' name="ruang" value="<?php if(isset($ruangedit))echo $ruangedit; ?>">
						</div>
					</div>
					
					<div class="form-group">	
						 <label class="col-sm-2 control-label">Hari</label>
						 <div class="col-sm-10">
							<input type=text  class='col-md-9 form-control' name="hari" value="<?php if(isset($hari))echo $hari; ?>">
						</div>
					</div>
					
					<div class="form-group">	
						 <label class="col-sm-2 control-label">Jam</label>
						 <div class="col-sm-3">
							<input type="text" name="jammulai" class="timepicker form-control" value="<?php if(isset($jam))echo $jam;?>"> 
							sampai 
							<input type="text" name="jamselesai" class="timepicker form-control" value="<?php if(isset($jamselesai))echo $jamselesai;?>">
						</div>
					</div>
				</div>
				<!-- END IS COURSE -->
				
				<div class="form-group">	
				 	<label for="keterangan" class="col-sm-2 control-label">Is Aktif</label>
					 <div class="col-sm-10">
					 <label class="checkbox">
					 	<input type="checkbox" name="isaktif" value="1" <?php if ($isaktif==1) { echo "checked"; } ?>>Ya</label>
					</div>
				</div>
				
				<div class="form-group">	
				 	<label class="col-sm-2 control-label">Is Course </label>
					<div class="col-md-10" id="check">
						<label class="checkbox">
						<input id="isonline" onclick="validate()" type="checkbox" name="isonline" value="1" <?php if ($isonline==1) { echo "checked"; } ?>>Ya
						</label>
					</div>
				</div>
				
				<div class="form-group">	
				 	<label class="col-sm-2 control-label"></label>
					<div class="col-md-10">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="writenew" value="1">			
						<input type="submit" name="b_jadwal" id="submit-jadwal" value="Submit" class="btn btn-primary">
					</div>
				</div>
				
			</form>
            </div>
            </div>
		</div>
	</div>
<?php
$this->foot();
?>