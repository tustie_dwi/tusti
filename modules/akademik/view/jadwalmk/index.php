<?php $this->head(); 
$header="Jadwal Mata Kuliah";
?>
<div class="row">
<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="#"><?php echo $header;  ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
    <?php if($user!="mahasiswa"&&$user!="dosen"){ ?>
	<a href="<?php echo $this->location('module/akademik/jadwalmk/writenew'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Jadwal Mata Kuliah </a> 
    <?php } ?>
	<a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>" class="btn btn-default"><i class="fa fa-bars"></i> Mata Kuliah Ditawarkan List</a> 
	
	</div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php endif;?>
	<div class="row">
	<div class="col-md-4">	
		<div class="block-box">
		<form class="form-horizontal" action="<?php echo $this->location('module/akademik/jadwalmk'); ?>" role="form" id="param" method="POST">
			<div class="form-group">	
				<label class="control-label">Fakultas</label>				
				<?php echo '<select id="select_fakultas" class="form-control e9" name="fakultas" >'; ?>
				<option value="0">Select Fakultas</option>
				<?php if(count($get_fakultas)> 0) {
					foreach($get_fakultas as $dt) :
						echo "<option value='".$dt->fakultasid."' ";
						if(isset($fakultasid)){
							if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
								echo "selected";
							}
						}
						echo " >".ucWords($dt->keterangan)."</option>";
					endforeach;
				} ?>
				</select>
			</div>
			
			<div class="form-group">	
				<label class="control-label">Cabang</label>
						
					<?php echo '<select id="select_cabang" class="form-control e9" name="cabang" >'; ?>
					<option value="0" data-uri='1'>Select Cabang</option>
					<?php if(count($cabang)> 0) {
						foreach($cabang as $c) :
							echo "<option value='".$c->cabang_id."' ";
							if(isset($cabangid)){
								if($cabangid==$c->cabang_id){
									echo "selected";
								}
							}
							echo " >".ucWords($c->keterangan)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
		
			<div class="form-group">	
				<label class="control-label">Type</label>
				
					<?php echo '<select disabled id="select_type" class="form-control e9" name="type" >'; ?>
					<option value="x" >Select Type</option>
					<?php echo "<option value='course' ";
						if(isset($type)){
							if($type=='course'){
								echo "selected";
							}
						}
						echo " >Course</option>"; ?>
						
					<?php echo "<option value='online' ";
						if(isset($type)){
							if($type=='online'){
								echo "selected";
							}
						}
						echo " >Online</option>"; ?>
					</select>
			</div>
			
			<div id="parameter" >
			<div class="form-group">	
				<label class="control-label">Tahun akademik</label>
				
					<?php echo '<select disabled id="select_thnakademik" class="form-control e9" name="thnakademik" >'; ?>
					<option value="0" data-uri='1'>Select Tahun akademik</option>
					<?php if(count($thnakademik)> 0) {
						foreach($thnakademik as $th) :
							echo "<option value='".$th->tahun_akademik."' ";
							if(isset($thnakademikid)){
								if($thnakademikid==$th->tahun_akademik){
									echo "selected";
								}
							}
							echo " >".ucWords($th->thnakademik)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
			<div class="form-group">	
				<label class="control-label">Prodi</label>
				
					<?php echo '<select disabled id="select_prodi" class="form-control e9" name="prodi">'; ?>
					<option value="0" data-uri='1'>Select Prodi</option>
					<?php if(count($prodi)> 0) {
						foreach($prodi as $p) :
							echo "<option value='".$p->prodi_id."' ";
							if(isset($prodiid)){
								if($prodiid==$p->prodi_id){
									echo "selected";
								}
							}
							echo " >".ucWords($p->keterangan)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
			<div class="form-group">	
				<label class="control-label" >Mata Kuliah</label>
					<?php echo '<select disabled id="select_mk" class="form-control e9" name="mtk" >'; ?>
					<option value="0" data-uri='1'>Select Mata Kuliah</option>
					<?php if(count($matakuliah)> 0) {
						foreach($matakuliah as $mk) :
							echo "<option value='".$mk->mkditawarkan_id."' ";
							if(isset($matakuliahid)){
								if($matakuliahid==$mk->mkditawarkan_id){
									echo "selected";
								}
							}
							echo " >".ucWords($mk->namamk)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
			
			<div class="form-group">	
				<label class="control-label" >Pengampu</label>
				
					<select disabled id="select_pengampu" class="form-control e9" name="pengampu">
					<option value="0">Select Dosen Pengampu</option>
					<?php if(count($pengampu)> 0) {
						foreach($pengampu as $pm) :
							echo "<option value='".$pm->pengampu_id."' ";
							if(isset($pengampuid)){
								if($pengampuid==$pm->pengampu_id){
									echo "selected";
								}
							}
							echo " >".ucWords($pm->nama)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
			<div class="form-group">	
				<label class="control-label">Kelas</label>
				
					<?php echo '<select disabled id="select_kelas" class="form-control e9" name="kelas" >'; ?>
					<option value="0" data-uri='1'>Select Kelas</option>
					<?php if(count($kelas)> 0) {
						foreach($kelas as $k) :
							echo "<option value='".$k->kelas."' ";
							if(isset($kelasid)){
								if($kelasid==$k->kelas){
									echo "selected";
								}
							}
							echo " >".ucWords($k->kelas)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
		</form>
        </div>
	</div>
	
	</div>
	
	<div class="col-md-8">
	<!------------------------TABLE------------------------------------------->
	<!-- <div id="content"></div> -->
		<?php
	 if( isset($posts) ) :	?>
	
		<table class='table table-hover example'>
				<thead>
					<tr>
						<th>&nbsp;</th>
			    	</tr>
				</thead>
				<tbody id='content'>
			
	<?php	if($posts > 0){
				$i='0';
				foreach ($posts as $dt): ?>
					<tr id='column<?php echo $i ?>'>
						<td>
						<div class="col-md-3">
						<small><i class="fa fa-clock-o"></i> <?php echo str_replace('-', '/',$dt->YMD)?>	<?php echo substr($dt->waktu, 0, -3) ?></small>
						</div>
							
						<div class="col-md-6">
							<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/jadwalmk/detail/'.$dt->jadwal_id) ?>">
								<span class='text text-default'><strong><?php echo $dt->namamk ?></strong></span></a>
								<?php if($dt->is_aktif == '1') { ?>
									<span class='label label-success'>Aktif</span>
								<?php	} else { ?>
									<span class='label label-danger'>Not Aktif</span>
								<?php   } ?><br>
							<small><span class="text text-danger"><i class="fa fa-user"></i> <?php echo $dt->pengampu ?></span>&nbsp;
								 <i class="fa fa-bullseye"></i> Kelas <?php echo $dt->kelas ?>&nbsp;
								 <i class="fa fa-calendar-o"></i> <?php echo $dt->hari ?>, <?php echo date("H:i",strtotime($dt->jam_mulai))." - ".date("H:i",strtotime($dt->jam_selesai)) ?><br>
								 <i class="fa fa-map-marker"></i> R. <?php echo $dt->ruang ?>
							</small>
						</div>

						<div class="col-md-3">
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown pull-right'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/jadwalmk/edit/'.$dt->jadwal_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/nilaimhs/add/'.$dt->jadwal_id) ?>"><i class='fa fa-pencil'></i> Nilai</a>	
									</li>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/test/by/jadwal/'.$dt->jadwal_id) ?>"><i class='fa fa-file-text-o'></i> Test by Jadwal</a>	
									</li>
									<li>
									<a class='delete_btn<?php echo $i ?>' href='#' onclick='doDelete(<?php echo $i ?>)' ><i class='fa fa-trash-o'></i> Delete</a>
									<input type='hidden' class='deleted<?php echo $i ?>' value="<?php echo $dt->jadwal_id ?>" uri = "<?php echo $uri_location ?>"/>	
									</li>
								  </ul>
								</li>
							</ul>
							</div>
						</td></tr>
					<?php  
					$i++;
				 endforeach; 
			 } ?>
			</tbody></table>
		<?php 
	
	 else: 
	 ?> 
    <div class="col-md-13" align="center" style="margin-top:20px; ">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
    <!------------------------TABLE------------------------------------------->
	</div>
</div>
</div>
<?php $this->foot(); ?>