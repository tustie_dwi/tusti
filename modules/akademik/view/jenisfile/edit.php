<?php
if($user!="mahasiswa" && $user!="dosen"){
$this->head();

if($posts!=""){
	$header		= "Edit Jenis File";
	
	foreach ($posts as $dt):
	$id			= $dt->hid_id;
	$kodejenis 	= $dt->kode;
	$keterangan	= $dt->jenis;
	$max		= $dt->max;
	$ext		= $dt->ext;
		
	endforeach;
	$frmact 	= $this->location('module/akademik/jenisfile/save');		
	
}else{
	$header		= "Write New Jenis File";
	$id			= "";
	$kodejenis 	= "";
	$keterangan	= "";
	$max		= "";
	$ext		= "";
	$newjen		= 1;
	$frmact 	= $this->location('module/akademik/jenisfile/save');		
}


?>

<div>  
	
    <div class="row">    
    <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/jenisfile'); ?>">Jenis File</a></li>
	  <li class="active"><a href="#"><?php echo str_replace('Write ','',str_replace('Jenis File','',$header));?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if($posts!=""){	?>
			<a href="<?php echo $this->location('module/akademik/jenisfile/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Jenis File</a>
		<?php } ?>
    </div>
    
        <div class="col-md-12">
		
			<form method=post id="upload-form-jenis-file" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
							<input type="text" class="form-control"  name='jenisfilename' required="required" value="<?php if(isset($keterangan)) echo $keterangan; ?>"><br>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Extention</label>
					<div class="controls">
						<div class="col-sm-10">
							<input type="text" class="form-control"  name='extention' required="required" value="<?php if(isset($ext)) echo $ext; ?>"><br>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Max size</label>
					<div class="controls">
						<div class="col-sm-10">
							<input type="text" class="form-control"  name='maxsize' required="required" value="<?php if(isset($max)) echo $max; ?>"><br>
						</div>
					</div>
				</div>
				 <div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="hidden" name="hidId" value="<?php if(isset($id)) echo $id;?>">
						<input type="hidden" name="newjen" value="<?php if(isset($newjen)) echo $newjen;?>">
						<input type="submit" name="b_jenis" onclick="submit_jenis_file()"  value="Submit" class="btn btn-primary">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
}
?>