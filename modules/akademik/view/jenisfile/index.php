<?php if($user!="mahasiswa" && $user != "dosen"){
		 $this->head();	  
		 $header="Jenis File";
?>
<div class="row">
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="#"><?php echo $header;?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/akademik/materimk/'); ?>" class="btn btn-default">
			<i class="fa fa-bars"></i> Materi MK List</a>
			<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
		<a href="<?php echo $this->location('module/akademik/jenisfile/write'); ?>" class="btn btn-primary">
    		<i class="fa fa-pencil"></i> New Jenis File</a><?php } ?>
            </div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	?>
		
		<table class='table table-hover' id='example' data-id='module/akademik/jenisfile/'>
				<thead>
					<tr>
						<th>No</th>
						<th>Jenis File</th>
		<?php
		if($user!="Mahasiswa" && $user!="Dosen"){	?>			
						<th>Act</th>
		<?php
		}?>
					</tr>
				</thead>
				<tbody>
		<?php
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
					$max_file = substr((((($dt->max)/1024)/1024)),0,5);
				?>
					<tr id='post-".$dt->jenis_id."' data-id='".$dt->jenis_id."' valign=top>
						<td><?php echo $i++?></td>
						<td style="width: 550px"><?php echo $dt->jenis?>&nbsp;<span class='label pull-right label-info' style='text-align:center;width:100px;font-weight:normal;'></span>
							<span class='label label-success'><?php echo "Max size ".$max_file." MByte"?></span>
							<br><code>Jenis Extention <?php echo $dt->ext?></code>
						</td> 
						<?php
					if($user!="Mahasiswa" && $user!="Dosen"){ ?>
						<td style='min-width: 80px;'>         
								<ul class='nav nav-pills' style='margin:0;'>
									<li class='dropdown'>
									  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
									  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
										<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/jenisfile/edit/'.$dt->jenis_id)?>"><i class='fa fa-edit'></i> Edit</a>	
										</li>	
										
									  </ul>
									</li>
								</ul>
						</td>
					<?php
					}?>
					</tr>
					<?php
				 endforeach; 
			 } ?>
				</tbody>
		</table>
	<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
    </div>
<?php
$this->foot();
}
?>