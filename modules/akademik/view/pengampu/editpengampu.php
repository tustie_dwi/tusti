<?php
$this->head();

if($posts !=""){
	$header		= "Edit Pengampu";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$namamk			= $dt->namamk;
		$mkditawarkanid	= $dt->mkditawarkan_id;
		$pengampuid		= $dt->pengampu_id;
		$iskoor			= $dt->is_koordinator;
		$nama			= $dt->nama;
		$karyawanid		= $dt->karyawan_id;
	endforeach;
}else{
	$header		= "Write New Mata Kuliah Ditawarkan";
	$id			= "";
	$fakultas_id= "";
	$gettahunid = "";
	$isblok		= "";
	$ispraktikum= "";
	$edit		= "";
	//$ceknew		= '1';
}


?> 
	
	<legend>
		<a href="<?php echo $this->location('module/akademik/pengampu'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Pengampu Mata Kuliah List</a> 
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post name="form" id="form-editpengampu" class="form-horizontal">
				<div class="form-group">
					<label for="namamk" class="col-sm-2 control-label">Nama Dosen Pengampu </label>
						<div class="col-md-10">
							<input disabled type="text" value="<?php if(isset($nama))echo $nama ?>" class="form-control"/>	
						</div>
				</div>
				
				<div class="form-group">
					<label for="namamk typeahead" class="col-sm-2 control-label">Nama Mata Kuliah </label>
						<div class="col-md-10">
							<input required id="namamkpengampu" type="text" value="<?php if(isset($namamk))echo $namamk ?>" class='col-md-6 form-control typeahead'/>	
							<input type="hidden" id="namamk" name="namamk" value="<?php if(isset($mkditawarkanid))echo $mkditawarkanid ?>"/>
						</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Is Koordinator ?</label>
					<div class="col-md-10">
						<label class="checkbox"><input type="checkbox" name="iskoor" value="1" <?php if ($iskoor==1) { echo "checked"; } ?>>Ya</label><br>
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="mkditawarkanid" value="<?php echo $mkditawarkanid ?>"/>
						<input type="hidden" name="karyawanid" value="<?php echo $karyawanid ?>"/>					
						<input type="submit" name="b_pengampu" id="submit-edit" value="Submit" class="btn btn-primary">
					</div>
				</div>
				
			</form>
		</div>
	</div>
<?php
$this->foot();
?>