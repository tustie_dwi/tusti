<?php $this->head(); ?>
<fieldset>
	<legend>

	<a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>" class="btn btn-default pull-right">
    <i class="fa fa-bars icon-white"></i> Mata Kuliah Ditawarkan List</a> 

    Dosen Pengampu List
	</legend>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
		
		//--------------------------------------------------------
	 if( isset($dosen) ) :
		 $i =0;
		 foreach ($dosen as $d) { ?>
		<div class='panel-group' id='accordion'>
		  <div class='panel panel-default'>
		    <div class='panel-heading'>
		      <h4 class='panel-title'>
		        <a data-toggle='collapse' data-parent='#accordion' href='#collapse<?php echo $i ?>'>
		           <i class='fa fa-user'></i> <em>Nama Dosen : <b><?php echo $d->nama ?></em></b>
		        </a>
		      </h4>
		    </div>
		    
		    <div id='collapse<?php echo $i ?>' class='panel-collapse collapse' style='padding-bottom : 40px;'>
		      <div class='panel-body'>
			
			<table class='table table-hover'>
				<thead>
					<tr>
						<th>Mata Kuliah Diampu</th>
						<th>Fakultas</th>
	  <?php if($user!="mahasiswa"&&$user!="dosen"){	?>		
						<th>Act</th>
	  <?php	} ?>
					</tr>
				</thead>
			
			<!-- ------isi accordion-----------------------------------   -->
	  <?php foreach ($datamk as $mk) {
				if($d->karyawan_id==$mk->karyawan_id){ ?>
					 <tbody><tr valign=top><td><?php echo $mk->namamk ?>
					 <?php if($mk->is_koordinator=="1"){ ?>
					 	<code>Koordinator </code>
					 <?php }else {} ?>
					 &nbsp;<span class="label label-info"><small><?php echo '*'.$mk->cabang ?></small></span>
						</td>
						<td><?php echo $mk->fakultas ?></td>
					 
					<?php if($user!="mahasiswa"&&$user!="dosen"){ ?>
						<td style='min-width: 80px;'>   
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/pengampu/editpengampu/'.$mk->pengampu_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td>
					<?php } ?>
					 </tr></tbody>
				<?php }
			} ?>
			<!-- ------END isi accordion-------------------------------- -->
			 </table>
			 <?php if($user!="mahasiswa"&&$user!="dosen"){ ?>
				<div class='well'>
					<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/pengampu/writenamamk/'.$d->hid_karyawan) ?>">
					<button type='button' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah Mata Kuliah</button></a>
					</div>
			<?php } ?>
		    	</div>
		    </div>
		  </div>
		</div>
		<?php $i++;
		}
			
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; 
		//----------------------------------------------------------
	 ?>

    
</fieldset>
<?php $this->foot(); ?>