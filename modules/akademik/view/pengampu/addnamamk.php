<?php
$this->head();

if($dosen !=""){
	$header		= "Tambah Mata Kuliah";
	$iskoor = "";
	$cabangid = '';
	foreach ($dosen as $dt):
		$namadosen		= $dt->nama;
		$karyawanid		= $dt->karyawan_id;
	endforeach;
	$id			= "";
}else{
	$header		= "Tambah";
	$id			= "";
	$fakultas_id= "";
	$gettahunid = "";
	$isblok		= "";
	$ispraktikum= "";
	$edit		= "";
	//$ceknew		= '1';
}


?> 
	
	<legend>
		<a href="<?php echo $this->location('module/akademik/pengampu'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Pengampu Mata Kuliah List</a> 
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post name="form" id="form-addmk" class="form-horizontal">
				<div class="form-group">
					<label for="namamk" class="col-sm-2 control-label">Nama Dosen Pengampu </label>
						<div class="col-md-10">
							<input disabled type="text" value="<?php if(isset($namadosen))echo $namadosen ?>" class="form-control"/>	
						</div>
				</div>
				
				<div class="form-group">
					<label for="namamk typeahead" class="col-sm-2 control-label">Mata Kuliah </label>
						<div class="col-md-10">
							<?php
							foreach ($datamk as $d) {
								echo "<span class='label label-info'>".$d->namamk."</span> ";
							};
							?>
						</div>
				</div>
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Cabang</label>
					<div class="col-md-10">
						<?php echo '<select id="select_cabang" class="e9 form-control" name="cabang" >'; ?>
							<option value="0">Select Cabang</option>
							<?php if(count($cabang)> 0) {
								foreach($cabang as $c) :
									echo "<option value='".$c->cabang_id."' ";
									if($cabangid==$c->cabang_id){
										echo "selected";
									}
									echo " >".$c->keterangan."</option>";
								endforeach;
							} ?>
						</select>
					</div>
				</div>
					
				<div class="form-group">
					<label for="namamk typeahead" class="col-sm-2 control-label">Tambah Mata Kuliah </label>
						<div class="col-md-10">
							<input type="text" disabled id ='addnamamk' placeholder="Nama Mata Kuliah" name="namamk[]" class="form-control typeahead" data-role="tagsinput"/>
						</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="col-md-10">
						<!-- <label class="checkbox"><input type="checkbox" name="iskoor" value="1" <?php if ($iskoor==1) { echo "checked"; } ?>>Ya</label><br>
						 --><input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="karyawanid" value="<?php echo $karyawanid ?>"/>					
						<input type="submit" name="b_tambahmk" id="submit-mkpengampu" value="Submit" class="btn btn-primary">
					</div>
				</div>
				
			</form>
		</div>
	</div>
<?php
$this->foot();
?>