<?php
if($user!="mahasiswa" && $user!="dosen"){ 
$this->head();

if($posts!=""){
	$header		= "Edit Komponen Silabus";
	
	foreach ($posts as $dt):
		$id			= $dt->hidden_id;
		$parentid  	= $dt->parent_id;
		$keterangan	= $dt->keterangan;
		$isaktif	= $dt->is_aktif;
		
	endforeach;
	$frmact 	= $this->location('module/akademik/silabus/komponen/savekomponen');		
	
}else{
	$header		= "Write New Komponen Silabus";
	$id			= "";
	$parentid  	= "";
	$keterangan	= "";
	$isaktif	= 0;
	$newkomp	= 1;
	$frmact 	= $this->location('module/akademik/silabus/komponen/savekomponen');		
}


?>

<div class="row"> 
	<div class="col-md-12">
		<h2 class="title-page"><?php echo $header; ?></h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
		  <li class="active"><a href="#"><?php echo $header;?></a></li>
		</ol>
	
		<div class="breadcrumb-more-action">
			<a href="<?php echo $this->location('module/akademik/silabus/viewkomponen'); ?>" class="btn btn-default"><i class="fa fa-bars"></i> Komponen Silabus List</a> 
			<?php if($posts!=""){	?>
			<a href="<?php echo $this->location('module/akademik/silabus/writekomponen'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Komponen Silabus</a>
			<?php } ?>
		</div>
			
		<form method=post id="upload-form-komponen" action="<?php echo $this->location('module/akademik/silabus'); ?>" class="form-horizontal">
        
                <div class="block-box">
			 <div class="form-group">	
				<label class="col-sm-2 control-label">Sub Komponen dari</label>
				<div class="controls">
					<div class="col-sm-10">
					<select name="cmbkomponen" id="parent" class="form-control e9">
						<option class="sub_01" value="0">Please Select..</option>			
						<?php
															
							foreach($komponen as $dt):
								echo "<option class='sub_".$dt->komponen_id."' value='".$dt->komponen_id."' ";
								if($parentid==$dt->komponen_id){
									echo "selected";
								}
								echo ">".$dt->keterangan."</option>";
							endforeach;
						?>
					</select>
					</div>
				</div>
			</div>
			 <div class="form-group">	
				<label class="col-sm-2 control-label">Keterangan</label>
				<div class="controls">
					<div class="col-sm-10">
						<textarea class="form-control"  name='komponen' required="required" ><?php if(isset($keterangan)) echo $keterangan; ?></textarea><br>
					</div>
				</div>
			</div>
			 <div class="form-group">
				<label class="col-sm-2 control-label">Is Aktif ?</label>
				<div class="controls">
					<div class="col-sm-10">
					<label class="checkbox"><input type="checkbox" name="isaktif" value="1" <?php if ($isaktif==1) { echo "checked"; } ?>>Ya</label><br>
					<input type="hidden" name="hidId" value="<?php if(isset($id)) echo $id;?>">
					<input type="hidden" name="newkomp" value="<?php if(isset($newkomp)) echo $newkomp;?>">
					<input type="submit" onclick="submit_komponen()" name="b_komponen" value="Submit" class="btn btn-primary">
					</div>
				</div>
			</div>	
            </div>			
		</form>

	</div>
</div>
<?php
$this->foot();
}
?>