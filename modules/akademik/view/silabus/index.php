<?php $this->head(); 
$header="Silabus";?>
<div class="row">
	<div class="col-md-12">
		<h2 class="title-page"><?php echo $header; ?></h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
		  <li class="active"><a href="#"><?php echo $header;?></a></li>
		</ol>
		<div class="breadcrumb-more-action">
			<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
			<a href="<?php echo $this->location('module/akademik/silabus/viewkomponen'); ?>" class="btn btn-default">
	    		<i class="fa fa-wrench"></i> Setting Komponen</a>
	    	
	    	<a href="<?php echo $this->location('module/akademik/silabus/writesilabus'); ?>" class="btn btn-primary">
	    		<i class="fa fa-pencil"></i> New Silabus</a>
	    	<?php } ?>
    	</div>
	</div>
    <div class="">
	<div class="col-md-4">
    <div class="block-box">
		<div class="form-group">	
			<label>Fakultas</label>			
			
			<?php if($fakultas_id != '-'){
						echo '<select class="form-control e9" name="fakultas" id="select_indexfakultas" disabled>';
				  }
 				  else echo '<select class="form-control e9" name="fakultas" id="select_indexfakultas">';
			?>
				<option class="sub_01" value="0">Select Fakultas</option>			
				<?php						
					foreach($fakultas as $dt):
						echo "<option class='sub_".$dt->fakultasid."' value='".$dt->hid_id."' ";
						if($fakultas_id==$dt->hid_id){
							echo "selected";
						}
						echo ">".$dt->keterangan."</option>";
					endforeach;
				?>
			</select>				
		</div>
	 	<div class="form-group">	
			<label>Cabang</label>
			<?php echo '<select class="form-control e9" name="cabang" id="select_indexcabang">' ?>
				<option class="sub_01" value="0">Select Cabang</option>			
				<?php						
					foreach($cabang as $dt):
						echo "<option class='sub_".$dt->cabang_id."' value='".$dt->cabang_id."' ";
						echo ">".$dt->keterangan."</option>";
					endforeach;
				?>
			</select>				
		</div>
		<div class="form-group">	
			<label>Tahun Akademik</label>				
			<?php $uri_parent = $this->location('module/akademik/silabus/tampilkan_indexmk'); ?>
				<?php echo '<select class="form-control e9" name="select_thn" id="select_indexthn" disabled data-uri="'.$uri_parent.'">' ?>
				<option value="0">Select Tahun Akademik</option>
				<?php foreach($thnedit as $dt):
						echo "<option class='sub_".$dt->mktahun_akademik."' value='".$dt->thn_id."' ";
						echo ">".$dt->tahun."</option>";
					endforeach;
				?>
			</select>					
		</div>
		<div class="form-group">	
			<label>Mata Kuliah</label>				
			<?php $uri_parent = $this->location('module/akademik/silabus/tampilkan_index');?>
			<?php echo '<select class="form-control e9" name="mkditawarkan" id="select_indexmk" disabled="disabled" data-uri="'.$uri_parent.'" >' ?>
				<option class="sub_01" value="0">Select Mata Kuliah</option>
			</select>					
		</div>
        </div>
	</div>
	<div class="col-md-8" id="display"></div>
    </div>
</div>
<?php $this->foot(); ?>