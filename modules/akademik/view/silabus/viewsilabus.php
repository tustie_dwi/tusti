<?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; ?>
	
		
			 <?php
			 if( isset($posts) ) :	
				?>
                <div class="block-box">
				<table class='table table-hover' id='example' data-id='module/akademik/silabus'>
						<thead>
							<tr>
								<th>Silabus</th>
								<th>MK Ditawarkan</th>
								<th>Fakultas</th>
								<th>Komponen</th>
				<?php
							if($user!="mahasiswa"){ ?>
								<th>Act</th>
				<?php 		} ?>
				
							</tr>
						</thead>
						<tbody>
				<?php
					$i = 1;
					if($posts > 0){
						foreach ($posts as $dt): ?>
							<tr id='post-".$dt->silabus_id."' data-id='".$dt->silabus_id."' valign=top>
								<td style='min-width: 120px;'><?php echo $dt->silabus?>&nbsp;<span class='label pull-right label-info' style='text-align:center;width:150px;font-weight:normal;'></span>
									<br><span class='label label-info'><?php echo $dt->tahun?></span>
										
								</td>
								<td style='min-width: 80px;'>         
									<?php echo $dt->namamk?>&nbsp;<span class='label pull-right' style='width:150px;font-weight:normal;'></span>
									<br><code><?php echo $dt->english?></code>
								</td>
								<td style='width: 150px;'>         
										<?php echo $dt->fak?>
								</td>
								<td style='min-width: 80px;'>         
									<?php echo $dt->komponen?>&nbsp;<span class='label pull-right' style='width:150px;font-weight:normal;'></span>
								</td>
						<?php
						if($user!="mahasiswa" && $user!="dosen"){ ?>
							<td style='min-width: 80px;'>         
									<ul class='nav nav-pills' style='margin:0;'>
										<li class='dropdown'>
										  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
										  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
											<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/silabus/editsilabus/'.$dt->detail_id)?>"><i class='fa fa-edit'></i> Edit</a>	
											</li>	
											
										  </ul>
										</li>
									</ul>
							</td>
						<?php
						} ?>
						</tr>
							<?php	
						 endforeach; 
					 }?>
				</tbody></table>
                </div>
			<?php
			 else: 
			 ?>
		    <div class="span3" align="center" style="margin-top:20px;">
			    <div class="well">Sorry, no content to show</div>
		    </div>
		    <?php endif; ?>