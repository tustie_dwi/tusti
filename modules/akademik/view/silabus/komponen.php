<?php if($user!="mahasiswa" && $user!="dosen"){ $this->head(); 
	$header="Komponen";?>
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="#"><?php echo $header;?></a></li>
	</ol>
	<div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/akademik/silabus/'); ?>" class="btn btn-default">
			<i class="fa fa-bars"></i> Silabus List</a>
			<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
		<a href="<?php echo $this->location('module/akademik/silabus/writekomponen'); ?>" class="btn btn-primary">
    		<i class="fa fa-pencil"></i> New Komponen Silabus</a><?php } ?>
	</div>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	
		?>
        
                <div class="block-box">
		<table class='table table-hover' id='example' data-id='module/akademik/silabus/viewkomponen'>
				<thead>
					<tr>
						<th>No</th>
						<th>Komponen Silabus</th>
		<?php
		if($user!="mahasiswa" && $user!="dosen"){
		?>				
						<th>Act</th>
		<?php
		}
		?>
				</tr>
				</thead>
				<tbody>
		<?php
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
					if ($dt->is_aktif==1){
						$aktif = "Aktif";
						$label = "label-success";
					}
					else{
						$aktif = "Tidak Aktif";
						$label = "label-danger";
					}
		?>
					
				<tr id='post-".$dt->komponen_id."' data-id='".$dt->komponen_id."' valign=top>
					<td><?php echo $i++ ?></td>
					<td><?php echo $dt->keterangan?>&nbsp;<span class='label pull-right label-info' style='text-align:center;width:150px;font-weight:normal;'></span>
						<span class='label <?php echo $label?>'><?php echo $aktif?></span>
		<?php						
					if($dt->parent){
		?>
						<br><code>Sub Komponen dari <?php echo $dt->parent?></code>
		<?php		} ?>
					</td>
		<?php
					if($user!="mahasiswa" && $user!="dosen"){
		?>
					<td style='min-width: 80px;'>         
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/silabus/editkomponen/'.$dt->komponen_id)?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>	
									
								  </ul>
								</li>
							</ul>
					</td>
		<?php		} ?>
				</tr>
		<?php	 endforeach; 
			 } ?>
		</tbody></table>
        </div>
	<?php
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</fieldset>
<?php
$this->foot();
}
?>