<?php
$this->head();
if($user!="mahasiswa" && $user!="dosen"){ 

if($posts!=""){
	$header		= "Edit Silabus";
	
	foreach ($posts as $dt):
		$silid				= $dt->hidsil;
		$detid				= $dt->hiddet;
		$keterangan			= $dt->silabus;
		$komponen_id		= $dt->komponen_id;
		$hid_id				= $dt->fakultas_id;
		$tahun_id			= $dt->tahun_id;
		$mkditawarkan_id	= $dt->idmk;
	endforeach;
	$frmact 	= $this->location('module/akademik/silabus/savesilabus');		
	
}else{
	$header				= "Write New Silabus";
	$silid				= "";
	$detid				= "";
	$mkditawarkan_id	= "";
	$tahun_id			= "";
	$namamkdit			= "";
	$hid_id				= "";
	$komponen_id    	= "";
	$keterangan			= "";
	$new				= 1;
	$frmact 			= $this->location('module/akademik/silabus/savesilabus');		
}


?>

<div class="row"> 
	<div class="col-md-12">
		<h2 class="title-page"><?php echo $header; ?></h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
		  <li class="active"><a href="#"><?php echo $header;?></a></li>
		</ol>
		<div class="breadcrumb-more-action">
			<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
				<a href="<?php echo $this->location('module/akademik/silabus/'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Silabus List</a> 
				<?php if($posts!=""){	?>
					<a href="<?php echo $this->location('module/akademik/silabus/writesilabus'); ?>" class="btn btn-primary pull-right" style="margin:0px 5px"><i class="fa fa-pencil"></i> Write New Silabus</a>
				<?php } ?>
	    	<?php } ?>
    	</div>
	
		<form method=post id="upload-form-silabus" class="form-horizontal" action="<?php echo $this->location('module/akademik/silabus'); ?>" >
        
                <div class="block-box">
			 <div class="form-group">	
				<label class="col-sm-2 control-label">Fakultas</label>
				<div class="controls">
					<div class="col-sm-10">
					<?php $uri_parent = $this->location('module/akademik/silabus/tampilkan_thn'); ?>
					<?php echo '<select class="form-control e9" name="fakultas" id="select_fakultas"  data-uri="'.$uri_parent.'">' ?>
						<option class="sub_01" value="0">Select Fakultas</option>			
						<?php
															
							foreach($fakultas as $dt):
								echo "<option class='sub_".$dt->fakultasid."' value='".$dt->hid_id."' ";
								if($hid_id==$dt->hid_id){
									echo "selected";
								}
								echo ">".$dt->keterangan."</option>";
							endforeach;
						?>
					</select>
					</div>
				</div>
			</div>
			<div class="form-group">	
				<label class="col-sm-2 control-label">Tahun Akademik</label>
				<div class="controls">
					<div class="col-sm-10">
					<?php $uri_parent = $this->location('module/akademik/silabus/tampilkan_mk'); ?>
						<?php echo '<select class="form-control e9" name="select_thn" id="select_thn" disabled data-uri="'.$uri_parent.'">' ?>
						<option value="0">Select Tahun Akademik</option>
						<?php 
						if(isset($thnedit)){
							foreach($thnedit as $dt):
								echo "<option class='sub_".$dt->mktahun_akademik."' value='".$dt->thn_id."' ";
								if($tahun_id==$dt->thn_id){
									echo "selected";
								}
								echo ">".$dt->tahun."</option>";
							endforeach;
							}
						?>
					</select>
					</div>
				</div>
			</div>
			<div class="form-group">	
				<label class="col-sm-2 control-label">MK Ditawarkan</label>
				<div class="controls" id="mk">
					<div class="col-sm-10">
					<?php echo '<select class="form-control e9" name="mkditawarkan" id="select_mk" disabled="disabled">' ?>
						<option class="sub_01" value="0">Select Mata Kuliah</option>			
						<?php
															
							foreach($mkditawarkan as $dt):
								echo "<option class='sub_".$dt->mkditawarkan_id."' value='".$dt->hid_id."' ";
								if($mkditawarkan_id==$dt->hid_id){
									echo "selected";
								}
								echo ">".$dt->keterangan."</option>";
							endforeach;
						?> -->
					</select>
					
					<?php if($mkditawarkan_id!="") { 
						echo "<input type='hidden' name='mkditawarkan' value='".$mkditawarkan_id."'>";
					}?>
					</div>
				</div>
			</div>
			<div class="form-group">	
				<label class="col-sm-2 control-label">Komponen</label>
				<div class="controls">
					<div class="col-sm-10">
					<select class="form-control e9" name="cmbkomponen" id="parent">
						<option class="sub_01" value="0">Please Select..</option>			
						<?php
															
							foreach($komponen as $dt):
								echo "<option class='sub_".$dt->komponen_id."' value='".$dt->komponen_id."' ";
								if($komponen_id==$dt->komponen_id){
									echo "selected";
								}
								echo ">".$dt->keterangan."</option>";
							endforeach;
						?>
					</select>
					</div>
				</div>
			</div>
			 <div class="form-group">	
				<label class="col-sm-2 control-label">Keterangan</label>
				<div class="controls">
					<div class="col-sm-10">
					<textarea class="form-control" name='silabus' required="required" ><?php echo $keterangan; ?></textarea><br>
					</div>
				</div>
	
			</div>
			 <div class="form-group">
			 	<label class="col-sm-2 control-label"></label>
				<div class="controls">
					<div class="col-sm-10">
					<input type="hidden" name="hidsilId" value="<?php if(isset($silid)) echo $silid;?>">
					<input type="hidden" name="hiddetId" value="<?php if(isset($detid)) echo $detid;?>">
					<input type="hidden" name="new" value="<?php if(isset($new)) echo $new;?>">
					<input  type="submit" onclick="submit_silabus()" name="b_silabus" value="Submit" class="btn btn-primary">
					</div>
				</div>
			</div>		
            </div>		
		</form>
	</div>
</div>

<?php
$this->foot();
}
?>