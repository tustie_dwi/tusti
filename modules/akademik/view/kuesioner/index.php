<?php
$this->head();

if(isset($posts) && ($posts)){
	$header		= "Edit Konfigurasi Akademik";			
	$tahun		= $posts->tahun;
	$semester	= $posts->is_ganjil;
	$pendek		= $posts->is_pendek;
	$keterangan	= $posts->keterangan;	
	$aktif		= $posts->is_aktif;
	$judul		= $posts->judul;
	$tmulai 	= $posts->tgl_mulai;
	$tselesai	= $posts->tgl_selesai;
	$hidid		= $posts->kuesioner_id;
	
	
}else{
	$header		= "Konfigurasi";
	$tahun		= date("Y");
	$semester	= '';
	$pendek		= '';
	$keterangan	= '-';	
	$aktif		= 0;
	$judul		= "";
	$tmulai 	= "";
	$tselesai	= "";
	$hidid		= "";
}
?>

<div class="row">  	
    <div class="col-md-12 block-box"> 		
        <div class="col-md-6">	
			<form action="<?php echo $this->location('module/akademik/kuesioner/save'); ?>" method="post"  style="margin-top:1em;" class="form-horizontal">
                	<input type="hidden" name="username" value="<?php echo $user->username; ?>" />
					<input type="hidden" name="hidid" value="<?php echo $hidid; ?>" />
					<h4>Semester</h4>
                	<div class="well">
	                    <div class="control-group">
	                        <label class="control-label">Tahun</label>
	                        <div class="controls">
	                        <input type="number" name='tahun' value="<?php echo $tahun; ?>" class="form-control" pattern="\d{4}" required>
	                        </div>
	                    </div>
					
	                    <div class="control-group">
	                        <label class="control-label">Semester</label>
	                        <div class="controls">
	                       <select name="semester" class="form-control">
								<option value="genap" <?php if($semester=='genap'){  echo "selected";} ?>>GENAP</option>
								<option value="ganjil" <?php if($semester=='ganjil'){  echo "selected";} ?>>GANJIL</option>
							</select>
	                        </div>
	                    </div>
	                    <div class="control-group">
	                        <label class="control-label">Semester Pendek ?</label>
	                        <div class="checkbox">
							<label><input type="checkbox" name='pendek' value='pendek' <?php if($pendek=='pendek'){  echo "checked"; } ?>> Ya</label>
	                        </div>
	                    </div>    
                    </div>
					<h4>General Info</h4>
					
					<div class="well">
						<div class="control-group">
	                        <label class="control-label">Judul</label>
	                        <div class="controls">
								<input type="text" class="form-control" name='judul' value="<?php echo $judul; ?>" required >
	                        </div>
	                    </div>
						<div class="control-group">
	                        <label class="control-label">Keterangan</label>
	                        <div class="controls">
								<textarea name="catatan" class="form-control"><?php echo $keterangan ?></textarea>
	                        </div>
	                    </div>
						<div class="control-group">
							<label class="control-label">Pelaksanaan</label>
							<div class="controls">
								<input type="text" name="tmulai" class="form-control date" placeholder="Tgl Mulai" value="<?php echo $tmulai;  ?>"> s/d <input type="text" name="tselesai" class="form-control date" placeholder="Tgl Selesai" value="<?php echo $tselesai;  ?>" >
							</div>
						</div>
						<div class="control-group">
	                        <label class="control-label">&nbsp;</label>
	                        <div class="controls">
								<input type="checkbox" name="chkaktif" value="1" <?php if($aktif=='1'){  echo "checked"; } ?>> Aktif
	                        </div>
	                    </div>
					</div>
					
					<h4>Setting MK</h4>
					<div class="well">
						<?php
						foreach($prodi as $row):
						?>
						<div class='control-group'> 
							<label class="control-label"><b><?php echo $row->value ?></b></label>
							<div class="controls">
							<input type="hidden" name="hidprodi[]" value="<?php echo $row->id ?>">
							<?php
							$temp = array();
							
							if($hidid){
								$mkid = $mconf->get_kuesioner_detail($hidid, $row->id);								
								if($mkid){
									foreach($mkid as $dt):
										$temp[] = $dt->matakuliah_id;
										echo "<input type=hidden name='hidtmp".$row->id."[]' value='".$dt->matakuliah_id."' >";
									endforeach;	
								}								
							}
												
							echo "<select name='cmbmk".$row->id."[]'  class='cmbmulti form-control'  multiple >";
								foreach($mk as $dt):
									echo  "<option value='".$dt->id."' ";
									if(in_array($dt->id, $temp)){
										echo  "selected";
									}
									echo ">".$dt->value."</option>";
								endforeach;
							?>
							</select>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
					
                    <div class="well">
						<input type="submit" name='b_conf' id='b_conf' value='  Save Konfigurasi  ' class='btn btn-primary btn-update'>
                    </div>
            </form>
		</div>
		<div class="col-md-6">	
			<?php $this->view('kuesioner/list.php', $data); ?>
		</div>
	</div>
</div>
		   
<?php	
$this->foot();

?>