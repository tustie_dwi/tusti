<?php $this->head(); ?>
	<legend>Daftar Peminat MK Pilihan
		&nbsp;<a href="<?php echo $this->location('module/akademik/kuesioner/daftar/'.$kuesioner.'/report'); ?>" class="btn btn-primary pull-right" target="_blank">
		<i class="fa fa-table"></i> Export To Excel</a>&nbsp;
		&nbsp;<a href="<?php echo $this->location('module/akademik/kuesioner'); ?>" class="btn btn-default pull-right">
		<i class="fa fa-bars"></i> Setting MK Pilihan</a>&nbsp;			
		&nbsp;<a href="<?php echo $this->location('module/akademik/kuesioner/daftar/'.$kuesioner.'/peminat'); ?>" class="btn btn-default pull-right">
		<i class="fa fa-bars"></i> Jumlah Peminat By MK</a>&nbsp;
	
	</legend>
	 <?php 
	if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	?>
	<?php		
		 if( isset($posts) ) :	
			?>
			<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
			<?php
				$i = 1;
				
				if($posts){				
					$i=0;
					foreach($posts as $dt){
						$i++;					
						?>
						<tr valign="top">
							<td>
								<div class="col-md-6">
								<b><a href="#" class="text-default"><?php echo $dt->nama; ?></a></b> &nbsp; <small><span class="text text-info"> <i class="fa fa-clock-o"></i> <?php echo date("M d, Y", strtotime($dt->tgl_isi)); ?></span></small> &nbsp; <br>
								<small><abbr title="NIM <?php echo $dt->nim; ?>"><?php echo $dt->nim; ?></abbr></small>
								<span class="label label-warning"><?php echo $dt->prodi; ?></span>							
								</div>
								<div class="col-md-6">
									<span class="badge">Semester <?php echo $dt->semester; ?></span><br>
									<?php
									$mk = $mconf->get_mk_pilihan_mhs($kuesioner, $dt->mahasiswa_id);
									
									if($mk):
										foreach($mk as $row):
											?>
											<code><i class="fa fa-check-square-o"></i> <?php echo $row->nama_mk; ?></code>
											<?php
										endforeach;
									endif;
									?>
								</div>
							</td>
						</tr>
						<?php
					}
				 }
			?>
			</tbody></table><?php
			
		 else: 
		 ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php endif; ?>
<?php $this->foot(); ?>