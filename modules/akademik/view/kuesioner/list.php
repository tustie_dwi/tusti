
	<div class="row">
		
		<?php
		 if( isset($kuesioner) ) :	
		?><table class='table table-hover' id='example'>
			<thead>
				<tr>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($kuesioner as $dt):
				?>
					<tr><td>
						<div class="col-md-9"><b><?php echo $dt->judul ?></b> <small><span class="text text-danger"><?php echo $dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek?></span> </small>
						<?php if($dt->is_aktif==1) echo '<span class="label label-success">Aktif</span>' ?><br>
						<small> <i class="fa fa-calendar-o"></i> <?php echo date("M d, Y", strtotime($dt->tgl_mulai)) ?> - <?php echo date("M d, Y", strtotime($dt->tgl_selesai)) ?></small>
						</div>
						<div class="col-md-3">
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									
								
									<li>
										<a href="<?php echo $this->location('module/akademik/kuesioner/daftar/'.$dt->kuesioner_id); ?>"><i class='fa fa-book'></i> Data peminat</a>	
									</li>
									<li>
										<a href="<?php echo $this->location('module/akademik/kuesioner/edit/'.$dt->kuesioner_id); ?>"><i class='fa fa-pencil'></i> Edit</a>	
									</li>
									
								  </ul>
								</li>
							</ul>
						</div>
					</td></tr>
					<?php
				endforeach;
				?>
			</tbody>
			</table>
					
		<?php
		 else: 
		 ?>
	
			<div class="well">Sorry, no content to show</div>
		<?php endif; ?>
	</div>

