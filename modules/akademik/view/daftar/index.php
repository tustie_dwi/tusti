<?php $this->head(); ?>
	<legend>Daftar Ulang List
		&nbsp;<a href="<?php echo $this->location('module/akademik/daftar/report/'.$semesterid); ?>" class="btn btn-primary pull-right" target="_blank">
		<i class="fa fa-table"></i> Export To Excel</a>&nbsp;
		<?php if(isset($jenis)){ ?>
			&nbsp;<a href="<?php echo $this->location('module/akademik/daftar'); ?>" class="btn btn-default pull-right">
			<i class="fa fa-bars"></i> Need To Validate List</a>&nbsp;
		<?php }else{ ?>
		&nbsp;<a href="<?php echo $this->location('module/akademik/daftar/valid'); ?>" class="btn btn-default pull-right">
		<i class="fa fa-bars"></i> Data Valid List</a>&nbsp;
		<?php } ?>
	</legend>
	 <?php 
	if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	?>
	
	<div class="row-fluid">
		<div class="span12">
		<form class="form-inline" method="post" action="<?php echo $url ?>" >	
				<div class="form-group">	
						<label>Semester</label>
						<select name="cmbsemester" onChange='form.submit();'>
							<option value="-">Please Select..</option>
							<?php
							foreach($semester as $dt):
								echo "<option value='".$dt->tahun_akademik."' ";
								if($semesterid==$dt->tahun_akademik){
									echo "selected";
								}
								echo ">".ucwords($dt->tahun." ".$dt->is_ganjil." ".$dt->is_pendek)."</option>";
							endforeach;
							?>
						</select>		
				</div>
							
			</form>		
		</div>
	</div>
	<br>
	 
		<?php		
		 if( isset($posts) ) :	
			?>
			<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th width="30%">&nbsp;</th>
						<th>Status</th>
						<th>Last Update</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
			<?php
				$i = 1;
				
				if($posts){				
					$i=0;
					foreach($posts as $dt){
						$i++;
						if($dt->is_valid==0){
							$valid = "<span class='label label-danger'>Need to Validate</span>";
						}else{
							if($dt->is_finish==0){
								$valid = "<span class='label label-warning'>Data valid</span>";
							}else{
								$valid = "<span class='label label-success'>* Finished</span>";
							}
						}
						
						
						?>
						<tr id="post-<?php echo $dt->id; ?>" data-id="<?php echo $dt->id; ?>" valign="top">
							<td width=60%><b><a href="<?php echo $this->location('module/akademik/daftar/detail/'.$semesterid.'/'.$dt->id); ?>" class="text-default"><?php echo $dt->nama; ?></a></b><br>
							<code><abbr title="NIM <?php echo $dt->nim; ?>"><?php echo $dt->nim; ?></abbr></code>
							<span class="label label-info"><?php echo $dt->prodi; ?></span></td>
							<td>
							<small>Tgl Daftar :</small>  <span class="text text-warning"> <i class="fa fa-clock-o"></i> <?php echo date("M d, Y", strtotime($dt->tgl_daftar)); ?></span>
							<br><?php echo $valid; ?>
							<?php /*echo in_array($mod->id, $active_modules) ? 
									'<div class="label label-success" id="status-'.$mod->id.'">Active</a>' : 
									'<div class="label" id="status-'.$mod->id.'">Inactive</a>';*/ ?>
							</td>
							<td><small><span class='text text-warning'><em><?php echo $dt->user; ?></em></span><br>
							<i class='fa fa-clock-o'></i>&nbsp;<?php if($dt->last_update){ echo date("M d, Y H:i", strtotime($dt->last_update));} else{  echo date("M d, Y H:i", strtotime($dt->tgl_daftar));}?>					
							</small></td>
							<?php 
							if($dt->is_valid==1){ 
								echo "<td>&nbsp;</td>";
							}else{ ?>		
							<td>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									
									<!--<li>
										<a href='<?php// echo $this->location('module/akademik/daftar/activate/'.$dt->daftar_id); ?>'><i class='fa fa-refresh'></i> Toggle Status</a>	
									</li>-->
									
									<li>
										<a href="<?php echo $this->location('module/akademik/daftar/detail/'.$semesterid.'/'.$dt->id); ?>"><i class='fa fa-pencil'></i> Validate</a>	
									</li>
									
								  </ul>
								</li>
							</ul>
						</td>
						<?php } ?>
						</tr>
						<?php
					}
				 }
			?>
			</tbody></table><?php
			
		 else: 
		 ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php endif; ?>
<?php $this->foot(); ?>