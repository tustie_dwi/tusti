<?php
$this->head();

if($krs !=""){
	$header		= "Edit KRS";	
	
	foreach ($krs as $row):	
		$id			= $row->mahasiswa_id;
		$namamk		= $row->namamk;
		$nama		= $row->nama;
		$nim		= $row->nim;
		$strsemester   = $row->tahun.' - '.strToUpper($row->is_ganjil);
		$semesterid	= $row->tahun_akademik;
	endforeach;	

	
	/*foreach($pengampu as $row):
		$pengampu		= $row->nama;
	endforeach;*/
}else{

	$header			= "Write New KRS";
	$id				= "";
	$namamk			= "";
	$nama			= "";	
	$nim			= "";
	
	$strsemester  = $semester->tahun.' - '.strToUpper($semester->is_ganjil). ' '. strToUpper($semester->is_pendek);
	$semesterid	= $semester->tahun_akademik;
}
?>

<div class="container-fluid">  
	<fieldset>
	<legend>
		<a href="<?php echo $this->location('module/akademik/krs'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> KRS List</a> 
		<?php if($krs !=""){	?>
		<a href="<?php echo $this->location('module/akademik/krs/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New KRS</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row-fluid">    
        <div class="span12">
<?php		
			$str = "<form method=post  action=".$this->location('module/akademik/krs/save').">";								
						$str.= "<label>Semester</label>";
						$str.= "<span class='uneditable-input'>$strsemester</span><input type=hidden name=semesterid value='".$semesterid."'><br>";
						$str.= "<label>Mahasiswa</label>";
						$str.= "<span class='input-xlarge uneditable-input'>".$nim." - ".$nama."</span><br>";
						
						$str.= "<label>KRS</label>";
						/*if($namamk!=""){
							$str.= "<ul class='token-input-list'>";
								$namamk = explode("@", $namamk);
								for($i=0;$i<count($namamk);$i++){
									$str.= "<li class='token-input-token'>
												<p>".($i+1).".&nbsp;".$namamk[$i]."</p>
											</li>";
								}			
							$str.= "</ul>";
						}
						$str.= "<textarea name='krs' id='token-krs'></textarea>"; */
						$temp = array();
						if($mkmhs){													
							foreach($mkmhs as $dt):
								$temp[] = $dt->matakuliah_id;
							endforeach;							
						}
											
						$str.= "<select name='cmbmk[]'  multiple class='cmbmulti span12 populate'>";
							foreach($mk as $dt):
								$str.= "<option value='".$dt->id."' ";
								if(in_array($dt->id, $temp)){
									$str.= "selected";
								}
								$str.= ">".$dt->value."</option>";
							endforeach;
						$str.= "</select><br><br><br></div></div>";
						
						$str.= "<input type=hidden name=hidId value='".$id."'><input type=hidden name=hiduser	value='".$user->username."' >";
						$str.= "<input type=submit name='b_krs' id='b_krs' value='  Save  ' class='btn btn-primary'>";
					$str.= "</form>";					
			echo $str;
		echo "</div></div></fieldset></div>";
$this->foot();

?>