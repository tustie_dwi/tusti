<?php
$this->head();

$header		= "View Agenda ";	

/*if($posts){
	foreach ($posts as $dt):
		$id		= $dt->daftar_id;
		$nim	= $dt->nim;
		$nama	= $dt->nama;
		$prodi	= $dt->prodi;
		$alamat	= $dt->alamat;
		$lahir	= $dt->tgl_lahir;
		$tempat	= $dt->tmp_lahir;
		$email	= $dt->email;
		$prodi	= $dt->prodi;
		$angkatan	= $dt->angkatan;	
		$jalur	= $dt->jalur_seleksi;
		$telp	= $dt->telp;
		$hp		= $dt->hp;
		$ortu	= $dt->orang_tua;
		$aortu	= $dt->alamat_ortu;
		$eortu	= $dt->email_ortu;
		$tortu	= $dt->telp_ortu;
		$hortu	= $dt->hp_ortu;
		$valid	= $dt->is_valid;
		$asurat	= $dt->alamat_surat;
		$catatan = $dt->catatan;
	endforeach;
}else{
	$id		= "";
	$nim	= "";
	$nama	= "";
	$prodi	= "";
	$alamat	= "";
	$email	= "-";
	$prodi	="-";
	$angkatan="-";
	$angkatan	= "-";	
	$jalur	= "-";
	$telp	= "-";
	$hp		= "-";
	$ortu	= "-";
	$aortu	= "-";
	$eortu	= "-";
	$tortu	= "-";
	$hortu	= "-";
	$valid	= 0;
	$asurat	= "-";
		$catatan = "-";
}*/

if(isset($posts)){
	foreach ($posts as $mhs):
		$id		= $mhs->daftar_id;
		$nim = $mhs->mahasiswa_id;
		$nama = $mhs->nama;
		$lahir	= $mhs->tmp_lahir;
		$tgl	= $mhs->tgl_lahir;
		$email	= $mhs->email;
		$seleksi= $mhs->jalur_seleksi;
		$prodi	= $mhs->prodi;
		$prodiid= $mhs->prodi_id;
		$alamat	= $mhs->alamat;
		$telp	= $mhs->telp;
		$hp		= $mhs->hp;
		$ortu	= $mhs->orang_tua;
		$emailo	= $mhs->email_ortu;
		$alamato= $mhs->alamat_ortu;
		$telpo	= $mhs->telp_ortu;
		$hpo	= $mhs->hp_ortu;
		$catatan= $mhs->catatan;
		$alamats= $mhs->alamat_surat;
		$valid	= $mhs->is_valid;
		$semester=$mhs->tahun_akademik;
		$prodi	= $mhs->prodi;
		$angkatan	= $mhs->angkatan;	
	endforeach;
}else{
	$nim	= "";
	$nama 	= "";
	$lahir	= "";
	$tgl	= "";
	$email	= "";
	$seleksi= "";
	$prodi	= "";
	$prodiid= "";
	$alamat	= "";
	$telp	= "";
	$hp		= "";
	$ortu	= "";
	$emailo	= "";
	$alamato= "";
	$telpo	= "";
	$hpo	= "";
	$catatan= "";
	$alamats= "";
	$valid	= 0;
	$semester = "";
	$prodi	="-";
	$angkatan="-";
}

?>
<div class="container-fluid">  	
	<legend>
		<a href="<?php echo $this->location('module/akademik/daftar'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Daftar Ulang List</a>
		Data Mahasiswa
    </legend>
    <div class="row-fluid">    
      
		<form action="<?php echo $this->location('module/akademik/daftar/save'); ?>" method=post name='frmMain'>
		
			<!--<div class="col-md-12">
			<h3>General Info</h3>
			<table class='table table-bordered'>
				<tbody>
					<tr>
						<td width="20%"><em>NIM</em></td>
						<td><b><?php if($nim){ echo $nim;} else{ echo "Lain-lain";} ?></b></td>
					</tr>
					<tr>
						<td width="20%"><em>Nama</em></td>
						<td><b><?php echo $nama; ?></b></td>
					</tr>
					<tr>
						<td width="20%"><em>Angkatan</em></td>
						<td><?php echo $angkatan; ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Prodi</em></td>
						<td><?php if($prodi){ echo $prodi;} else {echo "-";} ?></td>
					</tr>							
				</tbody>
			</table>
			
			<h3>Biodata Mahasiswa</h3>
			<table class='table table-bordered'>
				<tbody>
					<tr>
						<td width="20%"><em>Tempat, Tgl Lahir</em></td>
						<td><?php 
						if($tempat){
								echo $tempat;		
							
							}else{
								echo "-";
							}
							echo "</span> &nbsp;<i class='fa fa-clock-o'></i> <span class='text text-warning'>".date("M d, Y",strtotime($dt->tgl_lahir))."</span>"; ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Jalur Masuk</em></td>
						<td><?php 
						if($jalur){
							if($jalur=='sap'){ echo "Alih Jenjang/SAP"; }else{ echo ucWords($jalur); }
					
						} else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Alamat Di Malang</em></td>
						<td><?php if($alamat){echo $alamat;  } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Email</em></td>
						<td><?php if($email){echo $email;  } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Telepon</em></td>
						<td><?php if($telp){echo $telp;  } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>HP</em></td>
						<td><?php if($hp){echo $hp;  } else{ echo "-"; } ?></td>
					</tr>
				</tbody>
			</table>
			
		
		<h3>Biodata Orang Tua</h3>
		<table class='table table-bordered'>
				<tbody>
					<tr>
						<td width="20%"><em>Nama Orang Tua</em></td>
						<td><?php if($ortu){echo $ortu; } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Alamat</em></td>
						<td><?php if($aortu){echo $aortu;  } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Email</em></td>
						<td><?php if($eortu){echo $eortu;  } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Telepon</em></td>
						<td><?php if($tortu){echo $tortu;  } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>HP</em></td>
						<td><?php if($hortu){echo $hortu;  } else{ echo "-"; } ?></td>
					</tr>
				</tbody>
			</table>
			
			<h3>Surat Menyurat</h3>
			<table class='table table-bordered'>
				<tbody>
					<tr>
						<td width="20%"><em>Alamat</em></td>
						<td><?php if($asurat){echo $asurat; } else{ echo "-"; } ?></td>
					</tr>
					<tr>
						<td width="20%"><em>Catatan</em></td>
						<td><?php if($catatan){echo $catatan;  } else{ echo "-"; } ?></td>
					</tr>
					
				</tbody>
			</table>
		</div>-->
		
		<div class="span6">
		
		<h4>Biodata Pribadi</h4>
		<div class="well">
			<div class="form-group">
				<div class="control-label">Nama</div>	
				<input type="text" name="nama" value="<?php echo ucWords(strToLower($nama)); ?>" placeholder="Nama lengkap"  required>	
				<input type="hidden" name="hidmhs" value="<?php echo $nim; ?>">	
				<input type="hidden" name="hidsemester" value="<?php echo $semester; ?>">					
			</div>
			<div class="form-group">
				<div class="control-label">TTL</div>	
				<input type="text" name="lahir" value="<?php echo $lahir; ?>" placeholder="Tempat lahir" required>&nbsp;
				<input type="text" name="tgl" class="tgl form-control span4" value="<?php echo $tgl; ?>" placeholder="Tgl lahir YYYY-MM-DD"  required ></small>
			</div>
			<div class="form-group">
				<div class="control-label">Jalur Seleksi</div>	
				<!--<input type="text" name="jalurmasuk" value="<?php //echo $seleksi; ?>" placeholder="Jalur seleksi masuk UB" required>-->
				<select name="jalurmasuk">
					<option value="reguler" <?php if($seleksi =='reguler'){ echo "selected"; } ?>>Reguler</option>
					<option value="sap"  <?php if($seleksi =='sap'){ echo "selected"; } ?>>Alih Jenjang/SAP</option>
				</select>
			</div>
			<div class="form-group">
				<div class="control-label">Angkatan</div>	
				<input type="text" name="angkatan" value="<?php echo $angkatan; ?>" placeholder="Program studi" readonly>	
			</div>
			<div class="form-group">
				<div class="control-label">Program Studi</div>	
				<input type="text" name="prodi" value="<?php echo $prodi; ?>" placeholder="Program studi" readonly>		
				<input type="hidden" name="hidprodi" value="<?php echo $prodiid; ?>">
			</div>
			<div class="form-group">
				<div class="control-label">Email</div>
				<input type="email" name="email" class="span6" value="<?php echo $email; ?>" placeholder="Email aktif" required>
			</div>
			<div class="form-group">
				<div class="control-label">Alamat Malang</div>			
				<textarea name="alamat" placeholder="Alamat di Malang" required><?php echo $alamat; ?></textarea>		
			</div>
			
			<div class="form-group">
				<div class="control-label">Telp</div>
				<input type="text" name="telp" class="span6" value="<?php echo $telp; ?>" placeholder="Telepon" required>
			</div>
			
			<div class="form-group">
				<div class="control-label">HP</div>
				<input type="text" name="hp" class="span6" value="<?php echo $hp; ?>" placeholder="HP" required>
			</div>
		</div>
	</div>
	<div class="span6">
		<h4>Data Orang Tua</h4>
			<div class="well">
				<div class="form-group">
					<div class="control-label">Nama</div>	
					<input type="text" name="namaortu" value="<?php echo $ortu; ?>" placeholder="Nama orang tua" required>			
				</div>
				<div class="form-group">
					<div class="control-label">Email</div>
					<input type="email" name="emailortu" value="<?php echo $emailo; ?>" placeholder="Email orang tua" class="span6">
				</div>
				<div class="form-group">
					<div class="control-label">Alamat</div>			
					<textarea name="alamatortu" placeholder="Alamat orang tua" required><?php echo $alamato; ?></textarea>		
				</div>
				
				<div class="form-group">
					<div class="control-label">Telp</div>
					<input type="text" name="telportu" value="<?php echo $telpo; ?>" placeholder="Telepon orang tua" class="span6" required>
				</div>
				
				<div class="form-group">
					<div class="control-label">HP</div>
					<input type="text" name="hportu" class="span6" value="<?php echo $hpo; ?>" placeholder="HP orang tua" required>
				</div>
			</div>
		
			<h4>Surat Menyurat</h4>
			<div class="well">
				<div class="form-group">
				<span class="text text-danger">Tuliskan alamat surat menyurat dengan benar. Apabila alamat surat menyurat berbeda dengan alamat orang tua, beri keterangan pada kolom <b>Catatan</b></span>
				</div>
				<div class="form-group">
					<div class="control-label">Alamat</div>			
					<textarea name="alamatsurat" placeholder="Alamat surat menyurat" required><?php echo $alamats; ?></textarea>		
				</div>
				
				<div class="form-group">
					<div class="control-label">Catatan *)</div>			
					<textarea name="catatan" placeholder="Catatan"><?php echo $catatan; ?></textarea>		
				</div>
			</div>
		</div>
	</div>
	<?php if($valid==0){ ?>
		<div class="form-actions">
		<input type=hidden name="hidId" value="<?php echo $id;?>"><input type=hidden name="hidData" id="hidData">					
		<input type=submit name='b_proses' id='b_agenda' value='   Data Valid & Proses Dafar Ulang  ' class='btn btn-info'>&nbsp;
		<!--<a href="<?php //echo $this->location('module/akademik/daftar/detail/'.$dt->id); ?>"><i class='fa fa-pencil'></i> Validate</a>&nbsp;-->
			<a href="<?php echo $this->location('module/akademik/daftar'); ?>" class="btn btn-default">Cancel</a>			
		</div>
		<?php } ?>
		</form>
</div>


<?php		

$this->foot();
?>