<?php $this->head(); ?>
	<legend>Peminat MK
		&nbsp;<a href="<?php echo $this->location('module/akademik/daftar/report_peminat'); ?>" class="btn btn-primary pull-right" target="_blank">
		<i class="fa fa-table"></i> Export To Excel</a>&nbsp;
		
			
		&nbsp;<a href="<?php echo $this->location('module/akademik/daftar/krs'); ?>" class="btn btn-default pull-right">
		<i class="fa fa-bars"></i> Data List</a>&nbsp;
	
	</legend>
		

		<?php		
		 if( $posts ) :	
			?>
			<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
			<?php

					$total = 0;
					foreach($posts as $dt):
						$total = $total + $dt->jml;												
						?>
						<tr valign="top">
							<td><div class="col-md-1"><b><?php echo $dt->kode_mk ?></b></div><div class="col-md-4"><?php echo $dt->nama_mk ?></div><div class="col-md-2"><?php echo $dt->sks ?> sks</div>
							<div class="col-md-5"><?php echo $dt->jml ?> mahasiswa</div></td>						
						</tr>
						<?php
					endforeach;
				
			?>
			<!--<tr>
				<td><b><div class="col-md-7"><div class="pull-right">Total</div></div><div class="col-md-5"><?php echo $total;?></b> </div></td>
			</tr>	-->
			</tbody></table><?php
			
		 else: 
		 ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php endif; ?>
<?php $this->foot(); ?>