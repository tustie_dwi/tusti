<?php 
$this->head();
if(isset($mhs)){
	$nim = $mhs->mahasiswa_id;
	$nama = $mhs->nama;
	$lahir	= $mhs->tmp_lahir;
	$tgl	= $mhs->tgl_lahir;
	$email	= $mhs->email;
	$seleksi= $mhs->jalur_seleksi;
	$prodi	= $mhs->prodi;
	$prodiid= $mhs->prodi_id;
	$alamat	= $mhs->alamat;
	$telp	= $mhs->telp;
	$hp		= $mhs->hp;
	$pembimbingid = $mhs->dosen_pembimbing;
	$pembimbing = $mhs->pembimbing;
	$tgldaftar	= $mhs->tgl_daftar;
	
}else{
	$nim	= "";
	$nama 	= "";
	$lahir	= "";
	$tgl	= "";
	$email	= "";
	$seleksi= "";
	$prodi	= "";
	$prodiid= "";
	$alamat	= "";
	$telp	= "";
	$hp		= "";
	$pembimbingid="";
	$pembimbing="";
	$tgldaftar	= date("Y-m-d H:i");;
	
}

?>
<legend class="hide-from-print">Daftar Semester Pendek
		<a href="<?php echo $this->location('module/akademik/daftar/krs'); ?>" class="btn btn-default pull-right">
			<i class="fa fa-bars"></i> Back To List</a>
	</legend>
<form method="post" id="form-save-daftar" role="form" action="<?php echo $this->location('info/akademik'); ?>">
			<div class="col-lg-6 hide-from-print">
				
				<h4>Biodata Pribadi</h4>
				<div class="well">
					<div class="form-group">
						<div class="control-label">Nama</div>	
						<input type="text" name="nama" value="<?php echo ucWords(strToLower($nama)); ?>" placeholder="Nama lengkap"  readonly>	
						<input type="hidden" id="hidmhs" name="hidmhs" value="<?php echo $nim; ?>">
						<input type="hidden" id="hidtgl" name="hidtgl" value="<?php echo $tgldaftar; ?>">
						<input type="hidden" id="hidsemester" name="hidsemester" value="<?php echo $semester; ?>">							
					</div>
										
					<div class="form-group">
						<div class="control-label">Program Studi</div>	
						<input type="text" name="prodi" value="<?php echo $prodi; ?>" placeholder="Program studi" readonly>		
						<input type="hidden" name="hidprodi" value="<?php echo $prodiid; ?>">
					</div>
					<div class="form-group">
						<div class="control-label">Email</div>
						<input type="email" name="email" class="span6" value="<?php echo $email; ?>" placeholder="Email aktif" required>
					</div>
										
					<div class="form-group">
						<div class="control-label">Telp</div>
						<input type="text" name="telp" class="span6" value="<?php echo $telp; ?>" placeholder="Telepon" required>
					</div>
					
					<div class="form-group">
						<div class="control-label">HP</div>
						<input type="text" name="hp" class="span6" value="<?php echo $hp; ?>" placeholder="HP" required>
					</div>
					<div class="form-group">
						<label class="control-label">Dosen Pembimbing</label>
						<select class="col-md-12 e2" name="cmbpembimbing">
							<option value="-">Please Select</option>
						<?php
						 if($dosen):
							foreach($dosen as $dt):
								?>
								<option value="<?php echo $dt->id; ?>" <?php if($pembimbingid==$dt->id) echo "selected";?>><?php echo $dt->name; ?></option>
								<?php
							endforeach;
						 endif;
						?>
						</select>
					</div>
				</div>
				<?php
				if(!$daftar):
				?>
				
				<div class="form-actions hide-from-print">					
						<input type="submit" id="btn-save-daftar" name="b_proses" value = " Validasi Semester Pendek " class="btn btn-primary" onClick="return save_proses();">
						<a href="#" id="btn-cetak-daftar"  class="btn btn-danger" onClick="return cetak_kuitansi();"><i class="fa fa-print"></i> Cetak Lembar Pengisian</a>
						<span class="status-daftar" style="margin-left:1em;font-weigt:bold;">&nbsp;</span>					
				</div>
				<?php else: ?>
					<div class="clearfix"></div>
						<div class="form-actions hide-from-print">						
							<input type="submit" id="btn-save-daftar" name="b_proses" value = " Validasi Semester Pendek " class="btn btn-primary" onClick="return save_proses();">
							<a href="#" id="btn-cetak-daftar-ok"  class="btn btn-danger" onClick="return cetak_kuitansi();"><i class="fa fa-print"></i> Cetak Lembar Pengisian</a>						
					</div>
				<?php endif; ?>
			</div>
			
			<div class="col-lg-6" id="content-print">
				<h4>Matakuliah Semester Pendek</h4>			
				<?php $mconf = new model_confinfo(); 
				if($daftar) echo '<input type=hidden name=hidtmp value="'.$nim.'">';
				
				echo '<input type=hidden name=hiduser value="'.$this->coms->authenticatedUser->id.'">';
				?>
				
				<div class="form-tambah-mk hide-from-print">
						<div class="form-group">
							<div class="input-group">							
								<select class="col-md-12" id="sel-mk">
									<?php 
										
										$mk = $mconf->get_mkditawarkan(0,$semester);
										$biaya = $mconf->konfigurasi_akademik();
										
										foreach($mk as $key){
											echo "<option value='".$key->id.'|'.$key->sks.'|'.$key->kode_mk .' - '.$key->namamk."'>" . '' . $key->kode_mk . ' - '.$key->namamk ." [".$key->sks." sks] </option>";
										}
									?> 
								</select>
								<span class="input-group-btn">
									<button class="btn btn-primary" type="button" onclick="addmk(document.getElementById('sel-mk').value)">Pilih MK <i class="fa fa-check"></i></button>
								</span>
							</div>				  
					</div></div>
					
				<?php 
						
				$krs = $mconf->get_krs_tmp_mhs($semester, $nim);
				if($krs):
					?>
					<div class="form-group table-mk hide-from-print">
					<table class="table">
							<thead>
								<tr>
									<th>
										<div class="col-md-6">MK</div>
										<div class="col-md-2">SKS</div>
										<div class="col-md-3">Biaya</div>
										<div class="col-md-1">&nbsp;</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="wrap-mk">
										<?php 
										$sks = 0;
										$total = 0;
										foreach($krs as $key): 
											$sks = $sks + $key->sks;
											$nbiaya = ($key->inf_biaya * $key->sks);
											$total = $total + $nbiaya;
										?>		
											<div class="mk row" r_id="<?php echo $key->mkditawarkan_id ?>" style="padding:5px;">
												<div class="col-md-6"><?php echo $key->kode_mk. ' - '. $key->nama_mk;?> &nbsp;<a href="#" onclick="del_data_sp('<?php echo $key->mkditawarkan_id;?>','<?php echo $key->kode_mk. ' - '. $key->nama_mk;?>','<?php echo $key->sks;?>','<?php echo $nbiaya;?>','<?php echo $key->krs_id; ?>')"><i class="fa fa-trash-o"></i></a>
												<input type="hidden" name="sks[]" value="<?php echo $key->sks;?>"><input type="hidden" name="mk[]" value="<?php echo $key->kode_mk. ' - '. $key->nama_mk;?>"><input type="hidden" name="mkid[]" value="<?php echo $key->mkditawarkan_id;?>" class="mkidtmp"><input type="hidden" name="biaya[]" value="<?php echo $key->inf_biaya;?>'">
												</div>
												<div class="col-md-2"><?php echo $key->sks;?> sks</div>
												<div class="col-md-3">Rp. <?php echo number_format($nbiaya);?> <span class="text text-danger label-bintang" r_id="<?php echo $key->krs_id; ?>"> <?php 
												if($key->is_approve==1) echo "<small>* ok</small>"; 
												if($key->is_approve==2) echo "<small>dibatalkan</small>";
												?></div>
												<div class="col-md-1">
												
													<?php 
													switch ($key->is_approve){
														case "0":
														?>
															<a href="#" onclick="toggle_sp('<?php echo $key->krs_id; ?>','1')"><span class="label label-danger label-status" r_id="<?php echo $key->krs_id; ?>">Setujui</span>
														<?php
														break;
														case "1":
														?>
															<a href="#" onclick="toggle_sp('<?php echo $key->krs_id; ?>','2')"><span class="label label-danger label-status" r_id="<?php echo $key->krs_id; ?>">Batalkan</span>
														<?php
														break;
														case "2":
														?>
															<a href="#" onclick="toggle_sp('<?php echo $key->krs_id; ?>','1')"><span class="label label-danger label-status" r_id="<?php echo $key->krs_id; ?>">Setujui</span>
														<?php
														break;
													}
													?>
												</a>&nbsp;
												
												
												</div>
											</div>
										<?php 
										endforeach; ?>
									</td>
								</tr>
								<tr><td>
									<div class="col-md-6"><div class="pull-right"><b>Total SKS</b></div></div>
									<div class="col-md-2"><span id="sks"><b><?php echo $sks;?> sks</b></span></div>
									<div class="col-md-3"><span id="biaya"><b>Rp. <?php echo number_format($total);?></b></span></div>
									<div class="col-md-1">&nbsp;</div>
									</td>
								</tr>
							</tbody>
						</table>

						<input type="hidden" id="no" value="0">
						<input type="hidden" id="tmpb" value="<?php echo $biaya->biaya_sks; ?>">
						<input type="hidden" id="tsks" value="<?php echo $sks;?>">
						<input type="hidden" id="tbiaya" value="<?php echo $total;?>">	
						<input type="hidden" id="totalsks" value="20">						
					</div>
					<?php
				else:
				?>	
					
				
					<div class="form-group table-mk hide-from-print">
						<input type="hidden" id="no" value="0">
						<input type="hidden" id="tmpb" value="<?php echo $biaya->biaya_sks; ?>">
						<input type="hidden" id="tsks" value=0>
						<input type="hidden" id="tbiaya" value=0>						
						<table class="table">
							<thead>
								<tr>
									<th>
										<div class="col-md-6">MK</div>
										<div class="col-md-2">SKS</div>
										<div class="col-md-3">Biaya</div>
										<div class="col-md-1">&nbsp;</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr><td id="wrap-mk">&nbsp;</td></tr>
								<tr><td><div class="col-md-6"><div class="pull-right"><b>Total SKS</b></div></div>
										<div class="col-md-2"><span id="sks"><b>0 sks</b></span></div>
										<div class="col-md-3"><span id="biaya"><b>Rp. 0</b></span></div>
										<div class="col-md-1">&nbsp;</div></td></tr>
							</tbody>
						</table>				       
					</div>
				<?php endif; ?>
			</div>
		
	</form>
	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->location('assets/js/select2/select2.css')?>" />
	<script src="<?php echo $this->location('assets/js/select2/select2.js')?>"></script>
	<script src="<?php echo $this->location('assets/js/info/accounting.js')?>"></script>
	<script src="<?php echo $this->location('assets/js/info/jskrs.js')?>"></script>
	
<?php $this->foot();?>
