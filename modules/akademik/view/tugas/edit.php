<?php
if($user!="mahasiswa" && $user!="dosen"){
$this->head();

if($posts!=""){
	$header		= "Edit Tugas";
	foreach ($posts as $dt):
		$id 		= $dt->tugasid;
		$jadwalid 	= $dt->jadwalid;
		$materiid 	= $dt->materiid;
		$judul	 	= $dt->judul;
		$instruksi 	= $dt->instruksi;
		$keterangan = $dt->keterangan;
		$tgl_mulai 	= $dt->tgl_mulai;
		$tgl_selesai= $dt->tgl_selesai;
	endforeach;
	$frmact 	= $this->location('module/akademik/tugas/save');		
	
}else{
	$header		= "Write New Tugas";
	$id			="";
	$materiid	="";
	$jadwalid	="";
	$judul		="";
	$instruksi	="";
	$keterangan	="";
	$tgl_mulai	="";
	$tgl_selesai="";
	$newtugas	= 1;
	$frmact 	= $this->location('module/akademik/tugas/save');		
}


?> 
	     
    <div class="row">    
        <div class="col-md-12">	
        <h2 class="title-page"><?php echo $header; ?></h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/tugas'); ?>">Tugas</a></li>
		  <li class="active"><?php echo str_replace('Write ','',str_replace('Tugas','',$header));?></li>
		</ol>
        <div class="breadcrumb-more-action">
		<?php if($posts!=""){	?>
			<a href="<?php echo $this->location('module/akademik/tugas/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Tugas</a>
		<?php } ?>
        </div>
		
   	 	
   	 	
			<form method=post id="upload-tugas-form" class="form-horizontal">
				 
				 <div class="form-group">
					<label class="col-sm-2 control-label">Jadwal</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php $uri_parent = $this->location('module/akademik/tugas/tampilkan_materi'); ?>
							<select class="form-control e9" name="jadwal" id="select_jadwal"  data-uri="<?php echo $uri_parent ?>">
								<option value='0'>Select Jadwal</option>
							<?php 
								foreach($jadwal as $dt):
									echo "<option class='sub_".$dt->jadwal_id."' value='".$dt->jadwal_id."'";
									if($jadwalid == $dt->jadwal_id){
										echo "selected";
									}
									echo ">".$dt->jadwal."</option>";
								endforeach;
							?>					
							</select>
						</div>
					</div>
				</div>
				 
				 <div class="form-group">
					<label class="col-sm-2 control-label">Materi</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php echo '<select id="select_materi" class="form-control e9" disabled="disabled" name="materi" >'; ?>
								<option value='0'>Select Materi</option>
							<?php 
								foreach($materi as $dt):
									echo "<option class='sub_".$dt->materi_id."' value='".$dt->materi_id."' ";
									if($materiid==$dt->materi_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								endforeach;
							?>					
						</select>
						<?php if($materiid!="") {?>
							<input type="hidden" class="form-control" id="materi" name='materi' value="<?php if(isset($materiid)) echo $materiid; ?>">
						<?php } ?>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Judul</label>
					<div class="controls">
						<div class="col-sm-10">
							<input type="text" class="form-control" id="judul" name='judul' value="<?php if(isset($judul)) echo $judul; ?>">
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Instruksi</label>
					<div class="controls">
						<div class="col-sm-10">
							<textarea class="form-control ckeditor" id="instruksi" name='instruksi' ><?php if(isset($instruksi)) echo $instruksi; ?></textarea>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
							<textarea class="form-control ckeditor" id="keterangan" name='keterangan' ><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
						</div>
					</div>
				</div>
				
				<div class="form-group">	
					 <label class="col-sm-2 control-label">Tanggal Pengerjaan</label>
					 <div class="col-sm-10">
						<input type="text" id="tanggalmulai" name="tanggalmulai" class="form_datetime form-control" value="<?php if(isset($tgl_mulai))echo $tgl_mulai;?>"> 
						sampai 
						<input type="text" id="tanggalselesai" name="tanggalselesai" onchange='validate_tanggal()' class="form_datetime form-control" value="<?php if(isset($tgl_selesai))echo $tgl_selesai;?>">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="hidden" name="hidId" value="<?php if(isset($id)) echo $id;?>">
						<input type="hidden" name="newtugas" value="<?php if(isset($newtugas)) echo $newtugas;?>">
						<input type="submit" name="b_jenis" onclick="submit_tugas()" value="Submit" class="btn btn-primary">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
<?php
$this->foot();
}
?>