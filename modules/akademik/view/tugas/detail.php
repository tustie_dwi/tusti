<?php $this->head(); 
$now = new DateTime();
$time = $now->format('Y-m-d H:i:s');
foreach ($posts as $p) {
?>
 <h2 class="title-page">Detail Tugas</h2>
 <ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/tugas'); ?>">Tugas</a></li>
		  <li class="active">Detail</li>
		</ol>
	<div class="breadcrumb-more-action">
		<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
    	<a href="<?php echo $this->location('module/akademik/tugas/write'); ?>" class="btn btn-primary">
    		<i class="fa fa-pencil"></i> New Tugas</a>
    	<?php } ?>
    </div>
            
		<table class='table table-bordered' id='example'>
			<tr>
				<td ><em>Judul</em></td>
				<td ><?php echo $p->judul;
						   if(($p->tgl_selesai) > $time ){
								echo "<br><small><span class='label label-success'>Open</span></small><br>";
						   }
						   else {
								echo "<br><small><span class='label label-warning'>Closed</span></small><br>";
						   }
					 ?></td>
			</tr>
				
		  	<tr>
				<td><em>Mata Kuliah</em></td>
				<td><?php echo $p->namamk?></td>
			</tr>
			
			<tr>
				<td><em>Materi</em></td>
				<td><?php echo $p->materi?></td>
			</tr>
			
			<tr>
				<td><em>Instruksi</em></td>
				<td><?php echo $p->instruksi?></td>
			</tr>
			
			<tr>
				<td><em>Keterangan</em></td>
				<td><?php echo $p->keterangan?></td>
			</tr>
			
			<tr>
				<td><em>Tanggal Pengerjaan</em></td>
				<td><?php echo "Start ".$p->tgl_mulai."<br>End ".$p->tgl_selesai ?></td>
			</tr>
				
			<tr>
				<td><em>Last Modified</em></td>
				<td><?php echo $p->last_update?>&nbsp; <br>Upload By <span class='label label-info'><?php echo $p->user?></span></td>
			</tr>
				
		</table>
		<?php if($user!="mahasiswa" && $user!="dosen"){ ?>		 
		<div class='well'>
			<a href=<?php echo $this->location('module/akademik/tugas/edit/'.$p->tugasid) ?> class='btn btn-info'>
			<i class='fa fa-edit'></i> Edit Tugas</a>
		</div>
		
		<?php } ?>
<?php 
} 

$this->foot(); ?>