<?php $this->head(); 
$now = new DateTime();
$time = $now->format('Y-m-d H:i:s');
//echo "<em class='pull-right'>".$time."</em><br>";
?>
<h2 class="title-page">Tugas</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active">Tugas</li>
	</ol>

	<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
     <div class="breadcrumb-more-action">
    	<a href="<?php echo $this->location('module/akademik/tugas/write'); ?>" class="btn btn-primary">
    		<i class="fa fa-pencil"></i> New Tugas</a>
            </div>
    <?php } ?>
    

	
	<?php if(isset($status) and $status) : ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $statusmsg; ?>
	</div>
	<?php endif; ?>
		
	<?php
			
	 if( isset($posts) ) :	?>
		<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>No</th>
						<th>Judul Tugas</th>
						<th>Mata Kuliah</th>
						<th>Materi</th>
						<th>Last Modified</th>
						<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
						<th>Act</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
			
			<?php 
				$i = 1;
				if($posts > 0){
					foreach ($posts as $dt): 
			?>
				<tr valign=top>
					<td><?php echo $i ?></td>
					<td width="250px">
						<?php echo "<a class='btn-edit-post' href=".$this->location('module/akademik/tugas/detail/'.$dt->tugasid)."><span class='text text-default'><strong>".$dt->judul."</span></strong></a>" ?>
							<?php
							
							if(($dt->tgl_selesai) > $time ){
								echo "<small><span class='label label-success'>Open</span></small><br>";
							}
							else {
								echo "<small><span class='label label-warning'>Closed</span></small><br>";
							}
							
							echo "<em><small>Start ".$dt->tgl_mulai."<br>End ".$dt->tgl_selesai."</small></em>";
 							?>
					</td>
					<td width="200px"><?php echo $dt->namamk ?></td>
					<td width="200px"><?php if(isset($dt->materi)){
								echo $dt->materi;
							  }else echo "<em>Not Specified</em>";  
						?>
					</td>
					<td width="150px">
						<?php echo str_replace('-', '/',$dt->last_update) ?><br>
							<small> by :
							<em style='color : orange'><?php echo $dt->user ?></em><br>
							</small>
					</td>
					<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
					<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
										<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/tugas/edit/'.$dt->tugasid) ?>"><i class='fa fa-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td>
					<?php } ?>
				</tr>
		<?php 
		$i++;
		endforeach; 
		} ?>
		</tbody></table>
		
		
		
	 <?php else: ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>

<?php $this->foot(); ?>