<?php if($user!="mahasiswa" && $user != "dosen"){
		 $this->head();	  
		 $header="Pembimbing Akademik";
	     if(isset($edit)){
	     	foreach($edit as $d){
				$fakultasid		= $d->fakultas_id;
			    $mhsid			= $d->mahasiswa_id;
			    $karyawan_id 	= $d->kar_id;
			    $tahun_akademik	= $d->tahun_akademik;
				$mhs			= $d->nama;
			}
			 $form	= "Edit Pembimbing";
		 }
		 else{
			 $fakultasid		= "";
			 $mhsid				= "";
			 $karyawan_id 		= "";
			 $tahun_akademik	= "";
			 $newjen			= 1;
			 $form	= "Write New Pembimbing";
		 }
?>

     <h2 class="title-page"><?php echo $header; ?></h2>
		

	<div class="row">
		<div class="col-md-8">	
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
		  <li class="active"><a href="#"><?php echo $header;?></a></li>
		</ol>
		 <?php
		 
		 if(isset($status) and $status) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $statusmsg; ?>
			</div>
		<?php 
		endif; 
		
		 if( isset($posts) ) :	?>
			<div class="block-box">
			<table class='table table-hover' id='example' data-id='module/akademik/pembimbing/'>
					<thead>
						<tr>						
							<th>&nbsp;</th>						
						</tr>
					</thead>
					<tbody>
			<?php
				$i = 1;
				if($posts > 0){
					foreach ($posts as $dt): 
					?>
						<tr id="post-<?php echo $dt->mhs_id; ?>" data-id="<?php echo $dt->mhs_id; ?>" valign=top>
							
							<td>
								<div class="col-md-10">
								<?php 
								echo "<b>".$dt->nim."</b><br>"; 
								echo "<small>".ucWords(strToLower($dt->nama))."</small>";
								echo "&nbsp;<code><i class='fa fa-user'></i> $dt->dosen</code>";
								?>
								
									<span class='label label-success'><?php echo ucWords($dt->tahun) ?></span>
								</div>
								<?php
								if($user!="Mahasiswa" && $user!="Dosen"){ 
								$uri_location = $this->location('module/akademik/pembimbing/delete');
								?>
								<div class="col-md-2">
									<ul class='nav nav-pills' style='margin:0;'>
										<li class='dropdown'>
										  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
										  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
											<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/pembimbing/index/'.$dt->mhs_id)?>"><i class='fa fa-edit'></i> Edit</a>
											</li>
											<li>
											<a class='btn-edit-post' href="#" onclick="doDelete(<?php echo $dt->mahasiswa_id ?>)"><i class='fa fa-trash-o'></i> Hapus</a>	
											<input type="hidden" value="<?php echo $dt->mahasiswa_id ?>" class='deleted<?php echo $dt->mahasiswa_id ?>' uri = "<?php echo $uri_location ?>"/>
											</li>	
										  </ul>
										</li>
									</ul>
								</div>
								<?php } ?>
							</td>
						</tr>
						<?php
					 endforeach; 
				 } ?>
					</tbody>
			</table>
            </div>
		<?php
		 else: 
		 ?>
		<div class="span3" align="center" style="margin-top:20px;">
			<div class="well">Sorry, no content to show</div>
		</div>
		<?php endif; ?>
    </div>
    <div class="col-md-4">	
		<div class="panel panel-default">
		  <div class="panel-heading"><i class="fa fa-pencil-square"></i> <?php echo $form ?></div>
		  <input type="hidden" id="head-form" value="<?php echo $form ?>" />
		  <div class="panel-body">
			
			<form method=post id="upload-form-pembimbing" role="form">
					<div class="form-group">	
						<label>Fakultas</label>
						
								<?php $uri_parent = $this->location('module/akademik/pembimbing/tampilkan_dosen'); ?>
								<?php echo '<select class="form-control e9" name="fakultas" id="select_fakultas"  data-uri="'.$uri_parent.'">' ?>
									<option class="sub_01" value="0">Select Fakultas</option>			
									<?php							
										foreach($get_fakultas as $dt):
											echo "<option class='sub_".$dt->fakultasid."' value='".$dt->fakultasid."' ";
											if($fakultasid==$dt->fakultasid){
												echo "selected";
											}
											echo ">".$dt->keterangan."</option>";
										endforeach;
									?>
								</select>
							
					</div>
										
					<div class="form-group">	
						<label>Pembimbing</label>						
						<?php $uri_parent = $this->location('module/akademik/pembimbing/tampilkan_mhs'); ?>
						<?php echo '<select class="form-control e9" name="dosen" id="select_dosen" disabled data-uri="'.$uri_parent.'">' ?>
							<option class="sub_01" value="0">Select Dosen Pembimbing</option>			
							<?php
																
								foreach($get_dosen as $dt):
									echo "<option class='sub_".$dt->kar_id."' value='".$dt->kar_id."' ";
									if($karyawan_id==$dt->kar_id){
										echo "selected";
									}
									echo ">".$dt->nama."</option>";
								endforeach;
							?>
						</select>						
					</div>
					<div class="form-group">	
						<label>Tahun Akademik</label>						
						<select class="form-control e9" name="select_thn" id="select_thn" disabled>
							<option value="0">Select Tahun Akademik</option>
							<?php foreach($get_thn_akademik as $dt):
									echo "<option class='sub_".$dt->thn_id."' value='".$dt->hid_id."' ";
									if($tahun_akademik==$dt->hid_id){
										echo "selected";
									}
									echo ">".ucwords($dt->tahun)."</option>";
								endforeach;
							?>
						</select>							
					</div>
					<div class="form-group">	
						<label>Mahasiswa</label>
						
								<?php if(isset($mhsid)&&isset($mhs)){?>
									<?php echo "<input type='text' class='form-control' value='".$mhs."' disabled>";?>
									<?php echo "<input type='hidden' name='mhs_edit' value='".$mhsid."'>";?>
								<?php } else{ ?>
								<input type="text" class='form-control typeahead' name="mhs" id="select_mhs" data-role="tagsinput"/>
								<?php } ?>
							
					</div>
					
					 <div class="form-group">
						<label></label>					
							<input type="hidden" name="newjen" value="<?php if(isset($newjen)) echo $newjen;?>">
							<input type="submit" name="b_pemb" onclick="submit_pembimbing()"  value=" Submit " class="btn btn-primary">
							<a href="<?php echo $this->location("module/akademik/pembimbing"); ?>" class="btn btn-default"> <i class="fa fa-ban"></i > Cancel</a>
							
					</div>				
				</form>
				
		  </div>
		</div>    
    </div>
 </div> <!-- row -->
<?php
$this->foot();
}
?>