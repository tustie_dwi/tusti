<?php
if($user!="mahasiswa" && $user!="dosen"){
$this->head();

if($posts!=""){
	$header		= "Edit Pembimbing Akademik";
	
	foreach ($posts as $dt):
	$fakultasid		= $dt->fakultas_id;
	$mhsid			= $dt->mahasiswa_id;
	$karyawan_id 	= $dt->karyawan_id;
	$tahun_akademik	= $dt->tahun_akademik;
	$mhs			= $dt->nama;
	endforeach;
	$frmact 	= $this->location('module/akademik/pembimbing/save');		
	
}else{
	$header			= "Write New Pembimbing Akademik";
	$fakultasid		= "";
	$mhsid			= "";
	$karyawan_id 	= "";
	$tahun_akademik	= "";
	$newjen			= 1;
	$frmact 		= $this->location('module/akademik/pembimbing/save');		
}


?>

<div>  
	
    <div class="row">    
    <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/pembimbing'); ?>">Pembimbing Akademik</a></li>
	  <li class="active"><?php echo str_replace('Write ','',str_replace('Jenis File','',$header));?></li>
	</ol>
    <div class="breadcrumb-more-action">
		<?php if($posts!=""){	?>
			<a href="<?php echo $this->location('module/akademik/pembimbing/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Pembimbing Akademik</a>
		<?php } ?>
    </div>
    
        <div class="col-md-12">
		
			<form method=post id="upload-form-pembimbing" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Fakultas</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php $uri_parent = $this->location('module/akademik/pembimbing/tampilkan_dosen'); ?>
							<?php echo '<select class="form-control e9" name="fakultas" id="select_fakultas"  data-uri="'.$uri_parent.'">' ?>
								<option class="sub_01" value="0">Select Fakultas</option>			
								<?php							
									foreach($get_fakultas as $dt):
										echo "<option class='sub_".$dt->fakultasid."' value='".$dt->fakultasid."' ";
										if($fakultasid==$dt->fakultasid){
											echo "selected";
										}
										echo ">".$dt->keterangan."</option>";
									endforeach;
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Dosen Pembimbing</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php $uri_parent = $this->location('module/akademik/pembimbing/tampilkan_mhs'); ?>
							<?php echo '<select class="form-control e9" name="dosen" id="select_dosen" disabled data-uri="'.$uri_parent.'">' ?>
								<option class="sub_01" value="0">Select Dosen Pembimbing</option>			
								<?php
																	
									foreach($get_dosen as $dt):
										echo "<option class='sub_".$dt->kar_id."' value='".$dt->hid_id."' ";
										if($karyawan_id==$dt->hid_id){
											echo "selected";
										}
										echo ">".$dt->nama."</option>";
									endforeach;
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Mahasiswa</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php if(isset($mhsid)&&isset($mhs)){?>
								<?php echo "<input type='text' class='form-control' value='".$mhs."' disabled>";?>
								<?php echo "<input type='hidden' name='mhs_edit' value='".$mhsid."'>";?>
							<?php } else{ ?>
							<select multiple class="form-control e9" name="mhs" id="select_mhs" disabled>
							</select>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Tahun Akademik</label>
					<div class="controls">
						<div class="col-sm-10">
							<select class="form-control e9" name="select_thn" id="select_thn" disabled>
								<option value="0">Select Tahun Akademik</option>
								<?php foreach($get_thn_akademik as $dt):
										echo "<option class='sub_".$dt->thn_id."' value='".$dt->hid_id."' ";
										if($tahun_akademik==$dt->hid_id){
											echo "selected";
										}
										echo ">".ucwords($dt->tahun)."</option>";
									endforeach;
								?>
							</select>
						</div>
					</div>
				</div>
				 <div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="hidden" name="mhs_id" value="">
						<input type="hidden" name="newjen" value="<?php if(isset($newjen)) echo $newjen;?>">
						<input type="submit" name="b_pemb" onclick="submit_pembimbing()"  value="Submit" class="btn btn-primary">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
}
?>