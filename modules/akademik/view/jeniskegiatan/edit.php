<?php
$this->head();

if($posts !=""){
	$header		= "Edit Fakultas";
	
	foreach ($posts as $dt):
		$keterangan	= $dt->keterangan;
		$kode_kegiatan = $dt->kode_kegiatan;
		$kategori = $dt->kategori;
		$id			= $dt->hid_id;
	endforeach;
	$ceknew			= 0;
	$frmact 	= $this->location('module/akademik/jeniskegiatan/save');		
	
}else{
	$header			= "Write New Jenis Kegiatan";
	$id				= "";
	$keterangan		= "";
	$kode_kegiatan 	= "";
	$kategori	 	= "";
	$ceknew			= 1;
	$frmact 		= $this->location('module/akademik/jeniskegiatan/save');		
}


?>

<div class="container-fluid">  
	
	<legend>
		<a href="<?php echo $this->location('module/akademik/jeniskegiatan'); ?>" class="btn btn-info pull-right"><i class="icon-list"></i> Jenis Kegiatan List</a> 
		<?php if($id !=""){	?>
		<a href="<?php echo $this->location('module/akademik/jeniskegiatan/write'); ?>" class="btn pull-right" style="margin:0px 5px"><i class="icon-pencil"></i> Write New Jenis Kegiatan</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post  action="<?php echo $this->location('module/akademik/jeniskegiatan/save'); ?>" class="form-horizontal">
				<div class="form-group">	
					<label class="col-sm-2 control-label">Kode Jenis Kegiatan</label>
					<div class="col-sm-10">
						<input required="required" type=text  class="form-control" name="kode_kegiatan" value="<?php echo $kode_kegiatan; ?>">
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Nama Jenis Kegiatan</label>
					<div class="col-sm-10">
						<input required="required" type=text  class="form-control" name="keterangan" value="<?php echo $keterangan; ?>">
					</div>
				</div>	
				<div class="form-group">
					 <label for="kategori" class="col-sm-2 control-label">Kategori</label>
					 <div class="col-sm-10">
						<select name="kategori" class="form-control">
							<option value="-">Pilih Kategori</option>
							<option value="akademik" <?php if($kategori=="akademik"){
										echo "selected";
									} ?>>Akademik</option>
							<option value="non" <?php if($kategori=="non"){
										echo "selected";
									} ?>>Non Akademik</option>
						</select>
					</div>
				</div>									
				<div class="form-group">		
					<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="ceknew" value="<?php echo $ceknew;?>">	
						<input type="submit" name="b_jeniskegiatan" value="Submit" class="btn btn-primary">
					</div>
				</div>		
							
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
?>