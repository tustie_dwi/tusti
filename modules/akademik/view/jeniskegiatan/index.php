<?php $this->head(); ?>
<fieldset>
	<legend><a href="<?php echo $this->location('module/akademik/jeniskegiatan/write'); ?>" class="btn btn-primary pull-right">
    <i class="icon-pencil icon-white"></i> New Jenis Kegiatan</a> Jenis Kegiatan List
	</legend>
	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	 if( isset($posts) ) :	
		$str="<table class='table table-hover' id='example'>
				<thead>
					<tr>
						<th>No</th>
						<th>Kode Jenis Kegiatan</th>
						<th>Nama Jenis Kegiatan</th>
						<th>Act</th>
					</tr>
				</thead>
				<tbody>";
		
			$i = 1;
			if($posts > 0){
				foreach ($posts as $dt): 
					$str.=	"<tr id='post-".$dt->jenis_kegiatan_id."' data-id='".$dt->jenis_kegiatan_id."' valign=top>
								<td>".$i++."</td>
								<td>".$dt->kode_kegiatan."</td>
								<td>".$dt->keterangan."<span class='label label-success'>".$dt->kategori."</span></td>";
										
					$str.= "<td style='min-width: 80px;'>            
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
								  <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
								  <ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
									<li>
									<a class='btn-edit-post' href=".$this->location('module/akademik/jeniskegiatan/edit/'.$dt->jenis_kegiatan_id)."><i class='icon-edit'></i> Edit</a>	
									</li>
								  </ul>
								</li>
							</ul>
						</td></tr>";
				 endforeach; 
			 }
		$str.= "</tbody></table>";
		
		echo $str;
	
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
</fieldset>
<?php $this->foot(); ?>