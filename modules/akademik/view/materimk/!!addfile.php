<?php
if($user!="Mahasiswa"){ 
$this->head();

if($posts!=""){
	$header		= "Edit File Materi";
	
	foreach ($posts as $dt):
	$hid_id				= $dt->file_id;
	$jen_file_id		= $dt->jenis_file_id;
	$materiid			= $dt->materi_id;
	$judul				= $dt->judul;
	$keterangan			= $dt->keterangan;
	$file_name			= $dt->file_name;
	$file_type			= $dt->file_type;
	$file_loc			= $dt->file_loc;
	$file_size			= $dt->file_size;
	$file_content		= $dt->file_content;
	$tgl_upload			= $dt->tgl_upload;
	$upload_by			= $dt->upload_by;
	$ispublish			= $dt->is_publish;
	endforeach;
	$frmact 	= $this->location('module/akademik/file/save');		
	
}else{
	$header				= "Write New File Materi";
	$judul				= "";
	$hid_id				= "";
	$keterangan			= "";
	$materiid			= "";
	$ispublish			= 0;
	$new				= 1;
	$frmact 			= $this->location('module/akademik/file/save');		
}


?>

<div class="container-fluid">  
	
	<legend>
		<a href="<?php echo $this->location('module/akademik/file/'); ?>" class="btn btn-info pull-right"><i class="fa fa-list"></i> File Materi List</a> 
		<?php if($posts!=""){	?>
		<a href="<?php echo $this->location('module/akademik/file/write'); ?>" class="btn btn-primary pull-right" style="margin:0px 5px"><i class="fa fa-pencil"></i> Write New File Materi</a>
		<?php } ?>
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post id="upload-form" enctype="multipart/form-data" action="<?php echo $this->location('module/akademik/file/save'); ?>" class="form-horizontal">
				
				<div class="form-group">	
					<label class="col-sm-2 control-label">Pilih Materi MK</label>
					<div class="controls">
						<div class="col-sm-10">
						<select class="form-control" name="fakultas" id="select_fakultas">
							<option class="sub_01" value="0">Select Materi</option>			
							<?php								
								foreach($materi as $dt):
									echo "<option class='sub_".$dt->materi_id."' value='".$dt->hid_id."' ";
									if($materiid==$dt->hid_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								endforeach;
							?>
						</select>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Judul</label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="text" class="form-control" name='judulx' required="required" value="<?php if(isset($judul)) echo $judul; ?>">
						</div>
					</div>
				</div>
				 <div class="form-group">	
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
						<textarea class="form-control" name='keterangan' required="required" ><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Is Publish ?</label>
					<div class="controls">
						<div class="col-sm-10">
							<label class="checkbox"><input type="checkbox" name="ispublish" value="1" <?php if ($ispublish==1) { echo "checked"; } ?>>Ya</label>
						</div>
					</div>
				</div>
				<?php if ($new == 1) { ?>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Upload File</label>
					<div class="controls">
						<div class="col-sm-10">
							<input type="file" required="required" class="form-control" name="uploads[]" id="uploads" multiple>
						</div>
					</div>
				</div>
				<?php } ?>
				 <div class="form-group">
					<div class="controls">
						<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="jenis_file_id" value="<?php if(isset($jen_file_id)) echo $jen_file_id;?>">
						<input type="hidden" name="materi_id" value="<?php if(isset($materiid)) echo $materiid;?>">
						<input type="hidden" name="file_name" value="<?php if(isset($file_name)) echo $file_name;?>">
						<input type="hidden" name="file_type" value="<?php if(isset($file_type)) echo $file_type;?>">
						<input type="hidden" name="file_loc" value="<?php if(isset($file_loc)) echo $file_loc;?>">
						<input type="hidden" name="file_size" value="<?php if(isset($file_size)) echo $file_size;?>">
						<input type="hidden" name="file_content" value="<?php if(isset($file_content)) echo $file_content;?>">
						<input type="hidden" name="tgl_upload" value="<?php if(isset($tgl_upload)) echo $tgl_upload;?>">
						<input type="hidden" name="upload_by" value="<?php if(isset($upload_by)) echo $upload_by;?>">
						<input type="hidden" name="hidId" value="<?php if(isset($hid_id)) echo $hid_id;?>">
						<input type="hidden" name="new" value="<?php if(isset($new)) echo $new;?>">
						<input  type="submit" id="btn-submit" name="b_file" value="Submit" class="btn btn-primary">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
}
?>