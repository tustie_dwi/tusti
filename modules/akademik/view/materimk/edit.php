<?php
if($user!="mahasiswa" && $user!="dosen"){ 
$this->head();

if($posts!=""){
	$header		= "Edit Materi MK";
	
	foreach ($posts as $dt):
	$mkditawarkan_id	= $dt->idmk;
	$cabang_id			= $dt->id_cabang;
	$parent_id			= $dt->parent_id;
	$judul				= $dt->judul;
	$hid_id				= $dt->hidmat;
	$keterangan			= $dt->keterangan;
	$fakultasid			= $dt->fakultas_id;
	$campusonly			= $dt->campus;
	$materiid			= $dt->materi_id;
	$ispublish			= $dt->is_publish;
	$stat				= $dt->status;
	$urut				= $dt->urut;
	$icon				= $dt->icon;
	$tahun_id			= $dt->tahun_id;
	endforeach;
	$frmact 	= $this->location('module/akademik/materimk/savematerimk');		
	
}else{
	$header				= "Write New Materi MK";
	if(isset($mkid)){
		$jumlah=$jumlahsub+1;
		foreach($materi as $dt){
			$parent_id			= $dt->hid_id;
			$urut				= $dt->urut.".".$jumlah;
		}
		foreach($pos as $t){
			$mkditawarkan_id	= $t->idmk;
			//echo $mkditawarkan_id;
		}
	}
	else{
		if(isset($jumlahmatbymk)){
		$countmatbymk 		= $jumlahmatbymk+1;
		$urut				= $countmatbymk;
		}
		$mkditawarkan_id	= "";
		$parent_id			= "";
		
	} 
	//$parent_id			= "";
	$cabang_id			= "";
	$judul				= "";
	$campusonly			= "";
	
	if($fakultas_id != '-'){
		$fakultasid		= $fakultas_id;
	}
	else $fakultasid 	= "";
	
	$hid_id				= "";
	$keterangan			= "";
	//$materiid			= "";
	$stat				= "";
	$ispublish			= 0;
	$tahun_id			= "";
	//$urut				= "";
	$icon				= "";
	$new				= 1;
	$frmact 			= $this->location('module/akademik/materimk/savematerimk');		
}


?>

<div>  
	
    <div class="row">  
     <h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/materimk'); ?>">Materi Mata Kuliah</a></li>
	  <li class="active"><a href="#"><?php echo $header; ?></a></li>
	</ol>
    <div class="breadcrumb-more-action">
		<a href="<?php echo $this->location('module/akademik/materimk/'); ?>" class="btn btn-default"><i class="fa fa-bars"></i> Materi MK List</a> 
		<?php if($posts!=""){	?>
		<a href="<?php echo $this->location('module/akademik/materimk/write'); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Write New Materi MK</a>
		<?php } ?>
        </div>  
        <div class="">
		
			<form method=post id="upload-form-materi" enctype="multipart/form-data" class="form-horizontal block-box">
				 <?php if(!isset($mkid)){ ?>
				 <div class="form-group">	
					<label class="col-sm-2 control-label">Fakultas</label>
					<div class="controls">
						<div class="col-sm-10">
						
						<?php if($fakultas_id != '-'){
									echo '<select class="form-control e9" name="fakultas" id="select_fakultas" disabled >';
							  } else  echo '<select class="form-control e9" name="fakultas" id="select_fakultas">';
						?>
							<option class="sub_01" value="0">Select Fakultas</option>			
							<?php
																
								foreach($fakultas as $dt):
									echo "<option class='sub_".$dt->fakultasid."' value='".$dt->hid_id."' ";
									if($fakultasid==$dt->hid_id){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Cabang</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php $uri_parent = $this->location('module/akademik/materimk/tampilkan_thn'); ?>
							<?php echo '<select class="form-control e9" name="cabang" id="select_cabang" data-uri="'.$uri_parent.'">' ?>
								<option class="sub_01" value="0">Select Cabang</option>			
								<?php						
									foreach($cabang as $dt):
										echo "<option class='sub_".$dt->cabang_id."' value='".$dt->cabang_id."' ";
										if($cabang_id==$dt->cabang_id){
											echo "selected";
										}
										echo ">".$dt->keterangan."</option>";
									endforeach;
								?>
							</select>				
						</div>
					</div>
				 </div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Tahun Akademik</label>
					<div class="controls">
						<div class="col-sm-10">
						<?php $uri_parent = $this->location('module/akademik/materimk/tampilkan_mk'); ?>
							<?php echo '<select class="form-control e9" name="select_thn" id="select_thn" disabled data-uri="'.$uri_parent.'">' ?>
							<option value="0">Select Tahun Akademik</option>
							<?php foreach($thnedit as $dt):
									echo "<option class='sub_".$dt->mktahun_akademik."' value='".$dt->thn_id."' ";
									if($tahun_id==$dt->thn_id){
										echo "selected";
									}
									echo ">".$dt->tahun."</option>";
								endforeach;
							?>
						</select>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="form-group">	
					<label class="col-sm-2 control-label">MK Ditawarkan</label>
					<div class="controls" id="mk">
						<div class="col-sm-10">
						<?php $uri_parent = $this->location('module/akademik/materimk/tampilkan_submk');
						      $uri_jum = $this->location('module/akademik/materimk/tampilkan_urut'); ?>
						<?php echo '<select class="form-control e9" name="mkditawarkan" id="select_mk" disabled="disabled" data-uri="'.$uri_parent.'" data-jum="'.$uri_jum.'">' ?>
							<option class="sub_01" value="0">Select Mata Kuliah</option>			
							<?php
																
								foreach($mkditawarkan as $dt):
									echo "<option class='sub_".$dt->mkditawarkan_id."' value='".$dt->hid_id."' ";
									if($mkditawarkan_id==$dt->hid_id){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
	
						<?php if($mkditawarkan_id!="") { 
							echo "<input type='hidden' name='mkditawarkan' value='".$mkditawarkan_id."'>";
						}?>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Sub Materi dari</label>
					<div class="controls">
						<div class="col-sm-10">
						<?php  $uri_jum = $this->location('module/akademik/materimk/tampilkan_urut');
							  echo '<select name="submateri" id="select_submk" class="form-control e9" disabled="disabled" data-jum="'.$uri_jum.'">'; ?>
							<option class="sub_01" value="0">Select Sub Materi</option>			
							<?php
// 																
								foreach($materi as $dt):
									echo "<option class='sub_".$dt->materi_id."' value='".$dt->hid_id."' ";
									if($parent_id==$dt->hid_id){
										echo "selected";
									}
									echo ">".$dt->judul."</option>";
								endforeach;
							?>
						</select>
						<?php if($parent_id!="") { 
							echo "<input type='hidden' name='submateri' value='".$parent_id."'>";
						}?>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Status</label>
					<div class="controls">
						<div class="col-sm-10">
						<?php if(!isset($mkid)){
							 	echo '<select name="statmateri" id="select_statmk" class="form-control e9" required="required" disabled="disabled">'; 
							  }
							  else echo '<select name="statmateri" id="select_statmk" class="form-control e9" required="required">'; 
						?>
							<option class="sub_01" value="0">Select Status</option>			
							<?php
// 																
								foreach($statmateri as $dt):
									echo "<option class='sub_".$dt->status."' value='".$dt->status."' ";
									if($stat==$dt->status){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
						<?php if($stat!="") { 
							echo "<input type='hidden' name='statmateri' value='".$stat."'>";
						}?>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Judul</label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="text" class="form-control" name='judul' required="required" value="<?php if(isset($judul)) echo $judul; ?>">
						</div>
					</div>
				</div>
				 <div class="form-group">	
					<label class="col-sm-2 control-label">Keterangan</label>
					<div class="controls">
						<div class="col-sm-10">
						<textarea class="form-control ckeditor" name='keterangan'><?php if(isset($keterangan)) echo $keterangan; ?></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label"><!-- No Urut --></label>
					<div class="controls">
						<div class="col-sm-10">
						<input type="hidden" class="form-control" id="urut" name='urut' required="required" value="<?php if(isset($urut)) echo $urut; ?>">
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Campus Only ?</label>
					<div class="controls">
						<div class="col-sm-10">
							<label class="checkbox"><input type="checkbox" name="campusonly" value="1" <?php if ($campusonly==1) { echo "checked"; } ?>>Ya</label>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">Upload Icon</label>
					<div class="controls">
						<div class="col-sm-10">
							<?php 
							if(!isset($new)){
								if(isset($icon)){ ?>
									<div class='well'>
										<img style="width: 100px; height: auto;" src="<?php echo $this->asset($icon); ?>"/>
										<input type="hidden" name="icon" id="icon" value="<?php if(isset($icon)) echo $icon; ?>">
									</div>
								<?php } else { ?>
									<div class='well'>
										<p>Icon Belum Tersedia</p>
									</div>
								<?php } 
							}?>
							<input type="file" class="form-control" name="icon" id="icon">
						</div>
					</div>
				</div>
				 <div class="form-group">
					<div class="controls">
						<div class="col-sm-offset-2 col-sm-10">
						<input type="hidden" name="hidmatId" value="<?php if(isset($hid_id)) echo $hid_id;?>">
						<input type="hidden" name="new" value="<?php if(isset($new)) echo $new;?>">
						<input  type="submit" id="btn-submit-publish" onclick="submit_materi('publish')" name="b_savepublish" value="Save and Publish" class="btn btn-primary">
						<input  type="submit" id="btn-submit-draft" onclick="submit_materi('draft')" name="b_draft" value="Save as Draft" class="btn btn-warning">
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
</div>
<?php
$this->foot();
}
?>