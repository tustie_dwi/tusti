<?php $this->head(); 
$header="Detail Materi MK";
?>
<div class="row">
	<div class="col-md-12">
		<h2 class="title-page"><?php echo $header; ?></h2>
		<ol class="breadcrumb">
		  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
		  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
		  <li class="active"><a href="#"><?php echo $header;?></a></li>
		</ol>
		<div class="breadcrumb-more-action">
			<a href="<?php echo $this->location('module/akademik/materimk/'); ?>" class="btn btn-default"><i class="fa fa-list"></i> Materi MK List</a>
			<?php if($user!="mahasiswa" && $user!="dosen"){ ?>
	    	<a href="<?php echo $this->location('module/akademik/materimk/write'); ?>" class="btn btn-primary">
	    		<i class="fa fa-pencil"></i> New Materi MK</a>
	    	<?php } ?>
    	</div>
    		
	
	
		 <?php
	 
		 if(isset($status) and $status) : ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $statusmsg; ?>
			</div>
		<?php 
		endif; 
		
		 if( isset($posts) ) {
			 foreach($posts as $p ){
			 		if ($p->is_publish==1){
							$pub = "Published";
							$label = "label-success";
					}
					else{
							$pub = "";
							$label = "";
					}
						
					if ($p->campus==1){
							$camp = "Campus Only";
							$label1 = "label-warning";
					}
						else{
							$camp = "";
							$label1 = "";
					} ?>
						
					<table class='table table-bordered' id='example'>
						   <tr>
							   <td><em>Judul</em></td>
							   <td><?php echo $p->judul;
							   		if($p->english){?>
											&nbsp;(<?php echo $p->english?>)
							  <?php }?>
							   		<br><span class='label <?php echo$label?>'><?php echo $pub?></span>
							   		<br><span class='label label-info'>Status : <?php echo $p->status?></span>
					<?php			if($p->campus==1){ ?>
									<br><span class='label <?php echo $label1?>'><?php echo $camp?></span>
					<?php			} ?>
					<?php			if($p->parent){ ?>
										<br><code>Sub Materi dari <?php echo $p->parent?></code>
					<?php			}?>
							  </td>
						  </tr>
						  <tr>
							   <td><em>Keterangan</em></td>
							   <td><?php echo $p->keterangan?></td>
						  </tr>
					      <tr>
							   <td><em>MK Ditawarkan</em></td>
							   <td><?php echo $p->namamk?></td>
						  </tr>
						  <tr>
							   <td><em>Tahun Akademik</em></td>
							   <td><?php echo $p->tahun?></td>
						  </tr>
						  <tr>
							   <td><em>Fakultas</em></td>
							   <td><?php echo $p->fak?></td>
						  </tr>
						  <tr>
							   <td><em>Icon</em></td>
							   <td>
							   	<?php if(isset($p->icon)){?>
							   		<img src="<?php echo $this->asset($p->icon)?>">
							   	<?php }else {?>
							   		<small><em>Belum Tersedia</em></small>
							   	<?php } ?>
							   </td>
						  </tr>
					</table>	
					<?php
			}
		 }
		 else{ 
		 ?>
	    <div class="span3" align="center" style="margin-top:20px;">
		    <div class="well">Sorry, no content to show</div>
	    </div>
	    <?php } ?>
	</div>
</div>
<?php $this->foot(); ?>