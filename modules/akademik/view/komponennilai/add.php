<?php
$this->head();

if($posts !=""){
	$header		= "Edit Komponen Nilai";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$namamk			= $dt->namamk;
		$keterangan		= $dt->keterangan;
		$bobot			= $dt->bobot;
		$komponenid		= $dt->parent_id;
		$mkid			= $dt->hid_mkid;
	endforeach;
	
}else{
	$header		= "Tambah Komponen Nilai";
	$id			= "";
	foreach ($datanilai as $dt):
		$namamk			= $dt->namamk;
		$komponenid		= $dt->hid_id;
		$mkid			= $dt->hid_mkid;
	endforeach;
}


?>
	<legend>
		<a href="<?php echo $this->location('module/akademik/komponennilai'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Komponen Nilai List</a> 
		<?php echo $header; ?>
    </legend> 
    <div class="row">    
        <div class="col-md-12">
		
			<form method=post name="form" id="form" class="form-horizontal">

				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Nama Mata Kuliah</label>
					 <div class="col-sm-10">
						<input disabled required="required" type=text  class='col-md-9 form-control' value="<?php if(isset($namamk))echo $namamk; ?>">
					</div>
				</div>
				
					<div class="form-group">	
					<label class="col-sm-2 control-label">Komponen Dari</label>
					<div class="col-sm-10">
						<select name="parent" id="parent" disabled class='col-md-9 form-control'>
							<option class="sub_01" value="0">Please Select..</option>			
							<?php														
								foreach($komponen as $dt):
									echo "<option class='sub_".$dt->komponen_id."' value='".$dt->komponen_id."' ";
									if($komponenid==$dt->komponen_id){
										echo "selected";
									}
									echo ">".$dt->keterangan."</option>";
								endforeach;
							?>
						</select>
					</div>
				</div>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Bobot</label>
					 <div class="col-sm-10">
						<input required="required" type=text id="bobot" class='col-md-9 form-control' name="bobot" value="<?php if(isset($bobot))echo $bobot; ?>">
					</div>
				</div>
				
				<div class="form-group">	
				 	<label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
					 <div class="col-sm-10">
					 	<input required="required" type=text id="keterangan" class='col-md-9 form-control' name="keterangan" value="<?php if(isset($keterangan))echo $keterangan; ?>"><br>
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<input type="hidden" name="namamk" value="<?php echo $mkid;?>">	
						<input type="hidden" name="parent" value="<?php echo $komponenid;?>">			
						<br><br><input type="submit" name="b_komponen" id="submit-add" value="Submit" class="btn btn-primary">
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
$this->foot();
?>