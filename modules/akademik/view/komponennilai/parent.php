<ul>
	<?php
	if(isset($posts)){ 
		foreach ($posts as $mk) :
			if($mkditawarkan_id==$mk->mkditawarkan_id): ?>
				<li>
					<span class='span'><?php echo $mk->keterangan ?>&nbsp;
						<span class='label label-success'><?php echo $mk->bobot ?></span>
					</span>
					<a class='btn-edit-post btn-edit' href="<?php echo $this->location('module/akademik/komponennilai/edit/'.$mk->komponen_id) ?>"><i class='fa fa-edit'></i><span class='text'> Edit</span></a>
					<a class='btn-edit-post btn-add' href="<?php echo $this->location('module/akademik/komponennilai/add/'.$mk->komponen_id) ?>"><i class='fa fa-plus-circle'></i><span class='text'> Tambah Komponen</span></a>
					<?php echo $this->komponenchild($mk->hid_id, $cabang, $fakultas, $thnakademik, $nmkid); ?>
				</li>
			<?php 
			endif;
		endforeach; 
	} ?>
</ul>