<?php $this->head();

if(!isset($cabangid))$cabangid='';
if(!isset($fakultasid))$fakultasid='';
if(!isset($thnakademikid))$thnakademikid='';
if(!isset($nmkid))$nmkid='';

?>
<fieldset>
    <h2 class="title-page">Komponen Nilai</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/komponennilai'); ?>">Komponen Nilai</a></li>
	  <li><a href="#">Data</a></li>
	</ol>
	<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>" class="btn btn-default pull-right">
    <i class="fa fa-bars icon-white"></i> Mata Kuliah Ditawarkan List</a> 
	</div>
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php endif; ?>
	
		<form class="form-horizontal" action="<?php echo $this->location('module/akademik/komponennilai'); ?>" role="form" id="param" method="POST">
			<div class="form-group">	
				<label class="col-sm-2 control-label">Fakultas</label>
				<div class="col-md-10">					
					<?php echo '<select id="pilih_fakultas" class="form-control e9" name="fakultas" >'; ?>
					<option value="0" data-uri='1'>Select Fakultas</option>
					<?php if(count($get_fakultas)> 0) {
						foreach($get_fakultas as $dt) :
							echo "<option value='".$dt->fakultasid."' ";
							if(isset($fakultasid)){
								if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
									echo "selected";
								}
							}
							echo " >".ucWords($dt->keterangan)."</option>";
						endforeach;
					} ?>
					</select>
				</div>
			</div>
			
			<div class="form-group">	
				<label class="col-sm-2 control-label">Cabang</label>
				<div class="col-md-10">		
					<?php echo '<select id="pilih_cabang" class="form-control e9" name="cabang" >'; ?>
					<option value="0">Select Cabang</option>
					<?php if(count($cabang)> 0) {
						foreach($cabang as $c) :
							echo "<option value='".$c->cabang_id."' ";
							if(isset($cabangid)){
								if($cabangid==$c->cabang_id){
									echo "selected";
								}
							}
							echo " >".ucWords($c->keterangan)."</option>";
						endforeach;
					} ?>
					</select>
				</div>
			</div>
			
		<div id="parameter" >
			<div class="form-group">	
				<label class="col-sm-2 control-label">Tahun Akademik</label>
				<div class="col-md-10">			
					<?php echo '<select disabled id="select_thnakademik" class="form-control e9" name="thn_akademik" >'; ?>
					<option value="0">Select Tahun akademik</option>
					<?php if(count($thnakademik)> 0) {
						foreach($thnakademik as $th) :
							echo "<option value='".$th->tahun_akademik."' ";
							if(isset($thnakademikid)){
								if($thnakademikid==$th->tahun_akademik){
									echo "selected";
								}
							}
							echo " >".ucWords($th->thnakademik)."</option>";
						endforeach;
					} ?>
					</select>
				</div>
			</div>
			
			<div class="form-group">	
				<label class="col-sm-2 control-label">Mata Kuliah</label>
				<div class="col-md-10">			
					<?php echo '<select disabled id="select_mk" class="form-control e9" name="select_mk" >'; ?>
					<option value="0">Select Mata Kuliah</option>
					<?php if(count($namamk)> 0) {
						foreach($namamk as $nmk) :
							echo "<option value='".$nmk->mkid."' ";
							if(isset($nmkid)){
								if($nmkid==$nmk->mkid){
									echo "selected";
								}
							}
							echo " >".ucWords($nmk->namamk)."</option>";
						endforeach;
					} ?>
					</select>
				</div>
			</div>
		</form>
	</div>
	
	<div class="tree">
    	<ul>
	<?php if( isset($namamk) ) :
		 $i =0;
		 foreach ($namamk as $d) { ?>
		 	<li>
		 		<span class="span"><?php echo $d->namamk ?></span>
		 		<a class='btn-edit-post btn-add' href="<?php echo $this->location('module/akademik/komponennilai/addfrommk/'.$d->mkid) ?>"><i class='fa fa-plus-circle'></i><span class='text'> Tambah Komponen</span></a>
		 		<?php echo $this->komponenparent($d->mkditawarkan_id, $cabangid, $fakultasid, $thnakademikid, $nmkid); ?>
		 	</li>
		 <?php $i++;
		} ?>
	     </ul>
	</div>
	 
	<?php else: ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>
    
</fieldset>
<?php $this->foot(); ?>