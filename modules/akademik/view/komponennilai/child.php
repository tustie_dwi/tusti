<ul>
	<?php 
	if(isset($child)){ 
		foreach ($child as $ch) :
			if($komponenid==$ch->parent_id): ?>
				<li>
					<span class='span'><?php echo $ch->keterangan ?>
						<span class='label label-success'><?php echo $ch->bobot ?></span>
					</span>
					<a class='btn-edit-post btn-edit' href="<?php echo $this->location('module/akademik/komponennilai/edit/'.$ch->komponen_id) ?>"><i class='fa fa-edit'></i><span class='text'> Edit</span></a>
					<a class='btn-edit-post btn-add' href="<?php echo $this->location('module/akademik/komponennilai/add/'.$ch->komponen_id) ?>"><i class='fa fa-plus-circle'></i><span class='text'> Tambah Komponen</span></a>
					<?php echo $this->komponenchild($ch->hid_id, $cabang, $fakultas, $thnakademik, $nmkid); ?>
				</li>
			<?php
			endif;
		endforeach;
	} ?>
</ul>