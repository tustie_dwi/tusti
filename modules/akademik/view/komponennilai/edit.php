<?php
$this->head();

if($posts !=""){
	$header		= "Edit Komponen Nilai";
	
	foreach ($posts as $dt):
		$id				= $dt->hid_id;
		$namamk			= $dt->namamk;
		$keterangan		= $dt->keterangan;
		$bobot			= $dt->bobot;
		$mkid			= $dt->hid_mkid;
	endforeach;
	$edit = "1";
}else{
	$header		= "Tambah Komponen Nilai";
	$id			= "";
	foreach ($namamk as $mk):
		$mkid				= $mk->mkditawarkan_id;
		$namamk			= $mk->namamk;
	endforeach;
}


?>
	<h2 class="title-page"><?php echo $header; ?></h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>">Mkditawarkan</a></li>
	  <li><a href="#">Tambah Komponen nilai</a></li>
	</ol>
    <div class="breadcrumb-more-action">
	<?php if(!isset($edit)){ ?>
	<a href="<?php echo $this->location('module/akademik/komponennilai'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Komponen Nilai List</a>
	<?php } ?> 
	<?php if(isset($cek)) { ?>
	<a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>" class="btn btn-default pull-right" style="margin:0px 5px"><i class="fa fa-bars"></i> Mata Kuliah Ditawarkan List</a> 
	<?php } ?> 
	</div>
    <!--------------------------------------------------------------TAMPILKAN DATA----------------------------------------->
    <?php if(!isset($edit)){ ?>
    
	<?php if( isset($datakomponen) ) :
		 $i =0; ?>
	<div class="tree">
    	<ul>
    	 <?php 
		 foreach ($datakomponen as $d) { ?>
		 	<li>
		 		<span class="span"><?php echo $d->namamk ?></span>
		 		<a class='btn-edit-post btn-add' href="<?php echo $this->location('module/akademik/komponennilai/addfrommk/'.$d->mkid) ?>"><i class='fa fa-plus-circle'></i><span class='text'> Tambah Komponen</span></a>
		 		<?php echo $this->komponenparent($d->mkditawarkan_id, "", "", "", ""); ?>
		 	</li>
		 <?php $i++;
		} ?>
	    </ul>
	</div>
	
    <?php
	else:
		
	endif; ?>
    
    <!--------------------------------------------------------END TAMPILKAN DATA----------------------------------------->
    <?php } else { ?>
    <br>
    <!-- <legend>
    	<a href="<?php echo $this->location('module/akademik/komponennilai'); ?>" class="btn btn-default pull-right"><i class="fa fa-bars"></i> Komponen Nilai List</a> 
		<?php echo $header; ?>
    </legend>  -->
    <?php } ?>
    
    <div class="row">    
        <div class="col-md-12">
			<form method=post name="form" id="form" class="form-horizontal">
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Nama Mata Kuliah</label>
					 <div class="col-sm-10">
						<input id='namamk' required='required' type=text  class='col-md-9 form-control typeahead' disabled value='<?php if(isset($namamk))echo $namamk ?>'>
					</div>
				</div>
				
				<div class="form-group">	
					 <label for="namamk" class="col-sm-2 control-label">Bobot</label>
					 <div class="col-sm-10">
						<input required="required" type=text id='bobot' class='col-md-9 form-control' name="bobot" value="<?php if(isset($bobot))echo $bobot; ?>">
					</div>
				</div>
				
				<div class="form-group">	
				 	<label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
					 <div class="col-sm-10">
					 	<input required="required" type=text id='keterangan' class='col-md-9 form-control' name="keterangan" value="<?php if(isset($keterangan))echo $keterangan; ?>"><br>
						<input type="hidden" name="hidId" value="<?php echo $id;?>">
						<?php //if(isset($cek)){ ?>
						<input type="hidden" name="namamk" value="<?php if(isset($mkid))echo $mkid?>">
						<?php //} ?>					
						<br><br><input type="submit" name="b_komponen" id="submit" value="Submit" class="btn btn-primary">
					</div>
				</div>
			</form>
		</div>
	</div>
<?php
$this->foot();
?>