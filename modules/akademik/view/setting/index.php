<?php $this->head(); ?>
<h2 class="title-page">Konfigurasi Akademik</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/akademik/setting'); ?>">Konfigurasi</a></li>
	</ol>
    	<?php
		if(isset($tahun)){
			foreach ($tahun as $key) {
				$tahun = $key->tahun;
				$id = $key->hidId;
				$isganjil = $key->is_ganjil;
				$ispendek = $key->is_pendek;
			}
		}	
		else{
			$tahun = '';
			$id = '';
			$isganjil = '';
			$ispendek = '';
		}
	?>
	
	<div class="row">
		<div class="col-md-12">
			<span id="notif"></span>
			
			<form id="form-setting" method="post" action="<?php echo $this->location('module/akademik/setting/save'); ?>" class="form-horizontal">
            <div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading"><span class="fa fa-calendar-o"></span> Tahun Akademik</div>
						<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
									<label for="namamk typeahead" class="control-label">Tahun Akademik</label>
									<div class="control-group" >
										<input required='required' type='hidden' name ='tahunid' value ='<?php echo $id ?>' >	
										<input type="hidden" value="<?php echo $this->location('module/akademik/setting/save'); ?>" id="uri">
										<input required='required' class="form-control" type='text' name ='tahun' value ='<?php echo $tahun ?>' >	
									</div>
								</div>
								
								<div class="form-group">
									<label for="namamk typeahead" class="control-label">Semester</label>
									<div class="control-group" >
										<select required='required' class="form-control" type='text' name ='isganjil'>
											<option value="1">Pilih Semester</option>
											<option <?php if($isganjil == 'ganjil') echo "selected"; ?> value="ganjil">Ganjil</option>
											<option <?php if($isganjil != 'ganjil') echo "selected"; ?> value="genap">Genap</option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label for="namamk typeahead" class="control-label">Pendek ?</label>
									<div class="control-group" >
										<label class="checkbox">
											<input <?php if($ispendek == 'Pendek') echo "checked" ?> type='checkbox' name='ispendek' value="Pendek" id="ispendek" > Ya
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<button id="button-setting" class="btn btn-primary"> Save Konfigurasi</button>
								<a href="<?php echo $this->location('module/akademik/setting'); ?>" class="btn btn-default"> <i class="fa fa-ban"></i> Cancel </a>
							</div>
						</div>
					</div>
				</div>
			
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading"><span class="fa fa-check"></span> Kegiatan Akademik</div>
						<div class="panel-body">
							<?php $tglmulai = $tglselesai = $kalenderid = $isaktif = $jeniskegiatan = ''; ?>
				
							<table class="table">
								<tr>
									<th>Nama Kegiatan</th>
									<th>Tgl Mulai</th>
									<th style="text-align: center">Tgl Selesai</th>
								</tr>
								<?php if(isset($kegiatan)){ ?>
									<?php foreach($kegiatan as $key){ ?>
										<?php 
											$tglmulai = $tglselesai = $kalenderid = $isaktif = '';
											$jeniskegiatan = $key->jenis_kegiatan_id;
											
											if(($kalender)) :
												foreach($kalender as $dt){
													if($key->jenis_kegiatan_id == $dt->jenis_kegiatan_id){
														$tglmulai 	= $dt->tgl_mulai;
														$tglselesai = $dt->tgl_selesai;
														$kalenderid = $dt->kalender_id;
														$isaktif = $dt->is_aktif;
														$jeniskegiatan = $dt->jenis_kegiatan_id;
														break;
													}
												}
											endif;
											
										?>
										<input class="kalender" name="kalender[]" value="<?php echo $kalenderid ?>" type="hidden" />
										<input class="kegiatan" name="kegiatan[]" value="<?php echo $jeniskegiatan ?>" type="hidden" />
										<tr>
											<td><?php echo $key->keterangan ?></td>
											<td><input class="form-control date tglmulai" type='text' name ='tglmulai[]' value="<?php echo $tglmulai ?>" ></td>
											<td><input class="form-control date tglselesai" type='text' name ='tglselesai[]' value="<?php echo $tglselesai ?>"></td>
										</tr>
									<?php } ?>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
				</div>
			</form>	 	
			<!--<div class="col-md-12" >
				<div>
					<button id="button-setting" class="btn btn-primary">Simpan</button>
				</div>
			</div>-->
		</div>
	</div>
		
<?php $this->foot(); ?>