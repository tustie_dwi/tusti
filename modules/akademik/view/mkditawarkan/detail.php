<?php $this->head(); ?>
	<h2 class="title-page">Matakuliah Ditawarkan</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>">Matakuliah Ditawarkan</a></li>
	</ol>
	
	<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/akademik/mkditawarkan/write'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Mata Kuliah Ditawarkan</a> 
    </div>
    	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; 
	
	
	 if( isset($posts) ) :
		 if($posts > 0){
			foreach ($posts as $dt): ?>
				<div class="media">				
				  <img class="media-object  pull-left img-thumbnail" src="<?php if($dt->icon){ echo $this->asset($dt->icon); }else{ echo $this->asset('images/topic.jpg'); } ?>" width="200">
					<div class="media-body">
					  <h2><?php echo $dt->namamk ?> <span class="label label-default"><?php echo ucWords($dt->thnakademik) ?></span>&nbsp;<?php if($dt->is_online == '1'){ echo "<span class='label label-success'>Online</span>";}else{ echo '<span class="label label-danger">Course</span>'; } ?></h2>
					  <i class="fa fa-briefcase"></i> <?php echo $dt->kuota ?> orang &nbsp; <?php if($dt->is_blok==1){ echo "Blok MK $dt->parentmk"; } ?><br>
					  
					  <h3>Pengampu</h3>
					  <ul class='list-unstyled'>
						  <?php if(isset($pengampu)){
									foreach ($pengampu as $p) { ?>
										<li><?php echo $p->nama ?>
							  <?php if($p->is_koordinator=="1"){ ?>
										<small><span class='label label-danger'><em>Koordinator</em></span></small></li>
							  <?php } 
								 else { ?>
									  </li>
							  <?php }
									}
								}else { echo "<small>Belum terdapat Pengampu untuk matakuliah ini"; } 
							
						?>						
							</ul>
					</div>
			  </div>

			
	  <?php endforeach; 
			 } ?>
		
			<div class='well'>
				<a href="<?php echo $this->location('module/akademik/mkditawarkan/edit/'.$dt->mkditawarkan_id) ?>" class='btn btn-info'>
    			<i class='fa fa-edit'></i> Edit Mata Kuliah Ditawarkan</a>
    			<a href="<?php echo $this->location('module/akademik/jadwalmk/write/'.$dt->mkditawarkan_id) ?>" class='btn btn-info'>
    			<i class='fa fa-list-alt'></i> Tambah Jadwal Mata Kuliah Ditawarkan</a>
    			<a href="<?php echo $this->location('module/akademik/komponennilai/addfrommk/'.$dt->mkditawarkan_id) ?>" class='btn btn-info'>
    			<i class='fa fa-plus-circle'></i> Tambah Komponen Nilai</a>
			</div>
	<?php
	
	 else: 
	 ?>
    <div class="span3" align="center" style="margin-top:20px;">
	    <div class="well">Sorry, no content to show</div>
    </div>
    <?php endif; ?>

<?php $this->foot(); ?>