<?php $this->head(); ?>
	<h2 class="title-page">Matakuliah Ditawarkan</h2>
	<ol class="breadcrumb">
	  <li><a href="<?php echo $this->location('apps'); ?>">Home</a></li>
	  <li><a href="<?php echo $this->location('module/akademik/setting'); ?>">Akademik</a></li>
	  <li class="active"><a href="<?php echo $this->location('module/akademik/mkditawarkan'); ?>">Matakuliah Ditawarkan</a></li>
	</ol>
	
	<div class="breadcrumb-more-action">
	<a href="<?php echo $this->location('module/akademik/mkditawarkan/write'); ?>" class="btn btn-primary">
    <i class="fa fa-pencil icon-white"></i> New Mata Kuliah Ditawarkan</a> 
    </div>
    	
	 <?php
	 
	 if(isset($status) and $status) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $statusmsg; ?>
		</div>
	<?php 
	endif; ?>
	
<div class="row">
	<div class="col-md-12">
    <div class="row">
	<div class="col-md-4">
    <div id="parameter" class="block-box">
		<form action="<?php echo $this->location('module/akademik/mkditawarkan'); ?>" role="form" id="param" method="POST">
			<div class="form-group">	
				<label class="control-label">Fakultas</label>
								
					<?php echo '<select id="pilih_fakultas" class="form-control e9" name="fakultas" >'; ?>
					<option value="0" data-uri='1'>Select Fakultas</option>
					<?php if(count($get_fakultas)> 0) {
						foreach($get_fakultas as $dt) :
							echo "<option value='".$dt->fakultasid."' ";
							if(isset($fakultasid)){
								if($fakultasid==$dt->fakultasid||$fakultasid==$dt->hid_id){
									echo "selected";
								}
							}
							echo " >".ucWords($dt->keterangan)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
			<div class="form-group">	
				<label class="control-label">Cabang</label>					
					<?php echo '<select id="pilih_cabang" class="form-control e9" name="cabang" >'; ?>
					<option value="0" data-uri='1'>Select Cabang</option>
					<?php if(count($cabang)> 0) {
						foreach($cabang as $c) :
							echo "<option value='".$c->cabang_id."' ";
							if(isset($cabangid)){
								if($cabangid==$c->cabang_id){
									echo "selected";
								}
							}
							echo " >".ucWords($c->keterangan)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
			
			<div class="form-group">	
				<label class="control-label">Tahun Akademik</label>						
					<?php echo '<select id="select_thnakademik" class="form-control e9" name="thn_akademik" >'; ?>
					<option value="0" data-uri='1'>Select Tahun akademik</option>
					<?php if(count($thnakademik)> 0) {
						foreach($thnakademik as $th) :
							echo "<option value='".$th->tahun_akademik."' ";
							if(isset($thnakademikid)){
								if($thnakademikid==$th->tahun_akademik){
									echo "selected";
								}
							}
							echo " >".ucWords($th->thnakademik)."</option>";
						endforeach;
					} ?>
					</select>
			</div>
		</form>
        </div>
	</div>
	
	
	<div id="isi" class="col-md-8">
		<?php
			 if( isset($posts) ) :	?>
			
				<table class='table table-hover' id='example' data-id='module/akademik/mkditawarkan'>
					<thead>
						<tr>
							<th>&nbsp;</th>							
							
						</tr>
					</thead>
					<tbody>
					
			<?php	if($posts > 0){
						$i = 1;
						foreach ($posts as $dt): ?>
							<tr>							
							<td>
								<div class="col-md-3">
								<small><i class="fa fa-clock-o"></i> <?php echo str_replace('-', '/',$dt->YMD)."  ".$dt->waktu."<br>"; ?>
									 <span class="text text-danger"><?php echo ucWords($dt->user_id) ?></span></small>
							</div>
							<div class="col-md-6">
								<a href="<?php echo $this->location('module/akademik/mkditawarkan/detail/'.$dt->mkditawarkan_id) ?>"><span class="text text-default"><b><?php echo $dt->kodemk ?></b></span></a>
									<?php echo $dt->namamk ?>
								<span class='label label-default' >Kuota <?php echo $dt->kuota ?></span>&nbsp;
								<?php if($dt->is_blok == '1'){ ?>
								<span class='label label-success'>*<?php echo $dt->parentmk ?></span>
								<?php } ?>
								<code><?php echo ucwords($dt->thnakademik) ?> </code><br>
								<?php
								$mconf = new model_mkditawarkan();
								
								$pengampu = $mconf->get_dosen_pengampu($dt->mkditawarkan_id);
								if($pengampu){
									foreach($pengampu as $dk):
										echo "<span class='label label-normal label-inline-block'><i class='fa fa-user'></i> ".$dk->nama."</span>&nbsp;";
									endforeach;
								}
								?>
							</div>
						
						
							<div class="col-md-3">   
								<ul class='nav nav-pills'>
									<li class='dropdown pull-right'>
										 <a class='dropdown-toggle btn btn-table' id='drop4' role='button' data-toggle='dropdown' href='#'>Action <b class='caret'></b></a>
										 <ul id='menu1' class='dropdown-menu pull-right' role='menu' aria-labelledby='drop4'>
											<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/mkditawarkan/edit/'.$dt->mkditawarkan_id) ?>"><i class='fa fa-edit'></i> Edit</a>	
											</li>
											<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/jadwalmk/write/'.$dt->mkditawarkan_id) ?>"><i class='fa fa-list-alt'></i> Jadwal</a>	
											</li>
											<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/test/by/mk/'.$dt->mkditawarkan_id) ?>"><i class='fa fa-file-text-o'></i> Test by MK</a>	
											</li>
											<li>
											<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/komponennilai/addfrommk/'.$dt->mkditawarkan_id) ?>"><i class='fa fa-plus-circle'></i> Komponen Nilai</a>	
											</li>
										  </ul>
										</li>
									</ul>
								</div>
								</td></tr>
								<?php 
							 $i++; 
						 endforeach;
						
					 } ?>
				</tbody></table>
				
				
			 <?php	
			 else: 
			 ?>
			<div class="well">Sorry, no content to show</div>
			<?php endif; ?>
		</div>
	</div>
</div>
</div>
<?php $this->foot(); ?>