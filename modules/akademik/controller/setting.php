<?php
class akademik_setting extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mconf = new model_conf();		
		
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/jquery.tokeninput.js');
		
		$this->add_script('js/set.js');
		$this->add_script('js/jsFunction.js');
		
		$data['tahun'] = $mconf->readtahun();	
		foreach($data['tahun'] as $key){
			$tahunid = $key->hidId;
		}
		$data['kegiatan'] = $mconf->get_kegiatan();
		$data['kalender'] = $mconf->get_kalender($tahunid);
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
			case 'del';
				$data['status'] 	= 'Delete';
				$data['statusmsg']  = 'Ok, data telah dihapus.';
			break;
		}
		
		$this->view('setting/index.php', $data);
	}
	
	public function edit($id){
		$mconf = new model_conf();		
		$data['posts'] = $mconf->readtahun($id);	
		
		$this->view('tahunakademik/edit.php',$data);
	}
	
	public function delete($id){
		$mconf = new model_conf();
		$mconf->deletetahun($id);
		exit();
	}
	
	public function save(){
		$mconf = new model_conf();
		
		if(! isset($_POST['ispendek'])) {
			$ispendek1 = '00';
			$ispendek = '';
		}
		else {
			$ispendek1 = '01';
			$ispendek = 'Pendek';
		}
		
		if($_POST['isganjil'] == 'ganjil') $isganjil = '01';
		else $isganjil = '02';
				
		$id = $_POST['tahun'] . $isganjil . $ispendek1;
		
		$data = Array(
			'tahun_akademik' => $id,
			'tahun' => $_POST['tahun'],
			'is_ganjil' => $_POST['isganjil'],
			'is_pendek' => $ispendek,
			'is_aktif' => '1'
		);
		
		foreach ($data as $key => $value) {
			echo $key . ' => ' . $value . '<br>';
		}
		
		$mconf->replace_tahun($data); //ok
		$mconf->update_kalender();
		
		for($i=0; $i<count($_POST['kalender']); $i++){
			if($_POST['tglmulai'][$i] != '' AND $_POST['tglselesai'][$i] != ''){
				
				$id_kalender 	= $_POST['tahun'] . $isganjil . $ispendek1 . $i;				
				$data = Array(
					'kalender_id' => $id_kalender,
					'jenis_kegiatan_id' => $_POST['kegiatan'][$i],
					'tahun_akademik' => $id,
					'tgl_mulai' => $_POST['tglmulai'][$i],
					'tgl_selesai' => $_POST['tglselesai'][$i],
					'is_aktif' => '1'
				);
				$mconf->replace_kalender($data);
			}
		}
		$this->redirect('module/akademik/setting');
		exit();
	}
	
	public function write(){
		$this->view('tahunakademik/edit.php');
	}
	
}
?>