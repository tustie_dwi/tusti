<?php
class akademik_pengampu extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mpengampu 		= new model_pengampu();
		$user 			= $this->coms->authenticatedUser->role;
		
		$data['user']	= $user;		
		$data['posts'] 	= $mpengampu->read("");	
		$data['dosen']	= $mpengampu->get_dosen();
		$data['datamk']	= $mpengampu->get_matakuliah();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
			case 'notyet';
				$data['status'] 	= 'Not yet';
				$data['statusmsg']  = 'Maaf, data belum terdapat pada mkditawarkan.';
			break;
		}
		
		$this->view('pengampu/index.php', $data);
	}
	
	function writenamamk($id){
		$mpngm	= new model_pengampu();
		$mconf 	= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		$data['dosen']    		= $mpngm->get_pengampu($id);
		$data['datamk']			= $mpngm->get_mk_pengampu($id);
		$data['cabang']			= $mconf->get_cabangub();
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->coms->add_script('js/jquery/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/pengampu/pengampu.js');
		
		$this->view( 'pengampu/addnamamk.php', $data );
		}
	}
	
	//----------------------------------------------------------------------------------------
	
	function editpengampu($id){	
		$mpngm	= new model_pengampu();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts']=$mpngm->read($id);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->coms->add_script('js/jquery/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/pengampu/pengampu_edit.js');
		
		$this->view( 'pengampu/editpengampu.php', $data );
		}
	}
	
	//------------------------------SAVE EDIT PENGAMPU--------------------------------------------------------
	
	function savepengampu(){
			if(isset($_POST['b_pengampu'])){
				$this->saveToDBpengampu();
				exit();
			}else{
				$this->index();
				exit;
			}
	}
	
	function saveToDBpengampu(){
		ob_start();
		$mpngm	= new model_pengampu();

		if($_POST['hidId']!=""){
			$pengampu_id 	= $_POST['hidId'];
			$action 			= "update";
		}else{
			$pengampu_id	= $mpngm->get_reg_number();	
			$action 			= "insert";	
		}
		
		if(isset($_POST['iskoor'])!=""){
			$iskoor	 = $_POST['iskoor'];
			$cekkoor = $mpngm->cek_koor($_POST['namamk']);
		}else{
			$iskoor	= 0;
		}
		
		if($cekkoor){
			echo "exist";
			exit;
		}
				
		$namamk			= $_POST['namamk'];
		$karyawanid		= $_POST['karyawanid'];
		
		
		if(isset($pengampu_id, $namamk, $iskoor)){
			
			$datanya 	= Array(
								'pengampu_id'=>$pengampu_id, 
								'mkditawarkan_id'=>$namamk, 
								'karyawan_id'=>$karyawanid, 
								'is_koordinator'=>$iskoor
								);
			$mpngm->replace_pengampu($datanya);

			$this->redirect('module/akademik/pengampu/index/ok');
			exit();
		}else{
			$this->redirect('module/akademik/pengampu/index/nok');
			exit();
		 }
		
	}

	//----------------------------------------------------------------------------------------
	
	//------------------------------SAVE MK PENGAMPU--------------------------------------------------------
	
	function savemkpengampu(){
			if(isset($_POST['b_tambahmk'], $_POST['hidden-namamk'])){
				$this->saveToDBmkpengampu();
				exit();
			}else{
				$this->index();
				exit;
			}
	}
	
	function saveToDBmkpengampu(){
		$mpngm	= new model_pengampu();

		if($_POST['hidId']!=""){
			$pengampu_id 	= $_POST['hidId'];
			$action 		= "update";
		}else{
			$pengampu_id	= $mpngm->get_reg_number();	
			$action 		= "insert";	
		}
		
		if(isset($_POST['iskoor'])!=""){
			$iskoor	= $_POST['iskoor'];
		}else{
			$iskoor	= 0;
		}
		
		$cabang			= $_POST['cabang'];
		$namamkx		= $_POST['hidden-namamk'];
		$i=0;
		foreach ($namamkx as $a){
			$namamkp=explode(",",$a);
			foreach ($namamkp as $mk){
					$i++;
					$mkid[] = $mpngm->cek_mkditawarkan($mk, $cabang);
				}
		}
		
		$karyawanid		= $_POST['karyawanid'];
		
		if(isset($mkid, $iskoor)){	
			 for ($x=0; $x <$i ; $x++) {
					$pengampuid = $mpngm->get_reg_number($mkid[$x], $karyawanid); 
					
					$datanyaadd	= Array(
								'pengampu_id'=>$pengampuid, 
								'mkditawarkan_id'=>$mkid[$x], 
								'karyawan_id'=>$karyawanid, 
								'is_koordinator'=>$iskoor
								);
					$mpngm->replace_pengampu($datanyaadd);
				}

			// $this->redirect('module/akademik/pengampu/index/ok');
			// exit();
		}else{

			// $this->redirect('module/akademik/pengampu/index/nok');
			// exit();
		 }
		
	}

	//----------------------------------------------------------------------------------------
	
}
?>