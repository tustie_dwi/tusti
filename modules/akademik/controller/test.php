<?php
class akademik_test extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	//------TEST-----------------------------------------------------//
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mtest = new model_test();
		
		$user = $this->coms->authenticatedUser->role;
		
		$data['user']=$user;
		$data['posts'] = $mtest->read("");	
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
		}
		

		$this->view('test/test/index.php', $data);
	}	
	
	//----------------------------------------------------------------------------------------
	
	function by($cek=NULL, $id=NULL){
		$mtest = new model_test();
		$user = $this->coms->authenticatedUser->role;
		
		if($cek=='jadwal'){
			$this->jadwal($id);
		}
		elseif ($cek=='mk') {
			$this->mk($id);
		}
		elseif ($cek=='materi') {
			$this->materi($id);
		}
		
	}
	
	function jadwal($id=NULL){
		$mtest = new model_test();

		$user = $this->coms->authenticatedUser->role;
		
		$data['posts'] 		= "";
		$data['cek']		= "1";
		$data['head']		= "Write New Test From Jadwal";
		$data['datajadwal']	= $mtest->get_jadwal_data($id);
		$getmkditawarkanid	= $mtest->get_jadwal_data($id);
		foreach ($getmkditawarkanid as $c):
			$mkid			= $c->mkditawarkan_id;
		endforeach;
		
		$data['materi']		= $mtest->get_materi($mkid, "");
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js'); 
		$this->add_script('js/test/test.js');
		
		$this->view( 'test/test/edit.php', $data );

	}
	
	function mk($id=NULL){
		$mtest = new model_test();

		$user = $this->coms->authenticatedUser->role;
		
		$data['posts'] 		= "";
		$data['cek']		= "2";
		$data['head']		= "Write New Test From MK";
		$data['mkditawarkanid']		= $id;
		$data['namamk']		= $mtest->get_namamk($id, "1");
		$data['materi']		= $mtest->get_materi($id, "1");
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js'); 
		$this->add_script('js/jquery/timepicker.js');
		$this->add_script('js/test/test.js');
		
		$this->view( 'test/test/edit.php', $data );

	}

	function materi($id=NULL){
		$mtest = new model_test();

		$user = $this->coms->authenticatedUser->role;
		
		$data['posts'] 				= "";
		$data['cek']				= "3";
		$data['head']				= "Write New Test From Materi";
		$data['mkditawarkanid']		= $mtest->get_mkid_by_materi($id);
		$mkid						= $mtest->get_mkid_by_materi($id);
		$data['namamk']				= $mtest->get_namamk($mkid, "1");
		$data['materi']				= $mtest->get_materi($mkid, "1");
		$data['materiid']			= $id;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js'); 
		$this->add_script('js/jquery/timepicker.js');
		$this->add_script('js/test/test.js');
		
		$this->view( 'test/test/edit.php', $data );

	}
	
	//----------------------------------------------------------------------------------------
	
	public function edit($id=NULL){	
		$user = $this->coms->authenticatedUser->role;
		
		$mtest = new model_test();	
		
		$data['posts'] = $mtest->read($id);
		$mk			   = $mtest->read($id);
		foreach ($mk as $dt):
			$mkid		= $dt->mkditawarkan_id;
			$cekjadwal	= $dt->jadwal_id;
		endforeach;
		if($cekjadwal!="cd98f0"){//cd98f0 == null
			$data['cek']	= "1";
			$data['datajadwal']	= $mtest->get_jadwal_data($id);
		}
		$data['datajadwal']	= $mtest->get_jadwal_data($cekjadwal);
		$data['materi']		= $mtest->get_materi($mkid, "1");
		$data['namamk']		= $mtest->get_namamk($mkid, "1");
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js'); 
		// $this->add_script('js/jquery/jquery-ui-timepicker-addon.js');
		$this->add_script('js/jquery/timepicker.js');
		$this->add_script('js/test/test.js');
		
		$this->view( 'test/test/edit.php', $data );
	}
	
	//----------------------------------------------------------------------------------------
	public function detail($id){
		$mtest = new model_test();	
		$user = $this->coms->authenticatedUser->role;
		
		$data['user']=$user;
		$data['posts'] = $mtest->read($id);
		$data['soal'] = $mtest->get_soal($id);
		$data['jawab'] = $mtest->get_jawaban();
		$data['testid'] = $id;
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/test/test.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/datepicker/datetimepicker.css');
		$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js'); 
		
		$this->view( 'test/test/detail.php', $data );
	}

	function saveToDB(){
		$mtest = new model_test();
		
		$hid_id 			= $_POST['hidId'];
		$judul				= $_POST['judul'];
		$instruksi			= $_POST['instruksi'];
		$keterangan			= $_POST['keterangan'];
		$tanggalmulai		= $_POST['tanggalmulai'];
		$tanggalselesai		= $_POST['tanggalselesai'];
		$jadwalid			= $mtest->get_jadwal_id($_POST['hidjadwalid']);
		$mkditawarkanid		= $mtest->get_mkid($_POST['hidmkid']);
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		if($_POST['materi']=='0'){
			$materiid	= "";
		}else{
			$materiid	= $mtest->get_materi_id($_POST['materi']);
		}
		
		if(isset($_POST['israndom'])&&$_POST['israndom']!=""){
			$israndom	= 1;
		}else{
			$israndom	= 0;
		}
		
		if(isset($_POST['b_savepublish'])){
			$ispublish = 1;
		}
		elseif(isset($_POST['b_draft'])){
			$ispublish = 0;
		}
		
		// if(isset($_POST['ispublish'])&&$_POST['ispublish']!=""){
			// $ispublish	= 1;
		// }else{
			// $ispublish	= 0;
		// }
		
		if($hid_id!=""){
			$testid 	= $hid_id;
			$action 	= "update";
		}else{
			$testid		= $mtest->get_reg_number();	
			$action 	= "insert";	
		}
		
		
		if(isset($judul, $instruksi, $keterangan, $tanggalmulai, $tanggalselesai)){
			$datanya 	= Array(
								'test_id'=>$testid,
								'jadwal_id'=>$jadwalid, 
								'materi_id'=>$materiid, 
								'mkditawarkan_id'=>$mkditawarkanid,
								'judul'=>$judul,
								'tgl_mulai'=>$tanggalmulai,
								'tgl_selesai'=>$tanggalselesai,    
								'instruksi'=>$instruksi, 
								'keterangan'=>$keterangan, 
								'is_random'=>$israndom,
								'is_publish'=>$ispublish,
								'user_id'=>$userid, 
								'last_update'=>$lastupdate
								);
			$mtest->replace_test($datanya);
			
			$hid_testid = $mtest->get_mdtestid($testid);
			echo $hid_testid;
			//$this->redirect('module/akademik/test/test/index/ok');
			//exit();
		}else{
			//$this->redirect('module/akademik/test/test/index/nok');
			//exit();
		}
		
	}
	//------TEST-----------------------------------------------------//
	
	
	//------KATEGORI-----------------------------------------------------//
	
	function kategori($menu=NULL,$id=NULL){
		$user = $this->coms->authenticatedUser->role;
		if($user!='Dosen' && $user!='Mahasiswa'){
			if($menu=='view' || $menu==''){
				$this->view_kategori();
			}
			elseif($menu=='write'){
				$this->writekategori();
			}
			elseif($menu=='edit'){
				$this->editkategori($id);
			}
			elseif($menu=='save'){
				$this->savekategori();
			}
		}
	}
	
	function view_kategori($by = 'all'){
		$mtestkat = new model_test();
		$user = $this->coms->authenticatedUser->role;
		$data['user'] = $user;
		
		if($user != 'Dosen' && $user != 'Mahasiswa'){
			$data['posts'] = $mtestkat->readkategori('');
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
			// $this->add_script('js/komponen.js');
			switch($by){
				case 'ok';
					$data['status'] 	= 'OK';
					$data['statusmsg']  = 'OK, data telah diupdate.';
				break;
				case 'duplicate';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data dengan nama kategori tersebut sudah ada.';
				break;
				case 'nok';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
				break;
			}
			$this->view('test/kategori/indexkategori.php', $data );
		}
	}
	
	function writekategori(){
		$mtestkat = new model_test();
		$user = $this->coms->authenticatedUser->role;
		$data['user'] = $user;
		
		if($user != 'Dosen' && $user != 'Mahasiswa'){
			$data['posts'] = "";
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('ckeditor/ckeditor.js');
	
			$this->add_script('js/test/testkategori.js');
	
			$this->view( 'test/kategori/editkategori.php', $data );
		}
	}
	
	function editkategori($id){
		if( !$id ) {
			$this->redirect('module/akademik/test/');
			exit;
		}
		
		$mtestkat = new model_test();
		$user = $this->coms->authenticatedUser->role;
		$data['user'] = $user;
		
		if($user != 'Dosen' && $user != 'Mahasiswa'){
			$data['posts'] = $mtestkat->readkategori($id);
		
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->add_script('js/test/testkategori.js');
			//$this->add_script('js/silabus.js');
			
			$this->view( 'test/kategori/editkategori.php', $data );
		}
	}
	
	function savekategori(){
	
		if(isset($_POST['keterangan'])!=""){
			$this->savekategoriToDB();
			exit();
		}else{
			$this->index();
			exit;
		}
	}
	
	function savekategoriToDB(){
		ob_start();
		
		$mtestkat = new model_test();
					
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
			
		if($_POST['hidId']!=""){
			$kategoriid 		= $_POST['hidId'];
			$action 			= "update";
		}else{
			$kategoriid		= $mtestkat->id_testkategori();
			$action 			= "insert";			
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->username;
		//$prodi		= $_POST['cmbprodi'];
		$keterangan	= $_POST['keterangan'];
		$par_inp	= $_POST['jenis_input'];
		$new	= $_POST['newjen'];
		$ceknew = $mtestkat->ceknewkategori($keterangan);
		$cekbyid = $mtestkat->cekbyidkategori($keterangan);
		
		if(($keterangan!='')){
			if($new==1){
				if(!isset($ceknew)){
					// echo $keterangan."<br>";
					// echo $jenisfileid."<br>";
					$datanya 	= Array('kategori_id'=>$kategoriid, 'keterangan'=>$keterangan, 'parameter_input'=>$par_inp);
					$mtestkat->replace_test_kategori($datanya);
					
					
					$this->redirect('module/akademik/test/view_kategori/ok');
					exit();
				}
				else{
					$this->redirect('module/akademik/test/view_kategori/duplicate');
					exit();
				}
			}
			elseif(($new!=1)&&$cekbyid==$kategoriid||$cekbyid==NULL){
				//echo "a";
				$datanya 	= Array('kategori_id'=>$kategoriid, 'keterangan'=>$keterangan, 'parameter_input'=>$par_inp);
				$mtestkat->replace_test_kategori($datanya);
				
				$this->redirect('module/akademik/test/view_kategori/ok');
				exit();
			}
			else{
				$this->redirect('module/akademik/test/view_kategori/duplicate');
				exit();
			}
			
		}else{
			$this->redirect('module/akademik/test/view_kategori/nok');
			exit();
		}
	}
	
	//------KATEGORI-----------------------------------------------------//
	
	//------JAWAB-----------------------------------------------------//	
	function answer_option(){
		$mtest = new model_test();
		
		$id		= $_POST['kategori_id'];
		$param	= $mtest->getparam_input($id);
		echo $param;
	}
	
	function deletejawaban(){
		$mtest = new model_test();
		$jawabid = $_POST['jawabid'];
		
		if($jawabid){
			if($mtest->deleteById($jawabid)==TRUE){
				echo "sukses";
			}
			else {
				echo "";
			}
		}
		else {
			echo "";
		}
	}
	
	function change_answerByParam(){
		$mtest = new model_test();
		
		$id		= $_POST['kategori_id'];
		$soal_id= $_POST['soal_id'];
		
		$param	= $mtest->getparam_input($id);
		$hasil	= $mtest->get_jawaban_edit($soal_id);
		
		$result[] 	= array('param' => $param,'hasil' => $hasil);
		echo json_encode($result);
	}
	
	function change_answerByParam_byBank(){
		$mtest = new model_test();
		
		$id		= $_POST['kategori_id'];
		$soal_id= $_POST['soal_id'];
		
		$param	= $mtest->getparam_input($id);
		$hasil	= $mtest->get_jawaban_edit_bank($soal_id);
		
		$result[] 	= array('param' => $param,'hasil' => $hasil);
		echo json_encode($result);
	}
	//------JAWAB-----------------------------------------------------//
	
	//-------SOAL-----------------------------------------------------//
	
	function soal($menu=NULL,$id=NULL){
		$user = $this->coms->authenticatedUser->role;
		if($user!='Dosen' && $user!='Mahasiswa'){
			if($menu=='write'){
				$this->writesoal($id);
			}
			elseif($menu=='edit'){
				$this->editsoal($id);
			}
			elseif($menu=='save'){
				$this->savesoal();
			}
		}
	}

	function writesoal($id=NULL){
		$mtest = new model_test();
		$user = $this->coms->authenticatedUser->role;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
				
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		
		$this->add_script('js/test/soal.js');
		$this->add_script('js/test/test.js');
		
		if($user!="mahasiswa"){
			if(isset($_POST['bankselect'])&&$_POST['bankselect']!=''){
				$soalid = $_POST['bankselect'];

				$repo_soal_id = explode(',', $soalid);
				$i=0;
				while($i < count($repo_soal_id))
				{
					$soal_id 		 = $repo_soal_id[0];
					$i++;
				}
				//echo $soal_id."<br><br>";
				unset($repo_soal_id[0]);
				// foreach ($repo_soal_id as $s) {
					// echo $s."<br>";
				// }
				
				$data['postsfrombank']  = $mtest->read_bank_soal($soal_id);
				$data['jawaban']		= $mtest->get_bank_jawaban_by_soalid($soal_id);
				$data['paraminput']		= $mtest->getparam_by_bank_soal($soal_id);
				$data['randomanswer']	= $mtest->get_is_random($soal_id);
				$data['repo']			= implode(",", $repo_soal_id);
				
				$data['soal'] 			= $mtest->get_soal($id);
				$data['jawab'] 			= $mtest->get_jawaban();
				$data['get_category'] 	= $mtest->readkategori();
				$data['get_test'] 		= $mtest->get_test();
				$data['testid'] 		= $id;
				$data['user'] 			= $user;
			}
			else{
				$data['user'] 			= $user;
				$data['get_category'] 	= $mtest->readkategori();
				$data['get_test'] 		= $mtest->get_test();
				$data['soal'] 			= $mtest->get_soal($id);
				$data['jawab'] 			= $mtest->get_jawaban();
				$data['posts'] 			= "";
				$data['testid'] 		= $id;
				
			}
			$this->view( 'test/soal/edit.php', $data );
		}
	}
	
	function editsoal($id){
		if( !$id ) {
			$this->redirect('module/akademik/test/');
			exit;
		}
		
		$mtest = new model_test();
		$user = $this->coms->authenticatedUser->role;
		if($user!="mahasiswa"){
			$data['user'] 			= $user;
			$data['posts'] 			= $mtest->readsoal($id);
			$data['get_category'] 	= $mtest->readkategori();
			$data['get_test'] 		= $mtest->get_test();
			$data['jawaban']		= $mtest->get_jawaban_edit($id);
			$data['paraminput']		= $mtest->getparam_by_soal($id);
			$data['randomanswer']	= $mtest->get_is_random($id);
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->add_script('js/test/soal.js');
			//$this->add_script('js/silabus.js');
			
			$this->view( 'test/soal/edit.php', $data );
		}
	}
	
	function deletesoal(){
		$mtest = new model_test();
		
		$id = $_POST['delete_id'];
		$del = $mtest->delete_soal($id);
		if ($del==TRUE){
			echo "true";
		}
	}
	
	function savesoal(){
		if(isset($_POST['pertanyaan'])!=""){
			$this->saveToDBsoal();
			exit();
		}else{
			$this->index();
			exit;
		}
	}
	
	function saveToDBsoal(){
		ob_start();
		
		$mtest = new model_test();
		
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
			
		if($_POST['hidId']!=""){
			$soalid 			= $_POST['hidId'];
			$action 			= "update";
		}else{
			$soalid				= $mtest->id_soal();
			$action 			= "insert";			
		}
		
		if($_POST['banksoal']!=""){
			$banksoal 			= 1;
		}else{
			$banksoal			= 0;		
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;
		//$prodi		= $_POST['cmbprodi'];
		$pertanyaan	= $_POST['pertanyaan'];
		$test_id	= $_POST['test'];
		$kategoriid	= $_POST['kategori'];
		
		if(isset($_POST['matakuliah'])){
			$mkid	  = $_POST['matakuliah'];
			$namamkid = $mtest->get_namamkid($mkid);
		}
		else $mkid="";
		
		if(isset($_POST['jadwal'])){
			$jadwalid=$_POST['jadwal'];
		}
		else $jadwalid="";
		
		if(isset($_POST['materi'])){
			$matid=$_POST['materi'];
		}
		else $matid="";
		
		$new	= $_POST['newsoal'];
		$ceknew = $mtest->ceknewsoal($pertanyaan, $test_id);
		$cekbyid = $mtest->cekbyidsoal($pertanyaan, $test_id);
		
		if(($pertanyaan!='')){
			if($new==1){
				if(!isset($ceknew)){
					// echo $keterangan."<br>";
					// echo $jenisfileid."<br>";
					$datanya 	= Array('soal_id'=>$soalid, 'jadwal_id'=>$jadwalid, 'materi_id'=>$matid, 'mkditawarkan_id'=>$mkid, 'test_id'=>$test_id, 'kategori_id'=>$kategoriid, 'pertanyaan'=>$pertanyaan, 'user_id'=> $user, 'last_update'=>$lastupdate);
					$mtest->replace_test_soal($datanya);
					
					if($banksoal!=1){
					$datanya 	= Array('soal_id'=>$soalid, 'jadwal_id'=>$jadwalid, 'materi_id'=>$matid, 'mkditawarkan_id'=>$mkid, 'namamk_id'=>$namamkid, 'kategori_id'=>$kategoriid, 'pertanyaan'=>$pertanyaan, 'user_id'=> $user, 'last_update'=>$lastupdate);
					$mtest->replace_bank_soal($datanya);
					}
					$this->saveToDBjawaban($soalid, $banksoal, $new);
					
					$this->redirect('module/akademik/soal/index/ok');
					exit();
				}
				else{
					echo "soal-sudah-ada";
					exit();
				}
			}
			elseif(($new!=1)&&$cekbyid==$soalid||$cekbyid==NULL){
					//echo "a";
					$datanya 	= Array('soal_id'=>$soalid, 'jadwal_id'=>$jadwalid, 'materi_id'=>$matid, 'mkditawarkan_id'=>$mkid, 'test_id'=>$test_id, 'kategori_id'=>$kategoriid, 'pertanyaan'=>$pertanyaan, 'user_id'=> $user, 'last_update'=>$lastupdate);
					$mtest->replace_test_soal($datanya);
					
					// $datanya 	= Array('soal_id'=>$soalid, 'jadwal_id'=>$jadwalid, 'materi_id'=>$matid, 'mkditawarkan_id'=>$mkid, 'kategori_id'=>$kategoriid, 'pertanyaan'=>$pertanyaan, 'user_id'=> $user, 'last_update'=>$lastupdate);
					// $mtest->replace_bank_soal($datanya);
					
					$this->saveToDBjawaban($soalid, $banksoal, $new);
					
					//$this->redirect('module/akademik/soal/index/ok');
					//exit();
			}
			else{
					$this->redirect('module/akademik/soal/index/duplicate');
					exit();
			}
			
		}else{
			$this->redirect('module/akademik/soal/index/nok');
			exit();
		}
	}
	
	function saveToDBjawaban($soalid, $banksoal, $new){
		ob_start();
		
		$mtest = new model_test();
		
		if(isset($_POST['essayanswer'])&&$_POST['essayanswer']!=''){
			
			$answer		= $_POST['essayanswer'];
			if($new==1){
				$mtest->deleteBySoalId($soalid);
				$jawabid = $mtest->get_jawabid("", $soalid);
				
				if($banksoal!=1){
				$datanya_bank 	= Array('jawaban_id'=>$jawabid, 
								'soal_id'=>$soalid, 
								'keterangan'=>$answer
								);
				$mtest->replace_bank_jawab($datanya_bank);
				}
			}
			else{
				if(isset($_POST['hidJawabId'])&&$_POST['hidJawabId']!=""){	//edit
					$mtest->deleteBySoalId($soalid);
					$jawabid = $mtest->get_jawabid($_POST['hidJawabId'], $soalid);
					
				}
				else {
					$mtest->deleteBySoalId($soalid);
					$jawabid = $mtest->get_jawabid("", $soalid);
				}
			}
			
			/*if(isset($_POST['hidJawabId'])&&$_POST['hidJawabId']!=""){	//edit
				$jawabid = $mtest->get_jawabid($_POST['hidJawabId'], $soalid);
				$mtest->deleteBySoalId($soalid);
			}else{														//write new
				$mtest->deleteBySoalId($soalid);
				$jawabid = $mtest->get_jawabid("", $soalid);
				
				if($banksoal==1){
				$datanya_bank 	= Array('jawaban_id'=>$jawabid, 
								'soal_id'=>$soalid, 
								'keterangan'=>$answer
								);
				$mtest->replace_bank_jawab($datanya_bank);
				}
			}*/

			$datanya 	= Array('jawaban_id'=>$jawabid, 
								'soal_id'=>$soalid, 
								'keterangan'=>$answer
								);
			$mtest->replace_test_jawab($datanya);
		}
		else{//--else--
		
			$answer	= $_POST['answer'];
			$skor	= $_POST['skor'];
			$isbenar= $_POST['isvalbenar'];
			
			
			if(!isset($_POST['israndomjawaban'])&&$_POST['israndomjawaban']==''){
				$israndom= '0';
			}
			else {
				$israndom= '1';
			}
			
			$i = 0;
			foreach($isbenar as $ib){
				$i++;	
				if($ib=='1'){
					$isbenarcek[]= '1';
				}
				else {
					$isbenarcek[]= '0';
				}
			}
			
			if(isset($_POST['hidJawabId'])&&$_POST['hidJawabId']!=""){ //edit
				for($x=0;$x<$i;$x++){
					if($banksoal==1){
						$jawabid = $mtest->get_jawabid("", $soalid);
					}
					else {
						$jawabid = $mtest->get_jawabid($_POST['hidJawabId'][$x], $soalid);
					}
					$datanya 	= Array('jawaban_id'=>$jawabid, 
									'soal_id'=>$soalid, 
									'keterangan'=>$answer[$x], 
									'is_benar'=>$isbenarcek[$x], 
									'skor'=>$skor[$x], 
									'urut'=>($x+1),
									'is_random'=>$israndom
									);
					$mtest->replace_test_jawab($datanya);
				}
			}else{														//write new
				for($x=0;$x<$i;$x++){
					$jawabid = $mtest->get_jawabid("", $soalid);
					$datanya 	= Array('jawaban_id'=>$jawabid, 
									'soal_id'=>$soalid, 
									'keterangan'=>$answer[$x], 
									'is_benar'=>$isbenarcek[$x], 
									'skor'=>$skor[$x], 
									'urut'=>($x+1),
									'is_random'=>$israndom
									);
					$mtest->replace_test_jawab($datanya);
					
					$datanya_bank 	= Array('jawaban_id'=>$jawabid, 
									'soal_id'=>$soalid, 
									'keterangan'=>$answer[$x], 
									'is_benar'=>$isbenarcek[$x], 
									'skor'=>$skor[$x], 
									'urut'=>($x+1)
									);
					$mtest->replace_bank_jawab($datanya_bank);
				}
				
			}
			
			/*for($x=0;$x<$i;$x++){
				// echo ($x+1).") ".$answer[$x]." - ".$skor[$x]." - ".$isbenarcek[$x]."<br>";
				//$jawabid[$x] = $mtest->get_id_jawaban();
				$datanya 	= Array('jawaban_id'=>$jawabid[$x], 
									'soal_id'=>$soalid, 
									'keterangan'=>$answer[$x], 
									'is_benar'=>$isbenarcek[$x], 
									'skor'=>$skor[$x], 
									'urut'=>($x+1),
									'is_random'=>$israndom
									);
				$mtest->replace_test_jawab($datanya);
			}*/
		
		}//--else--
	}

	function tampilkan_detail_test(){
		$mtest = new model_test();
		$id = $_POST['test_id'];
		$detail = $mtest->get_detail_test($id);
		// echo "<option value='0'>Select Tahun Akademik</option>" ;
		if($id!=0){
			foreach($detail as $dt )
			{
				echo '<div class="form-group">
						<label class="col-sm-2 control-label">Mata Kuliah</label>
						<div class="controls">
							<div class="col-sm-10">
								<input type="text" class="form-control" disabled="disabled" value="'.$dt->mata_kuliah.'"/>
								<input type="hidden" class="form-control" name="matakuliah" value="'.$dt->mkditawarkan_id.'"/>
							</div>
						</div>
					 </div>';
				if(isset($dt->materi)){
					echo '<div class="form-group">
							<label class="col-sm-2 control-label">Materi</label>
							<div class="controls">
								<div class="col-sm-10">
									<input type="text" class="form-control" disabled="disabled" value="'.$dt->materi.'"/>
									<input type="hidden" class="form-control" name="materi" value="'.$dt->materi_id.'"/>
								</div>
							</div>
						 </div>';
				}
				
				if(isset($dt->jadwal)){
					echo '<div class="form-group">
							<label class="col-sm-2 control-label">Jadwal Kelas</label>
							<div class="controls">
								<div class="col-sm-10">
									<input type="text" class="form-control" disabled="disabled" value="'.$dt->jadwal.'"/>
									<input type="hidden" class="form-control" name="jadwal" value="'.$dt->jadwal_id.'"/>
								</div>
							</div>
						 </div>';
				}	
			}
		}
	}
	//-------SOAL-----------------------------------------------------//
	
	//------TEST BANK-----------------------------------------------------//
	
	function bank($cek=NULL, $id=NULL){
		$user = $this->coms->authenticatedUser->role;
		if($user!='Mahasiswa'){
			
		if($cek=='soal'){
			$this->banksoal($id); //id = testid
		}
		
		}
	}
	
	function banksoal($id=NULL){
		$mtest = new model_test();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/test/bank.js');
		
		$banksoal = $mtest->get_bank($id);
		foreach ($banksoal as $bs) {
			$namamk = $bs->namamk;
			$namamkid = $bs->namamk_id;
		}
		$data['banksoal'] = $mtest->get_bank_soal($namamkid);
		$data['bankjawaban'] = $mtest->get_bank_jawaban();
		$data['banknamamk'] = $namamk;
		$data['testid']		= $id;
		
		$this->view('test/bank/index.php', $data);
	}
	
	//------TEST BANK-----------------------------------------------------//
	
}
?>