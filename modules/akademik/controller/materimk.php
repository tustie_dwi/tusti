<?php
class akademik_materimk extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		
		
		$user = $this->coms->authenticatedUser->role;
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		if($user!="mahasiswa" && $user!="dosen"){
			$mmateri = new model_materimk();
			$mconf = new model_conf();
			
			$data['posts'] = $mmateri->read('');
			$data['user'] = $user;
			$data['fakultas_id'] = $fakultas_id;
			$data['thn'] = $mmateri->tahun_akademik('');
			$data['thnedit'] = $mmateri->tahun_edit();
			$data['fakultas'] = $mconf->get_fakultas();
			$data['cabang'] = $mconf->get_cabangub();
			$data['materi'] = $mmateri->get_materi();
			$data['mkditawarkan'] = $mmateri->mkditawarkan();
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			$this->add_script('js/mk/materimk.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			switch($by){
				case 'ok';
					$data['status'] 	= 'OK';
					$data['statusmsg']  = 'OK, data telah diupdate.';
				break;
				case 'duplicate';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data dengan nama atau no.urut materimk tersebut sudah ada';
				break;
				case 'nok';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
				break;
				case 'wrongfiletype';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, tipe file yang di upload salah';
				break;
				case 'failed';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda';
				break;
			}
				
			$this->view('materimk/index.php', $data);
		}
	}

	function detail($id){
		
		
		$user = $this->coms->authenticatedUser->role;
		if($user!="mahasiswa" && $user!="dosen"){
			$mmateri = new model_materimk();
			$data['posts'] = $mmateri->read($id);
			$data['user'] = $user;
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
				
			$this->view('materimk/detail.php', $data);
		}
	}
	
	function write(){
		$user = $this->coms->authenticatedUser->role;
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		if($user!="mahasiswa" && $user!="dosen"){
			$mmateri = new model_materimk();
			$mconf = new model_conf();
			$data['user'] = $user;
			$data['fakultas_id'] = $fakultas_id;
			$data['posts'] = "";
			$data['thn'] = $mmateri->tahun_akademik('');
			$data['thnedit'] = $mmateri->tahun_edit();
			$data['fakultas'] = $mconf->get_fakultas();
			$data['cabang'] = $mconf->get_cabangub();
			$data['materi'] = $mmateri->get_materi();
			$data['statmateri'] = $mmateri->get_status_materi();
			$data['mkditawarkan'] = $mmateri->mkditawarkan();
			
			
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			//$this->add_script('js/jquery.filedrop.js');
			//$this->add_script('js/upload-file.js');
			//$this->add_style('css/upload.css');
			$this->add_script('js/mk/materimk.js');
			$this->coms->add_script('js/submit.js');
			$this->coms->add_script('ckeditor/ckeditor.js');
	
			$this->view( 'materimk/edit.php', $data );
		}
	}

	function edit($id){
		if( !$id ) {
			$this->redirect('module/akademik/materimk/');
			exit;
		}
		$user = $this->coms->authenticatedUser->role;
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		if($user!="mahasiswa" && $user!="dosen"){
			$mmateri = new model_materimk();
			$mconf = new model_conf();
			$data['user'] = $user;
			$data['fakultas_id'] = $fakultas_id;
			$data['posts'] = $mmateri->read($id);
			$data['thn'] = $mmateri->tahun_akademik('');
			$data['thnedit'] = $mmateri->tahun_edit();
			$data['fakultas'] = $mconf->get_fakultas();
			$data['cabang'] = $mconf->get_cabangub();
			$data['materi'] = $mmateri->get_materi();
			$data['statmateri'] = $mmateri->get_status_materi();
			$data['mkditawarkan'] = $mmateri->mkditawarkan();

			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->add_script('js/mk/materimk.js');
			$this->coms->add_script('js/submit.js');
			$this->coms->add_script('ckeditor/ckeditor.js');
	
			$this->view( 'materimk/edit.php', $data );
		}
	}
	
	function newsub($id=NULL,$mkid=NULL){
		if( !$id ) {
			$this->redirect('module/akademik/materimk/');
			exit;
		}
		$user = $this->coms->authenticatedUser->role;
		$fakultas_id = $this->coms->authenticatedUser->fakultas;
		if($user!="mahasiswa" && $user!="dosen"){
			$mmateri = new model_materimk();
			$mconf = new model_conf();
			$data['user'] = $user;
			$data['fakultas_id'] = $fakultas_id;
			$data['posts'] = "";
			$data['pos'] = $mmateri->read($id);
			$data['thn'] = $mmateri->tahun_akademik('');
			$data['thnedit'] = $mmateri->tahun_edit();
			$data['fakultas'] = $mconf->get_fakultas();
			$data['cabang'] = $mconf->get_cabangub();
			$data['materi'] = $mmateri->get_materi($mkid,$id);
			$data['statmateri'] = $mmateri->get_status_materi();
			$data['mkditawarkan'] = $mmateri->mkditawarkan();
			
			$data['jumlahsub']	= $mmateri->get_submateri_count($id);
			$data['jumlahmatbymk']	= $mmateri->get_materibymk_count($mkid);
			$data['mkid']		= $mkid;
			
			
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->add_script('js/mk/materimk.js');
			$this->coms->add_script('js/submit.js');
			$this->coms->add_script('ckeditor/ckeditor.js');
	
			$this->view( 'materimk/edit.php', $data );
		}
	}
	
	function tampilkan_thn()
	{
		$mmateri = new model_materimk();
		//$id = $_POST['fakultas_id'];
		$parent = $mmateri->tahun_akademik();
		echo "<option value='0'>Select Tahun Akademik</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->thn_id."'>".$p->tahun."</option>" ;	
		}	
	}
	
	function tampilkan_mk()
	{
		$mmateri = new model_materimk();
		$id 	= $_POST['hid_id'];
		$thn_id 	= $_POST['thn_id'];
		$cabang_id	= $_POST['cabang_id'];
		$parent = $mmateri->tampilkan_mk($id,$thn_id,$cabang_id);
		
		echo "<option value='0'>Select Mata Kuliah</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->mkditawarkan_id."'";
			echo ">".$p->keterangan."</option>" ;	
		}
	}

	function tampilkan_submk()
	{
		$mmateri = new model_materimk();
		$id 	= $_POST['hid_id'];
		$thn_id 	= $_POST['thn_id'];
		$mk_id 	= $_POST['mkditawar_id'];
		$parent = $mmateri->tampilkan_submateri($id,$thn_id,$mk_id);
				
		echo "<option value='0'>Select Sub Materi</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->materi_id."'";
			echo ">".$p->judul."</option>" ;	
		}
	}
	
	function tampilkan_urut()
	{
		$mmateri = new model_materimk();
		$id 	= $_POST['hid_id'];
		$thn_id 	= $_POST['thn_id'];
		$mk_id 	= $_POST['mkditawar_id'];
		if(isset($_POST['materi_id'])){
			$mat_id 	= $_POST['materi_id'];
		}
		$parent = $mmateri->tampilkan_submateri($id,$thn_id,$mk_id);
		
		
		if(!isset($mat_id) || $mat_id == 0){
			$jum = $mmateri->get_materibymk_count($mk_id);
			echo $jum+1;
		}
		elseif(isset($mat_id)&&$mat_id!=0)
		{
			 $jum = $mmateri->get_submateri_count($mat_id);
			 $materi = $mmateri->get_materi($mk_id,$mat_id);
			 foreach($materi as $dt){
			 	$urut = $dt->urut;
			 }
			 echo $urut.".".($jum+1);
		}
	}
	
	function savematerimk(){
	
		if(isset($_POST['judul'])&&$_POST['judul']!=""){
			$this->savemateriToDB();
			exit();
		}else{
			$this->index();
			exit;
		}
		
		//echo "tes";
	
	}
	
	function savemateriToDB(){
		ob_start();
		
		$mmateri = new model_materimk();
					
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
			
		if($_POST['hidmatId']!=""){
			$materiid 		= $_POST['hidmatId'];
			$action 		= "update";
		}else{
			$materiid			= $mmateri->id_materimk();
			$action 			= "insert";			
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;
		$parent_id	= $_POST['submateri'];
		$mkditawarkan_id	= $_POST['mkditawarkan'];
		$judul	= $_POST['judul'];
		$keterangan	= $_POST['keterangan'];
		$urut	= $_POST['urut'];
		
		$statmateri	= $_POST['statmateri'];
	
		if(isset($_POST['b_savepublish'])){
			$ispublish = 1;
		}
		elseif(isset($_POST['b_draft'])){
			$ispublish = 0;
		}
		
		if(isset($_POST['campusonly'])!=""){
			$campusonly	= $_POST['campusonly'];
		}else{
			$campusonly	= 0;
		}
		
		$new	= $_POST['new'];
		$ceknew = $mmateri->cek_new($judul);
		$ceknewbyid = $mmateri->cek_new_by_id($judul);
		$cekurut = $mmateri->cek_urut($urut,$mkditawarkan_id);
		$cekurutbyid = $mmateri->cek_urut_by_id($urut);
		
		$month = date('m');
		$year = date('Y');
		if(($judul!='')){
			if($new==1){
				if(!isset($ceknew)&&!isset($cekurut)){
					foreach ($_FILES['icon'] as $id => $icon) {
						if($id == 'error'){
							if($icon!=0){
								$cekicon = 'error';
							}
							else $cekicon = 'sukses';
						}
					}
					
					if($cekicon!='error'){
							$name = $_FILES['icon']['name'];
							$ext	= $this->get_extension($name);
							$seleksi_ext = $mmateri->get_ext($ext);
							if($seleksi_ext!=NULL){
									$jenis_file_id 	= $seleksi_ext->jenis_file_id;
									$jenis			= $seleksi_ext->keterangan;
									$maxfilesize	= $seleksi_ext->max_size;
									
									switch(strToLower($jenis)){
										case 'image':
											$extpath = "image";
										break;
									}
							}
							else{
									echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
									exit();
							}
							
							$allowed_ext = array('jpg','jpeg','png', 'gif');
							
							$icon_size=$_FILES["icon"]["size"] ; 
							$ceknamaicon = $mmateri->cek_nama_icon($name);
							$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
							$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
							
							if (!file_exists($upload_dir)) {
										mkdir($upload_dir, 0777, true);
										chmod($upload_dir, 0777);
							}
							$icon_loc = $upload_dir_db . $name;
							if(!in_array($ext,$allowed_ext)){
									echo "Maaf, tipe file yang di upload salah";
									exit();
							}
							elseif (($ceknamaicon==NULL) && ($icon_size <= $maxfilesize)){
									// //echo $file_size;				
									if($_SERVER['REQUEST_METHOD'] == 'POST') {
												rename($_FILES['icon']['tmp_name'], $upload_dir . $name);
									}
							}
					}
					else{
						if(isset($_POST['icon'])){
							$icon_loc = $_POST['icon'];
						}
					}
					$url  = trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($judul))))));
	
					if(isset($icon_loc)){
						$datanya 	= Array('materi_id'=>$materiid,
										    'mkditawarkan_id'=>$mkditawarkan_id,
										    'page_link'=>$url,
										    'judul'=>$judul,
										    'keterangan'=>$keterangan,
										    'is_publish'=>$ispublish,
										    'status'=>$statmateri,
										    'urut'=>$urut,
										    'parent_id'=>$parent_id,
										    'is_campus'=>$campusonly,
										    'icon'=>$icon_loc,
										    'user_id'=>$user,
										    'last_update'=>$lastupdate
											);
					}
					else {
						$datanya 	= Array('materi_id'=>$materiid,
										    'mkditawarkan_id'=>$mkditawarkan_id,
										    'page_link'=>$url,
										    'judul'=>$judul,
										    'keterangan'=>$keterangan,
										    'is_publish'=>$ispublish,
										    'status'=>$statmateri,
										    'urut'=>$urut,
										    'parent_id'=>$parent_id,
										    'is_campus'=>$campusonly,
										    'user_id'=>$user,
										    'last_update'=>$lastupdate
											);
					}
											
					  $mmateri->replace_materimk($datanya);
					  echo "OK, data telah diupdate.";
					  exit();
				}
				else{
					echo "Maaf, data dengan nama atau no.urut materimk tersebut sudah ada";
					exit();
				}
			}
			elseif(($new!=1)&&$ceknewbyid==$materiid || $cekurutbyid==$materiid|| $cekurutbyid==NULL || $ceknewbyid==NULL){

				foreach ($_FILES['icon'] as $id => $icon) {
					if($id == 'error'){
						if($icon!=0){
							$cekicon = 'error';
						}
						else $cekicon = 'sukses';
					}
				}
				
				if($cekicon!='error'){
						$name = $_FILES['icon']['name'];
						$ext	= $this->get_extension($name);
						$seleksi_ext = $mmateri->get_ext($ext);
						if($seleksi_ext!=NULL){
								$jenis_file_id 	= $seleksi_ext->jenis_file_id;
								$jenis			= $seleksi_ext->keterangan;
								$maxfilesize	= $seleksi_ext->max_size;
								
								switch(strToLower($jenis)){
									case 'image':
										$extpath = "image";
									break;
								}
						}
						else{
								echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
								exit(); 	
						}
						
						$allowed_ext = array('jpg','jpeg','png', 'gif');
						
						$icon_size=$_FILES["icon"]["size"] ; 
						$ceknamaicon = $mmateri->cek_nama_icon($name);
						$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
						$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;
						
						if (!file_exists($upload_dir)) {
									mkdir($upload_dir, 0777, true);
									chmod($upload_dir, 0777);
						}
						$icon_loc = $upload_dir_db . $name;
						if(!in_array($ext,$allowed_ext)){
									echo "Maaf, tipe file yang di upload salah";
									exit();
						}
						elseif (($ceknamaicon==NULL) && ($icon_size <= $maxfilesize)){
								// //echo $file_size;				
								if($_SERVER['REQUEST_METHOD'] == 'POST') {
											rename($_FILES['icon']['tmp_name'], $upload_dir . $name);
								}
						}
				}
				else{
					$icon_loc = $_POST['icon'];
				}
				$url  = trim(preg_replace('/[ \/]/', '-', (preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 \/]/', '', strtolower($judul))))));
				//echo $url;
				if(isset($icon_loc)){
					$datanya 	= Array('materi_id'=>$materiid, 
									    'mkditawarkan_id'=>$mkditawarkan_id,
									    'page_link'=>$url,
									    'judul'=>$judul,
									    'keterangan'=>$keterangan,
									    'is_publish'=>$ispublish,
									    'status'=>$statmateri,
									    'urut'=>$urut,
									    'parent_id'=>$parent_id,
									    'is_campus'=>$campusonly,
									    'icon'=>$icon_loc,
									    'user_id'=>$user,
									    'last_update'=>$lastupdate
										);
				}
				else {
					$datanya 	= Array('materi_id'=>$materiid, 
									    'mkditawarkan_id'=>$mkditawarkan_id,
									    'page_link'=>$url,
									    'judul'=>$judul,
									    'keterangan'=>$keterangan,
									    'is_publish'=>$ispublish,
									    'status'=>$statmateri,
									    'urut'=>$urut,
									    'parent_id'=>$parent_id,
									    'is_campus'=>$campusonly,
									    'user_id'=>$user,
									    'last_update'=>$lastupdate
										);
				}
										
				  $mmateri->replace_materimk($datanya);	
				   echo "OK, data telah diupdate.";
				   exit();
			}
			else{
				echo "Maaf, data dengan nama atau no.urut materimk tersebut sudah ada";
				exit();
			}	
		}else{
			echo "Maaf, data tidak dapat tersimpan.";
			exit();
		}
	}

	function exit_status($str){
		echo json_encode(array('status'=>$str));
		exit;
	}

	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_title($file_name){
		$name = explode('.', $file_name);
		array_pop($name);
		return implode('.', $name);
	}

	function delete($mat=NULL){
		$mmateri = new model_materimk();
		if (!$mat){
			$id = $_POST['delete_id'];
		}
		else $id=$mat;
		// echo $id;
		// exit();
		$getid = $mmateri->get_id_from_parent($id);
		$delpar = $mmateri->delete($id);
		if(isset($getid)){
			foreach ($getid as $c) {
				$matid=$c->materi_id;
				$this->delete($matid);
			}
		}
		
		echo "Data telah berhasil terhapus!!";
	}
	
	function tampilkan_indexthn()
	{
		$mmateri 	= new model_materimk();
		//$id = $_POST['fakultas_id'];
		$parent 	= $mmateri->tahun_akademik();
		echo "<option value='0'>Select Tahun Akademik</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->thn_id."'>".$p->tahun."</option>" ;	
		}	
	}
	
	function tampilkan_indexmk()
	{
		$mmateri 	= new model_materimk();
		$id 		= $_POST['hid_id'];
		$cabang 		= $_POST['cabang'];
		$thn_id 	= $_POST['thn_id'];
		
		$parent 	= $mmateri->tampilkan_mk($id,$thn_id,$cabang);
		
		echo "<option value='0'>Select Mata Kuliah</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->mkditawarkan_id."'";
			echo ">".$p->keterangan."</option>" ;	
		}
	}
	
	function tampilkan_index()
	{
		$mmateri 	= new model_materimk();
		$id 		= $_POST['hid_id'];
		$thn_id 	= $_POST['thn_id'];
		$mk_id 		= $_POST['mkditawar_id'];
		$cabang 	= $_POST['cabang'];
		$parent 	= $mmateri->tampilkan_index($id,$thn_id,$mk_id,$cabang);
		$user 		= $this->coms->authenticatedUser->role;
		
		$data['parent']=$parent;
		$data['user']=$user;
		
		$this->view('materimk/viewselection.php',$data);
	}

	function detailmaterichild($mk, $id, $p, $i=NULL){
		
		$mmateri = new model_materimk();
		$materi = $mmateri->get_materi($mk, "",$id);
		$par = $p; //ambil urutan looping parent
		$user = $this->coms->authenticatedUser->role;
		
		if($materi){
			$i+=1;//jumlahnya nambah tiap manggil detail materi atau bila punya child
			$tab = str_repeat("&nbsp;&nbsp;&nbsp;", $i);//tab untuk child
			$a=1;
			foreach ($materi as $dt):
				$n=$par.$a;//urutan parent+self
				// $i++;
				?>
										
						<!-- <td>&nbsp;&nbsp;<a href="<?php echo $this->location('module/content/course/read/content/'.$dt->mk.'/'.$dt->id); ?>"><?php	echo $dt->urut.". ".$dt->judul;?></a>&nbsp;&nbsp; -->							
						<?php $uri_location = $this->location('module/akademik/materimk/delete'); ?>
						     
							<ul class='nav nav-pills' style='margin:0;'>
								<li class='dropdown'>
									<a class='dropdown-toggle' id='drop4' role='button' data-toggle='dropdown' href='#'><?php echo $tab.$par.".".$a.". ".$dt->judul;?>&nbsp;&nbsp;</a>
										<ul id='menu1' class='dropdown-menu' role='menu' aria-labelledby='drop4'>
											<li>
												<?php if($user!="mahasiswa"){ ?>
												<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/materimk/detail/'.$dt->materi_id); ?>"><i class='fa fa-search'></i> View</a>	
												<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/materimk/edit/'.$dt->materi_id); ?>"><i class='fa fa-pencil'></i> Edit</a>
												<a class='btn-edit-post' href="<?php echo $this->location('module/akademik/materimk/newsub/'.$dt->materi_id.'/'.$dt->mk); ?>"><i class='fa fa-plus'></i> Tambah Sub Materi</a>
												<?php } ?>
											</li>
											<li>
												<?php if($user!="mahasiswa"){ ?>
												<a class='btn-edit-post' href="#" onclick="doDelete(<?php echo $dt->hid_id ?>)"><i class='fa fa-trash-o'></i> Delete Materi</a>		
												<input type='hidden' class='deleted<?php echo $dt->hid_id ?>' value="<?php echo $dt->hid_id ?>" uri = "<?php echo $uri_location ?>"/>				
												<?php } ?>
											</li>
										</ul>
								</li>
							</ul>
						
				<?php
				$a++;
				$cont = $par.".".($a-1);
				echo $this->detailmaterichild($dt->mkditawarkan_id, $dt->hid_id, $cont, $i);
				
			endforeach;
		}
	}
	
}
?>