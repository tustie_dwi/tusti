<?php
class akademik_file extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all'){
		
		$mfile = new model_file();
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="mahasiswa" && $user!="dosen"){
			//$data['posts'] = $mfile->read();
			$data['user'] = $user;
			//$data['kategori'] = $mfile->get_kategori();
			
			
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_script('js/log.js');
			
			switch($by){
				case 'ok';
					$data['status'] 	= 'OK';
					$data['statusmsg']  = 'OK, data telah diupdate';
				break;
				case 'duplicate';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data dengan nama atau no.urut file tersebut sudah ada';
				break;
				case 'nok';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan';
				break;
				case 'wrongfiletype';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, tipe file yang di upload salah';
				break;
				case 'failed';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda';
				break;
			}
			
			$this->add_script('js/mk/file.js');
			
			$this->view('file/index.php', $data);
		}
	}
	
	function write(){
		
		
		
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="mahasiswa" && $user!="dosen"){
		$mfile = new model_file();
		$data['user'] = $user;
		
		$data['posts'] = "";
		$data['materi'] = $mfile->get_materi();
		
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->add_script('js/mk/file.js');
		$this->coms->add_script('js/submit.js');

		$this->view( 'file/edit.php', $data );
		}
	}
	
	function edit($id){
		if( !$id ) {
			$this->redirect('module/akademik/file/');
			exit;
		}

		$user = $this->coms->authenticatedUser->role;
		if($user!="mahasiswa" && $user!="dosen"){
		$mfile = new model_file();
		$data['user'] = $user;
		
		$data['posts'] = $mfile->read($id);
		$data['file'] = $mfile->get_file();
		$data['materi'] = $mfile->get_materi();
		
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('ckeditor/ckeditor.js');
		$this->add_script('js/mk/file.js');
		$this->coms->add_script('js/submit.js');

		$this->view( 'file/edit.php', $data );
		}
	}
	
	function views($id){
		if( !$id ) {
			$this->redirect('module/akademik/file/');
			exit;
		}
		
		$mfile = new model_file();
		
		
		$user = $this->coms->authenticatedUser->role;
		$data['user'] = $user;
		
		$data['posts'] = $mfile->viewdetail($id);
						
		$this->add_script('js/mk/file.js');
		$this->coms->add_script('js/log.js');

		$this->view( 'file/view.php', $data );
	}
	
	function tampilkan_kategori()
	{
		$mfile = new model_file();
		$j = $_POST['jenis'];
		if($j==1){
			//echo "GENERAL";
			$parent = $mfile->get_kategori_by_general();
			echo "<option value='0'>Select Category</option>" ;
			foreach($parent as $p )
				{
					echo "<option value='".$p->jenis_file_id."'>".$p->keterangan."</option>";	
				}
		}
		elseif($j==2){
			//echo "MATERI";
			$parent = $mfile->get_kategori_by_materimk();
			echo "<option value='0'>Select Category</option>" ;
			foreach($parent as $p )
				{
					echo "<option value='".$p->jenis_file_id."'>".$p->keterangan."</option>";	
				}
		}	
	}
	
	function tampilkan_materi()
	{
		$mfile = new model_file();
		$jenis_fileid = $_POST['jenis_file_id'];
		$j = $_POST['jenis'];
		if($j==1){
			
			$matid = "";
			
			$parentgen = $mfile->get_file_by_kategori_jenis_general($jenis_fileid,$matid);
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
			$data ['post'] = $parentgen;
			$this->view( 'file/viewselection.php', $data );
		}
		elseif($j==2){
			//echo "MATERI";
			$parent = $mfile->get_materi_by_kategori($jenis_fileid);
			if($parent){
			echo "<option value='0'>Select Materi</option>" ;
			foreach($parent as $p )
				{
					echo "<option value='".$p->mat_id."'>".$p->nama_materi."</option>";	
				}
			}
		}	
	}
	
	function tampilkan_file()
	{
		$mfile = new model_file();
		$id = $_POST['jenis_file_id'];
		$jenis = $_POST['jenis'];
		$matid = $_POST['materi_id'];

			if($jenis==1){ //GENERAL
				
			}

			elseif($jenis==2){//MATERIMK
				$parentmmk = $mfile->get_file_by_kategori_jenis_materimk($id,$matid);
				$data ['post'] = $parentmmk;
				
				$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
				$this->coms->add_script('js/datatables/jquery.dataTables.js');
				$this->coms->add_script('js/datatables/DT_bootstrap.js');
		
				$this->view( 'file/viewselection.php', $data );
			}
		//}	
	}
	
	function save(){
	
		if((isset($_POST['judulx']))&&$_POST['judulx']!=""){
			$this->saveToDB();
			exit();
			//echo "halo";
		}else{
			//echo "judul =" . $_POST['judulx'];
			echo "Maaf, data tidak dapat tersimpan";
			exit();
		}
	
	}
	
	function saveToDB(){
		ob_start();
		
		$mfile = new model_file();

		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));

		if($_POST['hidId']!=""){
			$fileid 		= $_POST['hidId'];
			$action 		= "update";
		}else{
			$fileid			= $mfile->id_file();
			$action 		= "insert";			
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;

		$judul	= $_POST['judulx'];
		$keterangan	= $_POST['keterangan'];

		if(isset($_POST['b_publish'])){
				$ispublish = 1;
		}
		elseif(isset($_POST['b_draft'])){
				$ispublish = 0;
		}
		else $ispublish = 0;

		$new	= $_POST['new'];
		$ceknew = $mfile->cek_new($judul);
		$ceknewbyid = $mfile->cek_new_by_id($judul);

		$month = date('m');
		$year = date('Y');				

		if($_POST['materi']==0){
				$materi_id = "";
		}
		else $materi_id	= $_POST['materi'];

		$mkd_id	= $mfile->get_mkditawarkan_by_materi_id($materi_id);
		if(!isset($mkd_id)){
			$mkd_id = 0;
		}
		
		
		if(isset($judul)){
			if($new==1){
				if(!isset($ceknew)){
				
				$uploadby 	= $this->coms->authenticatedUser->username;
				// //=============FILE UPLOAD START==================
				$files=$_FILES['uploads'];
				$this->upload_file($fileid, $judul, $keterangan, $ispublish, $materi_id, $mkd_id, $uploadby, $lastupdate, $user);
				// //=============FILE UPLOAD END====================

					echo "OK, data telah diupdate";
					exit();
				}
				else{
					echo "Maaf, data dengan nama file tersebut sudah ada";
					exit();
				}
			}
			elseif(($new!=1)&&$ceknewbyid==$materi_id || $ceknewbyid==NULL){
				$jen_file_id		= $_POST['jenis_file_id'];
				$file_name			= $_POST['file_name'];
				$file_type			= $_POST['file_type'];
				$file_loc			= $_POST['file_loc'];
				$file_size			= $_POST['file_size'];
				$file_content		= $_POST['file_content'];
				$tgl_upload			= $_POST['tgl_upload'];
				$upload_by			= $_POST['upload_by'];

				$datanya2 	= Array('file_id'=>$fileid,
							'jenis_file_id'=>$jen_file_id,
							'materi_id'=>$materi_id,
							'mkditawarkan_id'=>$mkd_id,
						    'judul'=>$judul,
						    'keterangan'=>$keterangan,
						    'file_name'=>$file_name,
						    'file_type'=>$file_type,
						    'file_loc'=>$file_loc,
						    'file_size'=>$file_size,
						    'file_content'=>$file_content,
						    'tgl_upload'=>$tgl_upload,
						    'upload_by'=>$upload_by,
						    'is_publish'=>$ispublish,
						    'user_id'=>$user,
						    'last_update'=>$lastupdate
							);
				$mfile->replace_file($datanya2);

				echo "OK, data telah diupdate";
				exit();
			}
			else{
				echo "Maaf, data dengan nama file tersebut sudah ada";
				exit();
			}	
		}else{
			echo "Maaf, data tidak dapat tersimpan";
			exit();
		}
	}

	function exit_status($str){
		echo json_encode(array('status'=>$str));
		exit;
	}

	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function get_title($file_name){
		$name = explode('.', $file_name);
		array_pop($name);
		return implode('.', $name);
	}
	
	function download($filename){
		$mfile = new model_file();
		
		$path = "assets/".$mfile->get_locfile($filename);	
			
		if (file_exists($path)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename=' . urlencode(basename($path)));
			// header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($path));
			ob_clean();
			flush();
			readfile($path);
			exit;
		}else{
			die('File Not Found');
		}
		
		
	}
	
	function log_view(){
		$mfile = new model_file();
		$user = $this->coms->authenticatedUser->id;
		$action = $_POST['action'];
		$fileid = $_POST['file_id'];
		$result = $mfile->log_data($user, $action, $fileid);
		if($result == TRUE){
			echo "BERHASIL";
		}
		else echo "GAGAL";
	}
	
	function log_download(){
		$mfile = new model_file();
		$user = $this->coms->authenticatedUser->id;
		$action = $_POST['action'];
		$fileid = $_POST['file_id'];
		$result = $mfile->log_data($user, $action, $fileid);
		if($result == TRUE){
			echo "BERHASIL";
		}
		else echo "GAGAL";
	}

	function upload_file($fileid, $judul, $keterangan, $ispublish, $materi_id=NULL, $mkd_id=NULL, $uploadby, $lastupdate, $user){
		$mfile = new model_file();
		$month = date('m');
		$year = date('Y');
		foreach($_FILES["uploads"]['name'] as $id => $name){
			$ext	= $this->get_extension($name);
			$seleksi_ext = $mfile->get_ext($ext);

			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
				
				switch(strToLower($jenis)){
					case 'image':
						$extpath = "image";
					break;
					case 'presentation':
						$extpath = "presentation";
					break;
					case 'document':
						$extpath = "document";
					break;
					case 'video':
						$extpath = "video";
					break;
					case 'spreadsheet':
						$extpath = "spreadsheet";
					break;
				}
			}
			else{
				echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
				exit();
			}

			$allowed_ext = array('jpg','jpeg','png','gif','pdf', 'docx', 'doc', 'ppt', 'pptx', 'xls', 'xlsx', 'webm');
			//$fileid = $mfile->id_file();

			$file_name = $name;
			$ceknamafile = $mfile->cek_nama_file($file_name);
			//$file = addslashes(file_get_contents($_FILES["uploads"]["tmp_name"][$id]));

			$file = "";
			$file_type=$_FILES["uploads"]["type"][$id] ; 
			$file_size=$_FILES["uploads"]["size"][$id] ; 
			$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
			$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;

			if (!file_exists($upload_dir)) {
				mkdir($upload_dir, 0777, true);
				chmod($upload_dir, 0777);
			}
			$file_loc = $upload_dir_db . $name;
			
			$tgl_upload = date("Y-m-d H:i:s");
			
			if(!in_array($ext,$allowed_ext)){
				echo "Maaf, tipe file yang di upload salah";
				exit();
			}
			elseif (($ceknamafile==NULL) && ($file_size <= $maxfilesize)){			
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
							rename($_FILES['uploads']['tmp_name'][$id], $upload_dir . $name);
				}
					$datanya2 	= Array('file_id'=>$fileid,
										'jenis_file_id'=>$jenis_file_id,
										'materi_id'=>$materi_id,
										'mkditawarkan_id'=>$mkd_id,
									    'judul'=>$judul,
									    'keterangan'=>$keterangan,
									    'file_name'=>$file_name,
									    'file_type'=>$file_type,
									    'file_loc'=>$file_loc,
									    'file_size'=>$file_size,
									    'file_content'=>$file,
									    'tgl_upload'=>$tgl_upload,
									    'upload_by'=>$uploadby,
									    'is_publish'=>$ispublish,
									    'user_id'=>$user,
									    'last_update'=>$lastupdate
								  );
					$mfile->replace_file($datanya2);
	
			}
			else{
				echo "Maaf, upload file gagal, periksa kembali nama file , ukuran file dan tipe file anda";
				exit();
			}
		}		
	}
}
?>