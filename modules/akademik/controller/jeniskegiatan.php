<?php
class akademik_jeniskegiatan extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mjenkeg = new model_jeniskegiatan();	
		
		//$data['posts'] = $mjenkeg->clean();	
		$data['posts'] = $mjenkeg->read('');	

		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
			case 'asign';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data telah di Assign.';
			break;
			case 'duplicate';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data telah ada.';
			break;
		}
		
		
		$this->view( 'jeniskegiatan/index.php', $data );
	}
	
	function write(){
		$mjenkeg = new model_jeniskegiatan();	
		
		$data['posts'] 		= "";	
				
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/jsFunction.js');
		
		$this->view('jeniskegiatan/edit.php', $data);
		
	}	
	
	function edit($id){
		if(  !$id ) {
			$this->redirect('module/akademik/jeniskegiatan');
			exit;
		}
		
		$mjenkeg = new model_jeniskegiatan();	
		
			$data['posts'] 		= $mjenkeg->read($id);	
			
							
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
			$this->add_script('js/jsFunction.js');
			
			$this->view('jeniskegiatan/edit.php', $data);
	}
	
	
	function save(){
			if(isset($_POST['b_jeniskegiatan'])!=""){
				$this->saveToDB();
				exit();
			}else{
				$this->index();
				exit;
			}
	}
	
	function saveToDB(){
		ob_start();
		
		$mjenkeg = new model_jeniskegiatan();	
		
		$ceknew 	= $_POST['ceknew'];
		$ketcek		= $mjenkeg->cekketjeniskegiatan($_POST['keterangan']);
		$kodecek	= $mjenkeg->cekkodejeniskegiatan($_POST['kode_kegiatan']);
		$katcek		= $mjenkeg->cekkatjeniskegiatan($_POST['kategori']);

		if($ceknew==1){
			if(isset($ketcek)||isset($kodecek)||isset($katcek)){
				$this->index('duplicate');
				exit();
			}
		}
			
						
		if($_POST['hidId']!=""){
			$jenis_kegiatan_id 	= $_POST['hidId'];
			$action 	= "update";
		}else{
			$jenis_kegiatan_id	= $mjenkeg->get_reg_number();
			$action 	= "insert";	
		}
		
		$keterangan		= $_POST['keterangan'];
		$kode_kegiatan	= $_POST['kode_kegiatan'];	
		$kategori	= $_POST['kategori'];		
		
		$datanya 	= Array(
								'jenis_kegiatan_id'=>$jenis_kegiatan_id, 
								'keterangan'=>$keterangan, 
								'kode_kegiatan'=>$kode_kegiatan,
								'kategori'=>$kategori
								);
			$mjenkeg->replace_jeniskegiatan($datanya);
			
			$this->index('ok');
			exit();
	}
	
}
?>