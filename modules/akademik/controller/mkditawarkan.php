<?php
class akademik_mkditawarkan extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mmkdtw= new model_mkditawarkan();
		$mconf = new model_conf();			
		$user = $this->coms->authenticatedUser->role;
		
		$data['pengampu']		= $mmkdtw->get_all_dosen_pengampu();
		$data['thnakademik']	= $mmkdtw->get_all_thn_akademik();
		$data['user']  			= $user;
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		$data['get_fakultas']	= $mconf->get_fakultas();
		$data['cabang']			= $mconf->get_cabangub();
		
		if(isset($_POST['thn_akademik'])){
			$thnakademik = $_POST['thn_akademik'];
			$data['thnakademikid']=$thnakademik;
		}else $thnakademik=NULL;
		
		if(isset($_POST['fakultas'])){
			$fakultas = $_POST['fakultas'];
			$data['fakultasid']=$fakultas;
		}else $fakultas=NULL;
		
		if(isset($_POST['cabang'])){
			$cabang = $_POST['cabang'];
			$data['cabangid']=$cabang;
		}else $cabang=NULL;
		
		$data['posts'] = $mmkdtw->read_by_parameter($thnakademik, $fakultas, $cabang);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/mk/mkditawarkan.js');
			
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
			case 'failed';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, icon tidak dapat tersimpan.';
			break;
			case 'toobig';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, ukuran file terlalu besar.';
			break;
			case 'wrongfiletype';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, jenis file tidak sesuai.';
				break;
		}
				
		if($user!="mahasiswa"&&$user!="dosen") {
			$this->view( 'mkditawarkan/index.php', $data );
		}
		
	}
	
	function detail($id=NULL){
		$mmkdtw= new model_mkditawarkan();			
		$user = $this->coms->authenticatedUser->role;
		
		$data['posts']=$mmkdtw->read($id);
			$data['pengampu']=$mmkdtw->get_dosen_pengampu($id);
			$data['user']=$user;
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
						
			$this->view( 'mkditawarkan/detail.php', $data );
	}
	
	//-------------------------------------------------------------
	function write(){
		$mmkdtw	= new model_mkditawarkan();	
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
			
		$data['posts']    		= "";
		$data['getthnakademik']	= $mmkdtw->get_tahun_akademik();
		$data['fakultas'] 		= $mconf->get_fakultas();
		$data['cabang']			= $mconf->get_cabangub();
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->coms->add_script('js/jquery/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/mk/mkditawarkan.js');
		
		$this->view( 'mkditawarkan/edit.php', $data );
		}
	}
	
	//-------------------------------------------------------------
	function edit($id=NULL){
		$mmkdtw	= new model_mkditawarkan();	
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts']=$mmkdtw->read($id);
		$data['getthnakademik']	= $mmkdtw->get_tahun_akademik();
		$data['fakultas'] 		= $mconf->get_fakultas();
		$data['cabang']			= $mconf->get_cabangub();
		$data['edit']			='1';
		$data['getdatamk'] 		= $mmkdtw->get_all_mk_from_mkditawarkan();
		$data['pengampu']		= $mmkdtw->get_dosen_pengampu($id);
		
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->coms->add_script('js/jquery/tagmanager.js');
		$this->coms->add_style('css/bootstrap/tagmanager.css');
		$this->add_script('js/mk/mkditawarkan.js');
		$this->add_script('js/mk/mkditawarkan_edit.js');

		$this->view( 'mkditawarkan/edit.php', $data );
		}
	}
	
	//-------------------------------------------------------------
	
	function save(){
		if($_POST['edit']!="1"){
			if(isset($_POST['b_mkditawarkan'])&&$_POST['tahun_akademik']!=""&&$_POST['fakultas']!=""){
				$this->saveToDB();
				exit();
			}else{
				$this->redirect('module/akademik/mkditawarkan/index/nok');
				exit;
			}
		}
		else {
			if(isset($_POST['b_mkditawarkan'])){
				$this->saveToDBedit();
				exit();
			}else{
				$this->redirect('module/akademik/mkditawarkan/index/nok');
				exit;
			}
		}
	}
	
	function saveToDB(){
		ob_start();
		$mmkdtw	= new model_mkditawarkan();
		$mpngm	= new model_pengampu();
		
		//-------------------cek mata kuliah and insert if there isnt any-----------------------
		
		$matakuliahid	= $mmkdtw->get_mkid_from_namamk($_POST['namamk']);
		
		if(!isset($matakuliahid)){
			$ceknamamkid = $mmkdtw->get_namamk_id_from_namamk($_POST['namamk']);
			if(!isset($ceknamamkid)){
				//-----di tbl_namamk dan tbl_matakuliah tidak ada datanya-------------------
				
				//-------------insert tbl_namamk------------------------------------
				$mmk		= new model_mk();
				
				$namamk_id	= $mmk->get_reg_number();
				$fakultasid = $mmk->get_fakultas_id($_POST['fakultas']);
				$keterangan = $_POST['namamk'];
				$engver		= "";
				
				$datanya 	= Array(
								'namamk_id'=>$namamk_id, 
								'fakultas_id'=>$fakultasid, 
								'keterangan'=>$keterangan, 
								'english_version'=>$engver
								);
				$mmk->replace_namamk($datanya);
				//-------------end of insert tbl_namamk----------------------------
				
				//-------------insert tbl_matakuliah------------------------------------
				$matakuliahid	= $mmk->get_reg_numbermk();
				$kodemk 		= "";
				$kurikulum		= "";
				$sks			= "";	
				
				$datanya 	= Array(
										'matakuliah_id'=>$matakuliahid, 
										'namamk_id'=>$namamk_id, 
										'kode_mk'=>$kodemk, 
										'kurikulum'=>$kurikulum,
										'sks'=>$sks
										);
					$mmk->replace_mk($datanya);
				//-------------end of insert tbl_matakuliah----------------------------
				
			}
			else {
				//-----di tbl_namamk ada datanya dan tbl_matakuliah tidak ada datanya-------
				$mmk		= new model_mk();
				
				//-------------insert tbl_matakuliah------------------------------------
				$matakuliahid	= $mmk->get_reg_numbermk();
				$kodemk 		= "";
				$kurikulum		= "";
				$sks			= "";	
				
				$datanya 	= Array(
										'matakuliah_id'=>$matakuliahid, 
										'namamk_id'=>$ceknamamkid, 
										'kode_mk'=>$kodemk, 
										'kurikulum'=>$kurikulum,
										'sks'=>$sks
										);
					$mmk->replace_mk($datanya);
				//-------------end of insert tbl_matakuliah----------------------------
				
			}
		}
		
		//-------------------------------------------------------------------------------------
		
		if($_POST['hidId']!=""){
			$mkditawarkan_id 	= $_POST['hidId'];
			$action 			= "update";
		}else{
			$mkditawarkan_id	= $mmkdtw->get_reg_number();	
			$action 			= "insert";	
		}
		
		if(isset($_POST['isblok'])!=""){
			$isblok	= $_POST['isblok'];
			$parent_id		= $_POST['parent_mk'];
		}else{
			$isblok	= 0;
			$parent_id		= "";
		}
		
		if(isset($_POST['iscourse'])!=""){//id is_course = 1 is_online = 0
			$isonline	= 0;
		}else{
			$isonline	= $_POST['iscourse'];
		}
		
		if(isset($_POST['ispraktikum'])!=""){
			$ispraktikum	= $_POST['ispraktikum'];
		}else{
			$ispraktikum	= 0;
		}
		
		if(isset($_POST['iskoordinator'])!=""){
			$iskoor	= $_POST['iskoordinator'];
		}else{
			$iskoor	= 0;
		}
		
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$cabangid		= $_POST['cabang'];
		$thnakademik	= $_POST['tahun_akademik'];
		
		//-----isi dosen pengampu-------------//
		$dosenkoor		= $_POST['dosenkoor'];
		$iskoor			= $_POST['iskoordinator'];
		$dosenpengampu	= $_POST['hidden-dosenpengampu'];
		$kuota			= $_POST['kuota'];
		$month 			= date('m');
		$year 			= date('Y');
		
		foreach ($_FILES['icon'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					echo $cekicon = 'error';
				}
			}
		}
		
		//------------------UPLOAD FILE START---------------------------------------------
		if($cekicon!='error'){
			foreach ($_FILES['icon'] as $id => $icon) {
				$brokeext			= $this->get_extension($icon);
				if($id == 'name'){
					$ext 	= $brokeext;
					$file	= $icon;
				}
				if($id == 'type'){
					$file_type = $_FILES['icon']['type'];
				}
				if($id == 'size'){
					$file_size = $_FILES['icon']['size'];
				}
				if($id == 'tmp_name'){
					$file_tmp_name = $_FILES['icon']['tmp_name'];
				}
			}
			$seleksi_ext 	= $mmkdtw->get_ext($ext);
			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
		
				switch(strToLower($jenis)){
					case 'image':
					$extpath = "image";
					break;
				}
			}
			else{
				$this->redirect('module/akademik/mkditawarkan/index/failed');
			}
			$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
			$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;	
			
			if (!file_exists($upload_dir)) {
				mkdir($upload_dir, 0777, true);
				chmod($upload_dir, 0777);
			}
			$file_loc = $upload_dir_db . $file;
			
			$allowed_ext = array('jpg','jpeg','png','gif');
			if(!in_array($ext,$allowed_ext)){
				$this->redirect('module/akademik/mkditawarkan/index/wrongfiletype');
			}
			if($file_size > $maxfilesize){
				$this->redirect('module/akademik/mkditawarkan/index/toobig');
			}
		}
		else {
				$file_loc	= "notinc";
		}			
		//------------------UPLOAD FILE END-----------------------------------------------------
		
		
		if(isset($mkditawarkan_id, $matakuliahid, $thnakademik, $isblok)){

			if($_SERVER['REQUEST_METHOD'] == 'POST') {
				rename($file_tmp_name, $upload_dir . $file);
			}
			//----------------insert mkditawarkan---------------------
			if($file_loc=="notinc"){
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			else {
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'icon'=>$file_loc,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			$mmkdtw->replace_mkditawarkan($datanya);
			
			//----------------end insert ditawarkan---------------------
			
			//----------------insert koor---------------------
			
			$dosenkoorstat = $mmkdtw->get_status($dosenkoor);
			$pengampuidkoor = $mpngm->get_reg_number($mkditawarkan_id, $dosenkoorstat);
			
			$datanya2 	= Array(
								'pengampu_id'=>$pengampuidkoor, 
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'karyawan_id'=>$dosenkoorstat, 
								'is_koordinator'=>$iskoor
								);
			$mpngm->replace_pengampu($datanya2);
			// echo $pengampuidkoor."<br>";
			//----------------end insert koor---------------------
			
			//---------------------insert pengampu------------------------
			foreach ($dosenpengampu as $d) {
				$dosen=explode(",",$d);
			}
			
			for($x=0;$x<count($dosen);$x++){
				$karyawanid = $mmkdtw->get_status($dosen[$x]);
				
				$id = $mpngm->get_reg_number($mkditawarkan_id, $karyawanid);
				
				$datanya3 	= Array(
							'pengampu_id'=>$id, 
							'mkditawarkan_id'=>$mkditawarkan_id, 
							'karyawan_id'=>$karyawanid, 
							'is_koordinator'=>0
							);
				// echo $id."<br>";
				$mpngm->replace_pengampu($datanya3);	
			}		
			//---------------------end insert pengampu------------------------
 			
				
			
			// $this->redirect('module/akademik/mkditawarkan/index/ok');
			// exit();
		}else{
			// $this->redirect('module/akademik/mkditawarkan/index/nok');
			// exit();
		 }
		
	}
	
	//----------------------------------------------------------------------------------------
	function saveToDBedit(){
		ob_start();
		$mmkdtw	= new model_mkditawarkan();
		$mpngm	= new model_pengampu();

		if($_POST['hidId']!=""){
			$mkditawarkan_id 	= $_POST['hidId'];
			$action 			= "update";
		}else{
			$mkditawarkan_id	= $mmkdtw->get_reg_number();	
			$action 			= "insert";	
		}
		
		$month 			= date('m');
		$year 			= date('Y');
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		$thnakademik	= $_POST['tahun_akademik'];
		$matakuliahid	= $mmkdtw->get_mkid_from_namamk($_POST['namamk']);
		$kuota			= $_POST['kuota'];
		$cabangid		= $_POST['cabang'];
		
		if(isset($_POST['isblok'])!=""){
			$isblok	= $_POST['isblok'];
			$parent_id		= $_POST['parent_mk'];
		}else{
			$isblok	= 0;
			$parent_id		= "";
		}
		
		if(isset($_POST['iscourse'])!=""){//id is_course = 1 is_online = 0
			$isonline	= 0;
		}else{
			$isonline	= $_POST['iscourse'];
		}
		
		foreach ($_FILES['icon'] as $id => $icon) {
			if($id == 'error'){
				if($icon!=0){
					$cekicon = 'error';
				}
			}
		}
		
		if($cekicon!='error'){
			//------------------UPLOAD FILE START---------------------------------------------
			foreach ($_FILES['icon'] as $id => $icon) {
				$brokeext			= $this->get_extension($icon);
				if($id == 'name'){
					$ext 	= $brokeext;
					$file	= $icon;
				}
				if($id == 'type'){
					$file_type = $_FILES['icon']['type'];
				}
				if($id == 'size'){
					$file_size = $_FILES['icon']['size'];
				}
				if($id == 'tmp_name'){
					$file_tmp_name = $_FILES['icon']['tmp_name'];
				}
			}
			$seleksi_ext 	= $mmkdtw->get_ext($ext);
			if($seleksi_ext!=NULL){
				$jenis_file_id 	= $seleksi_ext->jenis_file_id;
				$jenis			= $seleksi_ext->keterangan;
				$maxfilesize	= $seleksi_ext->max_size;
		
				switch(strToLower($jenis)){
					case 'image':
					$extpath = "image";
					break;
				}
			}
			else{
				$this->redirect('module/akademik/mkditawarkan/index/failed');
			}
			$upload_dir = 'assets/upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;		
			$upload_dir_db = 'upload/'.$year. '-' .$month. DIRECTORY_SEPARATOR .$extpath. DIRECTORY_SEPARATOR ;	
			
			if (!file_exists($upload_dir)) {
				mkdir($upload_dir, 0777, true);
				chmod($upload_dir, 0777);
			}
			$file_loc = $upload_dir_db . $file;
			
			$allowed_ext = array('jpg','jpeg','png','gif');
			if(!in_array($ext,$allowed_ext)){
				$this->redirect('module/akademik/mkditawarkan/index/wrongfiletype');
			}
			if($file_size > $maxfilesize){
				$this->redirect('module/akademik/mkditawarkan/index/toobig');
			}
			
			if($_SERVER['REQUEST_METHOD'] == 'POST') {
				rename($file_tmp_name, $upload_dir . $file);
			}			
			//------------------UPLOAD FILE END-----------------------------------------------------
		}else{
			if(!($_POST['icon_loc'])){
				$file_loc	= "notinc";
			}else {
				$file_loc	= $_POST['icon_loc'];
			}
		}
		
		
		if(isset($mkditawarkan_id, $thnakademik, $isblok)){
			
			//----------------insert mkditawarkan---------------------
			if($file_loc=="notinc"){
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			else {
				$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid,
								'cabang_id'=>$cabangid, 
								'is_blok'=>$isblok,
								'is_online'=>$isonline,
								'kuota'=>$kuota,
								'icon'=>$file_loc,
								'parent_id'=>$parent_id,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			}
			$mmkdtw->replace_mkditawarkan($datanya);
			
			//----------------end insert ditawarkan---------------------
			

			$this->redirect('module/akademik/mkditawarkan/index/ok');
			exit();
		}else{
			$this->redirect('module/akademik/mkditawarkan/index/nok');
			exit();
		 }
		
	}
	
	//----------------------------------------------------------------------------------------
	function get_extension($file_name){
		$ext = explode(".", $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	
	function tampilkan_thn()
	{
		$mmkdtw	= new model_mkditawarkan();
		$id = $mmkdtw->get_fakultas_id($_POST['fakultas_id']);
		$parent = $mmkdtw->tahun_akademik($id);
		echo "<option value='0'>Select Tahun Akademik</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->thn_id."'>".$p->tahun."</option>" ;	
		}	
	}
	
	function blok_mk()
	{
		$cek = $_POST['fakultas_id'];
		if($cek=='0'){
			echo '';
		}
		else{	
			$mmkdtw	= new model_mkditawarkan();
			$id = $mmkdtw->get_fakultas_id($_POST['fakultas_id']);
			$parent = $mmkdtw->get_mk_from_mkditawarkan($id);
			echo "<option value='0'>Select Mata Kuliah</option>" ;
			foreach($parent as $p )
			{
				echo "<option value='".$p->mkid."'>".$p->keterangan."</option>" ;	
			}	
		}
	}
	
	function delete_pengampu()
	{
		$mpengampu 		= new model_pengampu();
		$pengampuid 	= $_POST['delete_id'];
		$del			= $mpengampu->delete_pengampu($pengampuid);
		if($del==TRUE){
			echo "sukses";
		}
	}
	
	function add_pengampu()
	{
		$mpengampu 		= new model_pengampu();
		$karyawanid 	= $_POST['karyawan_id'];
		$mkid 			= $_POST['mk_id'];
		$iskoor			= 0;
		$pengampuid		= $mpengampu->get_reg_number($mkid, $karyawanid);
		
		$datanya 	= Array(
						'pengampu_id'=>$pengampuid, 
						'mkditawarkan_id'=>$mkid, 
						'karyawan_id'=>$karyawanid, 
						'is_koordinator'=>$iskoor
						);
		$mpengampu->replace_pengampu($datanya);
	}
	
	function koor_pengampu()
	{
		ob_start();
		$mpengampu 		= new model_pengampu();
		$pengampuid 	= $_POST['pengampuid'];
		$iskoor 		= $_POST['iskoor'];
		
		$mpengampu->update_koor($pengampuid, $iskoor);
		ob_end_clean();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------
	
}