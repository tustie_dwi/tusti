<?php
class akademik_tugas extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	//========TUGAS=======//
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mtugas = new model_tugas();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!="mahasiswa" && $user!="dosen"){
			$data['user']=$user;
			$data['posts'] = $mtugas->read("");	
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			
			switch($by){
				case 'ok';
					$data['status'] 	= 'OK';
					$data['statusmsg']  = 'OK, data telah diupdate.';
				break;
				case 'nok';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
				break;
			}
	
			$this->view('tugas/index.php', $data);
		}
	}
	
	function write(){
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="mahasiswa" && $user!="dosen"){
			$mtugas = new model_tugas();
			$data['user'] = $user;
			
			$data['posts'] = "";
			$data['jadwal'] = $mtugas->get_jadwal();
			$data['materi'] = $mtugas->get_materi_by_mk();
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_style('css/datepicker/datetimepicker.css');
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
			$this->add_script('js/tugas/tugas.js');
	
			$this->view( 'tugas/edit.php', $data );
		}
	}
	
	function edit($id){
		if( !$id ) {
			$this->redirect('module/akademik/tugas/');
			exit;
		}
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="mahasiswa" && $user!="dosen"){
			$mtugas = new model_tugas();
			$data['user'] = $user;
			
			$data['posts'] = $mtugas->read($id);
			$data['jadwal'] = $mtugas->get_jadwal();
			$data['materi'] = $mtugas->get_materi_by_mk();
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_style('css/datepicker/datetimepicker.css');
			$this->coms->add_script('ckeditor/ckeditor.js');
			$this->coms->add_script('js/datepicker/bootstrap-datetimepicker.js');
			$this->add_script('js/tugas/tugas.js');
	
			$this->view( 'tugas/edit.php', $data );
		}
	}
	
	function detail($id){
		if( !$id ) {
			$this->redirect('module/akademik/tugas/');
			exit;
		}
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="mahasiswa" && $user!="dosen"){
			$mtugas = new model_tugas();
			$data['user'] = $user;
			
			$data['posts'] = $mtugas->read($id);
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');
			$this->coms->add_script('js/datatables/DT_bootstrap.js');

			$this->add_script('js/tugas/tugas.js');
	
			$this->view( 'tugas/detail.php', $data );
		}
	}
	
	function tampilkan_materi(){
		$mtugas = new model_tugas();
		$jadwal_id = $_POST['jadwal_id'];
		$mk_id = $mtugas->get_mk_by_jadwal($jadwal_id);
		if($mk_id){
			foreach ($mk_id as $dt){
				$mk = $dt->mkid;
			}
			//echo $mk;
			$materi = $mtugas->get_materi_by_mk($mk);
			echo "<option value='0'>Select Materi</option>" ;
			if($materi){
				foreach($materi as $p )
				{
					echo "<option value='".$p->materi_id."'";
					echo ">".$p->judul."</option>" ;	
				}
			}
		}
	}
	
	function save(){
		if((isset($_POST['judul'])) && $_POST['judul']!=""){
			$this->saveToDB();
			exit();
		}else{
			echo "Maaf, data tidak dapat tersimpan";
			exit();
		}
	}
	
	function saveToDB(){
		ob_start();
		$mtugas = new model_tugas();
		if($_POST['hidId']!=""){
			$tugasid 		= $mtugas->get_tugas_id_by_md5($_POST['hidId']);
			$action 		= "update";
		}else{
			$tugasid		= $mtugas->tugas_id();
			$action 		= "insert";			
		}
		if(isset($_POST['materi'])){
			$materi_id	= $mtugas->get_materi_id_by_md5($_POST['materi']);
		}
		if(isset($_POST['jadwal'])){
			$jadwal_id	= $mtugas->get_jadwal_id_by_md5($_POST['jadwal']);
		}
		$judul		= $_POST['judul'];
		$instruksi	= $_POST['instruksi'];
		$keterangan	= $_POST['keterangan'];
		$tgl_mulai	= $_POST['tanggalmulai'];
		$tgl_selesai= $_POST['tanggalselesai'];
		$user		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
		$new	= $_POST['newtugas'];
		$ceknewtugas = $mtugas->cek_new_tugas_by_judul($judul);
		$ceknewtugasbyid = $mtugas->cek_id_tugas_by_id($judul);
		if(isset($judul) && isset($materi_id) && isset($jadwal_id)){
			if($new==1){
				if(!isset($ceknewtugas)){
					$datanya 	= Array('tugas_id'=>$tugasid, 
										'materi_id'=>$materi_id, 
										'jadwal_id'=>$jadwal_id, 
										'judul'=>$judul,
										'instruksi'=>$instruksi, 
										'keterangan'=>$keterangan, 
										'tgl_mulai'=> $tgl_mulai, 
										'tgl_selesai'=> $tgl_selesai, 
										'user_id'=>$user,
										'last_update'=>$lastupdate
									   );
					$mtugas->replace_tugas($datanya);
					
					echo "OK, data telah diupdate.";
					exit();
				}
				else {
					echo "Maaf, data dengan judul tugas tersebut sudah ada.";
					exit();
				}
			}
			else if(($new!=1)&&$ceknewtugasbyid==$tugasid||$ceknewtugasbyid==NULL){
					$datanya 	= Array('tugas_id'=>$tugasid, 
										'materi_id'=>$materi_id, 
										'jadwal_id'=>$jadwal_id, 
										'judul'=>$judul,
										'instruksi'=>$instruksi, 
										'keterangan'=>$keterangan, 
										'tgl_mulai'=> $tgl_mulai, 
										'tgl_selesai'=> $tgl_selesai, 
										'user_id'=>$user,
										'last_update'=>$lastupdate
									   );
					$mtugas->replace_tugas($datanya);
					
					echo "OK, data telah diupdate.";
					exit();
			}
			else{
				echo "Maaf, data dengan judul tugas tersebut sudah ada.";
				exit();
			}
		}
		else {
			echo "Maaf, data tidak dapat tersimpan.";
			exit();
		}
	}
}
?>