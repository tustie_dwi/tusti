<?php
class akademik_nilaimhs extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mnilaimhs = new model_nilaimhs();		
		$data['posts'] = $mnilaimhs->read("");	
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
		}
		
		$this->view('nilaimhs/index.php', $data);
	}
	
	function tes(){
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->add_style('css/select2.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/select2.js');
		$this->add_script('js/dropdownsearchtes.js');

		
		$this->view( 'nilaimhs/tes.php', $data );
	}

	function add($id){
		$mnilaimhs = new model_nilaimhs();	
		
		$data['posts'] 		= "";
		$data['komponen']	= $mnilaimhs->getkomponen($id);
		$data['jadwal']		= $mnilaimhs->getjadwal($id);
		$data['add']		= '1';
			
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/typeahead.js');
		$this->coms->add_script('js/typeahead.min.js');
		$this->add_script('js/nilaimhs.js');
		
		$this->view( 'nilaimhs/edit.php', $data );

	}
	
	//----------------------------------------------------------------------------------------
	
	public function edit($id){
		$mnilaimhs = new model_nilaimhs();
			
		$data['posts'] 		= $mnilaimhs->read($id);
		$data['komponen']	= $mnilaimhs->getkomponenfromnilai($id);
		$data['jadwal']		= $mnilaimhs->getjadwalfromnilai($id);
		$data['edit']		= '1';
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/typeahead.js');
		$this->coms->add_script('js/typeahead.min.js');
		$this->add_script('js/nilaimhs.js');
		
		$this->view( 'nilaimhs/edit.php', $data );
	}
	
	//----------------------------------------------------------------------------------------
	function save(){
			if(isset($_POST['b_nilaimhs'])){
				$this->saveToDB();
				exit();
			}else{
				$this->index();
				exit;
			}
	}

	function saveToDB(){
		ob_start();
		$mnilaimhs = new model_nilaimhs();	

		$hid_id = $_POST['hidId'];
		
		if($hid_id!=""){
			$nilai_id 		= $hid_id;
			$action 		= "update";
		}else{
			$nilai_id		= $mnilaimhs->get_reg_number();	
			$action 		= "insert";	
		}
		
		
		$mkditawarkanid		= $_POST['mkditawarkanid'];
		$komponen			= $_POST['komponen'];
		$nilai				= $_POST['nilai'];
		$jadwalid			= $_POST['jadwalid'];
		
		//$mhs				= $_POST['namamhs'];
		$mhsx				= explode(' - ',$_POST['namamhs']);
		foreach ($mhsx as $a){
					$mhsp[] =  $a;			
				}
		
		for($i=0;$i<1;$i++){
			$nimmhs = $mhsp[0];
			$namamhs= $mhsp[1];
		}
		
		$idmhs		= $mnilaimhs->get_idmhs($nimmhs, $namamhs);
		$krsid		= $mnilaimhs->get_krsid($mkditawarkanid, $idmhs);
		//$userid		= $mnilaimhs->get_userid($user);
		$userid		= $this->coms->authenticatedUser->id;
		$lastupdate	= date("Y-m-d H:i:s");
		
			// echo"sukses<br>";
			// echo $krsid."<br>";
			// echo $komponen."<br>";
			// echo $nilai."<br>";
			// //echo $mhs."<br>";
			// echo $userid."<br>";
			// echo $lastupdate."<br>";
		
		if(isset($nilai_id, $krsid, $komponen, $jadwalid, $nilai)){
			// echo"sukses<br>";
			// echo $krsid."<br>";
			// echo $komponen."<br>";
			// echo $nilai."<br>";
			// echo $userid."<br>";
			// echo $lastupdate."<br>";
			$datanya 	= Array(
								'nilai_id'=>$nilai_id, 
								'krs_id'=>$krsid, 
								'komponen_id'=>$komponen, 
								'jadwal_id'=>$jadwalid,
								'nilai'=>$nilai,
								'user_id'=>$userid,
								'last_update'=>$lastupdate
								);
			$mnilaimhs->replace_nilaimhs($datanya);
			
			$this->redirect('module/akademik/nilaimhs/index/ok');
			exit();
		}else{
			$this->redirect('module/akademik/nilaimhs/index/nok');
			exit();
		}
		
	}
	//----------------------------------------------------------------------------------------
}
?>