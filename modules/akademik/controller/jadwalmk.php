<?php
class akademik_jadwalmk extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mjadwal = new model_jadwalmk();
		$mconf = new model_conf();
		$user = $this->coms->authenticatedUser->role;
		
		$data['user']			= $user;
		$data['prodi']			= $mjadwal->get_prodi();
		$data['matakuliah']		= $mjadwal->get_namamk();
		$data['pengampu']		= $mjadwal->get_pengampuindex("");
		$data['kelas']			= $mjadwal->get_allkelas();
		$data['thnakademik']	= $mjadwal->get_all_thn_akademik();
		$data['get_fakultas']	= $mconf->get_fakultas();
		$data['cabang']			= $mconf->get_cabangub();
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		/*-----------------------type---------------------------------*/
		if (isset($_POST['type'])&&$_POST['type']!="0") {
			if($_POST['type']=='course'){
				$typeid 		= '0';
			}
			elseif($_POST['type']=='online'){
				$typeid 		= '1';
			}
			else {
				$typeid 		= '';
			}
			$data['type']	= $_POST['type'];
		} else {
			$typeid = null;
		}
		/*-----------------------fakultas---------------------------*/
		if (isset($_POST['fakultas'])&&$_POST['fakultas']!="0") {
			$fakultasid 	= $_POST['fakultas'];
			$data['fakultasid']=$fakultasid;
		} else {
			$fakultasid = null;
		}
		/*-----------------------thnakademik---------------------------*/
		if (isset($_POST['thnakademik'])&&$_POST['thnakademik']!="0") {
			$thnakademikid 	= $_POST['thnakademik'];
			$data['thnakademikid']=$thnakademikid;
		} else {
			$thnakademikid = null;
		}
		/*-----------------------prodi-------------------------------*/
		if (isset($_POST['prodi'])&&$_POST['prodi']!="0") {
			$prodiid = $_POST['prodi'];
			$data['prodiid']=$prodiid;
		} else {
			$prodiid = null;
		}
		/*-----------------------mk----------------------------------*/
		if (isset($_POST['mtk'])&&$_POST['mtk']!="0") {
			$mkid = $_POST['mtk'];
			$data['matakuliahid']=$mkid;
		} else {
			$mkid = null;
		}
		/*-----------------------pengampu----------------------------*/
		if (isset($_POST['pengampu'])&&$_POST['pengampu']!="0") {
			$pengampuid = $_POST['pengampu'];
			$data['pengampuid']=$pengampuid;
		} else {
			$pengampuid = null;
		}
		/*-----------------------kelas--------------------------------*/
		if (isset($_POST['kelas'])&&$_POST['kelas']!="0") {
			$kelasid 			= $_POST['kelas'];
			$data['kelasid']	= $kelasid;
		} else {
			$kelasid = null;
		}
		/*-----------------------cabang--------------------------------*/
		if (isset($_POST['cabang'])&&$_POST['cabang']!="0") {
			$cabangid 			= $_POST['cabang'];
			$data['cabangid']	= $cabangid;
		} else {
			$cabangid = null;
		}
		
		$read 	= $mjadwal->read_by_parameter($prodiid, $mkid, $pengampuid, $kelasid, $thnakademikid, $typeid, $fakultasid, $cabangid);
		/*--------------------------------------------------------------*/	
		if($read!=null){
			$data['posts'] = $read;
		}
		else {
			$data['posts'] = null;
		}
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->add_script('js/jadwalmk/jadwalmk_index.js');	
		$this->add_script('js/jadwalmk/delete.js');
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
			case 'wrongonline';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, mata kuliah hanya untuk perkuliahan.';
			break;
		}
		
		$this->view('jadwalmk/index.php', $data);
	}
	
	function write($id){
		$mjadwal 	= new model_jadwalmk();	
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 			= "";
		$data['read']			= $mjadwal->get_data($id);
		$data['datapengampu']	= $mjadwal->get_pengampu($id);
		$data['prodi']			= $mjadwal->get_prodi();	
		$data['fakultas']		= $mconf->get_fakultas();
		$data['thnakademik']	= $mjadwal->get_tahun_akademik();
		$data['write']			= '1';
		$data['cekonline']		= $mjadwal->get_isonline_from_mkditawarkan($id);
		$data['cabang']			= $mconf->get_cabangub();
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jadwalmk/jadwalmk.js');
		$this->add_script('js/jquery/jquery-ui-timepicker-addon.js');
		$this->add_script('js/jquery/timepicker.js');

		$this->view( 'jadwalmk/edit.php', $data );
		}

	}

		function writenew(){
		$mjadwal = new model_jadwalmk();	
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 			= "";
		$data['prodi']			= $mjadwal->get_prodi();	
		$data['fakultas']		= $mconf->get_fakultas();
		$data['thnakademik']	= $mjadwal->get_tahun_akademik();
		$data['write']			= '1';
		$data['cabang']			= $mconf->get_cabangub();
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jquery/jquery-ui-timepicker-addon.js');
		$this->add_script('js/jquery/timepicker.js');
		$this->add_script('js/jadwalmk/jadwalmk_addnew.js');
		
		$this->view( 'jadwalmk/writenew.php', $data );
		}

	}
	
	//----------------------------------------------------------------------------------------
	
	public function edit($id){
		$mjadwal = new model_jadwalmk();
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 			= $mjadwal->read($id);	
		$data['prodi']			= $mjadwal->get_prodi();
		$data['fakultas']		= $mconf->get_fakultas();
		$data['thnakademik']	= $mjadwal->get_tahun_akademik();
		$data['datapengampu']	= $mjadwal->get_pengampubymk($id);
		$data['edit']			= '1';
		$mkditawarkan			= $mjadwal->getmkditawarkan_byjadwalid($id);
		$data['cekonline']		= $mjadwal->get_isonline_from_mkditawarkan($mkditawarkan);
		$data['cabang']			= $mconf->get_cabangub();
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/jquery/jquery-ui-timepicker-addon.js');
		$this->add_script('js/jquery/timepicker.js');
		$this->add_script('js/jadwalmk/jadwalmk.js');
			
		$this->view( 'jadwalmk/edit.php', $data );
		}
	}

	//----------------------------------------------------------------------------------------
	
	public function detail($id)
	{
		$mjadwal = new model_jadwalmk();	
		$user = $this->coms->authenticatedUser->role;
		
		$data['posts']=$mjadwal->read($id);
		$data['user']=$user;
		$mkditawarkan			= $mjadwal->getmkditawarkan_byjadwalid($id);
		$data['cekonline']		= $mjadwal->get_isonline_from_mkditawarkan($mkditawarkan);
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
						
		$this->view( 'jadwalmk/detail.php', $data );
	}	
	
	//----------------------------------------------------------------------------------------
	
	public function delete()
	{
		$mjadwal = new model_jadwalmk();	
		// $user = $this->coms->authenticatedUser->role;
		
		 if(isset($_POST['delete_id']) && !empty($_POST['delete_id']) ) {
		 	$delete_id = $_POST['delete_id'];
			$result = $mjadwal->delete($delete_id);
			if($result==TRUE)
			{
				echo "true";
			}
		 }
	}	
	
	//----------------------------------------------------------------------------------------
	function save(){
			if(isset($_POST['b_jadwal'], $_POST['prodi'])){
				$this->saveToDB();
				exit();
			}else{
				$this->index();
				exit;
			}
	}

	function saveToDB(){
		ob_start();
		$mjadwal = new model_jadwalmk();
		$mmkdtw	= new model_mkditawarkan();
		$mmk	= new model_mk();
		$mpngm	= new model_pengampu();
		
		$hid_id 		= $_POST['hidId'];
		$user			= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		$cabang 		= $_POST['cabang'];
		
		//-------------------cek mkditawarkanid and insert if there isnt any-----------------------
		$namamk			= $mjadwal->ceknamamk_mkditawarkan($_POST['namamk']); 	//---get mkditawarkan_id
		if(!isset($namamk)){													//---cek mkditawarkan_id
			
			$cek_matakuliah = $mjadwal->ceknamamk_matakuliah($_POST['addnamamk']); //---get matakuliah_id
			if(!isset($cek_matakuliah)){										//---cek matakuliah_id
			
				$cek_namamk		= $mjadwal->ceknamamk_namamk($_POST['addnamamk']);	//---get namamk_id
				if(!isset($cek_namamk)){										//---cek namamk_id
				
				//------------------insert into namamk------------------------------
					$namamk_id	= $mmk->get_reg_number();						//---add jika data pada tabel namamk tidak ada
					$fakultas	= $mmk->get_fakultas_id($_POST['fakultas']);
					$keterangan = $_POST['addnamamk'];
					$eng_ver	= "";
					
					$datanya 	= Array(
								'namamk_id'=>$namamk_id, 
								'fakultas_id'=>$fakultas, 
								'keterangan'=>$keterangan, 
								'english_version'=>$eng_ver
								);
					$mmk->replace_namamk($datanya);
					// echo "insert to namamk";
				}
				
				//------------------insert into matakuliah------------------------------
				$matakuliah_id	= $mmk->get_reg_numbermk();	
				$namamkid		= $mjadwal->ceknamamk_namamk($_POST['addnamamk']);
				$kodemk			= "";
				$kurikulum		= "";
				$sks			= "";
				
				$datanya 	= Array(
									'matakuliah_id'=>$matakuliah_id, 
									'namamk_id'=>$namamkid, 
									'kode_mk'=>$kodemk, 
									'kurikulum'=>$kurikulum,
									'sks'=>$sks
									);
				$mmk->replace_mk($datanya);	
				// echo "insert to matakuliah";
			}
			
			
			//------------------insert into mk ditawarkan------------------------------
			$mkditawarkan_id	= $mmkdtw->get_reg_number();
			$thnakademik		= $_POST['thn_akademik'];
			$matakuliahid		= $mjadwal->ceknamamk_matakuliah($_POST['addnamamk']);
			$isblok				= "0";
			$ispraktikum		= "0";
			$parent_id			= "";
			$isonlinemk			= "1";
			$datanya 	= Array(
								'mkditawarkan_id'=>$mkditawarkan_id, 
								'tahun_akademik'=>$thnakademik, 
								'matakuliah_id'=>$matakuliahid, 
								'cabang_id'=>$cabang,
								'is_blok'=>$isblok,
								'is_online'=>$isonlinemk,
								'is_praktikum'=>$ispraktikum,
								'parent_id'=>$parent_id,
								'user_id'=>$user,
								'last_update'=>$lastupdate
								);
			$mmkdtw->replace_mkditawarkan($datanya);
			// echo "insert to mkditawarkan";
			$namamk = $mkditawarkan_id;
		}
		
		//-------------------END cek mkditawarkanid and insert if there isnt any-----------------------
		
				
		if(isset($_POST['isaktif'])!=""){
			$isaktif	= $_POST['isaktif'];
		}else{
			$isaktif	= 0;
		}
		
		/* jika is course 1 maka is online 0, jika is course 0 maka is online 1 */
		if(isset($_POST['isonline'])!=""){ //$_POST['isonline'] = is course
			$isonline	= 0;
		}else{
			$isonline	= 1;
		}
		
		if(isset($_POST['prodi'])&&$_POST['prodi']!=""){
			$prodiid			= $mjadwal->get_prodiid($_POST['prodi']);
		}else{
			$this->redirect('module/akademik/jadwalmk/index/nok');
			exit();
		}
		
		$kelas				= $_POST['kelas'];
		
		
		if($isonline==0){
			//----------------------RUANG----------------------------------------------
			$ruangx				= explode(' - ', $_POST['ruang']);
			foreach ($ruangx as $a){
				$ruangp[] = $a;
			}
			for($i=0;$i<1;$i++){
				$namaruang = $ruangp[0];
				$koderuang = $ruangp[1];
			}
			$ruang				= $mjadwal->get_kelas($namaruang, $koderuang);
			//----------------------END RUANG----------------------------------------------
			
			$hari				= $_POST['hari'];
			$jammulai			= $_POST['jammulai'];
			$jamselesai			= $_POST['jamselesai'];
			//$isonline			= $_POST['isonline'];
			//------------------------------------------------------------------------------
		}
		else{
			$cek = $mjadwal->get_is_online_by_mkditawarkan($namamk);
			if($cek=='0'){
				$this->redirect('module/akademik/jadwalmk/index/wrongonline');
				exit();
			}
			
			$ruang				= "";
			$hari				= "";
			$jammulai			= "";
			$jamselesai			= "";
		}
		
		if(isset($_POST['writenew'])){
			//------insert into pengampu----------------

			$karyawanid		= $_POST['dosen'];
			$pengampu_id	= $mpngm->get_reg_number($namamk, $karyawanid);
			$iskoor			= "";
			
			$datapengampu 	= Array(
								'pengampu_id'=>$pengampu_id, 
								'mkditawarkan_id'=>$namamk, 
								'karyawan_id'=>$karyawanid, 
								'is_koordinator'=>$iskoor
								);
			$mpngm->replace_pengampu($datapengampu);
			
			$dosenpengampu	= $mjadwal->get_pengampubydos_mk($karyawanid, $namamk);
		}
		else {
			$dosenpengampu		= $_POST['dosen'];
			$namamk				= $mjadwal->ceknamamk_mkditawarkan($_POST['namamk']);
		}
		
		if($hid_id!=""){
			$jadwal_id 	= $hid_id;
		}else{
			$jadwal_id	= $mjadwal->get_reg_number($namamk, $dosenpengampu);	
		}
		
		if(isset($jadwal_id, $prodiid, $dosenpengampu, $namamk, $kelas, $isaktif)){
			
			$datanya 	= Array(
								'jadwal_id'=>$jadwal_id, 
								'prodi_id'=>$prodiid, 
								'pengampu_id'=>$dosenpengampu,
								'mkditawarkan_id'=>$namamk,
								'cabang_id'=>$cabang,
								'kelas'=>$kelas,
								'is_aktif'=>$isaktif,
								'is_online'=>$isonline,
								'ruang_id'=>$ruang,
								'hari'=>$hari,
								'jam_mulai'=>$jammulai,
								'jam_selesai'=>$jamselesai,
								'user_id'=>$user,
								'last_update'=>$lastupdate,
								);
			$mjadwal->replace_jadwalmk($datanya);
			// echo "insert to jadwalmk";
			// $this->redirect('module/akademik/jadwalmk/index/ok');
			// exit();
		}else{
			// $this->redirect('module/akademik/jadwalmk/index/nok');
			// exit();
		}
		
	}
	//----------------------------------------------------------------------------------------
	
	function get_dosenbyfakultas()
	{
		$mjadwal = new model_jadwalmk();
		$id = $_POST['fakultas_id'];
		$parent = $mjadwal->get_pengampubyfak($id);
		echo "<option value='0'>Select Dosen Pengampu</option>" ;
		foreach($parent as $p )
		{
			echo "<option value='".$p->karyawan_id."'>".$p->nama."</option>" ;	
		}	
	}
	
	//-----------------------------------------------------------------------------------------
	
	function cekonline()
	{
		$mjadwal 		= new model_jadwalmk();
		$namamk 		= $_POST['namamk'];
		$cekonline		= $mjadwal->get_isonline_from_mk($namamk);
		if($cekonline!='0'){
			echo '';
		}
		else {
			echo $cekonline;
		}
	}
	
	function get_prodi_by_fakultas()
	{
		$mjadwal = new model_jadwalmk();
		if ($_POST['fakultas_id']!="0") {
			$fakultasid 	= $_POST['fakultas_id'];
			if (isset($_POST['prodi_id'])) {
			$prodiid = $_POST['prodi_id'];
			}
			else {
				$prodiid = "";
			}
			$parent 		= $mjadwal->get_prodi_by_fakultas($fakultasid);
			echo "<option></option>" ;
			echo "<option value='0'>Select Prodi</option>" ;
			if(count($parent)> 0) {
				foreach($parent as $p )
				{
					echo "<option value='".$p->prodi_id."' ";
					if(isset($prodiid)){
								if($prodiid==$p->prodi_id){
									echo "selected";
								}
							}
					echo ">".$p->keterangan."</option>" ;	
				}
			}	
		} else {
			echo '';
		}
	}
	
	function get_mk_by_thnakademik()
	{
		$mjadwal = new model_jadwalmk();
		if ($_POST['thnakademik_id']!="0") {
			$thnakademikid 	= $_POST['thnakademik_id'];
			$prodiid 		= $_POST['prodi_id'];
			$mkid 			= $_POST['mk_id'];
			$parent 		= $mjadwal->get_mkbythnakademik($thnakademikid, $prodiid);
			echo "<option></option>" ;
			echo "<option value='0'>Select Mata Kuliah</option>" ;
			if(count($parent)> 0) {
				foreach($parent as $p )
				{
					echo "<option value='".$p->mkditawarkan_id."' ";
					if(isset($mkid)){
								if($mkid==$p->mkditawarkan_id){
									echo "selected";
								}
							}
					echo ">".$p->namamk."</option>" ;	
				}
			}	
		} else {
			echo '';
		}
	}
	
	function get_pengampu_by_mk()
	{
		$mjadwal = new model_jadwalmk();
		if ($_POST['mk_id']!="0") {
			$id					= $_POST['mk_id'];
			$pengampuid			= $_POST['pengampu_id'];
			
			$parent = $mjadwal->get_pengampuindex($id);
			echo "<option></option>" ;
			echo "<option value='0'>Select Dosen Pengampu</option>" ;
			if(count($parent)> 0) {
				foreach($parent as $p )
				{
					echo "<option value='".$p->pengampu_id."' ";
					if(isset($pengampuid)){
						if($pengampuid==$p->pengampu_id){
							echo "selected";
							}
						}
					echo " >".$p->nama."</option>" ;	
				}
			}	
		} else {
			echo '';
		}
		
		
	}
	
	function cek_mkditawarkan()
	{
		$mjadwal = new model_jadwalmk();
		
		/*-----------------------cabang---------------------------*/
		if (isset($_POST['cabangid'])&&$_POST['cabangid']!="0") {
			$cabangid 	= $_POST['cabangid'];
		} else $cabangid = null;
		/*-----------------------fakultas---------------------------*/
		if (isset($_POST['fakultasid'])&&$_POST['fakultasid']!="0") {
			$fakultasid = $_POST['fakultasid'];
		} else $fakultasid = null;
		/*-----------------------thnakademik---------------------------*/
		if (isset($_POST['thnakademikid'])&&$_POST['thnakademikid']!="0") {
			$thnakademikid = $_POST['thnakademikid'];
		} else $thnakademikid = null;
		/*-----------------------ceknamamkval---------------------------*/
		if (isset($_POST['ceknamamkval'])&&$_POST['ceknamamkval']!="0") {
			$ceknamamkval = $_POST['ceknamamkval'];
		} else $ceknamamkval = null;
		
		$cekmkditawarkan_id = $mjadwal->get_mkditawarkan($cabangid, $fakultasid, $thnakademikid, $ceknamamkval);
		
		if($cekmkditawarkan_id){
			echo $cekmkditawarkan_id;
		}
		else echo '';
	}
	
}
?>