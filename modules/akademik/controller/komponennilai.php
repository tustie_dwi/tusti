<?php
class akademik_komponennilai extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mkomponen 	= new model_komponennilai();
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		//-----------cabang----------------------------------
		if(isset($_POST['cabang'])&&$_POST['cabang']!="0"){
			$cabang = $_POST['cabang'];
			$data['cabangid']=$cabang;
		}else $cabang=NULL;
		//-----------fakultas----------------------------------
		if(isset($_POST['fakultas'])&&$_POST['fakultas']!="0"){
			$fakultas = $_POST['fakultas'];
			$data['fakultasid']=$fakultas;
		}else $fakultas=NULL;
		//-----------thn_akademik----------------------------------
		if(isset($_POST['thn_akademik'])&&$_POST['thn_akademik']!="0"){
			$thnakademik = $_POST['thn_akademik'];
			$data['thnakademikid']=$thnakademik;
		}else $thnakademik=NULL;
		//-----------mk----------------------------------
		if(isset($_POST['select_mk'])&&$_POST['select_mk']!="0"){
			$nmk = $_POST['select_mk'];
			$data['nmkid']=$nmk;
		}else $nmk=NULL;
		
		$data['user']			= $user;
		$data['namamk']			= $mkomponen->get_all_mk($cabang, $fakultas, $thnakademik, $nmk);
		$data['get_fakultas']	= $mconf->get_fakultas();
		$data['cabang']			= $mconf->get_cabangub();
		$data['thnakademik']	= $mkomponen->get_all_thn_akademik();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/mk/komponennilai.js');	
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
		}
		
		if($user!="mahasiswa"){
		$this->view('komponennilai/index.php', $data);
		}
	}
	
	function write(){
		$mkomponen = new model_komponennilai();	
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] = "";
			
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('js/typeahead.js');
		$this->coms->add_script('js/typeahead.min.js');
		$this->add_script('js/mk/komponennilai.js');
		
		$this->view( 'komponennilai/edit.php', $data );
		}

	}
	
	function addfrommk($id=NULL){
		$mkomponen = new model_komponennilai();	
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 		= "";
		$data['namamk']		= $mkomponen->get_namamk($id);
		$data['cek']		= "1";
		$data['datakomponen']		= $mkomponen->get_all_mk("", "", "", $id);
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/mk/komponennilai.js');
		
		$this->view( 'komponennilai/edit.php', $data );
		}

	}
	
	function add($id){
		$mkomponen 			= new model_komponennilai();	
		$mmkdtw				= new model_mkditawarkan();	
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] 		= "";
		$data['datanilai'] 	= $mkomponen->read($id, "", "", "", "", "", "");	
		$data['komponen']	= $mkomponen->get_komponen();	
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		$this->add_script('js/mk/komponennilai.js');
		$this->view( 'komponennilai/add.php', $data );
		}

	}
	//----------------------------------------------------------------------------------------
	
	public function edit($id){
		$mkomponen = new model_komponennilai();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
			
		$cek 	   = $mkomponen->cek_parent($id);
		
		if(!$cek){
			$data['posts'] = $mkomponen->read($id, "", "", "", "", "", "");	
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
			$this->add_script('js/mk/komponennilai.js');
			
			$this->view( 'komponennilai/edit.php', $data );
		}
		else {
			$mkomponen = new model_komponennilai();	
			$data['posts'] = $mkomponen->read($id, "", "", "", "", "", "");	
			$data['komponen']	= $mkomponen->get_komponen();
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
			$this->add_script('js/mk/komponennilai.js');
			
			$this->view( 'komponennilai/add.php', $data );
		}
		}
	}
	
	//----------------------------------------------------------------------------------------
	function save(){
			if(isset($_POST['b_komponen'])){
				$this->saveToDB();
				exit();
			}else{
				$this->index();
				exit;
			}
	}

	function saveToDB(){
		ob_start();
		$mkomponen = new model_komponennilai();	

		$hid_id = $_POST['hidId'];
		
		if($hid_id!=""){
			$komponen_id 	= $hid_id;
			$action 		= "update";
		}else{
			$komponen_id	= $mkomponen->get_reg_number();	
			$action 		= "insert";	
		}
		
		$mkditawarkanid		= $mkomponen->cek_namamk($_POST['namamk']);
		$bobot				= $_POST['bobot'];
		$keterangan			= $_POST['keterangan'];
		$parent_id			= "";
		
		if(isset($bobot, $keterangan, $mkditawarkanid)){
			$datanya 	= Array(
								'komponen_id'=>$komponen_id, 
								'mkditawarkan_id'=>$mkditawarkanid, 
								'keterangan'=>$keterangan, 
								'bobot'=>$bobot,
								'parent_id'=>$parent_id
								);
			$mkomponen->replace_komponennilai($datanya);
			
			$this->redirect('module/akademik/komponennilai/index/ok');
			exit();
		}else{
			$this->redirect('module/akademik/komponennilai/index/nok');
			exit();
		}
		
	}
	//----------------------------------------------------------------------------------------
	
	//----------------------------------------------------------------------------------------
	function savetambahan(){
			if(isset($_POST['b_komponen'])){
				$this->saveToDBtambahan();
				exit();
			}else{
				$this->index();
				exit;
			}
	}

	function saveToDBtambahan(){
		ob_start();
		$mkomponen = new model_komponennilai();	

		$hid_id = $_POST['hidId'];
		
		if($hid_id!=""){
			$komponen_id 	= $hid_id;
			$action 		= "update";
		}else{
			$komponen_id	= $mkomponen->get_reg_number();	
			$action 		= "insert";	
		}
		
		$mkditawarkanid		= $mkomponen->cek_namamk($_POST['namamk']);
		//$namamk				= $_POST['namamk'];
		$bobot				= $_POST['bobot'];
		$keterangan			= $_POST['keterangan'];
		$parent_id			= $_POST['parent'];
		
		if(isset($bobot, $keterangan, $mkditawarkanid)){
			$datanya 	= Array(
								'komponen_id'=>$komponen_id, 
								'mkditawarkan_id'=>$mkditawarkanid, 
								'keterangan'=>$keterangan, 
								'bobot'=>$bobot,
								'parent_id'=>$parent_id
								);
			$mkomponen->replace_komponennilai($datanya);
			
			$this->redirect('module/akademik/komponennilai/index/ok');
			exit();
		}else{
			$this->redirect('module/akademik/komponennilai/index/nok');
			exit();
		}
		
	}
	//----------------------------------------------------------------------------------------
	
	function komponenparent($mkditawarkan_id=NULL, $cabang=NULL, $fakultas=NULL, $thnakademik=NULL, $nmkid=NULL){
		$mkomponen 	= new model_komponennilai();
		
		$data['posts']	= $mkomponen->read("", "parent", "", $cabang, $fakultas, $thnakademik, $nmkid);
		$data['mkditawarkan_id'] = $mkditawarkan_id;
		$data['cabang'] = $cabang;
		$data['fakultas'] = $fakultas;
		$data['thnakademik'] = $thnakademik;
		$data['nmkid'] = $nmkid;
		
		$this->view('komponennilai/parent.php', $data);
	}
	
	function komponenchild($komponenid=NULL, $cabang=NULL, $fakultas=NULL, $thnakademik=NULL, $nmkid=NULL){
		$mkomponen 	= new model_komponennilai();
		
		$data['child']			= $mkomponen->read("", "", "child", $cabang, $fakultas, $thnakademik, $nmkid);
		$data['komponenid'] = $komponenid;
		$data['cabang'] = $cabang;
		$data['fakultas'] = $fakultas;
		$data['thnakademik'] = $thnakademik;
		$data['nmkid'] = $nmkid;
		
		$this->view('komponennilai/child.php', $data);	
	}
	
	function get_nama_mk(){
		$mkomponen 	= new model_komponennilai();
		
		//-----------cabang----------------------------------
		if(isset($_POST['cabang'])&&$_POST['cabang']!="0"){
			$cabang = $_POST['cabang'];
			$data['cabangid']=$cabang;
		}else $cabang=NULL;
		//-----------fakultas----------------------------------
		if(isset($_POST['fakultas_id'])&&$_POST['fakultas_id']!="0"){
			$fakultas = $_POST['fakultas_id'];
		}else $fakultas=NULL;
		//-----------thn_akademik----------------------------------
		if(isset($_POST['thnid'])&&$_POST['thnid']!="0"){
			$thnakademik = $_POST['thnid'];
		}else $thnakademik=NULL;
		//-----------mkid----------------------------------
		if(isset($_POST['mkid'])&&$_POST['mkid']!="0"){
			$mkid = $_POST['mkid'];
		}
		
		$mk			= $mkomponen->get_all_mk($cabang, $fakultas, $thnakademik, "");
		
		echo "<option></option>" ;
		echo "<option value='0'>Select Mata Kuliah</option>" ;
		if(count($mk)> 0) {
			foreach($mk as $p )
			{
				echo "<option value='".$p->mkid."' ";
				if(isset($mkid)){
					if($mkid==$p->mkid){
						echo "selected";
						}
					}
				echo " >".$p->namamk."</option>" ;	
			}
		}
	
	}
}
?>