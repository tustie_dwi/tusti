<?php
class akademik_kuesioner extends comsmodule {

	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		// this controller requires authentication
		// initialize it
		$coms->require_auth('auth'); 
	}
	function daftar($id=NULL, $str=NULL){
		$mconf 	 = new model_confinfo();
		
		$data['posts'] = $mconf->get_mk_pilihan_by_mhs($id);
		$data['mconf'] = $mconf; 
		$data['kuesioner'] = $id;
		
		
		if($str){
			switch($str){
				case 'report':
					$this->report_mk($id);
				break;
				case 'peminat':
					$this->get_peminat_mk_pilihan($id);
				break;
				case 'rpeminat':
					$this->report_peminat_mk_pilihan($id);
				break;
			}
			
		}else{
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		
			$this->view( 'kuesioner/index_mhs.php', $data );
		}
	}	
	
	function report_peminat_mk_pilihan($id=NULL){
		$mconf 	 = new model_confinfo();
		
		$data['cols']	= $mconf->get_peminat_mk_pilihan($id,1);
		$data['post']	= $mconf->get_peminat_mk_pilihan($id);
				
		$this->add_script('js/jsread.js');
		
		$this->view( 'daftar/report.php', $data );
	}
	
	function get_peminat_mk_pilihan($id=NULL){
		$mconf 	 = new model_confinfo();
		
		$data['posts']	= $mconf->get_peminat_mk_pilihan($id);
		$data['kuesioner'] = $id;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		$this->view( 'kuesioner/peminat.php', $data );
		
	}
	
	
	function report_mk($id=NULL){
		$mdaftar = new model_confinfo();
		
		$data['cols'] = $mdaftar->read_report_mk('1');
		$data['post'] = $mdaftar->read_report_mk('', $id);
		
		$this->add_script('js/jsread.js');
		
		$this->view( 'daftar/report.php', $data );
	}
	
	
	function index($str=NULL){
		$mconf = new model_conf();	
		
		$data['prodi']		= $mconf->get_prodi();
		$data['mk']			= $mconf->get_mk();
		$data['mconf']		= $mconf;
		$data['kuesioner']	= $mconf->read_kuesioner_mk();
		$data['posts'] ="";
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->add_script('js/akademik.js');
		
		$this->view('kuesioner/index.php', $data);
	}
	
	function edit($str=NULL){
		$mconf = new model_conf();	
		
		$data['prodi']		= $mconf->get_prodi();
		$data['mk']			= $mconf->get_mk();
		$data['mconf']		= $mconf;
		$data['posts']		= $mconf->read_kuesioner_mk($str);
		$data['kuesioner']	= $mconf->read_kuesioner_mk();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_script('js/jquery-ui-1.8.16.custom.min.js');
		
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('select/select2.js');
		
		$this->add_script('js/akademik.js');
		
		$this->view('kuesioner/index.php', $data);
	}
	
	function save(){
		$mconf = new model_conf();
		
		ob_start();
				
		$tahun		= $_POST['tahun'];
		$semester	= $_POST['semester'];
		$judul		= $_POST['judul'];
		
		$catatan	= $_POST['catatan'];
		$mulai		= $_POST['tmulai'];
		$selesai	= $_POST['tselesai'];
		
		$kuesionerid = $mconf->get_reg_kuesioner($_POST['hidid']);
		
		if(isset($_POST['chkaktif'])){
			$isaktif = 1;
		}else{
			$isaktif = 0;
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user	 	= $this->coms->authenticatedUser->username;
		
		if($semester=='ganjil'){
			$kodesemester = "01";
		}else{
			$kodesemester = "02";
		}
		
		if(isset($_POST['pendek'])){
			$kode		= $tahun.$kodesemester.'01';
			$pendek		= $_POST['pendek'];
		}else{
			$kode		= $tahun.$kodesemester.'00';
			$pendek		= "";
		}
		
			
		if(isset($_POST['hidprodi'])){
			$idnya			= array('is_aktif'=>1);
			$dataupdate		= array('is_aktif'=>0);
					
			$mconf->update_kuesioner_mk($dataupdate,$idnya);
		
			$datadel = array('kuesioner_id'=>$kuesionerid);
			$mconf->delete_kuesioner_detail($datadel);
			
			$datanya = array('kuesioner_id'=>$kuesionerid, 'judul'=>$judul, 'keterangan'=>$catatan, 'tgl_mulai'=>$mulai, 'tgl_selesai'=>$selesai, 'tahun_akademik'=>$kode, 
						 'tahun'=>$tahun, 'is_ganjil'=>$semester, 'is_pendek'=>$pendek, 'is_aktif'=>$isaktif, 'user_id'=>$user, 'last_update'=>$lastupdate);
			$mconf->insert_kuesioner_mk($datanya);		
		
			
			for($i=0;$i<count($_POST['hidprodi']);$i++){
				$prodi = $_POST['hidprodi'][$i];
				
				if(isset($_POST['cmbmk'.$prodi])){
					$cmbmk = $_POST['cmbmk'.$prodi];
					
				
					for ($j=0;$j<count($cmbmk);$j++){	
						$namamk = $mconf->get_nama_mk_by_id($cmbmk[$j]);
						$detailid = $mconf->get_reg_kuesioner_detail($cmbmk[$j],$kuesionerid, $prodi);
						
						$nama_mk = $namamk->nama_mk;
						$kode_mk = $namamk->kode_mk;
						$sks	 = $namamk->sks;
						
						$datamk = array('kuesioner_id'=>$kuesionerid,'detail_id'=>$detailid, 'prodi_id'=>$prodi, 'matakuliah_id'=>$cmbmk[$j], 'nama_mk'=>$nama_mk, 'kode_mk'=>$kode_mk, 'sks'=>$sks);
						$mconf->insert_kuesioner_detail($datamk);
					}
				}
			}
		}
							
		$this->index();			
		exit();
	}
}

