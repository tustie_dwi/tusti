<?php
class akademik_mk extends comsmodule {
	private $coms;
	
	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
		
	function indexmk($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mmk= new model_mk();			
		
		$data['posts']=$mmk->read_namamk("");
		$user = $this->coms->authenticatedUser->role;
		$data['user']=$user;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
			case 'duplicate';
				$data['status'] 	= 'Duplicate Data';
				$data['statusmsg']  = 'Maaf, Nama mata kuliah sudah ada.';
			break;
		}
		if($user!="mahasiswa"){
		$this->view( 'mk/index.php', $data );
		}
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$mmk= new model_mk();			
		
		$data['posts']=$mmk->read_matakuliah("");
		$user = $this->coms->authenticatedUser->role;
		$data['user']=$user;
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
			case 'duplicate';
				$data['status'] 	= 'Duplicate Data';
				$data['statusmsg']  = 'Maaf, Nama mata kuliah sudah ada.';
			break;
		}
		
		if($user!="mahasiswa"){
		$this->view( 'mk/indexmatakuliah.php', $data );
		}
	}
	
	function write(){
		$mmk		= new model_mk();	
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts']    = "";
		$data['fakultas'] = $mconf->get_fakultas();
		

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/mk/namamk.js');
		
		$this->view( 'mk/edit.php', $data );
		}
	}
	
	function writemk(){
		$mmk		= new model_mk();
		$mconf		= new model_conf();
		
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts']	= "";
		$data['getnamamk']	=$mmk->read_namamk("");
		$data['fakultas'] = $mconf->get_fakultas();
		$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/mk/namamk.js');

		$this->view( 'mk/editmatakuliah.php', $data );
		}
	}
	
	//---------------------------------------------------------------------------------
	function edit($id=NULL){
		$mmk= new model_mk();
		$mconf		= new model_conf();
			
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] = $mmk->read_namamk($id);
		$data['fakultas'] = $mconf->get_fakultas();
		

		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/mk/namamk.js');
		
		$this->view( 'mk/edit.php', $data );
		}
	}
	
	function editmk($id=NULL){
		$mmk= new model_mk();
		$mconf		= new model_conf();
			
		$user = $this->coms->authenticatedUser->role;
		if($user!='mahasiswa'&&$user!='dosen'){
		
		$data['posts'] = $mmk->read_matakuliah($id);
		$data['getnamamk']	=$mmk->read_namamk("");
		$data['fakultas'] = $mconf->get_fakultas();
		//$data['fakultasid'] 	= $this->coms->authenticatedUser->fakultas;
		
		$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
		$this->coms->add_style('css/bootstrap/token-input.css');
		$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->coms->add_script('js/jquery/jquery.tokeninput.js');
		$this->add_script('js/mk/namamk.js');
		
		$this->view( 'mk/editmatakuliah.php', $data );
		}
	}
	
	//---------------------------------------------------------------------------------
	function save(){
		if(isset($_POST['b_namamk'])!=""&&$_POST['fakultas']!=""){
			$this->saveToDB();
			exit();
		}else{
			$this->index();
			exit;
		}
	}
	
	function saveToDB(){
		ob_start();
		$mmk	= new model_mk();
		
		$hid_id = $_POST['hidId'];
		$ceknew = $_POST['ceknew'];
		$idcek	= $mmk->cek_nama_mk($_POST['namamk']);
		if(isset($idcek)&&$ceknew==1){
			$this->index('duplicate');
			exit();
		}
		
		if($hid_id!=""){
			$namamk_id 	= $hid_id;
			$action 	= "update";
		}else{
			$namamk_id	= $mmk->get_reg_number();	
			$action 	= "insert";	
		}
		
		$user		= $this->coms->authenticatedUser->username;
		
		$fakultas	= $mmk->get_fakultas_id($_POST['fakultas']);
		$keterangan	= $_POST['namamk'];
		$eng_ver	= $_POST['namamkeng'];
		
		if(isset($fakultas, $keterangan, $eng_ver)){
			$datanya 	= Array(
								'namamk_id'=>$namamk_id, 
								'fakultas_id'=>$fakultas, 
								'keterangan'=>$keterangan, 
								'english_version'=>$eng_ver
								);
			$mmk->replace_namamk($datanya);
			
			$this->redirect('module/akademik/mk/indexmk/ok');
			exit();
		}else{
			$this->redirect('module/akademik/mk/indexmk/nok');
			exit();
		}
		
	}
	//---------------------------------------------------------------------------------
	
	function savemk(){
			if(isset($_POST['b_mk'])){
				$this->saveToDBmk();
				exit();
			}else{
				$this->indexmk();
				exit;
			}
	}
	
	function saveToDBmk(){
		ob_start();
		$mmk	= new model_mk();
		
		$hid_id 	= $_POST['hidId'];
		$kodemk		= $_POST['kodemk'];
		$kurikulum	= $_POST['kurikulum'];
		$ceknew 	= $_POST['ceknew'];
		$namamk		= $mmk->cek_mk_from_namamk($_POST['namamk']);
		$kodemk		= $_POST['kodemk'];
		$cekkodebymk= $mmk->cek_kode_mk_by_mkid($hid_id);
		$eng_ver	= $_POST['namamkeng'];
		
		$kodemkcek		= $mmk->cek_kode_mk($_POST['kodemk']);		
		$idcek			= $mmk->cek_nama_mk($_POST['namamk']);
		$idmkcek		= $mmk->cek_mata_kuliah_id($_POST['namamk']);
		$kurikulumcek	= $mmk->cek_kurikulum_mk($_POST['kurikulum']);
		
		$user			= $this->coms->authenticatedUser->id;
		$lastupdate		= date("Y-m-d H:i:s");
		
		if($ceknew==1){
			if(isset($idcek)||isset($kodemkcek)){
				$this->redirect('module/akademik/mk/indexmk/duplicate');
				exit();
			}
		}
		elseif (isset($idmkcek)&&$idmkcek!=$hid_id||isset($kodemkcek)&&$kodemkcek!=$cekkodebymk) {
			$this->redirect('module/akademik/mk/indexmk/duplicate');
			exit();
		}


		if($_POST['hidId']!=""){
			$matakuliah_id 	= $hid_id;
			$namamkid		= $_POST['namamkid'];
			$action 	= "update";
		}else{
			$matakuliah_id	= $mmk->get_reg_numbermk();	
			$action 	= "insert";	
		}
		
		$fakultas	= $mmk->get_fakultas_id($_POST['fakultas']);
		$sks		= $_POST['sks'];
			
		if($action=="update"){
			if(isset($kodemk, $kurikulum, $sks)){
			
			$datanya 	= Array(
								'namamk_id'=>$namamkid,
								'fakultas_id'=>$fakultas, 
								'keterangan'=>$_POST['namamk'], 
								'english_version'=>$eng_ver
								);
			$mmk->replace_namamk($datanya);
			
			$datanya 	= Array(
								'matakuliah_id'=>$matakuliah_id, 
								'namamk_id'=>$namamkid, 
								'kode_mk'=>$kodemk, 
								'kurikulum'=>$kurikulum,
								'sks'=>$sks
								);
			$mmk->replace_mk($datanya);
			$this->redirect('module/akademik/mk/index/ok');
			exit();
			}
			else{
			$this->redirect('module/akademik/mk/index/nok');
			exit();
			}
		}
		else{
			if(isset($kodemk, $kurikulum, $sks, $fakultas)){
				$namamk	= $mmk->get_reg_number();
				$keterangan	= $_POST['namamk'];

				
				//--------------------------insert ke tabel namamk-------------------------------	
		
				$datanya 	= Array(
								'namamk_id'=>$namamk, 
								'fakultas_id'=>$fakultas, 
								'keterangan'=>$keterangan, 
								'english_version'=>$eng_ver
								);
				$mmk->replace_namamk($datanya);
				
				//--------------------------END insert ke tabel namamk-------------------------------
				
				//--------------------------insert ke tabel mata kuliah-------------------------------
				
				$datanya 	= Array(
									'matakuliah_id'=>$matakuliah_id, 
									'namamk_id'=>$namamk, 
									'kode_mk'=>$kodemk, 
									'kurikulum'=>$kurikulum,
									'sks'=>$sks
									);
				$mmk->replace_mk($datanya);
				
				//--------------------------END insert ke tabel mata kuliah-------------------------------
				
				$this->redirect('module/akademik/mk/index/ok');
				exit();
				}
			else{
				$this->redirect('module/akademik/mk/index/nok');
				exit();
				}
			}
	}
	//---------------------------------------------------------------------------------
}
?>