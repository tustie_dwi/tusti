<?php
class akademik_pembimbing extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($id =NULL){
		$mpemb = new model_pembimbing();
		$mconf = new model_conf();
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="mahasiswa" && $user!="dosen"){
			$data['user'] = $user;
			$data['posts'] = $mpemb->read();
			if($id!=""){
				$data['edit'] = $mpemb->read($id);
			}
			$data['get_dosen'] = $mpemb->get_dosen('','');
			$data['get_fakultas'] = $mconf->get_fakultas();
			$data['get_thn_akademik'] = $mpemb->get_thn_akademik();
			
			$this->coms->add_style('css/custom-theme/jquery-ui-1.8.16.custom.css');
			$this->coms->add_script('js/jquery/jquery-ui-1.8.16.custom.min.js');
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			$this->coms->add_style('css/bootstrap/token-input.css');
			$this->coms->add_script('js/jquery/jquery.tokeninput.js');
			$this->coms->add_script('js/jquery/tagmanager.js');
			$this->coms->add_style('css/bootstrap/tagmanager.css');	
			$this->add_script('js/delete.js');
			$this->add_script('js/pembimbing.js');
			$this->coms->add_script('js/submit.js');
	
			$this->view('pembimbing/index.php', $data);
		}
	}
	
	function tampilkan_dosen(){
		$mpemb = new model_pembimbing();
		$id = $_POST['fakultas_id'];
		$dosen = $mpemb->get_dosen($id);
		echo "<option value='0'>Select Dosen Pembimbing</option>" ;
		if(isset($dosen)){
			foreach($dosen as $p )
			{
				echo "<option value='".$p->kar_id."'>".$p->nama."</option>" ;	
			}
		}	
	}
	
	function tampilkan_mhs(){
		$mpemb = new model_pembimbing();
		$id = $_POST['fakultas_id'];
		$mhs = $mpemb->get_mhs_by_fakultas($id);
		if(isset($mhs)){
			foreach($mhs as $p )
			{
				echo "<option value='".$p->hid_id."'>".$p->mhs."</option>" ;	
			}
		}
	}

	function save(){
		if((isset($_POST['hidden-mhs']) && $_POST['hidden-mhs']!=null) || (isset($_POST['mhs_edit']))){
			$this->saveToDB();
			exit();
			
		}else{
			echo "Proses penambahan gagal,ulangi kembali!";
			exit;
		}
	}

	function savetoDB(){
		ob_start();
		$mpemb = new model_pembimbing();
		
		if(isset($_POST['mhs_edit'])){ //edit
			$mahasiswa_id		= $_POST['mhs_edit'];
			$mhs_id				= $mahasiswa_id;
			$nama				= $mpemb->get_mhs_nama($mhs_id);
		    $karyawan_id 		= $mpemb->get_dosen("",$_POST['dosen']);
			$thn_id				= $_POST['select_thn'];
			$cekbyid			= $mpemb->cekbyid($mhs_id);
			//echo $karyawan_id;
			if(($mhs_id!='')){
				$datanya 	= Array('mahasiswa_id'=>$mhs_id, 'karyawan_id'=>$karyawan_id, 'tahun_akademik'=> $thn_id);
				$mpemb->replace_pembimbing($datanya);
				
				echo "Berhasil mengupdate pembimbing akademik untuk ".$nama."!";
			}else{
				echo "Gagal mengupdate pembimbing akademik!";
			}
		}
		else { //new
			$mahasiswa		= $_POST['hidden-mhs'];
			$mhs			= explode(",",$mahasiswa);
			//echo count($mhs);
			for($i=0;$i<count($mhs);$i++) {
				$id					= explode(" - ",$mhs[$i]);
				$mhs_id				= $id[0];
				$cek_mhs_id			= $mpemb->cek_by_id_before_save($mhs_id);
				//echo $cek_mhs_id;
				if(isset($cek_mhs_id)){
					$nama				= $mpemb->get_mhs_nama($mhs_id);
				    $karyawan_id 		= $mpemb->get_dosen("",$_POST['dosen']);
					$thn_id				= $_POST['select_thn'];
					$new				= $_POST['newjen'];
					$cekbyid			= $mpemb->cekbyid($mhs_id);
						
					if(($mhs_id!='')){
						if($new==1){
							if(!isset($cekbyid)){
								$datanya 	= Array('mahasiswa_id'=>$mhs_id, 'karyawan_id'=>$karyawan_id, 'tahun_akademik'=> $thn_id);
								$mpemb->replace_pembimbing($datanya);
									
								echo "Berhasil menambahkan pembimbing akademik untuk ".$nama."!\n";
							}
							else{
								echo "Data dengan nama ".$nama." sudah ada!\n";
							}
						}
					}else{
						echo "Gagal menambahkan pembimbing akademik!\n";
					}
				}
				else{
					echo "Mahasiswa tersebut tidak terdaftar!\n";
				}
			}
		}
		exit();
	}
	
	function delete(){
		$mpemb = new model_pembimbing();
		$id = $_POST['delete_id'];
		$del_pembimbing = $mpemb->delete($id);
		
		if($del_pembimbing){
			echo "Data telah berhasil terhapus!!";
		}
	}
}