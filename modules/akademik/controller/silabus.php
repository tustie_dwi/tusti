<?php
class akademik_silabus extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all', $keyword = NULL, $page = 1, $perpage = 500){
		$user = $this->coms->authenticatedUser->role;
		$fakultas = $this->coms->authenticatedUser->fakultas;
		if($user!="mahasiswa" && $user!="dosen"){
			$msilabus = new model_silabus();
			$mconf = new model_conf();
			
			$data['fakultas_id'] = $fakultas;
			$data['cabang'] = $mconf->get_cabangub();
			
			
			$data['posts'] = $msilabus->read('');
			$data['komponen'] = $msilabus->get_komponen_aktif();
			$data['fakultas'] = $mconf->get_fakultas();
			$data['mkditawarkan'] = $msilabus->mkditawarkan();
			$data['user'] = $user;
			$data['thnedit'] = $msilabus->tahun_edit();
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');	
	
			$this->add_script('js/mk/silabus.js');
			$this->coms->add_script('js/submit.js');
			switch($by){
				case 'ok';
					$data['status'] 	= 'OK';
					$data['statusmsg']  = 'OK, data telah diupdate.';
				break;
				case 'duplicate';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data dengan nama silabus tersebut sudah ada';
				break;
				case 'nok';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
				break;
			}
				
			$this->view('silabus/index.php', $data);
		}
	}
	
	function writesilabus(){
		
		
		
		$user = $this->coms->authenticatedUser->role;
		if($user!="Mahasiswa" && $user!="Dosen"){
		$msilabus = new model_silabus();
		$mconf = new model_conf();
		$data['user'] = $user;
		
		$data['komponen'] = $msilabus->get_komponen_aktif();
		$data['fakultas'] = $mconf->get_fakultas();
		$data['mkditawarkan'] = $msilabus->mkditawarkan();
		$data['thn'] = $msilabus->tahun_akademik('');
		$data['posts'] = "";
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');	

		$this->add_script('js/mk/silabus.js');
		$this->coms->add_script('js/submit.js');

		$this->view( 'silabus/tambahsilabus.php', $data );
		}
	}
	
	function editsilabus($id){
		if( !$id ) {
			$this->redirect('module/akademik/silabus/');
			exit;
		}
		
		$user = $this->coms->authenticatedUser->role;
		if($user!="Mahasiswa" && $user!="Dosen"){
			$msilabus = new model_silabus();
			$mconf = new model_conf();
			$data['user'] = $user;
			$data['posts'] = $msilabus->read($id);
			$data['komponen'] = $msilabus->get_komponen_aktif();
			$data['fakultas'] = $mconf->get_fakultas();
			$data['mkditawarkan'] = $msilabus->mkditawarkan();
			$data['thnedit'] = $msilabus->tahun_edit();
		
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
			$this->coms->add_script('select/select2.js');
			$this->coms->add_style('select/select2.css');
			
							
			$this->add_script('js/mk/silabus.js');
			$this->coms->add_script('js/submit.js');
			
			$this->view( 'silabus/tambahsilabus.php', $data );
		}
	}

	function clean(){
			$msilabus = new model_silabus();
			$msilabus->hapus();
		}
	
	function savesilabus(){
	
		if(isset($_POST['silabus'])&&$_POST['silabus']!=""){
			$this->savesilabusToDB();
			exit();
		}else{
			$this->index();
			exit;
		}
		
		echo "tes";
	
	}
	
	function savesilabusToDB(){
		ob_start();
		
		$msilabus = new model_silabus();
					
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
		$mkditawarkan_id	= $_POST['mkditawarkan'];
		$ceksil = $msilabus->cek_sil_id($mkditawarkan_id);
			
		if(($_POST['hidsilId']!="")&&($_POST['hiddetId']!="")){
			$silabusid 		= $_POST['hidsilId'];
			$silabusdetid 	= $_POST['hiddetId'];
			$action 		= "update";
		}else{
			if(!isset($ceksil)){
			$silabusid			= $msilabus->id_silabus();
			}
			else{
				foreach ($ceksil as $p) {
					$silid=$p->silabus_id;
				}
				$silabusid = $silid;
			}
			$silabusdetid	= $msilabus->id_silabus_detail();
			$action 			= "insert";			
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;
		//$prodi		= $_POST['cmbprodi'];
		
		$komp_id	= $_POST['cmbkomponen'];
		$keterangan	= $_POST['silabus'];
		$new	= $_POST['new'];
		$ceknew = $msilabus->cek_new($keterangan);
		$ceknewsil = $msilabus->cek_new_sil($komp_id,$silabusid);
		$ceknewbyid = $msilabus->cek_new_by_id($keterangan);
		
		if(($keterangan!='')){
			if($new==1){
				if(!isset($ceknewsil)){
					// echo "mk ".$mkditawarkan_id."<br>";
					// echo "kom_id ".$komp_id."<br>";
					// echo "ket ".$keterangan."<br>";
					// echo "sil_id ".$silabusid."<br>";
					// echo "detid ".$silabusdetid."<br>";
					$datanyasil 	= Array('silabus_id'=>$silabusid, 'mkditawarkan_id'=>$mkditawarkan_id);
					$msilabus->replace_silabus($datanyasil);
					
					$datanyadet 	= Array('detail_id'=>$silabusdetid, 'silabus_id'=>$silabusid,
									 'komponen_id'=>$komp_id, 'keterangan'=>$keterangan, 'user_id'=>$user, 'last_update'=>$lastupdate);
					$msilabus->replace_detail($datanyadet);
					
					
					$this->redirect('module/akademik/silabus/index/ok');
					exit();
				}
				else{
					$this->redirect('module/akademik/silabus/index/duplicate');
					exit();
				}
			}
			elseif(($new!=1)&&$ceknewbyid==$silabusdetid&&(!isset($ceknewsil)) || $ceknewbyid==NULL ){
				$datanyasil 	= Array('silabus_id'=>$silabusid, 'mkditawarkan_id'=>$mkditawarkan_id);
				$msilabus->replace_silabus($datanyasil);
				$datanyadet 	= Array('detail_id'=>$silabusdetid, 'silabus_id'=>$silabusid,
								 'komponen_id'=>$komp_id, 'keterangan'=>$keterangan, 'user_id'=>$user, 'last_update'=>$lastupdate);
				$msilabus->replace_detail($datanyadet);
				
				
				$this->redirect('module/akademik/silabus/index/ok');
				exit();
			}
			else{
				$this->redirect('module/akademik/silabus/index/duplicate');
				exit();
			}
			
		}else{
			$this->redirect('module/akademik/silabus/index/nok');
			exit();
		}
	}
	
	function tampilkan_thn()
	{
		$msilabus = new model_silabus();
		$parent = $msilabus->tahun_akademik();
		// echo $parent;
		if(isset($parent)){
			echo "<option value='0'>Select Tahun Akademik</option>" ;
			foreach($parent as $p )
			{
				echo "<option value='".$p->thn_id."'>".$p->tahun."</option>" ;	
			}	
		}
	}
	
	function tampilkan_mk()
	{
		$msilabus	= new model_silabus();
		$id 	= $_POST['hid_id'];
		$thn_id 	= $_POST['thn_id'];
		$parent = $msilabus->tampilkan_mk($id,$thn_id);
		
		if(isset($parent)){
			echo "<option value='0'>Select Mata Kuliah</option>" ;
			foreach($parent as $p )
			{
				echo "<option value='".$p->mkditawarkan_id."'";
				echo ">".$p->keterangan."</option>" ;	
			}
		}
		else echo "<option value='0'>Select Mata Kuliah</option>" ;
		
	}
	
	//--INDEX SILABUS--//
	
	function tampilkan_indexmk(){
		$msilabus	= new model_silabus();
		$fakultas_id	=	$_POST['fakultas'];
		$cabang_id	=	$_POST['cabang'];
		$thn_id	=	$_POST['thn'];
		$result = $msilabus->mkditawarkan($cabang_id,$fakultas_id,$thn_id);
		// echo $result;
		if($result){
			echo "<option value='0'>Select Mata Kuliah</option>" ;
			foreach($result as $p )
			{
				echo "<option value='".$p->hid_id."'";
				echo ">".$p->keterangan."</option>" ;	
			}
		}
	}
	
	function tampilkan_index(){
		$msilabus	= new model_silabus();
		$fakultas_id	=	$_POST['fakultas'];
		$cabang_id		=	$_POST['cabang'];
		$thn		=	$_POST['thn'];
		$mk_id		=	$_POST['mk'];
		$user		= $this->coms->authenticatedUser->role;
		$result 		= $msilabus->read("",$fakultas_id,$cabang_id,$mk_id,$thn);
		//echo $result;
		if($result){
			$data['posts'] = $result;
			$data['user'] = $user;
		
			$this->view('silabus/viewsilabus.php',$data);
		}
	}
	
	//================================KOMPONEN===================================
	
	function viewkomponen($by = 'all'){
		
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="Mahasiswa"){
		$mkomponen = new model_silabus();
		$data['user'] = $user;
		$data['posts'] = $mkomponen->read_komponen('');
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	

		// $this->add_script('js/komponen.js');
		switch($by){
			case 'ok';
				$data['status'] 	= 'OK';
				$data['statusmsg']  = 'OK, data telah diupdate.';
			break;
			case 'duplicate';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data dengan nama komponen tersebut sudah ada.';
			break;
			case 'nok';
				$data['status'] 	= 'Not OK';
				$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
			break;
		}
		$this->view( 'silabus/komponen.php', $data );
		}
	}
	
	function writekomponen(){
		$mkomponen = new model_silabus();
		$user = $this->coms->authenticatedUser->role;
		$data['user'] = $user;
		$data['posts'] = "";
		$data['komponen'] = $mkomponen->get_komponen();
		
		$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
		$this->coms->add_script('js/datatables/jquery.dataTables.js');	
		$this->coms->add_script('js/datatables/DT_bootstrap.js');	
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/mk/silabus.js');
		$this->coms->add_script('js/submit.js');

		$this->view( 'silabus/tambahkomponen.php', $data );
	}

	function editkomponen($id){
		if( !$id ) {
			$this->redirect('module/akademik/silabus/viewkomponen');
			exit;
		}
		$user = $this->coms->authenticatedUser->role;
		if($user!="Mahasiswa" && $user!="Dosen"){
		$mkomponen = new model_silabus();	
		
		$data['user'] = $user;
		$data['posts'] = $mkomponen->read_komponen($id);	
		$data['komponen'] = $mkomponen->get_komponen();
		
		$this->coms->add_script('select/select2.js');
		$this->coms->add_style('select/select2.css');
		$this->add_script('js/mk/silabus.js');
		$this->coms->add_script('js/submit.js');
		
		$this->view('silabus/tambahkomponen.php', $data);
		}
	}

	function savekomponen(){
	
		if(isset($_POST['komponen'])&&$_POST['komponen']!=""){
			$this->savekomponenToDB();
			exit();
		}else{
			$this->index();
			exit;
		}
		
		echo "tes";
	
	}
	
	function savekomponenToDB(){
		ob_start();
		
		$mkomponen = new model_silabus();
					
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
			
		if($_POST['hidId']!=""){
			$komponenid 	= $_POST['hidId'];
			$action 	= "update";
		}else{
			$komponenid	= $mkomponen->id_silabus_komponen();
			$action 	= "insert";			
		}
		
		if(isset($_POST['isaktif'])!=""){
			$isaktif	= $_POST['isaktif'];
		}else{
			$isaktif	= 0;
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->coms->authenticatedUser->id;
		//$prodi		= $_POST['cmbprodi'];
		$parentid	= $_POST['cmbkomponen'];
		$komponen		= $_POST['komponen'];
		$new		= $_POST['newkomp'];
		$ceknewkomp = $mkomponen->cek_new_komp($komponen);
		$ceknewkompbyid = $mkomponen->cek_new_komp_by_id($komponen);
		
		if(($komponen!='')){
			if($new==1){
				if(!isset($ceknewkomp)){
					$datanya 	= Array('komponen_id'=>$komponenid, 'keterangan'=>$komponen, 'is_aktif'=>$isaktif, 'parent_id'=>$parentid);
					$mkomponen->replace_komponen($datanya);
					
					
					$this->redirect('module/akademik/silabus/viewkomponen/ok');
					exit();
				}
				else{
					$this->redirect('module/akademik/silabus/viewkomponen/duplicate');
					exit();
				}
			}
			elseif (($new!=1) && $ceknewkompbyid==$komponenid || $ceknewkompbyid==NULL) {
				$datanya 	= Array('komponen_id'=>$komponenid, 'keterangan'=>$komponen, 'is_aktif'=>$isaktif, 'parent_id'=>$parentid);
				$mkomponen->replace_komponen($datanya);
				
				
				$this->redirect('module/akademik/silabus/viewkomponen/ok');
				exit();
			}
			else{
				$this->redirect('module/akademik/silabus/viewkomponen/duplicate');
				exit();
			}
		}else{
			$this->redirect('module/akademik/silabus/viewkomponen/nok');
			exit();
		}
	}

	
	
}
?>