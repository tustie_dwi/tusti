<?php
class akademik_jenisfile extends comsmodule {
	private $coms;

	function __construct($coms) {
		parent::__construct($coms);
		$this->coms = $coms;
		
		$coms->require_auth('auth'); 
	}
	
	function index($by = 'all'){
		$mjenisfile = new model_jenisfile();
		$user = $this->coms->authenticatedUser->role;
		
		if($user!="mahasiswa" && $user!="dosen"){
			$data['user'] = $user;
			$data['posts'] = $mjenisfile->read('');
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
			// $this->add_script('js/komponen.js');
			switch($by){
				case 'ok';
					$data['status'] 	= 'OK';
					$data['statusmsg']  = 'OK, data telah diupdate.';
				break;
				case 'duplicate';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data dengan nama jenis file tersebut sudah ada.';
				break;
				case 'nok';
					$data['status'] 	= 'Not OK';
					$data['statusmsg']  = 'Maaf, data tidak dapat tersimpan.';
				break;
			}
			$this->view(  'jenisfile/index.php', $data );
		}
	}
	
	function write(){
		$mjenisfile = new model_jenisfile();
		$user = $this->coms->authenticatedUser->role;
		if($user!="mahasiswa" && $user!="dosen"){
			$data['user'] = $user;
			$data['posts'] = "";
			
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->comscoms->authenticatedUser->id;
		//$prodi		= $_POST['cmbprodi'];
		$keterangan	= $_POST['jenisfilename'];
		$ext		= $_POST['extention'];
		$max		= $_POST['maxsize'];
		$new		= $_POST['newjen'];
		$ceknew 	= $mjenis->ceknew($keterangan);
		$cekbyid 	= $mjenis->cekbyid($keterangan);
		
		if(($keterangan!='')){
			if($new==1){
				if(!isset($ceknew)){
					// echo $keterangan."<br>";
					// echo $jenisfileid."<br>";
					$datanya 	= Array('jenis_file_id'=>$jenisfileid, 'keterangan'=>$keterangan, 'extention'=> $ext, 'max_size'=> $max, 'kode_jenis'=>$jenisfileid);
					$mjenis->replace_jenisfile($datanya);
					
					
					$this->redirect('module/akademik/jenisfile/index/ok');
					exit();
				}
				else{
					$this->redirect('module/akademik/jenisfile/index/duplicate');
					exit();
				}
			}
			elseif(($new!=1)&&$cekbyid==$jenisfileid||$cekbyid==NULL){
				//echo "a";
				$datanya 	= Array('jenis_file_id'=>$jenisfileid, 'keterangan'=>$keterangan, 'extention'=> $ext, 'max_size'=> $max, 'kode_jenis'=>$jenisfileid);
				$mjenis->replace_jenisfile($datanya);
				
				$this->redirect('module/akademik/jenisfile/index/ok');
				exit();
			}
			else{
				$this->redirect('module/akademik/jenisfile/index/duplicate');
				exit();
			}
			
		}else{
			$this->redirect('module/akademik/jenisfile/index/nok');
			exit();
		}
	}
}->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');	
	
			$this->coms->add_script('js/submit.js');
	
			$this->view( 'jenisfile/edit.php', $data );
		}
	}
	
	function edit($id){
		if( !$id ) {
			$this->redirect('module/akademik/jenisfile/');
			exit;
		}
		
		$mjenisfile = new model_jenisfile();
		$user = $this->coms->authenticatedUser->role;
		if($user!="mahasiswa" && $user!="dosen"){
			$data['user'] = $user;
			$data['posts'] = $mjenisfile->read($id);
		
			$this->coms->add_style('css/bootstrap/DT_bootstrap.css');
			$this->coms->add_script('js/datatables/jquery.dataTables.js');	
			$this->coms->add_script('js/datatables/DT_bootstrap.js');
							
			$this->coms->add_script('js/submit.js');
			
			$this->view( 'jenisfile/edit.php', $data );
		}
	}
	
	function save(){
	
		if(isset($_POST['jenisfilename'])!=""){
			$this->saveToDB();
			exit();
		}else{
			$this->index();
			exit;
		}
		
		//echo "tes";
	
	}
	
	function saveToDB(){
		ob_start();
		
		$mjenis = new model_jenisfile();
					
		$today	= str_replace('-', '', date("y-m-d"));
		$jam	= str_replace(':', '', date("H:i:s"));
			
		if($_POST['hidId']!=""){
			$jenisfileid 		= $_POST['hidId'];
			$action 	= "update";
		}else{
			$jenisfileid		= $mjenis->id_jenisfile();
			$action 			= "insert";			
		}
		
		$lastupdate	= date("Y-m-d H:i:s");
		$user		= $this->