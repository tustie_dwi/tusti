<?php
class model_register extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_user_account($email=NULL){
		$sql = "SELECT id FROM db_coms.coms_user WHERE (email = '".$email."'  OR username='$email') ";
		$result = $this->db->getRow( $sql );
	//echo $sql;
		return $result;	
	}
	
	function get_user_account_old($email=NULL, $pass=NULL){
		$password=md5($this->db->escape( $pass ));
		$sql = "SELECT id FROM db_coms.pt11k_user WHERE ((email = '".$email."'  OR username='$email') AND password='".$password."') ";
		$result = $this->db->getRow( $sql );
	//echo $sql;
		return $result;	
	}
	
	
	function get_karyawan_id($email=NULL){
		
		$sql = "SELECT `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`,
				`db_ptiik_apps`.`tbl_karyawan`.`nama`,
				`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
				`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`,
				mid(md5(`db_ptiik_apps`.`tbl_karyawan`.`nama`),7,9) as `pwd`,
				db_ptiik_apps.tbl_karyawan.is_status,
				db_ptiik_apps.tbl_karyawan.fakultas_id,
				db_ptiik_apps.tbl_karyawan.foto,
				(SELECT
					db_coms.coms_level.`level`
					FROM
					db_ptiik_apps.tbl_karyawan_unit
					INNER JOIN db_coms.coms_level ON db_ptiik_apps.tbl_karyawan_unit.unit_id = db_coms.coms_level.unit_id
					where tbl_karyawan_unit.karyawan_id = `tbl_karyawan`.`karyawan_id` AND tbl_karyawan_unit.is_aktif='1' ORDER BY tbl_karyawan_unit.periode_selesai DESC LIMIT 0,1 ) as `level`
				FROM
				`db_ptiik_apps`.`tbl_karyawan`  WHERE email = '".$email."' AND is_aktif NOT IN ('meninggal','keluar') ";
						
		$result = $this->db->getRow( $sql );
	//echo $sql;
		return $result;	
		
	}
	
	function get_asisten_id($email=NULL, $nim=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_mahasiswa.email,
					db_ptiik_apps.tbl_mahasiswa.is_aktif,
					mid(md5(db_ptiik_apps.tbl_mahasiswa.nama),7,9) AS pwd,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.prodi_id,
					db_ptiik_apps.tbl_prodi.fakultas_id,
					db_ptiik_apps.tbl_mahasiswa.foto
					FROM
					db_ptiik_apps.tbl_mahasiswa
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
					INNER JOIN db_ptiik_apps.tbl_praktikum_asisten ON tbl_praktikum_asisten.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
					WHERE 
				tbl_mahasiswa.is_aktif='aktif' AND tbl_praktikum_asisten.is_aktif='1' AND tbl_mahasiswa.email='".$email."' AND tbl_mahasiswa.nim='".$nim."' 
				";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_mahasiswa_id($email=NULL, $nim=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_mahasiswa.mahasiswa_id,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_mahasiswa.email,
					db_ptiik_apps.tbl_mahasiswa.is_aktif,
					mid(md5(db_ptiik_apps.tbl_mahasiswa.nama),7,9) AS pwd,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.prodi_id,
					db_ptiik_apps.tbl_prodi.fakultas_id,
					db_ptiik_apps.tbl_mahasiswa.foto
					FROM
					db_ptiik_apps.tbl_mahasiswa
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
					WHERE 
				tbl_mahasiswa.is_aktif='aktif'  AND tbl_mahasiswa.email='".$email."' AND tbl_mahasiswa.nim='".$nim."' 
				";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function aktivasi_user($id=NULL){
		$sql = "SELECT id  FROM db_coms.coms_user WHERE mid(md5(karyawan_id),5,5) = '".$id."' ";
		$result = $this->db->getRow($sql);
		
		return $result;
	}
	
	function generateHash($password) {
		if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
			$salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
			return crypt($password, $salt);
		}
	}
	
	function verify($password, $hashedPassword) {
		return crypt($password, $hashedPassword);
		
	}
	
	
	public function save_user($username=NULL, $password=NULL, $name=NULL, $email=NULL, $level=NULL, $status=NULL, $staffid=NULL, $mhsid=NULL, $fakultas=NULL, $foto=NULL) {
		
		$data['username'] 		= $this->db->escape($username);		
		$data['password'] 		= $this->generateHash($this->db->escape($password));
		$data['name'] 			= $this->db->escape($name);
		$data['email'] 			= $this->db->escape($email);
		$data['level']			= $this->db->escape($level);
		$data['status'] 		= $this->db->escape($status);				
		$data['karyawan_id'] 	= $this->db->escape($staffid);	
		$data['mahasiswa_id'] 	= $this->db->escape($mhsid);
		$data['fakultas_id']	= $this->db->escape($fakultas);
		$data['foto']			= $this->db->escape($foto);

		$result = $this->db->replace("db_coms`.`coms_user", $data);
	
		return $result;
		
	}
	
	function update_user_account($username=NULL, $password=NULL, $id=NULL){
		$pass = $this->generateHash($this->db->escape($password));
		
		$sql = "UPDATE db_coms.coms_user SET password = '$pass' WHERE id = '$id' ";
		echo $sql;
		return $this->db->query($sql);
		
		echo $sql;
		
	}
	
	function get_user_id($email=NULL){
		$sql = "SELECT id, mid(md5(karyawan_id),5,5) as `uid`, name, email FROM db_coms.coms_user WHERE email = '".$email."' ";
		$result = $this->db->getRow($sql);
	//	echo $sql;
		return $result;
	}
	
}