<?php
class model_rss extends model {
	public function __construct() {
		parent::__construct();
	}
		
	function rss_feed($lang=NULL, $cat=NULL){
	
		$sql= "SELECT
					mid(md5(db_ptiik_coms.coms_content.content_id),9,7) AS id,
					db_ptiik_coms.coms_content.content_category,
					db_ptiik_coms.coms_content.content_title,
					db_ptiik_coms.coms_content.content_title as judul_ori,
					db_ptiik_coms.coms_content.content_excerpt,
					db_ptiik_coms.coms_content.content,
					db_ptiik_coms.coms_content.content_link,
					db_ptiik_coms.coms_content.content_status,
					db_ptiik_coms.coms_content.content_lang,
					db_ptiik_coms.coms_content.content_author,
					db_ptiik_coms.coms_content.content_upload,
					db_ptiik_apps.tbl_unit_kerja.kode AS unit_id,
					db_coms.coms_user.`name` AS author,
					if( db_ptiik_apps.tbl_unit_kerja.kategori = 'laboratorium','lab',db_ptiik_apps.tbl_unit_kerja.kategori ) AS kategori
					FROM
					db_ptiik_coms.coms_content
					LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_coms.coms_content.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
					LEFT JOIN db_coms.coms_user ON db_ptiik_coms.coms_content.content_author = db_coms.coms_user.id
					WHERE
					db_ptiik_coms.coms_content.content_status = 'publish' AND
					db_ptiik_coms.coms_content.content_lang = '".$lang."' AND
					db_ptiik_coms.coms_content.content_category IN (".$cat.")
					ORDER BY
					db_ptiik_coms.coms_content.content_upload DESC
					LIMIT 0,10";
		
		$result = $this->db->query( $sql );
		return $result;
		//return $sql;
		
	}
	
	function read_event($lang=NULL, $unit=NULL){
		$sql = "SELECT
					mid(md5(db_ptiik_apps.tbl_agenda.agenda_id),9,7) as id,
					db_ptiik_apps.tbl_agenda.judul as judul_ori, db_ptiik_apps.tbl_agenda.keterangan as content_excerpt,
					db_ptiik_apps.tbl_agenda.lokasi,'event' as kategori, 'event' as content_category, ";
					
		if(strtolower($lang)=='in') $sql .= "db_ptiik_apps.tbl_agenda.judul as content_title, db_ptiik_apps.tbl_agenda.keterangan as content,db_ptiik_apps.tbl_unit_kerja.keterangan AS unit," ;
		else $sql .= "db_ptiik_apps.tbl_agenda.english_version as content_title, db_ptiik_apps.tbl_agenda.note_english as content, db_ptiik_apps.tbl_unit_kerja.english_version AS unit," ;
		
		$sql .= "
					db_ptiik_apps.tbl_agenda.tgl_mulai as content_upload,
					db_ptiik_apps.tbl_agenda.tgl_selesai as content_modified_end,
					db_ptiik_apps.tbl_agenda.page_link as content_link,
					db_ptiik_apps.tbl_agenda.last_update,
					'FILKOM UB' as author,
					db_ptiik_apps.tbl_unit_kerja.keterangan AS unit_ori,
					db_ptiik_apps.tbl_jeniskegiatan.keterangan AS kegiatan
					FROM
					db_ptiik_apps.tbl_agenda
					LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_apps.tbl_agenda.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
					INNER JOIN db_ptiik_apps.tbl_jeniskegiatan ON db_ptiik_apps.tbl_agenda.jenis_kegiatan_id = db_ptiik_apps.tbl_jeniskegiatan.jenis_kegiatan_id WHERE db_ptiik_apps.tbl_agenda.status_agenda = 'publish' ";
		
		if($unit && $unit!=0) $sql.= " AND db_ptiik_apps.tbl_agenda.unit_id ='$unit' ";
		
		$sql = $sql. " ORDER BY tbl_agenda.tgl_mulai DESC LIMIT 0, 10";
		
		//echo $sql."<br>";
		if(isset($id)) $result = $this->db->getRow( $sql ); 
		else $result = $this->db->query( $sql );
				
		return $result;

	}
	
	function read_unit_kerja($fakultas=NULL,$lang=NULL,$cat=NULL,  $kode=NULL, $id=NULL, $parent=NULL){
		$sql = "SELECT
				mid(md5(db_ptiik_apps.tbl_unit_kerja.unit_id),9,7) as `id`,
				tbl_unit_kerja.unit_id,
				tbl_unit_kerja.fakultas_id,
				tbl_unit_kerja.keterangan as judul_ori,
				tbl_unit_kerja.keterangan as content_excerpt,
				";
		if(strtolower($lang)=='in') $sql .= "db_ptiik_apps.tbl_unit_kerja.keterangan AS content_title,db_ptiik_apps.tbl_unit_kerja.tentang AS content," ;
		else $sql .= "db_ptiik_apps.tbl_unit_kerja.english_version AS content_title,db_ptiik_apps.tbl_unit_kerja.about AS content," ;
		
		$sql.= "tbl_unit_kerja.kode as content_link,
				'FILKOM UB' as author,
				tbl_unit_kerja.parent_id,
				tbl_unit_kerja.last_update as content_upload ,
				tbl_unit_kerja.kategori,
				tbl_unit_kerja.kategori as content_category,
				tbl_unit_kerja.is_aktif,
				tbl_unit_kerja.is_riset,
				tbl_unit_kerja.strata,
				tbl_unit_kerja.icon,
				tbl_unit_kerja.logo
				FROM
				db_ptiik_apps.tbl_unit_kerja WHERE is_aktif='1' AND tbl_unit_kerja.kategori <> 'fakultas' ";
		if($cat) $sql.= " AND tbl_unit_kerja.kategori = '$cat' ";
		if($fakultas) $sql.= " AND tbl_unit_kerja.fakultas_id = '$fakultas' ";
		if($kode) $sql.= " AND (lcase(tbl_unit_kerja.kode) = '$kode') ";
		if($id) $sql.= " AND (mid(md5(db_ptiik_apps.tbl_unit_kerja.unit_id),9,7) = '$id'  OR (tbl_unit_kerja.unit_id = '$id'))";
		if($parent) $sql.= " AND (mid(md5(db_ptiik_apps.tbl_unit_kerja.parent_id),9,7) = '$parent'  OR (tbl_unit_kerja.parent_id = '$parent'))";
		
		$sql .= " ORDER BY tbl_unit_kerja.strata DESC, tbl_unit_kerja.kode ASC";
	//if($cat=='riset') echo $sql."<br>";
		if($kode):
			return $this->db->getRow( $sql );		
		else:
			if($id) return $this->db->getRow( $sql );
			else	return $this->db->query( $sql );
		endif;
		
		
	}
}
?>