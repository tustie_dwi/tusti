<?php
class model_hadir extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function get_kegiatan($day,$month,$year, $runday, $jam, $uid=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$bulan = $this->addNol($month);
		$tglend	= $year."-".$month."-".($day+1);
		
		// $hari=$this->get_namahari($runday);
		
		$sql = "SELECT DISTINCT 
					db_ptiik_apps.tbl_aktifitas.karyawan_id,
					db_ptiik_apps.tbl_aktifitas.tgl,
					time_format(db_ptiik_apps.tbl_aktifitas.jam_mulai, '%H:%i') as `jam_mulai`,
					time_format(db_ptiik_apps.tbl_aktifitas.jam_selesai, '%H:%i') as `jam_selesai`,
					db_ptiik_apps.tbl_aktifitas.inf_ruang as ruang,
					db_ptiik_apps.tbl_aktifitas.jenis_kegiatan_id as `jenis`,
					db_ptiik_apps.tbl_aktifitas.judul as kegiatan,
					db_ptiik_apps.tbl_aktifitas.nama_mk as namamk,
					db_ptiik_apps.tbl_aktifitas.catatan as keterangan,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_karyawan.is_status
					FROM
					db_ptiik_apps.tbl_aktifitas
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_aktifitas.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				WHERE 1 = 1
					";
		$sql = $sql . " AND MONTH(db_ptiik_apps.tbl_aktifitas.tgl) = '$bulan' AND YEAR(db_ptiik_apps.tbl_aktifitas.tgl) = '$year'";
		
		if($tgl){
			// $sql = $sql . " MONTH(db_ptiik_apps.tbl_aktifitas.tgl) = '$bulan' AND YEAR(db_ptiik_apps.tbl_aktifitas.tgl) = '$year'";
			//$sql = $sql . " AND db_ptiik_apps.tbl_aktifitas.tgl = '".$tgl."' ";
			// $sql = $sql . " AND ('".$tgl."' BETWEEN db_ptiik_apps.tbl_aktifitas.tgl AND db_ptiik_apps.tbl_aktifitas.tgl_selesai) ";
		}
		
		if($uid){
			$sql = $sql . " AND (db_ptiik_apps.tbl_aktifitas.karyawan_id = '".$uid."' OR (mid(md5(db_ptiik_apps.tbl_aktifitas.karyawan_id),9,7) = '".$uid."')) ";
		}
		
		$sql = $sql . " ORDER BY time_format(db_ptiik_apps.tbl_aktifitas.jam_mulai, '%H:%i') ";
		$result = $this->db->query( $sql );
		
		return $result;	
	
	}
	
	function addnol($string){
		if(strlen($string)==1){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	function get_namahari($runday){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
			case '7': $hari='minggu'; break;
		}
		
		return $hari;
	}
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 15) {
			$stringCut = substr($string, 0, 25);
			// make sure it ends in a word so assassinate doesn't become ass...
			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'..'; 
		}
		return $string;
	}
	
	function get_blok_waktu(){
		$sql = "SELECT distinct time_format(jam_mulai,'%H:%i') as `jam_mulai`, time_format(jam_selesai,'%H:%i') as `jam_selesai` FROM db_ptiik_apps.tbl_blokwaktu ORDER BY db_ptiik_apps.tbl_blokwaktu.jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function potong_kalimat ($content, $length) {
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
}
?>