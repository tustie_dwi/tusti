<?php

class model_menu extends model {
	public function __construct() {
		parent::__construct();
	}
	
	public function read($id=NULL){
	
		$sql = "SELECT DISTINCT 
					`coms_menu`.`id`,
					`coms_menu`.`parent_id`,
					`coms_menu`.`link`,
					`coms_menu`.`judul`,
					`coms_menu`.`deskripsi`,
					`coms_menu`.`module`,
					`coms_menu`.`icon`,
					`coms_menu`.`is_aktif`,
					`coms_level_menu`.`level`
					FROM
					`coms_menu`
					Inner Join `coms_level_menu` ON `coms_menu`.`id` = `coms_level_menu`.`menu_id`
					WHERE 1=1 
					";
		if($id){
			$sql = $sql . " AND `coms_menu`.`is_aktif`=1 AND `coms_level_menu`.`level` IN (".$id.") GROUP BY
							coms_menu.id";
		}
		
		$sql.= " ORDER BY `coms_menu`.`urut` ASC ";
		$result = $this->db->query( $sql );
		//echo $sql;
		return $result;	
		
	}
	
	public function read_menu($id=NULL, $str=NULL){
	
		$sql = "SELECT DISTINCT 
					`coms_menu`.`id`,
					`coms_menu`.`parent_id`,
					`coms_menu`.`link`,
					`coms_menu`.`judul`,
					`coms_menu`.`deskripsi`,
					`coms_menu`.`module`,
					`coms_menu`.`icon`,
					`coms_menu`.`is_aktif`
					FROM
					`coms_menu`				
					WHERE 1=1 
					";
		if($id){
			$sql = $sql . " AND `coms_menu`.`is_aktif`=1 ";
		}
		
		if($str){
			$sql = $sql . " AND `coms_menu`.`parent_id`=0 ";
		}
		
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
	public function save($id, $value){
		
		$data['value'] = serialize($value);
		$where['name'] = $id;
		
		$affected = $this->db->update('coms_options', $data, $where);
		//var_dump($this->db);
		return $affected;
		
	}
	
	public function replace_menu($datanya){
		$result = $this->db->insert("coms_menu", $data);
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	
	function save_menu($link, $judul, $parentid, $nmodule, $icon, $isaktif){
		$data['parent_id'] 	= $parentid;
		$data['link'] 		= $link;
		$data['judul'] 		= $judul;
		$data['module'] 	= $nmodule;
		$data['icon'] 		= $icon;		
		$data['is_aktif'] 	= $isaktif;
		
		$result = $this->db->insert("coms_menu", $data);
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	
	
	function activate($id=NULL){
		$where['id'] = $id;
		$data['is_aktif'] = 1;
		
		$result = $this->db->update("coms_menu", $data, $where);
		
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	
	function deactivate($id=NULL){
		$where['id'] = $id;
		$data['is_aktif'] = 0;
		
		$result = $this->db->update("coms_menu", $data, $where);
		//echo $result;
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	

}