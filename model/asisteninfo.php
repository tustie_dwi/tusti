<?php
class model_asisteninfo extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read_asisten($id=NULL, $thn=NULL, $cabang=NULL, $fakultas=NULL, $lang=NULL){
		$sql = "SELECT 
				mid(md5(`tbl_praktikum_asisten`.`asisten_id`),9,7) as `asisten_id`,
				`tbl_praktikum_asisten`.`asisten_id` as `hidId`,
				`tbl_praktikum_asisten`.`mahasiswa_id`,
				`tbl_praktikum_asisten`.`unit_id`,
				`tbl_praktikum_asisten`.`tahun_akademik`,
				`tbl_praktikum_asisten`.`mkditawarkan_id`,
				`tbl_praktikum_asisten`.`tgl_mulai`,
				`tbl_praktikum_asisten`.`tgl_selesai`,
				`tbl_praktikum_asisten`.`is_aktif`,
				`tbl_praktikum_asisten`.`keterangan`,
				`tbl_unit_kerja`.`keterangan` as `namalab`,
				`tbl_mahasiswa`.`nama` as `namamhs`,
				`tbl_mahasiswa`.`nim`,
				`tbl_mahasiswa`.`foto`,
				`tbl_tahunakademik`.`is_ganjil`, ";
					if(strTolower($lang)=='in') $sql.=" tbl_namamk.keterangan as mk,  ";
					else $sql.=" tbl_namamk.english_version as mk, ";
			$sql.="
				`tbl_tahunakademik`.`tahun`
				FROM `db_ptiik_apps`.`tbl_praktikum_asisten` 
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa` ON `tbl_mahasiswa`.`mahasiswa_id` = `tbl_praktikum_asisten`.`mahasiswa_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_unit_kerja` ON `tbl_unit_kerja`.`unit_id` = `tbl_praktikum_asisten`.`unit_id`
				LEFT JOIN `db_ptiik_apps`.`tbl_tahunakademik` ON `tbl_tahunakademik`.`tahun_akademik` = `tbl_praktikum_asisten`.`tahun_akademik`
				LEFT JOIN `db_ptiik_apps`.`tbl_mkditawarkan` ON `tbl_mkditawarkan`.`mkditawarkan_id` = `tbl_praktikum_asisten`.`mkditawarkan_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_matakuliah` ON `tbl_matakuliah`.`matakuliah_id` = `tbl_mkditawarkan`.`matakuliah_id`
                LEFT JOIN `db_ptiik_apps`.`tbl_namamk` ON `tbl_namamk`.`namamk_id` = `tbl_matakuliah`.`namamk_id`
				WHERE 1
			   ";
		if($id){
			$sql = $sql . "AND tbl_praktikum_asisten.unit_id = '".$id."' ";
		}
		
		if($thn){
			$sql = $sql . "AND tbl_praktikum_asisten.tahun_akademik = '".$thn."' ";
		}
		
		if($cabang){
			$sql = $sql . "AND tbl_mahasiswa.cabang_id = '".$cabang."' ";
		}
		
		if($fakultas){
			$sql = $sql . "AND tbl_namamk.fakultas_id = '".$fakultas."' ";
		}
		// echo $sql;
		$sql = $sql . " ORDER BY `tbl_mahasiswa`.`nim`, `tbl_praktikum_asisten`.`tahun_akademik`,`tbl_praktikum_asisten`.`is_aktif` DESC";
		$result = $this->db->query( $sql );

		return $result;
	}
	
}
?>