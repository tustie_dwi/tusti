<?php
class model_file extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function read($id=NULL){
		$sql = "SELECT file_id,
					   jenis_file_id,
					   materi_id,
					   mkditawarkan_id, 
					   judul,
					   keterangan,
					   file_name, 
					   file_type, 
					   file_loc,
					   file_content,
					   file_size,
					   tgl_upload,
					   is_publish,
					   upload_by,
					   user_id,
					   db_ptiik_apps.tbl_file.last_update,
					db_coms.coms_user.`name` as `user`
					FROM
					db_ptiik_apps.tbl_file
					LEFT JOIN db_coms.coms_user ON db_ptiik_apps.tbl_file.user_id = db_coms.coms_user.id
					WHERE 1 = 1 ";
		if($id){
			$sql = $sql . " AND MID( MD5(`db_ptiik_apps`.`tbl_file`.file_id), 6, 6) = '".$id."' ";
		}

		$result = $this->db->query( $sql );
		
		
		return $result;
	}
	
	function viewdetail($id=NULL){
		$sql = "SELECT
					MID( MD5(file.file_id), 6, 6) AS fileid,
					file.tgl_upload,
					file.judul,
					file.keterangan,
					file.upload_by,
					file.file_loc,
					file.file_name,
					jenisfile.keterangan AS jen_file,
					db_coms.coms_user.`name`,
					file.file_size
					FROM
					db_ptiik_apps.tbl_file AS file
					INNER JOIN db_ptiik_apps.tbl_jenisfile AS jenisfile ON file.jenis_file_id = jenisfile.jenis_file_id
					INNER JOIN db_coms.coms_user ON file.user_id = db_coms.coms_user.id
					WHERE 1=1
				 ";
		if($id){
			$sql = $sql. " AND MID( MD5(file.file_id), 6, 6) = '".$id."' ";
		}
		$result = $this->db->query( $sql );
		
		
		return $result;	
	}
	
	function id_materimk($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(materi_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_materimk` WHERE left(materi_id, 6) = '".date("Ym")."' "; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}

	function get_fakultas(){
		$sql = "SELECT MID( MD5(fakultas_id), 6, 6) as fak_id, 
					   keterangan, 
					   fakultas_id as hid_id
				FROM `db_ptiik_apps`.`tbl_fakultas`";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_materi(){
		$sql = "SELECT  MID( MD5(`tbl_materimk`.`materi_id`), 6, 6) as materi_id,
					`tbl_materimk`.`materi_id` as hid_id,
					`tbl_materimk`.`judul` as `judul`
					FROM
					`db_ptiik_apps`.`tbl_materimk`
					WHERE is_valid = 1";
					
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_mkditawarkan_by_materi_id($id){
		$sql = "SELECT  mkditawarkan_id
				FROM
				`db_ptiik_apps`.`tbl_materimk`
				WHERE materi_id = '".$id."'";
				
		$result = $this->db->getRow( $sql );
		
		return $result->mkditawarkan_id;
		
	}
	
	function get_ext($str=NULL){
		$sql = "SELECT jenis_file_id, 
					   keterangan, 
					   extention, 
					   max_size 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE extention  like '%".$str."%' ";
		
		$result = $this->db->getRow( $sql );
		return $result;

	}
	
	function get_kategori_by_general(){
		$sql = "SELECT MID( MD5( jenisfile.jenis_file_id ) , 6, 6 ) AS jenis_id, 
					   jenisfile.keterangan, 
					   jenisfile.kode_jenis AS hid_id, 
					   file.file_id, 
					   jenisfile.jenis_file_id
				FROM  `db_ptiik_apps`.`tbl_jenisfile` jenisfile,  
					  `db_ptiik_apps`.`tbl_file` file
				WHERE jenisfile.jenis_file_id = file.jenis_file_id
				AND materi_id =  ''
				GROUP BY jenisfile.keterangan";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_kategori_by_materimk($mk=NULL){
		$sql = "SELECT MID( MD5( jenisfile.jenis_file_id ) , 6, 6 ) AS jenis_id, 
					   jenisfile.keterangan, 
					   jenisfile.kode_jenis AS hid_id, 
					   file.file_id, 
					   jenisfile.jenis_file_id
				FROM  `db_ptiik_apps`.`tbl_jenisfile` jenisfile,  
					  `db_ptiik_apps`.`tbl_file` file
				WHERE jenisfile.jenis_file_id = file.jenis_file_id
				AND materi_id !=  ''";
		if($mk){
			$sql .= " AND (mid(md5(file.mkditawarkan_id),9,7)='".$mk."' OR file.mkditawarkan_id = '".$mk."' )";
		}		
		$sql .= " GROUP BY jenisfile.keterangan";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_materi_by_kategori($id){
		$sql = "SELECT
				MID( MD5( jenisfile.jenis_file_id ) , 6, 6 ) AS jenis_id,
				jenisfile.keterangan,
				jenisfile.kode_jenis AS hid_id,
				file.file_id,
				file.file_name,
				mmk.judul AS nama_materi,
				file.materi_id AS mat_id,
				jenisfile.jenis_file_id,
				file.last_update,
				db_coms.coms_user.`name`
				FROM
				db_ptiik_apps.tbl_jenisfile AS jenisfile ,
				db_ptiik_apps.tbl_file AS file
				INNER JOIN db_ptiik_apps.tbl_materimk AS mmk ON file.materi_id = mmk.materi_id
				LEFT JOIN db_coms.coms_user ON file.user_id = db_coms.coms_user.id
				WHERE jenisfile.jenis_file_id = file.jenis_file_id
				AND file.materi_id <> ''
				AND file.jenis_file_id = '".$id."'
				AND file.tugas_id IS NULL
				GROUP BY nama_materi
				";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_file_by_kategori_jenis_general($id,$matid){
		$sql = "SELECT
				MID( MD5( jenisfile.jenis_file_id ) , 6, 6 ) AS jenis_id,
				jenisfile.keterangan,
				jenisfile.kode_jenis AS hid_id,
				MID( MD5(file.file_id) , 6, 6 ) AS file_id,
				file.file_id AS fileid,
				file.file_name,
				file.file_loc,
				file.materi_id AS mat_id,
				file.judul,
				file.file_type,
				file.upload_by,
				file.last_update,
				db_coms.coms_user.`name`
				FROM
				db_ptiik_apps.tbl_jenisfile AS jenisfile ,
				db_ptiik_apps.tbl_file AS file
				INNER JOIN db_coms.coms_user ON file.user_id = db_coms.coms_user.id
				WHERE jenisfile.jenis_file_id = file.jenis_file_id
				AND materi_id =  '".$matid."'
				AND file.jenis_file_id = '".$id."'
				";

		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_file_by_kategori_jenis_materimk($id,$matid){
		$sql = "SELECT
					MID( MD5( jenisfile.jenis_file_id ) , 6, 6 ) AS jenis_id,
					jenisfile.keterangan,
					jenisfile.kode_jenis AS hid_id,
					MID( MD5(file.file_id) , 6, 6 ) AS file_id,
					MID( MD5(file.file_id) , 9, 7 ) AS fileid_content,
					MID( MD5(file.materi_id) , 9, 7 ) AS materiid_content,
					file.file_id AS fileid,
					file.file_name,
					file.file_loc,
					file.file_type,
					file.materi_id AS mat_id,
					file.judul,
					file.last_update,
					db_ptiik_apps.tbl_materimk.judul AS nama_materi,
					db_coms.coms_user.`name`
					FROM
					db_ptiik_apps.tbl_jenisfile AS jenisfile ,
					db_ptiik_apps.tbl_file AS file
					INNER JOIN db_ptiik_apps.tbl_materimk ON file.materi_id = db_ptiik_apps.tbl_materimk.materi_id
					INNER JOIN db_coms.coms_user ON file.user_id = db_coms.coms_user.id
					WHERE
					jenisfile.jenis_file_id = file.jenis_file_id
				AND file.materi_id =  '".$matid."'
				AND file.jenis_file_id = '".$id."'
				AND file.tugas_id IS NULL
				";
		// echo $sql;
		$result = $this->db->query( $sql );

		return $result;	
	}
	
	function get_file(){
		$sql = "SELECT
					`tbl_materimk`.`materi_id`,
					`tbl_materimk`.`judul` as `judul`
					FROM
					`db_ptiik_apps`.`tbl_materimk`";
		
		//$sql = $sql . "ORDER BY `tbl_skripsi_tahap`.`urut` ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function cek_new($jud){
		$sql = "SELECT judul 
			    from `db_ptiik_apps`.`tbl_file` 
			    where judul = '".$jud."'";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function cek_new_by_id($jud){
		$sql = "SELECT materi_id 
				from `db_ptiik_apps`.`tbl_file` 
				where judul = '".$jud."'";
		
		$result = $this->db->query( $sql );
		if(isset($result)){
			foreach($result as $dt){
				$id=$dt->materi_id;
			}
			return $id;
		}
	}
	
	function replace_materimk($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_materimk',$datanya);
	}
	
	//=============================FILE UPLOAD=================================
	
	function id_file($semester=NULL){

		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(file_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_file`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function cek_file_jenis($cons) {
		$sql = "SELECT jenis_file_id 
				FROM `db_ptiik_apps`.`tbl_jenisfile` 
				WHERE keterangan = '".$cons."'";
		$result = $this->db->getRow( $sql );
		//echo $result;
		$strresult = $result->jenis_file_id;
		//echo $strresult;
		return $strresult;
		
	}
	
	function cek_nama_file($term){
		$sql = "SELECT file_name
				FROM `db_ptiik_apps`.`tbl_file`
				WHERE file_name = '".$term."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->file_name;
			return $result;
		}
		
	}
	
	function get_locfile($term){
		$sql = "SELECT file_loc
				FROM `db_ptiik_apps`.`tbl_file`
				WHERE file_name = '".$term."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->file_loc;
			return $result;
		}
		
	}
	
	function fileid_bymd5($fileid){
		$sql = "SELECT file_id 
				FROM `db_ptiik_apps`.`tbl_file`
				WHERE MID( MD5(file_id), 6, 6) = '".$fileid."' 
					  OR mid(md5(file_id),9,7) = '".$fileid."'
					  OR file_id = '".$fileid."'
				";
		$ur = $this->db->getrow( $sql );
		if(isset($ur)){
			$result = $ur->file_id;
			return $result;
		}
		else return $result='0';
	}
	
	public function log_data($user, $action, $fileid){
		// $return_arr = array();
		// array_push($return_arr,$datanya); 
		
		$data['user_id'] = $user;
		$data['session'] = session_id();
		$data['last_update'] = date("Y-m-d H:i:s");
		$data['reference'] = $_SERVER['HTTP_REFERER'];
		//$data['table_name'] = $tablename;
		//$data['field'] = json_encode($return_arr); 
		$data['action'] = $action; 
		$data['file_id'] = $this->fileid_bymd5($fileid);
		
		$result = $this->db->insert('db_ptiik_apps`.`tbl_log', $data);
		
		if( ($result and !$this->db->getLastError()) ) 
		return true;
		else if(!$result) return false;
	}
	
	function replace_file($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_file',$datanya);
	}
}
?>