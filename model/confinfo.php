<?php
class model_confinfo extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function konfigurasi_akademik(){
		
		$sql = "SELECT
					*
				FROM
					`db_ptiik_apps`.`tbl_config`					
				WHERE is_aktif='1'
				";
	
		$result = $this->db->getRow( $sql );
		
		return $result;	

	}
		
	function toggle_sp($id=NULL) {
		$sql = "UPDATE db_ptiik_apps.tbl_krs_tmp SET is_approve  = CASE is_approve WHEN 0 THEN 1 WHEN 1 THEN 2 ELSE 1 END WHERE krs_id = '$id'";
		
		$result = $this->db->query( $sql );
		if( !$result )
			$this->error = $this->db->getLastError();
			
		$sql = "SELECT is_approve FROM db_ptiik_apps.tbl_krs_tmp WHERE krs_id = '$id'";
		$this->nstatus = $this->db->getVar($sql);	
			
		return $result;
	}
	
	function delete_sp($id=NULL){
		$sql = "DELETE FROM db_ptiik_apps.tbl_krs_tmp WHERE krs_id = '$id'";
		
		$result = $this->db->query( $sql );
		if( !$result )	$this->error = $this->db->getLastError();
		
		return $result;
	}
	
	function get_peminat_krs($semester=NULL, $id=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_krs_tmp.mkditawarkan_id,
				trim(db_ptiik_apps.tbl_krs_tmp.nama_mk) as `nama_mk`,
				db_ptiik_apps.tbl_krs_tmp.kode_mk,
				db_ptiik_apps.tbl_krs_tmp.sks,
				db_ptiik_apps.tbl_krs_tmp.tahun_akademik,
				Count(db_ptiik_apps.tbl_krs_tmp.mahasiswa_id) as `jml`
				FROM
				db_ptiik_apps.tbl_krs_tmp
				WHERE
				db_ptiik_apps.tbl_krs_tmp.is_approve <> '2'  				
				";
		if($semester) $sql.= " AND db_ptiik_apps.tbl_krs_tmp.tahun_akademik ='$semester' ";
		
		$sql.= "GROUP BY
					db_ptiik_apps.tbl_krs_tmp.mkditawarkan_id,
					trim(db_ptiik_apps.tbl_krs_tmp.nama_mk),
					db_ptiik_apps.tbl_krs_tmp.kode_mk,
					db_ptiik_apps.tbl_krs_tmp.sks,
					db_ptiik_apps.tbl_krs_tmp.tahun_akademik ORDER BY db_ptiik_apps.tbl_krs_tmp.nama_mk ASC ";
		if($id){
			//$sql = $sql . " WHERE db_ptiik_apps.tbl_mahasiswa.nim='0810680044' ";
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		return $result;
	}
	
	
	function replace_daftar_pendek($datanya){
		$result = $this->db->replace("db_ptiik_apps`.`tbl_krs_tmp", $datanya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
	
	function update_daftar_pendek($datanya, $idnya){
		$result = $this->db->update("db_ptiik_apps`.`tbl_krs_tmp", $datanya, $idnya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
	
	function get_reg_daftar_pendek($semester=NULL, $mhs=NULL, $mkid=NULL){
		$sql = "SELECT krs_id FROM db_ptiik_apps.tbl_krs_tmp WHERE tahun_akademik ='".$semester."' AND mahasiswa_id='".$mhs."' AND mkditawarkan_id = '".$mkid."'";
		$row = $this->db->getRow( $sql );	
		
		if($row){
			$result = $row->krs_id;
		}else{
		
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '00000' , CAST(IFNULL(MAX(CAST(right(krs_id,5) AS unsigned)), 0) + 1 AS unsigned)),5)) as `data` 
					FROM db_ptiik_apps.tbl_krs_tmp WHERE left(krs_id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	
	function get_reg_daftar_pendek_tmp($semester=NULL, $mhs=NULL, $mkid=NULL){
		$sql = "SELECT krs_id FROM db_ptiik_apps.tbl_krs_tmp WHERE tahun_akademik ='".$semester."' AND mahasiswa_id='".$mhs."' AND mkditawarkan_id = '".$mkid."'";
		$result = $this->db->getRow( $sql );	
		
		
		return $result;
	}
	
	function delete_daftar_pendek($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_krs_tmp',$datanya);
	}
	
	function get_krs_tmp_mhs($semester=NULL, $mhs=NULL){
		$sql= "SELECT DISTINCT
			db_ptiik_apps.tbl_krs_tmp.krs_id,
			db_ptiik_apps.tbl_krs_tmp.mahasiswa_id,
			db_ptiik_apps.tbl_krs_tmp.mkditawarkan_id,
			db_ptiik_apps.tbl_krs_tmp.nama_mk,
			db_ptiik_apps.tbl_krs_tmp.kode_mk,
			db_ptiik_apps.tbl_krs_tmp.tgl_daftar,
			db_ptiik_apps.tbl_krs_tmp.sks,
			db_ptiik_apps.tbl_krs_tmp.tahun_akademik,
			db_ptiik_apps.tbl_krs_tmp.inf_semester,
			db_ptiik_apps.tbl_krs_tmp.nilai_akhir,
			db_ptiik_apps.tbl_krs_tmp.kelas,
			db_ptiik_apps.tbl_krs_tmp.inf_huruf,
			db_ptiik_apps.tbl_krs_tmp.is_approve,
			db_ptiik_apps.tbl_krs_tmp.catatan,
			db_ptiik_apps.tbl_krs_tmp.inf_biaya,
			db_ptiik_apps.tbl_krs_tmp.user_id,
			db_ptiik_apps.tbl_krs_tmp.last_update
			FROM
			db_ptiik_apps.tbl_krs_tmp WHERE 1 ";
			
		if($semester) $sql.= " AND db_ptiik_apps.tbl_krs_tmp.tahun_akademik ='$semester' ";
		
		if($mhs) $sql.= " AND db_ptiik_apps.tbl_krs_tmp.mahasiswa_id ='$mhs' ";
		
		$result = $this->db->query( $sql );
		
		return $result;
	}
	

	
	
	function kalender_akademik($str=NULL, $semester=NULL, $tgl=NULL){
			
		$sql = "SELECT
					db_ptiik_apps.tbl_kalenderakademik.kalender_id,
					db_ptiik_apps.tbl_kalenderakademik.jenis_kegiatan_id,
					db_ptiik_apps.tbl_kalenderakademik.tahun_akademik,
					db_ptiik_apps.tbl_kalenderakademik.tgl_mulai,
					db_ptiik_apps.tbl_kalenderakademik.tgl_selesai,
					db_ptiik_apps.tbl_kalenderakademik.keterangan,
					db_ptiik_apps.tbl_kalenderakademik.is_aktif
					FROM
					db_ptiik_apps.tbl_kalenderakademik 
					WHERE tbl_kalenderakademik.is_aktif='1'  
					";
		if($tgl){
			$sql = $sql. " AND 
						('".$tgl."' BETWEEN db_ptiik_apps.tbl_kalenderakademik.tgl_mulai AND db_ptiik_apps.tbl_kalenderakademik.tgl_selesai) ";
		}
		
		if($str){
			$sql = $sql. " AND db_ptiik_apps.tbl_kalenderakademik.jenis_kegiatan_id = '".$str."' ";
		}
		
		if($semester){
			$sql = $sql. " AND db_ptiik_apps.tbl_kalenderakademik.tahun_akademik = '".$semester."' ";
		}
		
		
		$result = $this->db->getRow( $sql );
		
		return $result;	

	}
	
	
		function read_report_daftar_ulang($semester=NULL, $id=NULL, $str=NULL){
		$sql = "SELECT
					concat(db_ptiik_apps.tbl_tahunakademik.tahun,' ', db_ptiik_apps.tbl_tahunakademik.is_ganjil,' ',db_ptiik_apps.tbl_tahunakademik.is_pendek) as `semester`,
					db_ptiik_apps.tbl_daftarulang.mahasiswa_id,
					db_ptiik_apps.tbl_daftarulang.nama,
					db_ptiik_apps.tbl_prodi.keterangan AS prodi,
					db_ptiik_apps.tbl_daftarulang.jalur_masuk,
					db_ptiik_apps.tbl_daftarulang.email,
					db_ptiik_apps.tbl_daftarulang.alamat_malang,
					db_ptiik_apps.tbl_daftarulang.telp,
					db_ptiik_apps.tbl_daftarulang.hp,
					db_ptiik_apps.tbl_daftarulang.orang_tua,
					db_ptiik_apps.tbl_daftarulang.alamat_ortu,
					db_ptiik_apps.tbl_daftarulang.email_ortu,
					db_ptiik_apps.tbl_daftarulang.telp_ortu,
					db_ptiik_apps.tbl_daftarulang.hp_ortu,
					db_ptiik_apps.tbl_daftarulang.is_valid,
					db_ptiik_apps.tbl_daftarulang.alamat_surat,
					db_ptiik_apps.tbl_daftarulang.catatan
					FROM
					db_ptiik_apps.tbl_daftarulang
					INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_daftarulang.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_daftarulang.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id WHERE 1 = 1 ";
		if($semester){
			$sql = $sql. " AND db_ptiik_apps.tbl_tahunakademik.tahun_akademik='".$semester."' ";
		}
		
		if($str){
			$sql = $sql. " AND db_ptiik_apps.tbl_daftarulang.is_valid='".$str."' ";
		}
		
		if($id){
			//$sql = $sql . " WHERE db_ptiik_apps.tbl_mahasiswa.nim='0810680044' ";
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;	

	}
	
	function read_report_sp($id=NULL, $str=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_mahasiswa.angkatan,
				db_ptiik_apps.tbl_krs_tmp.mahasiswa_id as `nim`,
				db_ptiik_apps.tbl_mahasiswa.nama,
				trim(db_ptiik_apps.tbl_krs_tmp.nama_mk) AS `nama_mk`,
				db_ptiik_apps.tbl_krs_tmp.sks,
				db_ptiik_apps.tbl_karyawan.nama AS pembimbing
				FROM
				db_ptiik_apps.tbl_krs_tmp
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_krs_tmp.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_mahasiswa.dosen_pembimbing = db_ptiik_apps.tbl_karyawan.karyawan_id WHERE 1 = 1 ";
		if($str){
			$sql = $sql. " AND db_ptiik_apps.tbl_krs_tmp.is_valid='".$str."' ";
		}
		
		$sql.= " ORDER BY db_ptiik_apps.tbl_mahasiswa.angkatan ASC, db_ptiik_apps.tbl_mahasiswa.mahasiswa_id ASC ";
		if($id){
			//$sql = $sql . " WHERE db_ptiik_apps.tbl_mahasiswa.nim='0810680044' ";
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;	

	}
	
	function read_report_mk($id=NULL, $str=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_mahasiswa.angkatan,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_prodi.keterangan AS prodi,
					db_ptiik_apps.tbl_kuesioner_mhs.semester,
					db_ptiik_apps.tbl_kuesioner_mhs.nama_mk,
					db_ptiik_apps.tbl_kuesioner_mhs.kode_mk,
					db_ptiik_apps.tbl_kuesioner_mhs.sks,
					db_ptiik_apps.tbl_kuesioner_mhs.tgl_isi
					FROM
					db_ptiik_apps.tbl_kuesioner_mhs
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
					 WHERE 1 = 1 ";
		if($str){
			$sql = $sql. " AND db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id='".$str."' ";
		}
		
		$sql.= " ORDER BY db_ptiik_apps.tbl_mahasiswa.angkatan ASC, db_ptiik_apps.tbl_mahasiswa.mahasiswa_id ASC ";
		if($id){
			//$sql = $sql . " WHERE db_ptiik_apps.tbl_mahasiswa.nim='0810680044' ";
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;	

	}
	
	
	
	function get_mkditawarkan_kuesioner($kuesioner=NULL, $prodi=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_kuesioner_detail.detail_id,
				db_ptiik_apps.tbl_kuesioner_detail.matakuliah_id as `id`,
				db_ptiik_apps.tbl_kuesioner_detail.matakuliah_id as `mkditawarkan_id`,
				db_ptiik_apps.tbl_kuesioner_detail.kuesioner_id,
				db_ptiik_apps.tbl_kuesioner_detail.nama_mk as `namamk`,
				db_ptiik_apps.tbl_kuesioner_detail.kode_mk,
				db_ptiik_apps.tbl_kuesioner_detail.sks,
				concat(`db_ptiik_apps`.`tbl_kuesioner_detail`.`kode_mk`,' - ' ,`db_ptiik_apps`.`tbl_kuesioner_detail`.`nama_mk`) as `value`,
				db_ptiik_apps.tbl_kuesioner_detail.prodi_id
				FROM
				db_ptiik_apps.tbl_kuesioner_detail WHERE 1 
				";
		if($kuesioner) $sql.= " AND db_ptiik_apps.tbl_kuesioner_detail.kuesioner_id='$kuesioner' ";
		if($prodi) $sql.= " AND db_ptiik_apps.tbl_kuesioner_detail.prodi_id='$prodi' ";
		
		$sql.= " ORDER BY db_ptiik_apps.tbl_kuesioner_detail.nama_mk ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	function get_reg_mk_pilihan($semester=NULL, $mhs=NULL, $mkid=NULL, $kuesioner=NULL){
		$sql = "SELECT id FROM db_ptiik_apps.tbl_kuesioner_mhs WHERE semester ='".$semester."' AND mahasiswa_id='".$mhs."' AND matakuliah_id = '".$mkid."' AND kuesioner_id ='$kuesioner' ";
		$row = $this->db->getRow( $sql );	
		
		if($row){
			$result = $row->id;
		}else{
		
			$kode = date("Ym");
					
			$sql="SELECT concat('".$kode."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_kuesioner_mhs WHERE left(id, 6)='".$kode."'";		
			$dt = $this->db->getRow( $sql );
			
			$result = $dt->data;
		}
		
		return $result;
	}
	
	
	function replace_mk_pilihan($datanya){
		$result = $this->db->replace("db_ptiik_apps`.`tbl_kuesioner_mhs", $datanya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
	
	
	function update_mhs($datanya, $idnya){
		$result = $this->db->update("db_ptiik_apps`.`tbl_mahasiswa", $datanya, $idnya);
		if( $result ) {			
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}	
	}
	
	function kalender_akademik_kuesioner($tgl=NULL){
			
		$sql = "SELECT
					db_ptiik_apps.tbl_kuesioner_mk.kuesioner_id,
					db_ptiik_apps.tbl_kuesioner_mk.judul,
					db_ptiik_apps.tbl_kuesioner_mk.keterangan,
					db_ptiik_apps.tbl_kuesioner_mk.tahun_akademik,
					db_ptiik_apps.tbl_kuesioner_mk.tahun,
					db_ptiik_apps.tbl_kuesioner_mk.is_ganjil,
					db_ptiik_apps.tbl_kuesioner_mk.is_pendek,
					db_ptiik_apps.tbl_kuesioner_mk.tgl_mulai,
					db_ptiik_apps.tbl_kuesioner_mk.tgl_selesai,
					db_ptiik_apps.tbl_kuesioner_mk.is_aktif
					FROM
					db_ptiik_apps.tbl_kuesioner_mk 
					WHERE is_aktif = '1' 
					";
		if($tgl){
			$sql = $sql. " AND 
						('".$tgl."' BETWEEN db_ptiik_apps.tbl_kuesioner_mk.tgl_mulai AND db_ptiik_apps.tbl_kuesioner_mk.tgl_selesai) ";
		}
		
		/*if($str){
			$sql = $sql. " AND db_ptiik_apps.tbl_kalenderakademik.jenis_kegiatan_id = '".$str."' ";
		}
		
		if($semester){
			$sql = $sql. " AND db_ptiik_apps.tbl_kalenderakademik.tahun_akademik = '".$semester."' ";
		}*/
		
		$result = $this->db->getRow( $sql );
		return $result;	

	}
	
	

	function get_peminat_mk_pilihan($kuesioner=NULL, $id=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_kuesioner_mhs.nama_mk,
				db_ptiik_apps.tbl_kuesioner_mhs.kode_mk,
				db_ptiik_apps.tbl_kuesioner_mhs.sks,
				Count(db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id) as `jml`,
					db_ptiik_apps.tbl_prodi.keterangan AS prodi
					FROM
					db_ptiik_apps.tbl_kuesioner_mhs
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id WHERE 1 ";
		if($kuesioner) $sql.= " AND db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id ='$kuesioner' ";	
		
		$sql.= "GROUP BY
			db_ptiik_apps.tbl_kuesioner_mhs.matakuliah_id,
			db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id,
			db_ptiik_apps.tbl_prodi.prodi_id";
		if($id){			
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}		
	
		return $result;
	}
	
	function get_mhs_daftar_krs($semester=NULL, $isvalid=NULL){
		$sql = "SELECT DISTINCT
				db_ptiik_apps.tbl_krs_tmp.mahasiswa_id,
				db_ptiik_apps.tbl_krs_tmp.tahun_akademik,
				db_ptiik_apps.tbl_krs_tmp.is_valid,
				db_ptiik_apps.tbl_krs_tmp.tgl_daftar,
				db_ptiik_apps.tbl_mahasiswa.nama,
				db_ptiik_apps.tbl_mahasiswa.nim,
				db_ptiik_apps.tbl_prodi.prodi_id,
				db_ptiik_apps.tbl_prodi.keterangan AS prodi,
				db_ptiik_apps.tbl_krs_tmp.user_id,
				db_ptiik_apps.tbl_krs_tmp.last_update,
				db_coms.coms_user.`name` AS `user`
				FROM
				db_ptiik_apps.tbl_krs_tmp
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_krs_tmp.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
				LEFT JOIN db_coms.coms_user ON db_ptiik_apps.tbl_krs_tmp.user_id = db_coms.coms_user.id

				WHERE 1
				";
		if($semester) $sql.= " AND db_ptiik_apps.tbl_krs_tmp.tahun_akademik ='$semester' ";
		
		if(($isvalid!="-") && ($isvalid)) $sql.= " AND db_ptiik_apps.tbl_krs_tmp.is_valid ='$isvalid' ";
		
		$result = $this->db->query( $sql );
	
		return $result;
	}
	
	function get_tahun_akademik_name($str=NULL){
		$sql= "SELECT
				db_ptiik_apps.tbl_tahunakademik.tahun,
				db_ptiik_apps.tbl_tahunakademik.is_pendek,
				db_ptiik_apps.tbl_tahunakademik.is_ganjil
				FROM
				db_ptiik_apps.tbl_tahunakademik WHERE tahun_akademik='".$str."' ";
		
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	function get_mk_pilihan_by_mhs($kuesioner=NULL, $mhs=NULL){
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id,
					db_ptiik_apps.tbl_kuesioner_mhs.semester,
					db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id,
					db_ptiik_apps.tbl_mahasiswa.nim,
					db_ptiik_apps.tbl_mahasiswa.nama,
					db_ptiik_apps.tbl_kuesioner_mhs.tgl_isi,
					db_ptiik_apps.tbl_prodi.keterangan AS prodi
					FROM
					db_ptiik_apps.tbl_kuesioner_mhs
					INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
					INNER JOIN db_ptiik_apps.tbl_prodi ON db_ptiik_apps.tbl_mahasiswa.prodi_id = db_ptiik_apps.tbl_prodi.prodi_id
				 WHERE 1 ";
		if($kuesioner) $sql.= " AND db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id ='$kuesioner' ";
		
		if($mhs) $sql.= " AND db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id ='$mhs' ";
		
		$result = $this->db->query( $sql );
	
		return $result;

	}
	
	function get_mk_pilihan_mhs($kuesioner=NULL, $mhs=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_kuesioner_mhs.id,
					db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id,
					db_ptiik_apps.tbl_kuesioner_mhs.semester,
					db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id,
					db_ptiik_apps.tbl_kuesioner_mhs.matakuliah_id,
					db_ptiik_apps.tbl_kuesioner_mhs.nama_mk,
					db_ptiik_apps.tbl_kuesioner_mhs.kode_mk,
					db_ptiik_apps.tbl_kuesioner_mhs.sks
					FROM
					db_ptiik_apps.tbl_kuesioner_mhs WHERE 1 ";
		if($kuesioner) $sql.= " AND db_ptiik_apps.tbl_kuesioner_mhs.kuesioner_id ='$kuesioner' ";
		
		if($mhs) $sql.= " AND db_ptiik_apps.tbl_kuesioner_mhs.mahasiswa_id ='$mhs' ";
		
		$result = $this->db->query( $sql );
	
		return $result;

	}
	
	/*----------------- end --------------------*/
	
	function get_fakultas($id=NULL){
		$sql = "SELECT mid(md5(fakultas_id),6,6) as id, keterangan, fakultas_id, mid(md5(fakultas_id),6,6) as fakultasid,  fakultas_id as hid_id
				FROM `db_ptiik_apps`.`tbl_fakultas` WHERE 1 ";	
		if($id){
			$sql .= " AND  fakultas_id = '".$id."' ";
		}	
		
		$sql.= "  ORDER BY keterangan ASC ";
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_cabangub(){
		$sql = "SELECT `cabang_id`, `keterangan`
				FROM `db_ptiik_apps`.`tbl_cabang` 
				ORDER BY keterangan ASC
				";		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function get_fakultas_id($id){
		$sql = "SELECT fakultas_id
				FROM `db_ptiik_apps`.`tbl_fakultas`
				WHERE mid(md5(fakultas_id),6,6) = '".$id."'
				";		
		$id = $this->db->getrow( $sql );
		if(isset($id)){
		return $result = $id->fakultas_id;}
	}
	
	
	function get_unit_content($fakultasid=NULL,$param=NULL){
		$sql = "SELECT 
					tbl_unit_kerja.unit_id,
					tbl_unit_kerja.keterangan,
					tbl_unit_kerja.fakultas_id,
        			tbl_fakultas.keterangan as fakultas
				FROM db_ptiik_apps.tbl_unit_kerja
				LEFT JOIN db_ptiik_apps.tbl_fakultas ON tbl_fakultas.fakultas_id = tbl_unit_kerja.fakultas_id
				WHERE 1
			   ";
		
		if($fakultasid){
			$sql .= " AND tbl_unit_kerja.fakultas_id = '".$fakultasid."' ";
		}
		
		if($param=='getfakultas'){
			$sql .= " GROUP BY tbl_unit_kerja.fakultas_id ";
		}
			   
		return $this->db->query($sql);
	}
	
	/* master konfigurasi */
	function read() {		
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_config`.`tahun`,
					`db_ptiik_apps`.`tbl_config`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_config`.`is_pendek`,
					`db_ptiik_apps`.`tbl_config`.`kode_skripsi`,
					`db_ptiik_apps`.`tbl_config`.`is_aktif`
				FROM
					`db_ptiik_apps`.`tbl_config`					
				WHERE is_aktif='1'
				";
	
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jenjang pendidikan */
	function  get_jenjang_pendidikan(){
		$sql = "SELECT `db_ptiik_apps`.`tbl_jenjangpendidikan`.`jenjang_pendidikan` FROM `db_ptiik_apps`.`tbl_jenjangpendidikan`";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jabatan */
	function  get_jabatan(){
		$sql = "SELECT `db_ptiik_apps`.`tbl_jabatan`.`jabatan_id`, `db_ptiik_apps`.`tbl_jabatan`.`keterangan` FROM `db_ptiik_apps`.`tbl_jabatan` ORDER BY urut ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester  */
	function get_semester(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`,
					CONCAT(`tbl_tahunakademik`.`tahun` , ' ', `db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil` , ' ' , `db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`) as semester
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					ORDER BY 
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik` DESC
					";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master semester aktif */
	function get_semester_aktif(){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun_akademik`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`tahun`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif`,
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`,
					CONCAT(`tbl_tahunakademik`.`tahun` , ' ', `db_ptiik_apps`.`tbl_tahunakademik`.`is_ganjil` , ' ' , `db_ptiik_apps`.`tbl_tahunakademik`.`is_pendek`) as semester
					FROM
					`db_ptiik_apps`.`tbl_tahunakademik`
					WHERE
					`db_ptiik_apps`.`tbl_tahunakademik`.`is_aktif` =  '1'
					";
		$result = $this->db->getRow( $sql );
		
		return $result;	
	}
	
	/* master program studi atau jurusan*/
	function get_prodi($term=NULL){
		$sql = "SELECT prodi_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_prodi` WHERE 1";
		if($term){
			$sql .= " AND keterangan like '%".$term."%' ";
		}
		
		// echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master unit kerja atau struktur*/
	function get_unit($term=NULL, $jenis=NULL){
		$sql= "SELECT unit_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_unit_kerja` WHERE  1 = 1 ";
		
		if($term){
			$sql = $sql . "AND nama like '%".$term."%'  ";
		}
		
		if($jenis){
			$sql = $sql . "AND jenis = '".$jenis."'  ";
		}
		
		$sql = $sql . "ORDER BY `db_ptiik_apps`.`tbl_unit_kerja`.`jenis` ASC, `db_ptiik_apps`.`tbl_unit_kerja`.`nama` ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	
	/*master mahasiswa */
	function get_mahasiswa($term=NULL){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `value` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_json_mhs($term=NULL){
		$sql= "SELECT mahasiswa_id as `id`, concat(nim,' - ',nama) as `text` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mahasiswa */
	function get_nama_mahasiswa($term=NULL){
		$sql= "SELECT nama FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE mahasiswa_id = '".$term."'  ";
		$result = $this->db->query( $sql );
	
		if($result){
			foreach($result as $dt){
				$strid = $dt->nama;
			}
		}else{
			$strid="-";
		}
		
		return $strid;	
	}

/*master id dosen */
	function get_id_dosen($str=NULL){
		$sql= "SELECT karyawan_id FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE nama = '".$str."' ";
		$result = $this->db->getRow( $sql );
		
		if($result){
			$strid = $result->karyawan_id;
		}else{
			$strid="-";
		}
		
		return $strid;			
	}
		
	
	/*master dosen */
	function get_dosen($term=NULL){
		$sql= "SELECT karyawan_id as `id`, nama as `value` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif='aktif' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master dosen */
	function get_staff($term=NULL){
		$sql= "SELECT nama as `tag` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_aktif='aktif' 
				ORDER BY nama ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	function get_mhs($term=NULL){
		$sql= "SELECT nama as `tag` FROM `db_ptiik_apps`.`tbl_mahasiswa` 
					WHERE (concat(nim,'-',nama)) like '%".$term."%' 
				ORDER BY nim DESC, nama ASC ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master pengampu */
	function get_pengampu($term=NULL){
		$sql= "SELECT nama as `value`, karyawan_id as `id` FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE (concat(nik,'-',nama)) like '%".$term."%' AND is_dosen='1' AND is_aktif='aktif' 
				ORDER BY nama ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master nama mk */
	function get_namamk($term=NULL){
		$sql= "SELECT namamk_id as `id`, keterangan as `value` FROM `db_ptiik_apps`.`tbl_namamk` 
					WHERE keterangan like '%".$term."%' 
				ORDER BY keterangan ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/*master mk */
	function get_mk($term=NULL){
		$sql= "SELECT `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` as `id`, 
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ', `db_ptiik_apps`.`tbl_namamk`.`keterangan`,'(',`db_ptiik_apps`.`tbl_matakuliah`.`sks`,' sks)') as `value` FROM `db_ptiik_apps`.`tbl_matakuliah`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id` 
					WHERE concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
				ORDER BY `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id` ASC limit 0,20 ";
		$result = $this->db->query( $sql );
		
		return $result;	
	}	
	
	/* master jam mulai*/
	function  get_jam_mulai($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_mulai as `value` FROM db_ptiik_apps.tbl_jam 
				WHERE jam_mulai like '%".$term."%'
				ORDER BY jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master jam selesai*/
	function  get_jam_selesai($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_selesai as `value` FROM db_ptiik_apps.tbl_jam  WHERE jam_selesai like '%".$term."%' ORDER BY jam_selesai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master hari*/
	function  get_hari($term=NULL){
		$sql = "SELECT id as `id`, hari as `value` FROM db_ptiik_apps.tbl_hari WHERE hari like '%".$term."%' ORDER BY tbl_hari.id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master get nama kegiatan */
	function  get_nama_kegiatan($str=NULL){
		$sql = "SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id = '".$str."' ";
		$result = $this->db->query( $sql );
		
		foreach($result as $dt):
			$strvalue	= $dt->keterangan;
		endforeach;
		
		return $strvalue;
	}
	
	
	/* master jam general*/
	function  get_blok_waktu($term=NULL){
		$sql = "SELECT jam_mulai as `id`, jam_mulai as `value` FROM db_ptiik_apps.tbl_blokwaktu
				WHERE jam_mulai like '%".$term."%'
				ORDER BY tbl_blokwaktu.jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master ruang*/
	function  get_ruang($term=NULL, $kategori=NULL){
		$sql = "SELECT ruang as `id`, concat(ruang, ' - ', keterangan) as `value` FROM db_ptiik_apps.tbl_ruang WHERE 1=1 ";
		
		if($kategori){
			$sql = $sql . "AND kategori_ruang = '".$kategori."' ";
		}
		
		if($term){
			$sql = $sql . "AND keterangan like '%".$term."%' ";
		}
		
		$sql = $sql . "ORDER BY ruang ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kategori ruang*/
	function  get_kategori_ruang($term=NULL){
		$sql = "SELECT kategori_ruang as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_kategoriruang WHERE keterangan like '%".$term."%' ORDER BY kategori_ruang ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kategori ruang*/
	function  get_kategori_ruang_aktif($term=NULL){
		$sql = "SELECT kategori_ruang as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_kategoriruang WHERE kategori_ruang IN ('kuliah', 'lab', 'seminar') AND keterangan like '%".$term."%' ORDER BY kategori_ruang ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/* master kelas*/
	function  get_kelas($term){
		$sql = "SELECT kelas_id as `id`, kelas_id as `value` FROM db_ptiik_apps.tbl_kelas WHERE keterangan like '%".$term."%' ORDER BY kelas_id ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master nama dosen */
	function get_nama_dosen($id){
		$sql= "SELECT nama , karyawan_id ,nik FROM `db_ptiik_apps`.`tbl_karyawan` 
					WHERE karyawan_id = '".$id."' ";
		$result = $this->db->query( $sql );
		
		
		foreach($result as $dt):
			$str	= $dt->nama;
		endforeach;
		
		return $str;	
		
	}
	
		
	/*master jadwal dosen */
	function get_jadwal_dosen(){
		$sql="SELECT
				`vw_jadwaldosen`.`kode_mk`,
				`vw_jadwaldosen`.`namamk`,
				`vw_jadwaldosen`.`sks`,
				`vw_jadwaldosen`.`kurikulum`,
				`vw_jadwaldosen`.`tahun`,
				`vw_jadwaldosen`.`is_ganjil`,
				`vw_jadwaldosen`.`is_pendek`,
				`vw_jadwaldosen`.`is_koordinator`,
				`vw_jadwaldosen`.`nik`,
				`vw_jadwaldosen`.`nama`,
				`vw_jadwaldosen`.`gelar_awal`,
				`vw_jadwaldosen`.`gelar_akhir`,
				`vw_jadwaldosen`.`kelas_id`,
				`vw_jadwaldosen`.`jam_mulai`,
				`vw_jadwaldosen`.`jam_selesai`,
				`vw_jadwaldosen`.`hari`,
				`vw_jadwaldosen`.`ruang`,
				`vw_jadwaldosen`.`prodi_id`
				FROM
				`db_ptiik_apps`.`vw_jadwaldosen`
				";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk ditawarkan */
	function get_mkditawarkan($ispraktikum=NULL,$semester=NULL,$term=NULL){
		$sql="SELECT DISTINCT 
				`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id` as `id`,
				concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,' - ' ,`db_ptiik_apps`.`tbl_namamk`.`keterangan`) as `value`,
					`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,
					`db_ptiik_apps`.`tbl_namamk`.`keterangan` AS `namamk`,
					`db_ptiik_apps`.`tbl_matakuliah`.`sks`,
					`db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
				FROM
				`db_ptiik_apps`.`tbl_mkditawarkan`
				Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
				Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`					
			  WHERE  concat(`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`, '-', `db_ptiik_apps`.`tbl_namamk`.`keterangan`) like '%".$term."%' 
			";
			
		if($ispraktikum!=""){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`is_praktikum`= '".$ispraktikum."' ";
		}
		if($semester){
			$sql = $sql . " AND `db_ptiik_apps`.`tbl_mkditawarkan`.`tahun_akademik` ='".$semester."' ";
		}
				
		$result = $this->db->query( $sql );
		echo $sql;
		return $result;	
	}
	
	/* master jenis kegiatan*/
	function  get_jenis_kegiatan(){
		$sql = "SELECT jenis_kegiatan_id as `id`, keterangan as `value` FROM db_ptiik_apps.tbl_jeniskegiatan  ORDER BY keterangan ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	/*master mk diampu */
	
	
	function get_mk_diampu($id=NULL,$mk=NULL){
		$sql = "SELECT
					`db_ptiik_apps`.`tbl_matakuliah`.`kode_mk`,
					`db_ptiik_apps`.`tbl_namamk`.`keterangan` AS `namamk`,
					`db_ptiik_apps`.`tbl_matakuliah`.`sks`,
					`db_ptiik_apps`.`tbl_dosenpengampu`.`is_koordinator`,
					`db_ptiik_apps`.`tbl_dosenpengampu`.`dosen_id`,
					`db_ptiik_apps`.`tbl_dosenpengampu`.`karyawan_id`,
					`db_ptiik_apps`.`tbl_dosenpengampu`.`mkditawarkan_id`,
					`db_ptiik_apps`.`tbl_karyawan`.`nama`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_awal`,
					`db_ptiik_apps`.`tbl_karyawan`.`gelar_akhir`
				FROM
					`db_ptiik_apps`.`tbl_mkditawarkan`
					Inner Join `db_ptiik_apps`.`tbl_matakuliah` ON `db_ptiik_apps`.`tbl_mkditawarkan`.`matakuliah_id` = `db_ptiik_apps`.`tbl_matakuliah`.`matakuliah_id`
					Inner Join `db_ptiik_apps`.`tbl_namamk` ON `db_ptiik_apps`.`tbl_matakuliah`.`namamk_id` = `db_ptiik_apps`.`tbl_namamk`.`namamk_id`
					Inner Join `db_ptiik_apps`.`tbl_dosenpengampu` ON `db_ptiik_apps`.`tbl_dosenpengampu`.`mkditawarkan_id` = `db_ptiik_apps`.`tbl_mkditawarkan`.`mkditawarkan_id`
					Inner Join `db_ptiik_apps`.`tbl_karyawan` ON `db_ptiik_apps`.`tbl_dosenpengampu`.`karyawan_id` = `db_ptiik_apps`.`tbl_karyawan`.`karyawan_id`
				WHERE `db_ptiik_apps`.`tbl_dosenpengampu`.`is_aktif` = '1' 
				";
				
		if($id){
			$sql=$sql . " AND (mid(md5(`db_ptiik_apps`.`tbl_dosenpengampu`.`karyawan_id`),5,5)='".$id."' OR `db_ptiik_apps`.`tbl_dosenpengampu`.`karyawan_id`='".$id."') ";
		}

		if($mk){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_dosenpengampu`.`mkditawarkan_id`='".$mk."' ";
		}		

		$result = $this->db->query($sql);
		
		return $result;
	}
	
	
	function get_dosen_id($id=NULL,$mkid=NULL){
		$sql = "SELECT dosen_id FROM db_ptiik_apps.tbl_dosenpengampu where `db_ptiik_apps`.`tbl_dosenpengampu`.`karyawan_id`='".$id."' AND 
				`db_ptiik_apps`.`tbl_dosenpengampu`.`mkditawarkan_id`='".$mkid."' AND `db_ptiik_apps`.`tbl_dosenpengampu`.`is_aktif` = '1' ";
		$result = $this->db->query($sql);
		
		foreach($result as $dt):
			$dosen	= $dt->dosen_id;
		endforeach;
		
		return $dosen;
	}
	
	function replace_conf($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_config',$datanya);
	}
	
	function replace_semester($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_tahunakademik',$datanya);
	}
	
	function update_conf($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_config',$datanya,$idnya);
	}
		
	function update_semester($datanya,$idnya) {
		return $this->db->update('db_ptiik_apps`.`tbl_tahunakademik',$datanya,$idnya);
	}
	
	public function log($tablename, $datanya, $username, $action){
		$return_arr = array();
		array_push($return_arr,$datanya);		
			
		$data['user']		= $username;
		$data['session']	= session_id();
		$data['tgl']		= date("Y-m-d H:i:s");
		$data['reference']	= $_SERVER['HTTP_REFERER'];
		$data['table_name']	= $tablename;
		$data['field']		= json_encode($return_arr);	
		$data['action']		= $action;			
		
		$result = $this->db->insert("coms`.`coms_log", $data);
		
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) return false;
	}
	
	public function getHari($str=NULL){
		switch($str){
			case '1':
				$strout	= 'senin';
			break;
			case '2':
				$strout	= 'selasa';
			break;
			case '3':
				$strout	= 'rabu';
			break;
			case '4':
				$strout	= 'kamis';
			break;
			case '5':
				$strout	= 'jumat';
			break;
			case '6':
				$strout	= 'sabtu';
			break;
			case '7':
				$strout	= 'minggu';
			break;
		}
		
		return $strout;
	}
	
	public function getNHari($str=NULL){
		switch(strToLower($str)){
			case 'senin':
				$strout	= '1';
			break;
			case 'selasa':
				$strout	= '2';
			break;
			case 'rabu':
				$strout	= '3';
			break;
			case 'kamis':
				$strout	= '4';
			break;
			case 'jumat':
				$strout	= '5';
			break;
			case 'sabtu':
				$strout	= '6';
			break;
			case 'minggu':
				$strout	= '7';
			break;
		}
		
		return $strout;
	}
	
	public function replace_jadwal_ruang($datanya) {
		//var_dump($datanya);
		return $this->db->replace('db_ptiik_apps`.`tbl_jadwalruang',$datanya);
	}
	
	public function replace_detail_jadwal_ruang($datanya) {
		//var_dump($datanya);
		return $this->db->replace('db_ptiik_apps`.`tbl_detailjadwalruang',$datanya);
	}
	
	function delete_jadwal_ruang($datanya){
		//var_dump($datanya);
		return $this->db->delete('db_ptiik_apps`.`tbl_jadwalruang',$datanya);
	}
	
	function delete_jadwal_ruang_detail($datanya){
		return $this->db->delete('db_ptiik_apps`.`tbl_detailjadwalruang',$datanya);
	}
	
	
	
}