<?php

class model_user extends model {
	
	public $error = NULL;
	
	
	function __construct() {
		
		// we require database object for this model
		// so execute parent construct 
		
		parent::__construct();
	}
	
	// user table installation method
	public function install() {
		$sql = "CREATE TABLE `coms_user` (
				  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				  `username` varchar(50) NOT NULL,
				  `password` varchar(32) NOT NULL,
				  `name` varchar(45) DEFAULT NULL,
				  `email` varchar(45) DEFAULT NULL,
				  `level` smallint(6) NOT NULL DEFAULT '0',
				  `status` tinyint(4) NOT NULL DEFAULT '1',
				  `karyawan_id` varchar(30) NOT NULL DEFAULT '-',
				  PRIMARY KEY (`id`),
				  UNIQUE KEY `u_user` (`username`,`password`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";	
				
		// installs		
		$this->db->query( $sql );
	}
	
	function verify($password, $hashedPassword) {
		return crypt($password, $hashedPassword);
		
	}
	
	function authcoment( $username, $password ) {
		
		// escaping submitted username and hashed password
		$username = $this->db->escape( $username );
		
		$password1=md5($this->db->escape( $password ));
		
		// build authentication query
		$sql1 = "SELECT password FROM db_coms.coms_user 
				WHERE (username = '$username' OR email = '$username') 
					AND status = 1 ";					
		$dt = $this->db->getRow( $sql1 );
		if($dt){
			$hash 	 = $dt->password;
			$password= $this->verify($this->db->escape( $password ), $hash);
		
			$sql = "SELECT id, name, email, level , karyawan_id, mahasiswa_id, username,
						(SELECT group_concat(description) FROM db_coms.coms_level WHERE coms_level.level IN (coms_user.level )) as `role`,
						 (SELECT group_concat(module) FROM db_coms.coms_level_module WHERE coms_level_module.level IN (coms_user.level)) as `module`,
						 (SELECT group_concat(distinct menu_id) FROM db_coms.coms_level_menu WHERE coms_level_menu.level IN (coms_user.level)) as `menu`
						
					FROM db_coms.coms_user 
					WHERE (username = '$username' OR email = '$username') 
						AND password = '$password' 
						AND status = 1 ";
						
			$result = $this->db->getRow( $sql );
			
			return $result;
		}else{
			return $dt;
		}
	}
	public function authenticate( $username, $password ) {
		
		// escaping submitted username and hashed password
		$username = $this->db->escape( $username );
		
		$password1=md5($this->db->escape( $password ));
		
		// build authentication query
		$sql1 = "SELECT password FROM db_coms.coms_user 
				WHERE (username = '$username' OR email = '$username') 
					AND status = 1 ";					
		$dt = $this->db->getRow( $sql1 );
		if($dt){
			$hash 	 = $dt->password;
			$password= $this->verify($this->db->escape( $password ), $hash);
			$uname = explode("@", $username);
			$unameval = reset($uname);
		
			$sql = "SELECT id, name, email, level , karyawan_id, mahasiswa_id, fakultas_id, foto, cabang_id,  
						(SELECT group_concat(description) FROM db_coms.coms_level WHERE coms_level.level IN (coms_user.level )) as `role`,
						 (SELECT group_concat(module) FROM db_coms.coms_level_module WHERE coms_level_module.level IN (coms_user.level)) as `module`,
						 (SELECT group_concat(distinct menu_id) FROM db_coms.coms_level_menu WHERE coms_level_menu.level IN (coms_user.level)) as `menu`,
						  (SELECT
								GROUP_CONCAT(db_ptiik_apps.tbl_karyawan_unit.unit_id)
								FROM
								db_ptiik_apps.tbl_karyawan_unit WHERE db_ptiik_apps.tbl_karyawan_unit.karyawan_id=db_coms.coms_user.karyawan_id) as unit
					FROM db_coms.coms_user 
					WHERE (username = '$username' OR email = '$username' OR username = '$unameval') 
						AND password = '$password' 
						AND status = 1 ";
			
			$result = $this->db->getRow( $sql );
			
			if( $result === NULL ) return $result;
			else {				
				$user = new model_user();				
				$this->log($username, 'login');
				
				$user->id = $result->id;
				$user->username  = $username;
				$user->name      = $result->name;
				$user->email 	 = $result->email;
				$user->level 	 = $result->level;
				$user->role 	 = strToLower($result->role);
				$user->module 	 = $result->module;
				$user->menu 	 = $result->menu;
				$user->staffid 	 = $result->karyawan_id;
				$user->mhsid 	 = $result->mahasiswa_id;
				$user->fakultas	 = $result->fakultas_id;
				$user->cabang	 = $result->cabang_id;
				$user->foto		 = $result->foto;
				 $user->unit_id	 = $result->unit;
				if($result->unit){
					$user->unit = explode(',', $result->unit);
				}
				
				$mmenu = new model_menu();
				$user->menus = $mmenu->read($result->level);
				$user->status = 1;
				
						
				return $user;
				
			}
		}else{
			return $dt;
		}
	}
	
	public function log($user, $action){
		$data['user']		= $user;
		$data['session']	= session_id();
		$data['tgl']		= date("Y-m-d H:i:s");
		$data['reference']	= $_SERVER['HTTP_REFERER'];
		$data['table_name']	= "coms_user";
		$data['field']		= "-";
		$data['action']		= $action;
		
		$result = $this->db->insert("coms_log", $data);
		
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) return false;
	}
	
	public function updatepassword( $username, $oldpassword, $newpassword ) {
		
		// escaping submitted username and hashed password
		$username = $this->db->escape( $username );
		$oldpassword = $this->generateHash($this->db->escape( $oldpassword ));
		$newpassword = $this->generateHash($this->db->escape( $newpassword ));
		
		$result = $this->db->update("coms_user", array('password'=>$newpassword), array("username"=>$username, "password"=>$oldpassword));
		
		//var_dump($this->db);
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) return false;
		
	}

	public function setPassword( $id, $newpassword ) {
		
		// escaping submitted username and hashed password
		$id = $this->db->escape( $id );
		$newpassword = $this->generateHash($this->db->escape( $newpassword ));
		
		$result = $this->db->update("coms_user", array('password'=>$newpassword), array("id"=>$id));
		//var_dump($this->db);
		
		if( ($result and !$this->db->getLastError()) ) 
			return true;
		else if(!$result) {
			if($this->db->getLastError() == '') return true;
			return false;
		}
		
	}
	
	public function getList($page = 1, $perpage = 500) {
		$offset = ($page-1) * $perpage;
		$sql = "SELECT id, username, password, name, email, level as `levelid`, status, karyawan_id, 
				(SELECT group_concat(description) FROM coms_level WHERE level IN (coms_user.level)) as `level` FROM db_coms.coms_user  ORDER BY name ASC, level, username  ";
		$users = $this->db->query( $sql );
		
		return $users;
	}
	
	function getTaskList($id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_task_karyawan.task_id,
					db_ptiik_apps.tbl_task_karyawan.karyawan_id,
					db_ptiik_apps.tbl_task_karyawan.judul,
					db_ptiik_apps.tbl_task_karyawan.keterangan,
					db_ptiik_apps.tbl_task_karyawan.tgl_mulai,
					db_ptiik_apps.tbl_task_karyawan.tgl_selesai,
					db_ptiik_apps.tbl_task_karyawan.progress,
					db_ptiik_apps.tbl_task_karyawan.`user`,
					db_ptiik_apps.tbl_task_karyawan.last_update
				FROM
					db_ptiik_apps.tbl_task_karyawan
				WHERE 1 = 1 
				";
		if($id){
			$sql = $sql . " AND db_ptiik_apps.tbl_task_karyawan.karyawan_id = '".$id."' ";
		}
		
		$users = $this->db->query( $sql );
		return $users;
	}
	
	public function getLevel() {
		
		$sql 	= "SELECT level, description FROM db_coms.coms_level ORDER BY level ASC";
		$result = $this->db->query( $sql );
	
		return $result;
	}
	
	function generateHash($password) {
		if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
			$salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
			return crypt($password, $salt);
		}
	}
	
	
	public function saveprofile($name, $email,$id = NULL) {
		
		$data['username'] = $this->db->escape($username);
		if( $password !== NULL )
			$data['password'] = $this->generateHash($this->db->escape($password));
			$data['name'] = $this->db->escape($name);
			$data['email'] = $this->db->escape($email);
				
		
		if(!$id) {
			
			$result = $this->db->insert("coms_user", $data);
			if( $result ) {
				return $result;
			} else {
				$this->error = $this->db->getLastError();
				return false;
			}
		} else {
			$where['id'] = $id;
			$result = $this->db->update("coms_user", $data, $where);
			if( $result ) {
				return $result;
			} else {
				$this->error = $this->db->getLastError();
				if($this->error == '') return true;
				return false;
			}
		}
	}
	
	
	public function save($username, $password, $name, $email, $level, $status, $id = NULL, $staffid) {
		
		$data['username'] = $this->db->escape($username);
		if( $password !== NULL )
			$data['password'] = $this->generateHash($this->db->escape($password));
			$data['name'] = $this->db->escape($name);
			$data['email'] = $this->db->escape($email);
			$data['level'] = $this->db->escape($level);
			$data['status'] = $this->db->escape($status);	
			if($staffid){
				$data['karyawan_id'] = $this->db->escape($staffid);	
			}			
		
		if(!$id) {
			
			$result = $this->db->insert("coms_user", $data);
			if( $result ) {
				return $result;
			} else {
				$this->error = $this->db->getLastError();
				return false;
			}
		} else {
			$where['id'] = $id;
			$result = $this->db->update("coms_user", $data, $where);
			if( $result ) {
				return $result;
			} else {
				$this->error = $this->db->getLastError();
				if($this->error == '') return true;
				return false;
			}
		}
	}
	
	public function delete($id) {
		$result = $this->db->delete("coms_user", array('id'=>$id));
		if( !$result )
			$this->error = $this->db->getLastError();
		return $result;
	}
	
	public function delete_task($id) {
		$result = $this->db->delete("db_ptiik_apps`.`tbl_task_karyawan", array('task_id'=>$id));
		if( !$result )
			$this->error = $this->db->getLastError();
		return $result;
	}
	
	public function toggleStatus($id) {
		$sql = "UPDATE coms_user SET status = CASE status WHEN 0 THEN 1 ELSE 0 END WHERE id = '$id'";
		$result = $this->db->query( $sql );
		if( !$result )
			$this->error = $this->db->getLastError();
			
		$sql = "SELECT status FROM coms_user WHERE id = '$id'";
		$this->status = $this->db->getVar($sql);	
			
		return $result;
	}
	
	public function toggleTask($id) {
		$sql = "UPDATE db_ptiik_apps.tbl_task_karyawan SET progress = CASE progress WHEN 0 THEN 1 ELSE 0 END WHERE task_id = '$id'";
		$result = $this->db->query( $sql );
		if( !$result )
			$this->error = $this->db->getLastError();
			
		$sql = "SELECT progress FROM db_ptiik_apps.tbl_task_karyawan WHERE task_id = '$id'";
		$this->status = $this->db->getVar($sql);	
			
		return $result;
	}
	
	function get_mhs_profile($id=NULL){
		$sql="SELECT
				db_ptiik_apps.tbl_mahasiswa.nim,
				db_ptiik_apps.tbl_mahasiswa.nama,
				db_coms.coms_user.foto,
				db_ptiik_apps.tbl_mahasiswa.angkatan,
				db_ptiik_apps.tbl_mahasiswa.jenis_kelamin,
				db_ptiik_apps.tbl_mahasiswa.tmp_lahir,
				db_ptiik_apps.tbl_mahasiswa.tgl_lahir,
				db_ptiik_apps.tbl_mahasiswa.alamat,
				db_ptiik_apps.tbl_mahasiswa.email,
				db_ptiik_apps.tbl_mahasiswa.hp,
				db_ptiik_apps.tbl_mahasiswa.telp,
				db_coms.coms_user.`password`,
				db_coms.coms_user.biografi,
				db_coms.coms_user.id,
				db_coms.coms_user.`name`,
				db_coms.coms_user.email,
				db_coms.coms_user.username,
				db_coms.coms_user.`level`,
				db_coms.coms_user.`status`,
				db_coms.coms_user.karyawan_id,
				db_coms.coms_user.mahasiswa_id,
				db_coms.coms_user.fakultas_id,
				db_coms.coms_user.cabang_id
				FROM
				db_ptiik_apps.tbl_mahasiswa
				INNER JOIN db_coms.coms_user ON db_coms.coms_user.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id 
				WHERE db_coms.coms_user.id = '$id'
					";
		//echo $sql;
		$result = $this->db->getRow( $sql ); //var_dump($this->db);
		return $result;
		
	}
	
	function get_user_profile($id=NULL) {
		
		//$sql = "SELECT id, username, password, name, email, level, status, karyawan_id FROM coms_user WHERE id = '$id'";
		$sql = "SELECT
					db_coms.coms_user.id,
					db_coms.coms_user.`name`,
					db_coms.coms_user.email,
					db_coms.coms_user.username,
					db_coms.coms_user.`password`,
					db_coms.coms_user.`level`,
					db_coms.coms_user.`status`,
					db_coms.coms_user.karyawan_id,
					db_coms.coms_user.mahasiswa_id,
					db_coms.coms_user.fakultas_id,
					db_coms.coms_user.cabang_id,
					db_coms.coms_user.foto,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.nickname,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_karyawan.tgl_lahir,
					db_ptiik_apps.tbl_karyawan.jenis_kelamin,
					db_ptiik_apps.tbl_karyawan.telp,
					db_ptiik_apps.tbl_karyawan.hp,
					db_ptiik_apps.tbl_karyawan.alamat,
					db_ptiik_apps.tbl_karyawan.email,
					db_ptiik_apps.tbl_karyawan.fakultas_id,
					db_ptiik_apps.tbl_karyawan.cabang_id,
					db_ptiik_apps.tbl_karyawan.interest,
					db_ptiik_apps.tbl_karyawan.biografi,
					db_ptiik_apps.tbl_karyawan.interest_en,
					db_ptiik_apps.tbl_karyawan.biografi_en,
					db_ptiik_apps.tbl_karyawan.about,
					db_ptiik_apps.tbl_karyawan.about_en,
					db_ptiik_apps.tbl_karyawan.website,
					db_ptiik_apps.tbl_karyawan.no_rekening,
					db_ptiik_apps.tbl_karyawan.nama_bank,
					db_ptiik_apps.tbl_karyawan.pendidikan_terakhir
					FROM
					db_coms.coms_user
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_coms.coms_user.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					WHERE db_coms.coms_user.id = '$id'
					";
		
		$result = $this->db->getRow( $sql ); //var_dump($this->db);
		return $result;
	}	
    
    public function save_task($mulai, $selesai, $task, $catatan, $status, $id = NULL, $staffid) {		
        
		$data['tgl_mulai']   = $this->db->escape($mulai);
        $data['tgl_selesai'] = $this->db->escape($selesai);	
        $data['judul']       = $this->db->escape($task);	
        $data['keterangan']  = $this->db->escape($catatan);	
		$data['progress'] = $this->db->escape($status);	
        $data['karyawan_id'] = $this->db->escape($staffid);	
        if(!$id){
           $data['task_id']  = $this->get_reg_id();
        }			
		
		if(!$id) {			
			$result = $this->db->insert("db_ptiik_apps`.`tbl_task_karyawan", $data);
			
			if( $result ) {
				return $result;
			} else {
				$this->error = $this->db->getLastError();
				return false;
			}
		} else {
			$where['task_id'] = $id;
			$result = $this->db->update("db_ptiik_apps`.`tbl_task_karyawan", $data, $where);
			if( $result ) {
				return $result;
			} else {
				$this->error = $this->db->getLastError();
				if($this->error == '') return true;
				return false;
			}
		}
	}
    
    function get_reg_id(){
        $sql="SELECT concat('".date('Ym')."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(task_id,4) AS unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
					FROM db_ptiik_apps.tbl_task_karyawan WHERE left(task_id, 6)='".date('Ym')."'";		
        $dt = $this->db->getRow( $sql );
			
        $strresult = $dt->data; 
        
        return $strresult;
    }
	
	function save_staff($datanya, $idnya){
		$result= $this->db->update('db_ptiik_apps`.`tbl_karyawan',$datanya, $idnya);
		var_dump($result);
		return $result;
	}
	
		
}