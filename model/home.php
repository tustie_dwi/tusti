<?php
class model_home extends model {
		function get_bimbingan_skripsi($id=NULL){
		$sqlx = "SELECT
						Count(skripsi.skripsi_id) as jml,
						skripsi.skripsi_status,
						skripsi.dosen_id1,
						skripsi.dosen_id2,
						skripsi.dosen1,
						skripsi.dosen2
						FROM
						db_ptiik_tesis.skripsi
						WHERE  1 
						";
		if($id) $sqlx.= " AND ( (MID(MD5(skripsi.dosen_id1),9,7) = '$id' OR skripsi.dosen_id1='$id') OR (MID(MD5(skripsi.dosen_id2),9,7) = '$id' OR skripsi.dosen_id2='$id')) ";
		$sqlx.=" GROUP BY
						skripsi.dosen_id1,
						skripsi.dosen_id2,
						skripsi.skripsi_status ";
						
		$sql= "SELECT *
				FROM(SELECT
					Count(a.dosen_id1) as jml,
					a.tahap_id,
					a.dosen_id1 as dosen1,
					'' as dosen2,
					a.skripsi_status
					FROM
					db_ptiik_tesis.skripsi as a
					WHERE (MID(MD5(a.dosen_id1),9,7) = '$id' OR a.dosen_id1='$id') 
					GROUP BY
					a.dosen_id1,
					a.tahap_id

					UNION

					SELECT
					Count(b.dosen_id2) as jml,
					b.tahap_id,
					'' as dosen,
					b.dosen_id2 as dosen2,
					b.skripsi_status
					FROM
					db_ptiik_tesis.skripsi as b
					WHERE (MID(MD5(b.dosen_id2),9,7) = '$id' OR b.dosen_id2='$id') 
					GROUP BY
					b.dosen_id2,
					b.tahap_id

					) skripsi
					order by dosen1 ASC, tahap_id ASC
					";
							
		return $this->db->query($sql);				
	
	}
	
	function get_jumlah_bimbingan($id=NULL){
		$sql = "SELECT
						Count(skripsi.skripsi_id) as jml,
						skripsi.skripsi_status,
						skripsi.dosen_id1,
						skripsi.dosen_id2,
						skripsi.dosen1,
						skripsi.dosen2
						FROM
						db_ptiik_tesis.skripsi
				WHERE  1 
						";
		if($id) $sql.= " AND ( (MID(MD5(skripsi.dosen_id1),9,7) = '$id' OR skripsi.dosen_id1='$id') OR (MID(MD5(skripsi.dosen_id2),9,7) = '$id' OR skripsi.dosen_id2='$id')) ";
		$sql.=" GROUP BY skripsi.skripsi_status ";
		return $this->db->query($sql);						
	}
	
	
	
	function get_quote(){
		$sql = "SELECT
					db_ptiik_coms.tbl_quote.keterangan,
					db_ptiik_coms.tbl_quote.by,
					db_ptiik_coms.tbl_quote.is_publish
					FROM
					db_ptiik_coms.tbl_quote WHERE is_publish='1' ORDER BY RAND() LIMIT 0,1
					";
		$result = $this->db->getRow( $sql );
		
		return $result;
	}
	
	function get_rekap_hr_mengajar($karyawan=NULL, $tahun=NULL, $start=NULL, $end=NULL){
	
		$start = $tahun . $start;
		$end = $tahun . $end;
		
		$sql="SELECT
				tbl_keu_bayar_detail.karyawan_id,
				tbl_keu_bayar_detail.bayar_id,
				Sum(tbl_keu_bayar_detail.total_bayar) as total,
				MID(tbl_keu_bayar.tgl_selesai,6,2) as bulan,
				tbl_karyawan.nama,
				tbl_karyawan.gelar_awal,
				tbl_karyawan.gelar_akhir,
				tbl_keu_bayar.tgl_mulai,
				tbl_keu_bayar.tgl_selesai,
				tbl_keu_bayar.periode
				FROM
				db_ptiik_apps.tbl_keu_bayar_detail
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_keu_bayar_detail.karyawan_id = tbl_karyawan.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_keu_bayar ON tbl_keu_bayar_detail.bayar_id = tbl_keu_bayar.bayar_id
				WHERE (MID(MD5(tbl_karyawan.karyawan_id),9,7) = '$karyawan' OR tbl_karyawan.karyawan_id='$karyawan')
					AND tbl_keu_bayar.periode BETWEEN $start AND $end
				GROUP BY
				tbl_keu_bayar_detail.karyawan_id,
				tbl_keu_bayar_detail.bayar_id
				";
		return $this->db->query($sql);
	}
	
	
	
	function get_rekap_absen($karyawan=NULL, $tahun=NULL, $start=NULL, $end=NULL){
		$start = $tahun . $start;
		$end = $tahun . $end;
		
		$sql = "SELECT 
					tbl_rekap_absen.periode,
					tbl_rekap_absen.hadir,
					tbl_rekap_absen.ijin,
					tbl_rekap_absen.sakit,
					tbl_rekap_absen.alpha,
					MID(tbl_rekap_absen.periode,5) bulan,
					tbl_karyawan.nama,
					tbl_karyawan.gelar_awal,
					tbl_karyawan.gelar_akhir
				FROM db_ptiik_apps.tbl_rekap_absen
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_rekap_absen.karyawan_id = tbl_karyawan.karyawan_id
				WHERE (MID(MD5(tbl_karyawan.karyawan_id),9,7) = '$karyawan' OR tbl_karyawan.karyawan_id='$karyawan')
					AND tbl_rekap_absen.periode BETWEEN $start AND $end
				ORDER BY tbl_rekap_absen.periode";
				
		return $this->db->query($sql);
	}
	
	function get_absen_dosen($dosen_id=NULL, $fak=NULL, $cabang=NULL, $tipe='dosen'){
		$sql = "SELECT 
					tbl_karyawan.nama,
					tbl_jadwalmk.kelas,
					tbl_jadwalmk.prodi_id,
					tbl_namamk.keterangan,
					COUNT(tbl_absen_dosen.absen_id) jml
				FROM db_ptiik_apps.tbl_absen_dosen
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_absen_dosen.pengampu_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_absen ON tbl_absen.absen_id = tbl_absen_dosen.absen_id
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.jadwal_id = tbl_absen.jadwal_id
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalmk.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON tbl_mkditawarkan.tahun_akademik = tbl_tahunakademik.tahun_akademik 
						AND tbl_tahunakademik.is_aktif = '1'
				WHERE 1 ";
				
				if($tipe == 'dosen') $sql .= " AND (MID(MD5(tbl_karyawan.karyawan_id),9,7) = '$dosen_id' OR tbl_karyawan.karyawan_id='$dosen_id') ";
				else $sql .= " AND MID(MD5(tbl_mkditawarkan.mkditawarkan_id),9,7) = '$dosen_id' ";
				
				
		$sql .= " GROUP BY tbl_namamk.keterangan, tbl_jadwalmk.prodi_id, tbl_jadwalmk.kelas 
				ORDER BY tbl_namamk.keterangan, tbl_jadwalmk.prodi_id, tbl_jadwalmk.kelas";
				
		return $this->db->query($sql);
	}
	
	function get_aktifitasId(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(aktifitas_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_aktifitas WHERE left(aktifitas_id,6)='".date("Ym")."' "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function save_aktifitas($data){
		if(! $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$data)) echo mysql_error();
	}
	
	// function get_aktifitas_for_calendar($role=NULL, $staff=NULL, $mhs=NULL){
		// $bln = date("m");
		// $tahun = date("Y");
		// if($role != 'mahasiswa') :
			// $sql = "SELECT MID(MD5(tbl_aktifitas.aktifitas_id),8,6) id, 
						// tbl_aktifitas.judul, DAY(tbl_aktifitas.tgl) tgl,  
						// DAY(tbl_aktifitas.tgl_selesai) tgl_selesai, 
						// SUBSTR(tbl_aktifitas.jam_mulai,1,5) jam_mulai, 
						// SUBSTR(tbl_aktifitas.jam_selesai,1,5) jam_selesai,
						// tbl_aktifitas.jenis_kegiatan_id jenis
					// FROM db_ptiik_apps.tbl_aktifitas 
					// LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_aktifitas.karyawan_id
					// WHERE MONTH(tgl) = '$bln' AND YEAR(tgl) = '$tahun' AND tbl_karyawan.karyawan_id = '$staff'";
// 			
		// else :
			// $sql = "SELECT MID(MD5(tbl_aktifitas.aktifitas_id),8,6) id, 
						// tbl_aktifitas.judul, DAY(tbl_aktifitas.tgl) tgl,  
						// DAY(tbl_aktifitas.tgl_selesai) tgl_selesai, 
						// SUBSTR(tbl_aktifitas.jam_mulai,1,5) jam_mulai, 
						// SUBSTR(tbl_aktifitas.jam_selesai,1,5) jam_selesai,
						// tbl_aktifitas.jenis_kegiatan_id jenis
					// FROM db_ptiik_apps.tbl_aktifitas 
					// LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_mahasiswa.mahasiswa_id = tbl_aktifitas.mahasiswa_id
					// WHERE MONTH(tgl) = '$bln' AND YEAR(tgl) = '$tahun' AND tbl_mahasiswa.mahasiswa_id = '$mhs'";
		// endif;
// 		
		// return $this->db->query($sql);
	// }
	
	function get_ruangan($fak=NULL, $cabang=NULL){
		$sql = "SELECT tbl_ruang.ruang_id, tbl_ruang.kode_ruang, tbl_ruang.keterangan
				FROM db_ptiik_apps.tbl_ruang
				WHERE tbl_ruang.cabang_id = '$cabang' AND tbl_ruang.fakultas_id = '$fak'";
				
		return $this->db->query($sql);
	}
	
	function get_jeniskegiatan(){
		$sql = "SELECT tbl_jeniskegiatan.jenis_kegiatan_id, tbl_jeniskegiatan.keterangan
				FROM db_ptiik_apps.tbl_jeniskegiatan";
		return $this->db->query($sql);
	}
	
	function get_task_now($user_id=null, $aktifitas_id=null){
		$sql = "SELECT aktifitas_id, tgl, jam_mulai, jam_selesai, judul, catatan, is_finish, aktifitas_id
				FROM db_ptiik_apps.tbl_aktifitas where (DATE(now()) BETWEEN tgl AND tgl_selesai)";
		if ($user_id){
			$sql .= "and user_id='$user_id'";
		}
		if($aktifitas_id){
			$sql .= " and aktifitas_id='$aktifitas_id'";
			return $this->db->getRow($sql);
		}
		return $this->db->query($sql);
	}
	
	function edit_aktifitas($mulai, $selesai, $tgl_selesai, $ruang){
		$id = $_POST['time'];
		$judul = $_POST['edit_judul'];
		$lokasi = $_POST['edit_lokasi'];
		$jenis = $_POST['edit_jenis_kegiatan'];
		
		$sql = "UPDATE db_ptiik_apps.tbl_aktifitas 
				SET judul = '$judul', 
					jam_mulai = '$mulai', 
					jam_selesai = '$selesai', 
					lokasi='$lokasi', 
					tgl_selesai = '$tgl_selesai',
					inf_ruang = '$ruang',
					jenis_kegiatan_id = '$jenis'
				WHERE MID(MD5(tbl_aktifitas.aktifitas_id),8,6) = '$id'";
				
			// echo $sql;
		$this->db->query($sql);
	}
	
	function hapus_aktifitas(){
		$id = $_POST['id'];
		$sql = "DELETE FROM db_ptiik_apps.tbl_aktifitas 
				WHERE MID(MD5(tbl_aktifitas.aktifitas_id),8,6) = '$id'";
		$this->db->query($sql);
	}
	
	function read($start = 1, $perpage = 10){
		$offset = ($start-1)*$perpage;
		$sql = "SELECT mid(md5(`vw_status`.`status_id`),6,6) as `status_id`,
					`vw_status`.`status_id` as `hid_id`,
					`vw_status`.`keterangan`,
					`vw_status`.`last_update`,
					`vw_status`.`parent_id`,
					`vw_status`.`user_id`,
					`vw_status`.`foto`,
					`vw_status`.`name`
				FROM
				`db_ptiik_apps`.`vw_status` 
				WHERE `vw_status`.`parent_id` = '0' 
					AND `vw_status`.`is_delete` = '0' 
				ORDER BY `vw_status`.`status_id` DESC LIMIT $offset, $perpage";
		$result = $this->db->query( $sql );		
		return $result;	
	}
	
	function comment($id=NULL){
		$sql = "SELECT mid(md5(`vw_status`.`status_id`),6,6) as `status_id`,
					`vw_status`.`status_id` as `hid_id`,
					`vw_status`.`keterangan`,
					`vw_status`.`last_update`,
					`vw_status`.`parent_id`,
					`vw_status`.`user_id`,
					`vw_status`.`foto`,
					`vw_status`.`name`
				FROM
					`db_ptiik_apps`.`vw_status`
				WHERE `vw_status`.`is_delete` = '0'";
		if($id!=""){
			$sql=$sql . "AND mid(md5(`vw_status`.`parent_id`),6,6)='".$id."' ";
		}
		
		$sql = $sql . "ORDER BY `vw_status`.`status_id` ASC";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function total() {
		$sql = "SELECT COUNT(*) AS `jumlah` 
			FROM `db_ptiik_apps`.`vw_status`
			WHERE `vw_status`.`parent_id` = '0' 
				AND `vw_status`.`is_delete` = '0'";
		$result = $this->db->getVar( $sql );
		return $result;
	}
	
	function share(){
		$sql = "SELECT mid(md5(`tbl_share`.`share_id`),6,6) as `share_id`,
				`tbl_share`.`share_id` as `hid_id`,
				`tbl_share`.`jadwal_id`,
				`tbl_share`.`mkditawarkan_id`,
				`tbl_share`.`status_id`
				FROM `db_ptiik_apps`.`tbl_share`";
		$result = $this->db->query( $sql );		
		return $result;	
	}
	
	function view_by_dosen($user=NULL){
		$sql = "SELECT 
					`vw_mk_by_dosen`.`jadwal_id`, 
					`vw_mk_by_dosen`.`mkditawarkan_id` 
			FROM `db_ptiik_apps`.`vw_mk_by_dosen` 
			WHERE `vw_mk_by_dosen`.`is_aktif`=1 
				AND `vw_mk_by_dosen`.`karyawan_id`='".$user."'";
		$result = $this->db->query( $sql );		
		
		return $result;	
	}
	
	function get_reg_aktifitas(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(aktifitas_id,6) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_apps.tbl_aktifitas
			WHERE left(aktifitas_id,6)='".date("Ym")."'"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function get_jadwal_id_dosen($namamk, $kelas){
		$sql = "SELECT vw_mk_by_dosen.jadwal_id
				FROM `db_ptiik_apps`.vw_mk_by_dosen
				WHERE vw_mk_by_dosen.namamk = '".$namamk."' 
					AND vw_mk_by_dosen.kelas = '".$kelas."'";
		$id = $this->db->getrow($sql);
		if(isset($id)){
		return $result = $id->jadwal_id;}
	}
	
	function get_mk_id_dosen($namamk){
		$sql = "SELECT vw_mk_by_dosen.mkditawarkan_id
				FROM `db_ptiik_apps`.vw_mk_by_dosen
				WHERE vw_mk_by_dosen.namamk = '".$namamk."'";
		$id = $this->db->getrow($sql);
		if(isset($id)){
		return $result = $id->mkditawarkan_id;}
	}
	
	function get_jadwal_id_mhs($namamk, $kelas){
		$sql = "SELECT vw_mk_by_mhs.jadwal_id
				FROM `db_ptiik_apps`.vw_mk_by_mhs
				WHERE vw_mk_by_mhs.namamk = '".$namamk."' 
					AND vw_mk_by_mhs.kelas = '".$kelas."'";
		$id = $this->db->getrow($sql);
		if(isset($id)){
		return $result = $id->jadwal_id;}
	}
	
	function get_mk_id_mhs($namamk){
		$sql = "SELECT vw_mk_by_mhs.mkditawarkan_id
				FROM `db_ptiik_apps`.vw_mk_by_mhs
				WHERE vw_mk_by_mhs.namamk = '".$namamk."'";
		$id = $this->db->getrow($sql);
		if(isset($id)){
		return $result = $id->mkditawarkan_id;}
	}
	
	function replace_status($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_status',$datanya);
	}
	
	function replace_share($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_share',$datanya);
	}
	
	function replace_aktifitas($datanya) {
		return $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$datanya);
	}
	
	function clean(){
		 $sql = "DELETE FROM `db_ptiik_apps`.`tbl_status`";
		 $this->db->query( $sql );
	}
	
	function delete($id){
		 $sql = "DELETE FROM `db_ptiik_apps`.`tbl_status` WHERE `tbl_status`.`status_id` = '".$id."'";
		 $this->db->query( $sql );
	}
	
	function del_comment($id){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_status` SET `tbl_status`.`is_delete` = 1 WHERE mid(md5(`tbl_status`.`status_id`),6,6) = '".$id."'";
		$this->db->query( $sql );
	}
	
	function del_status($id){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_status` SET `tbl_status`.`is_delete` = 1 WHERE mid(md5(`tbl_status`.`status_id`),6,6) = '".$id."'";
		$this->db->query( $sql );
	}
	
	function del_parent($id){
		$sql = "UPDATE `db_ptiik_apps`.`tbl_status` SET `tbl_status`.`is_delete` = 1 WHERE mid(md5(`tbl_status`.`parent_id`),6,6) = '".$id."'";
		$this->db->query( $sql );
	}
	
	function get_mk_mhs($term=NULL, $user=NULL){
		$sql= "SELECT 
					`vw_mk_by_mhs`.`mkditawarkan_id` as `id`, 
					`vw_mk_by_mhs`.`namamk` as `value` 
				FROM `db_ptiik_apps`.`vw_mk_by_mhs`
				WHERE `vw_mk_by_mhs`.`mahasiswa_id` = '".$user."' 
					AND (concat(`vw_mk_by_mhs`.`mkditawarkan_id`,'-',`vw_mk_by_mhs`.`namamk`)) like '%".$term."%' 
				ORDER BY `vw_mk_by_mhs`.`namamk` ASC";
		$result = $this->db->query( $sql );	
		return $result;	
	}
	
	function get_mk_dosen($term=NULL, $user=NULL){
		$sql= "SELECT distinct `vw_mk_by_dosen`.`mkditawarkan_id` as `id`,  `vw_mk_by_dosen`.`namamk` as `value` 
				FROM `db_ptiik_apps`.`vw_mk_by_dosen`
				WHERE `vw_mk_by_dosen`.`karyawan_id` = '".$user."' 
					AND `vw_mk_by_dosen`.`is_aktif` = 1 
					AND (concat(`vw_mk_by_dosen`.`mkditawarkan_id`,'-',`vw_mk_by_dosen`.`namamk`,'-',`vw_mk_by_dosen`.`kelas`)) like '%".$term."%' 
				ORDER BY `vw_mk_by_dosen`.`namamk` ASC";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_jadwal_mhs($term=NULL, $user=NULL){
		$sql= "SELECT `vw_mk_by_mhs`.`jadwal_id` as `id`, `vw_mk_by_mhs`.`namamk` as `value` 
				FROM `db_ptiik_apps`.`vw_mk_by_mhs`
				WHERE `vw_mk_by_mhs`.`mahasiswa_id` = '".$user."' 
					AND (concat(`vw_mk_by_mhs`.`jadwal_id`,'-',`vw_mk_by_mhs`.`namamk`)) like '%".$term."%' 
				ORDER BY `vw_mk_by_mhs`.`namamk` ASC";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_jadwal_dosen($term=NULL, $user=NULL){
		$sql= "SELECT `vw_mk_by_dosen`.`jadwal_id` as `id`, concat(`vw_mk_by_dosen`.`namamk`,'-',`vw_mk_by_dosen`.`kelas`) as `value` 
				FROM `db_ptiik_apps`.`vw_mk_by_dosen` 
				WHERE `vw_mk_by_dosen`.`karyawan_id` = '".$user."' 
					AND `vw_mk_by_dosen`.`is_aktif` = 1 
					AND (concat(`vw_mk_by_dosen`.`jadwal_id`,'-',`vw_mk_by_dosen`.`namamk`,'-',`vw_mk_by_dosen`.`kelas`)) like '%".$term."%' 
				ORDER BY `vw_mk_by_dosen`.`namamk` ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function get_aktifitas($jenis=NULL, $tgl=NULL, $id = NULL){
		$sql= "SELECT
				mid(md5(tbl_aktifitas.aktifitas_id),6,6) AS aktifitas_id,
				db_ptiik_apps.tbl_aktifitas.aktifitas_id AS hid_id,
				db_ptiik_apps.tbl_aktifitas.karyawan_id,
				db_ptiik_apps.tbl_aktifitas.user_id,
				db_ptiik_apps.tbl_aktifitas.mahasiswa_id,
				db_ptiik_apps.tbl_aktifitas.hari,
				db_ptiik_apps.tbl_aktifitas.tgl,
				db_ptiik_apps.tbl_aktifitas.tgl_selesai,
				TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_mulai,'%H:%i') as `jam_mulai`,
				TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_selesai,'%H:%i') as `jam_selesai`,
				db_ptiik_apps.tbl_aktifitas.judul,
				db_ptiik_apps.tbl_aktifitas.nama_mk,
				db_ptiik_apps.tbl_aktifitas.kode_mk,
				db_ptiik_apps.tbl_aktifitas.catatan,
				db_ptiik_apps.tbl_aktifitas.is_finish,
				db_ptiik_apps.tbl_aktifitas.inf_kategori,
				coms_user.`name` AS nama_user,
				db_ptiik_apps.tbl_aktifitas.inf_ruang,
				db_ptiik_apps.tbl_aktifitas.lokasi,
				tbl_aktifitas.jenis_kegiatan_id,
				db_ptiik_apps.tbl_jeniskegiatan.keterangan AS `jenis_kegiatan`
			FROM
					db_ptiik_apps.tbl_aktifitas
				LEFT JOIN db_coms.coms_user ON db_ptiik_apps.tbl_aktifitas.user_id = db_coms.coms_user.id
				LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan ON db_ptiik_apps.tbl_aktifitas.jenis_kegiatan_id = db_ptiik_apps.tbl_jeniskegiatan.jenis_kegiatan_id
					WHERE 1 = 1
				 ";
							
		if($jenis){
			$sql = $sql. " AND tbl_aktifitas.inf_kategori='".$jenis."' ";
		}
		
		if($tgl){
			$sql = $sql. " AND ('".$tgl."' BETWEEN tbl_aktifitas.tgl AND tbl_aktifitas.tgl_selesai) ";					  
		}
		
		if($id){
			$sql = $sql. " AND (tbl_aktifitas.user_id='".$id."' OR tbl_aktifitas.karyawan_id='".$id."' OR tbl_aktifitas.mahasiswa_id='".$id."') ";
		}
		
		$sql = $sql. " ORDER BY tbl_aktifitas.tgl DESC, tbl_aktifitas.jam_mulai DESC ";
	
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function finish_tasks($id=NULL,$val=NULL){
		$sql= "UPDATE db_ptiik_apps.tbl_aktifitas tbl_aktifitas
			   SET is_finish = '".$val."'
			   WHERE mid(md5(tbl_aktifitas.aktifitas_id),6,6) = '".$id."'";
				
		$result = $this->db->query( $sql );
		
		return $result;
	}
	function delete_tasks($id=NULL){
		$sql= "DELETE FROM db_ptiik_apps.tbl_aktifitas
			   WHERE mid(md5(db_ptiik_apps.tbl_aktifitas.aktifitas_id),6,6) = '".$id."'";
				
		if($this->db->query( $sql )){
			return TRUE;
		}
	}
	
	function getEvent($day=NULL,$month=NULL,$year=NULL, $runday=NULL, $user=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$this->addNol($month)."-".$this->addNol(($day+1));
		
		$hari=$this->get_namahari($runday);
		
		$sqlx = "SELECT DISTINCT mid(md5(agenda_id),5,5) as `id`, agenda_id, judul, 
					keterangan, (SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_agenda.jenis_kegiatan_id) as `jenis`,
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%H:%i') as `jam_mulai`, 
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_selesai, '%H:%i') as `jam_selesai`,
					DATE_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%d') as `ds`, 
					DATE_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_selesai, '%d') as `de` 
				FROM `db_ptiik_apps`.`tbl_agenda` 
					WHERE `tbl_agenda`.`status_agenda` = 'publish' AND `tbl_agenda`.`is_delete` = '0' AND (((tgl_mulai like '".$tgl."%' OR  tgl_selesai like '".$tgl."%')  AND inf_hari like '%".$hari."%' )
						OR ((date_format(tgl_mulai,'%Y-%m-%d') < '".$tgl."' AND date_format(tgl_selesai,'%Y-%m-%d') > '".$tgl."')  AND inf_hari like '%".$hari."%' )) ";
		$sqlx = $sqlx . " ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%H:%i') ASC ";
		
		$sql = "SELECT DISTINCT
					MID(MD5(db_ptiik_apps.tbl_aktifitas.aktifitas_id),8,6) id, 
					db_ptiik_apps.tbl_aktifitas.tgl,
					db_ptiik_apps.tbl_aktifitas.tgl_selesai,
					TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_mulai,'%H:%i') AS `jam_mulai`, 
					TIME_FORMAT(db_ptiik_apps.tbl_aktifitas.jam_selesai,'%H:%i') AS `jam_selesai`, 
					db_ptiik_apps.tbl_aktifitas.judul,
					db_ptiik_apps.tbl_aktifitas.nama_mk,
					db_ptiik_apps.tbl_aktifitas.kode_mk,
					db_ptiik_apps.tbl_aktifitas.catatan  AS `keterangan`,
					GROUP_CONCAT(' ', tbl_ruang.kode_ruang, ' - ', tbl_ruang.keterangan) ruang_ket,
					GROUP_CONCAT(tbl_ruang.kode_ruang) ruang,
					db_ptiik_apps.tbl_aktifitas.lokasi,
					db_ptiik_apps.tbl_aktifitas.inf_kategori,
					db_ptiik_apps.tbl_aktifitas.jenis_kegiatan_id,
					tbl_jeniskegiatan.keterangan jenis_kegiatan,
					db_ptiik_apps.tbl_aktifitas.agenda_id
				FROM db_ptiik_apps.tbl_aktifitas
				LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_aktifitas.jenis_kegiatan_id
				LEFT JOIN db_ptiik_apps.tbl_ruang ON tbl_aktifitas.inf_ruang LIKE CONCAT('%',tbl_ruang.ruang_id,'%')
				WHERE
				('".$tgl."' BETWEEN tbl_aktifitas.tgl AND tbl_aktifitas.tgl_selesai) ";
		if($user){
			$sql = $sql. " AND (tbl_aktifitas.user_id='".$user."' OR tbl_aktifitas.karyawan_id='".$user."' OR tbl_aktifitas.mahasiswa_id='".$user."') ";
		}
		$sql = $sql. " GROUP BY tbl_aktifitas.judul ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_aktifitas`.jam_mulai,'%H:%i') ASC ";
		
		return $this->db->query( $sql );
	
	}
	
	function get_all_event($user=NULL, $bulan=NULL, $tahun=NULL){
		$sql = "SELECT DISTINCT
					MID(MD5(tbl_aktifitas.aktifitas_id),8,6) id, 
					tbl_aktifitas.tgl,
					tbl_aktifitas.tgl_selesai,
					TIME_FORMAT(tbl_aktifitas.jam_mulai,'%H:%i') AS `jam_mulai`, 
					TIME_FORMAT(tbl_aktifitas.jam_selesai,'%H:%i') AS `jam_selesai`, 
					tbl_aktifitas.judul,
					GROUP_CONCAT(' ', tbl_ruang.kode_ruang, ' - ', tbl_ruang.keterangan) ruang_ket,
					GROUP_CONCAT(tbl_ruang.kode_ruang) ruang,
					tbl_aktifitas.lokasi,
					tbl_aktifitas.inf_kategori,
					tbl_aktifitas.jenis_kegiatan_id,
					tbl_jeniskegiatan.keterangan jenis_kegiatan
				FROM db_ptiik_apps.tbl_aktifitas
				LEFT JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_aktifitas.jenis_kegiatan_id
				LEFT JOIN db_ptiik_apps.tbl_ruang ON tbl_aktifitas.inf_ruang LIKE CONCAT('%',tbl_ruang.ruang_id,'%')
				WHERE (tbl_aktifitas.user_id='$user' OR tbl_aktifitas.karyawan_id='$user' OR tbl_aktifitas.mahasiswa_id='$user')
					AND $bulan BETWEEN MONTH(tbl_aktifitas.tgl) AND MONTH(tbl_aktifitas.tgl_selesai) 
					AND (YEAR(tbl_aktifitas.tgl) = $tahun OR YEAR(tbl_aktifitas.tgl_selesai) = $tahun)
				GROUP BY tbl_aktifitas.aktifitas_id 
				ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_aktifitas`.jam_mulai,'%H:%i') ASC ";
		
		return $this->db->query( $sql );
	}
	
	function get_namahari($runday){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
			case '7': $hari='minggu'; break;
		}
		
		return $hari;
	}
	
	function potong_kalimat ($content, $length) {
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
	function addnol($string){
		if(strlen($string)==1){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	
}

?>