<?php
class model_kknp extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function read(){
		$sql = "SELECT 
				tbl_kknp_pengajuan.pengajuan_id,
				tbl_kknp_pengajuan.perusahaan_id,
				tbl_kknp_pengajuan.tgl_pengajuan,
				tbl_kknp_pengajuan.tgl_mulai,
				tbl_kknp_pengajuan.tgl_selesai,
				tbl_kknp_pengajuan.tgl_laporan,
				tbl_kknp_pengajuan.is_status,
				
				tbl_kknp_perusahaan.nama as nama_perusahaan,
				tbl_kknp_perusahaan.alamat,
                                
                (SELECT GROUP_CONCAT(tbl_mahasiswa.nama separator ',')
                FROM db_ptiik_apps.tbl_kknp_mahasiswa
                LEFT JOIN db_ptiik_apps.tbl_mahasiswa ON tbl_kknp_mahasiswa.mahasiswa_id = tbl_mahasiswa.mahasiswa_id
                WHERE tbl_kknp_mahasiswa.pengajuan_id = tbl_kknp_pengajuan.pengajuan_id ) as nama_mhs
				
				FROM db_ptiik_apps.tbl_kknp_pengajuan
				LEFT JOIN db_ptiik_apps.tbl_kknp_perusahaan ON tbl_kknp_pengajuan.perusahaan_id = tbl_kknp_perusahaan.perusahaan_id
				WHERE 1
			   ";
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_mhs($name=NULL,$nim=NULL){
		$sql = "SELECT concat(nim, ' - ', nama) as value,
					   nim,
					   mahasiswa_id as hid_id,
					   MID( MD5(mahasiswa_id), 6, 6) as mhs_id 
				FROM `db_ptiik_apps`.`tbl_mahasiswa` tbl_mahasiswa
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` tbl_prodi ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
		 		WHERE tbl_mahasiswa.is_aktif = 'aktif' ";
		
		if($name){
			$sql .= " AND tbl_mahasiswa.nama = '".$name."' ";
		}
		
		if($nim){
			$sql .= " AND tbl_mahasiswa.nim = '".$nim."' ";
		}
		
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_company($name=NULL,$addr=NULL){
		$sql = "SELECT 
				MID( MD5(tbl_kknp_perusahaan.perusahaan_id), 6, 6) as perusahaan_id,
				tbl_kknp_perusahaan.perusahaan_id as hid_id,
				tbl_kknp_perusahaan.nama as value,
				tbl_kknp_perusahaan.alamat,
				tbl_kknp_perusahaan.telp
				FROM db_ptiik_apps.tbl_kknp_perusahaan 
				WHERE 1 ";
		
		if($name){
			$sql .= " AND tbl_kknp_perusahaan.nama = '".$name."' ";
		}
		
		if($addr){
			$sql .= " AND tbl_kknp_perusahaan.alamat = '".$addr."' ";
		}
		// echo $sql;
		$result = $this->db->query( $sql );
		return $result;	
	}
	
	function get_pengajuan_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(pengajuan_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_pengajuan "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_company_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(perusahaan_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_perusahaan "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function get_mhs_kknp_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kknp_mhs_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_apps.tbl_kknp_mahasiswa "; 
		$strresult = $this->db->getRow( $sql );
		return $strresult->data;
	}
	
	function replace_perusahaan($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_perusahaan',$datanya);
	}
	
	function replace_kknp($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_pengajuan',$datanya);
	}
	
	function replace_kknp_mhs($datanya){
		return $this->db->replace('db_ptiik_apps`.`tbl_kknp_mahasiswa',$datanya);
	}
	
}