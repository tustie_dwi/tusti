<?php

class model_hakakses extends model {
	public function __construct() {
		parent::__construct();
	}
	
	
	function read_hakakses(){
		$sql = "SELECT DISTINCT
					`coms_level_menu`.`level` as `id`,
					`coms_level`.`description` as `role`
					FROM
					db_coms.`coms_level`
					Inner Join db_coms.`coms_level_menu` ON `coms_level`.`level` = `coms_level_menu`.`level`
					";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	public function read($id=NULL){
	
		$sql = "SELECT
				`coms_level_menu`.`menu_id`,
				`coms_level_menu`.`level`,
				`coms_level`.`description`,
				`coms_menu`.`judul`,
				`coms_menu`.`parent_id`
				FROM
				db_coms.`coms_level`
				Inner Join db_coms.`coms_level_menu` ON `coms_level`.`level` = `coms_level_menu`.`level`
				Inner Join db_coms.`coms_menu` ON `coms_level_menu`.`menu_id` = `coms_menu`.`id`
				WHERE 1 = 1
					";
		if($id){
			$sql = $sql . " AND `coms_menu`.`is_aktif`=1 AND `coms_level_menu`.`level`='".$id."' ";
		}
	
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
	function read_role(){
		$sql = "SELECT
					level as `id`, description as `value`
					FROM
					db_coms.`coms_level`				
					WHERE 1=1 
					";
			
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	public function read_menu($id=NULL, $str=NULL){
	
		$sql = "SELECT
					`coms_menu`.`id`,
					`coms_menu`.`parent_id`,
					`coms_menu`.`link`,
					`coms_menu`.`judul`,
					`coms_menu`.`module`,
					`coms_menu`.`icon`,
					`coms_menu`.`is_aktif`
				FROM
					db_coms.`coms_menu`				
				WHERE 1=1 
					";
		if($id){
			$sql = $sql . " AND `coms_menu`.`is_aktif`=1 ";
		}
		
		if($str){
			$sql = $sql . " AND `coms_menu`.`parent_id`=0 ";
		}
		
		$result = $this->db->query( $sql );
		
		return $result;	
		
	}
	
	public function save($id, $value){
		
		$data['value'] = serialize($value);
		$where['name'] = $id;
		
		$affected = $this->db->update('db_coms`.`coms_options', $data, $where);
		//var_dump($this->db);
		return $affected;
		
	}
	
	function insert_hakakses_all($role=NULL){
		$sql = "SELECT
					`coms_menu`.`id`
				FROM db_coms.coms_menu WHERE is_aktif=1 ";
		$result = $this->db->query( $sql );
		
		foreach($result as $dt){
			$datanya = array('level'=>$role, 'menu_id'=>$dt->id);	
			$this->insert_hakakses($datanya);
		}
	}
	
	public function replace_menu($datanya){
		$result = $this->db->insert('db_coms`.`coms_menu', $data);
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	
	function save_hakakses($role, $menu){
		$data['parent_id'] 	= $parentid;
		$data['link'] 		= $link;
		$data['judul'] 		= $judul;
		$data['module'] 	= $nmodule;
		$data['icon'] 		= $icon;		
		$data['is_aktif'] 	= $isaktif;
		
		$result = $this->db->insert('db_coms`.`coms_menu', $data);
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	
	function insert_hakakses($data){
		return $this->db->replace('db_coms`.`coms_level_menu', $data);
	}
	
	
	function activate($id=NULL){
		$where['id'] = $id;
		$data['is_aktif'] = 1;
		
		$result = $this->db->update('db_coms`.`coms_menu', $data, $where);
		
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	
	function deactivate($id=NULL){
		$where['id'] = $id;
		$data['is_aktif'] = 0;
		
		$result = $this->db->update('db_coms`.`coms_menu', $data, $where);
		//echo $result;
		if( $result ) {
			return $result;
		} else {
			$this->error = $this->db->getLastError();
			return false;
		}
	}
	

}