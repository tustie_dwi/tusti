<?php
class model_eventinfo extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	
	function get_peserta_notif(){
		$sql = "SELECT
					tbl_peserta_registrasi.peserta_id,
					tbl_peserta_registrasi.kegiatan_id,
					tbl_peserta_registrasi.nama,
					tbl_peserta_registrasi.first_name,
					tbl_peserta_registrasi.last_name,
					tbl_peserta_registrasi.pin,
					tbl_peserta_registrasi.title,
					tbl_peserta_registrasi.email,
					tbl_peserta_registrasi.gelar_awal,
					tbl_peserta_registrasi.gelar_akhir,
					tbl_peserta_registrasi.jenis_peserta
					FROM
					db_ptiik_event.tbl_peserta_registrasi WHERE jenis_peserta NOT IN ('peserta') ";
		$result = $this->db->query($sql);
		return $result;
	}
	
	function delete_dokumen($id){
		$sql = "DELETE FROM `db_ptiik_event`.`tbl_peserta_upload`
				WHERE mid(md5(tbl_peserta_upload.upload_id),9,7) = '".$id."'";

		$result = $this->db->query($sql);
		return $result;
	}
	
	//lain lain
	function get_jenis_kegitan(){
		$sql = "select * from db_ptiik_apps.tbl_jeniskegiatan";
		return $this->db->query($sql);
	}
	function get_negara(){
		 $sql = "select * from db_ptiik_event.tbl_negara";
		return $this->db->query($sql);
	}
	
	function get_jenis_peserta($id=NULL){
		$sql = "SELECT
				tbl_jenis_peserta_kegiatan.kegiatan_id,
				tbl_jenis_peserta_kegiatan.jenis_peserta,
				tbl_jenis_peserta.keterangan
				FROM
				db_ptiik_event.tbl_jenis_peserta_kegiatan
				INNER JOIN db_ptiik_event.tbl_jenis_peserta ON tbl_jenis_peserta.jenis_peserta = tbl_jenis_peserta_kegiatan.jenis_peserta
				WHERE 1 
				";
		if($id) $sql.= " AND (tbl_jenis_peserta_kegiatan.kegiatan_id = '$id' OR mid(md5(tbl_jenis_peserta_kegiatan.kegiatan_id),9,7) = '$id') ";
		
		return $this->db->query($sql);
	}
	
	function get_jenis_peserta_by_biaya($id=NULL){
		$sql = "SELECT
					db_ptiik_event.tbl_master_biaya_jenis.jenis_biaya,
					db_ptiik_event.tbl_master_biaya_jenis.jenis_peserta,
					db_ptiik_event.tbl_master_biaya_jenis.keterangan,
					db_ptiik_event.tbl_jenis_peserta_kegiatan.kegiatan_id
					FROM
					db_ptiik_event.tbl_master_biaya_jenis
					INNER JOIN db_ptiik_event.tbl_jenis_peserta_kegiatan ON db_ptiik_event.tbl_master_biaya_jenis.jenis_peserta = db_ptiik_event.tbl_jenis_peserta_kegiatan.jenis_peserta
					WHERE 1
					";
		if($id) $sql.= " AND (tbl_jenis_peserta_kegiatan.kegiatan_id = '$id' OR mid(md5(tbl_jenis_peserta_kegiatan.kegiatan_id),9,7) = '$id') ";
		return $this->db->query($sql);
	}
	
	function get_komponen_biaya($id=NULL, $distinct=NULL,$nama=NULL,$jenis_peserta=NULL, $kategori=NULL ){
		switch($distinct){
			case 'nama_biaya':
				$sql="SELECT DISTINCT
						mid(md5(tbl_master_biaya_komponen.kegiatan_id),9,7) AS id,
						tbl_master_biaya_jenis.jenis_biaya,
						tbl_master_biaya_jenis.jenis_peserta,
						tbl_master_biaya_jenis.keterangan,
						tbl_master_biaya_komponen.nama_biaya,
						tbl_master_biaya_komponen.kegiatan_id
						FROM
							db_ptiik_event.tbl_master_biaya_jenis
							INNER JOIN db_ptiik_event.tbl_master_biaya_komponen ON tbl_master_biaya_jenis.jenis_biaya = tbl_master_biaya_komponen.jenis_biaya
						WHERE 1";
			break;
			case 'biaya':
				$sql= "SELECT DISTINCT 
						mid(md5(tbl_master_biaya_komponen.kegiatan_id),9,7)  as id, 
						db_ptiik_event.tbl_master_biaya_jenis.jenis_biaya,
						db_ptiik_event.tbl_master_biaya_jenis.jenis_peserta,
						db_ptiik_event.tbl_master_biaya_jenis.keterangan,
						db_ptiik_event.tbl_master_biaya_komponen.nama_biaya,
						db_ptiik_event.tbl_master_biaya_komponen.harga,
						db_ptiik_event.tbl_master_biaya_komponen.kegiatan_id,
						db_ptiik_event.tbl_master_biaya_komponen.jenis_biaya,
						db_ptiik_event.tbl_master_biaya_komponen.satuan						
						FROM
							db_ptiik_event.tbl_master_biaya_jenis
							INNER JOIN db_ptiik_event.tbl_master_biaya_komponen ON tbl_master_biaya_jenis.jenis_biaya = tbl_master_biaya_komponen.jenis_biaya
						WHERE 1 ";
			break;
			case 'jenis_peserta':
				$sql="SELECT DISTINCT 
				mid(md5(tbl_master_biaya_komponen.kegiatan_id),9,7)  as id, 
				db_ptiik_event.tbl_master_biaya_jenis.jenis_biaya,
				db_ptiik_event.tbl_master_biaya_jenis.jenis_peserta,
				tbl_master_biaya_komponen.kegiatan_id,
				db_ptiik_event.tbl_master_biaya_jenis.keterangan		
				FROM
							db_ptiik_event.tbl_master_biaya_jenis
							INNER JOIN db_ptiik_event.tbl_master_biaya_komponen ON tbl_master_biaya_jenis.jenis_biaya = tbl_master_biaya_komponen.jenis_biaya
						WHERE 1 ";
			break;
			case 'kategori':
				$sql= "SELECT DISTINCT
						mid(md5(tbl_master_biaya_komponen.kegiatan_id),9,7)  as id, 
						db_ptiik_event.tbl_master_biaya_komponen.keterangan as kategori
						FROM
							db_ptiik_event.tbl_master_biaya_jenis
							INNER JOIN db_ptiik_event.tbl_master_biaya_komponen ON tbl_master_biaya_jenis.jenis_biaya = tbl_master_biaya_komponen.jenis_biaya
						WHERE 1
						";
			break;
			
			default:
				$sql = "SELECT
							mid(md5(tbl_master_biaya_komponen.kegiatan_id),9,7)  as id, 
							tbl_master_biaya_jenis.jenis_biaya,
							tbl_master_biaya_jenis.jenis_peserta,
							tbl_master_biaya_jenis.keterangan,
							tbl_master_biaya_komponen.nama_biaya,
							tbl_master_biaya_komponen.harga,
							tbl_master_biaya_komponen.tgl_mulai,
							tbl_master_biaya_komponen.tgl_selesai,
							tbl_master_biaya_komponen.is_aktif,
							tbl_master_biaya_komponen.komponen_id,
							tbl_master_biaya_komponen.kegiatan_id,
							tbl_master_biaya_komponen.jenis_biaya,
							db_ptiik_event.tbl_master_biaya_komponen.keterangan as kategori,
							db_ptiik_event.tbl_master_biaya_komponen.satuan,
							db_ptiik_event.tbl_jenis_peserta.keterangan as jpeserta
							FROM
							db_ptiik_event.tbl_master_biaya_jenis
							INNER JOIN db_ptiik_event.tbl_master_biaya_komponen ON tbl_master_biaya_jenis.jenis_biaya = tbl_master_biaya_komponen.jenis_biaya 
							INNER JOIN db_ptiik_event.tbl_jenis_peserta ON db_ptiik_event.tbl_master_biaya_jenis.jenis_peserta = db_ptiik_event.tbl_jenis_peserta.jenis_peserta
							WHERE 1 
					";
			break;
		}
		
		if($id) $sql.= " AND (tbl_master_biaya_komponen.kegiatan_id = '$id' OR mid(md5(tbl_master_biaya_komponen.kegiatan_id),9,7) = '$id') ";
		if($nama) $sql.= " AND db_ptiik_event.tbl_master_biaya_komponen.nama_biaya='$nama' ";
		if($jenis_peserta) $sql.= " AND db_ptiik_event.tbl_master_biaya_jenis.jenis_peserta='$jenis_peserta' ";
		if($kategori) $sql.= " AND tbl_master_biaya_komponen.keterangan ='$kategori' ";
		
		$sql.= " ORDER BY tbl_master_biaya_jenis.urut ASC ";
		
		if($distinct=='harga') return $this->db->getRow($sql);
		else return $this->db->query($sql);
		
	}
	
	function get_peserta($peserta=NULL, $first_name = NULL, $last_name=NULL, $email=NULL, $pin=NULL, $jenis_peserta = NULL){
		$sql = "SELECT
					tbl_peserta_registrasi.peserta_id,
					tbl_peserta_registrasi.kegiatan_id,
					tbl_peserta_registrasi.jenis_peserta,
					tbl_peserta_registrasi.request_visa,
					tbl_peserta_registrasi.nama,
					tbl_peserta_registrasi.pin,
					tbl_peserta_registrasi.first_name,
					tbl_peserta_registrasi.last_name,
					tbl_peserta_registrasi.title,
					tbl_peserta_registrasi.negara,
					tbl_peserta_registrasi.gelar_awal,
					tbl_peserta_registrasi.gelar_akhir,
					tbl_peserta_registrasi.email,
					tbl_peserta_registrasi.keterangan,
					tbl_peserta_registrasi.tgl_registrasi,
					tbl_peserta_registrasi.jenis_pendaftaran,
					tbl_peserta_registrasi.metode_pembayaran,
					tbl_peserta_registrasi.total_tagihan,
					tbl_peserta_registrasi.total_bayar,
					tbl_peserta_registrasi.telp,
					tbl_peserta_registrasi.hp,
					tbl_peserta_registrasi.alamat,
					tbl_peserta_registrasi.instansi,
					tbl_peserta_registrasi.alamat_instansi,
					tbl_peserta_registrasi.is_valid,
					tbl_jenis_peserta.keterangan as kategori_peserta
					FROM
					db_ptiik_event.tbl_peserta_registrasi
					INNER JOIN db_ptiik_event.tbl_jenis_peserta ON tbl_peserta_registrasi.jenis_peserta = tbl_jenis_peserta.jenis_peserta
					WHERE 1 
					";
		if($peserta) $sql.= " AND (tbl_peserta_registrasi.peserta_id='$peserta' OR mid(md5(tbl_peserta_registrasi.peserta_id),9,7) ='$peserta' ) ";
		if($first_name) $sql.= " AND (lcase(trim(tbl_peserta_registrasi.first_name)) = '$first_name') ";
		if($last_name) $sql.= " AND (lcase(trim(tbl_peserta_registrasi.last_name)) = '$last_name') ";
		if($email) $sql.= " AND (tbl_peserta_registrasi.email = '$email') ";
		if($pin) $sql.= " AND tbl_peserta_registrasi.pin = '$pin' ";
		if($jenis_peserta) $sql.= " AND tbl_peserta_registrasi.jenis_peserta='$jenis_peserta' ";
		
		if($peserta || $pin || $email) return $this->db->getRow($sql);
		else return $this->db->query($sql);
	}
	
	
	function get_tagihan($id=NULL, $peserta=NULL, $isbayar=NULL){
		$sql = "SELECT
					mid(md5(tbl_peserta_tagihan.tagihan_id),9,7) as id, 
					tbl_peserta_tagihan.tagihan_id,
					tbl_peserta_tagihan.komponen_id,
					tbl_peserta_tagihan.peserta_id,
					tbl_peserta_tagihan.inf_jumlah,
					tbl_peserta_tagihan.is_bayar,
					tbl_peserta_tagihan.tgl_bayar,
					tbl_master_biaya_komponen.nama_biaya
					FROM
					db_ptiik_event.tbl_peserta_tagihan
					INNER JOIN db_ptiik_event.tbl_peserta_registrasi ON tbl_peserta_tagihan.peserta_id = tbl_peserta_registrasi.peserta_id
					INNER JOIN db_ptiik_event.tbl_master_biaya_komponen ON tbl_peserta_tagihan.komponen_id = tbl_master_biaya_komponen.komponen_id WHERE 1 
					";
		if($id) $sql.= " AND (tbl_peserta_tagihan.tagihan_id = '$id' OR mid(md5(tbl_peserta_tagihan.tagihan_id),9,7) = '$id') ";
		if($peserta) $sql.= " AND (tbl_peserta_tagihan.peserta_id='$peserta' OR mid(md5(tbl_peserta_tagihan.peserta_id),9,7) ='$peserta' ) ";
		if($isbayar!="-") $sql.= " AND tbl_peserta_tagihan.is_bayar='$isbayar' ";
		
		if($id) return $this->db->getRow($sql);
		else return $this->db->query($sql);
	}
	
	function get_dokumen($id=NULL, $peserta=NULL, $kategori=NULL, $isvalid=NULL){
		$sql = "SELECT
				mid(md5(db_ptiik_event.tbl_peserta_upload.upload_id),9,7) as id,
				db_ptiik_event.tbl_peserta_upload.upload_id,
				db_ptiik_event.tbl_peserta_upload.peserta_id,
				db_ptiik_event.tbl_peserta_upload.kategori_dokumen,
				db_ptiik_event.tbl_peserta_upload.keterangan,
				db_ptiik_event.tbl_peserta_upload.file_name,
				db_ptiik_event.tbl_peserta_upload.file_size,
				db_ptiik_event.tbl_peserta_upload.file_type,
				db_ptiik_event.tbl_peserta_upload.file_loc,
				db_ptiik_event.tbl_peserta_upload.is_valid,
				db_ptiik_event.tbl_peserta_upload.user_id,
				db_ptiik_event.tbl_peserta_upload.tgl_upload,
				db_ptiik_event.tbl_peserta_upload.last_update
				FROM
				db_ptiik_event.tbl_peserta_upload
				WHERE
				1 ";
		if($id) $sql.= " AND (db_ptiik_event.tbl_peserta_upload.upload_id = '$id' OR mid(md5(db_ptiik_event.tbl_peserta_upload.upload_id),9,7) = '$id') ";
		
		if($peserta) $sql.= " AND (tbl_peserta_upload.peserta_id='$peserta' OR mid(md5(tbl_peserta_upload.peserta_id),9,7) ='$peserta' ) ";
		if($kategori) $sql.= " AND (tbl_peserta_upload.kategori_dokumen='$kategori' ) ";
		if($isvalid!="-") $sql.= " AND tbl_peserta_upload.is_valid='$isvalid' ";
		
		$sql.= " ORDER BY db_ptiik_event.tbl_peserta_upload.tgl_upload DESC";
		//echo $sql."<br>";
		if($id) return $this->db->getRow($sql);
		else return $this->db->query($sql);
	}
	
	function get_feedback( $event, $id=NULL ) {
		$sql = "SELECT
				`db_ptiik_coms`.`coms_comment`.`comment_id`,
				`db_ptiik_coms`.`coms_comment`.`user_name`,
				`db_ptiik_coms`.`coms_comment`.`email`,
				`db_ptiik_coms`.`coms_comment`.`title`,
				`db_ptiik_coms`.`coms_comment`.`comment` 
				FROM
				`db_ptiik_coms`.`coms_comment`
				WHERE 1 ";
				
		if($event) $sql .= " AND db_ptiik_coms.coms_comment.event_id = '$event'";
		if($id) $sql.= " AND db_ptiik_coms.coms_comment.parent_id = '$id' ";
		else $sql.= " AND db_ptiik_coms.coms_comment.parent_id = '' ";
		$sql .= " AND db_ptiik_coms.coms_comment.`category` = 'faq' AND db_ptiik_coms.coms_comment.`is_approve` = 1";
		//echo $sql."<br>";
		return $this->db->query($sql);
	}
	
} 
?>
