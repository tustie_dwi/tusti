<?php

class model_page extends model {
	public function __construct() {
		parent::__construct();
	}
	
	function get_komponen_biaya_tagihan($jenis_peserta=NULL, $kegiatan=NULL, $tgl=NULL, $ke=NULL){
		$sql = "SELECT
				tbl_master_biaya_jenis.jenis_peserta,
				tbl_master_biaya_komponen.komponen_id,
				tbl_master_biaya_komponen.kegiatan_id,
				tbl_master_biaya_komponen.jenis_biaya,
				tbl_master_biaya_komponen.nama_biaya,
				tbl_master_biaya_komponen.harga,
				tbl_master_biaya_komponen.keterangan,
				tbl_master_biaya_komponen.satuan,
				tbl_master_biaya_komponen.tgl_mulai,
				tbl_master_biaya_komponen.tgl_selesai,
				tbl_master_biaya_komponen.is_aktif
				FROM
				db_ptiik_event.tbl_master_biaya_jenis
				INNER JOIN db_ptiik_event.tbl_master_biaya_komponen ON tbl_master_biaya_jenis.jenis_biaya = tbl_master_biaya_komponen.jenis_biaya WHERE 1
				 ";
		if($jenis_peserta) $sql.= " AND tbl_master_biaya_jenis.jenis_peserta='$jenis_peserta' ";
		if($kegiatan) $sql.= " AND (tbl_master_biaya_komponen.kegiatan_id='$kegiatan' OR mid(md5(tbl_master_biaya_komponen.kegiatan_id),9,7)='$kegiatan' ) ";
		if($tgl) $sql.= " AND (tbl_master_biaya_komponen.tgl_selesai > '$tgl') ";
		
		return $this->db->query($sql);
	}
	
	function get_peserta_exist($email=NULL, $kegiatan=NULL){
		$sql = "SELECT peserta_id FROM db_ptiik_event.tbl_peserta_registrasi WHERE email='$email' AND kegiatan_id='$kegiatan' ";
		return $this->db->query( $sql );
	}
	
	function get_nextid_kegiatan(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(kegiatan_id,4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_event.tbl_kegiatan"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function get_nextid_peserta_tagihan($peserta=NULL, $komponen=NULL){
		$sql = "SELECT tagihan_id FROM db_ptiik_event.tbl_peserta_tagihan WHERE peserta_id ='$peserta' AND kompinen_id='$komponen' ";
		$rs = $this->db->getRow( $sql );
		if($rs){
			return $rs->tagihan_id;
		}else{
			$sqli="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(tagihan_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_event.tbl_peserta_tagihan"; 
			$dt = $this->db->getRow( $sqli );
			$strresult = $dt->data;
			return $strresult;
		}
	}
	
	function get_nextid_peserta_upload($cat=NULL, $peserta=NULL){
		$sql = "SELECT upload_id FROM db_ptiik_event.tbl_peserta_upload WHERE kategori_dokumen ='$cat' AND peserta_id='$peserta' ";
		$rs = $this->db->getRow( $sql );
		if($rs){
			return $rs->upload_id;
		}else{
			$sqli="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(upload_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_event.tbl_peserta_upload"; 
			$dt = $this->db->getRow( $sqli );
			$strresult = $dt->data;
			return $strresult;
		}
	}
	
	function simpan_peserta_upload($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_upload',$data);
	}
	
	function simpan_kegiatan_peserta ($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_registrasi',$data);
	}
	
	function simpan_tagihan_peserta ($data){
		$result = $this->db->replace('db_ptiik_event`.`tbl_peserta_tagihan',$data);
	}
	
	function get_nextid_peserta_kegiatan($mhs=NULL, $kegiatan=NULL){
		$sql= "SELECT peserta_id FROM db_ptiik_event.tbl_peserta_registrasi WHERE mahasiswa_id ='$mhs' AND kegiatan_id = '$kegiatan' ";
		$rs = $this->db->getRow( $sql );
	
		if($rs){		
			return $rs->peserta_id;
		}else{
			$sqli="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(peserta_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM db_ptiik_event.tbl_peserta_registrasi"; 
			$dt = $this->db->getRow( $sqli );
			$strresult = $dt->data;
			return $strresult;
		}
	}
	
	
	function get_master_kegiatan_peserta($nim=NULL, $kegiatan=NULL, $id=NULL, $first_name = NULL, $last_name=NULL, $email=NULL, $pin=NULL, $jenis_peserta = NULL){
		$sql = "SELECT
					tbl_peserta_registrasi.peserta_id,
					tbl_peserta_registrasi.kegiatan_id,
					mid(md5(db_ptiik_event.tbl_peserta_registrasi.kegiatan_id),9,7) as kid,
					mid(md5(db_ptiik_event.tbl_peserta_registrasi.peserta_id),9,7) as id,
					tbl_peserta_registrasi.jenis_peserta,
					tbl_peserta_registrasi.request_visa,
					tbl_peserta_registrasi.nama,
					tbl_peserta_registrasi.pin,
					tbl_peserta_registrasi.first_name,
					tbl_peserta_registrasi.last_name,
					tbl_peserta_registrasi.title,
					tbl_peserta_registrasi.negara,
					tbl_peserta_registrasi.gelar_awal,
					tbl_peserta_registrasi.gelar_akhir,
					tbl_peserta_registrasi.email,
					tbl_peserta_registrasi.keterangan,
					tbl_peserta_registrasi.tgl_registrasi,
					tbl_peserta_registrasi.jenis_pendaftaran,
					tbl_peserta_registrasi.metode_pembayaran,
					tbl_peserta_registrasi.total_tagihan,
					tbl_peserta_registrasi.total_bayar,
					tbl_peserta_registrasi.telp,
					tbl_peserta_registrasi.hp,
					tbl_peserta_registrasi.alamat,
					tbl_peserta_registrasi.instansi,
					tbl_peserta_registrasi.alamat_instansi,
					tbl_peserta_registrasi.is_valid,
						db_ptiik_event.tbl_jenis_peserta.keterangan as jpeserta
						FROM
						db_ptiik_event.tbl_peserta_registrasi
						INNER JOIN db_ptiik_event.tbl_jenis_peserta ON db_ptiik_event.tbl_peserta_registrasi.jenis_peserta = db_ptiik_event.tbl_jenis_peserta.jenis_peserta
					";
		if($nim) $sql.= " AND tbl_peserta_registrasi.mahasiswa_id='$nim' ";
		if($kegiatan) $sql.= " AND (mid(md5(db_ptiik_event.tbl_peserta_registrasi.kegiatan_id),9,7)='$kegiatan' OR tbl_peserta_registrasi.kegiatan_id='$kegiatan') ";
		if($id) $sql.= " AND (mid(md5(db_ptiik_event.tbl_peserta_registrasi.peserta),9,7)='$id' OR tbl_peserta_registrasi.peserta='$id') ";
		if($first_name) $sql.= " AND (lcase(trim(tbl_peserta_registrasi.first_name)) = '$first_name') ";
		if($last_name) $sql.= " AND (lcase(trim(tbl_peserta_registrasi.last_name)) = '$last_name') ";
		if($email) $sql.= " AND (tbl_peserta_registrasi.email = '$email') ";
		if($pin) $sql.= " AND tbl_peserta_registrasi.pin = '$pin' ";
		if($jenis_peserta) $sql.= " AND tbl_peserta_registrasi.jenis_peserta='$jenis_peserta' ";
		
		if($pin || $email || $id || ($nim && $kegiatan)) return $this->db->getRow($sql);
		else return $this->db->query($sql);
	}
	
	
	function get_master_kegiatan($cat=NULL, $id=NULL, $parent=NULL, $tgl=NULL){
		$sql = "SELECT
					mid(md5(db_ptiik_event.tbl_kegiatan.kegiatan_id),9,7) as id,
					db_ptiik_event.tbl_kegiatan.kegiatan_id,
					db_ptiik_event.tbl_kegiatan.nama,
					db_ptiik_event.tbl_kegiatan.nama as judul,
					db_ptiik_event.tbl_kegiatan.nama as judul_ori,
					db_ptiik_event.tbl_kegiatan.keterangan,
					db_ptiik_event.tbl_kegiatan.kode_kegiatan,
					db_ptiik_event.tbl_kegiatan.lokasi,
					db_ptiik_event.tbl_kegiatan.tgl_mulai,
					db_ptiik_event.tbl_kegiatan.tgl_selesai,					
					db_ptiik_event.tbl_kegiatan.jenis_kegiatan,
					db_ptiik_event.tbl_kegiatan.unit_id,
					db_ptiik_event.tbl_kegiatan.logo,
					db_ptiik_event.tbl_kegiatan.is_aktif,
					db_ptiik_event.tbl_kegiatan.parent_id,
					db_ptiik_event.tbl_kegiatan.user_id,
					db_ptiik_event.tbl_kegiatan.last_update,
					db_ptiik_apps.tbl_unit_kerja.keterangan as unit
					FROM
					db_ptiik_event.tbl_kegiatan 
					LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_event.tbl_kegiatan.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
					WHERE tbl_kegiatan.is_aktif='1' 
					 ";
		if($cat) $sql.= " AND tbl_kegiatan.jenis_kegiatan='$cat' ";
		if($id) $sql.= " AND (mid(md5(db_ptiik_event.tbl_kegiatan.kegiatan_id),9,7)='$id' OR tbl_kegiatan.kegiatan_id='$id') ";
		if($parent!="-") $sql.= " AND (tbl_kegiatan.parent_id ='$parent' OR mid(md5(db_ptiik_event.tbl_kegiatan.parent_id),9,7)='$parent') ";
		if($tgl) $sql.= " AND 
						('".$tgl."' BETWEEN tbl_kegiatan.tgl_mulai AND tbl_kegiatan.tgl_selesai) ";
		
		$sql.= " ORDER BY tbl_kegiatan.tgl_mulai DESC ";
		
		//echo $sql;
		if($id) return $this->db->getRow($sql);
		else return $this->db->query($sql);		
	}
	
	function get_ptiik_event($id=NULL){
		$sql = "SELECT kegiatan_id, nama, keterangan, logo FROM db_ptiik_event.tbl_kegiatan WHERE kode_kegiatan='$id' AND is_aktif=1 ";
		return $this->db->getRow($sql);	
	}
		
	
	function update_hits_file($str=NULL){
		$sql = "UPDATE db_ptiik_coms.coms_file SET content_hit=content_hit+1 WHERE (mid(md5(db_ptiik_coms.coms_file.file_id),9,7)='".$str."' OR coms_file.file_id='$str') ";
		
		return $this->db->query($sql);	
	}
	
	
	function jenis_kegiatan(){
		$sql = "SELECT 
					tbl_jeniskegiatan.jenis_kegiatan_id,
					tbl_jeniskegiatan.keterangan
				FROM db_ptiik_apps.tbl_jeniskegiatan
				WHERE tbl_jeniskegiatan.jenis_kegiatan_id NOT IN  ('proker','krs','jurnal') ORDER BY tbl_jeniskegiatan.keterangan ASC";
		return $this->db->query($sql);
	}
	
	function get_agenda_day($date){
		$sql = "SELECT 
					DISTINCT DATE(tbl_agenda.tgl_mulai) tgl_mulai
				FROM db_ptiik_apps.tbl_agenda
				WHERE tbl_agenda.fakultas_id = 'PTIIK'
					AND tbl_agenda.tgl_mulai BETWEEN DATE('$date' - INTERVAL 6 MONTH) AND DATE('$date' + INTERVAL 6 MONTH)";
		return $this->db->query($sql);
	}
	
	function get_agenda_bulan($tgl, $bulan, $tahun, $lang=NULL){
		$sql = "SELECT 
					MID(MD5(tbl_agenda.agenda_id),9,7) agenda_id,
					tbl_agenda.jenis_kegiatan_id,
					tbl_jeniskegiatan.keterangan as kategori_kegiatan, 
					db_ptiik_apps.tbl_agenda.judul as judul_ori, 
					tbl_agenda.lokasi, ";
		if(strtolower($lang)=='in') $sql .= "db_ptiik_apps.tbl_agenda.judul as judul, db_ptiik_apps.tbl_agenda.keterangan," ;
		else $sql .= "db_ptiik_apps.tbl_agenda.english_version as judul, db_ptiik_apps.tbl_agenda.note_english as keterangan, " ;
			$sql.= "
					DATE(tbl_agenda.tgl_mulai) tgl_mulai,
					DATE(tbl_agenda.tgl_selesai) tgl_selesai,
					TIME(tbl_agenda.tgl_mulai) jam_mulai,
					TIME(tbl_agenda.tgl_selesai) jam_selesai,
					tbl_agenda.inf_hari,
					tbl_agenda.inf_peserta,
					tbl_agenda.page_link
				FROM db_ptiik_apps.tbl_agenda
				INNER JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_agenda.jenis_kegiatan_id
				WHERE tbl_agenda.fakultas_id = 'PTIIK'
					AND '$bulan' BETWEEN MONTH(tbl_agenda.tgl_mulai) AND  MONTH(tbl_agenda.tgl_selesai)
					AND '$tahun' BETWEEN YEAR(tbl_agenda.tgl_mulai) AND  YEAR(tbl_agenda.tgl_selesai)
					";
		
		if($tgl != '') $sql .= " AND '$tgl' BETWEEN DAY(tbl_agenda.tgl_mulai) AND DAY(tbl_agenda.tgl_selesai)";
		
		$sql .= "ORDER BY tbl_agenda.tgl_mulai desc";
		
		return $this->db->query($sql);
	}
	
	function get_agenda_json($date=NULL, $lang=NULL){
		$sql = "SELECT 
					MID(MD5(tbl_agenda.agenda_id),9,7) agenda_id,
					tbl_agenda.jenis_kegiatan_id,
					tbl_jeniskegiatan.keterangan as kategori_kegiatan, 
					db_ptiik_apps.tbl_agenda.judul as judul_ori, 
					tbl_agenda.lokasi, ";
				if(strtolower($lang)=='in') $sql .= "db_ptiik_apps.tbl_agenda.judul as judul, db_ptiik_apps.tbl_agenda.keterangan," ;
		else $sql .= "db_ptiik_apps.tbl_agenda.english_version as judul, db_ptiik_apps.tbl_agenda.note_english as keterangan, " ;
		$sql.= "
					DATE(tbl_agenda.tgl_mulai) tgl_mulai,
					DATE(tbl_agenda.tgl_selesai) tgl_selesai,
					TIME(tbl_agenda.tgl_mulai) jam_mulai,
					TIME(tbl_agenda.tgl_selesai) jam_selesai,
					tbl_agenda.inf_hari,
					tbl_agenda.inf_peserta,
					tbl_agenda.page_link
				FROM db_ptiik_apps.tbl_agenda
				INNER JOIN db_ptiik_apps.tbl_jeniskegiatan ON tbl_jeniskegiatan.jenis_kegiatan_id = tbl_agenda.jenis_kegiatan_id
				WHERE tbl_agenda.fakultas_id = 'PTIIK'
					AND
					( 
						'$date' BETWEEN DATE(tbl_agenda.tgl_mulai) AND DATE(tbl_agenda.tgl_selesai)
						OR  DATE(tbl_agenda.tgl_mulai) > '$date'
					)
				ORDER BY tbl_agenda.tgl_mulai desc";
				
		return $this->db->query($sql);
	}
	
	function get_search_karyawan($nilai){
		$sql = "SELECT 
					tbl_karyawan.nama,
					tbl_karyawan.biografi,
					tbl_karyawan.last_update,
					tbl_karyawan.foto,
					tbl_karyawan.is_status,
					MID(MD5(tbl_karyawan.karyawan_id),9,7) karyawan_id
				FROM db_ptiik_apps.tbl_karyawan
				WHERE tbl_karyawan.nama LIKE '%".$nilai."%'
				ORDER BY tbl_karyawan.nama";
		return $this->db->query($sql);
	}
	

	function read_mutu($cat=NULL, $lang=NULL, $unit=NULL, $siklus=NULL,$id=NULL){
		$sql = "SELECT
				mid(md5(tbl_gjm_file.file_id),9,7) as id, 
				tbl_gjm_file.file_id,
				tbl_gjm_file.kategori_id,
				tbl_gjm_file.siklus,
				tbl_gjm_file.judul as judul_ori,				
				tbl_gjm_file.keterangan,
				tbl_gjm_file.no_dokumen,
				tbl_gjm_file.unit_id,
				tbl_gjm_file.fakultas_id,
				tbl_gjm_file.cabang_id,
				tbl_gjm_file.file_loc,
				tbl_gjm_file.is_publish,
				tbl_unit_kerja.keterangan as unit_ori,
				";
		if(strtolower($lang)=='in') $sql .= "tbl_gjm_file.judul, db_ptiik_apps.tbl_unit_kerja.keterangan AS unit" ;
		else $sql .= "tbl_gjm_file.english_version as judul,db_ptiik_apps.tbl_unit_kerja.english_version AS unit" ;
		$sql.= "
				FROM
				db_ptiik_apps.tbl_gjm_file
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_gjm_file.unit_id = tbl_unit_kerja.unit_id WHERE 1    
				";
		if($cat) $sql.= " AND tbl_gjm_file.kategori_id='$cat' ";
		if($unit) $sql.= " AND tbl_gjm_file.unit_id='$unit' ";		
		if($siklus) $sql.= " AND tbl_gjm_file.siklus='$siklus' ";
		if($id) $sql.= " AND (tbl_gjm_file.file_id='$id' OR mid(md5(tbl_gjm_file.file_id),9,7) = '$id')  ";
		
		$sql.= "ORDER BY db_ptiik_apps.tbl_gjm_file.file_id ASC";
		
	//	echo $sql."<br>";;
		if($id) return $this->db->getRow( $sql );		
		else return $this->db->query( $sql );
	}
	
	function read_mutu_siklus($cat=NULL, $lang=NULL, $unit=NULL, $id=NULL){
		$sql = "SELECT DISTINCT 
				tbl_gjm_file.unit_id,
				tbl_gjm_file.siklus, ";
		if(strtolower($lang)=='in') $sql .= "'Siklus' AS nama " ;
		else $sql .= "'Cycle' AS 'nama' " ;			
		$sql.= " FROM
				db_ptiik_apps.tbl_gjm_file
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_gjm_file.unit_id = tbl_unit_kerja.unit_id WHERE tbl_gjm_file.is_publish='1'   
				";
		if($cat) $sql.= " AND tbl_gjm_file.kategori_id='$cat' ";
		if($unit) $sql.= " AND tbl_gjm_file.unit_id='$unit' ";		
		if($id) $sql.= " AND (tbl_gjm_file.file_id='$id' OR mid(md5(tbl_gjm_file.file_id),9,7) = '$id')  ";
		
		$sql.= "ORDER BY db_ptiik_apps.tbl_gjm_file.siklus ASC";
		
		//echo $sql."<br>";;
		if($id) return $this->db->getRow( $sql );		
		else return $this->db->query( $sql );
	}
	
	function read_mutu_unit($cat=NULL, $lang=NULL){
		$sql = "SELECT DISTINCT
				tbl_gjm_file.is_publish,
				tbl_gjm_file.kategori_id,
				tbl_unit_kerja.keterangan AS unit_ori,";
		if(strtolower($lang)=='in') $sql .= " db_ptiik_apps.tbl_unit_kerja.keterangan AS unit," ;
		else $sql .= " db_ptiik_apps.tbl_unit_kerja.english_version AS unit," ;
		$sql .= " tbl_gjm_file.unit_id,
				mid(md5(tbl_gjm_file.unit_id),9,7) as id
				FROM
				db_ptiik_apps.tbl_gjm_file
				LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON tbl_gjm_file.unit_id = tbl_unit_kerja.unit_id WHERE 1   
				";
		if($cat) $sql.= " AND tbl_gjm_file.kategori_id='$cat' ";
		//echo $sql."<br>";
		return $this->db->query( $sql );
	}
	
	function get_search_content($nilai=NULL, $lang=NULL, $fak=NULL, $cabang=NULL, $unit=NULL){
		$nilai = strtolower($nilai);
		$sql = "SELECT
					MID(MD5(coms_content.content_id),9,7) AS id,
					db_ptiik_coms.coms_content.content_title,
					MID(coms_content.content,1,400) AS content,
					db_ptiik_coms.coms_content.content_data,
					db_ptiik_coms.coms_content.content_upload,
					db_ptiik_coms.coms_content.content_thumb_img,
					db_ptiik_coms.coms_content.content_category,
					db_ptiik_coms.coms_content.unit_id,
					if(db_ptiik_apps.tbl_unit_kerja.kategori='laboratorium', 'lab',db_ptiik_apps.tbl_unit_kerja.kategori) as kategori ,
					lcase(db_ptiik_apps.tbl_unit_kerja.kode) as kode_unit
					FROM
					db_ptiik_coms.coms_content
					LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_coms.coms_content.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
				WHERE (lcase(coms_content.content_title) LIKE '%$nilai%' OR lcase(coms_content.content) LIKE '%$nilai%')
					AND coms_content.content_status = 'publish' AND coms_content.content_category NOT IN ('inside_menu')
					
				";
			if($lang) $sql.=" AND coms_content.content_lang = '$lang' ";
			if($fak) $sql.=" AND coms_content.fakultas_id = '$fak' ";
			if($cabang) $sql.=" AND coms_content.cabang_id = '$cabang' ";
			if($unit) $sql.=" AND coms_content.unit_id = '$unit' ";
					
				$sql.=" ORDER BY coms_content.content_upload DESC, coms_content.last_update DESC ";
		
		//  echo $sql;
		return $this->db->query($sql);
	}
	
	
	
	function get_unit_kerja($cat=NULL, $fakultas=NULL,$lang=NULL,  $kode=NULL, $id=NULL, $parent=NULL, $keterangan=NULL){
		$sql = "SELECT
				mid(md5(db_ptiik_apps.tbl_unit_kerja.unit_id),9,7) as `id`,
				tbl_unit_kerja.unit_id,
				tbl_unit_kerja.fakultas_id,
				tbl_unit_kerja.keterangan as unit_ori,
				";
		if(strtolower($lang)=='in') $sql .= "db_ptiik_apps.tbl_unit_kerja.keterangan AS unit,db_ptiik_apps.tbl_unit_kerja.tentang AS about," ;
		else $sql .= "db_ptiik_apps.tbl_unit_kerja.english_version AS unit,db_ptiik_apps.tbl_unit_kerja.about AS about," ;
		
		$sql.= "tbl_unit_kerja.kode,
				tbl_unit_kerja.parent_id,
				tbl_unit_kerja.cabang_id,
				tbl_unit_kerja.kategori,
				tbl_unit_kerja.is_aktif,
				tbl_unit_kerja.is_riset,
				tbl_unit_kerja.host_name,
				tbl_unit_kerja.strata,
				tbl_unit_kerja.icon,
				tbl_unit_kerja.logo
				FROM
				db_ptiik_apps.tbl_unit_kerja WHERE is_aktif='1' AND tbl_unit_kerja.kategori <> 'fakultas' ";
		if($cat) $sql.= " AND tbl_unit_kerja.kategori = '$cat' ";
		if($fakultas) $sql.= " AND tbl_unit_kerja.fakultas_id = '$fakultas' ";
		if($kode) $sql.= " AND (lcase(tbl_unit_kerja.kode) = '$kode') ";
		if($id) $sql.= " AND (mid(md5(db_ptiik_apps.tbl_unit_kerja.unit_id),9,7) = '$id'  OR (tbl_unit_kerja.unit_id = '$id'))";
		if($parent) $sql.= " AND (mid(md5(db_ptiik_apps.tbl_unit_kerja.parent_id),9,7) = '$parent'  OR (tbl_unit_kerja.parent_id = '$parent'))";
		
		if($keterangan){
			if(strtolower($lang)=='in') $sql .= " AND db_ptiik_apps.tbl_unit_kerja.keterangan = '$keterangan' " ;
			else $sql .= " AND  db_ptiik_apps.tbl_unit_kerja.english_version = '$keterangan' " ;
		}
		
		$sql .= " ORDER BY tbl_unit_kerja.strata DESC, tbl_unit_kerja.kode ASC";
	//if($cat=='riset') echo $sql."<br>";
		if($kode):
			return $this->db->getRow( $sql );		
		else:
			if($keterangan){
				return $this->db->getRow( $sql );		
			}else{
				if($id) return $this->db->getRow( $sql );
				else	return $this->db->query( $sql );
			}
		endif;
		
		
	}
	
	function get_peserta_by_group($id=NULL, $jenis=NULL, $status=NULL){
		$sql = "SELECT DISTINCT group_by 
				FROM
				 `db_ptiik_apps`.`tbl_agenda_peserta`
				WHERE 
					 group_by<> 'nama' AND agenda_id = '".$id."' AND  sebagai='".$jenis."' 
				";
		if($status){
			$sql = $sql . "AND `tbl_agenda_peserta`.`jenis_peserta`='".$status."' ";
		}			
		
		return $this->db->query( $sql );
	}
	
	
	function get_peserta_agenda($id=NULL, $jenis=NULL, $status=NULL, $grup=NULL){
		$sql = "SELECT
				tbl_agenda_peserta.`peserta_id`,
				tbl_agenda_peserta.`karyawan_id`,
				tbl_agenda_peserta.`mahasiswa_id`,
				tbl_agenda_peserta.`agenda_id`,
				tbl_agenda_peserta.`jenis_peserta`,
				tbl_agenda_peserta.`nama`,
				tbl_agenda_peserta.`instansi`,
				tbl_agenda_peserta.`sebagai`
				FROM
				 `db_ptiik_apps`.tbl_agenda_peserta
				WHERE 
					1= 1 AND (mid(md5(tbl_agenda_peserta.agenda_id),5,5)= '".$id."' OR `db_ptiik_apps`.tbl_agenda_peserta.agenda_id = '".$id."') AND  sebagai='".$jenis."' 
				";
		if($status){
			$sql = $sql . "AND tbl_agenda_peserta.`jenis_peserta`='".$status."' ";
		}
		
		if($grup){
			$sql = $sql . "AND tbl_agenda_peserta.`group_by`='".$grup."' ";
		}
		
		$sql = $sql. "ORDER BY tbl_agenda_peserta.`nama` ASC";
		
		return $this->db->query( $sql );
	}
	
	function read_event($unit=NULL, $lang=NULL, $str=NULL, $id=NULL, $limit = NULL){
		$sql = "SELECT
					mid(md5(db_ptiik_apps.tbl_agenda.agenda_id),9,7) as `id`,
					db_ptiik_apps.tbl_agenda.agenda_id,
					db_ptiik_apps.tbl_agenda.unit_id,
					db_ptiik_apps.tbl_agenda.jenis_kegiatan_id,
					db_ptiik_apps.tbl_agenda.judul as judul_ori, db_ptiik_apps.tbl_agenda.keterangan as keterangan_ori,
					db_ptiik_apps.tbl_agenda.lokasi,";
					
		if(strtolower($lang)=='in') $sql .= "db_ptiik_apps.tbl_agenda.judul as judul, db_ptiik_apps.tbl_agenda.keterangan,db_ptiik_apps.tbl_unit_kerja.keterangan AS unit," ;
		else $sql .= "db_ptiik_apps.tbl_agenda.english_version as judul, db_ptiik_apps.tbl_agenda.note_english as keterangan, db_ptiik_apps.tbl_unit_kerja.english_version AS unit," ;
		
		$sql .= "db_ptiik_apps.tbl_agenda.penyelenggara,
					db_ptiik_apps.tbl_agenda.tgl_mulai,
					db_ptiik_apps.tbl_agenda.tgl_mulai as content_modified,
					db_ptiik_apps.tbl_agenda.tgl_selesai as content_modified_end,
					db_ptiik_apps.tbl_agenda.tgl_selesai,
					db_ptiik_apps.tbl_agenda.is_allday,
					db_ptiik_apps.tbl_agenda.page_link,
					db_ptiik_apps.tbl_agenda.inf_peserta,
					db_ptiik_apps.tbl_agenda.inf_hari,
					db_ptiik_apps.tbl_agenda.icon_thumb as icon,
					db_ptiik_apps.tbl_agenda.jenis_agenda,
					db_ptiik_apps.tbl_agenda.status_agenda,
					db_ptiik_apps.tbl_agenda.inf_ruang,
					db_ptiik_apps.tbl_agenda.is_delete,
					db_ptiik_apps.tbl_agenda.group_peserta,
					db_ptiik_apps.tbl_agenda.inf_undangan,
					db_ptiik_apps.tbl_agenda.user_id,
					db_ptiik_apps.tbl_agenda.last_update,
					db_ptiik_apps.tbl_agenda.last_update ,
					db_ptiik_apps.tbl_unit_kerja.keterangan AS unit_ori,
					db_ptiik_apps.tbl_jeniskegiatan.keterangan AS kegiatan
					FROM
					db_ptiik_apps.tbl_agenda
					LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_apps.tbl_agenda.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id
					INNER JOIN db_ptiik_apps.tbl_jeniskegiatan ON db_ptiik_apps.tbl_agenda.jenis_kegiatan_id = db_ptiik_apps.tbl_jeniskegiatan.jenis_kegiatan_id WHERE db_ptiik_apps.tbl_agenda.status_agenda = 'publish' ";
		if($id){
			$sql = $sql. " AND (tbl_agenda.agenda_id = '".$id."' OR mid(md5(db_ptiik_apps.tbl_agenda.agenda_id),9,7) = '".$id."') ";
		}
			
		if($unit && $unit!=0) $sql.= " AND db_ptiik_apps.tbl_agenda.unit_id ='$unit' ";
		
		$sql = $sql. " ORDER BY tbl_agenda.tgl_mulai DESC LIMIT 0, ".$limit;
		
		//echo $sql."<br>";
		if($id) $result = $this->db->getRow( $sql ); 
		else $result = $this->db->query( $sql );
				
		return $result;

	}
	
	/*function read_comment($content=NULL, $parent=NULL){
		$sql = "SELECT
					content_comments.id,
					content_comments.content_id,
					content_comments.comment,
					content_comments.user_name,
					content_comments.email,
					content_comments.parent_id,
					content_comments.last_update,
					content_comments.approved,
					content_comments.ip_address,
					contents.content_page,
					contents.content_title,
					coms_user.foto,
					coms_user.username,
					coms_user.name,
					content_comments.tgl_comment,
					content_comments.user_id
				FROM
					db_ptiik_coms.content_comments
					INNER JOIN db_ptiik_coms.contents ON db_ptiik_coms.content_comments.content_id = db_ptiik_coms.coms_content.content_id
					LEFT JOIN db_ptiik_coms.coms_user ON db_ptiik_coms.content_comments.email = db_ptiik_coms.coms_user.email
				WHERE 1 ";
		if($parent!=""){
			$sql = $sql. "AND content_comments.approved='1'  
					 AND content_comments.parent_id = '".$parent."' ";
		}
		if($content!=""){
			$sql = $sql. " AND content_comments.content_id = '".$content."' ";
		}
		$sql = $sql. " ORDER BY approved ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;

	}
	
	function cek_last_comment($user_id,$id){
		$sql = "SELECT content_comments.approved as approved
				FROM db_ptiik_coms.content_comments
				WHERE content_comments.content_id = '".$id."'
				AND content_comments.user_id = '".$user_id."'
				ORDER BY content_comments.tgl_comment DESC
				LIMIT 1";
		$result = $this->db->getRow( $sql );
		if(isset($result)){
			$appr = $result->approved;
			return $appr;
		}
		else {
			return FALSE;	
		}
		//echo $sql;
	}
	
	function delete_comments($id=NULL){
		$sql = "DELETE FROM db_ptiik_coms.content_comments
				WHERE content_comments.id = '".$id."'";
		$result = $this->db->query( $sql );
		if($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	function update_status_comments($id=NULL,$status=NULL,$lastupdate=NULL){
		$sql = "UPDATE db_ptiik_coms.content_comments
				SET content_comments.approved = '".$status."', content_comments.last_update = '".$lastupdate."'
				WHERE content_comments.id = '".$id."'";
		$result = $this->db->query( $sql );
		if($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}*/
	
	function read_gal($unit=NULL, $lang=NULL, $limit=NULL, $cat=NULL, $id=NULL, $parent=NULL, $event=NULL, $keyword=null, $page=null){
		if($keyword) $keyw = strToLower($keyword);
		else $keyw="";
		
		$sql= "SELECT
					mid(md5(coms_file.file_id),9,7) as id, 
					coms_file.file_id,
					coms_file.content_category,
					coms_file.content_data,
					coms_file.content_hit,
					coms_file.fakultas_id,
					coms_file.unit_id,
					coms_file.cabang_id,
					coms_file.file_lang,
					coms_file.file_title,
					coms_file.file_note,
					coms_file.file_data,
					coms_file.file_group,
					coms_file.file_type,
					coms_file.file_name,
					coms_file.file_size,
					coms_file.file_loc,
					coms_file.is_publish,
					coms_file.user_id,
					coms_file.parent_id,
					coms_file.last_update
					FROM
					db_ptiik_coms.coms_file WHERE is_publish='1' ";
		if($unit && $unit!="-") $sql.=" AND coms_file.unit_id = '$unit' ";
		
		if($cat):
			$sql.= " AND coms_file.content_category IN (".$cat.") "; //$sql.= " AND coms_file.content_category IN ('gallery', 'video') "; //$sql.= " AND coms_file.content_category='$cat' ";
		else:
			if(!$id) $sql.= " AND coms_file.content_category='slider' ";
		endif;
		
		if($event) $sql.= " AND db_ptiik_coms.coms_file.event_id = '$event' ";
		else $sql.= " AND db_ptiik_coms.coms_file.event_id ='0'";
		
		if($lang) $sql.= " AND coms_file.file_lang = '$lang' "; 
		if($cat='gallery' && !$id){
			if($parent):
				$sql.= " AND coms_file.parent_id = '$parent' "; 
			else:
				if(!$keyw) $sql.= " AND coms_file.parent_id = '0' "; 
			endif;
		}
		
		if($id) $sql.= " AND (mid(md5(coms_file.file_id),9,7) ='$id' OR coms_file.file_id='$id') ";
		if($keyw) $sql.= " AND (lcase(file_title) like '%$keyw%' or lcase(file_note) like '%$keyw%')";
		$sql.= " ORDER BY last_update DESC ";
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			if(!$limit) $result = $this->db->query( $sql );
			else {
				$result = $query = $this->db->query( $sql );
				$start = (!$page)?0*$limit:$page*$limit;
				if($query){
					$result = array();
					foreach ($query as $key => $value)
						if($key >= $start && $key < $start+$limit) $result [] = $value;
				}
			}
		}
		return $result;
	}
	
	function read_slide($unit=NULL, $lang=NULL, $limit=NULL, $cat=NULL, $id=NULL, $parent=NULL, $event=NULL, $keyword=null, $page=null){
		$sql= "SELECT
					mid(md5(coms_file.file_id),9,7) as id, 
					coms_file.file_id,
					coms_file.content_category,
					coms_file.content_data,
					coms_file.content_hit,
					coms_file.fakultas_id,
					coms_file.unit_id,
					coms_file.cabang_id,
					coms_file.file_lang,
					coms_file.file_title,
					coms_file.file_note,
					coms_file.file_data,
					coms_file.file_group,
					coms_file.file_type,
					coms_file.file_name,
					coms_file.file_size,
					coms_file.file_loc,
					coms_file.is_publish,
					coms_file.user_id,
					coms_file.parent_id,
					coms_file.last_update
					FROM
					db_ptiik_coms.coms_file WHERE is_publish='1' ";
		if($unit && $unit=="-") $sql.=" AND coms_file.unit_id = '0' ";
		else $sql.=" AND coms_file.unit_id = '$unit' ";
		
		if($cat):
			$sql.= " AND coms_file.content_category IN (".$cat.") "; //$sql.= " AND coms_file.content_category IN ('gallery', 'video') "; //$sql.= " AND coms_file.content_category='$cat' ";
		else:
			if(!$id) $sql.= " AND coms_file.content_category='slider' ";
		endif;
		
		if($event) $sql.= " AND db_ptiik_coms.coms_file.event_id = '$event' ";
		else $sql.= " AND db_ptiik_coms.coms_file.event_id ='0'";
		
		if($lang) $sql.= " AND coms_file.file_lang = '$lang' "; 
		if($cat='gallery' && !$id){
			if($parent)	$sql.= " AND coms_file.parent_id = '$parent' "; 
			else $sql.= " AND coms_file.parent_id = '0' "; 
		}
		
		if($id) $sql.= " AND (mid(md5(coms_file.file_id),9,7) ='$id' OR coms_file.file_id='$id') ";
		if($keyword) $sql.= " AND (file_title like '%$keyword%' or file_note like '%$keyword%')";
		$sql.= " ORDER BY last_update DESC ";
		if($limit) $sql.= " LIMIT 0, $limit ";
		if($id){
			$result = $this->db->getRow( $sql );
		}else{			
			$result = $query = $this->db->query( $sql );				
		}
		return $result;
	}
	
	
	function read_slideX($unit=NULL, $lang=NULL, $limit=NULL, $cat=NULL, $id=NULL, $parent=NULL, $event=NULL){
		
		$sql= "SELECT
					mid(md5(coms_file.file_id),9,7) as id, 
					coms_file.file_id,
					coms_file.content_category,
					coms_file.content_data,
					coms_file.content_hit,
					coms_file.fakultas_id,
					coms_file.unit_id,
					coms_file.cabang_id,
					coms_file.file_lang,
					coms_file.file_title,
					coms_file.file_note,
					coms_file.file_data,
					coms_file.file_group,
					coms_file.file_type,
					coms_file.file_name,
					coms_file.file_size,
					coms_file.file_loc,
					coms_file.is_publish,
					coms_file.user_id,
					coms_file.parent_id,
					coms_file.last_update
					FROM
					db_ptiik_coms.coms_file WHERE is_publish='1' ";
		if($unit!="-") $sql.=" AND coms_file.unit_id = '$unit' ";
		
		if($cat):
			$sql.= " AND coms_file.content_category IN (".$cat.") "; //$sql.= " AND coms_file.content_category IN ('gallery', 'video') "; //$sql.= " AND coms_file.content_category='$cat' ";
		else:
			if(!$id) $sql.= " AND coms_file.content_category='slider' ";
		endif;
		
		if($event) $sql.= " AND db_ptiik_coms.coms_file.event_id = '$event' ";
		else $sql.= " AND db_ptiik_coms.coms_file.event_id ='0'";
		
		if($lang) $sql.= " AND coms_file.file_lang = '$lang' "; 
		if($cat='gallery' && !$id){
			if($parent)	$sql.= " AND coms_file.parent_id = '$parent' "; 
			else $sql.= " AND coms_file.parent_id = '0' "; 
		}
		
		if($id) $sql.= " AND (mid(md5(coms_file.file_id),9,7) ='$id' OR coms_file.file_id='$id') ";
		
		$sql.= " ORDER BY last_update DESC LIMIT 0, $limit ";
		//if($cat=='slider_standard') echo $sql;
	
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		return $result;
	}
	
	function read_course($strata=NULL,$unit=NULL, $lang=NULL,$str=NULL, $id=NULL){
		
		if($id):
			$sql= "SELECT
					mid(md5(db_ptiik_apps.tbl_silabus.mkditawarkan_id),9,7) as `id`, ";
					if(strtolower($lang)=='in') $sql.= "
					db_ptiik_apps.tbl_namamk.keterangan AS namamk,db_ptiik_apps.tbl_namamk.keterangan AS judul, ";
			else $sql.= " db_ptiik_apps.tbl_namamk.english_version AS namamk, db_ptiik_apps.tbl_namamk.english_version AS judul,";
					$sql.="
					db_ptiik_apps.tbl_silabus_komponen.keterangan AS komponen,
					db_ptiik_apps.tbl_silabus_detail.keterangan
					FROM
					db_ptiik_apps.tbl_mkditawarkan
					INNER JOIN db_ptiik_apps.tbl_silabus ON db_ptiik_apps.tbl_silabus.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					INNER JOIN db_ptiik_apps.tbl_silabus_detail ON db_ptiik_apps.tbl_silabus.silabus_id = db_ptiik_apps.tbl_silabus_detail.silabus_id
					INNER JOIN db_ptiik_apps.tbl_silabus_komponen ON db_ptiik_apps.tbl_silabus_detail.komponen_id = db_ptiik_apps.tbl_silabus_komponen.komponen_id
				WHERE 1 = 1 ";
		else:
			$sql = "SELECT DISTINCT
					mid(md5(tbl_mkditawarkan.mkditawarkan_id),9,7) AS id,
					";
			if(strtolower($lang)=='in') $sql.= "
					db_ptiik_apps.tbl_namamk.keterangan AS namamk,db_ptiik_apps.tbl_namamk.keterangan AS judul, ";
			else $sql.= " db_ptiik_apps.tbl_namamk.english_version AS namamk, db_ptiik_apps.tbl_namamk.english_version AS judul,";
					$sql.="
					db_ptiik_apps.tbl_fakultas.keterangan AS fakultas,
					db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id,
					db_ptiik_apps.tbl_mkditawarkan.icon AS icon
					FROM
						db_ptiik_apps.tbl_mkditawarkan
						INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
						INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
						INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
						INNER JOIN db_ptiik_apps.tbl_fakultas ON db_ptiik_apps.tbl_namamk.fakultas_id = db_ptiik_apps.tbl_fakultas.fakultas_id	
							INNER JOIN db_ptiik_apps.tbl_silabus ON db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = db_ptiik_apps.tbl_silabus.mkditawarkan_id						
					WHERE
						db_ptiik_apps.tbl_tahunakademik.is_aktif = 1 
					";
		endif;
		
		if($strata) $sql.= " AND db_ptiik_apps.tbl_matakuliah.strata='$strata' ";
		
		if($id){
			$sql = $sql. "AND mid(md5(db_ptiik_apps.tbl_silabus.mkditawarkan_id),9,7) = '".$id."' ";
		}
		
		if($str){			
			if($str=='main'){
				$limit = "0,8";	
			}else{
				$limit = "0,4";	
			}		
		}else{
			$limit = "0, 100";
		}
		
		if($id)	$sql.= " ORDER BY db_ptiik_apps.tbl_silabus_komponen.urut ASC LIMIT ".$limit;	
		else $sql.= " ORDER BY db_ptiik_apps.tbl_matakuliah.kode_mk ASC, db_ptiik_apps.tbl_namamk.keterangan ASC LIMIT ".$limit;
		
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		//echo $sql;
		return $result;
	}
	
	function get_komponen_silabus(){
		$sql = "SELECT
					`tbl_silabus_komponen`.`komponen_id`,
					`tbl_silabus_komponen`.`keterangan` as `keterangan`,
					`tbl_silabus_komponen`.`is_aktif`
					FROM
					`db_ptiik_apps`.`tbl_silabus_komponen`
					WHERE `tbl_silabus_komponen`.`is_aktif` = 1 ORDER BY `tbl_silabus_komponen`.`menu_order` ASC";
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function read_silabus($unit=NULL, $lang=NULL,$id=NULL){
		$sql = "SELECT
					db_ptiik_apps.tbl_silabus_komponen.keterangan AS komponen,
					db_ptiik_apps.tbl_silabus_detail.keterangan,
					`tbl_silabus_komponen`.`komponen_id`
					FROM
					db_ptiik_apps.tbl_silabus
					INNER JOIN db_ptiik_apps.tbl_silabus_detail ON db_ptiik_apps.tbl_silabus.silabus_id = db_ptiik_apps.tbl_silabus_detail.silabus_id
					INNER JOIN db_ptiik_apps.tbl_silabus_komponen ON db_ptiik_apps.tbl_silabus_detail.komponen_id = db_ptiik_apps.tbl_silabus_komponen.komponen_id
				WHERE 1 = 1 
					";
		if($id){
			$sql = $sql. "AND mid(md5(db_ptiik_apps.tbl_silabus.mkditawarkan_id),9,7) = '".$id."' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function read_topic($unit=NULL, $lang=NULL,$str=NULL, $id=NULL){
		$sql = "SELECT
				tbl_fakultas.fakultas_id,
				tbl_fakultas.keterangan AS fakultas,
				tbl_namamk.keterangan AS namamk,
				mid(md5(tbl_materimk.mkditawarkan_id),9,7) as `id`,
				mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7) as `mid`,
				tbl_mkditawarkan.icon AS icon_mk,
				tbl_materimk.icon,
				tbl_materimk.judul,
				tbl_materimk.keterangan,
				tbl_materimk.is_publish,
				tbl_materimk.hits,
				tbl_materimk.`status`,
				tbl_materimk.materi_id,
				tbl_materimk.is_valid,
				tbl_materimk.mkditawarkan_id
				FROM
				db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
				INNER JOIN db_ptiik_apps.tbl_fakultas ON db_ptiik_apps.tbl_namamk.fakultas_id = db_ptiik_apps.tbl_fakultas.fakultas_id
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
				INNER JOIN db_ptiik_apps.tbl_materimk ON db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id = db_ptiik_apps.tbl_materimk.mkditawarkan_id
				WHERE
				db_ptiik_apps.tbl_materimk.is_valid ='1' AND db_ptiik_apps.tbl_materimk.is_publish ='1'  
		";
		if($id){			
			$sql = $sql . " AND mid(md5(db_ptiik_apps.tbl_materimk.materi_id),9,7) = '".$id."' ";
			$limit = "0, 20";
		}
		
		if($str){
			if($str=='main'){
				$limit = "0,2";	
			}else{
				$limit = "0,8";	
			}			
		}else{
			$limit = "0, 100";
		}
		
		$sql = $sql. " ORDER BY tbl_materimk.hits DESC, tbl_materimk.last_update DESC, db_ptiik_apps.tbl_namamk.keterangan ASC, db_ptiik_apps.tbl_materimk.menu_order LIMIT ".$limit;
		
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
		
	}
	
	function read_tweet($limit=NULL){
		$sql = "SELECT
					db_ptiik_coms.tbl_twitter.created_at,
					db_ptiik_coms.tbl_twitter.id,
					db_ptiik_coms.tbl_twitter.id_str,
					db_ptiik_coms.tbl_twitter.keterangan,
					db_ptiik_coms.tbl_twitter.`name`,
					db_ptiik_coms.tbl_twitter.screen_name,
					db_ptiik_coms.tbl_twitter.profile_image_url
					FROM
					db_ptiik_coms.tbl_twitter ORDER BY created_at DESC LIMIT 0,$limit
					";
		return $this->db->query( $sql );
	}
	
	function read_sub_content($unit=NULL, $lang=NULL,$cat=NULL, $parent=NULL){
		$sql= "SELECT
				mid(md5(db_ptiik_coms.coms_content.content_id),9,7) as `id`, 
				db_ptiik_coms.coms_content.content_id,
				db_ptiik_coms.coms_content.content_category,
				db_ptiik_coms.coms_content.menu_order,
				db_ptiik_coms.coms_content.unit_id,
				db_ptiik_coms.coms_content.fakultas_id,
				db_ptiik_coms.coms_content.cabang_id,
				db_ptiik_coms.coms_content.content_title,
				db_ptiik_coms.coms_content.content_title as judul,
				db_ptiik_coms.coms_content.content_excerpt,
				db_ptiik_coms.coms_content.content as keterangan,
				db_ptiik_coms.coms_content.content_link as content_page,
				db_ptiik_coms.coms_content.content_data,
				db_ptiik_coms.coms_content.content_status,
				db_ptiik_coms.coms_content.content_lang,
				db_ptiik_coms.coms_content.content_hit as hits,
				db_ptiik_coms.coms_content.content_author,
				db_ptiik_coms.coms_content.content_upload,
				db_ptiik_coms.coms_content.content_comment,
				db_ptiik_coms.coms_content.content_thumb_img,
				db_ptiik_coms.coms_content.content_thumb_img as icon,
				db_ptiik_coms.coms_content.is_sticky,
				db_ptiik_coms.coms_content.user_id,
				db_ptiik_coms.coms_content.last_update,
				db_ptiik_coms.coms_content.parent_id
				FROM
				db_ptiik_coms.coms_content WHERE content_status ='publish' 
				";
		if($cat) $sql.= " AND coms_content.content_category='$cat' ";
		if($lang) $sql.= " AND coms_content.content_lang='$lang' ";
		if($parent) $sql.= " AND (coms_content.parent_id='$parent' OR mid(md5(db_ptiik_coms.coms_content.parent_id),9,7)='$parent') ";
		//$sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		if($unit && $unit!="-") $sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		$sql.=" ORDER BY db_ptiik_coms.coms_content.menu_order ASC ";
		return $this->db->query( $sql );
	}
	
	function read_content($unit=NULL, $lang=NULL,$cat=NULL, $url=NULL, $id=NULL, $slimit=NULL,$order_by=NULL, $parent=NULL, $event=NULL){
	
		$sql= "SELECT
					mid(md5(db_ptiik_coms.coms_content.content_id),9,7) AS id,
					db_ptiik_coms.coms_content.content_id,
					db_ptiik_coms.coms_content.content_category,
					db_ptiik_coms.coms_content.menu_order,
					db_ptiik_coms.coms_content.unit_id,
					db_ptiik_coms.coms_content.fakultas_id,
					db_ptiik_coms.coms_content.cabang_id,
					db_ptiik_coms.coms_content.content_title,
					db_ptiik_coms.coms_content.content_title as judul_ori,
					db_ptiik_coms.coms_content.content_title AS judul,
					db_ptiik_coms.coms_content.content_excerpt,
					db_ptiik_coms.coms_content.content AS keterangan,
					db_ptiik_coms.coms_content.content AS keterangan_ori,
					db_ptiik_coms.coms_content.content_link AS content_page,
					db_ptiik_coms.coms_content.content_data,
					db_ptiik_coms.coms_content.content_status,
					db_ptiik_coms.coms_content.content_lang,
					db_ptiik_coms.coms_content.content_hit AS hits,
					db_ptiik_coms.coms_content.content_author,
					db_ptiik_coms.coms_content.content_upload,
					db_ptiik_coms.coms_content.content_comment,
					db_ptiik_coms.coms_content.content_thumb_img AS thumb_img,
					db_ptiik_coms.coms_content.content_thumb_img AS icon,
					db_ptiik_coms.coms_content.is_sticky,
					db_ptiik_coms.coms_content.user_id,
					db_ptiik_coms.coms_content.last_update AS content_modified,
					(SELECT content_title FROM db_ptiik_coms.coms_content as a WHERE a.content_id= db_ptiik_coms.coms_content.parent_id AND a.content_lang='$lang') as 'parent',
					db_ptiik_coms.coms_content.parent_id,db_ptiik_apps.tbl_unit_kerja.kode as kode_unit, ";
			if($lang=='in') $sql.= " db_ptiik_apps.tbl_unit_kerja.keterangan as unit_note ";
			else  $sql.= " db_ptiik_apps.tbl_unit_kerja.english_version as unit_note ";
			$sql.= "
					FROM
					db_ptiik_coms.coms_content
					LEFT JOIN db_ptiik_apps.tbl_unit_kerja ON db_ptiik_coms.coms_content.unit_id = db_ptiik_apps.tbl_unit_kerja.unit_id 
					WHERE content_status ='publish' AND  db_ptiik_coms.coms_content.content_title <> '' 
				";
		
		if($unit!='-')	$sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		//if($unit) $sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
		if($lang) $sql.= " AND db_ptiik_coms.coms_content.content_lang = '$lang' ";
		if($parent) $sql.= " AND db_ptiik_coms.coms_content.parent_id = '$parent' ";
		if(!$id){
			if($event) $sql.= " AND db_ptiik_coms.coms_content.event_id = '$event' ";
			else $sql.= " AND db_ptiik_coms.coms_content.event_id ='0' ";
		}
		
		if($cat){
			if(($cat!='pengumuman') && ($unit!='-'))	$sql.= " AND db_ptiik_coms.coms_content.unit_id = '$unit' ";
			
			$sql = $sql . " AND db_ptiik_coms.coms_content.content_category='".$cat."' AND db_ptiik_coms.coms_content.parent_id='0'  ";
			
			
			if($slimit){
				$limit = "0,".$slimit;
				
				if($cat=='header_menu')  $order = " db_ptiik_coms.coms_content.menu_order ASC ";
				else $order = "db_ptiik_coms.coms_content.content_upload DESC,db_ptiik_coms.coms_content.last_update DESC ";
			}else{
				if($cat=='news'){
					$order = " db_ptiik_coms.coms_content.content_hit DESC ";
					$limit = "0,5";
				}else{
					if($cat=='header_menu') :
						$order = " db_ptiik_coms.coms_content.menu_order ASC ";
						$limit = "0,100";
					else:
						$order = " db_ptiik_coms.coms_content.menu_order ASC ";
						$limit = "0,10";
					endif; 
				}
			}
		}
		if($url){
			$sql = $sql . " AND db_ptiik_coms.coms_content.content_link='".$url."' ";
			$order = " coms_content.last_update DESC ";
			$limit = "0,1";
		}
		
		if($id){
			$sql = $sql . " AND (db_ptiik_coms.coms_content.content_id='".$id."' OR mid(md5(db_ptiik_coms.coms_content.content_id),9,7) = '".$id."') ";
			$order = " coms_content.last_update DESC ";
			$limit = "0,1";
		}
		
		if($order_by=='urut') $order = " coms_content.menu_order ASC ";
		
		$sql.= " ORDER BY ".$order. " LIMIT ".$limit;
		
	//echo $sql."<br>";
		if(($url) || ($id)) $result = $this->db->getRow( $sql ); 
		else $result = $this->db->query( $sql );
		
		
		return $result;	
		
	}
	
	function  read_video($unit=NULL, $lang=NULL,$id=NULL){
		$sql = "SELECT
				db_ptiik_apps.tbl_file.file_id,
				mid(md5(db_ptiik_apps.tbl_file.file_id),9,7) as `id`,
				db_ptiik_apps.tbl_file.judul,
				db_ptiik_apps.tbl_file.keterangan,
				db_ptiik_apps.tbl_file.file_name,
				db_ptiik_apps.tbl_file.file_loc,
				db_ptiik_apps.tbl_file.tgl_upload,
				db_ptiik_apps.tbl_file.jenis_file_id,
				db_ptiik_apps.tbl_file.materi_id
				FROM
				db_ptiik_apps.tbl_file
				INNER JOIN db_ptiik_apps.tbl_jenisfile ON db_ptiik_apps.tbl_file.jenis_file_id = db_ptiik_apps.tbl_jenisfile.jenis_file_id
				WHERE
				db_ptiik_apps.tbl_file.jenis_file_id = '2013110002' AND db_ptiik_apps.tbl_file.is_publish='1' AND db_ptiik_apps.tbl_file.materi_id='' ";
		if($id){
			$sql = $sql . " AND (db_ptiik_apps.tbl_file.file_id='".$id."'  OR mid(md5(db_ptiik_apps.tbl_file.file_id),9,7) = '".$id."') ";
		}
		$sql = $sql. " ORDER BY db_ptiik_apps.tbl_file.last_update DESC LIMIT 0,2";
		if($id){
			$result = $this->db->getRow( $sql );
		}else{
			$result = $this->db->query( $sql );
		}
		
		return $result;
	}
	
	function content($content, $length) {	
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
	function get_statusmateri(){
		$sql = "SELECT
				db_ptiik_apps.tbl_statusmateri.`status`,
				db_ptiik_apps.tbl_statusmateri.keterangan
				FROM
				db_ptiik_apps.tbl_statusmateri
				";
		$result = $this->db->query($sql);		

		return $result;
	}
	
	function update_hits($id=NULL){
		$sql = "UPDATE db_ptiik_apps.tbl_materimk SET hits=hits+1 WHERE mid(md5(tbl_materimk.materi_id),9,7)='".$id."' ";
		return $this->db->query($sql);	
	}
	
	function update_hits_content($str=NULL){
		$sql = "UPDATE db_ptiik_coms.coms_content SET content_hit=content_hit+1 WHERE mid(md5(db_ptiik_coms.coms_content.content_id),9,7)='".$str."' ";
		
		return $this->db->query($sql);	
	}
	
	
	
	function get_hari($str=NULL){
		switch($str){
			case '1': $hari="Senin";break;
			case '2': $hari="Selasa";break;
			case '3': $hari="Rabu";break;
			case '4': $hari="Kamis";break;
			case '5': $hari="Jumat";break;
			case '6': $hari="Sabtu";break;
			case '7': $hari="Minggu";break;
		}
		
		return $hari;
		
		
	}
	
	//=====comment====//
	function read_comment($content=NULL, $parent=NULL, $category=NULL){
		$sql = "SELECT
					coms_comment.comment_id,
					coms_comment.content_id,
					coms_comment.comment,
					coms_comment.user_name,
					coms_comment.email,
					coms_comment.parent_id,
					coms_comment.last_update,
					coms_comment.is_approve,
					coms_comment.ip_address,
					coms_comment.category,
					coms_content.content_link,
					coms_content.content_title,
					coms_user.foto,
					coms_user.username,
					coms_user.name,
					coms_comment.comment_post,
					coms_comment.user_id
				FROM
					db_ptiik_coms.coms_comment
					LEFT JOIN db_ptiik_coms.coms_content ON db_ptiik_coms.coms_content.content_id = db_ptiik_coms.coms_comment.content_id
					LEFT JOIN db_coms.coms_user ON db_ptiik_coms.coms_comment.email = db_coms.coms_user.email
				WHERE 1";
		if($category){
			$sql = $sql. " AND coms_comment.category = '".$category."' ";
		}
		if($parent!=""){
			$sql = $sql. "AND coms_comment.is_approve='1'  
					 AND coms_comment.parent_id = '".$parent."' ";
		}
		if($content!=""){
			$sql = $sql. " AND coms_comment.content_id = '".$content."' ";
		}
		$sql = $sql. " ORDER BY is_approve ASC";
		// echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;

	}
	
	function cek_last_comment($user_id,$id,$category=NULL){
		$sql = "SELECT coms_comment.is_approve as approved
				FROM db_ptiik_coms.coms_comment
				WHERE coms_comment.content_id = '".$id."'
				AND coms_comment.user_id = '".$user_id."'
				";
		if($category){
			$sql .= " AND coms_comment.category='".$category."'";
		}
		$sql .= "ORDER BY coms_comment.comment_post DESC
				LIMIT 1";
		$result = $this->db->getRow( $sql );
		if(isset($result)){
			$appr = $result->approved;
			return $appr;
		}
		else {
			return FALSE;	
		}
		//echo $sql;
	}
	
	function delete_comments($id=NULL){
		$sql = "DELETE FROM db_ptiik_coms.coms_comment
				WHERE coms_comment.comment_id = '".$id."'";
		$result = $this->db->query( $sql );
		if($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	function update_status_comments($id=NULL,$status=NULL,$lastupdate=NULL){
		$sql = "UPDATE db_ptiik_coms.coms_comment
				SET coms_comment.is_approve = '".$status."', coms_comment.last_update = '".$lastupdate."'
				WHERE coms_comment.comment_id = '".$id."'";
		$result = $this->db->query( $sql );
		if($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	function comment_id(){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(comment_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_coms`.`coms_comment`"; 
		$dt = $this->db->getRow( $sql );
		
		$strresult = $dt->data;
		
		return $strresult;
	}
	
	function replace_comment($datanya){
		return $this->db->replace('db_ptiik_coms`.`coms_comment',$datanya);
	}

	function str_content($strcontent=NULL, $url=NULL, $url_old=NULL, $url_kc=NULL){
		
		$src 	= Array();
		$result = Array();
		
		$string = explode('<', $strcontent);
		
		foreach ($string as $s) {
			/*if(strpos($s,'table') !== false){
				$t_link = explode('<', $s);
				preg_match_all('/".*?"|\'.*?\'/', $t_link[0], $source);
				
				$file_name = str_replace('<table', '<table class="table"', $source[0][0]);
			}*/
			
			if(strpos($s,'href') !== false){
				$a_link = explode('<', $s);
				preg_match_all('/".*?"|\'.*?\'/', $a_link[0], $source);
				$not_allowed_arr = array('"cboxElement"');
				if(in_array($source[0][0], $not_allowed_arr)){
					$source_ = trim($source[0][1]);
					$link_type = '1';
				}else{
					$source_ = trim($source[0][0]);
					$link_type = '0';
				}
				
				//echo $source[0][0]."<br>";
				
				
				$file_name = str_replace('http://filkom.ub.ac.id', $url_old, substr(str_replace('%20', ' ', $source_), 1, -1));
								
				$the_file = explode('/', $file_name);
				$file_type = explode('.', end($the_file));
				$allowed_ext = array('png', 'jpeg', 'jpg', 'pdf','doc','docx','xls','xlsx', 'JPG', '.JPG');
				if(in_array(end($file_type), $allowed_ext)){
					$cek = explode('/', $source_);
					if(in_array('filkom.ub.ac.id', $cek)){ //----cek if url from filkom.ub.ac.id
						if($link_type=='1'){							
							if($source[0][3]):
								$src[] = '<a class='.$source[0][0].' href='.$source_.' rel='.$source[0][2].' style='.$source[0][3].'>';
							else:
								$src[] = '<a class='.$source[0][0].' href='.$source_.' rel='.$source[0][2].'>';
							endif;
						}else{
							if($source[0][1]):
								$src[] = '<a href='.$source_.' style='.$source[0][1].'>';
							else:
								$src[] = '<a href='.$source_.'>';
							endif;
						}
						
						$file_ = explode($_SERVER['HTTP_HOST'], $file_name);
						$file_name = end($file_);						
						
						$print = base64_encode(md5(urlencode($file_name)));
						
						$safe = $file_name;
						if($this->check_pict_session($print, $safe, 'check')==FALSE){
							$datanya = array('val_session' => $print,
											 'val_url' => $safe
											);
							$this->save_pict_session($datanya);
						}
						
						$url_file = "http://file.filkom.ub.ac.id";
						$result[] = '<a href="http://'.$_SERVER['HTTP_HOST']."/page/read_/".$print.'" target="_blank">';
						//$result[] = '<a href="'.$url_file.trim($file_name).'" target="_blank">';
					}else{
						if(in_array('informatika.ub.ac.id', $cek)){ //----cek if url from 253 or 254 kc finder
							$file_name_i = str_replace('http://informatika.ub.ac.id', "http://informatika.ub.ac.id", substr(str_replace('%20', ' ', $source_), 1, -1));
							
							if(isset($source[0][1])):
								$src[] = '<a href='.$source_.' style='.$source[0][1].'>';
							else:
								$src[] = '<a href='.$source_.'>';
							endif;
								
							//$src[] = '<a href='.$source_.'>';
							
							$print = base64_encode(md5(urlencode($file_name_i)));
							$safe = $file_name_i;
							
							if($this->check_pict_session($print, $safe, 'check')==FALSE){
								$datanya = array('val_session' => $print,
												 'val_url' => $safe
												);
								$this->save_pict_session($datanya);
							}							
							$result[] = '<a href="http://'.$_SERVER['HTTP_HOST']."/page/read_/".$print.'" target="_blank">';
							//$result[] = '<a href='.$source_.' target="_blank">';
						}else{
							if(in_array('kcfinder', $cek)){ //----cek if url from 253 or 254 kc finder
						
								if(isset($source[0][1])):
									$src[] = '<a href='.$source_.' style='.$source[0][1].'>';
								else:
									$src[] = '<a href='.$source_.'>';
								endif;
									
								//$src[] = '<a href='.$source_.'>';
								
								$print = base64_encode(md5(urlencode($file_name)));
								$safe = $file_name;
								
								if($this->check_pict_session($print, $safe, 'check')==FALSE){
									$datanya = array('val_session' => $print,
													 'val_url' => $safe
													);
									$this->save_pict_session($datanya);
								}
								
								$result[] = '<a href="http://'.$_SERVER['HTTP_HOST']."/page/read_/".$print.'" target="_blank">';
								//$result[] = '<a href='.$source_.' target="_blank">';
							}
						}
					}
				}
			}
			
			if (strpos($s,'img') !== false) {
			
				$imgsrcx = explode('src', $s);
				preg_match_all('/".*?"|\'.*?\'/', $imgsrcx[1], $sourcex);
				
							
				if (strpos($s,'style=') !== false) {
					$s = str_replace('style=','',$s);
					$s = str_replace($sourcex[0][1],'',$s);		
				}else{
					$s = $s;
				}
				
				$imgsrc = explode('src', $s);
				preg_match_all('/".*?"|\'.*?\'/', $imgsrc[1], $source);
				
								
			//	 echo substr(str_replace('%20', ' ', $source[0][0]), 1, -1)."<br><br>";
				$file_tmp = substr(str_replace('%20', ' ', $source[0][0]), 1, -1);
				//echo $file_tmp;
				$src[] = $file_tmp;
				$cek = explode('/', $source[0][0]);
				
				
			
				if(in_array('filkom.ub.ac.id', $cek)){ //----cek if url from filkom.ub.ac.id
					$file_name = str_replace('http://filkom.ub.ac.id', $url_old, $file_tmp);
				}else{				
				
					if(in_array('175.45.187.242', $cek)){ //----cek if url from filkom.ub.ac.id
						$file_name = str_replace('http://filkom.ub.ac.id', $url_old, $file_tmp);
					}else{
					
						if(in_array('175.45.187.251', $cek)){
							//$file_name = $file_tmp;
							$file_name = str_replace('http://175.45.187.251', 'http://file.filkom.ub.ac.id', $file_tmp);
						}else{
							if(in_array('adl.ptiik.ub.ac.id', $cek)){
								$file_name = str_replace('http://adl.ptiik.ub.ac.id', 'http://file.filkom.ub.ac.id', $file_tmp);
							}else{
								//$file_name = $file_tmp;
								$file_name = str_replace('http://175.45.187.244', 'http://file.filkom.ub.ac.id', $file_tmp);
								if(in_array('file.filkom.ub.ac.id', $cek)){
									$file_name = $file_tmp;
								}else{
									$file_name = $url."/".$file_tmp;
								}
							}
						}
					}
				}
				//echo $file_tmp;
				$result[] = $file_name;
				
			}
			
			if (strpos($s,'[caption') !== false) {
				// echo $s."<br><br>";
				$strcontent = str_replace($s, '', $strcontent);
			}
			
			$strcontent = str_replace('[/caption]', '', $strcontent);
			
		};
				
		$strcontent = str_replace($src, $result, $strcontent);
		
		return $strcontent;
	}
	
	function check_pict_session($print=NULL, $safe=NULL, $param=NULL){
		$sql = "SELECT * 
				FROM db_ptiik_coms.coms_conf
				WHERE 1
			   ";
		
		if($print){
			$sql .= " AND coms_conf.val_session = '".$print."' ";
		}
		
		if($safe){
			$sql .= " AND coms_conf.val_url = '".$safe."' ";
		}
		
		$result = $this->db->query( $sql );
		
		if($param=='check'){
			if($result){
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		elseif($param=='view') {
			return $result;
		}
		
		
	}
	
	function save_pict_session($datanya){
		return $this->db->replace('db_ptiik_coms`.`coms_conf',$datanya);
	}
	
	function cek_file($val_session=NULL){
		$sql = "SELECT * 
			FROM db_ptiik_coms.coms_conf
			WHERE 1 
		   ";
			if($val_session){
				$sql .= " AND coms_conf.val_session = '".$val_session."' ";
			}
		//echo $sql;
		return $this->db->getRow($sql);
	}


}