<?php
class model_services extends model {
	
	public function __construct() {
		parent::__construct();	
	}
	/* API */
	function get_staff($status=NULL){
		$sql="SELECT DISTINCT
				mid(md5(db_ptiik_apps.tbl_pengampu.karyawan_id),11,6) AS karyawan_id,
				db_ptiik_apps.tbl_karyawan.nama,
				db_ptiik_apps.tbl_karyawan.gelar_awal,
				db_ptiik_apps.tbl_karyawan.gelar_akhir,
				db_ptiik_apps.tbl_karyawan.email
				FROM
				db_ptiik_apps.tbl_pengampu
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_pengampu.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
				INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				WHERE tbl_tahunakademik.is_aktif='1' ";
		return $this->db->query( $sql );
	}
	
	function get_mk_aktif($dosen=NULL, $isaktif=NULL){
		$sql= "SELECT
					mid(md5(db_ptiik_apps.tbl_pengampu.karyawan_id),11,6) AS karyawan_id,
					db_ptiik_apps.tbl_matakuliah.kode_mk,
					db_ptiik_apps.tbl_namamk.keterangan AS nama_mk,
					mid(md5(db_ptiik_apps.tbl_matakuliah.matakuliah_id),11,6) AS mk_id,
					db_ptiik_apps.tbl_karyawan.nama
					FROM
					db_ptiik_apps.tbl_pengampu
					INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON db_ptiik_apps.tbl_pengampu.mkditawarkan_id = db_ptiik_apps.tbl_mkditawarkan.mkditawarkan_id
					INNER JOIN db_ptiik_apps.tbl_matakuliah ON db_ptiik_apps.tbl_mkditawarkan.matakuliah_id = db_ptiik_apps.tbl_matakuliah.matakuliah_id
					INNER JOIN db_ptiik_apps.tbl_namamk ON db_ptiik_apps.tbl_matakuliah.namamk_id = db_ptiik_apps.tbl_namamk.namamk_id
					INNER JOIN db_ptiik_apps.tbl_tahunakademik ON db_ptiik_apps.tbl_mkditawarkan.tahun_akademik = db_ptiik_apps.tbl_tahunakademik.tahun_akademik
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_pengampu.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
					WHERE 1 
				";
		if($dosen) $sql.=" AND mid(md5(db_ptiik_apps.tbl_pengampu.karyawan_id),11,6)='$dosen' ";
		if($isaktif) $sql.=" AND tbl_tahunakademik.is_aktif='$isaktif' ";
		return $this->db->query( $sql );
	}
	/*--- */
	
	function read(){
		
	}
	
	function get_jabatan($nik=NULL){
		$sql = "SELECT 
					tbl_karyawan.nik,
					tbl_karyawan.karyawan_id,
					tbl_karyawan.nama,
					tbl_master_jabatan.keterangan,
					tbl_unit_kerja.unit_id,
					tbl_unit_kerja.keterangan nama_unit
				FROM db_ptiik_apps.tbl_karyawan
				INNER JOIN db_ptiik_apps.tbl_karyawan_kenaikan 
					ON tbl_karyawan.karyawan_id = tbl_karyawan_kenaikan.karyawan_id
					AND tbl_karyawan_kenaikan.is_aktif = '1' AND tbl_karyawan_kenaikan.jenis = 'jabatan'
				INNER JOIN db_ptiik_apps.tbl_master_jabatan 
					ON tbl_master_jabatan.jabatan_id = tbl_karyawan_kenaikan.jabatan_id
				INNER JOIN db_ptiik_apps.tbl_unit_kerja 
					ON tbl_unit_kerja.unit_id = tbl_karyawan_kenaikan.unit_id
				WHERE tbl_karyawan.karyawan_id = '$nik'";
		
		return $this->db->getRow($sql);
	}
	
	public function get_next_id($nama_table=NULL, $primary_key=NULL){
		$sql="SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(".$primary_key.",4) AS 
			unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
			FROM db_ptiik_skripsi.".$nama_table; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	
	
	function get_mhs($nim=NULL){
		$sql = "SELECT mid(md5(`tbl_mahasiswa`.`mahasiswa_id`),6,6) as mhs_id,
					   tbl_mahasiswa.*,
					   `tbl_prodi`.keterangan as prodi, 
					   tbl_fakultas.keterangan as fakultas
				FROM `db_ptiik_apps`.`tbl_mahasiswa`
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi`
					ON tbl_prodi.prodi_id = tbl_mahasiswa.prodi_id
				LEFT JOIN `db_ptiik_apps`.`tbl_fakultas`
					ON tbl_prodi.fakultas_id = tbl_fakultas.fakultas_id WHERE  tbl_mahasiswa.is_aktif='aktif' ";
		if($nim!=""){
			$sql=$sql . " AND  `tbl_mahasiswa`.`mahasiswa_id`='".$nim."' ";
			// echo $sql; 
			return $this->db->getRow( $sql );
		}
		$sql = $sql . " ORDER BY `tbl_mahasiswa`.`mahasiswa_id` ASC";

		return $this->db->query( $sql );
	}
	
	function daftar_id(){
		$sql = "SELECT concat('".date("Ym")."',RIGHT(concat( '0000' , CAST(IFNULL(MAX(CAST(right(daftar_id,4) AS 
				unsigned)), 0) + 1 AS unsigned)),4)) as `data` 
				FROM `db_ptiik_apps`.`tbl_daftar_ulang`"; 
		$dt = $this->db->getRow( $sql );
		$strresult = $dt->data;
		return $strresult;
	}
	
	function read_daftar_ulang($thn_akademik=NULL, $mhs=NULL,$fakultas_id=NULL,$cabang=NULL,$angkatan=NULL,$validasi=NULL){
		$sql = "SELECT tbl_daftar_ulang.*,
						tbl_prodi.keterangan as prodi,
						tbl_mahasiswa.nim,
						mid(md5(`tbl_mahasiswa`.`mahasiswa_id`),6,6) as mhs_id,
						tbl_mahasiswa.is_aktif,
					    tbl_mahasiswa.foto
				FROM `db_ptiik_apps`.`tbl_daftar_ulang`
				LEFT JOIN `db_ptiik_apps`.`tbl_mahasiswa` 
					ON `db_ptiik_apps`.`tbl_daftar_ulang`.mahasiswa_id = `db_ptiik_apps`.`tbl_mahasiswa`.mahasiswa_id
				LEFT JOIN `db_ptiik_apps`.`tbl_prodi` 
					ON `db_ptiik_apps`.`tbl_prodi`.prodi_id = `db_ptiik_apps`.`tbl_mahasiswa`.prodi_id
				WHERE 1";
		if($validasi!=""){
			$sql=$sql . " AND tbl_daftar_ulang.is_valid = '".$validasi."'";
		}
		if($fakultas_id){
			$sql=$sql . " AND `db_ptiik_apps`.`tbl_prodi`.fakultas_id = '".$fakultas_id."'
						  AND `db_ptiik_apps`.`tbl_mahasiswa`.cabang_id = '".$cabang."'";
		}
		if($angkatan!="*"){
			$sql .= " AND `db_ptiik_apps`.`tbl_mahasiswa`.angkatan = '".$angkatan."'";
		}
		if($thn_akademik){
			$sql=$sql . " AND tbl_daftar_ulang.tahun_akademik = '".$thn_akademik."'";
		}
		if($mhs!=""){
			$sql=$sql . " AND (`tbl_daftar_ulang`.`mahasiswa_id`='".$mhs."' OR mid(md5(`tbl_daftar_ulang`.`mahasiswa_id`),6,6)='".$mhs."') ";
			// echo $sql;
			return $this->db->getRow( $sql );
		}
		// echo $sql;
		$sql = $sql . " ORDER BY `tbl_daftar_ulang`.`daftar_id` DESC";
		return $this->db->query( $sql );
	}
	
	function replace_daftarulang($data=NULL){
		return $this->db->replace('db_ptiik_apps`.`tbl_daftar_ulang', $data);
	}
	
	function validate_daftarulang($daftar=NULL, $mhs=NULL, $data=NULL, $userid=NULL){
		//return $data->daftar_id;
		$last_update	= date("Y-m-d H:i:s");
		
		$sql	= "UPDATE db_ptiik_apps.tbl_daftar_ulang
				   SET is_valid = '1', user_id = '".$userid."', last_update = '".$last_update."'
				   WHERE daftar_id = '".$daftar."'";
		$result = $this->db->query( $sql );
		if($result){
			$sql2	= "UPDATE db_ptiik_apps.tbl_mahasiswa
					   SET nama = '".$data->nama."',
					       tmp_lahir = '".$data->tmp_lahir."',
					       tgl_lahir = '".$data->tgl_lahir."',
					       alamat = '".$data->alamat_malang."',
					       email = '".$data->email."',
					       hp = '".$data->hp."',
					       telp = '".$data->telp."',
					       jalur_seleksi = '".$data->jalur_masuk."',
					       nama_ortu = '".$data->orang_tua."',
					       alamat_ortu = '".$data->alamat_ortu."',
					       alamat_surat = '".$data->alamat_surat."',
					       catatan = '".$data->catatan."',
					       telp_ortu = '".$data->telp_ortu."',
					       hp_ortu = '".$data->hp_ortu."',
					       email_ortu = '".$data->email_ortu."',
					   	   user_id = '".$userid."', 
					   	   last_update = '".$last_update."'
					   WHERE mahasiswa_id = '".$mhs."'";
		    return $this->db->query( $sql2 );
		}
		else return false;
	}
}
?>