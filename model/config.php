<?php defined('PANADA') or die('Can\'t access directly!');
/**
 * EN: Panada Lite installation configuration
 */

$CONFIG['assets_folder'] = 'assets';

/**
 * Environment and error reporting
 * valid values:
 *    development, production
 */
$CONFIG['environment'] = 'development';

/**
 * Set this value to empty string instead of 'index.php' 
 * if you wish an url without "index.php" .
 * but don't forget to configure the .htaccess file if you change this
 */ 
$CONFIG['index_file']                       = ''; 

/**
 * EN: Database configuration.
 */
 
$CONFIG['db']['default']['driver']          = 'mysql';
$CONFIG['db']['default']['host']            = '175.45.187.253'; 
$CONFIG['db']['default']['user']            = 'webdevel'; 
$CONFIG['db']['default']['password']        = 'rahasiagan123'; 
$CONFIG['db']['default']['database']        = 'db_coms';
$CONFIG['db']['default']['charset']         = 'utf8';
$CONFIG['db']['default']['collate']         = 'utf8_general_ci';
$CONFIG['db']['default']['persistent']      = false;

$CONFIG['db']['master']['driver']          = 'mysql';
$CONFIG['db']['master']['host']            = '175.45.187.253'; 
$CONFIG['db']['master']['user']            = 'beta'; 
$CONFIG['db']['master']['password']        = 'b3tha'; 
$CONFIG['db']['master']['database']        = 'db_ptiik_apps';
$CONFIG['db']['master']['charset']         = 'utf8';
$CONFIG['db']['master']['collate']         = 'utf8_general_ci';
$CONFIG['db']['master']['persistent']      = false;

/**
 * Session configuration.
 */
$CONFIG['session']['expiration']        = 7200; /* 2 hour. */
$CONFIG['session']['name']              = 'PAN_SID';
$CONFIG['session']['cookie_expire']     = 0;
$CONFIG['session']['cookie_path']       = '/';
$CONFIG['session']['cookie_secure']     = false;
$CONFIG['session']['cookie_domain']     = '';
$CONFIG['session']['driver']            = 'native';
$CONFIG['session']['driver_connection'] = 'default'; /* Connection name for the driver. */
$CONFIG['session']['storage_name']      = 'sessions';

/**
 * regular expression in filtering method and controller names
 * comment out the following filter_regex configuration if you don't prefer filtering out 
 * controller and method name paresed from URI string
 */
$CONFIG['filter_regex'] = "/[^a-z0-9_]/i"; 

/** 
 * set the default controller and method if not set from the URL
 */
$CONFIG['default_controller'] = 'home';
$CONFIG['default_method'] = 'index';
/**
 * session secret_key
 */
$CONFIG['secret_key'] = "tH1s/is#4+seCREt/key''couLD bE\\anyThInG";

$CONFIG['page_title'] = "PTIIK APPS";

$CONFIG['file_url_view']   = 'http://175.45.187.253/beta/fileupload/assets';
$CONFIG['default_pic']   = 'http://175.45.187.253/beta/fileupload/assets/upload/user.jpg';
$CONFIG['default_thumb']   = 'http://175.45.187.253/beta/fileupload/assets/upload/thumb.gif';

$CONFIG['file_url']   = 'http://175.45.187.253/beta/fileupload/upload.php';
/*$CONFIG['file_url_view']   = 'http://175.45.187.250/fileupload/assets';
$CONFIG['default_pic']   = 'http://175.45.187.250/fileupload/assets/upload/user.jpg';

$CONFIG['file_url']   = 'http://175.45.187.250/fileupload/upload.php';*/

$CONFIG['safe_upload']  = true;

$CONFIG['assets_folder'] = APPLICATION . "assets";
$CONFIG['web_base_url'] = 'http://175.45.187.253/beta';

// if(!isset($_COOKIE['lang-switch'])){
	  	// setcookie('lang-switch','en',time()+3600*24,"/");
	// var_dump($_COOKIE['lang-switch']);
// }

		if(!isset($_COOKIE['lang-switch'])){
		  	$CONFIG['lang']="en";
	  }
	  else{
		  	$CONFIG['lang']=$_COOKIE['lang-switch'];
	  }	