<?php
class model_pemakaianruang extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_pemakaian_ruang($strtgl=NULL){
		$tgl = date("Y-m-d", $strtgl);
		$sql = "SELECT DISTINCT
					tbl_pemakaian_ruang_detail.jam_mulai,
					tbl_pemakaian_ruang_detail.jam_selesai,
					tbl_pemakaian_ruang.kegiatan,
					tbl_pemakaian_ruang_detail.ruang
				FROM db_ptiik_apps.tbl_pemakaian_ruang
				INNER JOIN db_ptiik_apps.tbl_pemakaian_ruang_detail 
					ON tbl_pemakaian_ruang.jadwal_id = tbl_pemakaian_ruang_detail.jadwal_id
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_pemakaian_ruang_detail.ruang = tbl_ruang.ruang_id
				WHERE '$tgl' BETWEEN tbl_pemakaian_ruang.tgl_mulai AND tbl_pemakaian_ruang.tgl_selesai
					AND tbl_pemakaian_ruang.is_valid = '1'";
		return $this->db->query($sql);
	}
	
	function get_ruang(){
		$sql = "SELECT 
					tbl_ruang.kode_ruang,
					tbl_ruang.keterangan,
					tbl_ruang.ruang_id,
					tbl_ruang.kategori_ruang,
					tbl_ruang.lokasi
				FROM db_ptiik_apps.tbl_ruang
				WHERE tbl_ruang.kategori_ruang IN ('kuliah','seminar','lab')
				ORDER BY tbl_ruang.kode_ruang";
		return $this->db->query($sql);
	}
	
	
}
?>