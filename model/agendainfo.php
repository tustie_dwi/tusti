<?php
class model_agendainfo extends model {
	 public $title;
     public $calendar;
     public $month;
     public $year;
     public $style;
     public $selectMonth;
     public $selectYear;
     public $nextMonth;
     public $previousMonth;
     public $yearNextMonth;
     public $yearPreviousMonth;
     public $nextMonthUrl;
     public $previousMonthUrl;
     public $storeNextLink;
     public $storePreviousLink;
     public $displayControls;
     public $startForm;
     public $closeForm;


	public function __construct() {
		parent::__construct();	
	}
	
	
	function get_detail_agenda($id=NULL, $tgl=NULL, $jeniskegiatan=NULL){
		$sql = "SELECT
					`pt11k_user`.`name`,
					mid(md5(`tbl_agenda`.agenda_id),9,7) as `id`,
					`tbl_agenda`.`agenda_id`,
					`tbl_agenda`.`lokasi`,
					`tbl_agenda`.`ruang`,
					`tbl_agenda`.`unit_id`,
					`tbl_agenda`.`inf_ruang`,
					`tbl_agenda`.`judul`,
					`tbl_agenda`.`inf_peserta`,
					`tbl_agenda`.`prodi_id`,
					`tbl_agenda`.`keterangan`,
					`tbl_agenda`.`inf_hari`,
					date_format(`tbl_agenda`.`tgl_mulai` ,'%Y-%m-%d %H:%i') as `tmulai`, 
					date_format(`tbl_agenda`.`tgl_selesai`,'%Y-%m-%d %H:%i') as `tselesai`, 
					`tbl_agenda`.`tgl_mulai`,
					`tbl_agenda`.`tgl_selesai`,
					 if(`db_ptiik_apps`.`tbl_agenda`.`prodi_id`='all', 'All Prodi', (SELECT keterangan FROM db_ptiik_apps.tbl_prodi WHERE prodi_id=db_ptiik_apps.tbl_agenda.prodi_id)) as `prodi`,
					`tbl_agenda`.`is_allday`,
					`tbl_agenda`.`is_publish`,
					`tbl_agenda`.`jenis_kegiatan_id`,
					`tbl_agenda`.penyelenggara,
					`tbl_agenda`.`user`,
					`tbl_agenda`.`last_update`,
					`db_ptiik_apps`.`tbl_unit_kerja`.`nama` as `unit_penyelenggara`,
					`tbl_jeniskegiatan`.`keterangan` as `jenis`
				FROM
					`db_ptiik_apps`.`tbl_agenda`
					left Join `db_ptiik_apps`.`tbl_jeniskegiatan` ON `db_ptiik_apps`.`tbl_agenda`.`jenis_kegiatan_id` = `db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`
					left Join `coms`.`pt11k_user` ON `db_ptiik_apps`.`tbl_agenda`.`user` = `coms`.`pt11k_user`.`username`
					Left Join `db_ptiik_apps`.`tbl_unit_kerja` ON `db_ptiik_apps`.`tbl_agenda`.`unit_id` = `db_ptiik_apps`.`tbl_unit_kerja`.`unit_id`
				WHERE `tbl_agenda`.`is_valid` = '1' 
				";
		if($id){
			$sql = $sql . " AND (mid(md5(`tbl_agenda`.agenda_id),9,7)= '".$id."' OR `db_ptiik_apps`.`tbl_agenda`.agenda_id = '".$id."') ";
		}
		
		if($tgl){
			$sql = $sql . " AND date_format(`tbl_detailagenda`.`tgl` ,'%Y-%m-%d') = '".$tgl."' ";
		}
		
		if($jeniskegiatan){
			$sql = $sql . " AND `tbl_agenda`.`jenis_kegiatan_id` = '".$jeniskegiatan."' ";
		}
		
		$sql = $sql. "ORDER BY date_format(`db_ptiik_apps`.`tbl_agenda`.`tgl_mulai` ,'%Y-%m-%d %H:%i') DESC";
		
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function get_kehadiran($id=NULL, $tgl=NULL, $civitas=NULL){
	
	}
	
	function get_agenda($id=NULL, $tgl=NULL, $jeniskegiatan=NULL){
		$sql = "SELECT DISTINCT
					`pt11k_user`.`name`,
					mid(md5(`tbl_agenda`.agenda_id),9,7) as `id`,
					`tbl_agenda`.`agenda_id`,
					`tbl_agenda`.`lokasi`,
					`tbl_agenda`.`ruang`,
					`tbl_agenda`.`unit_id`,
					`tbl_agenda`.`inf_ruang`,
					`tbl_agenda`.`judul`,
					`tbl_agenda`.`inf_peserta`,
					`tbl_agenda`.`prodi_id`,
					`tbl_agenda`.`keterangan`,
					`tbl_agenda`.`inf_hari`,
					date_format(`tbl_agenda`.`tgl_mulai`, '%a, %b %d %Y') as `tstart`,
					date_format(`tbl_agenda`.`tgl_selesai`, '%a, %b %d %Y') as `tfinish`,
					date_format(`tbl_agenda`.`tgl_mulai`, '%H:%i') as `from`,
					date_format(`tbl_agenda`.`tgl_selesai`, '%H:%i') as `to`,
					date_format(`tbl_agenda`.`tgl_mulai` ,'%Y-%m-%d %h:%i') as `tmulai`, 
					date_format(`tbl_agenda`.`tgl_selesai`,'%Y-%m-%d %h:%i') as `tselesai`, 
					`tbl_agenda`.`is_allday`,
					`tbl_agenda`.`is_publish`,
					`tbl_agenda`.`jenis_kegiatan_id`,
					`tbl_agenda`.penyelenggara,
					`tbl_agenda`.`user`,
					`tbl_agenda`.`last_update`,
					`tbl_jeniskegiatan`.`keterangan` as `jenis`,
					`db_ptiik_apps`.`tbl_unit_kerja`.`nama` as `unit_penyelenggara`
				FROM
				`db_ptiik_apps`.`tbl_agenda`
				Left Join `db_ptiik_apps`.`tbl_jeniskegiatan` ON `db_ptiik_apps`.`tbl_agenda`.`jenis_kegiatan_id` = `db_ptiik_apps`.`tbl_jeniskegiatan`.`jenis_kegiatan_id`
				Left Join `coms`.`pt11k_user` ON `db_ptiik_apps`.`tbl_agenda`.`user` = `coms`.`pt11k_user`.`username`
				Left Join `db_ptiik_apps`.`tbl_unit_kerja` ON `db_ptiik_apps`.`tbl_agenda`.`unit_id` = `db_ptiik_apps`.`tbl_unit_kerja`.`unit_id`
				Inner Join `db_ptiik_apps`.`tbl_detailagenda` ON `db_ptiik_apps`.`tbl_agenda`.`agenda_id` = `db_ptiik_apps`.`tbl_detailagenda`.`agenda_id`
				WHERE `tbl_agenda`.`is_valid` = '1' AND `tbl_agenda`.`is_private` = '0'  
				";
		if($id){
			$sql = $sql . "AND `db_ptiik_apps`.`tbl_agenda`.agenda_id = '".$id."' ";
		}
		
		if($tgl){
			$sql = $sql . "AND date_format(`tbl_detailagenda`.`tgl` ,'%Y-%m-%d') = '".$tgl."' ";
		}
		
		if($jeniskegiatan){
			$sql = $sql . "AND `tbl_agenda`.`jenis_kegiatan_id` = '".$jeniskegiatan."' ";
		}
		
		$sql = $sql. "ORDER BY date_format(`db_ptiik_apps`.`tbl_agenda`.`tgl_mulai` ,'%Y-%m-%d %H:%i') DESC";
		//echo $sql;
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	/*get peserta*/
	function get_peserta_agenda($id=NULL, $jenis=NULL, $status=NULL, $grup=NULL){
		$sql = "SELECT
				`tbl_peserta`.`peserta_id`,
				`tbl_peserta`.`karyawan_id`,
				`tbl_peserta`.`agenda_id`,
				`tbl_peserta`.`jenis_peserta`,
				`tbl_peserta`.`nama`,
				`tbl_peserta`.`instansi`,
				`tbl_peserta`.`status_peserta`,
				`tbl_peserta`.`is_valid`
				FROM
				 `db_ptiik_apps`.`tbl_peserta`
				WHERE 
					1= 1 AND (mid(md5(`tbl_peserta`.agenda_id),9,7)= '".$id."' OR `db_ptiik_apps`.`tbl_peserta`.agenda_id = '".$id."') AND  jenis_peserta='".$jenis."' 
				";
		if($status){
			$sql = $sql . "AND `tbl_peserta`.`status_peserta`='".$status."' ";
		}
		
		if($grup){
			$sql = $sql . "AND `tbl_peserta`.`group_by`='".$grup."' ";
		}
		
		$sql = $sql. "ORDER BY `tbl_peserta`.`nama` ASC";
		
		return $this->db->query( $sql );
	}
	
	function drawWeekCalendarLama($month, $year, $tgl, $style, $url=NULL){
		// set current date
		$in = strtotime(date('Y-m-d'));
		$date = $tgl;
		$month= date("m", strtotime($date));
		$nmonth= date("M", strtotime($date));
		$nyear= date("Y", strtotime($date));
		
		$ts = strtotime($date);
		
		$year = date('o', $ts);
		$week = date('W', $ts);		
				
		$str = '<table  cellspacing="0" class="table table-bordered table-calendar">';
      		
		$str.= '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head" width="5%">'.$nmonth. " ".$nyear.'</td>';
		for($i = 0; $i < 7; $i++) {
			
			$ts = strtotime($year.'W'.$week.$i);
			$tw = date("N", $ts);
			$td = date("D", $ts);
			$tn = date("Y-m-d", $ts);
			//print date("m/d/Y l", $ts) . "\n";
			if($tw==$i){
				$tp = $ts;				
			 }else{
				$tp = strtotime(date($tn, strtotime('last day')));
				
			 }
			$list_day = date("d", $tp);	
			$str.= '<td class="'. $style .'-day-head">'.$td.", ".$list_day.'</td>';
		}
		/* final row */
         $str.= '</tr></thead><tbody>';
		 
		 $blokwaktu	= $this->get_blok_waktu();
		 
		 foreach($blokwaktu as $dt):
			$str.= '<tr>';
				$str.= '<td><b>'.$dt->jam_mulai.'</b></td>';
					for($i = 0; $i < 7; $i++) {
						// timestamp from ISO week date format
						$ts = strtotime($year.'W'.$week.$i);
						$tw = date("N", $ts);
						$tn = date("Y-m-d", $ts);
						//print date("m/d/Y l", $ts) . "\n";
						if($tw==$i){
							$tp = $ts;
									
						 }else{
							$tp = strtotime(date($tn, strtotime('last day')));
							
						 }
						 
						 $list_day = date("d", $tp);
						 $mmonth = date("m", $tp);
						 
						if($list_day == date("j",mktime(0,0,0,$month))) {
							$tstyle = $style .'-current-day';
						}else{
							$tstyle = $style;
						}
								 
						$data = $this->getEventDetail($list_day, $mmonth, $year, $i, $dt->jam_mulai);
						$str.= '<td width="10%" class="'.$tstyle.'">';	 
						 if($data){
							$k=0;
							foreach($data as $row):
							 $k++;
							switch (strToLower($row->jenis)){
								case 'konseling':
									$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'rapat':
									$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliah tamu':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='fa fa-star'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='fa fa-signal'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'uts':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-info';									
								break;
								case 'uas':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-putih';
								break;
								case 'olahraga dan seni':
									$icon = "<i class='fa fa-flag'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'porseni';
									$tclass= 'text-putih';
								break;
								case 'lain-lain':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
									$tclass= 'text-putih';
								break;
								default:
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'info';
									$tclass= 'text-putih';
								break;
							}
								
															
								$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<a href="'.$url.'/agenda/'.$row->id.'"><small><span class="'.$tclass.'">'.$icon.$note.'</span></small></a></div>';
						 			
							endforeach;
								
							}else{
								$str.= "&nbsp;";
							}							
						 $str.= "</td>";
					}
			$str.= '</tr>';
		 endforeach;
		 
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
	}
	
	function drawWeekCalendar($month, $year, $tgl, $style, $url=NULL){
		// set current date
		$in = strtotime(date('Y-m-d'));
		$date = $tgl;
		$month= date("m", strtotime($date));
		$nmonth= date("M", strtotime($date));
		$nyear= date("Y", strtotime($date));
		
		$ts = strtotime($date);
		
		$year = date('o', $ts);
		$week = date('W', $ts);		
				
		$str = '<table  cellspacing="0" class="table table-bordered table-calendar">';
      		
		$str.= '<thead><tr class="'. $style .'-row">';
		for($i = 0; $i < 7; $i++) {
			
			$ts = strtotime($year.'W'.$week.$i);
			$tw = date("N", $ts);
			$td = date("D", $ts);
			$tn = date("Y-m-d", $ts);
			
			if($tw==$i){
				$tp = $ts;				
			 }else{
				$tp = strtotime(date($tn, strtotime('last day')));
				
			 }
			$list_day = date("d", $tp);	
			$str.= '<td class="'. $style .'-day-head">'.$td.", ".$list_day.'</td>';
		}
		/* final row */
         $str.= '</tr></thead><tbody>';
		 
		 $blokwaktu	= $this->get_blok_waktu();
		 
		// foreach($blokwaktu as $dt):
			$str.= '<tr>';
				//$str.= '<td><b>'.$dt->jam_mulai.'</b></td>';
					for($i = 0; $i < 7; $i++) {
						// timestamp from ISO week date format
						$ts = strtotime($year.'W'.$week.$i);
						$tw = date("N", $ts);
						$tn = date("Y-m-d", $ts);
						//print date("m/d/Y l", $ts) . "\n";
						if($tw==$i){
							$tp = $ts;
									
						 }else{
							$tp = strtotime(date($tn, strtotime('last day')));
							
						 }
						 
						 $list_day = date("d", $tp);
						 $mmonth = date("m", $tp);
						 
						if($list_day == date("j",mktime(0,0,0,$month))) {
							$tstyle = $style .'-current-day';
						}else{
							$tstyle = $style;
						}
								 
						$data = $this->getEventDetail($list_day, $mmonth, $year, $i);
						$str.= '<td width="10%" class="'.$tstyle.'">';	 
						 if($data){
							$k=0;
							foreach($data as $row):
							 $k++;
							switch (strToLower($row->jenis)){
								case 'konseling':
									$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'rapat':
									$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliah tamu':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='fa fa-star'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='fa fa-signal'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'uts':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-info';									
								break;
								case 'uas':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-putih';
								break;
								case 'olahraga dan seni':
									$icon = "<i class='fa fa-flag'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'porseni';
									$tclass= 'text-putih';
								break;
								case 'lain-lain':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
									$tclass= 'text-putih';
								break;
								default:
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'info';
									$tclass= 'text-putih';
								break;
							}
								
															
								$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<a href="'.$url.'/agenda/'.$row->id.'"><small><span class="'.$tclass.'"><i class="fa fa-clock-o"></i>'.$row->jam_mulai.', '.$note.'</span></small></a></div>';
						 			
							endforeach;
								
							}else{
								$str.= "&nbsp;";
							}							
						 $str.= "</td>";
					}
			$str.= '</tr>';
		 //endforeach;
		 
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
	}
	
	function drawWeekKehadiran($month, $year, $tgl, $uid,$style, $url=NULL){
		// set current date
		$in = strtotime(date('Y-m-d'));
		$date = $tgl;
		$month= date("m", strtotime($date));
		$nmonth= date("M", strtotime($date));
		$nyear= date("Y", strtotime($date));
		
		$ts = strtotime($date);
		
		$year = date('o', $ts);
		$week = date('W', $ts);		
				
		$str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
      		
		$str.= '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head" width="5%">'.$nmonth. " ".$nyear.'</td>';
		for($i = 0; $i < 7; $i++) {
			
			$ts = strtotime($year.'W'.$week.$i);
			$tw = date("N", $ts);
			$td = date("D", $ts);
			$tn = date("Y-m-d", $ts);
			//print date("m/d/Y l", $ts) . "\n";
			if($tw==$i){
				$tp = $ts;				
			 }else{
				$tp = strtotime(date($tn, strtotime('last day')));
				
			 }
			$list_day = date("d", $tp);	
			$str.= '<td class="'. $style .'-day-head">'.$td.", ".$list_day.'</td>';
		}
		/* final row */
         $str.= '</tr></thead><tbody>';
		 
		 $blokwaktu	= $this->get_blok_waktu();
		 
		 foreach($blokwaktu as $dt):
			$str.= '<tr>';
				$str.= '<td><b>'.$dt->jam_mulai.'</b></td>';
					for($i = 0; $i < 7; $i++) {
						// timestamp from ISO week date format
						$ts = strtotime($year.'W'.$week.$i);
						$tw = date("N", $ts);
						$tn = date("Y-m-d", $ts);
						//print date("m/d/Y l", $ts) . "\n";
						if($tw==$i){
							$tp = $ts;
									
						 }else{
							$tp = strtotime(date($tn, strtotime('last day')));
							
						 }
						 
						 $list_day = date("d", $tp);
						 $mmonth = date("m", $tp);
						 
						if($list_day == date("j",mktime(0,0,0,$month))) {
							$tstyle = $style .'-current-day';
						}else{
							$tstyle = $style;
						}
								 
						$data = $this->getEventDetailCivitas($list_day, $mmonth, $year, $i, $dt->jam_mulai, $uid);
						
						$kuliah = $this->getPerkuliahan($list_day, $mmonth, $year, $i, $dt->jam_mulai, $uid);
						
						$ujian = $this->getPengawasByWeek($list_day, $mmonth, $year, $i, $dt->jam_mulai, $uid);
						
						$str.= '<td width="10%" class="'.$tstyle.'">';	 
						 if($data){
							$k=0;
							foreach($data as $row):
							 $k++;
								switch (strToLower($row->jenis)){
								case 'rapat':
									$icon = "<i class='icon-check icon-white'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'konseling':
									$icon = "<i class='icon-share icon-white'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliah tamu':
									$icon = "<i class='icon-edit icon-white'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='icon-star icon-white'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='icon-share icon-white'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='icon-signal icon-white'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='icon-star'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='icon-star-empty icon-white'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa  fa-clock-o icon-white'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='icon-ok-circle icon-white'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'uts':
									$icon = "<i class='icon-ok-circle'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'success';	
									$tclass= 'text-info';									
								break;
								case 'uas':
									$icon = "<i class='icon-ok-circle'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'success';	
									$tclass= 'text-info';
								break;
								default:
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'info';
									$tclass= 'text-info';
								break;
							}
								
															
								$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
										<a href="'.$url.'/agenda/'.$row->id.'"><small><span class="'.$tclass.'">'.$icon.$note.'</span></small></a></div>';						 							
								
							endforeach;
								
							}else{
								$str.= "&nbsp;";
							}
							
							if($uid){
								if($ujian){
									foreach($ujian as $row):										
										$str.= '<div class="alert-agenda alert-agenda-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
											<a href="#"><small><span class="text-info"><i class="fa  fa-clock-o"></i>&nbsp;Pengawas '.strToUpper($row->kegiatan)." - ".$row->ruang.'</span></small></a></div>';										
									endforeach;
								}
								
								if($kuliah){
									foreach($kuliah as $row):
										if($row->namamk){
											$str.= '<div class="alert-agenda alert-agenda-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
											<small><a href="#"><small><span class="text-info"><i class="fa  fa-clock-o"></i>&nbsp;'.$row->namamk." - ".$row->catatan.'</span></small></a></small></div>';
										}else{
										
										$str.= '<div class="alert-agenda alert-agenda-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
											<small><a href="#"><small><span class="text-info"><i class="fa  fa-clock-o"></i>&nbsp;'.$row->kegiatan." - ".$row->catatan.'</span></small></a></small></div>';
											}
									endforeach;
								}
							
							}
							
						 $str.= "</td>";
					}
			$str.= '</tr>';
		 endforeach;
		 
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
	}
	
	function draw_month_hadir($month, $year, $uid, $style, $url=NULL) {		
		 if(($month == NULL) || ($year == NULL))
         {
             // Month in numbers with the leading 0
             $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
         }else{
			 $title = date('F Y',mktime(0,0,0,$month,1,$year));
		 }

		/* We need to take the month value and turn it into one without a leading 0 */
         if((substr($month, 0, 1)) == 0)
         {
             // if value is between 01 - 09, drop the 0
             $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         /* draw table */
		
         $str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
        
         /* table headings */
         $headings = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
         $str.= '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         /* days and weeks vars now ... */
         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         /* row for week one */
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         /* print "blank" days until the first of the current week */
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
            
                 /* add in the day number */
                 $str.= '<div class="'. $style .'-day-number">'.$list_day.'</div>';
				 
					$data = $this->get_kegiatan($list_day, $month, $year, $running_day, "",$uid);
						
						/*$kuliah = $this->getPerkuliahan($list_day, $mmonth, $year, $i, $dt->jam_mulai, $uid);
						
						$ujian = $this->getPengawasByWeek($list_day, $mmonth, $year, $i, $dt->jam_mulai, $uid);*/
						
						
						 if($data){
							$k=0;
							foreach($data as $row):
							 $k++;
								switch (strToLower($row->jenis)){
								case 'konseling':
									$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'rapat':
									$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliahtamu':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='fa fa-star'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='fa fa-signal'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'porseni':
									$icon = "<i class='fa fa-flag'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'porseni';
									$tclass= 'text-putih';
								break;
								
								case 'uts':
									if($row->keterangan=='Pengawas'){
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}else{
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}								
								break;
								case 'uas':
									if($row->keterangan=='Pengawas'){
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}else{
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}								
								break;
								
								case 'kuliah':
									$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
									$note = $row->kegiatan." ".$row->namamk;	
									$sclass= 'success';	
									$tclass= 'text-putih';
								break;
								default:
									$icon = "<i class='fa  fa-clock-o'></i>".$row->jam_mulai;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'info';
									$tclass= 'text-putih';
								break;
							}
								
															
								$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
										<a href="#"><small><span class="'.$tclass.'"><i class="fa  fa-clock-o"></i> '.$row->jam_mulai.", R.".$row->ruang.", ".$note.'</span></small></a></div>';						 							
								
							endforeach;
								
							}else{
								$str.= "&nbsp;";
							}							
				 
				 
                
             $str.= '</td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
    
         /* finish the rest of the days in the week */
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
    
         /* final row */
         $str.= '</tr>';
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
     }

	
	function draw_week_hadir($month, $year, $tgl, $uid,$style, $url=NULL){
		// set current date
		$in = strtotime(date('Y-m-d'));
		$date = $tgl;
		$month= date("m", strtotime($date));
		$nmonth= date("M", strtotime($date));
		$nyear= date("Y", strtotime($date));
		
		$ts = strtotime($date);
		
		$year = date('o', $ts);
		$week = date('W', $ts);		
				
		$str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
      		
		$str.= '<thead><tr class="'. $style .'-row">';
		for($i = 0; $i < 7; $i++) {
			
			$ts = strtotime($year.'W'.$week.$i);
			$tw = date("N", $ts);
			$td = date("D", $ts);
			$tn = date("Y-m-d", $ts);
			//print date("m/d/Y l", $ts) . "\n";
			if($tw==$i){
				$tp = $ts;				
			 }else{
				$tp = strtotime(date($tn, strtotime('last day')));
				
			 }
			$list_day = date("d", $tp);	
			$str.= '<td class="'. $style .'-day-head">'.$td.", ".$list_day.'</td>';
		}
		/* final row */
         $str.= '</tr></thead><tbody>';
		 
		 $blokwaktu	= $this->get_blok_waktu();
		 
		// foreach($blokwaktu as $dt):
			$str.= '<tr>';
				
					for($i = 0; $i < 7; $i++) {
						
						$ts = strtotime($year.'W'.$week.$i);
						$tw = date("N", $ts);
						$tn = date("Y-m-d", $ts);
						
						if($tw==$i){
							$tp = $ts;
									
						 }else{
							$tp = strtotime(date($tn, strtotime('last day')));
							
						 }
						 
						 $list_day = date("d", $tp);
						 $mmonth = date("m", $tp);
						 
						if($list_day == date("j",mktime(0,0,0,$month))) {
							$tstyle = $style .'-current-day';
						}else{
							$tstyle = $style;
						}
								 
						$data = $this->get_kegiatan($list_day, $mmonth, $year, $i, "", $uid);
						
						/*$kuliah = $this->getPerkuliahan($list_day, $mmonth, $year, $i, $dt->jam_mulai, $uid);
						
						$ujian = $this->getPengawasByWeek($list_day, $mmonth, $year, $i, $dt->jam_mulai, $uid);*/
						
						$str.= '<td width="10%" class="'.$tstyle.'">';	 
						 if($data){
							$k=0;
							foreach($data as $row):
							 $k++;
								switch (strToLower($row->jenis)){
								case 'konseling':
									$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'rapat':
									$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliahtamu':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='fa fa-star'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='fa fa-signal'></i>&nbsp;";
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'uts':
									if($row->keterangan=='Pengawas'){
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}else{
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}								
								break;
								case 'uas':
									if($row->keterangan=='Pengawas'){
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = "Pengawas ". strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}else{
										$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
										$note = strToUpper($row->kegiatan)." ".$row->namamk;	
										$sclass= 'warning';	
										$tclass= 'text-info';
									}								
								break;
								case 'kuliah':
									$icon = "<i class='fa  fa-clock-o'></i>&nbsp;";
									$note = $row->kegiatan." ".$row->namamk;	
									$sclass= 'success';	
									$tclass= 'text-putih';
								break;
								default:
									$icon = "<i class='fa  fa-clock-o'></i>".$row->jam_mulai;
									$note = $this->potong_kalimat($row->kegiatan,5);
									$sclass= 'info';
									$tclass= 'text-putih';
								break;
							}
								
															
								$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
										<a href="#"><small><span class="'.$tclass.'"><i class="fa  fa-clock-o"></i> '.$row->jam_mulai.", R.".$row->ruang.", ".$note.'</span></small></a></div>';						 							
								
							endforeach;
								
							}else{
								$str.= "&nbsp;";
							}							
							
							
						 $str.= "</td>";
					}
			$str.= '</tr>';
		 //endforeach;
		 
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
	}
	
	/* draw calendar */
	function drawCalendar($month, $year, $style, $url=NULL) {		
		 if(($month == NULL) || ($year == NULL))
         {
             // Month in numbers with the leading 0
             $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
         }else{
			 $title = date('F Y',mktime(0,0,0,$month,1,$year));
		 }

		/* We need to take the month value and turn it into one without a leading 0 */
         if((substr($month, 0, 1)) == 0)
         {
             // if value is between 01 - 09, drop the 0
             $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         /* draw table */
		
         $str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
        
         /* table headings */
         $headings = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
         $str.= '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         /* days and weeks vars now ... */
         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         /* row for week one */
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         /* print "blank" days until the first of the current week */
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
            
                 /* add in the day number */
                 $str.= '<div class="'. $style .'-day-number">'.$list_day.'</div>';
				 
				 $data = $this->getEvent($list_day, $month, $year, $running_day);
				 
				 if(count($data)>0){
					$i=0;
					 foreach($data as $row):
						$i++;
						switch (strToLower($row->jenis)){
								case 'konseling':
									$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'rapat':
									$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliah tamu':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='fa fa-star'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='fa fa-signal'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'uts':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-info';									
								break;
								case 'uas':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'info';	
									$tclass= 'text-putih';
								break;
								case 'lain-lain':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
									$tclass= 'text-putih';
								break;
								case 'olahraga dan seni':
									$icon = "<i class='fa fa-flag'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'porseni';
									$tclass= 'text-putih';
								break;
								default:
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'info';
									$tclass= 'text-putih';
								break;
							}
							
							
							
							$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<a href="'.$url.'/agenda/'.$row->id.'"><small><span class="'.$tclass.'"><i class="fa fa-clock-o"></i>'.$row->jam_mulai.', '.$note.'</span></small></a></div>';						 							
							
					 endforeach;
				 }
                
             $str.= '</td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
    
         /* finish the rest of the days in the week */
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
    
         /* final row */
         $str.= '</tr></tbody>';
    
         /* end the table */
         $str.= '</table>';
		 
		 return $str;
     }

	 
	
	/* draw calendar */
	function drawCalendarKehadiran($month, $year, $uid, $style, $url=NULL) {		
		 if(($month == NULL) || ($year == NULL))
         {
             // Month in numbers with the leading 0
             $month = date("m");    
             $year  = date("Y");    
			 $title = date("F Y");
         }else{
			 $title = date('F Y',mktime(0,0,0,$month,1,$year));
		 }

		/* We need to take the month value and turn it into one without a leading 0 */
         if((substr($month, 0, 1)) == 0)
         {
             // if value is between 01 - 09, drop the 0
             $tempMonth = substr($month, 1);                                                                                              
             $month = $tempMonth;
         }
        
         /* draw table */
		
         $str = '<table cellpadding="0" cellspacing="0" class="table table-bordered table-calendar">';
        
         /* table headings */
         $headings = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
         $str.= '<thead><tr class="'. $style .'-row"><td class="'. $style .'-day-head">'
             .implode('</td><td class="'. $style .'-day-head">',$headings).'</td></tr></thead>';

         /* days and weeks vars now ... */
         $running_day = date('w',mktime(0,0,0,$month,1,$year));
         $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
         $days_in_this_week = 1;
         $day_counter = 0;
         $dates_array = array();
    
         /* row for week one */
         $str.= '<tbody><tr class="'. $style .'-row">';
    
         /* print "blank" days until the first of the current week */
         for($x = 0; $x < $running_day; $x++):
             $str.= '<td class="'. $style .'-day-np"> </td>';
             $days_in_this_week++;
         endfor;
    
         /* keep going with days.... */
		 
		 $list_day=0;
		 $skip = false;
					
         for($list_day = 1; $list_day <= $days_in_month; $list_day++):
             if($list_day == date("j",mktime(0,0,0,$month)))
             {    
                 $str.= '<td class="'. $style .'-current-day">';
				 $txtstyle = $style .'-current-day';
             }
             else            
             {    
                 if(($running_day == "0") || ($running_day == "6"))
                 {
                     $str.= '<td class="'. $style .'-weekend-day">';
					  $txtstyle = $style .'-weekend-day';
                 }
                 else
                 {
                     $str.= '<td class="'. $style .'-day">';  
					 $txtstyle = $style ;	
                 }
             }
            
                 /* add in the day number */
                 $str.= '<div class="'. $style .'-day-number">'.$list_day.'</div>';
				 
				 $data = $this->getEventCivitas($list_day, $month, $year, $running_day, $uid);
				 
				 $kuliah = $this->getPerkuliahanByBulan($list_day, $month, $year, $running_day, $uid);
				 
				 $ujian = $this->getPengawasByMonth($list_day, $month, $year, $running_day, $uid);
				 
				 if($data){
					$i=0;
					 foreach($data as $row):
						$i++;
						switch (strToLower($row->jenis)){
								case 'konseling':
									$icon = "<i class='fa fa-stack-exchange'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'konseling';
									$tclass= 'text-putih';
								break;
								
								case 'rapat':
									$icon = "<i class='fa fa-puzzle-piece'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rapat';
									$tclass= 'text-putih';
								break;
								
								case 'kemahasiswaan':
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'mhs';
									$tclass= 'text-putih';
								break;
								
								case 'kuliah tamu':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kuliahtamu';
									$tclass= 'text-putih';
								break;
								
								case 'wisuda':
									$icon = "<i class='fa fa-star'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'wisuda';
									$tclass= 'text-putih';
								break;
								
								case 'kunjungan':
									$icon = "<i class='fa fa-dot-circle-o'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'kunjungan';
									$tclass= 'text-putih';
								break;
								
								case 'rekrutmen':
									$icon = "<i class='fa fa-signal'></i>&nbsp;";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'rekrutmen';
									$tclass= 'text-putih';
								break;
								
								case 'penelitian':
									$icon = "<i class='fa fa-star-half-empty'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'success';
								break;
								
								case 'pelatihan':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'pelatihan';
									$tclass= 'text-putih';
								break;
								
								case 'workshop':
									$icon = "<i class='fa fa-info-circle'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'workshop';
									$tclass= 'text-putih';
								break;
								case 'seminar':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									//$note = $row->jenis;
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'seminar';
									$tclass= 'text-putih';
								break;
								case 'uts':
									$icon = "<i class='fa fa-arrow-circle-o-right'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'success';	
									$tclass= 'text-info';									
								break;
								case 'uas':
									$icon = "<i class='fa fa-check-circle-o'></i>&nbsp;";
									$note = $row->jenis;	
									$sclass= 'success';	
									$tclass= 'text-info';
								break;
								default:
									$icon = "";
									$note = $this->potong_kalimat($row->judul,5);
									$sclass= 'info';
									$tclass= 'text-info';
								break;
							}
							
							
							
							$str.= '<div class="alert-agenda alert-agenda-'.$sclass.'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<a href="'.$url.'/agenda/'.$row->id.'"><small><span class="'.$tclass.'">'.$icon.$note.'</span></small></a></div>';						 							
							
					 endforeach;
				 }
				 
				 if($uid){
					if($ujian){
						foreach($ujian as $row):										
							$str.= '<div class="alert-agenda alert-agenda-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
								<a href="#"><small><span class="text-info"><i class="fa  fa-clock-o"></i>&nbsp;Pengawas '.strToUpper($row->kegiatan)." - ".$row->ruang.'</span></small></a></div>';										
						endforeach;
					}
					
					if($kuliah){
						if(count($kuliah) > 30){
							$str.= '<div class="alert-agenda alert-agenda-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
									<small><a href="#"><small><span class="text-info"><i class="fa  fa-clock-o"></i>&nbsp;'.count($kuliah).' Perkuliahan</span></small></a></small></div>';
						}else{
							foreach($kuliah as $row):
								if($row->namamk){
											$str.= '<div class="alert-agenda alert-agenda-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
											<small><a href="#"><small><span class="text-info"><i class="fa  fa-clock-o"></i>&nbsp;'.$row->namamk." - ".$row->catatan.'</span></small></a></small></div>';
										}else{
										
										$str.= '<div class="alert-agenda alert-agenda-info"><button type="button" class="close" data-dismiss="alert">&times;</button>
											<small><a href="#"><small><span class="text-info"><i class="fa  fa-clock-o"></i>&nbsp;'.$row->kegiatan." - ".$row->catatan.'</span></small></a></small></div>';
											}
							endforeach;
						}
					}
				
				}
							
				 
				 
                
             $str.= '</td>';
			
             if($running_day == 6):
                 $str.= '</tr>';
                 if(($day_counter+1) != $days_in_month):
                     $str.= '<tr class="'. $style .'-row">';
                 endif;
                 $running_day = -1;
                 $days_in_this_week = 0;
             endif;
             $days_in_this_week++; $running_day++; $day_counter++;
         endfor;
    
         /* finish the rest of the days in the week */
         if($days_in_this_week < 8) :
             for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                 $str.= '<td class="'. $style .'-day-np"> </td>';
             endfor;
         endif;
    
         /* final row */
         $str.= '</tr>';
    
         /* end the table */
         $str.= '</tbody></table>';
		 
		 return $str;
     }

	
	function getEvent($day,$month,$year, $runday){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$this->addNol($month)."-".$this->addNol(($day+1));
		
		$hari=$this->get_namahari($runday);
		
		$sql = "SELECT mid(md5(agenda_id),9,7) as `id`, agenda_id, judul, 
					keterangan, (SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_agenda.jenis_kegiatan_id) as `jenis`,
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%H:%i') as `jam_mulai`, 
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_selesai, '%H:%i') as `jam_selesai`,
					DATE_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%d') as `ds`, 
					DATE_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_selesai, '%d') as `de` 
				FROM `db_ptiik_apps`.`tbl_agenda` 
					WHERE `tbl_agenda`.`is_valid` = '1' AND `tbl_agenda`.`is_private` = '0' AND (((tgl_mulai like '".$tgl."%' OR  tgl_selesai like '".$tgl."%')  AND inf_hari like '%".$hari."%' )
						OR ((date_format(tgl_mulai,'%Y-%m-%d') < '".$tgl."' AND date_format(tgl_selesai,'%Y-%m-%d') > '".$tgl."')  AND inf_hari like '%".$hari."%' )) ORDER BY TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%H:%i') ASC";
		
		return $this->db->query( $sql );
	
	}
	
	function getEventCivitas($day,$month,$year, $runday, $uid=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$this->addNol($month)."-".$this->addNol(($day+1));
		
		$hari=$this->get_namahari($runday);
		
		$sql = "SELECT DISTINCT mid(md5(tbl_agenda.agenda_id),9,7) as `id`, tbl_agenda.agenda_id, tbl_agenda.judul, 
					tbl_agenda.keterangan, (SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_agenda.jenis_kegiatan_id) as `jenis`,
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_mulai,'%H:%i') as `jam_mulai`, 
					TIME_FORMAT(`db_ptiik_apps`.`tbl_agenda`.tgl_selesai, '%H:%i') as `jam_selesai`,
					DATE_FORMAT(tbl_agenda.tgl_mulai,'%d') as `ds`, 
					DATE_FORMAT(tbl_agenda.tgl_selesai, '%d') as `de` 
				FROM `db_ptiik_apps`.`tbl_agenda` 
					INNER JOIN `db_ptiik_apps`.tbl_detailagenda ON tbl_agenda.agenda_id = tbl_detailagenda.agenda_id
					INNER JOIN `db_ptiik_apps`.tbl_peserta ON tbl_detailagenda.agenda_id = tbl_peserta.agenda_id
					WHERE 1 = 1   
						";
		
		if($uid){
			$sql = $sql. " AND (`db_ptiik_apps`.tbl_peserta.karyawan_id = '".$uid."'  OR  mid(md5(`db_ptiik_apps`.tbl_peserta.karyawan_id),9,7)='".$uid."' )";
		}
		
		$sql= $sql . " AND tbl_peserta.karyawan_id NOT IN ('', '-') AND `tbl_agenda`.`is_valid` = '1'  AND date_format(tbl_detailagenda.tgl,'%Y-%m-%d') = '".$tgl."'  AND tbl_detailagenda.hari = '".$hari."'  
					  ORDER BY `tbl_agenda`.tgl_mulai ASC";
		
		return $this->db->query( $sql );
	
	}
	
	function getPengawasByDate($tgl=NULL, $uid=NULL){
	
		$sql = "SELECT DISTINCT
					time_format(db_ptiik_apps.tbl_jadwalujian.jam_mulai, '%H:%i') as `jam_mulai`,
					time_format(db_ptiik_apps.tbl_jadwalujian.jam_selesai, '%H:%i') as `jam_selesai`,
					db_ptiik_apps.tbl_penjadwalan.jenis_kegiatan_id AS kegiatan,
					db_ptiik_apps.tbl_jadwalujian.kelas_id,
					db_ptiik_apps.tbl_pengawas.karyawan_id,
					db_ptiik_apps.tbl_jadwalujian.tgl,
					db_ptiik_apps.tbl_jadwalujian.hari,
					db_ptiik_apps.tbl_jadwalujian.ruang,
					db_ptiik_apps.vw_mkditawarkan.namamk,
					db_ptiik_apps.vw_mkditawarkan.kode_mk,
					tbl_jadwalujian.ujian_id
					FROM
					db_ptiik_apps.tbl_penjadwalan
					INNER JOIN db_ptiik_apps.tbl_detailpenjadwalan ON db_ptiik_apps.tbl_penjadwalan.jadwal_id = db_ptiik_apps.tbl_detailpenjadwalan.jadwal_id
					INNER JOIN db_ptiik_apps.tbl_jadwalujian ON db_ptiik_apps.tbl_detailpenjadwalan.detail_id = db_ptiik_apps.tbl_jadwalujian.ujian_id
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id
					INNER JOIN db_ptiik_apps.vw_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.vw_mkditawarkan.mkditawarkan_id WHERE 1 = 1 ";
		if($tgl){
			$sql = $sql . " AND db_ptiik_apps.tbl_jadwalujian.tgl = '".$tgl."' ";
		}
		
		
		if($uid){
			$sql = $sql . " AND db_ptiik_apps.tbl_pengawas.karyawan_id = '".$uid."' ";
		}
		
		return $this->db->query( $sql );
	}
	
	function getPengawasByWeek($day=NULL,$month=NULL,$year=NULL, $runday=NULL, $jam=NULL, $uid=NULL){
		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$month."-".($day+1);
		
		$hari=$this->get_namahari($runday);
		
		$sqlx = "SELECT DISTINCT
					time_format(db_ptiik_apps.tbl_jadwalujian.jam_mulai, '%H:%i') as `jam_mulai`,
					time_format(db_ptiik_apps.tbl_jadwalujian.jam_selesai, '%H:%i') as `jam_selesai`,
					db_ptiik_apps.tbl_jadwalujian.tgl,
					db_ptiik_apps.tbl_jadwalujian.prodi_id,
					db_ptiik_apps.tbl_jadwalujian.hari,
					db_ptiik_apps.tbl_jadwalujian.ruang,
					db_ptiik_apps.tbl_jadwalujian.is_project,
					db_ptiik_apps.vw_jadwaldosen.kode_mk,
					db_ptiik_apps.vw_jadwaldosen.namamk,
					db_ptiik_apps.tbl_jadwalujian.kelas_id,
					db_ptiik_apps.vw_jadwaldosen.prodi,
					db_ptiik_apps.tbl_detailjadwalruang.inf_jenis_kegiatan as `kegiatan`
					FROM
					db_ptiik_apps.tbl_jadwalujian
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id
					INNER JOIN db_ptiik_apps.vw_jadwaldosen ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.vw_jadwaldosen.mkditawarkan_id AND db_ptiik_apps.tbl_jadwalujian.kelas_id = db_ptiik_apps.vw_jadwaldosen.kelas_id AND db_ptiik_apps.tbl_jadwalujian.prodi_id = db_ptiik_apps.vw_jadwaldosen.prodi_id
					INNER JOIN db_ptiik_apps.tbl_detailjadwalruang ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_detailjadwalruang.jadwal_id
					WHERE 1 = 1 
					";
		$sql = "SELECT DISTINCT
					time_format(db_ptiik_apps.tbl_jadwalujian.jam_mulai, '%H:%i') as `jam_mulai`,
					time_format(db_ptiik_apps.tbl_jadwalujian.jam_selesai, '%H:%i') as `jam_selesai`,
					db_ptiik_apps.tbl_penjadwalan.jenis_kegiatan_id AS kegiatan,
					db_ptiik_apps.tbl_jadwalujian.kelas_id,
					db_ptiik_apps.tbl_jadwalujian.tgl,
					db_ptiik_apps.tbl_jadwalujian.hari,
					db_ptiik_apps.tbl_jadwalujian.ruang,
					db_ptiik_apps.vw_mkditawarkan.namamk,
					db_ptiik_apps.vw_mkditawarkan.kode_mk
					FROM
					db_ptiik_apps.tbl_penjadwalan
					INNER JOIN db_ptiik_apps.tbl_detailpenjadwalan ON db_ptiik_apps.tbl_penjadwalan.jadwal_id = db_ptiik_apps.tbl_detailpenjadwalan.jadwal_id
					INNER JOIN db_ptiik_apps.tbl_jadwalujian ON db_ptiik_apps.tbl_detailpenjadwalan.detail_id = db_ptiik_apps.tbl_jadwalujian.ujian_id
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id
					INNER JOIN db_ptiik_apps.vw_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.vw_mkditawarkan.mkditawarkan_id WHERE 1 = 1 ";
		if($tgl){
			$sql = $sql . " AND db_ptiik_apps.tbl_jadwalujian.tgl = '".$tgl."' ";
		}
		
		if($jam){
			$sql = $sql. "				
				AND ((time_format(db_ptiik_apps.tbl_jadwalujian.jam_mulai, '%H:%i') <= '".$jam."'  AND time_format(db_ptiik_apps.tbl_jadwalujian.jam_selesai, '%H:%i') > '".$jam."') 
					OR (time_format(db_ptiik_apps.tbl_jadwalujian.jam_mulai, '%H:%i') >= '".$jam."'  AND time_format(db_ptiik_apps.tbl_jadwalujian.jam_selesai, '%H:%i') < '".$jam."') 
					OR (time_format(db_ptiik_apps.tbl_jadwalujian.jam_selesai, '%H:%i') = '".$jam."')) ";
		}
		
		if($uid){
			$sql = $sql . " AND db_ptiik_apps.tbl_pengawas.karyawan_id = '".$uid."' ";
		}
		
		return $this->db->query( $sql );
	}
	
	function getPengawasByMonth($day,$month,$year, $runday, $uid=NULL){
		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$month."-".($day+1);
		
		$hari=$this->get_namahari($runday);
		
		$sqlx = "SELECT DISTINCT
					db_ptiik_apps.tbl_jadwalujian.tgl,
					db_ptiik_apps.tbl_jadwalujian.prodi_id,
					db_ptiik_apps.tbl_jadwalujian.hari,
					db_ptiik_apps.tbl_jadwalujian.ruang,
					db_ptiik_apps.tbl_jadwalujian.is_project,
					db_ptiik_apps.vw_jadwaldosen.kode_mk,
					db_ptiik_apps.vw_jadwaldosen.namamk,
					db_ptiik_apps.tbl_jadwalujian.kelas_id,
					db_ptiik_apps.vw_jadwaldosen.prodi,
					db_ptiik_apps.tbl_detailjadwalruang.inf_jenis_kegiatan as `kegiatan`
					FROM
					db_ptiik_apps.tbl_jadwalujian
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id
					INNER JOIN db_ptiik_apps.vw_jadwaldosen ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.vw_jadwaldosen.mkditawarkan_id AND db_ptiik_apps.tbl_jadwalujian.kelas_id = db_ptiik_apps.vw_jadwaldosen.kelas_id AND db_ptiik_apps.tbl_jadwalujian.prodi_id = db_ptiik_apps.vw_jadwaldosen.prodi_id
					INNER JOIN db_ptiik_apps.tbl_detailjadwalruang ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_detailjadwalruang.jadwal_id
					WHERE 1=1 
					";
		$sql = "SELECT DISTINCT
					db_ptiik_apps.tbl_penjadwalan.jenis_kegiatan_id AS kegiatan,
					db_ptiik_apps.tbl_jadwalujian.kelas_id,
					db_ptiik_apps.tbl_jadwalujian.tgl,
					db_ptiik_apps.tbl_jadwalujian.hari,
					db_ptiik_apps.tbl_jadwalujian.ruang,
					db_ptiik_apps.vw_mkditawarkan.namamk,
					db_ptiik_apps.vw_mkditawarkan.kode_mk
					FROM
					db_ptiik_apps.tbl_penjadwalan
					INNER JOIN db_ptiik_apps.tbl_detailpenjadwalan ON db_ptiik_apps.tbl_penjadwalan.jadwal_id = db_ptiik_apps.tbl_detailpenjadwalan.jadwal_id
					INNER JOIN db_ptiik_apps.tbl_jadwalujian ON db_ptiik_apps.tbl_detailpenjadwalan.detail_id = db_ptiik_apps.tbl_jadwalujian.ujian_id
					INNER JOIN db_ptiik_apps.tbl_pengawas ON db_ptiik_apps.tbl_jadwalujian.ujian_id = db_ptiik_apps.tbl_pengawas.ujian_id
					INNER JOIN db_ptiik_apps.vw_mkditawarkan ON db_ptiik_apps.tbl_jadwalujian.mkditawarkan_id = db_ptiik_apps.vw_mkditawarkan.mkditawarkan_id WHERE 1 = 1 ";
		if($tgl){
			$sql = $sql . " AND db_ptiik_apps.tbl_jadwalujian.tgl = '".$tgl."' ";
		}		
			
		if($uid){
			$sql = $sql . " AND db_ptiik_apps.tbl_pengawas.karyawan_id = '".$uid."' ";
		}
		//echo $sql;
		return $this->db->query( $sql );
	}
	
	function getEventDetailCivitas($day,$month,$year, $runday, $jam, $uid=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$month."-".($day+1);
		
		$hari=$this->get_namahari($runday);
		
		$sql = "SELECT DISTINCT mid(md5(`tbl_agenda`.agenda_id),9,7) as `id`, `tbl_agenda`.agenda_id, `tbl_agenda`.judul, 
					`tbl_agenda`.keterangan, (SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_agenda.jenis_kegiatan_id) as `jenis`,
					DATE_FORMAT(`tbl_agenda`.tgl_mulai,'%d') as `ds`, 
					DATE_FORMAT(`tbl_agenda`.tgl_selesai, '%d') as `de`,
					`tbl_detailagenda`.`tgl`,
					`tbl_detailagenda`.`jam_mulai`
				FROM
				`db_ptiik_apps`.`tbl_agenda`
				Inner Join `db_ptiik_apps`.`tbl_detailagenda` ON `tbl_agenda`.`agenda_id` = `tbl_detailagenda`.`agenda_id` 
				INNER JOIN `db_ptiik_apps`.tbl_peserta ON tbl_agenda.agenda_id = tbl_peserta.agenda_id
				WHERE  tbl_peserta.karyawan_id NOT IN ('', '-') AND `tbl_agenda`.`is_valid` = '1' AND 
				`tbl_detailagenda`.tgl='".$tgl."' 
					AND ((`tbl_detailagenda`.jam_mulai <= '".$jam."'  AND `tbl_detailagenda`.jam_selesai > '".$jam."') 
					OR (`tbl_detailagenda`.jam_mulai >= '".$jam."'  AND `tbl_detailagenda`.jam_selesai < '".$jam."') 
					OR (`tbl_detailagenda`.jam_selesai = '".$jam."'))
				 ";
		if($uid){
			$sql = $sql. " AND (`db_ptiik_apps`.tbl_peserta.karyawan_id = '".$uid."'  OR mid(md5(`db_ptiik_apps`.tbl_peserta.karyawan_id),9,7) = '".$uid."') ";
		}
		$sql= $sql . "ORDER BY `tbl_agenda`.tgl_mulai ASC";
		
		
		return $this->db->query( $sql );
	
	}
	
	function get_kegiatan($day,$month,$year, $runday, $jam, $uid=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$month."-".($day+1);
		
		$hari=$this->get_namahari($runday);
		
		$sql = "SELECT DISTINCT 
					db_ptiik_apps.tbl_aktifitas.karyawan_id,
					db_ptiik_apps.tbl_aktifitas.tgl,
					time_format(db_ptiik_apps.tbl_aktifitas.jam_mulai, '%H:%i') as `jam_mulai`,
					time_format(db_ptiik_apps.tbl_aktifitas.jam_selesai, '%H:%i') as `jam_selesai`,
					db_ptiik_apps.tbl_aktifitas.inf_ruang as ruang,
					db_ptiik_apps.tbl_aktifitas.jenis_kegiatan_id as `jenis`,
					db_ptiik_apps.tbl_aktifitas.judul as kegiatan,
					db_ptiik_apps.tbl_aktifitas.nama_mk as namamk,
					db_ptiik_apps.tbl_aktifitas.catatan as keterangan,
					db_ptiik_apps.tbl_karyawan.nama,
					db_ptiik_apps.tbl_karyawan.gelar_awal,
					db_ptiik_apps.tbl_karyawan.gelar_akhir,
					db_ptiik_apps.tbl_karyawan.is_status
					FROM
					db_ptiik_apps.tbl_aktifitas
					INNER JOIN db_ptiik_apps.tbl_karyawan ON db_ptiik_apps.tbl_aktifitas.karyawan_id = db_ptiik_apps.tbl_karyawan.karyawan_id
				WHERE 1 = 1
					";
		if($tgl){
			//$sql = $sql . " AND db_ptiik_apps.tbl_aktifitas.tgl = '".$tgl."' ";
			$sql = $sql . " AND ('".$tgl."' BETWEEN db_ptiik_apps.tbl_aktifitas.tgl AND db_ptiik_apps.tbl_aktifitas.tgl_selesai) ";
		}
		
		if($jam){
			$sql = $sql . " AND ((db_ptiik_apps.tbl_aktifitas.jam_mulai <= '".$jam."'  AND db_ptiik_apps.tbl_aktifitas.jam_selesai > '".$jam."') 
					OR (db_ptiik_apps.tbl_aktifitas.jam_mulai >= '".$jam."'  AND db_ptiik_apps.tbl_aktifitas.jam_selesai < '".$jam."') 
					OR (db_ptiik_apps.tbl_aktifitas.jam_selesai = '".$jam."')) ";
			
		}
		
		if($uid){
			$sql = $sql . " AND (db_ptiik_apps.tbl_aktifitas.karyawan_id = '".$uid."' OR (mid(md5(db_ptiik_apps.tbl_aktifitas.karyawan_id),9,7) = '".$uid."')) ";
		}
		
		$sql = $sql . " ORDER BY time_format(db_ptiik_apps.tbl_aktifitas.jam_mulai, '%H:%i') ";
		$result = $this->db->query( $sql );
		
		return $result;	
	
	}
	
	function getPerkuliahanByBulan($day,$month,$year, $runday, $uid=NULL){
	
		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$this->addNol($month)."-".$this->addNol(($day+1));
		
		$hari=$this->get_namahari($runday);
		
		$sql ="SELECT  DISTINCT 
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`ruang`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`kegiatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`catatan`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`kode_mk`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`namamk`
				FROM
				`db_ptiik_apps`.`tbl_detailjadwalruang`
				Inner Join `db_ptiik_apps`.`tbl_jadwalruang` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`
				Left Join `db_ptiik_apps`.`vw_jadwaldosen` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`vw_jadwaldosen`.`jadwal_id`
				WHERE `db_ptiik_apps`.`tbl_detailjadwalruang`.`dosen_id` NOT IN ('', '-') AND `db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl` = '".$tgl."' 				
				AND  `db_ptiik_apps`.`tbl_detailjadwalruang`.`is_aktif` = 1 
				";
		
		if($uid){
			$sql = $sql . " AND  (`db_ptiik_apps`.`vw_jadwaldosen`.`karyawan_id` = '".$uid."'  OR mid(md5(`db_ptiik_apps`.`vw_jadwaldosen`.`karyawan_id`),9,7) = '".$uid."')";
		}
		
			
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function getPerkuliahan($day,$month,$year, $runday, $jam, $uid=NULL){
	
		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}
		
		$tglend	= $year."-".$month."-".($day+1);
		
		$hari=$this->get_namahari($runday);
		
		$sqlx ="SELECT
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') as `jam_mulai`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') as `jam_selesai`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`dosen_id`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`ruang`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`kegiatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`catatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`kode_mk`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`namamk`
				FROM
				`db_ptiik_apps`.`tbl_detailjadwalruang`
				Inner Join `db_ptiik_apps`.`tbl_jadwalruang` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`
				Left Join `db_ptiik_apps`.`vw_jadwaldosen` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`vw_jadwaldosen`.`jadwal_id`
				WHERE `db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl` = '".$tgl."' 				
				AND ((time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') <= '".$jam."'  AND time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') > '".$jam."') 
					OR (time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') >= '".$jam."'  AND time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') < '".$jam."') 
					OR (time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') = '".$jam."')) 
				AND  `db_ptiik_apps`.`tbl_detailjadwalruang`.`is_aktif` = 1 
				";
		$sql = "SELECT DISTINCT 
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') as `jam_mulai`,
				time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') as `jam_selesai`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`dosen_id`,
				`db_ptiik_apps`.`tbl_detailjadwalruang`.`ruang`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`kegiatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`catatan`,
				`db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`kode_mk`,
				`db_ptiik_apps`.`vw_jadwaldosen`.`namamk`
				FROM
				`db_ptiik_apps`.`tbl_detailjadwalruang`
				Inner Join `db_ptiik_apps`.`tbl_jadwalruang` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`tbl_jadwalruang`.`jadwal_id`
				Left Join `db_ptiik_apps`.`vw_jadwaldosen` ON `db_ptiik_apps`.`tbl_detailjadwalruang`.`jadwal_id` = `db_ptiik_apps`.`vw_jadwaldosen`.`jadwal_id`
				WHERE `db_ptiik_apps`.`tbl_detailjadwalruang`.`dosen_id` NOT IN ('', '-') AND `db_ptiik_apps`.`tbl_detailjadwalruang`.`tgl` = '".$tgl."' 				
				AND ((time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') <= '".$jam."'  AND time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') > '".$jam."') 
					OR (time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') >= '".$jam."'  AND time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') < '".$jam."') 
					OR (time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_selesai`, '%H:%i') = '".$jam."')) 
				AND  `db_ptiik_apps`.`tbl_detailjadwalruang`.`is_aktif` = '1'
				";
		if($uid){
			$sql = $sql . " AND  (`db_ptiik_apps`.`vw_jadwaldosen`.`karyawan_id` = '".$uid."'  OR mid(md5(`db_ptiik_apps`.`vw_jadwaldosen`.`karyawan_id`),9,7) = '".$uid."') ";
		}
		
		$sql = $sql . " ORDER BY time_format(`db_ptiik_apps`.`tbl_detailjadwalruang`.`jam_mulai`, '%H:%i') ASC";
		
		$result = $this->db->query( $sql );
		
		return $result;	
	}
	
	function getEventDetail($day,$month,$year, $runday, $jam=NULL){

		if(strlen($month)==1){
			$tgl	= $year."-".$this->addNol($month)."-".$this->addNol($day);
		}else{
			$tgl	= $year."-".$month."-".$day;
		}
		
		$tglend	= $year."-".$month."-".($day+1);
		
		$hari=$this->get_namahari($runday);
		
		$sql = "SELECT DISTINCT mid(md5(`tbl_agenda`.agenda_id),9,7) as `id`, `tbl_agenda`.agenda_id, `tbl_agenda`.judul, 
					`tbl_agenda`.keterangan, (SELECT keterangan FROM db_ptiik_apps.tbl_jeniskegiatan WHERE jenis_kegiatan_id=tbl_agenda.jenis_kegiatan_id) as `jenis`,
					DATE_FORMAT(`tbl_agenda`.tgl_mulai,'%d') as `ds`, 
					DATE_FORMAT(`tbl_agenda`.tgl_selesai, '%d') as `de`,
					`tbl_detailagenda`.`tgl`,
					TIME_FORMAT(`tbl_detailagenda`.`jam_mulai`, '%H:%i') as `jam_mulai`
				FROM
				`db_ptiik_apps`.`tbl_agenda`
				Inner Join `db_ptiik_apps`.`tbl_detailagenda` ON `tbl_agenda`.`agenda_id` = `tbl_detailagenda`.`agenda_id` WHERE  `tbl_agenda`.`is_valid` = '1' AND `tbl_agenda`.`is_private` = '0' AND 
				`tbl_detailagenda`.tgl='".$tgl."' 
					
				 ";
		if($jam){
			$sql = $sql. "AND ((`tbl_detailagenda`.jam_mulai <= '".$jam."'  AND `tbl_detailagenda`.jam_selesai > '".$jam."') 
					OR (`tbl_detailagenda`.jam_mulai >= '".$jam."'  AND `tbl_detailagenda`.jam_selesai < '".$jam."') 
					OR (`tbl_detailagenda`.jam_selesai = '".$jam."')) ";
		}
		$sql= $sql . "ORDER BY `tbl_detailagenda`.jam_mulai ASC";
		
		return $this->db->query( $sql );
	
	}
	
	function getEventByMonth($day,$month,$year){
		$tgl	= $year."-".$this->addNol($month);
		$tglend	= $year."-".$month."-".($day+1);
		//$sql = "select Id, keterangan,DATE_FORMAT(tgl_mulai,'%d') as `ds`, DATE_FORMAT(tgl_selesai, '%d') as `de` from `test`.`tbl_agenda` WHERE tgl_mulai between '".$tgl."' AND '".$tglend."' ORDER BY tgl_mulai ASC";
		$sql = "SELECT mid(md5(agenda_id),9,7) as `id`, agenda_id, keterangan,DATE_FORMAT(tgl_mulai,'%d') as `ds`, DATE_FORMAT(tgl_selesai, '%d') as `de` 
				FROM `db_ptiik_apps`.`tbl_agenda` WHERE `tbl_agenda`.`is_valid` = '1'  AND `tbl_agenda`.`is_private` = '0' AND tgl_mulai like '".$tgl."%' ORDER BY tgl_mulai ASC";
	
		return $this->db->query( $sql );
	
	}
	
	
	function stringToText($string){
		$string = strip_tags($string);

		if (strlen($string) > 15) {
			$stringCut = substr($string, 0, 25);
			// make sure it ends in a word so assassinate doesn't become ass...
			$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'..'; 
		}
		return $string;
	}
	
	function potong_kalimat ($content, $length) {
		$content = strip_tags($content);
		$tmp = explode(" ", $content);
		$data = array();
		$i = 0;
		if(count($tmp) < $length) {
			$data = $tmp;
		} else {
			while($i<$length) {
				$data[$i] = $tmp[$i];
				$i++;
			}
			$data[] = "...";
		}
		return implode(" ", $data);
	}
	
	function addnol($string){
		if(strlen($string)==1){
			$string = "0".$string;
		}
		
		return $string;
	}
	
	function get_namahari($runday){
		switch($runday){
			case '0': $hari='minggu'; break;
			case '1': $hari='senin'; break;
			case '2': $hari='selasa'; break;
			case '3': $hari='rabu'; break;
			case '4': $hari='kamis'; break;
			case '5': $hari='jumat'; break;
			case '6': $hari='sabtu'; break;
			case '7': $hari='minggu'; break;
		}
		
		return $hari;
	}
	
	function get_event($str=NULL){
		$sql = "SELECT
				`tbl_agenda`.`agenda_id` AS `id`,
				`tbl_agenda`.`judul` AS `title`,
				date_format(`tbl_agenda`.`tgl_mulai`, '%Y-%m-%d %H:%i' ) AS `start`,
				if(is_allday=1,date_format(`tbl_agenda`.`tgl_selesai`, '%Y-%m-%d 16:00' ),date_format(`tbl_agenda`.`tgl_selesai`, '%Y-%m-%d %H:%i' )) AS `end`
				FROM
				db_ptiik_apps.`tbl_agenda`";
		return $this->db->query( $sql );
	}
	
	
	function get_blok_waktu(){
		$sql = "SELECT distinct time_format(jam_mulai,'%H:%i') as `jam_mulai`, time_format(jam_selesai,'%H:%i') as `jam_selesai` FROM db_ptiik_apps.tbl_blokwaktu ORDER BY db_ptiik_apps.tbl_blokwaktu.jam_mulai ASC";
		$result = $this->db->query( $sql );
		
		return $result;
	}
	
	function replace_agenda_staff($datanya){
		$result= $this->db->replace('db_ptiik_apps`.`tbl_aktifitas',$datanya);
		return $result;
	}
	
}