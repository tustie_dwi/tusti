<?php
class model_jadwalinfo extends model {

	public function __construct() {
		parent::__construct();	
	}
	
	function get_prodi(){
		$sql = "SELECT
					tbl_unit_kerja.unit_id,
					tbl_unit_kerja.kode,
					tbl_unit_kerja.keterangan as value,
					tbl_unit_kerja.english_version,
					tbl_unit_kerja.kategori,
					tbl_unit_kerja.is_aktif,
					tbl_unit_kerja.strata
				FROM
				db_ptiik_apps.tbl_unit_kerja
				WHERE kategori='prodi' AND is_aktif='1' ORDER BY strata DESC, keterangan ASC";
				
		return $this->db->query($sql);
	}
	
	function get_jenis_ujian(){
		$sql = "SELECT distinct tbl_jadwalujian.jenis_ujian FROM db_ptiik_apps.tbl_jadwalujian WHERE is_aktif='1' AND YEAR(tbl_jadwalujian.tgl)='".date("Y")."' ";
		return $this->db->getRow($sql);
	}
	//ujian
	function get_jam_ujian(){
		$sql = "SELECT tbl_jam.jam_mulai, tbl_jam.jam_selesai, tbl_jam.urut
				FROM db_ptiik_apps.tbl_jam
				WHERE tbl_jam.is_ujian = '1'
				ORDER BY tbl_jam.urut";
		return $this->db->query($sql);
	}
	
	function get_range_tgl(){
		$tahun = date('Y');
		//$tahun='2015';
		$sql = "SELECT 
					dat.hari,
					GROUP_CONCAT(dat.tgl) tgl
				FROM (
					SELECT DISTINCT
					tbl_jadwalujian.hari, 
					tbl_jadwalujian.tgl
					FROM db_ptiik_apps.tbl_jadwalujian
					WHERE is_aktif = '1' AND YEAR(tbl_jadwalujian.tgl) = $tahun
					ORDER BY tbl_jadwalujian.hari, tbl_jadwalujian.tgl
				) dat
				GROUP BY dat.hari";
		return $this->db->query($sql);
	}
	
	function get_jadwal_ujian(){
		$sql = "SELECT DISTINCT 
					tbl_jadwalujian.kelas,
					MID(MD5(tbl_mkditawarkan.matakuliah_id),9,7) as mk_id,
					tbl_namamk.keterangan,
					tbl_jadwalujian.hari,
					tbl_jadwalujian.tgl,
					tbl_jadwalujian.jam_mulai,
					tbl_jadwalujian.jam_selesai,
					tbl_jadwalujian.ruang_id,
					tbl_karyawan.nama dosen,
					tbl_jadwalujian.prodi_id prodi,
					tbl_jadwalujian.jenis_ujian,
					b.urut
				FROM db_ptiik_apps.tbl_jadwalujian
				INNER JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_jadwalujian.mkditawarkan_id
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				INNER JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_jadwalujian.pengampu_id
				INNER JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_jam AS b ON tbl_jadwalujian.jam_mulai = b.jam_mulai AND b.is_ujian = '1'
				INNER JOIN db_ptiik_apps.tbl_hari ON tbl_jadwalujian.hari = tbl_hari.hari
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_ruang.ruang_id = tbl_jadwalujian.ruang_id
				WHERE tbl_jadwalujian.is_aktif = '1'
				ORDER BY tbl_hari.id, tbl_ruang.kode_ruang, b.urut";
		return $this->db->query($sql);
	}
	
	
	//jadwal kuliah
	function get_is_pendek(){
		$sql = "SELECT is_pendek FROM db_ptiik_apps.tbl_tahunakademik  WHERE tbl_tahunakademik.is_aktif = '1'";
		$x = $this->db->getRow($sql);
		if($x->is_pendek) return 1;
		else return 0;
		//return $x->is_pendek;
	}
	
	function get_jam($is_pendek, $is_ujian=0){
		$sql = "SELECT tbl_jam.jam_mulai, tbl_jam.jam_selesai, tbl_jam.urut
				FROM db_ptiik_apps.tbl_jam
				WHERE tbl_jam.is_aktif = '1' AND tbl_jam.is_pendek = '$is_pendek' AND tbl_jam.is_ujian = '0'
				ORDER BY tbl_jam.urut";
		return $this->db->query($sql);
	}
	
	function get_ruang($cabang=NULL, $fakultas=NULL){
		$sql = "SELECT tbl_ruang.ruang_id, tbl_ruang.kode_ruang, tbl_ruang.keterangan
				FROM db_ptiik_apps.tbl_ruang
				WHERE tbl_ruang.fakultas_id = '$fakultas' 
					AND tbl_ruang.cabang_id = '$cabang'
					AND tbl_ruang.kategori_ruang  IN ('kuliah','lab') 
				ORDER BY tbl_ruang.kode_ruang ASC";
		return $this->db->query($sql);
	}
	
	function get_dosen($cabang=NULL, $fakultas=NULL){
		$sql = "SELECT tbl_karyawan.nama, MID(MD5(tbl_karyawan.karyawan_id),9,7) karyawan_id, tbl_karyawan.gelar_akhir, tbl_karyawan.gelar_awal
				FROM db_ptiik_apps.tbl_karyawan
				WHERE tbl_karyawan.fakultas_id = '$fakultas' 
					AND tbl_karyawan.cabang_id = '$cabang'
					AND tbl_karyawan.is_status = 'dosen' AND is_aktif NOT IN ('keluar', 'meninggal') 
				ORDER BY tbl_karyawan.nama";
		return $this->db->query($sql);
	}
	
	function get_matakuliah($cabang=NULL, $fakultas=NULL){
		$sql = "SELECT DISTINCT
					tbl_namamk.keterangan,
					tbl_matakuliah.kode_mk,
					MID(MD5(tbl_matakuliah.matakuliah_id),9,7)as mk_id
				FROM db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_tahunakademik
					ON tbl_tahunakademik.tahun_akademik = tbl_mkditawarkan.tahun_akademik 
					AND tbl_tahunakademik.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
					AND tbl_jadwalmk.cabang_id = '$cabang'
					AND tbl_jadwalmk.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = tbl_jadwalmk.prodi_id AND tbl_prodi.fakultas_id = '$fakultas'
				ORDER BY tbl_namamk.keterangan";
		return $this->db->query($sql);
	}
	
	function get_praktikum($cabang=NULL, $fakultas=NULL, $unit=NULL){
		$sql = "SELECT DISTINCT
					tbl_namamk.keterangan,
					tbl_matakuliah.kode_mk,
					MID(MD5(tbl_matakuliah.matakuliah_id),9,7) as mk_id
				FROM db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_tahunakademik
					ON tbl_tahunakademik.tahun_akademik = tbl_mkditawarkan.tahun_akademik 
					AND tbl_tahunakademik.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
					AND tbl_jadwalmk.cabang_id = '$cabang'
					AND tbl_jadwalmk.is_aktif = '1'
					AND tbl_jadwalmk.is_praktikum = '1'
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = tbl_jadwalmk.prodi_id AND tbl_prodi.fakultas_id = '$fakultas'
				";
		if($unit) $sql.="INNER JOIN db_ptiik_apps.tbl_praktikum_lab ON tbl_matakuliah.matakuliah_id = tbl_praktikum_lab.matakuliah_id AND tbl_praktikum_lab.unit_id = '$unit' "; 
		$sql.="ORDER BY tbl_namamk.keterangan";
				
		return $this->db->query($sql);
	}
	
	function get_jadwal_ori($cabang=NULL, $fakultas=NULL, $is_pendek=NULL, $lang=NULL, $str=NULL){
		$sql = "SELECT  DISTINCT 
					MID(MD5(tbl_mkditawarkan.matakuliah_id),9,7) as mk_id,tbl_namamk.keterangan as mk_ori, ";
					if(strTolower($lang)=='in') $sql.=" tbl_namamk.keterangan as mk, concat('Kelas - ', tbl_jadwalmk.kelas) as strkelas,  ";
					else $sql.=" tbl_namamk.english_version as mk, concat('Class - ', tbl_jadwalmk.kelas) as strkelas, ";
			$sql.="
					tbl_jadwalmk.kelas,
					tbl_jadwalmk.is_praktikum,
					tbl_ruang.ruang_id,
					tbl_ruang.kode_ruang ruang,
					tbl_jadwalmk.jam_mulai,
					tbl_jadwalmk.jam_selesai,
					tbl_jadwalmk.hari,
					MID(MD5(tbl_karyawan.karyawan_id),9,7) as karyawan_id,
					tbl_karyawan.nama as dosen,
					tbl_prodi.kode_prodi as prodi,
					tbl_jam.urut,
					db_ptiik_apps.tbl_jadwalmk.repeat_on,
					db_ptiik_apps.tbl_jadwalmk.tgl_mulai,
					db_ptiik_apps.tbl_jadwalmk.tgl_selesai,
					b.urut as urut_selesai,
					tbl_jadwalmk.repeat_on,
					tbl_jadwalmk.tgl_mulai,
					tbl_jadwalmk.tgl_selesai
				FROM db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_tahunakademik
					ON tbl_tahunakademik.tahun_akademik = tbl_mkditawarkan.tahun_akademik 
					AND tbl_tahunakademik.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
					AND tbl_jadwalmk.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_ruang.ruang_id = tbl_jadwalmk.ruang_id
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_jadwalmk.pengampu_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = tbl_jadwalmk.prodi_id 
				INNER JOIN db_ptiik_apps.tbl_jam ON tbl_jam.jam_mulai = tbl_jadwalmk.jam_mulai AND tbl_jam.is_aktif = '1' AND tbl_jam.is_pendek = '$is_pendek'
				INNER JOIN db_ptiik_apps.tbl_hari ON tbl_hari.hari = tbl_jadwalmk.hari
				INNER JOIN db_ptiik_apps.tbl_jam AS b ON tbl_jadwalmk.jam_selesai = b.jam_mulai AND b.is_aktif = '1' AND b.is_pendek = '$is_pendek'
				WHERE tbl_jadwalmk.is_aktif = '1' ";
			if($str) $sql.=" ORDER BY  tbl_hari.id, tbl_jam.urut ASC, tbl_ruang.kode_ruang ASC ";
			else $sql.=" ORDER BY tbl_ruang.kode_ruang ASC, tbl_hari.id, tbl_jam.urut ";
		//echo $sql;
		return $this->db->query($sql);
	}
	
	function get_jadwal($cabang=NULL, $fakultas=NULL, $is_pendek=NULL, $lang=NULL, $str=NULL){
		$tgl = date("Y-m-d");
		$tgl_= strtotime("+10 day", $tgl);
		$tgl_end = date("Y-m-d", $tgl_);
		
		$sql = "SELECT  DISTINCT 
					MID(MD5(tbl_mkditawarkan.matakuliah_id),9,7) as mk_id,tbl_namamk.keterangan as mk_ori, ";
					if(strTolower($lang)=='in') $sql.=" tbl_namamk.keterangan as mk, concat('Kelas - ', tbl_jadwalmk.kelas) as strkelas,  ";
					else $sql.=" tbl_namamk.english_version as mk, concat('Class - ', tbl_jadwalmk.kelas) as strkelas, ";
			$sql.="
					tbl_jadwalmk.kelas,
					tbl_jadwalmk.is_praktikum,
					tbl_ruang.ruang_id,
					tbl_ruang.kode_ruang ruang,
					tbl_jadwalmk.jam_mulai,
					tbl_jadwalmk.jam_selesai,
					tbl_jadwalmk.hari,
					MID(MD5(tbl_karyawan.karyawan_id),9,7) as karyawan_id,
					tbl_karyawan.nama as dosen,
					tbl_prodi.kode_prodi as prodi,
					tbl_jam.urut,
					db_ptiik_apps.tbl_jadwalmk.repeat_on,
					db_ptiik_apps.tbl_jadwalmk.tgl_mulai,
					db_ptiik_apps.tbl_jadwalmk.tgl_selesai,
					b.urut as urut_selesai,
					tbl_jadwalmk.repeat_on,
					tbl_jadwalmk.tgl_mulai,
					tbl_jadwalmk.tgl_selesai
				FROM db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_tahunakademik
					ON tbl_tahunakademik.tahun_akademik = tbl_mkditawarkan.tahun_akademik 
					AND tbl_tahunakademik.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
					AND tbl_jadwalmk.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_ruang.ruang_id = tbl_jadwalmk.ruang_id
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_jadwalmk.pengampu_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = tbl_jadwalmk.prodi_id 
				INNER JOIN db_ptiik_apps.tbl_jam ON tbl_jam.jam_mulai = tbl_jadwalmk.jam_mulai AND tbl_jam.is_aktif = '1' AND tbl_jam.is_pendek = '$is_pendek'
				INNER JOIN db_ptiik_apps.tbl_hari ON tbl_hari.hari = tbl_jadwalmk.hari
				INNER JOIN db_ptiik_apps.tbl_jam AS b ON tbl_jadwalmk.jam_selesai = b.jam_mulai AND b.is_aktif = '1' AND b.is_pendek = '$is_pendek'
				WHERE tbl_jadwalmk.is_aktif = '1' ";
			if($str) $sql.=" ORDER BY  tbl_hari.id, tbl_jam.urut ASC, tbl_ruang.kode_ruang ASC ";
			else $sql.=" ORDER BY tbl_ruang.kode_ruang ASC, tbl_hari.id, tbl_jam.urut ";
		//echo $sql;
		return $this->db->query($sql);
	}
	
	function get_jadwal_praktikum($cabang=NULL, $fakultas=NULL, $is_pendek=NULL, $lang=NULL, $unit=NULL){
		$sql = "SELECT  DISTINCT 
					MID(MD5(tbl_mkditawarkan.matakuliah_id),9,7) as mk_id,tbl_namamk.keterangan as mk_ori, ";
					if(strTolower($lang)=='in') $sql.=" tbl_namamk.keterangan as mk, concat('Kelas - ', tbl_jadwalmk.kelas) as strkelas,  ";
					else $sql.=" tbl_namamk.english_version as mk, concat('Class - ', tbl_jadwalmk.kelas) as strkelas, ";
			$sql.="
					tbl_jadwalmk.kelas,
					tbl_jadwalmk.is_praktikum,
					tbl_ruang.ruang_id,
					tbl_ruang.kode_ruang ruang,
					tbl_jadwalmk.jam_mulai,
					tbl_jadwalmk.jam_selesai,
					tbl_jadwalmk.hari,
					MID(MD5(tbl_karyawan.karyawan_id),9,7) as karyawan_id,
					tbl_karyawan.nama as dosen,
					tbl_prodi.kode_prodi as prodi,
					tbl_jam.urut,
					db_ptiik_apps.tbl_jadwalmk.repeat_on,
					db_ptiik_apps.tbl_jadwalmk.tgl_mulai,
					db_ptiik_apps.tbl_jadwalmk.tgl_selesai,
					b.urut as urut_selesai
				FROM db_ptiik_apps.tbl_mkditawarkan
				INNER JOIN db_ptiik_apps.tbl_tahunakademik
					ON tbl_tahunakademik.tahun_akademik = tbl_mkditawarkan.tahun_akademik 
					AND tbl_tahunakademik.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_jadwalmk ON tbl_jadwalmk.mkditawarkan_id = tbl_mkditawarkan.mkditawarkan_id 
					AND tbl_jadwalmk.cabang_id = '$cabang'
					AND tbl_jadwalmk.is_aktif = '1'
				INNER JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				INNER JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				INNER JOIN db_ptiik_apps.tbl_ruang ON tbl_ruang.ruang_id = tbl_jadwalmk.ruang_id
				LEFT JOIN db_ptiik_apps.tbl_pengampu ON tbl_pengampu.pengampu_id = tbl_jadwalmk.pengampu_id
				LEFT JOIN db_ptiik_apps.tbl_karyawan ON tbl_karyawan.karyawan_id = tbl_pengampu.karyawan_id
				INNER JOIN db_ptiik_apps.tbl_prodi ON tbl_prodi.prodi_id = tbl_jadwalmk.prodi_id AND tbl_prodi.fakultas_id = '$fakultas'
				INNER JOIN db_ptiik_apps.tbl_jam ON tbl_jam.jam_mulai = tbl_jadwalmk.jam_mulai AND tbl_jam.is_aktif = '1' AND tbl_jam.is_pendek = '$is_pendek'
				INNER JOIN db_ptiik_apps.tbl_hari ON tbl_hari.hari = tbl_jadwalmk.hari
				INNER JOIN db_ptiik_apps.tbl_jam AS b ON tbl_jadwalmk.jam_selesai = b.jam_mulai AND b.is_aktif = '1' AND b.is_pendek = '$is_pendek'
				INNER JOIN db_ptiik_apps.tbl_praktikum_lab ON tbl_matakuliah.matakuliah_id = tbl_praktikum_lab.matakuliah_id
				WHERE tbl_jadwalmk.is_aktif = '1'  AND tbl_jadwalmk.is_praktikum='1' AND tbl_praktikum_lab.unit_id = '$unit'
				ORDER BY tbl_ruang.kode_ruang, tbl_hari.id, tbl_jam.urut";
		//echo $sql;
		return $this->db->query($sql);
	}
}
?>