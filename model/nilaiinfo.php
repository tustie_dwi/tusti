<?php
class model_nilaiinfo extends model {
	public function __construct() {
		parent::__construct();	
	}
	
	function read_mk_praktikum($unit=NULL,$thn=NULL, $cabang=NULL, $fakultas=NULL, $lang=NULL){
		$sql	= "SELECT 
						mid(md5(tbl_praktikum.mkditawarkan_id),9,7) as mkid,
						tbl_namamk.namamk_id,";
					if(strTolower($lang)=='in') $sql.=" tbl_namamk.keterangan as mk,  ";
					else $sql.=" tbl_namamk.english_version as mk, ";
			$sql.="
				   	  tbl_namamk.namamk_id
				   FROM db_ptiik_apps.tbl_praktikum
				   LEFT JOIN db_ptiik_apps.tbl_mkditawarkan ON tbl_mkditawarkan.mkditawarkan_id = tbl_praktikum.mkditawarkan_id
				   LEFT JOIN db_ptiik_apps.tbl_matakuliah ON tbl_matakuliah.matakuliah_id = tbl_mkditawarkan.matakuliah_id
				   LEFT JOIN db_ptiik_apps.tbl_namamk ON tbl_namamk.namamk_id = tbl_matakuliah.namamk_id
				   LEFT JOIN db_ptiik_apps.tbl_praktikum_asisten 
				    ON tbl_praktikum_asisten.`mkditawarkan_id` = tbl_praktikum.`mkditawarkan_id`
				   INNER JOIN db_ptiik_apps.tbl_praktikum_lab ON tbl_praktikum_lab.matakuliah_id = tbl_matakuliah.matakuliah_id
				   WHERE 1
				   ";
		if($unit){
	    	$sql .= " AND tbl_praktikum_lab.unit_id ='".$unit."'";
	    }
	    if($thn){
	    	$sql .= " AND `tbl_mkditawarkan`.`tahun_akademik` ='".$thn."'";
	    }
	    if($fakultas){
	    	$sql .= " AND `tbl_namamk`.`fakultas_id` ='".$fakultas."'";
	    }
	    if($cabang){
	    	$sql .= " AND `tbl_mkditawarkan`.`cabang_id` ='".$cabang."'";
	    }
		
		$sql .= " GROUP BY `tbl_praktikum`.`mkditawarkan_id`";
		
		$result = $this->db->query( $sql );

		return $result;
	}

	function get_penilaian($jadwal=NULL, $materi=NULL, $mk=NULL, $nilai=NULL,$inf_kategori=NULL){
		$sql = "SELECT DISTINCT tbl_praktikum.jadwal_id,
					   mid(md5(tbl_praktikum.jadwal_id),9,7) as jdwl_id,
					   tbl_praktikum.kode_mk, 
					   tbl_praktikum.mkditawarkan_id,
					   mid(md5(tbl_praktikum.mkditawarkan_id),9,7) as mk_id,
					   tbl_praktikum.nama_mk,
					   tbl_praktikum.kelas_id,
					   tbl_praktikum.prodi_id,
					   tbl_praktikum_materi.judul as judul_materi,
					   tbl_praktikum.tahun_akademik,
					   mid(md5(tbl_praktikum_nilai.nilai_id),9,7) as nilaiid,
					   mid(md5(tbl_praktikum_nilai.materi_id),9,7) as materiid,
					   tbl_praktikum_nilai.*,
					   coms_user.name
				FROM db_ptiik_apps.tbl_praktikum_nilai
				LEFT JOIN db_coms.coms_user ON coms_user.id = tbl_praktikum_nilai.user_id
				LEFT JOIN db_ptiik_apps.tbl_praktikum ON tbl_praktikum_nilai.jadwal_id = tbl_praktikum.jadwal_id 
				AND tbl_praktikum_nilai.mkditawarkan_id = tbl_praktikum.mkditawarkan_id				
				LEFT JOIN db_ptiik_apps.tbl_praktikum_materi ON tbl_praktikum_materi.materi_id = tbl_praktikum_nilai.materi_id
				WHERE 1";
		if($jadwal!=""){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwal."'";
		}
		
		
		if($inf_kategori){
			$sql .= " AND tbl_praktikum_nilai.kategori = '".$inf_kategori."'";
		}
		
		if($mk!=""){
			$sql .= " AND mid(md5(tbl_praktikum.mkditawarkan_id),9,7) = '".$mk."'";
		}
		
		if($materi!=""&&$materi!="proses"&&$materi!="list"&&$materi!="get"&&$materi!="get_penilaian_by_kelas"){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.materi_id),9,7) = '".$materi."'";
		}elseif($materi==""){
			$sql .= " AND tbl_praktikum_nilai.materi_id = '-'";
		}elseif($materi=="proses"){
			$sql .= " ORDER BY tbl_praktikum.mkditawarkan_id,tbl_praktikum_nilai.materi_id ASC ";
		}elseif($materi=="get_penilaian_by_kelas"){
			$sql .= " GROUP BY tbl_praktikum.jadwal_id";
		}
		
		if($nilai!=""){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.nilai_id),9,7) = '".$nilai."'";
			// echo $sql;
			$result = $this->db->getRow( $sql );
		}
		else{
			// echo $sql;
			$result = $this->db->query( $sql );
		}
		
		
		return $result;
	}
	
	function get_data_nilai_mhs($nilaiid=NULL,$mhsid=NULL,$param=NULL,$md5=NULL){
		$sql = "SELECT 
				tbl_praktikum_nilai_mhs.*,
				mid(md5(tbl_praktikum_nilai_mhs.komponen_id),9,7) as komponenid,
				tbl_mahasiswa.nama
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai_mhs` 
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_praktikum_nilai_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
				WHERE 1
			   ";
		
		if($nilaiid){
			if($md5=='nomd5'){
				$sql .= " AND tbl_praktikum_nilai_mhs.nilai_id = '".$nilaiid."' ";
			}else{
				$sql .= " AND mid(md5(tbl_praktikum_nilai_mhs.nilai_id),9,7) = '".$nilaiid."' ";
			}
		}
		
		if($mhsid){
			$sql .= " AND tbl_praktikum_nilai_mhs.mahasiswa_id = '".$mhsid."' ";
		}
		
		if($param=='bymhs'){
			$sql .= " GROUP BY tbl_praktikum_nilai_mhs.mahasiswa_id ORDER BY tbl_mahasiswa.nama ASC";
		}
		
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;
	}
	
	function read_komponen($id=NULL,$thn=NULL,$komponen=NULL,$materiid=NULL,$param=NULL,$jadwalid=NULL,$materi_nilai=NULL,$param2=NULL,$kategori=NULL,$id2=NULL,$nilai_id=NULL,$nilai_param=NULL){
		$sql = "SELECT 
				mid(md5(`tbl_praktikum_komponen_nilai`.`komponen_id`),9,7) as `kpn_id`,
				`tbl_praktikum_komponen_nilai`.`komponen_id`,
				`tbl_praktikum_komponen_nilai`.`materi_id`,
				mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) as `jadwalid`,
				`tbl_praktikum_komponen_nilai`.`jadwal_id`,
				`tbl_praktikum_komponen_nilai`.`judul`,
				`tbl_praktikum_komponen_nilai`.`bobot`,
				`tbl_praktikum_komponen_nilai`.`parent_id`,
				`tbl_praktikum_komponen_nilai`.`kategori`, ";
		
		if($param2=='view-mk'){		
		$sql .="`tbl_praktikum`.`judul` as `namamateri`,
				`tbl_praktikum`.`nama_mk` as `namamk`,
				
                `tbl_praktikum`.`mkditawarkan_id`, ";
		}else{
		$sql .="`tbl_praktikum_materi`.`judul` as `namamateri`,
				`tbl_praktikum_materi`.`nama_mk` as `namamk`,
				
                `tbl_praktikum_materi`.`mkditawarkan_id`, ";	
		}
      
				
		if($param=='list'){
			$sql .= "  (SELECT k.bobot FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` as k WHERE k.komponen_id = tbl_praktikum_komponen_nilai.`parent_id` ) as bobot_parent,
					   (SELECT COUNT(*) FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` as k WHERE k.`parent_id` = `tbl_praktikum_komponen_nilai`.`komponen_id`) as countchild, 
					   CASE (SELECT COUNT(*) FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai` as k WHERE k.`parent_id` = `tbl_praktikum_komponen_nilai`.`komponen_id`) WHEN 0 THEN 1 ELSE 0 END as inforow,
					";
		}
		
		if($param2=='view-mk'){		
         $sql.="mid(md5(`tbl_praktikum`.`mkditawarkan_id`),9,7) as `mkid`

				FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai`
				LEFT JOIN `db_ptiik_apps`.`tbl_praktikum` ON `tbl_praktikum`.`jadwal_id` = `tbl_praktikum_komponen_nilai`.`jadwal_id`
				WHERE 1
			   ";
		}else{
		$sql.="mid(md5(`tbl_praktikum_materi`.`mkditawarkan_id`),9,7) as `mkid`

				FROM `db_ptiik_apps`.`tbl_praktikum_komponen_nilai`
				LEFT JOIN `db_ptiik_apps`.`tbl_praktikum_materi` ON `tbl_praktikum_materi`.`materi_id` = `tbl_praktikum_komponen_nilai`.`materi_id`
				WHERE 1
			   ";	
		}
		
		
		if($param=="parent"){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` = '0' ";
		}
		
		if($param=="child"){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` != '0' ";
		}
		
		if($thn){
			$sql .= " AND `tbl_praktikum_materi`.`tahun_akademik` = '".$thn."' ";
		}
		
		if($komponen){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` = '".$komponen."' ";
		}
		
		if($materiid){
			$sql .= " AND mid(md5(`tbl_praktikum_materi`.`materi_id`),9,7) = '".$materiid."' ";
		}
		
		if($materi_nilai){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`materi_id` = '".$materi_nilai."' ";
		}
		
		if($param2=='parent-kom'){
			 $sql .= " AND mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
		
		if($nilai_id){
			if($nilai_param=='md5'){
				$sql .= " AND mid(md5(`tbl_praktikum_komponen_nilai`.`nilai_id`),9,7) = '".$nilai_id."' ";
			}else{
				$sql .= " AND `tbl_praktikum_komponen_nilai`.`nilai_id` = '".$nilai_id."' ";
			}
			 
		}
		
		if($kategori!='akhir'){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`kategori` != 'akhir' ";
		}else{
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`kategori` = 'akhir' ";
		}
		
		if($id){
			if($param=="komponen"){
				 $sql .= " AND `tbl_praktikum_komponen_nilai`.`parent_id` = '0' ";
				 $sql .= " AND mid(md5(`tbl_praktikum_komponen_nilai`.`jadwal_id`),9,7) = '".$id."' ";
			}
			else{
				if($param2=='view-mk'){
					$sql .= " AND mid(md5(`tbl_praktikum`.`mkditawarkan_id`),9,7) = '".$id."' ";
				}else{
					$sql .= " AND mid(md5(`tbl_praktikum_materi`.`mkditawarkan_id`),9,7) = '".$id."' ";
				}
			}
			
			if($param2=='view-mk'){
				$sql .= " GROUP BY `tbl_praktikum_komponen_nilai`.`komponen_id` ";
			}
			
			$result = $this->db->query( $sql );
			// echo $sql;
			return $result;
		}
		
		if($param=='list'){
			$sql .= " AND `tbl_praktikum_komponen_nilai`.`jadwal_id` = '".$jadwalid."' ";
			
			if($param2=='get-by-param'){
				$sql .= " AND `tbl_praktikum_komponen_nilai`.`komponen_id` = '".$id2."' ";
			}
			
			$result = $this->db->query( $sql );
			// echo $sql;
			return $result;	 
		}
		
	}
	
	function get_nilaiid_byjadwal($jadwalid=NULL){
		$sql = "SELECT `tbl_praktikum_nilai`.`nilai_id`,
				mid(md5(`tbl_praktikum_nilai`.`nilai_id`),9,7) as `nilaiid`
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai` 
				WHERE 1 AND tbl_praktikum_nilai.kategori != 'akhir'
			   ";
		
		if($jadwalid){
			$sql .= " AND mid(md5(`tbl_praktikum_nilai`.`jadwal_id`),9,7) = '".$jadwalid."' ";
		}
				
		$result = $this->db->query( $sql );
		// echo $sql;
		return $result;	 
	}
	
	function get_nilai_proses($nilaiid=NULL,$mhsid=NULL,$param=NULL){
		$sql = "SELECT
				tbl_praktikum_nilai_mhs.nilai_id,
				tbl_praktikum_nilai_mhs.mahasiswa_id,
				tbl_praktikum_nilai.judul,
				tbl_mahasiswa.nama,
				
				SUM(CASE tbl_praktikum_nilai_mhs.parent_id
				  WHEN 0 THEN (tbl_praktikum_nilai_mhs.inf_bobot/100)*tbl_praktikum_nilai_mhs.nilai
				  ELSE ((tbl_praktikum_nilai_mhs.inf_bobot/100)*tbl_praktikum_nilai_mhs.nilai)*(tbl_praktikum_nilai_mhs.bobot_parent/100)
				END) as nilai_akhir
				
				FROM db_ptiik_apps.tbl_praktikum_nilai_mhs
				INNER JOIN db_ptiik_apps.tbl_mahasiswa ON db_ptiik_apps.tbl_praktikum_nilai_mhs.mahasiswa_id = db_ptiik_apps.tbl_mahasiswa.mahasiswa_id
				INNER JOIN db_ptiik_apps.tbl_praktikum_nilai ON tbl_praktikum_nilai.nilai_id = tbl_praktikum_nilai_mhs.nilai_id
				WHERE 1
			   ";
		
		if($param=='NA'){
			$sql .= " AND tbl_praktikum_nilai_mhs.nilai_id = '".$nilaiid."' ";
		}
		
		if($param=='NA-mhs'){
			$sql .= " AND mid(md5(tbl_praktikum_nilai_mhs.nilai_id),9,7) = '".$nilaiid."'
                      AND tbl_praktikum_nilai_mhs.mahasiswa_id = '".$mhsid."'
					";
		}
		
		$sql .= " AND tbl_praktikum_nilai_mhs.inf_kategori = 'proses'
				  GROUP BY tbl_praktikum_nilai_mhs.mahasiswa_id
				  ORDER BY tbl_mahasiswa.nama ASC 
			   ";
		// echo $sql."<br>";
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function read_nilai_inf($jadwal,$param){
		$sql = "SELECT mid(md5(tbl_praktikum_nilai.nilai_id),9,7) as nilai_id,
				tbl_praktikum_nilai.nilai_id as nilaiid
				FROM db_ptiik_apps.tbl_praktikum_nilai
				WHERE 1";
				
		if($jadwal!=""){
			$sql .= " AND mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwal."'";
		}
		
		if($param=="akhir"){
			$sql .= " AND tbl_praktikum_nilai.kategori = 'akhir' ";
		}
		
		$result = $this->db->query( $sql );
		return $result;
	}
	
	function check_NA($jadwalid){
		$sql = "SELECT tbl_praktikum_nilai.nilai_id
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai` 
				WHERE mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwalid."'
				AND tbl_praktikum_nilai.kategori = 'akhir'
				AND tbl_praktikum_nilai.judul = 'Nilai Akhir'
			   ";
		// echo $sql;tbl_praktikum_nilai.is_proses
		$dt = $this->db->getRow( $sql );
		if($dt){
			return $dt->nilai_id;
		}else{
			return '';
		}
	}
	
	function check_NA_proses($jadwalid){
		$sql = "SELECT tbl_praktikum_nilai.is_proses
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai` 
				WHERE mid(md5(tbl_praktikum_nilai.jadwal_id),9,7) = '".$jadwalid."'
				AND tbl_praktikum_nilai.kategori = 'akhir'
				AND tbl_praktikum_nilai.judul = 'Nilai Akhir'
			   ";
		
		$dt = $this->db->getRow( $sql );
		if($dt){
			return $dt->is_proses;
		}else{
			return '';
		}
	}
	
	function get_nilai_komp($komponenid,$nilaiid,$mhsid){
		$sql = "SELECT `tbl_praktikum_nilai_mhs`.`nilai`
				FROM `db_ptiik_apps`.`tbl_praktikum_nilai_mhs` 
				WHERE `tbl_praktikum_nilai_mhs`.`komponen_id` = '".$komponenid."'
				AND `tbl_praktikum_nilai_mhs`.`nilai_id` = '".$nilaiid."'
				AND `tbl_praktikum_nilai_mhs`.`mahasiswa_id` = '".$mhsid."'
			   ";
		// echo $sql;	   
		$dt = $this->db->getRow( $sql );
		// $strresult = $dt->nilaiid;
		return $dt;
	}
}
?>