<?php defined('PANADA') or die('Can\'t access directly!');
/**
 * EN: Panada Lite installation configuration
 */

$CONFIG['assets_folder'] = 'assets';

/**
 * Environment and error reporting
 * valid values:
 *    development, production
 */
$CONFIG['environment'] = 'development';

/**
 * Set this value to empty string instead of 'index.php' 
 * if you wish an url without "index.php" .
 * but don't forget to configure the .htaccess file if you change this
 */ 
$CONFIG['index_file']                       = ''; 

/**
 * EN: Database configuration.
 */
 
$CONFIG['db']['default']['driver']          = 'mysqli';
$CONFIG['db']['default']['host']            = '172.21.0.251'; 
$CONFIG['db']['default']['user']            = 'beta'; 
$CONFIG['db']['default']['password']        = 'b4s0ayam'; 
$CONFIG['db']['default']['database']        = 'db_coms';
$CONFIG['db']['default']['charset']         = 'utf8';
$CONFIG['db']['default']['collate']         = 'utf8_general_ci';
$CONFIG['db']['default']['persistent']      = false;

$CONFIG['db']['sms']['driver']          = 'mysqli';
$CONFIG['db']['sms']['host']            = '172.21.0.244'; 
$CONFIG['db']['sms']['user']            = 'beta'; 
$CONFIG['db']['sms']['password']        = 'betadev'; 
$CONFIG['db']['sms']['database']        = 'db_ptiik_gammu';
$CONFIG['db']['sms']['charset']         = 'utf8';
$CONFIG['db']['sms']['collate']         = 'utf8_general_ci';
$CONFIG['db']['sms']['persistent']      = false;

/**
 * Session configuration.
 */
$CONFIG['session']['expiration']        = 7200; /* 2 hour. */
$CONFIG['session']['name']              = 'PAN_SID';
$CONFIG['session']['cookie_expire']     = 0;
$CONFIG['session']['cookie_path']       = '/';
$CONFIG['session']['cookie_secure']     = false;
$CONFIG['session']['cookie_domain']     = '';
$CONFIG['session']['driver']            = 'native';
$CONFIG['session']['driver_connection'] = 'default'; /* Connection name for the driver. */
$CONFIG['session']['storage_name']      = 'sessions';

/**
 * regular expression in filtering method and controller names
 * comment out the following filter_regex configuration if you don't prefer filtering out 
 * controller and method name paresed from URI string
 */
$CONFIG['filter_regex'] = "/[^a-z0-9_]/i"; 

/** 
 * set the default controller and method if not set from the URL
 */
$CONFIG['default_controller'] = 'home';
$CONFIG['default_method'] = 'index';
/**
 * session secret_key
 */
$CONFIG['secret_key'] = "tH1s/is#4+seCREt/key''couLD bE\\anyThInG";

 if(!isset($_COOKIE['lang-switch'])){
	$CONFIG['lang']="en";
	$CONFIG['page_title'] = "Program of Information Technology and Computer Science (PTIIK)";
}
else{
	$CONFIG['lang']=$_COOKIE['lang-switch'];
	if($CONFIG['lang']=='en') $CONFIG['page_title'] = "Program of Information Technology and Computer Science (PTIIK)";
	else $CONFIG['page_title'] = "Program Teknologi Informasi dan Ilmu Komputer";
}
	
if(!isset($_COOKIE['skin-switch'])){
	$CONFIG['skin']="ub";
	$CONFIG['slider']="slider_standard";
}else{
	$CONFIG['skin']=$_COOKIE['skin-switch'];
	if($_COOKIE['skin-switch']!='ptiik'){
		$CONFIG['slider']="slider_standard";
	}else{
		$CONFIG['slider']="slider";
	}
}


$CONFIG['page_keywords'] ="FILKOM,UB,S2,MILKOM,PTIIKUB,riset,geoinformatika,geoinformatics,magister,master,ptiik,s2,learning,informatika,informatic,sistem,komputer,informasi,information,computer,systems,ilmu,science,ptiik,unibraw,kalender,akademik,academic,calender,of,informatics,jadwal,daftar,ulang,registration,ibm,gdp,ni,cisco,nokia,national,instrument,adins,oracle,kerjasama";
$CONFIG['fakultas']  = "PTIIK";
$CONFIG['cabang']  = "UBM";

$CONFIG['file_url_upload']  = 'http://localhost/beta/fileupload';
$CONFIG['file_url_view']   	= 'http://localhost/beta/fileupload/assets';
$CONFIG['default_pic']   	= 'http://localhost/beta/fileupload/assets/upload/user.jpg';
$CONFIG['default_thumb']   	= 'http://localhost/beta/fileupload/assets/upload/thumb.gif';
$CONFIG['default_thumb_web']= 'http://localhost/beta/fileupload/assets/upload/thumb.png';

$CONFIG['file_url']   = 'http://localhost/beta/fileupload/upload.php';
$CONFIG['file_url_old']   = 'http://localhost/beta/fileupload/old';


$CONFIG['default_email_acc']  = "bptik.ptiik@ub.ac.id";
$CONFIG['default_email'] 	  = "bptik.ptiik";
$CONFIG['default_email_pass'] = "bptik999";

$CONFIG['safe_upload']  	= false;

$CONFIG['assets_folder'] 	= APPLICATION . "assets";
$CONFIG['web_base_url'] 	= 'http://localhost/beta/';
$CONFIG['file_base_url'] 	= 'http://localhost/beta';

