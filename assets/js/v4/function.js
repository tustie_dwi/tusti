$('#navigation').affix({
    offset: {
        top: $('header').height() - $('#navigation').height()
    }
});

$('#second-navigation').affix({
    offset: {
        top: $('header').height() - $('#second-navigation').height()
    }
});
//      $('body').scrollspy({ target: '.navigation-home' })

$('.slider-video').bxSlider({
    adaptiveHeight: true,
    pager: false,
    //                controls: false,
    nextSelector: '.v-next',
    prevSelector: '.v-prev',
    nextText: '<span class="fa fa-chevron-right"></span>',
    prevText: '<span class="fa fa-chevron-left"></span>'
});

$('.media-comment .reply').click(function(e) {
    e.preventDefault();
    $url = $(this).data('url');
    $form = $('.form-comment');
    $form.remove();
    $('.comment-post').fadeIn();
    $form2 = '<form class="form form-reply form-comment" action="' + $url + '" method="post">' + $form.html() + '</form>';
    $(this).parent('.comment-action').parent('.media-body').append($form2);
});
$('.comment-post').click(function(e) {
    e.preventDefault();
    $url = $(this).data('url');
    $form = $('.form-comment');
    $form.remove();
    $(this).fadeOut();
    $form2 = '<form class="form form-comment" action="' + $url + '" method="post">' + $form.html() + '</form>';
    $(this).parent().append($form2);
});

$(".to-login").click(function(e) {
	e.preventDefault();
    $(".is-register").slideToggle();
    $(".is-login").slideToggle("slow");
    $("body").removeClass("register-page");

    $('body .left-panel').animate({
        'background-position-x': '0%'
    }, 500, 'linear');
    $("body").addClass("login-page");
});

$(".to-register").click(function(e) {
	e.preventDefault();
    $(".is-login").slideToggle();
    $(".is-register").slideToggle("slow");
    $("body").removeClass("login-page");

    $('body .left-panel').animate({
        'background-position-x': '100%'
    }, 500, 'linear');
    $("body").addClass("register-page");

});

function convert_month(number) {
    switch (number) {
        case 1:
            return 'Januari';
            break;
        case 2:
            return 'Februari';
            break;
        case 3:
            return 'Maret';
            break;
        case 4:
            return 'April';
            break;
        case 5:
            return 'Mei';
            break;
        case 6:
            return 'Juni';
            break;
        case 7:
            return 'Juli';
            break;
        case 8:
            return 'Augustus';
            break;
        case 9:
            return 'September';
            break;
        case 10:
            return 'Oktober';
            break;
        case 11:
            return 'November';
            break;
        case 12:
            return 'Desember';
            break;

    }
} 