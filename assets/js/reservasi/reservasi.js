$(document).on("change",".ruang-select", function(){
	var nilai = $(this).val();
	if(nilai == '') $("[row-ruang]").show();
	else{
		$("[row-ruang]").hide();
		$("[row-ruang='"+nilai+"']").show();
	}
});

function hide_order(){
	$("#book-wrap").hide();
}

function order_ruang(ruang, jam_mulai, jam_selesai){
	$("#book-wrap").show();
	var isi = ruang + "|" + jam_mulai + "|" + jam_selesai; 
	if($("[data-ruang='"+isi+"']").text() == ''){
		str  = '<a data-ruang="'+isi+'" class="btn-xs btn btn-primary">';
			str += '<i class="fa fa-bookmark"></i> '+ruang+' <i class="fa fa-clock-o"></i> '+jam_mulai+' - '+jam_selesai+' ';
			str += '<i class="fa fa-times del-ruang" data-ruang="'+isi+'" ></i>';
		str += '</a>';
		
		inp = '<input name="ruang[]" type="hidden" value="'+isi+'">';
		
		$("#ruang-field").append(str+inp);
	}
}

$(document).on("click",".del-ruang", function(){
	var nilai = $(this).data("ruang");
	$("[value='"+nilai+"']").remove();
	$("[data-ruang='"+nilai+"']").remove();
});

//detail reservasi
function detail_reservasi(jadwal){
	$(".list-reservasi a").attr("class","list-group-item");
	$(".list-reservasi a[dataid='"+jadwal+"']").attr("class","list-group-item active");
	var uri = base_url + 'module/master/reservasi/detail_reservasi';
	$("#loading").show();
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({id : jadwal}),
        success:function(msg) 
        {
        	$(".detail-reservasi").html(msg);
            $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	$("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
}

$(document).ready(function(){
	$(".list-reservasi a").hide();
	$(".list-reservasi a[page=1]").show();
})

function prev(){
	var cur_page = $("#cur_page").val();
	if(cur_page > 1) {
		var nilai = parseInt(cur_page) - 1;
		$(".list-reservasi a").hide();
		$(".list-reservasi a[page="+nilai+"]").show();
		$("#cur_page").val(nilai);
	}
}

function next(){
	var max_page = $("#max_page").val();
	var cur_page = $("#cur_page").val();
	
	if(cur_page < max_page) {
		var nilai = parseInt(cur_page) + 1;
		$(".list-reservasi a").hide();
		$(".list-reservasi a[page="+nilai+"]").show();
		$("#cur_page").val(nilai);
	}
}

//approve
function approve(jadwal_, init_){
	var uri = base_url + 'module/master/reservasi/approval';
	var nilai = 'onclick="approve(\''+jadwal_+'\',\''+init_+'\')"'; 
	$("#loading").show();
	$.ajax({
        url : uri,
        type: "POST",
        dataType : "HTML",
        data : $.param({jadwal : jadwal_, init : init_}),
        success:function(msg) 
        {
        	if(init_ == '1') {
        		var str = '<button onclick="approve(\''+jadwal_+'\',\'2\')" class="btn btn-danger btn-xs"><i class="fa fa-ban"></i> Tolak</button>';
        		$("#approval-wrap").html(str);
        	}
        	else if(init_ == '2'){
        		var str = '<button onclick="approve(\''+jadwal_+'\',\'1\')" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Setujui</button>';
        		$("#approval-wrap").html(str);
        	}
            $("#loading").fadeOut();
        },
        error: function(msg) 
        {
        	$("#loading").fadeOut();
			alert("Proses gagal, mohon ulangi lagi !!"); 
        }
    });
}

$(function () {
    $('.pick-date').datetimepicker({
        pickDate: true,
        pickTime: false
    });
});

$(function () {
    $('.pick-time').datetimepicker({
        pickDate: false
    });
});














