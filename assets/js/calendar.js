$(document).ready(function(){
	var el = $(".table-calendar tbody tr td").first();
  	var position = el.position();
  	
  	var startLeft = position.left;
  	var startTop  = position.top + 2;
  	var width = el.width() - 5;
  	
  	var obj = JSON.parse(eventJSON);
  	var w = width;
  	
  	$.each( obj, function( key, value ) {
		$.each(value, function(i, e){
			create_event(e.judul, e.panjang, startLeft, startTop, width);
		});
	});
  	
  	// $(".label-event").css({
  		// "top" 	: startTop, 
  		// "left" 	: startLeft, 
  		// "width" : w+"px"
  	// });
});


function create_event(judul, lama, startLeft, startTop, width){
	var label = document.createElement("label");
	var leftStart = startLeft + (width * (lama-1)) + 10;
	label.innerHTML=judul; 
	label.setAttribute("class","label label-info label-event");
	label.style.top = startTop+"px";
	label.style.left = leftStart+"px";
	label.style.width = width+"px";
	
	console.log(width*lama);
	document.getElementById("kegiatan-wrap").appendChild(label);
}
