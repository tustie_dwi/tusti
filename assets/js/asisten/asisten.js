$("#asisten_table_wrapper").hide();
$(document).ready(function(){
	$(".e9").select2();
	var unit 			= $("#unitid").val();
	var url_view 		= $("#url_view").val();
	var default_pic 	= $("#default_pic").val();
	var url_ 			= base_apps + 'unit/asisten_prak_json/'+unit;
	// alert(url_);
	var output 			= "";
	var table			= $('#asisten_table').DataTable();
	$.ajax({
        url : url_,
        type: "POST",
        dataType : "json",
        success:function(data) 
        {
        	if(data !== "failed"){
        		$("#asisten_table_wrapper").show();
        		$.each(data, function(i, f){
        			if(f.is_aktif=="1"){
        				var aktif = "<label class='label label-success'>Aktif</label>";
        			} else var aktif = "<label class='label label-warning'>Tidak Aktif</label>";
        		
	        		if(f.foto!==""){
	        			var pict = "<img width='10%' src='"+url_view+"/"+f.foto+"'>";
	        		}else var pict = "<img width='10%' src='"+f.default_pic+"'>";
	        		
	        		table.row.add([
	        			pict+" <b>"+f.namamhs+"</b> "+aktif,
	        			f.nim,
	        			f.mk
	        		]).draw();
				});
        	}
        	else{
        		$("#asisten_table_wrapper").hide();
	    		$(".no_data").show();
	    		setTimeout(function(){
	    			$(".no_data").fadeOut();
	    		},1000);
        	}
        }
    });
    
    $("#pilih_thn").change(function(){
    	
    	var thn = $(this).val();
    	
    	if(thn!=="0"){
    		var url_ 			= base_apps + 'unit/asisten_prak_json/'+unit+'/'+thn;
    		table.clear().draw();
    		$.ajax({
		        url : url_,
		        type: "POST",
		        dataType : "html",
		        success:function(data) 
		        {
		        	//alert(data);
		        	if(data !== "failed"){
		        		$("#asisten_table_wrapper").show();
		        		$.each(data, function(i, f){
		        			if(f.is_aktif=="1"){
		        				var aktif = "<label class='label label-success'>Aktif</label>";
		        			} else var aktif = "<label class='label label-warning'>Tidak Aktif</label>";
		        		
			        		if(f.foto!==""){
			        			var pict = "<img width='10%' src='"+url_view+"/"+f.foto+"'>";
			        		}else var pict = "<img width='10%' src='"+url_view+"/"+f.default_pic+"'>";
			        		
			        		table.row.add([
			        			pict+" <b>"+f.namamhs+"</b> "+aktif,
			        			f.nim,
			        			f.mk
			        		]).draw();
						});
		        	}
		        	else{
			    		$("#asisten_table_wrapper").hide();
			    		$(".no_data").show();
			    		setTimeout(function(){
			    			$(".no_data").fadeOut();
			    		},1000);
			    	}
		        }
		    });
    	}
    	else{
    		$("#asisten_table_wrapper").hide();
    		$(".no_data").show();
    		setTimeout(function(){
    			$(".no_data").fadeOut();
    		},1000);
    	}
    })
    
});