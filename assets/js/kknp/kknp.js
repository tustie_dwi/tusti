$(document).ready(function() {
	jQuery("#mhs_kknp").tagsManager();
	
	$('#tgl_mulai, #tgl_selesai').datetimepicker({
	  pickTime : false,
	  format : 'YYYY-MM-DD'
	});
		
	$.ajax({
		type : "POST",
		dataType: "JSON",
		url: base_url + "info/mhs",
		success: function(data){
			// alert(data);
			$('#mhs_kknp').autocomplete(
			{
				source: data,
				minLength: 0,
				select: function(event, ui) { 
					$('#mhs_kknp').val(ui.item.value);
				}    
			});
		}
	});
	
	$.ajax({
		type : "POST",
		dataType: "json",
		url: base_url + "info/company",
		success: function(data){
			// alert(data);
			$('#perusahaan_name').autocomplete(
			{
				source: data,
				minLength: 0,
				select: function(event, ui) { 
					$('#perusahaan_name').val(ui.item.value);
					$('#perusahaan_alamat').val(ui.item.alamat);
				}    
			});
		}
	});
	
});

$('#submit-submission').click(function(e) {
	var p_name 		  = $('#perusahaan_name').val();
	var p_address	  = $('#perusahaan_alamat').val();
	var tgl_mulai	  = $('#tgl_mulai').val();
	var tgl_selesai	  = $('#tgl_selesai').val();
	var mhs	  		  = $('[name="hidden-mhs_kknp"]').val();
	
	if(p_name.length!=0 && p_address.length!=0 && tgl_mulai.length!=0 && tgl_selesai.length!=0 && mhs.length!=0 ){
		submit_submission('pengajuan',e);
	}else{
		alert('Lengkapi data anda!');
	}
});

function submit_submission(status, e){
	var postData = new FormData($('#form-daftar-ulang')[0]);
	postData.append("status", status);
	$.ajax({
		url : base_url + "info/save_kknp",
		type: "POST",
		data : postData,
		async: false,
		success:function(data, textStatus, jqXHR) 
		{
			// alert(data);
			if(data=='Proses Simpan Berhasil!'){
				alert(data);
				location.reload();
			}else{
				alert(data);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) 
		{
		    alert ('Proses Simpan Gagal!');      
	    },
	    cache: false,
		contentType: false,
		processData: false
	  });
	e.preventDefault(); //STOP default action
	return false;
}
