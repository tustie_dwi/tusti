var konten_ujian_ujian;
var init = 'hari';
var warna = ["#16a085","#2980b9","#d35400","#c0392b","#27ae60","#8e44ad","#f39c12","#2c3e50"];
$(document).ready(function(){
	var url_ = base_url + 'info/jadwal_ujian';
	
	$.ajax({
        url : url_,
        type: "POST",
        dataType : "json",
        success:function(msg) 
        {
			// console.log(msg);
        	get_hari_start(msg);
        }
    });
    
    // console.log(jam);
})	

function jadwal_tipe(tipe){
	$("#tipe-judul").html(tipe);
	
	var hari_ini = $("#hari_ini").val() + "|" + $("#hari_eng").val();
	jadwal_hari(hari_ini,'-');
}

function set_range(hari){
	var str = '';
	var hari_ini = $("#hari_ini").val() + "|" + $("#hari_eng").val();
	
	if(range[hari] !== undefined){
		$.each(range[hari], function(key, val){
			if(str == '') var aktif = 'active';
			else aktif = '';
			
			str += '<li onclick="jadwal_hari(\''+hari_ini+'\',\''+val+'\')" class="'+aktif+ ' ' +val+'"><a href="#" role="tab" data-toggle="tab"><b>'+val+'</b></a></li>';
		});
	}
	$("#myTab").html(str);
}

function get_hari_start(msg){
	konten_ujian = msg;
	var hari_ini = $("#hari_ini").val();
	var jml_jam = $("#jml_jam").val();
	var jenis = $("#pilih_jenis").val();
	
	set_range(hari_ini);
	
	$("#jadwal-wrap").html('');
	$.each(konten_ujian, function(hari, a){
		if(hari == hari_ini){
			$.each(a, function(ruang, jadwal_detail){
				var i =0;	
				var k = 0;
				var walk = 1;
				var str = '<tr>';
					str += '<td colspan="2">'+ruang+'</td>';
					
					$.each(jadwal_detail, function(id, jadwal){
						
						if(jadwal.tgl == range[hari_ini][0] && jadwal.jenis.toUpperCase() == jenis) {
							switch(jadwal.prodi){
								case 'SISKOM':
									strclass = 'label-success';
								break;
								case 'ILKOM':
									strclass = 'label-info';
								break;
								case 'SI':
									strclass = 'label-warning';
								break;
								case '2014120001':
									strclass = 'label-milkom';
								break;
								case 'MILKOM':
								strclass = 'label-milkom';
							break;
							}
							
							var colspan = set_colspan(jadwal.urut,jadwal.jam_selesai);
												
							k = k + (colspan);
							
							for(j=walk; j < parseInt(jadwal.urut); j++) {
								
								str += '<td></td>';
							}					
							if(jadwal.lang='IN') {strkelas = "Kelas";}
							else{ strkelas = "Class";}
							
							var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> '+ strkelas + ' - ' + jadwal.kelas;
							
							if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
							str += '<td tgl="'+jadwal.tgl+'" class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
							walk = parseInt(jadwal.urut) + parseInt(colspan) + 1;
														
							i+=parseInt(colspan-1);
							skip = true;
						}
					});			
					
					if(walk <= parseInt(jml_jam)){
						for(i=0; i<= parseInt(jml_jam - walk); i++) str += '<td></td>';
					}
					
				str += '</tr>';
				$("#jadwal-wrap").append(str);
			});
		}
    });
}

function jadwal_hari(hari_data, tgl){
	var tmp = hari_data.split("|");
	var hari_ini  = tmp[0];
	hari_ini = hari_ini.toLowerCase();
	var jml_jam = $("#jml_jam").val();
	var jenis = $("#pilih_jenis").val();
	
	$("#hari_ini").val(hari_ini);
	$("#hari_eng").val(tmp[1]);
	
	if(tgl == '-' && range[hari_ini] !== undefined) tgl = range[hari_ini][0];
	
	set_range(hari_ini);
	$('#myTab .'+tgl).tab('show')
	
	$("#sub-header").html(": <i class='fa fa-calendar'></i> "+tmp[1]);
	
	$("#jadwal-wrap").html('');
	$.each(konten_ujian, function(hari, a){
		if(hari == hari_ini){
			$.each(a, function(ruang, jadwal_detail){
				var i =0;	
				var k = 0;
				var walk = 1;
				var str = '<tr>';
					str += '<td colspan="2">'+ruang+'</td>';
					
					$.each(jadwal_detail, function(id, jadwal){
						if(jadwal.tgl == tgl  && jadwal.jenis.toUpperCase() == jenis) {
							switch(jadwal.prodi){
								case 'SISKOM':
									strclass = 'label-success';
								break;
								case 'ILKOM':
									strclass = 'label-info';
								break;
								case 'SI':
									strclass = 'label-warning';
								break;
								case '2014120001':
									strclass = 'label-milkom';
								break;
								case 'MILKOM':
								strclass = 'label-milkom';
							break;
							}
							
							var colspan = set_colspan(jadwal.urut,jadwal.jam_selesai);
												
							k = k + (colspan);
							
							for(j=walk; j < parseInt(jadwal.urut); j++) {
								
								str += '<td></td>';
							}					
							if(jadwal.lang='IN') {strkelas = "Kelas";}
							else{ strkelas = "Class";}
							
							var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> '+ strkelas + ' - ' + jadwal.kelas;
							
							if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
							str += '<td tgl="'+jadwal.tgl+'" class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
							walk = parseInt(jadwal.urut) + parseInt(colspan) + 1;
														
							i+=parseInt(colspan-1);
							skip = true;
						}
					});			
					
					if(walk <= parseInt(jml_jam)){
						for(i=0; i<= parseInt(jml_jam - walk); i++) str += '<td></td>';
					}
				str += '</tr>';
				$("#jadwal-wrap").append(str);
			});
		}
    });
}


function set_colspan(urut, jam_selesai){
	//console.log(jam[jam_selesai] + ' ' + urut);
	return ( parseInt(jam[jam_selesai]) - parseInt(urut) );
	//return ( parseInt(jam[jam_selesai]) - parseInt(urut) );
}




