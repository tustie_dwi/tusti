var konten;
var warna = ["#16a085","#2980b9","#d35400","#c0392b","#27ae60","#8e44ad","#f39c12","#2c3e50"];
$(document).ready(function(){
	var unit = $("#unitid").val();
	
	//var url_ = base_url + 'info/jadwal_prak_json/'+unit;
	//var url_ = base_url + '/jadwal_prak_json/'+unit;
	var url_ = 'http://ptiik.ub.ac.id/info/jadwal_prak_json/'+unit;
	//alert(url_);
	$.ajax({
        url : url_,
        type: "POST",
        dataType : "json",
        success:function(msg) 
        {
			//alert(msg);
        	konten = msg;
        	get_hari_start(konten);
        }
    });
    
   // console.log(warna);
})

function pilih_tipe(tipe){
	$(".pilih-form").hide();
	$("#pilih_"+tipe).fadeIn();
}

function get_hari_start(konten){
	var hari_ini = $("#hari_ini").val();
	var jml_jam = $("#jml_jam").val();
	
	//alert(jml_jam);
	
	$("#jadwal-wrap").html('');
	$.each(konten, function(hari, a){
		if(hari == hari_ini){
			$.each(a, function(ruang, jadwal_detail){
				var i =0;	
				var k = 0;
				var walk = 1;
				var str = '<tr>';
					str += '<td colspan="2">'+ruang+'</td>';
					
					$.each(jadwal_detail, function(id, jadwal){
						switch(jadwal.prodi){
							case 'SISKOM':
								strclass = 'label-success';
							break;
							case 'ILKOM':
								strclass = 'label-info';
							break;
							case 'SI':
								strclass = 'label-warning';
							break;
						}
						
						var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
											
						k = k + (colspan);
						
						for(j=walk; j< parseInt(jadwal.urut); j++) {
							
							str += '<td></td>';
							k = k + 1;
						}					
						if(jadwal.lang='IN') {strkelas = "Kelas";}
						else{ strkelas = "Class";}
						
						var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> '+ jadwal.str_kelas ;
						
						if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
						str += '<td class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
						walk = parseInt(jadwal.urut) + parseInt(colspan);
													
						i+=parseInt(colspan-1);
						skip = true;
						//break;
						
					});			
					
					if(k < parseInt(jml_jam)){
							for(i=1; i<= parseInt(jml_jam - k); i++) str += '<td></td>';
						}
					/*$.each(jadwal_detail, function(id, jadwal){
						for(i=walk; i< parseInt(jadwal.urut); i++) {
							str += '<td></td>';
						}
						
						var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
						
						if(colspan < 2){
							colspan = colspan +1;
						}else{
							colspan = colspan;
						}
						
						var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> Kelas - '+jadwal.kelas;
						
						if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
						str += '<td class="text-center" style="background: #2FA0CC; color: white" colspan="'+colspan+'">'+jadwal.urut+''+isi+'</td>';
						walk = parseInt(jadwal.urut) + parseInt(colspan);
						//$walk = i;
						
						i=parseInt(colspan-1);
						skip = true;
					});
						//for(i=walk; i<= parseInt(jml_jam); i++) str += '<td></td>';
					if(walk < parseInt(jml_jam)){
						for(i=0; i<= parseInt(jml_jam - walk); i++) str += '<td>'+i+ '' +walk+'</td>';
					}*/
					
				str += '</tr>';
				$("#jadwal-wrap").append(str);
			});
		}
    });
}

function jadwal_hari(hari_ini){
	hari_ini = hari_ini.toLowerCase();
	var jml_jam = $("#jml_jam").val();
	
	$("#jadwal-wrap").html('');
	$.each(konten, function(hari, a){
		if(hari == hari_ini){
			$.each(a, function(ruang, jadwal_detail){
				var i =0;	
				var k = 0;
				var walk = 1;
				var str = '<tr>';
					str += '<td colspan="2">'+ruang+'</td>';
					
					$.each(jadwal_detail, function(id, jadwal){
						switch(jadwal.prodi){
							case 'SISKOM':
								strclass = 'label-success';
							break;
							case 'ILKOM':
								strclass = 'label-info';
							break;
							case 'SI':
								strclass = 'label-warning';
							break;
						}
						
						var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
											
						k = k + (colspan);
						
						for(j=walk; j< parseInt(jadwal.urut); j++) {
							
							str += '<td></td>';
							k = k + 1;
						}		
						
						
						var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br>'+jadwal.str_kelas;
						
						if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
						str += '<td class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
						walk = parseInt(jadwal.urut) + parseInt(colspan);
													
						i+=parseInt(colspan-1);
						skip = true;
						//break;
						
					});			
					
					if(k < parseInt(jml_jam)){
							for(i=1; i<= parseInt(jml_jam - k); i++) str += '<td></td>';
					}
				/*var skip = false;
				var walk = 1;
				var str = '<tr>';
					str += '<td colspan="2">'+ruang+'</td>';
					$.each(jadwal_detail, function(id, jadwal){
						for(i=walk; i< parseInt(jadwal.urut); i++) {
							str += '<td></td>';
						}
						
						var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
						
						if(colspan < 2){
							colspan = colspan +1;
						}else{
							colspan = colspan;
						}
													
						var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> Kelas - '+jadwal.kelas;
						if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
						str += '<td class="text-center" style="background: #2FA0CC; color: white" colspan="'+colspan+'">'+isi+'</td>';
						walk = parseInt(jadwal.urut) + parseInt(colspan);
						
						i+=parseInt(colspan-1);
						skip = true;
						//break;
					});
					//if(!skip) str += '<td>d</td>';
					//if(hari)  str += '<td>'+walk+'</td>';
					//for(i=walk; i<= parseInt(jml_jam); i++) str += '<td>d</td>';
					if(walk < parseInt(jml_jam)){
						for(i=0; i<= parseInt(jml_jam - walk); i++) str += '<td></td>';
					}*/
				str += '</tr>';
				$("#jadwal-wrap").append(str);
			});
		}
    });
}

function jadwal_ruang(ruang_ini){
	var jml_jam = $("#jml_jam").val();
	$("#jadwal-wrap").html('');
	$.each(konten, function(hari, a){
		$.each(a, function(ruang, jadwal_detail){
			if(ruang == ruang_ini){
				/*var walk = 1;
				var str = '<tr>';
					str += '<td>'+hari+'</td>';
					str += '<td>'+ruang+'</td>';
					$.each(jadwal_detail, function(id, jadwal){
						for(i=walk; i< parseInt(jadwal.urut); i++) {
							str += '<td></td>';
						}
						
						var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
						var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> Kelas - '+jadwal.kelas;
						if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
						str += '<td class="text-center" style="background: #2FA0CC; color: white" colspan="'+colspan+'">'+isi+'</td>';
						walk = parseInt(jadwal.urut) + parseInt(colspan);
					});
					
					for(i=walk; i<= parseInt(jml_jam); i++) str += '<td></td>';*/
					var i =0;	
					var k = 0;
					var walk = 1;
					var str = '<tr>';
					str += '<td>'+hari+'</td>';
					str += '<td>'+ruang+'</td>';
					
					$.each(jadwal_detail, function(id, jadwal){
						
						switch(jadwal.prodi){
							case 'SISKOM':
								strclass = 'label-success';
							break;
							case 'ILKOM':
								strclass = 'label-info';
							break;
							case 'SI':
								strclass = 'label-warning';
							break;
						}
						
						var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
											
						k = k + (colspan);
						
						for(j=walk; j< parseInt(jadwal.urut); j++) {
							
							str += '<td></td>';
							k = k + 1;
						}							
						
						var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> '+jadwal.str_kelas;
						
						if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
						str += '<td class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
						walk = parseInt(jadwal.urut) + parseInt(colspan);
													
						i+=parseInt(colspan-1);
						skip = true;
						//break;
						
					});			
					
					if(k < parseInt(jml_jam)){
							for(i=1; i<= parseInt(jml_jam - k); i++) str += '<td></td>';
					}
				str += '</tr>';
				$("#jadwal-wrap").append(str);
			}
		});
    });
}

function jadwal_dosen(dosen_ini){
	$("#jadwal-wrap").html('');
	$.each(konten, function(hari, a){
		var count = 0;
		$.each(a, function(ruang, jadwal_detail){
			var dosen_cocok = cari_jadwal_dosen(jadwal_detail, dosen_ini);
			if(dosen_cocok != ''){
				var str = '<tr>';
					if(count == 0) str += '<td hari="'+hari+'">'+hari+'</td>';
					str += '<td>'+ruang+'</td>';
					
					str += dosen_cocok;
				str += '</tr>';
				$("#jadwal-wrap").append(str);
				count++;
			}
		});
		
		$("[hari='"+hari+"']").attr("rowspan",count);
    });
}

function cari_jadwal_dosen(jadwal_detail, dosen_ini){
	var walk = 1;
	var str = '';
	var ada = 0;
	var jml_jam = $("#jml_jam").val();
	$.each(jadwal_detail, function(id, jadwal){
		for(i=walk; i< parseInt(jadwal.urut); i++) {
			str += '<td></td>';
		}
		
		if(dosen_ini == jadwal.karyawan_id) {
			switch(jadwal.prodi){
				case 'SISKOM':
					strclass = 'label-success';
				break;
				case 'ILKOM':
					strclass = 'label-info';
				break;
				case 'SI':
					strclass = 'label-warning';
				break;
			}
						
			ada = 1;
			var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
			var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> '+jadwal.str_kelas;
			if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
			str += '<td class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
			walk = parseInt(jadwal.urut) + parseInt(colspan);
		}
		else{
			walk = parseInt(jadwal.urut);
		}
	});
	
	for(i=walk; i<= parseInt(jml_jam); i++) str += '<td></td>';
	
	if(ada == 1) return str;
	else return '';
	
}

function jadwal_mk(mk_ini){
	$("#jadwal-wrap").html('');
	$.each(konten, function(hari, a){
		var count = 0;
		$.each(a, function(ruang, jadwal_detail){
			var mk_cocok = cari_jadwal_mk(jadwal_detail, mk_ini);
			if(mk_cocok != ''){
				var str = '<tr>';
					if(count == 0) str += '<td hari="'+hari+'">'+hari+'</td>';
					str += '<td>'+ruang+'</td>';
					
					str += mk_cocok;
				str += '</tr>';
				$("#jadwal-wrap").append(str);
				count++;
			}
		});
		
		$("[hari='"+hari+"']").attr("rowspan",count);
    });
}

function cari_jadwal_mk(jadwal_detail, mk_ini){
	var walk = 1;
	var str = '';
	var ada = 0;
	var jml_jam = $("#jml_jam").val();
	$.each(jadwal_detail, function(id, jadwal){
		for(i=walk; i< parseInt(jadwal.urut); i++) {
			str += '<td></td>';
		}
		
		if(mk_ini == jadwal.mk_id) {
			switch(jadwal.prodi){
				case 'SISKOM':
					strclass = 'label-success';
				break;
				case 'ILKOM':
					strclass = 'label-info';
				break;
				case 'SI':
					strclass = 'label-warning';
				break;
			}
			
			ada = 1;
			var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
			var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> '+jadwal.str_kelas;
			if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
			str += '<td class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
			walk = parseInt(jadwal.urut) + parseInt(colspan);
		}
		else{
			walk = parseInt(jadwal.urut);
		}
	});
	
	for(i=walk; i<= parseInt(jml_jam); i++) str += '<td></td>';
	
	if(ada == 1) return str;
	else return '';
	
}

function jadwal_praktikum(praktikum_ini){
	$("#jadwal-wrap").html('');
	$.each(konten, function(hari, a){
		var count = 0;
		$.each(a, function(ruang, jadwal_detail){
			var mk_cocok = cari_jadwal_praktikum(jadwal_detail, praktikum_ini);
			if(mk_cocok != ''){
				var str = '<tr>';
					if(count == 0) str += '<td hari="'+hari+'">'+hari+'</td>';
					str += '<td>'+ruang+'</td>';
					
					str += mk_cocok;
				str += '</tr>';
				$("#jadwal-wrap").append(str);
				count++;
			}
		});
		
		$("[hari='"+hari+"']").attr("rowspan",count);
    });
}

function cari_jadwal_praktikum(jadwal_detail, mk_ini){
	var walk = 1;
	var str = '';
	var ada = 0;
	var jml_jam = $("#jml_jam").val();
	$.each(jadwal_detail, function(id, jadwal){
		for(i=walk; i< parseInt(jadwal.urut); i++) {
			str += '<td></td>';
		}
		
		if(mk_ini == jadwal.mk_id && jadwal.praktikum == '1') {
			switch(jadwal.prodi){
				case 'SISKOM':
					strclass = 'label-success';
				break;
				case 'ILKOM':
					strclass = 'label-info';
				break;
				case 'SI':
					strclass = 'label-warning';
				break;
			}
			
			ada = 1;
			var colspan = set_colspan(jadwal.urut,jadwal.urut_selesai);
			var isi = '<strong>'+jadwal.matakuliah+'</strong><br>'+jadwal.dosen+'<br> '+jadwal.str_kelas;
			if(jadwal.praktikum == "1") isi += '<br><code>praktikum</code>';
			str += '<td class='+strclass+' style="text-align: center; color: white" colspan="'+colspan+'">'+isi+'</td>';
			walk = parseInt(jadwal.urut) + parseInt(colspan);
		}
		else{
			walk = parseInt(jadwal.urut);
		}
	});
	
	for(i=walk; i<= parseInt(jml_jam); i++) str += '<td></td>';
	
	if(ada == 1) return str;
	else return '';
	
}


function set_colspan(urut, jam_selesai){
	console.log(urut + ' s/d ' +jam_selesai + ' ' + jam_selesai);
	return ( parseInt(jam_selesai) - parseInt(urut) );
	//return ( parseInt(jam[jam_selesai]) - parseInt(urut) );
}




