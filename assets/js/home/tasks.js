var edit = false;
var elEdit = null;
$(document).ready(function(){
	$("#form_date").datepicker({dateFormat: 'yy-mm-dd'});
	$("#button_newtask").click(function(){
		$('#btn-cancel-task').css({"display":"none"});
		cancel_submit_tasks();
		$("#display-newtask").attr("style","display:block;");
		$("#display-tasks").attr("style","display:none;");
		$("#button_tasklist").show();
		$(this).hide();
	});
	$("#button_tasklist").click(function(){
		$("#display-newtask").attr("style","display:none;");
		$("#display-tasks").attr("style","display:block;");
		$("#button_newtask").show();
		$(this).hide();
	});
	$('.timepicker1').timepicker({
		defaultTime: false
	});
	$('.timepicker').datetimepicker({
        format: 'HH:mm:ss',
        autoclose: true,
        showMeridian: true,
        startView: 1,
        maxView: 1,
        pickDate: false
    });
});
function cancel_submit_tasks(){
	edit = false;
	$('#form-newtask :input[required="required"]').each(function(){
	    $(this).parent().removeClass('has-success');
	    $(this).parent().removeClass('has-error');
	});
	$("#form-newtask")[0].reset();
	$('#hidId').val("");
	$("#display-newtask").attr("style","display:none;");
	$("#display-tasks").attr("style","display:block;");
}
function submit_tasks(){
	var cekvalid = true;
	$('#form-newtask :input[required="required"]').each(function(){
		    $(this).parent().addClass('has-success');
		    $(this).parent().removeClass('has-error');
		    if(!this.validity.valid)
		    {
		        $(this).focus();
		        $(this).parent().addClass('has-error');
		        cekvalid = false;
		    }else{
		    	$(this).parent().addClass('has-success');
		    }
		});
	if(cekvalid){
		$.ajax({
	    	url : base_url + 'apps/savetaskToDB',
		    type: "POST",
		    data : $("#form-newtask").serialize(),
		    success:function(data, textStatus, jqXHR){
		    	if(data){
		    		var parse = JSON.parse(data);
			    	var template = "<li class='list-group-item' id='user-"+parse.aktifitas_id+"' data-id='"+parse.aktifitas_id+"' "+(parse.is_finish==1? "style='text-decoration:line-through'" : "style='text-decoration:none'")+">"+
			    		"<input type='hidden' class='label-tasks' value='"+parse.is_finish+"'>"+
			    		"<input type='checkbox' onclick=\"finish_tasks('"+parse.aktifitas_id+"',this);\" id='finish"+parse.aktifitas_id+"' class='btn-toggle-task' data-uid='"+parse.aktifitas_id+"' value='1' "+
			    		(parse.is_finish==1 ? "checked" : "") +"> <span class='label-task'>"+parse.judul+"</span><br>"+
			    		"<small><span class='text text-danger'><i class='fa fa-clock-o'></i> "+ parse.jam_mulai.substr(0,5)+" - "+ parse.jam_selesai.substr(0,5)+ "</span>"+
			    		(parse.catatan ? "&nbsp;<i class='fa fa-file-text-o'></i> "+parse.catatan : "" )+"</small>" +
			    		"<button type='button' class='btn btn-danger btn-xs pull-right' onclick='doDeleteTask(\""+parse.aktifitas_id+"\",this)' href='#'>"+
						  "<i class='fa fa-trash-o'></i> Delete"+
						"</button>"+ 
						"<button type='button' class='btn btn-warning btn-xs pull-right' data-task='' onclick='doEditTask(\""+Base64.encode(data)+"\",this)' href='#'>"+
						  "<i class='fa fa-trash-o'></i> Edit"+
						"</button>"+
						"</li>";
					if(!edit){
						$('#list-task').append(template);
					}else{
						$(elEdit).parent().before(template);
						$(elEdit).parent().remove();
					}
				}
				cancel_submit_tasks();
		    }
		});
	}
};

function finish_tasks(id,element){
	var check = $("#finish"+id).is(':checked');
	var finish;
	if(check == true){
		finish = 1;
	}
	else finish = 0;
	
	var URL = base_url + 'apps/finish_task';
      $.ajax({
        url : URL,
        type: "POST",
        dataType : "html",
        data : $.param({
				task_id : id,
        		finish : finish
		}),
        success:function(msg) 
        {
        	if(check == true) $(element).parent().find('.label-task').css({ "text-decoration":"line-through" });
        	else $(element).parent().find('.label-task').css({ "text-decoration":"none" });
        }
   	 });
};

function doDeleteTask(i,element) {
  	var x = confirm("Are you sure you want to delete?");
 	if (x){
  		var del_id	= i;
  		var url		= base_url + 'apps/delete_task';
	  	$.ajax({
	  		type : "POST",
	  		dataType : "html",
	  		url : url,
			data : $.param({delete_id : del_id}),
			success : function(msg) {
				$(element).parent().remove();
			}
		});
	}
  else {
  }
};
function doEditTask(param, element){
	task = JSON.parse(Base64.decode(param));
	edit = true;
	elEdit = element;
	console.log(task);
	$('#form_date').val(task.tgl);
	//var jam = task.jam_mulai.split(/[- :]/); 
	//$('#jam_mulai').val((jam[0]<10?jam[0].substr(1):jam[0]<12?jam[0]:jam[0]-12)+":"+jam[1]+" "+(jam[0]<12?"AM":"PM"));
	//var jam = task.jam_selesai.split(/[- :]/); 
	//$('#jam_selesai').val((jam[0]<10?jam[0].substr(1):jam[0]<12?jam[0]:jam[0]-12)+":"+jam[1]+" "+(jam[0]<12?"AM":"PM"));
	$('#jam_mulai').val(task.jam_mulai);
	$('#jam_selesai').val(task.jam_selesai);
	$('#judulTask').val(task.judul);
	$('#catatan').val(task.catatan);
	$('#hidId').val(task.aktifitas_id);
	$("#display-newtask").attr("style","display:block;");
	$("#display-tasks").attr("style","display:none;");
	$('#btn-cancel-task').css({"display":"inline-block"});
}
