$('.textarea-timeline').not('.textarea-status').focusin(function(){
	var $data=$(this).data('formtimeline');
	//$('#ckeditor').ckeditor();
	$('.btn-form-timeline[data-formtimeline='+$data+']').fadeIn();
	});
$('.textarea-timeline').not('.textarea-status').focusout(function(){
	var $data=$(this).data('formtimeline');
	//$('#ckeditor').ckeditor();
	var text=$(this).val();
	if(text.length<=0)
	$('.btn-form-timeline[data-formtimeline='+$data+']').fadeOut();
});

$('.textarea-status').focusin(function(){
	var $data=$(this).data('formtimeline');
	$('.textarea-status').ckeditor();
	$('.btn-form-status[data-formtimeline='+$data+']').show();
	});
	
$('.textarea-status').focusout(function(){
	var $data=$(this).data('formtimeline');
	var text=$(this).val();
	var status=$('.btn-select-strata').data('click');
	if(text.length<=0 && status==false)
	$('.btn-form-status[data-formtimeline='+$data+']').fadeIn();
});

$('.btn-select-strata').click(function(e) {
	e.preventDefault();
  $(this).data('click', 'true');
  $('.btn-form-status').fadeIn();
});

$('.btn-cancel').click(function(e) {
	var editor = CKEDITOR.instances['autogrow']; if(editor) { editor.destroy(true); } 
	 $('.btn-form-status').fadeOut();
});

$('.btn-cancel-timeline').click(function(e) {
	//var editor = CKEDITOR.instances['ckeditor']; if(editor) { editor.destroy(true); } 
	 $('.form-comment').fadeOut();
});


function show_all_comment($data){
	$('*[data-showit='+$data+']').fadeIn();
	$('*[data-showall='+$data+']').fadeOut();	
}

$("#pilih-tipe").change(function(){
	var tipe = $(this).val();
	if(tipe == 'jadwal'){
		$("#share-jadwal").show();
		$("#share-jadwal").removeAttr("disabled");
		$("#share-mk").hide();
		$("#share-mk").attr("disabled", "disabled");
	}else if(tipe == 'mk'){
		$("#share-jadwal").hide();
		$("#share-jadwal").attr("disabled", "disabled");
		$("#share-mk").show();
		$("#share-mk").removeAttr("disabled");
	}
	else{
		$("#share-jadwal").hide();
		$("#share-jadwal").attr("disabled", "disabled");
		$("#share-mk").hide();
		$("#share-mk").attr("disabled", "disabled");
	}
});

$('.pilih-tipe a').click(function(e){
	e.preventDefault();	
	var $data=$(this).data('value');
	$('.input-strata').val($data);
	var $html=$(this).html()+' <span class="caret"></span>';
	$('.btn-select-strata').html($html);
	if($data == 'jadwal'){
		$("#share-jadwal").show();
		$("#share-jadwal").removeAttr("disabled");
		$("#share-mk").hide();
		$("#share-mk").attr("disabled", "disabled");
	}else if($data == 'mk'){
		$("#share-jadwal").hide();
		$("#share-jadwal").attr("disabled", "disabled");
		$("#share-mk").show();
		$("#share-mk").removeAttr("disabled");
	}
	else{
		$("#share-jadwal").hide();
		$("#share-jadwal").attr("disabled", "disabled");
		$("#share-mk").hide();
		$("#share-mk").attr("disabled", "disabled");
	}
});


function delete_comment_status(uri,id,type){
var uri_ = uri;
	var id_ = id;
	$.ajax({
		type	: 'POST',
		dataType : 'html',
		url		: uri_,
		success : function(msg){
			if(type=="status"){
			$(".status-wrap[data-id='"+id_+"']").fadeOut();
			}else if(type=="comment"){
			$(".comment-wrap[data-id='"+id_+"']").fadeOut();
			}
		}
	});
}

	function reply_click(id){
		$('.media.status-wrap form').hide('slow');
		$('.media.status-wrap[data-id="'+id+'"] form').show('slow');
		$('.media.status-wrap[data-id="'+id+'"] form textarea').focus();
	}

// $(document).ready(function() {
    // $("#autogrow").autoGrow();
	// $('#calendarModal').hide();
  // });
  
$(function() {
	   jQuery("#share-mk").tagsManager({		
		prefilled:"",
        preventSubmitOnEnter: true,
        preventDuplicates: true,
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidmk'
      });  
      
      jQuery("#share-jadwal").tagsManager({		
		prefilled:"",
        preventSubmitOnEnter: true,
        preventDuplicates: true,
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'hidjadwal'
      });  
 });

$(function() {
		$("#share-mk").autocomplete({ 
			source: base_url + "home/mk",
			minLength: 0, 
			select: function(event, ui) { 
				$('#share-mk-id').val(ui.item.id); 
				$('#share-mk').val(ui.item.value);
			} 
		});	
		
		$("#share-jadwal").autocomplete({ 
			source: base_url + "home/jadwal",
			minLength: 0, 
			select: function(event, ui) { 
				$('#share-jadwal-id').val(ui.item.id); 
				$('#share-jadwal').val(ui.item.value);
			} 
		});		  				
});


 // var ias = jQuery.ias({
  // container : '.this-timeline',
    // item: '.media.status-wrap',
    // pagination: '.pagination',
    // next: 'a.next-page'
// });
// ias.extension(new IASSpinnerExtension({
    // html: '<div style="text-align:center"><span class="fa fa-spinner fa-spin"></span></div>', // optionally
// }));

$(".btn-tooltip").mouseenter(function(){
	$(".btn-tooltip").tooltip('show');
});

$('.show-calendar-it').click(function(e){
	e.preventDefault();
	$(this).parent('small').parent('div').parent('td').children('.hide-it').fadeIn().removeClass('hide-it');
	$(this).hide();
	$('#calendarModal').hide();
	});

	
$('#btn-modal-close').click(function(e){
	e.preventDefault();	
	$('#calendarModal').hide();
});

$('#btn-modal-close-manage').click(function(e){
	$('#cal-detail').show();
	$('#cal-edit').hide();
});

$("#btn-ubah-kegiatan").click(function(){
	$('#cal-edit').show();
	$('#cal-detail').hide();
});

//calendar
var is_detail = 0;

$('.popup-form').click(function(e){
	e.preventDefault();
	var $year=$(this).data('calendaryear');
	var $month=$(this).data('calendarmonth');
	var $day=$(this).data('calendarday')
	var day = ("0" + $day).slice(-2);
    var month = ("0" + ($month)).slice(-2);
	var today = $year+"-"+(month)+"-"+(day);
	$('#calendarModal .input-datetime').val(today);
	$('#calendarModal .input-datetime').datepicker();
	//$('#calendarModal').modal('toggle');
	$('#calendarModal').show('slow');
	
	if(is_detail == 1){
		is_detail = 0;
	}
	else{
		$('#cal-add').show();
		$('#cal-edit, #cal-detail').hide();
	}
	});
	
$(".label-aktifitas").click(function(){
	is_detail = 1;
	$('#cal-add, #cal-edit').hide();
	$('#cal-detail').show();	
	
	$("#info-judul").html(": "+$(this).data("judul"));
	$("#info-waktu").html(": "+$(this).data("tgl") + " " + $(this).data("jam_mulai") + " s/d " + $(this).data("tgl_selesai") + " " + $(this).data("jam_selesai")  );
	$("#info-ruang").html(": "+$(this).data("ruang"));
	$("#info-lokasi").html(": "+$(this).data("lokasi"));
	
	// load_select_ruang($(this).data("ruang"));
	
	// for(var i=0;i<ruang.length;i++){
// 		
	  // $("ul.select2-choices").append('<li class="select2-search-choice"><div>A1.2 - R. Administrasi</div><a href="#" onclick="return false;" class="select2-search-choice-close" tabindex="-1"></a></li>');
      // console.log(ruang[i]);
// 
   // }
	
	$("[name='time']").val($(this).data("time"));
	$("[name='edit_judul']").val($(this).data("judul"));
	$("[name='edit_lokasi']").html($(this).data("lokasi"));
	$("[name='edit_jam_mulai']").val($(this).data("jam_mulai"));
	$("[name='edit_jam_selesai']").val($(this).data("jam_selesai"));
});

function load_select_ruang(ruang_){
	var URL = base_url + 'home/load_select_ruangan';
    $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : $.param({ruang : ruang_}),
        success:function(msg) 
        {
            $("#edit-ruang").html(msg);
        }
    });
}


function save_aktifitas(){
	var formData = new FormData($("#form-aktifitas")[0]);
	var URL = base_url + 'home/add_kegiatan';
	$(".loading").show();
    $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : formData,
        async: false,
        success:function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();
        },
        error: function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();    
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function edit_aktifitas(){
	var formData = new FormData($("#form-edit-aktifitas")[0]);
	var URL = base_url + 'home/edit_kegiatan';
	$(".loading").show();
    $.ajax({
        url : URL,
        type: "POST",
        dataType : "HTML",
        data : formData,
        async: false,
        success:function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();
        },
        error: function(msg) 
        {
        	$(".loading").fadeOut();
            location.reload();    
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function hapus_aktifitas(id_){
	if(confirm('Are your sure wanna delete this activity?')){
		// var formData = new FormData($("#form-edit-aktifitas")[0]);
		var URL = base_url + 'home/hapus_kegiatan';
		$(".loading").show();
	    $.ajax({
	        url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : $.param({id : id_}),
	        success:function(msg) 
	        {
	        	$(".loading").fadeOut();
	            location.reload();
	        },
	        error: function(msg) 
	        {
	        	$(".loading").fadeOut();
	            location.reload();    
	        }
	    });
   }
}

$(document).ready(function(){
	$('#calendarModal').hide();
});

function check_tgl(){
	if($("[name='tanggal_sampai']").val() != '') {
		var start = new Date($("[name='tanggal']").val());
		var end = new Date($("[name='tanggal_sampai']").val());

		if(end.getDate() < start.getDate()) $("[name='tanggal_sampai']").val('');
	}
}

$(function () {
    $('.pick-date').datetimepicker({
        pickDate: true,
        pickTime: false
    });
});


$(function () {
    $('.pick-time').datetimepicker({
        pickDate: false
    });
});




