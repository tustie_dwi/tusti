$(document).ready(function(){
	$(".form_date").datepicker({dateFormat: 'yy-mm-dd'});
	$("#button_newtask").click(function(){
		$("#display-newtask").attr("style","display:block;");
		$("#display-tasks").attr("style","display:none;");
		$("#button_tasklist").show();
		$(this).hide();
	});
	$("#button_tasklist").click(function(){
		$("#display-newtask").attr("style","display:none;");
		$("#display-tasks").attr("style","display:block;");
		$("#button_newtask").show();
		$(this).hide();
	});
	$('.timepicker1').timepicker({
		defaultTime: false
	});
});

function submit_tasks(){
	$('#form-newtask').submit(function (e) {
    	var tanggal = $('#tanggal').val();
    	var jam_mulai = $('#jam_mulai').val();
    	var jam_selesai = $('#jam_selesai').val();
    	var judul = $('#judul').val();
    	var catatan = $('#catatan').val();

    	if(tanggal.length > 0 && jam_mulai.length > 0 && jam_selesai.length > 0 && judul.length > 0 && catatan.length > 0){
	    	var formData = new FormData($(this)[0]);
	    	var URL = base_url + 'home/savetaskToDB';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.reload();
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else{
	    	alert("Form tidak boleh kosong!");
	    }
	});
};

function finish_tasks(id){
	var check = $("#finish"+id).is(':checked');
	var finish;
	if(check == true){
		finish = 1;
	}
	else finish = 0;
	
	var URL = base_url + 'home/finish_task';
      $.ajax({
        url : URL,
        type: "POST",
        dataType : "html",
        data : $.param({
				task_id : id,
        		finish : finish
		}),
        success:function(msg) 
        {
        }
   	 });
};

function doDeleteTask(i) {
  	var x = confirm("Are you sure you want to delete?");
 	if (x){
  		var del_id	= i;
  		var url		= base_url + 'home/delete_task';
  		//alert(i);

	  	$.ajax({
				type : "POST",
				dataType : "html",
				url : url,
				data : $.param({
					delete_id : del_id
				}),
				success : function(msg) {
					if (msg) {
						alert(msg);
						location.reload();
					} else {
						alert('Gagal menghapus task!');
					}
				}
		});
	}
  else {
  }
  	
};
