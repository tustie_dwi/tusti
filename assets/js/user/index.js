$(function(){
	$('.btn-delete-task').click(function(){
		
		if(confirm("Any deleted user can not login to the system and this action once done can not be undone. Continue delete task?")) {			
		
		uid = $(this).data('uid'); //alert(uid);
		row = $(this).parents('tr');
		
		$.post(
			base_url + 'user/delete_task',
			{id: uid},
			function(data){
				if(data.status.trim() == "OK") {
					row.fadeOut();
				}
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
		
		return false;
	});
	
	$('.btn-delete-task-o').click(function(){
		
		if(confirm("Any deleted user can not login to the system and this action once done can not be undone. Continue delete task?")) {			
		
		uid = $(this).data('uid');
		row = $(this).parents('li');
		
		$.post(
			base_url + 'user/delete_task',
			{id: uid},
			function(data){
				if(data.status.trim() == "OK") {
					row.fadeOut();
				}
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
		
		return false;
	});
	
	
	$('.btn-delete-user').click(function(){
		
		if(confirm("Any deleted user can not login to the system and this action once done can not be undone. Continue delete user?")) {			
		
		uid = $(this).data('uid'); //alert(uid);
		row = $(this).parents('tr');
		
		$.post(
			base_url + 'user/delete',
			{id: uid},
			function(data){
				if(data.status.trim() == "OK") {
					row.fadeOut();
				}
				else alert(data.error);
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
			});
			
		}
		
		return false;
	});
	
	$('.btn-toggle-status-user').click(function(){
		uid = $(this).data('uid'); // alert(uid);
		label = $(this).parents('tr').find('.label-status'); // alert(label);
		btn = $(this);
		btn.button('loading');	
		
		$.post(
			base_url + 'user/togglestatus',
			{id: uid},
			function(data){
				
				btn.button('reset');
				
				if(data.status.trim() == "OK") {
					label.html(data.ustatus);
					if(data.ustatus == 'Active')
						label.removeClass('label-warning').addClass('label-success');
					else label.removeClass('label-success').addClass('label-warning');
				}
				else alert(data.error);
				
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
		});
		
		return false;
	});
	
	$('.btn-toggle-task').click(function(){
		uid = $(this).data('uid'); // alert(uid);
		label = $(this).parents('ul').find('.label-task'); // alert(label);
		labels = $(this).parents('ul').find('.label-tasks'); // alert(label);
		 //if (this.checked) {
			$.post(
				base_url + 'user/toggletask',
				{id: uid},
				function(data){		
					//alert(data.ustatus);
					btn.button('reset');					
					if(data.status.trim() == "OK") {						
						if(data.ustatus == '1')
							$(this).parents('ul').css('text-decoration',"line-through");
						else $(this).parents('ul').css("text-decoration","none");
					}
					else alert(data.error);
					
				},
				"json"
				).error(function(xhr) {
					alert(xhr.responseText);
			});
		//}
		
	});
	
});

function strikeIt(el){
	el.parentNode.style.textDecoration = (el.checked===true) ? "line-through" : "none";

}