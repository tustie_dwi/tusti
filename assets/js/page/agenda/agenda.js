$(document).ready(function(){
	init();
	//today();
});

function init(){
	//alert(base_apps);
    $("#my-calendar").zabuto_calendar({
        action_nav: function () {
            return myNavFunction(this.id);
        },
        action: function () {
            return myDateFunction(this.id, false);
        },
        ajax: {
          url: base_apps + 'page/init_calendar_json'
      	},
        show_previous: 6,
      	show_next: 6,
        today: true
    });
}

function myDateFunction(tgl, bol){
	var date = $("#" + tgl).data("date");
	hari = new Date(date);
	
	//alert(tgl);
	
	var url_ = base_apps + 'page/get_event_hari';
	$.ajax({
        url : url_,
        type: "POST",
        dataType : "html",
        data : $.param({tgl : date}),
        success:function(msg) 
        {
        	if(msg == ''){
        		$("#title").html('Event on '+ convert_month(hari.getMonth()+1) + ' ' + hari.getDate() +', ' + hari.getFullYear());	
        		$("#event-konten").html(blank_());
        		$("#pagination-event").html("");
        	}
        	else{
	        	msg = JSON.parse(msg);
	        	get_event_hari(msg, date);
        	}
        }
    });
}

function myNavFunction(id) {
    var nav = $("#" + id).data("navigation");
    var to = $("#" + id).data("to");
	
	
    
    var url_ = base_apps + 'page/get_event_bulan';
	$.ajax({
        url : url_,
        type: "POST",
        dataType : "html",
        data : $.param({bulan : to.month, tahun : to.year}),
        success:function(msg) 
        {
        	if(msg == ''){
        		// console.log(msg);
        		$("#title").html('Event on '+ convert_month(to.month) +' ' + to.year);	
        		$("#event-konten").html(blank_());
        		$("#pagination-event").html("");
        	}
        	else{
	        	msg = JSON.parse(msg);
	        	get_event_bulan(msg, to.month, to.year);
        	}
        }
    });
}

function get_event_hari(data, haris){
	$("#event-konten").html("");
	var jml_konten = 0;
	hari = new Date(haris);
	
	$("#title").html('Event on '+ convert_month(hari.getMonth()+1) + ' ' + hari.getDate() +', ' + hari.getFullYear() );	
	$.each(data, function(e, v){
		$("#event-konten").append(template_konten(v));
		jml_konten++;
	});
	
	var jml_page = Math.floor(jml_konten / 10);
	create_page(jml_page);
}


function get_event_bulan(data, bulan, tahun){
	$("#event-konten").html("");
	var jml_konten = 0;
	
	$("#title").html('Event on '+convert_month(bulan) +' ' + tahun);	
	$.each(data, function(e, v){
		$("#event-konten").append(template_konten(v));
		jml_konten++;
	});
	
	if(jml_konten == 0) {
		$("#event-konten").html(blank_());
	}
	
	var jml_page = Math.floor(jml_konten / 10);
	create_page(jml_page);
}

function today(){
	var now = $("#date-now").val();

	/*$("#event-konten").html("");
	var jml_konten = 0;
	$.each(data, function(e, v){
		if(v['tgl_mulai'] == now){
			$("#event-konten").append(template_konten(v));
			jml_konten++;
		}
	});
	
	if(jml_konten == 0) {
		$("#event-konten").html(blank_());
	}
	var jml_page = Math.floor(jml_konten / 10);
	create_page(jml_page);*/
	
	var url_ = base_apps + 'page/get_event_hari';
	$.ajax({
        url : url_,
        type: "POST",
        dataType : "html",
        data : $.param({tgl : now}),
        success:function(msg) 
        {
		
        	if(msg == ''){
        		$("#title").html('Today Event');		
        		$("#event-konten").html(blank_());
        		$("#pagination-event").html("");
        	}
        	else{
	        	msg = JSON.parse(msg);
	        	get_event_hari(msg, now);
        	}
        }
    });
}

function get_event(jenis, judul){
	$("#event-konten").html("");
	var jml_konten = 0;
	
	$("#title").html(judul);

	$.each(data, function(e, v){
		
		if(v['jenis_kegiatan_id'] == jenis){
			$("#event-konten").append(template_konten(v));
			jml_konten++;
		}
	});
	
	if(jml_konten == 0) {
		$("#event-konten").html(blank_());
	}
	var jml_page = Math.floor(jml_konten / 10);
	create_page(jml_page);
}

function create_page(page){
	$("#pagination-event").html("");
	if(page > 0){
		if(page > 0){
			$("#pagination-event").append('<li><a onclick="prev_page()" style="cursor: pointer">Prev</a></li>');
			for(i=0;i<page;i++){
				$("#pagination-event").append('<li><a onclick="show_page(\''+(i+1)+'\')" style="cursor: pointer">'+(i+1)+'</a></li>');
			}
			
			max_page = page;
			$("#pagination-event").append('<li><a onclick="next_page()" style="cursor: pointer">Next</a></li>');
			
			show_page(1);
		}
	}
}

function next_page(){
	next = $("#pagination-event li[class='active'] a").text();
	
	if(next < max_page){
		next = parseInt(next) + 1;
		show_page(next);
	}
}

function prev_page(){
	next = $("#pagination-event li[class='active'] a").text();
	
	if(next > 1){
		next = parseInt(next) - 1;
		show_page(next);
	}
}

function blank_(){
	var str = "<div class='well'>Events not found</div>";
	return str;
}

function show_page(num){
	switch_page(num);
	num++;
	
	$("#pagination-event li").hide().removeAttr("class");	
	$("#pagination-event li:nth-child("+num+")").attr('class','active');
	
	$("#pagination-event li:first()").show();
	$("#pagination-event li:nth-child("+(num-2)+")").show();
	$("#pagination-event li:nth-child("+(num-1)+")").show();
	$("#pagination-event li:nth-child("+num+")").show();
	$("#pagination-event li:nth-child("+(num+1)+")").show();
	$("#pagination-event li:nth-child("+(num+2)+")").show();
	$("#pagination-event li:last()").show();
}

function switch_page(page){
	$("#event-konten .media").hide();
	
	bts_up = page * 10;
	bts_down = (page - 1) * 10;
	
	for(i=bts_down; i<=bts_up;i++){
		$("#event-konten .media:nth-child("+i+")").show();
	}
}

function template_konten(data){
	if(data['inf_peserta'] != null){
		user = '<br><small><i class="fa fa-user"></i> '+data['inf_peserta']+'</small>';
	}
	else user = '';
	
	if(data['judul'] != null){
		judul = data['judul'];
	}else judul = data['judul_ori'];
	
	var url = $("#url-event").val() + data['page_link'] + '/' + data['agenda_id'];
	
	var str = '<div class="media" style="border-bottom:dashed 1px #ccc;padding-bottom:5px">' +
					'<a target="blank" class="pull-left" href="'+url+'">' +
						'<img class="media-object img-responsive" src="http://file.filkom.ub.ac.id/fileupload/assets/upload/thumb.png" width="100px">' +
					'</a>' +
					'<div class="media-body">' +
						'<a class="title-article" href="'+url+'">' +
						'<a class="title-article" href="'+url+'">' +
							'<h5 class="media-heading">'+judul+'</h5>' +
						'</a><small>' +
						ucfirst(data['inf_hari'])+' <span class="fa fa-calendar"></span>' +
						'<time>&nbsp;'+data['tgl_mulai']+' <span class="fa fa-map-marker"></span> '+data['lokasi']+
						'</p></small>' +
					'</div>' +
				'</div>';
				
	return str;
}

function ucfirst(str) {
  str += '';
  var f = str.charAt(0)
    .toUpperCase();
  return f + str.substr(1);
}

function convert_month(number){
	switch(number){
	case 0:return 'Jan';break;
	case 1:return 'Jan';break;
	case 2:return 'Feb';break;
	case 3:return 'Mar';break;
	case 4:return 'Apr';break;
	case 5:return 'May';break;
	case 6:return 'Jun';break;
	case 7:return 'Jul';break;
	case 8:return 'Aug';break;
	case 9:return 'Sep';break;
	case 10:return 'Oct';break;
	case 11:return 'Nov';break;
	case 12:return 'Des';break;
	
	}
}
		
$(document).on('click',"#nav-jenis-kegiatan li", function(){
	$("#nav-jenis-kegiatan li").removeAttr('class');
	$(this).attr('class', 'active');
});
