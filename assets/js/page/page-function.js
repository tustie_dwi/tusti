$('#carousel-top-slider').carousel();

var $docWidth = $(document).width();
$(window).resize(function() {
    $docWidth = $(window).width();
    resizeContentHeight();
});

resizeContentHeight();

function resizeContentHeight() {
    heightWrapper = 'auto';
    if ($docWidth > 768) {
        heightWrapper = $('#wrap').height() - $('.top-slider').height();
    }
    $('.sidebar').height(heightWrapper);
    $('.main-content').height(heightWrapper);
}
$('.media-comment .reply').click(function(e) {
    e.preventDefault();
    $url = $(this).data('url');
	$parent=$(this).data('parentid');
	if($parent==null)$parent=0;
	$('#parentid').val($parent);
    $form = $('.form-comment');
	
    $form.remove();
    $('.comment-post').fadeIn();
    $form2 = '<form class="form form-reply form-comment" id="form-submit-comment-news" action="' + $url + '" method="post">' + $form.html() + '</form>';
    $(this).parent('.comment-action').parent('.media-body').append($form2);
});
$('.comment-post').click(function(e) {
    e.preventDefault();
    $url = $(this).data('url');
	$('#parentid').val(0);
    $form = $('.form-comment');
    $form.remove();
    $(this).fadeOut();
    $form2 = '<form class="form form-comment" id="form-submit-comment-news" action="' + $url + '" method="post">' + $form.html() + '</form>';
    $(this).parent().append($form2);
});

$('.mobile-menu-toggle').click(function(e){
	e.preventDefault();
	$('.nav-justified').toggleClass('slide');
	});

(function($) {

    var $win = $(window);
    var $nav = $('.navigation');
    var navTop = $('.main-content').length - 70 && $('.main-content').offset().top - 70;
    var navTop2 = $('#wrap').length && $('#wrap').offset().top;
    var isFixed = 0;

    processScroll();

    $win.scroll(processScroll);
    function processScroll(e) {
        var scrollTop = $win.scrollTop();
        if (scrollTop >= navTop2) {
            $nav.addClass('navigation-hide');
            $('.logo-left').addClass('navigation-hide');
        } else if (scrollTop <= navTop2) {

            $nav.removeClass('navigation-hide');
            $('.logo-left').removeClass('navigation-hide');
        }
        if (scrollTop >= navTop && !isFixed) {
            isFixed = 1;
            $nav.addClass('navigation-fixed');
			$('.logo-left').addClass('fixed');
			$width = $('.navigation-fixed .container').width();
			//$width = $(window).width()-$width;
			//$width=$width/2;
			//$('.logo-left.fixed').width($width-45);
        } else if (scrollTop <= navTop && isFixed) {
            isFixed = 0;
            $nav.removeClass('navigation-fixed');
			$('.logo-left').removeClass('fixed');
			//$('.logo-left').width($('.logo-right').width());
        }
    }

})(jQuery);

$('.btn-regis-student').click(function(e) {
    e.preventDefault();
    $('.form-signin').slideUp('fast');
    $('.form-regist-dosen').slideUp('fast');
    $('.form-regist-student').slideDown('slow');

});
$('.btn-regis-dosen').click(function(e) {
    e.preventDefault();
    $('.form-signin').slideUp('fast');
    $('.form-regist-student').slideUp('fast');
    $('.form-regist-dosen').slideDown('slow');

});

$('.close-form').click(function(e) {
    e.preventDefault();

    $('.form-regist-student').slideUp('fast');
    $('.form-regist-dosen').slideUp('fast');
    $('.form-signin').slideDown('fast');
});