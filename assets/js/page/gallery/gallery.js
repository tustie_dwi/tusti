	var homeGallery = $('#content-video').clone();
	var searchElement = $('#content-search').clone();
	var cat = "all";
	function templateVideo(data){
		var templateElement = "";
		templateElement += "<div class='content-list-category-landscape'><div class='row'><div class='col-md-12'>";
		$.each(data,function(index, value){
			templateElement = templateElement+ "<div class='col-sm-3'>"+
				"<a href='#' class='tube-title-link' data-list='"+ value.id +"' data-lang='"+ lang +"' data-unit='"+unit+"'>"+
				"<figure class='tube-figure'>"+
					"<img src='"+(file_url+"/"+value.file_loc)+"' class='img-responsive img-responsive-center img-tube img-tube-thumb'>"+
				"</figure>"+
				"</a>"+
				"<a href='#' class='tube-title-link tube-title-link-default' data-list='"+ value.id +"' data-lang='"+ lang +"' data-unit='"+unit+"'>"+
					value.file_title+"</a>"+
				"<div class='tube-metadata'>"+
				"<div>"+
				"<span class='view-count'>"+ value.content_hit +" views</span>"+
				"<span class='time-upload'>"+value.last_update+"</span>"+
				"</div></div></div>";
		});
		return templateElement;
	}
	function scrollVideo(hal){
		$(window).on("scroll", function loadData() {
			if($(window).scrollTop() >= $(document).height()- $('footer').height() - $(window).height()){
				hal++;
				var scrolltop = $(window).scrollTop();
				$.ajax({
					type : "POST",
					url: base_url + "/page/read_video",
					data : $.param({lang:lang, unit:unit, hal:hal}),
					beforeSend : function (){
						$(window).off("scroll",loadData);
					},
					success : function(data) {
						$(window).scrollTop(scrolltop);
						var result = JSON.parse(data);
						$('#video').append(templateVideo(result)).hide().slideDown('fast');
						if(result.length<limit) $(window).off("scroll",loadData);
						else $(window).on("scroll",loadData);
					}
				});
			}
		});
	}
	function templateImages(data,needHR){
		var templateElement = "";
		var Index = 0;
		$.each(data,function(index, value){
			if(!value.content) return;
			if(Index!=0||needHR) templateElement += "<hr class='hr-tube'>";
			Index ++;
			templateElement = templateElement+"<h4 class='title-tube margin-top-no'>"+value.file_note+"</h4>"+
				"<div class='content-list-category-landscape'>"+
					"<div class='row'>"+
						"<div class='col-md-12'>";
			$.each(value.content,function(i, v){
				templateElement  = templateElement + 
					"<div class='col-sm-3'>"+
						"<a href='#' class='tube-title-link' data-list='"+v.id+"' data-lang='"+lang+"' data-unit='"+unit+"'>"+
							"<figure class='tube-figure'>"+
								"<img src='"+ (file_url+"/"+v.file_loc) +"' class='img-responsive img-responsive-center img-tube img-tube-thumb'>"+
							"</figure>"+	
						"</a>"+
						"<a href='#' class='tube-title-link tube-title-link-default' data-list='"+v.id+"' data-lang='"+lang+"' data-unit='"+unit+"'>"+v.file_title+"</a>"+
						"<div class='tube-metadata'>"+
							"<span class='view-count'>"+ v.content_hit +" views</span>"+
							"<span class='time-upload'>"+ v.last_update+"</span>"+
						"</div>"+
					"</div>";
			});
			templateElement += "</div></div></div>";
		});
		return templateElement;
	}
	function scrollImage(hal){
		$(window).on("scroll", function loadData() {
			if($(window).scrollTop() >= $(document).height()- $('footer').height() - $(window).height()){
				var scrolltop = $(window).scrollTop();
				$.ajax({
					type : "POST",
					url: base_url + "/page/read_images",
					data : $.param({lang:lang, unit:unit, hal:hal}),
					beforeSend : function (){
						$(window).off("scroll",loadData);
					},
					success : function(data) {
						$(window).scrollTop(scrolltop);
						var result = JSON.parse(data);
						hal+=result.length;
						var count = 0;
						$.each(result,function (i,vali){
							if(vali.content)
							$.each(vali.content,function (j,valj) {
								count++;
							});
						});
						$('#images').append(templateImages(result,true)).hide().slideDown('fast');
						if(count<limit) $(window).off("scroll",loadData);
						else $(window).on("scroll",loadData);
					}
				});
			}
		});
	}
	$(document).on('click',".tube-title-link",function(){
			var id = $(this).data("list");
			var lang = $(this).data("lang");
			var unit = $(this).data("unit");
			if(unit!=0){
				url = base_url + "/read/read_gallery";
			}else{
				url = base_url + "/page/read_gallery";
			}
			$.ajax({
				type : "POST",
				dataType: "HTML",
				url: url,
				data : $.param({
					id : id,lang:lang, unit:unit
				}),
				success : function(data) {
					$('#content-video').slideUp('fast');
					$('#content-video').html(data);
					$('#content-video').slideDown('fast');
				}
			});
		});
	$(document).on('click','#galleryHome', function (){
		$('#content-video').slideUp('fast');
		$('#content-video').html(homeGallery.clone());
		$('#content-video').slideDown('slow');
		cat = "all";
	});
	$(document).on('click','#videoType', function (){
		var hal = 0;
		$(window).on('scroll', function loadData(){
			return false;
		});
		$.ajax({
			type : "POST",
			url: base_url + "/page/read_video",
			data : $.param({lang:lang, unit:unit, hal:hal}),
			beforeSend : function (){
				$('#content-pencarian').slideUp('fast');
				$('#content-pencarian').empty();
				$('#content-pencarian').html(searchElement);
			},
			success : function(data) {
				$('#content-pencarian').append("<div class='content-pane-tube' id='video'><h4 class='title-tube margin-top-no'>Video</h4>" + templateVideo(JSON.parse(data)) + "</div></div></div>");
				$('#content-pencarian').hide().slideDown('fast');
				cat = "video";
				scrollVideo(hal);
			}
		});
	});
	$(document).on('click','#imagesType', function (){
		var hal = 0;
		$(window).on('scroll', function loadData(){
			return false;
		});
		$.ajax({
			type : "POST",
			url: base_url + "/page/read_images",
			data : $.param({lang:lang, unit:unit, hal:hal}),
			beforeSend : function (){
				$('#content-pencarian').slideUp('fast');
				$('#content-pencarian').empty();
				$('#content-pencarian').html(searchElement);
			},
			success : function (data){
				var result = JSON.parse(data);
				$('#content-pencarian').append("<div class='content-pane-tube' id='images'>"+templateImages(result)+"</div>");
				$('#content-pencarian').hide().slideDown('fast');
				cat = "images";
				hal+=result.length;
				scrollImage(hal);
			}
		});
	});
	$(document).on('keyup','#search', function (event){
		if(event.keyCode == 13) {
			$(window).on('scroll', function loadData(){
				return false;
			});
			if(cat == "images"){
				var hal = 0;
				$(window).on('scroll', function loadData(){
					return false;
				});
				$.ajax({
					type : "POST",
					url: base_url + "/page/read_images/"+$('#search').val(),
					data : $.param({lang:lang, unit:unit, hal:hal}),
					beforeSend : function (){
						$('#content-pencarian').slideUp('fast');
						$('#content-pencarian').empty();
						$('#content-pencarian').html(searchElement);
					},
					success : function(data) {
						var hasil;
						if(data!=null && (hasil = JSON.parse(data)).length>0 ){
							var count = 0;
							$.each(hasil,function (i,vali){
								if(vali.content)
								$.each(vali.content,function (j,valj) {
									count++;
								});
							});
							hal += hasil.length;
							if(count>=limit) scrollImage(hal);
							if(count==0) $('#content-pencarian').append("<div class='content-pane-tube' id='video'><h4 class='title-tube margin-top-no'><center>Tidak ada Gallery yang sesuai</center> </h4></div></div></div>");
							else $('#content-pencarian').append("<div class='content-pane-tube' id='images'>"+templateImages(hasil)+"</div>");
						}
						$('#content-pencarian').hide().slideDown('fast');
					}
				});
			}
			if(cat == "video"){
				var hal = 0;
				$(window).on('scroll', function loadData(){
					return false;
				});
				$.ajax({
					type : "POST",
					url: base_url + "/page/read_video/"+$('#search').val(),
					data : $.param({lang:lang, unit:unit, hal:hal}),
					beforeSend : function (){
						$('#content-pencarian').slideUp('fast');
						$('#content-pencarian').empty();
						$('#content-pencarian').html(searchElement);
					},
					success : function(data) {
						if(data!=null){
							//hal ++;
							var hasil = JSON.parse(data);
							$('#content-pencarian').append("<div class='content-pane-tube' id='video'><h4 class='title-tube margin-top-no'>Video</h4>" + templateVideo(hasil) + "</div></div></div>");
							if(hasil.length==limit) scrollVideo(hal);
						}
						else{
							$('#content-pencarian').append("<div class='content-pane-tube' id='video'><h4 class='title-tube margin-top-no'><center>Tidak ada Video yang sesuai</center> </h4></div></div></div>");
						}
						$('#content-pencarian').hide().slideDown('fast');						
					}
				});
			}
			if(cat == "all"){
				var halvideo = 0;var halimages = 0;
				$(window).on('scroll', function loadData(){
					return false;
				});
				$.ajax({
					type : "POST",
					url: base_url + "/page/read_search",
					data : $.param({lang:lang, unit:unit, keyword:$('#search').val(), hal:hal}),
					beforeSend : function (){
						$('#content-pencarian').slideUp('fast');
						$('#content-pencarian').empty();
						$('#content-pencarian').html(searchElement);
					},
					success : function(data) {
						var hasil = JSON.parse(data);
						if(hasil.video!=null){
							$('#content-pencarian').append("<div class='content-pane-tube' id='video'><h4 class='title-tube margin-top-no'>Video</h4>" + templateVideo(hasil.video) + "</div></div></div>");
							if(hasil.video.length==limit){
								$('#video').append("<a href='#' id='videomore' style='position: absolute;right: 50%;padding: 0px 14px;background-color:white; box-shadow: 0 1px 2px rgba(0,0,0,.1);'><i class='fa fa-chevron-down'></i></a>");
								$('#videomore').click(function (){
									halvideo++;
									$.ajax({
										type : "POST",
										url: base_url + "/page/read_video",
										data : $.param({lang:lang, unit:unit, hal:halvideo}),
										success : function(data) {
											var result = JSON.parse(data);
											$('#videomore').before(templateVideo(result));
											if(result.length<limit) $('#videomore').hide();
										}
									});
								});
							}
						}
						if(hasil.images.length>0){
							var count = 0;
							$.each(hasil.images,function (i,vali){
								if(vali.content)
								$.each(vali.content,function (j,valj) {
									count++;
								});
							});
							halimages+=hasil.images.length;
							if(count==0){} 
							else {
								$('#content-pencarian').append("<div class='content-pane-tube' id='images'>"+templateImages(hasil.images)+"</div>");
								if(count>=limit) {
									$('#images').append("<a href='#' id='imagesmore' style='position: absolute;right: 50%;padding: 0px 14px;background-color:white; box-shadow: 0 1px 2px rgba(0,0,0,.1);'><i class='fa fa-chevron-down'></fa></a>");
									$('#imagesmore').click(function (){
										$.ajax({
											type : "POST",
											url: base_url + "/page/read_images",
											data : $.param({lang:lang, unit:unit, hal:halimages}),
											success : function(data) {
												var result = JSON.parse(data);
												halimages+=result.length;
												var count = 0;
												$.each(result,function (i,vali){
													if(vali.content)
													$.each(vali.content,function (j,valj) {
														count++;
													});
												});
												if(count<limit) $('#imagesmore').hide();
												$('#imagesmore').before(templateImages(result,true));
											}
										});
									});
								}
							}								
						}
						$('#content-pencarian').hide().slideDown('fast');
					}
				});
			}
		}
	})
