$(".table-mk").hide();
$(".form-tambah-mk").hide();
$("#btn-cetak-daftar").hide();
$("#btn-save-daftar").attr("disabled", "disabled");

$("#cmbsemester").change(function(){	
	var semester  = $(document.getElementById("cmbsemester")).val();
	$('#totalmk').val(0);	
	$('#no').val(0);
	
	switch(semester) {
		case '5':
		   mk =4;
			break;
		case '7':
			mk =5;
			break;
		default:
			mk =12;
	}
	
	 $('#totalmk').val(mk);
	 $(".form-tambah-mk").show();
	 $(".mk").fadeOut();
	 $(".mk").remove();
});

	
	function addmk(nilai){
		$(".table-mk").show();
		
		var data = nilai.split('|');
		var no  = $(document.getElementById("no")).val();
		//var nbiaya  = $(document.getElementById("tmpb")).val();
		
		var sks  = $(document.getElementById("tsks")).val();
		//var totalsks  = $(document.getElementById("totalsks")).val();
		var totalmk  = $(document.getElementById("totalmk")).val();
			
		var nomor = parseInt(no)+parseInt(1);
		if(cek_mk(data[0])){
			var r_id = "'"+data[0]+"'";
			var m_id = "'"+data[2]+"'";
			var data_mk = data[2].split('#');
			var nama_mk = data_mk[0] + " - " + data_mk[1];
			//var biaya = (nbiaya * data[1]);
			
			var total_sks = parseInt(sks)+parseInt(data[1]);
			
			if(nomor>totalmk){
				alert('Maaf, jumlah MK yang diijinkan hanya '+totalmk+' !');
			}else{
			
				var el = '<div class="mk row" r_id="'+data[0]+'" style="padding:5px;"><div class="col-md-9"><input type="hidden" name="sks[]" value="'+data[1]+'"><input type="hidden" name="mk[]" value="'+data[2]+'"><input type="hidden" name="mkid[]" value="'+data[0]+'" class="mkidtmp">'+nama_mk+'</div><div class="col-md-2">'+data[1]+' sks</div><div class="col-md-1 del"><a href="#" onclick="del_data('+r_id+','+m_id+','+data[1]+')"><i class="fa fa-trash-o"></i></a></div></div>';
				
				$('#tsks').val(total_sks);
				$('#no').val(nomor);
				
				$('#sks').html('<span id="sks"><b>'+total_sks+' sks</b></span>');
				$("input[type=submit]").removeAttr("disabled");
				$('#wrap-mk').append(el);
			}
		}
		else{
			alert('Maaf, matakuliah sudah dipilih!');
		}
	}
	function cek_mk(r_id){
			var init = true;
			$("#wrap-mk div").each(function(){
				var mk = $(this).find('.mkidtmp').val();
				if(mk == r_id) init = false;
			});
			
			return init;
		}

		function cek_mk_choose(){
			var init = false;
			$("#wrap-mk div").each(function(){
				init = true;
			});
			
			if(! init) alert('Silakan pilih mkan yang ingin dipesan!!');
			else{
				return confirm('Apakah Anda yakin ingin mengambil matakuliah ini?');	
			}
		}

		function del_data(mk, name, sks){
			if(confirm('Apakah Anda membatalkan matakuliah ' + name + '?')){
				
				var no  = $(document.getElementById("no")).val();
				var nomor = parseInt(no) - parseInt(1);		
				
				var totalsks  = $(document.getElementById("totalsks")).val();
				var tsks  = $(document.getElementById("tsks")).val();
							
				var total_sks = parseInt(tsks)-parseInt(sks);				
				
				$(".mk[r_id='"+mk+"']").fadeOut();
				$(".mk[r_id='"+mk+"']").remove();
				$('#no').val(nomor);
				$('#tsks').val(total_sks);				
								
				$('#sks').html('<span id="sks"><b>'+total_sks+' sks</b></span>');
				
			}
		}
		
	function toggle_sp(uid,xstatus){
			
		$.post(
			base_url + 'module/akademik/daftar/togglestatus_sp',
			{id: uid},
			function(data){				
				if(data.nstatus.trim() == "OK") {				
					$(".label-bintang[r_id='"+uid+"']").html(data.bintang);
					$(".label-status[r_id='"+uid+"']").html(data.ustatus);					
				}
				
				
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
		});
		
		return false;
	}
	
	function del_data_sp(mk, name, sks, biaya,uid,xstatus){
			if(confirm('Apakah Anda membatalkan matakuliah ' + name + '?')){
				var no  = $(document.getElementById("no")).val();
				var nomor = parseInt(no) - parseInt(1);		
				
				var totalsks  = $(document.getElementById("totalsks")).val();
				var tsks  = $(document.getElementById("tsks")).val();
				var total  = $(document.getElementById("tbiaya")).val();
				
				var total_sks = parseInt(tsks)-parseInt(sks);
				var total_biaya = parseInt(total)-parseInt(biaya);
				
				$(".mk[r_id='"+mk+"']").fadeOut();
				$(".mk[r_id='"+mk+"']").remove();
				$('#no').val(nomor);
				$('#tsks').val(total_sks);
				$('#tbiaya').val(total_biaya);
				
				if(total_biaya==0){
					$("input[type=submit]").attr("disabled", "disabled");
				}else{
					$("input[type=submit]").removeAttr("disabled");
				}
				
				$('#sks').html('<span id="sks"><b>'+total_sks+' sks</b></span>');
				$('#biaya').html('<span id="biaya"><b>'+accounting.formatMoney(total_biaya)+'</b></span>');
				
				$.post(
						base_url + 'module/akademik/daftar/delete_sp',
						{id: uid},
						function(data){				
														
						},
						"json"
						).error(function(xhr) {
							alert(xhr.responseText);
					});
					
					return false;
			}
			
		
	}

	$(function() {
			$(".e2").select2({
				placeholder: "Select Dosen Pembimbing",
				allowClear: true
			});
			
			$("#sel-mk").select2({
				placeholder: "Select MK",
				allowClear: true
			});
	}); 	