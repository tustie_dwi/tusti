$(function() {
	
	//$(".date").datetimepicker({format: 'yyyy-mm-dd',pickTime: false});
	//$(document).on('#form-content-daftar', '.tgl-lahir', function(){
	//alert("ini di akses");
	//	$(this).datetimepicker({format: 'yyyy-mm-dd',pickTime: false});
	//});
	
	document.addEventListener('DOMContentLoaded', function() {
    var link = document.getElementById('btn-validasi');
    // onClick's logic below:
		link.addEventListener('click', function() {
			alert('xxx');
		});
	});

	
	 $('input.tgl-lahir').live('click', function() {
           $(this).datepicker({showOn:'focus'}).focus();
        });
	
});


function save_proses(){
	
	$('#form-save-daftar').submit(function (e) {
	
		var today = new Date();			
    	var postData = $(this).serializeArray();
    	var formURL = base_url + 'info/save_form_mk_pilihan';
		
		var mhs  = $(document.getElementById("hidmhs")).val();
		var semester  = $(document.getElementById("cmbsemester")).val();
		
		var pembimbing  = $(document.getElementById("cmbpembimbing")).val();
		if(pembimbing=="-"){
			alert('Pilih dosen pembimbing akademik.');
			return false;
		}else{
		
			  $.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data, textStatus, jqXHR) 
				{	
					
					$("input[type=submit]").attr("disabled", "disabled");
					$("#wrap-mk div").each(function(){
						$(this).find('.del').hide();					
					});
					//$("#btn-cetak-daftar").show();
					$('.status-daftar').html("<em>OK ! Last saved on "+today+"</em>");
										
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					alert ('Failed!');      
				}
			  });
			e.preventDefault(); //STOP default action
			return false;
		}
	});
	
}

$("#btn-cetak-daftar-ok").click(function(){	
	cetak_kuitansi();		
});



function cetak_kuitansi(){
	
	var semester 	= document.getElementById("hidsemester");
		var semesterid	= $(semester).val();
				
		var mhs		= document.getElementById("hidmhs");
		var mhsid	= $(mhs).val();
		
		var surl = base_url + 'info/cetak_kuitansi/'+semesterid+'/'+mhsid;
	
		$.ajax({			
				type : "POST",
				dataType : "html",
				url : base_url + 'info/cetak_kuitansi',
				data : $.param({
					hidsemester : semesterid,
					hidmhs		: mhsid		
				}),
				success : function(msg) {
				
					if (msg == '') {
					} else {						
						$("#content-print").html(msg);
						window.print();
						
					}
				}
			});
	 e.preventDefault(); //STOP default action
	return false;
}

function checkEnter(event)
{
   	if(((event.keyCode==13) || !(event.keyCode)) && (event.keyCode!=0)){
		//location.reload();
		//$('#input-kode').val();
		get_mhs();
		return false;
	}
    
}



function get_mhs(){
	var mhs 	= document.getElementById("inputmhs");
	var mhsid	= $(mhs).val();
	
	var kode	= $(document.getElementById("input-kode")).val();
	var flagid	= $(document.getElementById("flag")).val();

	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'info/akademik/pilihan/mk',
		data : $.param({
			inputmhs : mhsid,
			input: kode,
			flag : flagid
		}),
		success : function(msg) {	
		
			if (msg == '') {
				return false;
			} else {
				if(msg=='NOK'){
					$("#form-content-daftar").html('<p>&nbsp;</p><p>&nbsp;</p><div class="col-md-12"><span class="alert alert-danger">Oops,</strong> proses validasi data gagal. Pastikan data yang Anda masukkan benar :( </span></div>');					
				}else{
					$("#frm-tmp").hide();
					$("#form-content-daftar").html(msg);
					$('.tgl-lahir').datetimepicker({
						format: 'YYYY-MM-DD', pickTime: false
					});
				}
			}
		}
	});
}


