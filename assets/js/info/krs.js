$(function() {
	
	//$(".date").datetimepicker({format: 'yyyy-mm-dd',pickTime: false});
	//$(document).on('#form-content-daftar', '.tgl-lahir', function(){
	//alert("ini di akses");
	//	$(this).datetimepicker({format: 'yyyy-mm-dd',pickTime: false});
	//});
	
	 $('input.tgl-lahir').live('click', function() {
           $(this).datepicker({showOn:'focus'}).focus();
        });
	
});

function cetak_kuitansi(){	
	var semester 	= document.getElementById("hidsemester");
	var semesterid	= $(semester).val();
			
	var mhs		= document.getElementById("hidmhs");
	var mhsid	= $(mhs).val();
		
	var surl = base_url + 'info/cetak_kuitansi/'+semesterid+'/'+mhsid;
	var url = base_url + 'info/cetak_kuitansi';
	
	$.ajax({			
		type : "POST",
		url : base_url + 'info/cetak_kuitansi',
		data : $.param({
			hidsemester : semesterid,
			hidmhs		: mhsid		
		}),
		success : function(msg) {			
			if (msg == '') {
				return false;
			} else {						
				$("#content-print").html(msg);
				window.print();
				
			}
		}
	});
	
}

function save_proses(is_valid){
	
	$('#form-save-daftar').submit(function (e) {
		
		var today = new Date();			
    	var postData = $(this).serializeArray();
    	var formURL = base_url + 'info/save_daftar_pendek/'+is_valid;
		
		var mhs  = $(document.getElementById("hidmhs")).val();
		var semester  = $(document.getElementById("hidsemester")).val();
		
		var pembimbing  = $(document.getElementById("cmbpembimbing")).val();
		
		if(pembimbing=="-"){
			alert('Pilih dosen pembimbing akademik.');
			return false;
		}else{
		
			  $.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data, textStatus, jqXHR) 
				{	
					alert('Success!!' + today);
					$("input[type=submit]").attr("disabled", "disabled");
					$("#wrap-mk div").each(function(){
						$(this).find('.del').hide();					
					});
					$("#btn-cetak-daftar").show();
					$('.status-daftar').html("<em>OK ! Last saved on "+today+"</em>");
										
				},
				error: function(jqXHR, textStatus, errorThrown) 
				{
					alert ('Failed!');      
				}
			  });
			e.preventDefault(); //STOP default action
			return false;
		}
	});
	
}

$("#btn-cetak-daftar-ok").click(function(){	
	cetak_kuitansi();		
});


function checkEnter(event)
{
   	if(event.keyCode==13){
		get_mhs();
		return false;
	}
    
}



function get_mhs(){
	var mhs 	= document.getElementById("inputmhs");
	var mhsid	= $(mhs).val();
	
	$.ajax({			
		type : "POST",
		dataType : "html",
		url : base_url + 'info/akademik/krs',
		data : $.param({
			inputmhs : mhsid
		}),
		success : function(msg) {
			if (msg == '') {
				return false;
			} else {
				$("#form-content-daftar").html(msg);
				$('.tgl-lahir').datetimepicker({
					format: 'YYYY-MM-DD', pickTime: false
				});
			}
		}
	});
}


