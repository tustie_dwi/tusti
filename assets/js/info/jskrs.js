$(".table-mk").hide();
$("#btn-cetak-daftar").hide();
$("#btn-save-daftar").attr("disabled", "disabled");
	
	function addmk(nilai, kelas){
		$(".table-mk").show();
	
		var data = nilai.split('|');
		var no  = $(document.getElementById("no")).val();
		var nbiaya  = $(document.getElementById("tmpb")).val();
		
		var sks  = $(document.getElementById("tsks")).val();
		var total  = $(document.getElementById("tbiaya")).val();
		var totalsks  = $(document.getElementById("totalsks")).val();
				
		var nomor = parseInt(no)+parseInt(1);
		if(cek_mk(data[0])){
			var r_id = "'"+data[0]+"'";
			var m_id = "'"+data[2]+"'";
			var biaya = (nbiaya * data[1]);
			
			var total_sks = parseInt(sks)+parseInt(data[1]);
			var total_biaya = parseInt(total)+parseInt(biaya);
			
			if(total_sks>totalsks){
				alert('Maaf, total sks yang diijinkan hanya '+totalsks+' sks!');
			}else{
			
				var el = '<div class="mk row" r_id="'+data[0]+'" style="padding:5px;"><div class="col-md-6"><input type="hidden" name="kelas[]" value="'+kelas+'"><input type="hidden" name="sks[]" value="'+data[1]+'"><input type="hidden" name="mk[]" value="'+data[2]+'"><input type="hidden" name="mkid[]" value="'+data[0]+'" class="mkidtmp"><input type="hidden" name="biaya[]" value="'+nbiaya+'"> '+data[2]+', kelas '+ kelas +'</div><div class="col-md-2">'+data[1]+' sks</div><div class="col-md-3">'+accounting.formatMoney(biaya) +'</div><div class="col-md-1 del"><a href="#" onclick="del_data('+r_id+','+m_id+','+data[1]+','+biaya+')"><i class="fa fa-trash-o"></i></a></div></div>';
				
				$('#tsks').val(total_sks);
				$('#tbiaya').val(total_biaya);
				$('#no').val(nomor);
				
				$('#sks').html('<span id="sks"><b>'+total_sks+' sks</b></span>');
				$('#biaya').html('<span id="biaya"><b>'+accounting.formatMoney(total_biaya)+'</b></span>');
				$("input[type=submit]").removeAttr("disabled");
				$('#wrap-mk').append(el);
			}
		}
		else{
			alert('Maaf, matakuliah sudah dipilih!');
		}
	}
	function cek_mk(r_id){
			var init = true;
			$("#wrap-mk div").each(function(){
				var mk = $(this).find('.mkidtmp').val();
				if(mk == r_id) init = false;
			});
			
			return init;
		}

		function cek_mk_choose(){
			var init = false;
			$("#wrap-mk div").each(function(){
				init = true;
			});
			
			if(! init) alert('Silakan pilih mkan yang ingin dipesan!!');
			else{
				return confirm('Apakah Anda yakin ingin mengambil matakuliah ini?');	
			}
		}

		function del_data(mk, name, sks, biaya){
			if(confirm('Apakah Anda membatalkan matakuliah ' + name + '?')){
				var no  = $(document.getElementById("no")).val();
				var nomor = parseInt(no) - parseInt(1);		
				
				var totalsks  = $(document.getElementById("totalsks")).val();
				var tsks  = $(document.getElementById("tsks")).val();
				var total  = $(document.getElementById("tbiaya")).val();
				
				var total_sks = parseInt(tsks)-parseInt(sks);
				var total_biaya = parseInt(total)-parseInt(biaya);
				
				$(".mk[r_id='"+mk+"']").fadeOut();
				$(".mk[r_id='"+mk+"']").remove();
				$('#no').val(nomor);
				$('#tsks').val(total_sks);
				$('#tbiaya').val(total_biaya);
				
				if(total_biaya==0){
					$("input[type=submit]").attr("disabled", "disabled");
				}else{
					$("input[type=submit]").removeAttr("disabled");
				}
				
				$('#sks').html('<span id="sks"><b>'+total_sks+' sks</b></span>');
				$('#biaya').html('<span id="biaya"><b>'+accounting.formatMoney(total_biaya)+'</b></span>');
			}
		}
		
	function toggle_sp(uid,xstatus){
			
		$.post(
			base_url + 'module/akademik/daftar/togglestatus_sp',
			{id: uid},
			function(data){				
				if(data.nstatus.trim() == "OK") {				
					$(".label-bintang[r_id='"+uid+"']").html(data.bintang);
					$(".label-status[r_id='"+uid+"']").html(data.ustatus);					
				}
				
				
			},
			"json"
			).error(function(xhr) {
				alert(xhr.responseText);
		});
		
		return false;
	}
	
	function del_data_sp(mk, name, sks, biaya,uid,xstatus){
			if(confirm('Apakah Anda membatalkan matakuliah ' + name + '?')){
				var no  = $(document.getElementById("no")).val();
				var nomor = parseInt(no) - parseInt(1);		
				
				var totalsks  = $(document.getElementById("totalsks")).val();
				var tsks  = $(document.getElementById("tsks")).val();
				var total  = $(document.getElementById("tbiaya")).val();
				
				var total_sks = parseInt(tsks)-parseInt(sks);
				var total_biaya = parseInt(total)-parseInt(biaya);
				
				$(".mk[r_id='"+mk+"']").fadeOut();
				$(".mk[r_id='"+mk+"']").remove();
				$('#no').val(nomor);
				$('#tsks').val(total_sks);
				$('#tbiaya').val(total_biaya);
				
				if(total_biaya==0){
					$("input[type=submit]").attr("disabled", "disabled");
				}else{
					$("input[type=submit]").removeAttr("disabled");
				}
				
				$('#sks').html('<span id="sks"><b>'+total_sks+' sks</b></span>');
				$('#biaya').html('<span id="biaya"><b>'+accounting.formatMoney(total_biaya)+'</b></span>');
				
				$.post(
						base_url + 'info/delete_sp',
						{id: uid},
						function(data){				
														
						},
						"json"
						).error(function(xhr) {
							alert(xhr.responseText);
					});
					
					return false;
			}
			
		
	}

	$(function() {
			$(".e2").select2({
				placeholder: "Select Dosen Pembimbing",
				allowClear: true
			});
			
			$("#sel-mk").select2({
				placeholder: "Select MK",
				allowClear: true
			});
	}); 	