function cari_konten(e){
	var nilai = $("#searchTxt").val();
	
	if (e.keyCode == 13) {
	        
        document.getElementById('form-search').submit();
        return false;
    }
}

var konten;
var konten_skripsi;

/*$(document).ready(function(){
	
		var url_ = base_url + 'info/skripsi_json';	
	
		$.ajax({
			url : url_,
			type: "POST",
			dataType : "json",
			success:function(msg) 
			{
				//console.log(msg);
				if (msg == '') {
					$("#loading").html("<div class='alert alert-danger'>Data not available</div>");				
					return false;
				}else{
					konten_skripsi = msg;
					get_konten_skripsi(konten_skripsi);
					$("#loading").fadeOut();
				}
			}
		});
	});*/
	
function get_konten(nilai){
	
	$("#konten-wrap").html('');
	var page = 1;
	var max_page = 1;
	$.each(konten, function(i, f){
		var init = f.url_web.toLowerCase().indexOf(nilai.toLowerCase());
		
		if( init >= 0){
			var str;
			str += '<tr style="border-top:0px;" page="'+page+'" >';
				str += '<td style="border-top:0px;">';
					str += '<div class="media">';
					str += '<a class="pull-left" href="'+f.url_web+'">';
					str += '<img class="media-object img-responsive" src="'+f.img+'" width="200px">';
					str += '</a><div class="media-body">';
					str += '<a class="title-article" href="'+f.url_web+'">';
					str += '<h4 class="media-heading">'+f.judul+'</h4></a><small>';
					str += '<span class="fa fa-calendar"></span><time datetime="2014-10-04 00:36:43">&nbsp;'+f.tgl+'</time>';
					str += ' <span class="category"><span class="fa fa-tags"></span>&nbsp;'+f.kategori+'</span>';
					str += '</small><p class="post-content">'+f.isi+'</p>';
			str += '</div></div></td></tr>';
			$("#konten-wrap").append(str);
			
			if(max_page % 5 == 0) page++;
			max_page++;
		}
		// console.log(f.judul);
	});
	
	pindah_halaman(1);
		
	$("#pagination").html('');
	
	var batas = 5;
	
	// $("#pagination").append('<li><a style="cursor: pointer" onclick="pindah_halaman(\''+(1)+'\',\''+page+'\')"><i class="fa fa-angle-double-left"></i></a></li>');
	$("#pagination").append('<li><a style="cursor: pointer" onclick="pindah_halaman(\''+(1)+'\',\''+page+'\')">First</a></li>');
	for(var i=0;i<page; i++) {
		if(i == 0) var c = 'class="active"';
		else var c = '';
		
		if( (i+1) > batas) hide = 'style="display:none"';
		else hide = '';
		
		$("#pagination").append('<li '+c+hide+' id="pageButton'+(i+1)+'"><a style="cursor: pointer" onclick="pindah_halaman(\''+(i+1)+'\',\''+page+'\')">'+(i+1)+'</a></li>');
	}
	$("#pagination").append('<li><a style="cursor: pointer" onclick="pindah_halaman(\''+(page)+'\',\''+page+'\')">Last</a></li>');
	// $("#pagination").append('<li><a style="cursor: pointer" onclick="pindah_halaman(\''+(1)+'\',\''+page+'\')"><i class="fa fa-angle-double-right"></i></a></li>');
}

function pindah_halaman(page){
	$("[page]").hide();
	$("[page="+page+"]").show();
	$("[id^='pageButton']").removeAttr("class").hide();
	$("#pageButton"+page).attr("class","active");
	
	page = parseInt(page);
	$("#pageButton"+(page-2)).show();
	$("#pageButton"+(page-1)).show();
	$("#pageButton"+page).show();
	$("#pageButton"+(page+1)).show();
	$("#pageButton"+(page+2)).show();
}

	
		
	function get_konten_skripsi(msg){
		var status_ini = $("#cmbskripsitmp").val();
		
		$("#skripsi-wrap").html('');
		$.each(konten_skripsi, function(status, a){
			if(status == status_ini){
				$.each(a, function(i, skripsi){
					var i =0;	
					
						var str = '<tr>';
						str += '<td>'+skripsi.skripsi_status+'</td>';				
						str += '<td>'+skripsi.status_id+'</td>';				
					str += '</tr>';
					$("#skripsi-wrap").append(str);
				});
			}
		});
	}
	
	function skripsi_data(status_ini){
	//	var status_ini = $("#cmbskripsitmp").val();
		var table	   = $('#skripsi-table').DataTable();
		
		//$("#skripsi-wrap").html('');
		$.each(konten_skripsi, function(status, a){
			if(status == status_ini){
				$.each(a, function(i, skripsi){
					var i =0;	
					table.row.add([
	        			skripsi.skripsi_status,
	        			skripsi.status_id
	        		]).draw();
					/*var str = '<tr>';
						str += '<td>'+skripsi.skripsi_status+'</td>';				
						str += '<td>'+skripsi.status_id+'</td>';
						
					str += '</tr>';*/
					//$("#skripsi-wrap").append(str);
				});
			}
		});
	}










