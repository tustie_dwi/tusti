function submit_materi(i){
	var publish = i;
    $('#upload-form-materi').submit(function (e) {
    	var status = $('#select_statmk').val();
    	//alert(status);
    	if(status !== '0'){
	    	var formData = new FormData($(this)[0]);
	    	if(publish=='publish'){
		   		formData.append("b_savepublish", "1");
		   	}else{
		   		formData.append("b_draft", "1");
		   	}
	    	var URL = base_url + 'module/akademik/materimk/savematerimk';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		            location.reload();
		        },
		        error: function(jqXHR, textStatus, errorThrown) 
		        {
		            alert (msg);
		            location.reload();  
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else{
	    	alert("Status tidak boleh kosong!");
	    }
	});
};

function submit_file(i){
	var publish = i;
    $('#upload-form-file').submit(function (e) {
    	var formData = new FormData($(this)[0]);
    	if(publish=='publish'){
	   		formData.append("b_publish", "1");
	   	}else{
	   		formData.append("b_draft", "1");
	   	}
    	var URL = base_url + 'module/akademik/file/save';
          $.ajax({
            url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert (msg);
	            location.reload();
	        },
	        error: function(msg) 
	        {
	            alert (msg);     
	            location.reload(); 
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
};

function submit_silabus(){
    $('#upload-form-silabus').submit(function (e) {
    	var formData = new FormData($(this)[0]);
    	var URL = base_url + 'module/akademik/silabus/savesilabus';
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Silabus Success!');
	            location.reload();
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Silabus Failed!');
	            location.reload();      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
};

function submit_komponen(){
    $('#upload-form-komponen').submit(function (e) {
    	var formData = new FormData($(this)[0]);
    	var URL = base_url + 'module/akademik/silabus/savekomponen';
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Komponen Success!');
	            location.reload();
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Komponen Failed!');
	            location.reload();      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
};

function submit_jenis_file(){
    $('#upload-form-jenis-file').submit(function (e) {
    	var formData = new FormData($(this)[0]);
    	var URL = base_url + 'module/akademik/jenisfile/save';
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(data, textStatus, jqXHR) 
	        {
	            alert ('Upload Jenis File Success!');
	            location.reload();
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Jenis File Failed!');
	            location.reload();      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
};

function submit_pembimbing(){
	$("#upload-form-pembimbing").submit(function(e){		
		var formData = new FormData($(this)[0]);
    	var URL = base_url + 'module/akademik/pembimbing/save';
          $.ajax({
            url : URL,
	        type: "POST",
	        dataType : "HTML",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert (msg);
	            location.href = base_url + "module/akademik/pembimbing/";
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
};

//====Content====//
function submit_file_content(i){
	
	var publish = i;
    $('#upload-form').submit(function (e) {
    	//alert("TES");
    	var judul = $("#judul_file").val().length;
    	var keterangan = $("#keterangan_file").val().length;
    	//alert(judul);
		if(judul > 0 && keterangan > 0){
	    	var formData = new FormData($(this)[0]);
	    	if(publish=='publish'){
		   		formData.append("b_filepublish", "1");
		   	}else{
		   		formData.append("b_filedraft", "1");
		   	}
	    	var addURL = base_url + 'module/content/course/save_file';
	          $.ajax({
	            url : addURL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(data, textStatus, jqXHR) 
		        {
		            alert ('Upload File Berhasil!');  
		        },
		        error: function(jqXHR, textStatus, errorThrown) 
		        {
		            alert ('Upload File Failed!');      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		      });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else{
	    	alert("Form tidak boleh kosong!");
	    } 
	});
};

function submit_silabus_content(){
    $('#silabus-form').submit(function (e) {
    	var postData = $(this).serializeArray();
    	var formURL = base_url + 'module/content/course/save_silabus';
    	var ket = $("#cmbkomponen").val();
    	//alert(ket);
          $.ajax({
            url : formURL,
	        type: "POST",
	        data : postData,
	        success:function(msg) 
	        {
	            alert (msg);  
	        },
	        error: function(msg) 
	        { 
	            alert ("Update silabus gagal!");    
	        }
	      });
	    e.preventDefault(); //STOP default action
	});
};

function submit_topik_content(i){
	var publish = i;
    $('#form-save-topik').submit(function (e) {
    	var formData = new FormData($(this)[0]);
    	if(publish=='publish'){
	   		formData.append("b_savepublish", "1");
	   	}else{
	   		formData.append("b_draft", "1");
	   	}
    	var URL = base_url + 'module/content/course/save';
          $.ajax({
            url : URL,
	        type: "POST",
	        data : formData,
	        async: false,
	        success:function(msg) 
	        {
	            alert (msg);
	            location.href = base_url + 'module/content/course/';
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	            alert ('Upload Materi Failed!');      
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	    e.preventDefault(); //STOP default action
	    return false;
	});
};

function submit_edit_topik_content(i){
	var publish = i;
	//alert(i);
    $('#upload-form-edit-topik').submit(function (e) {
    	var judul = $("#judul").val();
    	if(judul.length > 0){
	    	var formData = new FormData($(this)[0]);
	    	if(publish=='publish'){
		   		formData.append("b_savepublish", "1");
		   	}else{
		   		formData.append("b_draft", "1");
		   	}
	    	var URL = base_url + 'module/content/course/saved';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		        },
		        error: function(msg) 
		        {
		            alert ('Update Materi Failed!');      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else alert("Judul tidak boleh kosong!");
	});
};

function submit_edit_file_content(i){
	var publish = i;
	// alert(i);
    $('#upload-form-edit-file').submit(function (e) {
    	var judul = $("#judul").val();
    	if(judul.length > 0){
	    	var formData = new FormData($(this)[0]);
	    	if(publish=='publish'){
		   		formData.append("b_savepublish", "1");
		   	}else{
		   		formData.append("b_draft", "1");
		   	}
	    	var URL = base_url + 'module/content/course/saved';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert (msg);
		        },
		        error: function(msg) 
		        {
		            alert ('Update Materi Failed!');      
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else alert("Judul tidak boleh kosong!");
	});
};

//===============NEWS=================//
function submit_comment_news(){
	$('#form-submit-comment-news').submit(function (e) {
    	var email = $("#comment-email").val();
    	var pass = $("#comment-password").val();
    	var comment = $("#comment-content").val();
    	if(email.length > 0 && pass.length > 0 && comment.length > 0){
	    	var formData = new FormData($(this)[0]);
	    	var URL = base_url + 'page/add_comment';
	          $.ajax({
	            url : URL,
		        type: "POST",
		        data : formData,
		        async: false,
		        success:function(msg) 
		        {
		            alert(msg);
		            location.reload();
				    $("#comment-email").val("");
			    	$("#comment-password").val("");
			    	$("#comment-content").val("");
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		    e.preventDefault(); //STOP default action
		    return false;
	    }
	    else{
	    	alert("Form tidak boleh kosong!");
	    }
	    
	});
};

$(document).on('click', ".btn-cancel-feedback", function(){
	$("#feedback-email").val('');
	$("#feedback-password").val('');
	$("#feedback-content").val('');
});

$(document).ready(function(){
	$("#form-feedback-page").submit(function(e){
		var email = $("#feedback-email").val();
    	var pass = $("#feedback-password").val();
    	var feedback = $("#feedback-content").val();
		if(email.trim()!=="" && pass.trim()!=="" && feedback.trim()!==""){
			var postData = new FormData($(this)[0]);
			$.ajax({
				url : base_url + 'page/save_feedback',
				type: "POST",
				data : postData,
				async: false,
				success:function(msg) 
				{
					alert(msg);
					//location.reload();
					$("#feedback-email").val('');
					$("#feedback-password").val('');
					$("#feedback-content").val('');
				},
			    cache: false,
				contentType: false,
				processData: false
			});
		}
		else{
	    	alert("Form tidak boleh kosong!");
	    }
		e.preventDefault(); //STOP default action
	    return false;
	});
	
	//===============daftar-ulang============//
	if($("#nim_mhs").length){
		$('#form-daftar-ulang').submit(function (e) {
			var id = $("input[name='nim_mhs']").val();
			var pass = $("input[name='pass_mhs']").val();
			if(id.trim()!=""){
				var form = document.createElement("form");
				var input = document.createElement("input");
				var input2 = document.createElement("input");
				
				form.action = base_url + 'info/akademik/daftar';
				form.method = "post"
				
				input.name = "nim_mhs";
				input.value = id;
				form.appendChild(input);
				
				input2.name = "pass_mhs";
				input2.value = pass;
				form.appendChild(input2);
				
				document.body.appendChild(form);
				form.submit();
			}
			else{
				alert("Lengkapi form terlebih dahulu");
			}
			e.preventDefault(); //STOP default action
		    return false;
		});
		
		$('#form-apply-sub').submit(function (e) {
			var id = $("input[name='nim_mhs']").val();
			var pass = $("input[name='tmp_id']").val();
			if(id.trim()!=""){
				var postData = new FormData($(this)[0]);
				$.ajax({
					url : base_url + 'page/submission',
					type: "POST",
					data : postData,
					async: false,
					success:function(msg) 
					{
						$("#form-apply").html(msg);
					},
				    cache: false,
					contentType: false,
					processData: false
				});
			}
			else{
				alert("Lengkapi form terlebih dahulu");
			}
			e.preventDefault(); //STOP default action
		    return false;
		});
		
		$("#form-apply-submit").submit(function(e){
			
			var nama_mhs		= $("input[name='nama_mhs']").val();
			var hidId_mhs		= $("input[name='hidId_mhs']").val();
			var hidId			= $("input[name='tmpid']").val();
			if(nama_mhs.trim()!=="" && hidId_mhs.trim()!==""){
				var postData = $("#form-apply-submit").serialize();
				$.ajax({
					url : base_url + 'page/save_submission',
					type: "POST",
					data : postData,
					async: false,
					success:function(msg) 
					{
						alert(msg);	
						$("#form-apply").show();						
						$("#form-apply").html(msg);
					},
				    cache: false,
					contentType: false,
					processData: false
				});
			}
			else{
				alert("Lengkapi form terlebih dahulu");
			}
			e.preventDefault(); //STOP default action
		    return false;
		});
		
		$("#form-mhs-daftar-ulang").submit(function(e){
			var alamatsurat		= $("textarea[name='alamat_surat']").val();
			var nama_ortu		= $("input[name='nama_ortu']").val();
			var nama_mhs		= $("input[name='nama_mhs']").val();
			var hidId_mhs		= $("input[name='hidId_mhs']").val();
			if(alamatsurat.trim()!=="" && nama_ortu.trim()!=="" && nama_mhs.trim()!=="" && hidId_mhs.trim()!==""){
				var postData = new FormData($(this)[0]);
				$.ajax({
					url : base_url + 'info/save_daftarulang',
					type: "POST",
					data : postData,
					async: false,
					success:function(msg) 
					{
						alert(msg);
						var form = document.createElement("form");
						var input = document.createElement("input");
						
						form.action = base_url + 'info/akademik/daftar';
						form.method = "post"
						
						input.name = "nim_mhs";
						input.value = hidId_mhs;
						form.appendChild(input);
						
						document.body.appendChild(form);
						form.submit();
					},
				    cache: false,
					contentType: false,
					processData: false
				});
			}
			else{
				alert("Lengkapi form terlebih dahulu");
			}
			e.preventDefault(); //STOP default action
		    return false;
		});
		
		$(".form_datetime").datepicker({
			format: 'yyyy-mm-dd',
			viewMode: 2
		});
		
		if($(".msg").length){
			setTimeout(function(){
				$(".msg").fadeOut();
			}, 2000);
		}
	}
});
