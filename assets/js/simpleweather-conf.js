$.simpleWeather({
                location: 'Malang',
                woeid: '56000382',
                unit: 'c',
                success: function(weather) {
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }

                    today = convert_month(mm) + ' ' + dd + ' ' + yyyy;
                    html = '<h5 class="w-today">'+today+'</h5>';
                    html += '<h4 class="w-location">' + weather.city + ' ' + weather.temp + '&deg;' + weather.units.temp + '</h4>';
                    html += '<h3 class="w-icon"><i class="icon-weather icon-weather-' + weather.code + '"></i></h3>';
					html += '<div class="w-currently">' + weather.currently + '</div>';
                    
                    $(".weather").html(html);
                },
                error: function(error) {
                    $(".weather").html('<p>' + error + '</p>');
                }
            });
            
            
function convert_month(number) {
    switch (number) {
        case 0:
            return 'Januari';
            break;
        case 1:
            return 'Februari';
            break;
        case 2:
            return 'Maret';
            break;
        case 3:
            return 'April';
            break;
        case 4:
            return 'Mei';
            break;
        case 5:
            return 'Juni';
            break;
        case 6:
            return 'Juli';
            break;
        case 7:
            return 'Augustus';
            break;
        case 8:
            return 'September';
            break;
        case 9:
            return 'Oktober';
            break;
        case 10:
            return 'November';
            break;
        case 11:
            return 'Desember';
            break;

    }
} 