$(document).ready(function(){

	$(".e9").select2();
	$("#tbl_penilaian_wrapper").hide();
	$("#pilih_thn").change(function(){
		$("#daftar_nilai").empty();
		$("#penilaian").empty();
		$("#tbl_penilaian_wrapper").hide();
		$("#pilih_kategori").select2('val','0');
		$("#pilih_mk").select2('val','0');
		$("#pilih_kelas").select2('val','0');
			// alert();
		var thn = $(this).val();
		var unit = $("#unitid").val();
		$.ajax({
			url : base_apps + "unit/get_mk_nilai",
			type : "POST",
			data : $.param({
					thn : thn,
					unit : unit
			}),
			dataType : "html",
			success:function(data) 
			{
				// alert(data);
				$("#pilih_mk").html(data);
			}
		});
	});
	
	$("#pilih_kategori").change(function(){
		$("#daftar_nilai").empty();
		$("#penilaian").empty();
		$("#tbl_penilaian_wrapper").hide();
		$("#pilih_mk").select2('val','0');
		$("#pilih_kelas").select2('val','0');
	});
	
	$("#pilih_mk").change(function(){
		$("#daftar_nilai").empty();
		$("#penilaian").empty();
		$("#tbl_penilaian_wrapper").hide();
		$("#pilih_kelas").select2('val','0');
		
		var thn 		= $("#pilih_thn").val();
		var unit 		= $("#unitid").val();
		var kategori 	= $("#pilih_kategori").val();
		var mk			= $(this).val();
		$.ajax({
			url : base_apps + "unit/get_penilaian",
			type : "POST",
			data : $.param({
					thn : thn,
					unit : unit,
					mk : mk,
					kategori : kategori
			}),
			dataType : "html",
			success:function(data) 
			{
				$("#pilih_kelas").html(data);
				// alert(data);
			}
		});
	});
	
	$("#pilih_kelas").change(function(){
		$("#daftar_nilai").empty();
		$("#penilaian").empty();
		$("#tbl_penilaian_wrapper").hide();
		
		var thn 		= $("#pilih_thn").val();
		var unit 		= $("#unitid").val();
		var kategori 	= $("#pilih_kategori").val();
		var mk			= $("#pilih_mk").val();
		var kelas		= $(this).val();
		if(kategori=="proses"){
			$.ajax({
				url : base_apps + "unit/get_penilaian",
				type : "POST",
				data : $.param({
						thn : thn,
						unit : unit,
						mk : mk,
						kategori : kategori,
						kelas : kelas
				}),
				dataType : "json",
				success:function(data) 
				{
					//alert(JSON.stringify(data));
					if(data!==""){
						$("#tbl_penilaian_wrapper").show();
						var table = $('#tbl_penilaian').DataTable();
						var x=1;
						$.each(data, function(i, f){
							table.row.add([ "<a href='javascript:' onclick='get_nilai(\""+f.nilaiid+"\")'>"+x+". "+f.judul+"</a>" ])
							    .draw()
							    .node();
							x++;
					    });
					}
					else{
						alert("No Data");
					}
				}
			});
		}
		else{
			$.ajax({
				url : base_apps + "unit/get_nilai",
				type : "POST",
				data : $.param({
						thn : thn,
						unit : unit,
						mk : mk,
						kategori : kategori,
						kelas : kelas
				}),
				dataType : "html",
				success:function(data) 
				{
					$("#daftar_nilai").html(data);
				}
			});
		}
	});
});

function get_nilai(nilai){
	$.ajax({
		url : base_apps + "unit/get_nilai",
		type : "POST",
		data : $.param({
				nilai : nilai
		}),
		dataType : "html",
		success:function(data) 
		{
			$("#daftar_nilai").html(data);
		}
	});
};
