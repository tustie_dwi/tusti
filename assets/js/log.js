function doView(id){
	var file_id_ = id;
	var action_ = "view";
	var url = base_url + 'module/akademik/file/log_view';

	$.ajax({
		type :"POST",
		dataType : "HTML",
		url : url,
		async: false,
		data : $.param({
			file_id : file_id_,
			action : action_
		}),
		success : function(msg){
			//alert(msg);
		}
	});
};

function doDownload(id) {
	var file_id_ = id;
	var action_ = "download";
	var url = base_url + 'module/akademik/file/log_download';
	//alert(file_id_);
	// alert(action_);
	// alert(URL);
	$.ajax({
		type :"POST",
		dataType : "HTML",
		url : url,
		async: false,
		data : $.param({
			file_id : file_id_,
			action : action_
		}),
		success : function(msg){
			//alert(msg);
		}
	});
};

function doDownloadTugas(id) {
	var file_id_ = id;
	var action_ = "download_tugas";
	var url = base_url + 'module/akademik/file/log_download';
	//alert(file_id_);
	// alert(action_);
	// alert(URL);
	$.ajax({
		type :"POST",
		dataType : "HTML",
		url : url,
		async: false,
		data : $.param({
			file_id : file_id_,
			action : action_
		}),
		success : function(msg){
			//alert(msg);
		}
	});
};