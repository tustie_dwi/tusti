$(function() {	
	$('#writeTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
  });
  
   $('#myModal').modal('hide', function(){
	
   });
   
   
   $('#registerModal').modal({
		show: true,
		remote: base_url + "/register/msg"
	});
   
   
   $("[data-toggle=tooltip]").tooltip();
  
	$(".pop").each(function() {
		var $pElem= $(this);
		$pElem.popover(
			{
			  title: getPopTitle($pElem.attr("id")),
			  content: getPopContent($pElem.attr("id")),
			  trigger:'hover'
			}
		);
	});
					
	function getPopTitle(target) {
		return $("#" + target + "_content > div.popTitle").html();
	};
			
	function getPopContent(target) {
		return $("#" + target + "_content > div.popContent").html();
	};  
	
		
	$( "#date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$( ".date" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	
	$(".cmbmulti").select2();	
	$("#cmbmulti").select2();
	
	$("#approve").click(function(){
	  if ($('.pic').attr('disabled') == "disabled" ) {
		$('.pic').removeAttr('disabled'); 
      }else {
		$('.pic').removeAttr('disabled'); 
      }
	});
	
	$(".reject").click(function(){
	  if ($('.pic').attr('disabled') == "disabled" ) {
		$(".pic").attr('disabled', 'disabled');    
      }else {
		$(".pic").attr('disabled', 'disabled');      
      }
	});
	$("#tabs").tabs();
		$("#accordion").accordion({autoHeight: true,fillSpace: false});
		$("#tabs-child").tabs();
		$('#example').dataTable();		
		$('.datatable').dataTable();	
		$(".typeahead").typeahead();	
	
			
});

function optionFormatResult(mhs) {	
        var markup = "<table class='movie-result'><tr>";       
        markup += "<td>"+mhs.text+"";         
        markup += "</td></tr></table>"

        return markup;
    }

function optionFormatSelection(mhs) {	
	return mhs.text;
}
	
	
