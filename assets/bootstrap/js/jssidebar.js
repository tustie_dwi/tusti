$('[data-toggle=collapse]').click(function(){
  	$(this).find("i").toggleClass("icon-chevron-right icon-chevron-down");
  
});

$('.collapse').on('show', function (e) {
  
  	$('.collapse').each(function(){
      if ($(this).hasClass('in')) {
          $(this).collapse('toggle');
      }
    });
  
})