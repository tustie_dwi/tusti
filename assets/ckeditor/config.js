/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'en';
	// config.uiColor = '#AADC6E';
	//config.extraPlugins = 'youtube';
	
	/*   config.filebrowserBrowseUrl = 'http://adl.ptiik.ub.ac.id/kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = 'http://adl.ptiik.ub.ac.id/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = 'http://adl.ptiik.ub.ac.id/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = 'http://adl.ptiik.ub.ac.id/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = 'http://adl.ptiik.ub.ac.id/kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = 'http://adl.ptiik.ub.ac.id/kcfinder/upload.php?type=flash';*/
   config.filebrowserBrowseUrl = '../../../../../../kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = '../../../../../../kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = '../../../../../../kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = '../../../../../../kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = '../../../../../../kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = '../../../../../../kcfinder/upload.php?type=flash';
  /* config.toolbar = [
	['Preview', 'Print','Save'], ['Bold','Italic','Strike','-','Subscript','Superscript'],['NumberedList','BulletedList','-','Blockquote'],['JustifyLeft','JustifyCenter','JustifyBlock','JustifyRight'],
	['Link','Unlink','-','PageBreak'],['Find','Replace','-','Maximize'],['Source'],
	'/',
	['Format', 'Styles','Font','FontSize','Bold','Italic', 'Underline','-','TextColor','HorizontalRule'],['PasteText','PasteFromWord','-','RemoveFormat'],['Table', 'Image','Youtube','Mathjax','SpecialChar'],['Outdent','Indent'],
	['Undo','Redo']
	] ;
  */
  
  config.toolbarGroups = [
	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
	'/',
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup', 'list','align','indent' ] },
	{ name: 'paragraph', groups: [  'blocks','insert' ] },
	'/',
	{ name: 'styles' },
	{ name: 'colors' },
	{ name: 'tools' },{ name:'links'}
];
};
