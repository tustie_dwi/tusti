var content;

$(document).ready(function() {
	//alert("aa");
	$.ajax({
		url : base_url + "module/master/conf/get_mhs",
		type : "POST",
		data : $.param({
			init : 'ok'
		}),
		beforeSend : function() {
		},
		complete : function() {
		},
		success : function(data_) {
					
			var angkatan=[];
			var data_alumni = JSON.parse(data_);
			
			var val_s= 0;
			var val_i =0;
			var val_si=0;

		
			$.each(data_alumni, function(tahun, a){					
				$.each(a, function(prodi, data_detail){
					$.each(data_detail, function(id, value){					
						switch(prodi){
							case 'SISKOM':
								 val_s= value.jml;											
							break;
							case 'ILKOM':										
								 val_i =value.jml;											
							break;
							case 'SI':
								 val_si=value.jml;										
							break;
						}											
					});
				});
				angkatan.push({
					Tahun : tahun, 
					ILKOM:val_i,SI:val_si,SISKOM:val_s
				})				
			});
			new Morris.Bar({
			  element: 'graphmhs',
			  data:  angkatan,
			  xkey: 'Tahun',
			  ykeys: ['ILKOM', 'SI', 'SISKOM'],
			  labels: ['ILKOM', 'SI', 'SISKOM']
			});
		},
		error : function() {
			showGrowl('Gagal mengambil data', 'danger');
		}
	});
	
	$.ajax({
		url : base_url + "module/master/conf/get_mhs_lulus",
		type : "POST",
		data : $.param({
			init : 'ok'
		}),
		beforeSend : function() {
		},
		complete : function() {
		},
		success : function(data_) {

			var tahun_=[];
			var data_lulus = JSON.parse(data_);
			
			var val_s= 0;
			var val_i =0;
			var val_si=0;
			
			$.each(data_lulus, function(tahun, a){	
				var total=0;
				var str = '<tr>';
				$.each(a, function(prodi, data_detail){
					$.each(data_detail, function(id, value){					
						switch(prodi){
							case 'SISKOM':
								 val_s= value.jml;								
							break;
							case 'ILKOM':							
								 val_i =value.jml;								
							break;
							case 'SI':
								 val_si=value.jml;										
							break;
						}											
					});
				});	
				total = parseInt(val_s)+ parseInt(val_si)+parseInt(val_i);
				str+= '<td>'+ tahun+'</td><td>'+val_i +'</td><td>'+val_s +'</td><td>'+val_si+'</td><td>'+total+'</td>';	
				
				tahun_.push({
							Tahun : tahun, 
							ILKOM:val_i,SI:val_si,SISKOM:val_s
						})	
				str+= '</tr>';
				$("#content-graph-wrap").append(str);
			});
			
			new Morris.Bar({
			  element: 'graphmhslulus',
			  data:  tahun_,
			  xkey: 'Tahun',
			  ykeys: ['ILKOM', 'SI', 'SISKOM'],
			  labels: ['ILKOM', 'SI', 'SISKOM']
			});
		},
		error : function() {
			showGrowl('Gagal mengambil data', 'danger');
		}
	});

});